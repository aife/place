import {Utilisateur} from './utilisateur';

export interface CreationAudit {
  utilisateur?: Utilisateur;
  date?: Date;
}

export interface UpdateAudit {
  utilisateur?: Utilisateur;
  date?: Date;
  commentaire?: string;
}

