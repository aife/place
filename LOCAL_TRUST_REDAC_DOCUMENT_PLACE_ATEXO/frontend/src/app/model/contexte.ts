import { Environnement } from './environnement';
import { Consultation } from './consultation';
import { UUID } from './uuid';

export type Plateforme = string;

export interface Contexte {
    uuid?: UUID;
    uri: string;
    consultation?: Consultation;
    plateforme?: Plateforme;
    environnement?: Environnement;
    reference?: string;
}
