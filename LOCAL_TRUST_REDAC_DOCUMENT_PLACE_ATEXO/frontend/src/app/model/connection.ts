import { Utilisateur } from './utilisateur';

export class Connection {
    public creation?: Date = new Date();
    public agent?: Utilisateur;
    public uuid?: string;
}

