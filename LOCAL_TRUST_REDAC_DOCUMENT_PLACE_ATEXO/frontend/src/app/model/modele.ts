import {Referentiel} from './referentiel';
import {UUID} from './uuid';
import {Utilisateur} from './utilisateur';
import {CreationAudit, UpdateAudit} from "./audit";

export enum CategorieModele {
  MODELE_PLATEFORME = 'MODELE_PLATEFORME',
  MODELE_DAJ = 'MODELE_DAJ',
  DOCUMENT_LIBRE = 'DOCUMENT_LIBRE',
  DOCUMENT_CANEVAS = 'DOCUMENT_CANEVAS'
}

export enum Niveau {
  AGENT = 'AGENT',
  ENTITE_ACHAT = 'ENTITE_ACHAT',
  ORGANISME = 'ORGANISME',
  PLATEFORME = 'PLATEFORME'
}

export interface Modele {
  id: number;
  uuid: UUID;
  actif?: boolean;
  type?: Referentiel;
  libelle?: string;
  nomFichier?: string;
  administrable?: boolean;
  niveau?: Niveau;
}

export interface Gabarit {
  id?: number;
  uuid?: UUID;
  nom?: string;
  niveau?: Referentiel;
  modele?: Modele;
  actif?: boolean;
  creation?: CreationAudit;
  modification?: UpdateAudit;
}

export interface Surcharge {
  ordre: number;
  niveau: Niveau;
  libelle: string;
  gabarit: Gabarit;
}

