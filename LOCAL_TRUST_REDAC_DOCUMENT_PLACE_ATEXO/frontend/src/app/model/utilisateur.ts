import { Organisme } from './organisme';
import { Service } from './service';
import { UUID } from './uuid';

export interface Utilisateur {
    uuid?: UUID;
    nom?: string;
    prenom?: string;
    email?: string;
    service?: Service;
    habilitations?: string[];
}
