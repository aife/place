import { SweetAlertIcon, SweetAlertOptions, SweetAlertResult } from 'sweetalert2';

export type Icone = SweetAlertIcon;
export type Notification = SweetAlertOptions;
export type ResultationNotification = SweetAlertResult;
