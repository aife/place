export interface Version {
    build?: Build;
}

export interface Build {
    version?: string;
    time?: Date;
    codename?: String;
}
