import { Environnement } from './environnement';
import { UUID } from './uuid';

export interface Organisme {
    uuid?: UUID;
    libelle?: string;
    acronyme?: string;
    identifiantExterne?: string;
    environnement?: Environnement;
}
