import { UUID } from './uuid';

export interface Environnement {
    id?: number;
    uuid?: UUID;
    client?: string;
    plateforme?: string;
    urlLogo?: string;
}
