export interface Message {
    type: 'info' | 'warn' | 'error';
    value: string;
}
