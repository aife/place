import { Environnement } from './environnement';

export interface Parametre<T> {
    environnement: Environnement;
    cle: string;
    valeur: T;
}
