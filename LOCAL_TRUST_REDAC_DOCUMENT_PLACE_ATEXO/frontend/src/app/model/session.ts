import { Utilisateur } from './utilisateur';
import { Contexte } from './contexte';
import { Version } from './version';
import { Niveau } from './modele';

export class Session {
    public creation?: Date = new Date();
    public token?: Token;
    public contexte?: Contexte;
    public agent?: Utilisateur;
    public redirection?: string;
    public credential?: Version;
    public niveau?: Niveau;
}

export interface Token {
    access_token?: string;
    refresh_token?: string;
}

