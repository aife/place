import { UUID } from './uuid';
import { Organisme } from './organisme';

export interface Service {
  uuid?: UUID;
  libelle?: string;
  identifiantExterne?: string;
  organisme?: Organisme;
}
