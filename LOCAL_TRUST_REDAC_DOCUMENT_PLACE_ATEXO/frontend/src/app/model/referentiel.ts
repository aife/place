export interface Referentiel {
    id: number;
    type?: string;
    code?: string;
    libelle?: string;
    description?: string;
    ordre: number;
    parent?: Referentiel;
}
