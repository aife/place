import { Procedure } from './procedure';
import { Organisme } from './organisme';
import { Service } from './service';
import { Lot } from './lot';

export interface Consultation {
    objet?: string;
    intitule?: string;
    procedure?: Procedure;
    organisme?: Organisme;
    service?: Service;
    dateRemisePlis?: Date;
    reference?: string;
    numero?: string;
    mps?: boolean;
    lots?: Lot[];
}
