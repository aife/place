import { Procedure } from './procedure';
import { NaturePrestation } from './nature-prestation';

export interface StatutCanevas {
    id: number;
    libelle: string;
}

export interface Libelle {
  libelle: string;
}

export interface Canevas {
    id: number;
    reference: string;
    auteur: string;
    statut: StatutCanevas;
    titre: string;
    naturePrestation: NaturePrestation;
    procedures: Procedure[];
    contrats: Libelle[];
    dateCreation: Date;
    dateModification: Date;
    version: string;
    selection: boolean;
}
