import { Environnement } from './environnement';
import { UUID } from './uuid';

export interface Destinataire {
    uuid?: UUID;
    nom?: string;
    prenom?: string;
    email?: string;
    service?: string;
    environnement?: Environnement;
}
