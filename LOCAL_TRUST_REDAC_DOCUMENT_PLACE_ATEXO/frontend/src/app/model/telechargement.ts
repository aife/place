import { DropzoneFile } from 'dropzone';
import { UUID } from './uuid';

export interface Telechargement {
    uuid?: UUID;
    taille?: number;
    nom?: string;
    file?: DropzoneFile;
    progression?: number;
    erreur?: string;
    extension?: string;
}
