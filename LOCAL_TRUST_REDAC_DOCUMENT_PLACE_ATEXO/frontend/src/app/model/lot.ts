import { CPV } from './CPV';

export interface Lot {
    id?: number;
    numero?: string;
    libelle?: string;
    description?: string;
    cpv?: CPV[];
}
