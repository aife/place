import {Contexte} from './contexte';
import {Modele} from './modele';
import {Canevas} from './canevas';
import {Comparator} from '@patterns';
import {CreationAudit, UpdateAudit} from './audit';
import { Lot } from './lot';

export enum CategorieDocument {
  DOCUMENT_CANEVAS = 'DOCUMENT_CANEVAS',
  MODELE_DAJ = 'MODELE_DAJ',
  MODELE_PLATEFORME = 'MODELE_PLATEFORME',
  DOCUMENT_LIBRE = 'DOCUMENT_LIBRE'
}

export enum StatutDocument {
  VALIDE = 'VALIDE', DEMANDE_VALIDATION = 'DEMANDE_VALIDATION', BROUILLON = 'BROUILLON', ERREUR = 'ERREUR'
}

export enum ActionDocument {
  AFFICHAGE = 'AFFICHAGE',
  DEMANDE_OUVERTURE = 'DEMANDE_OUVERTURE',
  OUVERTURE = 'OUVERTURE',
  ANNULATION = 'ANNULATION',
  FERMETURE_SANS_MODIFICATION = 'FERMETURE_SANS_MODIFICATION',
  INCONNU = 'INCONNU',
  TELECHARGEMENT = 'TELECHARGEMENT',
  NOTIFICATION = 'NOTIFICATION',
  DUPLICATION = 'DUPLICATION',
  FERMETURE_AVEC_MODIFICATION = 'FERMETURE_AVEC_MODIFICATION',
  ENREGISTREMENT = 'ENREGISTREMENT',
  CREATION = 'CREATION',
  MODIFICATION_DU_NOM = 'MODIFICATION_DU_NOM',
  DEMANDE_VALIDATION = 'DEMANDE_VALIDATION',
  REFUS_DEMANDE_VALIDATION = 'REFUS_DEMANDE_VALIDATION',
  INVALIDATION = 'INVALIDATION',
  VALIDATION = 'VALIDATION'
}

export function DEFAULT_SORT(d1: Document, d2: Document) {
  return new Comparator<Date>().comparing(d2.creation?.date, d1.creation?.date);
}

export interface Document {
  id?: number;
  actif?: boolean;
  nom?: string;
  statut?: StatutDocument;
  libelle?: string;
  modele?: Modele;
  uuid?: string;
  progression?: number;
  taille?: number;
  contexte?: Contexte;
  action?: ActionDocument;
  canevas?: Canevas;
  lot?: Lot;
  ouvert?: boolean;
  extension?: string;
  menu?: boolean;
  creation?: CreationAudit;
  modification?: UpdateAudit;
}
