import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentListComponent, GabaritListComponent } from '@components';

const ROUTES: Routes = [
    { path: 'documents', component: DocumentListComponent },
    { path: 'gabarits', component: GabaritListComponent },
    { path: '**', redirectTo: '/documents' }
];

@NgModule( {
    imports: [RouterModule.forRoot( ROUTES )],
    exports: [RouterModule]
} )
export class AppRoutingModule {
}
