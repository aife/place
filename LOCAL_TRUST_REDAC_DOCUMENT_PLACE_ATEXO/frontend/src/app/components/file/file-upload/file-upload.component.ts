import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import Dropzone from 'dropzone/dist/dropzone';
import { DocumentService, FichierService } from '@services';
import { Telechargement } from '@model';
import { SessionManager } from '@managers';

@Component({
  selector: 'atexo-redacdocument-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements AfterViewInit, OnDestroy {
  // Inputs
  @Input() public maximumFile = 1;
  @Input() public maximumSize = 10;
  @Input() public mimetypes = ['docx', 'xlsx'];
  // Outputs
  @Output() public ajout = new EventEmitter<Telechargement>();
  @Output() public suppression = new EventEmitter<Telechargement>();
  // Attributs
  public dropzone: Dropzone;
  public telechargements: Telechargement[] = [];
  public erreur: string;

  constructor(
    private documentService: DocumentService,
    private fichierService: FichierService,
    private sessionManager: SessionManager
  ) {
  }

  public ngAfterViewInit(): void {
    if (!this.dropzone) {
      this.generer();
    }
    this.dropzone.enable();
    this.dropzone.removeAllFiles();

  }

  public ngOnDestroy(): void {
    this.dropzone.destroy();
    this.dropzone = null;
  }

  public generer(): void {
    this.dropzone = new Dropzone(`#file-upload-form`, {
      url: '/api-redac-document/fichiers',
      acceptedFiles: this.mimetypes.map(extension => `.${extension}`).join(','),
      maxFiles: this.maximumFile,
      clickable: '#dz-button',
      dictDefaultMessage: 'Déposez ici les fichiers pour les joindre',
      dictFallbackMessage: 'Votre navigateur ne supporte pas encore le glisser/déposer.',
      dictFileTooBig: 'Votre pièce jointe dépasse la taille maximale autorisée : {{maxFilesize}} Mio',
      dictResponseError: 'Le téléversement échoué.',
      dictCancelUpload: 'Annuler',
      dictCancelUploadConfirmation: `Voulez vous vraiment annuler l'ajout du document ?`,
      dictRemoveFile: '<i class=\'fa fa-trash\'></i>',
      dictRemoveFileConfirmation: null,
      dictInvalidFileType: `Vous avez choisi un fichier dont l'extension n'est pas autorisé. IL doit avoir l'une des extensions suivantes : ${this.mimetypes.join(' ou ')}`,
      dictMaxFilesExceeded: `Vous ne pouvez ajouter qu'un seul fichier`,
      previewTemplate: '<div style="display:none"></div>',
      previewsContainer: '#uploadContainer',
      addRemoveLinks: false,
      paramName: 'uploadfile',
      maxFilesize: this.maximumSize,
      maxThumbnailFilesize: 1,
      error: (file, message: string) => {
        // si info taille max dépassée par le back ou par dictFileTooBig du front
        if (message.includes('Maximum upload size exceeded') || message.includes('Votre pièce jointe dépasse la taille')) {
          this.erreur = `Le document importé dépasse la taille maximale autorisée (${this.maximumSize}Mo)`;
        } else {
          this.erreur = 'Impossible d\'ajouter le fichier';
        }
        this.dropzone.reset();

        const document = this.telechargements.find(d => d.file === file);

        if (document) {
          document.uuid = null;
        }
      },
      headers: {
        'Authorization': `bearer ${this.sessionManager?.session?.token.access_token}`,
        'Accept': `application/atexo.redacdocument.v2+json`
      }
    });

    this.dropzone.on('success', (file, resp: string) => {
      const response: Telechargement = JSON.parse(resp);

      this.erreur = null;

      const document: Telechargement = {
        file: file,
        progression: 100,
        ...response
      };

      this.telechargements.push(document);
      this.ajout.emit(document);
    });

    this.dropzone.on('addedfile', _ => this.telechargements.find(d => d.file));

    this.dropzone.on('uploadprogress', (file, progress) => {
      const document = this.telechargements.find(d => d.file === file);
      if (document) {
        document.progression = progress;
      }
    });
  }

  public supprimer(telechargement: Telechargement): void {
    // si la pièce jointe est correctement renseignée
    if (telechargement.file) {
      this.dropzone.removeFile(telechargement.file);
    }

    const suppression = () => {
      this.dropzone.removeFile(telechargement.file);
      this.telechargements = this.telechargements.filter(d => d !== telechargement);
      this.suppression.emit(telechargement);
    };

    if (telechargement.uuid) {
      this.fichierService.supprimer(telechargement.uuid).subscribe(suppression);
    } else {
      suppression();
    }
  }

  public telecharger(telechargement: Telechargement): void {
    this.fichierService.telecharger(telechargement.uuid, this.documentService.formater({
      nom: telechargement.nom,
      extension: telechargement.extension
    }), telechargement.extension);
  }
}
