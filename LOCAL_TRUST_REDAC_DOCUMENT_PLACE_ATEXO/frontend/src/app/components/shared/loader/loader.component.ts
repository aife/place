import { Component, Input } from '@angular/core';

@Component( {
    selector: 'atexo-redacdocument-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
} )
export class LoaderComponent {

    @Input() public message = 'Chargement en cours, merci de patienter';
}
