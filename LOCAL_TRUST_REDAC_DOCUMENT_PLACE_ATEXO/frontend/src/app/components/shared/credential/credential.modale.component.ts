import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { Build } from '@model';
import { SessionManager } from '@managers';
import { APP_BASE_HREF } from '@angular/common';
import { environment } from '@environments/environment.prod';

@Component( {
    selector: 'atexo-redacdocument-credential-modale',
    templateUrl: './credential.modale.component.html',
    styleUrls: ['./credential.modale.component.scss']
} )
export class CredentialModaleComponent implements OnInit {
    // Outputs
    @Output() public fermeture = new EventEmitter<void>();
    // Attributs
    public build: Build;

    constructor(
        private sessionManager: SessionManager,
        @Inject( APP_BASE_HREF ) private baseHref: string
    ) {
    }

    public close(): void {
        this.fermeture.emit();
    }

    public ngOnInit(): void {
        this.build = this.sessionManager.session?.credential?.build;
    }

    public contextualiser(): string {
        let base = this.baseHref;
        if ( environment.production ) {
            base = '/front/';
        }
        let logoUrl =  `${base}assets/img/atexo.png`;
        if (!!this.sessionManager.session && !!this.sessionManager.session.contexte &&
          !!this.sessionManager.session.contexte.environnement &&
          !!this.sessionManager.session.contexte.environnement.urlLogo) {
          return this.sessionManager.session.contexte.environnement.urlLogo;
        } else {
          return logoUrl;
        }

    }
}

