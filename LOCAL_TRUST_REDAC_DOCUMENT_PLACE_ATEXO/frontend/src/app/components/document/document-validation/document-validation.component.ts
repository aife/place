import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { DocumentService, NotificationService } from '@services';
import { AlertModule } from 'ngx-bootstrap/alert';

export interface Validation {
    document?: Document;
    extension?: string;
}

@Component( {
    selector: 'atexo-redacdocument-document-validation',
    templateUrl: './document-validation.component.html',
    styleUrls: ['./document-validation.component.scss']
} )
export class DocumentValidationComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public validation = new EventEmitter<Validation>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public acceptation = new EventEmitter<Validation>();

    constructor(
        private documentService: DocumentService,
        private notificationService: NotificationService
    ) {
    }

    public valider( type: string ) {
        this.acceptation.emit( { document: this.document, extension: type } );

        this.notificationService.info(
            {
                html: `<div><span>Le document <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours de validation</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes.</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );

        console.debug( `Sauvegarde de la validation du document` );

        this.documentService.valider( this.document, type )
            .subscribe(
                document => this.validation.emit( { document: this.document, extension: type } ),
                _ => this.erreur.emit( `Erreur lors de la validation du document <strong>${this.document?.nom}</strong>` )
            );
    }
}
