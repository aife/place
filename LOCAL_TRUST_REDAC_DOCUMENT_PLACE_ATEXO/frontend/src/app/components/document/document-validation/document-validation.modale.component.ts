import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { Validation } from '@components/document';

@Component( {
    selector: 'atexo-redacdocument-document-validation-modale',
    templateUrl: './document-validation.modale.component.html',
    styleUrls: ['./document-validation.modale.component.scss']
} )
export class DocumentValidationModaleComponent {
    @Input() public document: Document;
    @Output() public validation = new EventEmitter<Validation>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public fermeture = new EventEmitter<void>();
}
