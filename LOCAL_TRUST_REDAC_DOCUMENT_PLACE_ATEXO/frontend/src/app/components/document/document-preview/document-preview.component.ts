import { Component, Input } from '@angular/core';
import { Document } from '@model';
import { DocumentService } from '@services';
import { Observable } from 'rxjs';

@Component( {
    selector: 'atexo-redacdocument-document-preview',
    templateUrl: './document-preview.component.html',
    styleUrls: ['./document-preview.component.scss']
} )
export class DocumentPreviewComponent {
    // Attributs
    public view: Observable<string>;

    constructor(
        private documentService: DocumentService
    ) {
    }

    @Input() set document( document: Document ) {
        if ( document ) {
            this.view = this.documentService.permalien( document );
        }
    }
}
