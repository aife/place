import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';

@Component( {
    selector: 'atexo-redacdocument-document-preview-modale',
    templateUrl: './document-preview.modale.component.html',
    styleUrls: ['./document-preview.modale.component.scss']
} )
export class DocumentPreviewModaleComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
}
