import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Destinataire, Document, Utilisateur} from '@model';
import {DocumentService, NotificationService, UtilisateurService} from '@services';
import {Observable, Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {Matcher} from '@patterns';
import {SessionManager} from '@managers';
import {HttpErrorResponse} from '@angular/common/http';

class RecipientMatcher implements Matcher<Destinataire> {
    match( query: string, item: Destinataire ): boolean {
        console.log( `Searching recipients with query '${query}'` );

        const safequery = query ? query.toLowerCase() : '';

        return item.nom.toLowerCase().includes( safequery ) || item.prenom.toLowerCase().includes( safequery ) || item.email.toLowerCase().includes( safequery ) || item.service.toLowerCase().includes( safequery );
    }
}

@Component( {
    selector: 'atexo-redacdocument-broadcast',
    templateUrl: './broadcast.component.html',
    styleUrls: ['./broadcast.component.scss']
} )
export class BroadcastComponent {
    // Outputs
    @Output() public envoi = new EventEmitter<Utilisateur[]>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public acceptation = new EventEmitter<Utilisateur[]>();
    // Attributs
    public destinataires: Destinataire[] = [];
    public copie: boolean;
    private _document: Document;
    public view: Observable<string>;
    public clients: Destinataire[] = [];
  public filteredClients: Destinataire[] = [];
    public requete = new Subject<string>();
  searchDelay = 500;

    constructor(
        private documentService: DocumentService,
        private sessionManager: SessionManager,
        private utilisateurService: UtilisateurService,
        private notificationService: NotificationService) {
      this.requete.pipe(debounceTime(this.searchDelay)).subscribe((term) => {
            console.debug( `Le terme de recherche est '${term}'` );
        this.filteredClients = this.rechercher(term);
        } );

    }

    private rechercher( query: string ): Destinataire[] {
        return this.clients.filter( ( recipient: Destinataire ) => new RecipientMatcher().match( query, recipient ) );
    }

    @Input() set document( value: Document ) {
        if ( value ) {
            this._document = value;

            this.view = this.documentService.permalien( this._document );

            this.utilisateurService.rechercher().subscribe(
                ( destinataires: Utilisateur[] ) => {
                    this.clients = destinataires.map(
                        destinataire => ({
                            nom: destinataire.nom,
                            prenom: destinataire.prenom,
                            email: destinataire.email,
                            uuid: destinataire.uuid,
                            service: destinataire.service?.libelle,
                            environnement: destinataire.service?.organisme?.environnement
                        })
                    );
                },
                ( response: HttpErrorResponse) => {
                    console.log( `Erreur lors de la récupération des utilisateurs. Le message est = '${response.error}'` );

                    this.clients = [];

                    this.erreur.emit('Erreur lors de la récupération des destinataires potentiels.');
                }, () => {
                this.filteredClients = [...this.clients];
                    console.log( `Destinataires potentiels = `, this.clients );
                }
            );
        }
    }

    public get document(): Document {
        return this._document;
    }


    public recherche( termes: string ) {
        console.log( `Termes de recherche = `, termes );

        if ( termes.empty() ) {
            this.requete.next( termes );
        }
    }

    public denombrer(): number {
        return this.destinataires.length + (this.copie ? 1 : 0);
    }

    public envoyer(): void {
        const moi = this.sessionManager?.session?.agent;
        const destinataires = [];

        destinataires.push( ...this.destinataires );

        if ( this.copie ) {
            destinataires.push( {
                nom: moi.nom,
                prenom: moi.prenom,
                email: moi.email,
                uuid: moi.uuid,
                service: moi.service?.libelle,
                environnement: this.sessionManager?.session?.contexte?.environnement
            } );
        }

        this.acceptation.emit(destinataires);

        this.documentService.notifier( this.document, destinataires ).subscribe( ( utilisateurs: Utilisateur[] ) => {
                this.envoi.emit( utilisateurs );

                let message;

                if ( utilisateurs.filled() ) {
                    message = `${utilisateurs.length} destinataires ${this.copie ? '(avec vous en copie) ' : ''}ont correctement été notifiés de la validation`;
                } else {
                    message = `Le destinataire ${this.copie ? '(vous) ' : ''}a correctement été notifié de la validation`;
                }

                this.notificationService.info(
                    {
                        icon: 'success',
                        html: message,
                        timer: 10000
                    }
                );
            }, _ => {
                this.notificationService.erreur( { html: `<span class="swal-error-message">Envoi du message en erreur.</span><span>Merci de réessayer et de contacter votre support si le problème persiste.</span>` } );

                this.erreur.emit( 'Envoi du message en erreur' );
            }
        );
    }

    public valide(): boolean {
        return this.destinataires.filled() || this.copie;
    }
}

