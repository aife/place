import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document, Utilisateur } from '@model';


@Component( {
    selector: 'atexo-redacdocument-broadcast-modale',
    templateUrl: './broadcast.modale.component.html',
    styleUrls: ['./broadcast.modale.component.scss']
} )
export class BroadcastModaleComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public envoi = new EventEmitter<Utilisateur[]>();
    @Output() public erreur = new EventEmitter<string>();
}

