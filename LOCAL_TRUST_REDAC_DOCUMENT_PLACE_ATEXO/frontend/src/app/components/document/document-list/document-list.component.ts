import {Component, Inject, isDevMode, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  Application,
  DocumentService,
  ModaleService,
  NotificationService,
  ParametreService,
  StockageLocalService,
  StorageKey,
  WebsocketService
} from '@services';
import {
  CategorieModele,
  Consultation,
  DEFAULT_SORT,
  Document,
  Redirection,
  ResultationNotification,
  StatutDocument
} from '@model';
import {catchError, tap} from 'rxjs/operators';
import {TabDirective, TabsetComponent} from 'ngx-bootstrap/tabs';
import {SessionManager} from '@managers';
import {DocumentHandler} from '@events';
import {
  BroadcastModaleComponent,
  DocumentCreateModaleComponent,
  DocumentDuplicateModaleComponent,
  DocumentHistoryModaleComponent,
  DocumentInvalidationModaleComponent,
  DocumentUnsubmitModaleComponent,
  DocumentUpdateModaleComponent,
  DocumentValidationModaleComponent
} from '@components/document';
import {APP_BASE_HREF} from '@angular/common';
import {DocumentStatutLibelleMapper, DocumentStatutTypeMapper} from '@mappers';
import {DocumentPreviewModaleComponent} from '@components/document/document-preview/document-preview.modale.component';
import {EMPTY} from 'rxjs';

const TAB = 'tab';

class Panel {
  private statut: 'resume' | 'complet' = 'resume';
  private etat: 'ouvert' | 'ferme' = 'ferme';

  public agrandir() {
    this.statut = 'complet';
  }

  public resumer() {
    this.statut = 'resume';
  }

  public fermer(): void {
    this.etat = 'ferme';
  }

  public ouvrir(): void {
    this.etat = 'ouvert';
  }

  public estFerme(): boolean {
    return this.etat === 'ferme';
  }

  public estResume(): boolean {
    return this.statut === 'resume';
  }

  public estComplet(): boolean {
    return this.statut === 'complet';
  }

  public inverser(): void {
    if (this.estFerme()) {
      this.ouvrir();
    } else {
      this.fermer();
    }
  }
}

export class Tab {
  private readonly cle: StorageKey;

  constructor(
    private sessionManager: SessionManager,
    private stockageLocalService: StockageLocalService
  ) {
    this.cle = StorageKey.with
      .application(Application.REDACTION)
      .contexte(this.sessionManager?.session?.contexte)
      .cle(TAB)
      .build();
  }

  public changer(tab: TabDirective): void {
    this.stockageLocalService.ajouter(this.cle, tab.id);
  }

  public actif(tab: HTMLElement): boolean {
    const selected = this.stockageLocalService.recuperer<string>(this.cle, 'tab-documents-complex');

    return tab.id === selected;
  }
}

export type Timeout = NodeJS.Timeout;

export class Poller {
  private id: Timeout;

  public demarrer(callback: (...args: any[]) => void, rate: number): void {
    console.info('🚀 Démarrage du pooling');

    this.id = setInterval(callback, rate);
  }

  public arreter(): void {
    console.info('💤 Arrêt du pooling');

    if (this.id) {
      clearInterval(this.id);
    }
  }
}

@Component({
  selector: 'atexo-redacdocument-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy {
  // Childs
  @ViewChild('tabset', {static: false}) tabset?: TabsetComponent;
  // Attributs
  public Statuts = StatutDocument;
  public Math = Math;
  public Modeles = CategorieModele;
  public documents: Document[] = [];
  public consultation: Consultation;
  public panel: Panel;
  public tab: Tab;
  public poller: Poller;
  public modeleDaj: boolean;
  public modelePlateforme: boolean;
  public loading: boolean = false;

  constructor(
    private documentHandler: DocumentHandler,
    private documentService: DocumentService,
    private modaleService: ModaleService,
    private parametreService: ParametreService,
    private websocketService: WebsocketService,
    private notificationService: NotificationService,
    private stockageLocalService: StockageLocalService,
    private sessionManager: SessionManager,
    public documentStatutTypeMapper: DocumentStatutTypeMapper,
    public documentStatutLabelMapper: DocumentStatutLibelleMapper,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.tab = new Tab(sessionManager, stockageLocalService);
    this.panel = new Panel();
    this.poller = new Poller();
  }

  public ngOnInit(): void {
    if (!this.sessionManager?.session?.contexte?.consultation) {
      this.notificationService.fatal({html: `<span class="swal-error-message">Impossible de trouver la consultation, le contexte documentaire semble corrompu.</span><br/><span>Merci de contacter votre support.</span>`});

      return;
    }

    console.info(`Identifiant du contexte = ${this.sessionManager?.session?.contexte?.uuid}`);

    // Récupération de la consultation
    this.consultation = this.sessionManager?.session?.contexte?.consultation;
    this.loading = true;
    // Récupération des documents
    this.documentService.recuperer()
      .pipe(tap(documents => this.documents = documents.sort(DEFAULT_SORT)))
      .pipe(tap(_ =>
        this.websocketService.connect([{
          channel: `/topic/contextes/${this.sessionManager?.session?.contexte?.uuid}/documents/*`,
          handlers: [this.documentHandler],
          data: this.documents
        }])
      ))
      .subscribe(
        _ => {
          console.log('Liste des documents = ', this.documents);
          this.loading = false;
        },
        _ => {
          this.loading = false;
          this.notificationService.erreur(
            {html: `<span class="swal-error-message">Impossible de contacter le serveur d'édition de document. Il s'agit surement d'une indisponiblité temporaire.</span><span>Merci de prévenir votre support si le problème persiste.</span>`}
          );
        }
      );

    // Récupération des paramètres
    this.parametreService.recuperer()
      .subscribe(
        parameters => {
          const mode = parameters.find(parameter => parameter.cle === 'docgen.mode')?.valeur;
          this.modeleDaj = parameters.find(parameter => parameter.cle === 'modele.daj')?.valeur !== 'false';
          this.modelePlateforme = parameters.find(parameter => parameter.cle === 'modele.plateforme')?.valeur !== 'false';
          switch (mode) {
            case 'pooling':
              const rate = parameters.find(parameter => parameter.cle === 'docgen.pooling.rate')?.valeur;

              console.log('Pooling activé pour DOCGEN');

              this.poller.demarrer(() => this.documentService.recuperer()
                .pipe(
                  catchError(() => {
                    this.poller.arreter();

                    this.notificationService.erreur({
                      html: `<span class="swal-error-message">Impossible de contacter le serveur d'édition de document. Il s'agit surement d'une indisponiblité temporaire.</span><span>Merci de prévenir votre support si le problème persiste.</span>`
                    });

                    return EMPTY;
                  })
                )
                .subscribe(value => {this.documents = value}), rate);

              break;
            case 'callback' :
            default:
              console.log(`Pas de pooling, la communication avec DOCGEN se fait par ${mode}`);

              break;
          }
        }
      );
  }

  public ngOnDestroy(): void {
    this.poller.arreter();
  }

  public editer(document: Document): void {
    const modale = this.modaleService.afficher(DocumentUpdateModaleComponent);
    modale.content.categorie = document?.modele?.type?.parent?.code;
    modale.content.document = document;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));
  }

  public supprimer(document: Document): void {
    this.notificationService.confirmation({
        html: `<strong>Êtes de sûr de vouloir supprimer le document <br/>${document.nom}? </strong><br/><br/>Cette action est irréversible`,
        confirmButtonText: 'Oui, le supprimer',
        cancelButtonText: 'Non, ne rien faire'
      }
    )
      .subscribe((result: ResultationNotification) => {
        if (result.isConfirmed) {
          this.documentService.supprimer(document)
            .subscribe(
              () =>
                this.notificationService.succes({
                  html: `Le fichier <strong>${document.nom}</strong> a été supprimé`
                }),
              _ => this.notificationService.erreur({
                html: `<span class="swal-error-message">Erreur lors de la suppression du document ${document.nom}.</span><br/><span>Merci de contacter votre support.</span>`
              })
            );
        }
      });
  }

  public dupliquer(document: Document): void {
    this.notificationService.confirmation({
        html: `<strong>Êtes de sûr de vouloir dupliquer le document <br/>${document.nom}? </strong><br/><br/>`,
        icon: 'question',
        confirmButtonText: 'Oui, faire une copie',
        cancelButtonText: 'Non, ne rien faire'
      }
    ).subscribe((result: ResultationNotification) => {
      if (result.isConfirmed) {
        this.documentService.dupliquer(document)
          .subscribe((resultat: Document) =>
              this.notificationService.succes({
                title: 'Dupliqué!',
                html: `Le fichier ${resultat.nom} a été dupliqué`,
                icon: 'success'
              }),
            _ => this.notificationService.erreur({
              html: `<span class="swal-error-message">Erreur lors de la duplication du fichier ${document.nom}.</span><br/><span>Merci de contacter votre support.</span>`
            })
          );
      }
    });
  }

  public inventorier(document: Document): void {
    const modale = this.modaleService.afficher(DocumentHistoryModaleComponent);
    modale.content.document = document;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));
  }

  public telecharger(document: Document): void {
    console.debug(`Téléchargement du document${JSON.stringify(document)}`);

    this.documentService.telecharger(document, this.documentService.formater(document));
  }

  public ouvrir(document: Document): void {
    console.debug(`Ouverture du document ${JSON.stringify(document)} dans only-office`);

    this.documentService.editer(document)
      .pipe(tap((redirection: Redirection) => console.debug(`Ok, ${document.id} va etre ouvert en suivant la redirection ${redirection.endpoint}`)))
      .pipe(tap((redirection: Redirection) => window.open(redirection.endpoint)))
      .subscribe(value => {}, error => {
        this.documentService.recuperer()
          .pipe(tap(documents => this.documents = documents.sort(DEFAULT_SORT)))
          .pipe(tap(_ =>
            this.websocketService.connect([{
              channel: `/topic/contextes/${this.sessionManager?.session?.contexte?.uuid}/documents/*`,
              handlers: [this.documentHandler],
              data: this.documents
            }])
          ))
          .subscribe(
            _ => {
              console.log('Liste des documents = ', this.documents);
              this.loading = false;
            },
            _ => {
              this.loading = false;
              this.notificationService.erreur(
                {html: `<span class="swal-error-message">Impossible de contacter le serveur d'édition de document. Il s'agit surement d'une indisponiblité temporaire.</span><span>Merci de prévenir votre support si le problème persiste.</span>`}
              )
            }
          );
      });
  }

  public previsualiser(document: Document): void {
    console.debug(`Prévisualisation du document ${JSON.stringify(document)} dans only-office`);

    const modale = this.modaleService.afficher(DocumentPreviewModaleComponent, {class: 'modal-wide'});
    modale.content.document = document;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));
  }

  public notifier(document: Document): void {
    const modale = this.modaleService.afficher(BroadcastModaleComponent);
    modale.content.document = document;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));
  }

  public creer(categorie: CategorieModele): void {
    const modale = this.modaleService.afficher(DocumentCreateModaleComponent);
    modale.content.categorie = categorie;
    modale.content.consultation = this.consultation;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));

  }

  public recopier(tab: string): void {
    const modale = this.modaleService.afficher(DocumentDuplicateModaleComponent);
    modale.content.categorie = tab;
    modale.content.fermeture
      .pipe(tap(_ => modale.hide()))
      .subscribe();
    modale.content.erreur
      .pipe(tap(_ => modale.hide()))
      .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>`}));
  }

  public valider(document: Document): void {
    if (document?.statut === StatutDocument.BROUILLON || document?.statut === StatutDocument.DEMANDE_VALIDATION) {
      const modale = this.modaleService.afficher(DocumentValidationModaleComponent);
      modale.content.document = document;
      modale.content.fermeture
        .pipe(tap(_ => modale.hide()))
        .subscribe();
      modale.content.erreur
        .pipe(tap(_ => modale.hide()))
        .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><span>Merci de contacter votre support.</span>`}));
    }
  }

  public invalider(document: Document): void {
    if (document.statut === StatutDocument.DEMANDE_VALIDATION || document.statut === StatutDocument.VALIDE) {
      const modale = this.modaleService.afficher(DocumentInvalidationModaleComponent);
      modale.content.document = document;
      modale.content.fermeture
        .pipe(tap(_ => modale.hide()))
        .subscribe();
      modale.content.erreur
        .pipe(tap(_ => modale.hide()))
        .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><span>Merci de contacter votre support.</span>`}));
    }
  }

  public soumettre(document: Document): void {
    if (document.statut === StatutDocument.BROUILLON) {
      this.notificationService.info(
        {
          html: `<div><span>Le document <strong>${document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${document.nom}</strong> est actuellement en cours de soumission</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes.</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
          customClass: 'dark-popin',
          toast: true
        }
      );

      this.documentService.soumettre(document)
        .subscribe(
          (resultat: Document) => console.log(`Sousmission du document ${resultat.nom} réalisé avec succes`),
          _ => this.notificationService.erreur({html: `<span class="swal-error-message">Erreur lors de la sousmission du document ${document?.nom}</span><span>Merci de contacter votre support.</span>`}));
    }
  }

  public insoumettre(document: Document): void {
    if (document.statut === StatutDocument.DEMANDE_VALIDATION) {
      const modale = this.modaleService.afficher(DocumentUnsubmitModaleComponent);
      modale.content.document = document;
      modale.content.fermeture
        .pipe(tap(_ => modale.hide()))
        .subscribe();
      modale.content.erreur
        .pipe(tap(_ => modale.hide()))
        .subscribe((erreur: string) => this.notificationService.erreur({html: `<span class="swal-error-message">${erreur}</span><span>Merci de contacter votre support.</span>`}));
    }
  }

  public contextualiser(image: string): string {
    let base = this.baseHref;

    if (!isDevMode()) {
      base = '/front/';
    }

    return `${base}assets/img/${image}`;
  }

  public autorise(autorisation: string): boolean {
    return this.sessionManager?.session?.agent?.habilitations?.some(it => it === autorisation);
  }

  public fermeture(document: Document): void {
    this.notificationService.confirmation({
        html: `<strong>Êtes de sûr de vouloir forcer la fermeture du document <br/>${document.nom}? </strong>. Cela peut engendrer la perte des dernières modifications d'édition.<br/><br/>`,
        icon: 'question',
        confirmButtonText: 'Oui, fermer ce document',
        cancelButtonText: 'Non, ne rien faire'
      }
    ).subscribe((result: ResultationNotification) => {
      if (result.isConfirmed) {
        document.ouvert = false;

        this.documentService.modifier(document)
          .subscribe((resultat: Document) =>
              console.log(`Le document ${resultat} vient d'être fermé`, resultat),
            _ => this.notificationService.erreur({
              html: `<span class="swal-error-message">Erreur lors de la duplication du fichier ${document.nom}.</span><br/><span>Merci de contacter votre support.</span>`
            })
          );
      }
    });
  }
}
