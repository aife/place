import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { Invalidation } from '@components/document';

@Component( {
    selector: 'atexo-redacdocument-document-invalidation-modale',
    templateUrl: './document-invalidation.modale.component.html',
    styleUrls: ['./document-invalidation.modale.component.scss']
} )
export class DocumentInvalidationModaleComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public invalidation = new EventEmitter<Invalidation>();
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
}
