import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { DocumentService, NotificationService } from '@services';

export interface Invalidation {
    document?: Document;
}

@Component( {
    selector: 'atexo-redacdocument-document-invalidation',
    templateUrl: './document-invalidation.component.html',
    styleUrls: ['./document-invalidation.component.scss']
} )
export class DocumentInvalidationComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public invalidation = new EventEmitter<Invalidation>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public acceptation = new EventEmitter<Document>();
    // Attributs
    public commentaire: string;

    constructor(
        private documentService: DocumentService,
        private notificationService: NotificationService
    ) {
    }

    public invalider(): void {
        this.acceptation.emit( this.document );

        this.document.modification.commentaire = this.commentaire;

        this.notificationService.info(
            {
                html: `<div><span>Le document <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours d'invalidation</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes.</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );
        this.documentService.invalider( this.document )
            .subscribe(
                ( document: Document ) => this.invalidation.emit( { document: document } ),
                _ => this.erreur.emit( `Erreur lors de l'invalidation du document <strong>${this.document?.nom}</strong>` )
            );
    }
}
