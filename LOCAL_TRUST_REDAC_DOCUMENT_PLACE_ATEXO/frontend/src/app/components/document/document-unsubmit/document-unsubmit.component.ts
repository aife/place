import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { DocumentService, NotificationService } from '@services';

export interface Insoumission {
    document?: Document;
}

@Component( {
    selector: 'atexo-redacdocument-document-unsubmit',
    templateUrl: './document-unsubmit.component.html',
    styleUrls: ['./document-unsubmit.component.scss']
} )
export class DocumentUnsubmitComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public insoumission = new EventEmitter<Insoumission>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public acceptation = new EventEmitter<Insoumission>();
    // Attributs
    public commentaire: string;

    constructor(
        private documentService: DocumentService,
        private notificationService: NotificationService
    ) {
    }

    public insoumettre(): void {
        this.acceptation.emit( { document: this.document } );

        this.document.modification.commentaire = this.commentaire;

        this.notificationService.info(
            {
                html: `<div><span>Le document <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours d'insoumission</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes.</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );

        console.debug( `Sauvegarde de l'insoumission du document` );

        this.documentService.insoumettre( this.document )
            .subscribe(
                ( document: Document ) => this.insoumission.emit( { document: document } ),
                _ => this.erreur.emit( `Erreur lors de l'insoumission du document <strong>${this.document?.nom}</strong>` )
            );

    }
}
