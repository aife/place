import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';
import { Insoumission } from './document-unsubmit.component';

@Component( {
    selector: 'atexo-redacdocument-document-unsubmit-modale-component',
    templateUrl: './document-unsubmit.modale.component.html',
    styleUrls: ['./document-unsubmit.modale.component.scss']
} )
export class DocumentUnsubmitModaleComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public insoumission = new EventEmitter<Insoumission>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public fermeture = new EventEmitter<void>();
}
