import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';

@Component( {
    selector: 'atexo-redacdocument-document-update-modale',
    templateUrl: './document-update.modale.component.html',
    styleUrls: ['./document-update.modale.component.scss']
} )
export class DocumentUpdateModaleComponent {
    // Inputs
    @Input() document: Document;
    // Outputs
    @Output() sauvegarde = new EventEmitter<Document>();
    @Output() erreur = new EventEmitter<string>();
    @Output() fermeture = new EventEmitter<void>();
}
