import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { Document } from '@model';
import { DocumentService, NotificationService } from '@services';
import { APP_BASE_HREF } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';

@Component( {
    selector: 'atexo-redacdocument-document-update',
    templateUrl: './document-update.component.html',
    styleUrls: ['./document-update.component.scss']
} )
export class DocumentUpdateComponent {
    // Inputs
    @Input() document: Document;
    // Outputs
    @Output() public sauvegarde = new EventEmitter<Document>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public acceptation = new EventEmitter<Document>();

    constructor(
        private documentService: DocumentService,
        private notificationService: NotificationService,
        @Inject( APP_BASE_HREF ) private baseHref: string
    ) {
    }

    public sauvegarder(): void {
        this.acceptation.emit( this.document );

        this.notificationService.info(
            {
                html: `<div><span>Le document <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours de validation</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes.</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );

        console.debug( `Sauvegarde de la modification du document` );

        this.documentService.modifier( this.document )
            .subscribe(
                ( document: Document ) => this.sauvegarde.emit( document )
                , _ => this.erreur.emit( `Une erreur s'est produite empêchant la modification du document <br/><strong>${this.document.nom}</strong>.` )
            );
    }

    public valide(): boolean {
        return !this.document?.nom?.empty() && !!this.document?.modele;
    }
}
