import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';

@Component( {
    selector: 'atexo-redacdocument-document-duplicate-modale',
    templateUrl: './document-duplicate.modale.component.html',
    styleUrls: ['./document-duplicate.modale.component.scss']
} )
export class DocumentDuplicateModaleComponent {
    // Inputs
    @Input() public categorie: 'editable' | 'canevas';
    // Outputs
    @Output() public duplication = new EventEmitter<Document>();
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();

}
