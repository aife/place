import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CategorieDocument, Document, Message } from '@model';
import { DocumentService, ModaleService, NotificationService } from '@services';
import { SessionManager } from '@managers';
import { DocumentPreviewModaleComponent } from '@components/document';
import { tap } from 'rxjs/operators';

enum Step {
    RECHERCHE,
    SELECTION,
    NOMMAGE,
    VALIDATION
}

@Component( {
    selector: 'atexo-redacdocument-document-duplicate',
    templateUrl: './document-duplicate.component.html',
    styleUrls: ['./document-duplicate.component.scss']
} )
export class DocumentDuplicateComponent implements OnInit {
    // Inputs
    @Input() public categorie: 'editable' | 'canevas';
    // Outputs
    @Output() public message = new EventEmitter<Message>();
    @Output() public duplication = new EventEmitter<Document>();
    @Output() public annulation = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public acceptation = new EventEmitter<Document>();
    // Attributs
    public document: Document;
    public documents: Document[] = [];
    public nom: string;
    public Steps = Step;
    public step: Step = Step.RECHERCHE;
    public messages: Message[] = [];
    public terms: string;
    private search = false;

    constructor(
        private documentService: DocumentService,
        private sessionManager: SessionManager,
        private notificationService: NotificationService,
        private modaleService: ModaleService
    ) {
    }

    public ngOnInit(): void {
        this.message.subscribe( message => {
            if ( this.messages.filter( m => m.value === message.value ).empty() ) {
                this.messages.push( message );
            }
        } );
    }

    public rechercher(): void {
      const search: string = this.terms;
      this.messages = [];

        if ( !this.search && !search?.empty() && this.categorie ) {
          this.search = true;
          let categories: CategorieDocument[] = [];

          switch ( this.categorie ) {
            case 'editable' :
              categories = [CategorieDocument.MODELE_DAJ, CategorieDocument.MODELE_PLATEFORME, CategorieDocument.DOCUMENT_LIBRE];
              break;
            case 'canevas' :
              categories = [CategorieDocument.DOCUMENT_CANEVAS];
              break;
            default:
              break;
          }

          this.documentService.rechercher( this.sessionManager?.session?.contexte?.environnement, [search], categories ).subscribe(
            docs => {
              this.documents = docs.filter( doc => doc.contexte.reference !== this.sessionManager?.session?.contexte.consultation.reference );

              console.log(`${this.documents?.length} documents trouvés`);

              this.step = Step.SELECTION;

              if ( this.documents.empty() ) {
                this.message.emit( { type: 'warn', value: `Aucun document ne correspond a votre recherche. Merci de modifier votre saisie.` } );
              }

              this.search = false;
            }
          );
        } else {
          this.document = null;
          this.documents = [];

          this.step = Step.RECHERCHE;
        }
    }

    public previsualiser( document: Document ): void {
        console.debug( `Prévisualisation du document ${JSON.stringify( document )} dans l'éditeur en ligne` );

        const modale = this.modaleService.afficher( DocumentPreviewModaleComponent, { class: 'modal-wide' } );
        modale.content.document = document;
        modale.content.fermeture
            .pipe( tap( _ => modale.hide() ) )
            .subscribe();
    }

    public verifier(): void {
        if ( this.document?.nom?.filled() ) {
            this.step = Step.VALIDATION;
        } else {
            this.step = Step.NOMMAGE;
        }
    }

    public selectionner( document: Document ): void {
        if ( this.document === document ) {
            this.document = null;

            this.step = Step.SELECTION;
        } else {
            this.document = document;

            this.nom = `${document?.nom?.nom()} (copie).${document?.nom?.extension()}`;

            this.step = Step.VALIDATION;
        }
    }

    public dupliquer(): void {
        this.acceptation.emit( this.document );

        this.notificationService.info(
            {
                html: `<div><span>Le document <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours de duplication</span></div><div class="mt-2"></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );

        this.documentService.dupliquer( this.document )
            .subscribe(
                ( document: Document ) => this.duplication.emit( document ),
                _ => this.erreur.emit( `Erreur lors de la duplication du document <strong>${this.document?.nom}</strong>` )
            );
    }
}
