import { ChangeDetectorRef, Component, EventEmitter, Inject, isDevMode, Output } from '@angular/core';
import { Canevas, Consultation, Message, Modele } from '@model';
import { Comparator } from '@patterns';
import { CanevasService } from '@services';
import { finalize } from 'rxjs/operators';
import { APP_BASE_HREF } from '@angular/common';

enum Action {
    AUCUNE, RECHERCHE
}

@Component( {
    selector: 'atexo-redacdocument-canevas-list',
    templateUrl: './canevas-list.component.html',
    styleUrls: ['./canevas-list.component.scss']
} )
export class CanevasListComponent {
    // Outputs
    @Output() public selection = new EventEmitter<Canevas>();
    // Attributs
    public Actions = Action;
    public modeles: Modele[] = [];
    public canevas: Canevas[];
    public choix: Canevas;
    public resultats: Canevas[] = [];
    public messages: Message[] = [];
    public filtre: string;
    public action: Action = Action.AUCUNE;

    constructor(
        private changeDetector: ChangeDetectorRef,
        private canevasService: CanevasService,
        @Inject( APP_BASE_HREF ) private baseHref: string
    ) {
    }

    public rechercher( consultation: Consultation, modele: Modele, lot: number ): void {
        this.messages = [];
        this.resultats = [];

        this.action = Action.RECHERCHE;

        this.canevasService.rechercher( consultation?.reference, modele?.type?.code, lot )
            .pipe( finalize( () => this.action = Action.AUCUNE ) )
            .subscribe(
                canevas => {
                    console.log( `Liste des canevas = {}`, canevas );

                    canevas = canevas.filter((item: Canevas) =>
                      !item.procedures || item.procedures?.length === 0 || item.procedures?.some(procedure => procedure.libelle === consultation.procedure.libelle)
                    );

                    if ( !canevas || canevas.empty() ) {
                        this.messages.push( { type: 'error', value: `Aucun canevas n'est actuellement disponible pour ce type de document. Veuillez en ajouter avant de créer un document ou contactez votre administrateur pour continuer.` } );
                    }

                    this.resultats = this.canevas = canevas.sort( ( c1: Canevas, c2: Canevas ) => new Comparator<string>().comparing( c2.reference, c1.reference ) );
                    this.canevas.sort( ( c1: Canevas, c2: Canevas ) => new Comparator<Date>().comparing( c2.dateModification, c1.dateModification ) );
                }, _ => {
                    this.messages.push( { type: 'error', value: `Une erreur s'est produite lors de la recherche de canevas. Merci de contacter votre support` } );

                    this.resultats = [];
                    this.canevas = [];
                }, () => this.changeDetector.detectChanges()
            );
    }

    public iconifier( canevas: Canevas ): string {
        let value: string;
        switch ( canevas?.statut?.id ) {
            case 1 :
            case 2 :
                value = 'en_cours.gif';
                break;
            case 3:
                value = 'valide.gif';
                break;
            default:
                value = 'autre.gif';
                break;
        }

        let base = this.baseHref;

        if ( !isDevMode() ) {
            base = '/front/';
        }

        return `${base}assets/img/statut/${value}`;
    }

    public selectionner( canevas: Canevas ): void {
        if ( this.choix === canevas ) {
            console.debug( `Désélection du canevas ${this.choix?.reference}` );

            this.choix = null;
            this.selection.emit( null );
        } else {
            console.debug( `Sélection du canevas ${canevas.reference}` );

            this.choix = canevas;
            this.selection.emit( this.choix );
        }
    }

    public filtrer(): void {
        this.resultats = this.canevas.filter( ( element: Canevas ): boolean => {
            const search = this.filtre.toUpperCase();

            return element?.reference?.toUpperCase()?.includes( search )
                || element?.titre?.toUpperCase()?.includes( search )
                || element?.auteur?.toUpperCase()?.includes( search )
                || element?.version?.toUpperCase()?.includes( search );
        } );
    }
}
