import { Component, EventEmitter, Inject, Input, Output, ViewChild } from '@angular/core';
import { Canevas, CategorieModele, Consultation, Document, Gabarit, Lot, Modele, Telechargement } from '@model';
import { Comparator } from '@patterns';
import { CanevasService, DocumentService, GabaritService, ModaleService, ModelesService, NotificationService } from '@services';
import { APP_BASE_HREF } from '@angular/common';
import { SessionManager } from '@managers';
import { GabaritPreviewModaleComponent } from '@components/gabarit';
import { CanevasListComponent } from '@components/document';
import { tap } from 'rxjs/operators';

enum Step {
    MODELE,
    LOT,
    FICHIER,
    CANEVAS,
    NOMMAGE,
    VALIDATION
}

@Component( {
    selector: 'atexo-redacdocument-document-create',
    templateUrl: './document-create.component.html',
    styleUrls: ['./document-create.component.scss']
} )
export class DocumentCreateComponent {
    // Views
    @ViewChild( CanevasListComponent ) public canevas!: CanevasListComponent;
    // Outputs
    @Output() public acceptation = new EventEmitter<Document>();
    @Output() public sauvegarde = new EventEmitter<Document>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    // Attributs
    private _categorie: CategorieModele;
    private _consultation: Consultation;
    public Steps = Step;
    public CategoriesModeles = CategorieModele;
    public step: Step = Step.MODELE;
    public document: Document = {};
    public modeles: Modele[] = [];

    constructor(
        private canevasService: CanevasService,
        private modeleService: ModelesService,
        private sessionManager: SessionManager,
        private documentService: DocumentService,
        private modaleService: ModaleService,
        private gabaritService: GabaritService,
        private notificationService: NotificationService,
        @Inject( APP_BASE_HREF ) private baseHref: string
    ) {
    }

    public get categorie(): CategorieModele {
        return this._categorie;
    }

    @Input()
    public set categorie( categorie: CategorieModele ) {
        if ( categorie ) {
            this._categorie = categorie;

            this.modeleService.recupererParCategorie( this._categorie ).subscribe( modeles => {
                this.modeles = modeles;

                if ( this.modeles.length === 1 ) {
                    this.document.modele = this.modeles.shift();

                    this.changerModele( this.document.modele, this.consultation );
                }
            } );
        }
    }

    public get consultation(): Consultation {
        return this._consultation;
    }

    @Input()
    public set consultation( consultation: Consultation ) {
        this._consultation = consultation;

        if ( this._consultation && this._consultation.lots && this._consultation.lots.length > 0 ) {
            const tous: Lot = { libelle: 'Tous les lots' };

            this._consultation.lots.sort(
                ( lotA: Lot, lotB: Lot ) => new Comparator<string>().comparing( lotA.numero.padStart( 8 ), lotB.numero.padStart( 8 ) )
            );

            this._consultation.lots.unshift( tous );
        }
    }

    public rechercher(requete: string, modele: Modele): boolean {
        const normaliser = (texte): string =>  texte.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

        const evaluer = (haystack, needle): boolean => new RegExp(normaliser(needle), 'gi').test(normaliser(haystack));

        return evaluer(modele.libelle, requete) || evaluer(modele.nomFichier, requete);
    }

    public changerModele( modele: Modele, consultation: Consultation ): void {
        switch ( this.categorie ) {
            case CategorieModele.DOCUMENT_CANEVAS :
                // si document canevas
                // on passe au choix du canevas
                this.step = Step.CANEVAS;
                // et on recherche la liste des canevas
                this.document.nom = `${modele.type?.code}_${consultation?.intitule}`;

                this.canevas.rechercher( this.consultation, this.document?.modele, this.document?.lot?.id );

                break;
            case CategorieModele.DOCUMENT_LIBRE :
                // si document libre
                // on passe au choix du fichier
                this.step = Step.FICHIER;
                break;
            default:
                // autres types
                // on va directement au nommage du fichier
                this.document.nom = `${modele.type?.code}_${consultation?.intitule}`;

                this.step = Step.VALIDATION;

                break;
        }
    }

    public ajouter( telechargement: Telechargement ): void {
        this.document.uuid = telechargement.uuid;
        this.document.nom = telechargement.nom;
        this.document.extension = telechargement.extension;

        this.step = Step.VALIDATION;
    }

    public supprimer(telechargement: Telechargement  ): void {
        console.debug(`Supression du telechargement`, telechargement);

        this.document.uuid = null;
        this.document.nom = null;
        this.document.extension = null;
    }

    public selectionner( canevas: Canevas ): void {
        if ( canevas ) {
            this.document.canevas = canevas;

            this.step = Step.VALIDATION;
        } else {
            this.document.canevas = null;

            this.step = Step.CANEVAS;
        }
    }

    public previsualiser( modele: Modele ): void {
        if ( modele ) {
            console.debug( `Prévisualisation du gabarit du document ${JSON.stringify( document )} dans l'éditeur en ligne` );

            this.gabaritService.recupererVersionEligible( modele ).subscribe(
                ( surchage: Gabarit ) => {
                    const modale = this.modaleService.afficher( GabaritPreviewModaleComponent, { class: 'modal-wide' } );
                    modale.content.surcharge = surchage;
                    modale.content.fermeture
                        .pipe( tap( _ => modale.hide() ))
                        .subscribe();
                }
            );
        }
    }

    public changer( lot: Lot ): void {
        console.debug( `Sélection du lot ${lot}` );

        this.document.lot = lot;
    }

    public verifier(): void {
        if ( !this.document?.nom || this.document?.nom?.empty() ) {
            this.step = Step.NOMMAGE;
        } else {
            this.step = Step.VALIDATION;
        }
    }

    public valider(): void {
        this.acceptation.emit( this.document );

        this.notificationService.info(
            {
                html: `<div><span>Le <strong>${this.document.modele.type.libelle.toLowerCase()}</strong> intitulé <strong>${this.document.nom}</strong> est actuellement en cours de création</span></div><div class="mt-2"><span>Cela peut prendre quelques minutes${this.categorie === CategorieModele.DOCUMENT_CANEVAS ? ' suivant la taille du canevas.' : '.'}</span></div><div class="mt-2"><span>Merci de patienter.</span></div>`,
                customClass: 'dark-popin',
                toast: true
            }
        );

        this.documentService.creer( this.document )
            .subscribe(
                ( document: Document ) => this.sauvegarde.emit( document ),
                _ => this.erreur.emit( `Impossible de créer le document <br/><strong>${this.document?.nom}</strong>` )
            );

    }
}
