import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CategorieModele, Consultation, Document } from '@model';

@Component( {
    selector: 'atexo-redacdocument-document-create-modale',
    templateUrl: './document-create.modale.component.html',
    styleUrls: ['./document-create.modale.component.scss']
} )
export class DocumentCreateModaleComponent {
    // Inputs
    @Input() public categorie: CategorieModele;
    @Input() public consultation: Consultation;
    // Ouptuts
    @Output() public sauvegarde = new EventEmitter<Document>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public fermeture = new EventEmitter<void>();
    // Attributs
    public Categories = CategorieModele;
}
