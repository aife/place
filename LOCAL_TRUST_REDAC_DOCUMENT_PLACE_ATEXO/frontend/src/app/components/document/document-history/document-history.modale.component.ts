import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Document } from '@model';

@Component( {
    selector: 'atexo-redacdocument-document-history-modale',
    templateUrl: './document-history.modale.component.html',
    styleUrls: ['./document-history.modale.component.scss']
} )
export class DocumentHistoryModaleComponent {
    // Inputs
    @Input() public document: Document;
    // Outputs
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
}
