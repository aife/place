import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActionDocument, Document } from '@model';
import { Comparator } from '@patterns';
import { DocumentService } from '@services';
import { DocumentActionIconeMapper, DocumentActionLibelleMapper, DocumentStatutLibelleMapper, DocumentStatutTypeMapper } from '@mappers';

@Component( {
    selector: 'atexo-redacdocument-document-history',
    templateUrl: './document-history.component.html',
    styleUrls: ['./document-history.component.scss']
} )
export class DocumentHistoryComponent {
    // Outputs
    @Output() public erreur = new EventEmitter<string>();
    // Attributs
    public _document: Document;
    public revisions: Document[] = [];
    private comparateur: ( a: Document, b: Document ) => number = ( a: Document, b: Document ) => new Comparator<Date>().comparing( b?.modification?.date, a?.modification?.date );

    constructor(
        private documentService: DocumentService,
        public documentStatutTypeMapper: DocumentStatutTypeMapper,
        public documentStatutLibelleMapper: DocumentStatutLibelleMapper,
        public documentActionLibelleMapper: DocumentActionLibelleMapper,
        public documentActionIconeMapper: DocumentActionIconeMapper,
    ) {
    }

    get document(): Document {
        return this._document;
    }

    @Input() set document( document: Document ) {
        if ( document ) {
            this._document = document;

            this.documentService.log( document ).subscribe(
                ( documents: Document[] ) => this.revisions = documents.sort( this.comparateur ),
                _ => this.erreur.emit( `Impossible de récuperer l'historique du document <br/><strong>${this.document?.nom}</strong>` )
            );
        }
    }

    public telecharger( document: Document ): void {
        this.documentService.telecharger( document, this.documentService.formater( document ) );
    }

    public telechargeable( document: Document ): boolean {
        return [
            ActionDocument.CREATION,
            ActionDocument.NOTIFICATION,
            ActionDocument.TELECHARGEMENT,
            ActionDocument.ANNULATION,
            ActionDocument.FERMETURE_AVEC_MODIFICATION,
            ActionDocument.ENREGISTREMENT,
            ActionDocument.DEMANDE_VALIDATION,
            ActionDocument.REFUS_DEMANDE_VALIDATION,
            ActionDocument.INVALIDATION,
            ActionDocument.VALIDATION
        ].includes( document.action );
    }

    public estDerniereVersion( document: Document ): boolean {
        return document === this.revisions.filter( d => this.telechargeable( d ) ).sort( this.comparateur ).shift();
    }
}
