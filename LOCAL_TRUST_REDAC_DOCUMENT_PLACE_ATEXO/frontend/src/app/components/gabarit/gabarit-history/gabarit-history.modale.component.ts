import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Gabarit } from '@model';

@Component( {
    selector: 'atexo-redacdocument-gabarit-history-modale',
    templateUrl: './gabarit-history.modale.component.html',
    styleUrls: ['./gabarit-history.modale.component.scss']
} )
export class GabaritHistoryModaleComponent {
    // Inputs
    @Input() public gabarit: Gabarit;
    // Outputs
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
}
