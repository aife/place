import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Gabarit } from '@model';
import { GabaritService } from '@services';
import { tap } from 'rxjs/operators';

@Component( {
    selector: 'atexo-redacdocument-gabarit-history',
    templateUrl: './gabarit-history.component.html',
    styleUrls: ['./gabarit-history.component.scss']
} )
export class GabaritHistoryComponent {
    // Outputs
    @Output() public erreur = new EventEmitter<string>();
    // Attributs
    public evenements: Gabarit[] = [];

    constructor(
        private gabaritService: GabaritService
    ) {
    }

    @Input() set gabarit( gabarit: Gabarit ) {
        if ( gabarit ) {
            this.gabaritService.recupererRevisions( gabarit )
                .pipe( tap( ( gabarits: Gabarit[] ) => this.evenements = gabarits ) )
                .pipe( tap( _ => this.evenements.sort((g1: Gabarit, g2: Gabarit) => g1.modification?.date > g2.modification?.date ? 1 : -1)  ) )
                .pipe( tap( _ => {
                    if ( this.evenements.empty() ) {
                        this.evenements.push( gabarit );
                    }
                } ) )
                .subscribe(
                    ( gabarits: Gabarit[] ) => console.log( `${gabarits?.length} récupérés` ),
                    _ => this.erreur.emit( `Impossible de récuperer l'historique du gabarit <br/><strong>${gabarit?.nom}</strong>` ));
        }
    }

    public telecharger( surcharge: Gabarit ): void {
        this.gabaritService.telecharger( surcharge );
    }
}
