import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Gabarit } from '@model';
import { Observable } from 'rxjs';

@Component({
  selector: 'atexo-redacdocument-gabarit-preview-modale',
  templateUrl: './gabarit-preview.modale.component.html',
  styleUrls: ['./gabarit-preview.modale.component.scss']
})
export class GabaritPreviewModaleComponent {
  // Inputs
  @Input() public surcharge: Gabarit;
  // Outputs
  @Output() public fermeture = new EventEmitter<void>();
  @Output() public erreur = new EventEmitter<string>();

  public data: Observable<string>;
}
