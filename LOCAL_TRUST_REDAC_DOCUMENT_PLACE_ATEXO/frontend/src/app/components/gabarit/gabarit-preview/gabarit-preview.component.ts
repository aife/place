import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Gabarit } from '@model';
import { GabaritService } from '@services';
import { Observable } from 'rxjs';

@Component({
  selector: 'atexo-redacdocument-gabarit-preview',
  templateUrl: './gabarit-preview.component.html',
  styleUrls: ['./gabarit-preview.component.scss']
})
export class GabaritPreviewComponent {

  @Output() public erreur = new EventEmitter<string>();

  // Attributs
  public data: Observable<string>;

  constructor(
    private gabaritService: GabaritService
  ) {
  }

  @Input()
  public set surcharge(surcharge: Gabarit) {
    if (surcharge) {
      console.debug(`Géneration du lien pour la surcharge =`, surcharge);

      this.data = this.gabaritService.permalien(surcharge);

      this.data.subscribe(
        (data: string) => console.trace('Gabarit OK pour affichage'),
        (error) => this.erreur.emit(`Impossible d'afficher le gabarit`)
      );
    }
  }
}
