import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Gabarit } from '@model';

@Component( {
    selector: 'atexo-redacdocument-gabarit-edit-modale',
    templateUrl: './gabarit-edit.modale.component.html',
    styleUrls: ['./gabarit-edit.modale.component.scss']
} )
export class GabaritEditModaleComponent {
    // Inputs
    @Input() public gabarit: Gabarit;
    // Outputs
    @Output() public sauvegarde = new EventEmitter<Gabarit>();
    @Output() public fermeture = new EventEmitter<void>();
    @Output() public erreur = new EventEmitter<string>();
}
