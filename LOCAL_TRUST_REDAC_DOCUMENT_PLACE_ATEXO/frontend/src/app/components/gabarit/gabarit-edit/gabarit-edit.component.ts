import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Gabarit, Niveau, Referentiel, Telechargement } from '@model';
import { GabaritService, NotificationService, ReferentielService } from '@services';
import { SessionManager } from '@managers';
import { mergeMap, tap } from 'rxjs/operators';


enum Step {
    NIVEAU,
    FICHIER,
    NOMMAGE,
    VALIDATION
}

@Component( {
    selector: 'atexo-redacdocument-gabarit-edit',
    templateUrl: './gabarit-edit.component.html',
    styleUrls: ['./gabarit-edit.component.scss']
} )
export class GabaritEditComponent implements OnInit {
    // Inputs
    @Input() public gabarit: Gabarit;
    // Outputs
    @Output() public sauvegarde = new EventEmitter<Gabarit>();
    @Output() public erreur = new EventEmitter<string>();
    @Output() public annulation = new EventEmitter<void>();
    // Attributs
    public Niveaux = Niveau;
    public Steps = Step;
    public step: Step = Step.NIVEAU;
    public niveau;

    constructor(
        private gabaritService: GabaritService,
        private sessionManager: SessionManager,
        private referentielsService: ReferentielService,
        private notificationService: NotificationService
    ) {
    }

    public ngOnInit(): void {
        this.niveau = this.sessionManager?.session?.niveau;

        if ( !this.niveau ) {
            this.niveau = Niveau.AGENT;
        }

        this.step = Step.FICHIER;
    }

    public ajouter( telechargement: Telechargement ): void {
        this.gabarit.uuid = telechargement.uuid;
        this.gabarit.nom = telechargement.nom + '.' + telechargement.extension;

        this.step = Step.VALIDATION;
    }

    public sauvegarder(): void {
        console.debug( `Recherche du niveau ${this.niveau}` );

        this.referentielsService.rechercherParCode( this.niveau )
            .pipe( tap( ( niveau: Referentiel ) => this.gabarit.niveau = niveau ) )
            .pipe( mergeMap( ( niveau: Referentiel ) => this.gabaritService.recuperer( this.gabarit.modele, Niveau[niveau.code] ) ) )
            .subscribe(
                gabarit => {
                    if ( gabarit ) {
                        this.notificationService.confirmation( {
                                html: `<strong>Êtes de sûr de vouloir créer le gabarit de niveau<br/>${gabarit.niveau.code}? </strong><br/><br/>Il existe déj&agrave; un gabarit personnalis&eacute;, cette action le supprimera et est irréversible`,
                                confirmButtonText: 'Oui, le supprimer',
                                cancelButtonText: 'Non, ne rien faire'
                            }
                        ).subscribe( result => {
                            if ( result.isConfirmed ) {
                                this.creer();
                            }
                        } );
                    } else {
                        this.creer();
                    }
                }
            );
    }

    public creer(): void {
      this.gabaritService.creer( this.gabarit ).subscribe( surcharge => this.sauvegarde.emit( surcharge ) );
    }

    public valide(): boolean {
        return this.niveau && this.gabarit?.uuid && this.gabarit.nom && !this.gabarit?.nom?.trim().empty();
    }
}
