import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Gabarit, Modele, Niveau, Surcharge } from '@model';
import { GabaritService, ModaleService, ModelesService, NotificationService, ReferentielService, WebsocketService } from '@services';
import { SessionManager } from '@managers';
import { GabaritEditModaleComponent, GabaritHistoryModaleComponent, GabaritPreviewModaleComponent } from '@components/gabarit';
import { GabaritHandler } from '@events';
import { map, tap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component( {
    selector: 'atexo-redacdocument-gabarit-list',
    templateUrl: './gabarit-list.component.html',
    styleUrls: ['./gabarit-list.component.scss']
} )
export class GabaritListComponent implements OnInit {
    // Attributs
    public Niveaux = Niveau;
    public modeles: Modele[] = [];
    public niveau: string = Niveau.AGENT;
    public surcharges: Surcharge[] = [];
    public selection: Modele;
    public isLoading: boolean = false;

    constructor(
        private modeleService: ModelesService,
        private gabaritService: GabaritService,
        private sessionManager: SessionManager,
        private modaleService: ModaleService,
        private websocketService: WebsocketService,
        private gabaritHandler: GabaritHandler,
        private notificationService: NotificationService,
        private referentielService: ReferentielService,
        private changeDetector: ChangeDetectorRef
    ) {
    }

    public ngOnInit(): void {
        this.modeleService.recupererModelesAdministrable().subscribe( modeles => {
            this.modeles = modeles;
            if ( modeles.filled() ) {
                this.selection = modeles[0];

                this.charger();
            }
        } );

        if ( this.sessionManager?.session?.niveau ) {
            this.niveau = this.sessionManager?.session?.niveau;
        }
    }

    public charger(): void {
        this.isLoading = true;
        this.referentielService.rechercherParType( 'NIVEAU' ).subscribe(
            niveaux =>
                forkJoin( niveaux.map( niveau => this.gabaritService.recuperer( this.selection, Niveau[niveau.code] )
                    .pipe( map( gabarit => (
                        { niveau: Niveau[niveau.code], ordre: niveau.ordre, libelle: niveau.libelle, gabarit: gabarit }
                    ) ) ) ) )
                    .subscribe(
                        surcharges => {
                            this.surcharges = surcharges.sort( ( s1, s2 ) => s1.ordre > s2.ordre ? 1 : -1 );
                            this.isLoading = false;
                            console.log( 'Liste des surcharges = ', this.surcharges );

                            this.websocketService.connect( [{
                                channel: `/topic/utilisateurs/${this.sessionManager?.session?.agent?.uuid}/gabarits/*`,
                                handlers: [this.gabaritHandler],
                                data: this.surcharges
                            }, {
                                channel: `/topic/services/${this.sessionManager?.session?.agent?.service?.uuid}/gabarits/*`,
                                handlers: [this.gabaritHandler],
                                data: this.surcharges
                            }, {
                                channel: `/topic/organismes/${this.sessionManager?.session?.contexte?.uuid}/gabarits/*`,
                                handlers: [this.gabaritHandler],
                                data: this.surcharges
                            }, {
                                channel: `/topic/plateformes/${this.sessionManager?.session?.contexte?.uuid}/gabarits/*`,
                                handlers: [this.gabaritHandler],
                                data: this.surcharges
                            }] );
                        }
                    ),
          ( response: HttpErrorResponse ) => {
              console.log(`Erreur lors de la récupération des gabarits pour le modele '${this.selection}'`, response?.error);
              this.isLoading = false;
              this.notificationService.erreur( { html: `<span class="swal-error-message">Erreur lors de la récupération des gabarits</span><br/><span>Merci de contacter votre support.</span>` } );
          },
        () => this.changeDetector.detectChanges());
    }

    public inventorier( gabarit: Gabarit ): void {
        const modale = this.modaleService.afficher( GabaritHistoryModaleComponent );
        modale.content.gabarit = gabarit;
        modale.content.fermeture
            .pipe( tap( _ => modale.hide() ) )
            .subscribe();
        modale.content.erreur
            .pipe( tap( _ => modale.hide() ) )
            .subscribe( ( erreur: string ) => this.notificationService.erreur( { html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>` } ) );

    }

    public previsualiser( surcharge: Gabarit ): void {
        console.debug( `Prévisualisation de la surcharge ${JSON.stringify( surcharge )} dans only-office` );

        const modale = this.modaleService.afficher( GabaritPreviewModaleComponent, { class: 'modal-wide' } );
        modale.content.surcharge = surcharge;
        modale.content.fermeture.subscribe( _ => modale.hide() );
        modale.content.erreur
            .pipe( tap( _ => modale.hide() ) )
            .subscribe( ( erreur: string ) => this.notificationService.erreur( { html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>` } ) );
    }

    public creer(): void {
        const modale = this.modaleService.afficher( GabaritEditModaleComponent );
        modale.content.gabarit = { modele: this.selection };
        modale.content.sauvegarde.subscribe( _ => modale.hide() );
        modale.content.fermeture.subscribe( _ => modale.hide() );
        modale.content.erreur
            .pipe( tap( _ => modale.hide() ) )
            .subscribe( ( erreur: string ) => this.notificationService.erreur( { html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>` } ) );
    }

    public telecharger( gabarit: Gabarit ): void {
        this.gabaritService.telecharger( gabarit );
    }

    public supprimer( gabarit: Gabarit ): void {
        let icone: string;

        switch ( gabarit.niveau.code ) {
            case Niveau.AGENT:
            default:
                icone = 'fa fa-user';
                break;
            case Niveau.ENTITE_ACHAT:
                icone = 'fa fa-users';
                break;
            case Niveau.ORGANISME:
                icone = 'fa fa-university';
                break;
            case Niveau.PLATEFORME:
                icone = 'fa fa-globe';
                break;
        }

        this.notificationService.confirmation( {
                html: `<strong>Êtes de sûr de vouloir supprimer le gabarit <div class="mt-4 mb-4"><em class="${icone}"></em>&nbsp;${gabarit.niveau.code}</div>${gabarit.nom}? </strong><br/><br/><span class="text-danger">Cette action est irréversible</span>`,
                confirmButtonText: 'Oui, le supprimer',
                cancelButtonText: 'Non, ne rien faire'
            }
        ).subscribe( ( result ) => {
            if ( result.isConfirmed ) {
                this.gabaritService.supprimer( gabarit )
                    .subscribe( _ => this.notificationService.erreur( { html: `<span class="swal-error-message">Erreur lors de la suppression du fichier ${gabarit.nom}.</span><br/><span>Merci de contacter votre support.</span>` } ) );
            }
        } );
    }

    public grouper( modele: Modele ): string {
        return modele?.type?.parent?.libelle;
    }

    public active( surcharge: Surcharge ) {
        const surcharges = this.surcharges.filter( s => s.gabarit !== null ).sort( ( s1, s2 ) => s1.ordre > s2.ordre ? 1 : -1 );

        return surcharge === surcharges.pop();
    }
}
