import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, DoBootstrap, Injector, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DEFAULT_TIMEOUT, HttpErrorInterceptor, HttpHeaderAcceptInterceptor, HttpHeaderAccountInterceptor, HttpHeaderAutorizationInterceptor, HttpHeaderContextInterceptor, HttpHeaderFetchModeInterceptor, HttpTimeoutInterceptor } from '@interceptors';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { createCustomElement } from '@angular/elements';
import {
    AuthentificationService,
    CanevasService,
    ContexteService,
    DocumentService,
    FichierService,
    GabaritService,
    ModaleService,
    ModelesService,
    NotificationService,
    ParametreService,
    ReferentielService,
    SessionService,
    StockageLocalService,
    StockageSessionService,
    UtilisateurService,
    VersionService,
    WebsocketService
} from '@services';
import {
    AuthentificationRemoteService,
    CanevasRemoteService,
    ContexteRemoteService,
    CredentialRemoteService,
    DocumentRemoteService,
    FichierRemoteService,
    GabaritRemoteService,
    ModelesRemoteService,
    ParametreRemoteService,
    ReferentielRemoteService,
    SessionRemoteService,
    UtilisateurRemoteService,
    WebsocketRemoteService
} from './services/remote';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DocumentComplexCounterPipe, DocumentComplexFilterPipe, DocumentSimpleCounterPipe, DocumentSimpleFilterPipe, DownloadableEventFilterPipe, EventFilterPipe, FileSizePipe, FrenchDatePipe, SafePipe } from './pipes';
import {
    BroadcastComponent,
    BroadcastModaleComponent,
    CanevasListComponent,
    CredentialModaleComponent,
    DocumentCreateComponent,
    DocumentCreateModaleComponent,
    DocumentDuplicateComponent,
    DocumentDuplicateModaleComponent,
    DocumentHistoryComponent,
    DocumentHistoryModaleComponent,
    DocumentInvalidationComponent,
    DocumentInvalidationModaleComponent,
    DocumentListComponent,
    DocumentPreviewComponent,
    DocumentPreviewModaleComponent,
    DocumentUnsubmitComponent,
    DocumentUnsubmitModaleComponent,
    DocumentUpdateComponent,
    DocumentUpdateModaleComponent,
    DocumentValidationComponent,
    DocumentValidationModaleComponent,
    FileUploadComponent,
    LoaderComponent,
    GabaritEditComponent,
    GabaritEditModaleComponent,
    GabaritHistoryComponent,
    GabaritHistoryModaleComponent,
    GabaritListComponent,
    GabaritPreviewComponent,
    GabaritPreviewModaleComponent
} from '@components';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { APP_BASE_HREF } from '@angular/common';
import { AlertModule } from 'ngx-bootstrap/alert';
import { NgSelectModule } from '@ng-select/ng-select';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModaleClientService, NotificationClientService, StockageLocalClientService, StockageSessionClientService } from './services/client';
import { SessionManager } from '@managers';
import { SessionLocalManager } from './managers/local';
import { DocumentHandler, NotificationHandler, RefreshHandler } from '@events';
import { DocumentActionIconeMapper, DocumentActionLibelleMapper, DocumentStatutLibelleMapper, DocumentStatutTypeMapper } from '@mappers';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@NgModule( {
    declarations: [
        AppComponent,
        FileSizePipe,
        SafePipe,
        EventFilterPipe,
        FrenchDatePipe,
        DocumentComplexCounterPipe,
        DocumentComplexFilterPipe,
        DocumentSimpleCounterPipe,
        DocumentSimpleFilterPipe,
        DownloadableEventFilterPipe,
        DocumentUnsubmitComponent,
        DocumentUnsubmitModaleComponent,
        DocumentInvalidationComponent,
        DocumentInvalidationModaleComponent,
        DocumentValidationComponent,
        DocumentValidationModaleComponent,
        FileUploadComponent,
        DocumentDuplicateComponent,
        DocumentUpdateComponent,
        DocumentCreateComponent,
        CanevasListComponent,
        GabaritEditComponent,
        DocumentHistoryComponent,
        DocumentHistoryModaleComponent,
        GabaritHistoryComponent,
        DocumentListComponent,
        GabaritListComponent,
        LoaderComponent,
        BroadcastModaleComponent,
        CredentialModaleComponent,
        DocumentPreviewComponent,
        DocumentPreviewModaleComponent,
        GabaritPreviewComponent,
        GabaritEditModaleComponent,
        DocumentDuplicateModaleComponent,
        DocumentCreateModaleComponent,
        DocumentUpdateModaleComponent,
        GabaritHistoryModaleComponent,
        GabaritPreviewModaleComponent,
        BroadcastComponent,
        DocumentInvalidationComponent
    ],
    imports: [
        TypeaheadModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        AlertModule.forRoot(),
        NgSelectModule,
        ProgressbarModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        NgSelectModule,
        BsDropdownModule.forRoot(),
        ButtonsModule.forRoot(),
        FormsModule,
        CollapseModule.forRoot()
    ],
    providers: [
        RefreshHandler,
        NotificationHandler,
        DocumentHandler,
        DocumentStatutTypeMapper,
        DocumentStatutLibelleMapper,
        DocumentActionLibelleMapper,
        DocumentActionIconeMapper,
        { provide: SessionManager, useClass: SessionLocalManager },
        { provide: SessionService, useClass: SessionRemoteService },
        { provide: AuthentificationService, useClass: AuthentificationRemoteService },
        { provide: CanevasService, useClass: CanevasRemoteService },
        { provide: ContexteService, useClass: ContexteRemoteService },
        { provide: FichierService, useClass: FichierRemoteService },
        { provide: VersionService, useClass: CredentialRemoteService },
        { provide: DocumentService, useClass: DocumentRemoteService },
        { provide: GabaritService, useClass: GabaritRemoteService },
        { provide: ModelesService, useClass: ModelesRemoteService },
        { provide: ReferentielService, useClass: ReferentielRemoteService },
        { provide: UtilisateurService, useClass: UtilisateurRemoteService },
        { provide: WebsocketService, useClass: WebsocketRemoteService },
        { provide: ParametreService, useClass: ParametreRemoteService },
        { provide: NotificationService, useClass: NotificationClientService },
        { provide: StockageLocalService, useClass: StockageLocalClientService },
        { provide: StockageSessionService, useClass: StockageSessionClientService },
        { provide: ModaleService, useClass: ModaleClientService },
        { provide: APP_BASE_HREF, useValue: '/' },
        { provide: DEFAULT_TIMEOUT, useValue: 5 * 60 * 1000 },
        { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpTimeoutInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderAutorizationInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderAccountInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderContextInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderAcceptInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderFetchModeInterceptor, multi: true },
        {
            provide: APP_INITIALIZER,
            useFactory: ( _: ReferentielRemoteService ) => () => {
            },
            deps: [ReferentielRemoteService],
            multi: true
        }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
} )
export class AppModule implements DoBootstrap {
    constructor( private injector: Injector ) {
        const el = createCustomElement( AppComponent, {
            injector: injector
        } );
        customElements.define( 'atexo-redaction', el );
    }

    ngDoBootstrap( appRef: ApplicationRef ): void {
        console.debug( 'Boostrap app' );
    }
}
