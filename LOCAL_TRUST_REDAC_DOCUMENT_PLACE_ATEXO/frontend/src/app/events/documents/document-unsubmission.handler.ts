import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentUnsubmissionHandler implements Handler<DocumentMessage> {

    @Output() insoumission = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const update = message.document;

        if ( message.action === DocumentAction.INSOUMISSION ) {
            console.debug( `Insoumission du document ${update.id}` );

            const index = documents.findIndex( document => document.id === update.id );

            documents[index] = update;

            this.insoumission.subscribe( update );
        }
    }
}
