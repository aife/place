import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentUnvalidationHandler implements Handler<DocumentMessage> {

    @Output() invalidation = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const update = message.document

        if ( message.action === DocumentAction.INVALIDATION ) {
            console.debug( `Invalidation du document ${update.id}` );

            const index = documents.findIndex( document => document.id === update.id );

            documents[index] = update;

            this.invalidation.subscribe( update );
        }
    }
}
