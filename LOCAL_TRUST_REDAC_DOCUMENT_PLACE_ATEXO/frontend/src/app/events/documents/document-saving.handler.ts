import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentSavingHandler implements Handler<DocumentMessage> {

    @Output() sauvegarde = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const update = message.document;

        if ( message.action === DocumentAction.SAUVEGARDE ) {
            console.debug( `Fermeture du document ${update.id}` );

            const index = documents.findIndex( document => document.id === update.id );

            documents[index] = update;

            this.sauvegarde.emit( message.document );
        }
    }
}
