import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentDeletionHandler implements Handler<DocumentMessage> {

    @Output() suppression = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const id = message.document.id;

        if ( message.action === DocumentAction.SUPPRESSION ) {
            console.debug( `Suppression du document ${id} de la liste` );

            const index = documents.findIndex( document => document.id === id );

            documents.splice( index, 1 );

            this.suppression.emit( message.document );
        }
    }
}
