import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentSubmissionHandler implements Handler<DocumentMessage> {

    @Output() soumission = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const update = message.document;

        if ( message.action === DocumentAction.SOUMISSION ) {
            console.debug( `Soumission du document ${update.id}` );

            const index = documents.findIndex( document => document.id === update.id );

            documents[index] = update;

            this.soumission.emit( update );
        }
    }
}
