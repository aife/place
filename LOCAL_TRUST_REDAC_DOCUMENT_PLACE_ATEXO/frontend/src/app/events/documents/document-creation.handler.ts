import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { DEFAULT_SORT, Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentCreationHandler implements Handler<DocumentMessage> {

    @Output() creation = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const id = message.document.id;

        if ( message.action === DocumentAction.CREATION ) {
            if ( !documents.find( document => document.id === id ) ) {
                console.debug( `Ajout du document ${id} à la liste` );

                documents.push( message.document );
                documents.sort( DEFAULT_SORT );

                this.creation.emit( message.document );
            }
        }
    }
}
