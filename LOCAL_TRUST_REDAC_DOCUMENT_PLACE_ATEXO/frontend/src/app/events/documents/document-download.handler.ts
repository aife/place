import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentDownloadHandler implements Handler<DocumentMessage> {

    @Output() telechargement = new EventEmitter<Document>();

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const id = message.document.id;

        if ( message.action === DocumentAction.TELECHARGEMENT ) {
            console.debug( `Sauvegarde du document ${id}` );

            this.telechargement.emit( message.document );
        }
    }
}
