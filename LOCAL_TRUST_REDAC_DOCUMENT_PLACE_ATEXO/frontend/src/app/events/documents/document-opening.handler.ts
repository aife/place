import { DocumentAction, DocumentMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Document } from '@model';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentOpeningHandler implements Handler<DocumentMessage> {

    @Output() ouverture = new EventEmitter<Document>();

    public constructor(
        private sessionManager: SessionManager
    ) {
    }

    public handle( message: DocumentMessage, documents: Document[] ): void {
        const id = message.document.id;

        if ( message.action === DocumentAction.OUVERTURE ) {
            console.debug( `Ouverture du document ${id}` );

            documents.find( document => document.id === id ).ouvert = true;

            // Désactivation du disclaimer sur only-office
            if ( message?.agent?.uuid === this.sessionManager.session.agent.uuid ) {
                console.debug( `Désactivation de l'avertissement sur only-office pour l'agent ${this.sessionManager.session.agent?.uuid}` );

                this.sessionManager.avertir( false );
            }

            this.ouverture.emit( message.document );
        }
    }
}
