import { DocumentAction, DocumentMessage, ModaleService, NotificationService } from '@services';
import { Injectable } from '@angular/core';
import { Handler } from '../handler';
import { BroadcastModaleComponent } from '@components';
import { tap } from 'rxjs/operators';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentSubmissionMailingHandler implements Handler<DocumentMessage> {

    public constructor(
        private modaleService: ModaleService,
        private notificationService: NotificationService
    ) {
    }

    public handle( message: DocumentMessage ): void {
        if ( message.action === DocumentAction.SOUMISSION ) {
            console.debug( `Envoi de mail suite a soumission du document ${message.document.id}` );

            const modale = this.modaleService.afficher( BroadcastModaleComponent );

            modale.content.document = message.document;
            modale.content.fermeture.subscribe( _ => modale.hide() );
            modale.content.envoi.subscribe( _ => modale.hide() );
            modale.content.erreur
                .pipe( tap( _ => modale.hide() ) )
                .subscribe( erreur => this.notificationService
                    .erreur( { html: `<span class="swal-error-message">${erreur}</span><br/><span>Merci de contacter votre support.</span>` } ) );
        }
    }
}
