import { DocumentAction, DocumentMessage } from '@services';
import { Injectable } from '@angular/core';
import { Handler } from './handler';
import { Document } from '@model';
import { DocumentClosingHandler } from './documents/document-closing.handler';
import { DocumentCreationHandler } from './documents/document-creation.handler';
import { DocumentDeletionHandler } from './documents/document-deletion.handler';
import { DocumentOpeningHandler } from './documents/document-opening.handler';
import { DocumentSubmissionHandler } from './documents/document-submission.handler';
import { DocumentSubmissionMailingHandler } from './documents/document-submission-mailing.handler';
import { DocumentUnsubmissionHandler } from './documents/document-unsubmission.handler';
import { DocumentUnvalidationHandler } from './documents/document-unvalidation.handler';
import { DocumentValidationHandler } from './documents/document-validation.handler';
import { DocumentDuplicationHandler } from './documents/document-duplication.handler';
import { DocumentDownloadHandler } from './documents/document-download.handler';
import { DocumentSavingHandler } from './documents/document-saving.handler';
import { DocumentModificationHandler } from './documents/document-modification.handler';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentHandler implements Handler<DocumentMessage> {

    constructor(
        private documentCreationHandler: DocumentCreationHandler,
        private documentDuplicationHandler: DocumentDuplicationHandler,
        private documentModificationHandler: DocumentModificationHandler,
        private documentClosingHandler: DocumentClosingHandler,
        private documentUnsubmissionHandler: DocumentUnsubmissionHandler,
        private documentUnvalidationHandler: DocumentUnvalidationHandler,
        private documentOpeningHandler: DocumentOpeningHandler,
        private documentSavingHandler: DocumentSavingHandler,
        private documentSubmissionHandler: DocumentSubmissionHandler,
        private documentSubmissionMailingHandler: DocumentSubmissionMailingHandler,
        private documentDeletionHandler: DocumentDeletionHandler,
        private documentDownloadHandler: DocumentDownloadHandler,
        private documentValidationHandler: DocumentValidationHandler
    ) {
    }

    public handle( message: DocumentMessage, data: Document[] ): void {
        switch ( message.action ) {
            case DocumentAction.CREATION:
                this.documentCreationHandler.handle( message, data );
                break;
            case DocumentAction.DUPLICATION:
                this.documentDuplicationHandler.handle( message, data );
                break;
            case DocumentAction.MODIFICATION:
                this.documentModificationHandler.handle( message, data );
                break;
            case DocumentAction.FERMETURE:
                this.documentClosingHandler.handle( message, data );
                break;
            case DocumentAction.INSOUMISSION:
                this.documentUnsubmissionHandler.handle( message, data );
                break;
            case DocumentAction.INVALIDATION:
                this.documentUnvalidationHandler.handle( message, data );
                break;
            case DocumentAction.OUVERTURE:
                this.documentOpeningHandler.handle( message, data );
                break;
            case DocumentAction.SAUVEGARDE:
                this.documentSavingHandler.handle( message, data );
                break;
            case DocumentAction.SOUMISSION:
                this.documentSubmissionHandler.handle( message, data );
                this.documentSubmissionMailingHandler.handle( message );
                break;
            case DocumentAction.SUPPRESSION:
                this.documentDeletionHandler.handle( message, data );
                break;
            case DocumentAction.TELECHARGEMENT:
                this.documentDownloadHandler.handle( message, data );
                break;
            case DocumentAction.VALIDATION:
                this.documentValidationHandler.handle( message, data );
                break;
            default:
                console.warn( `Impossible de traiter le message avec l'action = ${message.action} car celle-ci est inconnue!` );
                break;
        }
    }
}
