import { GabaritAction, GabaritMessage } from '@services';
import { Injectable } from '@angular/core';
import { Handler } from './handler';
import { Surcharge } from '@model';
import { GabaritDeletionHandler } from './gabarits/gabarit-deletion.handler';
import { GabaritCreationHandler } from './gabarits/gabarit-creation.handler';

@Injectable( {
    providedIn: 'root'
} )
export class GabaritHandler implements Handler<GabaritMessage> {

    constructor(
        private gabaritCreationHandler: GabaritCreationHandler,
        private gabaritDeletionHandler: GabaritDeletionHandler
    ) {
    }

    public handle( message: GabaritMessage, data: Surcharge[] ): void {
        switch ( message.action ) {
            case GabaritAction.SUPPRESSION:
                this.gabaritDeletionHandler.handle( message, data );
                break;
            case GabaritAction.CREATION:
                this.gabaritCreationHandler.handle( message, data );
                break;
            default:
                console.warn( `Impossible de traiter le message avec l'action = ${message.action} car celle-ci est inconnue!` );
                break;
        }
    }
}
