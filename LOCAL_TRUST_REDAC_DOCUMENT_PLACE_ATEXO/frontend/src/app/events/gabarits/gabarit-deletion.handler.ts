import { GabaritAction, GabaritMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Gabarit, Surcharge } from '@model';

@Injectable({
  providedIn: 'root'
})
export class GabaritDeletionHandler implements Handler<GabaritMessage> {

  @Output() suppression = new EventEmitter<Gabarit>();

  public handle(message: GabaritMessage, surcharges: Surcharge[]): void {
    const gabarit = message.gabarit;

    if (message.action === GabaritAction.SUPPRESSION) {
      console.debug(`Suppression du gabarit ${gabarit?.uuid} de la liste`);

      surcharges.forEach(surcharge => {
        if (surcharge?.gabarit?.uuid === gabarit?.uuid) {
          surcharge.gabarit = null;
        }
      });

      this.suppression.emit(message.gabarit);
    }
  }
}
