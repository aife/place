import { GabaritAction, GabaritMessage } from '@services';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Handler } from '../handler';
import { Gabarit, Niveau, Surcharge } from '@model';

@Injectable({
  providedIn: 'root'
})
export class GabaritCreationHandler implements Handler<GabaritMessage> {

  @Output() creation = new EventEmitter<Gabarit>();

  public handle(message: GabaritMessage, surcharges: Surcharge[]): void {
    console.debug(`Nouveau message de création d'un gabarit`);

    const gabarit = message.gabarit;

    if (message.action === GabaritAction.CREATION) {

      surcharges.forEach(surcharge => {
        if (surcharge?.niveau === Niveau[gabarit.niveau?.code]) {
          surcharge.gabarit = gabarit;
        }
      });

      this.creation.emit(gabarit);
    }
  }
}
