import { NotificationMessage, NotificationService } from '@services';
import { Notification } from '@model';
import { Injectable } from '@angular/core';
import { Handler } from './handler';

@Injectable( {
    providedIn: 'root'
} )
export class NotificationHandler implements Handler<NotificationMessage> {

    public constructor(
        private notificationService: NotificationService
    ) {
    }

    public handle( message: NotificationMessage ): void {
        this.notificationService.info( message as Notification );
    }
}
