import { Message } from '@services';

export interface Handler<T extends Message> {
    handle( message: T, data?: any ): void;
}
