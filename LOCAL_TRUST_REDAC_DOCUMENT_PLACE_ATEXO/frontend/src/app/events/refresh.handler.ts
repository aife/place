import { Message } from '@services';
import { Injectable } from '@angular/core';
import { Handler } from './handler';

@Injectable( {
    providedIn: 'root'
} )
export class RefreshHandler implements Handler<Message> {

    public handle( message: Message ): void {
        if ( message.rafraichissement ) {
            console.debug( 'Une nouvelle demande de rechargement = ', message );
            setTimeout( () => {
                console.debug( 'Rechargement' );

                window.location.reload();
            }, 3000 );

        }
    }
}
