interface String {
    empty(): boolean;

    filled(): boolean;

    extension(): string;

    nom(): string;
}

String.prototype.empty = function(): boolean {
    return this?.length === 0;
};


String.prototype.filled = function(): boolean {
    return !this.empty();
};

String.prototype.extension = function(): string {
    return this.match( /([0-9a-z]+)(?=[?#])|\w+$/gmi ).pop();
};

String.prototype.nom = function(): string {
    return this.match( /(.+)(?=\.\w+$)/gmi ).pop();
};
