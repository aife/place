interface Array<T> {
    empty(): boolean;

    filled(): boolean;
}

Array.prototype.empty = function(): boolean {
    return this?.length === 0;
};

Array.prototype.filled = function(): boolean {
    return !this.empty();
};

