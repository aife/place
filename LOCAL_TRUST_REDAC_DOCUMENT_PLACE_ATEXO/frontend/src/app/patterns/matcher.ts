export interface Matcher<T> {
    match( query: string, item: T ): boolean;
}
