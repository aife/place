export class Comparator<T> {

    /**
     * Comparing <code>ASC</code>
     * @param itemA the first item
     * @param itemB the second
     */
    public comparing( itemA: T, itemB: T ) {
        return itemA <= itemB ? -1 : 1;
    }
}
