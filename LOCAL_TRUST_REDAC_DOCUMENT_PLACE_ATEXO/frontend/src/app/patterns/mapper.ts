export interface Mapper<T, U> {
    map( from: T ): U;
}
