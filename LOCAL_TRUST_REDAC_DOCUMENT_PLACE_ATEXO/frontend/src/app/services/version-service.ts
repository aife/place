import { Version } from '@model';
import { Observable } from 'rxjs';

export abstract class VersionService {
    abstract recuperer(): Observable<Version>;
}
