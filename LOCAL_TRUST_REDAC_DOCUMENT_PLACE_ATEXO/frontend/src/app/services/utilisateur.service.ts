import { Utilisateur, UUID } from '@model';
import { Observable } from 'rxjs';

export abstract class UtilisateurService {
    abstract rechercher(): Observable<Utilisateur[]>;

    abstract recuperer( uuid: UUID ): Observable<Utilisateur>;
}
