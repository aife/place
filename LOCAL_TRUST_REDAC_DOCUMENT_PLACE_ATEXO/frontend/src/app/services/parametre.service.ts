import { Parametre } from '@model';
import { Observable } from 'rxjs';

export abstract class ParametreService {
    abstract recuperer(): Observable<Parametre<any>[]>;
}
