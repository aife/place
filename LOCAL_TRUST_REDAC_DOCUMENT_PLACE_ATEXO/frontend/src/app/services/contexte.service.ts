import { Contexte, Token } from '@model';
import { Observable } from 'rxjs';

export abstract class ContexteService {

    /**
     * Permet de récupérer le contexte courant
     * @param token le jeton
     * @param contexteUuid l'identifiant du contexte
     */
    abstract recuperer( token: Token, contexteUuid: string ): Observable<Contexte>;
}
