import { StockageService } from './stockage-service';

export { StorageKey } from './stockage-service';

export abstract class StockageSessionService extends StockageService {
}
