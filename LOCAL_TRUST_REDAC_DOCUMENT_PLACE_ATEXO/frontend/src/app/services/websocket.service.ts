import { Document, Gabarit, Notification, Utilisateur } from '@model';
import { Handler } from '../events/handler';

export enum DocumentAction {
    VALIDATION = 'validation',
    INVALIDATION = 'invalidation',
    SOUMISSION = 'soumission',
    INSOUMISSION = 'insoumission',
    OUVERTURE = 'ouverture',
    FERMETURE = 'fermeture',
    CREATION = 'creation',
    SUPPRESSION = 'suppression',
    MODIFICATION = 'modification',
    DUPLICATION = 'duplication',
    TELECHARGEMENT = 'telechargement',
    SAUVEGARDE = 'sauvegarde'
}

export enum GabaritAction {
    SUPPRESSION = 'suppression',
    CREATION = 'creation'
}

export interface Message {
    rafraichissement?: boolean;
}

export interface NotificationMessage extends Notification, Message {
}

export interface GabaritMessage extends Message {
    action: GabaritAction;
    gabarit?: Gabarit;
    agent?: Utilisateur;
}

export interface DocumentMessage extends Message {
    action: DocumentAction;
    document?: Document;
    agent?: Utilisateur;
}

export interface TopicHandler {
    channel?: string;
    handlers?: Handler<any>[];
    data?: any;
}

export abstract class WebsocketService {
    abstract connect( topics: TopicHandler[], attempt?: number );

    abstract disconnect();
}
