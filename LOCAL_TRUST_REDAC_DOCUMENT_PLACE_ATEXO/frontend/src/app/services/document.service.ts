import {
  CategorieDocument,
  Destinataire,
  Document,
  Environnement,
  Redirection,
  StatutDocument,
  Utilisateur
} from '@model';
import { Observable } from 'rxjs';

export abstract class DocumentService {
    abstract creer( document: Document ): Observable<Document>;

    abstract modifier( document: Document ): Observable<Document>;

    abstract soumettre( document: Document ): Observable<Document>;

    abstract insoumettre( document: Document ): Observable<Document>;

    abstract valider( document: Document, type: string ): Observable<Document>;

    abstract invalider( document: Document ): Observable<Document>;

    abstract editer( document: Document ): Observable<Redirection>;

    abstract telecharger( document: Document, nom: string ): void;

    abstract formater( document: Document ): string;

    abstract supprimer( document: Document ): Observable<void>;

    abstract dupliquer( document: Document ): Observable<Document>;

    abstract recuperer( expressions?: string[], categories?: CategorieDocument[], status?: StatutDocument[] ): Observable<Document[]>;

    abstract rechercher( environnement: Environnement, expressions?: string[], categories?: CategorieDocument[], status?: StatutDocument[] ): Observable<Document[]>;

    abstract log( document: Document ): Observable<Document[]>;

    abstract notifier( document: Document, destinataires: Destinataire[] ): Observable<Utilisateur[]>;

    abstract permalien( document: Document ): Observable<any>;
}
