import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Document } from '@model';
import { saveAs } from 'file-saver';
import { Observable } from 'rxjs';
import { FichierService } from '@services';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class FichierRemoteService extends FichierService {

    private endpoint = '/api-redac-document/fichiers';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public telecharger( uuid: string, nom: string, extension: string ): void {
        const url = `${this.endpoint}/${uuid}?action=telechargement`;
        const parameters = { nom: nom, extension: extension, disposition: 'attachment' };

        this.httpClient.get( url, { params: parameters, responseType: 'blob' } ).subscribe( blob => saveAs( blob, nom ) );
    }

    public supprimer( uuid: string ): Observable<Document> {
        const url = `${this.endpoint}/${uuid}`;

        return this.httpClient.delete<Document>( url );
    }
}
