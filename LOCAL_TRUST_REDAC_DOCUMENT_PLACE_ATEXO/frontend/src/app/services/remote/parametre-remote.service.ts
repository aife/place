import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Parametre } from '@model';
import { ParametreService } from '@services';
import { SessionManager } from '@managers';
import { Observable } from 'rxjs';

@Injectable( {
    providedIn: 'root'
} )
export class ParametreRemoteService extends ParametreService {

    private endpoint = '/api-redac-document/parametres';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }


    public recuperer(): Observable<Parametre<any>[]> {
        const environnement = this.sessionManager?.session?.contexte?.environnement;

        return this.httpClient.get<Parametre<any>[]>( `${this.endpoint}?environnement=${environnement?.uuid}` );
    }
}
