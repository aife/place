import { Injectable } from '@angular/core';
import {Modele, Utilisateur, UUID} from '@model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UtilisateurService } from '@services';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class UtilisateurRemoteService extends UtilisateurService {

    private endpoint = '/api-redac-document/utilisateurs';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public rechercher(): Observable<Utilisateur[]> {
        const session = this.sessionManager.session;
        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}`;

        return this.httpClient.get<Modele[]>( url, { observe: 'response' } ).pipe(
            switchMap(
                response => {
                    let result: Observable<Utilisateur[]>;
                    result = this.httpClient.get<Utilisateur[]>( url, { observe: 'response' } )
                    .pipe( map( r => r.body ) );
                    return result;
                }
            )
        );
    }

    public recuperer( uuid: UUID ): Observable<Utilisateur> {
        const url = `${this.endpoint}/${uuid}`;

        return this.httpClient.get<Utilisateur>( url );
    }

}
