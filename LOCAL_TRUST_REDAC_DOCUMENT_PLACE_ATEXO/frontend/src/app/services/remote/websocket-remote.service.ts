import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Message, SessionService, TopicHandler, WebsocketService } from '@services';
import { SessionManager } from '@managers';

interface Frame {
    body?: string;
}

@Injectable( {
    providedIn: 'root'
} )
export class WebsocketRemoteService extends WebsocketService {
    static SUBSCRIPTION_DELAY = 2000;
    static RETRY_DELAY = 5000;
    static RETRY_LIMIT = 10;
    private endpoint = '/api-redac-document/ws';
    client: any;

    constructor(
        private sessionManager: SessionManager,
        private sessionService: SessionService
    ) {
        super();
    }

    public connect( topics: TopicHandler[], attempt?: number ) {
        if ( undefined === attempt ) {
            attempt = 1;
        }

        const ws = new SockJS( this.endpoint );
        this.client = Stomp.over( ws );
        const that = this;

        console.log( `Connection en cours a ${this.client.ws.url} (from ${this.endpoint})` );

        this.client.connect( { user: this?.sessionManager?.session?.agent?.uuid }, _ => {
            console.info( `Maintenant connecté ${this.endpoint}` );
            setTimeout( () => {
                topics.forEach(
                    ( topic ) => {
                        console.info( `Souscription au topic ${topic.channel}` );
                        that.client.subscribe( topic.channel, ( data: Frame ) => {
                            const message: Message = JSON.parse( data.body );

                            topic.handlers.forEach( handler => handler.handle( message, topic.data ) );
                        } );
                    }
                );
            }, WebsocketRemoteService.SUBSCRIPTION_DELAY );

            this.sessionService.connect().subscribe( () => console.log( 'Utilisateur connecté' ) );

        }, ( error ) => {
            console.error( `Websocket à l'URL ${this.endpoint} envoit : ${error}` );

            attempt++;

            console.debug( `Tentative de reconnection à ${this.endpoint} après ${WebsocketRemoteService.RETRY_DELAY}ms. Essai #${attempt}` );

            if ( attempt < WebsocketRemoteService.RETRY_LIMIT ) {
                setTimeout( () => this.connect( topics, attempt ), WebsocketRemoteService.RETRY_DELAY );
            }
        } );
    }

    public disconnect() {
        if ( this.client !== null ) {
            this.client.disconnect( _ => {
            }, { user: this?.sessionManager?.session?.agent?.uuid } );
        }

        console.debug( `Maintenant déconnecté de l'url ${this.endpoint}` );
    }

}
