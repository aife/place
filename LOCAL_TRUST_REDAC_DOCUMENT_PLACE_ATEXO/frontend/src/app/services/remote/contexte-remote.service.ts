import { Injectable } from '@angular/core';
import { Contexte, Token } from '@model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ContexteService } from '@services';

@Injectable( {
    providedIn: 'root'
} )
export class ContexteRemoteService extends ContexteService {

    private endpoint = '/api-redac-document/contextes';

    constructor(
        private httpClient: HttpClient
    ) {
        super();
    }

    public recuperer( token: Token, contexte: string ): Observable<Contexte> {
        const options = {
            headers: new HttpHeaders( {
                Authorization: `Bearer ${token?.access_token}`
            } )
        };

        return this.httpClient.get<Contexte>( `${this.endpoint}/${contexte}`, options );
    }
}
