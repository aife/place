import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  CategorieDocument,
  Destinataire,
  Document,
  Environnement,
  Redirection,
  StatutDocument,
  Utilisateur
} from '@model';
import { saveAs } from 'file-saver';
import { Observable } from 'rxjs';
import { DocumentService } from '@services';
import { SessionManager } from '@managers';
import { of } from 'rxjs/internal/observable/of';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentRemoteService extends DocumentService {

    private endpoint = '/api-redac-document/documents';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public creer( document: Document ): Observable<Document> {
        const parameters = { contexte: this.sessionManager?.session?.contexte?.uuid };

        return this.httpClient.post<Document>( this.endpoint, document, { params: parameters } );
    }

    public modifier( document: Document ): Observable<Document> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.patch<Document>( url, document );
    }

    public soumettre( document: Document ): Observable<Document> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.patch<Document>( url, document, { params: { action: 'soumission' } } );
    }

    public insoumettre( document: Document ): Observable<Document> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.patch<Document>( url, document, { params: { action: 'insoumission' } } );
    }

    public valider( document: Document, type: string ): Observable<Document> {
        const url = `${this.endpoint}/${document.id}`;

        document.modification.commentaire = null;

        return this.httpClient.patch<Document>( url, document, { params: { action: 'validation', type: type } } );
    }

    public invalider( document: Document ): Observable<Document> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.patch<Document>( url, document, { params: { action: 'invalidation' } } );
    }

    public editer( document: Document ): Observable<Redirection> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.patch<Redirection>( url, document, { params: { action: 'edition' } } );
    }

    public telecharger( document: Document, nom: string ): void {
        const url = `${this.endpoint}/${document.id}/revisions/${document.uuid}`;

        this.httpClient.get( url, { params: { action: 'telechargement', disposition: 'attachment' }, responseType: 'blob' } )
            .subscribe( blob => saveAs( blob, nom ) );
    }

    public formater( document: Document ): string {
        return (document.nom.substring( 0, document.nom.lastIndexOf( '.' ) ) || document.nom) + '.' + document.extension;
    }

    public supprimer( document: Document ): Observable<void> {
        const url = `${this.endpoint}/${document.id}`;

        return this.httpClient.delete<void>( url );
    }

    public dupliquer( document: Document ): Observable<Document> {
        const url = `${this.endpoint}/${document?.id}`;
        const parameters = { action: 'duplication' };

        return this.httpClient.post<Document>( url, document, { params: parameters } );
    }

    public recuperer( expressions?: string[], categories?: CategorieDocument[], status?: StatutDocument[] ): Observable<Document[]> {
        const contexte = this.sessionManager?.session?.contexte;

        return this.httpClient.get<Document[]>( this.endpoint, {
            params: {
                environnement: contexte?.environnement?.uuid,
                contexte: contexte?.uuid,
                ...expressions ? { expressions: expressions } : {},
                ...categories ? { categories: categories } : {},
                ...status ? { status: status } : {}
            }
        } );
    }

    public rechercher(
        environnement: Environnement,
        expressions?: string[],
        categories?: CategorieDocument[],
        status?: StatutDocument[]
    ): Observable<Document[]> {
        return this.httpClient.get<Document[]>( this.endpoint, {
            params: {
                environnement: environnement?.uuid,
                ...expressions ? { expressions: expressions } : {},
                ...categories ? { categories: categories } : {},
                ...status ? { status: status } : {}
            }
        } );
    }

    public log( document: Document ): Observable<Document[]> {
        const url = `${this.endpoint}/${document.id}/revisions`;

        return this.httpClient.get<Document[]>( url );
    }

    public notifier( document: Document, destinataires: Destinataire[] ): Observable<Utilisateur[]> {
        const url = `${this.endpoint}/${document.id}`;

        console.log( `Envoi du mail de validation du document ${document.nom} vers les destinaires suivants = `, destinataires );

        return this.httpClient.post<Utilisateur[]>( url, destinataires, { params: { action: 'notification' } } );
    }

    public permalien( document: Document ): Observable<string> {
        return of( `${this.endpoint}/${document.id}?disposition=inline&action=telechargement&format=pdf` );
    }
}
