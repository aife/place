import { Injectable } from '@angular/core';
import { Connection } from '@model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from '@services';
import { SessionManager } from '@managers';


@Injectable( {
    providedIn: 'root'
} )
export class SessionRemoteService extends SessionService {

    private endpoint = '/api-redac-document/sessions';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public connect(): Observable<Connection> {
        const url = `${this.endpoint}?action=connect&environnement=${this.sessionManager?.session?.contexte?.environnement?.uuid}`;

        return this.httpClient.get( url );
    }

    public disconnect(): Observable<Connection> {
        const url = `${this.endpoint}?action=disconnect`;

        return this.httpClient.get( url );
    }

}
