import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Modele, Referentiel} from '@model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ReferentielService } from '@services';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class ReferentielRemoteService extends ReferentielService {

    private endpoint = '/api-redac-document/referentiels';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public charger(): void {
        const session = this.sessionManager.session;

        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}`;

      this.httpClient.get<Modele[]>( url, { observe: 'response' } ).subscribe(
            response => {
                if ( response && response.ok ) {
                    console.debug( `Référentiels déjà en cache pour la plateforme ${session?.contexte?.plateforme}` );
                } else {
                    console.debug( `Chargement des référentiels et mis en cache pour la plateforme ${session?.contexte?.plateforme} à l'URL ${url}` );

                    this.httpClient.get<Referentiel>( url, { observe: 'response' } )
                        .pipe( map( r => r.body ) )
                        .subscribe( r => console.debug( `Référentiels pour mise en cache récupérés depuis l'URL ${url} :`, r ) );
                }
            }
        );
    }

    public rechercherParCode( code: string ): Observable<Referentiel> {
        const session = this.sessionManager.session;
        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}`;

        return this.httpClient.get<Referentiel[]>( url, { observe: 'response' } )
          .pipe( map( r => r.body ) ,
            map( referentiels => referentiels.find( ref => ref.code.toLowerCase() === code.toLowerCase() ) )
        );
    }

    public rechercherParType( type: string ): Observable<Referentiel[]> {
      console.log("rechercherParType called");
        const session = this.sessionManager.session;
        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}`;

        return this.httpClient.get<Referentiel[]>( url, { observe: 'response' } )
          .pipe( map( r => r.body ) ,
            map( referentiels => referentiels.filter( ref => ref.type.toLowerCase() === type.toLowerCase() ) )
        );
    }
}
