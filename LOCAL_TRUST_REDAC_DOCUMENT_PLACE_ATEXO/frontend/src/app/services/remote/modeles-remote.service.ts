import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CategorieModele, Modele} from '@model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ModelesService} from '@services';
import {SessionManager} from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class ModelesRemoteService extends ModelesService {

    private endpoint = '/api-redac-document/modeles';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public recupererParCategorie( categorie: CategorieModele ): Observable<Modele[]> {
        const session = this.sessionManager?.session;

        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}&filtre=categorie&categorie=${CategorieModele[categorie]}`;

        return this.httpClient.get<Modele[]>( url, { observe: 'response' } )
          .pipe( map( r => r.body ) );
    }

    public recupererModelesAdministrable(): Observable<Modele[]> {
        const session = this.sessionManager?.session;
        const url = `${this.endpoint}?contexte=${session?.contexte?.uuid}&filtre=administrable`;

        return this.httpClient.get<Modele[]>( url, { observe: 'response' } )
          .pipe( map( r => r.body ) );
    }
}
