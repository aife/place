import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Version } from '@model';
import { Observable } from 'rxjs';
import { VersionService } from '@services';

@Injectable( {
    providedIn: 'root'
} )
export class CredentialRemoteService extends VersionService {

    private endpoint = '/api-redac-document/actuator';

    constructor( private httpClient: HttpClient ) {
        super();
    }

    public recuperer(): Observable<Version> {
        const url = `${this.endpoint}/info`;
        const headers = new HttpHeaders( { 'Accept': 'application/json' } );

        return this.httpClient.get<Version>( url, { headers: headers } );
    }

}
