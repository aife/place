import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Authentification } from '@model';
import { Observable } from 'rxjs';
import { environment } from '@environment';
import { AuthentificationService } from '@services';

@Injectable( {
    providedIn: 'root'
} )
export class AuthentificationRemoteService extends AuthentificationService {

    private static HEADERS = new HttpHeaders( { 'Content-Type': 'application/x-www-form-urlencoded' } );
    private endpoint = '/api-redac-document/authentication';

    constructor( private httpClient: HttpClient ) {
        super();
    }

    public authentifier(): Observable<Authentification> {
        const url = `${this.endpoint}/realms/${environment.realm}/protocol/openid-connect/token`;
        const body = `grant_type=${environment.type}&client_id=${environment.client}&username=${environment.login}&password=${environment.password}`;

        return this.httpClient.post<Authentification>( url, body, { headers: AuthentificationRemoteService.HEADERS } );
    }
}
