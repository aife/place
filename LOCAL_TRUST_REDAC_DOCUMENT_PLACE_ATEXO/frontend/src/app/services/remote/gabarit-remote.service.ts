import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Gabarit, Modele, Niveau} from '@model';
import {saveAs} from 'file-saver';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {GabaritService} from '@services';
import {SessionManager} from '@managers';

@Injectable({
  providedIn: 'root'
})
export class GabaritRemoteService extends GabaritService {

  private endpoint = '/api-redac-document/gabarits';

  constructor(
    private httpClient: HttpClient,
    private sessionManager: SessionManager
  ) {
    super();
  }

  public recupererVersionEligible(modele: Modele): Observable<Gabarit> {
    return this.httpClient.get<Gabarit>(this.endpoint, {
      params: {
        environnement: this.sessionManager?.session?.contexte?.environnement?.uuid,
        modele: modele.uuid,
        filtre: 'eligible'
      }
    });
  }

  public recuperer(modele: Modele, niveau: Niveau): Observable<Gabarit> {
    switch (niveau) {
      case Niveau.AGENT:
        return this.httpClient.get<Gabarit>(this.endpoint, {
          params: {
            niveau: Niveau.AGENT,
            modele: modele?.uuid,
            agent: this?.sessionManager?.session?.agent?.uuid
          }
        });
      case Niveau.ENTITE_ACHAT:
        return this.httpClient.get<Gabarit>(this.endpoint, {
          params: {
            niveau: Niveau.ENTITE_ACHAT,
            modele: modele?.uuid,
            service: this?.sessionManager?.session?.agent?.service?.uuid
          }
        });
      case Niveau.ORGANISME:
        return this.httpClient.get<Gabarit>(this.endpoint, {
          params: {
            niveau: Niveau.ORGANISME,
            modele: modele?.uuid,
            organisme: this?.sessionManager?.session?.agent?.service?.organisme?.uuid
          }
        });
      case Niveau.PLATEFORME:
        return this.httpClient.get<Gabarit>(this.endpoint, {
          params: {
            niveau: Niveau.PLATEFORME,
            modele: modele?.uuid,
            environnement: this?.sessionManager?.session?.contexte?.environnement?.uuid
          }
        });
    }
  }


  public creer(surcharge: Gabarit): Observable<Gabarit> {
    return this.httpClient.post<Gabarit>(this.endpoint, surcharge, {
      params: {
        contexte: this.sessionManager?.session?.contexte?.uuid
      }
    });
  }

  public recupererRevisions(surcharge: Gabarit): Observable<Gabarit[]> {
    return this.httpClient.get<Gabarit[]>(`${this.endpoint}/${surcharge.id}/revisions`);
  }

  public supprimer(surcharge: Gabarit): Observable<Gabarit> {
    return this.httpClient.delete<Gabarit>(`${this.endpoint}/${surcharge.id}`);
  }

  public telecharger(surcharge: Gabarit): void {
    let extension: string = surcharge.nom.split('.').pop();
    const parameters = {action: 'telechargement', disposition: 'attachment', format: extension};

    this.httpClient.get(`${this.endpoint}/${surcharge.id}/revisions/${surcharge.uuid}`, {
      params: parameters,
      responseType: 'blob'
    })
      .subscribe(blob => saveAs(blob, surcharge.nom));
  }

  public permalien(surcharge: Gabarit): Observable<string> {
    return of(`${this.endpoint}/${surcharge.id}?disposition=inline&action=telechargement&format=pdf`);
  }
}
