import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Canevas } from '@model';
import { Observable } from 'rxjs';
import { CanevasService } from '@services';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class CanevasRemoteService extends CanevasService {

    private endpoint = '/api-redac-document/canevas';

    constructor(
        private httpClient: HttpClient,
        private sessionManager: SessionManager
    ) {
        super();
    }

    public rechercher( consultation: string, type: string, lot: number ): Observable<Canevas[]> {
        return this.httpClient.get<Canevas[]>( this.endpoint, {
            params: {
                contexte: this.sessionManager?.session?.contexte?.uuid,
                consultation: consultation,
                type: type,
                ...lot ? { lot: lot } : {}
            }
        } );
    }
}
