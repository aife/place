import { Referentiel } from '@model';
import { Observable } from 'rxjs';

export abstract class ReferentielService {
    abstract charger(): void;

    abstract rechercherParCode( code: string ): Observable<Referentiel>;

    abstract rechercherParType( type: string ): Observable<Referentiel[]>;
}
