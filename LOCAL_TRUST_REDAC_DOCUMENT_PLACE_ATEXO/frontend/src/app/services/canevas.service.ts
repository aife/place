import { Canevas } from '@model';
import { Observable } from 'rxjs';

export abstract class CanevasService {
    abstract rechercher(
        consultation: string,
        type: string,
        lot: number
    ): Observable<Canevas[]>;
}
