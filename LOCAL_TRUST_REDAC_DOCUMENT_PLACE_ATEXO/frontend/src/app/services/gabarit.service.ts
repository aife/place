import { Gabarit, Modele, Niveau } from '@model';
import { Observable } from 'rxjs';

export abstract class GabaritService {

    abstract recuperer( modele: Modele, niveau: Niveau ): Observable<Gabarit>;

    abstract recupererVersionEligible( modele: Modele ): Observable<Gabarit>;

    abstract creer( surcharge: Gabarit ): Observable<Gabarit>;

    abstract telecharger( surcharge: Gabarit ): void;

    abstract recupererRevisions( surcharge: Gabarit ): Observable<Gabarit[]>;

    abstract supprimer( surcharge: Gabarit ): Observable<Gabarit>;

    abstract permalien( surcharge: Gabarit ): Observable<string>;
}
