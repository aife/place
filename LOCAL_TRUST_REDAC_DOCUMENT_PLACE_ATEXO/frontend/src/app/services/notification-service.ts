import { Observable } from 'rxjs';
import { Notification, ResultationNotification } from '@model';

export abstract class NotificationService {
    abstract info( alerte: Notification ): void;

    abstract fatal( alerte: Notification ): void;

    abstract erreur( alerte: Notification ): void;

    abstract succes( alerte: Notification ): void;

    abstract confirmation( alerte: Notification ): Observable<ResultationNotification>;
}
