import { Inject, Injectable } from '@angular/core';
import { StockageLocalService, StorageKey } from '@services';
import { LOCAL_STORAGE, StorageService as Storage } from 'ngx-webstorage-service';

@Injectable( {
    providedIn: 'root'
} )
export class StockageLocalClientService extends StockageLocalService {
    constructor(
        @Inject( LOCAL_STORAGE ) private localStorage: Storage
    ) {
        super();
    }

    public existe( cle: StorageKey | string ): boolean {
        return this.localStorage.has( cle.toString() );
    }

    public recuperer<V>( cle: StorageKey | string, defaultValue?: V ): V {
        return this.existe( cle ) ? this.localStorage.get( cle.toString() ) as V : defaultValue;
    }

    public ajouter<V>( cle: StorageKey | string, valeur: V ) {
        this.localStorage.set( cle.toString(), valeur );
    }

    public supprimer( cle: StorageKey | string ) {
        this.localStorage.remove( cle.toString() );
    }

}
