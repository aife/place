import { Injectable, TemplateRef } from '@angular/core';
import { ModaleService } from '@services';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalOptions } from 'ngx-bootstrap/modal/modal-options.class';
import { Modale } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class ModaleClientService extends ModaleService {

    constructor( private bsModalService: BsModalService ) {
        super();
    }

    afficher<T = Object>( content: string | TemplateRef<any> | (new( ...args: any[] ) => T), options?: ModalOptions<T> ): Modale {
        return this.bsModalService.show( content, { backdrop: 'static', ...options } );
    }

}
