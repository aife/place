import { Inject, Injectable } from '@angular/core';
import { StockageSessionService, StorageKey } from '@services';
import { SESSION_STORAGE, StorageService as Storage } from 'ngx-webstorage-service';

@Injectable( {
    providedIn: 'root'
} )
export class StockageSessionClientService extends StockageSessionService {
    constructor(
        @Inject( SESSION_STORAGE ) private sessionStorage: Storage
    ) {
        super();
    }

    public existe( cle: StorageKey | string ): boolean {
        return this.sessionStorage.has( cle.toString() );
    }

    public recuperer<V>( cle: StorageKey | string, defaultValue?: V ): V {
        return this.existe( cle ) ? this.sessionStorage.get( cle.toString() ) as V : defaultValue;
    }

    public ajouter<V>( cle: StorageKey | string, valeur: V ) {
        this.sessionStorage.set( cle.toString(), valeur );
    }

    public supprimer( cle: StorageKey | string ) {
        this.sessionStorage.remove( cle.toString() );
    }
}
