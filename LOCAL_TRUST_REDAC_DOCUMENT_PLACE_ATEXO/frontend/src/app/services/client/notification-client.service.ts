import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { NotificationService } from '@services';
import { from, Observable } from 'rxjs';
import { Notification, ResultationNotification } from '@model';

@Injectable( {
    providedIn: 'root'
} )
export class NotificationClientService extends NotificationService {

    info( options: Notification ): void {
        Swal.fire( {
            ...{
                position: 'top-end',
                icon: 'info',
                showConfirmButton: false
            }, ...options
        } );
    }

    fatal( options: Notification ): void {
        Swal.fire( {
            ...{
                backdrop: '#fff',
                background: '#fff',
                allowOutsideClick: false,
                allowEscapeKey: false,
                icon: 'error',
                title: 'Oops...',
                showCancelButton: false,
                showCloseButton: false,
                showConfirmButton: false,
            }, ...options
        } );
    }

    erreur( options: Notification ): void {
        Swal.fire( {
            ...{
                icon: 'error',
                title: 'Oops...',
                showCancelButton: false,
                showCloseButton: false,
                showConfirmButton: false,
                allowOutsideClick: true,
            }, ...options
        } );
    }

    succes( options: Notification ): void {
      Swal.fire( {
        ...{
          position: 'top-end',
          icon: 'success',
          showConfirmButton: false
        }, ...options
      } );
    }

    confirmation( options: Notification ): Observable<ResultationNotification> {
        return from( Swal.mixin( {
            customClass: {
                confirmButton: 'btn btn-success ms-2',
                cancelButton: 'btn btn-secondary'
            },
            buttonsStyling: false
        } ).fire( {
                ...{
                    icon: 'warning',
                    confirmButtonText: 'Valider',
                    cancelButtonText: 'Annuler',
                    showCancelButton: true,
                    reverseButtons: true,
                    allowOutsideClick: false
                }, ...options
            }
        ) );
    }
}
