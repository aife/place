import { StockageService } from './stockage-service';

export { StorageKey, Application } from './stockage-service';

export abstract class StockageLocalService extends StockageService {
}
