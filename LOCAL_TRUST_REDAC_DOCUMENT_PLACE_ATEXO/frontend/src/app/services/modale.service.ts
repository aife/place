import { TemplateRef } from '@angular/core';
import { ModalOptions } from 'ngx-bootstrap/modal/modal-options.class';
import { Modale } from '@model';

export abstract class ModaleService {
    abstract afficher<T = Object>( content: string | TemplateRef<any> | (new( ...args: any[] ) => T), options?: ModalOptions<T> ): Modale;
}
