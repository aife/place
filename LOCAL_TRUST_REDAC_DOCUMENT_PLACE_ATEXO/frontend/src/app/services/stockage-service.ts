import { Contexte, Plateforme } from '@model';
import { NAMESPACE, WILDCARD } from '@managers';

export enum Application {
  REDACTION
}

class Builder {
  private _application: Application;
  private _plateforme: Plateforme;
  private _contexte: Contexte;
  private _cle: string;

  public application(application: Application): Builder {
    this._application = application;

    return this;
  }

  public plateforme(plateforme: Plateforme): Builder {
    this._plateforme = plateforme;

    return this;
  }

  public cle(cle: string): Builder {
    this._cle = cle;

    return this;
  }

  public contexte(contexte: Contexte): Builder {
    this._contexte = contexte;

    return this;
  }

  public build(): StorageKey {
    if (!this._cle) {
      throw new Error(`La clé est obligatoire, mais n'est pas renseignée`);
    }

    if (this._contexte && !this._plateforme) {
      this._plateforme = this._contexte.plateforme;
    }

    return new StorageKey(this._cle, this._application, this._plateforme, this._contexte);
  }
}

export class StorageKey {
  public static with: Builder = new Builder();

  constructor(
    protected cle: string,
    protected application?: Application,
    protected plateforme?: Plateforme,
    protected contexte?: Contexte
  ) {
  }

  public toString(): string {
    let value;

    switch (this.application) {
      case Application.REDACTION:
        value = `${NAMESPACE}/${this.plateforme ? this.plateforme : WILDCARD}/${this.contexte?.uuid ? this.contexte?.uuid : WILDCARD}/${this.cle}`;
        break;
      default:
        value = this.cle;
        break;

    }

    return value;
  }
}

export abstract class StockageService {
  abstract recuperer<V>(cle: StorageKey, defaut?: V): V;

  abstract ajouter<V>(cle: StorageKey, valeur: V);

  abstract existe(cle: StorageKey): boolean;

  abstract supprimer(cle: StorageKey);
}
