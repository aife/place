export abstract class SessionService {
    abstract connect();

    abstract disconnect();
}
