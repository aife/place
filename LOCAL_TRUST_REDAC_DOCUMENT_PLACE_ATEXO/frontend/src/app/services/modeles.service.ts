import { CategorieModele, Modele } from '@model';
import { Observable } from 'rxjs';

export abstract class ModelesService {
    abstract recupererParCategorie( categorie: CategorieModele ): Observable<Modele[]>;

    abstract recupererModelesAdministrable(): Observable<Modele[]>;
}
