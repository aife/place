import { Document } from '@model';
import { Observable } from 'rxjs';

export abstract class FichierService {
    abstract telecharger( uuid: string, nom: string, extension: string ): void;

    abstract supprimer( uuid: string ): Observable<Document>;
}
