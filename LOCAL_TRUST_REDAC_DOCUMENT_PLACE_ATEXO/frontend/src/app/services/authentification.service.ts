import { Authentification } from '@model';
import { Observable } from 'rxjs';

export abstract class AuthentificationService {
    abstract authentifier(): Observable<Authentification>;
}
