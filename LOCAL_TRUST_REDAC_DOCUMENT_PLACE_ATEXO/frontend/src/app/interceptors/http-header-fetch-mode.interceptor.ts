import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionManager } from '@managers';
import { Application, StockageLocalService, StorageKey } from '@services';

const POOLING = 'pooling';

@Injectable( {
    providedIn: 'root'
} )
export class HttpHeaderFetchModeInterceptor implements HttpInterceptor {
    constructor(
        private sessionManager: SessionManager,
        private stockageLocalService: StockageLocalService
    ) {
    }

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
        const pooling = this.stockageLocalService.existe(
            StorageKey.with.application( Application.REDACTION ).plateforme( this.sessionManager?.session?.contexte?.plateforme ).cle( POOLING ).build()
        );

        if ( !request.headers.has( 'Fetch-mode' ) && this.sessionManager?.session?.token?.access_token ) {
            request = request.clone( {
                setHeaders: {
                    'Fetch-mode': pooling ? 'pooling' : 'callback'
                }
            } );
        }

        return next.handle( request );
    }
}
