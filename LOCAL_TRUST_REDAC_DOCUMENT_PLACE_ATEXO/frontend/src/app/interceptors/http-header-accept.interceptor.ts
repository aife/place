import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable( {
    providedIn: 'root'
} )
export class HttpHeaderAcceptInterceptor implements HttpInterceptor {

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        if ( !request.headers.has( 'Accept' ) ) {
            request = request.clone( {
                setHeaders: {
                    'Accept': `application/atexo.redacdocument.v2+json`
                }
            } );
        }

        return next.handle( request );
    }
}
