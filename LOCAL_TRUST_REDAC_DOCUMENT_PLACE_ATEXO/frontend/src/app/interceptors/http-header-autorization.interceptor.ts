import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class HttpHeaderAutorizationInterceptor implements HttpInterceptor {
    constructor( private sessionManager: SessionManager ) {
    }

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        if ( !request.headers.has( 'Authorization' ) && this.sessionManager?.session?.token?.access_token ) {
            request = request.clone( {
                setHeaders: {
                    Authorization: `Bearer ${this.sessionManager?.session?.token?.access_token}`
                }
            } );
        }

        return next.handle( request );
    }
}
