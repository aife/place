import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificationService } from '@services';

@Injectable( {
    providedIn: 'root'
} )
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor( private router: Router, private alerteService: NotificationService ) {
    }

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
        return next.handle( request )
            .pipe(
                catchError( ( error: HttpErrorResponse ) => {
                    console.error( `Ooops. Une erreur est survenue au niveau du ${error.error instanceof ErrorEvent ? 'client' : 'serveur'} = ${error.message} (${error.status} | ${error.statusText})` );

                    switch ( error.status ) {
                        case 401:
                            this.alerteService.erreur( { html: 'Votre session est maintenant expirée, merci de recharger votre navigateur.' } );

                            break;
                        case 500:
                            this.alerteService.erreur( { html: `Une erreur technique s'est produite, merci de recharger votre navigateur.` } );

                            break;
                        default:
                            // nothing to display
                            break;
                    }

                    return throwError( error );
                } )
            );
    }
}
