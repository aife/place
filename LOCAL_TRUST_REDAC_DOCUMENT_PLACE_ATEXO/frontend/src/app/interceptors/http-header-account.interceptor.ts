import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class HttpHeaderAccountInterceptor implements HttpInterceptor {
    constructor( private sessionManager: SessionManager ) {
    }

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        if ( !request.headers.has( 'Account' ) && this.sessionManager?.session?.token?.access_token ) {
            request = request.clone( {
                setHeaders: {
                    'Account': `${this.sessionManager?.session?.agent?.uuid}`
                }
            } );
        }

        return next.handle( request );
    }
}
