import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionManager } from '@managers';

@Injectable( {
    providedIn: 'root'
} )
export class HttpHeaderContextInterceptor implements HttpInterceptor {
    constructor( private sessionManager: SessionManager ) {
    }

    intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        if ( !request.headers.has( 'Context' ) && this.sessionManager?.session?.token?.access_token ) {
            request = request.clone( {
                setHeaders: {
                    'Context': `${this.sessionManager?.session?.contexte?.uuid}`
                }
            } );
        }

        return next.handle( request );
    }
}
