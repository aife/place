import { Session } from '@model';

export const NAMESPACE = 'com/atexo/redaction';
export const WILDCARD = '*';

export abstract class SessionManager {

    /**
     * Permet de récupérer la session utilisateur courante
     */
    abstract get session(): Session;

    /**
     * Permet de récupérer la session utilisateur courante
     */
    abstract set session( session: Session );

    /**
     * Active ou désactive l'avertissement utilisateur
     * @param valeur vrai pour activer l'avertissement, faux sinon
     */
    abstract avertir( valeur: boolean ): void;

    /**
     * Permet de savoir si la session est ok
     */
    abstract get ok(): boolean;
}
