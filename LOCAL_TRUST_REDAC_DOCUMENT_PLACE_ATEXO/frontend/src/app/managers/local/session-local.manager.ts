import { Injectable } from '@angular/core';
import { Session } from '@model';
import { Application, StockageLocalService, StockageSessionService, StorageKey } from '@services';
import { SessionManager } from '@managers';

const SESSION = 'session';
const DISCLAIMER = 'initComponent';

@Injectable( {
    providedIn: 'root'
} )
export class SessionLocalManager extends SessionManager {
    private cle: StorageKey;

    constructor(
        private stockageSessionService: StockageSessionService,
        private stockageLocalService: StockageLocalService
    ) {
        super();
    }

    public get session(): Session {
        if ( !this.cle || !this.stockageSessionService.existe( this.cle ) ) {
            return null;
        }

        return this.stockageSessionService.recuperer<Session>( this.cle );
    }

    public set session( session: Session ) {
        this.cle = StorageKey.with
            .application( Application.REDACTION )
            .contexte( session.contexte )
            .cle( SESSION )
            .build();

        this.stockageSessionService.ajouter<Session>( this.cle, session );
    }

    public avertir( value: boolean ): void {
        const cle = StorageKey.with
            .cle( DISCLAIMER )
            .build();

        if ( value ) {
            this.stockageLocalService.supprimer( cle );
        } else {
            this.stockageLocalService.ajouter( cle, new Date().toISOString() );
        }
    }

    get ok(): boolean {
        return !!this.session?.agent && !!this.session?.contexte;
    }
}
