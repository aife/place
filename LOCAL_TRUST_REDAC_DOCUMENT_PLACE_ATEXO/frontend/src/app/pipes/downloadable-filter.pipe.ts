import { Pipe, PipeTransform } from '@angular/core';
import { ActionDocument, Document } from '@model';

@Pipe( {
    name: 'downloadable_event_filter',
    pure: false
} )
export class DownloadableEventFilterPipe implements PipeTransform {
    transform( item: Document ): boolean {

        // actions disponibles pour téléchargement
        let actions: ActionDocument[] = [ActionDocument.CREATION, ActionDocument.TELECHARGEMENT, ActionDocument.ANNULATION, ActionDocument.FERMETURE_AVEC_MODIFICATION, ActionDocument.ENREGISTREMENT, ActionDocument.DEMANDE_VALIDATION, ActionDocument.REFUS_DEMANDE_VALIDATION, ActionDocument.INVALIDATION, ActionDocument.VALIDATION];

        return actions.includes( item.action );
    }
}
