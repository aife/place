import { Pipe, PipeTransform } from '@angular/core';
import { ActionDocument, Document, StatutDocument } from '@model';

@Pipe( {
    name: 'event_filter',
    pure: false
} )
export class EventFilterPipe implements PipeTransform {
    transform( items: Document[] ): Document[] {

        // actions à afficher
        const actions: ActionDocument[] = [
            ActionDocument.CREATION,
            ActionDocument.MODIFICATION_DU_NOM,
            ActionDocument.NOTIFICATION,
            ActionDocument.ANNULATION,
            ActionDocument.FERMETURE_AVEC_MODIFICATION,
            ActionDocument.ENREGISTREMENT,
            ActionDocument.DEMANDE_VALIDATION,
            ActionDocument.REFUS_DEMANDE_VALIDATION,
            ActionDocument.INVALIDATION,
            ActionDocument.VALIDATION,
            ActionDocument.DUPLICATION
        ];

        // statuts à afficher
        const statuts: StatutDocument[] = [];

        return items.filter( item => (statuts.includes( item.statut ) || actions.includes( item.action )) );
    }
}
