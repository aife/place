import { Pipe, PipeTransform } from '@angular/core';
import { Document } from '@model';
import { BaseDocumentComplexFilter } from './base-document-complex-filter';

@Pipe( {
    name: 'document_complex_counter',
    pure: false
} )
export class DocumentComplexCounterPipe extends BaseDocumentComplexFilter implements PipeTransform {
    transform( items: Document[] ): any {
        return super.transform( items ).length;
    }
}
