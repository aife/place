import { CategorieModele, Document } from '@model';

export abstract class BaseDocumentSimpleFilter {
    transform( items: Document[] ): any {
        // filter items array, items which match and return true will be
        // kept, false will be filtered out

        return items.filter( item => CategorieModele.DOCUMENT_CANEVAS !== item?.modele?.type?.parent?.code );
    }
}
