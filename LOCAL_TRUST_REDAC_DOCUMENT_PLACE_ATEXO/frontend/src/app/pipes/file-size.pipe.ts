import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe( { name: 'fileSize' } )
@Injectable( {
    providedIn: 'root'
} )
export class FileSizePipe implements PipeTransform {
    transform( value: number, args: Array<any> = null ): string {
        const units = [' Octets', ' Ko', ' Mo', ' Go', ' To'];

        if ( isNaN( value ) ) {
            return '0';
        }
        let amountOf2s = Math.floor( Math.log( +value ) / Math.log( 2 ) );
        if ( amountOf2s < 1 ) {
            amountOf2s = 0;
        }
        const i = Math.floor( amountOf2s / 10 );

        value = +value / Math.pow( 2, 10 * i );

        // arrondi 2 chiffres après la virgule
        if ( value.toString().length > value.toFixed( 2 ).toString().length ) {
            return value.toFixed( 2 ) + units[i];
        }
        return value + units[i];
    }
}
