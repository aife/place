import { CategorieModele, Document } from '@model';

export abstract class BaseDocumentComplexFilter {
    transform( items: Document[] ): any {
        return items.filter( item => CategorieModele.DOCUMENT_CANEVAS === item?.modele?.type?.parent?.code );
    }
}
