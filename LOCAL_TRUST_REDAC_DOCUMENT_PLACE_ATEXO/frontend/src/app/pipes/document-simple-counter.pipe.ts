import { Pipe, PipeTransform } from '@angular/core';
import { BaseDocumentSimpleFilter } from './base-document-simple-filter';
import { Document } from '@model';

@Pipe( {
    name: 'document_simple_counter',
    pure: false
} )
export class DocumentSimpleFilterPipe extends BaseDocumentSimpleFilter implements PipeTransform {
    transform( items: Document[] ): any {
        return super.transform( items ).length;
    }
}
