import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe( { name: 'safe' } )
export class SafePipe implements PipeTransform {

    constructor( private sanitizer: DomSanitizer ) {
    }

    transform( url ) {
        return url ? this.sanitizer.bypassSecurityTrustResourceUrl( url ) : this.sanitizer.bypassSecurityTrustResourceUrl( "/assets/atexo.png" );
    }
}
