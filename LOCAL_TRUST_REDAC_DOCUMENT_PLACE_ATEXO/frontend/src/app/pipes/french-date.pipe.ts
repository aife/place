import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe( { name: 'fr_date' } )
export class FrenchDatePipe extends DatePipe implements PipeTransform {

    public static DATE_FORMAT = 'dd/MM/yyyy à HH\'h\'mm';

    transform( value: Date | string | number ): any {
        return super.transform( value, FrenchDatePipe.DATE_FORMAT );
    }
}
