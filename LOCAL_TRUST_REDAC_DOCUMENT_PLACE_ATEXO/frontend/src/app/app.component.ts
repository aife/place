import { Component, HostListener, Input, isDevMode, OnInit } from '@angular/core';
import { Authentification, Contexte, Niveau, Token, Utilisateur, UUID, Version } from '@model';
import {
  Application,
  AuthentificationService,
  ContexteService,
  ModaleService,
  NotificationService,
  ReferentielService,
  SessionService,
  StockageLocalService,
  StorageKey,
  UtilisateurService,
  VersionService,
  WebsocketService
} from '@services';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environment';
import { CredentialModaleComponent } from '@components';
import { SessionManager } from '@managers';
import { NotificationHandler, RefreshHandler } from '@events';
import '@extensions';

const VERSION = 'version';
const TEXTE_GRAS = 'font-weight: bold;';
const TEXTE_ITALIQUE = 'font-style: italic;';
const TEXTE_GRIS = 'color:#aaa;';
const TEXTE_NORMAL = 'font-weight: inherit';

@Component( {
    selector: 'atexo-redacdocument',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
} )
export class AppComponent implements OnInit {

    constructor(
        public sessionManager: SessionManager,
        private activatedRoute: ActivatedRoute,
        private contexteService: ContexteService,
        private utilisateurService: UtilisateurService,
        private router: Router,
        private websocketService: WebsocketService,
        private authenticationService: AuthentificationService,
        private referentielService: ReferentielService,
        private credentialService: VersionService,
        private modaleService: ModaleService,
        private notificationService: NotificationService,
        private stockageService: StockageLocalService,
        private refreshHandler: RefreshHandler,
        private notificationHandler: NotificationHandler,
        private sessionService: SessionService
    ) {
    }

    private _contexte: string;
    private _utilisateur: string;
    private _token: string;
    private _redirection: string;
    private _niveau: string;

    @Input( 'contexte' )
    public set contexte( value: string ) {
        if ( value ) {
            this._contexte = value;
        }
    }

    @Input( 'utilisateur' )
    public set utilisateur( value: string ) {
        if ( value ) {
            this._utilisateur = value;
        }
    }

    @Input( 'token' )
    public set token( value: string ) {
        if ( value ) {
            this._token = value;
        }
    }

    @Input( 'redirection' )
    public set redirection( value: string ) {
        if ( value ) {
            this._redirection = value;
        }
    }

    @Input( 'niveau' )
    public set niveau( value: string ) {
        if ( value ) {
            this._niveau = value;
        }
    }

    private readonly DEFAULT_REDIRECTION = '/documents';

    private disableLog() {
        ['log', 'debug', 'warn', 'info', 'group', 'error', 'groupEnd', 'alert'].forEach(
            method => console[method] = () => {
            }
        );
    }

    public ngOnInit(): void {
        if ( environment.production ) {
          const debug = this.stockageService.recuperer(StorageKey.with.cle('debug').build(), false);

          if (!debug) {
            this.disableLog();
          }
        }

        console.group( `Paramètres de l'URL` );

        this.parameters.forEach(
            ( value, key, _ ) => console.log( `${key} = ${value}` )
        );

        console.groupEnd();

        console.group( 'Informations contextuelles' );
        // --------
        // Contexte
        // --------
        const contexte = this.parameters.get( 'contexte' );
        let contexteUuid;

        if ( contexte ) {
            console.info(
                `Le contexte est fourni %cdans les paramètres de l'URL\n%c${contexte}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            contexteUuid = contexte;
        } else if ( this._contexte ) {
            const json = JSON.parse( this._contexte ) as Contexte;
            console.info(
                `Le contexte est fournie %cdans la balise du web component\n%c${json.uuid}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            contexteUuid = json.uuid;
        } else {
            console.error(
                `Le contexte %cn'a pas été trouvé%c (ce paramètre est obligatoire) : ni dans l'URL, ni dans la balise du web component, ni dans le local storage. Impossible de continuer, abandon`,
                TEXTE_GRAS,
                TEXTE_NORMAL
            );

            this.notificationService.fatal( { html: `<span class="swal-error-message">Le contexte n'a pas pu être récupéré, impossible malheureusement de continuer.</span><br/><span>Merci de contacter votre support.</span>` } );

            return;
        }

        // -----------
        // Utilisateur
        // -----------
        const utilisateur = this.parameters.get( 'utilisateur' );
        let utilisateurUuid;

        if ( utilisateur ) {
            console.info( `L'utilisateur est fourni %cdans les paramètres de l'URL\n%c${utilisateur}`, TEXTE_GRAS, 'color:#aaa;' + TEXTE_ITALIQUE );

            utilisateurUuid = utilisateur;
        } else if ( this._utilisateur ) {
            const json = JSON.parse( this._utilisateur ) as Utilisateur;
            console.info( `L'utilisateur est fournie %cdans la balise du web component\n%c${json.uuid}`, TEXTE_GRAS, 'color:#aaa;' + TEXTE_ITALIQUE );

            utilisateurUuid = json.uuid;
        } else {
            console.error( `L'utilisateur %cn'a pas été trouvé%c (ce paramètre est obligatoire) : ni dans l'URL, ni dans la balise du web component, ni dans le local storage. Impossible de continuer, abandon`, TEXTE_GRAS, TEXTE_NORMAL );

            this.notificationService.fatal( { html: `<span class="swal-error-message">L'utilisateur n'a pas pu être récupéré. Impossible de continuer.</span><br/><span>Merci de contacter votre support.</span>` } );

            return;
        }

        // -----------
        // Redirection
        // -----------
        const redirectionParameter = this.parameters.get( 'redirection' );

        let redirection: string;

        if ( redirectionParameter ) {
            console.info(
                `La redirection est fourni %cdans les paramètres de l'URL\n%c${redirectionParameter}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            redirection = redirectionParameter;

        } else if ( this._redirection ) {
            console.info(
                `La redirection est fournie %cdans la balise du web component\n%c${this._redirection}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            redirection = this._redirection;
        } else if ( this.sessionManager?.session?.redirection ) {
            console.info(
                `La redirection est %crécupéré du local storage\n%c${this.sessionManager?.session?.redirection}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );
        } else {
            console.warn( `La redirection %cn'a pas été trouvé%c (ce paramètre est facultatif, valeur par default = ${this.DEFAULT_REDIRECTION})`, TEXTE_GRAS, TEXTE_NORMAL );

            redirection = this.DEFAULT_REDIRECTION;
        }

        // ---
        // Niveau d'intervention pour les gabarits
        // ---
        const niveauParameter = this.parameters.get( 'niveau' );

        let niveau: Niveau;

        if ( niveauParameter ) {
            console.info( `Le niveau pour les gabarits est fourni %cdans les paramètres de l'URL\n%c${niveauParameter}`,
                TEXTE_GRAS, TEXTE_GRIS + TEXTE_ITALIQUE );

            niveau = Niveau[niveauParameter];

        } else if ( this._niveau ) {
            console.info( `Le niveau pour les gabarits est fournie %cdans la balise du web component\n%c${this._niveau}`,
                TEXTE_GRAS, TEXTE_GRIS + TEXTE_ITALIQUE );

            niveau = Niveau[this._niveau];
        } else {
            console.info( `Le niveau pour les gabarits est %crécupéré du local storage\n%c`, TEXTE_GRAS );
        }

        // -----
        // Jeton
        // -----
        const tokenParameter: string = this.parameters.get( 'token' );

        let token: Token;

        if ( tokenParameter ) {
            console.info(
                `Le jeton d'accès est fourni %cdans les paramètres de l'URL\n%c${tokenParameter}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            token = { access_token: tokenParameter };

        } else if ( this._token ) {
            const json = JSON.parse( this._token ) as Token;
            console.info(
                `Le jeton d'accès est fournie %cdans la balise du web component\n%c${json.access_token}`,
                TEXTE_GRAS,
                TEXTE_GRIS + TEXTE_ITALIQUE
            );

            token = json;
        } else {
            if ( isDevMode() ) {
                this.authenticationService.authentifier().subscribe(
                    ( credentials: Authentification ) => {
                        token = { access_token: credentials.access_token };

                        this.rechercher( token, contexteUuid, utilisateurUuid, niveau, redirection );

                        console.groupEnd();
                    }, error => {
                        console.error( `Impossible de se connecter au serveur d'autorisation`, error );

                        this.notificationService.fatal( { html: `<span class="swal-error-message">Impossible de se connecter au serveur d'autorisation et donc de continuer.</span><br/><span>Merci de contacter votre support.</span>` } );
                    }
                );

                return;
            } else {
                console.error(
                    `Le jeton d'accès %cn'a pas été trouvé%c (ce paramètre est obligatoire) : ni dans l'URL, ni dans la balise du web component, ni dans le local storage. Impossible de continuer, abandon`,
                    TEXTE_GRAS,
                    TEXTE_NORMAL
                );

                this.notificationService.fatal( { html: `<span class="swal-error-message">Le jeton d'authentification n'a pas pu être récupéré. Impossible de continuer.</span><span>Merci de contacter votre support</span>` } );

                return;
            }
        }

        this.rechercher( token, contexteUuid, utilisateurUuid, niveau, redirection );

        console.groupEnd();
    }

    private rechercher( token: Token, contexteUuid: UUID, utilisateurUuid: UUID, niveau: Niveau, redirection: string ): void {
        let credential: Version;

        this.contexteService.recuperer( token, contexteUuid )
            .subscribe(
            contexte => {
                const cle = StorageKey.with
                    .application( Application.REDACTION )
                    .plateforme( contexte?.plateforme )
                    .cle( VERSION )
                    .build();

                this.credentialService.recuperer().subscribe(
                    version => {
                        console.info( `La version est '${version?.build?.version}' (date de release = ${version?.build?.time})` );

                        const current: Version = this.stockageService.recuperer<Version>( cle );

                        if ( current?.build?.version !== version?.build?.version ) {
                            console.warn( `Suppression de tous les caches` );
                        }

                        this.stockageService.ajouter<Version>( cle, version );

                        credential = version;

                    }, error => {
                        console.warn( `Impossible de récuperer les informations de version`, error );

                        credential = this.stockageService.recuperer<Version>( cle );
                    }, () => {
                        this.sessionManager.session = {
                            token: token,
                            contexte: contexte,
                            niveau: niveau,
                            redirection: redirection,
                            credential: credential
                        };

                        this.utilisateurService.recuperer( utilisateurUuid ).subscribe( agent => {
                                this.sessionManager.session = {
                                    ...this.sessionManager.session, agent: agent
                                };

                                this.connecter();

                                this.referentielService.charger();

                                this.rediriger();
                            }
                        );
                    } );
            }, () => {
                this.notificationService.fatal( { html: `<span class="swal-error-message">Impossible de récupérer le contexte ou celui-ci est corrompu.</span><br/><span>Merci de contacter votre support.</span>` } );
            }
        );
    }

    private connecter(): void {
        this.websocketService.connect( [
            {
                channel: `/topic/*`,
                handlers: [this.notificationHandler, this.refreshHandler]
            },
            {
                channel: `/topic/contextes/${this.sessionManager?.session?.contexte?.uuid}`,
                handlers: [this.notificationHandler, this.refreshHandler]
            },
            {
                channel: `/topic/utilisateurs/${this.sessionManager?.session?.agent?.uuid}`,
                handlers: [this.notificationHandler, this.refreshHandler]
            },
            {
                channel: `/topic/services/${this.sessionManager?.session?.agent?.service?.uuid}`,
                handlers: [this.notificationHandler, this.refreshHandler]
            },
            {
                channel: `/topic/organismes/${this.sessionManager?.session?.agent?.service?.organisme?.uuid}`,
                handlers: [this.notificationHandler, this.refreshHandler]
            },
            {
                channel: `/topic/environnements/${this.sessionManager?.session?.contexte?.environnement?.uuid}`,
                handlers: [this.notificationHandler, this.refreshHandler]
            }
        ] );
    }

    private rediriger(): void {
        if ( this.sessionManager?.session?.contexte && this.sessionManager?.session?.token ) {
            const redirect = this.sessionManager?.session?.redirection;
            if ( redirect ) {
                this.router.navigate( [redirect], { queryParamsHandling: 'preserve' } ).then(
                    _ => console.log( `Redirection vers ${redirect} OK` )
                );
            } else {
                this.router.navigate( [this.DEFAULT_REDIRECTION], { queryParamsHandling: 'preserve' } ).then(
                    _ => console.log( `Redirection vers ${this.DEFAULT_REDIRECTION} OK` )
                );
            }
        }
    }

    private get parameters(): Map<string, string> {
        const result = new Map<string, string>();
        const splits = window.location.href.split( '?' );

        if ( splits.length > 1 ) {
            splits
                .pop()
                .split( '&' )
                .forEach( ( param: string ) => {
                    const values = param.split( '=' );

                    result.set( values[0], values[1] );
                } );
        }

        return result;
    }

    public credentials(): void {
        const modale = this.modaleService.afficher( CredentialModaleComponent, { class: 'modal-middle' } );
        modale.content.fermeture.subscribe( _ => modale.hide() );
    }

    public open(): void {
        const session = this.sessionManager.session;

        const url = `${session?.contexte?.uri}?redirection=${session?.redirection}&client=${session?.contexte?.environnement?.client}&utilisateur=${session?.agent?.uuid}&contexte=${session?.contexte?.uuid}&token=${session?.token?.access_token}`;

        window.open( url );
    }

    @HostListener( 'window:beforeunload', ['$event'] )
    public beforeUnloadHandler() {
        this.sessionService.disconnect().subscribe(
            _ => console.log( 'Utilisateur déconnecté' )
        );
    }
}
