import { Mapper } from '@patterns';
import { ActionDocument, Document } from '@model';
import { Injectable } from '@angular/core';

export interface ActionIcone {
    color?: string;
    icon?: string;
}

@Injectable( {
    providedIn: 'root'
} )
export class DocumentActionIconeMapper implements Mapper<Document, ActionIcone> {
    public map( document: Document ): ActionIcone {
        let result;
        switch ( document.action ) {
            case ActionDocument.OUVERTURE:
                result = { color: 'info', icon: 'fa fa-eye' };
                break;
            case ActionDocument.ANNULATION:
                result = { color: 'saved', icon: 'fas fa-undo-alt' };
                break;
            case ActionDocument.FERMETURE_SANS_MODIFICATION :
                result = { color: 'info', icon: 'fa fa-folder-minus' };
                break;
            case ActionDocument.INCONNU :
                result = { color: 'error', icon: 'fas fa-times' };
                break;
            case ActionDocument.TELECHARGEMENT :
                result = { color: 'info', icon: 'fas fa-cloud-download-alt' };
                break;
            case ActionDocument.DUPLICATION :
                result = { color: 'info', icon: 'fa fa-folder-open' };
                break;
            case ActionDocument.NOTIFICATION :
                result = { color: 'submitted', icon: 'fas fa-envelope' };
                break;
            case ActionDocument.FERMETURE_AVEC_MODIFICATION :
            case ActionDocument.MODIFICATION_DU_NOM:
            case ActionDocument.ENREGISTREMENT:
                result = { color: 'saved', icon: 'fas fa-save' };
                break;
            case ActionDocument.CREATION :
                result = { color: 'created', icon: 'fas fa-file-upload' };
                break;
            case ActionDocument.DEMANDE_VALIDATION :
                result = { color: 'submitted', icon: 'fas fa-user-check' };
                break;
            case ActionDocument.REFUS_DEMANDE_VALIDATION :
                result = { color: 'unsubmitted', icon: 'fas fa-user-times' };
                break;
            case ActionDocument.INVALIDATION :
                result = { color: 'unsubmitted', icon: 'fas fa-user-times' };
                break;
            case ActionDocument.VALIDATION :
                result = { color: 'validated', icon: 'fas fa-check' };
                break;
        }

        return result;
    }
}
