import { Mapper } from '@patterns';
import { Document, StatutDocument } from '@model';
import { Injectable } from '@angular/core';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentStatutLibelleMapper implements Mapper<Document, String> {
    public map( document: Document ): string {
        let result;

        switch ( document.statut ) {
            case StatutDocument.BROUILLON :
                result = 'Brouillon';
                break;
            case StatutDocument.DEMANDE_VALIDATION:
                result = 'En att. de validation';
                break;
            case StatutDocument.VALIDE:
                result = 'Validé';
                break;
            default:
                break;
        }

        return result;
    }
}
