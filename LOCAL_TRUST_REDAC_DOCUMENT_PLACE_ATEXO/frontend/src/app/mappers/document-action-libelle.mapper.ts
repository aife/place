import { Mapper } from '@patterns';
import { ActionDocument, Document } from '@model';
import { Injectable } from '@angular/core';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentActionLibelleMapper implements Mapper<Document, String> {
    public map( document: Document ): string {
        let result;
        switch ( document.action ) {
            case ActionDocument.OUVERTURE:
                result = 'Ouverture pour édition du document';
                break;
            case ActionDocument.ANNULATION:
                result = 'Retour automatique a la version antérieure';
                break;
            case ActionDocument.FERMETURE_SANS_MODIFICATION :
                result = 'Fermeture sans modification du document';
                break;
            case ActionDocument.INCONNU:
                result = 'Erreur lors de la sauvegarde du document';
                break;
            case ActionDocument.TELECHARGEMENT :
                result = 'Téléchargement du document';
                break;
            case ActionDocument.ENREGISTREMENT :
                result = 'Enregistrement du document';
                break;
            case ActionDocument.DUPLICATION :
                result = 'Duplication du document';
                break;
            case ActionDocument.FERMETURE_AVEC_MODIFICATION :
                result = 'Fermeture avec enregistrement du document';
                break;
            case ActionDocument.CREATION :
                result = 'Création du document';
                break;
            case ActionDocument.MODIFICATION_DU_NOM :
                result = 'Modification du nom du document';
                break;
            case ActionDocument.DEMANDE_VALIDATION :
                result = 'Demande de validation du document';
                break;
            case ActionDocument.REFUS_DEMANDE_VALIDATION  :
                result = 'Refus de la demande de validation du document';
                break;
            case ActionDocument.INVALIDATION :
                result = 'Invalidation du document';
                break;
            case ActionDocument.VALIDATION :
                result = 'Validation du document';
                break;
            case ActionDocument.NOTIFICATION  :
                result = 'Notification de la validation du document';
                break;
        }

        return result;
    }
}
