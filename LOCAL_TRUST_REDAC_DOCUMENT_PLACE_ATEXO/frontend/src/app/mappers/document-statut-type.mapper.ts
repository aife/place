import { Mapper } from '@patterns';
import { Document, StatutDocument } from '@model';
import { Injectable } from '@angular/core';
import { ProgressbarType } from 'ngx-bootstrap/progressbar/progressbar-type.interface';

@Injectable( {
    providedIn: 'root'
} )
export class DocumentStatutTypeMapper implements Mapper<Document, String> {
    public map( document: Document ): ProgressbarType {
        let result;

        switch ( document.statut ) {
            case StatutDocument.BROUILLON :
                result = 'brouillon';
                break;
            case StatutDocument.DEMANDE_VALIDATION:
                result = 'soumission';
                break;
            case StatutDocument.VALIDE:
                result = 'validation';
                break;
            default:
                break;
        }

        return result;
    }
}
