# Module document frontend

## Technical stack
- [Angular 15.0.0](https://github.com/angular/angular-cli)
- [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction)

## Project setup instructions
Run `npm install` to set up.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4300/`. The application will automatically reload if you change any of the source files.

## Examples and others env run
You can either run :

- `npm run serve-release` to use keycloak from atexo-identite-release.local-trust.com
- `npm run serve-rec` to use keycloak from atexo-identite-rec.local-trust.com
or
- `npm run serve-local` to use local keycloak (equivalente to `ng serve`)

## Build to production
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Authors
- [Mikael Chobert](mailto:mikaelchobert@predictivelane.com)
