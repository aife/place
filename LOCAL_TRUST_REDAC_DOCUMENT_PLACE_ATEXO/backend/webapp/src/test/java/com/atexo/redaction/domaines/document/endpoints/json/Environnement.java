package com.atexo.redaction.domaines.document.endpoints.json;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@EqualsAndHashCode(of = {"uuid", "client", "plateforme"})
public class Environnement {

	private UUID uuid;

	private String client;

	private String plateforme;
}
