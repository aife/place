package com.atexo.redaction.domaines.document.endpoints;

import com.atexo.redaction.domaines.document.DocumentBackendApplication;
import com.atexo.redaction.domaines.document.controllers.restful.BaseRestController;
import com.atexo.redaction.domaines.document.endpoints.json.Connection;
import com.atexo.redaction.domaines.document.shared.tests.BaseTest;
import com.atexo.redaction.domaines.document.shared.web.Location;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.OK;

/**
 * Test du point d'acces des session {@link com.atexo.redaction.domaines.document.controllers.restful.SessionRestController}
 */
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DocumentBackendApplication.class)
@TestPropertySource(locations = {"classpath:application-test.yml", "classpath:document.properties", "classpath:application-messec.yml"})
@ActiveProfiles({"test", "messec", "keycloak"})
@Sql(scripts = "classpath:com/atexo/redaction/domaines/document/endpoints/scripts/2023_02_13_23_33_39-dump.sql")
public class SessionEndpointTest extends BaseTest {

	private static final String HEADER_ACCOUNT = "Account";

	private static final String HEADER_ACCEPT = HttpHeaders.ACCEPT;

	@Value("http://localhost:${local.server.port:8090}/document")
	private Location base;

	@Autowired
	private TestRestTemplate template;

	@Autowired
	private ObjectMapper mapper;

	@Test
	public void GIVEN_une_liste_d_utilisateurs_connectes_WHEN_lister_THEN_la_liste_correspond() {
		final var utilisateurs = Set.of("a472de6a-e6b3-11ec-9342-e4580d38f217", "a472de6a-e6b3-11ec-9342-e4580d38f218");

		// Ajout des utilisateurs
		final var connections = utilisateurs.stream().map(this::connect).collect(Collectors.toSet());

		final var url = this.base.combine("sessions?filtre={0}").format("actif").toString();

		final var response = super.template.exchange(url, GET, null, new ParameterizedTypeReference<List<Connection>>() {
		});

		assertThat(response).isNotNull();
		assertThat(HttpStatus.resolve(response.getStatusCodeValue())).isEqualTo(OK);

		final var results = response.getBody();

		assertThat(results).isNotNull();

		assertThat(results.size()).isEqualTo(connections.size());
		results.forEach(result -> assertThat(result).isIn(connections));

	}

	private Connection connect(final String utilisateur) {
		final var url = this.base.combine("/sessions?action={0}&environnement={1}").format("connect", "98b12fa3-5717-11ed-8af2-040300000000").toString();

		final var headers = new HttpHeaders();
		headers.set(HEADER_ACCOUNT, utilisateur);
		headers.set(HEADER_ACCEPT, BaseRestController.HEADER_VALUE_V2_JSON);

		final var result = super.template.exchange(url, GET, new HttpEntity<>(headers), Connection.class);

		assertThat(result).isNotNull();
		assertThat(HttpStatus.resolve(result.getStatusCodeValue())).isEqualTo(OK);

		return result.getBody();
	}
}
