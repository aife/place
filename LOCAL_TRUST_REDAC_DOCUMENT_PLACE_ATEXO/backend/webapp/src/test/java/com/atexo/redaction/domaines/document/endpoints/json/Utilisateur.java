package com.atexo.redaction.domaines.document.endpoints.json;

import lombok.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@EqualsAndHashCode(of = {"uuid", "service", "nom", "prenom", "email", "identifiantExterne", "habilitations"})
public class Utilisateur {
	private UUID uuid;

	private Service service;

	private String nom;

	private String prenom;

	private String email;

	private String identifiantExterne;

	private Set<String> habilitations = new HashSet<>();
}
