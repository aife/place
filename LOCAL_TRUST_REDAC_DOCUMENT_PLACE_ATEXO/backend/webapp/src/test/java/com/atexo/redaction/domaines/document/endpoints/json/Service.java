package com.atexo.redaction.domaines.document.endpoints.json;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@EqualsAndHashCode(of = {"uuid", "parent", "organisme", "libelle"})
public class Service {
	private UUID uuid;
	private Service parent;
	private Organisme organisme;
	private String libelle;
}
