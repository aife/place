package com.atexo.redaction.domaines.document.endpoints.json;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@EqualsAndHashCode(of = {"uuid", "libelle", "acronyme", "environnement"})
public class Organisme {
	private UUID uuid;
	private String libelle;
	private String acronyme;
	private Environnement environnement;
}
