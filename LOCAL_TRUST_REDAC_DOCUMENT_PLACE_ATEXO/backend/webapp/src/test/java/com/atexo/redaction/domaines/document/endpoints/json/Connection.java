package com.atexo.redaction.domaines.document.endpoints.json;

import lombok.*;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@EqualsAndHashCode(of = {"uuid", "creation", "utilisateur"})
public class Connection {
	private UUID uuid;
	private Instant creation;
	private Utilisateur utilisateur;


}
