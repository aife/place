package com.atexo.redaction.domaines.document.configuration;


import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.springframework.MariaDB4jSpringService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("test")
class EmbeddedMariaDBConfiguration {

	@Bean
	public MariaDB4jSpringService service() {
		return new MariaDB4jSpringService();
	}

	@Bean
	public DataSource dataSource(
			final MariaDB4jSpringService service,
			@Value("${database.nom:document}") final String databaseName,
			@Value("${spring.datasource.username}") final String username,
			@Value("${spring.datasource.password}") final String password,
			@Value("${spring.datasource.driver-class-name}") final String classname) throws ManagedProcessException {
		final var database = service.getDB();

		database.createDB(databaseName);

		final var config = service.getConfiguration();

		return DataSourceBuilder
				.create()
				.username(username)
				.password(password)
				.url(config.getURL(databaseName))
				.driverClassName(classname)
				.build();
	}
}
