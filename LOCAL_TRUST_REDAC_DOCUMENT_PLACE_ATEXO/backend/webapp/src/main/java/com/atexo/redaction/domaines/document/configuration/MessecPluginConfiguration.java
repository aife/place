package com.atexo.redaction.domaines.document.configuration;

import com.atexo.redaction.domaines.document.services.configuration.messec.MessecConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MessecConfiguration.class)
@Profile("messec")
public class MessecPluginConfiguration {
}
