package com.atexo.redaction.domaines.document.configuration.web;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class MappingConfiguration {

	@Bean
	public Module javaTimeModule() {
		log.debug("Chargement du module javatime pour jackson");

		return new JavaTimeModule();
	}

	@Bean
	public Module hibernate5Module() {
		log.debug("Chargement du module hibernate5 pour jackson");

		final var module = new Hibernate5Module();
		module.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, false);
		return module;
	}
}
