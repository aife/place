package com.atexo.redaction.domaines.document.configuration.security;

import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@KeycloakEnabled
public class EnhancedKeycloakAuthenticationProvider extends KeycloakAuthenticationProvider {

	@Override
	public Authentication authenticate(final Authentication authentication) {
		var authenticationToken = (KeycloakAuthenticationToken) authentication;
		return new Token(authenticationToken.getAccount(), authenticationToken.isInteractive(), authenticationToken.getAuthorities());
	}

	@Override
	public boolean supports(final Class<?> aClass) {
		return KeycloakAuthenticationToken.class.isAssignableFrom(aClass);
	}
}
