package com.atexo.redaction.domaines.document.configuration;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class LiquibaseConfiguration {

	@Value("${liquibase.changelog:classpath:db/changelogs/db.changelog-master.xml}")
	public String liquibaseChangeLog;

	@Bean
	public LiquibaseProperties liquibaseProperties() {
		var properties = new LiquibaseProperties();
		properties.setChangeLog(liquibaseChangeLog);
		return properties;
	}

	@Bean
	public SpringLiquibase liquibase(final DataSource dataSource, final LiquibaseProperties liquibaseProperties) {
		var liquibase = new SpringLiquibase();
		liquibase.setChangeLog(liquibaseProperties.getChangeLog());
		liquibase.setContexts(liquibaseProperties.getContexts());
		liquibase.setDataSource(dataSource);
		liquibase.setClearCheckSums(true);
		liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
		liquibase.setDropFirst(liquibaseProperties.isDropFirst());
		liquibase.setShouldRun(true);
		liquibase.setLabels(liquibaseProperties.getLabels());
		liquibase.setChangeLogParameters(liquibaseProperties.getParameters());
		return liquibase;
	}
}
