package com.atexo.redaction.domaines.document.configuration.web;

import com.atexo.redaction.domaines.document.controllers.restful.converters.CustomHandlerMethodArgumentResolver;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
import java.util.Set;

@Configuration
@RequiredArgsConstructor
public class CustomWebMvcConfigurer implements WebMvcConfigurer {

	@NonNull
	private Set<CustomHandlerMethodArgumentResolver> customHandlerMethodArgumentResolvers;

	@Override
	public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.addAll(this.customHandlerMethodArgumentResolvers);
	}
}
