package com.atexo.redaction.domaines.document.interceptors;

import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.lang.reflect.Parameter;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
@Slf4j
@Conditional(OnLogDebugEnable.class)
public class ExecutionTimeAdvice {

	@Around("@annotation(com.atexo.redaction.domaines.document.shared.annotations.LogTime)")
	public static Object executionTime(final ProceedingJoinPoint point) throws Throwable {
		final var signature = (MethodSignature) point.getSignature();

		final var method = signature.getMethod();
		final var annotation = method.getAnnotation(LogTime.class);

		var taskname = annotation.value();

		if (null == taskname) {
			final var classname = signature.getDeclaringType().getCanonicalName();
			final var parameters = Arrays.stream(method.getParameters())
					.map(Parameter::getType)
					.map(Class::getSimpleName)
					.collect(Collectors.joining(", "));

			taskname = MessageFormat.format("{0}.{1}({2})", classname, method.getName(), parameters);
		}

		final var watch = new StopWatch(Strings.nullToEmpty(taskname));

		watch.start(annotation.value());

		final var object = point.proceed();

		watch.stop();

		log.debug("⏱️ {} en {}ms", taskname, watch.getTotalTimeMillis());

		return object;
	}
}
