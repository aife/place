package com.atexo.redaction.domaines.document.configuration.security;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@Configuration
@EnableWebSecurity
@KeycloakConfiguration
@KeycloakEnabled
@RequiredArgsConstructor
public class EnhancedKeycloakWebSecurityConfigurerAdapter extends KeycloakWebSecurityConfigurerAdapter {

	@NonNull
	private EnhancedKeycloakAuthenticationProvider enhancedKeycloakAuthenticationProvider;

	private static final String[] AUTH_WHITELIST = {
			// -- swagger ui
			"/(webjars|swagger-.*|v3)/.*",
			// actuator
			"/actuator/.*",
			// websockets
			"/ws/.*",
			// push
			"/(messages|evenements)/.*",
			"/(messages|evenements)",
			".*action=(telechargement|visualisation).*",
			"/monitoring.*",
			".*filtre=actif.*"
	};

	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder auth) {
		this.enhancedKeycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
		auth.authenticationProvider(this.enhancedKeycloakAuthenticationProvider);
	}

	@Override
	protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
		return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		super.configure(http);
		http.headers().frameOptions().sameOrigin().disable().cors().disable().csrf().disable().authorizeRequests().and()
				.exceptionHandling().authenticationEntryPoint(this.authenticationEntryPoint()).and().authorizeRequests()
				.regexMatchers(AUTH_WHITELIST)
				.permitAll()
				.anyRequest()
				.authenticated();
	}
}
