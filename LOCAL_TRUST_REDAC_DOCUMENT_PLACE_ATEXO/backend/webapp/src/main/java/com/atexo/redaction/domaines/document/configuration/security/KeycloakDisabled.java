package com.atexo.redaction.domaines.document.configuration.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@ConditionalOnProperty(value = "keycloak.enabled", havingValue = "false", matchIfMissing = true)
public @interface KeycloakDisabled {
}
