package com.atexo.redaction.domaines.document;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
@PropertySource(value = "classpath:keycloak.properties", ignoreResourceNotFound = true)
@PropertySource(value = {"classpath:document.properties", "classpath:application-keycloak.yml", "classpath:application-messec.yml"})
@EnableCaching
@OpenAPIDefinition(
		info = @Info(
				title = "Atexo redacdocument",
				description = "Outil de rédaction des pièces de marchés",
				version = "1.0",
				contact = @Contact(
						name = "mikael chobert",
						email = "mikael.chobert@atexo.com"
				),
				license = @License(name = "Tous droits réservés")
		)
)
public class DocumentBackendApplication {

	public static void main(final String... args) {
		SpringApplication.run(DocumentBackendApplication.class, args);
	}
}
