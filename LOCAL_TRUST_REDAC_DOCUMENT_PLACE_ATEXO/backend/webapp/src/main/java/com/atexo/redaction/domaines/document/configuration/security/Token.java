package com.atexo.redaction.domaines.document.configuration.security;

import lombok.Getter;
import lombok.Setter;
import org.keycloak.adapters.spi.KeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
public class Token extends KeycloakAuthenticationToken {

	public Token(final KeycloakAccount account, final boolean interactive) {
		super(account, interactive);
	}

	public Token(final KeycloakAccount account, final boolean interactive, final Collection<? extends GrantedAuthority> authorities) {
		super(account, interactive, authorities);
	}

	public String getIdentifiant() {
		return getAccount().getPrincipal().getName();
	}

}
