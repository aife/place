package com.atexo.redaction.domaines.document.liquibase;

import com.atexo.redaction.domaines.document.model.exceptions.api.configuration.ConfigurationInvalideException;
import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.envers.RevisionType;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class GabaritInitializerTask implements CustomTaskChange {

	private static final String TOTAL = "total";

	private static final String COUNT_REVINFO =
			"SELECT COUNT(*)  AS '" + TOTAL + "'  FROM revinfo;";
	private static final String CREATION_REVINFO =
			"INSERT INTO revinfo (rev, revtstmp) VALUES (1, 1627981636032);";
	private static final String INSERT_SURCHARGE_MODELE_AUD =
			"INSERT surcharge_modele_aud( " +
			"   id, " +
			"   rev, " +
			"   revtype, " +
			"   chemin, " +
			"   date_creation, " +
			"   date_modification, " +
			"   nom_fichier, " +
			"   uuid, " +
			"   id_utilisateur_creation, " +
			"   id_environnement, " +
			"   id_modele, " +
			"   id_utilisateur_modification, " +
			"   id_niveau, " +
			"   id_organisme, " +
			"   id_utilisateur, " +
			"   id_service, " +
			"   date_actualisation, " +
			"   date_insertion, " +
			"   commentaire " +
			") " +
			"SELECT " +
			"    surcharge.id, " +
			"    {0}, " +
			"    {1}, " +
			"    surcharge.chemin, " +
			"    surcharge.date_creation, " +
			"    surcharge.date_modification, " +
			"    surcharge.nom_fichier, " +
			"    surcharge.uuid, " +
			"    surcharge.id_utilisateur_creation, " +
			"    surcharge.id_environnement, " +
			"    surcharge.id_modele, " +
			"    surcharge.id_utilisateur_modification, " +
			"    surcharge.id_niveau, " +
			"    surcharge.id_organisme, " +
			"    surcharge.id_utilisateur, " +
			"    surcharge.id_service, " +
			"    surcharge.date_actualisation, " +
			"    surcharge.date_insertion, " +
			"    surcharge.commentaire " +
			"FROM surcharge_modele surcharge " +
			"LEFT JOIN surcharge_modele_aud audit ON surcharge.uuid = audit.uuid " +
			"WHERE audit.id IS NULL;";

	private static final String INSERT_SURCHAGE_MODELE =
			"INSERT INTO surcharge_modele( " +
			"   id_modele, " +
			"   id_niveau, " +
			"   chemin, " +
			"   uuid, " +
			"   nom_fichier, " +
			"   date_creation, " +
			"   date_modification, " +
			"   id_environnement " +
			") VALUES ( " +
			"   ( SELECT m.id FROM modele m JOIN referentiel r ON r.id=m.id_type WHERE r.code =''{0}'' LIMIT 1 )," +
			"   ( SELECT r.id FROM referentiel r WHERE r.code =''{1}'' LIMIT 1 )," +
			"   ''{2}'', " +
			"   ''{3}'', " +
			"   ''{4}'', " +
			"   NOW(), " +
			"   NOW(), " +
			"   ( SELECT e.id FROM environnement e WHERE ''{5}'' REGEXP e.client )" +
			");";

	private static final String COUNT_SURCHARGE_MODELE =
			"SELECT COUNT(*) AS '" + TOTAL + "' " +
					"FROM surcharge_modele sm " +
					"   JOIN modele m ON sm.id_modele = m.id " +
					"   JOIN environnement env ON sm.id_environnement = env.id " +
					"   JOIN referentiel type ON m.id_type = type.id " +
					"   JOIN referentiel niveau ON sm.id_niveau = niveau.id " +
					"WHERE ''{0}'' REGEXP env.client AND niveau.code=''{1}'' AND type.code=''{2}''";

	private static final String GROUP_NAME = "repertoire";
	private static final Pattern PATTERN = Pattern.compile("classpath:(?<" + GROUP_NAME + ">.*)");

	@Setter
	private String destination;

	@Setter
	private String client;

	private Set<File> gabarits;

	@Override
	public void setUp() {
		if (null == destination || destination.isEmpty()) {
			throw new ConfigurationInvalideException("Le répertoire de destination n''est pas renseigné");
		}

		var matcher = PATTERN.matcher(destination);

		var repertoire = matcher.matches() ? new ClassPathResource(matcher.group(GROUP_NAME)) : new FileSystemResource(destination);

		log.debug("Utilisation des gabarits situés dans le répertoire '{}'", destination);

		if (!repertoire.exists()) {
			throw new ConfigurationInvalideException("Le répertoire ''{0}'' n''existe pas", destination);
		}

		try {
			this.gabarits = Arrays.stream(Objects.requireNonNull(repertoire.getFile().listFiles()))
					.filter(File::isFile)
					.filter(f -> f.getName().contains("."))
					.collect(Collectors.toSet());
		} catch (IOException e) {
			log.error("Impossible d'initialiser les gabarits", e);
		}
	}

	@Override
	public void execute(Database database) {
		var connection = (JdbcConnection) database.getConnection();

		if (null != gabarits) {
			for (var gabarit : gabarits) {
				log.debug("Traitement du modèle = {}", gabarit.getName());

				var code = FilenameUtils.removeExtension(gabarit.getName());

				if (existe(connection, code, client)) {
					log.debug("Une surcharge plateforme existe déjà pour le type de document '{}' et le client '{}'", code, client);

					continue;
				} else {
					log.info("La surcharge plateforme n´existe pas encore (ou à été supprimé) pour le type de document '{}' et le client '{}'. Elle va donc être crée.", code, client);
				}

				var uuid = UUID.randomUUID();

				try (var statement = connection.createStatement()) {
					var sql = MessageFormat.format(INSERT_SURCHAGE_MODELE, code, Niveau.PLATEFORME, gabarit.getAbsolutePath(), uuid, gabarit.getName(), client);

					log.trace(sql);

					statement.execute(sql);
				}catch (DatabaseException | SQLException e) {
					log.error("Erreur lors de la création des surcharges", e);
				}
			}
		}

		// Création de la révinfo
		log.debug("Création d'une entrée dans revinfo");

		boolean noRevinfo = false;

		try (var statement = connection.createStatement()) {
			var result = statement.executeQuery(COUNT_REVINFO);

			if (result.next()) {
				noRevinfo = result.getInt(TOTAL) == 0;
			}
		} catch (DatabaseException | SQLException e) {
			log.error("Erreur lors de la vérification des surcharges d'audit", e);
		}

		if (noRevinfo) {
			try (var statement = connection.createStatement()) {
				var sql = CREATION_REVINFO;

				log.trace(sql);

				statement.execute(sql);
			} catch (DatabaseException | SQLException e) {
				log.error("Erreur lors de la création de la revinfo", e);
			}
		}

		// Rattrapage des audits
		log.debug("Création des audits pour toutes les surcharges");

		try (var statement = connection.createStatement()) {
			var sql = MessageFormat.format(INSERT_SURCHARGE_MODELE_AUD, 1, RevisionType.ADD.getRepresentation());

			log.trace(sql);

			statement.execute(sql);
		} catch (DatabaseException | SQLException e) {
			log.error("Erreur lors de la création des surcharges d'audit", e);
		}
	}


	private boolean existe(JdbcConnection connection, String code, String client) {
		try (var statement = connection.createStatement()) {
			var result = statement.executeQuery(MessageFormat.format(COUNT_SURCHARGE_MODELE, client, Niveau.PLATEFORME, code));

			if (result.next()) {
				return result.getInt(TOTAL) > 0;
			}
		} catch (DatabaseException | SQLException e) {
			log.error("Erreur lors de la vérification des surcharges d'audit", e);
		}

		return true;
	}

	@Override
	public String getConfirmationMessage() {
		return null;
	}

	@Override
	public void setFileOpener(ResourceAccessor resourceAccessor) {
	}

	@Override
	public ValidationErrors validate(Database database) {
		return null;
	}
}
