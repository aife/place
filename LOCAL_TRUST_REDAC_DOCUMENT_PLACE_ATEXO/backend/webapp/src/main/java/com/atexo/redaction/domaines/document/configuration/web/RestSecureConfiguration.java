package com.atexo.redaction.domaines.document.configuration.web;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Profile("!unsecure")
@Configuration
public class RestSecureConfiguration {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder()
				.setConnectTimeout(Duration.ofMinutes(30))
				.setReadTimeout(Duration.ofMinutes(30))
				.requestFactory(this::creerHttpClientSansSecurite)
				.build();
	}

	protected ClientHttpRequestFactory creerHttpClientSansSecurite() {
		// disable all security
		return new HttpComponentsClientHttpRequestFactory(HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build());
	}
}
