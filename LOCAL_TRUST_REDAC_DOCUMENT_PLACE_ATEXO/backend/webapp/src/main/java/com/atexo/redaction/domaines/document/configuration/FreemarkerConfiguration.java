package com.atexo.redaction.domaines.document.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

@Configuration
public class FreemarkerConfiguration {

	@Value("${freemarker.templates.directory:classpath:/templates}")
	public String freeMarkerTemplatesDirectory;

	@Bean
	public FreeMarkerConfigurer freemarkerConfig() {
		var configurer = new FreeMarkerConfigurer();
		configurer.setTemplateLoaderPath(freeMarkerTemplatesDirectory);
		return configurer;
	}
}
