CREATE TABLE IF NOT EXISTS `revinfo`
(
    `rev`      int(11) NOT NULL AUTO_INCREMENT,
    `revtstmp` bigint(20) DEFAULT NULL,
    PRIMARY KEY (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `databasechangelog`
(
    `ID`            varchar(255) NOT NULL,
    `AUTHOR`        varchar(255) NOT NULL,
    `FILENAME`      varchar(255) NOT NULL,
    `DATEEXECUTED`  datetime     NOT NULL,
    `ORDEREXECUTED` int(11)      NOT NULL,
    `EXECTYPE`      varchar(10)  NOT NULL,
    `MD5SUM`        varchar(35)  DEFAULT NULL,
    `DESCRIPTION`   varchar(255) DEFAULT NULL,
    `COMMENTS`      varchar(255) DEFAULT NULL,
    `TAG`           varchar(255) DEFAULT NULL,
    `LIQUIBASE`     varchar(20)  DEFAULT NULL,
    `CONTEXTS`      varchar(255) DEFAULT NULL,
    `LABELS`        varchar(255) DEFAULT NULL,
    `DEPLOYMENT_ID` varchar(10)  DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `databasechangeloglock`
(
    `ID`          int(11) NOT NULL,
    `LOCKED`      bit(1)  NOT NULL,
    `LOCKGRANTED` datetime     DEFAULT NULL,
    `LOCKEDBY`    varchar(255) DEFAULT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `environnement`
(
    `id`         bigint(20)   NOT NULL AUTO_INCREMENT,
    `client`     varchar(255) NOT NULL,
    `plateforme` varchar(255) NOT NULL,
    `uuid`       char(36)     NOT NULL DEFAULT uuid(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_environnement_uuid` (`uuid`),
    UNIQUE KEY `uk_environnement_plateforme_client` (`plateforme`, `client`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `environnement_aud`
(
    `id`         bigint       NOT NULL,
    `rev`        int          NOT NULL,
    `revtype`    tinyint      NULL,
    `client`     varchar(20)  NULL,
    `plateforme` varchar(20)  NULL,
    `uuid`       varchar(255) NULL,
    PRIMARY KEY (id, rev),
    CONSTRAINT FK_environnement_aud_revinfo FOREIGN KEY (rev) REFERENCES revinfo (rev)
);

CREATE TABLE IF NOT EXISTS `contexte`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `payload`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `plateforme`       varchar(255)                                       DEFAULT NULL,
    `reference`        varchar(255)                                       DEFAULT NULL,
    `uuid`             varchar(255)                                       DEFAULT NULL,
    `id_environnement` bigint(20)                                         DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_contexte_uuid` (`uuid`),
    UNIQUE KEY `uk_contexte_id_environnement_reference` (`id_environnement`, `reference`),
    CONSTRAINT `fk_contexte_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `contexte_aud`
(
    `id`               bigint(20) NOT NULL,
    `rev`              int(11)    NOT NULL,
    `revtype`          tinyint(4)                                         DEFAULT NULL,
    `payload`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `plateforme`       varchar(255)                                       DEFAULT NULL,
    `reference`        varchar(255)                                       DEFAULT NULL,
    `uuid`             varchar(255)                                       DEFAULT NULL,
    `id_environnement` bigint(20)                                         DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_contexte_aud_revinfo` (`rev`),
    CONSTRAINT `fk_contexte_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `referentiel`
(
    `type_referentiel` varchar(31) NOT NULL,
    `id`               bigint(20)  NOT NULL AUTO_INCREMENT,
    `code`             varchar(255) DEFAULT NULL,
    `libelle`          longtext     DEFAULT NULL,
    `ordre`            int(11)      DEFAULT NULL,
    `id_parent`        bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_referentiel_parent` (`id_parent`),
    CONSTRAINT `fk_referentiel_parent` FOREIGN KEY (`id_parent`) REFERENCES `referentiel` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `referentiel_aud`
(
    `id`               bigint(20)  NOT NULL,
    `rev`              int(11)     NOT NULL,
    `type_referentiel` varchar(31) NOT NULL,
    `revtype`          tinyint(4)   DEFAULT NULL,
    `code`             varchar(255) DEFAULT NULL,
    `libelle`          longtext     DEFAULT NULL,
    `ordre`            int(11)      DEFAULT NULL,
    `id_parent`        bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_referentiel_aud_revinfo` (`rev`),
    CONSTRAINT `fk_referentiel_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `referentiel_environnement`
(
    `id_referentiel`   bigint(20) NOT NULL,
    `id_environnement` bigint(20) NOT NULL,
    PRIMARY KEY (`id_referentiel`, `id_environnement`),
    KEY `fk_referentiel_environnement_environnement` (`id_environnement`),
    CONSTRAINT `fk_referentiel_environnement_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`),
    CONSTRAINT `fk_referentiel_environnement_referentiel` FOREIGN KEY (`id_referentiel`) REFERENCES `referentiel` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `referentiel_environnement_aud`
(
    `rev`              int(11)    NOT NULL,
    `id_referentiel`   bigint(20) NOT NULL,
    `id_environnement` bigint(20) NOT NULL,
    `revtype`          tinyint(4) DEFAULT NULL,
    PRIMARY KEY (`rev`, `id_referentiel`, `id_environnement`),
    CONSTRAINT `fk_referentiel_environnement_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `modele`
(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `actif`         bit(1)     NOT NULL,
    `administrable` bit(1)              DEFAULT NULL,
    `libelle`       varchar(255)        DEFAULT NULL,
    `nom_fichier`   varchar(255)        DEFAULT NULL,
    `ordre`         int(11)             DEFAULT NULL,
    `uuid`          char(36)   NOT NULL DEFAULT uuid(),
    `id_type`       bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_modele_uuid` (`uuid`),
    KEY `fk_modele_type` (`id_type`),
    CONSTRAINT `fk_modele_type` FOREIGN KEY (`id_type`) REFERENCES `referentiel` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `modele_aud`
(
    `id`            bigint(20) NOT NULL,
    `rev`           int(11)    NOT NULL,
    `revtype`       tinyint(4)   DEFAULT NULL,
    `actif`         bit(1)       DEFAULT NULL,
    `administrable` bit(1)       DEFAULT NULL,
    `libelle`       varchar(255) DEFAULT NULL,
    `nom_fichier`   varchar(255) DEFAULT NULL,
    `ordre`         int(11)      DEFAULT NULL,
    `uuid`          varchar(255) DEFAULT NULL,
    `id_type`       bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_modele_aud_revinfo` (`rev`),
    CONSTRAINT `fk_modele_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `organisme`
(
    `id`                 bigint(20)  NOT NULL AUTO_INCREMENT,
    `id_environnement`   bigint(20)                                         DEFAULT NULL,
    `uuid`               char(36)                                           DEFAULT NULL,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `date_insertion`     datetime(6) NOT NULL                               DEFAULT current_timestamp(6),
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `externe_id`         bigint(20) GENERATED ALWAYS AS (json_value(`payload`, '$.id')) VIRTUAL,
    `libelle`            longtext GENERATED ALWAYS AS (json_value(`payload`, '$.libelleLong')) VIRTUAL,
    `acronyme`           varchar(255) GENERATED ALWAYS AS (json_value(`payload`, '$.acronyme')) VIRTUAL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_organisme_uuid` (`uuid`),
    UNIQUE KEY `uk_organisme_externe_id_id_environnement` (`externe_id`, `id_environnement`),
    KEY `fk_organisme_environnement` (`id_environnement`),
    CONSTRAINT `fk_organisme_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `organisme_aud`
(
    `id`                 bigint(20) NOT NULL,
    `rev`                int(11)    NOT NULL,
    `revtype`            tinyint(4)                                         DEFAULT NULL,
    `acronyme`           varchar(255)                                       DEFAULT NULL,
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `date_insertion`     datetime(6)                                        DEFAULT NULL,
    `externe_id`         bigint(20)                                         DEFAULT NULL,
    `libelle`            longtext                                           DEFAULT NULL,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `uuid`               varchar(255)                                       DEFAULT NULL,
    `id_environnement`   bigint(20)                                         DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_organisme_aud_revinfo` (`rev`),
    CONSTRAINT `fk_organisme_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `service`
(
    `id`                 bigint(20)  NOT NULL AUTO_INCREMENT,
    `id_organisme`       bigint(20)                                         DEFAULT NULL,
    `id_parent`          bigint(20)                                         DEFAULT NULL,
    `uuid`               char(36)                                           DEFAULT NULL,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `date_insertion`     datetime(6) NOT NULL                               DEFAULT current_timestamp(6),
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `externe_id`         bigint(20) GENERATED ALWAYS AS (json_value(`payload`, '$.id')) VIRTUAL,
    `libelle`            longtext GENERATED ALWAYS AS (json_value(`payload`, '$.libelleLong')) VIRTUAL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_service_uuid` (`uuid`),
    UNIQUE KEY `uk_service_externe_id_id_organisme` (`externe_id`, `id_organisme`),
    KEY `fk_service_organisme` (`id_organisme`),
    KEY `fk_service_parent` (`id_parent`),
    CONSTRAINT `fk_service_organisme` FOREIGN KEY (`id_organisme`) REFERENCES `organisme` (`id`),
    CONSTRAINT `fk_service_parent` FOREIGN KEY (`id_parent`) REFERENCES `service` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `service_aud`
(
    `id`                 bigint(20) NOT NULL,
    `rev`                int(11)    NOT NULL,
    `revtype`            tinyint(4)                                         DEFAULT NULL,
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `date_insertion`     datetime(6)                                        DEFAULT NULL,
    `externe_id`         bigint(20)                                         DEFAULT NULL,
    `libelle`            longtext                                           DEFAULT NULL,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `uuid`               varchar(255)                                       DEFAULT NULL,
    `id_organisme`       bigint(20)                                         DEFAULT NULL,
    `id_parent`          bigint(20)                                         DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_service_aud_revision` (`rev`),
    CONSTRAINT `fk_service_aud_revision` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `utilisateur`
(
    `id`                 bigint(20)  NOT NULL AUTO_INCREMENT,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `uuid`               char(36)    NOT NULL,
    `id_service`         bigint(20)                                         DEFAULT NULL,
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `date_insertion`     datetime(6) NOT NULL                               DEFAULT current_timestamp(6),
    `externe_id`         varchar(255) GENERATED ALWAYS AS (json_value(`payload`, '$.id')) VIRTUAL,
    `email`              varchar(255) GENERATED ALWAYS AS (json_value(`payload`, '$.courriel')) VIRTUAL,
    `nom`                varchar(255) GENERATED ALWAYS AS (json_value(`payload`, '$.nom')) VIRTUAL,
    `prenom`             varchar(255) GENERATED ALWAYS AS (json_value(`payload`, '$.prenom')) VIRTUAL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_utilisateur_uuid` (`uuid`),
    UNIQUE KEY `uk_utilisateur_externe_id_id_service` (`externe_id`, `id_service`),
    KEY `fk_utilisateur_service` (`id_service`),
    CONSTRAINT `fk_utilisateur_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `utilisateur_aud`
(
    `id`                 bigint(20) NOT NULL,
    `rev`                int(11)    NOT NULL,
    `revtype`            tinyint(4)                                         DEFAULT NULL,
    `date_actualisation` datetime(6)                                        DEFAULT NULL,
    `date_insertion`     datetime(6)                                        DEFAULT NULL,
    `email`              varchar(255)                                       DEFAULT NULL,
    `externe_id`         varchar(255)                                       DEFAULT NULL,
    `nom`                varchar(255)                                       DEFAULT NULL,
    `payload`            longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    `prenom`             varchar(255)                                       DEFAULT NULL,
    `uuid`               varchar(255)                                       DEFAULT NULL,
    `id_service`         bigint(20)                                         DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_utilisateur_aud_revinfo` (`rev`),
    CONSTRAINT `fk_utilisateur_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `document_editable`
(
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT,
    `actif`                       bit(1)     NOT NULL,
    `action`                      varchar(255) DEFAULT NULL,
    `id_canevas`                  bigint(20)   DEFAULT NULL,
    `reference_canevas`           varchar(255) DEFAULT NULL,
    `chemin`                      varchar(255) DEFAULT NULL,
    `commentaire`                 longtext     DEFAULT NULL,
    `date_creation`               datetime(6)  DEFAULT NULL,
    `date_modification`           datetime(6)  DEFAULT NULL,
    `extension`                   varchar(6)   DEFAULT NULL,
    `externe_id`                  bigint(20)   DEFAULT NULL,
    `jeton`                       longtext     DEFAULT NULL,
    `lot`                         bigint(20)   DEFAULT NULL,
    `nom`                         longtext     DEFAULT NULL,
    `ouvert`                      bit(1)       DEFAULT NULL,
    `progression`                 double       DEFAULT NULL,
    `statut`                      varchar(255) DEFAULT NULL,
    `uuid`                        varchar(255) DEFAULT NULL,
    `xml`                         longtext     DEFAULT NULL,
    `id_contexte`                 bigint(20)   DEFAULT NULL,
    `id_utilisateur_creation`     bigint(20)   DEFAULT NULL,
    `id_modele`                   bigint(20)   DEFAULT NULL,
    `id_utilisateur_modification` bigint(20)   DEFAULT NULL,
    `date_actualisation`          datetime(6)  DEFAULT NULL,
    `date_insertion`              datetime(6)  DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_document_uuid` (`uuid`),
    KEY `fk_document_contexte` (`id_contexte`),
    KEY `fk_document_utilisateur_creation` (`id_utilisateur_creation`),
    KEY `fk_document_modele` (`id_modele`),
    KEY `fk_document_utilisateur_modification` (`id_utilisateur_modification`),
    CONSTRAINT `fk_document_contexte` FOREIGN KEY (`id_contexte`) REFERENCES `contexte` (`id`),
    CONSTRAINT `fk_document_modele` FOREIGN KEY (`id_modele`) REFERENCES `modele` (`id`),
    CONSTRAINT `fk_document_utilisateur_creation` FOREIGN KEY (`id_utilisateur_creation`) REFERENCES `utilisateur` (`id`),
    CONSTRAINT `fk_document_utilisateur_modification` FOREIGN KEY (`id_utilisateur_modification`) REFERENCES `utilisateur` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `document_editable_aud`
(
    `id`                          bigint(20) NOT NULL,
    `rev`                         int(11)    NOT NULL,
    `revtype`                     tinyint(4)   DEFAULT NULL,
    `actif`                       bit(1)       DEFAULT NULL,
    `action`                      varchar(255) DEFAULT NULL,
    `id_canevas`                  bigint(20)   DEFAULT NULL,
    `reference_canevas`           varchar(255) DEFAULT NULL,
    `chemin`                      varchar(255) DEFAULT NULL,
    `commentaire`                 longtext     DEFAULT NULL,
    `date_creation`               datetime(6)  DEFAULT NULL,
    `date_modification`           datetime(6)  DEFAULT NULL,
    `extension`                   varchar(6)   DEFAULT NULL,
    `externe_id`                  bigint(20)   DEFAULT NULL,
    `jeton`                       longtext     DEFAULT NULL,
    `lot`                         bigint(20)   DEFAULT NULL,
    `nom`                         longtext     DEFAULT NULL,
    `ouvert`                      bit(1)       DEFAULT NULL,
    `progression`                 double       DEFAULT NULL,
    `statut`                      varchar(255) DEFAULT NULL,
    `uuid`                        varchar(255) DEFAULT NULL,
    `xml`                         longtext     DEFAULT NULL,
    `id_contexte`                 bigint(20)   DEFAULT NULL,
    `id_utilisateur_creation`     bigint(20)   DEFAULT NULL,
    `id_modele`                   bigint(20)   DEFAULT NULL,
    `id_utilisateur_modification` bigint(20)   DEFAULT NULL,
    `date_actualisation`          datetime(6)  DEFAULT NULL,
    `date_insertion`              datetime(6)  DEFAULT NULL,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_document_editable_aud_revinfo` (`rev`),
    CONSTRAINT `fk_document_editable_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `destinataire`
(
    `id`               bigint(20)   NOT NULL AUTO_INCREMENT,
    `email`            varchar(255) NOT NULL,
    `externe_id`       varchar(255) DEFAULT NULL,
    `nom`              varchar(255) NOT NULL,
    `prenom`           varchar(255) NOT NULL,
    `id_environnement` bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_destinataire_externe_id_id_environnement` (`externe_id`, `id_environnement`),
    KEY `fk_destinataire_environnement` (`id_environnement`),
    CONSTRAINT `fk_destinataire_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `notification`
(
    `id`                      bigint(20)  NOT NULL AUTO_INCREMENT,
    `numero`                  varchar(255) DEFAULT NULL,
    `reference`               varchar(255) DEFAULT NULL,
    `date_creation`           datetime(6) NOT NULL,
    `uri`                     varchar(255) DEFAULT NULL,
    `id_utilisateur_creation` bigint(20)   DEFAULT NULL,
    `id_document`             bigint(20)  NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_notification_utilisateur_creation` (`id_utilisateur_creation`),
    KEY `fk_notification_document` (`id_document`),
    CONSTRAINT `fk_notification_document` FOREIGN KEY (`id_document`) REFERENCES `document_editable` (`id`),
    CONSTRAINT `fk_notification_utilisateur_creation` FOREIGN KEY (`id_utilisateur_creation`) REFERENCES `utilisateur` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `notification_aud`
(
    `id`                      bigint  NOT NULL,
    `rev`                     int     NOT NULL,
    `revtype`                 tinyint NULL,
    `id_utilisateur_creation` bigint  NULL,
    PRIMARY KEY (id, rev),
    CONSTRAINT `fk_notification_aud_revinfo` FOREIGN KEY (rev) REFERENCES revinfo (rev)
);

CREATE TABLE IF NOT EXISTS `destinataire_notification`
(
    `id`              bigint(20) NOT NULL AUTO_INCREMENT,
    `defaut_uri`      bit(1)     NOT NULL,
    `uri`             varchar(255) DEFAULT NULL,
    `id_destinataire` bigint(20) NOT NULL,
    `id_notification` bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_destinataire_notification_notification` (`id_notification`),
    KEY `fk_destinataire_notification_destinataire` (`id_destinataire`),
    CONSTRAINT `fk_destinataire_notification_destinataire` FOREIGN KEY (`id_destinataire`) REFERENCES `destinataire` (`id`),
    CONSTRAINT `fk_destinataire_notification_notification` FOREIGN KEY (`id_notification`) REFERENCES `notification` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `evenement`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `commentaire`         longtext                                           DEFAULT NULL,
    `date_enregistrement` datetime(6)                                        DEFAULT NULL,
    `etape`               varchar(255)                                       DEFAULT NULL,
    `payload`             longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`payload`)),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `parametre`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `cle`              varchar(255) DEFAULT NULL,
    `valeur`           longtext     DEFAULT NULL,
    `id_environnement` bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_parametre_environnement` (`id_environnement`),
    CONSTRAINT `fk_parametre_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `revision`
(
    `action`              varchar(255) DEFAULT NULL,
    `date_creation`       datetime(6)  DEFAULT NULL,
    `date_modification`   datetime(6)  DEFAULT NULL,
    `etat`                varchar(255) DEFAULT NULL,
    `identifiant_externe` varchar(255) DEFAULT NULL,
    `rev_document`        int(11)      DEFAULT NULL,
    `source`              varchar(255) DEFAULT NULL,
    `statut`              varchar(255) DEFAULT NULL,
    `version`             int(11)      DEFAULT NULL,
    `id`                  bigint(20) NOT NULL,
    `id_document`         bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_revision_document` (`id_document`),
    CONSTRAINT `fk_revision_evenement` FOREIGN KEY (`id`) REFERENCES `evenement` (`id`),
    CONSTRAINT `fk_revision_document` FOREIGN KEY (`id_document`) REFERENCES `document_editable` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `surcharge_modele`
(
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT,
    `chemin`                      varchar(255) DEFAULT NULL,
    `date_creation`               datetime(6)  DEFAULT NULL,
    `date_modification`           datetime(6)  DEFAULT NULL,
    `nom_fichier`                 varchar(255) DEFAULT NULL,
    `uuid`                        char(36)   NOT NULL,
    `id_utilisateur_creation`     bigint(20)   DEFAULT NULL,
    `id_environnement`            bigint(20) NOT NULL,
    `id_modele`                   bigint(20)   DEFAULT NULL,
    `id_utilisateur_modification` bigint(20)   DEFAULT NULL,
    `id_niveau`                   bigint(20)   DEFAULT NULL,
    `id_organisme`                bigint(20)   DEFAULT NULL,
    `id_utilisateur`              bigint(20)   DEFAULT NULL,
    `id_service`                  bigint(20)   DEFAULT NULL,
    `date_actualisation`          datetime(6)                              null,
    `date_insertion`              datetime(6) default current_timestamp(6) not null,
    `commentaire`                 longtext                                 null,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_surcharge_modele_uuid` (`uuid`),
    KEY `fk_surcharge_modele_utilisateur_creation` (`id_utilisateur_creation`),
    KEY `fk_surcharge_modele_environnement` (`id_environnement`),
    KEY `fk_surcharge_modele_modele` (`id_modele`),
    KEY `fk_surcharge_modele_modification` (`id_utilisateur_modification`),
    KEY `fk_surcharge_modele_niveau` (`id_niveau`),
    KEY `fk_surcharge_organisme` (`id_organisme`),
    KEY `fk_surcharge_modele_proprietaire` (`id_utilisateur`),
    KEY `fk_surcharge_modele_service` (`id_service`),
    CONSTRAINT `fk_surcharge_modele_environnement` FOREIGN KEY (`id_environnement`) REFERENCES `environnement` (`id`),
    CONSTRAINT `fk_surcharge_modele_modele` FOREIGN KEY (`id_modele`) REFERENCES `modele` (`id`),
    CONSTRAINT `fk_surcharge_modele_modification` FOREIGN KEY (`id_utilisateur_modification`) REFERENCES `utilisateur` (`id`),
    CONSTRAINT `fk_surcharge_modele_niveau` FOREIGN KEY (`id_niveau`) REFERENCES `referentiel` (`id`),
    CONSTRAINT `fk_surcharge_modele_proprietaire` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`),
    CONSTRAINT `fk_surcharge_modele_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`),
    CONSTRAINT `fk_surcharge_modele_utilisateur_creation` FOREIGN KEY (`id_utilisateur_creation`) REFERENCES `utilisateur` (`id`),
    CONSTRAINT `fk_surcharge_organisme` FOREIGN KEY (`id_organisme`) REFERENCES `organisme` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `surcharge_modele_aud`
(
    `id`                          bigint(20) NOT NULL,
    `rev`                         int(11)    NOT NULL,
    `revtype`                     tinyint(4)   DEFAULT NULL,
    `chemin`                      varchar(255) DEFAULT NULL,
    `date_creation`               datetime(6)  DEFAULT NULL,
    `date_modification`           datetime(6)  DEFAULT NULL,
    `nom_fichier`                 varchar(255) DEFAULT NULL,
    `uuid`                        varchar(255) DEFAULT NULL,
    `id_utilisateur_creation`     bigint(20)   DEFAULT NULL,
    `id_environnement`            bigint(20)   DEFAULT NULL,
    `id_modele`                   bigint(20)   DEFAULT NULL,
    `id_utilisateur_modification` bigint(20)   DEFAULT NULL,
    `id_niveau`                   bigint(20)   DEFAULT NULL,
    `id_organisme`                bigint(20)   DEFAULT NULL,
    `id_utilisateur`              bigint(20)   DEFAULT NULL,
    `id_service`                  bigint(20)   DEFAULT NULL,
    date_actualisation          datetime(6)  null,
    date_insertion              datetime(6)  null,
    commentaire                 longtext     null,
    PRIMARY KEY (`id`, `rev`),
    KEY `fk_surcharge_modele_aud_revinfo` (`rev`),
    CONSTRAINT `fk_surcharge_modele_aud_revinfo` FOREIGN KEY (`rev`) REFERENCES `revinfo` (`rev`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
