<#assign background = '#eee'>
<#assign padding = '12px'>
<#assign margin = '24px'>
<#assign font = 'Verdana'>
<#assign td_default_style = 'style="padding:${padding}"'>
<#assign table_style = 'style="width:98vw"'>
<#assign tr_default_style = 'style="background-color:${background}"'>
<#assign td_title_style = 'style="padding:${padding}; font-weight:bold;"'>
<#assign span_style = 'style="display:block; margin: ${margin} 0 0 0"'>
<#assign main_style = 'style="font-family: ${font}, sans-serif; color: #333; padding: 15px 0;"'>

<div ${main_style}>
    <span ${span_style}>Bonjour,</span>
    <span ${span_style}>La consultation ci-dessous est en attente de validation : </span>
    <table ${table_style}>
        <tr ${tr_default_style}>
            <td ${td_title_style}>Entité publique</td>
            <td ${td_default_style}><#if consultation?? && consultation.organisme??>${(consultation.organisme.libelle)!'-'}<#else>-</#if></td>
        </tr>
        <tr>
            <td ${td_title_style}>Service</td>
            <td ${td_default_style}><#if consultation?? && consultation.service??>${(consultation.service.libelle)!'-'}<#else>-</#if></td>
        </tr>
        <tr ${tr_default_style}>
            <td ${td_title_style}>Intitulé de la consultation</td>
            <td ${td_default_style}><#if consultation??>${(consultation.intitule)!'-'}<#else>-</#if></td>
        </tr>
        <tr>
            <td ${td_title_style}>Objet de la consultation</td>
            <td ${td_default_style}><#if consultation??>${(consultation.objet)!'-'}<#else>-</#if></td>
        </tr>
        <tr ${tr_default_style}>
            <td ${td_title_style}>Référence de la consultation</td>
            <td ${td_default_style}><#if consultation??>${(consultation.numero)!'-'}<#else>-</#if></td>
        </tr>
        <tr>
            <td ${td_title_style}>Type de procédure</td>
            <td ${td_default_style}><#if consultation?? && consultation.procedure??>${(consultation.procedure.libelle)!'-'}<#else>-</#if></td>
        </tr>
        <tr ${tr_default_style}>
            <td ${td_title_style}>Date et heure de la remise des plis</td>
            <td ${td_default_style}><#if consultation??>${(consultation.dateRemisePlis?datetime?string("dd/MM/yyyy HH'h'mm"))!'-'}<#else>-</#if></td>
        </tr>
    </table>
    <#if consultationUri??>
        <span ${span_style}>Pour accéder à cette consultation, veuillez cliquer sur le lien suivant:</span>
        <span><a href="${consultationUri}">ici</a></span>
    </#if>
    <#if documentUri?? && document?? && document.modele?? && document.modele.type?? && utilisateur??>
        <span ${span_style}>Pour valider le ${(document.modele.type.libelle?lower_case)!'-'} ${(document.nom)!'-'} soumis par <a
                    href="mailto:${(utilisateur.email)!'-'}">${(utilisateur.prenom)!'-'} ${(utilisateur.nom)!'-'}</a> directement, veuillez cliquer </span>
        <span><a href="${(documentUri)!'-'}">ici</a></span>
    </#if>
</div>
