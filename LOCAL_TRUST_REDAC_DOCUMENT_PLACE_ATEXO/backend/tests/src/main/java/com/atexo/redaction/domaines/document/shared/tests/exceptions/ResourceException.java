package com.atexo.redaction.domaines.document.shared.tests.exceptions;

import org.springframework.core.io.Resource;

import java.nio.file.Path;
import java.text.MessageFormat;

public class ResourceException extends RuntimeException {

	public ResourceException(Resource resource) {
		super(MessageFormat.format("Impossible de lire/charger la ressource {0}", resource));
	}

	public ResourceException(Path path) {
		super(MessageFormat.format("Impossible de lire/charger la ressource {0}", path));
	}
}
