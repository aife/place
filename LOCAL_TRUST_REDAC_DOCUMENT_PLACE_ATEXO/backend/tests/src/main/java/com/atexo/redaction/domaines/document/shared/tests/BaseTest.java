package com.atexo.redaction.domaines.document.shared.tests;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public abstract class BaseTest {

	@Setter(onMethod = @__(@Autowired))
	protected TestRestTemplate template;

	protected <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
		return this.template.exchange(url, method, requestEntity, responseType, urlVariables);
	}

	protected <T> ResponseEntity<T> get(String url, HttpEntity<?> requestEntity) {
		return this.template.exchange(url, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<>() {
		});
	}

	protected <T> ResponseEntity<T> get(String url) {
		return this.template.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
		});
	}

	protected <T> ResponseEntity<T> get(String url, HttpEntity<?> requestEntity, Class<T> type) {
		return this.template.exchange(url, HttpMethod.GET, requestEntity, type);
	}

	protected <T> ResponseEntity<T> get(String url, Class<T> type) {
		return this.template.exchange(url, HttpMethod.GET, null, type);
	}
}
