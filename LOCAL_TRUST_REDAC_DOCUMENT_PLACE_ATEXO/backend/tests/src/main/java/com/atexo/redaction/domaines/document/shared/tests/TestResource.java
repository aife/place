package com.atexo.redaction.domaines.document.shared.tests;

import com.atexo.redaction.domaines.document.shared.tests.exceptions.ResourceException;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public class TestResource extends ClassPathResource {

	@Builder(builderMethodName = "with")
	public TestResource(@NonNull String path) {
		super(path);
	}

	public String content() {
		try (var stream = this.getInputStream()) {
			return new String(stream.readAllBytes(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new ResourceException(this);
		}
	}
}
