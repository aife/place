package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	Optional<Utilisateur> findByIdentifiantExterneAndService(String identifiantExterne, Service service);

	Optional<Utilisateur> findByUuid(UUID uuid);
}
