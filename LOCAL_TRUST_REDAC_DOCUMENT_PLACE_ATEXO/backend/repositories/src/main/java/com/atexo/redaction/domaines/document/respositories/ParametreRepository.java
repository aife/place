package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Parametre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ParametreRepository extends JpaRepository<Parametre, Long> {

	/**
	 * Permet de récuperer le paramètre definir par le clé pour un environnement
	 *
	 * @param environnement l'environnement
	 * @param cle la cle du paramètre
	 */
	Optional<Parametre> findByEnvironnementAndCle(Environnement environnement, String cle);


	/**
	 * Permet de récuperer le paramètre definir par le clé pour un environnement
	 *
	 * @param environnement l'environnement
	 */
	Set<Parametre> findAllByEnvironnement(Environnement environnement);
}
