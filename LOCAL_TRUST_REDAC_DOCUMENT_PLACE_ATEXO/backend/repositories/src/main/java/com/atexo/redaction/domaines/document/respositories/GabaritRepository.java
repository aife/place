package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Gabarit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface GabaritRepository extends JpaRepository<Gabarit, Long> {

	Optional<Gabarit> findByEnvironnementAndNiveauCodeAndModeleId(Environnement environnement, String niveau, Long idModele);

	Optional<Gabarit> findByNiveauCodeAndModeleIdAndProprietaireId(String niveau, Long idModele, Long idUtilisateur);

	Optional<Gabarit> findByNiveauCodeAndModeleIdAndServiceId(String niveau, Long idModele, Long idService);

	Optional<Gabarit> findByNiveauCodeAndModeleIdAndOrganismeId(String niveau, Long idModele, Long idOrganisme);

	Optional<Gabarit> findByUuid(UUID uuid);
}
