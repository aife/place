package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContexteRepository extends JpaRepository<Contexte, Long> {

	/**
	 * Permet de trouver un contexte depuis la reference de l'objet metier
	 *
	 * @param environnement l'environnement
	 * @param reference la reference de l'objet metier
	 *
	 * @return le contexte, optionnel
	 */
	Optional<Contexte> findByEnvironnementAndReference(Environnement environnement, String reference);

	/**
	 * Permet de trouver un contexte depuis son uuid
	 *
	 * @param uuid l'uuid du contexte
	 *
	 * @return le contexte, optionnel
	 */
	Optional<Contexte> findByUuid(UUID uuid);
}
