package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Statut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

	Optional<Document> findByUuid(UUID uuid);

	@Query(value = "SELECT * FROM document_editable d " +
			"LEFT JOIN contexte c on c.id = d.id_contexte " +
			"LEFT JOIN environnement e on e.id = c.id_environnement " +
			"JOIN modele m on m.id = d.id_modele " +
			"JOIN referentiel r on r.id = m.id_type " +
			"LEFT JOIN referentiel p on p.id = r.id_parent " +
			"WHERE ( :expression IS NULL OR ( c.reference REGEXP :expression ) OR ( d.nom REGEXP :expression) ) " +
			"AND ( COALESCE(:categories, '') = '' OR p.code IN (:categories) ) " +
			"AND ( COALESCE(:statuts, '') = '' OR d.statut IN (:statuts) ) " +
			"AND ( :contexte IS NULL OR d.id_contexte = :#{#contexte?.id} ) " +
			"AND ( :environnement IS NULL OR e.id = :#{#environnement?.id} ) " +
			"ORDER BY d.id DESC"
			, nativeQuery = true)
	List<Document> findAll(@Param("environnement") Environnement environnement, @Param("contexte") Contexte contexte, @Param("expression") String expression, @Param("categories") Collection<String> categories, @Param("statuts") Collection<String> statuts);

	Set<Document> findAllByContexteIdOrderByIdDesc(Long idContexte);

	Set<Document> findAllByContexteIdAndStatutOrderByIdDesc(Long idContexte, Statut statut);
}
