package com.atexo.redaction.domaines.document.respositories.referentiel;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TypeModeleRepository extends JpaRepository<TypeModele, Long> {

	Optional<TypeModele> findByCodeAndEnvironnementsContains(String code, Environnement environnement);
}
