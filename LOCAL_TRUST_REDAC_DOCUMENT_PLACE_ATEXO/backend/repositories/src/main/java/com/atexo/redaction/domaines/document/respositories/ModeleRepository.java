package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Modele;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ModeleRepository extends JpaRepository<Modele, Long> {

	List<Modele> findAllByTypeEnvironnementsContainingOrderByOrdreDesc(Environnement environnement);

	List<Modele> findAllByTypeEnvironnementsContainingAndTypeParentCodeOrderByOrdreDesc(Environnement environnement, String categorie);

	List<Modele> findAllByTypeEnvironnementsContainingAndAdministrableTrueOrderByTypeParentOrdre(Environnement environnement);

	Optional<Modele> findByUuid(UUID uuid);
}
