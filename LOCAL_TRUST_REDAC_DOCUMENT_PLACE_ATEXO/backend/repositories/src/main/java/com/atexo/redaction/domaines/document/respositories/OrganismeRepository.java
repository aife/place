package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface OrganismeRepository extends JpaRepository<Organisme, Long> {

	Optional<Organisme> findByIdentifiantExterneAndEnvironnement(Long identifiantExterne, Environnement environnement);

	Optional<Organisme> findByUuid(UUID uuid);
}
