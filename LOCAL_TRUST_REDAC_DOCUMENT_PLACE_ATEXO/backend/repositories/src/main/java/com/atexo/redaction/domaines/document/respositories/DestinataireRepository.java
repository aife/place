package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Destinataire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DestinataireRepository extends JpaRepository<Destinataire, Long> {

	/**
	 * Permet de trouver un destinataire par son identifiant unique
	 *
	 * @param identifiantExterne l'identifiant unique
	 *
	 * @return l'identifiant unique
	 */
	Optional<Destinataire> findByIdentifiantExterne(String identifiantExterne);
}
