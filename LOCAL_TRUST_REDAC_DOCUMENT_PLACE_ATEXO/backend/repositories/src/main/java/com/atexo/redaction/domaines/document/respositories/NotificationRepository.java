package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

}
