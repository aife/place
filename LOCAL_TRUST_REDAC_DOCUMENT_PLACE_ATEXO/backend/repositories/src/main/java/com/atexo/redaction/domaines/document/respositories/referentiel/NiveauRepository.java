package com.atexo.redaction.domaines.document.respositories.referentiel;

import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NiveauRepository extends JpaRepository<Niveau, Long> {

	Optional<Niveau> findByCode(String code);
}
