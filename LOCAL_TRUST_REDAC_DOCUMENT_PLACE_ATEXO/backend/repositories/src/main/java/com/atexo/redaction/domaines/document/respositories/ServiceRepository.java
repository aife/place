package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

	Optional<Service> findByIdentifiantExterneAndOrganisme(Long identifiantExterne, Organisme organisme);

	Optional<Service> findByUuid(UUID uuid);
}
