package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.BaseEvenement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvenementRepository extends JpaRepository<BaseEvenement, Long> {

}
