package com.atexo.redaction.domaines.document.respositories.referentiel;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ReferentielRepository extends JpaRepository<Referentiel, Long> {

	Set<Referentiel> findAllByEnvironnementsContaining(Environnement environnement);

}
