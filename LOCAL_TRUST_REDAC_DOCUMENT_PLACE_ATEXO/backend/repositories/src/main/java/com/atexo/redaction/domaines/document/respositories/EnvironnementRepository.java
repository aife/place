package com.atexo.redaction.domaines.document.respositories;

import com.atexo.redaction.domaines.document.model.Environnement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface EnvironnementRepository extends JpaRepository<Environnement, Long> {

	/**
	 * Permet de trouver un environnement
	 *
	 * @param client le client
	 * @param plateforme la plateforme
	 *
	 * @return l'environnement
	 */
	@Query(value = "select * from Environnement e where :client REGEXP e.client and :plateforme REGEXP e.plateforme", nativeQuery = true)
	Optional<Environnement> findByClientRegexAndPlateformeRegex(@Param("client") String client, @Param("plateforme") String plateforme);

	/**
	 * Permet de trouver un environnement depuis son UUID
	 *
	 * @param uuid l'UUID de l'environnement
	 *
	 * @return l'environnement
	 */
	Optional<Environnement> findByUuid(UUID uuid);

    Optional<Environnement> findByMpeUuid(String mpeUuid);
}
