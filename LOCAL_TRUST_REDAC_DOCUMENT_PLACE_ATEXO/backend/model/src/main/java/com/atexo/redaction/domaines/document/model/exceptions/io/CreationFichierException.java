package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;
import lombok.NonNull;

import java.nio.file.Path;

import static java.text.MessageFormat.format;

public class CreationFichierException extends BaseInvalideException {

	@Builder(builderMethodName = "with")
	public CreationFichierException(@NonNull Path fichier, final String erreur) {
		super(format("Impossible de créer le fichier ''{0}'' (''{1}'')", fichier, erreur));
	}
}
