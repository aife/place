package com.atexo.redaction.domaines.document.model.converters;

import javax.persistence.AttributeConverter;
import java.util.UUID;

public class UUIDConverter implements AttributeConverter<UUID, String> {

	@Override
	public String convertToDatabaseColumn(UUID uuid) {
		String result = null;

		if (null != uuid) {
			result = uuid.toString();
		}

		return result;
	}

	@Override
	public UUID convertToEntityAttribute(String s) {
		UUID result = null;

		if (null != s) {
			result = UUID.fromString(s);
		}

		return result;
	}
}
