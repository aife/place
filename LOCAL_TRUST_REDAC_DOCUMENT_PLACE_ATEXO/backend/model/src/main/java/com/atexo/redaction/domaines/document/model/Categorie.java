package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.helpers.EnumHelper;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum Categorie {

	MODELE_PLATEFORME("MODELE_PLATEFORME"),
	MODELE_DAJ("MODELE_DAJ"),
	DOCUMENT_LIBRE("DOCUMENT_LIBRE"),
	DOCUMENT_CANEVAS("DOCUMENT_CANEVAS");

	@NonNull
	private final String code;

	public static Optional<Categorie> trouver(String code) {
		return EnumHelper.trouver(Categorie.class, item -> code.equals(item.getCode()));
	}
}
