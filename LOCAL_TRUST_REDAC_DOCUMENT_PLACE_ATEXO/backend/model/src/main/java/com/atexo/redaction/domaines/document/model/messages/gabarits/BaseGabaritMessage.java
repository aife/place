package com.atexo.redaction.domaines.document.model.messages.gabarits;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "action")
@JsonSubTypes({
		@JsonSubTypes.Type(value = SuppressionGabaritMessage.class, name = "suppression"),
		@JsonSubTypes.Type(value = CreationGabaritMessage.class, name = "creation")
})
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
public abstract class BaseGabaritMessage extends BaseMessage {

	@NonNull
	protected Gabarit gabarit;

	protected Utilisateur utilisateur;

	@NonNull
	protected Contexte contexte;

	public Optional<Utilisateur> getUtilisateur() {
		return Optional.ofNullable(utilisateur);
	}
}
