package com.atexo.redaction.domaines.document.model.audit;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Embeddable
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
@Builder(builderMethodName = "with")
public class CreateAudit {

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE })
	private Utilisateur utilisateur;

	@Builder.Default
	private Instant date = Instant.now();
}
