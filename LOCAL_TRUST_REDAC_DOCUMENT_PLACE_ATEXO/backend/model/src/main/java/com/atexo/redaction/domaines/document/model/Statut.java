package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;

/**
 * Statut du document
 */
@AllArgsConstructor
public enum Statut {
	ERREUR(0), BROUILLON(1), DEMANDE_VALIDATION(2), VALIDE(3);

	private final int value;
}

