package com.atexo.redaction.domaines.document.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Topic {
	TOPIC_TOUS("/topic/*"),
	TOPIC_CONTEXTE("/topic/contextes/{0}"),
	TOPIC_UTILISATEUR("/topic/utilisateurs/{0}"),
	TOPIC_ENVIRONNEMENT("/topic/environnements/{0}"),
	TOPIC_UTILISATEUR_TOUS_GABARITS("/topic/utilisateurs/{0}/gabarits/*"),
	TOPIC_SERVICE_TOUS_GABARITS("/topic/services/{0}/gabarits/*"),
	TOPIC_ORGANISME_TOUS_GABARITS("/topic/organismes/{0}/gabarits/*"),
	TOPIC_ENVIRONNEMENT_TOUS_GABARITS("/topic/environnements/{0}/gabarits/*"),
	TOPIC_CONTEXTE_TOUS_DOCUMENTS("/topic/contextes/{0}/documents/*"),
	TOPIC_DOCUMENT("/topic/documents/{1,number,#}");

	@NonNull
	private final String channel;

	@Override
	public String toString() {
		return this.channel;
	}
}
