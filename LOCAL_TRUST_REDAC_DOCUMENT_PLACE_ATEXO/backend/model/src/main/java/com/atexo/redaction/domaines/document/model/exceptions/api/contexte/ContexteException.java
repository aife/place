package com.atexo.redaction.domaines.document.model.exceptions.api.contexte;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Contexte}
 */
public interface ContexteException {
}
