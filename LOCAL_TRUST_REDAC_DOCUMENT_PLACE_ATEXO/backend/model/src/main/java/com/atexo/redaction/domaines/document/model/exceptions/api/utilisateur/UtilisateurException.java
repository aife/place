package com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Utilisateur}
 */
public interface UtilisateurException {
}
