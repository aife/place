package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Entity
@Audited
@Table(
		name = "organisme",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_organisme_uuid"),
				@UniqueConstraint(columnNames = {"externe_id", "id_environnement"}, name = "uk_organisme_externe_id_id_environnement")
		}
)
@EqualsAndHashCode(of = {"identifiantExterne", "environnement"})
@DynamicUpdate
public class Organisme {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "externe_id", updatable = false)
	private Long identifiantExterne;

	@Lob
	@Column(name = "libelle", updatable = false)
	private String libelle;

	@Column(name = "acronyme", updatable = false)
	private String acronyme;

	@NotAudited
	@Setter
	@ManyToOne
	@JoinColumns(@JoinColumn(name = "id_environnement"))
	private Environnement environnement;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	private final UUID uuid = UUID.randomUUID();

	@Setter
	@Type(type = "json")
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "payload", columnDefinition = "json")
	private String payload;

	@CreationTimestamp
	@Column(name = "date_insertion", nullable = false)
	@ColumnDefault("NOW()")
	private Instant dateInsertion;

	@UpdateTimestamp
	@Column(name = "date_actualisation")
	private Instant dateActualisation;
}
