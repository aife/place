package com.atexo.redaction.domaines.document.model.messages.documents;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class InsoumissionDocumentMessage extends BaseDocumentMessage {

	@Builder(builderMethodName = "with")
	public InsoumissionDocumentMessage(@NonNull Document document, Utilisateur utilisateur, @NonNull Contexte contexte) {
		super(document, utilisateur, contexte);
	}
}
