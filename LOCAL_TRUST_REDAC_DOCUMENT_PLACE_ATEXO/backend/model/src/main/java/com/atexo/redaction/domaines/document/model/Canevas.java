package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Set;

@Embeddable
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class Canevas {

	private Long id;

	@Transient
	private String titre;

	@Transient
	private String type;

	@Transient
	private String auteur;

	@Transient
	private Date dateModification;

	@Transient
	private Date dateCreation;

	@Transient
	private Set<Procedure> procedures;

	private String reference;

	@Transient
	private StatutCanevas statut;

	@Transient
	private Organisme organisme;

	@Transient
	private NaturePrestation naturePrestation;

	@Transient
	private Set<Contrat> contrats;

	@Transient
	private String version;

	public Canevas(Long id) {
		this.id = id;
	}
}
