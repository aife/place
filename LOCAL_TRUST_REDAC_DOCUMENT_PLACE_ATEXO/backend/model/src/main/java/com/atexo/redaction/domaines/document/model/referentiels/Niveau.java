package com.atexo.redaction.domaines.document.model.referentiels;

import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.NIVEAU)
public class Niveau extends Referentiel {
	public static final String AGENT = "AGENT";
	public static final String ENTITE_ACHAT = "ENTITE_ACHAT";
	public static final String ORGANISME = "ORGANISME";
	public static final String PLATEFORME = "PLATEFORME";
}
