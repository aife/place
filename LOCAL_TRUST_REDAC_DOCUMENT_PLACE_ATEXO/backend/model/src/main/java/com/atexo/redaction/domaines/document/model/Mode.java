package com.atexo.redaction.domaines.document.model;

import java.io.Serializable;

/**
 * Mode d'édition
 */
public enum Mode implements Serializable {

	EDITION,
	VISUALISATION,
	REDACTION;
}
