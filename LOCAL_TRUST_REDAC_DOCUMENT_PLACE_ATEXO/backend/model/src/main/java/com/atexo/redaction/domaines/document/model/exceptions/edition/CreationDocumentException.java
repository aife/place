package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.web.client.HttpStatusCodeException;

import java.text.MessageFormat;

/**
 * Exception de creation d'un document
 */
@Getter
public class CreationDocumentException extends BaseServiceEnLigneException {

	@Builder(builderMethodName = "with")
	public CreationDocumentException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception) {
		super(MessageFormat.format("Impossible de créer le document depuis ''{0}'' (statut = {1}, erreur = ''{2}'')", uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());
	}
}
