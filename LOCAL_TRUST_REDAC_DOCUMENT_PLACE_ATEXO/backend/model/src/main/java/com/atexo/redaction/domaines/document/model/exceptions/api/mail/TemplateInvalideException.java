package com.atexo.redaction.domaines.document.model.exceptions.api.mail;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Modèles non trouvé
 */
public class TemplateInvalideException extends BaseInvalideException implements MailException {

	@Builder(builderMethodName = "with")
	public TemplateInvalideException(final String modele) {
		super(MessageFormat.format("Une erreur s'est produite lors du process du template {}", modele));
	}
}
