package com.atexo.redaction.domaines.document.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.context.annotation.Profile;

@Profile("messec")
@Getter
@Builder(builderMethodName = "with")
public class Tag {

	@NonNull
	private String cle;

	@NonNull
	private String code;

	@NonNull
	private String valeur;

}
