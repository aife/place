package com.atexo.redaction.domaines.document.model.exceptions.api.organisme;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Exception pour un organisme introuvable
 */
public class OrganismeIntrouvableException extends BaseIntrouvableException implements OrganismeException {

	@Builder(builderMethodName = "with")
	public OrganismeIntrouvableException(UUID uuid) {
		super(MessageFormat.format("Organisme ''{0}'' introuvable", uuid));
	}
}
