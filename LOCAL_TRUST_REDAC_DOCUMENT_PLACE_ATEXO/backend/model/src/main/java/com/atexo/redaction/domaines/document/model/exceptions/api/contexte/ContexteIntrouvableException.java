package com.atexo.redaction.domaines.document.model.exceptions.api.contexte;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Exception pour contexte introuvable
 */
public class ContexteIntrouvableException extends BaseIntrouvableException implements ContexteException {

	@Builder(builderMethodName = "with")
	public ContexteIntrouvableException(final UUID uuid) {
		super(MessageFormat.format("Contexte ''{0}'' introuvable", uuid));
	}

	@Builder(builderMethodName = "with")
	public ContexteIntrouvableException(final String client, final String plateforme) {
		super(MessageFormat.format("Contexte introuvable pour le client ''{0}'' et la plateforme ''{1}''", client, plateforme));
	}

	@Builder(builderMethodName = "with")
	public ContexteIntrouvableException(final Environnement environnement, final String reference) {
		super(MessageFormat.format("Contexte introuvable pour l''environnement ''{0}'' et la référence ''{1}''", environnement, reference));
	}
}
