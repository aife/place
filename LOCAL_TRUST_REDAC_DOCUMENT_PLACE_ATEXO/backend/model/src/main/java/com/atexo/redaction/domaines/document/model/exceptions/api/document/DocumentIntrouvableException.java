package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * Exception des documents
 */
@Getter
public class DocumentIntrouvableException extends BaseIntrouvableException implements DocumentException {

	@Builder(builderMethodName = "with")
	public DocumentIntrouvableException(final Long id) {
		super(MessageFormat.format("Document ''{0}'' introuvable!", id));
	}
}
