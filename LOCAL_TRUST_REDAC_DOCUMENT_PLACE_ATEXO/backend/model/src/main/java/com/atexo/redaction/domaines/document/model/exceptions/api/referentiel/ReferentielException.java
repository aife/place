package com.atexo.redaction.domaines.document.model.exceptions.api.referentiel;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.referentiels.Referentiel}
 */
public interface ReferentielException {
}
