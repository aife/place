package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;

/**
 * Statut du document
 */
@AllArgsConstructor
public enum EvenementEtape {
	REFUSE, ACCEPTE, ERREUR, ENREGISTRE;

}
