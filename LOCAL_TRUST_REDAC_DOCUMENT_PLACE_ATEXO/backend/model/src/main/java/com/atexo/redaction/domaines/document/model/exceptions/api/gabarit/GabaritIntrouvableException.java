package com.atexo.redaction.domaines.document.model.exceptions.api.gabarit;

import com.atexo.redaction.domaines.document.model.Modele;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour un gabarit introuvable
 */
public class GabaritIntrouvableException extends BaseIntrouvableException implements GabaritException {

	@Builder(builderMethodName = "with")
	public GabaritIntrouvableException(Long id) {
		super(MessageFormat.format("Gabarit ''{0}'' introuvable", id));
	}

	@Builder(builderMethodName = "with")
	public GabaritIntrouvableException(Modele modele) {
		super(MessageFormat.format("Le gabarit pour le type de document {0} est introuvable.", modele.getType().getCode()));
	}
}
