package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;

import java.io.File;
import java.text.MessageFormat;

public class XmlAbsentException extends BaseZipException {

	public XmlAbsentException(final Document document, final File zip) {
		super(document, zip, MessageFormat.format("L''archive zip ''{1}'' communiquée par le service d''édition pour le document ''{0}'' ne contient pas de xml. oO", document.getNom(), zip.getName()));
	}
}
