package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

import static java.text.MessageFormat.format;

/**
 * Exception des documents
 */
@Getter
public class RevisionIntrouvableException extends BaseIntrouvableException implements DocumentException {

	@Builder(builderMethodName = "with")
	public RevisionIntrouvableException(final UUID uuid, final Document document) {
		super(format("La revision ''{0}'' est introuvable pour le document {1}", document.getId()));
	}
}
