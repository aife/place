package com.atexo.redaction.domaines.document.model.converters;

import org.springframework.http.HttpStatus;

import javax.persistence.AttributeConverter;
import java.util.Arrays;

public class HttpStatusConverter implements AttributeConverter<HttpStatus, Integer> {

	@Override
	public Integer convertToDatabaseColumn(HttpStatus status) {
		Integer result = null;

		if (null != status) {
			result = status.value();
		}

		return result;
	}

	@Override
	public HttpStatus convertToEntityAttribute(Integer value) {
		HttpStatus result = null;

		if (null != value) {
			result = Arrays.stream(HttpStatus.values()).filter(status -> status.value() == value).findFirst().orElse(null);
		}

		return result;
	}
}
