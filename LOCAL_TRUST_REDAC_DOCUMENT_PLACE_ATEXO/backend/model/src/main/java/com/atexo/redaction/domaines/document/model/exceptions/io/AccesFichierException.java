package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.io.File;

import static java.text.MessageFormat.format;

public class AccesFichierException extends BaseInvalideException {

	@Builder(builderMethodName = "with")
	public AccesFichierException(File fichier) {
		super(format("Impossible d'acceder au fichier = {0}", fichier));
	}
}
