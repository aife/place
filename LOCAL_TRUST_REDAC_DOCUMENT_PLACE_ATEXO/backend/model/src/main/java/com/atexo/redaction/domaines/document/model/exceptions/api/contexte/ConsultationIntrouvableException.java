package com.atexo.redaction.domaines.document.model.exceptions.api.contexte;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * Exception pour une consultation introuvable
 */
@Getter
public class ConsultationIntrouvableException extends BaseIntrouvableException implements ContexteException {
	private Contexte contexte;

	@Builder(builderMethodName = "with")
	public ConsultationIntrouvableException(final Contexte contexte) {
		super(MessageFormat.format("Consultation introuvable pour le contexte ''{0}''", contexte.getUuid()));

		this.contexte = contexte;
	}
}
