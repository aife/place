package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.web.client.HttpStatusCodeException;

import static java.text.MessageFormat.format;

/**
 * Exception de verification de la plateforme
 */
@Getter
public class VerificationDocumentException extends BaseServiceEnLigneException {

	private final Plateforme plateforme;

	@Builder(builderMethodName = "with")
	public VerificationDocumentException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception, final Plateforme plateforme) {
		super(format("Impossible de vérifier les documents de la plateforme ''{0}'' depuis ''{1}'' (statut = {2}, erreur = ''{3}'')", plateforme, uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());

		this.plateforme = plateforme;
	}
}
