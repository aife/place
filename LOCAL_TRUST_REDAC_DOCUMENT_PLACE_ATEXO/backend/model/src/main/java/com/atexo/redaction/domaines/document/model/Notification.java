package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.LocationConverter;
import com.atexo.redaction.domaines.document.shared.web.Location;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@Entity
@Table(name = "notification", schema = "document")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Notification {

	public static class NotificationBuilder {

		private Set<Destinataire> destinataires = new HashSet<>();

		public NotificationBuilder destinataires(Set<Destinataire> destinataires) {
			this.destinataires = destinataires;

			return this;
		}

		public NotificationBuilder destinataire(Destinataire destinataire) {
			this.destinataires.add(destinataire);

			return this;
		}

		public Notification build() {
			var result = new Notification();

			result.setId(this.id);
			result.setDocument(this.document);
			result.setLien(this.lien);
			result.setCreePar(this.creePar);

			result.setDestinataireNotifications(this.destinataires.stream().map(
					destinataire -> DestinataireNotification.with()
							.notification(result)
							.destinataire(destinataire)
							.defautUri(destinataire.getDefautUri())
							.lien(destinataire.getLien())
							.build()
			).collect(Collectors.toSet()));

			return result;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NonNull
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false, name = "id_document", foreignKey = @ForeignKey(name = "fk_notification_document"))
	@JsonBackReference
	private Document document;

	@NotAudited
	@NonNull
	@OneToMany(mappedBy = "notification", cascade = CascadeType.ALL)
	private Set<DestinataireNotification> destinataireNotifications = new HashSet<>();

	@NonNull
	@Column(name = "date_creation", updatable = false, nullable = false)
	private Instant dateCreation = Instant.now();

	@Column(name = "uri")
	@Convert(converter = LocationConverter.class)
	private Location lien;

	@Embedded
	private Consultation consultation;

	@NonNull
	@Audited
	@ManyToOne
	@JoinColumn(name = "id_utilisateur_creation", foreignKey = @ForeignKey(name = "fk_notification_utilisateur_creation"))
	private Utilisateur creePar;

	@Builder(builderMethodName = "with")
	public Notification(Long id, @NonNull Document document, Location lien, @NonNull Utilisateur creePar, Consultation consultation) {
		this.id = id;
		this.document = document;
		this.lien = lien;
		this.creePar = creePar;
		this.consultation = consultation;
	}
}
