package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Exception generique pour l'édition en ligne des documents
 */
@Getter
public abstract class BaseServiceEnLigneException extends BaseInvalideException {

	protected final Location uri;

	protected final HttpStatus statut;

	protected final String erreur;

	public BaseServiceEnLigneException(final String message, final Location uri, final HttpStatus statut, final String erreur) {
		super(message);

		this.uri = uri;
		this.statut = statut;
		this.erreur = erreur;
	}
}
