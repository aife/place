package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Builder(builderMethodName = "with")
public class Session implements Serializable {

	private final Auteur editeur;

	private final String id;

	private final String titre;

	private final Mode mode;

	private final Location retour;

	private final Plateforme plateforme;

	@Override
	public String toString() {
		return "[ utilisateur='" + editeur + "', mode='" + mode + "', titre='" + titre + "', plateforme='" + plateforme + "', uuid='" + id + "', url de retour='" + retour + "' ]";
	}
}
