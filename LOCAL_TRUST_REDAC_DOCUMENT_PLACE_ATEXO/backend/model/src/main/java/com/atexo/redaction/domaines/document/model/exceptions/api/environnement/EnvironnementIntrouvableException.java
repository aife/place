package com.atexo.redaction.domaines.document.model.exceptions.api.environnement;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Exception pour environnement introuvable
 */
public class EnvironnementIntrouvableException extends BaseIntrouvableException implements EnvironnementException {

	@Builder(builderMethodName = "with")
	public EnvironnementIntrouvableException(final String client, final String plateforme) {
		super(MessageFormat.format("Environnement introuvable pour le client ''{0}'' et la plateforme ''{1}''", client, plateforme));
	}

	@Builder(builderMethodName = "with")
	public EnvironnementIntrouvableException(final UUID uuid) {
		super(MessageFormat.format("Environnement ''{0}'' introuvable", uuid));
	}

}
