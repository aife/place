package com.atexo.redaction.domaines.document.model.converters;

import com.atexo.redaction.domaines.document.shared.web.Location;

import javax.persistence.AttributeConverter;

public class LocationConverter implements AttributeConverter<Location, String> {

	@Override
	public String convertToDatabaseColumn(Location location) {
		String result = null;

		if (null != location) {
			result = location.toString();
		}

		return result;
	}

	@Override
	public Location convertToEntityAttribute(String value) {
		Location result = null;

		if (null != value) {
			result = new Location(value);
		}

		return result;
	}
}
