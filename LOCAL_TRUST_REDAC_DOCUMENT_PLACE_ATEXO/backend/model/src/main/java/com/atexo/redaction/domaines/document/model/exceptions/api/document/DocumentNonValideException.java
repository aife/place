package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Builder;

/**
 * Exception levée si le document devrait etre validé, mais ne l'est pas.
 */
public class DocumentNonValideException extends BaseStatutInvalideException {

	@Builder(builderMethodName = "with")
	public DocumentNonValideException(final Document document) {
		super(document, Statut.VALIDE);
	}
}
