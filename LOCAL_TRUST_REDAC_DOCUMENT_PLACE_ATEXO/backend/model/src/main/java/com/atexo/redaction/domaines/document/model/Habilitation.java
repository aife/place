package com.atexo.redaction.domaines.document.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Champs de fusion
 */
@RequiredArgsConstructor
@Getter
@Builder(builderMethodName = "with")
public class Habilitation {

	@NonNull
	private final String valeur;

	@Override
	public String toString() {
		return valeur;
	}
}
