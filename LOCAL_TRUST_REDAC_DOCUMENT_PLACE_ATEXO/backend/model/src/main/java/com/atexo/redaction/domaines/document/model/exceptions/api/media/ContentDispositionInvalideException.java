package com.atexo.redaction.domaines.document.model.exceptions.api.media;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

public class ContentDispositionInvalideException extends BaseInvalideException implements MediaException {
	@Builder(builderMethodName = "with")
	public ContentDispositionInvalideException(String code) {
		super(MessageFormat.format("Disposition du fichier ''{0}'' non supporté. La valeur doit être une des suivantes = {1}", code));
	}
}
