package com.atexo.redaction.domaines.document.model.referentiels;

import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.CATEGORIE_DOCUMENT)
public class CategorieModele extends Referentiel {
}
