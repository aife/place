package com.atexo.redaction.domaines.document.model.exceptions.api.gabarit;

import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.util.UUID;

import static java.text.MessageFormat.format;

/**
 * Exception pour un gabarit introuvable
 */
public class GabaritRevisionIntrouvableException extends BaseIntrouvableException implements GabaritException {

	@Builder(builderMethodName = "with")
	public GabaritRevisionIntrouvableException(final UUID uuid, final Gabarit gabarit) {
		super(format("La revision ''{0}'' est introuvable pour le gabarit {1}", gabarit.getId()));
	}
}
