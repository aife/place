package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.model.Session;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.core.io.Resource;
import org.springframework.web.client.HttpStatusCodeException;

import static java.text.MessageFormat.format;

/**
 * Exception de visualisation d'un document
 */
@Getter
public class VisualisationDocumentException extends BaseServiceEnLigneException {

	private final Resource document;

	private final Session session;

	@Builder(builderMethodName = "with")
	public VisualisationDocumentException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception, final Resource document, final Session session) {
		super(format("Impossible de visualiser le document ''{0}'' depuis ''{1}'' (statut = {2}, erreur = ''{3}'')", document, uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());

		this.document = document;
		this.session = session;
	}
}
