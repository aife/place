package com.atexo.redaction.domaines.document.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "revision", schema = "document")
@Slf4j
@NoArgsConstructor(force = true)
@Getter
@Setter
public class Revision extends BaseEvenement {

	public enum Source {
		CALLBACK, POOLING
	}

	@Column(name = "rev_document")
	@Type(type = "java.lang.Integer")
	private Number rev;

	@NotAudited
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false, name = "id_document", foreignKey = @ForeignKey(name = "fk_evenement_document"))
	@JsonBackReference
	private Document document;

	@JsonProperty("externe_id")
	@NonNull
	private String identifiantExterne;

	@NonNull
	@Enumerated(EnumType.STRING)
	@Column(name = "etat")
	private Etat etat;

	@Enumerated(EnumType.STRING)
	@Column(name = "action")
	private Action action;

	@Enumerated(EnumType.STRING)
	@Column(name = "statut")
	private Statut statut;

	@NonNull
	@Column(name = "version")
	private Integer version;

	@NonNull
	@Column(name = "date_creation")
	private Instant dateCreation;

	@NonNull
	@Column(name = "date_modification")
	private Instant dateModification;

	@Enumerated(EnumType.STRING)
	@Column(name = "source")
	private Source source;

	@Builder(builderMethodName = "with")
	public Revision(final Long id, final Instant dateEnregistrement, final Object payload, final EvenementEtape etape, final String commentaire, final Number rev, final Document document, final Etat etat, final Action action, final Statut statut, final Integer version, final Instant dateCreation, final Instant dateModification, final String identifiantExterne, final @NonNull Source source) {
		super(id, dateEnregistrement, payload, etape, commentaire);
		this.rev = rev;
		this.document = document;
		this.etat = etat;
		this.action = action;
		this.statut = statut;
		this.version = version;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.identifiantExterne = identifiantExterne;
		this.source = source;
	}

}
