package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import com.atexo.redaction.domaines.document.shared.patterns.Enregistrable;
import lombok.*;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Audited
@Table(
		name = "surcharge_modele",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_surcharge_modele_uuid")
		}
)
public class Gabarit extends BaseLoggable implements Enregistrable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid")
	@Convert(converter = UUIDConverter.class)
	@Builder.Default
	private UUID uuid = UUID.randomUUID();

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "id_modele", foreignKey = @ForeignKey(name = "fk_surcharge_modele_modele"))
	private Modele modele;

	@Audited
	@ManyToOne
	@JoinColumn(name = "id_service", foreignKey = @ForeignKey(name = "fk_surcharge_modele_service"))
	private Service service;

	@Audited
	@ManyToOne
	@JoinColumn(name = "id_organisme", foreignKey = @ForeignKey(name = "fk_surcharge_organisme"))
	private Organisme organisme;

	@Audited
	@ManyToOne
	@JoinColumn(name = "id_utilisateur", foreignKey = @ForeignKey(name = "fk_surcharge_modele_proprietaire"))
	private Utilisateur proprietaire;

	@Column(name = "chemin")
	private String chemin;

	@Column(name = "nom_fichier")
	private String nom;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "id_niveau", foreignKey = @ForeignKey(name = "fk_surcharge_modele_niveau"))
	private Niveau niveau;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "date", column = @Column(name = "date_creation"))
	})
	@AssociationOverrides(
			@AssociationOverride(name = "utilisateur", joinColumns = @JoinColumn(name = "id_utilisateur_creation", foreignKey = @ForeignKey(name = "fk_surcharge_modele_utilisateur_creation")))
	)
	@NonNull
	protected CreateAudit creation = new CreateAudit();

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "date", column = @Column(name = "date_modification")),
			@AttributeOverride(name = "commentaire", column = @Column(name = "commentaire"))
	})
	@AssociationOverrides(
			@AssociationOverride(name = "utilisateur", joinColumns = @JoinColumn(name = "id_utilisateur_modification", foreignKey = @ForeignKey(name = "fk_surcharge_modele_modification")))
	)
	protected UpdateAudit modification = new UpdateAudit();

	@CreationTimestamp
	@Column(name = "date_insertion", nullable = false)
	@ColumnDefault("NOW()")
	protected Instant dateInsertion;

	@UpdateTimestamp
	@Column(name = "date_actualisation")
	protected Instant dateActualisation;

	@Override
	public String getExtension() {
		return FilenameUtils.getExtension(this.nom);
	}


	@NotAudited
	@ManyToOne
	@JoinColumns(@JoinColumn(name = "id_environnement", nullable = false))
	private Environnement environnement;
}
