package com.atexo.redaction.domaines.document.model.messages.gabarits;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class SuppressionGabaritMessage extends BaseGabaritMessage {

	@Builder(builderMethodName = "with")
	public SuppressionGabaritMessage(@NonNull Gabarit gabarit, Utilisateur utilisateur, @NonNull Contexte contexte) {
		super(gabarit, utilisateur, contexte);
	}
}
