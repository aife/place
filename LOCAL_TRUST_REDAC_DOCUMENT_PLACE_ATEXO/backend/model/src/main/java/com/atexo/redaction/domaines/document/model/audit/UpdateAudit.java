package com.atexo.redaction.domaines.document.model.audit;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.Instant;

@Embeddable
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
@Builder(builderMethodName = "with")
public class UpdateAudit {

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE })
	private Utilisateur utilisateur;

	@Builder.Default
	private Instant date = Instant.now();

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String commentaire;
}

