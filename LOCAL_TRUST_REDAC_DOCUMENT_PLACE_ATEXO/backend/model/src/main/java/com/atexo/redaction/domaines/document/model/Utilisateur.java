package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import javax.persistence.*;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@Audited
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Table(name = "utilisateur", schema = "document", uniqueConstraints = {
		@UniqueConstraint(columnNames = "uuid", name = "uk_utilisateur_uuid"),
		@UniqueConstraint(columnNames = {"externe_id", "id_service"}, name = "uk_utilisateur_externe_id_id_service")
})
@TypeDefs(
		@TypeDef(name = "json", typeClass = JsonType.class)
)
@DynamicUpdate
public class Utilisateur extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	private final UUID uuid = UUID.randomUUID();

	@NonNull
	@ManyToOne
	@JoinColumn(name = "id_service", nullable = false, foreignKey = @ForeignKey(name = "fk_utilisateur_service"))
	private Service service;

	@NotAudited
	@Column(name = "externe_id", updatable = false)
	private String identifiantExterne;

	@Audited
	@Column(name = "nom", updatable = false)
	private String nom;

	@Audited
	@Column(name = "prenom", updatable = false)
	private String prenom;

	@Audited
	@Column(name = "email", updatable = false)
	private String email;

	@Setter
	@Type(type = "json")
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "payload", columnDefinition = "json")
	private String payload;

	@CreationTimestamp
	@Column(name = "date_insertion", nullable = false)
	@ColumnDefault("NOW()")
	private Instant dateInsertion;

	@UpdateTimestamp
	@Column(name = "date_actualisation")
	private Instant dateActualisation;

	public String getNomComplet() {
		return MessageFormat.format("{0} {1}", this.prenom, this.nom);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}
		final Utilisateur that = (Utilisateur) o;
		return Objects.equals(this.identifiantExterne, that.identifiantExterne);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.identifiantExterne);
	}

	@Override
	public String toString() {
		return this.getNomComplet();
	}
}
