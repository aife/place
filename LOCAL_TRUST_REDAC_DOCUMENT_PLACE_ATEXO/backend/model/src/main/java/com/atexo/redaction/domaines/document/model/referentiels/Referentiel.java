package com.atexo.redaction.domaines.document.model.referentiels;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_referentiel")
@Table(name = "referentiel")
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class Referentiel extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "type_referentiel", insertable = false, updatable = false)
	private String type;

	@Column(name = "code", unique = true)
	private String code;

	@Lob
	@Column(name = "libelle")
	private String libelle;

	@Column(name = "ordre")
	private Integer ordre;

	@ManyToOne
	@JoinColumn(name = "id_parent", foreignKey = @ForeignKey(name = "fk_referentiel_parent"))
	private Referentiel parent;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(schema = "document", name = "referentiel_environnement",
			joinColumns = @JoinColumn(name = "id_referentiel", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_environnement", referencedColumnName = "id")
	)
	private Set<Environnement> environnements;
}
