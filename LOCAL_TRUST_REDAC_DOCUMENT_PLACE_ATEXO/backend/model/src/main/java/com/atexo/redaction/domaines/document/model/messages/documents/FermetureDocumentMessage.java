package com.atexo.redaction.domaines.document.model.messages.documents;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class FermetureDocumentMessage extends BaseDocumentMessage {

	private Boolean enregistrement;

	@Builder(builderMethodName = "with")
	public FermetureDocumentMessage(@NonNull Document document, Utilisateur utilisateur, @NonNull Contexte contexte, @NonNull Boolean enregistrement) {
		super(document, utilisateur, contexte);

		this.enregistrement = enregistrement;
	}
}
