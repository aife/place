package com.atexo.redaction.domaines.document.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum TypeDocument {
	XLSX("xlsx"),
	DOCX("docx"),
	PDF("pdf");

	@NonNull
	private final String extension;

	public static Optional<TypeDocument> rechercher(String extension) {
		return Arrays.stream(values()).filter(type -> type.getExtension().equals(extension)).findFirst();
	}
}
