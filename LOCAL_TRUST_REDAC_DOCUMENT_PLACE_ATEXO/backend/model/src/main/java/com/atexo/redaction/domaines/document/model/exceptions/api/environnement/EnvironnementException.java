package com.atexo.redaction.domaines.document.model.exceptions.api.environnement;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Environnement}
 */
public interface EnvironnementException {
}
