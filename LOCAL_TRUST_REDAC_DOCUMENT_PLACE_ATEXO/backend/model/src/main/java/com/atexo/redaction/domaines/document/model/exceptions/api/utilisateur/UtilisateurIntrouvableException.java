package com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Exception pour un utilisateur introuvable
 */
public class UtilisateurIntrouvableException extends BaseIntrouvableException implements UtilisateurException {

	@Builder(builderMethodName = "with")
	public UtilisateurIntrouvableException(final UUID uuid) {
		super(MessageFormat.format("Utilisateur ''{0}'' introuvable", uuid));
	}
}
