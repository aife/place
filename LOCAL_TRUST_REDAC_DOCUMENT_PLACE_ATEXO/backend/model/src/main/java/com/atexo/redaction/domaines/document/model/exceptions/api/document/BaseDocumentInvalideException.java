package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Getter;

import java.text.MessageFormat;

@Getter
public abstract class BaseDocumentInvalideException extends BaseInvalideException implements DocumentException {

	public BaseDocumentInvalideException(final Document document, final String message) {
		super(MessageFormat.format("Le document ''{0}'' est invalide! {1}", document, message));
	}
}
