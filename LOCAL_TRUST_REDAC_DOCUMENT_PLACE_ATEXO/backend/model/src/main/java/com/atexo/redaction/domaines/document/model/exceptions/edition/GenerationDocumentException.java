package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.model.ChampFusion;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.core.io.Resource;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Set;

import static java.text.MessageFormat.format;

/**
 * Exception de la génération de document
 */
@Getter
public class GenerationDocumentException extends BaseServiceEnLigneException {

	private final Resource document;

	private final Set<ChampFusion> contexte;

	@Builder(builderMethodName = "with")
	public GenerationDocumentException(final @NonNull Location uri, final HttpStatusCodeException exception, final Resource document, final Set<ChampFusion> contexte) {
		super(format("Impossible de générer le document ''{0}'' depuis ''{1}'' (statut = {2}, erreur = ''{3}'')", document, uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());

		this.document = document;
		this.contexte = contexte;
	}
}
