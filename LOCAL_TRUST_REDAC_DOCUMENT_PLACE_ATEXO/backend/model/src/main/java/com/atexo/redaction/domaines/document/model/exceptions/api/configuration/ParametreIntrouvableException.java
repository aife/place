package com.atexo.redaction.domaines.document.model.exceptions.api.configuration;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception si paramètre introuvable
 */
public class ParametreIntrouvableException extends BaseIntrouvableException implements ConfigurationException {

	@Builder(builderMethodName = "with")
	public ParametreIntrouvableException(Environnement environnement, String cle) {
		super(MessageFormat.format("Paramètre introuvable pour l''environnement ''{0}'' et la clé ''{1}''", environnement, cle));
	}
}
