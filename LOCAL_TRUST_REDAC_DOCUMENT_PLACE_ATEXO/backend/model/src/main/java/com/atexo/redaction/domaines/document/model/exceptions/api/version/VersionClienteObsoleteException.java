package com.atexo.redaction.domaines.document.model.exceptions.api.version;

import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour environnement introuvable
 */
public class VersionClienteObsoleteException extends BaseInvalideException implements VersionException {

	@Builder(builderMethodName = "with")
	public VersionClienteObsoleteException(final Version version) {
		super(MessageFormat.format("La version cliente ''{0}'' est trop ancienne pour cette instance de redacdocument", version));
	}

}
