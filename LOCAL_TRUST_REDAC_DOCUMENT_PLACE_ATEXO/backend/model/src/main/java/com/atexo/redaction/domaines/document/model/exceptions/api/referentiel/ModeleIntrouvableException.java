package com.atexo.redaction.domaines.document.model.exceptions.api.referentiel;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Modèles non trouvé
 */
public class ModeleIntrouvableException extends BaseIntrouvableException implements ReferentielException {

	@Builder(builderMethodName = "with")
	public ModeleIntrouvableException(final UUID modele) {
		super(MessageFormat.format("Modele ''{0}'' introuvable", modele));
	}

}
