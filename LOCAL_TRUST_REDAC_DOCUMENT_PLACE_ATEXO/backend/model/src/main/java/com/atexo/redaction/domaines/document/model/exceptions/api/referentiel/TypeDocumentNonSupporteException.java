package com.atexo.redaction.domaines.document.model.exceptions.api.referentiel;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

public class TypeDocumentNonSupporteException extends BaseInvalideException implements ReferentielException {
	@Builder(builderMethodName = "with")
	public TypeDocumentNonSupporteException(String type) {
		super(MessageFormat.format("Type de document ''{0}'' non supporté", type));
	}
}
