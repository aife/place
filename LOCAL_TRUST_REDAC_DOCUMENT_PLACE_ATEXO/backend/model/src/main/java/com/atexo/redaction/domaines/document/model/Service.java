package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@NoArgsConstructor(force = true)
@Entity
@Audited
@Table(
		name = "service",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_service_uuid"),
				@UniqueConstraint(columnNames = {"externe_id", "id_organisme"}, name = "uk_service_externe_id_id_organisme")
		}
)
@DynamicUpdate
@EqualsAndHashCode(of = "uuid", callSuper = false)
public class Service extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	private final UUID uuid = UUID.randomUUID();

	@Setter
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "id_parent", foreignKey = @ForeignKey(name = "fk_service_parent"))
	private Service parent;

	@Setter
	@NonNull
	@ManyToOne
	@JoinColumn(name = "id_organisme", nullable = false, foreignKey = @ForeignKey(name = "fk_service_organisme"))
	private Organisme organisme;

	@NotAudited
	@Column(name = "externe_id", updatable = false)
	private Long identifiantExterne;

	@NotAudited
	@Lob
	@Column(name = "libelle", updatable = false)
	private String libelle;

	@Setter
	@NonNull
	@Type(type = "json")
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "payload", columnDefinition = "json")
	private String payload;

	@CreationTimestamp
	@Column(name = "date_insertion", nullable = false)
	@ColumnDefault("NOW()")
	private Instant dateInsertion;

	@UpdateTimestamp
	@Column(name = "date_actualisation")
	private Instant dateActualisation;

	@Builder(builderMethodName = "with")
	public Service(final Service parent, final Organisme organisme, final String payload, final String libelle) {
		this.parent = parent;
		this.organisme = organisme;
		this.payload = payload;
		this.libelle = libelle;
	}
}
