package com.atexo.redaction.domaines.document.model.exceptions.api.version;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Version}
 */
public interface VersionException {
}
