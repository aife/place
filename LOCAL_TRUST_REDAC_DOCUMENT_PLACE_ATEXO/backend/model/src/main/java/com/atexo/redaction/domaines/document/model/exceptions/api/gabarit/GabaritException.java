package com.atexo.redaction.domaines.document.model.exceptions.api.gabarit;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Gabarit}
 */
public interface GabaritException {
}
