package com.atexo.redaction.domaines.document.model.referentiels;

import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.TYPE_DOCUMENT)
public class TypeModele extends Referentiel {
}
