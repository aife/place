package com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;

/**
 * Exception pour des habilitations introuvables
 */
public class HabilitationsIntrouvableException extends BaseIntrouvableException implements UtilisateurException {

	public HabilitationsIntrouvableException() {
		super("Habilitations introuvable");
	}
}
