package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Audited
@Table(
		name = "contexte",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_contexte_uuid"),
				@UniqueConstraint(columnNames = {"id_environnement", "reference"}, name = "uk_contexte_id_environnement_reference")
		}
)
@TypeDefs(
		@TypeDef(name = "json", typeClass = JsonType.class)
)
public class Contexte extends BaseLoggable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	@Builder.Default
	private UUID uuid = UUID.randomUUID();

	@Column(name = "plateforme")
	private String plateforme;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumns(@JoinColumn(name = "id_environnement", foreignKey = @ForeignKey(name = "fk_contexte_environnement")))
	private Environnement environnement;

	@Type(type = "json")
	@Column(name = "payload", columnDefinition = "json")
	private String payload;

	@Column(name = "reference")
	private String reference;
}
