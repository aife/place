package com.atexo.redaction.domaines.document.model.exceptions;

import lombok.Getter;

@Getter
public abstract class BaseInvalideException extends RuntimeException {

	public BaseInvalideException(String message) {
		super(message);
	}
}
