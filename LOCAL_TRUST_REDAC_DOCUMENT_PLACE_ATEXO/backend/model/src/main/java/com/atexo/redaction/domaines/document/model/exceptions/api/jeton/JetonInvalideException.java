package com.atexo.redaction.domaines.document.model.exceptions.api.jeton;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;
import lombok.Getter;

/**
 * Exception de vaidation du document
 */
@Getter
public class JetonInvalideException extends BaseInvalideException implements JetonException {

	@Builder(builderMethodName = "with")
	public JetonInvalideException() {
		super("Le jeton récupéré est vide");
	}
}
