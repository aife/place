package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum Autorisation {
	VALIDATION("ROLE_ValidationDocumentsREDAC", "validation"),
	MODIFICATION("ROLE_RedactionDocumentsREDAC", "modification"),
	REPRENDRE("ROLE_integralement_article", "reprendre");

	private final String input;
	private final String output;

	public static Optional<Autorisation> find(final String habilitation) {
		return Arrays.stream(Autorisation.values()).filter(auth -> auth.getInput().equalsIgnoreCase(habilitation)).findFirst();
	}
}
