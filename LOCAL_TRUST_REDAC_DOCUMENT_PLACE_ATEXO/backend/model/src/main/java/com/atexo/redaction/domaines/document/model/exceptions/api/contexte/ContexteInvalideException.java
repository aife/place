package com.atexo.redaction.domaines.document.model.exceptions.api.contexte;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour contexte invalide
 */
public class ContexteInvalideException extends BaseInvalideException implements ContexteException {

	@Builder(builderMethodName = "with")
	public ContexteInvalideException(final String message, final Contexte contexte) {
		super(MessageFormat.format(message, contexte));
	}
}
