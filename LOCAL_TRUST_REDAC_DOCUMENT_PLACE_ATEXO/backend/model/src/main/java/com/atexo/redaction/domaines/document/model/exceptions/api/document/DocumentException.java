package com.atexo.redaction.domaines.document.model.exceptions.api.document;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Document}
 */
public interface DocumentException {
}
