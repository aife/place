package com.atexo.redaction.domaines.document.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Version implements Comparable<Version> {

	private static final String PATTERN = "\\d{4}-\\d{2}.\\d{2}.\\d{2}.*";

	@NonNull
	@Getter
	private final String value;

	public boolean estInvalide() {
		return !this.estValide();
	}

	public boolean estValide() {
		return this.value.matches(PATTERN);
	}

	@Override
	public int compareTo(final @NonNull Version version) {
		return this.value.compareTo(version.value);
	}

	@Override
	public String toString() {
		return this.value;
	}
}
