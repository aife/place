package com.atexo.redaction.domaines.document.model.exceptions;

import lombok.Getter;

@Getter
public abstract class BaseIntrouvableException extends RuntimeException {

	public BaseIntrouvableException(String message) {
		super(message);
	}
}
