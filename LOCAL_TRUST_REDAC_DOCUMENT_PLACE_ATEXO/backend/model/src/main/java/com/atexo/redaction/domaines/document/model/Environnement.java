package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Table(
		name = "environnement",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_environnement_uuid"),
				@UniqueConstraint(columnNames = {"plateforme", "client"}, name = "uk_environnement_plateforme_client")
		}
)
@EqualsAndHashCode(of = {"plateforme", "client"}, callSuper = false)
public class Environnement extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	@Builder.Default
	private UUID uuid = UUID.randomUUID();

	@Column(name = "plateforme", nullable = false)
	private String plateforme;

	@Column(name = "client", nullable = false)
	private String client;

	@Column(name = "url_logo")
	private String urlLogo;


	@Column(name = "mpe_uuid")
	private String mpeUuid;

	@Column(name = "mpe_url")
	private String mpeUrl;

	@NotAudited
	@OneToMany(mappedBy = "environnement", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Builder.Default
	@JsonManagedReference
	private Set<Parametre> parametres = new HashSet<>();
}
