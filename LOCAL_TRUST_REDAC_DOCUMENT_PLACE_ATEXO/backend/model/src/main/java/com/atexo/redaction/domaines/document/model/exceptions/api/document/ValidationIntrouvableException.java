package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Builder;

/**
 * Exception levée si la validation est introuvable dans l'historique du document
 */
public class ValidationIntrouvableException extends BaseStatutIntrouvableException {

	@Builder(builderMethodName = "with")
	public ValidationIntrouvableException(final Document document) {
		super(document, Statut.VALIDE);
	}
}
