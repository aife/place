package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.LocationConverter;
import com.atexo.redaction.domaines.document.shared.web.Location;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@Builder(builderMethodName = "with")
@Entity
@Table(name = "destinataire_notification", schema = "document")
public class DestinataireNotification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(nullable = false, name = "id_notification")
	private Notification notification;

	@NonNull
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(nullable = false, name = "id_destinataire")
	@JsonBackReference
	private Destinataire destinataire;

	@NonNull
	@Column(name = "uri")
	@Convert(converter = LocationConverter.class)
	private Location lien;

	@NonNull
	@Column(name = "defaut_uri", nullable = false)
	@Builder.Default
	private Boolean defautUri = false;
}
