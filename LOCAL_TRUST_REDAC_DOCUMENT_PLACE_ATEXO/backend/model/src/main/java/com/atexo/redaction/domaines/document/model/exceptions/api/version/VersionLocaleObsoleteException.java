package com.atexo.redaction.domaines.document.model.exceptions.api.version;

import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour environnement introuvable
 */
public class VersionLocaleObsoleteException extends BaseInvalideException implements VersionException  {

	@Builder(builderMethodName = "with")
	public VersionLocaleObsoleteException(final Version version) {
		super(MessageFormat.format("La version cliente ''{0}'' est trop récente pour cette instance de redacdocument", version));
	}

}
