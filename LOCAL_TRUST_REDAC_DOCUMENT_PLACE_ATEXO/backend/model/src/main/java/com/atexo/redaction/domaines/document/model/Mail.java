package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.Set;

@Builder(builderMethodName = "with")
@Getter
public class Mail {
	private final Consultation consultation;
	private final Document document;
	private final Utilisateur utilisateur;
	private final Location consultationUri;
	private final Location documentUri;
	@Singular
	private final Set<Destinataire> destinataires;
	@Singular
	private final Set<Tag> tags;
}
