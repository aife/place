package com.atexo.redaction.domaines.document.model.referentiels;

public interface ReferentielDiscriminators {

	String TYPE_DOCUMENT = "TYPE_DOCUMENT";

	String CATEGORIE_DOCUMENT = "CATEGORIE_DOCUMENT";

	String NIVEAU = "NIVEAU";
}
