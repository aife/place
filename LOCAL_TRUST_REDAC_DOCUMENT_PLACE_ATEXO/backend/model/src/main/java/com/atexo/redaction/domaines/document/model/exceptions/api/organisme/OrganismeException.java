package com.atexo.redaction.domaines.document.model.exceptions.api.organisme;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Organisme}
 */
public interface OrganismeException {
}
