package com.atexo.redaction.domaines.document.model.messages.notifications;

import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.text.MessageFormat;

@Getter
@Setter
@AllArgsConstructor
@Builder(builderMethodName = "with")
public class NotificationMessage extends BaseMessage {

	public static class NotificationMessageBuilder {

		public NotificationMessageBuilder titre(String titre, Object... args) {
			this.titre = MessageFormat.format(titre, args);

			return this;
		}

		public NotificationMessageBuilder contenu(String contenu, Object... args) {
			this.contenu = MessageFormat.format(contenu, args);

			return this;
		}

		public NotificationMessageBuilder contenu(String contenu) {
			this.contenu = contenu;

			return this;
		}
	}

	@NonNull
	@JsonProperty("icon")
	private NotificationType type;

	@JsonProperty("title")
	private String titre;

	@NonNull
	@JsonProperty("html")
	private String contenu;

	@JsonProperty("timer")
	private Long delai;
}
