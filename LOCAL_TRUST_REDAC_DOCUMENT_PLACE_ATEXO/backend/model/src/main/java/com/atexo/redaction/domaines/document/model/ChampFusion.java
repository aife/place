package com.atexo.redaction.domaines.document.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.text.MessageFormat;

/**
 * Champs de fusion
 */
@RequiredArgsConstructor
@Getter
@Setter
public class ChampFusion {

	/**
	 * Préfixe
	 */
	protected static final String PATTERN = "{0}";

	@JsonProperty("key")
	@NonNull
	private final String cle;

	@JsonProperty("value")
	@NonNull
	private final String valeur;

	@Override
	public String toString() {
		return MessageFormat.format("[{0}={1}]", cle, valeur);
	}
}
