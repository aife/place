package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class Procedure {

	private Integer id;

	private String libelle;

	private String acronyme;
}
