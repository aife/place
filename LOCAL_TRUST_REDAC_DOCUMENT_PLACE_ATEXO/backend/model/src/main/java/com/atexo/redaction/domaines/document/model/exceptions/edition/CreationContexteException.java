package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.web.client.HttpStatusCodeException;

import java.text.MessageFormat;

/**
 * Exception d'initialisation du contexte
 */
@Getter
public class CreationContexteException extends BaseServiceEnLigneException {

	private final Contexte contexte;

	@Builder(builderMethodName = "with")
	public CreationContexteException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception, final Contexte contexte) {
		super(MessageFormat.format("Impossible d'initialiser le contexte ''{0}'' depuis ''{1}'' (statut = {2}, erreur = ''{3}'')", contexte, uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());

		this.contexte = contexte;
	}
}
