package com.atexo.redaction.domaines.document.model.exceptions;

import lombok.Getter;

@Getter
public abstract class BaseNonAutoriseException extends RuntimeException {

	public BaseNonAutoriseException(String message) {
		super(message);
	}
}
