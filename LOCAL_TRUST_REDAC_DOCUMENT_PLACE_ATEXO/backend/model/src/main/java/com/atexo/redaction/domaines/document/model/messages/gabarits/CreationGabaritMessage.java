package com.atexo.redaction.domaines.document.model.messages.gabarits;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class CreationGabaritMessage extends BaseGabaritMessage {

	@Builder(builderMethodName = "with")
	public CreationGabaritMessage(@NonNull Gabarit gabarit, Utilisateur utilisateur, @NonNull Contexte contexte) {
		super(gabarit, utilisateur, contexte);
	}
}
