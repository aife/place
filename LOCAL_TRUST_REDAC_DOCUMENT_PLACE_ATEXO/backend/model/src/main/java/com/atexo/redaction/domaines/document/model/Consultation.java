package com.atexo.redaction.domaines.document.model;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Set;

@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@AllArgsConstructor
@Getter
@Setter
@Embeddable
public class Consultation {

	@Transient
	private Integer id;

	@Transient
	private String objet;

	@Transient
	private String intitule;

	@Transient
	private Procedure procedure;

	@Transient
	private Organisme organisme;

	@Transient
	private Service service;

	@Transient
	private Date dateRemisePlis;

	private String reference;

	private String numero;

	@Transient
	private boolean mps;

	@Transient
	private Set<Lot> lots;

	public Consultation(String reference) {
		this.reference = reference;
	}
}
