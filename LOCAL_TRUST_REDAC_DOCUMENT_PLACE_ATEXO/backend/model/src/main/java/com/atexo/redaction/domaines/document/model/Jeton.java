package com.atexo.redaction.domaines.document.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Jeton d'authentification
 */
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
public class Jeton {

	@JsonValue
	@NonNull
	private String valeur;

	@Override
	public String toString() {
		return valeur;
	}
}
