package com.atexo.redaction.domaines.document.model;

import lombok.*;

import javax.persistence.Embeddable;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Getter
@Setter
@Embeddable
public class Plateforme {

	@NonNull
	private String id;

	@Override
	public String toString() {
		return id;
	}
}
