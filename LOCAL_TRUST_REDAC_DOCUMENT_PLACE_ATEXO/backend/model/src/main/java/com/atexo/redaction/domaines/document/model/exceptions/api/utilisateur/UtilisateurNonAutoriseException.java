package com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur;

import com.atexo.redaction.domaines.document.model.Autorisation;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.exceptions.BaseNonAutoriseException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour utilisateur introuvable
 */
public class UtilisateurNonAutoriseException extends BaseNonAutoriseException implements UtilisateurException {

	@Builder(builderMethodName = "with")
	public UtilisateurNonAutoriseException(final Utilisateur utilisateur, final Autorisation... autorisations) {
		super(MessageFormat.format("L'utilisateur ''{0}'' n'est pas autorisé! Il lui manque l'habilitation ''{1}''", utilisateur.getIdentifiantExterne(), autorisations));
	}
}
