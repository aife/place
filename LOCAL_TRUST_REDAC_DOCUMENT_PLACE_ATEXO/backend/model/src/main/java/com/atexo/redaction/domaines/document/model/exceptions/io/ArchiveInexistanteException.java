package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;

import java.text.MessageFormat;

public class ArchiveInexistanteException extends BaseTelechargementException {

	public ArchiveInexistanteException(final Document document) {
		super(document, MessageFormat.format("L''archive zip n''a pas pu être recuperée pour le document ''{0}''", document.getNom()));
	}
}
