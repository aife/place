package com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

/**
 * Exception pour utilisateur introuvable
 */
public class UtilisateurAbsentException extends BaseInvalideException implements UtilisateurException {

	@Builder(builderMethodName = "with")
	public UtilisateurAbsentException() {
		super("Utilisateur absent. Huh?");
	}
}
