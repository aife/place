package com.atexo.redaction.domaines.document.model.exceptions.api.service;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Exception levée pour un service introuvable
 */
public class ServiceIntrouvableException extends BaseIntrouvableException implements ServiceException {

	@Builder(builderMethodName = "with")
	public ServiceIntrouvableException(UUID uuid) {
		super(MessageFormat.format("Service ''{0}'' introuvable", uuid));
	}
}
