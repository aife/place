package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Getter;

@Getter
public abstract class BaseTelechargementException extends BaseInvalideException {

	private final Document document;

	public BaseTelechargementException(final Document document, final String message) {
		super(message);

		this.document = document;
	}
}
