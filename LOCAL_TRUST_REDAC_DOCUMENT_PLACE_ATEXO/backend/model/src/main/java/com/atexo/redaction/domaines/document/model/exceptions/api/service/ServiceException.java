package com.atexo.redaction.domaines.document.model.exceptions.api.service;

/**
 * Exception des {@link com.atexo.redaction.domaines.document.model.Service}
 */
public interface ServiceException {
}
