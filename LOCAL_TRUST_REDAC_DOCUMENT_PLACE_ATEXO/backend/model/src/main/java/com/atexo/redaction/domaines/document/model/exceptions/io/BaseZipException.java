package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;
import lombok.Getter;

import java.io.File;

@Getter
public abstract class BaseZipException extends BaseTelechargementException {

	private final File zip;

	public BaseZipException(final Document document, final File zip, final String message) {
		super(document, message);

		this.zip = zip;
	}
}
