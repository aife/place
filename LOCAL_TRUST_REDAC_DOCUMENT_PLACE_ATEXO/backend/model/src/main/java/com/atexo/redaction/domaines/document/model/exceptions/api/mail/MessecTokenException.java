package com.atexo.redaction.domaines.document.model.exceptions.api.mail;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * Exception d'initialisation du contexte
 */
@Getter
public class MessecTokenException extends BaseInvalideException implements MailException {

	@Builder(builderMethodName = "with")
	public MessecTokenException(Location uri) {
		super(MessageFormat.format("Impossible d'obtenir le jeton de messec depuis ''{0}''", uri));
	}
}
