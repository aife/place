package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.core.io.Resource;
import org.springframework.web.client.HttpStatusCodeException;

import java.text.MessageFormat;

/**
 * Exception de conversion de document
 */
@Getter
public class ConversionDocumentException extends BaseServiceEnLigneException {

	private final Resource ressource;

	private final String type;

	@Builder(builderMethodName = "with")
	public ConversionDocumentException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception, final Resource ressource, final String type) {
		super(MessageFormat.format("Impossible de convertir la ressource ''{0}'' depuis ''{1}'' (statut = {2}, erreur = ''{3}'')", ressource.getFilename(), uri, exception.getStatusCode(), exception.getMessage()), uri, exception.getStatusCode(), exception.getMessage());

		this.ressource = ressource;
		this.type = type;
	}
}
