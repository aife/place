package com.atexo.redaction.domaines.document.model.exceptions.api.media;

/**
 * Exception des {@link org.springframework.http.MediaType}
 */
public interface MediaException {
}
