package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Builder;

/**
 * Exception levée si la demande de validation est introuvable dans l'historique du document
 */
public class DemandeValidationIntrouvableException extends BaseStatutIntrouvableException {

	@Builder(builderMethodName = "with")
	public DemandeValidationIntrouvableException(final Document document) {
		super(document, Statut.DEMANDE_VALIDATION);
	}
}
