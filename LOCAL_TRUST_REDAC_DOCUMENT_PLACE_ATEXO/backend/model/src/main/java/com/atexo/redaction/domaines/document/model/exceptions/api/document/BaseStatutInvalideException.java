package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * Statut du document invalide
 */
@Getter
public abstract class BaseStatutInvalideException extends BaseDocumentInvalideException implements DocumentException {

	public BaseStatutInvalideException(final Document document, final Statut attendu) {
		super(document, MessageFormat.format("Le statut du document devrait etre ''{0}'' mais est ''{1}''", attendu, document.getStatut()));
	}
}
