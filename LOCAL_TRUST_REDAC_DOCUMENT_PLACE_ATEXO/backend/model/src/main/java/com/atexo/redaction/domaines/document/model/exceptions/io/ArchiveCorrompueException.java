package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;

import java.io.File;
import java.text.MessageFormat;

public class ArchiveCorrompueException extends BaseZipException {

	public ArchiveCorrompueException(final Document document, final File zip) {
		super(document, zip, MessageFormat.format("L''archive ''{1}'' communiquée par le service d''édition pour le document ''{0}'' est corrompue (impossible de le lire).", document.getNom(), zip.getName()));
	}
}
