package com.atexo.redaction.domaines.document.model.messages.notifications;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter(onMethod = @__(@JsonValue))
@RequiredArgsConstructor
public enum NotificationType {

	SUCCES("success"),
	ERREUR("error"),
	AVERTISSEMENT("warning"),
	INFORMATION("info"),
	QUESTION("question");

	@NonNull
	private final String label;
}
