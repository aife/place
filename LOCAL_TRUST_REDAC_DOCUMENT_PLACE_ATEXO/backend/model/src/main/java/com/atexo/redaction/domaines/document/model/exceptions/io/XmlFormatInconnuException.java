package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;
import lombok.Getter;

import java.io.File;
import java.text.MessageFormat;

@Getter
public class XmlFormatInconnuException extends BaseXmlException {

	public XmlFormatInconnuException(final Document document, final File zip, final File xml) {
		super(document, zip, xml, MessageFormat.format("Le document ''{0}'' à été validé, mais le xml ''{1}'' du zip ''{2}'' ne contient pas de format de sortie", document.getNom(), xml, zip.getName()));
	}
}
