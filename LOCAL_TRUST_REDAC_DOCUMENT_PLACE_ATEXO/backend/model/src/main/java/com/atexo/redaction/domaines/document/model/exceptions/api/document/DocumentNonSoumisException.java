package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Builder;

/**
 * Exception levée si le document devrait etre soumis, mais ne l'est pas.
 */
public class DocumentNonSoumisException extends BaseStatutInvalideException {

	@Builder(builderMethodName = "with")
	public DocumentNonSoumisException(final Document document) {
		super(document, Statut.DEMANDE_VALIDATION);
	}
}
