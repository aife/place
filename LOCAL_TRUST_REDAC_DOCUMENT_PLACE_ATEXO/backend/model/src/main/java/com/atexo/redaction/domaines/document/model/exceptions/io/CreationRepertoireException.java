package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.io.File;
import java.text.MessageFormat;

public class CreationRepertoireException extends BaseInvalideException {

	@Builder(builderMethodName = "with")
	public CreationRepertoireException(String message, File repertoire) {
		super(MessageFormat.format(message, repertoire));
	}
}
