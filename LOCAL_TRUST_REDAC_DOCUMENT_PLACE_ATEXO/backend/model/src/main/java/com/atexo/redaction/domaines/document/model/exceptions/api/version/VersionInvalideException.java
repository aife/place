package com.atexo.redaction.domaines.document.model.exceptions.api.version;

import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour version invalide
 */
public class VersionInvalideException extends BaseInvalideException implements VersionException  {

	@Builder(builderMethodName = "with")
	public VersionInvalideException(final Version version) {
		super(MessageFormat.format("La version cliente ''{0}'' est invalide", version));
	}
}
