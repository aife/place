package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Table(name = "parametre", schema = "document")
public class Parametre extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NonNull
	@ManyToOne
	@JoinColumns(@JoinColumn(name = "id_environnement"))
	@JsonBackReference
	private Environnement environnement;

	@NonNull
	@Column(name = "cle")
	private String cle;

	@Lob
	@NonNull
	@Column(name = "valeur")
	private String valeur;
}
