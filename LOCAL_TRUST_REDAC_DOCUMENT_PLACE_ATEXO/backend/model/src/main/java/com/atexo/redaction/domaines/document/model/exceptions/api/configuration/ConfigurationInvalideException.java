package com.atexo.redaction.domaines.document.model.exceptions.api.configuration;

import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour contexte invalide
 */
public class ConfigurationInvalideException extends BaseInvalideException implements ConfigurationException {

	@Builder(builderMethodName = "with")
	public ConfigurationInvalideException(final String message, final Object... params) {
		super(MessageFormat.format(message, params));
	}
}
