package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;

import java.io.File;
import java.text.MessageFormat;

public class ArchiveVideException extends BaseZipException {

	public ArchiveVideException(final Document document, final File zip) {
		super(document, zip, MessageFormat.format("L''archive zip ''{1}'' communiquée par le service d''édition pour le document ''{0}'' est vide (elle est lisible mais ne contient rien)", document.getNom(), zip.getName()));
	}
}
