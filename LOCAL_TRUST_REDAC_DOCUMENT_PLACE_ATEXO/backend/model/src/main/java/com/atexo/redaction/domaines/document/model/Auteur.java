package com.atexo.redaction.domaines.document.model;

import lombok.*;

import java.io.Serializable;
import java.util.Set;

@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Getter
@Setter
public class Auteur implements Serializable {

	@NonNull
	private String id;

	@NonNull
	private String nom;

	@Singular
	private Set<String> autorisations;
}
