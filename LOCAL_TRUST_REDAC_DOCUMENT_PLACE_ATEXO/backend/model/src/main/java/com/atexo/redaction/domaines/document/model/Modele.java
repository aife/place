package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.shared.patterns.BaseLoggable;
import lombok.*;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Table(
		name = "modele",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_modele_uuid")
		}
)
public class Modele extends BaseLoggable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid", updatable = false)
	@Convert(converter = UUIDConverter.class)
	@Builder.Default
	private UUID uuid = UUID.randomUUID();

	@Column(name = "libelle")
	private String libelle;

	@Column(name = "nom_fichier")
	private String nomFichier;

	@ManyToOne
	@JoinColumn(name = "id_type", foreignKey = @ForeignKey(name = "fk_modele_type"), nullable = false)
	private TypeModele type;

	@Column(name = "ordre")
	private int ordre;

	@Column(name = "administrable")
	private Boolean administrable;

	@Column(name = "actif", nullable = false)
	private boolean actif;

	public boolean est(final Categorie attendu) {
		return Optional.of(this)
				.map(Modele::getType)
				.map(TypeModele::getParent)
				.map(Referentiel::getCode)
				.map(Categorie::trouver)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(categorie -> attendu == categorie)
				.orElse(false);
	}

	public String extension() {
		return FilenameUtils.getExtension(this.getNomFichier());
	}
}
