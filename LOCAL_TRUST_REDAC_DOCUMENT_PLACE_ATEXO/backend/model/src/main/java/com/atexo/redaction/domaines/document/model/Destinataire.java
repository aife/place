package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.web.Location;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
@Entity
@Table(
		name = "destinataire",
		schema = "document",
		uniqueConstraints = @UniqueConstraint(columnNames = {"externe_id", "id_environnement"}, name = "uk_destinataire_externe_id_id_environnement")
)
public class Destinataire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "nom", nullable = false)
	private String nom;

	@Column(name = "prenom", nullable = false)
	private String prenom;

	@NonNull
	@Column(name = "externe_id")
	private String identifiantExterne;

	@NonNull
	@ManyToOne
	@JoinColumns(@JoinColumn(name = "id_environnement"))
	private Environnement environnement;

	@NonNull
	@Column(name = "email", nullable = false)
	private String email;

	@OneToMany(mappedBy = "destinataire", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Builder.Default
	@JsonManagedReference
	private Set<DestinataireNotification> destinataireNotifications = new HashSet<>();

	@Transient
	private Location lien;

	@Transient
	private Boolean defautUri;

	@Override
	public String toString() {
		return MessageFormat.format("{0} {1} ({2})", prenom, nom, email);
	}

	public String getNomComplet() {
		return MessageFormat.format("{0} {1}", prenom, nom);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Destinataire that = (Destinataire) o;
		return identifiantExterne.equalsIgnoreCase(that.identifiantExterne);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifiantExterne.toUpperCase());
	}
}
