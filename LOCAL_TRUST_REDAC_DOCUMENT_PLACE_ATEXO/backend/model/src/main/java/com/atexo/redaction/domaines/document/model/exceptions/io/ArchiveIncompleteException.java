package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;
import lombok.Getter;

import java.io.File;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

@Getter
public class ArchiveIncompleteException extends BaseZipException {

	private final Iterable<String> extensions;

	public ArchiveIncompleteException(final Document document, final File zip, final String... extensions) {
		this(document, zip, Arrays.stream(extensions).collect(Collectors.toSet()));
	}

	public ArchiveIncompleteException(final Document document, final File zip, final Iterable<String> extensions) {
		super(document, zip, MessageFormat.format("L''archive ''{1}'' communiquée par le service d''édition pour le document ''{0}'' ne contient pas le fichier attendu (avec les extensions = ''{2}'')", document.getNom(), zip.getName(), String.join(",", extensions)));

		this.extensions = extensions;
	}
}
