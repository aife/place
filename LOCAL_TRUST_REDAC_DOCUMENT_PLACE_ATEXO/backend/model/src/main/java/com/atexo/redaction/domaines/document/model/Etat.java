package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.shared.helpers.EnumHelper;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.Validate;

import java.util.Optional;

/**
 * Statut du document
 */
@RequiredArgsConstructor
@Getter
public enum Etat {

	CREE("CREATED"),
	MIS_A_JOUR("UPDATED"),
	INCONNU("UNKNOWN"),
	DEMANDE_OUVERTURE("REQUEST_TO_OPEN"),
	GENERE("GENERATED"),
	OUVERT("OPENED"),
	SAUVEGARDE_ET_FERME("SAVED_AND_CLOSED"),
	FERME_SANS_MODIFICATION("CLOSED_WITHOUT_EDITING"),
	TELECHARGE("DOWNLOADED"),
	SAUVEGARDE_AVEC_ERREUR("ERROR_WHILE_SAVING"),
	CORROMPU("CORRUPTED"),
	EDITE_ET_SAUVEGARDE("EDITED_AND_SAVED"),
	NON_TROUVE("NOT_FOUND"),
	DUPLIQUE("DUPLICATED"),
	SOUMIS("SUBMITTED"),
	INSOUMIS("UNSUBMITTED"),
	DEVALIDE("UNVALIDATED"),
	VALIDE("VALIDATED");

	@NonNull
	private final String libelle;

	/**
	 * Permet de trouver une valeur de l'énumération depuis son libellé
	 *
	 * @param libelle le libellé
	 *
	 * @return la valeur de l'énumération, optionnelle
	 */
	public static Optional<Etat> trouver(final String libelle) {
		Validate.notNull(libelle, "Le libellé de la valeur recherche ne peut pas être nul");

		return EnumHelper.trouver(Etat.class, item -> libelle.equals(item.getLibelle()));
	}
}
