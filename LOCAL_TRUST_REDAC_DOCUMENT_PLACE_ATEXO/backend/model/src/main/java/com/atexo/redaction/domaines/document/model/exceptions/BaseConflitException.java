package com.atexo.redaction.domaines.document.model.exceptions;

import lombok.Getter;

@Getter
public abstract class BaseConflitException extends RuntimeException {

	public BaseConflitException(String message) {
		super(message);
	}
}
