package com.atexo.redaction.domaines.document.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.text.MessageFormat;
import java.time.Instant;


@Entity
@Table(name = "evenement", schema = "document")
@Inheritance(strategy = InheritanceType.JOINED)
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Slf4j
public abstract class BaseEvenement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Getter
	@Setter
	private Long id;

	@Column(name = "date_enregistrement")
	private Instant dateEnregistrement = Instant.now();

	@Type(type = "json")
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "payload", columnDefinition = "json")
	@Getter
	@Setter
	private Object payload;

	@Enumerated(EnumType.STRING)
	@Column(name = "etape")
	private EvenementEtape etape = EvenementEtape.ENREGISTRE;

	@Lob
	@Column(name = "commentaire")
	private String commentaire;

	/**
	 * Permet de refuser un evnement
	 *
	 * @param commentaire le commentaire
	 * @param args les arguments
	 */
	public void refuser(String commentaire, Object... args) {
		log.warn(commentaire);

		this.etape = EvenementEtape.REFUSE;
		this.commentaire = MessageFormat.format(commentaire, args);
	}

	/**
	 * Permet d'accepter un evnement
	 */
	public void accepter() {
		this.etape = EvenementEtape.ACCEPTE;
	}

	/**
	 * Permet de rejeter en erreur un évènement
	 */
	public void rejeter() {
		this.etape = EvenementEtape.ERREUR;
	}
}
