package com.atexo.redaction.domaines.document.model.exceptions.api.referentiel;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Modèles non trouvé
 */
public class TypeModeleIntrouvableException extends BaseIntrouvableException implements ReferentielException {

	@Builder(builderMethodName = "with")
	public TypeModeleIntrouvableException(final String code) {
		super(MessageFormat.format("Type de modèle avec le code ''{0}'' introuvable", code));
	}
}
