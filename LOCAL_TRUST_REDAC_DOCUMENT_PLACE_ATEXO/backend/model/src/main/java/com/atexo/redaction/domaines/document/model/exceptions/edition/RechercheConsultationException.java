package com.atexo.redaction.domaines.document.model.exceptions.edition;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.web.client.HttpStatusCodeException;

import static java.text.MessageFormat.format;

/**
 * Exception de recherche d'une consultation
 */
@Getter
public class RechercheConsultationException extends BaseServiceEnLigneException {

	@Builder(builderMethodName = "with")
	public RechercheConsultationException(final @NonNull Location uri, final @NonNull HttpStatusCodeException exception) {
		super(format("Impossible de rechercher la consultation depuis ''{0}'' (statut = {1}, erreur = ''{2}'')", uri, exception.getStatusCode(), exception.getStatusText()), uri, exception.getStatusCode(), exception.getStatusText());
	}
}
