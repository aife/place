package com.atexo.redaction.domaines.document.model.messages.documents;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "action")
@JsonSubTypes({
		@JsonSubTypes.Type(value = CreationDocumentMessage.class, name = "creation"),
		@JsonSubTypes.Type(value = DuplicationDocumentMessage.class, name = "duplication"),
		@JsonSubTypes.Type(value = FermetureDocumentMessage.class, name = "fermeture"),
		@JsonSubTypes.Type(value = InsoumissionDocumentMessage.class, name = "insoumission"),
		@JsonSubTypes.Type(value = InvalidationDocumentMessage.class, name = "invalidation"),
		@JsonSubTypes.Type(value = OuvertureDocumentMessage.class, name = "ouverture"),
		@JsonSubTypes.Type(value = SauvegardeDocumentMessage.class, name = "sauvegarde"),
		@JsonSubTypes.Type(value = SauvegardeDocumentMessage.class, name = "modification"),
		@JsonSubTypes.Type(value = SoumissionDocumentMessage.class, name = "soumission"),
		@JsonSubTypes.Type(value = SuppressionDocumentMessage.class, name = "suppression"),
		@JsonSubTypes.Type(value = TelechargementDocumentMessage.class, name = "telechargement"),
		@JsonSubTypes.Type(value = ValidationDocumentMessage.class, name = "validation")
})
@RequiredArgsConstructor
@AllArgsConstructor
public abstract class BaseDocumentMessage extends BaseMessage {

	@NonNull
	@Getter
	protected Document document;

	protected Utilisateur utilisateur;

	@NonNull
	@Getter
	protected Contexte contexte;

	public Optional<Utilisateur> getUtilisateur() {
		return Optional.ofNullable(utilisateur);
	}
}
