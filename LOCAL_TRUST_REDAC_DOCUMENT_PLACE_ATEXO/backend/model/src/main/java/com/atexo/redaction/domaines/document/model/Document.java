package com.atexo.redaction.domaines.document.model;

import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.converters.UUIDConverter;
import com.atexo.redaction.domaines.document.shared.helpers.XmlHelper;
import com.atexo.redaction.domaines.document.shared.patterns.Enregistrable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.*;

import javax.persistence.*;
import java.io.File;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Audited
@Entity
@Table(
		name = "document_editable",
		schema = "document",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "uuid", name = "uk_document_uuid")
		}
)
public class Document implements Enregistrable {

	public static class DocumentBuilder {
		public DocumentBuilder chemin(final File file) {
			this.chemin = file.toString();
			return this;
		}

		public DocumentBuilder chemin(final String chemin) {
			this.chemin = chemin;
			return this;
		}

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Setter
	@Column(name = "uuid")
	@Convert(converter = UUIDConverter.class)
	@Builder.Default
	private UUID uuid = UUID.randomUUID();

	@NotAudited
	@OneToMany(mappedBy = "document", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Builder.Default
	@JsonManagedReference
	private Set<Notification> notifications = new HashSet<>();

	@NotAudited
	@OneToMany(mappedBy = "document", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Builder.Default
	@JsonManagedReference
	private Set<Revision> revisions = new HashSet<>();

	@Enumerated(EnumType.STRING)
	@Column(name = "action")
	@Builder.Default
	private Action action = Action.INCONNU;

	@Enumerated(EnumType.STRING)
	@Column(name = "statut")
	@Builder.Default
	private Statut statut = Statut.BROUILLON;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE })
	@JoinColumn(name = "id_modele", foreignKey = @ForeignKey(name = "fk_document_modele"))
	private Modele modele;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE })
	@JoinColumn(name = "id_contexte", foreignKey = @ForeignKey(name = "fk_document_contexte"))
	@JsonIgnore
	private Contexte contexte;

	@Setter
	@Lob
	@Column(name = "nom")
	private String nom;

	@Setter
	@Column(name = "extension", length = 6)
	private String extension;

	@Column(name = "actif", nullable = false)
	private boolean actif;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "id", column = @Column(name = "id_canevas")),
			@AttributeOverride(name = "reference", column = @Column(name = "reference_canevas"))
	}
	)
	private Canevas canevas;

	@Column(name = "lot")
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "id", column = @Column(name = "lot"))
	}
	)
	private Lot lot;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "xml")
	@Setter
	private String xml;

	@Setter
	@Column(name = "chemin")
	private String chemin;

	@Setter
	@Column(name = "progression")
	@Builder.Default
	private Double progression = 0d;

	@Setter
	@Lob
	@Column(name = "jeton")
	private String jeton;

	@Setter
	@Column(name = "ouvert")
	private boolean ouvert;

	@Column(name = "externe_id")
	private Long idExterne;

	@Embedded
	@Audited
	@AttributeOverrides({
			@AttributeOverride(name = "date", column = @Column(name = "date_creation"))
	})
	@AssociationOverrides(
			@AssociationOverride(name = "utilisateur", joinColumns = @JoinColumn(name = "id_utilisateur_creation", foreignKey = @ForeignKey(name = "fk_document_utilisateur_creation")))
	)
	@AuditOverrides({
			@AuditOverride(name = "utilisateur"),
			@AuditOverride(name = "date")
	})
	private CreateAudit creation = new CreateAudit();

	@Embedded
	@Audited
	@AttributeOverrides({
			@AttributeOverride(name = "date", column = @Column(name = "date_modification")),
			@AttributeOverride(name = "commentaire", column = @Column(name = "commentaire"))
	})
	@AssociationOverrides(
			@AssociationOverride(name = "utilisateur", joinColumns = @JoinColumn(name = "id_utilisateur_modification", foreignKey = @ForeignKey(name = "fk_document_utilisateur_modification")))
	)
	@AuditOverrides({
			@AuditOverride(name = "utilisateur"),
			@AuditOverride(name = "date"),
			@AuditOverride(name = "commentaire")
	})
	private UpdateAudit modification = new UpdateAudit();

	@CreationTimestamp
	@Column(name = "date_insertion", nullable = false)
	@ColumnDefault("NOW()")
	protected Instant dateInsertion;

	@UpdateTimestamp
	@Column(name = "date_actualisation")
	protected Instant dateActualisation;

	@Override
	public String toString() {
		return this.getNom();
	}

	@Override
	public String getExtension() {
		return this.extension;
	}

	public boolean est(final Statut... statuts) {
		return Arrays.stream(statuts).anyMatch(statut -> this.statut == statut);
	}

	public boolean estBrouillon() {
		return this.est(Statut.BROUILLON);
	}

	public boolean estEnDemandeDeValidation() {
		return this.est(Statut.DEMANDE_VALIDATION);
	}

	public boolean estValide() {
		return this.est(Statut.VALIDE);
	}

	public Document cloner() {
		return Document.with()
				.id(null)
				.action(this.action)
				.statut(this.statut)
				.modele(this.modele)
				.contexte(this.contexte)
				.nom(this.nom)
				.extension(this.extension)
				.actif(this.actif)
				.canevas(this.canevas)
				.lot(this.lot)
				.xml(this.xml)
				.chemin(this.chemin)
				.uuid(UUID.randomUUID())
				.progression(this.progression)
				.jeton(this.jeton)
				.ouvert(this.ouvert)
				.creation(this.creation)
				.notifications(this.notifications)
				.revisions(this.revisions)
				.modification(this.modification)
				.dateInsertion(this.dateInsertion)
				.build();
	}

	public Document dupliquer(final Contexte contexte, final CreateAudit creation, final UpdateAudit modification) {
		final var result = this.cloner();

		result.contexte = contexte;
		result.action = Action.DUPLICATION;
		result.statut = Statut.BROUILLON;
		result.ouvert = false;
		result.nom = MessageFormat.format("{0} (copie)", this.nom);
		if (this.xml != null) {
			result.xml = XmlHelper.supprimer(this.xml, "//document/@lastAction");
		}

		result.creation = creation;
		result.modification = modification;

		return result;
	}

	public Document invalider(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.ouvert = false;
		this.action = Action.INVALIDATION;
		this.statut = Statut.BROUILLON;
		this.modification = audit;

		return this;
	}

	public Document refuserDemandeValidation(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.REFUS_DEMANDE_VALIDATION;
		this.statut = Statut.BROUILLON;
		this.modification = audit;

		return this;
	}

	public Document enregistre(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.ENREGISTREMENT;
		this.statut = Statut.BROUILLON;
		this.modification = audit;

		return this;
	}

	public Document fermerSansModification(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.FERMETURE_SANS_MODIFICATION;
		this.ouvert = false;
		this.modification = audit;

		return this;
	}

	public Document fermerAvecModification(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.FERMETURE_AVEC_MODIFICATION;
		this.ouvert = false;
		this.modification = audit;

		return this;
	}

	public Document notifier(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.NOTIFICATION;
		this.modification = audit;

		return this;
	}

	public Document annuler(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.ANNULATION;
		this.modification = audit;

		return this;
	}

	public Document demanderValidation(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.DEMANDE_VALIDATION;
		this.statut = Statut.DEMANDE_VALIDATION;
		this.modification = audit;

		return this;
	}

	public Document renommer(final String nom, final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.MODIFICATION_DU_NOM;
		this.nom = nom;
		this.modification = audit;

		return this;
	}

	public Document telecharger(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.TELECHARGEMENT;
		this.modification = audit;

		return this;
	}

	public Document valider(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.VALIDATION;
		this.statut = Statut.VALIDE;
		this.modification = audit;

		return this;
	}

	public Document supprimer(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.SUPPRESSION;
		this.actif = false;
		this.modification = audit;

		return this;
	}

	public Document demanderOuverture(final Jeton jeton, final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.DEMANDE_OUVERTURE;
		this.ouvert = false;
		this.jeton = jeton.getValeur();
		this.modification = audit;

		return this;
	}

	public Document ouvrir(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.OUVERTURE;
		this.ouvert = true;
		this.modification = audit;

		return this;
	}

	public Document signaler(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.ouvert = false;
		this.statut = Statut.ERREUR;
		this.modification = audit;

		return this;
	}

	public Document afficher(final UpdateAudit audit) {
		this.uuid = UUID.randomUUID();
		this.action = Action.AFFICHAGE;
		this.modification = audit;

		return this;
	}

	public void ajouter(final Revision revision) {
		revision.setDocument(this);

		this.revisions.add(revision);
	}
}
