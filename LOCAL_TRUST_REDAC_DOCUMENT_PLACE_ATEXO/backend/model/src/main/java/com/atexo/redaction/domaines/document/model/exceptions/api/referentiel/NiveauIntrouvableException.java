package com.atexo.redaction.domaines.document.model.exceptions.api.referentiel;

import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour utilisateur introuvable
 */
public class NiveauIntrouvableException extends BaseIntrouvableException implements ReferentielException {

	@Builder(builderMethodName = "with")
	public NiveauIntrouvableException(final String niveau) {
		super(
				MessageFormat.format("Niveau ''{0}'' introuvable", niveau));
	}
}
