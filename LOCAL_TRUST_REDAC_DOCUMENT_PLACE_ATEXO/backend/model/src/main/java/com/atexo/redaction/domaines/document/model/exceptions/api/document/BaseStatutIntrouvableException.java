package com.atexo.redaction.domaines.document.model.exceptions.api.document;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Statut;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * Statut du document invalide
 */
@Getter
public abstract class BaseStatutIntrouvableException extends BaseIntrouvableException implements DocumentException {

	public BaseStatutIntrouvableException(final Document document, final Statut attendu) {
		super(MessageFormat.format("Le statut ''{0}'' est introuvable pour le document ''{1}''", attendu, document.getStatut()));
	}
}
