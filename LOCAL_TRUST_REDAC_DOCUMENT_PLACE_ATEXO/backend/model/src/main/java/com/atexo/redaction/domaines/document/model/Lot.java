package com.atexo.redaction.domaines.document.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Set;
@Embeddable
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class Lot {

	private Integer id;

	@Transient
	private String numero;

	@Transient
	private String libelle;

	@Transient
	private String description;

	@Transient
	private Set<CPV> cpv;
}
