package com.atexo.redaction.domaines.document.model.exceptions.io;

import com.atexo.redaction.domaines.document.model.Document;
import lombok.Getter;

import java.io.File;

@Getter
public abstract class BaseXmlException extends BaseZipException {

	private final File xml;

	public BaseXmlException(final Document document, final File zip, final File xml, final String message) {
		super(document, zip, message);

		this.xml = xml;
	}
}
