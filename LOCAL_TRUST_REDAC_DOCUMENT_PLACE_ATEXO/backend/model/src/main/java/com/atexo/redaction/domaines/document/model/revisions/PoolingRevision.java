package com.atexo.redaction.domaines.document.model.revisions;

import com.atexo.redaction.domaines.document.model.*;
import lombok.Builder;
import lombok.NoArgsConstructor;
import java.time.Instant;

@NoArgsConstructor(force = true)
public class PoolingRevision extends Revision {

	@Builder
	public PoolingRevision(final Long id, final Instant dateEnregistrement, final Object payload, final EvenementEtape etape, final String commentaire, final Number rev, final Document document, final Etat etat, final Action action, final Statut statut, final Integer version, final Instant dateCreation, final Instant dateModification, final String identifiantExterne) {
		super(id, dateEnregistrement, payload, etape, commentaire, rev, document, etat, action, statut, version, dateCreation, dateModification, identifiantExterne, Source.POOLING);
	}
}
