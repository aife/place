package com.atexo.redaction.domaines.document.model.converters;

import org.springframework.http.HttpMethod;

import javax.persistence.AttributeConverter;

public class HttpMethodConverter implements AttributeConverter<HttpMethod, String> {

	@Override
	public String convertToDatabaseColumn(HttpMethod status) {
		String result = null;

		if (null != status) {
			result = status.name();
		}

		return result;
	}

	@Override
	public HttpMethod convertToEntityAttribute(String value) {
		HttpMethod result = null;

		if (null != value) {
			result = HttpMethod.resolve(value);
		}

		return result;
	}
}
