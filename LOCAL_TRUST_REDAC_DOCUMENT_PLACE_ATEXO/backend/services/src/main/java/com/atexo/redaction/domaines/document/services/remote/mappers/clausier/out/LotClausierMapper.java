package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.LotClausier;
import com.atexo.redaction.domaines.document.model.Lot;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(uses = CPVClausierMapper.class)
public interface LotClausierMapper extends Convertible<LotClausier, Lot> {
}
