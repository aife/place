package com.atexo.redaction.domaines.document.services.ws;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class ConnectionMatcher {

	@NonNull
	private Set<Connection> connections;

	public ConnectionMatcher(@NonNull Set<Connection> connections) {
		this.connections = connections;
	}

	public ConnectionMatcher filtrer(final Environnement environnement) {
		this.connections = this.connections.stream()
				.filter(connection -> connection.correspond(environnement))
				.collect(Collectors.toSet());

		return this;
	}

	public ConnectionMatcher exclure(final Utilisateur... utilisateurs) {
		this.connections = this.connections.stream()
				.filter(connection -> Arrays.stream(utilisateurs).noneMatch(utilisateur -> utilisateur.getUuid().equals(connection.getUtilisateur().getUuid())))
				.collect(Collectors.toSet());

		return this;
	}

	public <T> Set<T> map(Function<Connection, T> mapper) {
		return this.connections.stream().map(mapper).collect(Collectors.toSet());
	}
}
