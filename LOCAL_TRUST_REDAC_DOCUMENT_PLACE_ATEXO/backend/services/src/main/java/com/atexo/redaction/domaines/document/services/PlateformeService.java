package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Plateforme;
import org.springframework.stereotype.Service;

@Service
public interface PlateformeService {

	/**
	 * Permet de récupérer la plateforme d'un contexte
	 *
	 * @param contexte le contexte
	 *
	 * @return la plateforme
	 */
	Plateforme recuperer(final Contexte contexte);
}
