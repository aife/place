package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Version;
import org.springframework.stereotype.Service;

@Service
public interface VersionService {

	/**
	 * Permet de savoir si une version est compatible ou non
	 *
	 * @param version l'identifiant de la version
	 *
	 * @return vrai si la version est compatible, faux sinon
	 */
	boolean estCompatible(Version version);

	/**
	 * Permet de savoir si une version est trop ancienne ou non
	 *
	 * @param version l'identifiant de la version
	 *
	 * @return vrai si la version est trop ancienne, faux sinon
	 */
	boolean estTropAncienne(Version version);

	/**
	 * Permet de savoir si une version est trop recente ou non
	 *
	 * @param version l'identifiant de la version
	 *
	 * @return vrai si la version est trop recente, faux sinon
	 */
	boolean estTropRecente(Version version);
}
