package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import com.atexo.redaction.domaines.document.respositories.referentiel.ReferentielRepository;
import com.atexo.redaction.domaines.document.services.ReferentielService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "referentiels")
public class ReferentielLocalService implements ReferentielService {

	@NonNull
	private final ReferentielRepository repository;

	@Override
	@Cacheable
	public Set<Referentiel> filtrer(Environnement environnement) {
		return repository.findAllByEnvironnementsContaining(environnement);
	}
}
