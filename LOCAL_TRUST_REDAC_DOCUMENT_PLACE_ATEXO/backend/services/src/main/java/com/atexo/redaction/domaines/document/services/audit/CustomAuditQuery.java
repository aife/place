package com.atexo.redaction.domaines.document.services.audit;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.FlushMode;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.hibernate.envers.query.order.AuditOrder;

import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CustomAuditQuery<T> {

	@NonNull
	private final AuditQuery query;

	public CustomAuditQuery(final EntityManager entityManager, Class<T> type) {
		try (var factory = entityManager.unwrap(Session.class)) {
			this.query = AuditReaderFactory.get(factory.getSession()).createQuery().forRevisionsOfEntity(type, false, true);

			this.query.setFlushMode(FlushMode.MANUAL);
			this.query.setLockMode(LockMode.READ);
		}
	}

	public Revision<T> getSingleResult() {
		final var item = (Object[]) this.query.getSingleResult();

		return Revision.<T>with().numero(((DefaultRevisionEntity) item[1]).getId()).version((T) item[0]).build();
	}

	public List<Revision<T>> getResultList() {
		return ((List<?>) this.query.getResultList())
				.stream()
				.map(
						item -> Revision.<T>with().numero(((DefaultRevisionEntity) ((Object[]) item)[1]).getId()).version((T) ((Object[]) item)[0]).build()
				)
				.collect(Collectors.toList());
	}

	public CustomAuditQuery<T> where(final AuditCriterion criterion) {
		this.query.add(criterion);

		return this;
	}

	public CustomAuditQuery<T> and(final AuditCriterion criterion) {
		this.where(criterion);

		return this;
	}

	public CustomAuditQuery<T> orderBy(final AuditOrder order) {
		this.query.addOrder(order);

		return this;
	}

	public CustomAuditQuery<T> setMaxResults(final int max) {
		this.query.setMaxResults(max);

		return this;
	}

	public CustomAuditQuery<T> first() {
		this.query.setMaxResults(1);

		return this;
	}
}
