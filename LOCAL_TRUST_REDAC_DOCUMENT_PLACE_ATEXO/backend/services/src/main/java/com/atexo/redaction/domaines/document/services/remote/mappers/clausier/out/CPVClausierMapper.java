package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.CPVClausier;
import com.atexo.redaction.domaines.document.model.CPV;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface CPVClausierMapper extends Convertible<CPVClausier, CPV> {
}
