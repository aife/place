package com.atexo.redaction.domaines.document.services;

import org.springframework.stereotype.Service;

/**
 * Service de stockage de fichier
 */
@Service
public interface StockageGabaritService extends StockageService {
}
