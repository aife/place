package com.atexo.redaction.domaines.document.services;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;
import java.util.UUID;

/**
 * Service de stockage de ressource
 */
@Service
public interface StockageService {

	/**
	 * Permet de sauvegarder la ressource dans l'entrepot avec pour clé l'uuid
	 *
	 * @param fichier le fichier
	 * @param uuid l'uuid du fichier
	 *
	 * @return retourne le chemin du nouveau fichier
	 */
	File sauvegarder(final Resource fichier, final UUID uuid);

	/**
	 * Permet de copier le fichier depuis chemin dans l'entrepot avec pour clé l'uuid
	 *
	 * @param chemin le chemin du fichier à copier
	 * @param uuid l'uuid du fichier
	 *
	 * @return retourne le chemin du nouveau fichier
	 */
	File copier(final Path chemin, final UUID uuid);
}
