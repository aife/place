package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Notification;
import com.atexo.redaction.domaines.document.respositories.DestinataireRepository;
import com.atexo.redaction.domaines.document.respositories.NotificationRepository;
import com.atexo.redaction.domaines.document.services.NotificationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotificationLocalService implements NotificationService {

	@NonNull
	private final NotificationRepository notificationRepository;

	@NonNull
	private final DestinataireRepository destinataireRespository;

	@Override
	public Notification sauvegarder(Notification notification) {
		notification.getDestinataireNotifications().forEach(destinataireNotification ->
				destinataireRespository.findByIdentifiantExterne(destinataireNotification.getDestinataire().getIdentifiantExterne()).ifPresent(destinataireNotification::setDestinataire));

		return notificationRepository.save(notification);
	}

}
