package com.atexo.redaction.domaines.document.services.audit;

import lombok.Builder;
import lombok.Getter;

@Builder(builderMethodName = "with")
@Getter
public class Revision<T> {

	private final Number numero;

	private final T version;
}
