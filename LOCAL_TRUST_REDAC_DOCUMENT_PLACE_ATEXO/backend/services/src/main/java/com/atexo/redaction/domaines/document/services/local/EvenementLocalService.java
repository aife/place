package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.BaseEvenement;
import com.atexo.redaction.domaines.document.respositories.EvenementRepository;
import com.atexo.redaction.domaines.document.services.EvenementService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "evenements")
public class EvenementLocalService implements EvenementService {

	@NonNull
	private EvenementRepository repository;

	@Override
	public BaseEvenement sauvegarder(final BaseEvenement evenement) {
		Validate.notNull(evenement);

		return this.repository.save(evenement);
	}
}


