package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Service des documents éditables
 */
@Service
public interface DocumentService {

	/**
	 * Permet d'obtenir un document depuis son id
	 *
	 * @param id l'id du document
	 *
	 * @return le document
	 */
	Optional<Document> chercher(final Long id);

	/**
	 * Permet de savoir si un document existe depuis son id
	 *
	 * @param id l'id du document
	 *
	 * @return le document
	 */
	boolean existe(final Long id);

	/**
	 * Permet d'obtenir un document depuis son id
	 *
	 * @param id l'id du document
	 *
	 * @return le document
	 */
	Document recuperer(final Long id);

	/**
	 * Permet d'obtenir la modification d'un document depuis son UUID
	 *
	 * @param uuid l'UUID
	 *
	 * @return la dernière révision
	 */
	Optional<Revision<Document>> rechercherRevision(final UUID uuid);

	/**
	 * Permet de trouver toutes les modifications d'un document
	 *
	 * @param document le document
	 *
	 * @return les revisions
	 */
	List<Revision<Document>> recupererRevisions(final Document document);

	/**
	 * Permet de trouver la derniere révision du document pour le statut en paramètre
	 *
	 * @param document le document
	 *
	 * @return les revisions
	 */
	Optional<Revision<Document>> rechercherDerniereRevision(final Document document);

	/**
	 * Permet de trouver la derniere révision du document pour le statut en paramètre
	 *
	 * @param document le document
	 * @param statut le statut
	 *
	 * @return les revisions
	 */
	Optional<Revision<Document>> rechercherDerniereRevision(final Document document, final Statut statut);

	/**
	 * Permet de trouver la derniere révision du document pour l'action en paramètre
	 *
	 * @param document le document
	 * @param action l'action
	 *
	 * @return les revisions
	 */
	Optional<Revision<Document>> rechercherDerniereRevision(final Document document, final Action action);

	/**
	 * Permet de trouver la révision avant validation
	 *
	 * @param document le document
	 *
	 * @return les revisions
	 */
	Revision<Document> recupererDerniereRevisionEditable(final Document document);
	Document recupererDocumentOriginal(final Document document);

	/**
	 * Permet de dupliquer un document
	 *
	 * @param document le document
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 *
	 * @return les revisions
	 */
	Document dupliquer(Document document, Utilisateur utilisateur, Contexte contexte);

	/**
	 * Permet de trouver les documents par contexte
	 *
	 * @param contexte le contexte
	 *
	 * @return la liste des documents du contexte
	 */
	Set<Document> filtrer(final Contexte contexte);

	/**
	 * Permet de trouver les documents par contexte
	 *
	 * @param contexte le contexte
	 * @param statut les statuts
	 *
	 * @return la liste des documents du contexte
	 */
	Set<Document> filtrer(final Contexte contexte, final Statut statut);

	/**
	 * Permet de rechercher des documents
	 *
	 * @param environnement l'environnement
	 * @param expressions les expressions
	 * @param categories les categories
	 * @param statuts les statuts
	 *
	 * @return les documents
	 */
	List<Document> rechercher(final Environnement environnement, final Contexte contexte, final Collection<String> expressions, final Collection<Categorie> categories, final Collection<Statut> statuts);

	/**
	 * Permet de sauvegarder un document
	 *
	 * @param document le document à sauvegarder
	 */
	Document sauvegarder(Document document);

	/**
	 * Permet de sauvegarder <strong>immédiatement</strong> un document
	 *
	 * @param document le document à sauvegarder
	 */
	Document enregistrer(Document document);

	/**
	 * Permet de sauvegarder des documents
	 *
	 * @param documents les documents
	 *
	 * @return les documents
	 */
	List<Document> sauvegarder(Set<Document> documents);

	/**
	 * Permet de supprimer un document
	 *
	 * @param document le document à suprimer
	 */
	void supprimer(Document document);

	/**
	 * Permet de trouver la progression d'un document
	 *
	 * @param document le document
	 *
	 * @return le taux de progression
	 */
	Double extraireProgression(Document document);

	/**
	 * Permet de trouver le commentaire d'un document
	 *
	 * @param document le document
	 *
	 * @return le taux de progression
	 */
	String extraireCommentaire(Document document);

	/**
	 * Permet de trouver le format de sortie
	 *
	 * @param document le document
	 *
	 * @return le taux de progression
	 */
	String extraireFormat(Document document);

	/**
	 * Formattage de l'identifiant
	 *
	 * @param document le document
	 *
	 * @return le format
	 */
	String format(Document document);

	/**
	 * Récupération de l'identifiant
	 *
	 * @param id l'identifiant
	 *
	 * @return le format
	 */
	Long unformat(String id);

	/**
	 * Permet d'unzipper un fichier et de mettre à jour le document
	 *
	 * @param document le document
	 * @param zip le zip
	 */
	void unzip(Document document, Resource zip);

	/**
	 * Signaler un document en erreur
	 *
	 * @param document le document
	 * @param message le message
	 * @param contexte le contexte
	 * @param utilisateur l'auteur
	 */
	void signaler(final Document document, final String message, final Contexte contexte, final Utilisateur utilisateur);

	/**
	 * Récupération de l'extension d'un fichier
	 *
	 * @param document le document
	 *
	 * @return l'extension du fichier
	 */
	String getExtension(Document document);

	/**
	 * Permet de notifier le document
	 *
	 * @param document le document
	 * @param destinataires les destinataires
	 * @param createur le createur
	 * @param contexte le contexte
	 *
	 * @return l'extension du fichier
	 */
	Notification notifier(Document document, Set<Destinataire> destinataires, Utilisateur createur, Contexte contexte);

	/**
	 * Permet de télécharger le zip du document
	 *
	 * @param document le document
	 */
	void telecharger(final Document document);
}
