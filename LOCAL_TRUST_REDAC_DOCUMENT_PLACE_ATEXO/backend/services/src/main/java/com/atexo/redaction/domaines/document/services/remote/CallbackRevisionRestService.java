package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.model.Action;
import com.atexo.redaction.domaines.document.model.Etat;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.revisions.CallbackRevision;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@NoArgsConstructor(force = true)
@Slf4j
public class CallbackRevisionRestService extends BaseRevisionRestService<CallbackRevision> {

	@Override
	public void reviser(final CallbackRevision revision, final UpdateAudit audit) {
		final var etat = revision.getEtat();
		final var action = revision.getAction();

		// Téléchargement
		if (Etat.TELECHARGE == etat) {
			this.telecharger(revision, audit);
		}

		// Fermeture du document sans modification
		if (Etat.FERME_SANS_MODIFICATION == etat) {
			this.fermerSansEnregistrer(revision, audit);
		}

		// Sauvegarde et fermeture du document
		if (Etat.SAUVEGARDE_ET_FERME == etat) {
			this.sauvegarderEtFerme(revision, audit);
		}

		// Ouverture du document
		if (Etat.OUVERT == etat) {
			this.ouvrir(revision, audit);
		}

		// Demande de validation du document
		if (Action.DEMANDE_VALIDATION == action) {
			this.demanderValidation(revision, audit);

			return;
		}

		// Invalidation du document
		if (Action.INVALIDATION == action) {
			this.invalider(revision, audit);

			return;
		}

		// Validation du document
		if (Action.VALIDATION == action) {
			this.valider(revision, audit);

			return;
		}

		// Sauvegarde du document
		if (Action.ENREGISTREMENT == action) {
			this.sauvegarderBrouillon(revision, audit);

			return;
		}

		log.info("Aucune action pour le document {}", revision.getDocument());
	}
}
