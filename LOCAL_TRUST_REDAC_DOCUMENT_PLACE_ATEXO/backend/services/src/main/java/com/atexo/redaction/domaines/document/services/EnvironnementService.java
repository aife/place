package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Environnement;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * Service des documents éditables
 */
@Service
public interface EnvironnementService {

	/**
	 * Permet de chercher un environnement
	 *
	 * @param uuid l'identifiant de l'environnement
	 *
	 * @return l'environnement, optionel
	 */
	Optional<Environnement> chercher(final UUID uuid);

	/**
	 * Permet de chercher un environnement
	 *
	 * @param client     le client
	 * @param plateforme la plateforme
	 * @param mpeUuid
	 * @return l'environnement, optionel
	 */
	Optional<Environnement> chercher(final String client, final String plateforme, String mpeUuid);

	/**
	 * Permet de récupérer un environnement
	 *
	 * @param client     le client
	 * @param plateforme la plateforme
	 * @param mpeUuid
	 * @return l'environnement, optionel
	 */
	Environnement recuperer(final String client, final String plateforme, String mpeUuid);

	/**
	 * Permet de récupérer un environnement
	 *
	 * @param uuid l'uuid de l'environnement
	 *
	 * @return l'environnement, optionel
	 */
	Environnement recuperer(final UUID uuid);

	/**
	 * Permet de chercher l'environnement commun
	 *
	 * @return l'environnement, doit exister
	 */
	Environnement defaut();

	Environnement sauvegarder(Environnement environnement);
}
