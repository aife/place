package com.atexo.redaction.domaines.document.services.configuration.messec;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

@Getter
@Setter
@NoArgsConstructor(force = true)
@Profile("messec")
public class Reponse {

	private Location lien;
}
