package com.atexo.redaction.domaines.document.services.audit;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.Revision;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.hibernate.envers.query.order.AuditOrder;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class DocumentAuditQuery extends CustomAuditQuery<Document> {

	public DocumentAuditQuery(final EntityManager entityManager) {
		super(entityManager, Document.class);
	}
}
