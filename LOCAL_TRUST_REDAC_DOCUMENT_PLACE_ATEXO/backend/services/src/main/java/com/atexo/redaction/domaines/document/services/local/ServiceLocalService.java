package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.respositories.ServiceRepository;
import com.atexo.redaction.domaines.document.services.ServiceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "services")
public class ServiceLocalService implements ServiceService {

	@NonNull
	private ServiceRepository repository;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<com.atexo.redaction.domaines.document.model.Service> chercher(Long identifiantExterne, Organisme organisme) {
		return repository.findByIdentifiantExterneAndOrganisme(identifiantExterne, organisme);
	}

	@Override
	public Optional<com.atexo.redaction.domaines.document.model.Service> chercher(UUID uuid) {
		return repository.findByUuid(uuid);
	}

	@Override
	@CacheEvict(allEntries = true)
	public com.atexo.redaction.domaines.document.model.Service sauvegarder(com.atexo.redaction.domaines.document.model.Service service) {
		return repository.save(service);
	}
}
