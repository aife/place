package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.ContratClausier;
import com.atexo.redaction.domaines.document.model.Contrat;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface ContratClausierMapper extends Convertible<ContratClausier, Contrat> {
}
