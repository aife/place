package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface TypeModeleService {

	/**
	 * Permet de chercher un type de modele depuis son code
	 *
	 * @param code le code
	 * @param environnement l'environnement
	 *
	 * @return le type de modele
	 */
	Optional<TypeModele> chercher(String code, Environnement environnement);

	/**
	 * Permet de recuperer un type de modele depuis son code
	 *
	 * @param code le code
	 * @param environnement l'environnement
	 *
	 * @return le type de modele
	 */
	TypeModele recuperer(String code, Environnement environnement);
}
