package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.messec.MessecConnector;
import com.atexo.redaction.domaines.document.model.DestinataireNotification;
import com.atexo.redaction.domaines.document.model.Mail;
import com.atexo.redaction.domaines.document.model.Notification;
import com.atexo.redaction.domaines.document.model.Tag;
import com.atexo.redaction.domaines.document.services.MailService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;


@Service
@Slf4j
@RequiredArgsConstructor
@Profile("messec")
public class MessecMailService implements MailService {


	@Autowired
	private MessecConnector messecConnector;


	@Override
	@LogTime("Envoi du mail vers messec")
	public void envoyer(final Notification notification) {
		final var document = notification.getDocument();
		final var consultation = notification.getConsultation();
		final var utilisateur = notification.getCreePar();
		final var consultationUri = notification.getLien();

		var builder = Mail.with()
				.consultation(consultation)
				.document(document)
				.utilisateur(utilisateur)
				.consultationUri(consultationUri)
				.tag(
						Tag.with()
								.cle("Type de document")
								.code("TYPE_DOCUMENT")
								.valeur(document.getModele().getType().getCode().toUpperCase())
								.build()
				)
				.tag(
						Tag.with()
								.cle("Nom du document")
								.code("NOM_DOCUMENT")
								.valeur(document.getNom())
								.build()
				)
				.tag(
						Tag.with()
								.cle("Référence de consultation")
								.code("REFERENCE_CONSULTATION")
								.valeur(consultation.getReference())
								.build()
				);

		notification.getDestinataireNotifications().stream().collect(groupingBy(DestinataireNotification::getLien)).forEach((uri, destinatairesNotifications) -> {
			var data = builder
					.documentUri(uri)
					.destinataires(destinatairesNotifications.stream().map(DestinataireNotification::getDestinataire).collect(Collectors.toSet()))
					.build();

			log.info("Envoi de la notification à {} destinataires vers '{}'", data.getDestinataires().size(), data.getDocumentUri());

			messecConnector.envoyer(data);
		});
	}


}
