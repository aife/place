package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Categorie;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Modele;
import com.atexo.redaction.domaines.document.respositories.ModeleRepository;
import com.atexo.redaction.domaines.document.services.ModeleService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "modeles")
public class ModeleLocalService implements ModeleService {

	@NonNull
	private final ModeleRepository repository;

	@Override
	@Cacheable
	public List<Modele> recuperer(final Environnement environnement) {
		return this.repository.findAllByTypeEnvironnementsContainingOrderByOrdreDesc(environnement);
	}

	@Override
	@Cacheable
	public List<Modele> recupererParCategorie(final Environnement environnement, final Categorie categorie) {
		return this.repository.findAllByTypeEnvironnementsContainingAndTypeParentCodeOrderByOrdreDesc(environnement, categorie.getCode());
	}

	@Override
	@Cacheable
	public List<Modele> recupererAdministrables(final Environnement environnement) {
		return this.repository.findAllByTypeEnvironnementsContainingAndAdministrableTrueOrderByTypeParentOrdre(environnement);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Modele> chercher(final Long id) {
		return this.repository.findById(id);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Modele> chercher(final UUID uuid) {
		return this.repository.findByUuid(uuid);
	}
}
