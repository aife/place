package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.CanevasClausier;
import com.atexo.redaction.domaines.document.model.Canevas;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(uses = {
		StatutCanevasClausierMapper.class,
		ContratClausierMapper.class,
		ProcedureClausierMapper.class,
		NaturePrestationClausierMapper.class,
		OrganismeClausierMapper.class
})
public interface CanevasClausierMapper extends Convertible<CanevasClausier, Canevas> {
}
