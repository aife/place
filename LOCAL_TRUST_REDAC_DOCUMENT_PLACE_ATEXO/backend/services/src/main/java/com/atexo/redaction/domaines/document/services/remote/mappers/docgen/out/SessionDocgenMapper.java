package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.out;

import com.atexo.redaction.domaines.document.connectors.docgen.views.SessionDocgen;
import com.atexo.redaction.domaines.document.model.Session;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

import java.util.Set;

@org.mapstruct.Mapper(uses = {PlateformeDocgenMapper.class, AuteurDocgenMapper.class})
public interface SessionDocgenMapper extends Convertible<Session, SessionDocgen> {

	@Override
	SessionDocgen map(Session bo);

	@Override
	Set<SessionDocgen> map(Set<Session> result);

}
