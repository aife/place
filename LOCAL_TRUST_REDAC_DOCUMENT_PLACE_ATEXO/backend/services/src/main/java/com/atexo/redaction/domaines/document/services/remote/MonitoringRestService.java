package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.docgen.DocgenConnector;
import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionDocgen;
import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.model.revisions.PoolingRevision;
import com.atexo.redaction.domaines.document.services.DocumentService;
import com.atexo.redaction.domaines.document.services.MonitoringService;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class MonitoringRestService implements MonitoringService {

	@NonNull
	private final DocgenConnector docgen;

	@NonNull
	private final DocumentService documentService;

	@NonNull
	private final Convertible<RevisionDocgen, PoolingRevision> revisionMapper;

	@Override
	public Set<PoolingRevision> verifier(final Plateforme plateforme) {
		var revisions = revisionMapper.map(docgen.verifier(plateforme));

		revisions.forEach(
				revision -> {
					var id = documentService.unformat(revision.getIdentifiantExterne());

					documentService.chercher(id).ifPresent(document -> {
						revision.setDocument(document);

						documentService.rechercherDerniereRevision(document).map(Revision::getNumero).ifPresent(revision::setRev);
					});
				}
		);

		return revisions;
	}
}
