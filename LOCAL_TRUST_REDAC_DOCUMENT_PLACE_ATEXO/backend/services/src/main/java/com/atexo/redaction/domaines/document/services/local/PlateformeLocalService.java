package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.services.PlateformeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;

@Service
@Slf4j
@CacheConfig(cacheNames = "plateforme")
public class PlateformeLocalService implements PlateformeService {

	@Value("${docgen.plateform:default-env-{0,number,#}}")
	private Plateforme plateforme;

	@PostConstruct
	private void echo() {
		log.info("Plateforme = '{}'", this.plateforme);
	}

	@Override
	@Cacheable
	public Plateforme recuperer(Contexte contexte) {
		return new Plateforme(MessageFormat.format("{0}-{1,number,#}", plateforme, contexte.getId()));
	}
}
