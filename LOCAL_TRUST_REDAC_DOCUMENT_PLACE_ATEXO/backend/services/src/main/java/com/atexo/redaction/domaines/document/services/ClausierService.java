package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

/**
 * Service des documents éditables
 */
@Service
public interface ClausierService {

	/**
	 * Permet d'initialiser le document canevas à partir du clausier
	 *
	 * @param contexte le contexte
	 * @param createur le createur du document
	 * @param canevas le canevas
	 * @param type le type de document
	 * @param lot le lot
	 *
	 * @return les données du document
	 */
	String creer(final Contexte contexte, final Utilisateur createur, final Canevas canevas, final TypeModele type, final Lot lot);

	/**
	 * Permet de récupérer une consultation depuis sa référence
	 *
	 * @param contexte le contexte
	 *
	 * @return la consultation
	 */
	Optional<Consultation> recuperer(final Contexte contexte);

	/**
	 * Permet de récupérer la liste des canevas
	 *
	 * @param contexte le contexte
	 * @param type le type de document
	 * @param idLot l'identifiant du lot
	 *
	 * @return liste des canevas
	 */
	Set<Canevas> rechercher(final Contexte contexte, final TypeModele type, final Integer idLot);

	/**
	 * Permet de récupérer les utilisateurs depuis la consultation
	 *
	 * @param contexte le contexte
	 *
	 * @return la liste des agents concernés par la consultation
	 */
	Set<Utilisateur> rechercher(final Contexte contexte);
}
