package com.atexo.redaction.domaines.document.services.ws;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Slf4j
@RequiredArgsConstructor
@Getter
public class ConnectionHolder {

	@NonNull
	private final UtilisateurService utilisateurService;

	@NonNull
	private final SerialisationService serialisationService;

	private final Set<Connection> connections = new HashSet<>();

	public Connection add(final Environnement environnement, final Utilisateur utilisateur) {
		this.remove(utilisateur);

		final var result = Connection.with()
				.utilisateur(utilisateur)
				.environnement(environnement)
				.build();

		this.connections.add(result);

		return result;
	}

	public Optional<Connection> remove(final Utilisateur utilisateur) {
		final var result = this.connections
				.stream()
				.filter(connection -> connection.getUtilisateur().getUuid().equals(utilisateur.getUuid())).findFirst();

		result.ifPresent(this.connections::remove);

		return result;
	}

	public ConnectionMatcher matcher() {
		return new ConnectionMatcher(this.connections);
	}
}
