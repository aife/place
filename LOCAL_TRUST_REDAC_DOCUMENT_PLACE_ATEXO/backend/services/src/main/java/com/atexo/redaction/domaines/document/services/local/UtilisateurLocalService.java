package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.respositories.UtilisateurRepository;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "services")
public class UtilisateurLocalService implements UtilisateurService {

	@NonNull
	private final UtilisateurRepository repository;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Utilisateur> chercher(String identifiantExterne, com.atexo.redaction.domaines.document.model.Service service) {
		return repository.findByIdentifiantExterneAndService(identifiantExterne, service);
	}

	@Override
	@CacheEvict
	public Utilisateur sauvegarder(Utilisateur utilisateur) {
		return repository.save(utilisateur);
	}

	@Override
	public Optional<Utilisateur> chercher(final UUID uuid) {
		return this.repository.findByUuid(uuid);
	}
}
