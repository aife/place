package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.services.audit.Revision;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GabaritService {

	Optional<Gabarit> rechercher(Modele modele, Environnement environnement);

	Optional<Gabarit> rechercher(Modele modele, Utilisateur utilisateur);

	Optional<Gabarit> rechercher(Modele modele, Service service);

	Optional<Gabarit> rechercher(Modele modele, Organisme organisme);

	Optional<Gabarit> rechercher(UUID uuid);

	Optional<Gabarit> recuperer(Modele modele, Utilisateur utilisateur);

	Optional<Gabarit> recuperer(Modele modele, Service service);

	Optional<Gabarit> recuperer(Modele modele, Organisme organisme);

	Optional<Gabarit> recuperer(Modele modele, Environnement environnement);

	Gabarit sauvegarder(Gabarit surcharge);

	Optional<Gabarit> chercher(Long id);

	Gabarit creer(String niveau, Utilisateur utilisateur);

	void supprimer(Gabarit surcharge);

	Optional<Gabarit> rechercher(Environnement environnement, Utilisateur utilisateur, Modele modele);

	Optional<Revision<Gabarit>> rechercherRevision(UUID uuid);

	List<Revision<Gabarit>> recupererRevisions(Gabarit gabarit);
}
