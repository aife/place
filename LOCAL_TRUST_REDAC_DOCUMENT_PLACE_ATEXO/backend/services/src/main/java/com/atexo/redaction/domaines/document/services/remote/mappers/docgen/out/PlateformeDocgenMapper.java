package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.out;

import com.atexo.redaction.domaines.document.connectors.docgen.views.PlateformeDocgen;
import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface PlateformeDocgenMapper extends Convertible<Plateforme, PlateformeDocgen> {
}
