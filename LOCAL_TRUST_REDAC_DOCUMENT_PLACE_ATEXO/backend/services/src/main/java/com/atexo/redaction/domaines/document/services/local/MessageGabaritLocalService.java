package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Topic;
import com.atexo.redaction.domaines.document.model.messages.gabarits.CreationGabaritMessage;
import com.atexo.redaction.domaines.document.model.messages.gabarits.SuppressionGabaritMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationType;
import com.atexo.redaction.domaines.document.services.MessageGabaritService;
import com.atexo.redaction.domaines.document.services.MessageService;
import com.atexo.redaction.domaines.document.services.ws.Connection;
import com.atexo.redaction.domaines.document.services.ws.ConnectionHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

import static com.atexo.redaction.domaines.document.model.referentiels.Niveau.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageGabaritLocalService implements MessageGabaritService {

	/**
	 * Delai par défaut
	 */
	private static final long DEFAULT_MESSAGE_DELAI = 5000L;

	@NonNull
	private final ConnectionHolder connectionHolder;

	@NonNull
	private final MessageService messageService;

	@Override
	public void notifier(final SuppressionGabaritMessage message) {
		final var gabarit = message.getGabarit();

		switch (gabarit.getNiveau().getCode()) {
			case AGENT: {
				log.debug("Envoi du message de suppression du gabarit {} a l'utilisateur {}", gabarit.getId(), gabarit.getProprietaire().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_UTILISATEUR_TOUS_GABARITS, gabarit.getProprietaire().getUuid());
				break;
			}
			case ENTITE_ACHAT: {
				log.debug("Envoi du message de suppression du gabarit {} au service {}", gabarit.getId(), gabarit.getService().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_SERVICE_TOUS_GABARITS, gabarit.getService().getUuid());
				break;
			}
			case ORGANISME: {
				log.debug("Envoi du message de suppression du gabarit {} au l'organisme {}", gabarit.getId(), gabarit.getOrganisme().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_ORGANISME_TOUS_GABARITS, gabarit.getOrganisme().getUuid());
				break;
			}
			case PLATEFORME: {
				log.debug("Envoi du message de suppression du gabarit {} au l'environnement {}", gabarit.getId(), gabarit.getEnvironnement().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_ENVIRONNEMENT_TOUS_GABARITS, gabarit.getEnvironnement().getUuid());
				break;
			}
			default:
				throw new IllegalArgumentException("aucune surcharge trouvée");
		}

		log.debug("Envoi du message de confirmation de la suppression du gabarit aux utilisateurs = {}", this.connectionHolder.getConnections());

		final var type = StringUtils.uncapitalize(gabarit.getModele().getType().getLibelle());
		final var contexte = message.getContexte();

		message.getUtilisateur().ifPresentOrElse(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Suppression réussi du gabarit niveau <strong>{0}</strong> pour le type de document <strong>{1}</strong>", gabarit.getNiveau().getCode(), type))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);

					if (!gabarit.getNiveau().getCode().equals(AGENT)) {
						// Notification des autres utilisateurs si le niveau du gabarit est différent de 'AGENT' uniquement
						this.messageService.notifier(
								this.connectionHolder.matcher().filtrer(contexte.getEnvironnement()).exclure(auteur).map(Connection::getUtilisateur),
								NotificationMessage.with()
										.contenu(MessageFormat.format("{0} vient de de supprimer un gabarit niveau <strong>{1}</strong> pour le type de document <strong>{2}</strong>", auteur, gabarit.getNiveau().getCode(), type))
										.type(NotificationType.INFORMATION)
										.delai(DEFAULT_MESSAGE_DELAI)
										.build()
						);
					}
				},
				() -> {
					if (!gabarit.getNiveau().getCode().equals(AGENT)) {
						// Notification des autres utilisateurs si le niveau du gabarit est différent de 'AGENT' uniquement
						this.messageService.notifier(
								this.connectionHolder.matcher().filtrer(contexte.getEnvironnement()).map(Connection::getUtilisateur),
								NotificationMessage.with()
										.contenu(MessageFormat.format("Un gabarit niveau <strong>{0}</strong> vient d''être supprimé pour le type de document <strong>{1}</strong>", gabarit.getNiveau().getCode(), type))
										.type(NotificationType.INFORMATION)
										.delai(DEFAULT_MESSAGE_DELAI)
										.build()
						);
					}
				}
		);
	}

	@Override
	public void notifier(final CreationGabaritMessage message) {
		final var gabarit = message.getGabarit();

		switch (gabarit.getNiveau().getCode()) {
			case AGENT: {
				log.debug("Envoi du message de création du gabarit {} a l'utilisateur {}", gabarit.getId(), gabarit.getProprietaire().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_UTILISATEUR_TOUS_GABARITS, gabarit.getProprietaire().getUuid());
				break;
			}
			case ENTITE_ACHAT: {
				log.debug("Envoi du message de création du gabarit {} au service {}", gabarit.getId(), gabarit.getService().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_SERVICE_TOUS_GABARITS, gabarit.getService().getUuid());
				break;
			}
			case ORGANISME: {
				log.debug("Envoi du message de création du gabarit {} au l'organisme {}", gabarit.getId(), gabarit.getOrganisme().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_ORGANISME_TOUS_GABARITS, gabarit.getOrganisme().getUuid());
				break;
			}
			case PLATEFORME: {
				log.debug("Envoi du message de création du gabarit {} au l'environnement {}", gabarit.getId(), gabarit.getEnvironnement().getUuid());

				this.messageService.notifier(message, Topic.TOPIC_ENVIRONNEMENT_TOUS_GABARITS, gabarit.getEnvironnement().getUuid());
				break;
			}
			default:
				throw new IllegalArgumentException("aucune surcharge trouvée");
		}

		log.debug("Envoi du message de confirmation de la création du gabarit aux utilisateurs = {}", this.connectionHolder.getConnections());

		final var type = StringUtils.uncapitalize(gabarit.getModele().getType().getLibelle());
		final var contexte = message.getContexte();

		message.getUtilisateur().ifPresentOrElse(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Ajout réussi du gabarit <strong>{0}</strong> pour le type de document <strong>{1}</strong>", gabarit.getNiveau().getCode(), type))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);

					if (!gabarit.getNiveau().getCode().equals(AGENT)) {
						// Notification des autres utilisateurs si le niveau du gabarit est différent de 'AGENT' uniquement
						this.messageService.notifier(
								this.connectionHolder.matcher().filtrer(contexte.getEnvironnement()).exclure(auteur).map(Connection::getUtilisateur),
								NotificationMessage.with()
										.contenu(MessageFormat.format("{0} vient de d''ajouter un gabarit niveau <strong>{1}</strong> pour le type  de document <strong>{2}</strong>", auteur, gabarit.getNiveau().getCode(), type))
										.type(NotificationType.INFORMATION)
										.delai(DEFAULT_MESSAGE_DELAI)
										.build()
						);
					}
				},
				() -> {
					if (!gabarit.getNiveau().getCode().equals(AGENT)) {
						// Notification des autres utilisateurs si le niveau du gabarit est différent de 'AGENT' uniquement
						this.messageService.notifier(
								this.connectionHolder.matcher().filtrer(contexte.getEnvironnement()).map(Connection::getUtilisateur),
								NotificationMessage.with()
										.contenu(MessageFormat.format("Un gabarit niveau <strong>{0}</strong> vient d''être ajouté pour le type  de document <strong>{1}</strong>", gabarit.getNiveau().getCode(), type))
										.type(NotificationType.INFORMATION)
										.delai(DEFAULT_MESSAGE_DELAI)
										.build()
						);
					}
				}
		);


	}
}
