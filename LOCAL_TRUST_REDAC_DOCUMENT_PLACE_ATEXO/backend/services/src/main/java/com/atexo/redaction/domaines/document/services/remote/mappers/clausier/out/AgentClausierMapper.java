package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.AgentClausier;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapping;

@org.mapstruct.Mapper(uses = {ServiceLocalMapper.class})
public interface AgentClausierMapper extends Convertible<AgentClausier, Utilisateur> {

	@Override
	@Mapping(source = "service", target = "service", qualifiedByName = "serviceLocalMapper")
	Utilisateur map(AgentClausier agent);
}
