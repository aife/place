package com.atexo.redaction.domaines.document.services.audit;

import com.atexo.redaction.domaines.document.model.Gabarit;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;

@Slf4j
public class GabaritAuditQuery extends CustomAuditQuery<Gabarit> {

	public GabaritAuditQuery(final EntityManager entityManager) {
		super(entityManager, Gabarit.class);
	}
}
