package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public interface UtilisateurService {

	Optional<Utilisateur> chercher(String identifiantExterne, com.atexo.redaction.domaines.document.model.Service service);

	Utilisateur sauvegarder(Utilisateur utilisateur);

	Optional<Utilisateur> chercher(UUID uuid);
}
