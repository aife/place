package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.NaturePrestationClausier;
import com.atexo.redaction.domaines.document.model.NaturePrestation;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface NaturePrestationClausierMapper extends Convertible<NaturePrestationClausier, NaturePrestation> {
}
