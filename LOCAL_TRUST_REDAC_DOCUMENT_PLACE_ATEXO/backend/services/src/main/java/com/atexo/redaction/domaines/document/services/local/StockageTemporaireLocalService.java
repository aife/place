package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.exceptions.io.CreationFichierException;
import com.atexo.redaction.domaines.document.model.exceptions.io.CreationRepertoireException;
import com.atexo.redaction.domaines.document.services.FichierService;
import com.atexo.redaction.domaines.document.services.StockageTemporaireService;
import com.atexo.redaction.domaines.document.shared.patterns.Enregistrable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipInputStream;

@Service
@Slf4j
@RequiredArgsConstructor
public class StockageTemporaireLocalService implements StockageTemporaireService {

	@NonNull
	private final FichierService fileService;

	@Value("${document.tmp.dir}")
	private Path temp;

	@Override
	public Resource copier(final Enregistrable enregistrable) {
		final var origine = enregistrable.getEmplacement();
		final var nom = enregistrable.getUuid() + "." + enregistrable.getExtension();
		final var destination = fileService.copier(origine, temp, nom);

		return new FileSystemResource(destination);
	}

	@Override
	public Resource lire(final UUID uuid) {
		return new FileSystemResource(temp.resolve(uuid.toString()));
	}

	@Override
	public void supprimer(final UUID uuid) {
		try {
			Files.deleteIfExists(temp.resolve(uuid.toString()));
		} catch (final IOException e) {
			log.warn("Impossible de supprimer le fichier temporaire '{}'", uuid);
		}
	}

	@Override
	public File sauvegarder(final Resource resource) {
		return fileService.copier(resource, temp, fileService.creerNomAleatoire());
	}

	@Override
	public File sauvegarder(final InputStream resource) {
		return fileService.copier(resource, temp, fileService.creerNomAleatoire());
	}

	@Override
	public File upload(final InputStream resource) {
		return fileService.copier(resource, temp, fileService.creerNomAleatoire(), false);
	}

	@Override
	public Resource creer(final String contenu, final MimeType type) {
		final var repertoire = fileService.verifierRepertoire(fileService.horodaterRepertoire(temp));
		final var fichier = repertoire.resolve(fileService.creerNomAleatoire() + "." + type.getSubtype());

		try {
			Files.write(fichier, contenu.getBytes(StandardCharsets.UTF_8));
		} catch (IOException exception) {
			throw CreationFichierException.with()
					.fichier(fichier)
					.erreur(exception.getMessage())
					.build();
		}

		return new FileSystemResource(fichier);
	}

	@Override
	public Set<File> unzip(final File zip) throws IOException {
		final var repertoire = fileService.horodaterRepertoire(temp).resolve(fileService.creerNomAleatoire());

		fileService.verifierRepertoire(repertoire);

		return unzip(zip, repertoire);
	}

	@Override
	public Set<File> unzip(final File zip, final Path repertoire) throws IOException {
		final var result = new HashSet<File>();

		try (final var stream = new FileInputStream(zip)) {
			try (final var zis = new ZipInputStream(stream)) {
				var entry = zis.getNextEntry();

				while (null != entry) {
					final var file = repertoire.resolve(entry.getName()).toFile();

					if (!file.getCanonicalPath().startsWith(repertoire.toFile().getCanonicalPath() + File.separator)) {
						throw new IOException("Entry is outside of the target dir: " + entry.getName());
					}

					if (entry.isDirectory()) {
						if (!file.isDirectory() && !file.mkdirs()) {
							throw new CreationRepertoireException("Impossible de créer le répertoire du zip {}", file);
						}
					} else {
						// fix for Windows-created archives
						final var parent = file.getParentFile();

						if (!parent.isDirectory() && !parent.mkdirs()) {
							throw new CreationRepertoireException("Impossible de créer le répertoire parent {}", parent);
						}

						Files.copy(zis, file.toPath());
					}

					result.add(repertoire.resolve(entry.getName()).toFile());
					entry = zis.getNextEntry();
				}
				zis.closeEntry();
			}
		}

		return result;
	}
}
