package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.EnvironnementClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.OrganismeClausier;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.services.OrganismeService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@org.mapstruct.Mapper(componentModel = "spring")
@Named("organismeLocalMapper")
@Slf4j
public abstract class OrganismeLocalMapper implements Convertible<OrganismeClausier, Organisme> {

	@Setter(onMethod = @__(@Autowired))
	private OrganismeService organismeService;

	@Setter(onMethod = @__(@Autowired))
	private OrganismeClausierMapper organismeMapper;

	@Setter(onMethod = @__(@Autowired))
	private Convertible<EnvironnementClausier, Environnement> environnementMapper;

	@Override
	public Organisme map(final OrganismeClausier organisme) {
		Organisme result = null;

		if (null != organisme) {
			// Recherche de l'environnement
			var environnement = environnementMapper.map(organisme.getEnvironnement());

			if (null != environnement) {
				// Recherche de l'organisme
				result = this.organismeService.chercher(organisme.getIdentifiantExterne(), environnement).orElse(null);
			}

			// Si organisme non trouvé
			if (null == result) {
				result = this.organismeMapper.map(organisme);
			}
		}

		return result;
	}
}
	
