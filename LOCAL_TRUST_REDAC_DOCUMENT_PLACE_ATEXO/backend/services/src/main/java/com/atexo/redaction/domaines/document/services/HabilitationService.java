package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Autorisation;
import com.atexo.redaction.domaines.document.model.Habilitation;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service deś habilitations
 */
@Service
public interface HabilitationService {

	/**
	 * Permet d'extraire les habilitations
	 *
	 * @param utilisateur lutilisateur
	 *
	 * @return les habilitations
	 */
	Set<Habilitation> extraire(final Utilisateur utilisateur);

	/**
	 * Le contexte autorise-t-il l'action ?
	 *
	 * @param utilisateur l'utilisateur
	 * @param autorisation l'autorisation
	 *
	 * @return vrai si le contexte autorise l'action, faux sinon
	 */
	boolean autorise(final Utilisateur utilisateur, final Autorisation autorisation);
}
