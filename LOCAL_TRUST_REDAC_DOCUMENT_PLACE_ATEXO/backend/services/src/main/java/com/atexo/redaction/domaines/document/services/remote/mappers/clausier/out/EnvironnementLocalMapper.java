package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.EnvironnementClausier;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.api.environnement.EnvironnementIntrouvableException;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@org.mapstruct.Mapper(componentModel = "spring")
@Named("environnementLocalMapper")
@Slf4j
public abstract class EnvironnementLocalMapper implements Convertible<EnvironnementClausier, Environnement> {

	@Setter(onMethod = @__(@Autowired))
	private EnvironnementService environnementService;

	@Override
	public Environnement map(final EnvironnementClausier environnement) {
		Environnement result = null;

		if (null != environnement) {
			final var client = environnement.getClient();
			final var plateforme = environnement.getPlateforme();
			final var mpeUuid = environnement.getMpeUuid();

			if (null != client && null != plateforme) {
				result = this.environnementService.chercher(client, plateforme, mpeUuid).orElseThrow(() -> new EnvironnementIntrouvableException(client, plateforme));
			}
		}

		return result;
	}
}
	
