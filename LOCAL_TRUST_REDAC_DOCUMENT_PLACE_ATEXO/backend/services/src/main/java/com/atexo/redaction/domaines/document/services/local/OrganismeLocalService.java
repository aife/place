package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.respositories.OrganismeRepository;
import com.atexo.redaction.domaines.document.services.OrganismeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "modeles")
public class OrganismeLocalService implements OrganismeService {

	@NonNull
	private final OrganismeRepository repository;

	@Override
	@Cacheable
	public Optional<Organisme> chercher(Long identifiantExterne, Environnement environnement) {
		return repository.findByIdentifiantExterneAndEnvironnement(identifiantExterne, environnement);
	}

	@Override
	@Cacheable
	public Optional<Organisme> chercher(UUID uuid) {
		return repository.findByUuid(uuid);
	}

	@Override
	@CacheEvict(allEntries = true)
	public Organisme sauvegarder(Organisme organisme) {
		return repository.save(organisme);
	}
}
