package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.ServiceClausier;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.services.ServiceService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@org.mapstruct.Mapper(componentModel = "spring")
@Named("serviceLocalMapper")
@Slf4j
public abstract class ServiceLocalMapper implements Convertible<ServiceClausier, Service> {

	@Setter(onMethod = @__(@Autowired))
	private ServiceService serviceService;

	@Setter(onMethod = @__(@Autowired))
	private ServiceClausierMapper serviceMapper;

	@Setter(onMethod = @__(@Autowired))
	private OrganismeLocalMapper organismeMapper;

	@Override
	public Service map(final ServiceClausier service) {
		Service result = null;

		if (null != service) {
			var organisme = this.organismeMapper.map(service.getOrganisme());

			if (null != organisme.getId()) {
				// L'organisme existe en base
				// Recherche du service
				result = this.serviceService.chercher(service.getIdentifiantExterne(), organisme).orElse(null);
			}

			// Service inexistant en base
			if (null == result) {
				result = this.serviceMapper.map(service);
			}
		}


		return result;
	}
}
	
