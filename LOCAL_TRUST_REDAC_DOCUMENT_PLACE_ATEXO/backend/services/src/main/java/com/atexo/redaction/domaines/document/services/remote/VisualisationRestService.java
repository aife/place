package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.docgen.DocgenConnector;
import com.atexo.redaction.domaines.document.connectors.docgen.views.SessionDocgen;
import com.atexo.redaction.domaines.document.model.Jeton;
import com.atexo.redaction.domaines.document.model.Session;
import com.atexo.redaction.domaines.document.services.VisualisationService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class VisualisationRestService implements VisualisationService {

	@NonNull
	private final DocgenConnector docgen;

	@NonNull
	private final Convertible<Session, SessionDocgen> sessionMapper;

	@Override
	@LogTime("Envoi du document à docgen pour visualisation")
	public Jeton visualiser(final Resource document, final Session session) {
		return docgen.visualiser(document, sessionMapper.map(session));
	}


	@Override
	public Location redirect(final Jeton jeton) {
		return docgen.redirect(jeton);
	}

}
