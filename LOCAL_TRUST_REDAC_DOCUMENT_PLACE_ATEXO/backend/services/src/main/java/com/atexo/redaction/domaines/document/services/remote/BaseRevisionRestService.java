package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.model.Action;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.Revision;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.messages.documents.*;
import com.atexo.redaction.domaines.document.services.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@NoArgsConstructor(force = true)
public abstract class BaseRevisionRestService<T extends Revision> implements RevisionService<T> {

	@Builder(builderMethodName = "with")
	@Getter
	public static class Modification {

		@NonNull
		private final Integer version;

		@NonNull
		@Builder.Default
		private final Boolean telechargement = false;
	}

	@NonNull
	@Setter(onMethod = @__(@Autowired))
	private DocumentService documentService;

	@NonNull
	@Setter(onMethod = @__(@Autowired))
	private EvenementService evenementService;

	@NonNull
	@Setter(onMethod = @__(@Autowired))
	private EditionService editionService;

	@NonNull
	@Setter(onMethod = @__(@Autowired))
	private MessageDocumentService messageDocumentService;

	public abstract void reviser(final T revision, final UpdateAudit audit);

	@Override
	public void appliquer(final T revision) {
		final var document = revision.getDocument();

		if (null == document || !revision.getDateModification().isAfter(document.getModification().getDate())) {
			// Ici, le document n'a pas changé

			return;
		} else {
			log.debug("\uD83D\uDCA5 Evènement entrant de mise à jour du document {}, il passe de '{}/{}' à '{}/{}'", document, document.getStatut(), document.getAction(), revision.getEtat(), revision.getAction());
		}

		 final var audit = UpdateAudit.with().utilisateur(document.getModification().getUtilisateur()).date(revision.getDateModification()).build();

		 this.reviser(revision, audit);
	}

	/**
	 * Permet de tracer le téléchargement d'un document
	 *
	 * @param revision la révision du document
	 */
	protected void telecharger(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();

		log.debug("Téléchargement du document {}", document);

		revision.accepter();

		final var result = document.telecharger(audit);

		result.ajouter(revision);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification du téléchargement du document
		this.messageDocumentService.notifier(
				TelechargementDocumentMessage.with()
						.document(result)
						.contexte(document.getContexte())
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet de fermer un document sans l'enregistrer
	 *
	 * @param revision la révision du document
	 */
	protected void fermerSansEnregistrer(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Fermeture du document {} SANS modification", document);

		revision.accepter();

		final var result = document.fermerSansModification(audit);

		result.ajouter(revision);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de la fermeture du document
		this.messageDocumentService.notifier(
				FermetureDocumentMessage.with()
						.document(result)
						.enregistrement(false)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet d'ouvrir un document
	 *
	 * @param revision la révisition
	 */
	protected void ouvrir(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Ouverture du document {}", document);

		if (!(document.estBrouillon() || document.estEnDemandeDeValidation())) {
			revision.refuser("L'ouverture du document ne peut se faire que sur un brouillon ou une demande de validation. Abandon.");

			document.ajouter(revision);

			this.documentService.sauvegarder(document);

			return;
		}

		revision.accepter();

		final var result = document.ouvrir(audit);

		result.ajouter(revision);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de l'ouverture
		this.messageDocumentService.notifier(
				OuvertureDocumentMessage.with()
						.document(result)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet de valider un document
	 *
	 * @param revision la révision du document
	 */
	protected void valider(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Demande de validation du document {}.", document);

		if (!document.estBrouillon() && !document.estEnDemandeDeValidation()) {
			revision.refuser("La validation du document ne peut se faire que sur un brouillon ou une demande de validation. Abandon.");

			document.ajouter(revision);

			this.documentService.sauvegarder(document);

			return;
		}

		revision.accepter();

		final var result = document.valider(audit);

		// Téléchargement
		this.documentService.telecharger(result);

		result.ajouter(revision);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de la validation
		this.messageDocumentService.notifier(
				ValidationDocumentMessage.with()
						.document(result)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet d'invalider un document
	 *
	 * @param revision la révision du document
	 */
	protected void invalider(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Demande d'invalidation du document {}.", document);

		if (!document.estValide()) {
			revision.refuser("La dévalidation ne peut se faire que sur document validé, or le document {0} est ''{1}''. Abandon.", document, document.getStatut());

			document.ajouter(revision);

			this.documentService.sauvegarder(document);

			return;
		}

		revision.accepter();

		// Recuperation de la demande de validation
		final var demande = this.documentService.recupererDerniereRevisionEditable(document).getVersion();

		final var result = demande.invalider(audit);

		// Téléchargement
		this.documentService.telecharger(result);

		result.ajouter(revision);

		this.documentService.sauvegarder(result);

		// Notification de l'invalidation
		this.messageDocumentService.notifier(
				InvalidationDocumentMessage.with()
						.document(result)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet de demander la validation d'un document
	 *
	 * @param revision la révision du document
	 */
	protected void demanderValidation(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Demande de validation du document {}.", document);

		if (!document.estBrouillon()) {
			revision.refuser("La soumission ne peut se faire que sur un brouillon, or le document {0} est ''{1}''. Abandon.", document, document.getStatut());

			return;
		}

		revision.accepter();

		final var result = document.demanderValidation(audit);

		result.ajouter(revision);

		// Téléchargement
		this.documentService.telecharger(result);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de la soumission
		this.messageDocumentService.notifier(
				SoumissionDocumentMessage.with()
						.document(result)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);

	}

	/**
	 * Permet de gérer la sauvegarde du document, avec tests d'intégrité
	 *
	 * @param revision la révision du document
	 */
	protected void sauvegarderEtFerme(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		log.debug("Sauvegarde et fermeture du document {}.", document);

		revision.accepter();

		final Document result;

		if (document.estValide()) {
			log.warn("Impossible de modifier un document validé, or le document {} est '{}'. Ici, on ferme juste le document.", document, document.getStatut());

			result = document.fermerSansModification(audit);
		} else {
			result = document.fermerAvecModification(audit);
		}

		result.ajouter(revision);

		// Téléchargement
		this.documentService.telecharger(result);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de la fermeture avec sauvegarde
		this.messageDocumentService.notifier(
				FermetureDocumentMessage.with()
						.document(result)
						.enregistrement(true)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}

	/**
	 * Permet de gérer la sauvegarde du document
	 *
	 * @param revision la révision du document
	 */
	protected void sauvegarderBrouillon(final Revision revision, final UpdateAudit audit) {
		final var document = revision.getDocument();
		final var contexte = document.getContexte();

		final Document result;

		log.debug("Demande d'enregistrement du document {}.", document);

		if (document.estValide()) {
			revision.refuser("Impossible de modifier un document validé, or le document {0} est ''{1}''. Abandon.", document, document.getStatut());

			document.ajouter(revision);

			// Sauvegarde
			this.documentService.sauvegarder(document);

			return;
		}

		revision.accepter();

		if (document.estEnDemandeDeValidation()) {
			// Si on reçoit une demande de sauvegarde sur un document en demande de validation
			// On supprime la soumission du document
			audit.setCommentaire("Annulation automatique de la demande de validation (document en attente de validation ayant fait l'objet d'une modification dans l'éditeur en ligne)");

			result = document.refuserDemandeValidation(audit);
		} else {
			result = document.enregistre(audit);
		}

		result.ajouter(revision);

		// Téléchargement
		this.documentService.telecharger(result);

		// Sauvegarde
		this.documentService.sauvegarder(result);

		// Notification de la sauvegarde
		this.messageDocumentService.notifier(
				SauvegardeDocumentMessage.with()
						.document(result)
						.contexte(contexte)
						.utilisateur(audit.getUtilisateur())
						.build()
		);
	}
}
