package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.docgen.DocgenConnector;
import com.atexo.redaction.domaines.document.connectors.docgen.views.SessionDocgen;
import com.atexo.redaction.domaines.document.model.Jeton;
import com.atexo.redaction.domaines.document.model.Session;
import com.atexo.redaction.domaines.document.services.EditionService;
import com.atexo.redaction.domaines.document.services.FichierService;
import com.atexo.redaction.domaines.document.services.StockageTemporaireService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class EditionRestService implements EditionService {

	@NonNull
	private final StockageTemporaireService stockageTemporaireService;

	@NonNull
	private final DocgenConnector docgen;

	@NonNull
	private final FichierService fichierService;

	@NonNull
	private final Convertible<Session, SessionDocgen> sessionMapper;

	@Override
	@LogTime("Envoi du document canevas a docgen pour édition")
	public Jeton editer(final String data, final Resource document, final Session session) {
		final var xml = stockageTemporaireService.creer(data, MimeTypeUtils.TEXT_XML);

		Jeton result;

		try {
			result = docgen.editer(xml, document, sessionMapper.map(session));
		} finally {
			fichierService.supprimer(xml);
		}

		return result;
	}

	@Override
	@LogTime("Envoi du document simple a docgen pour édition")
	public Jeton editer(final Resource document, final Session session) {
		return docgen.editer(document, sessionMapper.map(session));
	}

	@Override
	@LogTime("Téléchargement du document depuis docgen")
	public Optional<Resource> telecharger(final Jeton jeton) {
		return docgen.telecharger(jeton);
	}

	@Override
	@LogTime("Envoi du document canevas a docgen pour validation")
	public Jeton valider(final Resource document, final Session session) {
		return docgen.valider(document, sessionMapper.map(session));
	}
}
