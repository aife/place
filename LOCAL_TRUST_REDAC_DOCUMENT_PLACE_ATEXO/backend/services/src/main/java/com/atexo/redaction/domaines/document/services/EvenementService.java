package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.BaseEvenement;

public interface EvenementService {

	/**
	 * Permet de sauvegarder un évenement
	 *
	 * @param evenement l'évenement à sauvegarder
	 */
	BaseEvenement sauvegarder(BaseEvenement evenement);
}
