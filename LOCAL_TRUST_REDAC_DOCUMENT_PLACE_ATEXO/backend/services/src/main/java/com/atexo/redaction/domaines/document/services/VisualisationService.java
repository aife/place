package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Jeton;
import com.atexo.redaction.domaines.document.model.Session;
import com.atexo.redaction.domaines.document.shared.web.Location;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * Service d'affichage de fichier
 */
@Service
public interface VisualisationService {

	/**
	 * Ouverture du fichier pour em lecture seule
	 *
	 * @param document le document à éditer
	 * @param session la session
	 *
	 * @return le jeton d'authentification
	 */
	Jeton visualiser(final Resource document, final Session session);

	/**
	 * Permet de formater le jeton pour obtenir l'URL de la vue
	 *
	 * @param jeton le jeton
	 *
	 * @return l'URL de la vue
	 */
	Location redirect(final Jeton jeton);
}
