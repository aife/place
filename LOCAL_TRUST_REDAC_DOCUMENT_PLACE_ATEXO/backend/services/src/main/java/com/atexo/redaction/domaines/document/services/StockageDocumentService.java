package com.atexo.redaction.domaines.document.services;

import org.springframework.stereotype.Service;

/**
 * Service de stockage de document
 */
@Service
public interface StockageDocumentService extends StockageService {
}
