package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.OrganismeClausier;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapping;

@org.mapstruct.Mapper(componentModel = "spring")
public interface OrganismeClausierMapper extends Convertible<OrganismeClausier, Organisme> {

	@Override
	@Mapping(target = "payload", ignore = true)
	Organisme map(OrganismeClausier organisme);
}
