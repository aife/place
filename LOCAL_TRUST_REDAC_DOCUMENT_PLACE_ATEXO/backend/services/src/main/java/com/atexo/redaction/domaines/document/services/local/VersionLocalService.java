package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionInvalideException;
import com.atexo.redaction.domaines.document.services.VersionService;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
@NoArgsConstructor(force = true)
@CacheConfig(cacheNames = "versions")
public class VersionLocalService implements VersionService {

	@Value("${version.min:#{null}}")
	private Version minVersion;

	@Value("${version.max:#{null}}")
	private Version maxVersion;

	@Override
	public boolean estCompatible(@NonNull final Version version) {
		if (version.estInvalide()) {
			throw new VersionInvalideException(version);
		}

		return !this.estTropRecente(version) && !this.estTropAncienne(version);
	}

	@Override
	public boolean estTropAncienne(@NonNull final Version version) {
		if (version.estInvalide()) {
			throw new VersionInvalideException(version);
		}

		return null != this.minVersion && version.compareTo(this.minVersion) < 0;
	}

	@Override
	public boolean estTropRecente(@NonNull final Version version) {
		if (version.estInvalide()) {
			throw new VersionInvalideException(version);
		}

		return null != this.maxVersion && version.compareTo(this.maxVersion) > 0;
	}

	@PostConstruct
	private void echo() {
		log.info("Versions compatibles = [{},{}]", this.minVersion, this.maxVersion);
	}
}
