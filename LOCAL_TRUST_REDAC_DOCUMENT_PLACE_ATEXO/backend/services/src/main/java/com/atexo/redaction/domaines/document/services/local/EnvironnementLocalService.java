package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.environnement.EnvironnementIntrouvableException;
import com.atexo.redaction.domaines.document.respositories.EnvironnementRepository;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "environnements")
public class EnvironnementLocalService implements EnvironnementService {

	/**
	 * Client commun
	 */
	private static final String CLIENT_COMMUN = "commun";

	@NonNull
	private final EnvironnementRepository environnementRepository;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Environnement> chercher(final String client, final String plateforme, String mpeUuid) {
		Optional<Environnement> environnement;
		if (mpeUuid != null) {
			environnement = this.environnementRepository.findByMpeUuid(mpeUuid);
			if (environnement.isPresent()) {
				return environnement;
			}
		}
		return this.environnementRepository.findByClientRegexAndPlateformeRegex(client, plateforme);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Environnement> chercher(final UUID uuid) {
		return this.environnementRepository.findByUuid(uuid);
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Environnement recuperer(String client, String plateforme, String mpeUuid) {
		return this.chercher(client, plateforme, mpeUuid).orElseThrow(() -> new EnvironnementIntrouvableException(client, plateforme));
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Environnement recuperer(final UUID uuid) {
		return this.environnementRepository.findByUuid(uuid).orElseThrow(() -> new EnvironnementIntrouvableException(uuid));
	}

	@Override
	@Cacheable
	public Environnement defaut() {
		return this.environnementRepository.findByClientRegexAndPlateformeRegex(CLIENT_COMMUN, "*")
				.orElseThrow(() -> new ContexteIntrouvableException("*", CLIENT_COMMUN));
	}

	@Override
	public Environnement sauvegarder(Environnement environnement) {
		return environnementRepository.save(environnement);
	}
}
