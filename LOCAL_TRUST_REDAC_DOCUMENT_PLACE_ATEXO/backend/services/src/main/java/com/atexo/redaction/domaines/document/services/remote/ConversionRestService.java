package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.docgen.DocgenConnector;
import com.atexo.redaction.domaines.document.services.ConversionService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConversionRestService implements ConversionService {

	@NonNull
	private DocgenConnector docgen;

	@Override
	@LogTime("Envoi du fichier a docgen pour conversion")
	public Resource convertir(Resource fichier, String type) {
		return docgen.convertir(fichier, type);
	}
}
