package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.ProcedureClausier;
import com.atexo.redaction.domaines.document.model.Procedure;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface ProcedureClausierMapper extends Convertible<ProcedureClausier, Procedure> {
}
