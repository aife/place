package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Revision;
import org.springframework.stereotype.Service;

/**
 * Service des révisions
 */
@Service
public interface RevisionService<T extends Revision> {

	/**
	 * Permet d'actualiser un document
	 *
	 * @param revision la révision
	 */
	void appliquer(final T revision);
}
