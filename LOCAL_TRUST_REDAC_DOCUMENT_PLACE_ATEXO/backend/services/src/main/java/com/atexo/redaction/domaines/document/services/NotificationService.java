package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Notification;
import org.springframework.stereotype.Service;

@Service
public interface NotificationService {

	/**
	 * Permet de sauvegarder une notification
	 *
	 * @param notification la notification à sauvegarder
	 */
	Notification sauvegarder(Notification notification);

}
