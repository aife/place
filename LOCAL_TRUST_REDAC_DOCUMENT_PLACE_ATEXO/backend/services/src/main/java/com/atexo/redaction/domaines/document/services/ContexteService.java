package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.*;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Service de génération de fichier
 */
@Service
public interface ContexteService {

	/**
	 * Permet de récupérer la liste des champs de fusion du contexte
	 *
	 * @param contexte le contexte
	 * @param utilisateur l'utilisateur
	 * @param document le document
	 *
	 * @return les champs de fusion
	 */
	Set<ChampFusion> extraire(final Contexte contexte, final Utilisateur utilisateur, final Document document);

	/**
	 * Permet de trouver un contexte depuis la reference de l'objet metier
	 *
	 * @param environnement l'environnement
	 * @param reference la reference de l'objet metier
	 *
	 * @return le contexte
	 */
	Optional<Contexte> chercher(Environnement environnement, String reference);

	/**
	 * Permet de trouver un contexte depuis la reference de l'objet metier
	 *
	 * @param id l'id du contexte
	 *
	 * @return le contexte, optionnel
	 */
	Optional<Contexte> chercher(Long id);

	/**
	 * Permet de trouver un contexte depuis son uuid
	 *
	 * @param uuid l'uuid du contexte
	 *
	 * @return le contexte, optionnel
	 */
	Optional<Contexte> chercher(UUID uuid);

	/**
	 * Permet de trouver un contexte depuis son uuid
	 *
	 * @param uuid l'uuid du contexte
	 *
	 * @return le contexte
	 */
	Contexte recuperer(UUID uuid);

	/**
	 * Permet de trouver un contexte depuis la reference de l'objet metier
	 *
	 * @param contexte le contexte
	 *
	 * @return le contexte sauvegardé
	 */
	Contexte sauvegarder(Contexte contexte);

	/**
	 * Permet d'actualiser un contexte
	 *
	 * @param contexte le contexte
	 * @param utilisateur l'utilisateur
	 */
	void actualiser(Contexte contexte, Utilisateur utilisateur);

	/**
	 * Permet de savoir si le contexte est en mode pooling ou pas
	 *
	 * @param contexte le contexte
	 *
	 * @return vrai si pooling
	 */
	boolean estEnModePooling(Contexte contexte);
}
