package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.ChampFusion;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service de génération de fichier
 */
@Service
public interface GenerationService {

	/**
	 * Permet de fusionner un gabarit avec les champs de fusion
	 *
	 * @param gabarit le gabarit
	 * @param contexte le contexte (liste des champs de fusion)
	 *
	 * @return le document
	 */
	Resource fusionner(final Resource gabarit, final Set<ChampFusion> contexte);

	/**
	 * Permet de fusionner un gabarit avec le document xml
	 *
	 * @param data les données XML
	 * @param gabarit le gabarit
	 * @param contexte le contexte (liste des champs de fusion)
	 *
	 * @return le document
	 */
	Resource fusionner(String data, Resource gabarit, Set<ChampFusion> contexte);
}
