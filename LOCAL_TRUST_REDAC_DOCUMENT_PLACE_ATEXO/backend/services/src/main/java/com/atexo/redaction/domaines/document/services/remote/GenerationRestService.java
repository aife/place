package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.docgen.DocgenConnector;
import com.atexo.redaction.domaines.document.model.ChampFusion;
import com.atexo.redaction.domaines.document.services.FichierService;
import com.atexo.redaction.domaines.document.services.GenerationService;
import com.atexo.redaction.domaines.document.services.StockageTemporaireService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;

import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class GenerationRestService implements GenerationService {

	@NonNull
	private final DocgenConnector docgen;

	@NonNull
	private final FichierService fichierService;

	@NonNull
	private final StockageTemporaireService stackingService;

	@Override
	@LogTime("Envoi du document simple à docgen pour fusion")
	public Resource fusionner(final Resource gabarit, final Set<ChampFusion> contexte) {
		return docgen.fusionner(gabarit, contexte);
	}

	@Override
	@LogTime("Envoi du document canevas à docgen pour fusion")
	public Resource fusionner(final String data, final Resource gabarit, final Set<ChampFusion> contexte) {
		final var xml = stackingService.creer(data, MimeTypeUtils.TEXT_XML);

		ByteArrayResource result;

		try {
			result = docgen.fusionner(xml, gabarit, contexte);
		} finally {
			fichierService.supprimer(xml);
		}

		return result;
	}

}
