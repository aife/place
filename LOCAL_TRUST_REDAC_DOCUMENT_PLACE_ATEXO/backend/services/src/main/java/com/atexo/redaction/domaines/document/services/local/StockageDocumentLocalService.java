package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.services.FichierService;
import com.atexo.redaction.domaines.document.services.StockageDocumentService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class StockageDocumentLocalService implements StockageDocumentService {

	@NonNull
	private final FichierService fileService;

	@Value("${document.documents.dir}")
	private Path repository;

	@Override
	public File sauvegarder(final Resource resource, final UUID uuid) {
		Validate.notNull(uuid, "L'uuid du gabarit doit etre fourni");
		return fileService.copier(resource, repository, uuid.toString());
	}

	@Override
	public File copier(final Path origine, final UUID uuid) {
		Validate.notNull(uuid, "L'uuid du gabarit doit etre fourni");
		return fileService.copier(origine, repository, uuid.toString());
	}
}
