package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.api.referentiel.TypeModeleIntrouvableException;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.respositories.referentiel.TypeModeleRepository;
import com.atexo.redaction.domaines.document.services.TypeModeleService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "type-modele")
public class TypeModeleLocalService implements TypeModeleService {

	@NonNull
	private final TypeModeleRepository repository;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<TypeModele> chercher(String code, Environnement environnement) {
		return repository.findByCodeAndEnvironnementsContains(code, environnement);
	}

	@Override
	@Cacheable(condition = "#result != null")
	public TypeModele recuperer(String code, Environnement environnement) {
		return this.chercher(code, environnement).orElseThrow(() -> new TypeModeleIntrouvableException(code));
	}
}
