package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ConsultationIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DemandeValidationIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.ValidationIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.io.*;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationType;
import com.atexo.redaction.domaines.document.respositories.DocumentRepository;
import com.atexo.redaction.domaines.document.services.*;
import com.atexo.redaction.domaines.document.services.audit.DocumentAuditQuery;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import com.atexo.redaction.domaines.document.shared.helpers.ListHelper;
import com.atexo.redaction.domaines.document.shared.helpers.XmlHelper;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.envers.query.AuditEntity;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.JoinType;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "documents")
@Transactional
public class DocumentLocalService implements DocumentService {

	@PersistenceContext
	private EntityManager entityManager;

	@NonNull
	private final DocumentRepository repository;

	@NonNull
	private final StockageTemporaireService stackingService;

	@NonNull
	private final StockageDocumentService storageService;

	@NonNull
	private final FichierService fileService;

	@NonNull
	private final MessageService messageService;

	@NonNull
	private final StockageDocumentService stockagePersistentService;

	@NonNull
	private final MessageDocumentService messageDocumentService;

	@NonNull
	private final SerialisationService serialisationService;

	@NonNull
	private final ClausierService clausierService;

	@NonNull
	private final NotificationService notificationService;

	@NonNull
	private EditionService editionService;

	@Override
	public Optional<Document> chercher(final Long id) {
		return this.repository.findById(id);
	}

	@Override
	public boolean existe(final Long id) {
		return this.repository.existsById(id);
	}

	@Override
	public Document recuperer(final Long id) {
		return this.chercher(id).orElseThrow(() -> new DocumentIntrouvableException(id));
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Revision<Document>> rechercherRevision(final UUID uuid) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.property("uuid").eq(uuid))
				.orderBy(AuditEntity.revisionNumber().desc())
				.first();

		Revision<Document> result = null;

		try {
			result = query.getSingleResult();
		} catch (final NoResultException nre) {
			log.error("L'identifiant '{}' n'a pas été trouvé", uuid, nre);
		} catch (final NonUniqueResultException nure) {
			log.error("L'identifiant '{}' existe plusieurs fois", uuid, nure);
		}

		return Optional.ofNullable(result);
	}

	@Override
	public List<Revision<Document>> recupererRevisions(final Document document) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.orderBy(AuditEntity.revisionNumber().desc());

		List<Revision<Document>> result = new ArrayList<>();

		try {
			result = query.getResultList();
		} catch (final NoResultException nre) {
			log.debug("Le document {} n'a pas de révision", document, nre);
		}

		return result;
	}

	@Override
	public Optional<Revision<Document>> rechercherDerniereRevision(final Document document) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.orderBy(AuditEntity.revisionNumber().desc())
				.first();

		Revision<Document> result = null;

		try {
			result = query.getSingleResult();
		} catch (final NoResultException nre) {
			log.debug("Le document {} n'a pas de révision qui correspond aux critères", document, nre);
		}

		return Optional.ofNullable(result);
	}

	@Override
	public Optional<Revision<Document>> rechercherDerniereRevision(final Document document, final Statut statut) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.and(AuditEntity.property("statut").eq(statut))
				.orderBy(AuditEntity.revisionNumber().desc())
				.first();

		Revision<Document> result = null;

		try {
			result = query.getSingleResult();
		} catch (final NoResultException nre) {
			log.debug("Le document {} n'a pas de révision qui correspond aux critères", document, nre);
		}

		return Optional.ofNullable(result);
	}

	@Override
	public Optional<Revision<Document>> rechercherDerniereRevision(final Document document, final Action action) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.and(AuditEntity.property("action").eq(action))
				.orderBy(AuditEntity.revisionNumber().desc())
				.first();

		Revision<Document> result = null;

		try {
			result = query.getSingleResult();
		} catch (final NoResultException nre) {
			log.debug("Le document {} n'a pas de révision qui correspond aux critères", document, nre);
		}

		return Optional.ofNullable(result);
	}

	@Override
	public Revision<Document> recupererDerniereRevisionEditable(final Document document) {
		final Revision<Document> result;
		Document original = this.recupererDocumentOriginal(document);
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.and(AuditEntity.property("extension").eq(original.getExtension()))
				.orderBy(AuditEntity.revisionNumber().desc())
				.first();

		if (document.estEnDemandeDeValidation()) {
			log.info("Le document {} est en demande de validation. Récupération de la version antérieure à la demande", document);

			final var demande = this.rechercherDerniereRevision(document, Action.DEMANDE_VALIDATION).orElseThrow(() -> new DemandeValidationIntrouvableException(document));

			log.debug("La dernière demande de validation pour le document {} porte la révision {}", document, demande.getNumero());

			query.where(AuditEntity.revisionNumber().lt(demande.getNumero()));

			try {
				result = query.getSingleResult();
			} catch (final NoResultException nre) {
				throw new DemandeValidationIntrouvableException(document);
			}

			log.debug("La revision précédente a la demande de validation pour le document {} est {}", document, result.getNumero());
		} else if (document.estValide()) {
			log.info("Le document {} est validé. Récupération de la version antérieure à la demande de validation ou a la validation", document);

			final var demande = this.rechercherDerniereRevision(document, Action.DEMANDE_VALIDATION)
					.map(
							modification -> {
								log.debug("La dernière demande de validation pour le document {} porte la révision {}", document, modification.getNumero());

								return modification;
							})
					.orElseGet(
							() -> {
								final var validation = this.rechercherDerniereRevision(document, Action.VALIDATION).orElseThrow(() -> new ValidationIntrouvableException(document));

								log.debug("Pas de demande de validation pour le document {}. La validation porte la révision {}", document, validation.getNumero());

								return validation;
							}
					);

			query.where(AuditEntity.revisionNumber().lt(demande.getNumero()));

			try {
				result = query.getSingleResult();
			} catch (final NoResultException nre) {
				throw new ValidationIntrouvableException(document);
			}

			log.debug("La revision précédente a la validation pour le document {} est {}", document, result.getNumero());
		} else {
			result = query.getSingleResult();

			log.debug("La revision courante pour le document {} est {}", document, result.getNumero());
		}

		return result;
	}

	@Override
	public Document recupererDocumentOriginal(Document document) {
		final var query = new DocumentAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(document.getId()))
				.orderBy(AuditEntity.revisionNumber().asc())
				.first();
		return query.getSingleResult().getVersion();
	}

	@Override
	public Document dupliquer(final Document document, final Utilisateur utilisateur, final Contexte contexte) {
		final var duplicata = this.recupererDerniereRevisionEditable(document).getVersion().dupliquer(contexte, CreateAudit.with().utilisateur(utilisateur).build(), UpdateAudit.with().utilisateur(utilisateur).build());

		if (document.estEnregistre()) {
			duplicata.setChemin(this.stockagePersistentService.copier(duplicata.getEmplacement(), duplicata.getUuid()).toString());
		}
		if (document.getCanevas() != null) {
			duplicata.setExtension(TypeDocument.DOCX.getExtension());
		}

		return duplicata;
	}

	@Override
	@Cacheable
	public Set<Document> filtrer(final Contexte contexte) {
		return this.repository.findAllByContexteIdOrderByIdDesc(contexte.getId());
	}

	@Override
	@Cacheable
	public Set<Document> filtrer(final Contexte contexte, final Statut statut) {
		return this.repository.findAllByContexteIdAndStatutOrderByIdDesc(contexte.getId(), statut);
	}

	@Override
	public List<Document> rechercher(final Environnement environnement, final Contexte contexte, final Collection<String> expressions, final Collection<Categorie> categories, final Collection<Statut> statuts) {
		return this.repository.findAll(environnement, contexte, ListHelper.securiser(ListHelper.serialiser(expressions)), ListHelper.toString(categories), ListHelper.toString(statuts));
	}

	@Override
	@CacheEvict(allEntries = true)
	public Document sauvegarder(final Document document) {
		return this.repository.save(document);
	}

	@Override
	@CacheEvict(allEntries = true)
	public Document enregistrer(final Document document) {
		return this.repository.saveAndFlush(document);
	}

	@Override
	@CacheEvict(allEntries = true)
	public List<Document> sauvegarder(final Set<Document> documents) {
		return this.repository.saveAll(documents);
	}

	@Override
	@CacheEvict(allEntries = true)
	public void supprimer(final Document document) {
		this.repository.delete(document);
	}

	@Override
	public Double extraireProgression(final Document document) {
		return XmlHelper.extraire(document.getXml(), "//document/@progress").map(Double::valueOf).orElse(null);
	}

	@Override
	public String extraireCommentaire(final Document document) {
		return XmlHelper.extraire(document.getXml(), "//document/@commentaire").orElse(null);
	}

	@Override
	public String extraireFormat(final Document document) {
		return XmlHelper.extraire(document.getXml(), "//document/@outputFormat").orElse(null);
	}

	@Override
	public String format(final Document document) {
		final var salt = UUID.randomUUID();
		return MessageFormatter.format("{}@{}", document.getId(), salt).getMessage();
	}

	@Override
	public Long unformat(final String id) {
		Long result = null;
		final var matcher = Pattern.compile("(?<id>.*)@(.*)").matcher(id);

		if (matcher.matches()) {
			result = Long.valueOf(matcher.group("id"));
		}

		return result;
	}

	@Override
	public void unzip(final Document document, final Resource fichier) {
		// Dans le cas d'un document canevas, on récupère un zip
		final var zip = this.stackingService.sauvegarder(fichier);

		log.trace("Récupération du \uD83D\uDCE6️ zip et dépôt à l'adresse {}", zip);

		// Dézippage
		final Set<File> files;
		try {
			files = this.stackingService.unzip(zip);
		} catch (final IOException e) {
			throw new ArchiveCorrompueException(document, zip);
		}

		if (files.isEmpty()) {
			throw new ArchiveVideException(document, zip);
		}

		if (files.size() < 2) {
			throw new ArchiveIncoherenteException(document, zip);
		}

		// Récupération du XML
		final var xml = files.stream().filter(file -> this.fileService.possedeTypeMime(file, "xml"))
				.findFirst().orElseThrow(() -> new XmlAbsentException(document, zip));

		document.setXml(this.fileService.extraire(xml, StandardCharsets.UTF_8));

		log.info("Dézippage du XML {} et stockage dans le document", xml);

		// Progression
		final var progression = this.extraireProgression(document);

		log.trace("Progression du document = {}", progression);

		if (null != progression && !progression.isNaN()) {
			document.setProgression(progression);
		}

		// Commentaire
		final var commentaire = this.extraireCommentaire(document);

		log.trace("Commentaire du document = {}", commentaire);

		if (null != commentaire) {
			document.getModification().setCommentaire(commentaire);
		}

		final File version;

		if (document.getAction() == Action.VALIDATION && document.getStatut() == Statut.VALIDE) {
			final var format = this.extraireFormat(document);

			if (null == format) {
				throw new XmlFormatInconnuException(document, zip, xml);
			} else {
				document.setExtension(format);

				log.info("Format de sortie  = {}", format);
			}

			final var extension = "validate_" + format;

			log.info("Récupération du fichier avec extension '{}'", extension);

			// Récupération du docx dans le répertoire temporaire dézippé
			version = files.stream().filter(file -> this.fileService.possedeTypeMime(file, extension))
					.findFirst().orElseThrow(() -> new ArchiveIncompleteException(document, zip, extension));

		} else {
			final var extensions = Set.of("docx", "xlsx");

			log.info("Récupération du fichier avec extensions '{}'", extensions);
			// Récupération du docx dans le répertoire temporaire dézippé
			version = files.stream().filter(file -> this.fileService.possedeTypeMime(file, extensions))
					.findFirst().orElseThrow(() -> new ArchiveIncompleteException(document, zip, extensions));
		}

		// Sauvegarde du doc dans le répertoire définitif
		final var destination = this.storageService.sauvegarder(new FileSystemResource(version), document.getUuid());

		// Modification de l'objet métier
		document.setChemin(destination.toString());

		log.trace("Dézippage du document docx/xlsx à l'adresse {}", destination);
	}

	@Override
	@CacheEvict(allEntries = true)
	public void signaler(final Document document, final String message, final Contexte contexte, final Utilisateur utilisateur) {
		// Derniere version (valide) du document
		final var derniereVersionValide = this.chercher(document.getId()).orElseThrow(() -> new DocumentIntrouvableException(document.getId()));

		this.entityManager.detach(derniereVersionValide);

		// Création de la version en erreur
		final var signalement = document.signaler(UpdateAudit.with().utilisateur(utilisateur).commentaire(message).build());

		// Sauvegarde de l'erreur
		this.sauvegarder(signalement);

		// ... et on rollback avec commentaire
		final var result = derniereVersionValide.annuler(UpdateAudit.with().utilisateur(utilisateur).commentaire("Suite à l'erreur constatée, le document est rétabli dans sa version sans erreur la plus récente").build());

		result.setOuvert(signalement.isOuvert());

		// Sauvegarde du retour en arriere
		final var sauvegarde = this.sauvegarder(result);

		this.messageService.notifier(
				contexte,
				NotificationMessage.with()
						.titre("Erreur sur le {0} <strong>{1}</strong>", StringUtils.uncapitalize(sauvegarde.getModele().getType().getLibelle()), sauvegarde.getNom())
						.contenu(message)
						.type(NotificationType.ERREUR)
						.build()
		);
	}

	@Override
	public String getExtension(final Document document) {
		return FilenameUtils.getExtension(document.getModele().getNomFichier());
	}

	@Override
	public Notification notifier(final Document document, final Set<Destinataire> destinataires, final Utilisateur createur, final Contexte contexte) {
		final var consultation = this.clausierService.recuperer(contexte).orElseThrow(() -> new ConsultationIntrouvableException(contexte));

		final var commentaire = MessageFormat.format("Document notifié aux destinaires suivants : {0}", destinataires.stream().map(Destinataire::toString).collect(Collectors.joining(",")));

		// Sauvegarde du document à l'etat notifié
		final var duplicate = this.sauvegarder(document.notifier(UpdateAudit.with().utilisateur(createur).commentaire(commentaire).build()));
		String urlVisualisationConsultation = "";
		if (contexte.getEnvironnement() != null && contexte.getEnvironnement().getMpeUrl() != null && !contexte.getEnvironnement().getMpeUrl().isEmpty() && consultation.getNumero() != null) {
			urlVisualisationConsultation = contexte.getEnvironnement().getMpeUrl() + "/agent/consultation/recherche?keyWord=" + consultation.getNumero();
			log.info("URL de visualisation de consultation {}", urlVisualisationConsultation);
		}
		else {
			log.warn("Données insuffisantes pour construire l'URL de visualisation de consultation dans le contexte '{}'", contexte.getUuid());
		}


		final var builder = Notification.with()
				.document(duplicate)
				.lien(new Location(urlVisualisationConsultation))
				.creePar(createur)
				.destinataires(destinataires);

		final var result = this.notificationService.sauvegarder(builder.build());

		result.setConsultation(consultation);

		return result;
	}

	/**
	 * Permet de télécharger le zip du document
	 */
	public void telecharger(@NonNull final Document document) {
		log.info("Demande de téléchargement de l'archive de la version pour le document {}", document);
		// Récupération du zip
		final var fichier = this.editionService.telecharger(new Jeton(document.getJeton()));

		fichier.ifPresentOrElse(
				(result) -> log.info("Le fichier pour le jeton '{}' a correctement été télécharger", document.getJeton()),
				() -> log.error("Le fichier n'a pas été récupéré"));

		this.unzip(document, fichier.orElseThrow(() -> new ArchiveInexistanteException(document)));
	}
}
