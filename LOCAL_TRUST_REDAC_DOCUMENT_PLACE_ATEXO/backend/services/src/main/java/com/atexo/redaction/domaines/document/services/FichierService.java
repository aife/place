package com.atexo.redaction.domaines.document.services;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;

/**
 * Service de maipulation de fichier
 */
@Service
public interface FichierService {

	/**
	 * Permet d'extraire le contenu d'un fichier
	 *
	 * @param fichier le fichier
	 *
	 * @return le contenu
	 */
	String extraire(final File fichier, final Charset charset);

	/**
	 * Permet de récupérer l'extension d'un fichier
	 *
	 * @param fichier le fichier
	 *
	 * @return l'extension
	 */
	String getExtension(final File fichier);

	/**
	 * Permet de vérifier si un fichier possède l'une des extensions en paramètre
	 *
	 * @param fichier le fichier
	 * @param extensions les extensions à garder
	 */
	boolean possedeTypeMime(final File fichier, final String... extensions);


	/**
	 * Permet de vérifier si un fichier possède l'une des extensions en paramètre
	 *
	 * @param fichier le fichier
	 * @param extensions les extensions à garder
	 */
	boolean possedeTypeMime(final File fichier, final Iterable<String> extensions);

	/**
	 * Horodatage d'un répertoire
	 *
	 * @param base la base
	 *
	 * @return le fichier horodater
	 */
	Path horodaterRepertoire(final Path base);

	/**
	 * Récupération de l'extension
	 *
	 * @param nom le nom du fichier
	 *
	 * @return le nom du fichier
	 */
	String getExtension(final String nom);

	/**
	 * Permet de copier un fichier
	 *
	 * @param origine le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 *
	 * @return le fichier copié
	 */
	File copier(final Path origine, final Path repertoire, final String nom);

	/**
	 * Permet de copier un fichier
	 *
	 * @param resource le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 *
	 * @return le fichier copié
	 */
	File copier(final Resource resource, final Path repertoire, final String nom);

	/**
	 * Permet de copier un fichier
	 *
	 * @param stream le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 *
	 * @return le fichier copié
	 */
	File copier(final InputStream stream, final Path repertoire, final String nom);

	/**
	 * Permet de copier un fichier
	 *
	 * @param origine le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 * @param horodatage vrai si le répertoire doit être horodaté, faux sinon
	 *
	 * @return le fichier copié
	 */
	File copier(final Path origine, final Path repertoire, final String nom, final boolean horodatage);

	/**
	 * Permet de copier un fichier
	 *
	 * @param resource le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 * @param horodatage vrai si le répertoire doit être horodaté, faux sinon
	 *
	 * @return le fichier copié
	 */
	File copier(final Resource resource, final Path repertoire, final String nom, final boolean horodatage);

	/**
	 * Permet de copier un fichier
	 *
	 * @param stream le fichier d'origine
	 * @param repertoire le répertoire
	 * @param nom le nom du fichier
	 * @param horodatage vrai si le répertoire doit être horodaté, faux sinon
	 *
	 * @return le fichier copié
	 */
	File copier(final InputStream stream, final Path repertoire, final String nom, final boolean horodatage);

	/**
	 * Permet d'obtenir un nom aléatoire
	 *
	 * @return le nom aléatoire
	 */
	String creerNomAleatoire();

	/**
	 * Permet de vérifier le répertoire
	 *
	 * @param repertoire le répertoire
	 *
	 * @return le chemin
	 */
	Path verifierRepertoire(final Path repertoire);

	/**
	 * Permet de créer le répertoire
	 *
	 * @param base la base
	 *
	 * @return le répertoire
	 */
	Path creerRepertoire(final Path base);

	/**
	 * Permet de créer le répertoire
	 *
	 * @param base la base
	 * @param horodatage l'horodatage
	 *
	 * @return le répertoire
	 */
	Path creerRepertoire(final Path base, final boolean horodatage);

	/**
	 * Permet de supprimer le fichier
	 *
	 * @param path le chemin du fichier a supprimer
	 *
	 * @return vrai si le fichier est supprime, faux sinon
	 */
	boolean supprimer(Resource path);
}
