package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public interface OrganismeService {

	/**
	 * Permet de rechercher un organisme
	 *
	 * @param identifiantExterne l'identifiant externe de l'organisme
	 * @param environnement l'environnement
	 *
	 * @return l'organisme
	 */
	Optional<Organisme> chercher(Long identifiantExterne, Environnement environnement);

	/**
	 * Permet de rechercher un organisme
	 *
	 * @param uuid l'uuid
	 *
	 * @return l'organisme
	 */
	Optional<Organisme> chercher(UUID uuid);

	/**
	 * Permet de créer un organisme
	 *
	 * @param organisme l'organisme
	 *
	 * @return l'organisme créé
	 */
	Organisme sauvegarder(Organisme organisme);
}
