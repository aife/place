package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.StatutCanevasClausier;
import com.atexo.redaction.domaines.document.model.StatutCanevas;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(componentModel = "spring")
public interface StatutCanevasClausierMapper extends Convertible<StatutCanevasClausier, StatutCanevas> {
}
