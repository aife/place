package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Topic;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationMessage;
import com.atexo.redaction.domaines.document.services.MessageService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageLocalService implements MessageService {

	@NonNull
	private final SimpMessagingTemplate template;

	@Override
	public <T extends BaseMessage> void notifier(final T message, final String channel) {
		this.template.convertAndSend(channel, message);
	}

	@Override
	public <T extends BaseMessage> void notifier(final T message, final Topic topic, final Object... args) {
		this.template.convertAndSend(MessageFormat.format(topic.getChannel(), args), message);
	}

	@Override
	public void notifier(final Set<Utilisateur> utilisateurs, final NotificationMessage message) {
		utilisateurs.forEach(utilisateur -> this.notifier(utilisateur, message));
	}

	@Override
	public void notifier(final Utilisateur utilisateur, final NotificationMessage message) {
		this.notifier(message, Topic.TOPIC_UTILISATEUR, utilisateur.getUuid());
	}

	@Override
	public void notifier(final Contexte contexte, final NotificationMessage message) {
		this.notifier(message, Topic.TOPIC_CONTEXTE, contexte.getUuid());
	}

	@Override
	public void notifier(final Environnement environnement, final NotificationMessage message) {
		this.notifier(message, Topic.TOPIC_ENVIRONNEMENT, environnement.getUuid());
	}
}
