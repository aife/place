package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Jeton;
import com.atexo.redaction.domaines.document.model.Session;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service d'édition de fichier
 */
@Service
public interface EditionService {

	/**
	 * Ouverture du fichier pour édition
	 *
	 * @param document le document à éditer
	 * @param session la session
	 *
	 * @return le jeton d'authentification
	 */
	Jeton editer(final Resource document, final Session session);

	/**
	 * Ouverture du fichier pour édition
	 *
	 * @param data les données du document au format xml
	 * @param document le document à éditer
	 * @param session la session
	 *
	 * @return le jeton d'authentification
	 */
	Jeton editer(final String data, final Resource document, final Session session);

	/**
	 * Permet de télécharger le fichier
	 *
	 * @param jeton le jeton
	 *
	 * @return l'édition
	 */
	Optional<Resource> telecharger(final Jeton jeton);

	/**
	 * Permet de demander la validation d'un document
	 *
	 * @param document le document
	 * @param session la session
	 *
	 * @return jeton
	 */
	Jeton valider(final Resource document, final Session session);
}
