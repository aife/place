package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.ConsultationClausier;
import com.atexo.redaction.domaines.document.model.Consultation;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;

@org.mapstruct.Mapper(uses = {
		LotClausierMapper.class,
		OrganismeClausierMapper.class,
		ServiceClausierMapper.class,
		ProcedureClausierMapper.class
})
public interface ConsultationClausierMapper extends Convertible<ConsultationClausier, Consultation> {
}
