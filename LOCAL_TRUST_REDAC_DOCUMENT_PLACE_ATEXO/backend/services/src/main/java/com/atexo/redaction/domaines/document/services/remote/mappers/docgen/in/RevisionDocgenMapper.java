package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.in;

import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionDocgen;
import com.atexo.redaction.domaines.document.model.Revision;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

public abstract class RevisionDocgenMapper<T extends Revision> implements Convertible<RevisionDocgen, T> {

	@Override
	@Mapping(source = "revision", target = "payload", qualifiedByName = "revisionToPayloadMapper")
	@Mapping(source = "dateCreation", target = "dateEnregistrement")
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "etape", ignore = true)
	@Mapping(target = "commentaire", ignore = true)
	public abstract T map(RevisionDocgen revision);

	@Named("revisionToPayloadMapper")
	public Object revisionToPayloadMapper(RevisionDocgen revisionDTO) {
		return revisionDTO;
	}

}
