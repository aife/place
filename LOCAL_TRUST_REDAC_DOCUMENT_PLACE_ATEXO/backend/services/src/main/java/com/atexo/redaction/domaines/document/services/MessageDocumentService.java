package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.messages.documents.*;

public interface MessageDocumentService {

	/**
	 * Permet d'envoyer une notification de la création du document
	 *
	 * @param message le message de création du document
	 */
	void notifier(CreationDocumentMessage message);

	/**
	 * Permet d'envoyer une notification du téléchargement du document
	 *
	 * @param message le message de téléchargement du document
	 */
	void notifier(TelechargementDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de la fermeture du document
	 *
	 * @param message le message de fermeture du document
	 */
	void notifier(FermetureDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de l'insoumission du document
	 *
	 * @param message le message d'insoumission du document
	 */
	void notifier(InsoumissionDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de l'invalidation du document
	 *
	 * @param message le message d'invalidation du document
	 */
	void notifier(InvalidationDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de l'ouverture du document
	 *
	 * @param message le message d'ouverture du document
	 */
	void notifier(OuvertureDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de soumission du document
	 *
	 * @param message le message de soumission du document
	 */
	void notifier(SoumissionDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de suppression du document
	 *
	 * @param message le message de suppression du document
	 */
	void notifier(SuppressionDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de validation du document
	 *
	 * @param message le message de validation du document
	 */
	void notifier(ValidationDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de sauvegarde du document
	 *
	 * @param message le message de validation du document
	 */
	void notifier(SauvegardeDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de duplication du document
	 *
	 * @param message le message de duplication du document
	 */
	void notifier(DuplicationDocumentMessage message);

	/**
	 * Permet d'envoyer une notification de modification du document
	 *
	 * @param message le message de modification du document
	 */
	void notifier(ModificationDocumentMessage message);
}
