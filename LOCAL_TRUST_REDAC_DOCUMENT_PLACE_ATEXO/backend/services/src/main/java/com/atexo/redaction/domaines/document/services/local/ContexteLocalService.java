package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.io.BaseTelechargementException;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.model.revisions.PoolingRevision;
import com.atexo.redaction.domaines.document.respositories.ContexteRepository;
import com.atexo.redaction.domaines.document.services.*;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "contextes")
public class ContexteLocalService implements ContexteService {

	private static final String POOLING = "pooling";

	private static final String DOCGEN_MODE = "docgen.mode";

	private static final String FORMAT_FRANCAIS_DATETIME = "dd/MM/yyyy - HH:mm";

	@NonNull
	private final SerialisationService serialisationService;

	@NonNull
	private final ContexteRepository repository;

	@NonNull
	private PlateformeService plateformeService;

	@NonNull
	private final MonitoringService monitoringService;

	@NonNull
	private final RevisionService<PoolingRevision> revisionService;

	@NonNull
	private final DocumentService documentService;

	@NonNull
	private final ParametreService parametreService;

	@Override
	@Cacheable
	public Set<ChampFusion> extraire(final Contexte contexte, final Utilisateur utilisateur, final Document document) {
		final var result = new HashSet<ChampFusion>();

		final var consultation = this.serialisationService.deserialise(contexte.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Contexte.class)
				.map(com.atexo.redaction.domaines.document.mpe.v2.Contexte::getConsultation);

		result.add(new ChampFusion("DateLimite",
				consultation
						.map(com.atexo.redaction.domaines.document.mpe.v2.Consultation::getDateRemisePlis)
						.map(dateRemisePlis -> new SimpleDateFormat(FORMAT_FRANCAIS_DATETIME).format(dateRemisePlis.toGregorianCalendar().getTime()))
						.orElse(""))
		);

		result.add(new ChampFusion("IntituleConsultation",
				consultation
						.map(com.atexo.redaction.domaines.document.mpe.v2.Consultation::getIntitule)
						.orElse(""))
		);

		result.add(new ChampFusion("ProcedureConsultation",
				consultation
						.map(com.atexo.redaction.domaines.document.mpe.v2.Consultation::getProcedure)
						.map(com.atexo.redaction.domaines.document.mpe.v2.ProcedureType::getLibelleLong)
						.orElse(""))
		);

		result.add(new ChampFusion("NumeroConsultation",
				consultation
						.map(com.atexo.redaction.domaines.document.mpe.v2.Consultation::getNumeroConsultation)
						.orElse(""))
		);

		result.add(new ChampFusion("ObjetConsultation",
				consultation
						.map(com.atexo.redaction.domaines.document.mpe.v2.Consultation::getObjet)
						.orElse(""))
		);

		final var agent = this.serialisationService.deserialise(utilisateur.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Agent.class);

		result.add(new ChampFusion("PouvoirAdjudicateur",
				agent
						.map(com.atexo.redaction.domaines.document.mpe.v2.Agent::getOrganisme)
						.map(com.atexo.redaction.domaines.document.mpe.v2.Organisme::getLibelleLong)
						.orElse(""))
		);

		result.add(new ChampFusion("DirectionService",
				agent
						.map(com.atexo.redaction.domaines.document.mpe.v2.Agent::getService)
						.map(com.atexo.redaction.domaines.document.mpe.v2.Service::getLibelleLong)
						.orElse(""))
		);

		final var optionalDocument = Optional.ofNullable(document);

		result.add(new ChampFusion("TypeDocument",
				optionalDocument
						.map(Document::getModele)
						.map(Modele::getType)
						.map(TypeModele::getLibelle)
						.orElse(""))
		);

		result.add(new ChampFusion("NomDocument",
				optionalDocument
						.map(Document::getNom)
						.orElse(""))
		);

		return result;
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Contexte> chercher(final Environnement environnement, final String reference) {
		return this.repository.findByEnvironnementAndReference(environnement, reference);
	}

	@Override
	@Cacheable(condition = "#result != null && result.isPresent()")
	public Optional<Contexte> chercher(final Long id) {
		return this.repository.findById(id);
	}

	@Override
	public Optional<Contexte> chercher(final UUID uuid) {
		return this.repository.findByUuid(uuid);
	}

	@Override
	public Contexte recuperer(final UUID uuid) {
		return this.repository.findByUuid(uuid).orElseThrow(() -> new ContexteIntrouvableException(uuid));
	}

	@Override
	@CacheEvict(allEntries = true)
	public Contexte sauvegarder(final Contexte contexte) {
		return this.repository.save(contexte);
	}

	@Override
	public void actualiser(@NonNull final Contexte contexte, final Utilisateur utilisateur) {
		final boolean pooling = this.estEnModePooling(contexte);

		if (pooling) {
			final var plateforme = this.plateformeService.recuperer(contexte);

			// Rafraichissement des documnets
			final var evenements = this.monitoringService.verifier(plateforme);

			evenements.forEach(
					evenement -> {
						try {
							this.revisionService.appliquer(evenement);
						} catch (final BaseTelechargementException exception) {
							log.error("Erreur lors de la communication avec only-office", exception);

							this.documentService.signaler(exception.getDocument(), exception.getMessage(), contexte, utilisateur);
						} catch (final DocumentIntrouvableException die) {
							log.trace("Le document avec l'UUID '{}' n'existe plus", evenement.getDocument());
						}
					}
			);
		}
	}


	@Override
	public boolean estEnModePooling(final Contexte contexte) {
		return POOLING.equalsIgnoreCase(this.parametreService.recuperer(contexte.getEnvironnement(), DOCGEN_MODE));
	}
}
