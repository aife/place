package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Topic;
import com.atexo.redaction.domaines.document.model.messages.documents.*;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationType;
import com.atexo.redaction.domaines.document.services.MessageDocumentService;
import com.atexo.redaction.domaines.document.services.MessageService;
import com.atexo.redaction.domaines.document.services.ws.Connection;
import com.atexo.redaction.domaines.document.services.ws.ConnectionHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageDocumentLocalService implements MessageDocumentService {

	/**
	 * Delai par défaut
	 */
	private static final long DEFAULT_MESSAGE_DELAI = 5000L;

	@NonNull
	private final ConnectionHolder connectionHolder;

	@NonNull
	private final MessageService messageService;

	@Override
	public void notifier(final CreationDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de création du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());

		log.debug("Envoi du message de création aux utilisateurs = {}", this.connectionHolder.getConnections());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Création réussie du {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.SUCCES)
									.build()
					);
				}
		);
	}

	@Override
	public void notifier(final TelechargementDocumentMessage message) {

		final var document = message.getDocument();
		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Téléchargement réussi du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);
	}

	@Override
	public void notifier(final FermetureDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de fermeture du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Fermeture réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);
	}

	@Override
	public void notifier(final InsoumissionDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message d'insoumission du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Insoumission réussi du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);
	}

	@Override
	public void notifier(final InvalidationDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message d'invalidation du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Invalidation réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);


	}

	@Override
	public void notifier(final OuvertureDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message d'ouverture du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Ouverture réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);


	}

	@Override
	public void notifier(final SoumissionDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de soumission du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Soumissione réussi du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.SUCCES)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}

		);


	}

	@Override
	public void notifier(final SuppressionDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = document.getContexte();

		log.debug("Envoi du message de suppression du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Suppression réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.SUCCES)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);


	}

	@Override
	public void notifier(final ValidationDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de validation du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Validation réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.SUCCES)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);


	}

	@Override
	public void notifier(final SauvegardeDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();
		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		log.debug("Envoi du message de validation du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Sauvegarde réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);
	}

	@Override
	public void notifier(final DuplicationDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de duplication du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Duplication réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.SUCCES)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);


	}

	@Override
	public void notifier(final ModificationDocumentMessage message) {
		final var document = message.getDocument();
		final var contexte = message.getContexte();

		log.debug("Envoi du message de modification du document {} au contexte {}", document.getId(), contexte.getId());

		this.messageService.notifier(message, Topic.TOPIC_CONTEXTE_TOUS_DOCUMENTS, contexte.getUuid());
		this.messageService.notifier(message, Topic.TOPIC_DOCUMENT, document.getId());

		final var type = StringUtils.uncapitalize(document.getModele().getType().getLibelle());

		message.getUtilisateur().ifPresent(
				auteur -> {
					// Notification de l'auteur
					this.messageService.notifier(
							auteur,
							NotificationMessage.with()
									.contenu(MessageFormat.format("Modification réussie du document {0} intitulé <strong>{1}</strong>", type, document))
									.type(NotificationType.INFORMATION)
									.delai(DEFAULT_MESSAGE_DELAI)
									.build()
					);
				}
		);
	}
}
