package com.atexo.redaction.domaines.document.services.remote;

import com.atexo.redaction.domaines.document.connectors.clausier.ClausierConnector;
import com.atexo.redaction.domaines.document.connectors.clausier.views.AgentClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.CanevasClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.ConsultationClausier;
import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ConsultationIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteInvalideException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.RechercheConsultationException;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.services.ClausierService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "clausier")
public class ClausierRestService implements ClausierService {

    @NonNull
    private final SerialisationService serialisationService;

    @NonNull
    private ClausierConnector clausierConnector;

    @NonNull
    private final Convertible<CanevasClausier, Canevas> canevasMapper;

    @NonNull
    private final Convertible<AgentClausier, Utilisateur> agentMapper;

    @NonNull
    private final Convertible<ConsultationClausier, Consultation> consultationMapper;

    @Override
    @CachePut
    @LogTime("Création de la trame du document par appel de l'URL d'initialisation du document fourni dans le contexte de l'agent")
    public String creer(final Contexte contexte, final Utilisateur createur, final Canevas canevas, final TypeModele type, final Lot lot) {
        final var payloadContexte = serialisationService.deserialise(contexte.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Contexte.class);
        if (lot != null && lot.getId() == null && lot.getLibelle().equals("Tous les lots")){
            lot.setId(0);
        }
        var uri = payloadContexte
                .map(com.atexo.redaction.domaines.document.mpe.v2.Contexte::getUrls)
                .map(com.atexo.redaction.domaines.document.mpe.v2.Acces::getUrlGenerationCanevas)
                .map(Location::new)
                .map(location -> location.format(contexte.getReference(), canevas.getReference(), createur.getIdentifiantExterne(), type.getCode()))
                .orElseThrow(() -> new ContexteInvalideException("Aucune URL de retour pour la création du clausier du document", contexte));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString());
        if (contexte.getEnvironnement().getMpeUuid() != null || lot != null) {
            if (contexte.getEnvironnement().getMpeUuid() != null) {
                builder.queryParam("plateformeUuid", contexte.getEnvironnement().getMpeUuid());
            }
            if (lot != null) {
                builder.queryParam("lot", lot.getId());
            }

        }

        String encodedUrl = builder.encode().toUriString().replace("+", "%2B");
        return clausierConnector.genererCanevas(new Location(encodedUrl));
        }
        @Override
        @LogTime("Recuperation de la consultation depuis l'URL de récupération de la consultation fourni dans le contexte de l'agent")
        public Optional<Consultation> recuperer ( final Contexte contexte){
            final var payloadContexte = serialisationService.deserialise(contexte.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Contexte.class);

            var uri = payloadContexte
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Contexte::getUrls)
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Acces::getUrlRecuperationConsultation)
                    .map(Location::new)
                    .map(location -> location.format(contexte.getReference()))
                    .orElseThrow(() -> new ContexteInvalideException("Aucune URL de retour pour la récupération de la consultation ''{0}''", contexte));

            if (contexte.getEnvironnement().getMpeUuid() != null) {
                UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString()).queryParam("plateformeUuid", contexte.getEnvironnement().getMpeUuid());
                uri = new Location(builder.toUriString());
            }
            Optional<Consultation> result;

            try {
                result = clausierConnector.rechercherConsultation(uri, contexte).map(this.consultationMapper::map);
            } catch (RechercheConsultationException rce) {
                throw new ConsultationIntrouvableException(contexte);
            }

            return result;
        }

        @Override
        @LogTime("Récupération des canevas depuis l'URL de recherche de canevas fourni dans le contexte de l'agent")
        public Set<Canevas> rechercher ( final Contexte contexte, final TypeModele type, final Integer idLot){
            final var params = new HashMap<String, Serializable>();
            params.put("consultation", contexte.getReference());
            params.put("type", type.getCode());

            if (null != idLot) {
                params.put("lot", idLot);
            }
            if (contexte.getEnvironnement().getMpeUuid() != null) {
                params.put("plateformeUuid", contexte.getEnvironnement().getMpeUuid());
            }

            final var parameters = params.entrySet()
                    .stream()
                    .map(entry -> MessageFormat.format("{0}={1}", entry.getKey(), entry.getValue()))
                    .collect(Collectors.joining("&"));

            final var payloadContexte = serialisationService.deserialise(contexte.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Contexte.class);

            var uri = payloadContexte
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Contexte::getUrls)
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Acces::getUrlRechercheCanevas)
                    .map(Location::new)
                    .map(location -> location.format(parameters))
                    .orElseThrow(() -> new ContexteInvalideException("Aucune URL de retour pour la recherche de canevas", contexte));

            return this.canevasMapper.map(clausierConnector.rechercherListeCanevas(uri));
        }

        @Override
        @LogTime("Récupération des agents depuis l'URL de recherche de agents fourni dans le contexte de l'agent")
        public Set<Utilisateur> rechercher ( final Contexte contexte){
            final var payloadContexte = serialisationService.deserialise(contexte.getPayload(), com.atexo.redaction.domaines.document.mpe.v2.Contexte.class);

            var uri = payloadContexte
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Contexte::getUrls)
                    .map(com.atexo.redaction.domaines.document.mpe.v2.Acces::getUrlRechercheAgents)
                    .map(Location::new)
                    .map(location -> location.format(contexte.getReference()))
                    .orElseThrow(() -> new ContexteInvalideException("Aucune URL de retour pour la récupération des agents de la consultation {}", contexte));
            if (contexte.getEnvironnement().getMpeUuid() != null) {
                UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri.toString()).queryParam("plateformeUuid", contexte.getEnvironnement().getMpeUuid());
                uri = new Location(builder.toUriString());
            }
            return this.agentMapper.map(clausierConnector.rechercherListeUtilisateurs(uri));
        }
    }
