package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface ReferentielService {

	/**
	 * Permet de récupérer tous les référentiels d'un {@code Environnement environnement}
	 *
	 * @param environnement l'environnement
	 *
	 * @return l'ensemble des référentiels
	 */
	Set<Referentiel> filtrer(final Environnement environnement);

}
