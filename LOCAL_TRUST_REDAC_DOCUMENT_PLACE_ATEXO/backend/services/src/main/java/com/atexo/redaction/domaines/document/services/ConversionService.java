package com.atexo.redaction.domaines.document.services;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * Service de génération de fichier
 */
@Service
public interface ConversionService {

	/**
	 * Convertit un fichier en pdf
	 *
	 * @param fichier le fichier à convertir
	 * @param type le type du fichier
	 *
	 * @return le fichier converti
	 */
	Resource convertir(final Resource fichier, final String type);
}
