package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Parametre;

import java.util.Optional;
import java.util.Set;

public interface ParametreService {

	/**
	 * Permet de récuperer le paramètre definit par le clé pour un environnement
	 *
	 * @param environnement l'environnement
	 * @param cle la cle du paramètre
	 */
	Optional<Parametre> chercher(final Environnement environnement, final String cle);

	/**
	 * Permet de récuperer le paramètre definit par la clé pour un environnement
	 *
	 * @param environnement l'environnement
	 * @param cle la cle du paramètre
	 */
	String recuperer(final Environnement environnement, final String cle);

	/**
	 * Permet de récuperer les paramètres pour un environnement
	 *
	 * @param environnement l'environnement
	 */
	Set<Parametre> recuperer(final Environnement environnement);
}
