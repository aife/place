package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Organisme;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public interface ServiceService {

	/**
	 * Permet de chercher le service à partir de son identifiant externe et de son organisme
	 *
	 * @param identifiantExterne l'identifiant externe du service, unique pour l'organisme
	 * @param organisme l'organisme du service
	 *
	 * @return le service, optionel
	 */
	Optional<com.atexo.redaction.domaines.document.model.Service> chercher(Long identifiantExterne, Organisme organisme);

	/**
	 * Permet de chercher le service à partir de son identifiant unique
	 *
	 * @param uuid l'uuid du service, unique
	 *
	 * @return le service, optionel
	 */
	Optional<com.atexo.redaction.domaines.document.model.Service> chercher(UUID uuid);

	/**
	 * Permet de sauvegarder le service
	 *
	 * @param service le service
	 *
	 * @return le service, optionel
	 */
	com.atexo.redaction.domaines.document.model.Service sauvegarder(com.atexo.redaction.domaines.document.model.Service service);
}
