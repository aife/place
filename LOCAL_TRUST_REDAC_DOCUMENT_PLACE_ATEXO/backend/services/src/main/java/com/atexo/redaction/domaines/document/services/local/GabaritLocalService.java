package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.exceptions.api.referentiel.NiveauIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurAbsentException;
import com.atexo.redaction.domaines.document.respositories.GabaritRepository;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import com.atexo.redaction.domaines.document.services.GabaritService;
import com.atexo.redaction.domaines.document.services.NiveauService;
import com.atexo.redaction.domaines.document.services.audit.GabaritAuditQuery;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.atexo.redaction.domaines.document.model.referentiels.Niveau.*;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "surcharges-modeles")
public class GabaritLocalService implements GabaritService {

	@NonNull
	private final EntityManager entityManager;

	@NonNull
	private final GabaritRepository repository;


	@NonNull
	private final NiveauService niveauService;

	@NonNull
	private final EnvironnementService environnementService;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final Modele modele, final Environnement environnement) {
		log.info("Recherche d'une surcharge pour l'environnement {} au niveau {} pour le modele {}", environnement, PLATEFORME, modele);

		return this.repository.findByEnvironnementAndNiveauCodeAndModeleId(environnement, PLATEFORME, modele.getId());
	}

	@Override
	@CacheEvict(allEntries = true)
	public Gabarit creer(final String niveau, final Utilisateur utilisateur) {
		return this.niveauService.chercher(niveau)
				.map(n -> Gabarit.with().niveau(n).creation(CreateAudit.with().utilisateur(utilisateur).build()).build())
				.orElseThrow(() -> new NiveauIntrouvableException(niveau));
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final Modele modele, final Utilisateur utilisateur) {
		log.info("Recherche d'une surcharge pour le niveau {} pour le modele {} et l'utilisateur {}", AGENT, modele, utilisateur);

		return this.repository.findByNiveauCodeAndModeleIdAndProprietaireId(AGENT, modele.getId(), utilisateur.getId());
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final Modele modele, final com.atexo.redaction.domaines.document.model.Service service) {
		log.info("Recherche d'une surcharge pour le niveau {} pour le modele {} et le service {}", ENTITE_ACHAT, modele, service);

		return this.repository.findByNiveauCodeAndModeleIdAndServiceId(ENTITE_ACHAT, modele.getId(), service.getId());
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final UUID uuid) {
		return this.repository.findByUuid(uuid);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final Modele modele, final Organisme organisme) {
		log.info("Recherche d'une surcharge pour le niveau {} pour le modele {} et l'organisme {}", ORGANISME, modele, organisme);

		return this.repository.findByNiveauCodeAndModeleIdAndOrganismeId(ORGANISME, modele.getId(), organisme.getId());
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Optional<Gabarit> recuperer(final Modele modele, final Utilisateur utilisateur) {
		return this.rechercher(modele, utilisateur);
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Optional<Gabarit> recuperer(final Modele modele, final com.atexo.redaction.domaines.document.model.Service service) {
		return this.rechercher(modele, service);
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Optional<Gabarit> recuperer(final Modele modele, final Organisme organisme) {
		return this.rechercher(modele, organisme);
	}

	@Override
	@Cacheable(condition = "#result != null")
	public Optional<Gabarit> recuperer(final Modele modele, final Environnement environnement) {
		return this.rechercher(modele, environnement).or(() -> this.rechercher(modele, this.environnementService.defaut()));
	}

	@Override
	@CacheEvict(allEntries = true)
	public Gabarit sauvegarder(final Gabarit gabarit) {
		return this.repository.save(gabarit);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> chercher(final Long id) {
		return this.repository.findById(id);
	}

	@Override
	@CacheEvict(allEntries = true)
	public void supprimer(final Gabarit gabarit) {
		this.repository.delete(gabarit);
	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Gabarit> rechercher(final Environnement environnement, final Utilisateur utilisateur, final Modele modele) {
		if (null == utilisateur) {
			throw new UtilisateurAbsentException();
		}

		return this.rechercher(modele, utilisateur)
				.or(() -> this.rechercher(modele, utilisateur.getService()))
				.or(() -> this.rechercher(modele, utilisateur.getService().getOrganisme()))
				.or(() -> this.rechercher(modele, environnement))
				.or(() -> this.rechercher(modele, this.environnementService.defaut()));

	}

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Revision<Gabarit>> rechercherRevision(final UUID uuid) {
		final var query = new GabaritAuditQuery(this.entityManager)
				.where(AuditEntity.property("uuid").eq(uuid));

		Revision<Gabarit> result = null;

		try {
			result = query.getSingleResult();
		} catch (final NoResultException nre) {
			log.error("L'identifiant '{}' n'a pas été trouvé", uuid, nre);
		} catch (final NonUniqueResultException nure) {
			log.error("L'identifiant '{}' existe plusieurs fois", uuid, nure);
		}

		return Optional.ofNullable(result);
	}

	@Override
	public List<Revision<Gabarit>> recupererRevisions(final Gabarit gabarit) {
		final var query = new GabaritAuditQuery(this.entityManager)
				.where(AuditEntity.id().eq(gabarit.getId()))
				.orderBy(AuditEntity.revisionNumber().desc());

		List<Revision<Gabarit>> result = new ArrayList<>();

		try {
			result = query.getResultList();
		} catch (final NoResultException nre) {
			log.debug("Le gabarit {} n'a pas de révision", gabarit, nre);
		}

		return result;
	}

}
