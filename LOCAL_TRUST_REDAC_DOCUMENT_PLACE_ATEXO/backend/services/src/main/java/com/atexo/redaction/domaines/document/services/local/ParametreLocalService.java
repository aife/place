package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Parametre;
import com.atexo.redaction.domaines.document.model.exceptions.api.configuration.ParametreIntrouvableException;
import com.atexo.redaction.domaines.document.respositories.ParametreRepository;
import com.atexo.redaction.domaines.document.services.ParametreService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "parametres")
public class ParametreLocalService implements ParametreService {

	@NonNull
	private final ParametreRepository repository;

	@Override
	public Optional<Parametre> chercher(final Environnement environnement, final String cle) {
		Validate.notNull(environnement, MessageFormat.format("Impossible de trouver le parametre ''{0}'' sans fournir l'environnement", cle));
		Validate.notNull(cle, "Impossible de trouver un parametre sans sa cle");

		return repository.findByEnvironnementAndCle(environnement, cle);
	}

	@Override
	public String recuperer(Environnement environnement, String cle) {
		return this.chercher(environnement, cle).map(Parametre::getValeur).orElseThrow(() -> new ParametreIntrouvableException(environnement, cle));
	}

	@Override
	public Set<Parametre> recuperer(final Environnement environnement) {
		Validate.notNull(environnement, "Impossible de trouver les parametres d'en environnement sans fournir l'environnement");

		return repository.findAllByEnvironnement(environnement);
	}
}
