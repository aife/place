package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Categorie;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Modele;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface ModeleService {

	/**
	 * Chercher les modeles actifs
	 *
	 * @param environnement l'environnement
	 *
	 * @return les modeles
	 */
	List<Modele> recuperer(Environnement environnement);

	/**
	 * Chercher les modeles actifs
	 *
	 * @param environnement l'environnement
	 * @param categorie la categorie
	 *
	 * @return les modeles
	 */
	List<Modele> recupererParCategorie(Environnement environnement, Categorie categorie);

	/**
	 * Chercher les modeles administrables actifs
	 *
	 * @param environnement l'environnement
	 *
	 * @return les modeles
	 */
	List<Modele> recupererAdministrables(Environnement environnement);

	/**
	 * Chercher un modele
	 *
	 * @param id l'id du modele
	 *
	 * @return le modele
	 */
	Optional<Modele> chercher(Long id);

	/**
	 * Chercher un modele
	 *
	 * @param uuid l'uuid du modele
	 *
	 * @return le modele
	 */
	Optional<Modele> chercher(UUID uuid);
}
