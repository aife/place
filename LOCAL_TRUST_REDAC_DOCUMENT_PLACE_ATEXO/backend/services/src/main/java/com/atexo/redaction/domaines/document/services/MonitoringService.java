package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.model.revisions.PoolingRevision;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Service de surveillance des fichiers
 */
@Service
public interface MonitoringService {

	/**
	 * Permet de vérifier le statut de l'édition des documents
	 *
	 * @param plateforme la plateforme
	 *
	 * @return les editions, contenant le statut pour le document
	 */
	Set<PoolingRevision> verifier(final Plateforme plateforme);
}
