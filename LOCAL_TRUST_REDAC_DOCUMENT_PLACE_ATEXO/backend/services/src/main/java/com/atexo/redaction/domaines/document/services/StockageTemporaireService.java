package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.shared.patterns.Enregistrable;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Set;
import java.util.UUID;

/**
 * Service de stockage temporaire de fichier
 */
@Service
public interface StockageTemporaireService {

	/**
	 * Permet de retrouver le fichier d'un document éditable
	 *
	 * @param enregistrable la ressource enregistrable
	 *
	 * @return le fichier physique, sous forme d'une ressource
	 */
	Resource copier(final Enregistrable enregistrable);

	/**
	 * Permet de lire un fichier temporaire
	 *
	 * @param uuid l'uuid du fichier
	 *
	 * @return la resource
	 */
	Resource lire(final UUID uuid);

	/**
	 * Permet de supprimer le fichier temporaire
	 *
	 * @param uuid l'uuid du fichier
	 */
	void supprimer(final UUID uuid);

	/**
	 * Permet de sauvegarder le fichier dans le répertoire temporaire
	 *
	 * @param resource la ressource
	 *
	 * @return le chemin
	 */
	File sauvegarder(final Resource resource);

	/**
	 * Permet de sauvegarder le flux dans le répertoire temporaire
	 *
	 * @param resource la ressource
	 *
	 * @return le chemin
	 */
	File sauvegarder(final InputStream resource);

	/**
	 * Permet d'uploader le flux dans le répertoire temporaire
	 *
	 * @param resource la ressource
	 *
	 * @return le chemin
	 */
	File upload(final InputStream resource);

	/**
	 * Créer fichier temporaire
	 *
	 * @param contenu le contenu du fichier à créer
	 * @param type l'extension du fichier
	 *
	 * @return le fichier
	 */
	Resource creer(final String contenu, final MimeType type);

	/**
	 * Permet de dézipper une archive
	 *
	 * @param zip le zip
	 *
	 * @return la liste des fichiers
	 */
	Set<File> unzip(final File zip) throws IOException;

	/**
	 * Permet de dézipper une archive
	 *
	 * @param zip le zip
	 * @param repertoire le repertoire de destination
	 *
	 * @return la liste des fichiers
	 */
	Set<File> unzip(final File zip, final Path repertoire) throws IOException;
}
