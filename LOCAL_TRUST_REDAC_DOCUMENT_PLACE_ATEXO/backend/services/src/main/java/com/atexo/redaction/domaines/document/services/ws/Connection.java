package com.atexo.redaction.domaines.document.services.ws;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.time.Instant;
import java.util.UUID;

@Builder(builderMethodName = "with")
@Getter
public class Connection {
	@Builder.Default
	private UUID uuid = UUID.randomUUID();
	@Builder.Default
	private Instant creation = Instant.now();
	@NonNull
	private Utilisateur utilisateur;
	@NonNull
	private Environnement environnement;

	public boolean correspond(final Environnement environnement) {
		return this.environnement.getClient().equals(environnement.getClient()) && this.environnement.getPlateforme().equals(environnement.getPlateforme());
	}
}
