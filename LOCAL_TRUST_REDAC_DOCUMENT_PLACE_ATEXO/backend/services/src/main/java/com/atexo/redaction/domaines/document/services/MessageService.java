package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Topic;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.atexo.redaction.domaines.document.model.messages.notifications.NotificationMessage;

import java.util.Set;

public interface MessageService {

	/**
	 * Notification sur un topic
	 *
	 * @param message le message
	 * @param topic le topic
	 * @param <T> le type de message
	 */
	<T extends BaseMessage> void notifier(final T message, final String topic);

	/**
	 * Notification sur un topic
	 *
	 * @param message le message
	 * @param topic le topic
	 * @param <T> le type de message
	 */
	<T extends BaseMessage> void notifier(final T message, final Topic topic, final Object... arguments);

	/**
	 * Permet d'envoyer une notification simple pour les utilisateurs
	 *
	 * @param utilisateurs les utilisateurs
	 * @param message le message
	 */
	void notifier(Set<Utilisateur> utilisateurs, NotificationMessage message);

	/**
	 * Permet d'envoyer une notification simple pour l'utilisateur
	 *
	 * @param utilisateur l'utilisateur
	 * @param message le message
	 */
	void notifier(Utilisateur utilisateur, NotificationMessage message);

	/**
	 * Permet d'envoyer une notification simple pour le contexte
	 *
	 * @param contexte le contexte
	 * @param message le message
	 */
	void notifier(Contexte contexte, NotificationMessage message);

	/**
	 * Permet d'envoyer une notification simple pour l'environnement
	 *
	 * @param environnement l'environnement
	 * @param message le message
	 */
	void notifier(Environnement environnement, NotificationMessage message);
}
