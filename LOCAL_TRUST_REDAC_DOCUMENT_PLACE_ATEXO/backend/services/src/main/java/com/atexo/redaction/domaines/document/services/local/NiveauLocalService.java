package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import com.atexo.redaction.domaines.document.respositories.referentiel.NiveauRepository;
import com.atexo.redaction.domaines.document.services.NiveauService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "niveaux")
public class NiveauLocalService implements NiveauService {

	@NonNull
	private NiveauRepository repository;

	@Override
	@Cacheable(condition = "#result != null && #result.isPresent()")
	public Optional<Niveau> chercher(String code) {
		return repository.findByCode(code);
	}
}
