package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Autorisation;
import com.atexo.redaction.domaines.document.model.Habilitation;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.services.HabilitationService;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = "habilitations")
public class HabilitationLocalService implements HabilitationService {

	@NonNull
	private final SerialisationService serialisationService;

	@Override
	public Set<Habilitation> extraire(final Utilisateur utilisateur) {
		final var habilitations = Optional.ofNullable(utilisateur.getPayload())
				.flatMap(payload -> serialisationService.deserialise(payload, com.atexo.redaction.domaines.document.mpe.v2.Agent.class))
				.map(com.atexo.redaction.domaines.document.mpe.v2.Agent::getHabilitations)
				.map(com.atexo.redaction.domaines.document.mpe.v2.Habilitations::getHabilitation)
				.orElse(new ArrayList<>());

		return habilitations
				.stream().map(Habilitation::new)
				.collect(Collectors.toSet());
	}

	@Override
	@Cacheable
	public boolean autorise(Utilisateur utilisateur, Autorisation autorisation) {
		return this.extraire(utilisateur).stream()
				.anyMatch(habilitation -> habilitation.getValeur().equalsIgnoreCase(autorisation.getInput()));
	}
}
