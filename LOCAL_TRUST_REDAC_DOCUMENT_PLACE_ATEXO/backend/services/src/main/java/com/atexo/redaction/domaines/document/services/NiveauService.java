package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.referentiels.Niveau;

import java.util.Optional;

public interface NiveauService {

	/**
	 * Permet de recuperer un niveau
	 *
	 * @param code le code du niveau
	 *
	 * @return le niveau
	 */
	Optional<Niveau> chercher(String code);
}
