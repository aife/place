package com.atexo.redaction.domaines.document.services.remote.mappers.clausier.out;

import com.atexo.redaction.domaines.document.connectors.clausier.views.ServiceClausier;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapping;

@org.mapstruct.Mapper(uses = {OrganismeClausierMapper.class})
public interface ServiceClausierMapper extends Convertible<ServiceClausier, Service> {

	@Override
	@Mapping(target = "payload", ignore = true)
	@Mapping(target = "parent", ignore = true)
	Service map(ServiceClausier service);
}
