package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.in;

import com.atexo.redaction.domaines.document.model.revisions.CallbackRevision;

@org.mapstruct.Mapper(componentModel = "spring")
public abstract class RevisionCallbackDocgenMapper extends RevisionDocgenMapper<CallbackRevision> {

}
