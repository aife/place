package com.atexo.redaction.domaines.document.services.configuration.messec;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

@Getter
@Setter
@NoArgsConstructor(force = true)
@Profile("messec")
public class Message {

	private String defaut;
	private String contenu;
	private String statut;
	private String type;
	private String entete;
	private String modele;
	private boolean html;
}
