package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.in;

import com.atexo.redaction.domaines.document.model.revisions.PoolingRevision;

@org.mapstruct.Mapper(componentModel = "spring")
public abstract class RevisionPoolingDocgenMapper extends RevisionDocgenMapper<PoolingRevision> {
}
