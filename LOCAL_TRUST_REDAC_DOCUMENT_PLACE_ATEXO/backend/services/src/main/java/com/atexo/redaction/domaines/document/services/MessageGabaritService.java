package com.atexo.redaction.domaines.document.services;

import com.atexo.redaction.domaines.document.model.messages.gabarits.CreationGabaritMessage;
import com.atexo.redaction.domaines.document.model.messages.gabarits.SuppressionGabaritMessage;

public interface MessageGabaritService {

	/**
	 * Permet d'envoyer une notification de suppression du gabarit
	 *
	 * @param message le message de suppression du gabarit
	 */
	void notifier(SuppressionGabaritMessage message);

	/**
	 * Permet d'envoyer une notification d'ajout du gabarit
	 *
	 * @param message le message de suppression du gabarit
	 */
	void notifier(CreationGabaritMessage message);
}
