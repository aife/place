package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.exceptions.io.CreationRepertoireException;
import com.atexo.redaction.domaines.document.services.FichierService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FichierLocalService implements FichierService {

	@Override
	public String extraire(final File fichier, final Charset charset) {
		String result = null;
		try {
			result = Files.readString(fichier.toPath(), charset);
		} catch (final IOException ioe) {
			log.error("Impossible de lire le fichier {}", fichier, ioe);
		}

		return result;
	}

	@Override
	public String getExtension(final String nom) {
		return FilenameUtils.getExtension(nom);
	}

	@Override
	public String getExtension(final File fichier) {
		return FilenameUtils.getExtension(fichier.getName());
	}

	@Override
	public boolean possedeTypeMime(final File fichier, final String... extensions) {
		return this.possedeTypeMime(fichier, Arrays.stream(extensions).collect(Collectors.toSet()));
	}

	@Override
	public boolean possedeTypeMime(File fichier, Iterable<String> extensions) {
		final var extension = getExtension(fichier);

		return Lists.newArrayList(extensions).stream().anyMatch(ext -> ext.equalsIgnoreCase(extension));
	}

	@Override
	public Path horodaterRepertoire(final Path base) {
		final var maintenant = LocalDate.now();

		return base.resolve(Path.of(String.valueOf(maintenant.getYear()), String.valueOf(maintenant.getMonthValue()), String.valueOf(maintenant.getDayOfMonth())));
	}

	@Override
	public Path creerRepertoire(final Path base, final boolean horodatage) {
		final var folder = horodatage ? horodaterRepertoire(base) : base;

		return verifierRepertoire(folder);
	}

	@Override
	public Path creerRepertoire(final Path base) {
		return creerRepertoire(base, true);
	}

	@Override
	public Path verifierRepertoire(final Path repertoire) {

		if (!repertoire.toFile().exists()) {
			final var creation = repertoire.toFile().mkdirs();

			if (!creation) {
				log.error("Impossible de créer le répertoire {}. Merci de vérifier les droits.", repertoire.toFile());

				throw new CreationRepertoireException("Impossible de créer le répertoire {0}. Merci de vérifier les droits.", repertoire.toFile());
			}
		} else if (!repertoire.toFile().isDirectory()) {
			log.error("Impossible de créer le répertoire {}. Il existe déjà et il s'agit d'un fichier", repertoire.toFile());

			throw new CreationRepertoireException("Impossible de créer le répertoire {0}. Il existe déjà et il s'agit d'un fichier.", repertoire.toFile());
		}

		return repertoire;
	}

	@Override
	public File copier(final Path origine, final Path repertoire, final String nom, final boolean horodatage) {
		final var folder = creerRepertoire(repertoire, horodatage);

		final var result = folder.resolve(nom);

		try {
			Files.copy(origine, result, StandardCopyOption.REPLACE_EXISTING);
		} catch (final IOException e) {
			log.error("Impossible d'écrire le fichier {}", result);
		}

		return result.toFile();
	}

	@Override
	public File copier(final Resource resource, final Path repertoire, final String nom, final boolean horodatage) {
		File result = null;
		try {
			result = copier(resource.getInputStream(), repertoire, nom, horodatage);
		} catch (final IOException e) {
			log.error("Impossible de lire le flux {}", resource);
		}

		return result;
	}

	@Override
	public File copier(final InputStream stream, final Path repertoire, final String nom, final boolean horodatage) {
		final var folder = creerRepertoire(repertoire, horodatage);

		final var result = folder.resolve(nom);
		try {
			Files.copy(stream, result, StandardCopyOption.REPLACE_EXISTING);
		} catch (final IOException e) {
			log.error("Impossible d'écrire le fichier {}", result);
		}

		return result.toFile();
	}

	@Override
	public File copier(final Path origine, final Path repertoire, final String nom) {
		return this.copier(origine, repertoire, nom, true);
	}

	@Override
	public File copier(final Resource resource, final Path repertoire, final String nom) {
		return this.copier(resource, repertoire, nom, true);
	}

	@Override
	public File copier(final InputStream stream, final Path repertoire, final String nom) {
		return this.copier(stream, repertoire, nom, true);
	}

	@Override
	public String creerNomAleatoire() {
		return UUID.randomUUID().toString();
	}

	@Override
	public boolean supprimer(Resource ressource) {
		boolean result = false;
		try {
			result = Files.deleteIfExists(ressource.getFile().toPath());
		} catch (IOException ioe) {
			log.warn("Impossible de supprimer le fichier temporaire '{}'", ressource.getFilename(), ioe);
		}

		return result;
	}
}
