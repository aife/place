package com.atexo.redaction.domaines.document.services.remote.mappers.docgen.out;

import com.atexo.redaction.domaines.document.connectors.docgen.views.AuteurDocgen;
import com.atexo.redaction.domaines.document.model.Auteur;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapping;

@org.mapstruct.Mapper(componentModel = "spring")
public interface AuteurDocgenMapper extends Convertible<Auteur, AuteurDocgen> {

	@Override
	@Mapping(target = "autorisation", ignore = true)
	AuteurDocgen map(Auteur bo);
}
