package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Action;
import com.atexo.redaction.domaines.document.model.BaseEvenement;
import com.atexo.redaction.domaines.document.model.Etat;
import com.atexo.redaction.domaines.document.model.Statut;
import com.atexo.redaction.domaines.document.model.revisions.CallbackRevision;
import com.atexo.redaction.domaines.document.respositories.EvenementRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@DisplayName("Tests du service des évènements")
@ExtendWith(MockitoExtension.class)
@Slf4j
public class EvenementLocalServiceTest {

	@InjectMocks
	private EvenementLocalService target;

	@Mock
	private EvenementRepository repository;

	/**
	 * Test des cas passants de la méthode {@link EvenementLocalService#sauvegarder(BaseEvenement)}
	 */
	@DisplayName("Test de sauvegarde d'un évènement - cas passants")
	@Test
	public void GIVEN_un_evenement_valide_WHEN_sauvegarder_THEN_l_evenement_est_sauvegarde() {
		final var revision = new CallbackRevision();
		revision.setEtat(Etat.SAUVEGARDE_ET_FERME);
		revision.setAction(Action.ENREGISTREMENT);
		revision.setRev(1);
		revision.setDateCreation(Instant.now());
		revision.setDateModification(Instant.now());
		revision.setStatut(Statut.VALIDE);
		revision.setVersion(1);

		final var expected = new CallbackRevision();
		expected.setId(1L);
		expected.setEtat(revision.getEtat());
		expected.setAction(revision.getAction());
		expected.setRev(revision.getRev());
		expected.setDateCreation(revision.getDateCreation());
		expected.setDateModification(revision.getDateModification());
		expected.setStatut(revision.getStatut());
		expected.setVersion(revision.getVersion());

		when(repository.save(Mockito.any())).thenReturn(expected);

		assertThat(this.target.sauvegarder(revision)).isEqualTo(expected);
	}

	/**
	 * Test des cas limites de la méthode {@link EvenementLocalService#sauvegarder(BaseEvenement)}
	 */
	@DisplayName("Test de sauvegarde d'un évènement - cas parametres nuls et/ou vides")
	@Test
	public void GIVEN_un_evenement_nul_WHEN_sauvegarder_THEN_une_exception_est_levee() {
		assertThrows(NullPointerException.class, () -> this.target.sauvegarder(null));
	}
}


