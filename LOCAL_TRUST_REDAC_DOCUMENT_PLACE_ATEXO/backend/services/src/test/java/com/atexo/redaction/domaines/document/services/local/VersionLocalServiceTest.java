package com.atexo.redaction.domaines.document.services.local;

import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionInvalideException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@DisplayName("Tests du service des versions")
@ExtendWith(MockitoExtension.class)
@Slf4j
public class VersionLocalServiceTest {

	@InjectMocks
	private VersionLocalService target;

	private static Stream<Arguments> GIVEN_une_version_valide_WHEN_estComptatible_THEN_le_resultat_est_vrai() {
		return of(
				arguments(new Version("2022-01.01.00"), new Version("2022-01.00.00"), null, true),
				arguments(new Version("2022-01.00.01"), new Version("2022-01.00.00"), null, true),
				arguments(new Version("2022-01.01.01"), new Version("2022-01.00.00"), null, true),
				arguments(new Version("2021-01.01.01"), new Version("2022-01.00.00"), null, false),
				arguments(new Version("2022-01.00.00"), null, new Version("2022-01.00.00"), true),
				arguments(new Version("2021-01.00.01"), null, new Version("2022-01.00.00"), true),
				arguments(new Version("2023-01.00.00"), null, new Version("2022-01.00.00"), false),
				arguments(new Version("2022-01.00.00"), new Version("2022-01.00.00"), new Version("2023-01.00.00"), true),
				arguments(new Version("2023-01.00.00"), new Version("2022-01.00.00"), new Version("2023-01.00.00"), true),
				arguments(new Version("2022-01.01.00"), new Version("2022-01.00.00"), new Version("2023-01.00.00"), true),
				arguments(new Version("2024-01.00.00"), new Version("2022-01.00.00"), new Version("2023-01.00.00"), false)
		);
	}

	/**
	 * Test des cas passants de la méthode {@link VersionLocalService#estCompatible(Version)}
	 */
	@DisplayName("Test de compatiblité de version - cas passants")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_valide_WHEN_estComptatible_THEN_le_resultat_est_vrai(Version version, Version min, Version max, boolean expected) {
		ReflectionTestUtils.setField(target, "minVersion", min);
		ReflectionTestUtils.setField(target, "maxVersion", max);

		assertThat(this.target.estCompatible(version)).isEqualTo(expected);
	}

	private static Stream<Arguments> GIVEN_une_version_nulle_ou_vide_WHEN_estComptatible_THEN_une_exception_est_levee() {
		return of(
				arguments(null, NullPointerException.class),
				arguments(new Version(""), VersionInvalideException.class),
				arguments(new Version("angus"), VersionInvalideException.class)
		);
	}

	/**
	 * Test des cas limites de la méthode {@link VersionLocalService#estCompatible(Version)}
	 */
	@DisplayName("Test de compatiblité de version - cas parametres nuls et/ou vides")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_nulle_ou_vide_WHEN_estComptatible_THEN_une_exception_est_levee(final Version version, final Class<Throwable> expected) {
		ReflectionTestUtils.setField(target, "minVersion", new Version("2022-01.00.00"));
		ReflectionTestUtils.setField(target, "maxVersion", new Version("2023-01.00.00"));

		assertThrows(expected, () -> this.target.estCompatible(version));
	}

	private static Stream<Arguments> GIVEN_une_version_valide_WHEN_estTropAncienne_THEN_le_resultat_est_vrai() {
		return of(
				arguments(new Version("2021-01.00.00"), new Version("2022-01.00.00"), true),
				arguments(new Version("2022-01.00.00"), new Version("2022-01.00.00"), false),
				arguments(new Version("2022-01.00.01"), new Version("2022-01.00.00"), false),
				arguments(new Version("2022-01.01.00"), new Version("2022-01.00.00"), false),
				arguments(new Version("2022-01.01.01"), new Version("2022-01.00.00"), false),
				arguments(new Version("2022-01.00.00"), null, false)
		);
	}

	/**
	 * Test des cas passants de la méthode {@link VersionLocalService#estTropAncienne(Version)}
	 */
	@DisplayName("Test d'ancienneté de version - cas passants")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_valide_WHEN_estTropAncienne_THEN_le_resultat_est_vrai(Version version, Version min, boolean expected) {
		ReflectionTestUtils.setField(target, "minVersion", min);

		assertThat(this.target.estTropAncienne(version)).isEqualTo(expected);
	}

	private static Stream<Arguments> GIVEN_une_version_nulle_ou_vide_WHEN_estTropAncienne_THEN_une_exception_est_levee() {
		return of(
				arguments(null, NullPointerException.class),
				arguments(new Version(""), VersionInvalideException.class),
				arguments(new Version("angus"), VersionInvalideException.class)
		);
	}

	/**
	 * Test des cas limites de la méthode {@link VersionLocalService#estTropAncienne(Version)}
	 */
	@DisplayName("Test d'ancienneté de version - cas parametres nuls et/ou vides")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_nulle_ou_vide_WHEN_estTropAncienne_THEN_une_exception_est_levee(final Version version, final Class<Throwable> expected) {
		ReflectionTestUtils.setField(target, "minVersion", new Version("2022-01.00.00"));
		ReflectionTestUtils.setField(target, "maxVersion", new Version("2023-01.00.00"));

		assertThrows(expected, () -> this.target.estTropAncienne(version));
	}

	private static Stream<Arguments> GIVEN_une_version_valide_WHEN_estTropRecente_THEN_le_resultat_est_vrai() {
		return of(
				arguments(new Version("2022-01.00.00"), new Version("2021-01.00.00"), true),
				arguments(new Version("2022-01.00.00"), new Version("2022-01.00.00"), false),
				arguments(new Version("2021-01.00.01"), new Version("2022-01.00.00"), false),
				arguments(new Version("2023-01.00.00"), new Version("2022-01.00.00"), true),
				arguments(new Version("2022-01.01.01"), null, false)
		);
	}

	/**
	 * Test des cas passants de la méthode {@link VersionLocalService#estTropRecente(Version)}
	 */
	@DisplayName("Test de récence de version - cas passants")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_valide_WHEN_estTropRecente_THEN_le_resultat_est_vrai(Version version, Version max, boolean expected) {
		ReflectionTestUtils.setField(target, "maxVersion", max);

		assertThat(this.target.estTropRecente(version)).isEqualTo(expected);
	}

	private static Stream<Arguments> GIVEN_une_version_nulle_ou_vide_WHEN_estTropRecente_THEN_une_exception_est_levee() {
		return of(
				arguments(null, NullPointerException.class),
				arguments(new Version(""), VersionInvalideException.class),
				arguments(new Version("angus"), VersionInvalideException.class)
		);
	}

	/**
	 * Test des cas limites de la méthode {@link VersionLocalService#estTropRecente(Version)}
	 */
	@DisplayName("Test de récence de version - cas parametres nuls et/ou vides")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_une_version_nulle_ou_vide_WHEN_estTropRecente_THEN_une_exception_est_levee(final Version version, final Class<Throwable> expected) {
		ReflectionTestUtils.setField(target, "minVersion", new Version("2022-01.00.00"));
		ReflectionTestUtils.setField(target, "maxVersion", new Version("2023-01.00.00"));

		assertThrows(expected, () -> this.target.estTropRecente(version));
	}
}
