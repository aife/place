<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>com.atexo.redaction.domaines.document</groupId>
		<artifactId>document-backend</artifactId>
		<version>2024-00.00.03</version>
	</parent>

	<groupId>com.atexo.redaction.domaines.document.document-backend</groupId>
	<artifactId>document-services</artifactId>
	<packaging>jar</packaging>
	<name>${project.groupId}.${project.artifactId}</name>
	<version>2024-00.00.03</version>
	<description>services backend du module document</description>

	<dependencies>
		<dependency>
			<groupId>com.atexo.redaction.domaines.document.document-backend</groupId>
			<artifactId>document-shared</artifactId>
			<version>2024-00.00.03</version>
		</dependency>

		<dependency>
			<groupId>com.atexo.redaction.domaines.document.document-backend</groupId>
			<artifactId>document-connectors</artifactId>
			<version>2024-00.00.03</version>
		</dependency>

		<dependency>
			<groupId>com.atexo.redaction.domaines.document.document-backend</groupId>
			<artifactId>document-repositories</artifactId>
			<version>2024-00.00.03</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
			<version>${spring-framework.version}</version>
		</dependency>
		
		<!-- JavaMelody Spring Boot Starter -->
		<dependency>
			<groupId>net.bull.javamelody</groupId>
			<artifactId>javamelody-spring-boot-starter</artifactId>
			<version>${javamelody.version}</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.dataformat</groupId>
			<artifactId>jackson-dataformat-xml</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-websocket</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-json</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
		
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
		
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<optional>true</optional>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>

        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Keycloak -->
        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-spring-security-adapter</artifactId>
            <version>${keycloak.version}</version>
        </dependency>

        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-spring-boot-starter</artifactId>
            <version>${keycloak.version}</version>
        </dependency>

        <!-- swagger -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>${swagger.version}</version>
        </dependency>

        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>${swagger.version}</version>
        </dependency>

        <!-- JAXB -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
        </dependency>

        <!-- Mapstruct -->
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>

        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct-processor</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>

        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>${jaxb.version}</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-core</artifactId>
            <version>${jaxb.version}</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>${jaxb.version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang3.version}</version>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>${commons-io.version}</version>
        </dependency>

        <dependency>
            <groupId>com.atexo.redaction.domaines.document.document-backend</groupId>
            <artifactId>document-tests</artifactId>
            <scope>test</scope>
            <version>2022-01.00.00-SNAPSHOT</version>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>jaxb2-maven-plugin</artifactId>
                <version>2.5.0</version>
                <executions>
                    <execution>
                        <id>mpe-ws-v1-consultation-api</id>
                        <goals>
                            <goal>xjc</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/main/resources/schemas/v1/consultation.xsd</source>
                            </sources>
                            <!-- The package of your generated sources -->
                            <packageName>com.atexo.redaction.domaines.document.mpe.v1</packageName>
                            <clearOutputDir>false</clearOutputDir>
                        </configuration>

                    </execution>
                    <execution>
                        <id>mpe-ws-v1-agent-api</id>
                        <goals>
                            <goal>xjc</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/main/resources/schemas/v1/agent.xsd</source>
                            </sources>
                            <!-- The package of your generated sources -->
                            <packageName>com.atexo.redaction.domaines.document.mpe.v1</packageName>
                            <clearOutputDir>false</clearOutputDir>
                        </configuration>

                    </execution>
                    <execution>
                        <id>mpe-ws-v2-contexte-api</id>
                        <goals>
                            <goal>xjc</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/main/resources/schemas/v2/contexte.xsd</source>
                            </sources>
                            <!-- The package of your generated sources -->
                            <packageName>com.atexo.redaction.domaines.document.mpe.v2</packageName>
                            <clearOutputDir>false</clearOutputDir>
                        </configuration>

                    </execution>
                    <execution>
                        <id>mpe-ws-v2-agent-api</id>
                        <goals>
                            <goal>xjc</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/main/resources/schemas/v2/agent.xsd</source>
                            </sources>
                            <!-- The package of your generated sources -->
                            <packageName>com.atexo.redaction.domaines.document.mpe.v2</packageName>
                            <clearOutputDir>false</clearOutputDir>
                        </configuration>

                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
