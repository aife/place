package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.CanevasController;
import com.atexo.redaction.domaines.document.controllers.views.CanevasView;
import com.atexo.redaction.domaines.document.model.Canevas;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.services.ClausierService;
import com.atexo.redaction.domaines.document.services.TypeModeleService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/canevas")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "canevas")
public class CanevasRestController extends BaseRestController implements CanevasController {

	@NonNull
	private final ClausierService clausierService;

	@NonNull
	private final Convertible<Canevas, CanevasView> canevasViewMapper;

	@NonNull
	private final TypeModeleService typeModeleService;

	@Override
	@GetMapping(headers = {HEADER_ACCEPT_JSON_V2, HEADER_ACCEPT_XML_V2}, produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<CanevasView> rechercher(
			@RequestParam("contexte") final Contexte contexte,
			@RequestParam("type") final String type,
			@RequestParam(value = "lot", required = false) final Integer idLot
	) {
		var code = typeModeleService.recuperer(type, contexte.getEnvironnement());

		return canevasViewMapper.map(clausierService.rechercher(contexte, code, idLot));
	}
}
