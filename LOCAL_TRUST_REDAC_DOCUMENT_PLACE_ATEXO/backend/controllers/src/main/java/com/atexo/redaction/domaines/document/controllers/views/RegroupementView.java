package com.atexo.redaction.domaines.document.controllers.views;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum RegroupementView {
	CANEVAS("canevas"),
	EDITABLE("editable");

	@NonNull
	@JsonValue
	private final String name;
}
