package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.GabaritView;
import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface GabaritViewMapper extends Convertible<Gabarit, GabaritView> {
	@Override
	@Mapping(target = "actif", ignore = true)
	GabaritView map(Gabarit gabarit);
}
