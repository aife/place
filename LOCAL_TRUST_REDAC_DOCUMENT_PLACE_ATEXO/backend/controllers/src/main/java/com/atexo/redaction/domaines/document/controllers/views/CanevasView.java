package com.atexo.redaction.domaines.document.controllers.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CanevasView {
	private Integer id;

	private String titre;

	private String type;

	private String auteur;

	private Date dateModification;

	private Date dateCreation;

	private Set<ProcedureView> procedures;

	private String reference;

	private StatutCanevasView statut;

	private OrganismeView organisme;

	private NaturePrestationView naturePrestation;

	private Set<ContratView> contrats;

	private String version;
}
