package com.atexo.redaction.domaines.document.controllers.mappers.in.mpe;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.mpe.v2.Agent;
import com.atexo.redaction.domaines.document.services.ServiceService;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;

import javax.transaction.Transactional;

@Mapper
@Slf4j
public abstract class UtilisateurMapper implements Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Agent, Service>, Utilisateur> {

	@Setter(onMethod = @__(@Autowired))
	private UtilisateurService utilisateurService;

	@Setter(onMethod = @__(@Autowired))
	private SerialisationService serialisationService;

	@Override
	@Transactional
	public Utilisateur map(Pair<com.atexo.redaction.domaines.document.mpe.v2.Agent, Service> pair) {
		var agentXML = pair.getFirst();
		var service = pair.getSecond();

		return this.utilisateurService.sauvegarder(
				this.utilisateurService.chercher(agentXML.getIdentifiantExterne(), service)
						.map(
								utilisateur -> {
									log.info("L'utilisateur '{}' existe déjà. Il sera mis a jour si besoin", agentXML.getIdentifiantExterne());

									utilisateur.setPayload(this.serialisationService.serialise(agentXML));

									return utilisateur;
								}
						)
						.orElseGet(() -> {
									log.info("L'utilisateur '{}' n'existe pas encore. Il va etre créé.", agentXML.getIdentifiantExterne());

									return Utilisateur.with()
											.service(service)
											.payload(this.serialisationService.serialise(agentXML))
											.build();
								}
						));
	}
}
	
