package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.mappers.out.custom.UriMapper;
import com.atexo.redaction.domaines.document.controllers.views.CanevasView;
import com.atexo.redaction.domaines.document.model.Canevas;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(uses = {ProcedureViewMapper.class, StatutCanevasViewMapper.class, OrganismeViewMapper.class, NaturePrestationViewMapper.class, ContratViewMapper.class})
public interface CanevasViewMapper extends Convertible<Canevas, CanevasView> {
}
