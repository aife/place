package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class TeleversementView {
	private String uuid;

	private String nom;

	private Long taille;

	private String extension;
}
