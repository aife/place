package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.ConsultationController;
import com.atexo.redaction.domaines.document.controllers.views.ConsultationView;
import com.atexo.redaction.domaines.document.model.Consultation;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ConsultationIntrouvableException;
import com.atexo.redaction.domaines.document.services.ClausierService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/consultations")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "consultations")
public class ConsultationRestController extends BaseRestController implements ConsultationController {

	@NonNull
	private final ClausierService clausierService;

	@NonNull
	private final Convertible<Consultation, ConsultationView> consultationViewMapper;

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public ConsultationView recuperer(
			@RequestParam("contexte") final Contexte contexte
	) {
		var reference = contexte.getReference();

		log.info("Recherche de la consultation depuis la référence unique du contexte {}", reference);

		var consultation = clausierService.recuperer(contexte).orElseThrow(() -> new ConsultationIntrouvableException(contexte));

		return consultationViewMapper.map(consultation);
	}

}
