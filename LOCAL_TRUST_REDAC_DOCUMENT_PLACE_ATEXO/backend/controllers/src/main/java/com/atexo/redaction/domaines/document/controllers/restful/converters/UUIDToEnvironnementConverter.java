package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToEnvironnementConverter implements Converter<String, Environnement> {

	@NonNull
	private EnvironnementService environnementService;

	@Override
	public Environnement convert(final String value) {
		var uuid = UUID.fromString(value);
		return environnementService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid));

	}
}
