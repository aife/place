package com.atexo.redaction.domaines.document.controllers.io;

import com.atexo.redaction.domaines.document.model.Categorie;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class RechercheCritere {
	private Environnement environnement;

	private Contexte contexte;

	private Set<String> expressions;

	private Set<Categorie> categories;

	private Set<Statut> statuts;
}
