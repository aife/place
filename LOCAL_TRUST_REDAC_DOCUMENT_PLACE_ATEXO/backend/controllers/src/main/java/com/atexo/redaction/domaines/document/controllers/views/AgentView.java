package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class AgentView {
	private OrganismeView organisme;

	private ServiceView service;

	private String nom;

	private String prenom;

	private String email;

	private String identifiantExterne;
}
