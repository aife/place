package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.OrganismeView;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface OrganismeModelMapper extends Convertible<OrganismeView, Organisme> {
}
