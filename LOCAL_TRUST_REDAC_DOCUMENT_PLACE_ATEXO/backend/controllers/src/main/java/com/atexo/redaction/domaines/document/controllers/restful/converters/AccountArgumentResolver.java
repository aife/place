package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AccountArgumentResolver implements CustomHandlerMethodArgumentResolver {

	public static final String ACCOUNT = "Account";

	@NonNull
	private UtilisateurService utilisateurService;

	@Override
	public boolean supportsParameter(final MethodParameter methodParameter) {
		return null != methodParameter.getParameterAnnotation(Account.class);
	}

	@Override
	public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
		Utilisateur result = null;

		var request = (HttpServletRequest) nativeWebRequest.getNativeRequest();
		var header = request.getHeader(ACCOUNT);

		if (null != header) {
			var uuid = UUID.fromString(header);

			result = utilisateurService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid));
		}

		return result;
	}
}
