package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class UtilisateurView {
	private UUID uuid;

	private ServiceView service;

	private String nom;

	private String prenom;

	private String email;

	private String identifiantExterne;

	private Set<String> habilitations = new HashSet<>();
}
