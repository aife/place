package com.atexo.redaction.domaines.document.controllers.mappers.in.mpe;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.UUID;

@Mapper
@Slf4j
public abstract class EnvironnementMapper implements Convertible<com.atexo.redaction.domaines.document.mpe.v2.Agent, Environnement> {

	@Setter(onMethod = @__(@Autowired))
	private EnvironnementService environnementService;

	@Override
	@Transactional
	public Environnement map(com.atexo.redaction.domaines.document.mpe.v2.Agent agent) {
		final var plateforme = agent.getEnvironnement().getPlateforme();
		final var client = agent.getEnvironnement().getClient();
		final var mpeUuid = agent.getEnvironnement().getMpeUuid();
		final var mpeUrl = agent.getEnvironnement().getMpeUrl();
		return this.environnementService.sauvegarder(this.environnementService.chercher(client, plateforme, mpeUuid).map(resultat -> {
			log.info("L'environnement {}/{}/{} existe déjà. Il sera mis a jour si besoin", plateforme, client, mpeUuid);
			if (mpeUuid != null) {
				resultat.setMpeUuid(mpeUuid);
			}
			if (mpeUrl != null) {
				resultat.setMpeUrl(mpeUrl);
			}
			return resultat;
		}).orElseGet(() -> {
			log.info("L'environnement {}/{}/{} n'existe pas encore. Il va etre créé.", plateforme, client, mpeUuid);

			return com.atexo.redaction.domaines.document.model.Environnement.with().plateforme(StringUtils.hasText(plateforme) ? plateforme : ".*").mpeUrl(mpeUrl).client(StringUtils.hasText(client) ? client : mpeUuid).uuid(UUID.randomUUID()).mpeUuid(mpeUuid).mpeUrl(mpeUrl).build();
		}));
	}
}
	
