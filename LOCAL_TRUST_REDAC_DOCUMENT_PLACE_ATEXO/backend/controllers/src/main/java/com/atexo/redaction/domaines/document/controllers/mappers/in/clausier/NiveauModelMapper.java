package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.ReferentielView;
import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface NiveauModelMapper extends Convertible<ReferentielView, Niveau> {

	@Mapping(target = "parent", ignore = true)
	@Mapping(target = "environnements", ignore = true)
	Niveau map(ReferentielView referentiel);
}
