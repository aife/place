package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.OrganismeView;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(uses = {EnvironnementViewMapper.class})
public interface OrganismeViewMapper extends Convertible<Organisme, OrganismeView> {

	@Override
	OrganismeView map(Organisme organisme);
}
