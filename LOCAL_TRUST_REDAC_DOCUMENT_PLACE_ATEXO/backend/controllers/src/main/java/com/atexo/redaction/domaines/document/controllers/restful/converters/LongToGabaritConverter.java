package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Gabarit;
import com.atexo.redaction.domaines.document.model.exceptions.api.gabarit.GabaritIntrouvableException;
import com.atexo.redaction.domaines.document.services.GabaritService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class LongToGabaritConverter implements ConditionalGenericConverter {
	@NonNull
	private GabaritService gabaritService;

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return sourceType.getType() == Long.class;
	}

	@Override
	public Set<GenericConverter.ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new GenericConverter.ConvertiblePair(Long.class, Gabarit.class));
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		var id = (Long) source;
		return gabaritService.chercher(id).orElseThrow(() -> new GabaritIntrouvableException(id));
	}
}
