package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class GabaritView {
	private Long id;

	private UUID uuid;

	private ModeleView modele;

	private ServiceView service;

	private OrganismeView organisme;

	private UtilisateurView proprietaire;

	private String chemin;

	private String nom;

	private ReferentielView niveau;

	@Builder.Default
	private boolean actif = false;

	private CreateAuditView creation;

	private UpdateAuditView modification;
}
