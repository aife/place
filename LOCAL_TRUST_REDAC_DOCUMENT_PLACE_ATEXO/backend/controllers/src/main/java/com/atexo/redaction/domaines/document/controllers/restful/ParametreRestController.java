package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.ParametreController;
import com.atexo.redaction.domaines.document.controllers.views.ParametreView;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Parametre;
import com.atexo.redaction.domaines.document.services.ParametreService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/parametres")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "parametres")
public class ParametreRestController extends BaseRestController implements ParametreController {

	@NonNull
	private ParametreService parametreService;

	@NonNull
	private Convertible<Parametre, ParametreView> parametreViewMapper;

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public Set<ParametreView> recuperer(
			@RequestParam("environnement") final Environnement environnement
	) {
		return this.parametreViewMapper.map(this.parametreService.recuperer(environnement));
	}

}
