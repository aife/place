package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.LotView;
import com.atexo.redaction.domaines.document.model.Lot;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface LotViewMapper extends Convertible<Lot, LotView> {
}
