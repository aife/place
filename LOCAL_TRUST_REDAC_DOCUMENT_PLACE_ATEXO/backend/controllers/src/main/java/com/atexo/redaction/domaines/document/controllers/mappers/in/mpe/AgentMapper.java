package com.atexo.redaction.domaines.document.controllers.mappers.in.mpe;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.mpe.v2.Agent;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;

import javax.transaction.Transactional;

@Mapper
@Slf4j
public abstract class AgentMapper implements Convertible<Agent, Utilisateur> {

	@Setter(onMethod = @__(@Autowired))
	private Convertible<com.atexo.redaction.domaines.document.mpe.v2.Agent, Environnement> environnementMapper;

	@Setter(onMethod = @__(@Autowired))
	private Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Organisme, Environnement>, Organisme> organismeMapper;

	@Setter(onMethod = @__(@Autowired))
	private Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Service, Organisme>, Service> serviceMapper;

	@Setter(onMethod = @__(@Autowired))
	private Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Agent, Service>, Utilisateur> utilisateurMapper;

	@Override
	@Transactional
	public Utilisateur map(Agent agent) {
		// -------------
		// Environnement
		// -------------
		final var environnement = this.environnementMapper.map(agent);

		// ---------
		// Organisme
		// ---------
		final var organisme = this.organismeMapper.map(Pair.of(agent.getOrganisme(), environnement));

		// -------
		// Service
		// -------
		final var service = this.serviceMapper.map(Pair.of(agent.getService(), organisme));

		// -----------
		// Utilisateur
		// -----------
		return this.utilisateurMapper.map(Pair.of(agent, service));
	}
}
	
