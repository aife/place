package com.atexo.redaction.domaines.document.controllers.mappers.out.custom;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.web.Location;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Value;

@Mapper
@Named("uriMapper")
public abstract class UriMapper implements Convertible<Contexte, Location> {

	@Value("${frontend.base.url}/front")
	private Location frontendURL;

	@Override
	public Location map(Contexte contexte) {
		return frontendURL;
	}
}
	
