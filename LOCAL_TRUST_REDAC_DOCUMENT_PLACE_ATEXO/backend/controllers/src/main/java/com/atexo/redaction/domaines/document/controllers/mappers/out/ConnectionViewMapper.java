package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ConnectionView;
import com.atexo.redaction.domaines.document.services.ws.Connection;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ConnectionViewMapper extends Convertible<Connection, ConnectionView> {
	@Override
	ConnectionView map(Connection connection);

}
