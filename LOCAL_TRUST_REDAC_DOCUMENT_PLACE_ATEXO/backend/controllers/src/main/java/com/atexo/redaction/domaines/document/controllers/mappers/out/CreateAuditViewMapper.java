package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.CreateAuditView;
import com.atexo.redaction.domaines.document.controllers.views.UtilisateurView;
import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(uses = {UtilisateurViewMapper.class, ServiceViewMapper.class})
public interface CreateAuditViewMapper extends Convertible<CreateAudit, CreateAuditView> {

	@Override
	CreateAuditView map(CreateAudit audit);
}
