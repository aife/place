package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "with")
public class DestinataireView {
	private String uuid;

	private String nom;

	private String prenom;

	private String email;

	private String service;

	private EnvironnementView environnement;
}
