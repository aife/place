package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.UpdateAuditView;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface UpdateAuditViewMapper extends Convertible<UpdateAudit, UpdateAuditView> {

	UpdateAuditView map(UpdateAudit audit);
}
