package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.model.TypeDocument;
import com.atexo.redaction.domaines.document.services.ConversionService;
import com.atexo.redaction.domaines.document.services.StockageTemporaireService;
import com.atexo.redaction.domaines.document.shared.patterns.Enregistrable;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@Component
@Slf4j
@ApiIgnore
public abstract class BaseRestController {

    @Deprecated
    public static final String HEADER_VALUE_V1_JSON = "application/atexo.redacdocument.v1+json";

    @Deprecated
    public static final String HEADER_VALUE_V1_XML = "application/atexo.redacdocument.v1+xml";

    public static final String HEADER_VALUE_V2_JSON = "application/atexo.redacdocument.v2+json";

    public static final String HEADER_VALUE_V2_XML = "application/atexo.redacdocument.v2+xml";

    protected static final String HEADER_ACCEPT_JSON = HttpHeaders.ACCEPT + "=" + MediaType.APPLICATION_JSON_VALUE;

    protected static final String HEADER_ACCEPT_JSON_V2 = HttpHeaders.ACCEPT + "=" + HEADER_VALUE_V2_JSON;

    protected static final String HEADER_ACCEPT_XML_V2 = HttpHeaders.ACCEPT + "=" + HEADER_VALUE_V2_XML;

    @Setter(onMethod = @__(@Autowired))
    private ConversionService conversionService;

    @Setter(onMethod = @__(@Autowired))
    private StockageTemporaireService stockageTemporaireService;

    protected static final String CACHE_CONTROL = "must-revalidate, post-check=0, pre-check=0";

    protected static final Map<String, String> MIMEYPES = Map.of(
            "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "docx", "application/vnd.openxmlformats-officedocument.wordprocessing",
            "pdf", "application/pdf",
            "default", "application/octet-stream"
    );

    protected ResponseEntity<Resource> telecharger(final Enregistrable enregistrable, final TypeDocument type, final ContentDisposition contentDisposition, final boolean mustConvert) {
        final String extension;

        final Resource ressource;
        if (null == type || type.getExtension().equals(enregistrable.getExtension())) {
            log.info("Export de l'enregistrable {} dans son type d'origine. Pas de conversion", enregistrable);
            extension = enregistrable.getExtension();
            if (!mustConvert) {
                ressource = new FileSystemResource(enregistrable.getFichier());
            } else {
                ressource = this.conversionService.convertir(this.stockageTemporaireService.copier(enregistrable), extension);
            }
        } else {
            log.info("Export de l'enregistrable {} avec conversion au format {}", enregistrable, type);
            extension = type.getExtension();
            ressource = this.conversionService.convertir(this.stockageTemporaireService.copier(enregistrable), extension);
        }


        return this.repondre(ressource, enregistrable.getNom(), extension, contentDisposition);
    }

    protected ResponseEntity<Resource> repondre(final Resource ressource, final String nom, final String extension, final ContentDisposition contentDisposition) {

        final var headers = new org.springframework.http.HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(MIMEYPES.get(extension)));
        headers.setContentDisposition(
                org.springframework.http.ContentDisposition
                        .builder(null == contentDisposition ? ContentDisposition.ATTACHMENT.getLibelle() : contentDisposition.getLibelle())
                        .filename(nom + "." + extension).build());
        headers.setCacheControl(CACHE_CONTROL);

        return ResponseEntity.ok().headers(headers).body(ressource);
    }
}
