package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.TypeDocument;
import com.atexo.redaction.domaines.document.model.exceptions.api.referentiel.TypeDocumentNonSupporteException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StringToTypeDocumentConverter implements Converter<String, TypeDocument> {

	@Override
	public TypeDocument convert(final String value) {
		return TypeDocument.rechercher(value).orElseThrow(() -> new TypeDocumentNonSupporteException(value));
	}
}
