package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class UpdateAuditView {

	private UtilisateurView utilisateur;

	private Instant date;

	private String commentaire;
}

