package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.mappers.out.custom.UriMapper;
import com.atexo.redaction.domaines.document.controllers.views.DocumentView;
import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.apache.commons.io.FilenameUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(uses = {ModeleViewMapper.class, ContexteViewMapper.class, UriMapper.class, CanevasViewMapper.class, CreateAuditViewMapper.class, UpdateAuditViewMapper.class })
public interface DocumentViewMapper extends Convertible<Document, DocumentView> {

	@Override
	@Mapping(target = "taille", ignore = true)
	@Mapping(target = "nom", source = "nom", qualifiedByName = "toNomFichier")
	DocumentView map(Document document);

	@Named("toNomFichier")
	static String toNomFichier(String nom) {
		return FilenameUtils.getBaseName(nom);
	}
}
