package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class ParametreView implements Serializable {

	@NonNull
	private EnvironnementView environnement;

	@NonNull
	private String cle;

	@NonNull
	private String valeur;
}
