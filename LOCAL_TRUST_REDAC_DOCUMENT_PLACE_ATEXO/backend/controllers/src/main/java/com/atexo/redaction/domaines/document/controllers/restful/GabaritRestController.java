package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.GabaritController;
import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Account;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Context;
import com.atexo.redaction.domaines.document.controllers.views.GabaritView;
import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.exceptions.api.gabarit.GabaritIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.gabarit.GabaritRevisionIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.referentiel.ModeleIntrouvableException;
import com.atexo.redaction.domaines.document.model.messages.gabarits.CreationGabaritMessage;
import com.atexo.redaction.domaines.document.model.messages.gabarits.SuppressionGabaritMessage;
import com.atexo.redaction.domaines.document.model.referentiels.Niveau;
import com.atexo.redaction.domaines.document.services.*;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.atexo.redaction.domaines.document.model.referentiels.Niveau.*;

@RestController
@RequestMapping("/gabarits")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "gabarits")
@Slf4j
public class GabaritRestController extends BaseRestController implements GabaritController {

	@NonNull
	private final ModeleService modeleService;

	@NonNull
	private final GabaritService gabaritService;

	@NonNull
	private final Convertible<Gabarit, GabaritView> gabaritViewMapper;

	@NonNull
	private final StockageTemporaireService stockageTemporaireService;

	@NonNull
	private final StockageGabaritService stockageGabaritService;

	@NonNull
	private final MessageGabaritService messageGabaritService;

	@Override
	@GetMapping(params = "filtre=eligible", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public GabaritView recupererVersionEligible(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("modele") Modele modele,
			final @RequestParam("environnement") Environnement environnement
	) {
		final var surcharge = this.gabaritService.rechercher(environnement, utilisateur, modele).orElseThrow(() -> {
			log.error("Gabarit introuvable pour le type de document '{}', l'environnement '{}' et l'agent '{}'", modele.getType().getCode(), environnement.getUuid(), utilisateur.getNomComplet());

			return new GabaritIntrouvableException(modele);
		});

		return this.gabaritViewMapper.map(surcharge);
	}

	@Override
	@GetMapping(params = "niveau=AGENT", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public GabaritView recuperer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("modele") Modele modele,
			final @RequestParam("agent") Utilisateur agent
	) {
		log.debug("Recherche de la surcharge de gabarits pour le modele {} et l'utilisateur '{}'", modele.getType().getCode(), utilisateur.getNomComplet());

		return this.gabaritService.recuperer(modele, agent).map(this.gabaritViewMapper::map).orElse(null);
	}

	@Override
	@GetMapping(params = "niveau=ENTITE_ACHAT", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public GabaritView recuperer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("modele") Modele modele,
			final @RequestParam("service") Service service
	) {
		log.debug("Recherche de la surcharge de gabarits pour le modele {} et le service {}", modele.getType().getCode(), service.getIdentifiantExterne());

		return this.gabaritService.recuperer(modele, service).map(this.gabaritViewMapper::map).orElse(null);
	}

	@Override
	@GetMapping(params = "niveau=ORGANISME", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public GabaritView recuperer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("modele") Modele modele,
			final @RequestParam("organisme") Organisme organisme
	) {
		log.debug("Recherche de la surcharge de gabarits pour le modele {} et l'organisme {}", modele.getType().getCode(), organisme.getAcronyme());

		return this.gabaritService.recuperer(modele, organisme).map(this.gabaritViewMapper::map).orElse(null);
	}

	@Override
	@GetMapping(params = "niveau=PLATEFORME", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public GabaritView recuperer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("modele") Modele modele,
			final @RequestParam("environnement") Environnement environnement
	) {
		log.debug("Recherche de la surcharge de gabarits pour l'environnement {} et le modele {}", environnement, modele.getType().getCode());

		return this.gabaritService.recuperer(modele, environnement).map(this.gabaritViewMapper::map).orElse(null);
	}

	@Override
	@PostMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@CacheEvict(allEntries = true)
	public GabaritView creer(
			@Account final Utilisateur utilisateur,
			@RequestParam("contexte") final Contexte contexte,
			@RequestBody final GabaritView surchargeDTO
	) {
		final var modele = this.modeleService.chercher(surchargeDTO.getModele().getUuid()).orElseThrow(() -> new ModeleIntrouvableException(surchargeDTO.getModele().getUuid()));

		final var service = utilisateur.getService();
		final var organisme = service.getOrganisme();
		final var niveauEnum = surchargeDTO.getNiveau().getCode();
		final var uuid = surchargeDTO.getUuid();

		Gabarit surcharge;
		switch (niveauEnum) {
			case AGENT: {
				surcharge = this.gabaritService.rechercher(modele, utilisateur).orElseGet(() -> this.gabaritService.creer(AGENT, utilisateur));
				surcharge.setProprietaire(utilisateur);
				break;
			}
			case ENTITE_ACHAT: {
				surcharge = this.gabaritService.rechercher(modele, service).orElseGet(() -> this.gabaritService.creer(ENTITE_ACHAT, utilisateur));
				surcharge.setService(service);
				break;
			}
			case ORGANISME: {
				surcharge = this.gabaritService.rechercher(modele, organisme).orElseGet(() -> this.gabaritService.creer(ORGANISME, utilisateur));
				surcharge.setOrganisme(organisme);
				break;
			}
			case PLATEFORME: {
				surcharge = this.gabaritService.rechercher(modele, contexte.getEnvironnement()).orElseGet(() -> this.gabaritService.creer(Niveau.PLATEFORME, utilisateur));
				break;
			}
			default:
				throw new IllegalArgumentException("aucune surcharge trouvée");
		}

		// Récupération du fichier uploadé
		final var tempResource = this.stockageTemporaireService.lire(uuid);
		// Sauvegarde
		final var to = this.stockageGabaritService.sauvegarder(tempResource, uuid);

		surcharge.setUuid(uuid);
		surcharge.setCreation(CreateAudit.with().utilisateur(utilisateur).date(Instant.now()).build());
		surcharge.setModification(UpdateAudit.with().utilisateur(utilisateur).date(Instant.now()).build());
		surcharge.setChemin(to.toString());
		surcharge.setModele(modele);
		surcharge.setNom(surchargeDTO.getNom());
		surcharge.setEnvironnement(contexte.getEnvironnement());
		surcharge = this.gabaritService.sauvegarder(surcharge);

		// Notification de la suppression
		this.messageGabaritService.notifier(CreationGabaritMessage.with()
				.gabarit(surcharge)
				.utilisateur(utilisateur)
				.contexte(contexte)
				.build());

		return this.gabaritViewMapper.map(surcharge);
	}

	@Override
	@GetMapping(value = "/{gabarit}/revisions", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GabaritView> lister(
			@PathVariable("gabarit") final Gabarit gabarit
	) {
		return this.gabaritService.recupererRevisions(gabarit)
				.stream()
				.map(Revision::getVersion)
				.map(this.gabaritViewMapper::map)
				.sorted(Comparator.comparing((GabaritView view) -> view.getCreation().getDate()).reversed())
				.collect(Collectors.toList());
	}

	@Override
	@DeleteMapping(value = "/{gabarit}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@CacheEvict(allEntries = true)
	public void supprimer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @PathVariable("gabarit") Gabarit gabarit
	) {

		this.gabaritService.supprimer(gabarit);

		// Notification de la suppression
		this.messageGabaritService.notifier(SuppressionGabaritMessage.with()
				.gabarit(gabarit)
				.utilisateur(utilisateur)
				.contexte(contexte)
				.build());
	}

	@Override
	@GetMapping(value = "/{gabarit}", params = "action=telechargement")
	public ResponseEntity<Resource> telecharger(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			@PathVariable("gabarit") final Gabarit gabarit,
			@RequestParam(name = "format", defaultValue = "pdf") final TypeDocument type,
			final @RequestParam(name = "disposition", required = false) ContentDisposition disposition
	) {
		return super.telecharger(gabarit, type, disposition, false);
	}

	@Override
	@GetMapping(value = "/{gabarit}/revisions/{uuid}", params = "action=telechargement")
	public ResponseEntity<Resource> telecharger(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			@PathVariable("gabarit") final Gabarit gabarit,
			final @PathVariable("uuid") UUID uuid,
			@RequestParam(name = "format", defaultValue = "pdf") final TypeDocument type,
			final @RequestParam(name = "disposition", required = false) ContentDisposition disposition
	) {
		final var watch = new StopWatch();

		final var revision = this.gabaritService.rechercherRevision(uuid).map(Revision::getVersion).orElseThrow(() -> new GabaritRevisionIntrouvableException(uuid, gabarit));

		if (!revision.getId().equals(gabarit.getId())) {
			throw new GabaritRevisionIntrouvableException(uuid, gabarit);
		}

		watch.start("Telechargement du gabarit " + revision.getUuid());

		log.info("Demande de téléchargement du gabarit avec uuid = {}", revision.getUuid());

		final var resource = super.telecharger(revision, type, disposition, false);

		log.info("Fin du téléchargement du gabarit avec uuid = {} (temps de téléchargement en milliseconds = {})", revision.getUuid(), watch.getTotalTimeMillis());

		watch.stop();

		return resource;
	}
}
