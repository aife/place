package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "with")
public class ModeleView {
	private Long id;

	private UUID uuid;

	private String libelle;

	private String nomFichier;

	private ReferentielView type;

	private int ordre;

	private Boolean administrable;

	private boolean actif;
}
