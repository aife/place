package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.DestinataireView;
import com.atexo.redaction.domaines.document.model.Destinataire;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface DestinataireModelMapper extends Convertible<DestinataireView, Destinataire> {

	@Override
	@Mapping(source = "uuid", target = "identifiantExterne")
	@Mapping(target = "destinataireNotifications", ignore = true)
	@Mapping(target = "lien", ignore = true)
	@Mapping(target = "defautUri", ignore = true)
	Destinataire map(DestinataireView destinataire);
}
