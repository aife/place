package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.NaturePrestationView;
import com.atexo.redaction.domaines.document.model.NaturePrestation;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface NaturePrestationViewMapper extends Convertible<NaturePrestation, NaturePrestationView> {
}
