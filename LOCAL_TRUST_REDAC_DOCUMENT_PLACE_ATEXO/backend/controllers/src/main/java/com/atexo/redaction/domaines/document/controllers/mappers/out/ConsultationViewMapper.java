package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ConsultationView;
import com.atexo.redaction.domaines.document.model.Consultation;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(uses = {ProcedureViewMapper.class, OrganismeViewMapper.class, ServiceViewMapper.class, StatutCanevasViewMapper.class})
public interface ConsultationViewMapper extends Convertible<Consultation, ConsultationView> {
}
