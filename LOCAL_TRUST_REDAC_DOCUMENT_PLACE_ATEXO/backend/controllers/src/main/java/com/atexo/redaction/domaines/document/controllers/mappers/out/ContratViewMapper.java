package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ContratView;
import com.atexo.redaction.domaines.document.model.Contrat;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ContratViewMapper extends Convertible<Contrat, ContratView> {
}
