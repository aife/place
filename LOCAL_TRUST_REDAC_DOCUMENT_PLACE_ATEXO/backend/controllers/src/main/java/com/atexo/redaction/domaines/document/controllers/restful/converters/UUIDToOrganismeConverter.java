package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.model.exceptions.api.organisme.OrganismeIntrouvableException;
import com.atexo.redaction.domaines.document.services.OrganismeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToOrganismeConverter implements Converter<String, Organisme> {

	@NonNull
	private OrganismeService organismeService;

	@Override
	public Organisme convert(final String value) {
		var uuid = UUID.fromString(value);
		return organismeService.chercher(uuid).orElseThrow(() -> new OrganismeIntrouvableException(uuid));

	}
}
