package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.EnvironnementController;
import com.atexo.redaction.domaines.document.controllers.views.EnvironnementView;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.exceptions.api.environnement.EnvironnementIntrouvableException;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/environnements")
@RequiredArgsConstructor
@Slf4j
@CacheConfig(cacheNames = "environnements")
public class EnvironnementRestController extends BaseRestController implements EnvironnementController {

	@NonNull
	private EnvironnementService environnementService;

	@NonNull
	private Convertible<Environnement, EnvironnementView> environnementViewMapper;

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public EnvironnementView recuperer(
			@RequestParam("client") final String client,
			@RequestParam("plateforme") final String plateforme,
			@RequestParam(value = "mpeUuid",required = false) final String mpeUuid

	) {
		return environnementService.chercher(client, plateforme, mpeUuid)
				.map(environnementViewMapper::map)
				.orElseThrow(() -> new EnvironnementIntrouvableException(client, plateforme));
	}
}
