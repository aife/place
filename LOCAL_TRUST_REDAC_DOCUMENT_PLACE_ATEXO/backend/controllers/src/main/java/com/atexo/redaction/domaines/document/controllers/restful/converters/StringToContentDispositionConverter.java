package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.model.exceptions.api.media.ContentDispositionInvalideException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StringToContentDispositionConverter implements Converter<String, ContentDisposition> {

	@Override
	public ContentDisposition convert(final String value) {
		return ContentDisposition.rechercher(value).orElseThrow(() -> new ContentDispositionInvalideException(value));
	}
}
