package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.StatutCanevasView;
import com.atexo.redaction.domaines.document.model.StatutCanevas;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface StatutCanevasModelMapper extends Convertible<StatutCanevasView, StatutCanevas> {
}
