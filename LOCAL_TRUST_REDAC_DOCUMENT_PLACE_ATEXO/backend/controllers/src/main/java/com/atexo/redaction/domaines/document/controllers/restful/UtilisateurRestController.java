package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.UtilisateurController;
import com.atexo.redaction.domaines.document.controllers.views.UtilisateurView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.HabilitationsIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionClienteObsoleteException;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionLocaleObsoleteException;
import com.atexo.redaction.domaines.document.mpe.v2.Agent;
import com.atexo.redaction.domaines.document.services.ClausierService;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import com.atexo.redaction.domaines.document.services.VersionService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/utilisateurs")
@RequiredArgsConstructor
@Slf4j
@CacheConfig(cacheNames = "utilisateurs")
public class UtilisateurRestController extends BaseRestController implements UtilisateurController {

	@NonNull
	private final UtilisateurService utilisateurService;

	@NonNull
	private final Convertible<Utilisateur, UtilisateurView> utilisateurViewMapper;

	@NonNull
	private final SerialisationService serialisationService;

	@NonNull
	private final ClausierService clausierService;

	@NonNull
	private final VersionService versionService;

	@NonNull
	private final Convertible<Agent, Utilisateur> utilisateurMapper;

	@Override
	@PostMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@CacheEvict
	public UtilisateurView creer(@RequestBody final com.atexo.redaction.domaines.document.mpe.v2.Agent agent) {
		final var release = agent.getEnvironnement().getVersion();

		if (null != release) {
			final var version = new Version(release);

			if (this.versionService.estTropAncienne(version)) {
				throw new VersionClienteObsoleteException(version);
			}

			if (this.versionService.estTropRecente(version)) {
				throw new VersionLocaleObsoleteException(version);
			}
		} else {
			log.warn("Aucune version n'est fournie, pas de contrôle de version");
		}

		return this.utilisateurViewMapper.map(utilisateurMapper.map(agent));
	}

	@Override
	@GetMapping(value = "/{utilisateur}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurView recuperer(
			@PathVariable("utilisateur") final UUID uuid
	) {
		return this.utilisateurViewMapper.map(this.utilisateurService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid)));
	}

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<UtilisateurView> rechercher(
			@RequestParam("contexte") final Contexte contexte
	) {
		return this.utilisateurViewMapper.map(this.clausierService.rechercher(contexte));
	}
}
