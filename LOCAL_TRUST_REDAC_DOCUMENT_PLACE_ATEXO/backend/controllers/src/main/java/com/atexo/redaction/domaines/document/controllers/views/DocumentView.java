package com.atexo.redaction.domaines.document.controllers.views;

import com.atexo.redaction.domaines.document.model.Action;
import com.atexo.redaction.domaines.document.model.Statut;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class DocumentView {

	private Long id;

	@Builder.Default
	private Action action = Action.INCONNU;

	@Builder.Default
	private Statut statut = Statut.BROUILLON;

	private Long taille;

	private ModeleView modele;

	private ContexteView contexte;

	private CanevasView canevas;

	private LotView lot;

	private String nom;

	private String extension;

	private UUID uuid;

	private boolean actif;

	@Builder.Default
	private Double progression = 0d;

	private boolean ouvert;

	private CreateAuditView creation;

	private UpdateAuditView modification;

}
