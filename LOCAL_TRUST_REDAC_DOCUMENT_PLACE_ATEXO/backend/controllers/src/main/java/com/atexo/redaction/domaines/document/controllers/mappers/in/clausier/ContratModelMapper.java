package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.mappers.out.ContratViewMapper;
import com.atexo.redaction.domaines.document.model.Contrat;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ContratModelMapper extends Convertible<ContratViewMapper, Contrat> {
}
