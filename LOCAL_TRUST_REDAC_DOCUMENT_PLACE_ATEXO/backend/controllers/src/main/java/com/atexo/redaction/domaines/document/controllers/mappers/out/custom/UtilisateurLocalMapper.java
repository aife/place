package com.atexo.redaction.domaines.document.controllers.mappers.out.custom;

import com.atexo.redaction.domaines.document.controllers.mappers.out.UtilisateurViewMapper;
import com.atexo.redaction.domaines.document.controllers.views.UtilisateurView;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.UUID;

@Mapper
@Named("utilisateurLocalMapper")
@Slf4j
public abstract class UtilisateurLocalMapper implements Convertible<UUID, UtilisateurView> {

	@Setter
	private UtilisateurService utilisateurService;

	@Setter
	private UtilisateurViewMapper utilisateurViewMapper;

	@Override
	public UtilisateurView map(UUID uuid) {
		return utilisateurViewMapper.map(utilisateurService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid)));
	}
}
	
