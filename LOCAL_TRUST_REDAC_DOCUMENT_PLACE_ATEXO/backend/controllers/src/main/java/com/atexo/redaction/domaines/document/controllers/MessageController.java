package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface MessageController {
	@PostMapping
	BaseMessage envoyer(@RequestParam("topic") String topic, @RequestBody BaseMessage message);
}
