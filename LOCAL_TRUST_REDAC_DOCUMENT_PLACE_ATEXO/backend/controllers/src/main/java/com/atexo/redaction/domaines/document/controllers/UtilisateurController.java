package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.UtilisateurView;
import com.atexo.redaction.domaines.document.model.Contexte;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Set;
import java.util.UUID;

public interface UtilisateurController {

	/**
	 * Permet de créer un utilisateur depuis l'agent MPE
	 *
	 * @param agent l'agent MPE
	 *
	 * @return l'utilisateur
	 */
	UtilisateurView creer(com.atexo.redaction.domaines.document.mpe.v2.Agent agent);

	/**
	 * Permet de récuperer un utilisateur depuis son identifiant
	 *
	 * @param uuid l'identifiant unique de l'utilisateur
	 *
	 * @return l'utilisateur
	 */
	UtilisateurView recuperer(UUID uuid);

	/**
	 * Permet de rechercher les agents d'un contexte
	 *
	 * @param contexte le contexte
	 *
	 * @return les utilisateurs
	 */
	Set<UtilisateurView> rechercher(Contexte contexte);
}
