package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ModeleView;
import com.atexo.redaction.domaines.document.model.Categorie;
import com.atexo.redaction.domaines.document.model.Contexte;

import java.util.List;

public interface ModeleController {

	/**
	 * Permet de récuperer la liste des modèles d'un contexte
	 *
	 * @param contexte le contexte
	 *
	 * @return la liste des modèles
	 */
	List<ModeleView> recuperer(Contexte contexte);

	/**
	 * Permet de récupérer la liste des modèles administrables
	 *
	 * @param contexte le contexte
	 *
	 * @return la liste des modèles
	 */
	List<ModeleView> recupererAdministrables(Contexte contexte);

	/**
	 * Permet de récuperer la lsite des modèles par type
	 *
	 * @param contexte le contexte
	 * @param categorie la catégorie de document
	 *
	 * @return la listes des modèles
	 */
	List<ModeleView> recupererParCategorie(Contexte contexte, Categorie categorie);
}
