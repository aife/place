package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.SessionController;
import com.atexo.redaction.domaines.document.controllers.mappers.out.ConnectionViewMapper;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Account;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Context;
import com.atexo.redaction.domaines.document.controllers.views.ConnectionView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.services.ws.Connection;
import com.atexo.redaction.domaines.document.services.ws.ConnectionHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/sessions")
@RequiredArgsConstructor
@Slf4j
public class SessionRestController extends BaseRestController implements SessionController {

	@NonNull
	private final ConnectionHolder connectionHolder;

	@NonNull
	private final ConnectionViewMapper connectionViewMapper;

	@Override
	@GetMapping(params = "action=connect", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public ConnectionView connect(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @RequestParam("environnement") Environnement environnement
	) {
		return this.connectionViewMapper.map(this.connectionHolder.add(environnement, utilisateur));
	}

	@Override
	@GetMapping(params = "action=disconnect", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public ConnectionView disconnect(
			final @Account Utilisateur utilisateur
	) {
		return this.connectionHolder.remove(utilisateur).map(this.connectionViewMapper::map).orElse(null);
	}

	@Override
	@GetMapping(params = "filtre=actif", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ConnectionView> lister() {
		return this.connectionViewMapper.map(
				this.connectionHolder.getConnections()
						.stream()
						.sorted(Comparator.comparing(Connection::getCreation))
						.collect(Collectors.toList())
		);
	}
}
