package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ConnectionView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Utilisateur;

import java.util.List;

public interface SessionController {

	/**
	 * Permet de connecter un agent
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param environnement l'environnement
	 *
	 * @return la connection
	 */
	ConnectionView connect(Utilisateur utilisateur, Contexte contexte, Environnement environnement);

	/**
	 * Permet de déconnecter un agent
	 *
	 * @return l'agent connecté
	 */
	ConnectionView disconnect(Utilisateur utilisateur);

	/**
	 * Permet de lister les agents connectés
	 *
	 * @return les utilisateurs connectés
	 */
	List<ConnectionView> lister();
}
