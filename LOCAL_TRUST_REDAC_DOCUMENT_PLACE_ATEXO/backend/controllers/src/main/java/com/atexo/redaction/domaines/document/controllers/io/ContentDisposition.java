package com.atexo.redaction.domaines.document.controllers.io;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum ContentDisposition {
	ATTACHMENT("attachment"),
	INLINE("inline");

	@NonNull
	private final String libelle;

	public static Optional<ContentDisposition> rechercher(String libelle) {
		return Arrays.stream(values()).filter(type -> type.getLibelle().equals(libelle)).findFirst();
	}

}
