package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.services.UtilisateurService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToUtilisateurConverter implements Converter<String, Utilisateur> {

	@NonNull
	private UtilisateurService utilisateurService;

	@Override
	public Utilisateur convert(final String value) {
		var uuid = UUID.fromString(value);

		return utilisateurService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid));

	}
}
