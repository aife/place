package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.mappers.out.CPVViewMapper;
import com.atexo.redaction.domaines.document.controllers.views.LotView;
import com.atexo.redaction.domaines.document.model.Lot;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = { CPVViewMapper.class })
public interface LotModelMapper extends Convertible<LotView, Lot> {
}
