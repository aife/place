package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.ModeleController;
import com.atexo.redaction.domaines.document.controllers.views.ModeleView;
import com.atexo.redaction.domaines.document.model.Categorie;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Modele;
import com.atexo.redaction.domaines.document.services.ModeleService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/modeles")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "modeles")
public class ModeleRestController extends BaseRestController implements ModeleController {

	@NonNull
	private final ModeleService modeleService;

	@NonNull
	private final Convertible<Modele, ModeleView> modeleViewMapper;

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public List<ModeleView> recuperer(
			@RequestParam("contexte") final Contexte contexte
	) {
		return modeleViewMapper.map(modeleService.recuperer(contexte.getEnvironnement()));
	}

	@Override
	@GetMapping(params = "filtre=administrable", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public List<ModeleView> recupererAdministrables(
			@RequestParam("contexte") final Contexte contexte
	) {
		return modeleViewMapper.map(modeleService.recupererAdministrables(contexte.getEnvironnement()));
	}

	@GetMapping(params = "filtre=categorie", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public List<ModeleView> recupererParCategorie(
			@RequestParam("contexte") final Contexte contexte,
			@RequestParam("categorie") final Categorie categorie
	) {
		return modeleViewMapper.map(modeleService.recupererParCategorie(contexte.getEnvironnement(), categorie));
	}
}
