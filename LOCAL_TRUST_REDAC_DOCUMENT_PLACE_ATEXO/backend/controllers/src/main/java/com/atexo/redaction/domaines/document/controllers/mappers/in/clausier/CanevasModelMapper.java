package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.CanevasView;
import com.atexo.redaction.domaines.document.model.Canevas;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface CanevasModelMapper extends Convertible<CanevasView, Canevas> {
}
