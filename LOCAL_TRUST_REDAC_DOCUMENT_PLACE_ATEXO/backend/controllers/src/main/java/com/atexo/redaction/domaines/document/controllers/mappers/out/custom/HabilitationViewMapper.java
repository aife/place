package com.atexo.redaction.domaines.document.controllers.mappers.out.custom;

import com.atexo.redaction.domaines.document.model.Habilitation;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.services.HabilitationService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper
@Named("habilitationMapper")
public abstract class HabilitationViewMapper implements Convertible<Utilisateur, Set<String>> {

	@Setter(onMethod = @__(@Autowired))
	private HabilitationService habilitationService;

	@Override
	public Set<String> map(Utilisateur utilisateur) {
		return this.habilitationService.extraire(utilisateur)
				.stream()
				.map(Habilitation::getValeur)
				.collect(Collectors.toSet()
				);
	}
}
	
