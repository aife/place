package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ServiceView;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {OrganismeViewMapper.class})
public interface ServiceViewMapper extends Convertible<Service, ServiceView> {

	@Override
	@Mapping(target = "parent", ignore = true)
	ServiceView map(Service service);
}
