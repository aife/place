package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.ProcedureView;
import com.atexo.redaction.domaines.document.model.Procedure;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ProcedureModelMapper extends Convertible<ProcedureView, Procedure> {
}
