package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Document;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentIntrouvableException;
import com.atexo.redaction.domaines.document.services.DocumentService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class LongToDocumentConverter implements ConditionalGenericConverter {

	@NonNull
	private DocumentService documentService;

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return sourceType.getType() == Long.class;
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new ConvertiblePair(Long.class, Document.class));
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		var id = (Long) source;
		return documentService.chercher(id).orElseThrow(() -> new DocumentIntrouvableException(id));
	}
}
