package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Modele;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurIntrouvableException;
import com.atexo.redaction.domaines.document.services.ModeleService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToModeleConverter implements Converter<String, Modele> {

	@NonNull
	private ModeleService modeleService;

	@Override
	public Modele convert(final String value) {
		var uuid = UUID.fromString(value);
		return modeleService.chercher(uuid).orElseThrow(() -> new UtilisateurIntrouvableException(uuid));

	}
}
