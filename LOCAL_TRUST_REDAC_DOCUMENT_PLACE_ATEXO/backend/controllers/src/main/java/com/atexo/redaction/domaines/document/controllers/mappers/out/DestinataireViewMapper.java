package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.DestinataireView;
import com.atexo.redaction.domaines.document.model.Destinataire;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface DestinataireViewMapper extends Convertible<Destinataire, DestinataireView> {
	@Override
	@Mapping(target = "service", ignore = true)
	@Mapping(source = "identifiantExterne", target = "uuid")
	DestinataireView map(Destinataire bo);
}
