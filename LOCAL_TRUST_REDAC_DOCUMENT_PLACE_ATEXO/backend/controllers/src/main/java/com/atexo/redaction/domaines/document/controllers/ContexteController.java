package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ContexteView;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.mpe.v2.Contexte;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface ContexteController {


	/**
	 * Permet de creer un contexte depuis le contexte MPE
	 *
	 * @param contexte le contexte MPE
	 *
	 * @return le contexte
	 */
	ContexteView creer(com.atexo.redaction.domaines.document.mpe.v2.Contexte contexte);

	/**
	 * Permet de récupérer un contexte
	 *
	 * @param uuid l'identifiant unique du contexte
	 *
	 * @return le contexte
	 */
	ContexteView recuperer(UUID uuid);

	/**
	 * Permet de rechercher un contexte depuis l´environnement et la référence de la consultation
	 *
	 * @param environnement l'environnement
	 * @param reference la référence de la consultation
	 *
	 * @return le contexte
	 */
	ContexteView recuperer(Environnement environnement, String reference);
}
