package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.CanevasView;
import com.atexo.redaction.domaines.document.model.Contexte;

import java.util.Set;

public interface CanevasController {

	/**
	 * Permet de récupérer de la liste des canevas
	 *
	 * @param contexte le contexte
	 * @param type le type de document
	 * @param idLot l'identifiant du lot
	 *
	 * @return la liste des canevas
	 */
	Set<CanevasView> rechercher(Contexte contexte, String type, Integer idLot);
}
