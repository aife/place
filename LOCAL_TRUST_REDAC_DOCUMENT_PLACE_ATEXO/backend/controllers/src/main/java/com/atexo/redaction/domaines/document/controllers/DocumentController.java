package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.io.RechercheCritere;
import com.atexo.redaction.domaines.document.controllers.restful.BaseRestController;
import com.atexo.redaction.domaines.document.controllers.views.DestinataireView;
import com.atexo.redaction.domaines.document.controllers.views.DocumentView;
import com.atexo.redaction.domaines.document.controllers.views.RedirectionView;
import com.atexo.redaction.domaines.document.model.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface DocumentController {

	/**
	 * Permet de rechercher les documents d'un contexte
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param critere le critere de recherche
	 *
	 * @return la liste des documents
	 */
	List<DocumentView> rechercher(Utilisateur utilisateur, Contexte contexte, RechercheCritere critere);

	/**
	 * Permet de créer un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param view le document a créer
	 *
	 * @return le document créé
	 */
	DocumentView creer(Utilisateur utilisateur, Contexte contexte, DocumentView view);

	/**
	 * Permet de mettre à jour un documentDTO
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 * @param view le document a modifier
	 *
	 * @return le document mis à jour
	 */
	DocumentView modifier(Utilisateur utilisateur, Contexte contexte, Document document, DocumentView view);

	/**
	 * Permet de supprimer un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 */
	void supprimer(Utilisateur utilisateur, Contexte contexte, Document document);

	/**
	 * Permet d'ouvrir un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 *
	 * @return la redirection
	 */
	RedirectionView visualiser(Utilisateur utilisateur, Contexte contexte, Document document);

	/**
	 * Permet d'ouvrir un document en edition
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 *
	 * @return la redirection
	 */
	RedirectionView editer(Utilisateur utilisateur, Contexte contexte, Document document);

	/**
	 * Permet de dupliquer un documentDTO
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 *
	 * @return le documentDTO éditable
	 */
	DocumentView dupliquer(Utilisateur utilisateur, Contexte contexte, Document document);

	/**
	 * Permet de convertir un document
	 *
	 * @param document le document
	 * @param type le type du document
	 * @param disposition la disposition du fichier en sortie
	 *
	 * @return la fichier
	 */
	ResponseEntity<Resource> telecharger(Document document, TypeDocument type, ContentDisposition disposition);

	/**
	 * Permet de récuperer une révision particulière d'un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 * @param uuid l'identifiant de la revision
	 */
	DocumentView recuperer(Utilisateur utilisateur, Contexte contexte, Document document, UUID uuid);

	/**
	 * Permet de récupérer la dernière version d'un document qui correspond au statut
	 *
	 * @param document le document
	 * @param statut le statut a filter
	 *
	 * @return le document
	 */
	DocumentView recuperer(Document document, Statut statut);

	/**
	 * Permet de récupérer l'historique d'un document
	 *
	 * @param document le document
	 *
	 * @return la liste des documents/versions
	 */
	List<DocumentView> lister(Document document);

	/**
	 * Permet de télécharger une révision du document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document
	 * @param uuid l'identifiant de la revision
	 * @param contentDisposition la disposition du fichier
	 */
	ResponseEntity<Resource> telecharger(Utilisateur utilisateur, Contexte contexte, Document document, UUID uuid, TypeDocument type, ContentDisposition contentDisposition);

	/**
	 * Permet de soumettre le document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document actuel
	 * @param vue le document avec modifications
	 *
	 * @return la redirection
	 */
	DocumentView soumettre(Utilisateur utilisateur, Contexte contexte, Document document, DocumentView vue);

	/**
	 * Permet de valider un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document actuel
	 * @param type le type de sortie
	 * @param vue le document avec modifications
	 *
	 * @return le document validé
	 */
	DocumentView valider(Utilisateur utilisateur, Contexte contexte, Document document, TypeDocument type, DocumentView vue);

	/**
	 * Permet de valider un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document actuel
	 * @param destinataires les destinataires
	 *
	 * @return la redirection
	 */
	Set<DestinataireView> notifier(Utilisateur utilisateur, Contexte contexte, Document document, Set<DestinataireView> destinataires);

	/**
	 * Permet de dévalider un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document actuel
	 * @param vue le document avec modifications
	 *
	 * @return la redirection
	 */
	DocumentView insoumettre(Utilisateur utilisateur, Contexte contexte, Document document, DocumentView vue);

	/**
	 * Permet de dévalider un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param document le document actuel
	 * @param vue le document avec modifications
	 *
	 * @return la redirection
	 */
	DocumentView invalider(Utilisateur utilisateur, Contexte contexte, Document document, DocumentView vue);
}
