package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "with")
public class OrganismeView {
	private UUID uuid;
	private String libelle;
	private String acronyme;
	private EnvironnementView environnement;
}
