package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.views.ContexteView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Version;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionClienteObsoleteException;
import com.atexo.redaction.domaines.document.model.exceptions.api.version.VersionLocaleObsoleteException;
import com.atexo.redaction.domaines.document.services.ContexteService;
import com.atexo.redaction.domaines.document.services.EnvironnementService;
import com.atexo.redaction.domaines.document.services.VersionService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/contextes")
@RequiredArgsConstructor
@Slf4j
@CacheConfig(cacheNames = "contextes")
public class ContexteRestController extends BaseRestController implements com.atexo.redaction.domaines.document.controllers.ContexteController {

	@NonNull
	private final ContexteService contexteService;

	@NonNull
	private final EnvironnementService environnementService;

	@NonNull
	private final Convertible<Contexte, ContexteView> contexteViewMapper;

	@NonNull
	private final SerialisationService serialisationService;

	@NonNull
	private final VersionService versionService;

	@Override
	@PostMapping(headers = {HEADER_ACCEPT_JSON_V2, HEADER_ACCEPT_XML_V2}, produces = MediaType.APPLICATION_JSON_VALUE)
	@CacheEvict
	public ContexteView creer(@RequestBody final com.atexo.redaction.domaines.document.mpe.v2.Contexte contexte) {
		final var release = contexte.getEnvironnement().getVersion();
		final var consultation = contexte.getConsultation();
		final var plateforme = contexte.getEnvironnement().getPlateforme();
		final var client = contexte.getEnvironnement().getClient();
		final var mpeUuid = contexte.getEnvironnement().getMpeUuid();
		final var json = serialisationService.serialise(contexte);

		// ----------
		// Version
		// ----------
		if (null != release) {
			var version = new Version(release);

			if (versionService.estTropAncienne(version)) {
				throw new VersionClienteObsoleteException(version);
			}

			if (versionService.estTropRecente(version)) {
				throw new VersionLocaleObsoleteException(version);
			}
		} else {
			log.warn("Aucune version n'est fournie, pas de contrôle de version");
		}

		var environnement = this.environnementService.recuperer(client, plateforme, mpeUuid);

		// ----------
		// Contexte
		// ----------
		Contexte result;

		if (null == consultation) {
			result = new Contexte();
		} else {
			result = contexteService.chercher(environnement, consultation.getReferenceExterne()).orElse(Contexte.with().reference(consultation.getReferenceExterne()).uuid(UUID.randomUUID()).build());
		}

		result.setEnvironnement(environnement);
		result.setPayload(json);
		result.setPlateforme(plateforme);

		result = contexteService.sauvegarder(result);

		// on ne map pas la consultation en retour
		result.setReference(null);

		return contexteViewMapper.map(result);
	}

	@Override
	@GetMapping(value = "/{contexte}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public ContexteView recuperer(
			@PathVariable("contexte") final UUID uuid
	) {
		return contexteViewMapper.map(contexteService.chercher(uuid).orElseThrow(() -> new ContexteIntrouvableException(uuid)));
	}

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public ContexteView recuperer(
			@RequestParam("environnement") final Environnement environnement,
			@RequestParam("reference") final String reference
	) {
		return contexteService.chercher(environnement, reference)
				.map(contexteViewMapper::map)
				.orElseThrow(() -> new ContexteIntrouvableException(environnement, reference));
	}
}
