package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ReferentielView;
import com.atexo.redaction.domaines.document.model.Contexte;

import java.util.Set;

public interface ReferentielController {

	/**
	 * Permet de récuper la liste des référentiels depuis le contexte
	 *
	 * @param contexte le contexte
	 *
	 * @return la liste des référentiels
	 */
	Set<ReferentielView> recuperer(Contexte contexte);
}
