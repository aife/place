package com.atexo.redaction.domaines.document.controllers.views;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Redirection
 */
@AllArgsConstructor
@Getter
@Builder(builderMethodName = "with")
public class RedirectionView {
	private final String endpoint;
}
