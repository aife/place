package com.atexo.redaction.domaines.document.controllers.views;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class ContexteView {
	private UUID uuid;

	private Location uri;

	private ConsultationView consultation;

	private String reference;

	private EnvironnementView environnement;

	private String plateforme;

}
