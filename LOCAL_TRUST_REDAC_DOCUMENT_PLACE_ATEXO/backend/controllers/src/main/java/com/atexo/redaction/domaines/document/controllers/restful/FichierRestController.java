package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.FichierController;
import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Account;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Context;
import com.atexo.redaction.domaines.document.controllers.views.TeleversementView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.services.FichierService;
import com.atexo.redaction.domaines.document.services.StockageTemporaireService;
import com.atexo.redaction.domaines.document.shared.annotations.LogTime;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/fichiers")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "fichiers")
public class FichierRestController extends BaseRestController implements FichierController {

	@NonNull
	private final FichierService fileService;

	@NonNull
	private final StockageTemporaireService stockageTemporaireService;

	@Override
	@PostMapping(headers = HEADER_ACCEPT_JSON_V2)
	public TeleversementView televerser(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final MultipartHttpServletRequest request
	) {
		TeleversementView doc = null;
		try {
			final var itr = request.getFileNames();
			while (itr.hasNext()) {
				final var uploadedFile = itr.next();
				final var multipartFile = request.getFile(uploadedFile);
				if (multipartFile != null) {
					final var file = stockageTemporaireService.upload(multipartFile.getInputStream());

					doc = TeleversementView.with()
							.uuid(file.getName())
							.taille(multipartFile.getSize())
							.nom(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()))
							.extension(fileService.getExtension(multipartFile.getOriginalFilename()))
							.build();
				}
			}
		} catch (final IOException e) {
			log.error("Impossible de créer le fichier temporaire", e);
		}

		return doc;
	}

	@Override
	@GetMapping(value = "/{fichier}", params = "action=telechargement", headers = HEADER_ACCEPT_JSON_V2)
	@Cacheable
	@LogTime("Téléchargement du fichier")
	public ResponseEntity<Resource> telecharger(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @PathVariable("fichier") UUID uuid,
			final @RequestParam("nom") String nom,
			final @RequestParam("extension") String extension,
			final @RequestParam(name = "disposition", required = false) ContentDisposition disposition
	) {
		log.info("Demande de téléchargement du document temporaire avec uuid = {}", uuid);

		return repondre(stockageTemporaireService.lire(uuid), nom, extension, disposition);
	}

	@Override
	@GetMapping(value = "/{fichier}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public Resource recuperer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			@PathVariable("fichier") final UUID uuid,
			@RequestParam("nom") final String nom,
			@RequestParam("extension") final String extension
	) {
		log.info("Demande de récupération du document temporaire avec uuid = {}", uuid);

		return stockageTemporaireService.lire(uuid);
	}

	@Override
	@DeleteMapping(value = "/{fichier}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@CacheEvict(allEntries = true)
	public void supprimer(
			final @Account Utilisateur utilisateur,
			final @Context Contexte contexte,
			final @PathVariable("fichier") UUID uuid
	) {
		log.info("Suppression du fichier temporaire '{}'", uuid);

		stockageTemporaireService.supprimer(uuid);
	}
}
