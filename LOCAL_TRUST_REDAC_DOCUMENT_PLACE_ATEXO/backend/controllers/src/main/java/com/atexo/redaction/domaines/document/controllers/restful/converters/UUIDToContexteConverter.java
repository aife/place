package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteIntrouvableException;
import com.atexo.redaction.domaines.document.services.ContexteService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToContexteConverter implements Converter<String, Contexte> {

	@NonNull
	private ContexteService contexteService;

	@Override
	public Contexte convert(final String value) {
		var uuid = UUID.fromString(value);
		return contexteService.chercher(uuid).orElseThrow(() -> new ContexteIntrouvableException(uuid));
	}
}
