package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.model.exceptions.api.service.ServiceIntrouvableException;
import com.atexo.redaction.domaines.document.services.ServiceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UUIDToServiceConverter implements Converter<String, Service> {

	@NonNull
	private ServiceService serviceService;

	@Override
	public Service convert(final String value) {
		var uuid = UUID.fromString(value);
		return serviceService.chercher(uuid).orElseThrow(() -> new ServiceIntrouvableException(uuid));

	}
}
