package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionDocgen;
import com.atexo.redaction.domaines.document.controllers.EvenementController;
import com.atexo.redaction.domaines.document.model.revisions.CallbackRevision;
import com.atexo.redaction.domaines.document.services.DocumentService;
import com.atexo.redaction.domaines.document.services.EvenementService;
import com.atexo.redaction.domaines.document.services.RevisionService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/evenements")
@RequiredArgsConstructor
public class EvenementRestController extends BaseRestController implements EvenementController {

	@NonNull
	private RevisionService<CallbackRevision> revisionService;

	@NonNull
	private EvenementService evenementService;

	@NonNull
	private DocumentService documentService;

	@NonNull
	private Convertible<RevisionDocgen, CallbackRevision> revisionMapper;

	@Override
	@PostMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public void enregistrer(@RequestBody RevisionDocgen source) {
		var revision = revisionMapper.map(source);
		var id = documentService.unformat(source.getIdentifiantExterne());

		documentService.chercher(id).ifPresentOrElse(
				document -> {
					revision.setDocument(document);

					log.info("Nouvelle revision entrante = {} pour le document {}", revision, document);

					revisionService.appliquer(revision);
				},
				() -> {
					revision.setDocument(null);

					log.info("Le document {} n'existe plus coté redacdocument", id);

					revision.refuser("Le document {0,number,#} n'existe plus coté redacdocument", id);

					evenementService.sauvegarder(revision);
				}
		);

	}

}
