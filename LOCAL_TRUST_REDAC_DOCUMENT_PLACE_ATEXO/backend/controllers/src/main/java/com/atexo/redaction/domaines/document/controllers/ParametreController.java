package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ParametreView;
import com.atexo.redaction.domaines.document.model.Environnement;

import java.util.Set;

public interface ParametreController {

	/**
	 * Récupération d'un parametre de l'environnement
	 *
	 * @param environnement l'environnnement
	 *
	 * @return le parametre
	 */
	Set<ParametreView> recuperer(Environnement environnement);
}
