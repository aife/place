package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.EnvironnementView;
import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface EnvironnementModelMapper extends Convertible<EnvironnementView, Environnement> {
}
