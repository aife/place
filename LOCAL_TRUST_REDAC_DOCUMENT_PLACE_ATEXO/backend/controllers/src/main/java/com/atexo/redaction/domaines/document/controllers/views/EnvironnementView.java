package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "with")
public class EnvironnementView {
	private String id;

	private UUID uuid;

	private String client;

	private String plateforme;

	private String urlLogo;
}
