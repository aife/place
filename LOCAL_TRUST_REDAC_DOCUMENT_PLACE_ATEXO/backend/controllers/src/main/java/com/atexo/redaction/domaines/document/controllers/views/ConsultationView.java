package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class ConsultationView {
	private String objet;

	private String intitule;

	private ProcedureView procedure;

	private OrganismeView organisme;

	private ServiceView service;

	private Date dateRemisePlis;

	private String reference;

	private String numero;

	private boolean mps;

	private Set<LotView> lots;

}
