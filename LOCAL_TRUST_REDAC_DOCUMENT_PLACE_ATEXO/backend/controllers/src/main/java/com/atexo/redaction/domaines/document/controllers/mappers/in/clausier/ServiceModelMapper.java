package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.ServiceView;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ServiceModelMapper extends Convertible<ServiceView, Service> {

	@Override
	@Mapping(target = "parent", ignore = true)
	Service map(ServiceView service);
}
