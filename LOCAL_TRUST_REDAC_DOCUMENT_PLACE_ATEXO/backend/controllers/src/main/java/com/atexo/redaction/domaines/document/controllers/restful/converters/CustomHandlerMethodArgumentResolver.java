package com.atexo.redaction.domaines.document.controllers.restful.converters;

import org.springframework.web.method.support.HandlerMethodArgumentResolver;

public interface CustomHandlerMethodArgumentResolver extends HandlerMethodArgumentResolver {
}
