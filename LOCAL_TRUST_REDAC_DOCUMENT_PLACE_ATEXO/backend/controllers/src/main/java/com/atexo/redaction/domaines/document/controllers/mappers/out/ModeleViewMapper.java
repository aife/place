package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ModeleView;
import com.atexo.redaction.domaines.document.model.Modele;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ModeleViewMapper extends Convertible<Modele, ModeleView> {
}
