package com.atexo.redaction.domaines.document.controllers.exceptions;

import com.atexo.redaction.domaines.document.model.exceptions.BaseConflitException;
import com.atexo.redaction.domaines.document.model.exceptions.BaseIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.BaseInvalideException;
import com.atexo.redaction.domaines.document.model.exceptions.BaseNonAutoriseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestControllerAdvice
@Slf4j
public class RestResponseEntityExceptionHandler {

	@ExceptionHandler(BaseConflitException.class)
	protected ResponseEntity<Object> handleConflict(BaseConflitException exception, WebRequest request) {
		log.error("handleBaseConflitException {}", exception.getMessage());
		WsMessage wsMessage = WsMessage.builder().message(exception.getMessage()).code(ExceptionEnum.CONFLICT.getCode()).type(ExceptionEnum.CONFLICT.getType()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(wsMessage.getCode()));
	}

	@ExceptionHandler(BaseIntrouvableException.class)
	protected ResponseEntity<Object> handleConflict(BaseIntrouvableException exception, WebRequest request) {
		log.error("handleBaseIntrouvableException {}", exception.getMessage());
		WsMessage wsMessage = WsMessage.builder().message(exception.getMessage()).code(ExceptionEnum.NOT_FOUND.getCode()).type(ExceptionEnum.NOT_FOUND.getType()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(wsMessage.getCode()));
	}

	@ExceptionHandler(BaseInvalideException.class)
	protected ResponseEntity<Object> handleConflict(BaseInvalideException exception, WebRequest request) {
		log.error("handleBaseInvalideException {}", exception.getMessage());
		WsMessage wsMessage = WsMessage.builder().message(exception.getMessage()).code(ExceptionEnum.DATA.getCode()).type(ExceptionEnum.DATA.getType()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(wsMessage.getCode()));
	}

	@ExceptionHandler(BaseNonAutoriseException.class)
	protected ResponseEntity<Object> handleConflict(BaseNonAutoriseException exception) {
		log.error("handleBaseNonAutoriseException {}", exception.getMessage());
		WsMessage wsMessage = WsMessage.builder().message(exception.getMessage()).code(ExceptionEnum.NOT_AUTHORIZED.getCode()).type(ExceptionEnum.NOT_AUTHORIZED.getType()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(wsMessage.getCode()));
	}

	@ExceptionHandler(HttpClientErrorException.class)
	public ResponseEntity<WsMessage> handleHttpClientErrorException(HttpClientErrorException ex) {
		log.error("handleHttpClientErrorException {}", ex.getMessage());
		WsMessage wsMessage = WsMessage.builder().message(ex.getResponseBodyAsString()).code(ex.getStatusCode().value()).build();
		return new ResponseEntity<>(wsMessage, ex.getStatusCode());
	}

	@ExceptionHandler({MethodArgumentNotValidException.class})
	public ResponseEntity<WsMessage> handleResourceNotFoundException(MethodArgumentNotValidException e) {
		String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).sorted((s, anotherString) -> s != null ? s.compareTo(anotherString) : 0).collect(Collectors.joining(", "));
		log.error("handleResourceNotFoundException {}", message);
		WsMessage wsMessage = WsMessage.builder().message(message).code(ExceptionEnum.MANDATORY.getCode()).type(ExceptionEnum.MANDATORY.getType()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
	}

	@ExceptionHandler({MissingRequestHeaderException.class})
	public ResponseEntity<WsMessage> handleResourceMissingRequestHeaderException(MissingRequestHeaderException e) {

		final String message = e.getHeaderName() + " est manquant";
		WsMessage wsMessage = WsMessage.builder().message(message).code(ExceptionEnum.MANDATORY.getCode()).type(ExceptionEnum.MANDATORY.getType()).build();
		log.error("handleResourceMissingRequestHeaderException {}", message);
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
	}

	@ExceptionHandler({MethodArgumentTypeMismatchException.class})
	public ResponseEntity<WsMessage> handleResourceMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {

		final String message = e.getName() + " est doit être de type " + e.getRequiredType().getName();
		WsMessage wsMessage = WsMessage.builder().message(message).code(ExceptionEnum.DATA.getCode()).type(ExceptionEnum.DATA.getType()).build();
		log.error("handleResourceMethodArgumentTypeMismatchException {}", message);
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.DATA.getCode()));
	}

	@ExceptionHandler({UnsatisfiedServletRequestParameterException.class})
	public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {

		final String message = String.join(", ", e.getParamConditions()) + " sont manquant(e)s";
		WsMessage wsMessage = WsMessage.builder().message(message).code(ExceptionEnum.MANDATORY.getCode()).type(ExceptionEnum.MANDATORY.getType()).build();
		log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
	}

	@ExceptionHandler({MissingServletRequestParameterException.class})
	public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(MissingServletRequestParameterException e) {

		final String message = format("Le paramètre '%s' est obligatoire", e.getParameterName());
		WsMessage wsMessage = WsMessage.builder().message(message).code(ExceptionEnum.MANDATORY.getCode()).type(ExceptionEnum.MANDATORY.getType()).build();
		log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
		return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<WsMessage> handleException(RuntimeException ex) {
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		ex.printStackTrace(pw);
		log.error("handleException {}", ex.getMessage());
		log.error("stackException {}", writer.toString());
		WsMessage wsMessage = WsMessage.builder().message("Veuillez contacter un administrateur").code(HttpStatus.BAD_REQUEST.value()).type(HttpStatus.BAD_REQUEST.getReasonPhrase()).build();
		return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
	}
}
