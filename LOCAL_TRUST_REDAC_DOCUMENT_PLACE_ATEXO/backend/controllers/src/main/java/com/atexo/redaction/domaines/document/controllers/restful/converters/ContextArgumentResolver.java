package com.atexo.redaction.domaines.document.controllers.restful.converters;

import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ContexteIntrouvableException;
import com.atexo.redaction.domaines.document.services.ContexteService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ContextArgumentResolver implements CustomHandlerMethodArgumentResolver {

	public static final String CONTEXT = "Context";

	@NonNull
	private ContexteService contexteService;

	@Override
	public boolean supportsParameter(final MethodParameter methodParameter) {
		return null != methodParameter.getParameterAnnotation(Context.class);
	}

	@Override
	public Object resolveArgument(final MethodParameter methodParameter, final ModelAndViewContainer modelAndViewContainer, final NativeWebRequest nativeWebRequest, final WebDataBinderFactory webDataBinderFactory) {
		Contexte result = null;

		final var request = (HttpServletRequest) nativeWebRequest.getNativeRequest();
		final var header = request.getHeader(CONTEXT);

		if (null != header) {
			final var uuid = UUID.fromString(header);

			result = this.contexteService.chercher(uuid).orElseThrow(() -> new ContexteIntrouvableException(uuid));
		}

		return result;
	}
}
