package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ReferentielView;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper
public interface ReferentielViewMapper extends Convertible<Referentiel, ReferentielView> {
}
