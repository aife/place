package com.atexo.redaction.domaines.document.controllers.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class ReferentielView {
	private String type;
	private String code;
	private String libelle;
	private Integer ordre;
	private ReferentielView parent;
	private Long id;
}
