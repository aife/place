package com.atexo.redaction.domaines.document.controllers.mappers.in.mpe;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.services.OrganismeService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;

import javax.transaction.Transactional;

@Mapper
@Slf4j
public abstract class OrganismeMapper implements Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Organisme, Environnement>, Organisme> {

	@Setter(onMethod = @__(@Autowired))
	private OrganismeService organismeService;

	@Setter(onMethod = @__(@Autowired))
	private SerialisationService serialisationService;

	@Override
	@Transactional
	public Organisme map(Pair<com.atexo.redaction.domaines.document.mpe.v2.Organisme, Environnement> pair) {
		var organismeXML = pair.getFirst();
		var environnement = pair.getSecond();

		return this.organismeService.sauvegarder(
				this.organismeService.chercher(organismeXML.getId(), environnement)
						.map(
								resultat -> {
									log.info("L'organisme '{}' pour l'environnement {} existe déjà. Il sera mis a jour si besoin", organismeXML.getId(), environnement);

									resultat.setPayload(this.serialisationService.serialise(organismeXML));

									return resultat;
								}
						)
						.orElseGet(
								() -> {
									log.info("L'organisme '{}' pour l'environnement {} n'existe pas encore. Il va etre créé.", organismeXML.getId(), environnement);

									return com.atexo.redaction.domaines.document.model.Organisme.with()
											.environnement(environnement)
											.payload(this.serialisationService.serialise(organismeXML))
											.build();
								}
						)
		);
	}
}
	
