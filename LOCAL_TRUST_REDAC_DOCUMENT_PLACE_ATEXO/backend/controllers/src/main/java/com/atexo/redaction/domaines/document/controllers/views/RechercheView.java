package com.atexo.redaction.domaines.document.controllers.views;

import com.atexo.redaction.domaines.document.model.Statut;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.MessageFormat;
import java.util.Set;

@NoArgsConstructor(force = true)
@Getter
@Setter
public class RechercheView {
	private String requete;
	private Set<String> categories;
	private Statut statut;

	public String getExpression() {
		return MessageFormat.format("%{0}%", this.getRequete().replace(' ', '%'));
	}
}
