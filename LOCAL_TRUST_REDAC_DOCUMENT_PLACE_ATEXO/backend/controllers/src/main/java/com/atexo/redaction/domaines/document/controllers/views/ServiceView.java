package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "with")
public class ServiceView {
	private UUID uuid;
	private ServiceView parent;
	private OrganismeView organisme;
	private String libelle;
}
