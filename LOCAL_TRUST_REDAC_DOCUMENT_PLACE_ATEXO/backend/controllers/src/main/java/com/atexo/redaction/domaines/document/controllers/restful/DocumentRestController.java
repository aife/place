package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.DocumentController;
import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.io.RechercheCritere;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Account;
import com.atexo.redaction.domaines.document.controllers.restful.converters.Context;
import com.atexo.redaction.domaines.document.controllers.views.*;
import com.atexo.redaction.domaines.document.model.*;
import com.atexo.redaction.domaines.document.model.audit.CreateAudit;
import com.atexo.redaction.domaines.document.model.audit.UpdateAudit;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentNonSoumisException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.DocumentNonValideException;
import com.atexo.redaction.domaines.document.model.exceptions.api.document.RevisionIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.gabarit.GabaritIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.referentiel.ModeleIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.api.utilisateur.UtilisateurNonAutoriseException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.BaseServiceEnLigneException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.GenerationDocumentException;
import com.atexo.redaction.domaines.document.model.exceptions.io.ArchiveInexistanteException;
import com.atexo.redaction.domaines.document.model.messages.documents.*;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.mpe.v1.DocumentType;
import com.atexo.redaction.domaines.document.services.*;
import com.atexo.redaction.domaines.document.services.audit.DocumentAuditQuery;
import com.atexo.redaction.domaines.document.services.audit.Revision;
import com.atexo.redaction.domaines.document.services.local.DocumentLocalService;
import com.atexo.redaction.domaines.document.shared.helpers.EnhancedStopWatch;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/documents")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "documents")
public class DocumentRestController extends BaseRestController implements DocumentController {

    @Value("${backend.base.url}/document/evenements")
    private Location evenementURL;

    @Value("{0}/redac/document/documents/{1,number,#}?action=telechargement&disposition=inline&format=pdf")
    private Location exportURL;

    @NonNull
    private final DocumentService documentService;

    @NonNull
    private final DocumentLocalService documentLocalService;

    @NonNull
    private final MailService mailService;

    @NonNull
    private final StockageTemporaireService stockageTemporaireService;

    @NonNull
    private final StockageDocumentService stockagePersistentService;

    @NonNull
    private final HabilitationService habilitationService;

    @NonNull
    private final PlateformeService plateformeService;

    @NonNull
    private final Convertible<Document, DocumentView> documentViewMapper;

    @NonNull
    private final ConversionService conversionService;

    @NonNull
    private final MessageDocumentService messageDocumentService;

    @NonNull
    private final Convertible<DestinataireView, Destinataire> destinataireModelMapper;

    @NonNull
    private final ClausierService clausierService;

    @NonNull
    private final ModeleService modeleService;

    @NonNull
    private final ContexteService contexteService;

    @NonNull
    private final GenerationService generationDocumentService;

    @NonNull
    private final FichierService fileService;

    @NonNull
    private final GabaritService gabaritService;

    @NonNull
    private final Convertible<ReferentielView, TypeModele> typeModeleModelMapper;

    @NonNull
    private final Convertible<CanevasView, Canevas> canevasModelMapper;
    @NonNull
    private final Convertible<LotView, Lot> lotModelMapper;

    @NonNull
    private final EditionService editionService;

    @NonNull
    private final VisualisationService visualisationService;

    @Override
    @GetMapping(value = "/{document}/revisions/{revision}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @Cacheable
    public DocumentView recuperer(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @PathVariable("revision") UUID revision
    ) {
        log.info("L'utilisateur {} a demandé a récupérer la révision '{}' du document {}", utilisateur, revision, document.getId());

        final var result = this.documentService.rechercherRevision(revision).map(Revision::getVersion).orElseThrow(() -> new RevisionIntrouvableException(revision, document));

        if (!result.getId().equals(document.getId())) {
            throw new RevisionIntrouvableException(revision, document);
        }

        return this.documentViewMapper.map(result);
    }

    @Override
    @GetMapping(value = "/{document}", headers = {HEADER_ACCEPT_JSON, HEADER_ACCEPT_JSON_V2})
    @Cacheable
    public DocumentView recuperer(@PathVariable("document") final Document document, @RequestParam(value = "statut", required = false, defaultValue = "VALIDE") final Statut statut) {
        return this.documentService.rechercherDerniereRevision(document, statut).map(Revision::getVersion).map(documentViewMapper::map).orElseThrow(() -> new DocumentIntrouvableException(document.getId()));
    }

    @Override
    @GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentView> rechercher(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final RechercheCritere critere
    ) {
        if (null != contexte) {
            try {
                this.contexteService.actualiser(contexte, utilisateur);
            } catch (Exception e) {
                log.error("Erreur lors de l'actualisation du contexte {}", e.getMessage(), e);
            }
        }

        final var documents = this.documentService.rechercher(
                critere.getEnvironnement(),
                critere.getContexte(),
                critere.getExpressions(),
                critere.getCategories(),
                critere.getStatuts()
        );

        log.info("{} documents trouvés", documents.size());

        return documents.stream().map(
                document -> {
                    DocumentView result = null;

                    try {
                        result = this.documentViewMapper.map(document);

                    } catch (final Exception cie) {
                        log.warn("La consultation '{}' est introuvable pour le contexte '{}'", critere.getContexte().getReference(), critere.getContexte().getUuid());
                    }

                    return result;
                }

        ).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    @PostMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView creer(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @RequestBody DocumentView view
    ) {
        final var watch = new EnhancedStopWatch();

        watch.start("Recherche du modele avec uuid {}", view.getModele().getUuid());

        final var modele = this.modeleService.chercher(view.getModele().getUuid()).orElseThrow(() -> new ModeleIntrouvableException(view.getModele().getUuid()));

        Referentiel type = modele.getType();
        ReferentielView typeDTO = view.getModele().getType();

        do {
            type.setParent(this.typeModeleModelMapper.map(typeDTO.getParent()));

            type = type.getParent();
            typeDTO = typeDTO.getParent();
        } while (null != typeDTO && null != type);

        watch.stop();

        watch.start("Construction du document intitulé {}", view.getNom());

        final var uuid = null == view.getUuid() ? UUID.randomUUID() : view.getUuid();

        final var documentBuilder = Document.with()
                .nom(view.getNom())
                .actif(true)
                .action(Action.CREATION)
                .ouvert(false)
                .progression(modele.est(Categorie.DOCUMENT_CANEVAS) ? 0d : 100d)
                .canevas(this.canevasModelMapper.map(view.getCanevas()))
                .uuid(uuid)
                .lot(this.lotModelMapper.map(view.getLot()))
                .contexte(contexte)
                .creation(CreateAudit.with().utilisateur(utilisateur).build())
                .modification(UpdateAudit.with().utilisateur(utilisateur).build())
                .modele(modele);

        if (modele.est(Categorie.DOCUMENT_LIBRE)) {
            documentBuilder.extension(view.getExtension());

            final var resource = this.stockageTemporaireService.lire(view.getUuid());

            documentBuilder.chemin(this.stockagePersistentService.sauvegarder(resource, view.getUuid()));
        } else {
            documentBuilder.extension(modele.extension());

            final var chemin = this.gabaritService.rechercher(contexte.getEnvironnement(), utilisateur, modele)
                    .map(Gabarit::getChemin)
                    .map(Path::of).orElseThrow(() -> {
                        log.error("Gabarit introuvable pour le type de document '{}', l'environnement '{}' et l'agent '{}'", modele.getType().getCode(), contexte.getEnvironnement().getUuid(), utilisateur.getNomComplet());

                        return new GabaritIntrouvableException(modele);
                    });

            documentBuilder.chemin(this.stockagePersistentService.copier(chemin, uuid));
        }

        final var document = documentBuilder.build();

        watch.stop();

        if (modele.est(Categorie.DOCUMENT_CANEVAS)) {
            watch.start("Génération du XML depuis clausier pour le document intitulé '{}'", document.getNom());

            // 1. Générer le squelette du document à partir du clausier
            final var xml = this.clausierService.creer(
                    contexte,
                    utilisateur,
                    document.getCanevas(),
                    document.getModele().getType(),
                    document.getLot()
            );
            log.debug("XML généré = {}", xml);

            document.setXml(xml);

            watch.stop();

            watch.start("Extraction des champs de fusion pour le document intitulé '{}'", document.getNom());

            final var champs = this.contexteService.extraire(contexte, utilisateur, document);

            watch.stop();

            watch.start("Récupération du gabarit pour le document intitulé '{}'", document.getNom());

            // Appeler only-office pour envoyer gabarit + xml
            // le résultat (un docx et un xml dans un zip) est sauvegardé
            final var gabarit = this.stockageTemporaireService.copier(document);

            watch.stop();

            watch.start("Demande de fusion docgen pour le document canevas intitulé '{}' a partir du contexte '{}'", document.getNom(), contexte);

            try {
                final var zip = this.generationDocumentService.fusionner(xml, gabarit, champs);

                if (null == zip) {
                    throw new ArchiveInexistanteException(document);
                }

                this.documentService.unzip(document, zip);
            } finally {
                this.fileService.supprimer(gabarit);
            }

            watch.stop();
        } else {
            final var champs = this.contexteService.extraire(contexte, utilisateur, document);

            // Appel du serveur only-office pour générer la première version du fichier
            // Cette version est créé à partir du gabarit (le modèle) et des champs de fusion issu du contexte
            final var gabarit = this.stockageTemporaireService.copier(document);

            watch.start("Demande de fusion docgen pour le document simple intitulé '{}'", document.getNom());

            try {
                Resource fichier = gabarit;

                try {
                    fichier = this.generationDocumentService.fusionner(gabarit, champs);
                } catch (final GenerationDocumentException exception) {
                    log.warn("Impossible de valoriser les champs de fusion du document", exception);
                }

                // Sauvegarde du fichier
                final var chemin = this.stockagePersistentService.sauvegarder(fichier, document.getUuid());

                document.setChemin(chemin.toString());
            } finally {
                this.fileService.supprimer(gabarit);
            }

            watch.stop();
        }

        // Sauvegarde du nouveau document
        final var result = this.documentService.sauvegarder(document);

        log.info("Sauvegarde OK️ du document intitulé '{}' (id = {})", document.getNom(), document.getId());

        // Notification de la création
        this.messageDocumentService.notifier(CreationDocumentMessage.with()
                .document(result)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        log.info(watch.prettyPrint());

        return this.documentViewMapper.map(result);
    }

    @Override
    @PatchMapping(value = "/{document}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView modifier(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestBody DocumentView view

    ) {
        // Le nom est modifiable
        final var result = document.renommer(view.getNom(), UpdateAudit.with().utilisateur(utilisateur).build());

        if (document.getModele().est(Categorie.DOCUMENT_LIBRE)) {
            result.setExtension(view.getExtension());

            final var resource = this.stockageTemporaireService.lire(view.getUuid());

            result.setChemin(this.stockagePersistentService.sauvegarder(resource, view.getUuid()).toString());
        }

        // Ouvert/fermé est modifiable
        result.setOuvert(view.isOuvert());

        // Notification de la soumission
        this.messageDocumentService.notifier(ModificationDocumentMessage.with()
                .document(result)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        return this.documentViewMapper.map(this.documentService.sauvegarder(result));
    }

    @Override
    @GetMapping(value = "/{document}", params = "action=telechargement")
    public ResponseEntity<Resource> telecharger(
            final @PathVariable("document") Document document,
            final @RequestParam(name = "format", required = false) TypeDocument type,
            final @RequestParam(name = "disposition", required = false) ContentDisposition disposition) {
        return super.telecharger(document, type, disposition, false);
    }

    @Override
    @DeleteMapping(value = "/{document}", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public void supprimer(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document
    ) {
        log.info("Suppression du document '{}'", document);

        final var sauvegarde = this.documentService.sauvegarder(document.supprimer(UpdateAudit.with().utilisateur(utilisateur).build()));

        this.documentService.supprimer(sauvegarde);

        // Notification de la suppression
        this.messageDocumentService.notifier(SuppressionDocumentMessage.with()
                .document(document)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());
    }

    @Override
    @PostMapping(value = "/{document}", params = "action=duplication", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView dupliquer(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document
    ) {
        final var duplicata = this.documentService.dupliquer(document, utilisateur, contexte);

        log.debug("Duplication du document {} pour le modele {}", document.getUuid(), duplicata.getModele().getType().getCode());

        // Sauvegarde du document
        final var sauvegarde = this.documentService.sauvegarder(duplicata);

        // Notification de la création
        this.messageDocumentService.notifier(DuplicationDocumentMessage.with()
                .document(sauvegarde)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        return this.documentViewMapper.map(sauvegarde);
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=visualisation")
    public RedirectionView visualiser(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document
    ) {
        final var plateforme = this.plateformeService.recuperer(document.getContexte());

        final Jeton jeton;

        final var autorisations = this.habilitationService.extraire(utilisateur)
                .stream()
                .map(Habilitation::getValeur)
                .map(Autorisation::find)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Autorisation::getOutput)
                .collect(Collectors.toSet());

        final var clone = document.afficher(UpdateAudit.with().utilisateur(utilisateur).build());

        try {
            // Si le jeton est nul dans le document
            // On envoie le fichier pour en obltenir un
            jeton = this.visualisationService.visualiser(
                    this.stockageTemporaireService.copier(document),
                    Session.with()
                            .mode(Mode.VISUALISATION)
                            .id(this.documentService.format(document))
                            .titre(document.getNom())
                            .editeur(
                                    Auteur.with()
                                            .id(utilisateur.getIdentifiantExterne())
                                            .nom(utilisateur.getNomComplet())
                                            .autorisations(autorisations)
                                            .build()
                            )
                            .plateforme(plateforme)
                            .build()
            );
        } catch (final BaseServiceEnLigneException exception) {
            final var audit = UpdateAudit.with()
                    .utilisateur(utilisateur)
                    .commentaire("Une erreur a été rencontrée lors de l'affichage du document. Veuillez renouveler l'opération.")
                    .build();

            final var signalement = document.signaler(audit);

            this.documentService.sauvegarder(signalement);

            throw exception;
        }

        log.info("Le jeton de navigation est = '{}'", jeton);

        clone.setJeton(jeton.getValeur());

        this.documentService.sauvegarder(clone);

        final var redirect = this.visualisationService.redirect(jeton);
        log.info("URL de redirection = '{}'", redirect);

        return RedirectionView.with().endpoint(redirect.toString()).build();
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=edition", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    public RedirectionView editer(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document
    ) {
        final var plateforme = this.plateformeService.recuperer(document.getContexte());
        final boolean pooling = this.contexteService.estEnModePooling(document.getContexte());

        final Jeton jeton;

        final var autorisations = this.habilitationService.extraire(utilisateur)
                .stream()
                .map(Habilitation::getValeur)
                .map(Autorisation::find)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Autorisation::getOutput)
                .collect(Collectors.toSet());

        final var auteur = Auteur.with()
                .id(utilisateur.getIdentifiantExterne())
                .nom(utilisateur.getNomComplet())
                .autorisations(autorisations)
                .build();

        //récupérer version docx du document
        var document2 = documentLocalService.recupererDerniereRevisionEditable(document).getVersion();

        final var id = this.documentService.format(document2);
        final var title = document.getNom();

        // On envoie le fichier pour en obtenir un nouveau temporaire
        final var temp = this.stockageTemporaireService.copier(document2);

        try {
            final Session session;

            // Si le jeton est nul dans le document
            if (document.getModele().est(Categorie.DOCUMENT_CANEVAS)) {
                session = Session.with()
                        .mode(Mode.REDACTION)
                        .id(id)
                        .titre(title)
                        .editeur(auteur)
                        .plateforme(plateforme)
                        .retour(pooling ? null : this.evenementURL)
                        .build();

                jeton = this.editionService.editer(document.getXml(), temp, session);
            } else {
                session = Session.with()
                        .mode(Mode.EDITION)
                        .id(id)
                        .titre(title)
                        .editeur(auteur)
                        .plateforme(plateforme)
                        .retour(pooling ? null : this.evenementURL)
                        .build();

                jeton = this.editionService.editer(temp, session);
            }
        } catch (final BaseServiceEnLigneException exception) {
            final var audit = UpdateAudit.with()
                    .utilisateur(utilisateur)
                    .commentaire("Une erreur a été rencontrée lors de l'édition du document. Veuillez renouveler l'opération.")
                    .build();

            final var signalement = document2.signaler(audit);

            this.documentService.sauvegarder(signalement);

            throw exception;
        } finally {
            this.fileService.supprimer(temp);
        }

        log.info("Le jeton de navigation est = '{}'", jeton);

        this.documentService.sauvegarder(document.demanderOuverture(jeton, UpdateAudit.with().utilisateur(utilisateur).build()));

        final var redirect = this.visualisationService.redirect(jeton);
        log.info("URL de redirection = '{}'", redirect);

        return RedirectionView.with().endpoint(redirect.toString()).build();
    }


    @Override
    @GetMapping(value = "/{document}/revisions", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentView> lister(
            @PathVariable("document") final Document document
    ) {
        return this.documentService.recupererRevisions(document)
                .stream()
                .map(Revision::getVersion)
                .map(this.documentViewMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping(value = "/{document}/revisions/{uuid}", params = "action=telechargement")
    public ResponseEntity<Resource> telecharger(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @PathVariable("uuid") UUID uuid,
            final @RequestParam(name = "format", required = false) TypeDocument type,
            final @RequestParam(name = "disposition", required = false) ContentDisposition disposition
    ) {
        final var watch = new StopWatch();

        final var revision = this.documentService.rechercherRevision(uuid).map(Revision::getVersion).orElseThrow(() -> new RevisionIntrouvableException(uuid, document));

        if (!revision.getId().equals(document.getId())) {
            throw new RevisionIntrouvableException(uuid, document);
        }

        watch.start("Telechargement du document " + revision.getUuid());

        log.info("Demande de téléchargement du document final avec uuid = {}", revision.getUuid());

        final var resource = super.telecharger(revision, type, disposition, document.getCanevas() != null);

        log.info("Fin du téléchargement du document avec uuid = {} (temps de téléchargement en milliseconds = {})", revision.getUuid(), watch.getTotalTimeMillis());

        watch.stop();

        return resource;
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=soumission", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView soumettre(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestBody DocumentView vue
    ) {
        // Creation de la demande de validation
        final var duplicata = document.demanderValidation(UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build());

        // Sauvegarde du document
        final var result = this.documentService.sauvegarder(duplicata);

        // Notification de la soumission
        this.messageDocumentService.notifier(SoumissionDocumentMessage.with()
                .document(result)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        return this.documentViewMapper.map(result);
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=validation", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView valider(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestParam("type") TypeDocument type,
            final @RequestBody DocumentView vue
    ) {
        if (!this.habilitationService.autorise(utilisateur, Autorisation.VALIDATION)) {
            log.error("L'utilisateur {} n'a pas les droits de validation!", utilisateur.getNom());

            throw new UtilisateurNonAutoriseException(utilisateur, Autorisation.VALIDATION);
        }

        log.info("L'utilisateur {} a les droits de validation. Conversion du document", utilisateur.getNom());
        //cas 1 : validation sans modification de l extension
        if (type == TypeDocument.DOCX || type == TypeDocument.XLSX) {
            final var temp = this.stockageTemporaireService.copier(document);
            final var validation = document.valider(UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build());
            validation.setOuvert(false);
            final var validationUuid = validation.getUuid();
            try {
                final var fichier = this.conversionService.convertir(temp, type.getExtension());

                final var path = this.stockagePersistentService.sauvegarder(fichier, validationUuid);
                validation.setUuid(UUID.randomUUID());
                validation.setChemin(path.toString());
                validation.setExtension(type.getExtension());
            } catch (final BaseServiceEnLigneException exception) {
                final var audit = UpdateAudit.with()
                        .utilisateur(utilisateur)
                        .commentaire("Une erreur a été rencontrée lors de la création de la version validée du document. Veuillez renouveler l'action de validation.")
                        .build();
                final var signalement = document.signaler(audit);
                this.documentService.sauvegarder(signalement);
                throw exception;
            }

            final var result = this.documentService.sauvegarder(validation);
            // Notification de la validation
            this.messageDocumentService.notifier(ValidationDocumentMessage.with()
                    .document(result)
                    .utilisateur(utilisateur)
                    .contexte(contexte)
                    .build());
            return this.documentViewMapper.map(result);
        }

        //cas 2 : validation PDF : on enregistre une validation docx ou xlsx + une validation pdf
        else {
            //enregistrer d'abord version docx ou xlsx dans aud
            //récupérer fichier original pour connaitre l'extension
            Document original = documentService.recupererDocumentOriginal(document);
            final var temp2 = this.stockageTemporaireService.copier(document);
            final var validation1 = document.valider(UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build());
            validation1.setOuvert(false);
            final var validationUuid = validation1.getUuid();
            try {
                final var fichier = this.conversionService.convertir(temp2, original.getExtension());
                final var path = this.stockagePersistentService.sauvegarder(fichier, validationUuid);
                validation1.setUuid(UUID.randomUUID());
                validation1.setChemin(path.toString());
                validation1.setExtension(original.getExtension());
            } catch (final BaseServiceEnLigneException exception) {
                final var audit = UpdateAudit.with()
                        .utilisateur(utilisateur)
                        .commentaire("Une erreur a été rencontrée lors de la création de la version validée du document. Veuillez renouveler l'action de validation.")
                        .build();
                final var signalement1 = document.signaler(audit);
                this.documentService.sauvegarder(signalement1);
                throw exception;
            }
            this.documentService.sauvegarder(validation1);

            //enregistrer ensuite la version pdf dans aud
            final var temp3 = this.stockageTemporaireService.copier(document);
            final var validation2 = document.valider(UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build());
            validation2.setOuvert(false);
            final var validationUuid2 = validation2.getUuid();

            try {
                final var fichier = this.conversionService.convertir(temp3, type.getExtension());

                final var path = this.stockagePersistentService.sauvegarder(fichier, validationUuid2);
                validation2.setUuid(UUID.randomUUID());
                validation2.setChemin(path.toString());
                validation2.setExtension(type.getExtension());
            } catch (final BaseServiceEnLigneException exception) {
                final var audit = UpdateAudit.with()
                        .utilisateur(utilisateur)
                        .commentaire("Une erreur a été rencontrée lors de la création de la version validée du document. Veuillez renouveler l'action de validation.")
                        .build();
                final var signalement2 = document.signaler(audit);
                this.documentService.sauvegarder(signalement2);
                throw exception;
            }

            final var result = this.documentService.sauvegarder(validation2);
            // Notification de la validation
            this.messageDocumentService.notifier(ValidationDocumentMessage.with()
                    .document(result)
                    .utilisateur(utilisateur)
                    .contexte(contexte)
                    .build());
            return this.documentViewMapper.map(result);
        }

    }

    @Override
    @PostMapping(value = "/{document}", params = "action=notification", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<DestinataireView> notifier(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestBody Set<DestinataireView> vue
    ) {
        log.info("Création de la notification pour {} destinataires", vue.size());
        String mpeUrl;
        if (contexte.getEnvironnement() != null && contexte.getEnvironnement().getMpeUrl() != null) {
            mpeUrl = contexte.getEnvironnement().getMpeUrl();
        } else {
            mpeUrl = "";
            log.info("mpeUrl n'est pas renseigné dans le contexte, un lien du mail ne fonctionnera pas");
        }
        final var destinataires = this.destinataireModelMapper.map(vue).stream()
                .peek(destinataire -> {
                            destinataire.setDefautUri(true);
                            destinataire.setEnvironnement(contexte.getEnvironnement());
                            destinataire.setLien(this.exportURL.format(mpeUrl, document.getId()));
                        }
                ).collect(Collectors.toSet());

        final var notification = this.documentService.notifier(document, destinataires, utilisateur, contexte);

        log.info("Envoi du mail = {}", notification);

        this.mailService.envoyer(notification);

        return vue;
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=insoumission", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView insoumettre(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestBody DocumentView vue
    ) {
        if (!document.estEnDemandeDeValidation()) {
            throw new DocumentNonSoumisException(document);
        }

        log.info("Le document {} est en demande de validation. Récupération de la version antérieure à la demande", document);

        final var demande = this.documentService.recupererDerniereRevisionEditable(document).getVersion();

        final var audit = UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build();

        // Refus et sauvegarde du document
        final var result = this.documentService.sauvegarder(demande.refuserDemandeValidation(audit));

        // Notification de l'insoumission
        this.messageDocumentService.notifier(InsoumissionDocumentMessage.with()
                .document(result)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        return this.documentViewMapper.map(result);
    }

    @Override
    @PatchMapping(value = "/{document}", params = "action=invalidation", headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(allEntries = true)
    public DocumentView invalider(
            final @Account Utilisateur utilisateur,
            final @Context Contexte contexte,
            final @PathVariable("document") Document document,
            final @RequestBody DocumentView vue
    ) {
        if (!document.estValide()) {
            throw new DocumentNonValideException(document);
        }

        final var demande = this.documentService.recupererDerniereRevisionEditable(document).getVersion();

        final var audit = UpdateAudit.with().utilisateur(utilisateur).commentaire(vue.getModification().getCommentaire()).build();

        // Invalidation et sauvegarde du document
        final var result = this.documentService.sauvegarder(demande.invalider(audit));

        // Notification de l'invalidation
        this.messageDocumentService.notifier(InvalidationDocumentMessage.with()
                .document(result)
                .utilisateur(utilisateur)
                .contexte(contexte)
                .build());

        return this.documentViewMapper.map(result);
    }

    @PostConstruct
    private void echo() {
        log.info("\uD83D\uDD17 URL de retour des évènements des documents = '{}'", this.evenementURL);
    }
}
