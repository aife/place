package com.atexo.redaction.domaines.document.controllers.mappers.out.custom;

import com.atexo.redaction.domaines.document.controllers.mappers.out.ConsultationViewMapper;
import com.atexo.redaction.domaines.document.controllers.views.ConsultationView;
import com.atexo.redaction.domaines.document.controllers.views.LotView;
import com.atexo.redaction.domaines.document.model.Consultation;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Lot;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ConsultationIntrouvableException;
import com.atexo.redaction.domaines.document.services.ClausierService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

@Mapper
@Named("consultationRemoteMapper")
@Slf4j
public abstract class ConsultationRemoteMapper implements Convertible<Contexte, ConsultationView> {

	@Setter(onMethod = @__(@Autowired))
	private ConsultationViewMapper consultationMapper;

	@Setter(onMethod = @__(@Autowired))
	private ClausierService clausierService;

	@Override
	public ConsultationView map(Contexte contexte) {
		ConsultationView result = null;
		if (null != contexte.getReference()) {
			var consultation = clausierService.recuperer(contexte).orElseThrow(() -> new ConsultationIntrouvableException(contexte));

			result = consultationMapper.map(consultation);
		}
		return result;
	}
}
	
