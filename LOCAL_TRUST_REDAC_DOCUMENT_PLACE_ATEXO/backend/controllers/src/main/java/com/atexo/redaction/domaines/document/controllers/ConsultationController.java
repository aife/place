package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.ConsultationView;
import com.atexo.redaction.domaines.document.model.Contexte;

public interface ConsultationController {
	/**
	 * Récupération de la consultation
	 *
	 * @param contexte l'identifiant du contexte
	 *
	 * @return la consultation
	 */
	ConsultationView recuperer(Contexte contexte);
}
