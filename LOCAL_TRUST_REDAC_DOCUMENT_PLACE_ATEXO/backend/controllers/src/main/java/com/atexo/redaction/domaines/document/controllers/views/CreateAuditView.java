package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class CreateAuditView {

	protected UtilisateurView utilisateur;

	protected Instant date;
}
