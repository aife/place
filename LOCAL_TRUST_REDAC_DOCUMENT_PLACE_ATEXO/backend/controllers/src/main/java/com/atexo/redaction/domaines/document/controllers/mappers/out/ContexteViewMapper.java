package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.mappers.out.custom.ConsultationRemoteMapper;
import com.atexo.redaction.domaines.document.controllers.mappers.out.custom.UriMapper;
import com.atexo.redaction.domaines.document.controllers.views.ContexteView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {ConsultationRemoteMapper.class, EnvironnementViewMapper.class, UriMapper.class})
public interface ContexteViewMapper extends Convertible<Contexte, ContexteView> {

	@Override
	@Mapping(source = "contexte", target = "consultation", qualifiedByName = "consultationRemoteMapper")
	@Mapping(source = "contexte", target = "uri", qualifiedByName = "uriMapper")
	ContexteView map(Contexte contexte);
}
