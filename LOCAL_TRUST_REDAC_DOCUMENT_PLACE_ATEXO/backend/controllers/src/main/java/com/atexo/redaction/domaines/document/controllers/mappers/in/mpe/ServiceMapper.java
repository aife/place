package com.atexo.redaction.domaines.document.controllers.mappers.in.mpe;

import com.atexo.redaction.domaines.document.model.Environnement;
import com.atexo.redaction.domaines.document.model.Organisme;
import com.atexo.redaction.domaines.document.model.Service;
import com.atexo.redaction.domaines.document.services.OrganismeService;
import com.atexo.redaction.domaines.document.services.ServiceService;
import com.atexo.redaction.domaines.document.services.local.ServiceLocalService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import com.atexo.redaction.domaines.document.shared.services.SerialisationService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;

import javax.transaction.Transactional;

@Mapper
@Slf4j
public abstract class ServiceMapper implements Convertible<Pair<com.atexo.redaction.domaines.document.mpe.v2.Service, Organisme>, Service> {

	@Setter(onMethod = @__(@Autowired))
	private ServiceService serviceService;

	@Setter(onMethod = @__(@Autowired))
	private SerialisationService serialisationService;

	@Override
	@Transactional
	public Service map(Pair<com.atexo.redaction.domaines.document.mpe.v2.Service, Organisme> pair) {
		var serviceXML = pair.getFirst();
		var organisme = pair.getSecond();

		return this.serviceService.sauvegarder(
				this.serviceService.chercher(serviceXML.getId(), organisme)
					.map(
							service -> {
								log.info("Le service '{}' existe déjà. Il sera mis a jour si besoin", serviceXML.getId());

								service.setParent(null != serviceXML.getParent() ? this.map(Pair.of(serviceXML.getParent(), organisme)) : null);
								service.setPayload(this.serialisationService.serialise(serviceXML));

								return service;
							}
					)
					.orElseGet(() -> Service.with()
							.organisme(organisme)
							.parent(null != serviceXML.getParent() ? this.map(Pair.of(serviceXML.getParent(), organisme)) : null)
							.payload(this.serialisationService.serialise(serviceXML))
							.build()
					)
		);
	}
}
	
