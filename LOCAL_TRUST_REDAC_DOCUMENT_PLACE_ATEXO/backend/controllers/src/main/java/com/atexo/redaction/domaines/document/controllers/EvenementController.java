package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionDocgen;

public interface EvenementController {

	/**
	 * Permet d'enregistrer une révision
	 *
	 * @param source la révision
	 */
	void enregistrer(RevisionDocgen source);
}
