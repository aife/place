package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.ReferentielController;
import com.atexo.redaction.domaines.document.controllers.views.ReferentielView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.referentiels.Referentiel;
import com.atexo.redaction.domaines.document.services.ReferentielService;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/referentiels")
@RequiredArgsConstructor
@CacheConfig(cacheNames = "referentiels")
public class ReferentielRestController extends BaseRestController implements ReferentielController {

	@NonNull
	private final ReferentielService referentielService;

	@NonNull
	private final Convertible<Referentiel, ReferentielView> referentielViewMapper;

	@Override
	@GetMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable
	public Set<ReferentielView> recuperer(
			@RequestParam("contexte") final Contexte contexte
	) {
		return referentielViewMapper.map(referentielService.filtrer(contexte.getEnvironnement()));
	}
}
