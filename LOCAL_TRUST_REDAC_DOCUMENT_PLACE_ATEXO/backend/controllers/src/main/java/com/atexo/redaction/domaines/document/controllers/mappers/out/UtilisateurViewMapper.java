package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.mappers.out.custom.HabilitationViewMapper;
import com.atexo.redaction.domaines.document.controllers.views.UtilisateurView;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {HabilitationViewMapper.class})
public interface UtilisateurViewMapper extends Convertible<Utilisateur, UtilisateurView> {
	@Override
	@Mapping(source = "utilisateur", target = "habilitations", qualifiedByName = "habilitationMapper")
	UtilisateurView map(Utilisateur utilisateur);
}
