package com.atexo.redaction.domaines.document.controllers.mappers.out;

import com.atexo.redaction.domaines.document.controllers.views.ParametreView;
import com.atexo.redaction.domaines.document.model.Parametre;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;

@Mapper(uses = {EnvironnementViewMapper.class})
public interface ParametreViewMapper extends Convertible<Parametre, ParametreView> {
}
