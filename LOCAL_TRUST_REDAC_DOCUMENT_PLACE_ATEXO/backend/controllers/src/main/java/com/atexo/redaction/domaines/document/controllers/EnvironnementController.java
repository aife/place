package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.views.EnvironnementView;

public interface EnvironnementController {

	/**
	 * Permet de récupérer un environnement depuis le client et la plateforme
	 *
	 * @param client le client
	 * @param plateforme la plateforme
	 *
	 * @return l'environnement
	 */
	EnvironnementView recuperer(String client, String plateforme, String mpUuid);
}
