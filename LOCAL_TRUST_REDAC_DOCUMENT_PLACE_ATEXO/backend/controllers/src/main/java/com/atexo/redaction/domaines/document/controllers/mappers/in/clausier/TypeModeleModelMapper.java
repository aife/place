package com.atexo.redaction.domaines.document.controllers.mappers.in.clausier;

import com.atexo.redaction.domaines.document.controllers.views.ReferentielView;
import com.atexo.redaction.domaines.document.model.referentiels.TypeModele;
import com.atexo.redaction.domaines.document.shared.patterns.Convertible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface TypeModeleModelMapper extends Convertible<ReferentielView, TypeModele> {

	@Mapping(target = "parent", ignore = true)
	@Mapping(target = "environnements", ignore = true)
	TypeModele map(ReferentielView referentiel);
}
