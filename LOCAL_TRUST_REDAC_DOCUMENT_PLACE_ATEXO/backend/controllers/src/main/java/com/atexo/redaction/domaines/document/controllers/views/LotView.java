package com.atexo.redaction.domaines.document.controllers.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LotView {
	private Integer id;

	private String numero;

	private String libelle;

	private String description;

	private Set<CPVView> cpv;
}
