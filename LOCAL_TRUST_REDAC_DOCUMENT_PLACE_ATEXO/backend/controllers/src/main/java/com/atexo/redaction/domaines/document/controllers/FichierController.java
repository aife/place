package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.views.TeleversementView;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.Utilisateur;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.UUID;

public interface FichierController {
	/**
	 * Téléverse un nouveau document
	 *
	 * @param request la requête
	 *
	 * @return le document uploadé
	 */
	TeleversementView televerser(Utilisateur utilisateur, Contexte contexte, MultipartHttpServletRequest request);

	/**
	 * Permet de télécharger un document
	 *
	 * @param uuid l'identifiant du fichier
	 * @param nom le nom du document
	 * @param extension l'extension du fichier
	 */
	Resource recuperer(Utilisateur utilisateur, Contexte contexte, UUID uuid, String nom, String extension);

	/**
	 * Permet de supprimer un document
	 *
	 * @param utilisateur l'utilisateur
	 * @param uuid l'identifiant du fichier
	 */
	void supprimer(Utilisateur utilisateur, Contexte contexte, UUID uuid);

	/**
	 * Permet de télécharger un document
	 *
	 * @param uuid l'identifiant du fichier
	 * @param nom le nom du document
	 * @param extension l'extension du fichier
	 * @param disposition la disposition du fichier en sortie
	 */
	ResponseEntity<Resource> telecharger(Utilisateur utilisateur, Contexte contexte, UUID uuid, String nom, String extension, ContentDisposition disposition);
}
