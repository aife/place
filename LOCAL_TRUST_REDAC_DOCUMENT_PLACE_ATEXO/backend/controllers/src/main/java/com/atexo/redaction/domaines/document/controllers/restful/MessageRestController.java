package com.atexo.redaction.domaines.document.controllers.restful;

import com.atexo.redaction.domaines.document.controllers.MessageController;
import com.atexo.redaction.domaines.document.model.messages.BaseMessage;
import com.atexo.redaction.domaines.document.services.MessageService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/messages")
@RequiredArgsConstructor
public class MessageRestController extends BaseRestController implements MessageController {

	@NonNull
	private final MessageService messageService;

	@Override
	@PostMapping(headers = HEADER_ACCEPT_JSON_V2, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseMessage envoyer(@RequestParam("topic") final String topic, @RequestBody final BaseMessage message) {
		this.messageService.notifier(message, topic);

		return message;
	}
}
