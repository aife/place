package com.atexo.redaction.domaines.document.controllers;

import com.atexo.redaction.domaines.document.controllers.io.ContentDisposition;
import com.atexo.redaction.domaines.document.controllers.views.GabaritView;
import com.atexo.redaction.domaines.document.model.*;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface GabaritController {
	/**
	 * Permet de récupérer la surcharge eligible d'un gabarit pour un environnement et un modele
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param modele le modele
	 * @param environnement l'environnement
	 *
	 * @return le document créé
	 */
	GabaritView recupererVersionEligible(Utilisateur utilisateur, Contexte contexte, Modele modele, Environnement environnement);

	/**
	 * Permet de récuperer la surcharge pour l'agent
	 *
	 * @param utilisateur l'agent
	 * @param contexte le contexte
	 * @param modele le modele de document
	 * @param agent l'agent
	 *
	 * @return le gabarit
	 */
	GabaritView recuperer(Utilisateur utilisateur, Contexte contexte, Modele modele, Utilisateur agent);

	/**
	 * Permet de récuperer la surcharge pour l'organisme
	 *
	 * @param utilisateur l'agent
	 * @param contexte le contexte
	 * @param modele le modele de document
	 * @param service le service
	 *
	 * @return le gabarit
	 */
	GabaritView recuperer(Utilisateur utilisateur, Contexte contexte, Modele modele, Service service);

	/**
	 * Permet de récuperer la surcharge pour l'organisme
	 *
	 * @param utilisateur l'agent
	 * @param contexte le contexte
	 * @param modele le modele de document
	 * @param organisme l'organisme
	 *
	 * @return le gabarit
	 */
	GabaritView recuperer(Utilisateur utilisateur, Contexte contexte, Modele modele, Organisme organisme);

	/**
	 * Permet de récuperer la surcharge pour l'environnement
	 *
	 * @param utilisateur l'agent
	 * @param contexte le contexte
	 * @param modele le modele de document
	 * @param environnement l'environnement
	 *
	 * @return le gabarit
	 */
	GabaritView recuperer(Utilisateur utilisateur, Contexte contexte, Modele modele, Environnement environnement);

	/**
	 * Permet de créer une surcharge
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param surchargeDTO la surcharge
	 *
	 * @return la surcharge
	 */
	GabaritView creer(Utilisateur utilisateur, Contexte contexte, GabaritView surchargeDTO);

	/**
	 * Permet de récuperer les révisions d'une surcharge
	 *
	 * @param gabarit la surcharge du modele
	 *
	 * @return la liste des révisions
	 */
	List<GabaritView> lister(Gabarit gabarit);

	/**
	 * Permet de supprimer une surcharge
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param gabarit la surcharge
	 */
	void supprimer(Utilisateur utilisateur, Contexte contexte, Gabarit gabarit);

	/**
	 * Permet de télécharger une sauvegarde
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param gabarit la sauvegarde
	 * @param type le type du document
	 * @param disposition la disposition du fichier en sortie
	 *
	 * @return la redirection
	 */
	ResponseEntity<Resource> telecharger(Utilisateur utilisateur, Contexte contexte, Gabarit gabarit, TypeDocument type, ContentDisposition disposition);

	/**
	 * Permet de télécharger une révision
	 *
	 * @param utilisateur l'utilisateur
	 * @param contexte le contexte
	 * @param gabarit le gabarit
	 * @param uuid la révision
	 * @param type le type du document
	 * @param disposition la disposition du fichier en sortie
	 *
	 * @return la redirection
	 */
	ResponseEntity<Resource> telecharger(Utilisateur utilisateur, Contexte contexte, Gabarit gabarit, UUID uuid, TypeDocument type, ContentDisposition disposition);
}
