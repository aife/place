package com.atexo.redaction.domaines.document.controllers.views;

import lombok.*;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class ConnectionView {
	private UUID uuid;
	private Instant creation;
	private UtilisateurView utilisateur;
}
