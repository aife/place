package com.atexo.redaction.domaines.document.shared.helpers;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

public class EnumHelper {

	/**
	 * Trouver une valeur optionnelle dans l'énumeration
	 *
	 * @param clazz la classe de l'énumeration
	 * @param predicate le prédicat de recherche
	 * @param <T> le type de l'énumération
	 *
	 * @return la valeur cherchée, optionnelle
	 */
	public static <T extends Enum<T>> Optional<T> trouver(final Class<T> clazz, final Predicate<? super T> predicate) {
		Validate.notNull(clazz, "La classe de l'énumération doit être fourni");
		Validate.notNull(predicate, "Le prédicat de recherche ne peut pas être nul");

		return Arrays.stream(clazz.getEnumConstants()).filter(predicate).findFirst();
	}
}
