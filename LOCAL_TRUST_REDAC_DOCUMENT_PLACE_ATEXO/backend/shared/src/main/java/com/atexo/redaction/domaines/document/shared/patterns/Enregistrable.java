package com.atexo.redaction.domaines.document.shared.patterns;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

public interface Enregistrable {

	Long getId();

	String getChemin();

	@JsonIgnore
	default Path getEmplacement() {
		return Optional.ofNullable(this.getChemin()).map(Paths::get).orElseThrow(IllegalArgumentException::new);
	}

	default boolean estEnregistre() {
		return null != this.getChemin() && Paths.get(this.getChemin()).toFile().exists();
	}

	@JsonIgnore
	default File getFichier() {
		return this.getEmplacement().toFile();
	}

	UUID getUuid();

	String getExtension();

	String getNom();
}
