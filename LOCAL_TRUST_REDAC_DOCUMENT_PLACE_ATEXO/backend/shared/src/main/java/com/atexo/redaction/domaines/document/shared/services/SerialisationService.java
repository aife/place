package com.atexo.redaction.domaines.document.shared.services;

import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service de génération de fichier
 */
@Service
public interface SerialisationService {

	/**
	 * Permet de sérialiser l'objet
	 *
	 * @param payload les données brutes
	 *
	 * @return l'objet désérialisé
	 */
	<T> String serialise(final T payload);

	/**
	 * Permet de désérialiser l'objet
	 *
	 * @param serialisation l'objet à désérialiser
	 * @param clazz la classe de l'objet
	 *
	 * @return l'objet
	 */
	<T> Optional<T> deserialise(final String serialisation, final Class<T> clazz);
}
