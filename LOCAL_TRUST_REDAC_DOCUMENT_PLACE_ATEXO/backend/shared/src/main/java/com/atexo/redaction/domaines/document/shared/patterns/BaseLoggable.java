package com.atexo.redaction.domaines.document.shared.patterns;

import com.atexo.redaction.domaines.document.shared.exceptions.SerialisationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public abstract class BaseLoggable {

	private final ObjectMapper mapper;

	public BaseLoggable() {
		this.mapper = new ObjectMapper();

		var hibernate5Module = new Hibernate5Module();
		hibernate5Module.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, false);
		var javaTimeModule = new JavaTimeModule();

		mapper.registerModules(javaTimeModule, hibernate5Module);

		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
	}

	@Override
	public String toString() {
		try {
			return mapper.writeValueAsString(this);
		} catch (final JsonProcessingException e) {
			// Nothing to do
			throw new SerialisationException("Impossible d'afficher l'objet {}", this, e);
		}
	}
}
