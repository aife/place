package com.atexo.redaction.domaines.document.shared.helpers;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.w3c.dom.Attr;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Optional;

@Slf4j
public class XmlHelper {

	private static final XPathFactory xpathFactory = XPathFactory.newInstance();

	private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();

	private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

	static {
		documentBuilderFactory.setNamespaceAware(true);
	}

	public static Optional<String> extraire(@NonNull final String xml, @NonNull final String expression) {
		Validate.notEmpty(xml);
		Validate.notEmpty(expression);

		String result = null;

		try (final var reader = new StringReader(xml)) {
			result = xpathFactory.newXPath().compile(expression).evaluate(new InputSource(reader));
		} catch (final XPathExpressionException xpe) {
			log.error("Impossible de récupérer l'attribut {} du XML {}", expression, xml);
		}

		return Optional.ofNullable(null == result || result.isEmpty() ? null : result);
	}

	public static String supprimer(@NonNull final String xml, @NonNull final String expression) {
		Validate.notEmpty(xml);
		Validate.notEmpty(expression);

		String result = xml;

		try (final var reader = new StringReader(xml); final var writer = new StringWriter()) {
			final var document = documentBuilderFactory.newDocumentBuilder().parse(new InputSource(reader));

			final var attribute = (Attr) xpathFactory.newXPath().compile(expression).evaluate(document, XPathConstants.NODE);

			if (null != attribute) {
				var element = attribute.getOwnerElement();

				if (null != element) {
					element.removeAttribute(attribute.getNodeName());
				}
			}

			final var transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			transformer.transform(new DOMSource(document), new StreamResult(writer));

			result = writer.toString();
		} catch (final SAXException | XPathExpressionException | IOException | ParserConfigurationException |
		               TransformerException e) {
			log.error("Impossible de supprimer l'attribut {} du XML {}", expression, xml);
		}

		return result;
	}
}
