package com.atexo.redaction.domaines.document.shared.helpers;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.util.StopWatch;

@Slf4j
public class EnhancedStopWatch extends StopWatch {

	public void start(String task) throws IllegalStateException {
		log.info(task);
		super.start(task);
	}

	public void start(String task, Object arg1) throws IllegalStateException {
		var message = MessageFormatter.format(task, arg1).getMessage();
		this.start(message);
	}

	public void start(String taskName, Object arg1, Object arg2) throws IllegalStateException {
		var message = MessageFormatter.format(taskName, arg1, arg2).getMessage();
		this.start(message);
	}

	public void start(String taskName, Object[] args) throws IllegalStateException {
		var message = MessageFormatter.format(taskName, args).getMessage();
		this.start(message);
	}

}
