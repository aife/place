package com.atexo.redaction.domaines.document.shared.patterns;

import java.io.IOException;

@FunctionalInterface
public interface Serialisable {

	String serialiser() throws IOException;
}
