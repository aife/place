package com.atexo.redaction.domaines.document.shared.web;


import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Resource locator
 * <p>
 * Typically, a wrapper for URL : add convenience methods.
 */
public class Location implements Serializable {

	/**
	 * Http/s scheme
	 */
	private static final String HTTP_SCHEME = "^http(s?)://";

	/**
	 * Empty string
	 */
	private static final String EMPTY = "";

	/**
	 * URL pattern
	 */
	private static final String URL_PATTERN = "{0}://{1}/{2}";

	/**
	 * Slash
	 */
	private static final String SLASH = "/";

	/**
	 * The URL
	 */
	@JsonValue
	private final String link;

	/**
	 * Constructor
	 *
	 * @param link the uri
	 * @param args the args
	 */
	public Location(final String link, final Object... args) {
		this.link = MessageFormat.format(link, args);
	}

	/**
	 * Constructor
	 *
	 * @param link the uri
	 */
	public Location(final String link) {
		Validate.notNull(link, "Le lien doit etre fourni");

		if (link.endsWith("/")) {
			this.link = link.substring(0, link.length() - 1);
		} else {
			this.link = link;
		}
	}

	/**
	 * Create a new locator formatting existing with parameters
	 *
	 * @param args the args
	 *
	 * @return the locator
	 */
	public Location format(final Object... args) {
		return new Location(MessageFormat.format(link, args));
	}

	/**
	 * Export {@link Location location} as {@link URL}
	 *
	 * @return the url.
	 */
	private URL toURL() throws MalformedURLException {
		return new URL(link);
	}

	/**
	 * Export {@link Location location} as {@link URI}
	 *
	 * @return the url.
	 */
	public URI toURI() {
		return URI.create(link);
	}

	@Override
	public String toString() {
		return this.link;
	}

	/**
	 * Combine location with path
	 *
	 * @return the url
	 */
	public Location combine(final String with) {
		Validate.notNull(with, "L'url a ajouter doit être fournie");

		String path;
		if (with.startsWith("/")) {
			path = with.substring(1);
		} else {
			path = with;
		}

		return new Location(this.link + '/' + path);
	}

	/**
	 * Resolve location
	 *
	 * @param location the location
	 *
	 * @return the url
	 */
	public URL relocate(final String location) throws MalformedURLException {
		String result = location;

		if (location.startsWith(SLASH)) {
			final URL url = toURL();

			result = MessageFormat.format(URL_PATTERN, url.getProtocol(), url.getHost(), location);
		}

		return new URL(result);
	}

	/**
	 * Check if an url is like one of the domains in parameters
	 *
	 * @param domains the domains to check if the url cover one of them
	 *
	 * @return true if the url match one of the domains, false otherwise
	 */
	public boolean match(final String... domains) {
		return Stream.of(domains).anyMatch((final String domain) -> this.link.contains(domain.replaceAll(HTTP_SCHEME, EMPTY)));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Location location = (Location) o;
		return Objects.equals(link, location.link);
	}

	@Override
	public int hashCode() {
		return Objects.hash(link);
	}
}
