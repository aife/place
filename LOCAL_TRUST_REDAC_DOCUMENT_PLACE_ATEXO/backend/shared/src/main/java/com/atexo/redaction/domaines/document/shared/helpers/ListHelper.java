package com.atexo.redaction.domaines.document.shared.helpers;

import java.util.Collection;
import java.util.stream.Collectors;

public class ListHelper {

	private static final String NON_ALPHANUMERIC = "([^a-zA-Z0-9 ])";

	/**
	 * Permet de transformer une liste d'enumeration en liste de string
	 *
	 * @param items la liste a transformer
	 *
	 * @return la liste
	 */
	public static Collection<String> toString(Collection<? extends Enum<?>> items) {
		Collection<String> result = null;

		if (null != items) {
			result = items.stream().map(Enum::name).collect(Collectors.toSet());
		}

		return result;
	}

	/**
	 * Permet de sérialiser une liste de string
	 *
	 * @param items la liste à sérialiser
	 *
	 * @return la liste
	 */
	public static String serialiser(Collection<String> items) {
		String result = null;

		if (null != items) {
			result = String.join("", items);
		}

		return result;
	}

	/**
	 * Permet de sécuriser une chaine de caractere
	 *
	 * @param value la valeur a securiser
	 *
	 * @return la liste
	 */
	public static String securiser(String value) {
		return null == value ? null : value.replaceAll(NON_ALPHANUMERIC, "[$1]");
	}
}
