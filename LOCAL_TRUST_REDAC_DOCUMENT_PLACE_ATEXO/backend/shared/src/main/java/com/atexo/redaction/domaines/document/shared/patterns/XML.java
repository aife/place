package com.atexo.redaction.domaines.document.shared.patterns;

import com.atexo.redaction.domaines.document.shared.exceptions.SerialisationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public abstract class XML implements Serialisable {

	private static final ObjectMapper MAPPER = new XmlMapper();

	@Override
	public String serialiser() {
		try {
			return MAPPER.writeValueAsString(this);
		} catch (final JsonProcessingException jpe) {
			throw new SerialisationException(jpe);
		}
	}
}
