package com.atexo.redaction.domaines.document.shared.exceptions;

import lombok.Builder;

import java.text.MessageFormat;

/**
 * Exception pour sérialisation invalide
 */
public class SerialisationException extends RuntimeException {

	@Builder(builderMethodName = "with")
	public SerialisationException(final String message, final Object... params) {
		super(MessageFormat.format(message, params));
	}

	@Builder(builderMethodName = "with")
	public SerialisationException(final Throwable throwable) {
		super("Erreur de sérialisation", throwable);
	}
}
