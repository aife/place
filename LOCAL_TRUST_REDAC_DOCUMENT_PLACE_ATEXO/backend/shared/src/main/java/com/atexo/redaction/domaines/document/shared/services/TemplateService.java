package com.atexo.redaction.domaines.document.shared.services;

public interface TemplateService {

	<T> String merge(String template, T data);
}
