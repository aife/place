package com.atexo.redaction.domaines.document.shared.services;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;

@Service
@Slf4j
@RequiredArgsConstructor
public class FreemarkerLocalService implements TemplateService {

	@NonNull
	private Configuration configuration;

	@Override
	public <T> String merge(String name, T data) {
		var out = new StringWriter();

		try {
			var template = configuration.getTemplate(name);

			template.process(data, out);
		} catch (IOException | TemplateException e) {
			throw new com.atexo.redaction.domaines.document.shared.exceptions.TemplateException(name);
		}

		return out.toString();
	}
}
