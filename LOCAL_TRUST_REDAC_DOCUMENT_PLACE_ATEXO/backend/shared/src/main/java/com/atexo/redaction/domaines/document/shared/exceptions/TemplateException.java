package com.atexo.redaction.domaines.document.shared.exceptions;

import lombok.Builder;

import java.text.MessageFormat;

/**
 * Modèles non trouvé
 */
public class TemplateException extends RuntimeException {

	@Builder(builderMethodName = "with")
	public TemplateException(final String modele) {
		super(MessageFormat.format("Une erreur s'est produite lors du process du template {}", modele));
	}
}
