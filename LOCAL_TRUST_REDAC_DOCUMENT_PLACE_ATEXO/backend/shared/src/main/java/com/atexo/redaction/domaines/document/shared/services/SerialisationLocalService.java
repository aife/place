package com.atexo.redaction.domaines.document.shared.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SerialisationLocalService implements SerialisationService {

	@NonNull
	private final ObjectMapper objectMapper;

	@Override
	public <T> String serialise(final T payload) {
		String result = null;
		try {
			result = objectMapper.writeValueAsString(payload);
		} catch (final JsonProcessingException e) {
			log.error("Impossible de sérialiser le contexte {}", payload);
		}

		return result;
	}

	@Override
	public <T> Optional<T> deserialise(final String contenu, final Class<T> clazz) {
		T result = null;

		try {
			result = objectMapper.readValue(contenu, clazz);
		} catch (final JsonProcessingException e) {
			log.error("Impossible de désérialiser le contexte {}", contenu);
		}

		return Optional.ofNullable(result);
	}
}
