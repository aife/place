package com.atexo.redaction.domaines.document.shared.patterns;

import java.util.List;
import java.util.Set;

/**
 * Contrat pour un objet convertible
 *
 * @param <U> le type d'entrée
 * @param <V> le type de conversion
 */
public interface Convertible<U, V> {

	V map(U from);

	List<V> map(List<U> from);

	Set<V> map(Set<U> from);
}
