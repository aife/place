package com.atexo.redaction.domaines.document.shared.helpers;

import com.atexo.redaction.domaines.document.shared.tests.TestResource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.xmlunit.assertj3.XmlAssert;

import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@DisplayName("Tests de l'utilitaire de manipulation des fichiers XML")
@Slf4j
public class XmlHelperTest {

	private static Stream<Arguments> GIVEN_un_XML_et_une_expression_XPath_valides_WHEN_extraire_THEN_le_resultat_est_conforme() {
		return Stream.of(
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml"), "//document/@progress", "35.555557"),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml"), "//document/@commentaire", null),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/1a19db9e-cdb4-4192-bd6e-f5f346b20e68.xml"), "//document/@commentaire", "Invalidation du document par Marc Passet"),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/1a19db9e-cdb4-4192-bd6e-f5f346b20e68.xml"), "//document/@outputFormat", "pdf"),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/d3485590-f1da-462d-aaa3-6991329307d1.xml"), "//document/@lastAction", "SAVE"),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/d3485590-f1da-462d-aaa3-6991329307d1.xml"), "//document/@ref", null)
		);
	}

	/**
	 * Test des cas passants de la méthode {@link XmlHelper#extraire(String, String)}
	 *
	 * @param xml le xml dont on cherche à extraire l'attribut
	 * @param expression l'expression
	 * @param expected l'attendue
	 */
	@DisplayName("Test d'extraction d'un attribut - cas passants")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_un_XML_et_une_expression_XPath_valides_WHEN_extraire_THEN_le_resultat_est_conforme(final TestResource xml, final String expression, final String expected) {
		assertThat(XmlHelper.extraire(xml.content(), expression)).isEqualTo(Optional.ofNullable(expected));
	}

	private static Stream<Arguments> GIVEN_un_XML_et_une_expression_XPath_nulles_ou_vides_WHEN_extraire_THEN_une_exception_est_levee() {
		return of(
				arguments(null, "//document/@progress", NullPointerException.class),
				arguments("", "//document/@progress", IllegalArgumentException.class),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml").content(), null, NullPointerException.class),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml").content(), "", IllegalArgumentException.class),
				arguments(null, null, NullPointerException.class),
				arguments("", null, NullPointerException.class),
				arguments(null, "", NullPointerException.class),
				arguments("", "", IllegalArgumentException.class)
		);
	}

	/**
	 * Test des cas limites de la méthode {@link XmlHelper#extraire(String, String)}
	 *
	 * @param xml le xml dont on cherche à extraire l'attribut
	 * @param expression l'expression
	 * @param expected l'attendue
	 */
	@DisplayName("Test d'extraction d'un attribut - parametres nuls et/ou vides")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_un_XML_et_une_expression_XPath_nulles_ou_vides_WHEN_extraire_THEN_une_exception_est_levee(final String xml, final String expression, final Class<Throwable> expected) {
		assertThrows(expected, () -> XmlHelper.extraire(xml, expression));
	}

	private static Stream<Arguments> GIVEN_un_XML_et_une_expression_XPath_valides_WHEN_supprimer_THEN_l_attribut_est_bien_supprime() {
		return of(
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/1a19db9e-cdb4-4192-bd6e-f5f346b20e68.xml"), "//document/@lastAction", new TestResource("com/atexo/redaction/domaines/document/shared/helpers/1a19db9e-cdb4-4192-bd6e-f5f346b20e69.xml"))
		);
	}

	/**
	 * Test des cas passants de la méthode {@link XmlHelper#supprimer(String, String)}
	 *
	 * @param xml le xml dont on cherche à extraire l'attribut
	 * @param expression l'expression
	 * @param expected l'attendue
	 */
	@DisplayName("Test de suppression d'un attribut - cas passants")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_un_XML_et_une_expression_XPath_valides_WHEN_supprimer_THEN_l_attribut_est_bien_supprime(final TestResource xml, final String expression, final TestResource expected) {
		final var result = XmlHelper.supprimer(xml.content(), expression);

		XmlAssert.assertThat(result).and(expected.content()).normalizeWhitespace().ignoreComments().areIdentical();
	}

	private static Stream<Arguments> GIVEN_un_XML_et_une_expression_XPath_nulles_ou_vides_WHEN_supprimer_THEN_une_exception_est_levee() {
		return of(
				arguments(null, "//document/@progress", NullPointerException.class),
				arguments("", "//document/@progress", IllegalArgumentException.class),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml").content(), null, NullPointerException.class),
				arguments(new TestResource("com/atexo/redaction/domaines/document/shared/helpers/f3fc205b-44fa-4005-9e7b-46259c38856a.xml").content(), "", IllegalArgumentException.class),
				arguments(null, null, NullPointerException.class),
				arguments("", null, NullPointerException.class),
				arguments(null, "", NullPointerException.class),
				arguments("", "", IllegalArgumentException.class)
		);
	}

	/**
	 * Test des cas limites de la méthode {@link XmlHelper#supprimer(String, String)}
	 *
	 * @param xml le xml dont on cherche à extraire l'attribut
	 * @param expression l'expression
	 * @param expected l'attendue
	 */
	@DisplayName("Test de suppression d'un attribut - parametres nuls et/ou vides")
	@ParameterizedTest
	@MethodSource
	public void GIVEN_un_XML_et_une_expression_XPath_nulles_ou_vides_WHEN_supprimer_THEN_une_exception_est_levee(final String xml, final String expression, final Class<Throwable> expected) {
		assertThrows(expected, () -> XmlHelper.supprimer(xml, expression));
	}
}
