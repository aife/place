package com.atexo.redaction.domaines.document.connectors.clausier;

import com.atexo.redaction.domaines.document.connectors.clausier.views.AgentClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.CanevasClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.ConsultationClausier;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.shared.web.Location;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public interface ClausierConnector {

	Set<CanevasClausier> rechercherListeCanevas(final Location uri);

	String genererCanevas(final Location uri);

	Optional<ConsultationClausier> rechercherConsultation(final Location uri, final Contexte contexte);

	Set<AgentClausier> rechercherListeUtilisateurs(final Location uri);
}
