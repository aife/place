package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor(force = true)
public class RevisionDocgen {
	@JsonProperty("key")
	private String cle;

	@JsonProperty("documentId")
	@NonNull
	private String identifiantExterne;

	@JsonProperty("status")
	@NonNull
	private RevisionEtatDocgen etat;

	@JsonProperty("redacAction")
	private RevisionActionDocgen action;

	@JsonProperty("redacStatus")
	@NonNull
	private RevisionStatutDocgen statut;

	@JsonProperty("version")
	@NonNull
	private Integer version;

	@NonNull
	private Instant dateCreation;

	@NonNull
	private Instant dateModification;

	@JsonProperty("creationDate")
	public void setDateCreation(final Long millisecond) {
		if (null != millisecond) {
			dateCreation = Instant.ofEpochMilli(millisecond);
		}
	}

	@JsonProperty("modificationDate")
	public void setDateModification(final Long millisecond) {
		if (null != millisecond) {
			dateModification = Instant.ofEpochMilli(millisecond);
		}
	}
}
