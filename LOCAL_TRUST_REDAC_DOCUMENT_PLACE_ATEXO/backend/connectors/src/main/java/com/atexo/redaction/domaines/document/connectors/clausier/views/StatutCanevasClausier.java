package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class StatutCanevasClausier {
	private Integer id;
	private String libelle;
}
