package com.atexo.redaction.domaines.document.connectors.messec.configuration;

import com.atexo.redaction.domaines.document.shared.patterns.Json;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@ConfigurationProperties("messec")
@Getter
@Setter
@NoArgsConstructor(force = true)
@Slf4j
@Profile("messec")
public class MessecConfiguration extends Json {

	private Plateforme emetteur;

	private Plateforme destinataire;

	private Reponse reponse;

	private Contact expediteur;

	private Message message;

	private String objet;

	@PostConstruct
	public void load() throws JsonProcessingException {
		log.info("Chargement de la configuration MESSEC = {}", this.serialiser());
	}
}
