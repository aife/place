package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class EnvironnementClausier {

	private String client;

	private String plateforme;

	private String mpeUuid;
}
