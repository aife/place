package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Set;

@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "with")
@ToString(of = "nom")
@Getter
@Setter
public class AuteurDocgen implements Serializable {

	@NonNull
	@JsonProperty("id")
	private String id;

	@NonNull
	@JsonProperty("name")
	private String nom;

	@Singular
	@JsonProperty("roles")
	private Set<String> autorisations;
}
