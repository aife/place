package com.atexo.redaction.domaines.document.connectors.messec.views;

import com.atexo.redaction.domaines.document.shared.patterns.Json;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import org.springframework.context.annotation.Profile;

@Profile("messec")
@AllArgsConstructor
@Builder(builderMethodName = "with")
public class TagMessec extends Json {

	@NonNull
	@JsonProperty("cle")
	private String cle;

	@NonNull
	@JsonProperty("code")
	private String code;

	@NonNull
	@JsonProperty("valeur")
	private String valeur;

}
