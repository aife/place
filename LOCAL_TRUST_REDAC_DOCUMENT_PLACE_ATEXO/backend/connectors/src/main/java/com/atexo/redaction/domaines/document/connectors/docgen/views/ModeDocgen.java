package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.atexo.redaction.domaines.document.shared.helpers.EnumHelper;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.Optional;

/**
 * Mode d'édition
 */
@AllArgsConstructor
public enum ModeDocgen implements Serializable {

	EDITION("edit"),
	VISUALISATION("view"),
	REDACTION("redac");

	@Getter(onMethod = @__(@JsonValue))
	private final String libelle;

	/**
	 * Permet de trouver une valeur de l'énumération depuis son libellé
	 *
	 * @param libelle le libellé
	 *
	 * @return la valeur de l'énumération, optionnelle
	 */
	public static Optional<ModeDocgen> trouver(final String libelle) {
		Validate.notNull(libelle, "Le libellé de la valeur recherche ne peut pas être nul");

		return EnumHelper.trouver(ModeDocgen.class, item -> libelle.equals(item.getLibelle()));
	}
}
