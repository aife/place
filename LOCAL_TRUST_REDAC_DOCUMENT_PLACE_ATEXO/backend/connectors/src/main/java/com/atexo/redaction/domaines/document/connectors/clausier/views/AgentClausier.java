package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class AgentClausier {
	private ServiceClausier service;

	private String nom;

	private String prenom;

	private String email;

	private String identifiantExterne;
}
