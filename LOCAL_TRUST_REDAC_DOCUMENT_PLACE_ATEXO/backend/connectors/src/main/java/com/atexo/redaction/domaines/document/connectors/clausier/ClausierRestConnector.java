package com.atexo.redaction.domaines.document.connectors.clausier;

import com.atexo.redaction.domaines.document.connectors.BaseRestDocumentService;
import com.atexo.redaction.domaines.document.connectors.clausier.views.AgentClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.CanevasClausier;
import com.atexo.redaction.domaines.document.connectors.clausier.views.ConsultationClausier;
import com.atexo.redaction.domaines.document.model.Contexte;
import com.atexo.redaction.domaines.document.model.exceptions.api.contexte.ConsultationIntrouvableException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.CreationDocumentException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.RechercheCavenasException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.RechercheConsultationException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.RechercheUtilisateurException;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClausierRestConnector extends BaseRestDocumentService implements ClausierConnector {

	@Override
	public Set<CanevasClausier> rechercherListeCanevas(final Location uri) {
		final Set<CanevasClausier> result;
		try {
			log.info("Récupération des canevas depuis l'URL de recherche de canevas fourni dans le contexte de l'agent = {}", uri);

			result = this.fetch(HttpMethod.GET, uri, new ParameterizedTypeReference<Set<CanevasClausier>>() {
					})
					.orElse(Collections.emptySet());

			log.trace("Liste des canevas recupérés du clausier = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw RechercheCavenasException.with()
					.uri(uri)
					.exception(exception)
					.build();
		}

		return result;
	}

	@Override
	public String genererCanevas(final Location uri) {
		final String result;

		log.info("Génération du clausier depuis l'URL = {}", uri);

		try {
			var response = this.http(HttpMethod.GET, uri, String.class);

			// Si contenu partiel, on continue
			while (response.getStatusCode() == HttpStatus.PARTIAL_CONTENT) {
				// on continue
				response = this.http(HttpMethod.GET, uri, String.class);
			}

			result = response.getBody();

			log.trace("XML récupéré = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw CreationDocumentException
					.with()
					.uri(uri)
					.exception(exception)
					.build();
		}

		return result;
	}

	@Override
	public Optional<ConsultationClausier> rechercherConsultation(final Location uri, final Contexte contexte) {
		final ConsultationClausier result;
		log.info("Recuperation de la consultation depuis l'URL de récupération de la consultation fourni dans le contexte de l'agent = {}", uri);

		try {
			result = this.fetch(HttpMethod.GET, uri, ConsultationClausier.class).orElseThrow(() -> new ConsultationIntrouvableException(contexte));

			log.trace("Consultation recupérée du clausier = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw RechercheConsultationException
					.with()
					.uri(uri)
					.exception(exception)
					.build();
		}

		return Optional.ofNullable(result);
	}

	@Override
	public Set<AgentClausier> rechercherListeUtilisateurs(final Location uri) {
		final Set<AgentClausier> result;
		try {
			log.info("Récupération des agents depuis l'URL de recherche de agents fourni dans le contexte de l'agent = {}", uri);

			result = this.fetch(HttpMethod.GET, uri, new ParameterizedTypeReference<Set<AgentClausier>>() {
					})
					.orElse(Collections.emptySet());

			log.info("Liste des agents recupérés du clausier = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw RechercheUtilisateurException
					.with()
					.uri(uri)
					.exception(exception)
					.build();
		}

		return result;
	}
}
