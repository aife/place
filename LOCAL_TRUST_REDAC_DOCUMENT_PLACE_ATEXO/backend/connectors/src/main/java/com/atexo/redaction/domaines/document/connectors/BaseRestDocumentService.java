package com.atexo.redaction.domaines.document.connectors;

import com.atexo.redaction.domaines.document.shared.web.Location;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;

import java.util.Optional;

public abstract class BaseRestDocumentService extends BaseRestService {

	protected static final MediaType OPEN_XML = MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

	protected static final MediaType JSON = MediaType.APPLICATION_JSON;

	protected static final MediaType XML = MediaType.APPLICATION_XML;

	protected static final MediaType FORM_DATA = MediaType.MULTIPART_FORM_DATA;

	protected HttpHeaders creerEntete(final MediaType mediaType) {
		final var result = new HttpHeaders();

		result.setContentType(mediaType);

		return result;
	}

	protected <T, U> ResponseEntity<T> http(HttpMethod method, Location uri, LinkedMultiValueMap<String, HttpEntity<?>> body, Class<T> clazz) {
		return getRestTemplate().exchange(uri.toURI(), method, new HttpEntity<>(body, creerEntete(FORM_DATA)), clazz);
	}

	protected <T, U> ResponseEntity<T> http(HttpMethod method, Location uri, HttpEntity<U> body, Class<T> clazz) {
		return getRestTemplate().exchange(uri.toURI(), method, body, clazz);
	}

	protected <T, U> ResponseEntity<T> http(HttpMethod method, Location uri, Class<T> clazz) {
		return getRestTemplate().exchange(uri.toURI(), method, null, clazz);
	}

	protected <T> ResponseEntity<T> http(HttpMethod method, Location uri, ParameterizedTypeReference<T> responseType) {
		return getRestTemplate().exchange(uri.toURI(), method, null, responseType);
	}

	protected <T, U> Optional<T> fetch(HttpMethod method, Location uri, LinkedMultiValueMap<String, HttpEntity<?>> body, Class<T> clazz) {
		return Optional.ofNullable(this.http(method, uri, new HttpEntity<>(body, creerEntete(FORM_DATA)), clazz).getBody());
	}

	protected <T, U> Optional<T> fetch(HttpMethod method, Location uri, HttpEntity<U> body, Class<T> clazz) {
		return Optional.ofNullable(this.http(method, uri, body, clazz).getBody());
	}

	protected <T, U> Optional<T> fetch(HttpMethod method, Location uri, Class<T> clazz) {
		return Optional.ofNullable(this.http(method, uri, clazz).getBody());
	}

	protected <T> Optional<T> fetch(HttpMethod method, Location uri, ParameterizedTypeReference<T> responseType) {
		return Optional.ofNullable(this.http(method, uri, responseType).getBody());
	}
}
