package com.atexo.redaction.domaines.document.connectors.messec.views;

import com.atexo.redaction.domaines.document.shared.patterns.Json;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.context.annotation.Profile;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Profile("messec")
public class DestinataireMessec extends Json {

	@JsonProperty("typeDestinataire")
	private String typeDestinataire;

	@JsonProperty("idEntrepriseDest")
	private Long idEntreprise;

	@JsonProperty("idContactDest")
	private Long idContact;

	@JsonProperty("nomEntrepriseDest")
	private String nomEntreprise;

	@NonNull
	@JsonProperty("nomContactDest")
	private String nom;

	@NonNull
	@JsonProperty("mailContactDestinataire")
	private String mail;

	@JsonProperty("type")
	private String type;

	@JsonProperty("ordre")
	private Long ordre;

}
