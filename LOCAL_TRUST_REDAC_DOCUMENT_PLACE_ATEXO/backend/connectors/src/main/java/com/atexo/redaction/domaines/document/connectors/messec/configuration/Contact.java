package com.atexo.redaction.domaines.document.connectors.messec.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

@Getter
@Setter
@NoArgsConstructor(force = true)
@Profile("messec")
public class Contact {

	private String nom;
	private String envoi;
	private String reponse;
	private String alerte;
}
