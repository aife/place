package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class OrganismeClausier {
	private String libelle;

	private String acronyme;

	private Long identifiantExterne;


	private EnvironnementClausier environnement;
}
