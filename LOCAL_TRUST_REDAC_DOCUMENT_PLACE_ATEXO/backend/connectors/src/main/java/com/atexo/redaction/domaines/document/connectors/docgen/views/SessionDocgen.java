package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.atexo.redaction.domaines.document.shared.web.Location;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Builder(builderMethodName = "with")
public class SessionDocgen implements Serializable {

	private final AuteurDocgen editeur;

	private String id;

	private String titre;

	private final ModeDocgen mode;

	private final Location retour;

	private final PlateformeDocgen plateforme;

	@Override
	public String toString() {
		return "[ utilisateur='" + editeur + "', mode='" + mode + "', titre='" + titre + "', plateforme='" + plateforme + "', uuid='" + id + "', url de retour='" + retour + "' ]";
	}

	@JsonProperty("documentId")
	public String getIdDocument() {
		return id;
	}

	@JsonProperty("documentTitle")
	public String getTitreDocument() {
		return titre;
	}

	@JsonProperty("mode")
	public ModeDocgen getMode() {
		return mode;
	}

	@JsonProperty("callback")
	public Location getRetour() {
		return retour;
	}

	@JsonProperty("user")
	public AuteurDocgen getEditeur() {
		return editeur;
	}

	@JsonProperty("plateformeId")
	public String getPlateformeId() {
		return plateforme.getId();
	}
}
