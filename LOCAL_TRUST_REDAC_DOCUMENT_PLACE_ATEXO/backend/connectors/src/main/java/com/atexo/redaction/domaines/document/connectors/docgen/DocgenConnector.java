package com.atexo.redaction.domaines.document.connectors.docgen;

import com.atexo.redaction.domaines.document.connectors.BaseRestDocumentService;
import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionDocgen;
import com.atexo.redaction.domaines.document.connectors.docgen.views.RevisionEtatDocgen;
import com.atexo.redaction.domaines.document.connectors.docgen.views.SessionDocgen;
import com.atexo.redaction.domaines.document.model.ChampFusion;
import com.atexo.redaction.domaines.document.model.Jeton;
import com.atexo.redaction.domaines.document.model.Plateforme;
import com.atexo.redaction.domaines.document.model.exceptions.api.jeton.JetonInvalideException;
import com.atexo.redaction.domaines.document.model.exceptions.edition.*;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class DocgenConnector extends BaseRestDocumentService {

	@Value("${url.affichage}")
	private Location affichageURL;

	@Value("${url.conversion.document}")
	private Location conversionDocumentURL;

	@Value("${url.edition.document-simple}")
	private Location editionDocumentSimpleURL;

	@Value("${url.edition.document-complexe}")
	private Location editionDocumentComplexeURL;

	@Value("${url.generation.document-simple}")
	private Location generationDocumentSimpleURL;

	@Value("${url.generation.document-complexe}")
	private Location generationDocumentComplexeURL;

	@Value("${url.monitoring.platforme}")
	private Location monitoringPlateformeURL;

	@Value("${url.telecharger}")
	private Location telechargementURL;

	@Value("${url.validation.document-simple}")
	private Location validationDocumentSimpleURL;

	@Value("${url.validation.document-complexe}")
	private Location validationDocumentComplexteURL;

	@PostConstruct
	private void echo() {
		log.info("\uD83D\uDD17 URL docgen pour l'affichage d'un document = '{}'", this.affichageURL);
		log.info("\uD83D\uDD17 URL docgen pour la conversion d'un document = '{}'", this.conversionDocumentURL);
		log.info("\uD83D\uDD17 URL docgen pour l'édition d'un document simple = '{}'", this.editionDocumentSimpleURL);
		log.info("\uD83D\uDD17 URL docgen pour l'édition d'un document complexe =  '{}'", this.editionDocumentComplexeURL);
		log.info("\uD83D\uDD17 URL docgen pour la génération d'un document simple = '{}'", this.generationDocumentSimpleURL);
		log.info("\uD83D\uDD17 URL docgen pour la génération d'un document complexe =  '{}'", this.generationDocumentComplexeURL);
		log.info("\uD83D\uDD17 URL docgen pour la validation d'un document simple  = '{}'", this.validationDocumentSimpleURL);
		log.info("\uD83D\uDD17 URL docgen pour la validation d'un document complexe = '{}'", this.validationDocumentComplexteURL);
		log.info("\uD83D\uDD17 URL docgen pour le monitoring de la plateforme = '{}'", this.monitoringPlateformeURL);
		log.info("\uD83D\uDD17 URL docgen pour le téléchargement d'un document = '{}'", this.telechargementURL);
	}

	public ByteArrayResource convertir(final Resource fichier, final String type) {
		final var uri = this.conversionDocumentURL.format(type);

		final ByteArrayResource result;

		try {
			log.info("Envoi du fichier {} a docgen pour conversion. URL = {}", fichier, uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();

			body.add("document", new HttpEntity<>(fichier, this.creerEntete(FORM_DATA)));

			result = this.fetch(HttpMethod.POST, uri, body, ByteArrayResource.class).orElseThrow(JetonInvalideException::new);

			log.trace("Document converti = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw ConversionDocumentException.with()
					.uri(uri)
					.type(type)
					.exception(exception)
					.build();
		}

		return result;
	}

	public Jeton editer(final Resource xml, final Resource document, final SessionDocgen session) {
		final var uri = this.editionDocumentComplexeURL;

		final Jeton result;

		try {
			log.info("Envoi du fichier {} avec le session {}. URL = {}", document, session, uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();
			body.add("xml", new HttpEntity<>(xml, this.creerEntete(OPEN_XML)));
			body.add("document", new HttpEntity<>(document, this.creerEntete(OPEN_XML)));
			body.add("editorRequest", new HttpEntity<>(session, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, String.class).map(Jeton::new).orElseThrow(JetonInvalideException::new);

			log.trace("Jeton récupéré suite a la demande d'édition = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			log.error("Impossible d'éditer le document '{}' avec les informations '{}'", document, session);
			throw ModificationDocumentException.with()
					.uri(uri)
					.document(document)
					.exception(exception)
					.build();
		}

		return result;
	}

	public Jeton editer(final Resource document, final SessionDocgen session) {
		final var uri = this.validationDocumentSimpleURL;

		final Jeton result;

		try {
			log.info("Envoi du document {} avec la session {} vers {}", document, session, uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();
			body.add("document", new HttpEntity<>(document, this.creerEntete(OPEN_XML)));
			body.add("editorRequest", new HttpEntity<>(session, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, String.class).map(Jeton::new).orElseThrow(JetonInvalideException::new);

			log.trace("Jeton récupéré suite a la demande d'édition = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			log.error("Impossible d'éditer le document '{}' avec les informations '{}'", document, session);
			throw ModificationDocumentException.with()
					.uri(uri)
					.document(document)
					.exception(exception)
					.build();
		}

		return result;
	}

	public Optional<Resource> telecharger(final Jeton jeton) {
		final var uri = this.telechargementURL.format(jeton.toString());
		final Optional<Resource> result;

		try {
			log.info("Téléchargement du document depuis docgen avec le jeton {} depuis {}", jeton, uri);

			result = this.fetch(HttpMethod.GET, uri, String.class)
					.map(value -> value.getBytes(StandardCharsets.ISO_8859_1))
					.map(ByteArrayResource::new);

			log.trace("Document récupéré avec succès = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			log.error("Impossible de telecharger le document depuis le jeton '{}'", jeton);
			throw TelechargementDocumentException.with()
					.uri(uri)
					.exception(exception)
					.build();
		}

		return result;
	}

	public Jeton valider(final Resource document, final SessionDocgen session) {
		final var uri = this.validationDocumentComplexteURL;

		final Jeton result;

		try {
			log.info("Envoi du document pour validation {} vers {}", document, uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();
			body.add("document", new HttpEntity<>(document, this.creerEntete(OPEN_XML)));
			body.add("editorRequest", new HttpEntity<>(session, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, String.class).map(Jeton::new).orElseThrow(JetonInvalideException::new);

			log.trace("Document validé avec succès = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw ValidationDocumentException.with()
					.uri(uri)
					.document(document)
					.exception(exception)
					.build();
		}

		return result;
	}

	public ByteArrayResource fusionner(final Resource gabarit, final Set<ChampFusion> contexte) {
		final var uri = this.generationDocumentSimpleURL;

		final ByteArrayResource result;

		try {
			log.info("Envoi du document simple à docgen pour fusion. URL = {}", uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();
			body.add("template", new HttpEntity<>(gabarit, this.creerEntete(OPEN_XML)));
			body.add("keyValues", new HttpEntity<>(contexte, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, ByteArrayResource.class).orElseThrow(() ->
					GenerationDocumentException.with()
							.uri(uri)
							.document(gabarit)
							.build()
			);

			log.trace("Document fusionné = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw GenerationDocumentException.with()
					.uri(uri)
					.document(gabarit)
					.contexte(contexte)
					.exception(exception)
					.build();
		}

		return result;
	}

	public ByteArrayResource fusionner(final Resource xml, final Resource gabarit, final Set<ChampFusion> contexte) {
		final var uri = this.generationDocumentComplexeURL;

		final ByteArrayResource result;

		try {
			log.info("Envoi du document canevas à docgen pour fusion. URL = {}", uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();

			body.add("xml", new HttpEntity<>(xml, this.creerEntete(XML)));
			body.add("template", new HttpEntity<>(gabarit, this.creerEntete(OPEN_XML)));
			body.add("keyValues", new HttpEntity<>(contexte, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, ByteArrayResource.class).orElseThrow(() ->
					GenerationDocumentException.with()
							.uri(uri)
							.document(gabarit)
							.build()
			);

			log.trace("Document fusionné = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw GenerationDocumentException.with()
					.uri(uri)
					.document(gabarit)
					.contexte(contexte)
					.exception(exception)
					.build();
		}

		return result;
	}

	public Set<RevisionDocgen> verifier(final Plateforme plateforme) {
		final var uri = this.monitoringPlateformeURL.format(plateforme);
		final Set<RevisionDocgen> result;

		try {
			log.trace("Vérification des fichiers de la plateforme {} à l'adresse {}", plateforme, uri);
			final var views = this.fetch(HttpMethod.GET, uri, new ParameterizedTypeReference<Set<RevisionDocgen>>() {
					})
					.orElseThrow(() -> VerificationDocumentException.with().uri(uri).plateforme(plateforme).build());

			result = views.stream()
					.filter(Objects::nonNull)
					.filter(view -> !RevisionEtatDocgen.CORROMPU.equals(view.getEtat()))
					.collect(Collectors.toSet());

			log.trace("Liste des révisions = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw VerificationDocumentException.with()
					.uri(uri)
					.plateforme(plateforme)
					.build();
		}

		return result;
	}


	public Jeton visualiser(final Resource document, final SessionDocgen session) {
		final var uri = this.editionDocumentSimpleURL;

		final Jeton result;

		try {
			log.info("\uD83D\uDCE8 Envoi du document {} vers l'URL = {}", document, uri);

			final var body = new LinkedMultiValueMap<String, HttpEntity<?>>();
			body.add("file", new HttpEntity<>(document, this.creerEntete(OPEN_XML)));
			body.add("editorRequest", new HttpEntity<>(session, this.creerEntete(JSON)));

			result = this.fetch(HttpMethod.POST, uri, body, String.class).map(Jeton::new).orElseThrow(JetonInvalideException::new);

			log.trace("Jeton récupéré pour affichage = '{}'", result);
		} catch (final HttpClientErrorException exception) {
			throw VisualisationDocumentException.with()
					.uri(uri)
					.document(document)
					.build();
		}

		return result;
	}

	public Location redirect(final Jeton jeton) {
		return this.affichageURL.format(jeton);
	}
}
