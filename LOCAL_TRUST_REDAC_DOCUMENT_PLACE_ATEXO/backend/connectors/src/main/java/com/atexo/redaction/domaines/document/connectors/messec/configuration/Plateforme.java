package com.atexo.redaction.domaines.document.connectors.messec.configuration;

import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

@Getter
@Setter
@NoArgsConstructor(force = true)
@Profile("messec")
public class Plateforme {

	private String id;
	private String nom;
	private Location lien;
	private Location visualisation;

}
