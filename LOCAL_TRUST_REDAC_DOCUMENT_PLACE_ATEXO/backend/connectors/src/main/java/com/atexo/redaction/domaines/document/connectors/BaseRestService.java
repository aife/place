package com.atexo.redaction.domaines.document.connectors;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.client.RestTemplate;

public abstract class BaseRestService {

	@Setter(onMethod = @__(@Autowired))
	@Getter(value = AccessLevel.MODULE, onMethod = @__(@Cacheable))
	private RestTemplate restTemplate;


}
