package com.atexo.redaction.domaines.document.connectors.messec;

import com.atexo.redaction.domaines.document.connectors.BaseRestDocumentService;
import com.atexo.redaction.domaines.document.connectors.messec.configuration.MessecConfiguration;
import com.atexo.redaction.domaines.document.connectors.messec.views.DestinataireMessec;
import com.atexo.redaction.domaines.document.connectors.messec.views.JetonMessec;
import com.atexo.redaction.domaines.document.connectors.messec.views.MessageMessec;
import com.atexo.redaction.domaines.document.connectors.messec.views.TagMessec;
import com.atexo.redaction.domaines.document.model.Mail;
import com.atexo.redaction.domaines.document.model.exceptions.api.mail.MessecTokenException;
import com.atexo.redaction.domaines.document.shared.services.TemplateService;
import com.atexo.redaction.domaines.document.shared.web.Location;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@Profile("messec")
public class MessecConnector extends BaseRestDocumentService {

	@Value("${url.messec.token}")
	private Location recuperationToken;

	@Value("${url.messec.message}")
	private Location envoiMessage;

	@Autowired
	private MessecConfiguration messecConfiguration;

	@Autowired
	private TemplateService templateService;

	public String authentifier(final Mail mail) {
		String result;
		try {
			var jeton = JetonMessec.with()
					.refObjetMetier(mail.getConsultation().getReference())
					.idObjetMetierPfEmetteur(null != mail.getConsultation().getId() ? mail.getConsultation().getId().toString() : "0")
					.idPfDestinataire(messecConfiguration.getDestinataire().getId())
					.idPfEmetteur(messecConfiguration.getEmetteur().getId())
					.typeMessageDefaut(messecConfiguration.getMessage().getDefaut())
					.nomPfEmetteur(messecConfiguration.getEmetteur().getNom())
					.urlPfEmetteur(messecConfiguration.getEmetteur().getLien().toString())
					.urlPfEmetteurVisualisation(messecConfiguration.getEmetteur().getVisualisation().toString())
					.nomPfDestinataire(messecConfiguration.getDestinataire().getNom())
					.urlPfDestinataire(messecConfiguration.getDestinataire().getLien().toString())
					.urlPfDestinataireVisualisation(messecConfiguration.getDestinataire().getVisualisation().toString())
					.urlPfReponse(messecConfiguration.getReponse().getLien().toString())
					.nomCompletExpediteur(messecConfiguration.getExpediteur().getNom())
					.emailExpediteur(messecConfiguration.getExpediteur().getEnvoi())
					.emailReponseExpediteur(messecConfiguration.getExpediteur().getReponse())
					.emailsAlerteReponse(messecConfiguration.getExpediteur().getAlerte())
					.build();

			log.debug("XML d'echange avec MESSEC pour obtention du token = {}", jeton.serialiser());

			var headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_XML);
			var entity = new HttpEntity<>(jeton, headers);

			result = http(HttpMethod.POST, recuperationToken, entity, String.class).getBody();
		} catch (HttpClientErrorException exception) {
			throw MessecTokenException
					.with()
					.uri(recuperationToken)
					.build();
		}

		return result;
	}

	public void envoyer(final Mail data) {
		var body = MessageMessec.with()
				.reference(data.getConsultation().getReference())
				.typeMessage(messecConfiguration.getMessage().getType())
				.nomExpediteur(messecConfiguration.getExpediteur().getNom())
				.emailExpediteur(messecConfiguration.getExpediteur().getReponse())
				.statut(messecConfiguration.getMessage().getStatut())
				.contenuHTML(messecConfiguration.getMessage().isHtml())
				.template(messecConfiguration.getMessage().getModele())
				.entete(templateService.merge(messecConfiguration.getMessage().getEntete(), data))
				.contenu(templateService.merge(messecConfiguration.getMessage().getContenu(), data))
				.objet(MessageFormat.format(messecConfiguration.getObjet(), data.getDocument().getNom()))
				.destinataires(
						data.getDestinataires().stream().map(
								destinataire -> DestinataireMessec.with()
										.nom(destinataire.getNomComplet())
										.mail(destinataire.getEmail())
										.build()
						).collect(Collectors.toSet())
				)
				.tags(data.getTags().stream().map(tag -> TagMessec.with()
						.code(tag.getCode())
						.cle(tag.getCle())
						.valeur(tag.getValeur())
						.build()).collect(Collectors.toSet())
				)
				.build();

		var token = this.authentifier(data);

		try {
			log.debug("JSON d'echange avec MESSEC pour obtention du token = {}", body.serialiser());

			var headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			var entity = new HttpEntity<>(body, headers);

			http(HttpMethod.POST, envoiMessage.format(token), entity, String.class);
		} catch (HttpClientErrorException exception) {
			throw MessecTokenException
					.with()
					.uri(recuperationToken)
					.build();
		}
	}

	@PostConstruct
	private void echo() {
		log.info("\uD83D\uDD17 URL de récupération du jeton de MESSEC = '{}'", recuperationToken);
		log.info("\uD83D\uDD17 URL d'envoi du message de MESSEC = '{}'", envoiMessage);

	}
}
