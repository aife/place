package com.atexo.redaction.domaines.document.connectors.messec.views;

import com.atexo.redaction.domaines.document.shared.patterns.Json;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Singular;
import org.springframework.context.annotation.Profile;

import java.util.Set;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Profile("messec")
public class MessageMessec extends Json {

	@JsonProperty("referenceObjetMetier")
	private String reference;

	@JsonProperty("typeMessage")
	private String typeMessage;

	@JsonProperty("cartouche")
	private String entete;

	@JsonProperty("nomCompletExpediteur")
	private String nomExpediteur;

	@JsonProperty("emailExpediteur")
	private String emailExpediteur;

	@JsonProperty("contenu")
	private String contenu;

	@JsonProperty("contenuHTML")
	private boolean contenuHTML;

	@JsonProperty("objet")
	private String objet;

	@JsonProperty("statut")
	private String statut;

	@Singular
	@JsonProperty("destsPfDestinataire")
	private Set<DestinataireMessec> destinataires;

	@Singular
	@JsonProperty("metaDonnees")
	private Set<TagMessec> tags;

	@JsonProperty("templateMail")
	private String template;

}
