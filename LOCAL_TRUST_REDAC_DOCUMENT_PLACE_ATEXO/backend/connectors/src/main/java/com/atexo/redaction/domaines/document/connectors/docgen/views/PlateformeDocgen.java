package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class PlateformeDocgen implements Serializable {

	@JsonProperty("id")
	private String id;
}
