package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class ServiceClausier {
	private ServiceClausier parent;

	private OrganismeClausier organisme;

	private Long identifiantExterne;

	private String codeExterne;

	private String libelle;
}
