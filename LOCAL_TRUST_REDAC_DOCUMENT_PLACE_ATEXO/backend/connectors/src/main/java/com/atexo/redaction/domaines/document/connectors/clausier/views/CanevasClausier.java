package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class CanevasClausier {
	private Integer id;

	private String titre;

	private String type;

	private String auteur;

	private Date dateModification;

	private Date dateCreation;

	private Set<ProcedureClausier> procedures;

	private String reference;

	private StatutCanevasClausier statut;

	private OrganismeClausier organisme;

	private NaturePrestationClausier naturePrestation;

	private Set<ContratClausier> contrats;

	private String version;
}
