package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@Setter
public class LotClausier {
	private Integer id;

	private String numero;

	private String libelle;

	private String description;

	private Set<CPVClausier> cpv;
}
