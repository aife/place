package com.atexo.redaction.domaines.document.connectors.messec.views;

import com.atexo.redaction.domaines.document.shared.patterns.XML;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;

@JacksonXmlRootElement(localName = "message_securise_init")
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Profile("messec")
public class JetonMessec extends XML {

	@JacksonXmlProperty(localName = "id_objet_metier_pf_emetteur", isAttribute = true)
	private String idObjetMetierPfEmetteur;

	@JacksonXmlProperty(localName = "id_pf_destinataire", isAttribute = true)
	private String idPfDestinataire;

	@JacksonXmlProperty(localName = "id_pf_emetteur", isAttribute = true)
	private String idPfEmetteur;

	@JacksonXmlProperty(localName = "type_message_defaut", isAttribute = true)
	private String typeMessageDefaut;

	@JacksonXmlProperty(localName = "ref_objet_metier")
	private String refObjetMetier;

	@JacksonXmlProperty(localName = "nom_pf_emetteur")
	private String nomPfEmetteur;

	@JacksonXmlProperty(localName = "url_pf_emetteur")
	private String urlPfEmetteur;

	@JacksonXmlProperty(localName = "url_pf_emetteur_visualisation")
	private String urlPfEmetteurVisualisation;

	@JacksonXmlProperty(localName = "nom_pf_destinataire")
	private String nomPfDestinataire;

	@JacksonXmlProperty(localName = "url_pf_destinataire")
	private String urlPfDestinataire;

	@JacksonXmlProperty(localName = "url_pf_destinataire_visualisation")
	private String urlPfDestinataireVisualisation;

	@JacksonXmlProperty(localName = "url_pf_reponse")
	private String urlPfReponse;

	@JacksonXmlProperty(localName = "nom_complet_expediteur")
	private String nomCompletExpediteur;

	@JacksonXmlProperty(localName = "email_expediteur")
	private String emailExpediteur;

	@JacksonXmlProperty(localName = "email_reponse_expediteur")
	private String emailReponseExpediteur;

	@JacksonXmlProperty(localName = "emails_alerte_reponse")
	private String emailsAlerteReponse;
}
