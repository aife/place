package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Etat du document
 */
@AllArgsConstructor
public enum RevisionActionDocgen {
	INCONNU("UNKNOWN"),
	DEMANDE_VALIDATION("SUBMIT"),
	REFUS_DEMANDE_VALIDATION("UNSUBMIT"),
	VALIDATION("VALIDATATION"),
	INVALIDATION("INVALIDATION"),
	ENREGISTREMENT("SAVE");

	@Getter(onMethod = @__(@JsonValue))
	private final String value;
}
