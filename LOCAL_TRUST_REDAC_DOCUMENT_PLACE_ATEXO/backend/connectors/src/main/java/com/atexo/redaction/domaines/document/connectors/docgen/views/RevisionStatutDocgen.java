package com.atexo.redaction.domaines.document.connectors.docgen.views;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Etat du document
 */
@AllArgsConstructor
public enum RevisionStatutDocgen {
	ERREUR(0), BROUILLON(1), DEMANDE_VALIDATION(2), VALIDE(3);

	@Getter(onMethod = @__(@JsonValue))
	private final int value;
}
