package com.atexo.redaction.domaines.document.connectors.clausier.views;

import lombok.*;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder(builderMethodName = "with")
public class ConsultationClausier {
	private String objet;

	private String intitule;

	private ProcedureClausier procedure;

	private OrganismeClausier organisme;

	private ServiceClausier service;

	private Date dateRemisePlis;

	private String reference;

	private String numero;

	private boolean mps;

	private Set<LotClausier> lots;

}
