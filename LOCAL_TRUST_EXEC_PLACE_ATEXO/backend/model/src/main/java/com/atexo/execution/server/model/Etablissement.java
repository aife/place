package com.atexo.execution.server.model;

import com.google.common.base.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ETABLISSEMENT", indexes = {@Index(name = "IX_ETABLISSEMENT_SIRET", columnList = "SIRET"), @Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL, region = "Etablissement")
public class Etablissement extends AbstractSynchronizedEntity {

	String siret;

	Boolean siege;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "adresse", column = @Column(name = "ADRESSE_ADRESSE")),
		@AttributeOverride(name = "adresse2", column = @Column(name = "ADRESSE_ADRESSE_2")),
		@AttributeOverride(name = "codePostal", column = @Column(name = "ADRESSE_CODE_POSTAL")),
		@AttributeOverride(name = "commune", column = @Column(name = "ADRESSE_COMMUNE")),
		@AttributeOverride(name = "pays", column = @Column(name = "ADRESSE_PAYS")),
		@AttributeOverride(name = "codeInseeLocalite", column = @Column(name = "ADRESSE_CODE_INSEE_LOCALITE"))
	})
	Adresse adresse;

	@ManyToOne
	@JoinColumn(name = "ID_FOURNISSEUR")
	Fournisseur fournisseur;

	@OneToMany(mappedBy = "etablissement")
	List<Contact> contacts;

	Boolean trusted;

	String naf;

	String libelleNaf;


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Etablissement that = (Etablissement) o;

		return Objects.equal(this.siret, that.siret) && Objects.equal(this.siege, that.siege);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(siret, siege);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Fournisseur fournisseur_ = getFournisseur();

		builder.append("Etablissement : ");
		builder.append(siret);

		if (fournisseur_ != null) {
			builder.append(" - ");
			builder.append(fournisseur_.getRaisonSociale());
		}

		if (adresse != null) {
			builder.append(" - ");
			builder.append(adresse.getCodePostal());
			builder.append(" ");
			builder.append(adresse.getCommune());
		}

		return builder.toString();
	}
}
