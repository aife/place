package com.atexo.execution.server.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = {"clef", "plateforme"}, callSuper = false)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE",columnDefinition = "VARCHAR(255) default 'APPLICATION'")
@Table(name = "PARAMETRAGE",uniqueConstraints = @UniqueConstraint(name = "UK_PARAMETRAGE_PLATEFORME" ,columnNames = {"CLEF", "ID_PLATEFORME"}))
public abstract class AbstractParametragelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String clef;

    @Lob
    @Column(name = "VALEUR")
    String valeur;

    String commentaire;

    @Column(name = "EXPORT_FRONT")
    Boolean exportFront;

    @ManyToOne
    @JoinColumn(name = "ID_PLATEFORME")
    Plateforme plateforme;

    @Transient
    public String getDiscriminatorValue() {
        DiscriminatorValue val = this.getClass().getAnnotation(DiscriminatorValue.class);
        return val == null ? null : val.value();
    }

}
