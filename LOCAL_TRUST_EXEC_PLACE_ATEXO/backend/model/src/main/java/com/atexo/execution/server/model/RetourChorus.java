package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.RETOUR_CHORUS)
public class RetourChorus extends AbstractReferentielEntity {
	public static final String COMMANDE_ID_EXTERNE = "COM";
	public static final String IRR = "IRR";
	public static final String REJ = "REJ";
}
