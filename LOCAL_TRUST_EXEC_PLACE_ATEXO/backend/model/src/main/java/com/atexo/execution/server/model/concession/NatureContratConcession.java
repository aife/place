package com.atexo.execution.server.model.concession;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import com.atexo.execution.server.model.AbstractReferentielEntity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.NATURE_CONTRAT_CONCESSION)
public class NatureContratConcession extends AbstractReferentielEntity {

}
