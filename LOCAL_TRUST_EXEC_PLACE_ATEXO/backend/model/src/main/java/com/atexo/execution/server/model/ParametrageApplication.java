package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static com.atexo.execution.common.def.ParametrageDiscriminators.APPLICATION;

@Getter
@Setter
@Entity
@DiscriminatorValue(APPLICATION)
public class ParametrageApplication extends AbstractParametragelEntity {

    public static final String CLE_SYNCHRONISATION_ECHANGES_CHORUS = "synchronisation.echanges.chorus.actif";
    public static final String CLE_SYNCHRONISATION_AVENANTS = "synchronisation.avenants";
    public static final String CLE_ACTES = "actes.actif";
}
