package com.atexo.execution.server.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "consultation", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class Consultation extends AbstractSynchronizedEntity {


    private String numero;

    @Lob
    private String intitule;

    @ManyToOne
    @JoinColumn(name = "id_categorie")
    private CategorieConsultation categorie;

    @Lob
    private String objet;


    @ManyToOne
    @JoinColumn(name = "id_procedure")
    private Procedure procedure;

    @Getter(AccessLevel.NONE)
    private boolean horsPassation;

    Integer origineUE;

    private Integer origineFrance;


    @ManyToOne
    @JoinColumn(name = "id_type_groupement")
    private TypeGroupement typeGroupementOperateurs;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONSULTATION_MODALITE_EXECUTION", joinColumns = {
            @JoinColumn(name = "ID_CONSULTATION")},
            inverseJoinColumns = {@JoinColumn(name = "ID_MODALITE_EXECUTION")})
    private Set<ModaliteExecution> modaliteExecutions;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONSULTATION_TECHNIQUE_ACHAT", joinColumns = {
            @JoinColumn(name = "ID_CONSULTATION")},
            inverseJoinColumns = {@JoinColumn(name = "ID_TECHNIQUE_ACHAT")})
    private Set<TechniqueAchat> techniqueAchats;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONSULTATION_TYPE_PRIX", joinColumns = {
            @JoinColumn(name = "ID_CONSULTATION")},
            inverseJoinColumns = {@JoinColumn(name = "ID_TYPE_PRIX")})
    private Set<TypePrix> typesPrix;

    private String numeroProjetAchat;

    private LocalDate decisionAttribution;


    public boolean getHorsPassation() {
        return horsPassation;
    }

    public void setHorsPassation(Boolean horsPassation) {
        this.horsPassation = Boolean.TRUE.equals(horsPassation);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("numero", numero).add("intitule", intitule).add("objet", objet).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Consultation that = (Consultation) o;

        return Objects.equal(this.numero, that.numero) && Objects.equal(this.intitule, that.intitule) && Objects.equal(this.objet, that.objet);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numero, intitule, objet);
    }


}
