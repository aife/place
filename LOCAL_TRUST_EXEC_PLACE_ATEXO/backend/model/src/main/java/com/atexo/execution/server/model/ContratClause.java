package com.atexo.execution.server.model;

import com.atexo.execution.server.model.clause.Clause;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "contrat_clause")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContratClause implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String valeur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_contrat")
    Contrat contrat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_clause")
    Clause clause;

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private ContratClause parent;
}
