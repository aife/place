package com.atexo.execution.server.model;

import com.atexo.execution.common.def.StatutSupervisionEchangeChorus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SupervisionSynchroEchangeChorus extends AbstractSynchronizedEntity {

    @Enumerated(EnumType.STRING)
    private StatutSupervisionEchangeChorus statut;
    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;
    private String messageErreur;
}
