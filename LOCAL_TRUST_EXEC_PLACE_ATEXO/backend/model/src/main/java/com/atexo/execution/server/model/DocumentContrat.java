package com.atexo.execution.server.model;

import com.atexo.execution.common.def.DocumentContratType;
import com.atexo.execution.common.def.DocumentDiscriminators;
import com.atexo.execution.common.dto.edition_en_ligne.FileStatusEnum;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue(DocumentDiscriminators.CONTRAT)
public class DocumentContrat extends Document {

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT")
    Contrat contrat;

    String objet;

    String commentaire;

    @ManyToOne
    @JoinColumn(name = "id_etablissement")
    Etablissement etablissement;

    @Enumerated(EnumType.STRING)
    DocumentContratType documentType;

    @OneToMany(mappedBy = "document", cascade = CascadeType.REMOVE, orphanRemoval = true)
    List<DocumentContratMiseADisposition> documentMiseADispositionList;

    @OneToMany(mappedBy = "document", cascade = CascadeType.PERSIST, orphanRemoval = true)
    List<DocumentContratUsersEnEdition> usersEnEdition;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "DOCUMENT_HAS_EVENEMENT", joinColumns = {
            @JoinColumn(name = "ID_DOCUMENT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_EVENEMENT")})
    Set<Evenement> evenements = new HashSet<>(0);

    @Column(name = "statut")
    @Enumerated(EnumType.STRING)
    private FileStatusEnum statut;

    @Column(name = "date_modification_edition")
    private LocalDateTime dateModificationEdition;

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    public DocumentContratType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentContratType documentType) {
        this.documentType = documentType;
    }

    public List<DocumentContratMiseADisposition> getDocumentMiseADispositionList() {
        return documentMiseADispositionList;
    }

    public void setDocumentMiseADispositionList(List<DocumentContratMiseADisposition> documentMiseADispositionList) {
        this.documentMiseADispositionList = documentMiseADispositionList;
    }

    public Set<Evenement> getEvenements() {
        return evenements;
    }

    public void setEvenements(Set<Evenement> evenements) {
        this.evenements = evenements;
    }

    public FileStatusEnum getStatut() {
        return statut;
    }

    public void setStatut(FileStatusEnum statut) {
        this.statut = statut;
    }

    public LocalDateTime getDateModificationEdition() {
        return dateModificationEdition;
    }

    public void setDateModificationEdition(LocalDateTime dateModificationEdition) {
        this.dateModificationEdition = dateModificationEdition;
    }

    public List<DocumentContratUsersEnEdition> getUsersEnEdition() {
        if (usersEnEdition == null) {
            usersEnEdition = new ArrayList<>();
        }
        return usersEnEdition;
    }

    public void setUsersEnEdition(List<DocumentContratUsersEnEdition> usersEnEdition) {
        this.usersEnEdition = usersEnEdition;
    }
}
