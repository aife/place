package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.TYPE_CONTRAT)
@Getter
@Setter
public class TypeContrat extends AbstractReferentielEntity {

    @Column(name = "mode_echange_chorus")
    private Integer modeEchangeChorus;
}
