package com.atexo.execution.server.model;


import com.atexo.execution.common.def.EchangeDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@DiscriminatorValue(EchangeDiscriminators.CHORUS)
@Table(name = "ECHANGE_CHORUS", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class EchangeChorus extends AbstractSynchronizedEntity {

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ID_CONTRAT")
    Contrat contrat;

    @ManyToOne
    @JoinColumn(name = "ID_STATUT")
    StatutChorus statut;

	@ManyToOne
	@JoinColumn(name = "ID_ACTE")
	Acte acte;

    String erreurPublication;

    LocalDateTime datePublication;

    LocalDateTime dateEnvoi;

    LocalDateTime dateEnvoiDocuments;

    private String reference;

    private String organisationAchat;

    private String groupementAchat;

    private LocalDateTime dateNotificationPrevisionnelle;

    @ManyToOne
    @JoinColumn(name = "ID_RETOUR_CHORUS")
    RetourChorus retourChorus;
}
