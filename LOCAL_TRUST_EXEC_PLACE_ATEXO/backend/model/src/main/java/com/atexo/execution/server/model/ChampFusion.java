package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@DiscriminatorValue(ReferentielDiscriminators.CHAMP_FUSION)
public class ChampFusion extends AbstractReferentielEntity {
        private String format;
}
