package com.atexo.execution.server.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = {"acronyme"}, callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ORGANISME", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class Organisme extends AbstractSynchronizedEntity {

    String nom;

    String acronyme;

    String sigle;

    String dataSet;

    String siren;

    String complement;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SERVICE_RACINE")
    Service serviceRacine;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ORGANISME_DOCUMENT_MODELE", joinColumns = @JoinColumn(name = "ID_ORGANISME"),
            inverseJoinColumns = @JoinColumn(name = "ID_DOCUMENT_MODELE"))
    private Set<DocumentModele> documentModeles;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ORGANISME_TYPE_CONTRAT_ENTITE_ELIGIBLE_ACTIF", joinColumns = {@JoinColumn(name = "ID_ORGANISME")}, inverseJoinColumns = {@JoinColumn(name = "ID_TYPE_CONTRAT")})
    private Set<TypeContrat> typeContrats = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organisme")
    private List<Service> services;

    private Integer idMpe;


    public String getSiret() {
        return this.getSiren() + this.getComplement();
    }

}
