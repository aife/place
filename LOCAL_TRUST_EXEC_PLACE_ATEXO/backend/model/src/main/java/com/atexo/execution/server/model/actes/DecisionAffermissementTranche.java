package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalDate;

@Entity
@Table(name = "acte_decision_affermissement_tranche")
@Getter
@Setter
public class DecisionAffermissementTranche extends Acte {

	public static final String CODE = "ACT_DAT";

	@Column(name = "NUMERO_TRANCHE")
	private String numeroTranche;

	@Column(name = "DEBUT")
	private LocalDate debutExecutionTranche;

	@Column(name = "FIN")
	private LocalDate finExecutionTranche;

	@Column(name = "DUREE")
	private Long dureeExecutionTranche;

	@Column(name = "MODALITE_HT")
	private Double montantHT;

	@Column(name = "MODALITE_TTC")
	private Double montantTTC;

	@Column(name = "TAUX_TVA")
	private Double tauxTVA;

    @Override
    public String getCode() {
        return CODE;
    }
}
