package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "acte_autre")
@Getter
@Setter
public class ActeAutre extends Acte {

	public static final String CODE = "ACT_AUT";

    @Column(name = "SIGNATURE_POUVOIR_ADJUDICATEUR")
    private LocalDate dateSignaturePouvoirAdjudicateur;

    public LocalDate getDateSignaturePouvoirAdjudicateur() {
        return dateSignaturePouvoirAdjudicateur;
    }

    public void setDateSignaturePouvoirAdjudicateur(LocalDate dateSignaturePouvoirAdjudicateur) {
        this.dateSignaturePouvoirAdjudicateur = dateSignaturePouvoirAdjudicateur;
    }

    @Override
    public String getCode() {
        return CODE;
    }
}
