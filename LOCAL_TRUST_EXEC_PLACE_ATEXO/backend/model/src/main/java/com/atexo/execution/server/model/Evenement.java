package com.atexo.execution.server.model;

import com.atexo.execution.common.def.EtatEvenement;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Evenement du calendrier d'exécution
 * Created by sta on 09/09/16.
 */
@Entity
@Table(name = "evenement")
public class Evenement extends AbstractEntity {

    LocalDate dateDebut;

    LocalDate dateFin;

    private LocalDate dateDebutReel;

    private LocalDate dateFinReel;

    private LocalDate dateRejet;

    private String commentaireEtat;

    private Boolean suiviRealisation;

    @Enumerated(EnumType.STRING)
    private EtatEvenement etatEvenement;

    /**
     * Si l'évenement est ponctuel, on ne stocke pas la date de fin
     */
    Boolean ponctuel;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT")
    Contrat contrat;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRACTANT")
    Etablissement contractant;

    @ManyToOne
    Etablissement nouveauContractant;

    String libelle;

    String commentaire;

    Boolean alerte;

    LocalDate datePreavis;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "EVENEMENT_DESTINATAIRES", joinColumns = @JoinColumn(name = "ID_EVENEMENT"),
            inverseJoinColumns = @JoinColumn(name = "ID_DESTINATAIRES"))
    Set<Utilisateur> destinataires;

    Boolean envoiAlerte;

    String destinatairesLibre;

    @ManyToOne
    @JoinColumn(name = "ID_TYPE_EVENEMENT")
    TypeEvenement typeEvenement;

    @ManyToMany(mappedBy = "evenements", fetch = FetchType.LAZY)
    List<DocumentContrat> documents= new ArrayList<>(0);

    @Embedded
    private AvenantType avenantType;

    @Embedded
    private BonCommandeType bonCommandeType;

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDate getDateDebutReel() {
        return dateDebutReel;
    }

    public void setDateDebutReel(LocalDate dateDebutReel) {
        this.dateDebutReel = dateDebutReel;
    }

    public LocalDate getDateFinReel() {
        return dateFinReel;
    }

    public void setDateFinReel(LocalDate dateFinReel) {
        this.dateFinReel = dateFinReel;
    }

    public LocalDate getDateRejet() {
        return dateRejet;
    }

    public void setDateRejet(LocalDate dateRejet) {
        this.dateRejet = dateRejet;
    }

    public String getCommentaireEtat() {
        return commentaireEtat;
    }

    public void setCommentaireEtat(String commentaireEtat) {
        this.commentaireEtat = commentaireEtat;
    }

    public Boolean getSuiviRealisation() {
        return suiviRealisation != null && suiviRealisation;
    }

    public void setSuiviRealisation(Boolean suiviRealisation) {
        this.suiviRealisation = suiviRealisation;
    }

    public EtatEvenement getEtatEvenement() {
        return etatEvenement;
    }

    public void setEtatEvenement(EtatEvenement etatEvenement) {
        this.etatEvenement = etatEvenement;
    }

    public Boolean getPonctuel() {
        return ponctuel;
    }

    public void setPonctuel(Boolean ponctuel) {
        this.ponctuel = ponctuel;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Boolean getAlerte() {
        return alerte;
    }

    public void setAlerte(Boolean alerte) {
        this.alerte = alerte;
    }

    public Etablissement getContractant() {
        return contractant;
    }

    public void setContractant(Etablissement contractant) {
        this.contractant = contractant;
    }

    public Etablissement getNouveauContractant() {
        return nouveauContractant;
    }

    public void setNouveauContractant(Etablissement nouveauContractant) {
        this.nouveauContractant = nouveauContractant;
    }

    public LocalDate getDatePreavis() {
        return datePreavis;
    }

    public void setDatePreavis(LocalDate datePreavis) {
        this.datePreavis = datePreavis;
    }

    @Transient
    public Integer getDelaiPreavis() {
        if(dateDebut == null || datePreavis == null) return null;
        return (int) ChronoUnit.DAYS.between(datePreavis, dateDebut);
    }

    @Transient
    public void setDelaiPreavis(Integer delaiPreavis) {
        if(dateDebut == null) return;
        datePreavis = delaiPreavis == null ? null : dateDebut.minusDays(delaiPreavis);
    }

    public Set<Utilisateur> getDestinataires() {
        return destinataires;
    }

    public void setDestinataires(Set<Utilisateur> destinataires) {
        this.destinataires = destinataires;
    }

    public List<String> getDestinatairesLibre() {
        if (destinatairesLibre != null)
            return Arrays.asList(destinatairesLibre.split(";"));
        else
            return null;
    }

    public void setDestinatairesLibre(String destinatairesLibre) {
        this.destinatairesLibre = destinatairesLibre;
    }

    public void addDestinataireLibre(String email) {
        if (this.destinatairesLibre == null)
            this.destinatairesLibre = "";
        this.destinatairesLibre = this.destinatairesLibre + email + ";";
    }

    public TypeEvenement getTypeEvenement() {
        return typeEvenement;
    }

    public void setTypeEvenement(TypeEvenement typeEvenement) {
        this.typeEvenement = typeEvenement;
    }

      public List<DocumentContrat> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentContrat> documents) {
        this.documents = documents;
    }

    public int getDocumentsCount() {
        return documents == null ? 0 : documents.size();
    }

    public Boolean getEnvoiAlerte() {
        return envoiAlerte;
    }

    public void setEnvoiAlerte(Boolean envoiAlerte) {
        this.envoiAlerte = envoiAlerte;
    }

    public AvenantType getAvenantType() {
        return avenantType;
    }

    public void setAvenantType(AvenantType avenantType) {
        this.avenantType = avenantType;
    }

    public BonCommandeType getBonCommandeType() {
        return bonCommandeType;
    }

    public void setBonCommandeType(BonCommandeType bonCommandeType) {
        this.bonCommandeType = bonCommandeType;
    }
}
