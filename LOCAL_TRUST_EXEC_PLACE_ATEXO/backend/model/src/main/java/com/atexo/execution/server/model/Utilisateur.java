package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "UTILISATEUR", indexes = {@Index(name = "IX_UTILISATEUR_IDENTIFIANT", columnList = "IDENTIFIANT"), @Index(name = "IX_UTILISATEUR_EMAIL", columnList = "EMAIL"),
        @Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class Utilisateur extends AbstractSynchronizedEntity {

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SERVICE")
    Service service;

    String identifiant;

    String nom;

    String prenom;

    String email;

    String telephone;

    Boolean actif;

	String uuid = UUID.randomUUID().toString();

    @Column(name = "technique", nullable = false, columnDefinition = "boolean default false")
    boolean technique = false;

    @Column(name = "ref_service", nullable = false, columnDefinition = "int default 0")
    int refService = 0;

    @Column(name = "ref_organisme")
    String refOrganisme;

    @Transient
    private Set<Habilitation> habilitations = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "utilisateur_organismes_associes", joinColumns = {
            @JoinColumn(name = "ID_UTILISATEUR")},
            inverseJoinColumns = {@JoinColumn(name = "ID_ORGANISME")})
    private Set<Organisme> organismesAssocies = new HashSet<>();



}
