package com.atexo.execution.server.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = {"dateDerniereModification", "typeModification"}, callSuper = false)
@ToString
@Table(name = "SYNCHRONISATION_EXTERNE_DATE")
public class SynchronisationExterneDate extends AbstractEntity {

    LocalDateTime dateDerniereModification;
    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_MODIFICATION")
    TypeModification typeModification;

    @ManyToOne
    @JoinColumn(name = "ID_PLATEFORME")
    Plateforme plateforme;
    public enum TypeModification {
        ETABLISSEMENT, FOURNISSEUR, CONTRAT_ETABLISSEMENT, CONTACT, UTILISATEUR, SERVICE, REFERENTIEL_TYPE_CONTRAT, ORGANISME, ECHANGES_CHORUS
    }

}
