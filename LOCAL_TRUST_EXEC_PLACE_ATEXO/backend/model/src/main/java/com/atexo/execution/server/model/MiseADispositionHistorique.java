package com.atexo.execution.server.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "MISE_A_DISPO_HISTORIQUE")
public class MiseADispositionHistorique extends AbstractEntity {

    @OneToOne
    @PrimaryKeyJoinColumn(name = "ID_MISEADISPOSITION")
    private MiseADisposition miseadisposition;

    @Column(name = "DATE_DEMANDE")
    private LocalDateTime dateDemande;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_DEMANDE")
    private TypeDemandeMAD typeDemande;

    private String requette;

    private String response;

    private Boolean statut = false;

    @Column(name = "DATE_FIN")
    private  LocalDateTime dateFin;


    public MiseADisposition getMiseadisposition() {
        return miseadisposition;
    }

    public void setMiseadisposition(MiseADisposition miseadisposition) {
        this.miseadisposition = miseadisposition;
    }

    public LocalDateTime getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    public TypeDemandeMAD getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(TypeDemandeMAD typeDemande) {
        this.typeDemande = typeDemande;
    }

    public String getRequette() {
        return requette;
    }

    public void setRequette(String requette) {
        this.requette = requette;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public LocalDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDateTime dateFin) {
        this.dateFin = dateFin;
    }

    @Override
    public String toString() {
        return "MiseADispositionHistorique{" +
                "miseadisposition=" + miseadisposition +
                ", dateDemande=" + dateDemande +
                ", typeDemande=" + typeDemande +
                ", requette='" + requette + '\'' +
                ", response='" + response + '\'' +
                ", statut=" + statut +
                ", dateFin=" + dateFin +
                '}';
    }
}
