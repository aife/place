package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.CATEGORIE_FOURNISSEUR)
public class CategorieFournisseur extends AbstractReferentielEntity {

}
