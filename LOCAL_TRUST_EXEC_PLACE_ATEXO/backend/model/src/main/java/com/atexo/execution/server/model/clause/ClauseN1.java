package com.atexo.execution.server.model.clause;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
@DiscriminatorValue(ReferentielDiscriminators.CLAUSE_N1)
public class ClauseN1 extends Clause {

}
