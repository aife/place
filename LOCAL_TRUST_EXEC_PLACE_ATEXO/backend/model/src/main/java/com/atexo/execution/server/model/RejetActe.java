package com.atexo.execution.server.model;

import com.atexo.execution.common.def.MotifRejetActe;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class RejetActe {

	@Enumerated(EnumType.STRING)
	@Column(name = "MOTIF")
	private MotifRejetActe motif;

	@Column(name = "COMMENTAIRE")
	private String commentaire;

	public MotifRejetActe getMotif() {
		return motif;
	}

	public void setMotif( MotifRejetActe motif ) {
		this.motif = motif;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire( String commentaire ) {
		this.commentaire = commentaire;
	}


}
