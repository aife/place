package com.atexo.execution.server.model;


import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.server.model.data.gouv.ContratPublication;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "DONNEES_ESSENTIELLES", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@Getter
@Setter
public class DonneesEssentielles extends AbstractSynchronizedEntity {

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT")
    Contrat contrat;

    @OneToMany(mappedBy = "donneesEssentielles")
    private List<ContratPublication> contratPublications;

    @Enumerated(EnumType.STRING)
    StatutPublicationDE statut;

    String erreurPublication;

    LocalDateTime datePublication;

    String nomFichier;

    @Enumerated(EnumType.STRING)
    ModeEnvoiDonneesEssentielles modeEnvoiDonneesEssentielles;

    @Lob
    private String jsonFile;
}
