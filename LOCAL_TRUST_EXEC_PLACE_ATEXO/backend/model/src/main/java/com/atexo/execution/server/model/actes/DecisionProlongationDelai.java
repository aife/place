package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalDate;

@Entity
@Table(name = "acte_decision_prolongation_delai")
@Getter
@Setter
public class DecisionProlongationDelai extends Acte {

	public static final String CODE = "ACT_DPD";

	@Column(name = "DEBUT")
	private LocalDate debut;

	@Column(name = "FIN")
	private LocalDate fin;

	@Column(name = "DUREE")
	private Long duree;

    @Override
    public String getCode() {
        return CODE;
    }
}
