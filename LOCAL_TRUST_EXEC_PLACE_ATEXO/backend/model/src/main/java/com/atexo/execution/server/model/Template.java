package com.atexo.execution.server.model;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class Template extends AbstractEntity {

    public static final String NOTIFICATION_ACTE = "NOTIFICATION_ACTE";
    public static final String NOTIFICATION_CONTRAT = "NOTIFICATION_CONTRAT";

    private String code;

    private String objet;

    @Lob
    private String corps;

    private int ordreAffichage;

    private String envoiModalite;

    private boolean envoiModaliteFigee;

    private boolean reponseAttendue;

    private boolean reponseAttendueFigee;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCorps() {
        return corps;
    }

    public void setCorps(String corps) {
        this.corps = corps;
    }

    public int getOrdreAffichage() {
        return ordreAffichage;
    }

    public void setOrdreAffichage(int ordreAffichage) {
        this.ordreAffichage = ordreAffichage;
    }

    public String getEnvoiModalite() {
        return envoiModalite;
    }

    public void setEnvoiModalite(String envoiModalite) {
        this.envoiModalite = envoiModalite;
    }

    public boolean isEnvoiModaliteFigee() {
        return envoiModaliteFigee;
    }

    public void setEnvoiModaliteFigee(boolean envoiModaliteFigee) {
        this.envoiModaliteFigee = envoiModaliteFigee;
    }

    public boolean isReponseAttendue() {
        return reponseAttendue;
    }

    public void setReponseAttendue(boolean reponseAttendue) {
        this.reponseAttendue = reponseAttendue;
    }

    public boolean isReponseAttendueFigee() {
        return reponseAttendueFigee;
    }

    public void setReponseAttendueFigee(boolean reponseAttendueFigee) {
        this.reponseAttendueFigee = reponseAttendueFigee;
    }
}
