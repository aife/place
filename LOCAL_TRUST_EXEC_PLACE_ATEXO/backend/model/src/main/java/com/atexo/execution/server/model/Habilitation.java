package com.atexo.execution.server.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "HABILITATION")
public class Habilitation extends AbstractSynchronizedEntity implements GrantedAuthority {

    private String code;

    private String libelle;

    @Column(name = "PAR_DEFAUT",columnDefinition = "bit default false")
    private boolean parDefaut;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isParDefaut() {
        return parDefaut;
    }

    public void setParDefaut(boolean parDefaut) {
        this.parDefaut = parDefaut;
    }

    @Override
    @Transient
    public String getAuthority() {
        return getCode();
    }
}
