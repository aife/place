package com.atexo.execution.server.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DOCUMENT_MISE_A_DISPOSITION")
public class DocumentContratMiseADisposition implements Serializable {

    private MiseADisposition miseADisposition;

    private DocumentContrat document;

    @Id
    @ManyToOne
    @JoinColumn(name = "ID_MISE_DISPO")
    public MiseADisposition getMiseADisposition() {
        return miseADisposition;
    }

    public void setMiseADisposition(MiseADisposition miseADisposition) {
        this.miseADisposition = miseADisposition;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "ID_DOCUMENT")
    public DocumentContrat getDocument() {
        return document;
    }

    public void setDocument( DocumentContrat document) {
        this.document = document;
    }
}
