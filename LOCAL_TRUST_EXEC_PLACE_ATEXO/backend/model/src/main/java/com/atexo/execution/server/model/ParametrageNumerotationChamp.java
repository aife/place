package com.atexo.execution.server.model;

import com.atexo.execution.common.dto.DureeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parametrage_numerotation_champ", uniqueConstraints = {@UniqueConstraint(name = "parametrage_numerotation_champ_uc", columnNames = {"clef"})})
public class ParametrageNumerotationChamp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "clef")
    private String clef;

    @Column(name = "description")
    private String description;

    @Column(name = "format")
    private String format;

    @Column(name = "methode_valeur")
    private String methodeValeur;

    @Column(name = "methode_conditionnement")
    private String methodeConditionnement;

    @Column(name = "valeur_conditionnement")
    private String valeurConditionnement;

    @Column(name = "longueur")
    private Integer longueur;

    @Column(name = "compteur", columnDefinition = "boolean default false")
    private boolean compteur = false;

    @Column(name = "duree_reset_compteur")
    @Enumerated(EnumType.STRING)
    private DureeEnum dureeResetCompteur;

}
