package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import static com.atexo.execution.common.def.ParametrageDiscriminators.MESSAGERIE_SECURISEE;

@Getter
@Setter
@Entity
@DiscriminatorValue(MESSAGERIE_SECURISEE)
public class ParametrageMessagerieSecurisee extends AbstractParametragelEntity {

    public static final String CLE_NOM_PF_EMETTEUR = "messagerie.NomPfEmetteur";
    public static final String CLE_SIGNATURE = "messageerie.SignatureAvisPassage";
    public static final String CLE_URL_PF_DESTINATAIRE = "messagerie.UrlPfDestinataire";
    public static final String CLE_NOM_PF_DESTINATAIRE = "messagerie.NomPfDestinataire";
    public static final String CLE_URL_PF_DESTINATAIRE_VISUALISATION = "messagerie.UrlPfDestinataireVisualisation";
    public static final String CLE_EMAIL_EXPEDITEUR = "messagerie.mail.expediteur";
    public static final String CLE_LOGO_SRC = "messagerie.logo.src";
    public static final String CLE_TAILLE_MAX_PJ = "messagerie.securisee.max.taille.fichiers";
}
