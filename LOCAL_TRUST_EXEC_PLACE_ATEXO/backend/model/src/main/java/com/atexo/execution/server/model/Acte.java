package com.atexo.execution.server.model;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.common.def.TypeNotification;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
public abstract class Acte extends AbstractSynchronizedEntity {

    @Lob
    @Column(name = "OBJET")
    private String objet;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT")
    private StatutActe statut;

    @Column(name = "NUMERO")
    private String numero;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TYPE_ACTE")
    private TypeActe typeActe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ACHETEUR")
    private Utilisateur acheteur;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ID_TITULAIRE_CONTACT")
    private Contact titulaire;

    @OneToMany(orphanRemoval = true, mappedBy = "acte", cascade = CascadeType.ALL)
    private List<DocumentActe> documents = new ArrayList<>();

    @Column(name = "COMMENTAIRE")
    private String commentaire;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CONTRAT")
    private Contrat contrat;

    @Embedded
    private Chorus chorus;

    @Column(name = "DATE_NOTIFICATION")
    private LocalDateTime dateNotification;

    @ManyToOne
    @JoinColumn(name = "ID_DONNEES_ESSENTIELLES")
    private DonneesEssentielles donneesEssentielles;

    @ManyToOne
    @JoinColumn(name = "ID_ECHANGE_CHORUS")
    private EchangeChorus echangeChorus;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_NOTIFICATION")
    private TypeNotification typeNotification;

    @Column(name = "VALIDATION_CHORUS")
    @ColumnDefault(value = "0")
    private boolean validationChorus;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT_PUBLICATION")
    private StatutAvenant statutPublication;

    @Column(name = "DATE_PUBLICATION")
    private LocalDateTime datePublication;

    @Column(name = "NUMERO_ORDRE")
    private Integer numeroOrdre;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "motif", column = @Column(name = "REJET_MOTIF")),
            @AttributeOverride(name = "commentaire", column = @Column(name = "REJET_COMMENTAIRE"))
    })
    private RejetActe rejet;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Acte acte = (Acte) o;
        return Objects.equals(objet, acte.objet) && Objects.equals(numero, acte.numero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), objet, numero);
    }

    public abstract String getCode();

    public void ajouter(DocumentActe document) {
        document.setActe(this);

        this.documents.add(document);
    }

    public void ajouter(List<DocumentActe> documents) {
        documents.forEach(this::ajouter);
    }
}
