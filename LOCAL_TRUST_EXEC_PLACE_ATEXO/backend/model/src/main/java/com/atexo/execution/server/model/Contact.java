package com.atexo.execution.server.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CONTACT", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@EqualsAndHashCode(of = {"dateModificationExterne", "idExterne", "plateforme", "email"})
public class Contact extends AbstractSynchronizedEntity {

    @Column(name = "NOM")
    String nom;

    @Column(name = "PRENOM")
    String prenom;

    @Column(name = "EMAIL")
    String email;

    @Column(name = "FONCTION")
    String fonction;

    @Column(name = "ACTIF")
    Boolean actif;

    @Column(name = "ADRESSE_POSTALE")
    String adresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ETABLISSEMENT")
    Etablissement etablissement;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.REMOVE, orphanRemoval = true)
    Set<ContactReferant> contactReferants = new HashSet<>();

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "SYNCHRONISABLE")
    Boolean synchronisable;

    @Column(name = "login")
    private String login;

    @Column(name = "inscrit_annuaire_defense")
    @ColumnDefault(value = "0")
    private Boolean inscritAnnuaireDefense;

    @Column(name = "date_modification_rgpd")
    private LocalDateTime dateModificationRgpd;

    @Column(name = "rgpd_communication_place")
    @ColumnDefault(value = "0")
    private Boolean rgpdCommunicationPlace;

    @Column(name = "rgpd_enquete")
    @ColumnDefault(value = "0")
    private Boolean rgpdEnquete;

    @Column(name = "rgpd_communication")
    @ColumnDefault(value = "0")
    private Boolean rgpdCommunication;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("nom", nom).add("prenom", prenom).add("email", email).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Contact that = (Contact) o;

        return Objects.equal(this.email, that.email) && Objects.equal(this.etablissement, that.etablissement);
    }
}
