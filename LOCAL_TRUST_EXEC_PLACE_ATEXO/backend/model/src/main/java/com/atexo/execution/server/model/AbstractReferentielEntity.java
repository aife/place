package com.atexo.execution.server.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(of = {"code", "label"}, callSuper = false)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_REFERENTIEL")
@Table(name = "REFERENTIEL", uniqueConstraints = @UniqueConstraint(name = "UK_ID_EXTERNE", columnNames = {"idExterne"}))
@ToString(exclude = {"groupe", "parents"})
public abstract class AbstractReferentielEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String idExterne;

    String code;

    String codeExterne;

    @ManyToOne
    @JoinColumn(name = "ID_GROUPE")
    AbstractReferentielEntity groupe;

    String label;

    Long ordre;

    Long version;

    @Column(name = "ACTIF", nullable = false, columnDefinition = "boolean default true")
    boolean actif;

    @CreatedDate
    @Column(name = "DATE_CREATION")
    private LocalDateTime dateCreation;

    @LastModifiedDate
    @Column(name = "DATE_MODIFICATION")
    private LocalDateTime dateModification;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "referentiel_parent",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_parent")
    )
    Set<AbstractReferentielEntity> parents = new HashSet<>();

    @Lob
    String toolTip;

    Long limitation;

    @Column(name = "symbole")
    private String symbole;


    public AbstractReferentielEntity(String value, String label, String idExterne) {
        super();
        this.setCode(value);
        this.setLabel(label);
        this.setIdExterne(idExterne);
    }

    public AbstractReferentielEntity() {
        super();
    }



    @Transient
    public String getDiscriminatorValue() {
        DiscriminatorValue val = this.getClass().getAnnotation(DiscriminatorValue.class);
        return val == null ? null : val.value();
    }


}
