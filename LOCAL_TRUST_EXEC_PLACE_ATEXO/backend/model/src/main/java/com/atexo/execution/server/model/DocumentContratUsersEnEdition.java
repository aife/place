package com.atexo.execution.server.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "document_contrat_users_en_edition")
public class DocumentContratUsersEnEdition implements Serializable {

    private Utilisateur utilisateur;

    private DocumentContrat document;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_utilisateur")
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "id_document")
    public DocumentContrat getDocument() {
        return document;
    }

    public void setDocument(DocumentContrat document) {
        this.document = document;
    }

   private String token;


    @Column(name = "token")
    @Lob
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
