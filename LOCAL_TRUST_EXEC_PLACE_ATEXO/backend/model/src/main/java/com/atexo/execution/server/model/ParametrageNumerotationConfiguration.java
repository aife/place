package com.atexo.execution.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parametrage_numerotation_configuration", uniqueConstraints = {@UniqueConstraint(name = "parametrage_numerotation_configuration_uc", columnNames = {"id_parametrage_numerotation_champ", "ordre", "separateur"})})
public class ParametrageNumerotationConfiguration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_parametrage_numerotation_champ")
    private ParametrageNumerotationChamp parametrageNumerotationChamp;

    @Column(name = "actif", nullable = false, columnDefinition = "boolean default true")
    private boolean actif;

    @Column(name = "ordre", nullable = false, columnDefinition = "int default 0")
    private int ordre;

    @Column(name = "separateur")
    private String separateur;

    @Column(name = "prefix")
    private String prefix;

}
