package com.atexo.execution.server.model;

import com.atexo.execution.common.def.*;
import com.atexo.execution.server.model.concession.NatureContratConcession;
import com.atexo.execution.server.model.concession.TypeConcession;
import com.google.common.base.Objects;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Entity
@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "uuid", "numero", "referenceLibre", "numeroLong"})
@Table(name = "CONTRAT", indexes = {@Index(name = "IX_CONTRAT_NUMERO", columnList = "numero"), @Index(name = "IX_CONTRAT_NUMERO_LONG", columnList = "numeroLong"), @Index(name = "IX_CONTRAT_REFERENCE_LIBRE", columnList = "referenceLibre")
        , @Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class Contrat extends AbstractSynchronizedEntity {

    String numero;
    String numEj;
    String numeroLong;
    String referenceLibre;

    Integer offresRecues;

    @Lob
    String objet;

    @ManyToOne
    @JoinColumn(name = "ID_TYPE")
    @Audited(targetAuditMode = NOT_AUDITED)
    TypeContrat type;

    @ManyToOne
    @JoinColumn(name = "ID_CONSULTATION")
    @Audited(targetAuditMode = NOT_AUDITED)
    Consultation consultation;

    @ManyToOne
    @JoinColumn(name = "ID_SERVICE")
    @Audited(targetAuditMode = NOT_AUDITED)
    Service service;

    @ManyToOne
    @JoinColumn(name = "ID_CREATEUR")
    @Audited(targetAuditMode = NOT_AUDITED)
    Utilisateur createur; // le créateur peut changer de service

    // calculé automatiquement
    @Enumerated(EnumType.STRING)
    StatutContrat statut = StatutContrat.ANotifier;

    /**
     * attributaires principal  + cotraitant si groupement + sous traitants
     */
    @OneToMany(mappedBy = "contrat")
    @NotAudited
    Set<ContratEtablissement> contratEtablissements = new HashSet<>();

    /**
     * référence vers l'attributaire principal ( importé depuis MPE )
     */
    @OneToOne
    @JoinColumn(name = "ID_ATTRIBUTAIRE")
    @Audited(targetAuditMode = NOT_AUDITED)
    ContratEtablissement attributaire;

    @OneToOne
    @JoinColumn(name = "ID_GROUPEMENT")
    @Audited(targetAuditMode = NOT_AUDITED)
    ContratEtablissementGroupement groupement;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT_CADRE_GROUPEMENT")
    @Audited(targetAuditMode = NOT_AUDITED)
    ContratCadreEtablissementGroupement contratCadreGroupement;

    @OneToMany(mappedBy = "contrat", cascade = CascadeType.REMOVE)
    @NotAudited
    Set<DocumentContrat> documents = new HashSet<>();

    Boolean synchronisable;
    BigDecimal montant;
    BigDecimal montantFacture;
    BigDecimal montantMandate;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_NOTIFICATION", columnDefinition = "VARCHAR(22) default 'MANUELLE'")
    private TypeNotification typeNotification;

    LocalDate dateNotification;
    LocalDate dateFinContrat;
    LocalDate dateMaxFinContrat;
    @Column(name = "chapeau", columnDefinition = " boolean default false COMMENT 'contrat chapeau'")
    boolean chapeau;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT_CHAPEAU")
    Contrat contratChapeau;

    @OneToMany(mappedBy = "contratChapeau", cascade = CascadeType.REMOVE)
    Set<Contrat> contrats = new HashSet<>();

    @OneToMany(mappedBy = "contrat", cascade = CascadeType.REMOVE)
    @NotAudited
    Set<Evenement> evenements = new HashSet<>();

    @OneToMany(mappedBy = "contrat", cascade = CascadeType.REMOVE)
    @NotAudited
    private Set<Acte> actes = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "ID_CATEGORIE")
    @Audited(targetAuditMode = NOT_AUDITED)
    CategorieConsultation categorie;

    @Getter(AccessLevel.NONE)
    private Boolean created;

    @ManyToOne
    @JoinColumn(name = "id_forme_prix")
    @Audited(targetAuditMode = NOT_AUDITED)
    FormePrix typeFormePrix;


    @Enumerated(EnumType.STRING)
    private TypeBorne typeBorne;

    private BigDecimal borneMinimale;
    private BigDecimal borneMaximale;
    private LocalDate dateDemaragePrestation;

    @ManyToOne
    @JoinColumn(name = "ID_UNITE_FONCTIONNELLE_OPERATION")
    @Audited(targetAuditMode = NOT_AUDITED)
    private UniteFonctionnelleOperation uniteFonctionnelleOperation;

    @ManyToOne
    @JoinColumn(name = "ID_CODE_ACHAT")
    @Audited(targetAuditMode = NOT_AUDITED)
    private CodeAchat codeAchat;

    @ManyToOne
    @JoinColumn(name = "ID_DONNEES_ESSENTIELLES")
    @Audited(targetAuditMode = NOT_AUDITED)
    DonneesEssentielles donneesEssentielles;

    @OneToMany(mappedBy = "contrat", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    Set<DonneesEssentielles> modifications = new HashSet<>();

    @OneToMany(mappedBy = "contrat")
    @NotAudited
    Set<EchangeChorus> echangesChorus = new HashSet<>();

    @Lob
    private String intitule;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONTRAT_CPV", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_CPV")})
    @NotAudited
    private Set<CPV> codesCpv = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONTRAT_LIEU_EXECUTION", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_LIEU_EXECUTION")})
    @NotAudited
    private Set<LieuExecution> lieuxExecution = new HashSet<>();

    @ManyToOne
    @Audited(targetAuditMode = NOT_AUDITED)
    @JoinColumn(name = "ID_CCAG_REFERENCE")
    private CCAGReference ccagApplicable;

    @ColumnDefault(value = "0")
    private boolean achatResponsable;

    private String trancheBudgetaire;
    private String modaliteRevisionPrix;

    @ManyToOne
    @JoinColumn(name = "id_revision_prix")
    @Audited(targetAuditMode = NOT_AUDITED)
    RevisionPrix revisionPrix;

    @ColumnDefault(value = "0")
    private boolean defenseOuSecurite;

    @ColumnDefault(value = "0")
    private boolean marcheInnovant;

    @ColumnDefault(value = "0")
    private boolean publicationDonneesEssentielles;

    @ColumnDefault(value = "0")
    private boolean attributionAvance;

    BigDecimal tauxAvance;

    private LocalDate datePrevisionnelleNotification;
    private LocalDate datePrevisionnelleFinMarche;
    private LocalDate datePrevisionnelleFinMaximaleMarche;
    private Integer dureeMaximaleMarche;
    private LocalDate dateDebutExecution;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONTRAT_ORGANISMES_ELIGIBLES", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_ORGANISME")})
    @NotAudited
    private Set<Organisme> organismesEligibles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "CONTRAT_SERVICES_ELIGIBLES", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_SERVICE")})
    @NotAudited
    private Set<Service> servicesEligibles = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private StatutRenouvellement statutRenouvellement;

    private LocalDate dateDebutEtudeRenouvellement;

    @ColumnDefault(value = "1")
    private boolean aRenouveler;

    @Column(columnDefinition = " INT(11) default 9 COMMENT 'durée en mois'")
    private Integer dureePhaseEtudeRenouvellement;

    private LocalDateTime dateEnvoiAlerte;
    private String idExterneCreateur;
    Integer numeroLot;

    @ColumnDefault(value = "0")
    private boolean considerationsEnvironnementales;

    @ColumnDefault(value = "0")
    private boolean considerationsSociales;

    @ColumnDefault(value = "0")
    private boolean besoinReccurent;

    private Integer idOffre;

    @ManyToOne
    @JoinColumn(name = "ID_CPV_PRINCIPAL")
    @Audited(targetAuditMode = NOT_AUDITED)
    CPV cpvPrincipal;

    @OneToMany(mappedBy = "contrat", cascade = CascadeType.REMOVE)
    @NotAudited
    private Set<ContratClause> contratClauses = new HashSet<>();

    @Lob
    private String statutEJ;

    @ManyToOne
    @JoinColumn(name = "LIEN_AC_SAD")
    Contrat lienAcSad;

    @OneToMany(mappedBy = "lienAcSad")
    Set<Contrat> contratsLienAcSad;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "CONTRAT_FAVORIS", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_UTILISATEUR")})
    @NotAudited
    private Set<Utilisateur> favoris = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "CONTRAT_INVITE_PONCTUEL", joinColumns = {
            @JoinColumn(name = "ID_CONTRAT")},
            inverseJoinColumns = {@JoinColumn(name = "ID_UTILISATEUR")})
    @NotAudited
    private Set<Utilisateur> invitesPonctuels = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "ID_NATURE_CONTRAT_CONCESSION")
    @NotAudited
    NatureContratConcession natureContratConcession;

    BigDecimal valeurGlobale;
    BigDecimal montantSubventionPublique;
    String numeroPublicationTNCP;

    Integer idExternesLot;


    public Contrat(String idExterne, boolean synchronisable, StatutRenouvellement statutRenouvellement) {
        super();
        this.setIdExterne(idExterne);
        this.setSynchronisable(synchronisable);
        this.setStatutRenouvellement(statutRenouvellement);
    }

    public int getDocumentsCount() {
        if (documents == null) {
            return 0;
        }
        return documents.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Contrat that = (Contrat) o;

        return Objects.equal(this.numero, that.numero) && Objects.equal(this.objet, that.objet) && Objects.equal(this.consultation, that.consultation);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numero, objet, consultation);
    }


    public Boolean getCreated() {
        if (created == null) {
            return Boolean.FALSE;
        }
        return created;
    }

    public void updateContratStatus() {
        final var localDate = LocalDate.now();
        if (StatutContrat.Notifie.equals(this.getStatut()) && dateNotification != null) {
            if (!localDate.isBefore(dateNotification) && dateFinContrat != null && !localDate.isAfter(dateFinContrat)) {
                this.setStatut(StatutContrat.EnCours);
                return;
            }
            if (dateFinContrat != null && localDate.isAfter(dateFinContrat)) {
                this.setStatut(StatutContrat.Clos);
            }
        }
    }


    public boolean isExNihilo() {
        return this.getIdExterne() == null || this.getIdExterne().isEmpty();
    }

    public List<Evenement> getListBondeCommandes() {
        return this.evenements.stream().filter(evenement -> evenement.getBonCommandeType() != null).collect(Collectors.toList());
    }

    public List<Evenement> getListAvenantsNonRejete() {
        return this.evenements.stream().filter(evenement -> evenement.getAvenantType() != null && EtatEvenement.REJETE != evenement.getEtatEvenement()).collect(Collectors.toList());
    }

    public boolean isConcession() {
        return Optional.ofNullable(this.getType())
                .map(AbstractReferentielEntity::getParents)
                .orElse(Collections.emptySet())
                .stream().anyMatch(e -> e instanceof TypeConcession);
    }

    public Set<ContratEtablissement> getSousTraitans() {
        // l'attributaire est un contrat etablissement, on filtre les sous traitants qui ne sont pas attributaires
        if (this.attributaire == null) {
            return new HashSet<>();
        }
        return this.contratEtablissements.stream().filter(ce -> !attributaire.getId().equals(ce.getId())).collect(Collectors.toSet());
    }
}
