package com.atexo.execution.server.model;


import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.common.dto.NumerotationTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NumerotationResult {
    private final ParametrageNumerotation parametrage;
    private final NiveauEnum niveau;
    private final Long idPlateforme;
    private final Long idOrganisme;
    private final Long idService;
    private NumerotationTypeEnum type;

    public NumerotationResult(ParametrageNumerotation parametrage, Long idPlateforme, Long idOrganisme, Long idService, NiveauEnum niveau) {
        this.parametrage = parametrage;
        this.idPlateforme = idPlateforme;
        this.idOrganisme = idOrganisme;
        this.niveau = niveau;
        this.idService = idService;
    }
}
