package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Embeddable
@Getter
@Setter
public class SousTraitant {
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
	@JoinColumn(name = "ID_SOUS_TRAITANT")
	private Contact contact;

	@Column(name = "DATE_DEMANDE")
	private LocalDate dateDemande;

	@Column(name = "DATE_NOTIFICATION")
	private LocalDate dateNotification;


}
