package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "acte_resiliation")
@Getter
@Setter
public class Resiliation extends Acte {

	public static final String CODE = "ACT_RES";

	@Column(name = "COUVERTURE")
	private String couverture;

	@Column(name = "MOTIF")
	private String motif;

	@Column(name = "FRAIS_ET_RISQUES_POUR_LE_TITULAIRE")
	private Boolean fraisEtRisquesPourLeTitulaire;

	@Column(name = "MISE_EN_OEUVRE")
	private String miseEnOeuvre;

	@Column(name = "FIN_CONTRAT")
	private LocalDate finContrat;

    @Override
    public String getCode() {
        return CODE;
    }
}
