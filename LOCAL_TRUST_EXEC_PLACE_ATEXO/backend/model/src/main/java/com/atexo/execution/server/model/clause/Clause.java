package com.atexo.execution.server.model.clause;

import com.atexo.execution.server.model.AbstractReferentielEntity;
import com.atexo.execution.server.model.ContratClause;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.NotAudited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Getter
@Setter
@Entity
public class Clause extends AbstractReferentielEntity {

    @OneToMany(mappedBy = "clause", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    private Set<ContratClause> contratClauses;
}
