package com.atexo.execution.server.model.actes;

import com.atexo.execution.common.def.Variation;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.SousTraitant;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
@Table(name = "acte_agrement_du_sous_traitant")
@Getter
@Setter
public class AgrementSousTraitant extends Acte {

	public static final String CODE = "ACT_AST";

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "dateDemande", column = @Column(name = "SOUS_TRAITANT_DATE_DEMANDE")),
			@AttributeOverride(name = "dateNotification", column = @Column(name = "SOUS_TRAITANT_DATE_NOTIFICATION")),
	})
	@AssociationOverrides(
			@AssociationOverride(name = "contact", joinColumns = @JoinColumn(name = "ID_SOUS_TRAITANT", foreignKey = @ForeignKey(name = "FK_AGREMENT_SOUS_TRAITANT_CONTACT")))
	)
	private SousTraitant sousTraitant;

	@Column(name = "SOUS_TRAITANT_SIRET")
	private String siret;

	@Column(name = "RANG")
	private Integer rang;

	@Column(name = "REFERENCE")
	private String reference;

	@Column(name = "MODALITE_SOUS_TRAITANCE")
	private Double modaliteSousTraitance;

	@Column(name = "MODALITE_HT")
	private Double montantHT;

	@Column(name = "MODALITE_TTC")
	private Double montantTTC;

	@Column(name = "TAUX_TVA")
	private Double tauxTVA;

	@Column(name = "VARIATION")
	private Variation variation;

	@Column(name = "DUREE")
	private Long duree;

    @Column(name = "PUBLICATION_DE")
	@ColumnDefault(value = "0")
    private Boolean publicationDonneesEssentielles;

	@Override
	public String getCode() {
		return CODE;
	}
}
