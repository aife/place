package com.atexo.execution.server.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Modèle de document destiné à la génération
 */
@Entity
@Table(name = "DOCUMENT_MODELE")
public class DocumentModele implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String label;

    private String filename;

    @Column(name = "ws_ressources")
    private String wsRessources;

    // Si le document modèle est custom, alors il doit être lié à un ou plusieurs organismes pour être accessible
    // Un document custom ayant le même code qu'un document non custom surcharge ce dernier (écrase à l'affichage)
    private Boolean custom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Boolean getCustom() {
        return custom;
    }

    public void setCustom(Boolean custom) {
        this.custom = custom;
    }

    public String getWsRessources() {
        return wsRessources;
    }

    public void setWsRessources(String wsRessources) {
        this.wsRessources = wsRessources;
    }
}
