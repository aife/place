package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "SERVICE", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
public class Service extends AbstractSynchronizedEntity {

	String nom;

	private String nomCourt;

	private String nomCourtArborescence;

	@ManyToOne
	@JoinColumn(name = "ID_ORGANISME")
	Organisme organisme;

	@ManyToOne
	@JoinColumn(name = "ID_PARENT")
	Service parent;

	Integer niveau;

	Boolean actif;

	Boolean actifLecture;

	@OneToMany(mappedBy = "parent")
	private List<Service> services = new ArrayList<>();

	boolean echangesChorus;

	@Column(name = "OLD_ID_EXTERNE")
	private String oldIdExterne;

	private String siren;

	private String complement;

	private String raisonSociale;

	private String sigle;

	public String getSiret() {
		return this.getSiren() + this.getComplement();
	}

}
