package com.atexo.execution.server.model;


import com.google.common.base.Objects;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Table Contact Reférant
 */
@Entity
@Table(name = "CONTACT_REFERANT")
public class ContactReferant implements Serializable {

    @EmbeddedId
    private ContactReferantId id;

    @ManyToOne
    @JoinColumn(name = "ID_CONTACT", updatable = false, insertable = false)
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRAT_ETABL", updatable = false, insertable = false)
    private ContratEtablissement contratEtablissement;

    private String roleContrat;


    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public ContratEtablissement getContratEtablissement() {
        return contratEtablissement;
    }

    public ContactReferantId getId() {
        return id;
    }

    public void setId(ContactReferantId id) {
        this.id = id;
    }

    public void setContratEtablissement(ContratEtablissement contratEtablissement) {
        this.contratEtablissement = contratEtablissement;
    }

    @Column(name = "ROLE_CONTRAT")
    public String getRoleContrat() {
        if (roleContrat == null) {
            roleContrat = "";
        }
        return roleContrat;
    }

    public void setRoleContrat(String roleContrat) {
        this.roleContrat = roleContrat;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ContactReferant that = (ContactReferant) o;

        return that.getContact().getId() == getContact().getId() && that.getContratEtablissement().getId() == getContratEtablissement().getId();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(contact.getId(), contratEtablissement.getId());
    }

}
