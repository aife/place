package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by sta on 09/09/16.
 */
@Entity
@DiscriminatorValue(ReferentielDiscriminators.CATEGORIE_TYPE_EVENEMENT)
public class CategorieTypeEvenement extends AbstractReferentielEntity {
}
