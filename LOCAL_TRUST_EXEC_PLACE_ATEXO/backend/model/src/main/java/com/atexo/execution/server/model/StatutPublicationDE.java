package com.atexo.execution.server.model;

public enum StatutPublicationDE {
    EN_COURS,A_PUBLIER,PUBLIE,ERROR,NON_PUBLIE
}
