package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.LIEU_EXECUTION)
@EqualsAndHashCode(callSuper = true)
public class LieuExecution extends AbstractReferentielEntity {

}
