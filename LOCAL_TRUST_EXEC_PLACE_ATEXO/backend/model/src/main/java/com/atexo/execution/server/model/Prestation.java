package com.atexo.execution.server.model;

import javax.persistence.Embeddable;

@Embeddable
public class Prestation {

	private String nature;

	private String categorie;

	public String getNature() {
		return nature;
	}

	public void setNature( String nature ) {
		this.nature = nature;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie( String categorie ) {
		this.categorie = categorie;
	}
}
