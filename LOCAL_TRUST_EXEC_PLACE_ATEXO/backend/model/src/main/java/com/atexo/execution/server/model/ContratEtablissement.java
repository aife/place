package com.atexo.execution.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "contrat_etablissement", indexes = {@Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContratEtablissement extends AbstractSynchronizedEntity {


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_contrat")
	Contrat contrat;

	@ManyToOne
	@JoinColumn(name = "id_etablissement")
	Etablissement etablissement;

	@OneToOne
	@JoinColumn(name = "id_contact")
	Contact contact;

	String role;

	// nature prestation
	@ManyToOne
	@JoinColumn(name = "id_categorie_consultation")
	CategorieConsultation categorieConsultation;

	Boolean mandataire; // non null dans le cas d'un groupement

	@ManyToOne
	@JoinColumn(name = "id_commanditaire")
	Etablissement commanditaire; // pour lequel l'etablissement est sous
	// traitant

	BigDecimal montant;

	LocalDate dateNotification;

	@OneToMany(mappedBy = "contratEtablissement", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<ContactReferant> contactReferantList = new HashSet<>();

	private Integer dureeMois;

	@ManyToOne
	@JoinColumn(name = "id_revision_prix")
	private RevisionPrix revisionPrix;
}
