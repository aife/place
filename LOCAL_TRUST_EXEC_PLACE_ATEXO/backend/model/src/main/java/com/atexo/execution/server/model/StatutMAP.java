package com.atexo.execution.server.model;

public enum StatutMAP {
    EN_ATTENTE, EN_COURS, ACQUITE, ECHEC
}
