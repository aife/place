package com.atexo.execution.server.model;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "FICHIER", indexes = {@Index(name = "IX_FICHIER_NOM", columnList = "nom")})
public class Fichier extends AbstractEntity {

	String nom;

	Long taille;

	String contentType;

	String nomEnregistre;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getTaille() {
		return taille;
	}

	public void setTaille(Long taille) {
		this.taille = taille;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
    }

    public String getNomEnregistre() {
        return nomEnregistre;
    }

    public void setNomEnregistre(String nomEnregistre) {
        this.nomEnregistre = nomEnregistre;
    }

    @Override
    public String toString() {
        return "Fichier{" +
                "nom='" + nom + '\'' +
                ", taille=" + taille +
                ", contentType='" + contentType + '\'' +
                ", nomEnregistre='" + nomEnregistre + '\'' +
                '}';
    }
}
