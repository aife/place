package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.Etablissement;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "acte_ordre_de_service")
@Getter
@Setter
public class OrdreService extends Acte {

	public static final String CODE = "ACT_ODS";

	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
	@JoinTable(
		name = "ACTE_COTRAITANTS",
		joinColumns = { @JoinColumn(name = "ID_ACTE") },
		inverseJoinColumns = { @JoinColumn(name = "ID_COTRAITANT") }
	)
	private Set<Etablissement> cotraitants = new HashSet<>();

	@Column(name = "RECONDUCTION")
	private Boolean reconduction;

	@Column(name = "MAITRE_OEUVRE")
	private String maitreOeuvre;

	@Column(name = "MAITRE_OEUVRAGE")
	private String maitreOuvrage;

    @Override
    public String getCode() {
        return CODE;
    }
}
