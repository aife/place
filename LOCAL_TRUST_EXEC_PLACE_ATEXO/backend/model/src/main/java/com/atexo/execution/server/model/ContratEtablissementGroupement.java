package com.atexo.execution.server.model;

import com.atexo.execution.common.def.TypeGroupement;

import javax.persistence.Entity;

@Entity
public class ContratEtablissementGroupement extends AbstractEntity {

	String nom;

	TypeGroupement type;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public TypeGroupement getType() {
		return type;
	}

	public void setType(TypeGroupement type) {
		this.type = type;
	}

}
