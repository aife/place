package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "acte_modificatif")
@Getter
@Setter
public class ActeModificatif extends Acte {

	public static final String CODE = "ACT_MOD";
	public static final String AVEC_INCIDENCE = "AVEC_INCIDENCE";

	@Column(name = "DUREE_AJOUREE_CONTRAT")
	private Long dureeAjoureeContrat;

	@Column(name = "NOUVELLE_FIN_CONTRAT")
	private LocalDate nouvelleFinContrat;

	@Column(name = "PUBLICATION_DE")
	@ColumnDefault(value = "0")
	private Boolean publicationDonneesEssentielles;

	@Column(name = "CLAUSE_REEXAMEN")
	private Boolean clauseReexamen;

	@Column(name = "TYPE_MODIFICATION")
	private String typeModification;

	@Column(name = "MODALITE_HT_CHIFFRE")
	private Double montantHTChiffre;

	@Column(name = "MODALITE_TTC_CHIFFRE")
	private Double montantTTCChiffre;

	@Column(name = "TAUX_TVA")
	private Double tauxTVA;

    @Override
    public String getCode() {
        return CODE;
    }
}
