package com.atexo.execution.server.model;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.common.dto.NumerotationTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parametrage_numerotation", uniqueConstraints = {@UniqueConstraint(name = "parametrage_numerotation_uc", columnNames = {"type", "niveau_plateforme_organisme_service", "id_plateforme_organisme_service"})})
public class ParametrageNumerotation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "parametrage_configuration_asso",
            joinColumns = @JoinColumn(name = "id_parametrage"),
            inverseJoinColumns = @JoinColumn(name = "id_configuration"))
    private Set<ParametrageNumerotationConfiguration> configurations = new HashSet<>();


    @Column(name = "unique_numero", nullable = false, columnDefinition = "boolean default false")
    private boolean uniqueNumero;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private NumerotationTypeEnum type;

    @Column(name = "niveau_plateforme_organisme_service", nullable = false)
    @Enumerated(EnumType.STRING)
    private NiveauEnum niveau;

    @Column(name = "id_plateforme_organisme_service", nullable = false)
    private long idPlateformeOrganismeService;

    @Column(name = "prefix")
    private String prefix;

    @Column(name = "suffix")
    private String suffix;

}
