package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "acte_donnees_execution")
@Getter
@Setter
public class DonneesExecution extends Acte {
    public static final String CODE = "ACT_DE";

    @Column(name = "PUBLICATION_DE")
    @ColumnDefault(value = "0")
    private Boolean publicationDonneesEssentielles;

    private Double depensesInvestissement;
    private String intituleTarif;
    private Double tarif;

    @Override
    public String getCode() {
        return CODE;
    }
}
