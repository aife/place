package com.atexo.execution.server.model;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"dateModificationExterne", "idExterne", "plateforme"})
@ToString(of = {"dateModificationExterne", "idExterne"})
@Table(indexes = {@Index(name = "IX_ID_EXTERNE", columnList = "ID_EXTERNE"), @Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@Audited
public abstract class AbstractSynchronizedEntity extends AbstractEntity {

    @Column(name = "DATE_MODIFICATION_EXTERNE")
    private LocalDateTime dateModificationExterne;

    @Column(name = "ID_EXTERNE")
    private String idExterne;

    @ManyToOne
    @JoinColumn(name = "ID_PLATEFORME")
    @Audited(targetAuditMode = NOT_AUDITED)
    Plateforme plateforme;

}
