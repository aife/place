package com.atexo.execution.server.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_ECHANGE")
@Table(name = "ECHANGE")
public abstract class AbstractEchangeEntity extends AbstractSynchronizedEntity {

    String statut;

    String erreurPublication;

    LocalDateTime datePublication;

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getErreurPublication() {
        return erreurPublication;
    }

    public void setErreurPublication(String erreurPublication) {
        this.erreurPublication = erreurPublication;
    }

    public LocalDateTime getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDateTime datePublication) {
        this.datePublication = datePublication;
    }

}
