package com.atexo.execution.server.model.actes;

import com.atexo.execution.server.model.Acte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "acte_decision_reconduction")
@Getter
@Setter
public class DecisionReconduction extends Acte {

	public static final String CODE = "ACT_DRN";

	@Column(name = "RECONDUCTION")
	private Boolean reconduction;

	@Column(name = "DEBUT")
	private LocalDate debut;

	@Column(name = "FIN")
	private LocalDate fin;

    @Override
    public String getCode() {
        return CODE;
    }
}
