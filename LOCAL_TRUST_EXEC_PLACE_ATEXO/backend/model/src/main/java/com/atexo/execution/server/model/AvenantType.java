package com.atexo.execution.server.model;

import com.atexo.execution.common.def.StatutAvenant;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Embeddable
public class AvenantType {


    @Column(name = "AVENANT_INCIDENCE")
    private Boolean incidence;

    @Column(name = "AVENANT_TRANSFERT")
    private Boolean avenantTransfert;

    @Column(name = "AVENANT_DATEMODIFIABLE")
    private Boolean dateModifiable;

    @Column(name = "AVENANT_AUTRE_TYPE")
    private Boolean autreType;

    @Column(name = "AVENANT_VALUE")
    private Boolean value;

    @Column(name = "AVENANT_MONTANT_VALUE")
    private BigDecimal montant;

    @ManyToOne
    @JoinColumn(name = "ID_AVENANT_TYPE_TYPE_AVENANT")
    private TypeAvenant typeAvenant;

    @Column(name = "AVENANT_DATE_DEM")
    private LocalDate dateDemaragePrestation;

    @Column(name = "AVENANT_DATE_DEM_N")
    private LocalDate dateDemaragePrestationNew;

    @Column(name = "AVENANT_DATE_FIN")
    private LocalDate dateFin;

    @Column(name = "AVENANT_DATE_FIN_MAX")
    private LocalDate dateFinMax;

    @Column(name = "AVENANT_DATE_FIN_N")
    private LocalDate dateFinNew;

    @Column(name = "AVENANT_DATE_FIN_MAX_N")
    private LocalDate dateFinMaxNew;

    @Column(name = "DATE_ACQUITTEMENT")
    private LocalDate dateAcquittement;

    @Column(name = "AVENANT_NUMERO")
    private String numero;

    @Column(name = "STATUT")
    @Enumerated(EnumType.STRING)
    StatutAvenant statut;

    public Boolean getIncidence() {
        if (incidence == null) {
            return false;
        }
        return incidence;
    }

    public void setIncidence(Boolean incidence) {
        this.incidence = incidence;
    }

    public Boolean getDateModifiable() {
        if (dateModifiable == null) {
            return false;
        }
        return dateModifiable;
    }

    public void setDateModifiable(Boolean dateModifiable) {
        this.dateModifiable = dateModifiable;
    }

    public Boolean getAutreType() {
        if (autreType == null) {
            return false;
        }
        return autreType;
    }

    public void setAutreType(Boolean autreType) {
        this.autreType = autreType;
    }

    public Boolean getValue() {
        if (value == null) {
            return false;
        }
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public TypeAvenant getTypeAvenant() {
        return typeAvenant;
    }

    public void setTypeAvenant(TypeAvenant typeAvenant) {
        this.typeAvenant = typeAvenant;
    }

    public LocalDate getDateDemaragePrestation() {
        return dateDemaragePrestation;
    }

    public void setDateDemaragePrestation(LocalDate dateDemaragePrestation) {
        this.dateDemaragePrestation = dateDemaragePrestation;
    }

    public LocalDate getDateDemaragePrestationNew() {
        return dateDemaragePrestationNew;
    }

    public void setDateDemaragePrestationNew(LocalDate dateDemaragePrestationNew) {
        this.dateDemaragePrestationNew = dateDemaragePrestationNew;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDate getDateFinMax() {
        return dateFinMax;
    }

    public void setDateFinMax(LocalDate dateFinMax) {
        this.dateFinMax = dateFinMax;
    }

    public LocalDate getDateFinNew() {
        return dateFinNew;
    }

    public void setDateFinNew(LocalDate dateFinNew) {
        this.dateFinNew = dateFinNew;
    }

    public LocalDate getDateFinMaxNew() {
        return dateFinMaxNew;
    }

    public void setDateFinMaxNew(LocalDate dateFinMaxNew) {
        this.dateFinMaxNew = dateFinMaxNew;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDateAcquittement() {
        return dateAcquittement;
    }

    public void setDateAcquittement(LocalDate dateAcquittement) {
        this.dateAcquittement = dateAcquittement;
    }

    public StatutAvenant getStatut() {
        return statut;
    }

    public void setStatut(StatutAvenant statut) {
        this.statut = statut;
    }

    public Boolean getAvenantTransfert() {
        return avenantTransfert;
    }

    public void setAvenantTransfert(Boolean avenantTransfert) {
        this.avenantTransfert = avenantTransfert;
    }
}
