package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.CPV)
@EqualsAndHashCode(callSuper = true)
public class CPV extends AbstractReferentielEntity {

    public CPV() {
        super();
    }

    public CPV(String value, String label, String idExterne) {
        super(value, label, idExterne);
        actif = true;
    }
}
