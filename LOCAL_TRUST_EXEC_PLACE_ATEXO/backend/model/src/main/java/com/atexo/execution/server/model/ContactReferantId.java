package com.atexo.execution.server.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContactReferantId implements Serializable {

    @Column(name = "ID_CONTACT")
    private Long idContact;

    @Column(name = "ID_CONTRAT_ETABL")
    private Long idContratEtablissement;
}
