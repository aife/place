package com.atexo.execution.server.model;

import com.atexo.execution.common.dto.NiveauEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parametrage_numerotation_compteur", uniqueConstraints = {@UniqueConstraint(name = "parametrage_numerotation_compteur_entite_uc", columnNames = {"id_parametrage_numerotation_champs", "niveau_plateforme_organisme_service", "id_plateforme_organisme_service"})})
public class ParametrageNumerotationCompteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "compteur", nullable = false, columnDefinition = "int default 0")
    private int compteur;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_parametrage_numerotation_champs")
    private ParametrageNumerotationChamp parametrageNumerotationChamp;

    @Column(name = "niveau_plateforme_organisme_service", nullable = false)
    @Enumerated(EnumType.STRING)
    private NiveauEnum niveau;

    @Column(name = "id_plateforme_organisme_service", nullable = false)
    private long idPlateformeOrganismeService;

}
