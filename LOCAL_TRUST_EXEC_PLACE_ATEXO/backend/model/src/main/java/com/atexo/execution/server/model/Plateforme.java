package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "plateforme")
public class Plateforme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uid", unique = true)
    String uid;

    @Column(name = "mpe_uid", unique = true)
    String mpeUid;
    String faqUid;
    String mpeUrl;
    String mpeApiLogin;
    String mpeApiPassword;
    String client;
    String environnement;
    LocalDateTime tokenUpdateDate;
    String tncpIdentifiant;
    String tncpIdentifiantTechnique;
    @Column(name = "active", columnDefinition = "boolean default true")
    boolean active = true;

    @ManyToOne
    @JoinColumn(name = "id_devise")
    private Devise devise;

    @OneToMany(mappedBy = "plateforme")
    private Set<Utilisateur> utilisateurs = new HashSet<>();

}
