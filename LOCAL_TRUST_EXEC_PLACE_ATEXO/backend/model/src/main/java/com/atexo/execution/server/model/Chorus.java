package com.atexo.execution.server.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Chorus {

	private Boolean actif;

	@Column(name = "VISA_ACCF")
	private String visaACCF;

	@Column(name = "VISA_PREFET")
	private String visaPrefet;

	private String typeFournisseurEntreprise1;

	public Boolean getActif() {
		return actif;
	}

	public void setActif( Boolean actif ) {
		this.actif = actif;
	}

	public String getVisaACCF() {
		return visaACCF;
	}

	public void setVisaACCF( String visaACCF ) {
		this.visaACCF = visaACCF;
	}

	public String getVisaPrefet() {
		return visaPrefet;
	}

	public void setVisaPrefet( String visaPrefet ) {
		this.visaPrefet = visaPrefet;
	}

	public String getTypeFournisseurEntreprise1() {
		return typeFournisseurEntreprise1;
	}

	public void setTypeFournisseurEntreprise1( String typeFournisseurEntreprise1 ) {
		this.typeFournisseurEntreprise1 = typeFournisseurEntreprise1;
	}

}
