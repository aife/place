package com.atexo.execution.server.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Audited
public abstract class AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer version;

    @CreatedDate
    @Column(name = "DATE_CREATION")
    private LocalDateTime dateCreation;

    @CreatedBy
    @ManyToOne
    @JoinColumn(name = "ID_UTILISATEUR_CREATION", updatable = false)
    @Audited(targetAuditMode = NOT_AUDITED)
    private Utilisateur utilisateurCreation;

    @LastModifiedDate
    @Column(name = "DATE_MODIFICATION")
    private LocalDateTime dateModification;

    @LastModifiedBy
    @ManyToOne
    @JoinColumn(name = "ID_UTILISATEUR_MODIFICATION")
    @Audited(targetAuditMode = NOT_AUDITED)
    private Utilisateur utilisateurModification;

    @Column(name = "uuid", length = 36)
    private String uuid;
    @Override
    public int hashCode() {
        return id.hashCode();
    }
    @PrePersist
    public void prePersist() {
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Utilisateur getUtilisateurCreation() {
        return utilisateurCreation;
    }

    public void setUtilisateurCreation(Utilisateur utilisateurCreation) {
        this.utilisateurCreation = utilisateurCreation;
    }

    public LocalDateTime getDateModification() {
        return dateModification;
    }

    public void setDateModification(LocalDateTime dateModification) {
        this.dateModification = dateModification;
    }

    public Utilisateur getUtilisateurModification() {
        return utilisateurModification;
    }

    public void setUtilisateurModification(Utilisateur utilisateurModification) {
        this.utilisateurModification = utilisateurModification;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
