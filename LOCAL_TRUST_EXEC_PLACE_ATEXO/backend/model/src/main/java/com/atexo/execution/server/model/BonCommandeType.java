package com.atexo.execution.server.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Embeddable
public class BonCommandeType {

    @Column(name = "BONCOMMANDE_NUM")
    private String numero;

    @Column(name = "BONCOMMANDE_MNTHT")
    private BigDecimal montantHT;

    @ManyToOne
    @JoinColumn(name = "ID_BON_COMMANDE_TYPE_TAUX_TVA")
    private TauxTVA tauxTVA;

    @Column(name = "BONCOMMANDE_MNTTVA")
    private BigDecimal montantTVA;

    @Column(name = "BONCOMMANDE_DELAILIV")
    private String delaiLivExec;

    @Column(name = "BONCOMMANDE_PROGNUM")
    private String programmeNum;

    @Column(name = "BONCOMMANDE_DEVISNUM")
    private String devisTitulaireNum;

    @Column(name = "BONCOMMANDE_ADDRLIV")
    private String adresseLivExec;

    @Column(name = "BONCOMMANDE_DELAIPAIE")
    private String delaiPaiement;

    @Column(name = "BONCOMMANDE_IMPUBUDG")
    private String imputationBudgetaire;

    @Column(name = "BONCOMMANDE_ENGAGNUM")
    private String engagementNum;

    @Column(name = "BONCOMMANDE_ADDFACTU")
    private String adresseFacturation;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(BigDecimal montantHT) {
        this.montantHT = montantHT;
    }

    public TauxTVA getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(TauxTVA tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public BigDecimal getMontantTVA() {
        return montantTVA;
    }

    public void setMontantTVA(BigDecimal montantTVA) {
        this.montantTVA = montantTVA;
    }

    public String getDelaiLivExec() {
        return delaiLivExec;
    }

    public void setDelaiLivExec(String delaiLivExec) {
        this.delaiLivExec = delaiLivExec;
    }

    public String getProgrammeNum() {
        return programmeNum;
    }

    public void setProgrammeNum(String programmeNum) {
        this.programmeNum = programmeNum;
    }

    public String getDevisTitulaireNum() {
        return devisTitulaireNum;
    }

    public void setDevisTitulaireNum(String devisTitulaireNum) {
        this.devisTitulaireNum = devisTitulaireNum;
    }

    public String getAdresseLivExec() {
        return adresseLivExec;
    }

    public void setAdresseLivExec(String adresseLivExec) {
        this.adresseLivExec = adresseLivExec;
    }

    public String getDelaiPaiement() {
        return delaiPaiement;
    }

    public void setDelaiPaiement(String delaiPaiement) {
        this.delaiPaiement = delaiPaiement;
    }

    public String getImputationBudgetaire() {
        return imputationBudgetaire;
    }

    public void setImputationBudgetaire(String imputationBudgetaire) {
        this.imputationBudgetaire = imputationBudgetaire;
    }

    public String getEngagementNum() {
        return engagementNum;
    }

    public void setEngagementNum(String engagementNum) {
        this.engagementNum = engagementNum;
    }

    public String getAdresseFacturation() {
        return adresseFacturation;
    }

    public void setAdresseFacturation(String adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }
}
