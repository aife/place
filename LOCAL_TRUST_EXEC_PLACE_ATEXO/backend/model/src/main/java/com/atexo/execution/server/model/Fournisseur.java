package com.atexo.execution.server.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "FOURNISSEUR", indexes = {@Index(name = "IX_FOURNISSEUR_SIREN", columnList = "siren"), @Index(name = "IX_FOURNISSEUR_RAISON_SOCIALE", columnList = "raisonSociale"), @Index(name = "IX_FOURNISSEUR_DESCRIPTION", columnList = "description"), @Index(name = "IX_ID_EXTERNE_ID_PLATEFORME", columnList = "ID_EXTERNE,ID_PLATEFORME")})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL, region = "Fournisseur")
public class Fournisseur extends AbstractSynchronizedEntity {


	String siren;

	String raisonSociale;

	String email;

	String formeJuridique;

	Integer taille;

	String telephone;

	String fax;

	String siteInternet;

	String pays;

	@Getter(AccessLevel.NONE)
	Boolean trusted;

	@OneToMany(mappedBy = "fournisseur")
	List<Etablissement> etablissements = new ArrayList<>();

	private String capitalSocial;

	/**
	 * Description de l'activité
 	 */
	String description;

	String codeApeNafNace;

	String nom;

	String prenom;


	@ManyToOne
	@JoinColumn(name = "categorie_fournisseur")
	CategorieFournisseur categorie;


	public Boolean getTrusted(){
		if (trusted == null) {
			return false;
		}
		return trusted;
	}


	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(this.siren).addValue(this.raisonSociale).addValue(this.siteInternet)
				.addValue(this.telephone).addValue(this.email).addValue(this.fax).toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Fournisseur))
			return false;

		Fournisseur that = (Fournisseur) o;

		return Objects.equal(siren, that.siren) && Objects.equal(siteInternet, that.siteInternet)
				&& Objects.equal(telephone, that.telephone) && Objects.equal(email, that.email) && Objects.equal(fax, that.fax);
	}



	@Override
	public int hashCode() {
		return Objects.hashCode(siren, siteInternet, telephone, email, fax);
	}


}
