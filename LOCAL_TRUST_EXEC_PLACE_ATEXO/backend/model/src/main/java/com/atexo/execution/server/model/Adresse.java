package com.atexo.execution.server.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Adresse implements Serializable {

	public static String PAYS_PAR_DEFAUT = "France";

	String adresse;

	String adresse2;

	String codePostal;

	String commune;

	String pays;

	String codeInseeLocalite;



	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("adresse", adresse).add("adresse2", adresse2).add("codePostal", codePostal)
				.add("commune", commune).add("pays", pays).toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Adresse that = (Adresse) o;

		return Objects.equal(this.adresse, that.adresse) && Objects.equal(this.codePostal, that.codePostal)
				&& Objects.equal(this.commune, that.commune) && Objects.equal(this.pays, that.pays);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(adresse, codePostal, commune, pays);
	}
}
