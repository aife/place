package com.atexo.execution.server.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MISE_A_DISPOSITION")
public class MiseADisposition implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "ID_CONTRAT")
    private Contrat contrat;

    @Column(name = "DATE_MISE_A_DISPOSITION")
    private transient LocalDate dateMiseADisposition;

    @Column(name = "DATE_RECUPERATION")
    private transient LocalDate dateRecuperation;

    @ManyToOne
    @JoinColumn(name = "ID_APPLICATION_TIERS")
    private ApplicationTiers applicationTiers;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT_MAP")
    private StatutMAP statutMAP;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "ID_UTILISATEUR")
    private Utilisateur utilisateur;

    @OneToMany(mappedBy = "miseADisposition", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<DocumentContratMiseADisposition> documentMiseADispositionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public LocalDate getDateMiseADisposition() {
        return dateMiseADisposition;
    }

    public void setDateMiseADisposition(LocalDate dateMiseADisposition) {
        this.dateMiseADisposition = dateMiseADisposition;
    }

    public LocalDate getDateRecuperation() {
        return dateRecuperation;
    }

    public void setDateRecuperation(LocalDate dateRecuperation) {
        this.dateRecuperation = dateRecuperation;
    }

    public ApplicationTiers getApplicationTiers() {
        return applicationTiers;
    }

    public void setApplicationTiers(ApplicationTiers applicationTiers) {
        this.applicationTiers = applicationTiers;
    }

    public StatutMAP getStatutMAP() {
        return statutMAP;
    }

    public void setStatutMAP(StatutMAP statutMAP) {
        this.statutMAP = statutMAP;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public List<DocumentContratMiseADisposition> getDocumentMiseADispositionList() {
        if (documentMiseADispositionList == null) {
            return new ArrayList<>();
        }
        return documentMiseADispositionList;
    }

    public void setDocumentMiseADispositionList(List<DocumentContratMiseADisposition> documentMiseADispositionList) {
        this.documentMiseADispositionList = documentMiseADispositionList;
    }
}
