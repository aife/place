package com.atexo.execution.server.model.data.gouv;

import com.atexo.execution.server.model.AbstractEntity;
import com.atexo.execution.server.model.DonneesEssentielles;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class ContratPublication extends AbstractEntity {
    private LocalDateTime datePublication;
    private Boolean publicationDataGouv;
    private Boolean publicationTncp;
    @Lob
    private String message;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_donnees_essentielles")
    private DonneesEssentielles donneesEssentielles;


    public ContratPublication(LocalDateTime datePublication, Boolean publicationDataGouv, Boolean publicationTncp, String message, DonneesEssentielles donneesEssentielles) {
        this.datePublication = datePublication;
        this.publicationDataGouv = publicationDataGouv;
        this.publicationTncp = publicationTncp;
        this.message = message;
        this.donneesEssentielles = donneesEssentielles;
    }

    public ContratPublication() {

    }

    public LocalDateTime getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDateTime datePublication) {
        this.datePublication = datePublication;
    }

    public Boolean getPublicationDataGouv() {
        return publicationDataGouv;
    }

    public void setPublicationDataGouv(Boolean publicationDataGouv) {
        this.publicationDataGouv = publicationDataGouv;
    }

    public Boolean getPublicationTncp() {
        return publicationTncp;
    }

    public void setPublicationTncp(Boolean publicationTncp) {
        this.publicationTncp = publicationTncp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DonneesEssentielles getDonneesEssentielles() {
        return donneesEssentielles;
    }

    public void setDonneesEssentielles(DonneesEssentielles donneesEssentielles) {
        this.donneesEssentielles = donneesEssentielles;
    }
}
