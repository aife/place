package com.atexo.execution.server.model;

import com.atexo.execution.common.def.DocumentDiscriminators;

import javax.persistence.*;

@Entity
@DiscriminatorValue(DocumentDiscriminators.ACTE)
public class DocumentActe extends Document {

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
	@JoinColumn(name = "ID_ACTE")
	Acte acte;

	public Acte getActe() {
		return acte;
	}

	public void setActe( Acte acte ) {
		this.acte = acte;
	}
}
