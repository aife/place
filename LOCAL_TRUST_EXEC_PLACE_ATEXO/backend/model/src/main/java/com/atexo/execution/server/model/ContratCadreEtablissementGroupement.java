package com.atexo.execution.server.model;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class ContratCadreEtablissementGroupement extends AbstractEntity {

    BigDecimal montant;

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

}
