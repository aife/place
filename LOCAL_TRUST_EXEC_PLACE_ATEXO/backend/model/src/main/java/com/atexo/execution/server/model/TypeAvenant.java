package com.atexo.execution.server.model;


import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.TYPE_AVENANT)
public class TypeAvenant extends AbstractReferentielEntity{
}
