package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.TYPE_ACTE)
public class TypeActe extends AbstractReferentielEntity {
    public static final String CODE_ACTE_MODIFICATIF = "ACT_MOD";
    public static final String CODE_AGREMENT_SOUS_TRAITANCE = "ACT_AST";
}
