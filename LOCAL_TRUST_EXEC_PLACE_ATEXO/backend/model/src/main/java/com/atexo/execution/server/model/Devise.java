package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.DEVISE)
@Getter
@Setter
public class Devise extends AbstractReferentielEntity {

    @OneToMany(mappedBy = "devise")
    private List<Plateforme> plateformes;
}
