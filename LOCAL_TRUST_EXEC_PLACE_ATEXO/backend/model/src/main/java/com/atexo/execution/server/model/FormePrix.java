package com.atexo.execution.server.model;

import com.atexo.execution.common.def.ReferentielDiscriminators;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.FORME_PRIX)
public class FormePrix extends AbstractReferentielEntity {


}
