package com.atexo.execution.server.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
@Table(name = "DOCUMENT")
public class Document extends AbstractEntity {

	String idExterne;

	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_FICHIER")
	Fichier fichier;

	LocalDateTime dateCreationUtilisateur;

	public String getIdExterne() {
		return idExterne;
	}

	public void setIdExterne(String idExterne) {
		this.idExterne = idExterne;
	}

	public Fichier getFichier() {
		return fichier;
	}

	public void setFichier(Fichier fichier) {
		this.fichier = fichier;
	}

	public LocalDateTime getDateCreationUtilisateur() {
		return dateCreationUtilisateur;
	}

	public void setDateCreationUtilisateur(LocalDateTime dateCreationUtilisateur) {
		this.dateCreationUtilisateur = dateCreationUtilisateur;
	}

	@Override
	public String toString() {
		return "Document{" +
				"idExterne='" + idExterne + '\'' +
				", fichier=" + fichier +
				'}';
	}
}
