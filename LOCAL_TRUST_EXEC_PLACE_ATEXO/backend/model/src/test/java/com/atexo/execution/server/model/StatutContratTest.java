package com.atexo.execution.server.model;

import com.atexo.execution.common.def.StatutContrat;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatutContratTest {

    @Test
    void Given_StatutArchive_Statut_Inchange() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.Archive);
        contrat.updateContratStatus();
        assertEquals(StatutContrat.Archive, contrat.getStatut());
    }

    @Test
    void Given_DateNotification_Null_Statut_A_Notifier() {
        var contrat = new Contrat();
        contrat.updateContratStatus();
        assertEquals(StatutContrat.ANotifier, contrat.getStatut());
    }

    @Test
    void Given_DateNotification_Not_Null_Statut_Notifie() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.Notifie);
        contrat.setDateNotification(LocalDate.now());
        contrat.updateContratStatus();
        assertEquals(StatutContrat.Notifie, contrat.getStatut());
    }

    @Test
    void Given_DateNotification_Not_Null_Statut_En_Cours() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.Notifie);
        contrat.setDateNotification(LocalDate.now().minusDays(1));
        contrat.setDateFinContrat(LocalDate.now().plusDays(1));
        contrat.updateContratStatus();
        assertEquals(StatutContrat.EnCours, contrat.getStatut());
    }

    @Test
    void Given_DateNotification_Not_Null_Statut_Clos() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.Notifie);
        contrat.setDateNotification(LocalDate.now().minusDays(1));
        contrat.setDateFinContrat(LocalDate.now().minusDays(1));
        contrat.updateContratStatus();
        assertEquals(StatutContrat.Clos, contrat.getStatut());
    }
}
