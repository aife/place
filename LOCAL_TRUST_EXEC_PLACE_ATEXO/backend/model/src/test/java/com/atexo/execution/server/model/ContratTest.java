package com.atexo.execution.server.model;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ContratTest {

    @Test
    void getSousTraitans() {
        var attributaire = new ContratEtablissement();
        attributaire.setId(1L);
        attributaire.setIdExterne("1");

        var sousTraitant = new ContratEtablissement();
        sousTraitant.setId(2L);
        sousTraitant.setIdExterne("2");

        var contrat = new Contrat();
        contrat.setAttributaire(attributaire);
        contrat.setContratEtablissements(Set.of(attributaire, sousTraitant));
        assertEquals(contrat.getSousTraitans().size(), 1);
        assertEquals(contrat.getSousTraitans().stream().findFirst().map(ContratEtablissement::getId).orElse(-1L), 2L);
    }
}
