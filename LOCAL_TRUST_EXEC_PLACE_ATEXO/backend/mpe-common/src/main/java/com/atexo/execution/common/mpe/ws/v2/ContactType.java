package com.atexo.execution.common.mpe.ws.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactType {

    @JsonProperty("hydra:member")
    private List<ContactMPE> contacts;
}
