package com.atexo.execution.common.mpe.ws.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactMPE {
    @JsonProperty("@id")
    private String id;
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
}
