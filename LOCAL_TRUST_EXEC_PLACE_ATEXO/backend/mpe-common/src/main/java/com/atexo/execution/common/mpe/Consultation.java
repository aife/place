package com.atexo.execution.common.mpe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Consultation {
    @JsonProperty("directionService")
    private String directionService;

    @JsonProperty("organisme")
    private String organisme;

    @JsonProperty("naturePrestation")
    private String naturePrestation;

    @JsonProperty("typeProcedure")
    private String typeProcedure;

    @JsonProperty("typeContrat")
    private String typeContrat;

    @JsonProperty("valeurEstimee")
    private Double valeurEstimee;

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("intitule")
    private String intitule;

    @JsonProperty("objet")
    private String objet;

    @JsonProperty("commentaireInterne")
    private String commentaireInterne;

    @JsonProperty("lieuxExecution")
    private List<Integer> lieuxExecution = new ArrayList<>();

    @JsonProperty("codeCpvPrincipal")
    private String codeCpvPrincipal;

    @JsonProperty("codeCpvSecondaire1")
    private String codeCpvSecondaire1;

    @JsonProperty("codeCpvSecondaire2")
    private String codeCpvSecondaire2;

    @JsonProperty("codeCpvSecondaire3")
    private String codeCpvSecondaire3;

    @JsonProperty("alloti")
    private Boolean alloti;

    @JsonProperty("referenceConsultationInit")
    private String referenceConsultationInit;
}
