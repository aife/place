package com.atexo.execution.common.mpe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ReferentielMPE {
    @JsonProperty("@type")
    private String type;

    @JsonProperty("@id")
    private String id;

    @JsonProperty("libelle")
    private String libelle;

    @JsonProperty("abreviation")
    private String abreviation;

    @JsonProperty("idExterne")
    private String idExterne;
}
