package com.atexo.execution.common.mpe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class MpeResponse {

    @JsonProperty("hydra:member")
    public List<ReferentielMPE> liste;
}
