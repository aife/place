package com.atexo.execution.common.mpe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TokenRequest {

    @JsonProperty("login")
    public String login;

    @JsonProperty("password")
    public String password;
}
