package com.atexo.execution.common.mpe;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgentMPE {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("login")
    private String login;

    @JsonProperty("email")
    private String email;

    @JsonProperty("nom")
    private String nom;

    @JsonProperty("prenom")
    private String prenom;

    @JsonProperty("telephone")
    private String telephone;

    @JsonProperty("libelleDetail")
    private String libelleDetail;

    @JsonProperty("modulesOrganisme")
    private List<String> modulesOrganisme = new ArrayList<>();

    @JsonProperty("organisme")
    private String uriOrganisme;

    @JsonProperty("service")
    private String uriService;

    @JsonAnySetter
    private Map<String, Object> dynamicValues = new LinkedHashMap<>();
}
