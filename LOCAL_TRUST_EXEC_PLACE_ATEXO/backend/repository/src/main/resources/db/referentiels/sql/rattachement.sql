-- Auvergne
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-ARA' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('01', '03', '07', '15', '26', '38', '42', '43', '63', '69', '73', '74') ;

-- Bourgogne-Franche-Comté
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-BFC' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('21','25','39','58','70','71','89','90') ;

-- Bretagne
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-BRE' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('22','29','35','56') ;

-- Centre-Val de Loire
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-CVL' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('18','28','36','37','41','45') ;

-- Corse
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-COR' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('2A','2B') ;

-- Grand Est
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-GES' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('08','10','51','52','54','55','57','67','68','88') ;

-- Hauts-de-France
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-HDF' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('02','59','60','62','80') ;

-- Île-de-France
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-IDF' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('75','77','78','91','92','93','94','95') ;

-- Normandie
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-NOR' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('14','27','50','61','76') ;

-- Nouvelle-Aquitaine
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-NAQ' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87') ;

-- Occitanie
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-OCC' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('09', '11', '12', '30', '31', '32', '34', '46', '48', '65', '66', '81', '82') ;

-- Pays de la Loire
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-PDL' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('44', '49', '53', '72', '85') ;

-- Provence-Alpes-Côte d'Azur
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-PAC' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('04', '05', '06', '13', '83', '84') ;

-- Guadeloupe
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-GUA' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code in ('971', '977', '978') ;

-- Martinique
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-MTQ' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '972' ;

-- Guyane
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-GUF' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '973' ;

-- La Réunion
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-LRE' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '974' ;

-- Saint-Pierre-et-Miquelon';
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-SPM' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '975' ;

-- Mayotte
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-MAY' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '976' ;

-- Nouvelle calédonie
UPDATE referentiel
SET id_groupe=(SELECT grp.id FROM referentiel grp WHERE grp.type_referentiel = 'REGION' and grp.code = 'FR-NVC' LIMIT 1)
WHERE type_referentiel = 'LIEU_EXECUTION' and code = '988' ;
