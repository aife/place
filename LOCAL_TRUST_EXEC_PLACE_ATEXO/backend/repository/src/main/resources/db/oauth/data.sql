INSERT IGNORE INTO oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity) VALUES
('lt-execution-default', 'resource-server-rest-api', /*lt-execution-webapp-s3cr3t en bcrypt*/'$2y$10$Mg.DJqEa1J7WW6e6W7ip1uYaMrEjlKK8PujG.qJ4r8yKmld7T1kY6', 'read,write', 'password,authorization_code,refresh_token,implicit', 'VOIR_TOUS_CONTRATS,MODIF_DONNEES_CONTRAT,CREER_CONTRAT,MODIF_MONTANTS_CONTRAT,VOIR_CONTRATS_SERVICE,VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES', 10800, 2592000),
('lt-execution-webapp', 'resource-server-rest-api',  /*lt-execution-webapp-s3cr3t en bcrypt*/'$2y$10$Mg.DJqEa1J7WW6e6W7ip1uYaMrEjlKK8PujG.qJ4r8yKmld7T1kY6', 'read', 'password,client_credentials', 'VOIR_TOUS_CONTRATS,MODIF_DONNEES_CONTRAT,CREER_CONTRAT,MODIF_MONTANTS_CONTRAT,VOIR_CONTRATS_SERVICE,VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES', 10800, 2592000);

INSERT IGNORE INTO oauth_user(id, user_name, password, account_expired, account_locked, credentials_expired, enabled) VALUES
(1, 'admin', /*admin1234*/'$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha', FALSE, FALSE, FALSE, TRUE),
(2, 'reader', /*reader1234*/'$2a$08$dwYz8O.qtUXboGosJFsS4u19LHKW7aCQ0LXXuNlRfjjGKwj5NfKSe', FALSE, FALSE, FALSE, TRUE),
(3, 'modifier', /*modifier1234*/'$2a$08$kPjzxewXRGNRiIuL4FtQH.mhMn7ZAFBYKB3ROz.J24IX8vDAcThsG', FALSE, FALSE, FALSE, TRUE);

INSERT IGNORE INTO oauth_authority(id, id_externe, name) VALUES
(1, 'CREERCONTRAT', 'CREER_CONTRAT'),
(2, 'EXECVOIRCONTRATSORGANISME', 'VOIR_TOUS_CONTRATS'),
(3, 'GERERCONTRAT', 'MODIF_DONNEES_CONTRAT'),
(4, 'GERERMONTANTCONTRAT', 'MODIF_MONTANTS_CONTRAT'),
(5, 'EXECVOIRCONTRATSEA', 'VOIR_CONTRATS_SERVICE'),
(6, 'EXECVOIRCONTRATSEADEPENDANTES', 'VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES');

INSERT IGNORE INTO oauth_users_authorities(user_id, authority_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6);
