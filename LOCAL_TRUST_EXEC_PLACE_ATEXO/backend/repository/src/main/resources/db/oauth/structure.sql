CREATE TABLE IF NOT EXISTS oauth_client_details
(
    client_id               VARCHAR(256) PRIMARY KEY,
    resource_ids            VARCHAR(256),
    client_secret           VARCHAR(256),
    scope                   VARCHAR(256),
    authorized_grant_types  VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities             VARCHAR(256),
    access_token_validity   INTEGER,
    refresh_token_validity  INTEGER,
    additional_information  VARCHAR(4096),
    autoapprove             VARCHAR(256)
);


CREATE TABLE IF NOT EXISTS oauth_client_token
(
    token_id          VARCHAR(256),
    token             BLOB,
    authentication_id VARCHAR(256) PRIMARY KEY,
    user_name         VARCHAR(256),
    client_id         VARCHAR(256)
);


CREATE TABLE IF NOT EXISTS oauth_access_token
(
    token_id          VARCHAR(256),
    token             BLOB,
    authentication_id VARCHAR(256) PRIMARY KEY,
    user_name         VARCHAR(256),
    client_id         VARCHAR(256),
    authentication    BLOB,
    refresh_token     VARCHAR(256)
);


CREATE TABLE IF NOT EXISTS oauth_refresh_token
(
    token_id       VARCHAR(256),
    token          BLOB,
    authentication BLOB
);


CREATE TABLE IF NOT EXISTS oauth_code
(
    code           VARCHAR(256),
    authentication BLOB
);

CREATE TABLE IF NOT EXISTS oauth_approvals
(
    userId         VARCHAR(256),
    clientId       VARCHAR(256),
    scope          VARCHAR(256),
    status         VARCHAR(10),
    expiresAt      TIMESTAMP,
    lastModifiedAt TIMESTAMP
);


CREATE TABLE IF NOT EXISTS oauth_user
(
    id                  BIGINT AUTO_INCREMENT PRIMARY KEY,
    account_expired     bit          NULL,
    account_locked      bit          NULL,
    credentials_expired bit          NULL,
    enabled             bit          NULL,
    password            VARCHAR(255) NULL,
    user_name           VARCHAR(255) NULL,
    CONSTRAINT UKhdtcmn0nu2nccvrnstyek27cf UNIQUE (user_name)
);

CREATE TABLE IF NOT EXISTS oauth_authority
(
    id   BIGINT AUTO_INCREMENT PRIMARY KEY,
    id_externe VARCHAR(255) NOT NULL,
    name VARCHAR(255) NULL,
    CONSTRAINT UKqaxbb2s2frf9nnuvs707g3dn4 UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS oauth_users_authorities
(
    user_id      BIGINT NOT NULL,
    authority_id BIGINT NOT NULL,
    CONSTRAINT FK9duhk4u306qyn36wk83iw101w FOREIGN KEY (user_id) REFERENCES oauth_user (id),
    CONSTRAINT FKakw06uj5i0vgb6d9kd334vu8a FOREIGN KEY (authority_id) REFERENCES oauth_authority (id)
);
