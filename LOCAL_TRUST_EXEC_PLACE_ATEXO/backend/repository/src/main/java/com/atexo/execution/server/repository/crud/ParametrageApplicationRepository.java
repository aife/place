package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ParametrageApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParametrageApplicationRepository extends JpaRepository<ParametrageApplication, Long> {

    List<ParametrageApplication> findByExportFrontTrueAndPlateformeMpeUid(String mpePfuid);

    Optional<ParametrageApplication> findByClefAndPlateformeMpeUid(String cle, String mpePfuid);

}
