package com.atexo.execution.server.repository.referentiels.clause;


import com.atexo.execution.server.model.clause.ClauseN1;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClauseN1Repository extends ReferentielRepository<ClauseN1> {
}
