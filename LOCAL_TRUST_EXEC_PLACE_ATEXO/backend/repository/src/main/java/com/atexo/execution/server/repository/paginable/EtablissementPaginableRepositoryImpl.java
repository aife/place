package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.repository.paginable.criteres.EtablissementCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.function.Function;

import static com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository.*;
import static org.springframework.util.ObjectUtils.isEmpty;

@Repository
public class EtablissementPaginableRepositoryImpl extends BasePaginableRepository<Etablissement, EtablissementCriteria> implements EtablissementPaginableRepository {

	static final Logger LOG = LoggerFactory.getLogger(EtablissementPaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
			EtablissementCriteria criteria,
			CriteriaQuery<V> query,
			Function<Expression<Etablissement>, Expression<V>> projection,
			Sort sort
	) {
		final var builder = em.getCriteriaBuilder();
		final var root = query.from(Etablissement.class);

		query.select(projection.apply(root));

		final var predicates = new HashSet<Predicate>();

		if (!isEmpty(criteria.getMotsCles())) {
			for (var token : criteria.getMotsCles().toLowerCase().split(SPACE_PATTERN)) {
				var expression = MessageFormat.format("%{0}%", token);
				LOG.debug("Recherche d'établissements sur l'expression : '{}'", expression);
				predicates.add(
						builder.or(
								builder.like(builder.lower(root.get(SIRET)), expression),
								builder.like(builder.lower(root.join(FOURNISSEUR).get(SIREN)), expression),
								builder.like(builder.lower(root.join(FOURNISSEUR).get(DESCRIPTION)), expression),
								builder.like(builder.lower(root.join(FOURNISSEUR).get(RAISON_SOCIALE)), expression)
						)
				);
			}
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null != sort) {
			query.orderBy(super.order(sort, builder, root));
		}

		return em.createQuery(query);
	}

	@Override
	public Class<Etablissement> getTarget() {
		return Etablissement.class;
	}

}
