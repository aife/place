package com.atexo.execution.server.repository.paginable.criteres;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EvenementCriteria {
    Long contrat;
    String motsCles;
    Boolean etatNonSuivi;
    Boolean etatValide;
    Boolean etatRejete;
    Boolean etatAVenir;
    Boolean etatEnAttente;
    Long[] contractants;
    LocalDate dateEnvoiDebut;
    LocalDate dateEnvoiFin;
    String[] evenementTypeCode;
    Boolean bonsCommande;
    Boolean avenants;
}
