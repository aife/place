package com.atexo.execution.server.repository.paginable.criteres;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.StatutRenouvellement;
import com.atexo.execution.server.model.CategorieConsultation;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.model.Utilisateur;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ContratCriteriaAppach {
	private Utilisateur utilisateur;

	private List<String> identifiantsAgent;
	private List<String> libellesService;
	private List<String> acronymesOrganisme;
	private String siretAttributaire;
	private List<StatutRenouvellement> statutsRenouvellement;
	private Integer numeroLot;
	private LocalDateTime dateModificationActe;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateDebutRenouvellementMin;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateDebutRenouvellementMax;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateMaxFinContratMin;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateMaxFinContratMax;

	private LocalDateTime dateModificationMin;

	private LocalDateTime dateModificationMax;

	private String motsCles;

	private PerimetreContrat perimetre;

	private StatutContrat[] statuts;

	private Boolean favoris;

	private Boolean transverse;

	private String codeAchat;

	private String uniteFonctionnelleOperation;
	private List<TypeContrat> typesContrat;
	private List<CategorieConsultation> categories;
	private Boolean chapeau;
	private Boolean considerationsEnvironnementales;
	private Boolean considerationsSociales;
	private LocalDate[] dateNotification;
	private LocalDate[] datePrevisionnelleNotification;
	private LocalDate[] dateFinContrat;
	private LocalDate[] dateMaxFinContrat;
	private List<String> codesExternesTypeContrat;
	private List<String> codesCategorie;
	private List<String> idExternesConsultation;
	private List<Integer> idExternesOffre;
	private String mpePfUid;
    private Integer idContratTitulaire;
    private List<Integer> idContratTitulaires = Collections.emptyList();
    private String numeroContrat;
    private List<String> numeroContrats = Collections.emptyList();
    private StatutContrat statutContrat;
    private List<StatutContrat> statutContrats = Collections.emptyList();
    private LocalDateTime dateModification;
    private String objetContrat;
    private String intitule;
    private Long idConsultation;
    private Integer idLot;
	private Boolean defenseOuSecurite;
    private LocalDate dateNotificationMin;
    private LocalDate dateNotificationMax;
    private String siren;
    private String complement;
    private Integer statutPublicationSn;
    private List<String> exclusionOrganisme;
    private List<Long> idServices;
	private List<Integer> statutContratAppach;
}
