package com.atexo.execution.server.repository.referentiels.clause;


import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClauseRepository extends ReferentielRepository<Clause> {
}
