package com.atexo.execution.server.repository.referentiels;


import com.atexo.execution.server.model.TauxTVA;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TauxTVARepository extends ReferentielRepository<TauxTVA> {
    List<TauxTVA> findAll();
}
