package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CPV;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CPVRepository extends ReferentielRepository<CPV> {

    TypeContrat findOneByIdExterne(String idExterne);

}
