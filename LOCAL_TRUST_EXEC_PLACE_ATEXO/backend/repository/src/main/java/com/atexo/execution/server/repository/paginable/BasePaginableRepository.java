package com.atexo.execution.server.repository.paginable;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class BasePaginableRepository<U, T> implements PaginableRepository<U, T> {

    public static final String ATTRIBUT = "[a-zA-Z.]*";

    public static final String SPACE_PATTERN = "\\s|\\t";
    private static final Logger LOG = LoggerFactory.getLogger(BasePaginableRepository.class);
    @PersistenceContext
    protected EntityManager em;

    @Override
    public List<U> rechercher(final T criteria) {
        final var builder = em.getCriteriaBuilder();
        final var query = builder.createQuery(this.getTarget());
        query.distinct(true);
        final var data = this.requeter(criteria, query, Function.identity(), null);

        return data.getResultList();
    }

    @Override
    public Page<U> rechercher(final T criteria, final Pageable pageable) {
        final var builder = em.getCriteriaBuilder();
        final var query = builder.createQuery(this.getTarget());
        query.distinct(true);
        final var data = this.requeter(criteria, query, Function.identity(), pageable.getSort());

        data.setFirstResult(Math.toIntExact(pageable.getOffset()));
        data.setMaxResults(pageable.getPageSize());

        final var resultList = data.getResultList();

        return new PageImpl<>(resultList, pageable, this.compter(criteria));
    }

    protected List<Order> order(Sort sort, CriteriaBuilder builder, From<?, ?> from) {
        return sort.stream()
                .map(
                        order -> {
                            Order result = null;

                            if (order.getProperty().matches(ATTRIBUT)) {
                                result = travel(from, order.getProperty()).map(
                                        path -> new OrderApplier(
                                                order.isAscending() ? builder::asc : builder::desc,
                                                path
                                        ).apply()
                                ).orElse(null);
                            } else {
                                LOG.warn("Impossible de trier sur la propriété = {}", order.getProperty());
                            }

                            return result;
                        }

                )
                .collect(Collectors.toList());
    }

    private Optional<Path<?>> travel(From<?, ?> from, String property) {
        var tokens = Arrays.stream(property.split("\\."))
                .filter(s -> !Strings.isNullOrEmpty(s))
                .collect(Collectors.toCollection(ArrayDeque::new));

        From<?, ?> temp = from;
        Path<?> result = null;

        do {
            var token = tokens.pop();

            if (tokens.isEmpty()) {
                result = temp.get(token);
            } else {
                temp = temp.join(token, JoinType.LEFT);
            }
        } while (!tokens.isEmpty());

        return Optional.ofNullable(result);
    }

    @Override
    public Long compter(final T criteria) {
        final var builder = em.getCriteriaBuilder();
        final var query = builder.createQuery(Long.class);
        final var contrats = this.requeter(criteria, query, builder::count, null);

        return contrats.getSingleResult();
    }

    private static class OrderApplier {

        private final Function<Expression<?>, Order> function;
        private final Path<?> path;

        public OrderApplier(Function<Expression<?>, Order> orderFunction, Path<?> pathToOrder) {
            this.function = orderFunction;
            this.path = pathToOrder;
        }

        public Order apply() {
            return this.function.apply(this.path);
        }
    }

}
