package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.ModaliteExecution;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ModaliteExecutionRepository extends ReferentielRepository<ModaliteExecution> {
    Optional<ModaliteExecution> findFirstByLabel(String label);
}
