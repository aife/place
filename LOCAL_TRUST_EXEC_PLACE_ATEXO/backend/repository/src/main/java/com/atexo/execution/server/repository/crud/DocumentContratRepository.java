package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.dto.edition_en_ligne.FileStatusEnum;
import com.atexo.execution.server.model.DocumentContrat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentContratRepository extends JpaRepository<DocumentContrat, Long> {

    Optional<DocumentContrat> findByIdAndContratServiceOrganismeId(Long id, Long organismeId);

    List<DocumentContrat> findAllByStatutInAndDateModificationEditionIsNotNull(List<FileStatusEnum> statusEnums);
}
