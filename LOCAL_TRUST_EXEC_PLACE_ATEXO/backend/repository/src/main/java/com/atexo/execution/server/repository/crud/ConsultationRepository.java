package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConsultationRepository extends JpaRepository<Consultation, Long> {

    Optional<Consultation> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);
}
