package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.MiseADisposition;
import com.atexo.execution.server.model.StatutMAP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MiseADispositionRepository extends JpaRepository<MiseADisposition, Long> {
    Optional<MiseADisposition> findById(Long id);

    List<MiseADisposition> findAllByStatutMAPIs(StatutMAP statutMAP);
}
