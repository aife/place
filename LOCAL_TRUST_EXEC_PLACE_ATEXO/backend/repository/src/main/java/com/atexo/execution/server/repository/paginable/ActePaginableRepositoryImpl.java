package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.repository.paginable.criteres.ActeCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.function.Function;

@Repository
public class ActePaginableRepositoryImpl extends BasePaginableRepository<Acte, ActeCriteria> implements ActePaginableRepository {

	private static final Logger LOG = LoggerFactory.getLogger(ActePaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
		ActeCriteria criteria,
		CriteriaQuery<V> query,
		Function<Expression<Acte>, Expression<V>> projection,
		Sort sort
	) {
		LOG.info("Recherche d'acte avec les critères suivants : {}", criteria);

		final var builder = em.getCriteriaBuilder();
		final From<?, Acte> cursor;

		final var predicates = new HashSet<Predicate>();

		final var root = query.from(Acte.class);
		query.select(projection.apply(root));

		cursor = root;

		if ( null != criteria.getMotsCles() && !criteria.getMotsCles().isEmpty() ) {
			var expression = MessageFormat.format("%{0}%", criteria.getMotsCles().toLowerCase());

			LOG.debug("Recherche d'actes sur l'expression : '{}'", expression);

			predicates.add(
				builder.or(
						builder.like(builder.lower(cursor.get(OBJET)), expression),
						builder.like(builder.lower(cursor.get(NUMERO)), expression),
						builder.like(builder.lower(cursor.join(ACHETEUR).get(NOM)), expression),
						builder.like(builder.lower(cursor.join(ACHETEUR).get(PRENOM)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).get(SIRET)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).join(FOURNISSEUR).get(SIREN)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).join(FOURNISSEUR).get(NOM)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).join(FOURNISSEUR).get(PRENOM)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).join(FOURNISSEUR).get(DESCRIPTION)), expression),
						builder.like(builder.lower(cursor.join(TITULAIRE).join(FOURNISSEUR).get(RAISON_SOCIALE)), expression)
				)
			);
		}

		if ( null != criteria.getObjet() ) {
			predicates.add(builder.equal(cursor.get(OBJET), criteria.getObjet()));
		}

		if ( null != criteria.getNumero() ) {
			predicates.add(builder.equal(cursor.get(NUMERO), criteria.getNumero()));
		}

		if ( criteria.getTypes() != null && !criteria.getTypes().isEmpty() ) {
			predicates.add(cursor.get(TYPE).in(criteria.getTypes()));
		}

		if ( criteria.getStatuts() != null && !criteria.getStatuts().isEmpty() ) {
			predicates.add(cursor.get(STATUT).in(criteria.getStatuts()));
		}

		if ( null != criteria.getContrat() ) {
			predicates.add(builder.equal(cursor.get(CONTRAT), criteria.getContrat()));
		}

		query.where(predicates.toArray(Predicate[]::new));

		if ( null != sort ) {
			query.orderBy(super.order(sort, builder, cursor));
		}
		query.distinct(true);
		return em.createQuery(query);
	}

	@Override
	public Class<Acte> getTarget() {
		return Acte.class;
	}
}
