package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.Procedure;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProcedureRepository extends ReferentielRepository<Procedure> {
    Optional<Procedure> findFirstByCode(String code);
}
