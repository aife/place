package com.atexo.execution.server.repository.paginable.criteres;

import com.atexo.execution.common.def.StatutContrat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ContratEtablissementCriteria {
    private Long idOrganisme;
    private Long idFournisseur;
    private String motsCles;
    private Long organisme;
    private StatutContrat[] statuts;
    private List<String> sirets;
    private Integer idExterneContratTitulaire;
    private List<Integer> idExterneContratTitulaires;
    private String numeroContrat;
    private List<String> numeroContrats;
    private StatutContrat statutContrat;
    private List<StatutContrat> statutContrats;
    private LocalDateTime dateModification;
    private String objetContrat;
    private String intitule;
    private Long idConsultation;
    private Integer idLot;
    private boolean defenseOuSecurite;
    private LocalDateTime dateNotificationMin;
    private LocalDateTime dateNotificationMax;
    private String siren;
    private String complement;
    private Integer statutPublicationSn;
    private List<String> exclusionOrganisme;
    private List<Long> idServices;
    private String pfUid;
    private boolean chapeau;
}
