package com.atexo.execution.server.repository.crud.data.gouv;

import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.data.gouv.ContratPublication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ContratPublicationRepository extends JpaRepository<ContratPublication, Long> {

    @Query("select count( distinct cp.datePublication ) from  ContratPublication  cp join cp.donneesEssentielles de  join Contrat c on c.donneesEssentielles = de join c.service s where s.organisme = :organisme  and DATE(cp.datePublication) = :currentDate and cp.message = :#{#statut.name()}")
    long countByOrganismeAndDatePublication(@Param("organisme") Organisme organisme, @Param("currentDate") Date currentDate, @Param("statut") StatutPublicationDE statut);

    @Query("select count( distinct cp.datePublication ) from  ContratPublication  cp join cp.donneesEssentielles de  join Contrat c on c.donneesEssentielles = de join c.service s where s.organisme = :organisme and cp.message = :#{#statut.name()}")
    long countByOrganisme(@Param("organisme") Organisme organisme, @Param("statut") StatutPublicationDE statut);

}
