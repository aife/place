package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CodeApeNaf;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CodeApeNafRepository extends ReferentielRepository<CodeApeNaf> {

    CodeApeNaf findOneByIdExterne(String idExterne);
}
