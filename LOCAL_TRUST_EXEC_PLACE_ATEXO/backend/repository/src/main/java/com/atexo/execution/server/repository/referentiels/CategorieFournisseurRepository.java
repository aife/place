package com.atexo.execution.server.repository.referentiels;


import com.atexo.execution.server.model.CategorieFournisseur;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategorieFournisseurRepository extends ReferentielRepository<CategorieFournisseur> {

    Optional<CategorieFournisseur> findFirstByCode(String code);

}
