package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.DocumentContrat;
import com.atexo.execution.server.repository.paginable.criteres.DocumentCriteria;

public interface DocumentPaginableRepository extends PaginableRepository<DocumentContrat, DocumentCriteria> {
}
