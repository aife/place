package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ParametrageNumerotationChamp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParametrageNumerotationChampRepository extends JpaRepository<ParametrageNumerotationChamp, Long> {

    List<ParametrageNumerotationChamp> findAllByCompteur(boolean compteur);

}
