package com.atexo.execution.server.repository.referentiels;


import com.atexo.execution.server.model.SurchargeLibelle;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurchargeLibelleRepository extends ReferentielRepository<SurchargeLibelle> {
    List<SurchargeLibelle> findAll();
}
