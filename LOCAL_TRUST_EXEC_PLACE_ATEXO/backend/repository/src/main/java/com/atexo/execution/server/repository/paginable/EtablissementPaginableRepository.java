package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.repository.paginable.criteres.EtablissementCriteria;


public interface EtablissementPaginableRepository extends PaginableRepository<Etablissement, EtablissementCriteria> {
    String SIRET = "siret";
}
