package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.TypePrix;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TypePrixRepository extends ReferentielRepository<TypePrix> {
    Optional<TypePrix> findFirstByLabel(String label);
}
