package com.atexo.execution.server.customLiquibase;

import liquibase.Scope;
import liquibase.change.custom.CustomSqlChange;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;

@Slf4j
public class PfUid extends AbstractChangeLog implements CustomTaskChange, CustomSqlChange {
    @SuppressWarnings({"UnusedDeclaration", "FieldCanBeLocal"})
    private ResourceAccessor resourceAccessor;

    @Override
    public void execute(Database database) {
        Scope.getCurrentScope().getLog(getClass()).info("Setting uid and mpe_uid for PLATEFORME");
    }
    @Override
    public String getConfirmationMessage() {
        return "uid and mpe_uid have been set in PLATEFORME";
    }
    @Override
    public void setUp() {
        ;
    }
    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        this.resourceAccessor = resourceAccessor;
    }
    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }



    @Override
    public SqlStatement[] generateStatements(Database database) {
        var pfUid = getProperties().getProperty("pf.uid");
        var mpePfUid = getProperties().getProperty("mpe.uid");
        var mpeUrl = getProperties().getProperty("mpe.url");
        var mpeApiLogin = getProperties().getProperty("mpe.api.login");
        var mpeApiPassword = getProperties().getProperty("mpe.api.password");
        log.info("pfUid: {}, mpePfUid: {}", pfUid, mpePfUid);
        String insertSql = MessageFormat.format(
                "INSERT IGNORE INTO PLATEFORME (uid, mpe_uid,mpe_url,mpe_api_login,mpe_api_password) VALUES (''{0}'',''{1}'',''{2}'',''{3}'',''{4}'');", pfUid, mpePfUid, mpeUrl, mpeApiLogin, mpeApiPassword);
        log.debug("insertSql: {}", insertSql);
        return new SqlStatement[]{new RawSqlStatement(insertSql)};
    }


}
