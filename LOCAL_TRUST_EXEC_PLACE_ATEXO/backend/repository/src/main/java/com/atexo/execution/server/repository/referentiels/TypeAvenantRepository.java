package com.atexo.execution.server.repository.referentiels;


import com.atexo.execution.server.model.TypeAvenant;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeAvenantRepository extends ReferentielRepository<TypeAvenant> {
    List<TypeAvenant> findAll();
}
