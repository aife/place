package com.atexo.execution.server.repository.crud.contrat.clause;


import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratClause;
import com.atexo.execution.server.model.clause.Clause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratClauseRepository extends JpaRepository<ContratClause, Long> {

    Optional<ContratClause> findByContratAndClause(Contrat contrat, Clause clause);
    List<ContratClause> findByContrat(Contrat contrat);

    void deleteByContrat(Contrat contrat);
}
