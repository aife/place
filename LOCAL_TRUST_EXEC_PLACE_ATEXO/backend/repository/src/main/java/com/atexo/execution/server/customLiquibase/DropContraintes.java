package com.atexo.execution.server.customLiquibase;

import liquibase.Scope;
import liquibase.change.custom.CustomSqlChange;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j
public class DropContraintes implements CustomTaskChange, CustomSqlChange {

    @Override
    public void execute(Database database) {
        Scope.getCurrentScope().getLog(getClass()).info("Suppression des contraintes");
    }

    @Override
    public String getConfirmationMessage() {
        return "Constraintes dropped successfully.";
    }

    @Override
    public void setUp() {
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }


    @Override
    public SqlStatement[] generateStatements(Database database) {
        var statement = "select concat('alter table ',tcs.table_name,' drop constraint if exists ',CONSTRAINT_NAME,';') as requete\n" +
                "from information_schema.table_constraints tcs\n" +
                "join information_schema.COLUMNS columns on tcs.table_name = columns.table_name\n" +
                "where constraint_type = 'UNIQUE' and (COLUMN_NAME='id_externe' OR COLUMN_NAME='USER_NAME' ) and tcs.TABLE_SCHEMA like '%exec%';";
        JdbcConnection jdbcCOnnection = (JdbcConnection) database.getConnection();
        var dropStatement = new ArrayList<SqlStatement>();
        try {
            var selectResult = jdbcCOnnection.createStatement().executeQuery(statement);
            while (selectResult.next()) {
                dropStatement.add(new RawSqlStatement(selectResult.getString("requete")));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return dropStatement.toArray(new SqlStatement[0]);
    }

}
