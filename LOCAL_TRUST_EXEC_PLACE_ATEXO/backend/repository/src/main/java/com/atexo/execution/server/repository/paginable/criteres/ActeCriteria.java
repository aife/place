package com.atexo.execution.server.repository.paginable.criteres;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.model.Utilisateur;

import java.util.Set;

public class ActeCriteria {
	private final String motsCles;

	private final String objet;

	private final String numero;

	private final Set<TypeActe> types;

	private final Set<StatutActe> statuts;

	private final Utilisateur utilisateur;

	private final Contrat contrat;

	public ActeCriteria( String motsCles, String objet, String numero, Set<TypeActe> types, Set<StatutActe> statuts, Utilisateur utilisateur, Contrat contrat ) {
		this.motsCles = motsCles;
		this.objet = objet;
		this.numero = numero;
		this.types = types;
		this.statuts = statuts;
		this.contrat = contrat;
		this.utilisateur = utilisateur;
	}

	public String getMotsCles() {
		return motsCles;
	}

	public String getObjet() {
		return objet;
	}

	public String getNumero() {
		return numero;
	}

	public Set<TypeActe> getTypes() {
		return types;
	}

	public Set<StatutActe> getStatuts() {
		return statuts;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public static class Builder {
		private String motsCles;

		private String objet;

		private String numero;

		private Set<TypeActe> types;

		private Set<StatutActe> statuts;

		private Utilisateur utilisateur;

		private Contrat contrat;

		public Builder motsCles( String motsCles ) {
			this.motsCles = motsCles;

			return this;
		}

		public Builder objet( String objet ) {
			this.objet = objet;

			return this;
		}

		public Builder numero( String numero ) {
			this.numero = numero;

			return this;
		}

		public Builder type( Set<TypeActe> types ) {
			this.types = types;

			return this;
		}

		public Builder statuts( Set<StatutActe> statuts ) {
			this.statuts = statuts;

			return this;
		}

		public Builder utilisateur( Utilisateur utilisateur ) {
			this.utilisateur = utilisateur;

			return this;
		}

		public Builder contrat( Contrat contrat ) {
			this.contrat = contrat;

			return this;
		}

		public ActeCriteria build() {
			return new ActeCriteria(motsCles, objet, numero, types, statuts, utilisateur, contrat);
		}
	}

	@Override
	public String toString() {
		return
			"mots clés = '" + motsCles + "'" +
				", objet = '" + objet + "'" +
				", numéro = '" + numero + "'" +
				", types = " + types +
				", statuts = " + statuts +
				", contrat = " + contrat;
	}
}
