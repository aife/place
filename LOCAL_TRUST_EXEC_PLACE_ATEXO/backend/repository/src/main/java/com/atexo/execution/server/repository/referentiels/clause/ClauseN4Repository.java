package com.atexo.execution.server.repository.referentiels.clause;


import com.atexo.execution.server.model.clause.ClauseN4;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClauseN4Repository extends ReferentielRepository<ClauseN4> {
}
