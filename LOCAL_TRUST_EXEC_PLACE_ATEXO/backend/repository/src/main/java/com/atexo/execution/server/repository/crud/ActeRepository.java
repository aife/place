package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.actes.ActeModificatif;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ActeRepository extends JpaRepository<Acte, Long> {
	<T extends Acte> Set<T> findByContratId(Long id);

	<T extends Acte> Optional<T> findByUuid(String uuid);

	<T extends Acte> Optional<T> findFirstByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

	@Query("select distinct a from ActeModificatif a " +
			"join a.contrat c " +
			"join a.donneesEssentielles de " +
			"join c.plateforme pf " +
			"where a.publicationDonneesEssentielles = true " +
			"and pf.active = true " +
			"and de.statut = :statut " +
			"and de.modeEnvoiDonneesEssentielles = :modeEnvoi " +
			"and a.dateNotification <= current_date() " +
			"and a.statut = :statutActe ")
	Page<ActeModificatif> findByActesToPublish(@Param("statut") StatutPublicationDE statut, @Param("statutActe") StatutActe statutActe, @Param("modeEnvoi") ModeEnvoiDonneesEssentielles modeEnvoiDonneesEssentielles, Pageable pageable);

	<T extends Acte> Optional<T> findByEchangeChorusIdExterneAndPlateformeMpeUid(String idExterne,  String plateformeUid);

	<T extends Acte> Optional<T> findByEchangeChorusIdAndPlateformeMpeUid(Long idEchange,  String plateformeUid);
}
