package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CodeExterneActe;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeExterneActeRepository extends ReferentielRepository<CodeExterneActe> {
}
