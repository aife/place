package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    Optional<Utilisateur> findOneByIdentifiant(String identifiant);

    Optional<Utilisateur> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<Utilisateur> findByEmailIn(List<String> emails);

    List<Utilisateur> findByServiceIdAndPlateformeIdOrderByNomAsc(Long serviceId,Long plateformeId);

    List<Utilisateur> findByActif(boolean actif);

    List<Utilisateur> findByEmail(String value);

    Optional<Utilisateur> findFirstByNomAndPrenom(String nom, String prenom);

	Optional<Utilisateur>  findByUuid( String uuid );

    Optional<Utilisateur> findByUuidAndPlateformeMpeUid(String uuid, String plateformeUid);
}
