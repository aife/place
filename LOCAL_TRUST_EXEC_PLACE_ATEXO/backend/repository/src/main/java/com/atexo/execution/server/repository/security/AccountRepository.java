package com.atexo.execution.server.repository.security;

import com.atexo.execution.server.model.security.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	Optional<Account> findByUsername( String username);

    Optional<Account> findFirstByUsernameAndPlateformeMpeUidOrderByIdDesc(String username, String plateformeUid);
}
