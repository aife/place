package com.atexo.execution.server.repository.security;

import com.atexo.execution.server.model.security.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

	Optional<Authority> findByName(String name);

	Optional<Authority> findByIdExterne(String idExterne);

}
