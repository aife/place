package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.MiseADispositionHistorique;
import com.atexo.execution.server.model.TypeDemandeMAD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MADHistoriqueRepository extends JpaRepository<MiseADispositionHistorique, Long> {
    Optional<MiseADispositionHistorique> findById(Long id);

    List<MiseADispositionHistorique> findByStatut(Boolean statut);

    List<MiseADispositionHistorique> findByTypeDemande(TypeDemandeMAD typeDemandeMAD);
}
