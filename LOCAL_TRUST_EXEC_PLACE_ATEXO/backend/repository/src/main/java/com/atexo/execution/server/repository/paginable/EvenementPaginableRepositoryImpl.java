package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.def.EtatEvenement;
import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.repository.paginable.criteres.EvenementCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.util.ObjectUtils.isEmpty;

@Repository
public class EvenementPaginableRepositoryImpl extends BasePaginableRepository<Evenement, EvenementCriteria> implements EvenementPaginableRepository {

	private static final Logger LOG = LoggerFactory.getLogger(EvenementPaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
		final EvenementCriteria criteria,
		final CriteriaQuery<V> query,
		final Function<Expression<Evenement>, Expression<V>> projection,
		final Sort sort
	) {
		final var builder = em.getCriteriaBuilder();
		final var root = query.from(Evenement.class);

		query.select(projection.apply(root));

		final var predicates = new HashSet<Predicate>();

		if (!isEmpty(criteria.getMotsCles())) {
			for (var token : criteria.getMotsCles().toLowerCase().split(SPACE_PATTERN)) {
				var expression = MessageFormat.format("%{0}%", token);
				LOG.debug("Recherche evenement sur l'expression : '{}'", expression);
				predicates.add(
						builder.or(
                                builder.like(builder.lower(root.get("libelle")), expression),
                                builder.like(builder.lower(root.get("commentaire")), expression),
                                builder.like(builder.lower(root.get("avenantType").get("numero")), expression),
                                builder.like(builder.lower(root.get("bonCommandeType").get("numero")), expression)
                        )
				);
			}
		}

		if (null != criteria.getContrat()) {
			predicates.add(builder.equal(root.join(CONTRAT).get("id"), criteria.getContrat()));
		}
		if (Boolean.TRUE.equals(criteria.getEtatNonSuivi())) {
			predicates.add(builder.isFalse(root.get(SUIVI_REALISATION)));
		}

		if (Boolean.TRUE.equals(criteria.getEtatAVenir())) {
			predicates.add(builder.and(
					builder.greaterThan(root.get(DATE_DEBUT), LocalDate.now())
			));
		}
		if (Boolean.TRUE.equals(criteria.getEtatEnAttente())) {
			predicates.add(builder.isNull(root.get(ETAT_EVENEMENT)));
			predicates.add(builder.isTrue(root.get(SUIVI_REALISATION)));
			predicates.add(builder.lessThanOrEqualTo(root.get(DATE_DEBUT), LocalDate.now()));
		}
		var etats = new HashSet<EtatEvenement>();
		if (Boolean.TRUE.equals(criteria.getEtatValide())) {
			etats.add(EtatEvenement.VALIDE);
		}
		if (Boolean.TRUE.equals(criteria.getEtatRejete())) {
			etats.add(EtatEvenement.REJETE);
		}
		if (!etats.isEmpty()) {
			predicates.add(root.get(ETAT_EVENEMENT).in(etats));
		}

        if (criteria.getContractants() != null && criteria.getContractants().length > 0) {
			predicates.add(root.join("contractant").get("id").in(criteria.getContractants()));
        }
		if (criteria.getEvenementTypeCode() != null && criteria.getEvenementTypeCode().length > 0) {
			predicates.add(root.join("typeEvenement").get("code").in(criteria.getEvenementTypeCode()));
        }
		if (criteria.getDateEnvoiDebut() != null && criteria.getDateEnvoiFin() != null) {
			predicates.add(builder.greaterThan(root.get(DATE_DEBUT), criteria.getDateEnvoiDebut()));
			predicates.add(builder.lessThan(root.get(DATE_FIN), criteria.getDateEnvoiFin()));
        }

		if(Boolean.TRUE.equals(criteria.getBonsCommande())) {
			predicates.add(builder.isNotNull(root.get("bonCommandeType")));
		}

		if(Boolean.TRUE.equals(criteria.getAvenants())) {
			predicates.add(builder.isNotNull(root.get("avenantType")));
		}

        query.where(predicates.toArray(Predicate[]::new));

        if (null != sort) {
            query.orderBy(super.order(sort, builder, root));
        }

        return em.createQuery(query);
    }

	@Override
	public Class<Evenement> getTarget() {
		return Evenement.class;
	}

	public static class EvenementsParContrat {

		private final Long contrat;
		private final Long total;

		public EvenementsParContrat( final Long id, final Long total ) {
			this.contrat = id;
			this.total = total;
		}

		public Long getContrat() {
			return contrat;
		}

		public Long getTotal() {
			return total;
		}
	}

	@Override
	public Map<Long, Long> compterEnAttente( final List<Long> contrats ) {

		final var builder = em.getCriteriaBuilder();
		final var query = builder.createQuery(EvenementsParContrat.class);

		final var root = query.from(Evenement.class);

		query
				.multiselect(root.join(CONTRAT).get("id"), builder.count(root).alias("count"))
			.where(
				builder.and(
						root.join(CONTRAT).get("id").in(contrats),
						builder.isTrue(root.get(SUIVI_REALISATION)),
						builder.isNull(root.get(ETAT_EVENEMENT)),
						builder.lessThanOrEqualTo(root.get(DATE_DEBUT), LocalDate.now())
				)
			)
			.groupBy(
					root.join(CONTRAT).get("id")
			);

		return em.createQuery(query).getResultList().stream().collect(Collectors.toMap(EvenementsParContrat::getContrat, EvenementsParContrat::getTotal));
	}

}
