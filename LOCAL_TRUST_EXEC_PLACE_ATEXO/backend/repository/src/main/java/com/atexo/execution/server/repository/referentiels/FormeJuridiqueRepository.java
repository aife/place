package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.FormeJuridique;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormeJuridiqueRepository extends ReferentielRepository<FormeJuridique> {

    FormeJuridique findOneByIdExterne(String idExterne);

}
