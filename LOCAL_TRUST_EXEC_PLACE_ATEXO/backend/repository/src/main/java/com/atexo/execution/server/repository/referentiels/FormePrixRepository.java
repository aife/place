package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.FormePrix;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormePrixRepository extends ReferentielRepository<FormePrix> {
    Optional<FormePrix> findFirstByCode(String code);
}
