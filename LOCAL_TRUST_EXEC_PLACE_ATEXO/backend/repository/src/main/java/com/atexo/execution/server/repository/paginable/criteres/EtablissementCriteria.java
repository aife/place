package com.atexo.execution.server.repository.paginable.criteres;

public class EtablissementCriteria {
    private final String motsCles;

    EtablissementCriteria(final String motsCles) {
        this.motsCles = motsCles;
    }

    public String getMotsCles() {
        return motsCles;
    }

    public static class Builder {
        private String motsCles;

        public Builder motsCles(final String motsCles) {
            this.motsCles = motsCles;

            return this;
        }

        public EtablissementCriteria build() {
            return new EtablissementCriteria(motsCles);
        }

    }
}
