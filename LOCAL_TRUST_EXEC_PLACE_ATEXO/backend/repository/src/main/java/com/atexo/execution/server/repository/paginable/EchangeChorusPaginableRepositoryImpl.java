package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.dto.EchangeChorusCriteriaDTO;
import com.atexo.execution.server.model.EchangeChorus;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.HashSet;
import java.util.function.Function;

@Repository
public class EchangeChorusPaginableRepositoryImpl extends BasePaginableRepository<EchangeChorus, EchangeChorusCriteriaDTO> implements EchangeChorusPaginableRepository {

	@Override
	public <V> TypedQuery<V> requeter( EchangeChorusCriteriaDTO criteria, CriteriaQuery<V> query, Function<Expression<EchangeChorus>, Expression<V>> projection, Sort sort ) {
		final var builder = em.getCriteriaBuilder();
		final var root = query.from(EchangeChorus.class);

		query.select(projection.apply(root));

		final var predicates = new HashSet<Predicate>();

		if (null!=criteria) {
			if ( null != criteria.getContrat()) {
				predicates.add(builder.equal(root.join("contrat").get("id"),criteria.getContrat()));
			}

			if ( null != criteria.getActe()) {
				predicates.add(builder.equal(root.join("acte").get("id"),criteria.getActe()));
			}
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null!=sort) {
			query.orderBy(super.order(sort, builder, root));
		}

		return em.createQuery(query);
	}

	@Override
	public Class<EchangeChorus> getTarget() {
		return EchangeChorus.class;
	}
}
