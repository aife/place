package com.atexo.execution.server.repository.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by MZO on 26/01/2016.
 */

@NoRepositoryBean
public interface ReferentielRepository<T> extends JpaRepository<T, Long> {

    T findOneByCode(String code);

    Optional<T> findByCode(String code);

    Optional<T> findByCodeExterne(String codeExterne);

    List<T> findByLabel(String label);

    Optional<T> findByIdExterne(String idExterne);
    Set<T> findAllByIdExterneIn(String... idExterne);

    List<T> findByActif(boolean actif);

    Optional<T> findByCodeAndGroupeCodeAndActif(String code, String parentCode, boolean actif);

    Optional<T> findByCodeAndActif(String code, boolean actif);

}
