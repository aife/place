package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.ChampFusion;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChampsFusionRepository extends ReferentielRepository<ChampFusion> {

}
