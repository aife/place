package com.atexo.execution.server.repository.paginable.criteres;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.model.Utilisateur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class DonneesEssentiellesCriteria {
    private Utilisateur utilisateur;
    private Long idDonneeEssentielle;
    private Long idContrat;
    private List<TypeContrat> typesContrat;
    private List<LocalDate> dateNotification;
    private List<LocalDateTime> dateEnvoi;
    private List<StatutPublicationDE> statuts;
    private PerimetreContrat perimetre;
    private String mpePfUid;
}
