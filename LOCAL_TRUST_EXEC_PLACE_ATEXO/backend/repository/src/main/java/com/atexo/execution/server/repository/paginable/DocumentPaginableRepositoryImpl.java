package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.DocumentContrat;
import com.atexo.execution.server.repository.paginable.criteres.DocumentCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.function.Function;

@Repository
public class DocumentPaginableRepositoryImpl extends BasePaginableRepository<DocumentContrat, DocumentCriteria> implements DocumentPaginableRepository {

    private final static Logger LOG = LoggerFactory.getLogger(DocumentPaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
		DocumentCriteria criteria,
		CriteriaQuery<V> query,
		Function<Expression<DocumentContrat>, Expression<V>> projection,
		Sort sort
	) {
		final var builder = em.getCriteriaBuilder();

		final var root = query.from(DocumentContrat.class);

		query.select(projection.apply(root));

		final var predicates = new HashSet<Predicate>();

		if (criteria.getEvenement() != null && criteria.getEvenement().length > 0) {
			// Evenement
			predicates.add(root.join("evenements").get("id").in(criteria.getEvenement()));
		}

		if (criteria.getTypeEvenement() != null && criteria.getTypeEvenement().length > 0) {
			// Evenement
			predicates.add(root.join("evenements").join("typeEvenement").get("code").in(criteria.getTypeEvenement()));
		}

		if (criteria.getEtablissement() != null && criteria.getEtablissement().length > 0) {
			// Document
			predicates.add(root.join("etablissement").get("id").in(criteria.getEtablissement()));
		}

		if (criteria.getTypes() != null && criteria.getTypes().length > 0) {
			// Document
			predicates.add(root.get("documentType").in(criteria.getTypes()));
		}

		if (null != criteria.getMotsCles() && !criteria.getMotsCles().isEmpty()) {
			for (var token : criteria.getMotsCles().toLowerCase().split(SPACE_PATTERN)) {
				var expression = MessageFormat.format("%{0}%", token);

				LOG.debug("Recherche de contrats sur l'expression : '{}'", expression);

				predicates.add(
						builder.or(
								builder.like(builder.lower(root.get("commentaire")), expression),
								builder.like(builder.lower(root.get("objet")), expression),
								builder.like(builder.lower(root.join("fichier").get("nom")), expression)
						)
				);
			}
		}

		if (criteria.getContrat() != null) {
			// Document
			predicates.add(builder.equal(root.join("contrat").get("id"), criteria.getContrat()));
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null!=sort) {
			query.orderBy(super.order(sort, builder, root));
		}

		return  em.createQuery(query);
	}

	@Override
	public Class<DocumentContrat> getTarget() {
		return DocumentContrat.class;
	}
}
