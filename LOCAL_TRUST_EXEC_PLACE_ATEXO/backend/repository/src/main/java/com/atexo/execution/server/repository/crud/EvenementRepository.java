package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Evenement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EvenementRepository extends JpaRepository<Evenement, Long> {

    List<Evenement> findByContratId(Long id);

    List<Evenement> findByAlerteAndEnvoiAlerte(Boolean alerte, Boolean envoiAlerte);

    @Query("select sum(e.avenantType.montant) from Evenement e where e.contrat.id = :idContrat and e.dateDebut < :dateAvenant")
    Double calculSommePrecedentsAvenants(@Param("idContrat") Long idContrat, @Param("dateAvenant") LocalDate dateAvenant);

    @Query("select sum(e.avenantType.montant) from Evenement e where e.contrat.id = :idContrat and e.dateDebut <= :dateAvenant")
    Double calculSommeCurrentsAvenants(@Param("idContrat") Long idContrat, @Param("dateAvenant") LocalDate dateAvenant);

}
