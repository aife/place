package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ContratRepository extends JpaRepository<Contrat, Long> {
	Optional<Contrat> findOneByIdExterneAndServiceOrganismeId(String idExterne, Long organismeId);
    Optional<Contrat> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<Contrat> findByNumeroAndServiceIdAndPlateformeMpeUid(String numero, Long idService, String plateformeUid);

    List<Contrat> findByNumeroAndPlateformeMpeUid(String numero, String plateformeUid);

    Contrat findByIdExterneAndAttributaireIdExterne(String contratIdExterne, String AttributaireIdExterne);

    Optional<Contrat> findByReferenceLibreAndAttributaireEtablissementSiret(String referenceLibre, String siret);

    List<Contrat> findByReferenceLibre(String referenceLibre);

    Optional<Contrat> findByUuidAndPlateformeMpeUid(String uuid, String plateformeUid);

    Optional<Contrat> findFirstByUuid(String uuid);

    List<Contrat> findByDateDebutEtudeRenouvellementAfterAndDateEnvoiAlerteIsNull(LocalDate now);


    @Modifying
    @Query("update Contrat c set c.createur = :utilisateur where c.createur is null and c.idExterneCreateur =  :#{#utilisateur?.idExterne}")
    void mettreAJourCreateur(@Param("utilisateur") Utilisateur utilisateur);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
	@Query(value = "update Contrat c set c.dateModification = current_date(), c.service = :nouveauService where c.service = :ancienService")
	void mettreAJourService(@Param("ancienService") Service ancienService, @Param("nouveauService") Service nouveauService);

    @Query("select distinct c from Contrat c " +
            "join c.donneesEssentielles de " +
            "join c.service s " +
            "join s.organisme o  " +
            "where o.id= :#{#organisme.id} " +
            "and c.publicationDonneesEssentielles = true " +
            "and de.statut = :statutPublication " +
            "and c.dateNotification <= current_date() " +
            "and c.statut != :statutContrat")
    List<Contrat> findContratByOrganisme(@Param("organisme") Organisme organisme, @Param("statutPublication") StatutPublicationDE statutPublication, @Param("statutContrat") StatutContrat statutContrat);

    Optional<Contrat> findFirstByConsultationIdExterneAndNumeroLotAndChapeau(String consultationIdExterne, Integer numeroLot, Boolean chapeau);

    boolean existsByNumeroAndServiceId(String numero, Long idService);

    boolean existsByNumeroAndServiceOrganismeId(String numero, Long idOrganisme);

    boolean existsByNumeroAndPlateformeId(String numero, Long idPlateforme);

    boolean existsByNumeroAndServiceIdAndIdNotAndChapeau(String numero, Long idService, Long id, boolean chapeau);

    boolean existsByNumeroAndServiceOrganismeIdAndIdNotAndChapeau(String numero, Long idOrganisme, Long id, boolean chapeau);

    boolean existsByNumeroAndPlateformeIdAndIdNotAndChapeau(String numero, Long idPlateforme, Long id, boolean chapeau);

    boolean existsByReferenceLibreAndServiceId(String numero, Long idService);

    boolean existsByReferenceLibreAndServiceOrganismeId(String numero, Long idOrganisme);

    boolean existsByReferenceLibreAndPlateformeId(String numero, Long idPlateforme);

    boolean existsByReferenceLibreAndServiceIdAndIdNotAndChapeau(String numero, Long idService, Long id, boolean chapeau);

    boolean existsByReferenceLibreAndServiceOrganismeIdAndIdNotAndChapeau(String numero, Long idOrganisme, Long id, boolean chapeau);

    boolean existsByReferenceLibreAndPlateformeIdAndIdNotAndChapeau(String numero, Long idPlateforme, Long id, boolean chapeau);

    boolean existsByNumeroLongAndServiceId(String numero, Long idService);

    boolean existsByNumeroLongAndServiceOrganismeId(String numero, Long idOrganisme);

    boolean existsByNumeroLongAndPlateformeId(String numero, Long idPlateforme);

    boolean existsByNumeroLongAndServiceIdAndIdNotAndChapeau(String numero, Long idService, Long id, boolean chapeau);

    boolean existsByNumeroLongAndServiceOrganismeIdAndIdNotAndChapeau(String numero, Long idOrganisme, Long id, boolean chapeau);

    boolean existsByNumeroLongAndPlateformeIdAndIdNotAndChapeau(String numero, Long idPlateforme, Long id, boolean chapeau);

    @Query("select distinct c from Contrat c " +
            "join c.donneesEssentielles de " +
            "join c.service s " +
            "join s.organisme o " +
            "join o.plateforme pf " +
            "where c.publicationDonneesEssentielles = true " +
            "and pf.active = true " +
            "and de.statut = :statut " +
            "and (de.modeEnvoiDonneesEssentielles != :modeEnvoi or de.modeEnvoiDonneesEssentielles is null) " +
            "and c.dateNotification <= current_date() " +
            "and c.statut != :statutContrat")
    Page<Contrat> findByContratToPublish(@Param("statut") StatutPublicationDE statut, @Param("statutContrat") StatutContrat statutContrat, @Param("modeEnvoi") ModeEnvoiDonneesEssentielles modeEnvoiDonneesEssentielles, Pageable pageable);

    List<Contrat> findAllByContratChapeauId(Long idContratChapeau);
}
