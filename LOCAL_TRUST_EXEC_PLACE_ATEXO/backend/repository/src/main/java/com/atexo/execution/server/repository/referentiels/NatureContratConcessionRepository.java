package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.concession.NatureContratConcession;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NatureContratConcessionRepository extends ReferentielRepository<NatureContratConcession> {
    Optional<NatureContratConcession> findFirstByCode(String code);
}
