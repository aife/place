package com.atexo.execution.server.repository.echanges;

import com.atexo.execution.server.model.EchangeChorus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EchangeChorusRepository extends JpaRepository<EchangeChorus, Long> {

    Optional<EchangeChorus> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<EchangeChorus> findByContratId(Long idContrat);
}
