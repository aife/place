package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.DocumentModele;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentModeleRepository extends JpaRepository<DocumentModele, Long> {

    DocumentModele findOneById(Long id);

    List<DocumentModele> findByCustomFalse();

    @Query("select dm from Organisme o join o.documentModeles dm where o.id = ?1")
    List<DocumentModele> findByOrganismeId(Long organismeId);

}
