package com.atexo.execution.server.repository.paginable.criteres;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FournisseurCriteria {
    private String motsCles;
    private String siren;
    private String pays;
    private String uuid;
}
