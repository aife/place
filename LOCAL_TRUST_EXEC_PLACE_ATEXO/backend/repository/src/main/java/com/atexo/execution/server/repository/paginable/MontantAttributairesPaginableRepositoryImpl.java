package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.ContratEtablissement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MontantAttributairesPaginableRepositoryImpl implements MontantAttributairesPaginableRepository {

	private static final Logger LOG = LoggerFactory.getLogger(MontantAttributairesPaginableRepositoryImpl.class);

	public static class MontantAttributaires {

		private final Long id;
		private final BigDecimal total;

		public MontantAttributaires( final Long id, final BigDecimal total ) {
			this.id = id;
			this.total = total;
		}

		public Long getId() {
			return id;
		}

		BigDecimal getTotal() {
			return total;
		}
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Map<Long, BigDecimal> recupererMontantsAttributaire( final List<Long> idsContrat) {
		if (idsContrat == null || idsContrat.isEmpty()) {
			return new HashMap<>();
		}

		final var builder = em.getCriteriaBuilder();
		final var query = builder.createQuery(MontantAttributaires.class);

		final var root = query.from(ContratEtablissement.class);

		query.multiselect(root.join("contrat").get("id"), builder.sum(builder.coalesce(root.get("montant"), 0)).alias("total"))
			.where(
				builder.and(
					root.join("contrat").get("id").in(idsContrat),
					builder.isNull(root.get("commanditaire"))
				)
			)
			.groupBy(root.join("contrat").get("id"));

		final var result = em.createQuery(query);

		final var elements = result.getResultList();

		return elements.stream().collect(Collectors.toMap(MontantAttributaires::getId, MontantAttributaires::getTotal));
	}
}
