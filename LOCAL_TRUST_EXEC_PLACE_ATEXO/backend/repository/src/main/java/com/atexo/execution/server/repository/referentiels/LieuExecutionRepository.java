package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.LieuExecution;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LieuExecutionRepository extends ReferentielRepository<LieuExecution> {

    TypeContrat findOneByIdExterne(String idExterne);

}
