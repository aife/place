package com.atexo.execution.server.repository.paginable;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface MontantAttributairesPaginableRepository {

    Map<Long, BigDecimal> recupererMontantsAttributaire( List<Long> idsContrat);
}
