package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;

public interface ContratPaginableRepository extends PaginableRepository<Contrat, ContratCriteria> {
	String SERVICE = "service";
	String SERVICES = "services";
	String INVITES_PONCTUELS = "invitesPonctuels";
	String CONTRAT = "contrat";
	String ID = "id";
	String UTILISATEUR = "utilisateur";
	String FAVORIS = "favoris";
	String CREATEUR = "createur";
	String PARENT = "parent";
	String STATUT = "statut";
	String REFERENCE_LIBRE = "referenceLibre";
	String NUMERO = "numero";
	String NUMERO_LONG = "numeroLong";
	String OBJET = "objet";
	String CONSULTATION = "consultation";
	String NOM = "nom";
	String PRENOM = "prenom";
	String IDENTIFIANT = "identifiant";
	String ORGANISME = "organisme";
	String CODE_ACHAT = "codeAchat";
	String CODE = "code";
    String CONTRAT_CHAPEAU = "contratChapeau";
	String DATE_PREVISIONNELLE_NOTIFICATION = "datePrevisionnelleNotification";
	String DATE_MAX_FIN_CONTRAT = "dateMaxFinContrat";
	String DATE_FIN_CONTRAT = "dateFinContrat";
	String DATE_DEBUT_ETUDE_RENOUVELLEMENT = "dateDebutEtudeRenouvellement";
	String CONSIDERATIONS_SOCIALES = "considerationsSociales";
	String CONSIDERATIONS_ENVIRONNEMENTALES = "considerationsEnvironnementales";
	String ID_OFFRE = "idOffre";
	String CATEGORIE = "categorie";
    String ID_EXTERNE = "idExterne";
    String STATUT_RENOUVELLEMENT = "statutRenouvellement";
    String UNITE_FONCTIONNELLE_OPERATION = "uniteFonctionnelleOperation";
    String DATE_MODIFICATION = "dateModification";
    String INTITULE = "intitule";
    String NUMERO_LOT = "numeroLot";
	String DATE_NOTIFICATION = "dateNotification";

	String SIREN = "siren";
	String COMPLEMENT = "complement";
	String TYPE_CONTRAT = "type";
	String ACRONYME = "acronyme";
	String DONNNEES_ESSENTIELLES = "donneesEssentielles";

	String STATUT_PUBLICATION_DE = "statut";
    String DEFENSE_OU_SECURITE = "defenseOuSecurite";
	String PLATEFORME = "plateforme";
	String MPE_UID = "mpeUid";
    String CHAPEAU = "chapeau";
}
