package com.atexo.execution.server.customLiquibase;

import liquibase.Scope;
import liquibase.change.custom.CustomSqlChange;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Properties;

import static com.atexo.execution.common.def.ParametrageDiscriminators.MESSAGERIE_SECURISEE;
import static com.atexo.execution.server.model.ParametrageMessagerieSecurisee.*;

@Slf4j
public class ParamtrageMessecLiquibase extends AbstractChangeLog implements CustomTaskChange, CustomSqlChange {

    @Override
    public void execute(Database database) {
        Scope.getCurrentScope().getLog(getClass()).info("Setting paramétrage messec");
    }

    @Override
    public String getConfirmationMessage() {
        return "Paramétrage messec set up successfully.";
    }

    @Override
    public void setUp() {
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }


    @Override
    public SqlStatement[] generateStatements(Database database) {
        var properties = getProperties();
        var pfUid = properties.getProperty("pf.uid");
        log.info("pfUid: {}", pfUid);
        var statemens = new ArrayList<SqlStatement>();
        statemens.add(new RawSqlStatement(generatStatement(CLE_NOM_PF_EMETTEUR, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_SIGNATURE, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_URL_PF_DESTINATAIRE, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_NOM_PF_DESTINATAIRE, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_URL_PF_DESTINATAIRE_VISUALISATION, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_EMAIL_EXPEDITEUR, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_LOGO_SRC, properties, pfUid)));
        statemens.add(new RawSqlStatement(generatStatement(CLE_TAILLE_MAX_PJ, properties, pfUid)));
        log.debug("insertSql: {}", statemens);
        return statemens.toArray(new SqlStatement[0]);
    }

    private String generatStatement(String clef, Properties properties, String pfUid) {
        return MessageFormat.format(
                "INSERT IGNORE INTO PARAMETRAGE (type, export_front ,clef,valeur,id_plateforme) VALUES (''{0}'',''{1}'',''{2}'',''{3}'',({4}));", MESSAGERIE_SECURISEE, false, clef, escapeSQL(properties.getProperty(clef)), "select id from plateforme where uid = '" + pfUid + "' limit 1") + "\n";
    }

    private String escapeSQL(String value) {
        if (value == null)
            return "";
        return value.replace("'", "''");
    }


}
