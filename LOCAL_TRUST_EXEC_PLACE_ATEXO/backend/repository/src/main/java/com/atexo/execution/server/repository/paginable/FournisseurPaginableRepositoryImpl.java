package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Fournisseur;
import com.atexo.execution.server.repository.paginable.criteres.FournisseurCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.function.Function;

import static org.springframework.util.ObjectUtils.isEmpty;

@Repository
public class FournisseurPaginableRepositoryImpl extends BasePaginableRepository<Fournisseur, FournisseurCriteria> implements FournisseurPaginableRepository {

	private static final Logger LOG = LoggerFactory.getLogger(FournisseurPaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
		FournisseurCriteria criteria,
		CriteriaQuery<V> query,
		Function<Expression<Fournisseur>, Expression<V>> projection,
		Sort sort
	) {
		final var builder = em.getCriteriaBuilder();
		final var root = query.from(Fournisseur.class);

		query.select(projection.apply(root));

		final var predicates = new HashSet<Predicate>();

		if (!isEmpty(criteria.getMotsCles())) {
			for (var token : criteria.getMotsCles().toLowerCase().split(SPACE_PATTERN)) {
				var expression = MessageFormat.format("%{0}%", token);
				LOG.debug("Recherche de fournisseurs sur l'expression : '{}'", expression);
				predicates.add(
						builder.or(
								builder.like(builder.lower(root.get(RAISON_SOCIALE)), expression),
								builder.like(builder.lower(root.get(SIREN)), expression),
								builder.like(builder.lower(root.get(DESCRIPTION)), expression),
								builder.like(builder.lower(root.get(NOM)), expression),
								builder.like(builder.lower(root.get(PRENOM)), expression)
						)
				);
			}
		}
		if (criteria.getSiren() != null) {
			predicates.add(builder.equal(root.get(SIREN), criteria.getSiren()));
		}
		if (criteria.getPays() != null) {
			predicates.add(builder.equal(root.get(PAYS), criteria.getPays()));
		}
		if (criteria.getUuid() != null) {
			predicates.add(builder.equal(root.get(UUID), criteria.getUuid()));
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null!=sort) {
			query.orderBy(super.order(sort, builder, root));
		}

		return em.createQuery(query);
	}

	@Override
	public Class<Fournisseur> getTarget() {
		return Fournisseur.class;
	}

}
