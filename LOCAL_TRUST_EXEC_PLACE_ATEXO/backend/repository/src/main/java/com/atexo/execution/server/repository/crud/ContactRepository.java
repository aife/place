package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    List<Contact> findByIdIn(Long[] ids);

    Optional<Contact> findFirstByIdExterneAndPlateformeMpeUidAndEmail(String idExterne, String plateformeUid, String email);

    Optional<Contact> findFirstByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    Contact findOneById(Long id);

    void deleteByEtablissementIsNullAndIdIn(List<Long> ids);

    Optional<Contact> findOneByIdExterne(String idExterne);

    List<Contact> findAllByEtablissementIdAndEmailAndPlateformeId(Long etablissementId, String email, Long plateformeId);

    List<Contact> findByEtablissementId(Long id);
}
