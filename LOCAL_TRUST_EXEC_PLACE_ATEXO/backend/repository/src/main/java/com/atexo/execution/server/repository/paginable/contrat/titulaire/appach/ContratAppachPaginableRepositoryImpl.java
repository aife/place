package com.atexo.execution.server.repository.paginable.contrat.titulaire.appach;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.paginable.BasePaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteriaAppach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class ContratAppachPaginableRepositoryImpl extends BasePaginableRepository<Contrat, ContratCriteriaAppach> implements ContratAppachPaginableRepository {

    private static final Logger LOG = LoggerFactory.getLogger(ContratAppachPaginableRepositoryImpl.class);

    @Override
    public <V> TypedQuery<V> requeter(
            ContratCriteriaAppach criteria,
            CriteriaQuery<V> query,
            Function<Expression<Contrat>, Expression<V>> projection,
            Sort sort
    ) {
        LOG.info("Recherche de contrat avec les critères suivants : {}", criteria);

        final var builder = em.getCriteriaBuilder();
        final var cursor = query.from(Contrat.class);
        query.select(projection.apply(cursor));
        final var predicates = new HashSet<Predicate>();

        buildContratTitulaireCriteria(criteria, builder, cursor, predicates);

        query.where(predicates.toArray(Predicate[]::new));

        if (null != sort) {
            query.orderBy(super.order(sort, builder, cursor));
        }

        return em.createQuery(query);
    }

    @Override
    public Class<Contrat> getTarget() {
        return Contrat.class;
    }

    private void buildContratTitulaireCriteria(ContratCriteriaAppach criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        var idExterne = prepareIdExterneList(criteria);
        var numeroContratList = prepareNumeroContratList(criteria);
        var statutContratList = prepareStatutContratList(criteria);
        if (!CollectionUtils.isEmpty(idExterne)) {
            Predicate in = builder.and(cursor.get(ID).in(idExterne), builder.greaterThan(cursor.get(ID), 2000000));
            Predicate or = builder.or(cursor.get(ID_EXTERNE).in(idExterne.stream().map(String::valueOf).collect(Collectors.toUnmodifiableSet())), in);
            predicates.add(or);
        }
        if (!CollectionUtils.isEmpty(numeroContratList)) {
            predicates.add(cursor.get(NUMERO).in(numeroContratList));
        }
        if (!CollectionUtils.isEmpty(statutContratList) && !CollectionUtils.isEmpty(criteria.getStatutContratAppach())) {
            if (criteria.getStatutContratAppach().size() == 1 && criteria.getStatutContratAppach().contains(1)) {
                predicates.add(builder.isEmpty(cursor.join(CONSULTATION).get(MODALITE_EXECUTIONS)));
            }
            if (criteria.getStatutContratAppach().size() == 1 && criteria.getStatutContratAppach().contains(3)) {
                predicates.add(builder.isNotEmpty(cursor.join(CONSULTATION).get(MODALITE_EXECUTIONS)));
            }
            predicates.add(builder.equal(cursor.get(CHAPEAU), false));
            predicates.add(cursor.get(STATUT).in(statutContratList));
        }
        LocalDateTime dateModification = criteria.getDateModification();
        if (dateModification != null) {
            LocalDateTime start = dateModification.withHour(0).withMinute(0).withSecond(0);
            LocalDateTime end = dateModification.withHour(23).withMinute(59).withSecond(59);
            predicates.add(builder.between(cursor.get(DATE_MODIFICATION), start, end));
        }
        if (criteria.getObjetContrat() != null && !criteria.getObjetContrat().isEmpty()) {
            predicates.add(builder.equal(cursor.get(OBJET), criteria.getObjetContrat()));
        }
        if (criteria.getIntitule() != null && !criteria.getIntitule().isEmpty()) {
            predicates.add(builder.equal(cursor.get(INTITULE), criteria.getIntitule()));
        }
        if (criteria.getIdConsultation() != null) {
            predicates.add(builder.equal(cursor.join(CONSULTATION).get(ID), criteria.getIdConsultation()));
        }
        if (criteria.getIdLot() != null) {
            predicates.add(builder.equal(cursor.get(NUMERO_LOT), criteria.getIdLot()));
        }
        LocalDate dateNotificationMin = criteria.getDateNotificationMin();
        if (dateNotificationMin != null) {
            predicates.add(builder.greaterThanOrEqualTo(cursor.get(DATE_NOTIFICATION), dateNotificationMin));
        }
        LocalDate dateNotificationMax = criteria.getDateNotificationMax();
        if (dateNotificationMax != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_NOTIFICATION), dateNotificationMax));
        }

        if (criteria.getSiren() != null && !criteria.getSiren().isEmpty()) {
            predicates.add(builder.equal(cursor.get(SERVICE).get(ORGANISME).get(SIREN), criteria.getSiren()));
        }
        List<String> acronymesOrganisme = criteria.getAcronymesOrganisme();
        if (acronymesOrganisme != null && !acronymesOrganisme.isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ACRONYME).in(acronymesOrganisme));
        }
        if (criteria.getComplement() != null && !criteria.getComplement().isEmpty()) {
            predicates.add(builder.equal(cursor.get(SERVICE).get(ORGANISME).get(COMPLEMENT), criteria.getComplement()));
        }
        if (criteria.getStatutPublicationSn() != null && criteria.getStatutPublicationSn() == 1) {
            predicates.add(cursor.get(DONNNEES_ESSENTIELLES).get(STATUT_PUBLICATION_DE).in(StatutPublicationDE.A_PUBLIER));
        }
        if (criteria.getStatutPublicationSn() != null && criteria.getStatutPublicationSn() == 0) {
            predicates.add(cursor.get(DONNNEES_ESSENTIELLES).get(STATUT_PUBLICATION_DE).in(StatutPublicationDE.A_PUBLIER).not());
        }
        if (criteria.getExclusionOrganisme() != null && !criteria.getExclusionOrganisme().isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ACRONYME).in(criteria.getExclusionOrganisme()).not());
        }
        if (criteria.getIdServices() != null && !criteria.getIdServices().isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ID).in(criteria.getIdServices()));
        }
        if (criteria.getChapeau() != null && criteria.getChapeau()) {
            predicates.add(builder.equal(cursor.get(CHAPEAU), criteria.getChapeau()));
        }
        if (criteria.getDefenseOuSecurite() != null) {
            predicates.add(builder.equal(cursor.get(DEFENSE_OU_SECURITE), criteria.getDefenseOuSecurite()));
        }
        LocalDateTime dateModificationActes = criteria.getDateModificationActe();
        if (dateModificationActes != null) {
            LocalDateTime start = dateModificationActes.withHour(0).withMinute(0).withSecond(0);
            LocalDateTime end = dateModificationActes.withHour(23).withMinute(59).withSecond(59);
            Join<Contrat, Acte> actesFetch = cursor.join(ACTES, JoinType.INNER);
            predicates.add(builder.between(actesFetch.get(DATE_MODIFICATION), start, end));
        }
        Utilisateur utilisateur = criteria.getUtilisateur();
        if (utilisateur != null && !CollectionUtils.isEmpty(utilisateur.getOrganismesAssocies())) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ID).in(utilisateur.getOrganismesAssocies().stream().map(AbstractEntity::getId).collect(Collectors.toUnmodifiableSet())));
        }
    }


    private Set<Integer> prepareIdExterneList(ContratCriteriaAppach criteria) {
        Set<Integer> idExterneList = new HashSet<>();
        if (criteria.getIdContratTitulaire() != null) {
            idExterneList.add(criteria.getIdContratTitulaire());
        }
        if (!CollectionUtils.isEmpty(criteria.getIdContratTitulaires())) {
            idExterneList.addAll(criteria.getIdContratTitulaires());
        }

        return idExterneList;
    }

    private Set<String> prepareNumeroContratList(ContratCriteriaAppach criteria) {
        Set<String> numeroContratList = new HashSet<>();
        if (criteria.getNumeroContrat() != null) {
            numeroContratList.add(criteria.getNumeroContrat());
        }
        if (!CollectionUtils.isEmpty(criteria.getNumeroContrats())) {
            numeroContratList.addAll(criteria.getNumeroContrats());
        }

        return numeroContratList;
    }

    private Set<StatutContrat> prepareStatutContratList(ContratCriteriaAppach criteria) {
        Set<StatutContrat> statutContratList = new HashSet<>();
        if (criteria.getStatutContrat() != null) {
            statutContratList.add(criteria.getStatutContrat());
        }
        if (!CollectionUtils.isEmpty(criteria.getStatutContrats())) {
            statutContratList.addAll(criteria.getStatutContrats());
        }

        return statutContratList;
    }

}
