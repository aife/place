package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Plateforme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlateformeRepository extends JpaRepository<Plateforme, Long> {
    Optional<Plateforme> findByMpeUid(String mpeUid);
    boolean existsByMpeUid(String mpeUid);

    @Query("SELECT p FROM Plateforme p LEFT JOIN p.utilisateurs u WHERE (u.identifiant = :login and u.technique=true) or p.mpeUid = :login")
    Optional<Plateforme> findByMpeUidOrUtilisateurTechnique(@Param("login") String login);

    List<Plateforme> findAllByActiveTrue();
}
