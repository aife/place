package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeContratRepository extends ReferentielRepository<TypeContrat> {

    TypeContrat findOneByIdExterne(String idExterne);

}
