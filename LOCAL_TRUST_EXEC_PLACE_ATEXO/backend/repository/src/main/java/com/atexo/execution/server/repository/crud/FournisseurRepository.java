package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur, Long> {

    Optional<Fournisseur> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    Optional<Fournisseur> findFirstBySirenAndIdExterneIsNull(String siren);

    Optional<Fournisseur> findOneById(Long id);

    Optional<Fournisseur> findOneBySiren(String siren);

    List<Fournisseur> findBySiren(String siren);

    Optional<Fournisseur> findOneBySirenAndPays(String siren, String pays);

    Optional<Fournisseur> findFirstBySirenAndPays(String siren, String pays);

    Optional<Fournisseur> findFirstBySiren(String siren);

    Optional<Fournisseur> findByUuid(String uuid);
}
