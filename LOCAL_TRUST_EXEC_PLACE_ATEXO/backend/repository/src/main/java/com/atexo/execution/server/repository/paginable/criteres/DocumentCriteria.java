package com.atexo.execution.server.repository.paginable.criteres;

import com.atexo.execution.common.def.DocumentContratType;

public class DocumentCriteria {

	private final Long contrat;
	private final String motsCles;
	private final Long[] evenement;
	private final Long[] etablissement;
	private final String[] typeEvenement;
	private final DocumentContratType[] types;

	public DocumentCriteria( Long contratId, String motsCles, Long[] evenementId, Long[] etablissementId, String[] typeEvenementId, DocumentContratType[] documentTypes ) {
		this.contrat = contratId;
		this.motsCles = motsCles;
		this.evenement = evenementId;
		this.etablissement = etablissementId;
		this.typeEvenement = typeEvenementId;
		this.types = documentTypes;
	}

	public Long getContrat() {
		return contrat;
	}

	public String getMotsCles() {
		return motsCles;
	}

	public Long[] getEvenement() {
		return evenement;
	}

	public Long[] getEtablissement() {
		return etablissement;
	}

	public String[] getTypeEvenement() {
		return typeEvenement;
	}

	public DocumentContratType[] getTypes() {
		return types;
	}

	public static class Builder {
		private Long contrat;
		private String motsCles;
		private Long[] evenement;
		private Long[] etablissement;
		private String[] typeEvenement;
		private DocumentContratType[] types;

		public Builder contrat( Long contrat ) {
			this.contrat = contrat;

			return this;
		}

		public Builder motsCles( String motsCles ) {
			this.motsCles = motsCles;

			return this;
		}

		public Builder evenement( Long[] evenement ) {
			this.evenement = evenement;

			return this;
		}

		public Builder etablissement( Long[] etablissement ) {
			this.etablissement = etablissement;

			return this;
		}

		public Builder typeEvenement( String[] typeEvenement ) {
			this.typeEvenement = typeEvenement;

			return this;
		}

		public Builder types( DocumentContratType[] types ) {
			this.types = types;

			return this;
		}

		public DocumentCriteria build() {
			return new DocumentCriteria(contrat, motsCles, evenement, etablissement, typeEvenement, types);
		}
	}
}
