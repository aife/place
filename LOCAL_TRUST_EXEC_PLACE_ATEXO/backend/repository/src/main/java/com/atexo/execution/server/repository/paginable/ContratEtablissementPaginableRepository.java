package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.repository.paginable.criteres.ContratEtablissementCriteria;

public interface ContratEtablissementPaginableRepository extends PaginableRepository<ContratEtablissement, ContratEtablissementCriteria> {
	String CONTRAT = "contrat";
	String SERVICE = "service";
	String ORGANISME = "organisme";
	String ID = "id";
	String STATUT = "statut";
	String ETABLISSEMENT = "etablissement";
	String SIRET = "siret";
	String ATTRIBUTAIRE = "attributaire";
	String ORGANISMES_ELIGIBLES = "organismesEligibles";
	String SERVICES_ELIGIBLES = "servicesEligibles";
}
