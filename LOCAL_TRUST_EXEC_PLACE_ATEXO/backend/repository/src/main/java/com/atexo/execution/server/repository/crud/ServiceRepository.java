package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

    Optional<Service> findOneByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<Service> findByOrganismeIdAndPlateformeIdOrderByNomAsc(Long organismeId, Long plateformeId);

    Optional<Service> findByIdAndOrganismeId(Long serviceId, Long organismeId);

    Optional<Service> findFirstByNomAndPlateformeMpeUid(String nom, String plateformeUid);

    Optional<Service>  findOneByOldIdExterneAndPlateformeMpeUid(String oldIdExterne, String uid);

    default Optional<Service> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid) {
        var service = this.findOneByIdExterneAndPlateformeMpeUid(idExterne, plateformeUid);

        // Séance de rattrapage
        if (service.isPresent()) {
            // On cherche un service plus récent, qui aurait pour oldIdExterne l'idExterne du notre
            var newService = this.findOneByOldIdExterneAndPlateformeMpeUid(service.get().getIdExterne(), plateformeUid);

            if (newService.isPresent()) {
                return newService;
            }
        }

        return service;
    }

    List<Service> findByOrganismeIdAndPlateformeIdAndParentIsNullOrderByNomAsc(Long organismeId, Long plateformeId);

    Optional<Service> findByIdAndOrganismeIdAndPlateformeIdOrderByNomAsc(Long id, Long organismeId, Long plateformeId);
}
