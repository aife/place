package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.repository.paginable.criteres.ContratEtablissementCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import static com.atexo.execution.server.repository.paginable.ContratPaginableRepository.*;
import static com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository.FOURNISSEUR;
import static com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository.RAISON_SOCIALE;


@Repository
public class ContratEtablissementPaginableRepositoryImpl extends BasePaginableRepository<ContratEtablissement, ContratEtablissementCriteria> implements ContratEtablissementPaginableRepository {

    private static final Logger LOG = LoggerFactory.getLogger(ContratEtablissementPaginableRepositoryImpl.class);

    @Override
    public <V> TypedQuery<V> requeter(
            ContratEtablissementCriteria criteria,
            CriteriaQuery<V> query,
            Function<Expression<ContratEtablissement>, Expression<V>> projection,
            Sort sort
    ) {
        LOG.info("Recherche de contrat/établissements avec les critères suivants : {}", criteria);

        final var builder = em.getCriteriaBuilder();
        final var root = query.from(ContratEtablissement.class);

        query.select(projection.apply(root));

        final var predicates = new HashSet<Predicate>();
        predicates.add(builder.equal(root.join(PLATEFORME).get(MPE_UID),criteria.getPfUid()));
        predicates.addAll(buildContratEtablissementCriteria(root, criteria, builder));

        query.where(predicates.toArray(Predicate[]::new));

        if (null != sort) {
            query.orderBy(super.order(sort, builder, root));
        }

        return em.createQuery(query);
    }

    @Override
    public Class<ContratEtablissement> getTarget() {
        return ContratEtablissement.class;
    }

    private Set<Predicate> buildContratEtablissementCriteria(Root<ContratEtablissement> root, ContratEtablissementCriteria criteria, CriteriaBuilder builder) {
        final var predicates = new HashSet<Predicate>();

        if (criteria.getOrganisme() != null) {
            predicates.add(builder.equal(root.join(CONTRAT).join(SERVICE).join(ORGANISME).get(ID), criteria.getOrganisme()));
        }

        if (null != criteria.getMotsCles() && !criteria.getMotsCles().isEmpty()) {
            var expression = MessageFormat.format("%{0}%", criteria.getMotsCles().toLowerCase());

            LOG.debug("Recherche de contrats/établissements sur l'expression : '{}'", expression);

            predicates.add(builder.or(
                    builder.like(builder.lower(root.join(CONTRAT).get(REFERENCE_LIBRE)), expression),
                    builder.like(builder.lower(root.join(CONTRAT).get(NUMERO)), expression),
                    builder.like(builder.lower(root.join(CONTRAT).get(NUMERO_LONG)), expression),
                    builder.like(builder.lower(root.join(CONTRAT).get(OBJET)), expression),
                    builder.like(builder.lower(root.join(CONTRAT).join(CONSULTATION).get(NUMERO)), expression),
                    builder.like(builder.lower(root.join(CONTRAT).join(CONSULTATION).get(OBJET)), expression),
                    builder.like(builder.lower(root.join(ETABLISSEMENT).get(SIRET)), expression),
                    builder.like(builder.lower(root.join(ETABLISSEMENT).join(FOURNISSEUR).get(SIREN)), expression),
                    builder.like(builder.lower(root.join(ETABLISSEMENT).join(FOURNISSEUR).get(RAISON_SOCIALE)), expression)
            ));
        }

        if (criteria.getStatuts() != null && criteria.getStatuts().length > 0) {
            predicates.add(root.join(CONTRAT).get(STATUT).in(criteria.getStatuts()));
        }

        if (!CollectionUtils.isEmpty(criteria.getSirets())) {
            predicates.add(root.join(ETABLISSEMENT).get(SIRET).in(criteria.getSirets()));
        }

        if (criteria.getIdFournisseur() != null) {
            predicates.add(builder.equal(root.join(ETABLISSEMENT).join(FOURNISSEUR).get(ID), criteria.getIdFournisseur()));
        }

        if (criteria.getIdOrganisme() != null) {
            predicates.add(builder.equal(root.join(CONTRAT).join(SERVICE).join(ORGANISME).get(ID), criteria.getIdOrganisme()));
        }

        return predicates;
    }
}
