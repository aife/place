package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ContactReferant;
import com.atexo.execution.server.model.ContactReferantId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactReferentRepository extends JpaRepository<ContactReferant, ContactReferantId> {

}
