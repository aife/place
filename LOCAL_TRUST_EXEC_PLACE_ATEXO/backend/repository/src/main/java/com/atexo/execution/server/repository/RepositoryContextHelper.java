package com.atexo.execution.server.repository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component("repositoryContextHelper")
public class RepositoryContextHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static <T> T getBeanByType(Class<T> requiredType) {
		return applicationContext.getBean(requiredType);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        RepositoryContextHelper.applicationContext = applicationContext;
    }
}
