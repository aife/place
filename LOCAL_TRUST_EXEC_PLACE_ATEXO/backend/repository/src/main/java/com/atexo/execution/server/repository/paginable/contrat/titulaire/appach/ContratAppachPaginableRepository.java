package com.atexo.execution.server.repository.paginable.contrat.titulaire.appach;

import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.paginable.PaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteriaAppach;

public interface ContratAppachPaginableRepository extends PaginableRepository<Contrat, ContratCriteriaAppach> {
    String SERVICE = "service";
    String CONTRAT = "contrat";
    String ID = "id";
    String STATUT = "statut";
    String NUMERO = "numero";
    String OBJET = "objet";
    String CONSULTATION = "consultation";

    String ORGANISME = "organisme";

    String CODE = "code";

    String ID_EXTERNE = "idExterne";
    String DATE_MODIFICATION = "dateModification";
    String INTITULE = "intitule";
    String NUMERO_LOT = "numeroLot";
    String DATE_NOTIFICATION = "dateNotification";

    String SIREN = "siren";
    String COMPLEMENT = "complement";
    String ACRONYME = "acronyme";
    String DONNNEES_ESSENTIELLES = "donneesEssentielles";

    String STATUT_PUBLICATION_DE = "statut";
    String DEFENSE_OU_SECURITE = "defenseOuSecurite";
    String ACTES = "actes";
    String CHAPEAU = "chapeau";
    String MODALITE_EXECUTIONS = "modaliteExecutions";
}
