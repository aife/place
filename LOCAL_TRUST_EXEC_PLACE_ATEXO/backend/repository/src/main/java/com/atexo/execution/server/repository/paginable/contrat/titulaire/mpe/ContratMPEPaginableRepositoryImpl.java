package com.atexo.execution.server.repository.paginable.contrat.titulaire.mpe;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.paginable.BasePaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class ContratMPEPaginableRepositoryImpl extends BasePaginableRepository<Contrat, ContratCriteria> implements ContratMPEPaginableRepository {

    private static final Logger LOG = LoggerFactory.getLogger(ContratMPEPaginableRepositoryImpl.class);

    @Override
    public <V> TypedQuery<V> requeter(
            ContratCriteria criteria,
            CriteriaQuery<V> query,
            Function<Expression<Contrat>, Expression<V>> projection,
            Sort sort
    ) {
        LOG.info("Recherche de contrat avec les critères suivants : {}", criteria);

        final var builder = em.getCriteriaBuilder();
        final var cursor = query.from(Contrat.class);
        query.select(projection.apply(cursor));
        final var predicates = new HashSet<Predicate>();

        buildContratTitulaireCriteria(criteria, builder, cursor, predicates);

        query.where(predicates.toArray(Predicate[]::new));

        if (null != sort) {
            query.orderBy(super.order(sort, builder, cursor));
        }

        return em.createQuery(query);
    }

    @Override
    public Class<Contrat> getTarget() {
        return Contrat.class;
    }

    private void buildContratTitulaireCriteria(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        var idExterne = prepareIdExterneList(criteria);
        var numeroContratList = prepareNumeroContratList(criteria);
        var statutContratList = prepareStatutContratList(criteria);
        if (!CollectionUtils.isEmpty(idExterne)) {
            predicates.add(builder.or(cursor.get(ID_EXTERNE).in(idExterne.stream().map(String::valueOf).collect(Collectors.toUnmodifiableSet())), cursor.get(ID).in(idExterne)));
        }
        if (!CollectionUtils.isEmpty(numeroContratList)) {
            predicates.add(cursor.get(NUMERO).in(numeroContratList));
        }
        if (!CollectionUtils.isEmpty(statutContratList)) {
            predicates.add(cursor.get(STATUT).in(statutContratList));
        }
        LocalDateTime dateModification = criteria.getDateModification();
        if (dateModification != null) {
            LocalDateTime start = dateModification.withHour(0).withMinute(0).withSecond(0);
            LocalDateTime end = dateModification.withHour(23).withMinute(59).withSecond(59);
            predicates.add(builder.between(cursor.get(DATE_MODIFICATION), start, end));
        }
        LocalDateTime dateModificationActes = criteria.getDateModificationActe();
        if (dateModificationActes != null) {
            LocalDateTime start = dateModificationActes.withHour(0).withMinute(0).withSecond(0);
            LocalDateTime end = dateModificationActes.withHour(23).withMinute(59).withSecond(59);
            Join<Contrat, Acte> actesFetch = cursor.join(ACTES, JoinType.INNER);
            predicates.add(builder.between(actesFetch.get(DATE_MODIFICATION), start, end));
        }
        if (criteria.getObjetContrat() != null && !criteria.getObjetContrat().isEmpty()) {
            predicates.add(builder.equal(cursor.get(OBJET), criteria.getObjetContrat()));
        }
        if (criteria.getIntitule() != null && !criteria.getIntitule().isEmpty()) {
            predicates.add(builder.equal(cursor.get(INTITULE), criteria.getIntitule()));
        }
        if (criteria.getIdExterneConsultation() != null) {
            predicates.add(builder.like(cursor.join(CONSULTATION).get(ID_EXTERNE), "%_" + criteria.getIdExterneConsultation()));
        }
        if (criteria.getIdLot() != null) {
            predicates.add(builder.equal(cursor.get(NUMERO_LOT), criteria.getIdLot()));
        }
        LocalDate dateNotificationMin = criteria.getDateNotificationMin();
        if (dateNotificationMin != null) {
            predicates.add(builder.greaterThanOrEqualTo(cursor.get(DATE_NOTIFICATION), dateNotificationMin));
        }
        LocalDate dateNotificationMax = criteria.getDateNotificationMax();
        if (dateNotificationMax != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_NOTIFICATION), dateNotificationMax));
        }

        if (criteria.getSiren() != null && !criteria.getSiren().isEmpty()) {
            predicates.add(builder.equal(cursor.get(SERVICE).get(ORGANISME).get(SIREN), criteria.getSiren()));
        }
        List<String> acronymesOrganisme = criteria.getAcronymesOrganisme();
        if (acronymesOrganisme != null && !acronymesOrganisme.isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ACRONYME).in(acronymesOrganisme));
        }
        if (criteria.getComplement() != null && !criteria.getComplement().isEmpty()) {
            predicates.add(builder.equal(cursor.get(SERVICE).get(ORGANISME).get(COMPLEMENT), criteria.getComplement()));
        }
        if (criteria.getStatutPublicationSn() != null && criteria.getStatutPublicationSn() == 1) {
            predicates.add(cursor.get(DONNNEES_ESSENTIELLES).get(STATUT_PUBLICATION_DE).in(StatutPublicationDE.A_PUBLIER));
        }
        if (criteria.getStatutPublicationSn() != null && criteria.getStatutPublicationSn() == 0) {
            predicates.add(cursor.get(DONNNEES_ESSENTIELLES).get(STATUT_PUBLICATION_DE).in(StatutPublicationDE.A_PUBLIER).not());
        }
        if (criteria.getExclusionOrganisme() != null && !criteria.getExclusionOrganisme().isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ACRONYME).in(criteria.getExclusionOrganisme()).not());
        }
        if (criteria.getIdServices() != null && !criteria.getIdServices().isEmpty()) {
            predicates.add(cursor.get(SERVICE).get(ID).in(criteria.getIdServices()));
        }
        List<Integer> idExternesService = criteria.getIdExternesService();
        if (idExternesService != null && !idExternesService.isEmpty()) {
            predicates.add(cursor.get(ID_EXTERNE).in(criteria.getIdServices()));
            predicates.add(builder.or(cursor.get(SERVICE).get(ID_EXTERNE).in(idExternesService.stream().map(String::valueOf).collect(Collectors.toUnmodifiableSet())), cursor.get(SERVICE).get(ID).in(idExternesService)));
        }
        if (criteria.getChapeau() != null) {
            predicates.add(builder.equal(cursor.get(CHAPEAU), criteria.getChapeau()));
        }
        if (criteria.getDefenseOuSecurite() != null) {
            predicates.add(builder.equal(cursor.get(DEFENSE_OU_SECURITE), criteria.getDefenseOuSecurite()));
        }
        Utilisateur utilisateur = criteria.getUtilisateur();
        if (utilisateur != null && !CollectionUtils.isEmpty(utilisateur.getOrganismesAssocies())) {
            predicates.add(cursor.get(SERVICE).get(ORGANISME).get(ID).in(utilisateur.getOrganismesAssocies().stream().map(AbstractEntity::getId).collect(Collectors.toUnmodifiableSet())));
        }
        if (criteria.getIdExternesLot() != null) {
            predicates.add(builder.equal(cursor.get(ID_EXTERNES_LOT), criteria.getIdExternesLot()));
        }
    }


    private Set<Integer> prepareIdExterneList(ContratCriteria criteria) {
        Set<Integer> idExterneList = new HashSet<>();
        if (criteria.getIdContratTitulaire() != null) {
            idExterneList.add(criteria.getIdContratTitulaire());
        }
        if (!CollectionUtils.isEmpty(criteria.getIdContratTitulaires())) {
            idExterneList.addAll(criteria.getIdContratTitulaires());
        }

        return idExterneList;
    }

    private Set<String> prepareNumeroContratList(ContratCriteria criteria) {
        Set<String> numeroContratList = new HashSet<>();
        if (criteria.getNumeroContrat() != null) {
            numeroContratList.add(criteria.getNumeroContrat());
        }
        if (!CollectionUtils.isEmpty(criteria.getNumeroContrats())) {
            numeroContratList.addAll(criteria.getNumeroContrats());
        }

        return numeroContratList;
    }

    private Set<StatutContrat> prepareStatutContratList(ContratCriteria criteria) {
        Set<StatutContrat> statutContratList = new HashSet<>();
        if (criteria.getStatutContrat() != null) {
            statutContratList.add(criteria.getStatutContrat());
        }
        if (!CollectionUtils.isEmpty(criteria.getStatutContrats())) {
            statutContratList.addAll(criteria.getStatutContrats());
        }

        return statutContratList;
    }

}
