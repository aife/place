package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.paginable.criteres.DonneesEssentiellesCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class DonneeEssentiellePaginableRepositoryImpl extends BasePaginableRepository<Contrat, DonneesEssentiellesCriteria> implements DonneeEssentiellePaginableRepository {

    private static final Logger LOG = LoggerFactory.getLogger(DonneeEssentiellePaginableRepositoryImpl.class);

    @Override
    public <V> TypedQuery<V> requeter(DonneesEssentiellesCriteria criteria, CriteriaQuery<V> query, Function<Expression<Contrat>, Expression<V>> projection, Sort sort) {
        LOG.info("Recherche de contrat/Données essentielles avec les critères suivants : {}", criteria);

        final var builder = em.getCriteriaBuilder();
        final var root = query.from(Contrat.class);

        root.join(ACTES, JoinType.LEFT);

        query.select(projection.apply(root));

        final var predicates = new HashSet<Predicate>();

        checkPerimetre(criteria, builder, root, predicates);
        addCriteria(root, criteria, builder, predicates);

        query.where(predicates.toArray(Predicate[]::new));
        if (null != sort) {
            query.orderBy(super.order(sort, builder, root));
        }

        return em.createQuery(query);
    }

    private void addCriteria(Root<Contrat> root, DonneesEssentiellesCriteria criteria, CriteriaBuilder builder, HashSet<Predicate> predicates) {
        predicates.add(builder.equal(root.get(PUBLICATION_DONNEES_ESSENTIELLES), true));
        if (criteria.getIdContrat() != null) {
            predicates.add(builder.equal(root.get(ID), criteria.getIdContrat()));
        }
        if (criteria.getIdDonneeEssentielle() != null) {
            predicates.add(builder.equal(root.join(DONNNEES_ESSENTIELLES).get(ID), criteria.getIdDonneeEssentielle()));
        }
        if (criteria.getTypesContrat() != null && !criteria.getTypesContrat().isEmpty()) {
            final var codes = criteria.getTypesContrat().stream().map(AbstractReferentielEntity::getCode).collect(Collectors.toSet());
            predicates.add(root.join(TYPE).get(CODE).in(codes));
        }
        if (criteria.getDateNotification() != null && criteria.getDateNotification().size() == 2) {
            predicates.add(builder.between(root.get(DATE_NOTIFICATION), criteria.getDateNotification().get(0), criteria.getDateNotification().get(1)));
        }
        if (criteria.getDateEnvoi() != null && criteria.getDateEnvoi().size() == 2) {
            predicates.add(builder.between(root.join(DONNNEES_ESSENTIELLES).get(DATE_PUBLICATION), criteria.getDateEnvoi().get(0), criteria.getDateEnvoi().get(1)));
        }
        if (criteria.getStatuts() != null && !criteria.getStatuts().isEmpty()) {
            predicates.add(root.join(DONNNEES_ESSENTIELLES).get(STATUT).in(criteria.getStatuts()));
        }
    }

    private void checkPerimetre(DonneesEssentiellesCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        var perimetre = criteria.getPerimetre();
        var mpeUid = Optional.ofNullable(criteria.getUtilisateur()).map(Utilisateur::getPlateforme).map(Plateforme::getMpeUid).orElse(criteria.getMpePfUid());
        if (mpeUid == null) {
            throw new IllegalArgumentException("La plateforme est obligatoire pour la recherche de contrat.");
        }
        predicates.add(builder.equal(cursor.join(PLATEFORME).get(MPE_UID), mpeUid));

        if (null == perimetre) {
            perimetre = PerimetreContrat.MOI;
        }

        switch (perimetre) {
            case TOUT:
                break;
            case SERVICE_ET_SOUS_SERVICES:
                var services = new ArrayList<Service>();
                aggreger(criteria.getUtilisateur().getService(), services);

                predicates.add(cursor.join(SERVICE).get(ID).in(services.stream().map(Service::getId).collect(Collectors.toSet())));
                break;
            case SERVICE:
                predicates.add(builder.equal(cursor.join(SERVICE).get(ID), criteria.getUtilisateur().getService().getId()));
                break;
            case MOI:
            default:
                if (criteria.getUtilisateur() != null) {
                    predicates.add(builder.equal(cursor.join(CREATEUR).get(ID), criteria.getUtilisateur().getId()));
                }
                break;

        }
    }

    private void aggreger(final Service service, final List<Service> aggregat) {
        aggregat.add(service);
        service.getServices().forEach(sousService -> aggreger(sousService, aggregat));
    }

    public Class<Contrat> getTarget() {
        return Contrat.class;
    }
}
