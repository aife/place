package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.TypeEvenement;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TypeEvenementRepository extends ReferentielRepository<TypeEvenement> {

    List<TypeEvenement> findAll(Sort sort);
}
