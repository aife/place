package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.DocumentActe;
import com.atexo.execution.server.model.DocumentContrat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DocumentActeRepository extends JpaRepository<DocumentActe, Long> {

    Optional<DocumentActe> findById( Long id );

}
