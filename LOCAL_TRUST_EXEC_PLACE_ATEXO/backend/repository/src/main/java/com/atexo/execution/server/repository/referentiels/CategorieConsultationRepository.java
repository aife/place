package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CategorieConsultation;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieConsultationRepository extends ReferentielRepository<CategorieConsultation> {

    CategorieConsultation findOneByIdExterne(String idExterne);
}
