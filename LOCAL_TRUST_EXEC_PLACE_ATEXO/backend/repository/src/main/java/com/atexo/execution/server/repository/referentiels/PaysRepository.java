package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.Pays;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaysRepository extends ReferentielRepository<Pays> {

    Pays findOneByIdExterne(String idExterne);
}
