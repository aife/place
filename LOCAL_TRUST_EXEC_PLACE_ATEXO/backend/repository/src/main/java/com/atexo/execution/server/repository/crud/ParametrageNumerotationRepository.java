package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.common.dto.NumerotationTypeEnum;
import com.atexo.execution.server.model.ParametrageNumerotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ParametrageNumerotationRepository extends JpaRepository<ParametrageNumerotation, Long> {

    ParametrageNumerotation findByTypeAndNiveauAndIdPlateformeOrganismeService(NumerotationTypeEnum type, NiveauEnum niveauEnum, Long id);
}
