package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ContratEtablissementGroupement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratEtablissementGroupementRepository extends JpaRepository<ContratEtablissementGroupement, Long> {

}
