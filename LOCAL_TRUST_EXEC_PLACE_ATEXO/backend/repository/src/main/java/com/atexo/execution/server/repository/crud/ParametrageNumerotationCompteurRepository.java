package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.server.model.ParametrageNumerotationCompteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParametrageNumerotationCompteurRepository extends JpaRepository<ParametrageNumerotationCompteur, Long> {

    ParametrageNumerotationCompteur findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(Long parametrageNumerotationChampId, NiveauEnum niveauEnum, Long id);
}
