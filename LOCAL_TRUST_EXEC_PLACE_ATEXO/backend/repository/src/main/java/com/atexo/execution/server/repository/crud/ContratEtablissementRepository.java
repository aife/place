package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ContratEtablissement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratEtablissementRepository extends JpaRepository<ContratEtablissement, Long> {

    List<ContratEtablissement> findByContratId(Long contratId);

    List<ContratEtablissement> findByContratIdAndCommanditaireId(Long contratId, Long etablissementCommanditaireId);

    // suppression des sous traitant d'un mandataire
    void deleteByContratIdAndCommanditaireId(Long contratId, Long etablissementCommanditaireId);

    Optional<ContratEtablissement> findFirstByContratIdAndEtablissementId(Long contratId, Long etablissementId);

    Optional<ContratEtablissement> findFirstByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<ContratEtablissement> findByCommanditaireId(Long etablissementCommanditaireId);

    List<ContratEtablissement> findByEtablissementFournisseurIdAndContratServiceOrganismeId(Long idFournisseur, Long idOrganisme);

    List<ContratEtablissement> findByEtablissementFournisseurId(Long idFournisseur);

}
