package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.TechniqueAchat;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TechniqueAchatRepository extends ReferentielRepository<TechniqueAchat> {
    Optional<TechniqueAchat> findFirstByLabel(String label);
}
