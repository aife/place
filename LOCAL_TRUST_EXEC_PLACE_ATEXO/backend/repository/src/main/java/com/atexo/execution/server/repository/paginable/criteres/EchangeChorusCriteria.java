package com.atexo.execution.server.repository.paginable.criteres;

public class EchangeChorusCriteria {
    private final Long acte;
    private final Long contrat;

    public EchangeChorusCriteria(Long contrat, Long acte) {
        this.acte = acte;
        this.contrat = contrat;
    }

    public Long getContrat() {
        return contrat;
    }

    public Long getActe() {
        return acte;
    }

    public static class Builder {
        private Long acte;
        private Long contrat;

        public Builder contrat(Long contrat) {
            this.contrat = contrat;

            return this;
        }

        public Builder acte(Long acte) {
            this.acte = acte;

            return this;
        }


        public EchangeChorusCriteria build() {
            return new EchangeChorusCriteria(contrat, acte);
        }

    }
}
