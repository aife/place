package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Habilitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HabilitationRepository extends JpaRepository<Habilitation, Long> {
    List<Habilitation> findAllByParDefaut(boolean parDefaut);

    Optional<Habilitation> findOneByIdExterne(String codeExterne);

    Optional<Habilitation> findByCode(String code);
}
