package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CodeAchat;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeAchatRepository extends ReferentielRepository<CodeAchat> {

    CodeAchat findOneByIdExterne(String idExterne);

}
