package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.paginable.criteres.DonneesEssentiellesCriteria;

public interface DonneeEssentiellePaginableRepository extends PaginableRepository<Contrat, DonneesEssentiellesCriteria> {
    String ID = "id";
    String DONNNEES_ESSENTIELLES = "donneesEssentielles";
    String TYPE = "type";
    String CODE = "code";
    String DATE_NOTIFICATION = "dateNotification";
    String DATE_PUBLICATION = "datePublication";
    String STATUT = "statut";
    String PLATEFORME = "plateforme";
    String MPE_UID = "mpeUid";
    String SERVICE = "service";
    String CREATEUR = "createur";
    String PUBLICATION_DONNEES_ESSENTIELLES = "publicationDonneesEssentielles";
    String ACTES = "actes";
}
