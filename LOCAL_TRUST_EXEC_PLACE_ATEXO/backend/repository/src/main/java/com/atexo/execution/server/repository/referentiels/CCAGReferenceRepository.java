package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.CCAGReference;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CCAGReferenceRepository extends ReferentielRepository<CCAGReference> {
    Optional<CCAGReference> findFirstByLabel(String label);
}
