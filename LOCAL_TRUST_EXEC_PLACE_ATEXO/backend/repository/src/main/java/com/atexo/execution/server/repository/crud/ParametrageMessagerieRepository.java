package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.ParametrageMessagerieSecurisee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParametrageMessagerieRepository extends JpaRepository<ParametrageMessagerieSecurisee, Long> {

    List<ParametrageMessagerieSecurisee> findAllByPlateformeUid(String plateformeUid);
}
