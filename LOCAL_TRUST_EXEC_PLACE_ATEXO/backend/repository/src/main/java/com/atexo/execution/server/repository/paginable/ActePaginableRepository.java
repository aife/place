package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.repository.paginable.criteres.ActeCriteria;

public interface ActePaginableRepository extends PaginableRepository<Acte, ActeCriteria> {

	String OBJET = "objet";
	String NUMERO = "numero";
	String ACHETEUR = "acheteur";
	String NOM = "nom";
	String PRENOM = "prenom";
	String TITULAIRE = "titulaire";
	String SIRET = "siret";
	String SIREN = "siren";
	String FOURNISSEUR = "fournisseur";
	String DESCRIPTION = "description";
	String RAISON_SOCIALE = "raisonSociale";
	String TYPE = "type";
	String STATUT = "statut";
	String CONTRAT = "contrat";
}
