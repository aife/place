package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.TypeGroupement;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TypeGroupementRepository extends ReferentielRepository<TypeGroupement> {
    Optional<TypeGroupement> findFirstByLabel(String label);
}
