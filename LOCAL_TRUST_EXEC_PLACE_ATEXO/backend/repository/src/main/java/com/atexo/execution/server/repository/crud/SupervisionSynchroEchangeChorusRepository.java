package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.SupervisionSynchroEchangeChorus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupervisionSynchroEchangeChorusRepository extends JpaRepository<SupervisionSynchroEchangeChorus, Long> {
}
