package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.repository.paginable.criteres.EvenementCriteria;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface EvenementPaginableRepository extends PaginableRepository<Evenement, EvenementCriteria> {
    String SUIVI_REALISATION = "suiviRealisation";
    String DATE_DEBUT = "dateDebut";
    String DATE_FIN = "dateFin";
    String ETAT_EVENEMENT = "etatEvenement";
    String CONTRAT = "contrat";

    Map<Long, Long> compterEnAttente(List<Long> contrats);
}
