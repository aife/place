package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.dto.EchangeChorusCriteriaDTO;
import com.atexo.execution.server.model.EchangeChorus;
import org.springframework.stereotype.Repository;

public interface EchangeChorusPaginableRepository extends PaginableRepository<EchangeChorus, EchangeChorusCriteriaDTO> {
}
