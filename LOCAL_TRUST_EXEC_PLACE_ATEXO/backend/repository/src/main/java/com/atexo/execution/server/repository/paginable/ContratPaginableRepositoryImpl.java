package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atexo.execution.server.repository.paginable.ContratEtablissementPaginableRepository.*;
import static com.atexo.execution.server.repository.paginable.EtablissementPaginableRepository.SIRET;
import static com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository.FOURNISSEUR;
import static com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository.RAISON_SOCIALE;

@Repository
public class ContratPaginableRepositoryImpl extends BasePaginableRepository<Contrat, ContratCriteria> implements ContratPaginableRepository {

    private static final Logger LOG = LoggerFactory.getLogger(ContratPaginableRepositoryImpl.class);

    @Override
    public <V> TypedQuery<V> requeter(
            ContratCriteria criteria,
            CriteriaQuery<V> query,
            Function<Expression<Contrat>, Expression<V>> projection,
            Sort sort
    ) {
        LOG.info("Recherche de contrat avec les critères suivants : {}", criteria);

        final var builder = em.getCriteriaBuilder();
        final var cursor = query.from(Contrat.class);
        query.select(projection.apply(cursor));
        final var predicates = new HashSet<Predicate>();
        var mpeUid = Optional.ofNullable(criteria.getUtilisateur()).map(Utilisateur::getPlateforme).map(Plateforme::getMpeUid).orElse(criteria.getMpePfUid());
        if (mpeUid == null) {
            throw new IllegalArgumentException("La plateforme est obligatoire pour la recherche de contrat.");
        }
        predicates.add(builder.equal(cursor.join(PLATEFORME).get(MPE_UID), mpeUid));

        checkFavoris(criteria, cursor, predicates);

        checkPerimetre(criteria, builder, cursor, predicates);

        if (criteria.getTransverse() != null && criteria.getTransverse()) {
            predicates.add(builder.isNull(cursor.join(SERVICE).get(PARENT)));
        }

        if (criteria.getStatuts() != null && criteria.getStatuts().length > 0) {
            predicates.add(cursor.get(STATUT).in((List.of(criteria.getStatuts()))));
        }

        checkMotsCles(criteria, builder, cursor, predicates);

        Utilisateur utilisateur = criteria.getUtilisateur();
        Set<Long> idOrganismes = new HashSet<>();
        if (utilisateur != null && utilisateur.getService() != null && utilisateur.getService().getOrganisme() != null) {
            idOrganismes.add(utilisateur.getService().getOrganisme().getId());
        }
        if (utilisateur != null && !CollectionUtils.isEmpty(utilisateur.getOrganismesAssocies())) {
            idOrganismes = utilisateur.getOrganismesAssocies().stream().map(Organisme::getId).collect(Collectors.toSet());
        }

        if (!CollectionUtils.isEmpty(idOrganismes)) {
            Predicate predicateCreateur = cursor.join(SERVICE).join(ORGANISME).get(ID).in(idOrganismes);
            var organismesEligibles = cursor.join(ORGANISMES_ELIGIBLES, JoinType.LEFT);
            Predicate predicateOrganismesEligibles = organismesEligibles.get(ID).in(idOrganismes);
            Predicate predicateStatut = builder.notEqual(cursor.get(STATUT), StatutContrat.ANotifier);

            if (!CollectionUtils.isEmpty(idOrganismes)) {
                predicateOrganismesEligibles = builder.and(predicateOrganismesEligibles, predicateStatut);
            }

            predicates.add(builder.or(predicateCreateur, builder.and(predicateOrganismesEligibles, predicateStatut)));
        }
        if (criteria.getCodeAchat() != null) {
            predicates.add(builder.equal(cursor.join(CODE_ACHAT).get(CODE), criteria.getCodeAchat()));
        }

        if (criteria.getUniteFonctionnelleOperation() != null) {
            predicates.add(builder.equal(cursor.join(UNITE_FONCTIONNELLE_OPERATION).get(CODE), criteria.getUniteFonctionnelleOperation()));
        }

        List<String> identifiantsAgent = criteria.getIdentifiantsAgent();
        if (identifiantsAgent != null && !identifiantsAgent.isEmpty()) {
            // Invités ponctuels
            var invitesPonctuels = cursor.join(INVITES_PONCTUELS, JoinType.LEFT);
            Predicate predicateInvitesPonctuels = invitesPonctuels.get(IDENTIFIANT).in(identifiantsAgent);
            // Créateur
            Predicate predicateCreateur = cursor.join(CREATEUR, JoinType.LEFT).get(IDENTIFIANT).in(identifiantsAgent);
            predicates.add(builder.or(predicateInvitesPonctuels, predicateCreateur));

        }

        if (criteria.getLibellesService() != null && !criteria.getLibellesService().isEmpty()) {
            predicates.add(cursor.join(SERVICE).get(NOM).in(criteria.getLibellesService()));
        }

        checkOrganisme(criteria, builder, cursor, predicates);

        checkAttributaire(criteria, cursor, predicates);

        if (criteria.getStatutsRenouvellement() != null && !criteria.getStatutsRenouvellement().isEmpty()) {
            predicates.add(cursor.get(STATUT_RENOUVELLEMENT).in(criteria.getStatutsRenouvellement()));
        }

		if (criteria.getNumeroLot() != null && criteria.getNumeroLot() != 0) {
			predicates.add(builder.equal(cursor.get(NUMERO_LOT), criteria.getNumeroLot()));
		}

		checkReferentiels(criteria, cursor, predicates);
		checkIdsExtenes(criteria, builder, cursor, predicates);
		checkBooleans(criteria, builder, cursor, predicates);
		checkDates(criteria, builder, cursor, predicates);

        query.where(predicates.toArray(Predicate[]::new));

        if (null != sort) {
            query.orderBy(super.order(sort, builder, cursor));
        }

        return em.createQuery(query);
    }

    private void checkReferentiels(ContratCriteria criteria, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (criteria.getTypesContrat() != null && !criteria.getTypesContrat().isEmpty()) {
            final var codes = criteria.getTypesContrat().stream().map(AbstractReferentielEntity::getCode).collect(Collectors.toSet());
            predicates.add(cursor.join(TYPE_CONTRAT).get(CODE).in(codes));
        }
        if (criteria.getCodesExternesTypeContrat() != null && !criteria.getCodesExternesTypeContrat().isEmpty()) {
            predicates.add(cursor.join(TYPE_CONTRAT).get(ID_EXTERNE).in(criteria.getCodesExternesTypeContrat()));
        }
        if (criteria.getCategories() != null && !criteria.getCategories().isEmpty()) {
            final var codes = criteria.getCategories().stream().map(AbstractReferentielEntity::getCode).collect(Collectors.toSet());
            predicates.add(cursor.join(CATEGORIE).get(CODE).in(codes));
        }
        if (criteria.getCodesCategorie() != null && !criteria.getCodesCategorie().isEmpty()) {
            predicates.add(cursor.join(CATEGORIE).get(CODE).in(criteria.getCodesCategorie()));
        }
    }

    private void checkIdsExtenes(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (criteria.getIdExternesConsultation() != null && !criteria.getIdExternesConsultation().isEmpty()) {
            var listOfORPredicates = criteria.getIdExternesConsultation().stream().map(idCons -> builder.like(cursor.join(CONSULTATION).get(ID_EXTERNE), "%\\_" + idCons)).collect(Collectors.toList());
            predicates.add(builder.or(listOfORPredicates.toArray(new Predicate[0])));
        }
        if (criteria.getIdExternesOffre() != null && !criteria.getIdExternesOffre().isEmpty()) {
            predicates.add(cursor.get(ID_OFFRE).in(criteria.getIdExternesOffre()));
        }
    }

    private void checkBooleans(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (Boolean.TRUE.equals(criteria.getChapeau())) {
            predicates.add(builder.isTrue(cursor.get(CHAPEAU)));
        }
        if (Boolean.TRUE.equals(criteria.getConsiderationsEnvironnementales())) {
            predicates.add(builder.isTrue(cursor.get(CONSIDERATIONS_ENVIRONNEMENTALES)));
        }
        if (Boolean.TRUE.equals(criteria.getConsiderationsSociales())) {
            predicates.add(builder.isTrue(cursor.get(CONSIDERATIONS_SOCIALES)));
        }
    }

    private void checkDates(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (criteria.getDateDebutRenouvellementMin() != null) {
            predicates.add(builder.greaterThanOrEqualTo(cursor.get(DATE_DEBUT_ETUDE_RENOUVELLEMENT), criteria.getDateDebutRenouvellementMin()));
        }

        if (criteria.getDateDebutRenouvellementMax() != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_DEBUT_ETUDE_RENOUVELLEMENT), criteria.getDateDebutRenouvellementMax()));
        }

        if (criteria.getDateMaxFinContratMin() != null) {
            predicates.add(builder.greaterThanOrEqualTo(cursor.get(DATE_MAX_FIN_CONTRAT), criteria.getDateMaxFinContratMin()));
        }

        if (criteria.getDateMaxFinContratMax() != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_MAX_FIN_CONTRAT), criteria.getDateMaxFinContratMax()));
        }
        if (criteria.getDateModificationMin() != null) {
            predicates.add(builder.greaterThanOrEqualTo(cursor.get(DATE_MODIFICATION), criteria.getDateModificationMin()));
        }

        if (criteria.getDateModificationMax() != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_MODIFICATION), criteria.getDateModificationMax()));
        }

        if (criteria.getDateMaxFinContratMax() != null) {
            predicates.add(builder.lessThanOrEqualTo(cursor.get(DATE_MAX_FIN_CONTRAT), criteria.getDateMaxFinContratMax()));
        }
        if (criteria.getDateNotification() != null && criteria.getDateNotification().length == 2) {
            LocalDate debut = criteria.getDateNotification()[0];
            LocalDate fin = criteria.getDateNotification()[1];
            predicates.add(builder.between(cursor.get(DATE_NOTIFICATION), debut, fin));
        }
        if (criteria.getDatePrevisionnelleNotification() != null && criteria.getDatePrevisionnelleNotification().length == 2) {
            LocalDate debut = criteria.getDatePrevisionnelleNotification()[0];
            LocalDate fin = criteria.getDatePrevisionnelleNotification()[1];
            predicates.add(builder.between(cursor.get(DATE_PREVISIONNELLE_NOTIFICATION), debut, fin));
        }
        if (criteria.getDateFinContrat() != null && criteria.getDateFinContrat().length == 2) {
            LocalDate debut = criteria.getDateFinContrat()[0];
            LocalDate fin = criteria.getDateFinContrat()[1];
            predicates.add(builder.between(cursor.get(DATE_FIN_CONTRAT), debut, fin));
        }
        if (criteria.getDateMaxFinContrat() != null && criteria.getDateMaxFinContrat().length == 2) {
            LocalDate debut = criteria.getDateMaxFinContrat()[0];
            LocalDate fin = criteria.getDateMaxFinContrat()[1];
            predicates.add(builder.between(cursor.get(DATE_MAX_FIN_CONTRAT), debut, fin));
        }
    }

    private void checkAttributaire(ContratCriteria criteria, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (criteria.getSiretAttributaire() != null && !criteria.getSiretAttributaire().isEmpty()) {
            var attributaireJoin = cursor.join(ATTRIBUTAIRE);
            var etablissementJoin = attributaireJoin.join(ETABLISSEMENT);
            predicates.add(etablissementJoin.get(SIRET).in(criteria.getSiretAttributaire()));
        }
    }

    private void checkOrganisme(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (criteria.getAcronymesOrganisme() != null && !criteria.getAcronymesOrganisme().isEmpty()) {
            var serviceJoin = cursor.join(SERVICE);
            var organismeJoin = serviceJoin.join(ORGANISME);
            var organismesEligibles = cursor.join(ORGANISMES_ELIGIBLES, JoinType.LEFT);
            predicates.add(builder.or(organismeJoin.get(ACRONYME).in(criteria.getAcronymesOrganisme()), organismesEligibles.get(ACRONYME).in(criteria.getAcronymesOrganisme())));
        }
    }

    private void checkFavoris(ContratCriteria criteria, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (Boolean.TRUE.equals(criteria.getFavoris())) {
            predicates.add(cursor.join(FAVORIS).get(ID).in(criteria.getUtilisateur().getId()));
        }
    }

    private void checkMotsCles(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        if (null != criteria.getMotsCles() && !criteria.getMotsCles().isEmpty()) {
            var attributaireEtablissement = cursor.join(ATTRIBUTAIRE, JoinType.LEFT).join(ETABLISSEMENT, JoinType.LEFT);
            var fournisseur = attributaireEtablissement.join(FOURNISSEUR, JoinType.LEFT);
            var consultationLeft = cursor.join(CONSULTATION, JoinType.LEFT);
            var consultation = cursor.join(CONSULTATION);
            var contratChapeau = cursor.join(CONTRAT_CHAPEAU, JoinType.LEFT);
            var createur = cursor.join(CREATEUR, JoinType.LEFT);
            for (var token : criteria.getMotsCles().toLowerCase().split(SPACE_PATTERN)) {
                var expression = MessageFormat.format("%{0}%", token);

                LOG.debug("Recherche de contrats sur l'expression : '{}'", expression);

                predicates.add(
                        builder.or(
                                builder.like(builder.lower(cursor.get(REFERENCE_LIBRE)), expression),
                                builder.like(builder.lower(cursor.get(NUMERO)), expression),
                                builder.like(builder.lower(cursor.get(NUMERO_LONG)), expression),
                                builder.like(builder.lower(cursor.get(OBJET)), expression),
                                builder.like(builder.lower(builder.coalesce(consultationLeft.get(NUMERO), consultation.get(NUMERO))), expression),
                                builder.like(builder.lower(builder.coalesce(consultationLeft.get(OBJET), consultation.get(OBJET))), expression),
                                builder.or(
                                        builder.like(builder.lower(attributaireEtablissement.get(SIRET)), expression),
                                        builder.like(builder.lower(fournisseur.get(SIREN)), expression),
                                        builder.like(builder.lower(fournisseur.get(RAISON_SOCIALE)), expression)
                                ),
                                builder.like(builder.lower(contratChapeau.get(REFERENCE_LIBRE)), expression),
                                builder.like(builder.lower(createur.get(NOM)), expression),
                                builder.like(builder.lower(createur.get(PRENOM)), expression)
                        )
                );
            }
        }
    }

    private void checkPerimetre(ContratCriteria criteria, CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates) {
        var perimetre = criteria.getPerimetre();
        Utilisateur utilisateur = criteria.getUtilisateur();
        if (null == utilisateur) {
            return;
        }
        if (null == perimetre) {
            perimetre = PerimetreContrat.MOI;
        }

        List<Long> idServices = criteria.getIdServices();
        // Invités ponctuels
        var invitesPonctuels = cursor.join(INVITES_PONCTUELS, JoinType.LEFT);
        Predicate predicateInvitesPonctuels = builder.equal(invitesPonctuels.get(ID), utilisateur.getId());

        Service utilisateurService = utilisateur.getService();
        switch (perimetre) {
            case TOUT:
                break;
            case SERVICE_ET_SOUS_SERVICES:

                if (!CollectionUtils.isEmpty(idServices) && idServices.contains(0L)) {
                    predicates.add(predicateInvitesPonctuels);
                    break;
                }

                var services = new ArrayList<Service>();
                aggreger(utilisateurService, services);
                Set<Long> servicesId = services.stream().map(Service::getId).collect(Collectors.toSet());
                buildServicesIdPredict(builder, cursor, predicates, idServices, servicesId, predicateInvitesPonctuels);

                break;
            case SERVICE:

                if (!CollectionUtils.isEmpty(idServices) && idServices.contains(0L)) {
                    predicates.add(predicateInvitesPonctuels);
                    break;
                }
                servicesId = new HashSet<>();
                servicesId.add(utilisateurService.getId());
                if (!CollectionUtils.isEmpty(idServices)) {
                    servicesId.retainAll(idServices);
                }

                buildServicesIdPredict(builder, cursor, predicates, idServices, servicesId, predicateInvitesPonctuels);


                break;
            case MOI:
            default:
                Predicate predicateAgentCreateur = builder.equal(cursor.join(CREATEUR).get(ID), utilisateur.getId());
                predicates.add(builder.or(predicateAgentCreateur, predicateInvitesPonctuels));
                break;

        }
    }

    private static void buildServicesIdPredict(CriteriaBuilder builder, From<?, Contrat> cursor, HashSet<Predicate> predicates, List<Long> idServices, Set<Long> servicesId, Predicate predicateInvitesPonctuels) {
        if (!CollectionUtils.isEmpty(idServices)) {
            servicesId.retainAll(idServices);
        }

        // Service créateur
        Predicate predicateServiceCreateur = cursor.join(SERVICE).get(ID).in(servicesId);
        // Services éligibles
        var servicesEligibles = cursor.join(SERVICES_ELIGIBLES, JoinType.LEFT);
        Predicate predicateServiceEligible = servicesEligibles.get(ID).in(servicesId);
        Predicate predicateStatut = builder.notEqual(cursor.get(STATUT), StatutContrat.ANotifier);
        // Organismes éligibles
        var organismesEligibles = cursor.join(ORGANISMES_ELIGIBLES, JoinType.LEFT);
        var serviceOrganismesEligibles = organismesEligibles.join(SERVICES, JoinType.LEFT);
        Predicate predicateOrganismeEligible = serviceOrganismesEligibles.get(ID).in(servicesId);
        // Ajout des prédicats

        if (!CollectionUtils.isEmpty(servicesId)) {
            predicateServiceEligible = builder.and(predicateServiceEligible, predicateStatut);
        }

        predicates.add(builder.or(predicateServiceCreateur, predicateServiceEligible, predicateOrganismeEligible, predicateInvitesPonctuels));
    }

    @Override
    public Class<Contrat> getTarget() {
        return Contrat.class;
    }

    private void aggreger(final Service service, final List<Service> aggregat) {
        aggregat.add(service);
        service.getServices().forEach(sousService -> aggreger(sousService, aggregat));
    }
}
