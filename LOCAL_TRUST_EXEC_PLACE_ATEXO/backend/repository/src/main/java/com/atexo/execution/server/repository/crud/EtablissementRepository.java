package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Etablissement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EtablissementRepository extends JpaRepository<Etablissement, Long> {

    Optional<Etablissement> findFirstByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    Etablissement findOneById(long id);

    Etablissement findOneBySiret(String siret);

    Optional<Etablissement> findFirstBySiret(String siret);

    List<Etablissement> findAllBySiretAndPlateformeId(String siret, Long plateformeId);

    Optional<Etablissement> findByIdExterne(String idExterne);
}
