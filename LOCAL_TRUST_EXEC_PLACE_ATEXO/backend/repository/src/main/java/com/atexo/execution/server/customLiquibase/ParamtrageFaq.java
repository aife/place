package com.atexo.execution.server.customLiquibase;

import liquibase.Scope;
import liquibase.change.custom.CustomSqlChange;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.ArrayList;

@Slf4j
public class ParamtrageFaq extends AbstractChangeLog implements CustomTaskChange, CustomSqlChange {

    @Override
    public void execute(Database database) {
        Scope.getCurrentScope().getLog(getClass()).info("Setting paramétrage FAQ");
    }

    @Override
    public String getConfirmationMessage() {
        return "Paramétrage FAQ set up successfully.";
    }

    @Override
    public void setUp() {
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }


    @Override
    public SqlStatement[] generateStatements(Database database) {
        var properties = getProperties();
        var faqUid = properties.getProperty("faq.identifiant-plateforme");
        log.info("faqUid: {}", faqUid);
        var statemens = new ArrayList<SqlStatement>();
        statemens.add(new RawSqlStatement(generatStatement(faqUid)));
        log.debug("insertSql: {}", statemens);
        return statemens.toArray(new SqlStatement[0]);
    }

    private String generatStatement(String faqUid) {
        return MessageFormat.format(
                "update plateforme set faq_uid=''{0}'' where faq_uid is null;", faqUid);
    }
}
