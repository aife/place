package com.atexo.execution.server.repository.echanges;


import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.StatutPublicationDE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DonneesEssentiellesRepository extends JpaRepository<DonneesEssentielles, Long> {

    List<DonneesEssentielles> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    Optional<DonneesEssentielles> findFirstByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);


    @Modifying
    @Query("update DonneesEssentielles de set de.statut = :statutDE where de.id in :idsDonneesEssentielles")
    void modifierStatutDonneesEssentielles(@Param("idsDonneesEssentielles") List<Long> idsDonneesEssentielles, @Param("statutDE") StatutPublicationDE statutDE);
}
