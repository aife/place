package com.atexo.execution.server.customLiquibase;

import liquibase.Scope;
import liquibase.change.custom.CustomSqlChange;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j
public class AssociationPlateforme implements CustomTaskChange, CustomSqlChange {

    @Override
    public void execute(Database database) {
        Scope.getCurrentScope().getLog(getClass()).info("Association des entités à la plateforme en cours...");
    }

    @Override
    public String getConfirmationMessage() {
        return "Association plateforme OK.";
    }

    @Override
    public void setUp() {
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }


    @Override
    public SqlStatement[] generateStatements(Database database) {
        JdbcConnection jdbcCOnnection = (JdbcConnection) database.getConnection();
        var updatePlateformeStatements = new ArrayList<SqlStatement>();
        var checkNbPlateformeStatement = "select count(*) as nbPlateforme from plateforme;";
        try {
            var nbPlateforme = jdbcCOnnection.createStatement().executeQuery(checkNbPlateformeStatement);
            if (nbPlateforme.next()) {
                if (nbPlateforme.getInt("nbPlateforme") > 1) {
                    log.error("Plusieurs plateformes sont déclarées dans la table Plateforme de exec, aucune association ne sera faite.");
                    return new SqlStatement[0];
                }
            }
            var statement = "select concat('update ',col.table_name,' set id_plateforme = (select id from plateforme);') as requete\n" +
                    "from information_schema.COLUMNS col\n" +
                    "where COLUMN_NAME='id_plateforme' and col.TABLE_SCHEMA like '%exec%';";


            var selectResult = jdbcCOnnection.createStatement().executeQuery(statement);
            while (selectResult.next()) {
                updatePlateformeStatements.add(new RawSqlStatement(selectResult.getString("requete")));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return updatePlateformeStatements.toArray(new SqlStatement[0]);
    }

}
