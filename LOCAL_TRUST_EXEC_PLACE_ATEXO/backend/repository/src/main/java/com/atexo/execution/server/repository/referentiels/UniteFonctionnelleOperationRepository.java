package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.UniteFonctionnelleOperation;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniteFonctionnelleOperationRepository extends ReferentielRepository<UniteFonctionnelleOperation> {

    UniteFonctionnelleOperation findOneByIdExterne(String idExterne);

}
