package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.RevisionPrix;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RevisionPrixRepository extends ReferentielRepository<RevisionPrix> {
    Optional<RevisionPrix> findFirstByCode(String code);
}
