package com.atexo.execution.server.repository.referentiels;

import com.atexo.execution.server.model.Pays;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeActeRepository extends ReferentielRepository<TypeActe> {
}
