package com.atexo.execution.server.repository.referentiels;


import com.atexo.execution.server.model.RetourChorus;
import com.atexo.execution.server.repository.crud.ReferentielRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RetourChorusRepository extends ReferentielRepository<RetourChorus> {
}
