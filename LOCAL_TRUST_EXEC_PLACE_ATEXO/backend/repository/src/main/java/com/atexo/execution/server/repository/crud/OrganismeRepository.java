package com.atexo.execution.server.repository.crud;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrganismeRepository extends JpaRepository<Organisme, Long> {
    Optional<Organisme> findByIdExterneAndPlateformeMpeUid(String idExterne, String plateformeUid);

    List<Organisme> findAllByPlateformeMpeUid(String plateformeUid);

    Optional<Organisme> findOneByAcronymeAndPlateformeMpeUid(String acronyme, String plateformeUid);

    @Query("select distinct o from Contrat c " +
            "join c.donneesEssentielles de " +
            "join c.service s " +
            "join s.organisme o " +
            "join o.plateforme pf " +
            "where c.publicationDonneesEssentielles = true " +
            "and pf.active = true " +
            "and de.statut = :statut " +
            "and c.dateNotification <= current_date() " +
            "and c.statut != :statutContrat ")
    Page<Organisme> findOrganismesByContratToPublish(@Param("statut") StatutPublicationDE statut, @Param("statutContrat") StatutContrat statutContrat, Pageable pageable);

    List<Organisme> findAllByPlateformeId(Long plateformeId);

    boolean existsByIdAndTypeContratsId(Long organismeId, Long typeContratId);
    boolean existsByUuidAndTypeContratsId(String uuid, Long typeContratId);
}
