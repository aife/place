package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.server.model.Fournisseur;
import com.atexo.execution.server.repository.paginable.criteres.FournisseurCriteria;


public interface FournisseurPaginableRepository extends PaginableRepository<Fournisseur, FournisseurCriteria> {
    String RAISON_SOCIALE = "raisonSociale";
    String FOURNISSEUR = "fournisseur";
    String SIREN = "siren";
    String DESCRIPTION = "description";
    String NOM = "nom";
    String PRENOM = "prenom";
    String PAYS = "pays";
    String UUID = "uuid";
}
