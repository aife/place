package com.atexo.execution.server.customLiquibase;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Properties;

@Slf4j
public abstract class AbstractChangeLog {

    protected Properties getProperties() {
        Properties properties = new Properties();
        try (InputStream input = getConfigFile()) {
            properties.load(input);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException("Error reading config-env.properties", e);
        }
    }

    private ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    private InputStream getPropertiesInputStream() {

        return getContextClassLoader().getResourceAsStream("config-env.properties");
    }

    private InputStream getConfigFile() throws IOException {
        var file = Arrays.stream(String.valueOf(System.getProperties().get("spring.config.location"))
                .split(",|;")).filter(files -> files.contains("config-env")).map(f -> f.replace("file:", "")).findFirst().orElse(null);
        if (file != null) {
            log.info("Reading config-env.properties from {}", file);
            var path = Path.of(file);
            if (Files.exists(path)) {
                return Files.newInputStream(path);
            }
        }
        return getPropertiesInputStream();
    }
}
