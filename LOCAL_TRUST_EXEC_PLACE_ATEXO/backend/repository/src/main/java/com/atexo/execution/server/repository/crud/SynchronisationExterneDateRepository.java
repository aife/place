package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SynchronisationExterneDateRepository extends JpaRepository<SynchronisationExterneDate, Long> {
    Optional<SynchronisationExterneDate> findByTypeModificationAndPlateformeMpeUid(SynchronisationExterneDate.TypeModification typeModification, String plateformeUid);
}
