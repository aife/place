package com.atexo.execution.server.repository.paginable;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Consultation;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.config.AtexoMariaDBExtension;
import com.atexo.execution.server.repository.config.DatabaseAbstractStarter;
import com.atexo.execution.server.repository.crud.ConsultationRepository;
import com.atexo.execution.server.repository.crud.ContratEtablissementRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.paginable.criteres.ContratEtablissementCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.List;


public class ContratEtablissementPaginableRepositoryTest extends DatabaseAbstractStarter {

    public static final String OBJET_CONTRAT = "travaux";
    public static final String NUMERO_CONTRAT = "111";
    public static final String NUMERO_CONTRAT_1 = "222";
    public static final int ID_LOT = 100;
    public static final long ID_CONSULTATION = 1L;
    public static final int ID_EXTERNE_CONTRAT_TITULAIRE = 1;
    public static final int ID_EXTERNE_CONTRAT_TITULAIRE_1 = 2;
    public static final int ID_EXTERNE_CONTRAT_TITULAIRE_2 = 3;
    public static final long ID_CONTRAT = 1L;
    public static final long ID_CONTRAT_ETABLISSEMENT = 1139858L;
    public static final String ID_EXTERNE = "1";

    @Autowired
    private ContratEtablissementPaginableRepository contratEtablissementPaginableRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ConsultationRepository consultationRepository;
    @Autowired
    private ContratEtablissementRepository contratEtablissementRepository;
    @Autowired
    private PlateformeRepository plateformeRepository;

    @RegisterExtension
    static final AtexoMariaDBExtension MARIA_DB_EXTENSION = new AtexoMariaDBExtension("execution");

    @Test
    void tester_recuperation_contrat_etablissement() {
        //GIVEN
        insertData();

        //WHEN
        ContratEtablissementCriteria contratEtablissementCriteria = ContratEtablissementCriteria.builder()
                .idExterneContratTitulaire(ID_EXTERNE_CONTRAT_TITULAIRE)
                .idExterneContratTitulaires(List.of(ID_EXTERNE_CONTRAT_TITULAIRE_1, ID_EXTERNE_CONTRAT_TITULAIRE_2))
                .numeroContrats(List.of(NUMERO_CONTRAT, NUMERO_CONTRAT_1))
                .statutContrat(StatutContrat.EnCours)
                .statutContrats(List.of(StatutContrat.EnCours, StatutContrat.Archive))
                .objetContrat(OBJET_CONTRAT)
                .idConsultation(ID_CONSULTATION)
                .idLot(ID_LOT)
                .pfUid("pfUid")
                .build();
        String idExterne = contratEtablissementPaginableRepository.rechercher(contratEtablissementCriteria, PageRequest.of(0, 10))
                .stream()
                .findFirst()
                .map(ContratEtablissement::getIdExterne)
                .orElse(null);

        //THEN
        Assertions.assertEquals(ID_EXTERNE, idExterne);
    }

    private void insertData() {

        var plateforme = new Plateforme();
        plateforme.setMpeUid("pfUid");
        plateforme = plateformeRepository.save(plateforme);

        Consultation consultation = new Consultation();
        consultation.setId(ID_CONSULTATION);
        consultation.setPlateforme(plateforme);

        Contrat contrat = new Contrat();
        contrat.setId(ID_CONTRAT);
        contrat.setNumero(NUMERO_CONTRAT);
        contrat.setStatut(StatutContrat.Archive);
        contrat.setObjet(OBJET_CONTRAT);
        contrat.setConsultation(consultation);
        contrat.setNumeroLot(ID_LOT);
        contrat.setIdExterne(ID_EXTERNE);
        contrat.setPlateforme(plateforme);

        ContratEtablissement contratEtablissement = new ContratEtablissement();
        contratEtablissement.setId(ID_CONTRAT_ETABLISSEMENT);
        contratEtablissement.setIdExterne(ID_EXTERNE);
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setPlateforme(plateforme);
        consultationRepository.save(consultation);
        contratRepository.save(contrat);
        contratEtablissementRepository.save(contratEtablissement);
    }
}
