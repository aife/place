package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.BonCommandeType;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.repository.config.AtexoMariaDBExtension;
import com.atexo.execution.server.repository.config.DatabaseAbstractStarter;
import com.atexo.execution.server.repository.paginable.EvenementPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.EvenementCriteria;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;


public class EvenementRepositoryTest extends DatabaseAbstractStarter {


    @Autowired
    private EvenementPaginableRepository evenementPaginableRepository;

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private ContratRepository contratRepository;

    @RegisterExtension
    static final AtexoMariaDBExtension MARIA_DB_EXTENSION = new AtexoMariaDBExtension("execution");

    private Contrat contrat;
    private Evenement evenement1;
    private Evenement evenement2;
    private Evenement evenement3;

    @BeforeEach
    void setUp() {
        contrat = new Contrat();
        contrat.setReferenceLibre("ref1");
        contrat = contratRepository.save(contrat);
        evenement1 = new Evenement();
        evenement1.setAlerte(false);
        evenement1.setContrat(contrat);
        evenement1.setDateDebut(LocalDate.now().minusDays(10));
        evenement1.setDateFin(LocalDate.now().minusDays(2));
        evenement1.setEnvoiAlerte(false);
        evenement1.setPonctuel(false);
        evenement1.setSuiviRealisation(false);
        evenement1.setLibelle("evt1");
        evenement1 = evenementRepository.save(evenement1);
        evenement2 = new Evenement();
        evenement2.setAlerte(false);
        evenement2.setContrat(contrat);
        evenement2.setDateDebut(LocalDate.now().minusDays(10));
        evenement2.setDateFin(LocalDate.now().minusDays(2));
        evenement2.setEnvoiAlerte(false);
        evenement2.setPonctuel(false);
        evenement2.setSuiviRealisation(true);
        evenement2.setLibelle("evt2");
        evenement2 = evenementRepository.save(evenement2);
        evenement3 = new Evenement();
        evenement3.setAlerte(false);
        evenement3.setContrat(contrat);
        evenement3.setDateDebut(LocalDate.now().plusDays(10));
        evenement3.setDateFin(LocalDate.now().plusDays(20));
        evenement3.setEnvoiAlerte(false);
        evenement3.setPonctuel(false);
        evenement3.setSuiviRealisation(true);
        evenement3.setLibelle("evt3");
        evenement3 = evenementRepository.save(evenement3);
    }

    @Test
    public void testSaveEvenement() {
        var evenement = new Evenement();
        evenement.setAlerte(false);
        evenement.setContrat(contrat);
        evenement.setDateDebut(LocalDate.now().minusDays(10));
        evenement.setDateFin(LocalDate.now().minusDays(2));
        evenement.setEnvoiAlerte(false);
        evenement.setPonctuel(false);
        evenement.setSuiviRealisation(false);
        evenement.setLibelle("evt");
        evenement = evenementRepository.save(evenement);
        Assertions.assertEquals("evt", evenement.getLibelle());
        evenementRepository.delete(evenement);
    }

    @Test
    public void findById() {
        Optional<Evenement> result = evenementRepository.findById(evenement1.getId());
        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals("evt1", result.get().getLibelle());
    }

    @Test
    public void rechercherEnAttente() {
        EvenementCriteria criteria = new EvenementCriteria();
        criteria.setContrat(contrat.getId());
        criteria.setEtatEnAttente(true);
        Pageable pageable = PageRequest.of(0, 10);
        var page = evenementPaginableRepository.rechercher(criteria, pageable);
        Assertions.assertEquals(1, page.getTotalElements());
    }

    @Test
    public void rechercherTous() {
        EvenementCriteria criteria = new EvenementCriteria();
        criteria.setContrat(contrat.getId());
        Pageable pageable = PageRequest.of(0, 10);
        var page = evenementPaginableRepository.rechercher(criteria, pageable);
        Assertions.assertEquals(3, page.getTotalElements());
    }

    @Test
    public void rechercherBonsCommandesWhen0() {
        EvenementCriteria criteria = new EvenementCriteria();
        criteria.setContrat(contrat.getId());
        criteria.setBonsCommande(true);
        Pageable pageable = PageRequest.of(0, 10);
        var page = evenementPaginableRepository.rechercher(criteria, pageable);
        Assertions.assertEquals(0, page.getTotalElements());
    }

    @Test
    public void rechercherBonsCommandesWhen1() {
        BonCommandeType bonCommandeType = new BonCommandeType();
        bonCommandeType.setNumero("numTest");
        EvenementCriteria criteria = new EvenementCriteria();
        criteria.setContrat(contrat.getId());
        criteria.setBonsCommande(true);
        var evenement = new Evenement();
        evenement.setAlerte(false);
        evenement.setContrat(contrat);
        evenement.setDateDebut(LocalDate.now().minusDays(10));
        evenement.setDateFin(LocalDate.now().minusDays(2));
        evenement.setEnvoiAlerte(false);
        evenement.setPonctuel(false);
        evenement.setBonCommandeType(bonCommandeType);
        evenement.setSuiviRealisation(false);
        evenement.setLibelle("evt");
        evenement = evenementRepository.save(evenement);
        Pageable pageable = PageRequest.of(0, 10);
        var page = evenementPaginableRepository.rechercher(criteria, pageable);
        Assertions.assertEquals(1, page.getTotalElements());
        evenementRepository.delete(evenement);
    }

    @AfterEach
    void clear() {
        evenementRepository.delete(evenement1);
        evenementRepository.delete(evenement2);
        contratRepository.delete(contrat);
    }
}
