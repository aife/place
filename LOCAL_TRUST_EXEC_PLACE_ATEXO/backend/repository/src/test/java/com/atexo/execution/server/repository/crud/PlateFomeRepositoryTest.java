package com.atexo.execution.server.repository.crud;

import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.config.AtexoMariaDBExtension;
import com.atexo.execution.server.repository.config.DatabaseAbstractStarter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class PlateFomeRepositoryTest extends DatabaseAbstractStarter {


    @Autowired
    private PlateformeRepository plateformeRepository;

    @RegisterExtension
    static final AtexoMariaDBExtension MARIA_DB_EXTENSION = new AtexoMariaDBExtension("execution");


    @Test
    public void testUserRepository() {
        var plateforme = new Plateforme();
        plateforme.setClient("test");
        plateforme = plateformeRepository.save(plateforme);
        Assertions.assertEquals("test", plateforme.getClient());
    }

    @Test
    public void testDefaultPlateform() {

        List<Plateforme> plateformes = plateformeRepository.findAllByActiveTrue();
        Assertions.assertEquals(1, plateformes.size());
        Plateforme plateforme = plateformes.get(0);
        Assertions.assertEquals("MPE_atexo_release", plateforme.getMpeUid());
        Assertions.assertEquals("EXEC_atexo_release", plateforme.getUid());
        Assertions.assertEquals("EXEC-JENKINS-EVOLUTION", plateforme.getFaqUid());
        Assertions.assertEquals("https://mpe-release.local-trust.com", plateforme.getMpeUrl());
        Assertions.assertEquals("mpe_rec", plateforme.getMpeApiLogin());
        Assertions.assertEquals("5Sq1d1dfzdsq", plateforme.getMpeApiPassword());

    }
}
