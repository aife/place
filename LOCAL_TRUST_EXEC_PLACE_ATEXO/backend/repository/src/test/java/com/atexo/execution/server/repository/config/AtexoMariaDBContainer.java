package com.atexo.execution.server.repository.config;

import org.testcontainers.containers.MariaDBContainer;

public class AtexoMariaDBContainer extends MariaDBContainer<AtexoMariaDBContainer> {
    private static AtexoMariaDBContainer ITCustomMariaDBContainer;

    private AtexoMariaDBContainer() {
        super();
    }

    public static AtexoMariaDBContainer getInstance(String databaseName) {
        if (ITCustomMariaDBContainer != null) {
            ITCustomMariaDBContainer.stop();
        }
        ITCustomMariaDBContainer = new AtexoMariaDBContainer().withDatabaseName(databaseName).withCommand("mysqld", "--lower_case_table_names=1");;

        return ITCustomMariaDBContainer;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", ITCustomMariaDBContainer.getJdbcUrl());
        System.setProperty("DB_USERNAME", ITCustomMariaDBContainer.getUsername());
        System.setProperty("DB_PASSWORD", ITCustomMariaDBContainer.getPassword());
    }

    @Override
    public void stop() {

    }
}
