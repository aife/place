package exceptions;

public class ServiceIntrouvableException extends RuntimeException {

    protected final String message;
    public ServiceIntrouvableException(String errorMessage) {
        super(errorMessage);
        this.message = errorMessage;

    }

}
