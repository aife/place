package exceptions;

public class ContratMPEException extends RuntimeException{

    protected final String message;
    public ContratMPEException(String errorMessage) {
        super(errorMessage);
        this.message = errorMessage;

    }

}
