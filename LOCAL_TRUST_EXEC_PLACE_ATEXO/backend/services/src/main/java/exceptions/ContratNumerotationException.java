package exceptions;

public class ContratNumerotationException extends RuntimeException {

    protected final String message;
    public ContratNumerotationException(String errorMessage, Throwable err) {
        super(err);
        this.message = errorMessage;

    }

}
