package com.atexo.execution.services;

import com.atexo.execution.common.dto.ClauseDto;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.clause.Clause;

import java.util.List;
import java.util.Map;

public interface ClauseService {

    void processClauses(List<ClauseDto> clausesDto, Contrat contrat);
    void deleteAllClause(Contrat contrat);
    List<ClauseDto> transformToClauseDto(ContratType.ClausesRSE clausesRSE);

    Map<String, List<? extends Clause>> getMapClauses();
}
