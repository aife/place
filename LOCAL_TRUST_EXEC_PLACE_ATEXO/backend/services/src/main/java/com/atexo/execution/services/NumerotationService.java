package com.atexo.execution.services;

import com.atexo.execution.common.dto.NumerotationTypeEnum;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.NumerotationResult;

public interface NumerotationService {
    String numeroter(Contrat contrat, NumerotationResult numerotationResult, String messageErreur) throws ApplicationBusinessException;

    NumerotationResult getNumerotation(Contrat contrat, NumerotationTypeEnum type, NumerotationTypeEnum defaut) throws ApplicationBusinessException;

    void verifierUniciteNumero(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException;

    void verifierUniciteNumeroLong(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException;

    void verifierUniciteReferenceLibre(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException;

    void updateCompteur(Contrat contrat);

    void reinitialiserCompteurs();
}
