package com.atexo.execution.services.impl;

import com.atexo.execution.common.def.StatutSupervisionEchangeChorus;
import com.atexo.execution.server.model.SupervisionSynchroEchangeChorus;
import com.atexo.execution.server.repository.crud.SupervisionSynchroEchangeChorusRepository;
import com.atexo.execution.services.SupervisionEchangeChorusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class SupervisionEchangeChorusServiceImpl implements SupervisionEchangeChorusService {

    @Autowired
    private SupervisionSynchroEchangeChorusRepository supervisionSynchroEchangeChorusRepository;

    @Override
    public void updateSupervision(StatutSupervisionEchangeChorus statut, LocalDateTime dateFin, String message) {
        var supervision = new SupervisionSynchroEchangeChorus();
        supervision.setDateFin(LocalDateTime.now());
        supervision.setMessageErreur(message);
        supervision.setStatut(statut);
        supervisionSynchroEchangeChorusRepository.save(supervision);
    }
}
