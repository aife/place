package com.atexo.execution.services;

import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.ContactType;

import java.util.List;

public interface ContactsService {
    public List<ContactType> getContactsByContrat(String uuid, String pfUid) throws ApplicationBusinessException;
}
