package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.ClauseDto;
import com.atexo.execution.common.exec.mpe.ws.ClauseN2Type;
import com.atexo.execution.common.exec.mpe.ws.ClauseN3Type;
import com.atexo.execution.common.exec.mpe.ws.ClauseN4Type;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratClause;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.repository.crud.contrat.clause.ContratClauseRepository;
import com.atexo.execution.server.repository.referentiels.clause.*;
import com.atexo.execution.services.ClauseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClauseServiceImpl implements ClauseService {
    public static final String CLAUSES_N_1 = "clausesN1";
    public static final String CLAUSES_N_2 = "clausesN2";
    public static final String CLAUSES_N_3 = "clausesN3";
    public static final String CLAUSES_N_4 = "clausesN4";

    private final ClauseRepository clauseRepository;

    private final ContratClauseRepository contratClauseRepository;

    private final ClauseN1Repository clauseN1Repository;

    private final ClauseN2Repository clauseN2Repository;

    private final ClauseN3Repository clauseN3Repository;

    private final ClauseN4Repository clauseN4Repository;

    public void processClauses(List<ClauseDto> clausesDto, Contrat contrat) {
        Optional.ofNullable(clausesDto)
                .orElse(Collections.emptyList())
                .forEach(e -> prepareContratClause(e, contrat));
    }

    @Override
    public void deleteAllClause(Contrat contrat) {
      var clauses =   contratClauseRepository.findByContrat(contrat);

      log.debug("{} clauses a supprimer = {}", clauses.size(), clauses.stream().map(ContratClause::getId).collect(Collectors.toSet()));

      contratClauseRepository.deleteAll(clauses);
    }

    private void prepareContratClause(ClauseDto clauseDto, Contrat contrat) {
        ContratClause contratClause = new ContratClause();
        Optional<Clause> clause = Optional.empty();
        if (clauseDto.getParent() != null) {
            var parentClause = clauseRepository.findByCodeAndActif(clauseDto.getParent().getCode(), true);
            if (parentClause.isPresent()) {
                var parentContratClause = contratClauseRepository.findByContratAndClause(contrat, parentClause.get());
                parentContratClause.ifPresent(contratClause::setParent);
            }
            clause = clauseRepository.findByCodeAndGroupeCodeAndActif(clauseDto.getCode(), clauseDto.getParent().getCode(), true);
        }
        if (clause.isEmpty()) {
            clause = clauseRepository.findByCodeAndActif(clauseDto.getCode(), true);
        }
        clause.ifPresent(contratClause::setClause);
        contratClause.setContrat(contrat);
        contratClause.setValeur(clauseDto.getValeur());
        var c = contratClauseRepository.save(contratClause);
        contrat.getContratClauses().add(c);
    }

    public List<ClauseDto> transformToClauseDto(ContratType.ClausesRSE clausesRSE) {
        return Optional.ofNullable(clausesRSE)
                .map(ContratType.ClausesRSE::getClauses)
                .orElse(Collections.emptyList())
                .stream()
                .flatMap(clauseType -> {
                    var clauseDto = new ClauseDto(clauseType.getReferentielClauseN1(), null, null);
                    return Stream.concat(
                            Stream.of(clauseDto),
                            prepareClauseN2(clauseType.getClausesN2(), clauseDto));
                })
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<? extends Clause>> getMapClauses() {
        return Map.of(
                CLAUSES_N_1, clauseN1Repository.findByActif(true),
                CLAUSES_N_2, clauseN2Repository.findByActif(true),
                CLAUSES_N_3, clauseN3Repository.findByActif(true),
                CLAUSES_N_4, clauseN4Repository.findByActif(true)
        );
    }

    public Stream<ClauseDto> prepareClauseN2(List<ClauseN2Type> clauseN2Types, ClauseDto parent) {
        return clauseN2Types.stream()
                .flatMap(clauseType -> {
                    var clauseDto = new ClauseDto(clauseType.getReferentielClauseN2(), null, parent);
                    return Stream.concat(
                            Stream.of(clauseDto),
                            prepareClauseN3(clauseType.getClausesN3(), clauseDto)
                    );
                });
    }

    public Stream<ClauseDto> prepareClauseN3(List<ClauseN3Type> clauseN3Types, ClauseDto parent) {
        return clauseN3Types.stream()
                .flatMap(clauseType -> {
                    var clauseDto = new ClauseDto(clauseType.getReferentielClauseN3(), null, parent);
                    return Stream.concat(
                            Stream.of(clauseDto),
                            prepareClauseN4(clauseType.getClausesN4(), parent)
                    );
                });
    }

    public Stream<ClauseDto> prepareClauseN4(List<ClauseN4Type> clauseN4Types, ClauseDto parent) {
        return clauseN4Types.stream()
                .map(clauseType -> new ClauseDto(clauseType.getReferentielClauseN4(), clauseType.getValeur(), parent));
    }

}
