package com.atexo.execution.services;

import com.atexo.execution.common.dto.docgen.ChampsFusionDTO;

import java.util.List;

public interface ChampsFusionService {
    List<ChampsFusionDTO> getChampsFusion(boolean eager, boolean tree, int max);

    List<ChampsFusionDTO> getChampsFusion(boolean eager, boolean tree, int max, String domaine);
}
