package com.atexo.execution.services;

import com.atexo.execution.common.dto.DonneesEssentiellesCriteriaDTO;
import com.atexo.execution.common.dto.DonneesEssentiellesDTO;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.StatutPublicationDE;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DonneesEssentiellesService {

    void creerPublicationDE(Contrat contrat);

    DonneesEssentielles updatePublicationDE(Contrat contrat, StatutPublicationDE statutPublicationDE);

    Page<DonneesEssentiellesDTO> findDonneesEssentielles(Long utilisateurId, DonneesEssentiellesCriteriaDTO donneesEssentiellesCriteriaDTO, Pageable pageable);

    boolean updateStaut(List<Long> idsDonneesEssentielles);

    Optional<DonneesEssentielles> telechargerFichier(Long idDonneesEssentielles);
}
