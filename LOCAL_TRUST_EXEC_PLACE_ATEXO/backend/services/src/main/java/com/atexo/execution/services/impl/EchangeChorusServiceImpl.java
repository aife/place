package com.atexo.execution.services.impl;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.RetourChorus;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.repository.referentiels.StatutChorusRepository;
import com.atexo.execution.services.EchangeChorusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class EchangeChorusServiceImpl implements EchangeChorusService {

    private final EchangeChorusRepository echangeChorusRepository;
    private final EchangeChorusMapper echangeChorusMapper;
    private final StatutChorusRepository statutChorusRepository;
    private final PlateformeRepository plateformeRepository;
    private final ContratRepository contratRepository;
    private final ActeRepository acteRepository;

    @Override
    public EchangeChorusDTO updateEchangeChorus(String pfUid, EchangeChorusType echangeChorusType) throws ApplicationBusinessException {
        var idEchange = String.valueOf(echangeChorusType.getId());
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(pfUid).orElseThrow(() -> new ApplicationBusinessException("Plateforme " + pfUid + " introuvable"));

        String mpeUid = plateforme.getMpeUid();
        log.info("Mise à jour de l'échange chorus {} pour la plateforme {}", idEchange, mpeUid);
        var uuidContrat = echangeChorusType.getUuidContrat();
        var savedContrat = contratRepository.findByUuidAndPlateformeMpeUid(uuidContrat, mpeUid).orElseThrow(() -> new ApplicationBusinessException("Contrat introuvable " + uuidContrat));

        var echange = echangeChorusRepository.findByIdExterneAndPlateformeMpeUid(idEchange, mpeUid).orElseGet(
                () -> {
                    log.warn("Echange Chorus introuvable pour l'identifiant externe {} et la plateforme {}, création d'un nouvel échange", idEchange, pfUid);

                    var result = new EchangeChorus();
                    result.setPlateforme(plateforme);
                    result.setIdExterne(idEchange);
                    result.setContrat(savedContrat);

                    return result;
                }
        );

        var statut = echangeChorusType.getStatut();

        if (null == statut) {
            log.warn("L'échange CHORUS {} pour le contrat avec l'uuid {} et la plateforme {} n'a aucun statut", idEchange, mpeUid, uuidContrat);
        } else {
            log.debug("L'échange CHORUS {} pour le contrat avec l'uuid {} et la plateforme {} a le statut {}", idEchange, mpeUid, uuidContrat, statut);
            statutChorusRepository.findByIdExterne(statut).ifPresentOrElse(
                    echange::setStatut,
                    () -> log.warn("Statut CHORUS introuvable pour l'identifiant externe {}, mise à jour du statut non effectuée", statut)
            );
        }

        if ((RetourChorus.IRR.equalsIgnoreCase(echangeChorusType.getStatut())
                || RetourChorus.REJ.equalsIgnoreCase(echangeChorusType.getStatut()))) {
            echange.setErreurPublication(echangeChorusType.getErreurPublication());
        }

        //Mise à jour de l'acte
        updateStatutActeByRetourChorus(echange.getIdExterne(), plateforme.getMpeUid(), echangeChorusType);

        return echangeChorusMapper.toDto(echangeChorusRepository.save(echange));
    }

    private void updateStatutActeByRetourChorus(String idExterneEchange, String pfUid, EchangeChorusType echangeChorusType) {
        var acte = acteRepository.findByEchangeChorusIdExterneAndPlateformeMpeUid(idExterneEchange, pfUid).orElse(null);
        if (acte != null) {
            var statutActe = StatutActe.EN_ATTENTE_VALIDATION;
            switch (echangeChorusType.getStatut()) {
                case RetourChorus.COMMANDE_ID_EXTERNE:
                    statutActe = StatutActe.VALIDE;
                    break;
                case RetourChorus.IRR:
                case RetourChorus.REJ:
                    statutActe = StatutActe.EN_ERREUR;
                    break;
                default:
                    break;
            }
            log.info("L'échange chorus lié à l'acte est au statut=[{}], on passe l'acte au statut=[{}]", echangeChorusType.getStatut(), statutActe);
            acte.setStatut(statutActe);
            acteRepository.save(acte);
        }
    }
}
