package com.atexo.execution.services;

import com.atexo.execution.common.appach.model.contrat.titulaire.ContratTitulaireAppach;
import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ContratTitulaire;
import com.atexo.execution.server.model.Contrat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ContratsService {

    ContratType save(ContratType contratType, String pfUid) throws ApplicationBusinessException;

    List<ContratTitulaire> get(String pfUid, ContratTitulaireFilter contratTitulaireFilter, int page, int itemPerPage, Map<String, Map<Integer, List<String>>> mappingPlaceInput, Map<String, Map<String, String>> mappingPlaceOutput) throws ApplicationException;

    Page<Contrat> rechercher(String mpePfUid, ContratCriteriaDTO criteria, Pageable pageable) throws ApplicationException;

    ContratDTO createOrUpdateContrat(ContratDTO contratDTO, String utilisateurUuid) throws ApplicationException;

    ContratDTO updateEchangeChorus(String pfUid, EchangeChorusType echangeChorusType) throws ApplicationBusinessException;

    Page<ContratDTO> rechercher(Long utilisateurId, ContratCriteriaDTO criteriaDTO, Pageable pageable);

    Page<ContratDTO> rechercher(Long utilisateurId, String codeAchat, String uniteFonctionnelleOperation, Pageable pageable);

    ContratDTO getContratById(Long utilisateurId, Long contratId) throws ApplicationException;

    void deleteContrat(OrganismeDTO organismeDTO, Long contratId) throws ApplicationException;

    Optional<ContratDTO> getContratByIdExterne(Long utilisateurId, String idExterne);

    List<DocumentContratDTO> getDocumentsById(Long utilisateurId, Long contratId);

    List<ContratEtablissementDTO> getContratByEvenement(Long utilisateurId, Long evenementId);

    ContratDTO getContratByEvenement(Long evenementId);

    void favorite(Long utilisateurId, Long contratId) throws ApplicationException;

    ContratDTO notifier(String uuid, String pfUid) throws ApplicationException;

    ContratDTO notifierContrat(final Long utilisateurId, final Long id) throws ApplicationException;

    List<ContratEtablissementDTO> getEtablissementsById(Long contratId);

    List<ContratDTO> getContratsLies(Long id, Long contratId);

    List<ContratDTO> getContratsEnfantsByIdParent(Long id, Long contratId) throws ApplicationTechnicalException;

    Long updateAllStatus();

    List<ContratTitulaireAppach> getContratAppach(String pfUid, ContratTitulaireFilter contratTitulaireFilter, Pageable pageRequest, Map<String, Map<Integer, List<String>>> mappingPlaceInput, Map<String, Map<String, String>> mappingPlaceOutput) throws ApplicationException;

    void calculStatutContratChapeau(Contrat contratChapeau);
}
