package com.atexo.execution.services.impl;

import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.ContactType;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.ContactReferant;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.services.ContactsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactsServiceImpl implements ContactsService {

    private final ContratRepository contratRepository;
    private final ContactMapper contactMapper;

    @Override
    public List<ContactType> getContactsByContrat(String uuid, String pfUid) throws ApplicationBusinessException {
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, pfUid).orElseThrow(() -> new ApplicationBusinessException("Contrat introuvable " + uuid));
        var contacts = new HashSet<Contact>();
        Set<Contrat> contrats = contrat.getContrats();
        if (!CollectionUtils.isEmpty(contrats)) {
            contrats.forEach(c -> addContacts(c, contacts));
        }
        addContacts(contrat, contacts);
        if (CollectionUtils.isEmpty(contacts)) {
            return List.of();
        }
        Map<String, List<Contact>> list = contacts.stream()
                .filter(contact -> contact.getEmail() != null)
                .collect(Collectors.groupingBy(Contact::getEmail));

        List<Contact> contactList = list.values().stream().map(contactsMap -> contactsMap.get(0)).collect(Collectors.toList());
        return contactList.stream().map(contactMapper::toContactType).collect(Collectors.toList());
    }


    private void addContacts(Contrat contrat, Set<Contact> otherContacts) {

        ContratEtablissement attributaire = contrat.getAttributaire();
        if (attributaire == null) {
            return;
        }
        if (attributaire.getContact() != null) {
            otherContacts.add(attributaire.getContact());
        }
        Set<ContactReferant> contactReferantList = attributaire.getContactReferantList();
        if (!CollectionUtils.isEmpty(contactReferantList)) {
            otherContacts.addAll(contactReferantList.stream().map(ContactReferant::getContact).collect(Collectors.toSet()));
        }
    }
}
