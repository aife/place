package com.atexo.execution.services;

import com.atexo.execution.common.def.StatutSupervisionEchangeChorus;

import java.time.LocalDateTime;

public interface SupervisionEchangeChorusService {

    void updateSupervision(StatutSupervisionEchangeChorus statut, LocalDateTime dateFin, String message);
}
