package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.ModificationContrat;
import com.atexo.execution.server.mapper.imports.ImportMapper;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import com.atexo.execution.services.ImportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class ImportServiceImpl implements ImportService {

    public static final String NULL = "null";
    public static final String ID_CONTRAT_TITULAIRE = "id_contrat_titulaire";
    public static final String ID = "id";
    public static final String NUM_ORDRE = "num_ordre";
    public static final String DATE_CREATION = "date_creation";
    public static final String DATE_MODIFICATION = "date_modification";
    public static final String ID_AGENT = "id_agent";
    public static final String OBJET_MODIFICATION = "objet_modification";
    public static final String DATE_SIGNATURE = "date_signature";
    public static final String MONTANT = "montant";
    public static final String ID_ETABLISSEMENT = "id_etablissement";
    public static final String DUREE_MARCHE = "duree_marche";
    public static final String STATUT_PUBLICATION_SN = "statut_publication_sn";
    public static final String DATE_PUBLICATION_SN = "date_publication_sn";
    public static final String ERREUR_SN = "erreur_sn";
    public static final String DATE_MODIFICATION_SN = "date_modification_sn";
    public static final String ACT_MOD = "ACT_MOD";
    public static final String PATTERN = "{0}_{1}";
    public static final String PATTERN_DATE = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private EtablissementRepository etablissementRepository;
    @Autowired
    private ActeRepository acteRepository;
    @Autowired
    private TypeActeRepository typeActeRepository;

    @Autowired
    private ImportMapper importMapper;

    @Override
    public List<ModificationContrat> importAvenant(MultipartFile file, String plateformeUid) {
        try {
            List<ModificationContrat> modificationContrats = csvToModificationContrat(file.getInputStream());
            insertData(modificationContrats, plateformeUid);
            return modificationContrats;
        } catch (Exception e) {
            log.error("erreur eu niveau de l'import: ", e);
            return Collections.emptyList();
        }
    }


    private void insertData(List<ModificationContrat> modificationContrats, String plateformeUid) {
        Optional.ofNullable(modificationContrats)
                .orElse(Collections.emptyList())
                .forEach(modificationContrat -> {
                    var contrat = contratRepository.findByIdExterneAndPlateformeMpeUid(modificationContrat.getIdContratTitulaire(), plateformeUid).orElse(null);
                    var utilisateur = utilisateurRepository.findByUuid(MessageFormat.format(PATTERN, plateformeUid, modificationContrat.getIdAgent())).orElse(null);
                    var contact = etablissementRepository.findByIdExterne(modificationContrat.getIdEtablissement()).map(Etablissement::getContacts).orElse(Collections.emptyList()).stream().findFirst().orElse(null);
                    var typeActe = typeActeRepository.findByCode(ACT_MOD).orElse(null);
                    acteRepository.save(importMapper.modificationContratToActeModificatif(modificationContrat, contrat, utilisateur, contact, typeActe));
                });
    }

    public List<ModificationContrat> csvToModificationContrat(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.builder()
                     .setDelimiter(',')
                     .setHeader()
                     .setSkipHeaderRecord(true)
                     .build())) {

            List<ModificationContrat> modificationContrats = new ArrayList<ModificationContrat>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_DATE);

            for (CSVRecord csvRecord : csvRecords) {
                ModificationContrat modificationContrat = new ModificationContrat();
                if (!emptyValue(csvRecord.get(ID)) && StringUtils.isNumeric(csvRecord.get(ID))) {
                    modificationContrat.setId(Long.parseLong(csvRecord.get(ID)));
                }
                if (!emptyValue(csvRecord.get(ID_CONTRAT_TITULAIRE))) {
                    modificationContrat.setIdContratTitulaire(csvRecord.get(ID_CONTRAT_TITULAIRE));
                }
                if (!emptyValue(csvRecord.get(NUM_ORDRE)) && StringUtils.isNumeric(csvRecord.get(NUM_ORDRE))) {
                    modificationContrat.setNumOrdre(Integer.parseInt(csvRecord.get(NUM_ORDRE)));
                }
                if (!emptyValue(csvRecord.get(DATE_CREATION))) {
                    modificationContrat.setDateCreation(LocalDateTime.parse(csvRecord.get(DATE_CREATION), formatter));
                }
                if (!emptyValue(csvRecord.get(DATE_MODIFICATION))) {
                    modificationContrat.setDateModification(LocalDateTime.parse(csvRecord.get(DATE_MODIFICATION), formatter));
                }
                if (!emptyValue(csvRecord.get(ID_AGENT))) {
                    modificationContrat.setIdAgent(csvRecord.get(ID_AGENT));
                }
                if (!emptyValue(csvRecord.get(OBJET_MODIFICATION))) {
                    modificationContrat.setObjetModification(csvRecord.get(OBJET_MODIFICATION));
                }
                if (!emptyValue(csvRecord.get(DATE_SIGNATURE))) {
                    modificationContrat.setDateSignature(LocalDateTime.parse(csvRecord.get(DATE_SIGNATURE), formatter));
                }
                if (!emptyValue(csvRecord.get(MONTANT))) {
                    var montant = parseDouble(csvRecord.get(MONTANT));
                    if (montant != null) {
                        modificationContrat.setMontant(montant);
                    }
                }
                if (!emptyValue(csvRecord.get(ID_ETABLISSEMENT))) {
                    modificationContrat.setIdEtablissement(csvRecord.get(ID_ETABLISSEMENT));
                }
                if (!emptyValue(csvRecord.get(DUREE_MARCHE)) && StringUtils.isNumeric(csvRecord.get(DUREE_MARCHE))) {
                    modificationContrat.setDureeMarche(Integer.parseInt(csvRecord.get(DUREE_MARCHE)));
                }
                if (!emptyValue(csvRecord.get(STATUT_PUBLICATION_SN)) && StringUtils.isNumeric(csvRecord.get(STATUT_PUBLICATION_SN))) {
                    modificationContrat.setStatutPublicationSn(Integer.parseInt(csvRecord.get(STATUT_PUBLICATION_SN)));
                }
                if (!emptyValue(csvRecord.get(DATE_PUBLICATION_SN))) {
                    modificationContrat.setDatePublicationSn(LocalDateTime.parse(csvRecord.get(DATE_PUBLICATION_SN), formatter));
                }
                if (!emptyValue(csvRecord.get(ERREUR_SN))) {
                    modificationContrat.setErreurSn(csvRecord.get(ERREUR_SN));
                }
                if (!emptyValue(csvRecord.get(DATE_MODIFICATION_SN))) {
                    modificationContrat.setDateModificationSn(LocalDateTime.parse(csvRecord.get(DATE_MODIFICATION_SN), formatter));
                }
                modificationContrats.add(modificationContrat);
            }
            return modificationContrats;
        } catch (IOException e) {
            throw new RuntimeException("erreur de parsing du fichier: " + e.getMessage());
        }
    }

    private boolean emptyValue(String s) {
        return s == null || NULL.equalsIgnoreCase(s);
    }

    private Double parseDouble(String s) {
        try {
            NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
            Number number = format.parse(s);
            return number.doubleValue();
        } catch (ParseException | NullPointerException e) {
            log.error("erreur au niveau du parsing du double: ", e);
            return null;
        }

    }
}
