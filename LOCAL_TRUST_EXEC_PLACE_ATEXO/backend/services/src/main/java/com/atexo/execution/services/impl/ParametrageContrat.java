package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ParametrageNumerotationChamp;
import com.atexo.execution.server.model.ParametrageNumerotationCompteur;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationCompteurRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;


@Getter
@Setter
public class ParametrageContrat {

    private final Contrat contrat;
    private final ParametrageNumerotationCompteurRepository parametrageNumerotationCompteurRepository;
    private ParametrageNumerotationChamp ParametrageNumerotationChamp;

    public ParametrageContrat(final Contrat valeur, ParametrageNumerotationCompteurRepository parametrageNumerotationCompteurRepository) {
        this.contrat = valeur;
        this.parametrageNumerotationCompteurRepository = parametrageNumerotationCompteurRepository;
    }


    public Integer getCompteurService() {
        if (contrat.getService() == null) return null;
        ParametrageNumerotationCompteur compteur = parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(ParametrageNumerotationChamp.getId(), NiveauEnum.SERVICE, contrat.getService().getId());
        return compteur == null ? 1 : compteur.getCompteur() + 1;
    }


    public Integer getCompteurOrganisme() {
        if (contrat.getService() == null || contrat.getService().getOrganisme() == null) return null;
        ParametrageNumerotationCompteur compteur = parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(ParametrageNumerotationChamp.getId(), NiveauEnum.ORGANISME, contrat.getService().getOrganisme().getId());
        return compteur == null ? 1 : compteur.getCompteur() + 1;
    }

    public Integer getCompteurPlateforme() {
        if (contrat.getPlateforme() == null)
            return null;
        ParametrageNumerotationCompteur compteur = parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(ParametrageNumerotationChamp.getId(), NiveauEnum.PLATEFORME, contrat.getPlateforme().getId());
        return compteur == null ? 1 : compteur.getCompteur() + 1;
    }


    public Integer getCompteurMultiAttributaire() {
        if (contrat.isChapeau())
            return 0;
        Contrat chap = contrat.getContratChapeau();
        if (chap == null)
            return 1;
        return !CollectionUtils.isEmpty(chap.getContrats()) ? chap.getContrats().size() + 1 : 1;
    }


}
