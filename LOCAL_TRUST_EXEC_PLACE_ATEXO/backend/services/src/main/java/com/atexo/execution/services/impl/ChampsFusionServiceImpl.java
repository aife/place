package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.dto.docgen.ChampsFusionDTO;
import com.atexo.execution.server.repository.referentiels.ChampsFusionRepository;
import com.atexo.execution.services.ChampsFusionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class ChampsFusionServiceImpl implements ChampsFusionService {

    public static final Map<String, Class> DOMAINES = Map.of("contrat.", ContratDTO.class, "etablissement.", EtablissementDTO.class, "evenement.", EvenementDTO.class);
    private static final List<String> FIELDS_TO_IGNORE = List.of("serialVersionUID");
    private final ChampsFusionRepository champsFusionRepository;

    @Override
    public List<ChampsFusionDTO> getChampsFusion(boolean eager, boolean tree, int max) {
        List<ChampsFusionDTO> allChamps = new ArrayList<>();
        for (var entry : DOMAINES.entrySet()) {
            String key = entry.getKey();
            if (tree) {
                String champsFusion = key.replace(".", "");
                allChamps.add(ChampsFusionDTO.builder().champFusion(champsFusion).champsFusionRelatif(champsFusion).simple(false).sousChampsFusion(getChampsFusion(eager, true, max, key)).libelle(champsFusion).build());
            } else {
                allChamps.addAll(getChampsFusion(eager, false, max, key));
            }
        }
        allChamps.sort(Comparator.comparing(ChampsFusionDTO::getOrdre, Comparator.nullsFirst(Comparator.naturalOrder())));
        return allChamps;
    }

    @Override
    public List<ChampsFusionDTO> getChampsFusion(boolean eager, boolean tree, int max, String domaine) {
        var entry = DOMAINES.entrySet().stream().filter(e -> e.getKey().startsWith(domaine)).findFirst().orElseThrow(IllegalArgumentException::new);
        var champsFusion = champsFusionRepository.findAll();

        var champs = calculateChampsFusion(entry.getKey(), eager, tree, 0, 0, max, entry.getValue());
        for (var champ : champs) {
            champsFusion.stream().filter(ch -> ch.getCode().equals(champ.getChampFusion())).findFirst().ifPresent(ch -> {
                champ.setLibelle(ch.getLabel());
                champ.setShow(ch.isActif());
                champ.setFormat(ch.getFormat());
            });
            if (champ.getLibelle() == null) {
                champ.setLibelle(champ.getChampFusion());
                champ.setShow(false);
            }
        }
        champs.sort(Comparator.comparing(ChampsFusionDTO::getOrdre, Comparator.nullsFirst(Comparator.naturalOrder())));
        return champs;
    }

    private List<ChampsFusionDTO> calculateChampsFusion(String root, boolean eager, boolean tree, int order, int niveau, int max, Class clazz) {
        if (niveau > max) {
            log.debug("trop de champs de fusion");
            return new ArrayList<>();
        }
        List<ChampsFusionDTO> champsFusion = new ArrayList<>();
        for (var field : clazz.getDeclaredFields()) {
            if (!FIELDS_TO_IGNORE.contains(field.getName())) {
                order++;
                var code = root + field.getName();
                var simple = isSimple(field);
                var collection = Collection.class.isAssignableFrom(field.getType());
                var type = field.getType().getSimpleName();
                var genericType = field.getGenericType();
                type = collection ? genericType.getTypeName() : type;
                simple = simple && !collection;
                var champ = ChampsFusionDTO.builder().champsFusionRelatif(field.getName()).simple(simple).champFusion(code).libelle(null).type(type).collection(collection).build();
                    champsFusion.add(champ);
                    log.debug("champ ajouté : {}", champ);
                    if (!simple && eager) {
                        List<ChampsFusionDTO> sousChampsFusion = calculateChampsFusion(code + ".", true, tree, order * 100, niveau + 1, max, field.getType());
                        if (tree) {
                            champ.setSousChampsFusion(sousChampsFusion);
                        } else {
                            champsFusion.addAll(sousChampsFusion);
                        }
                    }

            }
        }
        return champsFusion;
    }


    private boolean isSimple(Field field) {
        return field.getType().isPrimitive() || field.getType().isEnum() || field.getType().getPackageName().startsWith("java.");
    }


}
