package com.atexo.execution.services.impl;

import com.atexo.execution.common.appach.model.contrat.titulaire.CoTraitantType;
import com.atexo.execution.common.appach.model.contrat.titulaire.ContratTitulaireAppach;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.StatutRenouvellement;
import com.atexo.execution.common.def.TypeNotification;
import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.common.exec.mpe.ws.*;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ContratTitulaire;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.common.mapper.DocumentContratMapper;
import com.atexo.execution.server.common.mapper.contrat.appach.AppachContratTitulaireMapper;
import com.atexo.execution.server.common.mapper.contrat.titulaire.ContratTitulaireMapper;
import com.atexo.execution.server.mapper.ConsultationMapperV2;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.ContratWebServiceMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.repository.paginable.ContratPaginableRepository;
import com.atexo.execution.server.repository.paginable.EvenementPaginableRepository;
import com.atexo.execution.server.repository.paginable.MontantAttributairesPaginableRepository;
import com.atexo.execution.server.repository.paginable.contrat.titulaire.appach.ContratAppachPaginableRepository;
import com.atexo.execution.server.repository.paginable.contrat.titulaire.mpe.ContratMPEPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteriaAppach;
import com.atexo.execution.server.repository.referentiels.*;
import com.atexo.execution.services.ClauseService;
import com.atexo.execution.services.ContratsService;
import com.atexo.execution.services.DonneesEssentiellesService;
import com.atexo.execution.services.NumerotationService;
import exceptions.ContratMPEException;
import exceptions.ServiceIntrouvableException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.text.StrBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atexo.execution.server.mapper.utils.MapperUtils.buildIdExterne;

@Service
@Slf4j
@RequiredArgsConstructor
public class ContratsServiceImpl implements ContratsService {
    private static final Pattern lieuExecutionPattern = Pattern.compile("\\s*\\((?<code>[^\\)]*)\\)\\s*(?<libelle>\\w*)");
    public static final String STATUT_CONTRAT = "statut-contrat";
    public static final List<String> LIST_TYPE_CONTRAT_CHAPEAU = List.of("sad", "marmu", "amu");
    private final EchangeChorusRepository echangeChorusRepository;
    Map<String, List<? extends Clause>> mapClauses = new HashMap<>();

    private final ContratRepository contratRepository;

    private final ConsultationRepository consultationRepository;

    private final ServiceRepository serviceRepository;

    private final ContratEtablissementRepository contratEtablissementRepository;

    private final TypeContratRepository typeContratRepository;

    private final CategorieConsultationRepository categorieConsultationRepository;

    private final TypeGroupementRepository typeGroupementRepository;

    private final ProcedureRepository procedureRepository;

    private final LieuExecutionRepository lieuExecutionRepository;

    private final ModaliteExecutionRepository modaliteExecutionRepository;

    private final TechniqueAchatRepository techniqueAchatRepository;

    private final TypePrixRepository typePrixRepository;

    private final ContactRepository contactRepository;

    private final EtablissementRepository etablissementRepository;

    private final FournisseurRepository fournisseurRepository;

    private final UtilisateurRepository utilisateurRepository;

    private final CPVRepository cpvRepository;

    private final OrganismeRepository organismeRepository;

    private final ContratMapper contratMapperV2;

    private final CCAGReferenceRepository ccagReferenceRepository;

    private final ContratTitulaireMapper contratTitulaireMapper;

    private final FormePrixRepository formePrixRepository;

    private final RevisionPrixRepository revisionPrixRepository;

    private final ContratPaginableRepository contratPaginableRepository;

    private final DonneesEssentiellesRepository donneesEssentiellesRepository;

    private final ConsultationMapperV2 consultationMapperV2;

    private final NumerotationService numerotationService;

    private final MontantAttributairesPaginableRepository montantAttributairesRepository;

    private final ContratMapper contratMapper;

    private final EvenementPaginableRepository evenementPaginableRepository;

    private final EvenementRepository evenementRepository;

    private final DocumentContratMapper documentContratMapper;

    private final ContratEtablissementMapperV2 contratEtablissementMapperV2;

    private final DonneesEssentiellesService donneesEssentiellesService;

    private final ClauseService clauseService;

    private final ContratWebServiceMapper contratWebServiceMapper;

    private final PlateformeRepository plateformeRepository;

    private final NatureContratConcessionRepository natureContratConcessionRepository;

    private final ContratAppachPaginableRepository contratAppachPaginableRepository;

    private final AppachContratTitulaireMapper appachContratTitulaireMapper;

    private final ContratMPEPaginableRepository contratMPEPaginableRepository;

    @PostConstruct
    void initClauses() {
        mapClauses = clauseService.getMapClauses();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ContratType save(ContratType contratType, String pfUid) throws ApplicationBusinessException {
        log.info("Début du traitement de réception du contrat MPE de reference {}", contratType.getReferenceLibre());
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(pfUid).orElseThrow(() -> new ContratMPEException("Plateforme non trouvée " + pfUid));
        Contrat contrat = contratWebServiceMapper.toContrat(contratType);
        contrat.setPlateforme(plateforme);
        contrat.setIdExterne(String.valueOf(UUID.randomUUID()));
        contrat.setChapeau(false);
        Optional.ofNullable(contratType.getCcagApplicable()).map(String::trim).flatMap(ccagReferenceRepository::findFirstByLabel).ifPresent(contrat::setCcagApplicable);
        Optional.ofNullable(contratType.getFormePrix()).map(String::trim).flatMap(formePrixRepository::findFirstByCode).ifPresent(contrat::setTypeFormePrix);
        Optional.ofNullable(contratType.getModaliteRevisionPrix()).map(String::trim).flatMap(revisionPrixRepository::findFirstByCode).ifPresent(contrat::setRevisionPrix);
        Optional.ofNullable(contratType.getNatureContratConcession()).map(NatureContratConcessionType::value).map(String::trim).flatMap(natureContratConcessionRepository::findFirstByCode).ifPresent(contrat::setNatureContratConcession);
        if (contratType.getNumeroLot() != null) {
            contrat.setNumeroLot(contratType.getNumeroLot() == 0 ? null : contratType.getNumeroLot());
        }
        addCategorieToContrat(contratType, contrat);
        addOrganismesEligiblesToContrat(contratType, contrat, pfUid);

        var idExterneService = buildIdExterne(contratType.getOrganisme(), Optional.of(contratType.getIdService()).orElse(0));

        log.info("Recherche du service pour l'id externe {} et la plateforme {}", idExterneService, plateforme.getMpeUid());

        serviceRepository.findByIdExterneAndPlateformeMpeUid(idExterneService, plateforme.getMpeUid()).ifPresentOrElse(contrat::setService, () -> {
            throw new ServiceIntrouvableException(String.format("Le service est introuvable pour  l'id externe %s et la plateforme %s", idExterneService, plateforme.getMpeUid()));
        });

        com.atexo.execution.server.model.Service service = contrat.getService();
        if (service != null)
            log.info("Service trouvé pour l'id externe {} et la plateforme {} = {}", idExterneService, plateforme.getMpeUid(), service.getId());

        addCreateurToContrat(contratType, contrat);
        contrat.setPlateforme(plateforme);
        contrat.setOffresRecues(contratType.getOffresRecues());

        addConsultationToContrat(contratType, contrat);
        log.info("Enregistrement du contrat de reference {}", contratType.getReferenceLibre());
        TypeContrat typeContrat;
        if (contratType.getType() == null || contratType.getType().getCodeExterne() == null) {
            throw new ContratMPEException("Le type de contrat est obligatoire");
        } else {
            typeContrat = typeContratRepository.findByCodeExterne(contratType.getType().getCodeExterne())
                    .orElseThrow(() -> new ContratMPEException("Le type de contrat est invalide"));
            contrat.setType(typeContrat);
        }
        calculStatutRenouvellement(contrat);
        contrat = contratRepository.save(contrat);

        Contrat contratChapeau = processContratChapeau(contratType, contrat);
        processLienAcSad(contratType, contrat, pfUid);

        clauseService.processClauses(clauseService.transformToClauseDto(contratType.getClausesRSE()), contrat);
        if (contrat.getContratClauses() != null && !contrat.getContratClauses().isEmpty()) {
            boolean considerationSociale = false;
            boolean considerationEnvironnementale = false;
            boolean responsable = false;
            for (ContratClause contratClause : contrat.getContratClauses()) {
                if (contratClause.getClause() != null && contratClause.getClause().getCode() != null && contratClause.getClause().getCode().equals(ClauseTypeEnum.CLAUSE_SOCIALE.getValue())) {
                    considerationSociale = true;
                }
                if (contratClause.getClause() != null && contratClause.getClause().getCode() != null && contratClause.getClause().getCode().equals(ClauseTypeEnum.CLAUSE_ENVIRONNEMENTALE.getValue())) {
                    considerationEnvironnementale = true;
                }
                if (considerationSociale || considerationEnvironnementale) {
                    responsable = true;
                }
            }
            contrat.setConsiderationsEnvironnementales(considerationEnvironnementale);
            contrat.setConsiderationsSociales(considerationSociale);
            contrat.setAchatResponsable(responsable);
        }
        contrat = addContratEtablissement(contratType, contrat);
        contrat.getLieuxExecution().addAll(processCodeLieuExecution(contratType.getLieuExecutions()));
        addCpvToContrat(contratType, contrat);
        if (Boolean.TRUE.equals(contratType.isPublicationDonneesEssentielles())) {
            log.info("Publication des données essentielles pour le contrat de reference {}", contratType.getReferenceLibre());
            contrat.setPublicationDonneesEssentielles(true);
            var donneesEssntielles = new DonneesEssentielles();
            donneesEssntielles.setPlateforme(contrat.getPlateforme());
            donneesEssntielles.setStatut(StatutPublicationDE.A_PUBLIER);
            contrat.setDonneesEssentielles(donneesEssentiellesRepository.save(donneesEssntielles));
        }
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setUuid(UUID.randomUUID().toString());
        if (service != null && service.isEchangesChorus() && typeContrat != null && typeContrat.getModeEchangeChorus() != null && typeContrat.getModeEchangeChorus() == 1) {
            log.warn("Le type de contrat est en échange Chorus commande, on ne numérote pas pas le contrat");
        } else {
            setNumeroContratFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_NUMERO, null);
            setNumeroLongFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_NUMERO_LONG, null);
            setReferenceLibreFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_REFERENCE_LIBRE, null);
        }
        if (contratType.getInvitesPonctuels() != null && !CollectionUtils.isEmpty(contratType.getInvitesPonctuels().getInvitePonctuel()) && contratType.getInvitesPonctuels().getInvitePonctuel().stream().anyMatch(InvitePonctuelType::isHasHabilitation)) {
            Set<Utilisateur> invites = contratType.getInvitesPonctuels().getInvitePonctuel().stream().filter(InvitePonctuelType::isHasHabilitation).map(InvitePonctuelType::getAgentId).map(id -> utilisateurRepository.findByUuid(plateforme.getMpeUid() + "_" + id).orElse(null)).filter(Objects::nonNull).collect(Collectors.toSet());
            contrat.setInvitesPonctuels(invites);
        }
        contrat = contratRepository.save(contrat);
        if (service != null && service.isEchangesChorus() && typeContrat != null && typeContrat.getModeEchangeChorus() != null && typeContrat.getModeEchangeChorus() == 1) {
            log.warn("Le type de contrat est en échange Chorus commande, on ne met pas à jour le compteur pour {}", contrat.getId());
        } else {
            numerotationService.updateCompteur(contrat);
        }
        setNbOffresRecues(contratChapeau, contratType.getOffresRecues());
        return contratWebServiceMapper.toDto(contrat, mapClauses);

    }

    private Collection<? extends LieuExecution> processCodeLieuExecution(String lieuxExecutionMPE) {
        var lieuxExecution = new HashSet<LieuExecution>();
        if (lieuxExecutionMPE == null || lieuxExecutionMPE.isEmpty()) {
            return lieuxExecution;
        }
        for (String codeLieu : lieuxExecutionMPE.split(",|;")) {
            Matcher matcher = lieuExecutionPattern.matcher(codeLieu);
            if (matcher.find()) {
                String code = matcher.group("code");
                lieuExecutionRepository.findByCode(code).ifPresent(lieuxExecution::add);
            }
        }
        return lieuxExecution;

    }

    private void processLienAcSad(ContratType contratType, Contrat contrat, String pfUid) {
        if (Arrays.asList("marsp", "msa").contains(contrat.getType().getCode())) {
            log.info("Enregistrement du lienAcSad pour le contrat de reference {}", contratType.getReferenceLibre());
            Optional<Contrat> lienAcSad = contratRepository.findByUuidAndPlateformeMpeUid(contratType.getLienAcSad(), pfUid);
            lienAcSad.ifPresent(contrat::setLienAcSad);
        }
    }

    private Contrat processContratChapeau(ContratType contratType, Contrat contrat) throws ApplicationBusinessException {
        //si le type de contrat est accord cadre multi attributaire ou SAD, il faut créer le contrat chapeau
        Contrat contratChapeau = null;
        TypeContrat type = contrat.getType();
        if (type != null && type.getCode() != null && LIST_TYPE_CONTRAT_CHAPEAU.contains(type.getCode())) {
            log.info("Enregistrement du contrat chapeau pour le contrat de reference {}", contratType.getReferenceLibre());
            var idExterneConsultation = BeanSynchroUtils.buildIdExterne(contratType.getOrganisme(), String.valueOf(contratType.getIdConsultation()));
            contratChapeau = contratRepository.findFirstByConsultationIdExterneAndNumeroLotAndChapeau(String.valueOf(idExterneConsultation), contratType.getNumeroLot(), true).orElse(null);
            boolean nouveauContratChapeau = false;
            if (contratChapeau == null) {
                nouveauContratChapeau = true;
                contratChapeau = contratWebServiceMapper.toContratChapeau(contratType, contrat);
            }

            TypeContrat typeContrat = typeContratRepository.findOneByCode(type.getCode());
            contratChapeau.setType(typeContrat);
            contratChapeau.setConsultation(contrat.getConsultation());
            if (contratType.getNumeroLot() != null ) {
                contratChapeau.setNumeroLot(contratType.getNumeroLot() == 0 ? null : contratType.getNumeroLot());
            }
            contratChapeau.setCategorie(contrat.getCategorie());
            contratChapeau = contratRepository.save(contratChapeau);
            addCreateurToContrat(contratType, contratChapeau);
            addCpvToContrat(contratType, contratChapeau);
            if (nouveauContratChapeau) {
                com.atexo.execution.server.model.Service service = contratChapeau.getService();

                setNumeroContratFromParametrage(contratChapeau, NumerotationTypeEnum.CONTRAT_CHAPEAU_NUMERO, NumerotationTypeEnum.CONTRAT_NUMERO);
                setReferenceLibreFromParametrage(contratChapeau, NumerotationTypeEnum.CONTRAT_CHAPEAU_REFERENCE_LIBRE, NumerotationTypeEnum.CONTRAT_REFERENCE_LIBRE);

                contratChapeau = contratRepository.save(contratChapeau);

                numerotationService.updateCompteur(contratChapeau);

            }
            contratChapeau.getLieuxExecution().addAll(processCodeLieuExecution(contratType.getLieuExecutions()));
            contrat.setContratChapeau(contratChapeau);
        }
        return contratChapeau;
    }


    private Contrat addContratEtablissement(ContratType contratType, Contrat contrat) {
        final var plateforme = contrat.getPlateforme();
        ContratEtablissement contratEtablissement = new ContratEtablissement();
        // remplissage du contrat établissement
        contratEtablissement.setCategorieConsultation(contrat.getCategorie());
        var idExterneTitulaire = Optional.ofNullable(contratType.getEtablissementTitulaire()).map(EtablissementType::getId).map(String::valueOf).orElse(null);
        var etablissement = etablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(idExterneTitulaire, plateforme.getMpeUid()).orElse(null);
        if (etablissement != null) {
            contratEtablissement.setEtablissement(etablissement);
        } else {
            contratEtablissement.setEtablissement(saveEtablissement(plateforme, idExterneTitulaire, contratType));

        }
        ContactType contactTitulaire = contratType.getContactTitulaire();
        if (contactTitulaire != null && StringUtils.hasText(contactTitulaire.getEmail())) {

            Integer id = contactTitulaire.getId();
            var idExterneContactBuilder = new StrBuilder();
            if (id != null) {
                idExterneContactBuilder.append(id);
            } else {
                idExterneContactBuilder.append(contactTitulaire.getEmail());
                if (StringUtils.hasText(contactTitulaire.getNom())) {
                    idExterneContactBuilder.append("_").append(contactTitulaire.getNom());
                }
                if (StringUtils.hasText(contactTitulaire.getPrenom())) {
                    idExterneContactBuilder.append("_").append(contactTitulaire.getPrenom());
                }
            }
            String idExterneContact = idExterneContactBuilder.toString();
            Contact contact = contactRepository.findFirstByIdExterneAndPlateformeMpeUidAndEmail(idExterneContact, plateforme.getMpeUid(), contactTitulaire.getEmail()).orElse(null);
            contratEtablissement.setContact(Objects.requireNonNullElseGet(contact, () -> saveContact(plateforme, idExterneContact, contratType)));
        }
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setPlateforme(plateforme);
        contratEtablissement = contratEtablissementRepository.save(contratEtablissement);


        if (!contrat.isChapeau()) {
            contrat.setAttributaire(contratEtablissement);
            // on met à jour le statut du contrat
            contrat.updateContratStatus();
            contrat = contratRepository.save(contrat);
        } else {
            contrat.setAttributaire(null);
            calculStatutContratChapeau(contrat);
        }

        return contrat;
    }


    private Contact saveContact(Plateforme plateforme, String idExterne, ContratType contratType) {
        ContactType contactType = contratType.getContactTitulaire();
        Contact contact = new Contact();
        contact.setIdExterne(idExterne);
        contact.setPlateforme(plateforme);
        contact.setActif(true);
        contact.setEmail(contactType.getEmail());
        contact.setNom(contactType.getNom());
        contact.setPrenom(contactType.getPrenom());
        contact.setTelephone(contactType.getTelephone());
        contact.setLogin(contactType.getLogin());
        contact.setInscritAnnuaireDefense(contactType.isInscritAnnuaireDefense());
        contact.setDateModificationRgpd(MapperUtils.convertToLocalDateTime(contactType.getDateModificationRgpd()));
        contact.setRgpdCommunicationPlace(contactType.isRgpdCommunicationPlace());
        contact.setRgpdEnquete(contactType.isRgpdEnquete());
        contact.setRgpdCommunication(contactType.isRgpdCommunication());
        log.info("Enregistrement d'un nouveau contact pour le contrat {}", contratType.getReferenceLibre());
        return contactRepository.save(contact);
    }

    private Etablissement saveEtablissement(Plateforme plateforme, String idExterne, ContratType contratType) {
        if (contratType.getEtablissementTitulaire() != null && contratType.getEtablissementTitulaire().getSiret() != null && contratType.getEtablissementTitulaire().getFournisseur() != null) {
            Adresse adresse = null;
            if (contratType.getEtablissementTitulaire().getAdresse() != null) {
                AdresseType adresseType = contratType.getEtablissementTitulaire().getAdresse();
                adresse = Adresse.builder()
                        .adresse(adresseType.getRue())
                        .pays(adresseType.getPays())
                        .commune(adresseType.getVille())
                        .codePostal(adresseType.getCodePostal())
                        .build();
            }
            var fournisseurType = contratType.getEtablissementTitulaire().getFournisseur();
            var idExterneFournisseur = String.valueOf(fournisseurType.getId());
            Etablissement etablissement = new Etablissement();
            etablissement.setIdExterne(idExterne);
            etablissement.setPlateforme(plateforme);
            etablissement.setAdresse(adresse);
            etablissement.setSiege(contratType.getEtablissementTitulaire().isSiege());
            etablissement.setSiret(contratType.getEtablissementTitulaire().getSiret());
            //vérifier si fournisseur déjà en BDD
            Fournisseur fournisseurSaved = fournisseurRepository.findByIdExterneAndPlateformeMpeUid(idExterneFournisseur, plateforme.getMpeUid()).orElse(null);
            if (fournisseurSaved != null) {
                etablissement.setFournisseur(fournisseurSaved);
            } else {
                Fournisseur fournisseur = new Fournisseur();
                fournisseur.setIdExterne(idExterneFournisseur);
                fournisseur.setPlateforme(plateforme);
                fournisseur.setNom(fournisseurType.getNom());
                if (fournisseurType.getRaisonSociale() != null) {
                    fournisseur.setRaisonSociale(fournisseurType.getRaisonSociale());
                } else {
                    fournisseur.setRaisonSociale(fournisseurType.getNom());
                }
                fournisseur.setSiren(fournisseurType.getSiren());
                log.info("Enregistrement d'un nouveau fournisseur de siren {}", fournisseurType.getSiren());
                etablissement.setFournisseur(fournisseurRepository.save(fournisseur));
            }
            log.info("Enregistrement d'un nouvel établissement de siret {}", contratType.getEtablissementTitulaire().getSiret());
            return etablissementRepository.save(etablissement);
        } else {
            throw new ContratMPEException("Les données envoyées par MPE concernant l'établissement titulaire sont insuffisantes.");
        }
    }


    private void addCategorieToContrat(ContratType contratType, Contrat contrat) {
        CategorieConsultation categorieContrat =
                Optional.ofNullable(contratType.getNaturePrestation())
                        .map(NaturePrestationType::value)
                        .map(categorieConsultationRepository::findOneByCode).orElse(null);
        contrat.setCategorie(categorieContrat);
    }

    private void addCreateurToContrat(ContratType contratType, Contrat contrat) {
        final var plateforme = contrat.getPlateforme();
        var idExterneCreateur = buildIdExterne(contratType.getOrganisme(), String.valueOf(contratType.getIdCreateur()));
        contrat.setIdExterneCreateur(idExterneCreateur);
        contrat.setCreateur(utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterneCreateur, plateforme.getMpeUid()).orElse(null));
    }

    private void addOrganismesEligiblesToContrat(ContratType contratType, Contrat contrat, String pfUid) {
        Set<Organisme> organismesEligibles = new HashSet<>();
        if (contratType.getOrganismesEligibles() != null && !contratType.getOrganismesEligibles().getOrganisme().isEmpty()) {
            for (OrganismeType organisme : contratType.getOrganismesEligibles().getOrganisme()) {
                organismeRepository.findOneByAcronymeAndPlateformeMpeUid(organisme.getAcronyme(), pfUid).ifPresent(organismesEligibles::add);
            }

        }
        contrat.setOrganismesEligibles(organismesEligibles);
    }

    private void addConsultationToContrat(ContratType contratType, Contrat contrat) {
        String idConsultation = (buildIdExterne("hors_passation", contratType.getOrganisme(), String.valueOf(contratType.getId())));
        if (Boolean.TRUE.equals(!contratType.isHorsPassation())) {
            idConsultation = buildIdExterne(contratType.getOrganisme(), String.valueOf(contratType.getIdConsultation()));
        }

        CategorieConsultation categorieConsultation = Optional.ofNullable(contratType.getNaturePrestation()).map(NaturePrestationType::value).map(categorieConsultationRepository::findOneByCode).orElse(null);
        Consultation consultation = consultationRepository.findByIdExterneAndPlateformeMpeUid(idConsultation, contrat.getPlateforme().getMpeUid()).orElse(null);
        if (consultation == null) {
            //creation d'une nouvelle consultation
            consultation = new Consultation();
            consultation.setIdExterne(idConsultation);

        }
        consultation.setHorsPassation(contratType.isHorsPassation());
        consultation.setCategorie(categorieConsultation);
        if (contratType.getOrigineFrance() != null) {
            consultation.setOrigineFrance(contratType.getOrigineFrance().intValue());
        }
        if (contratType.getOrigineUE() != null) {
            consultation.setOrigineUE(contratType.getOrigineUE().intValue());
        }
        if (contratType.getNumeroProjetAchat() != null) {
            consultation.setNumeroProjetAchat(contratType.getNumeroProjetAchat());
        }
        Optional.ofNullable(contratType.getTypeGroupementOperateurs()).flatMap(typeGroupementRepository::findByCode).ifPresent(consultation::setTypeGroupementOperateurs);
        Optional.ofNullable(contratType.getTypeProcedure()).flatMap(procedureRepository::findByCode).ifPresent(consultation::setProcedure);
        if (contratType.getModalitesExecutions() != null) {
            consultation.setModaliteExecutions(processModaliteExecution(contratType.getModalitesExecutions()));
        }
        if (contratType.getTechniques() != null) {
            consultation.setTechniqueAchats(processTechniqueAchat(contratType.getTechniques()));
        }
        if (contratType.getTypesPrix() != null) {
            consultation.setTypesPrix(processTypePrix(contratType.getTypesPrix()));
        }
        if (StringUtils.hasText(contratType.getReferenceConsultation())) {
            consultation.setNumero(contratType.getReferenceConsultation());
        }

        consultation.setPlateforme(contrat.getPlateforme());

        contrat.setConsultation(consultationRepository.save(consultation));

    }

    private void addCpvToContrat(ContratType contratType, Contrat contrat) {
        Set<CPV> codesCpv = new HashSet<>();
        var codes = new HashSet<String>();
        if (contratType.getCpv() != null) {
            if (contratType.getCpv().getCodePrincipal() != null) {
                codes.add(contratType.getCpv().getCodePrincipal());
            }
            if (contratType.getCpv().getCodeSecondaire1() != null) {
                codes.add(contratType.getCpv().getCodeSecondaire1());
            }
            if (contratType.getCpv().getCodeSecondaire2() != null) {
                codes.add(contratType.getCpv().getCodeSecondaire2());
            }
            if (contratType.getCpv().getCodeSecondaire3() != null) {
                codes.add(contratType.getCpv().getCodeSecondaire3());
            }
            for (String code : codes) {
                CPV cpv = cpvRepository.findByCode(code).orElseGet(() -> cpvRepository.save(new CPV(code, code, code)));
                codesCpv.add(cpv);
            }
            if (contratType.getCpv().getCodePrincipal() != null) {
                contrat.setCpvPrincipal(cpvRepository.findByCode(contratType.getCpv().getCodePrincipal()).orElse(null));
            }
            contrat.setCodesCpv(codesCpv);
        }
    }


    private Set<ModaliteExecution> processModaliteExecution(ContratType.ModalitesExecutions modalites) {
        return Optional.ofNullable(modalites)
                .map(ContratType.ModalitesExecutions::getModaliteExecution)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(ModaliteExecutionType::value)
                .map(modaliteExecutionRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<TechniqueAchat> processTechniqueAchat(ContratType.Techniques techniques) {
        return Optional.ofNullable(techniques)
                .map(ContratType.Techniques::getTechnique)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(TechniqueAchatType::value)
                .map(techniqueAchatRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<TypePrix> processTypePrix(ContratType.TypesPrix typesPrix) {
        return Optional.ofNullable(typesPrix)
                .map(ContratType.TypesPrix::getTypePrix)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(TypePrixType::value)
                .map(typePrixRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    @Override
    public List<ContratTitulaire> get(String pfUid, ContratTitulaireFilter contratTitulaireFilter, int page, int itemPerPage, Map<String, Map<Integer, List<String>>> mappingPlaceInput, Map<String, Map<String, String>> mappingPlaceOutput) throws ApplicationException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(pfUid).orElseThrow(() -> new ApplicationException("Plateforme " + pfUid + " non trouvee"));
        ContratCriteria contratCriteria = contratWebServiceMapper.contratTitulaireFiltreMPEToContratCriteria(contratTitulaireFilter, mappingPlaceInput.get(STATUT_CONTRAT));
        contratCriteria.setMpePfUid(plateforme.getMpeUid());
        return contratMPEPaginableRepository.rechercher(contratCriteria, PageRequest.of(page, itemPerPage))
                .stream()
                .map(e -> contratTitulaireMapper.contratToContratTitulaire(e, mappingPlaceOutput))
                .collect(Collectors.toList());
    }

    @Override
    public Page<Contrat> rechercher(String mpePfUid, ContratCriteriaDTO criteria, Pageable pageable) throws ApplicationException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(mpePfUid).orElseThrow(() -> new ApplicationException("Plateforme " + mpePfUid + " non trouvee"));
        utilisateurRepository.findOneByIdentifiant(mpePfUid).ifPresent(utilisateur1 -> {
            List<String> acronymesOrganisme = criteria.getAcronymesOrganisme();
            if (acronymesOrganisme == null) {
                acronymesOrganisme = new ArrayList<>();
            }
            acronymesOrganisme.retainAll(utilisateur1.getOrganismesAssocies().stream().map(Organisme::getAcronyme).collect(Collectors.toSet()));
            criteria.setAcronymesOrganisme(acronymesOrganisme.stream().distinct().collect(Collectors.toList()));
        });
        var contratCriteria = contratMapperV2.toCriteria(criteria, plateforme.getMpeUid());
        return contratPaginableRepository.rechercher(contratCriteria, pageable);
    }

    @Override
    public ContratDTO createOrUpdateContrat(final ContratDTO contratDTO, final String utilisateurUid) throws ApplicationException {
        final var typeContrat = contratDTO.getType() == null ? null : Optional.of(contratDTO.getType()).map(ValueLabelDTO::getValue).map(typeContratRepository::findOneByCode).orElse(null);
        final var categorieConsultation = Optional.ofNullable(contratDTO.getCategorie()).map(ValueLabelDTO::getValue).map(categorieConsultationRepository::findOneByCode).orElse(null);
        var utilisateur = utilisateurRepository.findByUuid(utilisateurUid).orElseThrow(() -> new ApplicationException("Utilisateur " + utilisateurUid + " non trouve"));
        var plateforme = utilisateur.getPlateforme();
        Contrat contrat;
        var chapeau = false;
        Contrat contratChapeau = null;
        LocalDateTime dateCreation = null;
        if (contratDTO.getId() != null) {
            contrat = contratRepository.findById(contratDTO.getId()).orElseThrow(() -> new ApplicationException("Contrat " + contratDTO.getId() + " non trouve"));
            chapeau = contrat.isChapeau();
            contratChapeau = contrat.getContratChapeau();
            dateCreation = contrat.getDateCreation();
        }
        contrat = contratMapperV2.fromDTOtoContrat(contratDTO, chapeau);
        if (contrat.getCreateur() == null) {
            contrat.setCreateur(utilisateur);
        }
        calculStatutRenouvellement(contrat);
        contrat.setDateCreation(dateCreation);
        contrat.setContratChapeau(contratChapeau);
        contrat.setPlateforme(plateforme);
        Optional.ofNullable(contratDTO.getTypeFormePrix()).map(ValueLabelDTO::getValue).map(String::trim).flatMap(formePrixRepository::findFirstByCode).ifPresent(contrat::setTypeFormePrix);
        Optional.ofNullable(contratDTO.getCcagApplicable()).map(ValueLabelDTO::getLabel).map(String::trim).flatMap(ccagReferenceRepository::findFirstByLabel).ifPresent(contrat::setCcagApplicable);
        Optional.ofNullable(contratDTO.getIdLienAcSad()).flatMap(contratRepository::findById).ifPresent(contrat::setLienAcSad);
        contrat.setCategorie(categorieConsultation);
        contrat.setType(typeContrat);
        contrat.setConsultation(addConsultation(contratDTO, categorieConsultation, contrat));
        utilisateurRepository.findById(contratDTO.getCreateur().getId()).ifPresent(contrat::setCreateur);
        serviceRepository.findById(contratDTO.getService().getId()).ifPresent(contrat::setService);
        if (contratDTO.getAttributaire() == null && !contratDTO.isChapeau()) {
            throw new ApplicationBusinessException("L'attributaire est obligatoire");
        }
        if (!contratDTO.isChapeau() && contratDTO.getAttributaire().getId() != null) {
            contratEtablissementRepository.findById(contratDTO.getAttributaire().getId()).ifPresent(contrat::setAttributaire);
        }

        if (!CollectionUtils.isEmpty(contratDTO.getOrganismesEligibles())) {
            contrat.setOrganismesEligibles(processOrganismesEligibles(contratDTO.getOrganismesEligibles(), plateforme.getMpeUid()));
        }
        if (!CollectionUtils.isEmpty(contratDTO.getServicesEligibles())) {
            contrat.setServicesEligibles(processServicesEligibles(contratDTO.getServicesEligibles()));
        }
        if (contratDTO.getLieuxExecution() != null) {
            contrat.setLieuxExecution(processLieuxExecution(contratDTO.getLieuxExecution()));
        }

        if (contrat.getId() != null) {
            clauseService.deleteAllClause(contrat);
            clauseService.processClauses(contratDTO.getClausesDto(), contrat);
        }

        if (Boolean.TRUE.equals(contratDTO.isPublicationDonneesEssentielles())) {
            log.info("Publication des données essentielles pour le contrat de reference {}", contratDTO.getReferenceLibre());
            contrat.setPublicationDonneesEssentielles(true);
            var donneesEssntielles = new DonneesEssentielles();
            donneesEssntielles.setStatut(StatutPublicationDE.A_PUBLIER);
            donneesEssntielles.setPlateforme(plateforme);
            contrat.setDonneesEssentielles(donneesEssentiellesRepository.save(donneesEssntielles));
        }

        utilisateurRepository.findById(contratDTO.getCreateur().getId()).ifPresent(contrat::setCreateur);
        serviceRepository.findById(contratDTO.getService().getId()).ifPresent(contrat::setService);

        contrat.updateContratStatus();
        var nouveauContrat = contrat.getId() == null;
        processCodesCPV(contratDTO, contrat);
        if (contrat.getDateMaxFinContrat() != null && contratDTO.getDureePhaseEtudeRenouvellement() != null) {
            log.info("calcul de la date d'étude de renouvellement");
            var dateDebutEtude = contrat.getDateMaxFinContrat().minusMonths(contratDTO.getDureePhaseEtudeRenouvellement());
            contrat.setDateDebutEtudeRenouvellement(dateDebutEtude);
        }
        log.info("Enregistrement du contrat de référence {}", contratDTO.getReferenceLibre());
        contrat = contratRepository.save(contrat);

        if (nouveauContrat) {
            log.info("Enregistrement de l'attributaire pour le contrat de reference {}", contratDTO.getReferenceLibre());
            contrat.setStatut(StatutContrat.ANotifier);
            processContratChapeau(contratDTO, typeContrat, contrat);

            com.atexo.execution.server.model.Service service = contrat.getService();
            if (service != null && service.isEchangesChorus() && typeContrat != null && typeContrat.getModeEchangeChorus() != null && typeContrat.getModeEchangeChorus() == 1) {
                log.warn("Le type de contrat est en échange Chorus commande, on ne numérote pas pas le contrat");
            } else {
                setNumeroContratFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_NUMERO, null);
                setNumeroLongFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_NUMERO_LONG, null);
                setReferenceLibreFromParametrage(contrat, NumerotationTypeEnum.CONTRAT_REFERENCE_LIBRE, null);
            }
            contrat = contratRepository.save(contrat);
            if (service != null && service.isEchangesChorus() && typeContrat != null && typeContrat.getModeEchangeChorus() != null && typeContrat.getModeEchangeChorus() == 1) {
                log.warn("Le type de contrat est en échange Chorus commande, on ne met pas à jour le compteur pour {}", contrat.getId());
            } else {
                numerotationService.updateCompteur(contrat);
            }
            clauseService.processClauses(contratDTO.getClausesDto(), contrat);
        }
        processAttributaire(contrat, contratDTO);
        setNbOffresRecues(contratChapeau, contratDTO.getOffresRecues());
        calculStatutContratChapeau(contrat.getContratChapeau());
        return contratMapperV2.toFull(contrat);

    }

    @Override
    public ContratDTO updateEchangeChorus(String pfUid, EchangeChorusType echangeChorusType) throws ApplicationBusinessException {
        var idEchange = String.valueOf(echangeChorusType.getId());
        var uuidContrat = echangeChorusType.getUuidContrat();
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(pfUid).orElseThrow(() -> new ApplicationBusinessException("Plateforme " + pfUid + " introuvable"));

        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuidContrat, plateforme.getMpeUid()).orElseThrow(() -> new ApplicationBusinessException("Contrat introuvable " + uuidContrat));


        if (echangeChorusType.getNumEj() != null && !echangeChorusType.getNumEj().isEmpty()) {
            log.debug("Mise à jour du numero EJ du contrat {} pour la plateforme {}, nouveau statut EJ = {}", uuidContrat, pfUid, echangeChorusType.getNumEj());

            contrat.setNumEj(echangeChorusType.getNumEj());
        }

        if (echangeChorusType.getStatutEj() != null && !echangeChorusType.getStatutEj().isEmpty()) {
            log.debug("Mise à jour du statut EJ du contrat {} pour la plateforme {}, nouveau statut EJ = {}", uuidContrat, pfUid, echangeChorusType.getStatutEj());

            contrat.setStatutEJ(echangeChorusType.getStatutEj());
        }

        if (echangeChorusType.getNumero() != null && !echangeChorusType.getNumero().isEmpty()) {
            log.debug("Mise à jour du numéro du contrat {} pour la plateforme {}, nouveau numéro = {}", uuidContrat, pfUid, echangeChorusType.getNumero());

            contrat.setNumero(echangeChorusType.getNumero());
        }

        if (echangeChorusType.getNumeroLong() != null && !echangeChorusType.getNumeroLong().isEmpty()) {
            log.debug("Mise à jour du numéro long du contrat {} pour la plateforme {}, nouveau numéro long = {}", uuidContrat, pfUid, echangeChorusType.getNumeroLong());

            contrat.setNumeroLong(echangeChorusType.getNumeroLong());
        }

        log.info("Mise à jour du contrat {} pour la plateforme {} effectuée, nouveau statut/numero EJ = {}/{}", contrat.getUuid(), pfUid, echangeChorusType.getStatutEj(), echangeChorusType.getNumEj());

        contratRepository.save(contrat);

        return contratMapperV2.toDto(contrat);
    }

    private void setNbOffresRecues(Contrat contratChapeau, Integer offresRecues) {
        if (contratChapeau != null) {
            contratChapeau.setOffresRecues(offresRecues);
            contratChapeau = contratRepository.save(contratChapeau);
            contratRepository.saveAll(contratChapeau.getContrats().stream().peek(c -> c.setOffresRecues(offresRecues)).collect(Collectors.toList()));
        }
    }

    private void processAttributaire(Contrat contrat, ContratDTO contratDTO) {
        // Attributaire
        ContratEtablissementDTO contratEtablissementDTO = contratDTO.getAttributaire();
        if (contratEtablissementDTO != null) {
            ContratEtablissement attributaire = null;
            if (contratEtablissementDTO.getId() == null) {
                // nouvel attributaire
                attributaire = new ContratEtablissement();
                attributaire.setIdExterne(contrat.getIdExterne());
                attributaire.setPlateforme(contrat.getPlateforme());
                attributaire.setCategorieConsultation(contrat.getCategorie());
                attributaire.setMontant(contratDTO.getMontant());
                attributaire.setDateNotification(MapperUtils.convertToLocalDate(contratDTO.getDateNotification()));
                attributaire.setContrat(contrat);
            } else {
                attributaire = contratEtablissementRepository.findById(contratEtablissementDTO.getId()).orElse(new ContratEtablissement());
            }
            //Recuperation de l'etablissement
            if (contratDTO.getAttributaire() != null && contratDTO.getAttributaire().getEtablissement() != null) {
                var nouvelEtablissement = etablissementRepository.findById(contratDTO.getAttributaire().getEtablissement().getId()).orElse(null);
                attributaire.setEtablissement(nouvelEtablissement);
            } else {
                log.warn("L'attributaire n'a pas d'établissement");
            }
            attributaire = contratEtablissementRepository.save(attributaire);
            contrat.setAttributaire(attributaire);
            contratRepository.save(contrat);
        } else {
            log.warn("Le contrat {} n'a pas d'attributaire", contrat.getId());
        }

    }

    private void setNumeroContratFromParametrage(Contrat contrat, NumerotationTypeEnum numero, NumerotationTypeEnum defaut) throws ApplicationBusinessException {
        NumerotationResult numerotationResult = numerotationService.getNumerotation(contrat, numero, defaut);
        if (!StringUtils.hasText(contrat.getNumero())) {
            contrat.setNumero(numerotationService.numeroter(contrat, numerotationResult, "Le numéro de contrat est vide. Veuillez contacter un administrateur"));
        }
        numerotationService.verifierUniciteNumero(contrat, numerotationResult);
    }

    private void setNumeroLongFromParametrage(Contrat contrat, NumerotationTypeEnum numero, NumerotationTypeEnum defaut) throws ApplicationBusinessException {

        NumerotationResult numerotationResult = numerotationService.getNumerotation(contrat, numero, defaut);
        if (!StringUtils.hasText(contrat.getNumeroLong())) {
            contrat.setNumeroLong(numerotationService.numeroter(contrat, numerotationResult, "Le numéro long de contrat est vide. Veuillez contacter un administrateur"));
        }
        numerotationService.verifierUniciteNumeroLong(contrat, numerotationResult);
    }

    private void setReferenceLibreFromParametrage(Contrat contrat, NumerotationTypeEnum numero, NumerotationTypeEnum defaut) throws ApplicationBusinessException {

        NumerotationResult numerotationResult = numerotationService.getNumerotation(contrat, numero, defaut);
        if (!StringUtils.hasText(contrat.getReferenceLibre())) {
            contrat.setReferenceLibre(numerotationService.numeroter(contrat, numerotationResult, "La référence libre de contrat est vide. Veuillez contacter un administrateur"));
        }
        numerotationService.verifierUniciteReferenceLibre(contrat, numerotationResult);
    }

    @Override
    public Page<ContratDTO> rechercher(final Long utilisateurId, final ContratCriteriaDTO criteriaDTO, final Pageable pageable) {

        var utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));
        var criteria = contratMapperV2.toCriteria(utilisateur, criteriaDTO);
        final var contrats = contratPaginableRepository.rechercher(
                criteria,
                pageable
        );

        List<ContratDTO> result = contrats.getContent().stream().map(new ContratMapperFunction(utilisateurId)).collect(Collectors.toList());

        processResult(result);

        return new PageImpl<>(result, pageable, contrats.getTotalElements());
    }

    @Override
    public Page<ContratDTO> rechercher(final Long utilisateurId, final String codeAchat, final String uniteFonctionnelleOperation, final Pageable pageable) {
        final Page<Contrat> contrats = contratPaginableRepository.rechercher(
                ContratCriteria.builder()
                        .codeAchat(codeAchat)
                        .uniteFonctionnelleOperation(uniteFonctionnelleOperation)
                        .build(),
                pageable
        );

        final List<ContratDTO> result = contrats.getContent().stream().map(new ContratMapperFunction(utilisateurId)).collect(Collectors.toList());
        return new PageImpl<>(result, pageable, contrats.getTotalElements());
    }


    @Override
    public ContratDTO getContratById(final Long utilisateurId, final Long contratId) throws ApplicationException {
        var utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));
        var contrat = contratRepository.findById(contratId).orElseThrow(ApplicationException::new);

        // Ajouter le nombre d'événements en attente au DTO
        var contratDTO = contratMapperV2.toFull(contrat);
        processResult(List.of(contratDTO));
        return contratDTO;
    }

    private void processResult(List<ContratDTO> result) {
        // Ajouter le nombre d'événements en attente aux DTO
        final var contratDTOIds = result.stream().map(ContratDTO::getId).collect(Collectors.toList());
        final Map<Long, Long> evenementsMap = evenementPaginableRepository.compterEnAttente(contratDTOIds);
        for (final ContratDTO contratDTO : result) {
            if (evenementsMap.containsKey(contratDTO.getId())) {
                contratDTO.setEvenementsEnAttenteCount(evenementsMap.get(contratDTO.getId()).intValue());
            }
        }

        // Ajouter la somme des montants attribués
        final Map<Long, BigDecimal> montantsMap = montantAttributairesRepository.recupererMontantsAttributaire(contratDTOIds);
        for (final ContratDTO contratDTO : result) {
            if (montantsMap.containsKey(contratDTO.getId())) {
                contratDTO.setMontantTotalAttributaires(montantsMap.get(contratDTO.getId()));
            }
        }

        // Sous-traitans
        for (final ContratDTO contratDTO : result) {
            if (contratDTO.getGroupement() != null) {
                contratDTO.setCoTraitants(contratEtablissementRepository.findByContratId(contratDTO.getId()).stream()
                        .filter(contratEtablissement -> contratEtablissement.getCommanditaire() == null)
                        .map(contratEtablissementMapperV2::toDTO)
                        .collect(Collectors.toList()));

            }
        }
        // Type Contrat
        for (final ContratDTO contratDTO : result) {
            ValueLabelDTO type = contratDTO.getType();
            type.setEntiteEligibleActif(organismeRepository.existsByUuidAndTypeContratsId(contratDTO.getOrganismeUid(), type.getId()));
            contratDTO.setType(type);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteContrat(OrganismeDTO organismeDTO, Long contratId) throws ApplicationException {
        Contrat contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat " + contratId + " non trouve"));
        Long id = contrat.getId();
        if (contrat.isChapeau()) {
            log.error("Impossible de supprimer un contrat chapeau (contrat id={})", id);
            throw new ApplicationException("Impossible de supprimer un contrat chapeau (contrat {})" + contrat.getReferenceLibre());
        }
        if (!CollectionUtils.isEmpty(contrat.getContratsLienAcSad())) {
            log.error("Impossible de supprimer un contrat lié à un contrat AcSad (contrat id={})", id);
            throw new ApplicationException("Impossible de supprimer un contrat lié à un contrat AcSad (contrat {})" + contrat.getReferenceLibre());
        }

        log.info("Suppression du contrat {}", id);

        Set<ContratEtablissement> contratEtablissements = contrat.getContratEtablissements();
        Set<EchangeChorus> echangesChorus = contrat.getEchangesChorus();

        contrat.setAttributaire(null);
        contrat.setContratEtablissements(new HashSet<>());
        contrat.setFavoris(new HashSet<>());
        contrat.setEchangesChorus(new HashSet<>());
        contrat = contratRepository.save(contrat);

        if (!CollectionUtils.isEmpty(contratEtablissements)) {
            contratEtablissementRepository.deleteAll(contratEtablissements);
        }
        if (!CollectionUtils.isEmpty(echangesChorus)) {
            echangeChorusRepository.deleteAll(echangesChorus);
        }

        contratRepository.deleteById(contratId);
        contratRepository.flush();

        if (null != contrat.getContratChapeau()) {
            var idChapeau = contrat.getContratChapeau().getId();

            contratRepository.findById(idChapeau).ifPresentOrElse(
                    contratChapeau -> {
                        if (CollectionUtils.isEmpty(contratChapeau.getContrats())) {
                            log.info("Suppresion du contrat chapeau {} car il n'a plus de contrat", id);
                            contratRepository.deleteById(contratChapeau.getId());
                        }
                    },
                    () -> log.warn("Contrat chapeau {} non trouve pour l'organisme {}", idChapeau, organismeDTO.getId())
            );
        }
    }

    @Override
    public List<ContratEtablissementDTO> getEtablissementsById(final Long contratId) {
        final Optional<Contrat> contratOpt = contratRepository.findById(contratId);
        return contratOpt.map(contrat -> contrat.getContratEtablissements().stream().map(contratEtablissementMapperV2::toDTO).collect(Collectors.toList())).orElseGet(List::of);

    }

    @Override
    public List<ContratDTO> getContratsEnfantsByIdParent(Long utilisateurId, Long contratId) throws ApplicationTechnicalException {
        var utiilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));
        final var contratChapeau = contratRepository.findById(contratId).orElseThrow(ApplicationTechnicalException::new);
        return contratChapeau.getContrats().stream().map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ContratDTO> getContratsLies(Long id, Long contratId) {
        var contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat " + contratId + " non trouve"));
        if (contrat.isChapeau()) {
            return contrat.getContrats().stream().map(contratMapper::toDto).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<DocumentContratDTO> getDocumentsById(final Long utilisateurId, final Long contratId) {
        var contrat = contratDelegateGetContratById(utilisateurId, contratId);
        return contrat.getDocuments().stream().map(documentContrat -> {
            return documentContratMapper.toDTO(documentContrat, true);
        }).collect(Collectors.toList());

    }

    @Override
    public List<ContratEtablissementDTO> getContratByEvenement(final Long utilisateurId, final Long evenementId) {
        final var contratOpt = evenementRepository.findById(evenementId).map(Evenement::getContrat).orElseThrow(() -> new IllegalArgumentException("Evenement " + evenementId + " non trouve"));
        final var contrat = contratDelegateGetContratById(utilisateurId, contratOpt.getId());
        return contrat.getContratEtablissements().stream().map(contratEtablissementMapperV2::toDTO).collect(Collectors.toList());
    }

    @Override
    public ContratDTO getContratByEvenement(Long evenementId) {
        final var contratOpt = evenementRepository.findById(evenementId).map(Evenement::getContrat).orElseThrow(() -> new IllegalArgumentException("Evenement " + evenementId + " non trouve"));
        return contratMapper.toFull(contratOpt);
    }

    @Override
    public Optional<ContratDTO> getContratByIdExterne(Long utilisateurId, String idExterne) {
        Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));

        return contratRepository.findOneByIdExterneAndServiceOrganismeId(idExterne, utilisateur.getService().getOrganisme().getId()).map(contrat -> new ContratMapperFunction(utilisateurId).apply(contrat));
    }

    @Override
    public void favorite(final Long utilisateurId, final Long contratId) throws ApplicationException {
        Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));

        var contrat = contratDelegateGetContratById(utilisateurId, contratId);


        if (contrat.getFavoris().contains(utilisateur)) {
            contrat.getFavoris().remove(utilisateur);
        } else {
            contrat.getFavoris().add(utilisateur);
        }
        contratRepository.save(contrat);
    }

    // Note : en appelant directement le WS on peut avec une seule habilitation modifier des informations

    private void processCodesCPV(ContratDTO contratDTO, Contrat contrat) {
        contrat.getCodesCpv().clear();
        Optional.ofNullable(contratDTO.getCpvPrincipal()).map(ValueLabelDTO::getValue).map(cpvRepository::findByCode).ifPresent((cpv) -> contrat.setCpvPrincipal(cpv.orElse(null)));
        contratDTO.getCodesCPV().forEach(cpv -> {
            var refCPV = cpvRepository.findByCode(cpv.getValue()).orElseGet(() -> cpvRepository.save(new CPV(cpv.getValue(), cpv.getLabel(), cpv.getValue())));
            contrat.getCodesCpv().add(refCPV);
        });
    }

    @Override
    public ContratDTO notifier(String uuid, String pfUid) throws ApplicationException {
        log.info("Notification du contrat {}", uuid);

        Contrat contrat;
        if (pfUid != null) {
            contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, pfUid).orElseThrow(() -> new ApplicationException("contrat introuvable " + uuid));
        } else {
            contrat = contratRepository.findFirstByUuid(uuid).orElseThrow(() -> new ApplicationException("contrat introuvable " + uuid));
        }
        notifierContrat(contrat);
        return contratMapper.toFull(contrat);
    }

    @Override
    public ContratDTO notifierContrat(final Long utilisateurId, final Long id) throws ApplicationException {
        log.info("Notification manuelle du contrat {} et changement du statut en clos", id);
        final var contrat = contratDelegateGetContratById(utilisateurId, id);
        contrat.setTypeNotification(TypeNotification.MANUELLE);
        notifierContrat(contrat);
        return contratMapper.toFull(contrat);
    }

    private void notifierContrat(Contrat contrat) {
        if (contrat.getStatut() == StatutContrat.ANotifier && contrat.getTypeNotification() == TypeNotification.MESSAGERIE_SECURISEE) {
            log.info("première notification du contrat {} par messagerie sécurisée", contrat.getReferenceLibre());
            contrat.setDateNotification(LocalDate.now());
        }
        if (contrat.getTypeNotification() == TypeNotification.MANUELLE && contrat.getDateNotification() == null) {
            log.info("Notification manuelle du contrat {}", contrat.getReferenceLibre());
            contrat.setDateNotification(LocalDate.now());
        }
        contrat.setStatut(StatutContrat.Notifie);
        contrat.updateContratStatus();
        donneesEssentiellesService.creerPublicationDE(contrat);
        updateDateContratChapeau(contrat);
        contratRepository.save(contrat);
        calculStatutContratChapeau(contrat.getContratChapeau());
    }

    private void processContratChapeau(ContratDTO contratDTO, TypeContrat typeContrat, Contrat contrat) throws ApplicationBusinessException {
        //si un idChapeau a été renseigné dans le front, le contrat doit être rattaché à un contrat chapeau
        Contrat contratChapeau = null;
        if (contratDTO.getIdContratChapeau() != null) {
            contratChapeau = contratRepository.findById(contratDTO.getIdContratChapeau()).orElse(null);
        } else if (LIST_TYPE_CONTRAT_CHAPEAU.contains(typeContrat.getCode())) {
            log.info("Enregistrement du contrat chapeau pour le contrat de reference {}", contratDTO.getReferenceLibre());
            contratChapeau = contratMapperV2.dtoToContratChapeau(contratDTO, contrat);
            contratChapeau = contratRepository.save(contratChapeau);
            com.atexo.execution.server.model.Service service = contratChapeau.getService();
            setNumeroContratFromParametrage(contratChapeau, NumerotationTypeEnum.CONTRAT_CHAPEAU_NUMERO, NumerotationTypeEnum.CONTRAT_NUMERO);
            setReferenceLibreFromParametrage(contratChapeau, NumerotationTypeEnum.CONTRAT_CHAPEAU_REFERENCE_LIBRE, NumerotationTypeEnum.CONTRAT_REFERENCE_LIBRE);

            contratChapeau = contratRepository.save(contratChapeau);
            numerotationService.updateCompteur(contratChapeau);

            contratChapeau = contratRepository.save(contratChapeau);
        }

        contrat.setContratChapeau(contratChapeau);
    }

    private Consultation addConsultation(ContratDTO contratDTO, CategorieConsultation categorieConsultation, Contrat contrat) {
        var consultation = contratDTO.getConsultation() == null || contratDTO.getConsultation().getId() == null ? new Consultation() : consultationRepository.findById(contratDTO.getConsultation().getId()).orElse(new Consultation());
        consultation = consultationMapperV2.fromDTOtoConsultation(contratDTO.getConsultation());
        consultation.setCategorie(categorieConsultation);
        consultation.setPlateforme(contrat.getPlateforme());
        Optional.ofNullable(contratDTO.getConsultation())
                .map(ConsultationDTO::getProcedure)
                .map(ValueLabelDTO::getValue)
                .flatMap(procedureRepository::findByCode)
                .ifPresent(consultation::setProcedure);

        if (contratDTO.getConsultation() != null && contratDTO.getConsultation().getModaliteExecutions() != null) {
            consultation.setModaliteExecutions(processModaliteExecution(contratDTO.getConsultation().getModaliteExecutions()));
        }
        if (contratDTO.getConsultation() != null && contratDTO.getConsultation().getTechniqueAchats() != null) {
            consultation.setTechniqueAchats(processTechniqueAchat(contratDTO.getConsultation().getTechniqueAchats()));
        }
        if (contratDTO.getConsultation() != null && contratDTO.getConsultation().getTypesPrix() != null) {
            consultation.setTypesPrix(processTypePrix(contratDTO.getConsultation().getTypesPrix()));
        }
        if (contratDTO.getConsultation() != null && StringUtils.hasText(contratDTO.getConsultation().getNumero())) {
            consultation.setNumero(contratDTO.getConsultation().getNumero());
        }
        log.info("Enregistrement de la consultation de numéro {} pour le contrat de référence {}", consultation.getNumero(), contratDTO.getReferenceLibre());
        consultation = consultationRepository.save(consultation);
        contrat.setConsultation(consultation);
        return consultation;
    }

    private Set<Organisme> processOrganismesEligibles(List<OrganismeDTO> organismes, String pfUid) {
        return organismes.stream()
                .map(OrganismeDTO::getAcronyme)
                .map(s -> organismeRepository.findOneByAcronymeAndPlateformeMpeUid(s, pfUid))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<com.atexo.execution.server.model.Service> processServicesEligibles(List<ServiceDTO> serviceDTOS) {
        return serviceDTOS.stream()
                .map(ServiceDTO::getId)
                .map(serviceRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<LieuExecution> processLieuxExecution(List<ValueLabelDTO> lieuxExecution) {
        return lieuxExecution.stream()
                .map(ValueLabelDTO::getValue)
                .map(lieuExecutionRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<ModaliteExecution> processModaliteExecution(List<ValueLabelDTO> modalites) {
        return modalites.stream()
                .map(ValueLabelDTO::getValue)
                .map(modaliteExecutionRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<TechniqueAchat> processTechniqueAchat(List<ValueLabelDTO> techniques) {
        return techniques.stream()
                .map(ValueLabelDTO::getValue)
                .map(techniqueAchatRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Set<TypePrix> processTypePrix(List<ValueLabelDTO> typesPrix) {
        return typesPrix.stream()
                .map(ValueLabelDTO::getValue)
                .map(typePrixRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Contrat contratDelegateGetContratById(Long idUtilisateur, Long idContrat) {
        var utilisateur = utilisateurRepository.findById(idUtilisateur).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + idUtilisateur + " non trouve"));
        return contratRepository.findById(idContrat).orElseThrow(() -> new IllegalArgumentException("Contrat " + idContrat + " non trouve"));

    }

    private class ContratMapperFunction implements Function<Contrat, ContratDTO> {
        Long utilisateurId;

        ContratMapperFunction(final Long utilisateurId) {
            super();
            this.utilisateurId = utilisateurId;
        }

        @Override
        public ContratDTO apply(final Contrat contrat) {

            final ContratDTO contratDTO = contratMapperV2.toDto(contrat);
            contratDTO.setFavori(contrat.getFavoris().stream().anyMatch(u -> u.getId().equals(utilisateurId)));
            return contratDTO;
        }
    }

    @Override
    public Long updateAllStatus() {
        log.info("Mise à jour des statuts des contrats");
        var itemsPerPage = 200;
        var totalItems = contratRepository.count();
        Long nbPages = totalItems / itemsPerPage;
        var nbCPU = Runtime.getRuntime().availableProcessors();
        var threadPool = Executors.newFixedThreadPool(nbCPU);
        for (var page = 0; page < nbPages; page++) {
            threadPool.execute(runThread(page, itemsPerPage));
        }
        try {
            threadPool.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            return -1L;
        }
        log.info("Mise à jour des statuts des contrats terminée, nombre de contrats traités : {}", totalItems);
        return totalItems;
    }

    private Runnable runThread(int page, int maxItems) {
        return () -> {
            log.info("Mise à jour des statuts des contrats page {} avec {} items", page, maxItems);
            Pageable currentPage = PageRequest.of(page, maxItems);
            Page<Contrat> contrats = contratRepository.findAll(currentPage);
            updateStatus(contrats.getContent());
        };
    }

    @Transactional
    public void updateStatus(List<Contrat> contrats) {
        for (var contrat : contrats) {
            log.debug("Mise à jour du statut du contrat {}", contrat.getId());
            contrat.updateContratStatus();
            contratRepository.save(contrat);
            calculStatutContratChapeau(contrat.getContratChapeau());
        }
    }

    @Override
    public List<ContratTitulaireAppach> getContratAppach(String pfUid, ContratTitulaireFilter contratTitulaireFilter, Pageable pageable, Map<String, Map<Integer, List<String>>> mappingPlaceInput, Map<String, Map<String, String>> mappingPlaceOutput) throws ApplicationException {
        var utilisateur = utilisateurRepository.findOneByIdentifiant(pfUid).orElse(null);
        var plateforme = utilisateur != null ? utilisateur.getPlateforme() : plateformeRepository.findByMpeUid(pfUid).orElseThrow(() -> new ApplicationException("Plateforme " + pfUid + " non trouvee"));
        List<Integer> statutsAppach = List.of(0, 1, 3, 4);
        if (contratTitulaireFilter.getStatutContrat() != null && !statutsAppach.contains(contratTitulaireFilter.getStatutContrat())) {
            log.info("Statut de contrat non autorisé pour la recherche Appach : {}", contratTitulaireFilter.getStatutContrat());
            return new ArrayList<>();
        }
        if (!CollectionUtils.isEmpty(contratTitulaireFilter.getStatutContrats())) {
            List<Integer> statutContrats = contratTitulaireFilter.getStatutContrats().stream().filter(statutsAppach::contains).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(statutContrats)) {
                log.info("Statuts de contrat non autorisés pour la recherche Appach : {}", contratTitulaireFilter.getStatutContrats());
                return new ArrayList<>();
            }
            contratTitulaireFilter.setStatutContrats(statutContrats);
        }
        Set<Integer> statuts = new HashSet<>();
        if (contratTitulaireFilter.getStatutContrat() != null) {
            statuts.add(contratTitulaireFilter.getStatutContrat());
        }
        if (!CollectionUtils.isEmpty(contratTitulaireFilter.getStatutContrats())) {
            statuts.addAll(contratTitulaireFilter.getStatutContrats());
        }
        ContratCriteriaAppach contratCriteria = contratWebServiceMapper.contratTitulaireToContratAppachCriteria(contratTitulaireFilter, mappingPlaceInput.get(STATUT_CONTRAT));
        contratCriteria.setUtilisateur(utilisateur);
        contratCriteria.setMpePfUid(plateforme.getMpeUid());

        if (pageable != null) {
            return getContratsWithPageable(pageable, mappingPlaceOutput, contratCriteria, statuts);
        } else {
            return getContratsWithoutPageable(mappingPlaceOutput, contratCriteria, statuts);
        }
    }

    private List<ContratTitulaireAppach> getContratsWithoutPageable(Map<String, Map<String, String>> mappingPlaceOutput, ContratCriteriaAppach contratCriteria, Set<Integer> statuts) {
        return contratAppachPaginableRepository.rechercher(contratCriteria)
                .stream()
                .map(contrat -> {
                    var contratMappe = appachContratTitulaireMapper.contratToAppachContratTitulaire(contrat, mappingPlaceOutput, statuts);
                    return new AbstractMap.SimpleEntry<>(contrat, contratMappe);
                })
                .peek(entry -> {
                    List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByContratId(entry.getKey().getId())
                            .stream().filter(contratEtablissement -> contratEtablissement.getCommanditaire() != null).collect(Collectors.toList());
                    for (ContratEtablissement contratEtablissement : contratEtablissements) {
                        if (entry.getKey().getAttributaire().getEtablissement().getId().equals(contratEtablissement.getCommanditaire().getId())) {
                            com.atexo.execution.common.appach.model.contrat.titulaire.SousTraitantType sousTraitantType = appachContratTitulaireMapper.toSousTraitantType(contratEtablissement);
                            entry.getValue().getSousTraitants().add(sousTraitantType);
                        }
                    }

                    List<CoTraitantType> coTraitants = new ArrayList<>();
                    if (entry.getKey().getGroupement() != null) {
                        List<ContratEtablissement> contratEtablissementsCoTraitants = contratEtablissementRepository.findByContratId(entry.getKey().getId());
                        for (ContratEtablissement contratEtablissement : contratEtablissementsCoTraitants) {
                            if (contratEtablissement.getCommanditaire() == null && Boolean.FALSE.equals(contratEtablissement.getMandataire())) {
                                CoTraitantType coTraitantType = appachContratTitulaireMapper.toCoTraitantType(contratEtablissement);
                                List<com.atexo.execution.common.appach.model.contrat.titulaire.SousTraitantType> sousTraitants = new ArrayList<>();
                                contratEtablissements.stream().filter(contratEtablissement2 -> contratEtablissement2.getCommanditaire() != null)
                                        .forEach(contratEtablissement2 -> {
                                            if (contratEtablissement2.getCommanditaire().getId().equals(contratEtablissement.getEtablissement().getId())) {
                                                sousTraitants.add(appachContratTitulaireMapper.toSousTraitantType(contratEtablissement2));
                                            }
                                        });
                                coTraitantType.setSousTraitants(sousTraitants);
                                coTraitants.add(coTraitantType);
                            }
                        }
                        entry.getValue().setCoTraitants(coTraitants);
                    }
                })
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private List<ContratTitulaireAppach> getContratsWithPageable(Pageable pageable, Map<String, Map<String, String>> mappingPlaceOutput, ContratCriteriaAppach contratCriteria, Set<Integer> statuts) {
        return contratAppachPaginableRepository.rechercher(contratCriteria, pageable)
                .stream()
                .map(contrat -> {
                    var contratMappe = appachContratTitulaireMapper.contratToAppachContratTitulaire(contrat, mappingPlaceOutput, statuts);
                    return new AbstractMap.SimpleEntry<>(contrat, contratMappe);
                })
                .peek(entry -> {
                    List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByContratId(entry.getKey().getId())
                            .stream().filter(contratEtablissement -> contratEtablissement.getCommanditaire() != null).collect(Collectors.toList());
                    for (ContratEtablissement contratEtablissement : contratEtablissements) {
                        if (entry.getKey().getAttributaire().getEtablissement().getId().equals(contratEtablissement.getCommanditaire().getId())) {
                            com.atexo.execution.common.appach.model.contrat.titulaire.SousTraitantType sousTraitantType = appachContratTitulaireMapper.toSousTraitantType(contratEtablissement);
                            entry.getValue().getSousTraitants().add(sousTraitantType);
                        }
                    }

                    List<CoTraitantType> coTraitants = new ArrayList<>();
                    if (entry.getKey().getGroupement() != null) {
                        List<ContratEtablissement> contratEtablissementsCoTraitants = contratEtablissementRepository.findByContratId(entry.getKey().getId());
                        for (ContratEtablissement contratEtablissement : contratEtablissementsCoTraitants) {
                            if (contratEtablissement.getCommanditaire() == null && Boolean.FALSE.equals(contratEtablissement.getMandataire())) {
                                CoTraitantType coTraitantType = appachContratTitulaireMapper.toCoTraitantType(contratEtablissement);
                                List<com.atexo.execution.common.appach.model.contrat.titulaire.SousTraitantType> sousTraitants = new ArrayList<>();
                                contratEtablissements.stream().filter(contratEtablissement2 -> contratEtablissement2.getCommanditaire() != null)
                                        .forEach(contratEtablissement2 -> {
                                            if (contratEtablissement2.getCommanditaire().getId().equals(contratEtablissement.getEtablissement().getId())) {
                                                sousTraitants.add(appachContratTitulaireMapper.toSousTraitantType(contratEtablissement2));
                                            }
                                        });
                                coTraitantType.setSousTraitants(sousTraitants);
                                coTraitants.add(coTraitantType);
                            }
                        }
                        entry.getValue().setCoTraitants(coTraitants);
                    }
                })
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private void updateDateContratChapeau(Contrat contrat) {
        Contrat contratChapeau = null;
        if (contrat != null && contrat.getContratChapeau() != null) {
            contratChapeau = contratRepository.findById(contrat.getContratChapeau().getId()).orElse(null);
        }
        if (contratChapeau != null) {
            if (contratChapeau.getDateNotification() == null) {
                contratChapeau.setDateNotification(contrat.getDateNotification());
            }
            if (contratChapeau.getDateFinContrat() == null) {
                contratChapeau.setDateFinContrat(contrat.getDateFinContrat());
            }
            if (contratChapeau.getDateMaxFinContrat() == null) {
                contratChapeau.setDateMaxFinContrat(contrat.getDateMaxFinContrat());
            }
            contratChapeau = contratRepository.save(contratChapeau);
            contrat.setContratChapeau(contratChapeau);
        }
    }

    private void calculStatutRenouvellement(Contrat contrat) {
        if (contrat.isARenouveler() && contrat.getStatutRenouvellement() == null) {
            contrat.setStatutRenouvellement(StatutRenouvellement.EN_ATTENTE);
        }
    }

    public void calculStatutContratChapeau(Contrat contratChapeau) {
        if (contratChapeau == null) {
            return;
        }
        var contratsFils = contratRepository.findAllByContratChapeauId(contratChapeau.getId());

        if (checkStatutContrat(contratsFils, StatutContrat.Notifie)) {
            contratChapeau.setStatut(StatutContrat.Notifie);
        }
        if (checkStatutContrat(contratsFils, StatutContrat.EnCours)) {
            contratChapeau.setStatut(StatutContrat.EnCours);
        }
        if (checkStatutContrat(contratsFils, StatutContrat.Clos)) {
            contratChapeau.setStatut(StatutContrat.Clos);
        }
        contratRepository.save(contratChapeau);
    }

    private boolean checkStatutContrat(List<Contrat> contratFils, StatutContrat statutContrat) {
        if (StatutContrat.Clos.equals(statutContrat)) {
            return Optional.ofNullable(contratFils)
                    .orElse(Collections.emptyList())
                    .stream().allMatch(e -> statutContrat.equals(e.getStatut()));
        }
        return Optional.ofNullable(contratFils)
                .orElse(Collections.emptyList())
                .stream().anyMatch(e -> statutContrat.equals(e.getStatut()));
    }

}
