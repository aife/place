package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.DonneesEssentiellesCriteriaDTO;
import com.atexo.execution.common.dto.DonneesEssentiellesDTO;
import com.atexo.execution.server.mapper.DonneesEssentiellesMapper;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import com.atexo.execution.server.repository.paginable.DonneeEssentiellePaginableRepository;
import com.atexo.execution.services.DonneesEssentiellesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class DonneesEssentiellesServiceImpl implements DonneesEssentiellesService {

    static final Logger LOG = LoggerFactory.getLogger(DonneesEssentiellesServiceImpl.class);

    private final DonneesEssentiellesRepository donneesEssentiellesRepository;
    private final UtilisateurRepository utilisateurRepository;
    private final DonneesEssentiellesMapper donneesEssentiellesMapper;
    private final DonneeEssentiellePaginableRepository donneeEssentiellePaginableRepository;

    public void creerPublicationDE(Contrat contrat) {
        if (contrat.isPublicationDonneesEssentielles()) {
            log.info("Publication des données essentielles pour le contrat de reference {}", contrat.getReferenceLibre());
            if (contrat.getDonneesEssentielles() == null) {
                var donneesEssntielles = new DonneesEssentielles();
                donneesEssntielles.setPlateforme(contrat.getPlateforme());
                contrat.setDonneesEssentielles(donneesEssentiellesRepository.save(donneesEssntielles));
            }
            contrat.getDonneesEssentielles().setStatut(StatutPublicationDE.A_PUBLIER);
        }
    }

    @Override
    public DonneesEssentielles updatePublicationDE(Contrat contrat, StatutPublicationDE statutPublicationDE) {
        var idDonnesEssentielles = Optional.of(contrat)
                .map(Contrat::getDonneesEssentielles)
                .map(DonneesEssentielles::getId)
                .orElse(null);
        if (idDonnesEssentielles != null) {
            var optionalDonneesEssentielles = donneesEssentiellesRepository.findById(idDonnesEssentielles);
            if (optionalDonneesEssentielles.isPresent()) {
                var donneesEssentielles = optionalDonneesEssentielles.get();
                donneesEssentielles.setStatut(statutPublicationDE);
                return donneesEssentiellesRepository.save(donneesEssentielles);
            }
        }
        return null;
    }

    @Override
    public Page<DonneesEssentiellesDTO> findDonneesEssentielles(Long utilisateurId, DonneesEssentiellesCriteriaDTO donneesEssentiellesCriteriaDTO, Pageable pageable) {
        var utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));
        var criteria = donneesEssentiellesMapper.toCriteria(utilisateur, donneesEssentiellesCriteriaDTO);
        final var contrats = donneeEssentiellePaginableRepository.rechercher(
                criteria,
                pageable
        );
        final var result = contrats.getContent().stream().flatMap(contrat -> {
            if (!CollectionUtils.isEmpty(contrat.getActes())) {
                return contrat.getActes().stream().map(e -> donneesEssentiellesMapper.contratToDonneeEssentielleDTO1(contrat, e));
            } else {
                return Stream.of(donneesEssentiellesMapper.contratToDonneeEssentielleDTO1(contrat, null));
            }
        }).collect(Collectors.toList());
        return new PageImpl<>(result, pageable, contrats.getTotalElements());
    }

    @Transactional
    public boolean updateStaut(List<Long> idsDonneesEssentielles) {
        try {
            donneesEssentiellesRepository.modifierStatutDonneesEssentielles(idsDonneesEssentielles, StatutPublicationDE.A_PUBLIER);
            return true;
        } catch (Exception e) {
            LOG.error("erreur au niveau de updateStaut pour les ids données essentielles: [{}]", idsDonneesEssentielles, e);
            return false;
        }
    }

    @Override
    public Optional<DonneesEssentielles> telechargerFichier(Long idDonneesEssentielles) {
     return  donneesEssentiellesRepository.findById(idDonneesEssentielles);
    }
}
