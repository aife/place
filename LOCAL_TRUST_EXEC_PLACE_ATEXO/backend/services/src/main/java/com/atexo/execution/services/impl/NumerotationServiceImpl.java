package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.common.dto.NumerotationTypeEnum;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationChampRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationCompteurRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationRepository;
import com.atexo.execution.services.NumerotationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class NumerotationServiceImpl implements NumerotationService {

    final ContratRepository contratRepository;

    final ParametrageNumerotationRepository parametrageNumerotationRepository;
    final ParametrageNumerotationChampRepository parametrageNumerotationChampRepository;
    final ParametrageNumerotationCompteurRepository parametrageNumerotationCompteurRepository;

    @Override
    public String numeroter(Contrat contrat, NumerotationResult result, String messageErreur) throws ApplicationBusinessException {
        ParametrageNumerotation parametrage = result.getParametrage();
        Set<ParametrageNumerotationConfiguration> configurationsSet = parametrage.getConfigurations();
        List<ParametrageNumerotationConfiguration> configurations = configurationsSet.stream().filter(ParametrageNumerotationConfiguration::isActif).sorted(Comparator.comparingInt(ParametrageNumerotationConfiguration::getOrdre)).collect(Collectors.toList());
        ParametrageContrat valeur = new ParametrageContrat(contrat, parametrageNumerotationCompteurRepository);
        String numeroContrat = construireNumeroContrat(configurations, valeur);
        if (!StringUtils.hasText(numeroContrat)) {
            throw new ApplicationBusinessException(messageErreur);
        }
        if (StringUtils.hasText(parametrage.getPrefix())) {
            numeroContrat = parametrage.getPrefix() + numeroContrat;
        }
        if (StringUtils.hasText(parametrage.getSuffix())) {
            numeroContrat = numeroContrat + parametrage.getSuffix();
        }
        return numeroContrat;
    }

    @Override
    public NumerotationResult getNumerotation(Contrat contrat, NumerotationTypeEnum type, NumerotationTypeEnum defaut) throws ApplicationBusinessException {
        NumerotationResult result = getContratNumerotationFromType(contrat, type);
        if (result.getParametrage() == null && defaut != null) {
            result = getContratNumerotationFromType(contrat, defaut);
        }
        if (result.getParametrage() == null) {
            throw new ApplicationBusinessException("Aucun paramétrage de numérotation de contrat n'est défini pour la plateforme " + result.getIdPlateforme() + " l'organisme " + result.getIdOrganisme() + " et le service " + result.getIdService());
        }
        result.setType(type);
        return result;
    }

    private NumerotationResult getContratNumerotationFromType(Contrat contrat, NumerotationTypeEnum type) {
        com.atexo.execution.server.model.Service service = contrat.getService();
        ParametrageNumerotation parametrage = null;
        Long idPlateforme = null;
        Long idOrganisme = null;
        Long idService = null;
        NiveauEnum niveau = NiveauEnum.SERVICE;

        if (service != null) {
            idService = service.getId();
            parametrage = parametrageNumerotationRepository.findByTypeAndNiveauAndIdPlateformeOrganismeService(type, NiveauEnum.SERVICE, idService);
            if (parametrage == null && service.getOrganisme() != null) {
                idOrganisme = service.getOrganisme().getId();
                niveau = NiveauEnum.ORGANISME;
                parametrage = parametrageNumerotationRepository.findByTypeAndNiveauAndIdPlateformeOrganismeService(type, NiveauEnum.ORGANISME, idOrganisme);
            }
        }

        Plateforme plateforme = contrat.getPlateforme();
        if (parametrage == null && plateforme != null) {
            idPlateforme = plateforme.getId();
            niveau = NiveauEnum.PLATEFORME;
            parametrage = parametrageNumerotationRepository.findByTypeAndNiveauAndIdPlateformeOrganismeService(type, NiveauEnum.PLATEFORME, idPlateforme);
        }
        return new NumerotationResult(parametrage, idPlateforme, idOrganisme, idService, niveau);
    }



    @Override
    public void verifierUniciteNumero(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException {
        NumerotationTypeEnum type = numerotationResult.getType();
        String numero = contrat.getNumero();
        com.atexo.execution.server.model.Service service = contrat.getService();
        Plateforme plateforme = contrat.getPlateforme();
        ParametrageNumerotation parametrage = numerotationResult.getParametrage();
        NiveauEnum niveau = numerotationResult.getNiveau();

        if (parametrage == null) {
            return;
        }
        if (parametrage.isUniqueNumero()) {
            String typeValue = type.getValue();
            switch (niveau) {
                case SERVICE:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroAndServiceIdAndIdNotAndChapeau(numero, service.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour le service " + service.getNom());
                        }
                    } else if (contratRepository.existsByNumeroAndServiceId(numero, service.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour le service " + service.getNom());
                    }
                    break;
                case ORGANISME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroAndServiceOrganismeIdAndIdNotAndChapeau(numero, service.getOrganisme().getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                        }
                    } else if (contratRepository.existsByNumeroAndServiceOrganismeId(numero, service.getOrganisme().getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                    }
                    break;
                case PLATEFORME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroAndPlateformeIdAndIdNotAndChapeau(numero, plateforme.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec " + typeValue + " [" + numero + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec " + typeValue + " [" + numero + "] existe déjà");
                        }
                    } else if (contratRepository.existsByNumeroAndPlateformeId(numero, plateforme.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numero + "] existe déjà");
                    }
                    break;
            }
        }
    }

    @Override
    public void verifierUniciteNumeroLong(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException {
        NumerotationTypeEnum type = numerotationResult.getType();
        String numeroLong = contrat.getNumeroLong();
        com.atexo.execution.server.model.Service service = contrat.getService();
        Plateforme plateforme = contrat.getPlateforme();
        ParametrageNumerotation parametrage = numerotationResult.getParametrage();
        NiveauEnum niveau = numerotationResult.getNiveau();

        if (parametrage == null) {
            return;
        }
        if (parametrage.isUniqueNumero()) {
            String typeValue = type.getValue();
            switch (niveau) {
                case SERVICE:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroLongAndServiceIdAndIdNotAndChapeau(numeroLong, service.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour le service " + service.getNom());
                        }
                    } else if (contratRepository.existsByNumeroLongAndServiceId(numeroLong, service.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour le service " + service.getNom());
                    }
                    break;
                case ORGANISME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroLongAndServiceOrganismeIdAndIdNotAndChapeau(numeroLong, service.getOrganisme().getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                        }
                    } else if (contratRepository.existsByNumeroLongAndServiceOrganismeId(numeroLong, service.getOrganisme().getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                    }
                    break;
                case PLATEFORME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByNumeroLongAndPlateformeIdAndIdNotAndChapeau(numeroLong, plateforme.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec " + typeValue + " [" + numeroLong + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec " + typeValue + " [" + numeroLong + "] existe déjà");
                        }
                    } else if (contratRepository.existsByNumeroLongAndPlateformeId(numeroLong, plateforme.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + numeroLong + "] existe déjà");
                    }
                    break;
            }
        }
    }

    @Override
    public void verifierUniciteReferenceLibre(Contrat contrat, NumerotationResult numerotationResult) throws ApplicationBusinessException {
        NumerotationTypeEnum type = numerotationResult.getType();
        String referenceLibre = contrat.getReferenceLibre();
        com.atexo.execution.server.model.Service service = contrat.getService();
        Plateforme plateforme = contrat.getPlateforme();
        ParametrageNumerotation parametrage = numerotationResult.getParametrage();
        NiveauEnum niveau = numerotationResult.getNiveau();

        if (parametrage == null) {
            return;
        }
        if (parametrage.isUniqueNumero()) {
            String typeValue = type.getValue();
            switch (niveau) {
                case SERVICE:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByReferenceLibreAndServiceIdAndIdNotAndChapeau(referenceLibre, service.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour le service " + service.getNom());
                        }
                    } else if (contratRepository.existsByReferenceLibreAndServiceId(referenceLibre, service.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour le service " + service.getNom() + "[" + service.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour le service " + service.getNom());
                    }
                    break;
                case ORGANISME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByReferenceLibreAndServiceOrganismeIdAndIdNotAndChapeau(referenceLibre, service.getOrganisme().getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                        }
                    } else if (contratRepository.existsByReferenceLibreAndServiceOrganismeId(referenceLibre, service.getOrganisme().getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour l'organisme " + service.getOrganisme().getNom() + "[" + service.getOrganisme().getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour l'organisme " + service.getOrganisme().getNom());
                    }
                    break;
                case PLATEFORME:
                    if (contrat.getId() != null) {
                        if (contratRepository.existsByReferenceLibreAndPlateformeIdAndIdNotAndChapeau(referenceLibre, plateforme.getId(), contrat.getId(), contrat.isChapeau())) {
                            log.error("Un contrat avec " + typeValue + " [" + referenceLibre + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                            throw new ApplicationBusinessException("Un contrat avec " + typeValue + " [" + referenceLibre + "] existe déjà");
                        }
                    } else if (contratRepository.existsByReferenceLibreAndPlateformeId(referenceLibre, plateforme.getId())) {
                        log.error("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà pour la plateforme " + plateforme.getMpeUid() + "[" + plateforme.getId() + "]");
                        throw new ApplicationBusinessException("Un contrat avec le " + typeValue + " [" + referenceLibre + "] existe déjà");
                    }
                    break;
            }
        }
    }

    @Override
    public void updateCompteur(Contrat contrat) {
        com.atexo.execution.server.model.Service service = contrat.getService();
        Plateforme plateforme = contrat.getPlateforme();
        ParametrageContrat valeur = new ParametrageContrat(contrat, parametrageNumerotationCompteurRepository);
        List<ParametrageNumerotationCompteur> parametrageNumerotationChamps = new ArrayList<>();
        parametrageNumerotationChampRepository.findAllByCompteur(true).forEach(parametrageNumerotationChamp -> {
            valeur.setParametrageNumerotationChamp(parametrageNumerotationChamp);
            if (this.isConditionnementVerifie(valeur, parametrageNumerotationChamp) && parametrageNumerotationChamp.isCompteur()) {
                if (service != null && "compteurService()".equals(parametrageNumerotationChamp.getMethodeValeur())) {
                    log.info("Mise à jour du compteur pour le champ {} et le service {}", parametrageNumerotationChamp.getClef(), service.getNom());
                    parametrageNumerotationChamps.add(updateCompteur(parametrageNumerotationChamp, service.getId(), NiveauEnum.SERVICE));
                }
                if (service != null && service.getOrganisme() != null && "compteurOrganisme()".equals(parametrageNumerotationChamp.getMethodeValeur())) {
                    log.info("Mise à jour du compteur pour le champ {} et l'organisme {}", parametrageNumerotationChamp.getClef(), service.getOrganisme().getNom());
                    parametrageNumerotationChamps.add(updateCompteur(parametrageNumerotationChamp, service.getOrganisme().getId(), NiveauEnum.ORGANISME));
                }
                if (plateforme != null && "compteurPlateforme()".equals(parametrageNumerotationChamp.getMethodeValeur())) {
                    log.info("Mise à jour du compteur pour le champ {} et la plateforme {}", parametrageNumerotationChamp.getClef(), plateforme.getMpeUid());
                    parametrageNumerotationChamps.add(updateCompteur(parametrageNumerotationChamp, plateforme.getId(), NiveauEnum.PLATEFORME));
                }

            }
            valeur.setParametrageNumerotationChamp(null);


        });
        parametrageNumerotationCompteurRepository.saveAll(parametrageNumerotationChamps);
    }

    private ParametrageNumerotationCompteur updateCompteur(ParametrageNumerotationChamp parametrageNumerotationChamp, Long id, NiveauEnum niveauEnum) {
        var parametrageNumerotationCompteur = parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(parametrageNumerotationChamp.getId(), niveauEnum, id);
        if (parametrageNumerotationCompteur == null) {
            parametrageNumerotationCompteur = new ParametrageNumerotationCompteur();
            parametrageNumerotationCompteur.setParametrageNumerotationChamp(parametrageNumerotationChamp);
            parametrageNumerotationCompteur.setNiveau(niveauEnum);
            parametrageNumerotationCompteur.setIdPlateformeOrganismeService(id);
            parametrageNumerotationCompteur.setCompteur(1);
        } else {
            parametrageNumerotationCompteur.setCompteur(parametrageNumerotationCompteur.getCompteur() + 1);
        }
        return parametrageNumerotationCompteur;
    }

    private String construireNumeroContrat(List<ParametrageNumerotationConfiguration> parametrageNumerotationChamp, ParametrageContrat contrat) throws ApplicationBusinessException {
        StringBuffer numero = new StringBuffer();
        parametrageNumerotationChamp.forEach(parametrage -> numero.append(getValueFromClef(parametrage, contrat).toUpperCase()));
        log.info("Chaine générée : {}", numero);
        return numero.toString();
    }

    public String getValueFromClef(ParametrageNumerotationConfiguration parametrageNumerotation, ParametrageContrat contrat) {
        contrat.setParametrageNumerotationChamp(parametrageNumerotation.getParametrageNumerotationChamp());
        if (this.isConditionnementVerifie(contrat, parametrageNumerotation.getParametrageNumerotationChamp())) {
            return this.getValeur(contrat, parametrageNumerotation);
        }
        contrat.setParametrageNumerotationChamp(null);
        return "";
    }


    @Override
    public void reinitialiserCompteurs() {
        log.info("Reinitialisation des compteurs de numérotation de contrat selon le paramétrage");
        parametrageNumerotationCompteurRepository.findAll().forEach(parametrage -> {
            var duree = parametrage.getParametrageNumerotationChamp().getDureeResetCompteur();
            if (duree != null) {
                switch (duree) {
                    case JOUR:
                        parametrage.setCompteur(0);
                        parametrageNumerotationCompteurRepository.save(parametrage);
                        break;
                    case SEMAINE:
                        Calendar startDate = Calendar.getInstance();
                        if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                            parametrage.setCompteur(0);
                            parametrageNumerotationCompteurRepository.save(parametrage);
                        }
                        break;
                    case MOIS:
                        startDate = Calendar.getInstance();
                        if (startDate.get(Calendar.DAY_OF_MONTH) == 1) {
                            parametrage.setCompteur(0);
                            parametrageNumerotationCompteurRepository.save(parametrage);
                        }
                        break;
                    case ANNEE:
                        startDate = Calendar.getInstance();
                        if (startDate.get(Calendar.DAY_OF_YEAR) == 1) {
                            parametrage.setCompteur(0);
                            parametrageNumerotationCompteurRepository.save(parametrage);
                        }
                        break;
                }
            }
        });
    }

    private String getValeur(ParametrageContrat contrat, ParametrageNumerotationConfiguration parametrageNumerotation) {

        if (contrat == null || parametrageNumerotation == null) {
            return "";
        }
        String subNumero;
        ParametrageNumerotationChamp parametrageNumerotationChamp = parametrageNumerotation.getParametrageNumerotationChamp();

        String cheminContrat = parametrageNumerotationChamp.getMethodeValeur();
        if (!StringUtils.hasText(cheminContrat)) {
            return "";
        }
        List<Object> resultatList = getResultatList(contrat, cheminContrat, parametrageNumerotationChamp.getFormat(), parametrageNumerotationChamp.getLongueur());
        subNumero = resultatList.stream().map(Object::toString).reduce((s, s2) -> s + s2).orElse("");

        if (StringUtils.hasText(parametrageNumerotation.getPrefix()) && StringUtils.hasText(subNumero)) {
            subNumero = parametrageNumerotation.getPrefix() + subNumero;
        }

        if (StringUtils.hasText(parametrageNumerotation.getSeparateur()) && StringUtils.hasText(subNumero)) {
            subNumero = subNumero + parametrageNumerotation.getSeparateur();
        }
        return subNumero;
    }

    private boolean isConditionnementVerifie(ParametrageContrat contrat, ParametrageNumerotationChamp parametrageNumerotationChamp) {
        if (contrat == null || parametrageNumerotationChamp == null) {
            return false;
        }
        String cheminConditionnement = parametrageNumerotationChamp.getMethodeConditionnement();
        if (!StringUtils.hasText(cheminConditionnement)) {
            return true;
        }
        List<String> expectedValeurs;
        if (parametrageNumerotationChamp.getValeurConditionnement() != null) {
            expectedValeurs = List.of(parametrageNumerotationChamp.getValeurConditionnement().split(","));
        } else {
            expectedValeurs = List.of("true");
        }

        log.info("Methode de conditionnement = {} avec les valeurs attendues = {} pour le contrat ID = {}", cheminConditionnement, String.join(",", expectedValeurs), contrat.getContrat().getId());

        List<Object> resultatList = getResultatList(contrat, cheminConditionnement, parametrageNumerotationChamp.getFormat(), null);
        boolean conditionnement = false;
        try {

            log.info("Methode de conditionnement = {} avec les valeurs calculées = {} pour le contrat ID = {}", cheminConditionnement, resultatList.stream().map(Object::toString).collect(Collectors.toList()), contrat.getContrat().getId());


            // conditionnement est true s'il y a au moins une condition correspondante
            for (Object resultat : resultatList) {
                if (resultat == null) {
                    continue;
                }
                if (resultat instanceof List) {
                    conditionnement = ((List) resultat).stream().anyMatch(o -> {
                        if (o == null) {
                            return false;
                        }
                        return expectedValeurs.contains(o.toString());
                    });
                } else {
                    conditionnement = expectedValeurs.contains(resultat.toString());
                }
                if (conditionnement) {
                    break;
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e.fillInStackTrace());
            return false;
        }

        log.info("Conditionnement = {} pour le contrat ID = {} / Methode de conditionnement = {}", conditionnement, contrat.getContrat().getId(), cheminConditionnement);

        return conditionnement;
    }

    private List<Object> getResultatList(Object source, String cheminConditionnement, String format, Integer longueur) {
        String[] argumentTableau = cheminConditionnement.split(",|\\(|\\)");
        Object[] arguments = null;
        if (argumentTableau.length > 1) {
            arguments = new Object[argumentTableau.length - 1];
        }
        int ouvrante = cheminConditionnement.indexOf("(");
        String methode = cheminConditionnement.substring(0, ouvrante);
        List<Object> resultatList = new ArrayList<>();
        String[] methodes = methode.split("\\.");
        for (int i = 0; i < methodes.length; i++) {
            methode = methodes[i];

            if (i == (methodes.length - 1)) {
                Object object = lancerMethode(source, arguments, methode);
                if (object == null || !isSimple(object.getClass())) {
                    continue;
                }
                if (format != null) {
                    object = getObjectFromFormat(format, object);
                }
                if (longueur != null) {
                    if (object.toString().length() > longueur) object = object.toString().substring(0, longueur);
                    else if (object.toString().length() < longueur)
                        object = "0".repeat(longueur - object.toString().length()) + object;
                }

                resultatList.add(object);

            } else {
                source = lancerMethode(source, arguments, methode);
            }
        }
        return resultatList;
    }

    private boolean isSimple(Class<?> clazz) {
        return clazz.isPrimitive() || clazz.isEnum() || clazz.getPackageName().startsWith("java.");
    }


    private Object getObjectFromFormat(String format, Object object) {
        try {
            if (object instanceof LocalDateTime) {
                object = LocalDateTime.parse(object.toString()).format(java.time.format.DateTimeFormatter.ofPattern(format));
            } else if (object instanceof LocalDate) {
                object = LocalDate.parse(object.toString()).format(java.time.format.DateTimeFormatter.ofPattern(format));
            } else if (object instanceof ZonedDateTime) {
                object = ZonedDateTime.parse(object.toString()).format(java.time.format.DateTimeFormatter.ofPattern(format));
            } else if (object instanceof Date) {
                object = new java.text.SimpleDateFormat(format).format(object);
            } else {
                object = String.format(format, object);
            }
            return object;
        } catch (Exception e) {
            log.error("Erreur lors de la conversion de l'objet {} avec le format {} => {}", object, format, e.getMessage());
            return object;
        }
    }

    private Object lancerMethode(final Object invoqueur, final Object[] args, final String attribut) {
        log.debug("Lancement de la méthode avec objet = {}, methode = {}, args = {}", invoqueur, attribut, args);
        if (invoqueur == null) {
            return null;
        }
        Class<?>[] paramTypes = null;
        if (args != null) {
            paramTypes = new Class[args.length];
            for (int i = 0; i < args.length; ++i) {
                if (args[i].getClass().equals(Integer.class)) {
                    paramTypes[i] = int.class;
                } else {
                    paramTypes[i] = args[i].getClass();
                }
            }
        }
        Method methode;
        try {
            log.debug("Récupération de la méthode depuis son nom {} et param {}", attribut, paramTypes);

            methode = Arrays.stream(invoqueur.getClass().getMethods()).filter(m -> {
                String methodeSearch = attribut.substring(0, 1).toUpperCase() + attribut.substring(1);
                return m.getName().equals("is" + methodeSearch) || m.getName().equals("get" + methodeSearch);
            }).findFirst().orElse(null);
            if (methode == null) {
                return null;
            }
        } catch (SecurityException e) {
            log.error("Erreur lors de la récupération de la méthode depuis son nom {} et param {} => {}", attribut, paramTypes, e.getMessage());
            return null;
        }
        try {
            log.trace("Invocation de la méthode depuis son nom {} et param {}", attribut, paramTypes);

            return methode.invoke(invoqueur, args);
        } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException e) {
            log.error("Erreur lors de l'invocation de la méthode depuis son nom {} et param {} => {}", attribut, paramTypes, e.getMessage());
            return null;
        }
    }


}
