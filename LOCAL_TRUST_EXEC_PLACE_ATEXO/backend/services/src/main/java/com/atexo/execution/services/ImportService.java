package com.atexo.execution.services;

import com.atexo.execution.common.dto.ModificationContrat;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface ImportService {

    List<ModificationContrat> importAvenant(MultipartFile file, String plateformeUid);

    List<ModificationContrat> csvToModificationContrat(InputStream inputStream);
}
