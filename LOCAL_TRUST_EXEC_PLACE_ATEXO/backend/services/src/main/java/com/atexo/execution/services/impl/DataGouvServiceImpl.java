package com.atexo.execution.services.impl;

import com.atexo.execution.common.data.gouv.model.marche.ContratConcession;
import com.atexo.execution.common.data.gouv.model.marche.Marche;
import com.atexo.execution.common.data.gouv.model.marche.Marches;
import com.atexo.execution.common.data.gouv.ws.api.DataSetRequest;
import com.atexo.execution.common.data.gouv.ws.api.DataSetResponse;
import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvConcessionMapper;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvMarcheMapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.actes.AgrementSousTraitant;
import com.atexo.execution.server.model.data.gouv.ContratPublication;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import com.atexo.execution.server.repository.crud.data.gouv.ContratPublicationRepository;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import com.atexo.execution.services.DataGouvService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DataGouvServiceImpl implements DataGouvService {

    static final Logger LOG = LoggerFactory.getLogger(DataGouvServiceImpl.class);
    public static final String JSON = ".json";
    public static final String X_API_KEY = "x-api-key";
    public static final String FILE = "file";
    public static final String FILE_NAME_FORMAT = "/DECP-%s-%d-%s-%s-%s";
    public static final String FORMAT_SEQUENCE = "%02d";
    public static final String DATA_SET = "{dataSet}";
    private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
    public static final String FORMAT_ID_SEQUENCE = "{0,number,00}";

    @Value("${data.gouv.api.url:}")
    private String apiUploadUrl;
    @Value("${data.gouv.api.dataSet:}")
    private String apiCreateDataSetUrl;
    @Value("${data.gouv.apiKey:}")
    private String apiKey;

    private final ContratPublicationRepository contratPublicationRepository;
    private final DonneesEssentiellesRepository donneesEssentiellesRepository;
    private final DataGouvMarcheMapper dataGouvMarcheMapper;
    private final OrganismeRepository organismeRepository;
    private final ActeRepository acteRepository;
    private final DataGouvConcessionMapper dataGouvConcessionMapper;


    @Override
    public String getOrCreateDataSet(Organisme organisme) {
        String dataSet = organisme.getDataSet();
        if (dataSet == null) {
            LOG.info("Pas de DataSet pour l'organisme, id={}, nom={}", organisme.getId(), organisme.getNom());
            //Appel du ws pour la creation du dataSet
            DataSetResponse dataSetResponse = createDataSet(organisme);

            //mise à jour de la dataset dans la table organisme
            if (dataSetResponse != null) {
                organisme.setDataSet(dataSetResponse.getId());
                organismeRepository.save(organisme);
                dataSet = dataSetResponse.getId();
            }
        }
        return dataSet;
    }

    @Override
    public void uploadFile(Path tempFile, String dataSet) {
        // Prepare apiUrl
        String newApiUrl = apiUploadUrl.replace(DATA_SET, dataSet);

        // Prepare headers
        HttpHeaders headers = createHeader();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        // Prepare the request body
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add(FILE, new FileSystemResource(tempFile));

        // Prepare the request entity
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, createHeader());

        // Create RestTemplate
        RestTemplate restTemplate = new RestTemplate();

        // Make the POST request
        restTemplate.exchange(newApiUrl, HttpMethod.POST, requestEntity, String.class);
    }

    @Override
    public Marches mappingExecDataGouv(List<Contrat> contrats, Map<String, Map<String, String>> mapping, Organisme organisme) {
        Marches marches = new Marches();
        marches.getMarche().addAll(mappingContratsMarche(contrats, mapping, organisme));
        marches.getContratConcession().addAll(mappingContratsConcession(contrats, mapping, organisme));
        return marches;
    }

    @Override
    public Path preparationJsonFile(Marches marches, Organisme organisme) throws IOException {
        //compter les fichiers publiés le même jour pour le même organisme
        long numberOfFile = contratPublicationRepository.countByOrganismeAndDatePublication(organisme, Date.valueOf(LocalDate.now()), StatutPublicationDE.PUBLIE);

        //Nomenclature du fichier : DECP-SIRET-année-mois-jour-numéro de séquence.extension
        String fileName = String.format(FILE_NAME_FORMAT, organisme.getSiret()
                , LocalDate.now().getYear()
                , String.format(FORMAT_SEQUENCE, LocalDate.now().getMonthValue())
                , String.format(FORMAT_SEQUENCE, LocalDate.now().getDayOfMonth())
                , String.format(FORMAT_SEQUENCE, numberOfFile + 1));

        //Création du fichier Json
        Path tempFile = Path.of(System.getProperty(JAVA_IO_TMPDIR) + fileName + JSON);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(tempFile.toFile(), marches);
        return tempFile;
    }


    @Override
    @Transactional
    public void miseAjourContratPublication(List<Contrat> contrats, Boolean publicationDataGouv, Boolean publicationTncp, String message, StatutPublicationDE statut, LocalDateTime datePublication, StatutAvenant statutAvenant, String nomFichier) {
        List<ContratPublication> contratPublications = contrats.stream()
                .peek(c -> {
                    c.getDonneesEssentielles().setStatut(statut);
                    c.getDonneesEssentielles().setDatePublication(datePublication);
                    c.getDonneesEssentielles().setNomFichier(nomFichier);
                    c.getDonneesEssentielles().setModeEnvoiDonneesEssentielles(ModeEnvoiDonneesEssentielles.DATA_GOUV);
                    if (StatutPublicationDE.NON_PUBLIE.equals(statut)) {
                        c.getDonneesEssentielles().setErreurPublication(message);
                    }
                })
                .map(c -> new ContratPublication(datePublication, publicationDataGouv, publicationTncp, message, c.getDonneesEssentielles()))
                .collect(Collectors.toList());
        updateStatutActe(contrats, statutAvenant);
        contratPublicationRepository.saveAll(contratPublications);
    }

    @Override
    @Transactional
    public void miseAjourContratPublication(Contrat contrat, Boolean publicationDataGouv, Boolean publicationTncp, String message, StatutPublicationDE statut, LocalDateTime datePublication) {
        var donneesEssentielles = contrat.getDonneesEssentielles();
        if (donneesEssentielles == null) {
            donneesEssentielles = new DonneesEssentielles();
            contrat.setDonneesEssentielles(donneesEssentielles);
        }
        donneesEssentielles.setStatut(statut);
        donneesEssentielles.setDatePublication(datePublication);
        donneesEssentielles = donneesEssentiellesRepository.save(donneesEssentielles);
        var contratPublication = new ContratPublication(datePublication, publicationDataGouv, publicationTncp, message, donneesEssentielles);
        contratPublicationRepository.save(contratPublication);
    }

    @Override
    public void supprimerFichier(Path file) {
        try {
            Files.deleteIfExists(file);
        } catch (IOException e) {
            LOG.error("Erreur au niveau de la suppressuin du fichier: [{}]", file, e);
        }
    }

    private DataSetResponse createDataSet(Organisme organisme) {
        //Création de l'objet dataSet
        DataSetRequest dataSetRequest = new DataSetRequest();
        dataSetRequest.setTitle(organisme.getNom());
        dataSetRequest.setDescription(organisme.getNom());
        // Prepare the request entity
        HttpEntity<DataSetRequest> requestEntity = new HttpEntity<>(dataSetRequest, createHeader());

        // Create RestTemplate
        RestTemplate restTemplate = new RestTemplate();

        // Make the POST request
        ResponseEntity<DataSetResponse> response = restTemplate.exchange(apiCreateDataSetUrl, HttpMethod.POST, requestEntity, DataSetResponse.class);
        return response.getBody();
    }

    private HttpHeaders createHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(X_API_KEY, apiKey);
        return headers;
    }

    private Optional<Marche> mappingMarche(Contrat contrat, Map<String, Map<String, String>> map, Organisme organisme) {
        try {
            var publicationNumber = contratPublicationRepository.countByOrganisme(organisme, StatutPublicationDE.PUBLIE);
            var sequence = MessageFormat.format(FORMAT_ID_SEQUENCE, publicationNumber + 1);
            return Optional.of(dataGouvMarcheMapper.contratToDataGouvMarche(contrat, map, sequence));
        } catch (Exception e) {
            LOG.error("Erreur au niveau du mapping du contrat: id=[{}] intitulé=[{}]", contrat.getId(), contrat.getIntitule(), e);
            return Optional.empty();
        }
    }

    private void updateStatutActe(List<Contrat> contrats, StatutAvenant statutAvenant) {
        List<Acte> listActes = Optional.ofNullable(contrats)
                .orElse(Collections.emptyList())
                .stream()
                .flatMap(contrat ->
                        Optional.ofNullable(acteRepository.findByContratId(contrat.getId()))
                                .orElse(Collections.emptySet()).stream()
                                .filter(acte -> acte.getStatut() == StatutActe.NOTIFIE)
                                .filter(acte -> acte instanceof AgrementSousTraitant && ((AgrementSousTraitant) acte).getPublicationDonneesEssentielles() || acte instanceof ActeModificatif && ((ActeModificatif) acte).getPublicationDonneesEssentielles())
                                .peek(acte -> acte.setStatutPublication(statutAvenant)))
                .collect(Collectors.toList());
        acteRepository.saveAll(listActes);
    }

    private Optional<ContratConcession> mappingContratConcession(Contrat contrat, Map<String, Map<String, String>> map, Organisme organisme) {
        try {
            var publicationNumber = contratPublicationRepository.countByOrganisme(organisme, StatutPublicationDE.PUBLIE);
            var sequence = MessageFormat.format(FORMAT_ID_SEQUENCE, publicationNumber + 1);
            return Optional.of(dataGouvConcessionMapper.toContratConcession(contrat, map, sequence));
        } catch (Exception e) {
            LOG.error("Erreur au niveau du mapping du contrat concession: id=[{}] intitulé=[{}]", contrat.getId(), contrat.getIntitule(), e);
            return Optional.empty();
        }
    }

    private List<Marche> mappingContratsMarche(List<Contrat> contrats, Map<String, Map<String, String>> mapping, Organisme organisme) {
        return contrats
                .stream()
                .filter(contrat -> !contrat.isConcession())
                .map(contrat -> mappingMarche(contrat, mapping, organisme))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private List<ContratConcession> mappingContratsConcession(List<Contrat> contrats, Map<String, Map<String, String>> mapping, Organisme organisme) {
        return contrats
                .stream()
                .filter(Contrat::isConcession)
                .map(contrat -> mappingContratConcession(contrat, mapping, organisme))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
