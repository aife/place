package com.atexo.execution.services;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;

public interface EchangeChorusService {
    EchangeChorusDTO updateEchangeChorus(String pfUid, EchangeChorusType echangeChorusType) throws ApplicationBusinessException;
}
