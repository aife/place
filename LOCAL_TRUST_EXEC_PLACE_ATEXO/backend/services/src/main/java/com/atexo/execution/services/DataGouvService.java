package com.atexo.execution.services;

import com.atexo.execution.common.data.gouv.model.marche.Marches;
import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface DataGouvService {

    String getOrCreateDataSet(Organisme organisme);

    void uploadFile(Path tempFile, String dataSet) throws IOException;

    Marches mappingExecDataGouv(List<Contrat> contrats, Map<String, Map<String, String>> mapping, Organisme organisme);

    Path preparationJsonFile(Marches marches, Organisme organisme) throws IOException;

    void miseAjourContratPublication(List<Contrat> contrats, Boolean publicationDataGouv, Boolean publicationTncp, String message, StatutPublicationDE statut, LocalDateTime datePublication, StatutAvenant statutAvenant,  String nomFichier);

    void miseAjourContratPublication(Contrat contrat, Boolean publicationDataGouv, Boolean publicationTncp, String message, StatutPublicationDE statut, LocalDateTime datePublication);

    void supprimerFichier(Path file);

}
