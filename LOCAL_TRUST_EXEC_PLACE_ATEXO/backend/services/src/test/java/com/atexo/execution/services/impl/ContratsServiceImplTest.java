package com.atexo.execution.services.impl;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.TypeNotification;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exec.mpe.ws.Exec;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.ContratWebServiceMapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.referentiels.*;
import com.atexo.execution.services.ClauseService;
import com.atexo.execution.services.DonneesEssentiellesService;
import com.atexo.execution.services.NumerotationService;
import exceptions.ContratMPEException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContratsServiceImplTest {

    @InjectMocks
    ContratsServiceImpl contratsService;
    @Mock
    ContratRepository contratRepository;
    @Mock
    ContratMapper contratMapperV2;
    @Mock
    CCAGReferenceRepository ccagReferenceRepository;
    @Mock
    FormePrixRepository formePrixRepository;
    @Mock
    RevisionPrixRepository revisionPrixRepository;
    @Mock
    CategorieConsultationRepository categorieConsultationRepository;
    @Mock
    ServiceRepository serviceRepository;
    @Mock
    UtilisateurRepository utilisateurRepository;
    @Mock
    ConsultationRepository consultationRepository;
    @Mock
    EtablissementRepository etablissementRepository;
    @Mock
    ContactRepository contactRepository;
    @Mock
    ContratEtablissementRepository contratEtablissementRepository;
    @Mock
    LieuExecutionRepository lieuExecutionRepository;
    @Mock
    CPVRepository cpvRepository;
    @Mock
    TypeContratRepository typeContratRepository;
    @Mock
    TechniqueAchatRepository techniqueAchatRepository;
    @Mock
    ModaliteExecutionRepository modaliteExecutionRepository;
    @Mock
    TypePrixRepository typePrixRepository;

    @Mock
    NumerotationService numerotationService;

    @Mock
    ClauseService clauseService;

    @Mock
    ContratWebServiceMapper contratWebServiceMapper;

    @Mock
    TypeGroupementRepository typeGroupementRepository;

    @Mock
    ProcedureRepository procedureRepository;

    @Mock
    FournisseurRepository fournisseurRepository;

    @Mock
    ClauseMapper clauseMapper;

    @Mock
    PlateformeRepository plateformeRepository;

    @Mock
    NatureContratConcessionRepository natureContratConcessionRepository;

    @Mock
    DonneesEssentiellesService donneesEssentiellesService;


    @Test
    public void givenXML_whenSaveContrat_thenSaveContrat() throws JAXBException, ApplicationBusinessException {
        JAXBContext context = JAXBContext.newInstance(Exec.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Spécifiez le chemin du fichier XML
        File fichierXML = new File("src/test/resources/xml/contrat.xml");
        var exec = (Exec) unmarshaller.unmarshal(fichierXML);
        assertNotNull(exec.getEnvoi().getContrat());
        Contrat contrat = new Contrat();
        contrat.setReferenceLibre("CONS_allotie");
        TypeContrat type = new TypeContrat();
        type.setCodeExterne("MARCHES");
        contrat.setType(type);
        contrat.setCreateur(new Utilisateur());
        var plateforme = new Plateforme();
        plateforme.setUid("uid");
        plateforme.setMpeUid("mpeUid");
        contrat.getCreateur().setPlateforme(plateforme);
        contrat.setPlateforme(plateforme);

        Service service = new Service();
        service.setId(1L);
        service.setNomCourt("Mon service");
        when(serviceRepository.findByIdExterneAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(service));

        Utilisateur createur = new Utilisateur();
        createur.setPlateforme(new Plateforme());
        contrat.setCreateur(createur);
        when(contratWebServiceMapper.toContrat(any())).thenReturn(contrat);
        when(typeContratRepository.findByCodeExterne(anyString())).thenReturn(Optional.of(type));
        when(contratRepository.save(any())).thenReturn(contrat);
        when(contratWebServiceMapper.toDto(any(), any())).thenReturn(exec.getEnvoi().getContrat());
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.of(plateforme));
        var result = contratsService.save(exec.getEnvoi().getContrat(), "pfUid");
        assertNotNull(result);
        assertEquals("CONS_allotie", result.getReferenceLibre());

        verify(typeContratRepository, times(1)).findByCodeExterne(anyString());
        verify(contratRepository, times(3)).save(any());
        verify(contratWebServiceMapper, times(1)).toDto(any(), any());
    }


    @Test
    public void givenXML_whenSaveContrat_WithChorus_thenSaveContrat_Without_Numerotation() throws JAXBException, ApplicationBusinessException {
        JAXBContext context = JAXBContext.newInstance(Exec.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Spécifiez le chemin du fichier XML
        File fichierXML = new File("src/test/resources/xml/contrat.xml");
        var exec = (Exec) unmarshaller.unmarshal(fichierXML);
        assertNotNull(exec.getEnvoi().getContrat());
        Contrat contrat = new Contrat();
        contrat.setReferenceLibre("CONS_allotie");
        TypeContrat type = new TypeContrat();
        type.setCodeExterne("MARCHES");
        type.setModeEchangeChorus(1);
        contrat.setType(type);
        contrat.setCreateur(new Utilisateur());
        var plateforme = new Plateforme();
        plateforme.setUid("uid");
        plateforme.setMpeUid("mpeUid");
        contrat.getCreateur().setPlateforme(plateforme);
        contrat.setPlateforme(plateforme);

        Service service = new Service();
        service.setId(1L);
        service.setNomCourt("Mon service");
        service.setEchangesChorus(true);
        when(serviceRepository.findByIdExterneAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(service));

        Utilisateur createur = new Utilisateur();
        createur.setPlateforme(new Plateforme());
        contrat.setCreateur(createur);
        when(contratWebServiceMapper.toContrat(any())).thenReturn(contrat);
        when(typeContratRepository.findByCodeExterne(anyString())).thenReturn(Optional.of(type));
        when(contratRepository.save(any())).thenReturn(contrat);
        when(contratWebServiceMapper.toDto(any(), any())).thenReturn(exec.getEnvoi().getContrat());
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.of(plateforme));
        var result = contratsService.save(exec.getEnvoi().getContrat(), "pfUid");
        assertNotNull(result);
        assertNull(contrat.getNumero());
        assertNull(contrat.getNumeroLong());

        verify(typeContratRepository, times(1)).findByCodeExterne(anyString());
        verify(contratRepository, times(3)).save(any());
        verify(contratWebServiceMapper, times(1)).toDto(any(), any());
        verify(numerotationService, times(0)).numeroter(any(), any(), any());
    }


    @Test
    public void givenXML_whenSaveContrat_thenThrowTypeContratException() throws JAXBException, ApplicationBusinessException {
        JAXBContext context = JAXBContext.newInstance(Exec.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Spécifiez le chemin du fichier XML
        File fichierXML = new File("src/test/resources/xml/contrat_without_type.xml");
        var exec = (Exec) unmarshaller.unmarshal(fichierXML);
        assertNotNull(exec.getEnvoi().getContrat());
        Contrat contrat = new Contrat();
        contrat.setReferenceLibre("CONS_allotie");
        when(contratWebServiceMapper.toContrat(any())).thenReturn(contrat);
        Service service = new Service();
        service.setId(1L);
        service.setNomCourt("Mon service");
        when(serviceRepository.findByIdExterneAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(service));

        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.of(new Plateforme()));
        var thrown = assertThrows(ContratMPEException.class, () -> contratsService.save(exec.getEnvoi().getContrat(), "pfUid"));
        assertEquals(thrown.getMessage(), "Le type de contrat est obligatoire");
        verify(typeContratRepository, times(0)).findByCodeExterne(anyString());
        verify(contratRepository, times(0)).save(any());
        verify(contratWebServiceMapper, times(0)).toDto(any(), any());
    }

    @Test
    public void givenXML_whenSaveContrat_thenThrowCreateurException() throws JAXBException, ApplicationBusinessException {
        JAXBContext context = JAXBContext.newInstance(Exec.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Spécifiez le chemin du fichier XML
        File fichierXML = new File("src/test/resources/xml/contrat_without_type.xml");
        var exec = (Exec) unmarshaller.unmarshal(fichierXML);
        assertNotNull(exec.getEnvoi().getContrat());
        Contrat contrat = new Contrat();
        contrat.setReferenceLibre("CONS_allotie");
        contrat.setCreateur(new Utilisateur());
        when(contratWebServiceMapper.toContrat(any())).thenReturn(contrat);
        var plateforme = new Plateforme();
        plateforme.setMpeUid("mpeUid");

        Service service = new Service();
        service.setId(1L);
        service.setNomCourt("Mon service");
        when(serviceRepository.findByIdExterneAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(service));
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.of(plateforme));
        var thrown = assertThrows(ContratMPEException.class, () -> contratsService.save(exec.getEnvoi().getContrat(),"pfUid"));
        assertEquals(thrown.getMessage(), "Le type de contrat est obligatoire");
        verify(typeContratRepository, times(0)).findByCodeExterne(anyString());
        verify(contratRepository, times(0)).save(any());
        verify(contratWebServiceMapper, times(0)).toDto(any(), any());
    }

    @Test
    public void givenXML_whenSaveContrat_thenThrowPlateformeException() throws JAXBException, ApplicationBusinessException {
        JAXBContext context = JAXBContext.newInstance(Exec.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Spécifiez le chemin du fichier XML
        File fichierXML = new File("src/test/resources/xml/contrat_without_type.xml");
        var exec = (Exec) unmarshaller.unmarshal(fichierXML);
        assertNotNull(exec.getEnvoi().getContrat());
        Contrat contrat = new Contrat();
        contrat.setReferenceLibre("CONS_allotie");
        Utilisateur createur = new Utilisateur();
        contrat.setCreateur(createur);
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.empty());
        var thrown = assertThrows(ContratMPEException.class, () -> contratsService.save(exec.getEnvoi().getContrat(),"pfUid"));
        assertEquals(thrown.getMessage(), "Plateforme non trouvée pfUid");
        verify(typeContratRepository, times(0)).findByCodeExterne(anyString());
        verify(contratRepository, times(0)).save(any());
        verify(contratWebServiceMapper, times(0)).toDto(any(), any());
    }

    @Test
    public void given_updateAllStatus_when_updateAllStatus_then_updateAllStatus() {
        final var max = 200;
        final Pageable page1 = PageRequest.of(0, max);
        var contrats = new ArrayList<Contrat>();
        for (int i = 0; i < max; i++) {
            var contrat = new Contrat();
            contrats.add(contrat);
        }
        when(contratRepository.count()).thenReturn(201L);
        lenient().when(contratRepository.findAll(page1)).thenReturn(new PageImpl<>(contrats));
        var result = contratsService.updateAllStatus();
        assertEquals(max + 1, result);
    }

    @Test
    public void given_contrat_aNotifier_when_NotifierParMessagerie_then_update_date_notification() throws ApplicationException {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setTypeNotification(TypeNotification.MESSAGERIE_SECURISEE);
        contrat.setDateNotification(LocalDate.now().minusYears(1));
        when(contratRepository.findByUuidAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(contrat));
        contratsService.notifier("uuid", "pfUid");
        assertEquals(StatutContrat.Notifie, contrat.getStatut());
        assertEquals(LocalDate.now(), contrat.getDateNotification(), "La date de notification doit être mise à jour");
    }

    @Test
    public void given_contrat_aNotifier_when_NotifierManuellement_then_update_date_notification() throws ApplicationException {
        var contrat = new Contrat();
        when(contratRepository.findById(any())).thenReturn(Optional.of(contrat));
        when(utilisateurRepository.findById(any())).thenReturn(Optional.of(new Utilisateur()));
        contratsService.notifierContrat(1L, 1L);
        assertEquals(TypeNotification.MANUELLE, contrat.getTypeNotification());
        assertEquals(StatutContrat.Notifie, contrat.getStatut());
        assertEquals(LocalDate.now(), contrat.getDateNotification(), "La date de notification doit être mise à jour");
    }

}
