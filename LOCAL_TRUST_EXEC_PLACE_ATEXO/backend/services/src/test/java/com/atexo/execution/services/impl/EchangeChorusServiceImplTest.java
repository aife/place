package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.repository.referentiels.StatutChorusRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EchangeChorusServiceImplTest {

    @Mock
    EchangeChorusRepository echangeChorusRepository;

    @Mock
    EchangeChorusMapper echangeChorusMapper;

    @Mock
    StatutChorusRepository statutChorusRepository;

    @Mock
    PlateformeRepository plateformeRepository;

    @Mock
    ContratRepository contratRepository;

    @InjectMocks
    EchangeChorusServiceImpl echangeChorusService;

    @Test
    void updateEchangeChorus() throws ApplicationBusinessException {
        var plateforme = new Plateforme();
        plateforme.setMpeUid("pfUid");
        var echangeType = new EchangeChorusType();
        echangeType.setId(1);
        echangeType.setStatut("COM");
        echangeType.setUuidContrat("uuidContrat");
        when(contratRepository.findByUuidAndPlateformeMpeUid(anyString(), anyString())).thenReturn(Optional.of(new Contrat()));
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.of(plateforme));
        when(echangeChorusRepository.findByIdExterneAndPlateformeMpeUid(anyString(), anyString())).thenReturn(Optional.of(new EchangeChorus()));
        when(echangeChorusRepository.save(any(EchangeChorus.class))).thenReturn(new EchangeChorus());
        when(echangeChorusMapper.toDto(any(EchangeChorus.class))).thenReturn(new EchangeChorusDTO());
        assertNotNull(echangeChorusService.updateEchangeChorus("pfUid", echangeType));
    }

    @Test
    void updateEchangeChorus_shouldThrowApplicationBusinessException() {
        var plateforme = new Plateforme();
        plateforme.setMpeUid("pfUid");
        var echangeType = new EchangeChorusType();
        echangeType.setId(1);
        echangeType.setStatut("COM");
        when(plateformeRepository.findByMpeUidOrUtilisateurTechnique(anyString())).thenReturn(Optional.empty());
        assertThrows(ApplicationBusinessException.class, () -> echangeChorusService.updateEchangeChorus("pfUid", echangeType));
    }
}
