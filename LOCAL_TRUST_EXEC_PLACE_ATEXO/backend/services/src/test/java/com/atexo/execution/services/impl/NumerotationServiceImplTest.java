package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.NiveauEnum;
import com.atexo.execution.common.dto.NumerotationTypeEnum;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationChampRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationCompteurRepository;
import com.atexo.execution.server.repository.crud.ParametrageNumerotationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class NumerotationServiceImplTest {
    @Mock
    private ContratRepository contratRepository;

    @Mock
    private ParametrageNumerotationRepository parametrageNumerotationRepository;
    @Mock
    private ParametrageNumerotationChampRepository parametrageNumerotationChampRepository;
    @Mock
    private ParametrageNumerotationCompteurRepository parametrageNumerotationCompteurRepository;
    @InjectMocks
    private NumerotationServiceImpl numerotationService;

    private ParametrageNumerotation parametrageNumerotationService;

    private ParametrageNumerotationCompteur parametrageNumerotationCompteur;

    @BeforeEach
    void setup() {
        parametrageNumerotationService = new ParametrageNumerotation();
        parametrageNumerotationService.setUniqueNumero(true);
        parametrageNumerotationService.setType(NumerotationTypeEnum.CONTRAT_NUMERO);


        ParametrageNumerotationConfiguration configurationCompteurMs = getConfiguration("MS", "/", 2, getParametrageNumerotationChampCompteurMs());

        ParametrageNumerotationConfiguration configurationCompteurPlateforme = getConfiguration("CP", "%", 1, getParametrageNumerotationChampCompteurPlateforme());

        ParametrageNumerotationConfiguration configurationNumeroConsultation = getConfiguration("NC", "_", 0, getParametrageNumerotationChampNumeroConsultation());

        ParametrageNumerotationConfiguration configurationDateCreation = getConfiguration("DC", "-", 3, getParametrageNumerotationChampDateCreation());
        ParametrageNumerotationConfiguration configurationNumeroLot = getConfiguration("NL", "@", 4, getParametrageNumerotationChampNumeroLot());
        ParametrageNumerotationConfiguration configurationCategorie = getConfiguration("C", "@", 5, getParametrageNumerotationChampCategorie());
        configurationCategorie.setActif(false);

        parametrageNumerotationService.setConfigurations(Set.of(configurationCategorie, configurationNumeroConsultation, configurationNumeroLot, configurationCompteurPlateforme, configurationDateCreation, configurationCompteurMs));

    }

    @Test
    public void Given_ParametrageNumerotation_When_ServiceOrganismeOrPlateforme_is_Null() {
        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        var thrown = assertThrows(ApplicationException.class, () -> numerotationService.getNumerotation(contrat, NumerotationTypeEnum.CONTRAT_NUMERO, null));
        assertEquals(thrown.getMessage(), "Aucun paramétrage de numérotation de contrat n'est défini pour la plateforme null l'organisme null et le service null");
    }

    @Test
    public void Given_ParametrageNumerotation_When_AucunParamtrage_Then_ThrowException() {
        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Service service = new Service();
        service.setId(1L);
        contrat.setService(service);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        contrat.getService().setOrganisme(organisme);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);
        var thrown = assertThrows(ApplicationException.class, () -> {
            numerotationService.getNumerotation(contrat, NumerotationTypeEnum.CONTRAT_NUMERO, null);
        });
        assertEquals(thrown.getMessage(), "Aucun paramétrage de numérotation de contrat n'est défini pour la plateforme 1 l'organisme 1 et le service 1");
    }


    @Test
    public void Given_PlateformeNullAndDateCreationNull_When_NumeroterService_Then_ReturnOnlyNumeoConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        contrat.setService(service);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, null, null, 1L, NiveauEnum.SERVICE);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_", numero);
    }

    @Test
    public void Given_ConsultationNoAllotie_When_NumeroterService_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));

        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }

    @Test
    public void Given_ParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterService_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);
        //When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }


    @Test
    public void Given_ProcedureMs_When_NumeroterService_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%MS001/DC2021-", numero);
    }


    @Test
    public void Given_PlateformeNullAndDateCreationNull_When_NumeroterOrganisme_Then_ReturnOnlyNumeoConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, null, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_", numero);
    }

    @Test
    public void Given_Montant_When_Numeroter_Then_ReturnMontant() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        ParametrageNumerotation numerotation = new ParametrageNumerotation();
        ParametrageNumerotationConfiguration configurationMontant = getConfiguration("M", "EUR", 6, getParametrageNumerotationChampMontant());
        numerotation.setConfigurations(Set.of(configurationMontant));

        NumerotationResult numerotationResult = new NumerotationResult(numerotation, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("M1000.0EUR", numero);
    }

    @Test
    public void Given_Montant_When_Numeroter_Then_ReturnFormatedMontant() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        ParametrageNumerotation numerotation = new ParametrageNumerotation();
        ParametrageNumerotationChamp parametrageNumerotationChampMontant = getParametrageNumerotationChampMontant();
        parametrageNumerotationChampMontant.setFormat("%.0f");
        ParametrageNumerotationConfiguration configurationMontant = getConfiguration("M", "EUR", 6, parametrageNumerotationChampMontant);
        numerotation.setConfigurations(Set.of(configurationMontant));

        NumerotationResult numerotationResult = new NumerotationResult(numerotation, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("M1000EUR", numero);
    }

    @Test
    public void Given_DateFinContrat_When_Numeroter_Then_ReturnDateFinContrat() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        contrat.setDateFinContrat(LocalDate.of(2021, 1, 1));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        ParametrageNumerotation numerotation = new ParametrageNumerotation();
        ParametrageNumerotationChamp parametrageNumerotationChampDateFinContrat = getParametrageNumerotationChampDateFinContrat();
        ParametrageNumerotationConfiguration configurationDateFinContrat = getConfiguration("[", "]", 6, parametrageNumerotationChampDateFinContrat);
        numerotation.setConfigurations(Set.of(configurationDateFinContrat));
        NumerotationResult numerotationResult = new NumerotationResult(numerotation, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("[2021-01-01]", numero);
    }
  @Test
    public void Given_DateFinContrat_When_Numeroter_Then_ReturnFormatedDateFinContrat() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        contrat.setDateFinContrat(LocalDate.of(2021, 1, 1));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        ParametrageNumerotation numerotation = new ParametrageNumerotation();
        ParametrageNumerotationChamp parametrageNumerotationChampDateFinContrat = getParametrageNumerotationChampDateFinContrat();
      parametrageNumerotationChampDateFinContrat.setFormat("yyyyMMdd");
        ParametrageNumerotationConfiguration configurationDateFinContrat = getConfiguration("[", "]", 6, parametrageNumerotationChampDateFinContrat);
        numerotation.setConfigurations(Set.of(configurationDateFinContrat));
      NumerotationResult numerotationResult = new NumerotationResult(numerotation, 1L, 1L, 1L, NiveauEnum.ORGANISME);
      var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("[20210101]", numero);
    }

    @Test
    public void Given_ConsultationNoAllotie_When_NumeroterOrganisme_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }

    @Test
    public void Given_ParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterOrganisme_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        //When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }


    @Test
    public void Given_ProcedureMs_When_NumeroterOrganisme_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%MS001/DC2021-", numero);
    }

    @Test
    public void Given_PlateformeNullAndDateCreationNull_When_NumeroterPlateforme_Then_ReturnOnlyNumeoConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, null, 1L, 1L, NiveauEnum.PLATEFORME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%", numero);
    }

    @Test
    public void Given_ConsultationNoAllotie_When_NumeroterPlateforme_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }

    @Test
    public void Given_ParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterPlateforme_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        //When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%DC2021-", numero);
    }


    @Test
    public void Given_ProcedureMs_When_NumeroterPlateforme_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);


        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00001%MS001/DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurConsultationNoAllotie_When_NumeroterService_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));

        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);


        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);

        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterService_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }


    @Test
    public void Given_ExistedCompteurProcedureMs_When_NumeroterService_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.SERVICE);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%MS006/DC2021-", numero);
    }


    @Test
    public void Given_ExistedCompteurPlateformeNullAndDateCreationNull_When_NumeroterOrganisme_Then_ReturnOnlyNumeoConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_", numero);
    }

    @Test
    public void Given_ExistedCompteurConsultationNoAllotie_When_NumeroterOrganisme_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterOrganisme_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }


    @Test
    public void Given_ExistedCompteurProcedureMs_When_NumeroterOrganisme_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.ORGANISME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%MS006/DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurPlateformeNullAndDateCreationNull_When_NumeroterPlateforme_Then_ReturnOnlyNumeoConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        service.setId(1L);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, null, 1L, 1L, NiveauEnum.PLATEFORME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%", numero);
    }

    @Test
    public void Given_ExistedCompteurConsultationNoAllotie_When_NumeroterPlateforme_Then_ReturnCompteurWithoutNumeroConsultation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);


        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurParametrageNumerotationCompteurAndDateCreationNotNull_When_NumeroterPlateforme_Then_ReturnCompteurAndCreation() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);


        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        //When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%DC2021-", numero);
    }


    @Test
    public void Given_ExistedCompteurProcedureMs_When_NumeroterPlateforme_Then_ReturnCompteurMs() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%MS006/DC2021-", numero);
    }

    @Test
    public void Given_ExistedCompteurProcedureMs_When_NumeroterPlateforme_Then_ReturnCompteurMsWithGlobalSuffix() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        parametrageNumerotationService.setSuffix("_TESTSUFFIX");
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("NC00215_CP00006%MS006/DC2021-_TESTSUFFIX", numero);
    }

    @Test
    public void Given_ExistedCompteurProcedureMs_When_NumeroterPlateforme_Then_ReturnCompteurMsWithGlobalPrefix() throws ApplicationException {

        var contrat = new Contrat();
        CategorieConsultation categorie = new CategorieConsultation();
        contrat.setMontant(BigDecimal.valueOf(1000.0));
        categorie.setCode("SERVICES");
        contrat.setCategorie(categorie);
        contrat.setDateCreation(LocalDateTime.of(2021, 1, 1, 0, 0));
        Consultation consultation = new Consultation();
        consultation.setNumero("00215");
        Procedure procedure = new Procedure();
        procedure.setCode("MS");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);
        Service service = new Service();
        service.setId(1L);
        Organisme organisme = new Organisme();
        organisme.setId(1L);
        service.setOrganisme(organisme);
        contrat.setService(service);
        Plateforme plateforme = new Plateforme();
        plateforme.setId(1L);
        contrat.setPlateforme(plateforme);

        ParametrageNumerotationCompteur compteur = new ParametrageNumerotationCompteur();
        compteur.setCompteur(5);
        when(parametrageNumerotationCompteurRepository.findByParametrageNumerotationChampIdAndNiveauAndIdPlateformeOrganismeService(any(), any(), any())).thenReturn(compteur);

        parametrageNumerotationService.setPrefix("TESTPREFIX_");
        NumerotationResult numerotationResult = new NumerotationResult(parametrageNumerotationService, 1L, 1L, 1L, NiveauEnum.PLATEFORME);
        // When
        var numero = numerotationService.numeroter(contrat, numerotationResult, "messageErreur");
        assertNotNull(numero);
        assertEquals("TESTPREFIX_NC00215_CP00006%MS006/DC2021-", numero);
    }


    private static ParametrageNumerotationConfiguration getConfiguration(String prefix, String separateur, int ordre, ParametrageNumerotationChamp champCompteurPlateforme) {
        ParametrageNumerotationConfiguration configurationCompteurPlateforme = new ParametrageNumerotationConfiguration();
        configurationCompteurPlateforme.setOrdre(ordre);
        configurationCompteurPlateforme.setId((long) ordre);
        configurationCompteurPlateforme.setSeparateur(separateur);
        configurationCompteurPlateforme.setPrefix(prefix);
        configurationCompteurPlateforme.setActif(true);
        configurationCompteurPlateforme.setParametrageNumerotationChamp(champCompteurPlateforme);
        return configurationCompteurPlateforme;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampNumeroConsultation() {
        ParametrageNumerotationChamp parametrageNumerotationChamp2 = new ParametrageNumerotationChamp();
        parametrageNumerotationChamp2.setClef("consultation_numero");
        parametrageNumerotationChamp2.setDescription("Numéro de la Consultation");
        parametrageNumerotationChamp2.setMethodeValeur("contrat.consultation.numero()");
        return parametrageNumerotationChamp2;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampNumeroLot() {
        ParametrageNumerotationChamp parametrageNumerotationChamp2 = new ParametrageNumerotationChamp();
        parametrageNumerotationChamp2.setClef("lot_numero");
        parametrageNumerotationChamp2.setDescription("Numéro du lot");
        parametrageNumerotationChamp2.setMethodeValeur("contrat.numeroLot()");
        return parametrageNumerotationChamp2;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampCategorie() {
        ParametrageNumerotationChamp parametrageNumerotationChamp2 = new ParametrageNumerotationChamp();
        parametrageNumerotationChamp2.setClef("categorie");
        parametrageNumerotationChamp2.setDescription("Categorie du contrat");
        parametrageNumerotationChamp2.setMethodeValeur("contrat.categorie.code()");
        return parametrageNumerotationChamp2;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampMontant() {
        ParametrageNumerotationChamp parametrageNumerotationChamp2 = new ParametrageNumerotationChamp();
        parametrageNumerotationChamp2.setClef("montant");
        parametrageNumerotationChamp2.setDescription("Montant du contrat");
        parametrageNumerotationChamp2.setMethodeValeur("contrat.montant()");
        return parametrageNumerotationChamp2;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampDateFinContrat() {
        ParametrageNumerotationChamp parametrageNumerotationChamp2 = new ParametrageNumerotationChamp();
        parametrageNumerotationChamp2.setClef("dateFinContrat");
        parametrageNumerotationChamp2.setDescription("Date de fin du contrat");
        parametrageNumerotationChamp2.setMethodeValeur("contrat.dateFinContrat()");
        return parametrageNumerotationChamp2;
    }


    private static ParametrageNumerotationChamp getParametrageNumerotationChampCompteurPlateforme() {
        ParametrageNumerotationChamp parametrageNumerotationChampCompteur = new ParametrageNumerotationChamp();
        parametrageNumerotationChampCompteur.setClef("compteur_plateforme");
        parametrageNumerotationChampCompteur.setDescription("Compteur de la plateforme");
        parametrageNumerotationChampCompteur.setMethodeValeur("compteurPlateforme()");
        parametrageNumerotationChampCompteur.setCompteur(true);
        parametrageNumerotationChampCompteur.setLongueur(5);
        return parametrageNumerotationChampCompteur;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampCompteurMs() {
        ParametrageNumerotationChamp parametrageNumerotationChampCompteur = new ParametrageNumerotationChamp();
        parametrageNumerotationChampCompteur.setClef("compteur_plateforme");
        parametrageNumerotationChampCompteur.setDescription("Compteur de la plateforme");
        parametrageNumerotationChampCompteur.setMethodeValeur("compteurPlateforme()");
        parametrageNumerotationChampCompteur.setMethodeConditionnement("contrat.consultation.procedure.code()");
        parametrageNumerotationChampCompteur.setValeurConditionnement("MS");
        parametrageNumerotationChampCompteur.setCompteur(true);
        parametrageNumerotationChampCompteur.setLongueur(3);
        return parametrageNumerotationChampCompteur;
    }

    private static ParametrageNumerotationChamp getParametrageNumerotationChampDateCreation() {
        ParametrageNumerotationChamp parametrageNumerotationChampCompteur = new ParametrageNumerotationChamp();
        parametrageNumerotationChampCompteur.setClef("contrat_creation");
        parametrageNumerotationChampCompteur.setDescription("Date de création du contrat");
        parametrageNumerotationChampCompteur.setMethodeValeur("contrat.dateCreation()");
        parametrageNumerotationChampCompteur.setFormat("yyyy");
        return parametrageNumerotationChampCompteur;
    }

}
