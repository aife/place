package com.atexo.execution.services.impl;

import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.ContactType;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.repository.crud.ContratRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactsServiceImplTest {

    @Mock
    ContratRepository contratRepository;

    @Mock
    ContactMapper contactMapper;

    @InjectMocks
    ContactsServiceImpl contactsService;

    @Test
    void getContactsByContrat() throws ApplicationBusinessException {
        var contrat = buildContratWithContacts(1L);
        when(contratRepository.findByUuidAndPlateformeMpeUid(anyString(),anyString())).thenReturn(Optional.of(contrat));
        when(contactMapper.toContactType(any(Contact.class))).thenReturn(new ContactType());
        var result = contactsService.getContactsByContrat("uuid","pfUid");
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
    }

    @Test
    void getContactsByContratAndSousContrats() throws ApplicationBusinessException {
        var contratChapeay = buildContratWithContacts(0L);
        for (var i : List.of(1L, 2L, 3L)) {
            var sousContrat = buildContratWithContacts(i);
            contratChapeay.getContrats().add(sousContrat);
        }
        when(contratRepository.findByUuidAndPlateformeMpeUid(anyString(),anyString())).thenReturn(Optional.of(contratChapeay));
        when(contactMapper.toContactType(any(Contact.class))).thenReturn(new ContactType());
        var result = contactsService.getContactsByContrat("uuid","pfUid");
        assertFalse(result.isEmpty());
        assertEquals(4, result.size());
    }

    private Contrat buildContratWithContacts(Long i) {
        var contrat = new Contrat();
        contrat.setNumero(UUID.randomUUID().toString());
        var contratEtablissement = new ContratEtablissement();
        contratEtablissement.setIdExterne(UUID.randomUUID().toString());
        contrat.setAttributaire(contratEtablissement);
        var etablissement = new Etablissement();
        etablissement.setIdExterne(UUID.randomUUID().toString());
        contrat.getAttributaire().setEtablissement(etablissement);
        var contact = new Contact();
        contact.setIdExterne(UUID.randomUUID().toString());
        contact.setEmail(i + "-test@test.com");
        contrat.getAttributaire().getEtablissement().setContacts(List.of(contact));
        contratEtablissement.setContact(contact);
        return contrat;
    }
}
