package com.atexo.execution.services.impl;

import com.atexo.execution.common.dto.docgen.ChampsFusionDTO;
import com.atexo.execution.server.model.ChampFusion;
import com.atexo.execution.server.repository.referentiels.ChampsFusionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ChampsFusionServiceImplTest {

    @InjectMocks
    ChampsFusionServiceImpl champsFusionService;

    @Mock
    ChampsFusionRepository champsFusionRepository;

    @BeforeEach
    void setUp() {
        lenient().when(champsFusionRepository.findAll()).thenReturn(new ArrayList<>());
    }

    @Test
    public void givenEmpty_whenGetFlatChampsFusion_thenSucceed() {
        var result = champsFusionService.getChampsFusion(true, false, 10);
        assertFalse(result.isEmpty(), "champs de fusion contrat n'est pas vide");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetFlatChampsFusion_thenSucceed() {
        var champFusionRef = new ChampFusion();
        champFusionRef.setCode("contrat.objet");
        champFusionRef.setLabel("Objet du contrat");
        when(champsFusionRepository.findAll()).thenReturn(List.of(champFusionRef));
        var result = champsFusionService.getChampsFusion(true, false, 10);
        assertFalse(result.isEmpty(), "champs de fusion contrat n'est pas vide");
        var champ = result.stream().filter(ch -> ch.getChampFusion().equals("contrat.objet")).findFirst().orElseThrow(IllegalArgumentException::new);
        assertFalse(champ.getLibelle().isEmpty(), "libelle du champ de fusion contrat.objet n'est pas vide");
        assertEquals("Objet du contrat", champ.getLibelle(), "libelle du champ de fusion contrat.objet est Objet du contrat");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenEagerFlatFalse_thenSucceed() {
        var result = champsFusionService.getChampsFusion(false, false, 10);
        var domaines = ChampsFusionServiceImpl.DOMAINES.keySet().stream().collect(Collectors.joining(","));
        assertEquals(result.size(), result.stream().filter(ch -> domaines.indexOf(ch.getChampFusion().substring(0, ch.getChampFusion().indexOf("."))) != -1).collect(Collectors.toList()).size(), "tous les champs de fusion sont des champs de fusion du domaine contrat ou attributaire");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenEagerFlatTrue_thenSucceed_andCollectionExists() {
        var result = champsFusionService.getChampsFusion(true, false, 10);
        var champListe = result.stream().filter(ChampsFusionDTO::isCollection).findFirst().orElseThrow(IllegalArgumentException::new);
        assertNotNull(champListe, "au moins un champs liste existe");
        assertFalse(champListe.isSimple(), "le champ liste est complexe");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenEagerFlatFalse_thenSucceed_andLabeIsFilled() {
        var champFusionRef = new ChampFusion();
        champFusionRef.setCode("contrat.objet");
        champFusionRef.setLabel("Objet du contrat");
        when(champsFusionRepository.findAll()).thenReturn(List.of(champFusionRef));
        var result = champsFusionService.getChampsFusion(false, false, 10);
        var champs = result.stream().filter(ch -> ch.getLibelle() != null).collect(Collectors.toList());
        assertTrue(champs.size() >= 2, "Au moins deux champs ont un libellé, un qui vient de la BDD et l'autre de l'annotation dans ContratDTO");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetFlatByDomaine_thenSucceed() {
        var result = champsFusionService.getChampsFusion(false, false, 10, "contrat");
        assertFalse(result.isEmpty(), "Les champs du domaine contrat ne sont pas vides");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetFlatByUnkownDomaine_thenThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> champsFusionService.getChampsFusion(false, false, 10, "UNKNOWN"), "Domaine introuvable");
    }

    @Test
    public void givenEmpty_whenGetTreeChampsFusion_thenSucceed() {
        var result = champsFusionService.getChampsFusion(true, true, 10);
        assertFalse(result.isEmpty(), "champs de fusion contrat n'est pas vide");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetTreeChampsFusion_thenSucceed() {
        var champFusionRef = new ChampFusion();
        champFusionRef.setCode("contrat.objet");
        champFusionRef.setLabel("Objet du contrat");
        when(champsFusionRepository.findAll()).thenReturn(List.of(champFusionRef));
        var result = champsFusionService.getChampsFusion(true, true, 10);
        assertFalse(result.isEmpty(), "champs de fusion contrat n'est pas vide");
        var champ = result.stream().filter(ch -> ch.getChampFusion().equals("contrat")).findFirst().orElseThrow(IllegalArgumentException::new);
        assertEquals(87, champ.getSousChampsFusion().size(), "le champ de fusion contrat a 75 sous-champs");
        var sousChamp = champ.getSousChampsFusion().stream().filter(ch -> ch.getChampFusion().equals("contrat.objet")).findFirst().orElseThrow(IllegalArgumentException::new);
        assertFalse(sousChamp.getLibelle().isEmpty(), "libelle du champ de fusion contrat.objet n'est pas vide");
        assertEquals("Objet du contrat", sousChamp.getLibelle(), "libelle du champ de fusion contrat.objet est Objet du contrat");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenEagerTreeTrue_thenSucceed_andSousElementsExists() {
        var result = champsFusionService.getChampsFusion(true, true, 3);
        var champListeContrat = result.stream()
                .filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion()))
                .filter(champsFusionDTO -> champsFusionDTO.getChampFusion().equals("contrat"))
                .findFirst().orElseThrow(IllegalArgumentException::new);
        assertNotNull(champListeContrat, "au moins un champs avec des sous-éléments existe");
        assertEquals(3, result.size());
        assertEquals(87, champListeContrat.getSousChampsFusion().size());
        assertEquals(15, champListeContrat.getSousChampsFusion().stream().filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
        assertEquals(72, champListeContrat.getSousChampsFusion().stream().filter(champsFusionDTO -> CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
        assertFalse(champListeContrat.isSimple(), "le champ avec des sous-éléments est complexe");
        var champListEvenement = result.stream()
                .filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion()))
                .filter(champsFusionDTO -> champsFusionDTO.getChampFusion().equals("evenement"))
                .findFirst().orElseThrow(IllegalArgumentException::new);
        assertNotNull(champListEvenement, "au moins un champs avec des sous-éléments existe");
        assertEquals(23, champListEvenement.getSousChampsFusion().size());
        assertEquals(5, champListEvenement.getSousChampsFusion().stream().filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
        assertEquals(18, champListEvenement.getSousChampsFusion().stream().filter(champsFusionDTO -> CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
        assertFalse(champListEvenement.isSimple(), "le champ avec des sous-éléments est complexe");
        var listeEtablissement = result.stream()
                .filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion()))
                .filter(champsFusionDTO -> champsFusionDTO.getChampFusion().equals("etablissement"))
                .findFirst().orElseThrow(IllegalArgumentException::new);
        assertNotNull(listeEtablissement, "au moins un champs avec des sous-éléments existe");
        assertEquals(11, listeEtablissement.getSousChampsFusion().size());
        assertEquals(2, listeEtablissement.getSousChampsFusion().stream().filter(champsFusionDTO -> !CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
        assertEquals(9, listeEtablissement.getSousChampsFusion().stream().filter(champsFusionDTO -> CollectionUtils.isEmpty(champsFusionDTO.getSousChampsFusion())).count());
    }

    @Test
    public void givenCorrespondingChampsFusion_whenEagerTreeFalse_thenSucceed_andLabeIsFilled() {
        var champFusionRef = new ChampFusion();
        champFusionRef.setCode("contrat.objet");
        champFusionRef.setLabel("Objet du contrat");
        when(champsFusionRepository.findAll()).thenReturn(List.of(champFusionRef));
        var result = champsFusionService.getChampsFusion(false, true, 10);
        var champs = result.stream().filter(ch -> ch.getLibelle() != null).collect(Collectors.toList());
        assertTrue(champs.size() >= 2, "Au moins deux champs ont un libellé, un qui vient de la BDD et l'autre de l'annotation dans ContratDTO");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetTreeByDomaine_thenSucceed() {
        var result = champsFusionService.getChampsFusion(false, true, 10, "contrat");
        assertFalse(result.isEmpty(), "Les champs du domaine contrat ne sont pas vides");
    }

    @Test
    public void givenCorrespondingChampsFusion_whenGetTreeByUnkownDomaine_thenThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> champsFusionService.getChampsFusion(false, true, 10, "UNKNOWN"), "Domaine introuvable");
    }
}
