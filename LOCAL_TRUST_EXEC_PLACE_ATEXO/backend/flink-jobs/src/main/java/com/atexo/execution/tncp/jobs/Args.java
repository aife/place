package com.atexo.execution.tncp.jobs;

import com.beust.jcommander.Parameter;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public class Args {

    // pour flink les topics d'envoi et de réceptions sont inversés, on les garde pour la compatibilité
    // le topic de recetpion est : contrat-envoi
    // le topic d'envoi est : contrat-reception
    @Parameter(names = {"--broker", "-br"}, description = "Host du broker Kafka")
    private String broker = "kafka:9092";

    @Parameter(names = {"--topic-reception", "-tr"}, description = "Topic de reception des evenements TNCP")
    private String topicReception = "tncpContrats";

    @Parameter(names = {"--topic-envoi", "-te"}, description = "Topic d'envoi des evenements TNCP")
    private String topicEnvoi = "tncpContratsSuivi";

    @Parameter(names = {"--groupe", "-grp"}, description = "Groupe de consommateurs TNCP")
    private String groupe = "tncp";

    public static String[] filterSpringBootArgs(String[] args) {
        // Filtrer les arguments
        List<String> allowedPrefixes = List.of("--broker", "-br", "--topic-reception", "-tr", "--topic-envoi", "-te");
        return Arrays.stream(args)
                .filter(arg -> allowedPrefixes.stream().anyMatch(arg::startsWith))
                .toArray(String[]::new);
    }


}
