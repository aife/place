package com.atexo.execution.tncp.jobs;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.execution.tncp.envoi.TNCPEnvoi;
import fr.atexo.execution.tncp.reception.TNCPResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Slf4j
public class ContratStream {
    public static void main(String[] args) throws Exception {
        //TODO à récupérer depuis les args
        var main = new Args();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(Args.filterSpringBootArgs(args));
        String inputTopic = main.getTopicReception();
        String consumerGroup = main.getGroupe();
        String address = main.getBroker();
        var objectMapper = new ObjectMapper();
        var environment = StreamExecutionEnvironment.getExecutionEnvironment();

        var consumer = createStringConsumerForTopic(inputTopic, address, consumerGroup, objectMapper);
        var producer = createProducerForTopic(main.getTopicEnvoi(), address, objectMapper);
        environment.fromSource(consumer, WatermarkStrategy.noWatermarks(), "contrat_source")
                .map(new RichMapFunction<TNCPEnvoi, TNCPResponse>() {
                    @Override
                    public TNCPResponse map(TNCPEnvoi tncpEnvoi) throws Exception {
                        var reponse = new TNCPResponse();
                        reponse.setUuidContrat(tncpEnvoi.getUuidContrat());
                        reponse.setMessage("Contrat envoyé par ISMAIL");
                        reponse.setStatut("FINI");
                        return reponse;
                    }
                })
                .sinkTo(producer);
        var tncpProperties = new Properties();
        try (var propertiesStream = ContratStream.class.getClassLoader().getResourceAsStream("tncp.properties")) {
            tncpProperties.load(propertiesStream);
            log.info("Properties TNCP loaded: {}", tncpProperties);
        }
        environment.execute();
    }

    public static KafkaSource<TNCPEnvoi> createStringConsumerForTopic(
            String topic, String kafkaAddress, String kafkaGroup, ObjectMapper objectMapper) {
        var consumer = KafkaSource.<TNCPEnvoi>builder()
                .setBootstrapServers(kafkaAddress)
                .setGroupId(kafkaGroup)
                .setTopics(topic)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new DeserializationSchema<>() {
                    @Override
                    public TNCPEnvoi deserialize(byte[] bytes) throws IOException {
                        var inputString = new String(bytes);
                        log.info("Received: {}", inputString);
                        return objectMapper.readValue(bytes, TNCPEnvoi.class);
                    }

                    @Override
                    public boolean isEndOfStream(TNCPEnvoi student) {
                        return false;
                    }

                    @Override
                    public TypeInformation<TNCPEnvoi> getProducedType() {
                        return TypeInformation.of(TNCPEnvoi.class);
                    }
                })
                .build();
        return consumer;
    }

    public static KafkaSink<TNCPResponse> createProducerForTopic(
            String topic, String kafkaAddress, ObjectMapper objectMapper) {
        var sink = KafkaSink.<TNCPResponse>builder()
                .setBootstrapServers(kafkaAddress)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic(topic)
                        .setValueSerializationSchema((SerializationSchema<TNCPResponse>) tncpResponse -> {
                            String outputString;
                            try {
                                outputString = new String(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(tncpResponse));
                                log.info("Sending: {}", outputString);
                            } catch (IOException e) {
                                log.error("Error while serializing", e);
                                return new byte[0];
                            }
                            return outputString.getBytes(StandardCharsets.UTF_8);
                        })
                        .build())
                .build();
        return sink;
    }
}
