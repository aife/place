# Installation EXEC

## Description du livrable

* execution-server : contient le war representant le back de l'application
* excution-front : contient un zip contenant un répertoire par client representant la partie front (angular) de l'application
* execution-config-{client} : contient la configuration spécifique à un client. La configuration par defaut est contenu dans le repertoire execution-config-demo
 
## Procédure de déploiement

### Déploiement du Back
Copier le war (contenu dans execution-server) dans le webapp du tomcat.

### Déploiement du Front

1- Déziper le zip execution-front-xx.xx.xx-dist.zip (contenu dans execution-front). Copier le répertoire du client voulu
 sur le serveur apache.
 
### Configuration

Le zip execution-config-{client} contient :
 ```
 classpath/
    modeledocument/
    config-client.properties
    config-env.properties

 ```
 
#### Paramètres de configuration

**Description du contenu du repertoire classpath** 

* modeledocument : contient la liste des documents modèles.
* config-env.properties : contient le paramètrage commun pour tous les clients, ces paramètres doivent être mise à jour selon la palateforem installée.
* config-client.properties : contient le paramètrage spécifique à un client (non commun).

NB : Le contenu du fichier config-client.properties override le contenu du fichier config-env.properties. 

**Déploiement**

1- Copier le repertoire classpath dans le repertoire shared de tomcat

2- Modifier les fichiers _config-env.properties_ et _config-client.properties_.

3- ajouter/modifier le lien JNDI référencé dans le fichier __config-env.properties__ par la propriété : __jndi.name__ dans le fichier __${catalina.home}/conf/context.xml__, ce lien pourait être présenté sous la forme suivante :
```xml
<Resource driverClassName="com.mysql.jdbc.Driver" maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/exec" password="execution" type="javax.sql.DataSource" url="jdbc:mysql://localhost:3306/execution?UseUnicode=true&amp;characterEncoding=utf8" username="execution"/>
```
