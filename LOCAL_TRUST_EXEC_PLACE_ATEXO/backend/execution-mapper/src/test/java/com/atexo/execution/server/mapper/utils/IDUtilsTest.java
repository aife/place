package com.atexo.execution.server.mapper.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IDUtilsTest {

    @Test
    void extractOrganisme() {
        assertEquals("pa", IDUtils.extractIdFromURI("A/B/C/D/pa"));
        assertEquals("pmi-min-1", IDUtils.extractIdFromURI("/api/v2/referentiels/organismes/pmi-min-1"));
    }
}
