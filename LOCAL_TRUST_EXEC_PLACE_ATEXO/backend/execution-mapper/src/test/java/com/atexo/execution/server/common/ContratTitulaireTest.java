package com.atexo.execution.server.common;

import com.atexo.execution.common.mpe.model.contrat.titulaire.Cpv;
import com.atexo.execution.server.common.mapper.contrat.titulaire.ContratTitulaireMapper;
import com.atexo.execution.server.common.mapper.contrat.titulaire.ContratTitulaireMapperImpl;
import com.atexo.execution.server.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ContratTitulaireTest {

    private ContratTitulaireMapper contratTitulaireMapper;

    @BeforeEach
    public void setUp() {
        contratTitulaireMapper = new ContratTitulaireMapperImpl();
    }


    @Test
    public void test_to_cpv() {
        //GIVEN
        CPV cpv = new CPV();
        cpv.setCode("45000000");
        cpv.setOrdre(0L);
        CPV cpv1 = new CPV();
        cpv1.setCode("03222314");
        cpv1.setOrdre(1L);
        CPV cpv2 = new CPV();
        cpv2.setCode("03222313");
        cpv2.setOrdre(2L);
        CPV cpv3 = new CPV();
        cpv3.setCode("72200000");
        cpv3.setOrdre(3L);
        Contrat contrat = new Contrat();
        contrat.setCodesCpv(Set.of(cpv, cpv1, cpv2, cpv3).stream().sorted(Comparator.comparing(CPV::getOrdre)).collect(Collectors.toCollection(LinkedHashSet::new)));

        //WHEN
        Cpv cpvResult = contratTitulaireMapper.toCpv(contrat);

        //THEN
        assertThat(new Cpv()).usingRecursiveComparison().isNotEqualTo(cpvResult);
    }

    @Test
    public void test_to_old_id_service() {
        //Given
        Service service = new Service();
        service.setOldIdExterne("pmi-min-1_3795");
        Contrat contrat = new Contrat();
        contrat.setService(service);

        //When
        Integer oldIdService = contratTitulaireMapper.toOldIdService(contrat);

        //Then
        assertThat(oldIdService).isEqualTo(3795);
    }

    @Test
    public void test_to_type_prix() {
        //Given
        TypePrix typePrix = new TypePrix();
        typePrix.setCode("ACTUALISABLE");
        TypePrix typePrix1 = new TypePrix();
        typePrix1.setCode("REVISABLE");
        Consultation consultation = new Consultation();
        consultation.setTypesPrix(Set.of(typePrix1, typePrix));

        //When
        Set<String> typesPrix = contratTitulaireMapper.toTypesPrix(consultation);

        //Then
        assertThat(typesPrix).usingRecursiveComparison().isEqualTo(Set.of("ACTUALISABLE", "REVISABLE"));
    }


    @Test
    public void test_to_chapeau() {
        //Given
        Contrat contrat = new Contrat();
        contrat.setChapeau(true);

        //When
        Boolean chapeau = contratTitulaireMapper.toChapeau(contrat);

        //Then
        assertTrue(chapeau);
    }

    @Test
    void test_mapping_decision_attribution() {
        //GIVEN
        var date = LocalDate.parse("2024-04-05");
        //WHEN
        var actualDate = contratTitulaireMapper.toDecisionAttribution(date);
        //THEN
        var expectedDate = "2024-04-05T00:00:00+01:00";
        assertThat(actualDate).isEqualTo(expectedDate);
    }

    @Test
    void test_mapping_nature() {
        //GIVEN
        var contrat = new Contrat();
        var typeContrat = new TypeContrat();
        typeContrat.setCode("mar");
        contrat.setType(typeContrat);
        //WHEN
        var result = contratTitulaireMapper.toNature(contrat, Map.of("nature-contrat", Map.of("mar", "Marché")));
        //WHEN
        assertThat(result).isEqualTo("Marché");
    }
}
