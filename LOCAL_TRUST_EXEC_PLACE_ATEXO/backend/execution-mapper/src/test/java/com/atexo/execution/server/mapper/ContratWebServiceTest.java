package com.atexo.execution.server.mapper;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.dto.ContratTitulaireFilter;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ConditionExecution;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ConditionsExecutions;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapperImpl;
import com.atexo.execution.server.common.mapper.contrat.titulaire.*;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.clause.ClauseN1;
import com.atexo.execution.server.model.clause.ClauseN2;
import com.atexo.execution.server.model.clause.ClauseN3;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ContratWebServiceTest {

    @InjectMocks
    private ContratWebServiceMapperImpl contratWebServiceMapper;

    @InjectMocks
    private ContratTitulaireMapperImpl contratToContratTitulaire;
    @Mock
    private ReferentielMapper referentielMapper;

    @Mock
    private ClauseMapper clauseMapper;
    @InjectMocks
    private ClauseMapperImpl clauseMapper1;

    @Mock
    private AcheteurMapperImpl acheteurMapper;
    @InjectMocks
    private TitulaireMapperImpl titulaireMapper;
    @Mock
    private LieuExecutionMapper lieuExecutionMapper;
    @Mock
    private ContratTitulaireConsultationMapperImpl contratTitulaireConsultationMapper;


    Map<Integer, List<String>> mapStatutContrat;
    Map<String, Map<String, String>> mappingPlaceOutput;

    @BeforeEach
    void beforeAll() {
        mapStatutContrat = Map.of(
                0, List.of("Contrat chapeau"),
                1, List.of("ANotifier"),
                4, List.of("EnCours", "Clos", "Archive"));

        mappingPlaceOutput = Map.of(
                "nature-prestation", Map.of("TRAVAUX", "/api/v2/referentiels/nature-prestations/1"),
                "type-contrat", Map.of("dsp", "4"),
                "type-prix", Map.of("TP_FERME", "1"),
                "statut-contrat", Map.of("chapeau", "0", "ANotifier", "3"),
                "nature-contrat", Map.of("TRAVAUX", "Marché")
        );
    }

    @Test
    void test_mapping_contratTitulaire_avec_un_seul_statut_contrat_egal_a_1() {
        //GIVEN

        var contratTitulaireFilter = new ContratTitulaireFilter();
        contratTitulaireFilter.setStatutContrat(1);

        //WHEN
        var contratEtablissementCriteria = contratWebServiceMapper.contratTitulaireToContratCriteria(contratTitulaireFilter, mapStatutContrat);

        //THEN
        assertEquals("ANotifier", contratEtablissementCriteria.getStatutContrats().get(0).name());
    }

    @Test
    void test_mapping_contratTitulaire_avec_un_seul_statut_contrat_egal_a_4() {
        //GIVEN
        var contratTitulaireFilter = new ContratTitulaireFilter();
        contratTitulaireFilter.setStatutContrat(4);

        //WHEN
        var contratEtablissementCriteria = contratWebServiceMapper.contratTitulaireToContratCriteria(contratTitulaireFilter, mapStatutContrat);

        //THEN
        assertEquals(List.of(StatutContrat.EnCours, StatutContrat.Clos, StatutContrat.Archive), contratEtablissementCriteria.getStatutContrats());
    }

    @Test
    void test_mapping_contratTitulaire_avec_une_liste_statut_contrat_egal_a_4() {
        //GIVEN
        var contratTitulaireFilter = new ContratTitulaireFilter();
        contratTitulaireFilter.setStatutContrats(List.of(4));

        //WHEN
        var contratEtablissementCriteria = contratWebServiceMapper.contratTitulaireToContratCriteria(contratTitulaireFilter, mapStatutContrat);

        //THEN
        assertEquals(List.of(StatutContrat.EnCours, StatutContrat.Clos, StatutContrat.Archive), contratEtablissementCriteria.getStatutContrats());
    }

    @Test
    void test_mapping_contratTitulaire_chapeau() {
        //GIVEN
        var contratTitulaireFilter = new ContratTitulaireFilter();
        contratTitulaireFilter.setStatutContrat(0);
        contratTitulaireFilter.setStatutContrats(List.of(0, 4));

        //WHEN
        var contratEtablissementCriteria = contratWebServiceMapper.contratTitulaireToContratCriteria(contratTitulaireFilter, mapStatutContrat);

        //THEN
        assertTrue(contratEtablissementCriteria.getChapeau());
        assertEquals(List.of(StatutContrat.EnCours, StatutContrat.Clos, StatutContrat.Archive), contratEtablissementCriteria.getStatutContrats());
    }

    @Test
    void test_mapping_nature_prestation() {
        //GIVEN
        var contrat = new Contrat();
        var categorie = new CategorieConsultation();
        categorie.setCode("TRAVAUX");
        contrat.setId(1L);
        contrat.setCategorie(categorie);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals("/api/v2/referentiels/nature-prestations/1", contratTitulaire.getNaturePrestation());
    }

    @Test
    void test_mapping_type_contrat() {
        //GIVEN
        var contrat = new Contrat();
        var type = new TypeContrat();
        type.setCode("dsp");
        contrat.setId(1L);
        contrat.setType(type);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(4, contratTitulaire.getTypeContrat());
    }

    @Test
    void test_mapping_forme_prix() {
        //GIVEN
        var contrat = new Contrat();
        var consultation = new Consultation();
        var type = new TypePrix();
        type.setCode("TP_FERME");
        consultation.setTypesPrix(Set.of(type));
        contrat.setId(1L);
        contrat.setConsultation(consultation);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(1, contratTitulaire.getFormePrix());
    }

    @Test
    void test_mapping_statut_contrat_chapeau() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setChapeau(true);
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setId(1L);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals("0", contratTitulaire.getStatut());
    }

    @Test
    void test_mapping_statut_contrat_a_notifier() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setId(1L);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals("3", contratTitulaire.getStatut());
    }

    @Test
    void test_mapping_id_chapeauAcSad_avec_type_null() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setChapeau(true);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(null, contratTitulaire.getIdChapeauAcSad());
    }

    @Test
    void test_mapping_id_chapeauAcSad_avec_type_non_null() {
        //GIVEN
        var contrat = new Contrat();
        var type = new TypeContrat();
        type.setCode("sad");
        contrat.setId(1L);
        contrat.setType(type);
        contrat.setChapeau(true);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(1, contratTitulaire.getIdChapeauAcSad());
    }

    @Test
    void test_mapping_id_chapeauMultiAttributaire_avec_type_non_null() {
        //GIVEN
        var contrat = new Contrat();
        var type = new TypeContrat();
        type.setCode("amu");
        contrat.setId(1L);
        contrat.setIdExterne("111");
        contrat.setType(type);
        contrat.setChapeau(true);

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(111, contratTitulaire.getIdChapeauMultiAttributaire());
    }

    @Test
    void test_mapping_date_modification() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setDateModificationExterne(LocalDateTime.parse("2024-01-01T00:00:00"));
        contrat.setDatePrevisionnelleNotification(LocalDate.parse("2024-01-01"));
        var acte1 = new ActeModificatif();
        acte1.setIdExterne(UUID.randomUUID().toString());
        acte1.setDateNotification(LocalDateTime.parse("2024-01-10T00:00:00"));
        var acte2 = new ActeModificatif();
        acte2.setIdExterne(UUID.randomUUID().toString());
        acte2.setDateNotification(LocalDateTime.parse("2024-01-05T00:00:00"));
        contrat.setActes(Set.of(acte1, acte2));

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals("2024-01-10T00:00:00+01:00", contratTitulaire.getDateModification());
    }

    @Test
    void test_mapping_type_identifiant() {
        //GIVEN
        var contrat = new Contrat();
        var contratEtablissement = new ContratEtablissement();
        contrat.setId(1L);
        var etablissement = new Etablissement();
        etablissement.setSiret("123456789");
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setEtablissement(etablissement);

        //WHEN
        var titulaure = titulaireMapper.toTitulaire(contratEtablissement);

        //THEN
        assertEquals("SIRET", titulaure.getTypeIdentifiantEntreprise());
    }

    @Test
    void test_mapping_type_identifiant_hors_ue() {
        //GIVEN
        var contrat = new Contrat();
        var contratEtablissement = new ContratEtablissement();
        contrat.setId(1L);
        var etablissement = new Etablissement();
        etablissement.setSiret(null);
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setEtablissement(etablissement);

        //WHEN
        var titulaure = titulaireMapper.toTitulaire(contratEtablissement);

        //THEN
        assertEquals("Hors UE", titulaure.getTypeIdentifiantEntreprise());
    }

    @Test
    void test_mapping_id_contrat_avec_id_externe_uuid() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne(UUID.randomUUID().toString());

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(1, contratTitulaire.getId());
    }

    @Test
    void test_mapping_id_contrat_avec_id_externe_non_uuid() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("123");

        //WHEN
        var contratTitulaire = contratToContratTitulaire.contratToContratTitulaire(contrat, mappingPlaceOutput);

        //THEN
        assertEquals(123, contratTitulaire.getId());
    }


    @Test
    void test_mapping_clause() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("123");

        var clauseN1 = new ClauseN1();
        clauseN1.setCode("clausesSociales");
        var clauseN2 = new ClauseN2();
        clauseN2.setCode("conditionExecution");
        clauseN2.setActif(false);
        var clauseN3 = new ClauseN3();
        clauseN3.setCode("clauseSocialeAutre");
        clauseN3.setActif(false);

        var clauseN31 = new ClauseN3();
        clauseN31.setCode("lutteContreDiscriminations");
        clauseN31.setActif(false);

        ContratClause contratClause1 = new ContratClause();
        contratClause1.setClause(clauseN1);
        contratClause1.setContrat(contrat);
        ContratClause contratClause2 = new ContratClause();
        contratClause2.setClause(clauseN2);
        contratClause2.setContrat(contrat);
        ContratClause contratClause3 = new ContratClause();
        contratClause3.setClause(clauseN3);
        contratClause3.setContrat(contrat);
        contratClause3.setParent(contratClause2);
        ContratClause contratClause31 = new ContratClause();
        contratClause31.setClause(clauseN31);
        contratClause31.setContrat(contrat);
        contratClause31.setParent(contratClause2);

        contrat.setContratClauses(Set.of(contratClause1, contratClause2, contratClause3, contratClause31));

        //WHEN
        var clauses = clauseMapper1.toClausesSociales(contrat);

        //THEN
        assertTrue(clauses.getConditionExecution().getClauseSocialeAutre());
        assertTrue(clauses.getConditionExecution().getLutteContreDiscriminations());
    }

    @Test
    void test_mapping_clause_N2_verificationNonRespect() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("123");

        // ClauseN1
        var clauseN1 = new ClauseN1();
        clauseN1.setCode("clausesSociales");
        var clauseN11 = new ClauseN1();
        clauseN11.setCode("clauseEnvironnementale");

        //CLAUSEN2
        var clauseN2 = new ClauseN2();
        clauseN2.setCode("verificationNonRespect");
        clauseN2.setActif(true);
        var clauseN21 = new ClauseN2();
        clauseN21.setCode("verificationNonRespect");
        clauseN21.setActif(true);


        ContratClause contratClause1 = new ContratClause();
        contratClause1.setClause(clauseN1);
        contratClause1.setContrat(contrat);


        ContratClause contratClause2 = new ContratClause();
        contratClause2.setClause(clauseN11);
        contratClause2.setContrat(contrat);

        ContratClause contratClause3 = new ContratClause();
        contratClause3.setClause(clauseN2);
        contratClause3.setContrat(contrat);
        contratClause3.setParent(contratClause1);

        ContratClause contratClause4 = new ContratClause();
        contratClause4.setClause(clauseN21);
        contratClause4.setContrat(contrat);
        contratClause4.setParent(contratClause2);

        contrat.setContratClauses(Set.of(contratClause1, contratClause2, contratClause3, contratClause4));

        //WHEN
        var clauses1 = clauseMapper1.toClausesSocialesV2(contrat);
        var clauses2 = clauseMapper1.toClausesEnvironnementalesV2(contrat);

        //THEN
        assertTrue(clauses1.getVerificationNonRespect());
        assertTrue(clauses2.getVerificationNonRespect());
    }

    @Test
    void test_mapping_clause_N2_avec_un_seul_verificationNonRespect() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("123");

        // ClauseN1
        var clauseN1 = new ClauseN1();
        clauseN1.setCode("clausesSociales");
        var clauseN11 = new ClauseN1();
        clauseN11.setCode("clausesEnvironnementales");

        //CLAUSEN2
        var clauseN2 = new ClauseN2();
        clauseN2.setCode("verificationNonRespect");
        clauseN2.setActif(true);


        ContratClause contratClause1 = new ContratClause();
        contratClause1.setClause(clauseN1);
        contratClause1.setContrat(contrat);


        ContratClause contratClause2 = new ContratClause();
        contratClause2.setClause(clauseN11);
        contratClause2.setContrat(contrat);

        ContratClause contratClause3 = new ContratClause();
        contratClause3.setClause(clauseN2);
        contratClause3.setContrat(contrat);
        contratClause3.setParent(contratClause1);


        contrat.setContratClauses(Set.of(contratClause1, contratClause2, contratClause3));

        //WHEN
        var clauses1 = clauseMapper1.toClausesSocialesV2(contrat);
        var clauses2 = clauseMapper1.toClausesEnvironnementalesV2(contrat);

        //THEN
        assertTrue(clauses1.getVerificationNonRespect());
        assertFalse(clauses2.getVerificationNonRespect());
    }


    @Test
    void test_mapping_clause_v2() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("123");

        // ClauseN1
        var clausesSocialesV2 = new ClauseN1();
        clausesSocialesV2.setCode("clausesSociales");
        var clausesEnvironnementalesV2 = new ClauseN1();
        clausesEnvironnementalesV2.setCode("clausesEnvironnementales");

        //CLAUSEN2
        var conditionExecution = new ClauseN2();
        conditionExecution.setCode("conditionExecution");
        conditionExecution.setActif(true);

        var conditionsExecutions = new ClauseN2();
        conditionsExecutions.setCode("conditionsExecutions");
        conditionsExecutions.setActif(true);


        //ClauseN3
        var commerceEquitable = new ClauseN3();
        commerceEquitable.setCode("commerceEquitable");
        commerceEquitable.setActif(true);
        commerceEquitable.setParents(Set.of(conditionExecution));

        var clauseEnvironnementaleAutre = new ClauseN3();
        clauseEnvironnementaleAutre.setCode("clauseEnvironnementaleAutre");
        clauseEnvironnementaleAutre.setActif(true);
        clauseEnvironnementaleAutre.setParents(Set.of(conditionsExecutions));


        ContratClause contratClause1 = new ContratClause();
        contratClause1.setClause(clausesSocialesV2);
        contratClause1.setContrat(contrat);

        ContratClause contratClause2 = new ContratClause();
        contratClause2.setClause(clausesEnvironnementalesV2);
        contratClause2.setContrat(contrat);

        ContratClause contratClause3 = new ContratClause();
        contratClause3.setClause(conditionExecution);
        contratClause3.setContrat(contrat);
        contratClause3.setParent(contratClause1);

        ContratClause contratClause4 = new ContratClause();
        contratClause4.setClause(conditionsExecutions);
        contratClause4.setContrat(contrat);
        contratClause4.setParent(contratClause2);

        ContratClause contratClause5 = new ContratClause();
        contratClause5.setClause(commerceEquitable);
        contratClause5.setContrat(contrat);
        contratClause5.setParent(contratClause3);

        ContratClause contratClause6 = new ContratClause();
        contratClause6.setClause(clauseEnvironnementaleAutre);
        contratClause6.setContrat(contrat);
        contratClause6.setParent(contratClause4);


        contrat.setContratClauses(Set.of(contratClause1, contratClause2, contratClause3, contratClause4, contratClause5, contratClause6));

        //WHEN
        var clausesSocialesV2Result = clauseMapper1.toClausesSocialesV2(contrat);
        var clausesEnvironnementalesV2Result = clauseMapper1.toClausesEnvironnementalesV2(contrat);

        //THEN
        var conditionExecutionResult = new ConditionExecution();
        conditionExecutionResult.setCommerceEquitable(true);

        var conditionsExecutionsResult = new ConditionsExecutions();
        conditionsExecutionsResult.setClauseEnvironnementaleAutre(true);

        assertThat(clausesSocialesV2Result.getConditionExecution()).usingRecursiveComparison().isEqualTo(conditionExecutionResult);
        assertThat(clausesEnvironnementalesV2Result.getConditionsExecutions()).usingRecursiveComparison().isEqualTo(conditionsExecutionsResult);
    }


}
