package com.atexo.execution.server.mapper.utils;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class MapperUtilsTest {

    @Test
    void extractId() {
        assertEquals(1, MapperUtils.toInteger("pa_p2_e3r_1"));
        assertNull(MapperUtils.toInteger("e3r-1"));
        assertEquals(214, MapperUtils.toInteger("214"));
        assertNull(MapperUtils.toInteger(UUID.randomUUID().toString()));
        assertEquals(39317, MapperUtils.toInteger("pmi-min-1_39317"));
        assertNull(MapperUtils.toInteger(null));
    }
}
