package com.atexo.execution.server.common;

import com.atexo.execution.common.data.gouv.model.marche.*;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvConcessionMapper;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvConcessionMapperImpl;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.actes.DonneesExecution;
import com.atexo.execution.server.model.clause.ClauseN2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DataGouvConcessionMapperTest {

    private DataGouvConcessionMapper dataGouvConcessionMapper;
    private Map<String, Map<String, String>> mappingDataGouv;

    @BeforeEach
    public void setUp() {
        dataGouvConcessionMapper = new DataGouvConcessionMapperImpl();
        mappingDataGouv = Map.of(
                "type-procedure", new HashMap<>(),
                "nature-contrat", new HashMap<>(),
                "ccag-contrat", new HashMap<>(),
                "type-prix-contrat", new HashMap<>(),
                "variation-prix-contrat", new HashMap<>(),
                "technique-achat", new HashMap<>(),
                "modalite-execution", new HashMap<>(),
                "consideration-sociale", new HashMap<>(),
                "consideration-environnementale", new HashMap<>(),
                " type-groupement", new HashMap<>()
        );
    }

    @Test
    public void test_mapping_procedure_concession() {
        //GIVEN
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        Procedure procedure = new Procedure();
        procedure.setCode("PNNR");
        consultation.setProcedure(procedure);
        contrat.setConsultation(consultation);

        //WHEN
        ContratConcession.Procedure actualProcedure = dataGouvConcessionMapper.toTypeProcedure(contrat, Map.of("PNNR", "PROCÉDURE_NON_NÉGOCIÉE_RESTREINTE"));

        //THEN
        assertThat(actualProcedure).isEqualTo(ContratConcession.Procedure.PROCÉDURE_NON_NÉGOCIÉE_RESTREINTE);
    }

    @Test
    void test_mapping_donnees_execution() {
        //GIVEN
        Contrat contrat = new Contrat();
        DonneesExecution donneesExecution = new DonneesExecution();
        donneesExecution.setIntituleTarif("tarif1");
        donneesExecution.setTarif(100.0);
        donneesExecution.setDepensesInvestissement(50.0);
        donneesExecution.setDatePublication(LocalDateTime.parse("2024-03-27T00:00:00"));
        donneesExecution.setStatut(StatutActe.NOTIFIE);
        donneesExecution.setPublicationDonneesEssentielles(true);
        contrat.setActes(Set.of(donneesExecution));

        //WHEN
        Set<DonneesExecutionArray> donneesExecutionArrays = dataGouvConcessionMapper.toDonneesExecutionList(contrat);

        //THEN
        var donneesExecutionArray = new DonneesExecutionArray();
        var donneesExecution1 = new com.atexo.execution.common.data.gouv.model.marche.DonneesExecution();
        donneesExecution1.setDatePublicationDonneesExecution("2024-03-27");
        donneesExecution1.setDepensesInvestissement(50.0);
        var tarifArray = new TarifArray();
        var tarif = new Tarif();
        tarif.setIntituleTarif("tarif1");
        tarif.setTarif(100.0);
        tarifArray.setTarif(tarif);
        donneesExecution1.setTarifs(List.of(tarifArray));
        donneesExecutionArray.setDonneesAnnuelles(donneesExecution1);
        var donneesExecutionArrayExpected = Set.of(donneesExecutionArray);
        assertThat(donneesExecutionArrays).usingRecursiveComparison().isEqualTo(donneesExecutionArrayExpected);

    }

    @Test
    void test_mapping_concessionnaire() {
        //GIVEN
        var contrat = new Contrat();
        var etablissement = new Etablissement();
        etablissement.setSiret("111111111");
        var contratEtablissement = new ContratEtablissement();
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setEtablissement(etablissement);
        contrat.setContratEtablissements(Set.of(contratEtablissement));

        //WHEN
        Set<ConcessionnaireArray> concessionnaireArrays = dataGouvConcessionMapper.toConcessionnaires(contrat);

        //THEN
        var concessionnaireArrayExpected = new ConcessionnaireArray();
        var concessionnaireExpected = new Concessionnaire();
        concessionnaireExpected.setId("111111111");
        concessionnaireExpected.setTypeIdentifiant(Concessionnaire.TypeIdentifiant.SIRET);
        concessionnaireArrayExpected.setConcessionnaire(concessionnaireExpected);
        var concessionnaireArrayExpectedList = Set.of(concessionnaireArrayExpected);
        assertThat(concessionnaireArrays).usingRecursiveComparison().isEqualTo(concessionnaireArrayExpectedList);
    }

    @Test
    void test_mapping_contrat_sans_consideration_sociale() {
        //GIVEN
        Contrat contrat = new Contrat();
        contrat.setContratClauses(null);
        //WHEN
        ConsiderationsSociales__1 considerationsSociales = dataGouvConcessionMapper.toConsiderationsSociales(contrat, Map.of("conditionExecution", "CLAUSE_SOCIALE", "critereAttribution", "CRITÈRE_SOCIAL"));

        //THEN
        var considerationsSocialesExpected = new ConsiderationsSociales__1();
        considerationsSocialesExpected.setConsiderationSociale(Set.of(ConsiderationSocialeArray_.PAS_DE_CONSIDÉRATION_SOCIALE));
        assertThat(considerationsSociales).usingRecursiveComparison().isEqualTo(considerationsSocialesExpected);
    }

    @Test
    void test_to_modification() {
        //Given
        Contrat contrat = new Contrat();
        contrat.setDureeMaximaleMarche(10);
        contrat.setDateNotification(LocalDate.parse("2023-01-01"));
        DonneesEssentielles donneesEssentielles = new DonneesEssentielles();
        donneesEssentielles.setDatePublication(LocalDateTime.parse("2023-01-01T00:00:00"));
        contrat.setDonneesEssentielles(donneesEssentielles);
        ContratEtablissement contratEtablissement = new ContratEtablissement();
        Etablissement etablissement = new Etablissement();
        etablissement.setSiret("11111111111111");
        contratEtablissement.setEtablissement(etablissement);
        contrat.setContratEtablissements(Set.of(contratEtablissement));
        ActeModificatif acteModificatif = new ActeModificatif();
        acteModificatif.setId(1L);
        acteModificatif.setContrat(contrat);
        acteModificatif.setMontantHTChiffre(100.5);
        acteModificatif.setStatut(StatutActe.NOTIFIE);
        acteModificatif.setPublicationDonneesEssentielles(true);
        acteModificatif.setDateNotification(LocalDateTime.parse("2024-03-27T00:00:00"));
        contrat.setActes(Set.of(acteModificatif));

        //When
        Set<ModificationArray__1> modifications = dataGouvConcessionMapper.toModifications(contrat);

        //Then
        var modificationArrayExpeted = new ModificationArray__1();
        var modificationExpected = new Modification__1();
        modificationExpected.setId(1);
        modificationExpected.setDateSignatureModification("2024-03-27");
        modificationExpected.setDatePublicationDonneesModification("2023-01-01");
        modificationExpected.setDureeMois(10);
        modificationExpected.setValeurGlobale(100.5);
        modificationArrayExpeted.setModification(modificationExpected);
        assertThat(modifications).usingRecursiveComparison().isEqualTo(Set.of(modificationArrayExpeted));
    }

    @Test
    void test_mapping_consideration_sociale() {
        //GIVEN
        Contrat contrat = new Contrat();
        ClauseN2 conditionExecution = new ClauseN2();
        conditionExecution.setCode("conditionExecution");
        ClauseN2 critereAttribution = new ClauseN2();
        critereAttribution.setCode("critereAttribution");
        ContratClause contratClause1 = new ContratClause();
        contratClause1.setContrat(contrat);
        contratClause1.setClause(conditionExecution);
        ContratClause contratClause2 = new ContratClause();
        contratClause2.setContrat(contrat);
        contratClause2.setClause(critereAttribution);
        contrat.setContratClauses(Set.of(contratClause1, contratClause2));

        //WHEN
        ConsiderationsSociales__1 considerationsSociales = dataGouvConcessionMapper.toConsiderationsSociales(contrat, Map.of("conditionExecution", "CLAUSE_SOCIALE", "critereAttribution", "CRITÈRE_SOCIAL"));
        ConsiderationsEnvironnementales__1 considerationsEnvironnementales = dataGouvConcessionMapper.toConsiderationsEnvironnementales(contrat, Map.of("specificationsTechniques", "CLAUSE_ENVIRONNEMENTALE"));
        //THEN
        ConsiderationsSociales__1 considerationsSocialesExpected = new ConsiderationsSociales__1();
        considerationsSocialesExpected.setConsiderationSociale(Set.of(ConsiderationSocialeArray_.CLAUSE_SOCIALE, ConsiderationSocialeArray_.CRITÈRE_SOCIAL));
        ConsiderationsEnvironnementales__1 considerationsEnvironnementalesExpected = new ConsiderationsEnvironnementales__1();
        considerationsEnvironnementalesExpected.setConsiderationEnvironnementale(Set.of(ConsiderationEnvironnementaleArray_.PAS_DE_CONSIDÉRATION_ENVIRONNEMENTALE));
        assertThat(considerationsSociales).usingRecursiveComparison().isEqualTo(considerationsSocialesExpected);
        assertThat(considerationsEnvironnementales).usingRecursiveComparison().isEqualTo(considerationsEnvironnementalesExpected);
    }
}
