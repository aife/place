package com.atexo.execution.server.mapper;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.concession.TypeConcession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class EchangeChorusMapperTest {

    @InjectMocks
    private EchangeChorusMapperImpl echangeChorusMapper;

    @Mock
    ReferentielMapper referentielMapper;

    @Mock
    AgentMapper agentMapper;

    @Mock
    ActeMapperV2 acteMapperV2;

    @Mock
    ContratMapper contratMapper;

    @Mock
    EtablissementMapperV2 etablissementMapperV2;



    @Test
    void given_echangeChorus_assert_echangeChorusType_ok() {
        EchangeChorus echangeChorus = new EchangeChorus();
        Acte acte = new ActeModificatif();
        Contrat contrat = new Contrat();
        contrat.setId(1L);
        contrat.setIdExterne("72");
        acte.setContrat(contrat);
        echangeChorus.setContrat(contrat);
        echangeChorus.setIdExterne("66");
        acte.setId(1L);
        acte.setIdExterne("22");
        StatutChorus statutChorus = new StatutChorus();
        statutChorus.setCode("1");
        statutChorus.setLabel("Envoyé");
        echangeChorus.setStatut(statutChorus);
        RetourChorus retourChorus = new RetourChorus();
        retourChorus.setCode("COM");
        retourChorus.setLabel("Intégré");
        echangeChorus.setRetourChorus(retourChorus);
        echangeChorus.setErreurPublication("erreurTest");
        echangeChorus.setId(2L);
        EchangeChorusType result = echangeChorusMapper.toWS(acte, echangeChorus);
        assertEquals("Envoyé", result.getStatutEchange().getLibelle());
        assertEquals("1", result.getStatutEchange().getCode());
        assertEquals("Intégré", result.getRetourChorus().getLibelle());
        assertEquals("COM", result.getRetourChorus().getCode());
        assertEquals("erreurTest", result.getMessageErreur());
        assertEquals("72", result.getActe().getIdExterne());


    }


}
