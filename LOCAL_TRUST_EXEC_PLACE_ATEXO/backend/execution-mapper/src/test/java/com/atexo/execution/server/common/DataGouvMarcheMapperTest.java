package com.atexo.execution.server.common;

import com.atexo.execution.common.data.gouv.model.marche.*;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvMarcheMapper;
import com.atexo.execution.server.common.mapper.data.gouv.DataGouvMarcheMapperImpl;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.ModaliteExecution;
import com.atexo.execution.server.model.TypePrix;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.clause.ClauseN2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DataGouvMarcheMapperTest {

    private DataGouvMarcheMapper dataGouvMarcheMapper;
    private Map<String, Map<String, String>> mappingDataGouv;

    @BeforeEach
    public void setUp() {
        dataGouvMarcheMapper = new DataGouvMarcheMapperImpl();
        mappingDataGouv = Map.of(
                "type-procedure", new HashMap<>(),
                "nature-contrat", new HashMap<>(),
                "ccag-contrat", new HashMap<>(),
                "type-prix-contrat", new HashMap<>(),
                "variation-prix-contrat", new HashMap<>(),
                "technique-achat", new HashMap<>(),
                "modalite-execution", new HashMap<>(),
                "consideration-sociale", new HashMap<>(),
                "consideration-environnementale", new HashMap<>(),
                "type-groupement", new HashMap<>()
        );
    }

    @Test
    public void test_to_acte_sous_traitance() {
        //GIVEN
        Contrat contrat = new Contrat();
        contrat.setDureeMaximaleMarche(10);
        contrat.setDateNotification(LocalDate.parse("2023-01-01"));
        contrat.setDonneesEssentielles(new DonneesEssentielles());
        contrat.getDonneesEssentielles().setDatePublication(LocalDateTime.parse("2023-01-01T00:00:00"));
        RevisionPrix revisionPrix = new RevisionPrix();
        revisionPrix.setCode("REVISABLE");
        contrat.setRevisionPrix(revisionPrix);
        var sousTraitant = new ContratEtablissement();
        sousTraitant.setEtablissement(new Etablissement());
        sousTraitant.setId(1L);
        sousTraitant.setContrat(contrat);
        sousTraitant.setDateNotification(LocalDate.parse("2023-01-01"));
        sousTraitant.setMontant(new BigDecimal(100.5));
        sousTraitant.getEtablissement().setSiret("11111111111111");
        sousTraitant.setRevisionPrix(revisionPrix);
        sousTraitant.setDureeMois(10);

        contrat.setContratEtablissements(Set.of(sousTraitant));

        //WHEN
        Set<ActeSousTraitanceArray> actesSousTraitance = dataGouvMarcheMapper.toActesSousTraitance(contrat, Map.of("REVISABLE", "RÉVISABLE"));

        //THEN
        ActeSousTraitanceArray acteSousTraitanceArray = new ActeSousTraitanceArray();
        ActeSousTraitance acteSousTraitanceExpected = new ActeSousTraitance();
        acteSousTraitanceExpected.setId(1);
        acteSousTraitanceExpected.setDureeMois(10);
        acteSousTraitanceExpected.setDateNotification("2023-01-01");
        acteSousTraitanceExpected.setDatePublicationDonnees("2023-01-01");
        acteSousTraitanceExpected.setMontant(100.5);
        acteSousTraitanceExpected.setVariationPrix(ActeSousTraitance.VariationPrix.RÉVISABLE);
        com.atexo.execution.common.data.gouv.model.marche.SousTraitant sousTraitant1 = new com.atexo.execution.common.data.gouv.model.marche.SousTraitant();
        sousTraitant1.setId("11111111111111");
        sousTraitant1.setTypeIdentifiant(com.atexo.execution.common.data.gouv.model.marche.SousTraitant.TypeIdentifiant.SIRET);
        acteSousTraitanceExpected.setSousTraitant(sousTraitant1);
        acteSousTraitanceArray.setActeSousTraitance(acteSousTraitanceExpected);
        assertThat(actesSousTraitance).usingRecursiveComparison().isEqualTo(Set.of(acteSousTraitanceArray));
    }


    @Test
    void test_to_modification() {
        //Given
        Contrat contrat = new Contrat();
        contrat.setDureeMaximaleMarche(10);
        contrat.setDateNotification(LocalDate.parse("2023-01-01"));
        DonneesEssentielles donneesEssentielles = new DonneesEssentielles();
        donneesEssentielles.setDatePublication(LocalDateTime.parse("2023-01-01T00:00:00"));
        contrat.setDonneesEssentielles(donneesEssentielles);
        ContratEtablissement contratEtablissement = new ContratEtablissement();
        Etablissement etablissement = new Etablissement();
        etablissement.setSiret("11111111111111");
        contratEtablissement.setEtablissement(etablissement);
        contrat.setContratEtablissements(Set.of(contratEtablissement));
        ActeModificatif acteModificatif = new ActeModificatif();
        acteModificatif.setId(1L);
        acteModificatif.setContrat(contrat);
        acteModificatif.setMontantHTChiffre(100.5);
        acteModificatif.setStatut(StatutActe.NOTIFIE);
        acteModificatif.setPublicationDonneesEssentielles(true);
        acteModificatif.setDateNotification(LocalDateTime.parse("2023-01-01T00:00:00"));
        contrat.setActes(Set.of(acteModificatif));

        //When
        Set<ModificationArray> modifications = dataGouvMarcheMapper.toModifications(contrat);

        //Then
        ModificationArray modificationArrayExpeted = new ModificationArray();
        Modification modificationExpected = new Modification();
        modificationExpected.setId(1);
        modificationExpected.setDateNotificationModification("2023-01-01");
        modificationExpected.setDatePublicationDonneesModification("2023-01-01");
        modificationExpected.setDureeMois(10);
        modificationExpected.setMontant(100.5);
        TitulaireArray titulaireArray = new TitulaireArray();
        Titulaire titulaire = new Titulaire();
        titulaire.setId("11111111111111");
        titulaire.setTypeIdentifiant(Titulaire.TypeIdentifiant.SIRET);
        titulaireArray.setTitulaire(titulaire);
        modificationExpected.setTitulaires(Set.of(titulaireArray));
        modificationArrayExpeted.setModification(modificationExpected);
        assertThat(modifications).usingRecursiveComparison().isEqualTo(Set.of(modificationArrayExpeted));
    }

    @Test
    void test_to_technique() {
        //Given
        TechniqueAchat techniqueAchat = new TechniqueAchat();
        techniqueAchat.setCode("TA_AC");
        TechniqueAchat techniqueAchat1 = new TechniqueAchat();
        techniqueAchat1.setCode("TA_SAD");
        TechniqueAchat techniqueAchat2 = new TechniqueAchat();
        techniqueAchat2.setCode("TA_SO");
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        consultation.setTechniqueAchats(Set.of(techniqueAchat, techniqueAchat1, techniqueAchat2));
        contrat.setConsultation(consultation);

        //When
        Techniques techniques = dataGouvMarcheMapper.toTechniques(contrat, Map.of("TA_AC", "ACCORD_CADRE", "TA_SAD", "SYSTÈME_D_ACQUISITION_DYNAMIQUE", "TA_SO", "SANS_OBJET"));

        //Then
        Techniques techniquesExpected = new Techniques();
        Set<TechniqueArray> techniqueArray = Set.of(TechniqueArray.ACCORD_CADRE, TechniqueArray.SYSTÈME_D_ACQUISITION_DYNAMIQUE, TechniqueArray.SANS_OBJET);
        techniquesExpected.setTechnique(techniqueArray);

        assertThat(techniques).usingRecursiveComparison().isEqualTo(techniquesExpected);
    }

    @Test
    void test_to_percent() {
        //When
        Double origineUE = dataGouvMarcheMapper.toPercent(20);

        //Then
        assertThat(origineUE).isEqualTo(0.2);
    }

    @Test
    void test_modalite_execution() {
        //GIVEN
        ModaliteExecution modaliteExecution = new ModaliteExecution();
        modaliteExecution.setCode("ME_TRANCHES");
        ModaliteExecution modaliteExecution1 = new ModaliteExecution();
        modaliteExecution1.setCode("ME_SO");
        Consultation consultation = new Consultation();
        consultation.setModaliteExecutions(Set.of(modaliteExecution, modaliteExecution1));
        Contrat contrat = new Contrat();
        contrat.setConsultation(consultation);

        //WHEN
        ModalitesExecution modalitesExecution = dataGouvMarcheMapper.toModalitesExecution(contrat, Map.of("ME_TRANCHES", "TRANCHES", "ME_SO", "SANS_OBJET"));

        //THEN
        ModalitesExecution modalitesExecutionExpected = new ModalitesExecution();
        modalitesExecutionExpected.setModaliteExecution(Set.of(ModaliteExecutionArray.TRANCHES, ModaliteExecutionArray.SANS_OBJET));
        assertThat(modalitesExecution).usingRecursiveComparison().isEqualTo(modalitesExecutionExpected);
    }

    @Test
    void test_type_groupement_non_null() {
        //GIVEN
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        TypeGroupement typeGroupement = new TypeGroupement();
        typeGroupement.setCode("CONJOINT");
        consultation.setTypeGroupementOperateurs(typeGroupement);
        contrat.setConsultation(consultation);

        //WHEN
        Marche.TypeGroupementOperateurs typeGroupementOperateurs = dataGouvMarcheMapper.toTypeGroupementOperateurs(contrat, Map.of("CONJOINT", "CONJOINT"));

        //THEN
        assertThat(typeGroupementOperateurs).isEqualTo(Marche.TypeGroupementOperateurs.CONJOINT);
    }

    @Test
    void test_type_groupement_null() {
        //GIVEN
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        contrat.setConsultation(consultation);

        //WHEN
        Marche.TypeGroupementOperateurs typeGroupementOperateurs = dataGouvMarcheMapper.toTypeGroupementOperateurs(contrat, Map.of("CONJOINT", "CONJOINT"));

        //THEN
        assertThat(typeGroupementOperateurs).isEqualTo(Marche.TypeGroupementOperateurs.PAS_DE_GROUPEMENT);
    }

    @Test
    void test_type_de_prix() {
        //GIVEN
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        TypePrix typePrix = new TypePrix();
        TypePrix typePrix1 = new TypePrix();
        typePrix.setCode("TP_FERME");
        typePrix1.setCode("TP_PROVISOIRE");
        consultation.setTypesPrix(Set.of(typePrix, typePrix1));
        contrat.setConsultation(consultation);

        //WHEN
        TypesPrix typesPrix = dataGouvMarcheMapper.toTypesPrix(contrat, Map.of("TP_FERME", "DÉFINITIF_FERME",
                "TP_PROVISOIRE", "PROVISOIRE"));

        //THEN
        TypesPrix typesPrixExpected = new TypesPrix();
        typesPrixExpected.setTypePrix(Set.of(TypePrixArray.DÉFINITIF_FERME, TypePrixArray.PROVISOIRE));
        assertThat(typesPrix).isEqualTo(typesPrixExpected);
    }

    @Test
    void test_forme_prix() {
        //GIVEN
        Contrat contrat = new Contrat();
        FormePrix formePrix = new FormePrix();
        formePrix.setCode("UNITAIRE");
        contrat.setTypeFormePrix(formePrix);

        //WHEN
        Marche.FormePrix formePrixActual = dataGouvMarcheMapper.toFormePrix(contrat);

        //THEN
        assertThat(formePrixActual).isEqualTo(Marche.FormePrix.UNITAIRE);
    }

    @Test
    void test_forme_prix_null() {
        //GIVEN
        Contrat contrat = new Contrat();

        //WHEN
        Marche.FormePrix formePrixActual = dataGouvMarcheMapper.toFormePrix(contrat);

        //THEN
        assertThat(formePrixActual).isEqualTo(null);
    }

    @Test
    void test_origine_ue() {
        //GIVEN
        Contrat contrat = new Contrat();
        Consultation consultation = new Consultation();
        consultation.setOrigineUE(1000);
        contrat.setConsultation(consultation);

        //WHEN
        Marche marche = dataGouvMarcheMapper.contratToDataGouvMarche(contrat, mappingDataGouv, "01");

        //THEN
        assertThat(marche.getOrigineUE()).isEqualTo(10.0);
    }

    @Test
    void test_mapping_titulaire() {
        //GIVEN
        Contrat contrat = new Contrat();
        ContratEtablissement contratEtablissement = new ContratEtablissement();
        ContratEtablissement contratEtablissement1 = new ContratEtablissement();
        Etablissement etablissement = new Etablissement();
        Etablissement etablissement1 = new Etablissement();
        etablissement.setSiret("111111111");
        etablissement1.setSiret("222222222");
        contratEtablissement.setId(1L);
        contratEtablissement.setIdExterne("1");
        contratEtablissement.setDateModificationExterne(LocalDateTime.parse("2024-01-01T00:00:00"));
        contratEtablissement.setPlateforme(new Plateforme());
        contratEtablissement.setEtablissement(etablissement);
        contratEtablissement1.setId(2L);
        contratEtablissement1.setIdExterne("2");
        contratEtablissement1.setDateModificationExterne(LocalDateTime.parse("2024-01-02T00:00:00"));
        contratEtablissement1.setPlateforme(new Plateforme());
        contratEtablissement1.setEtablissement(etablissement1);
        contrat.setContratEtablissements(Set.of(contratEtablissement, contratEtablissement1));

        //WHEN
        Marche marche = dataGouvMarcheMapper.contratToDataGouvMarche(contrat, mappingDataGouv, "01");

        //THEN
        TitulaireArray titulaireArray = new TitulaireArray();
        Titulaire titulaire = new Titulaire();
        titulaire.setId("111111111");
        titulaire.setTypeIdentifiant(Titulaire.TypeIdentifiant.SIRET);
        titulaireArray.setTitulaire(titulaire);
        TitulaireArray titulaireArray1 = new TitulaireArray();
        Titulaire titulaire1 = new Titulaire();
        titulaire1.setId("222222222");
        titulaire1.setTypeIdentifiant(Titulaire.TypeIdentifiant.SIRET);
        titulaireArray1.setTitulaire(titulaire1);
        Set<TitulaireArray> titulairesExpected = Set.of(titulaireArray, titulaireArray1);
        assertThat(marche.getTitulaires()).usingRecursiveComparison().isEqualTo(titulairesExpected);
    }

    @Test
    void test_mapping_consideration_sociale() {
        //GIVEN
        Contrat contrat = new Contrat();
        ClauseN2 conditionExecution = new ClauseN2();
        conditionExecution.setCode("conditionExecution");
        ClauseN2 critereAttribution = new ClauseN2();
        critereAttribution.setCode("critereAttribution");
        ContratClause contratClause1 = new ContratClause();
        contratClause1.setContrat(contrat);
        contratClause1.setClause(conditionExecution);
        ContratClause contratClause2 = new ContratClause();
        contratClause2.setContrat(contrat);
        contratClause2.setClause(critereAttribution);
        contrat.setContratClauses(Set.of(contratClause1, contratClause2));

        //WHEN
        ConsiderationsSociales considerationsSociales = dataGouvMarcheMapper.toConsiderationsSociales(contrat, Map.of("conditionExecution", "CLAUSE_SOCIALE", "critereAttribution", "CRITÈRE_SOCIAL"));
        ConsiderationsEnvironnementales considerationsEnvironnementales = dataGouvMarcheMapper.toConsiderationsEnvironnementales(contrat, Map.of("specificationsTechniques", "CLAUSE_ENVIRONNEMENTALE"));
        //THEN
        ConsiderationsSociales considerationsSocialesExpected = new ConsiderationsSociales();
        considerationsSocialesExpected.setConsiderationSociale(Set.of(ConsiderationSocialeArray.CLAUSE_SOCIALE, ConsiderationSocialeArray.CRITÈRE_SOCIAL));
        ConsiderationsEnvironnementales considerationsEnvironnementalesExpected = new ConsiderationsEnvironnementales();
        considerationsEnvironnementalesExpected.setConsiderationEnvironnementale(Set.of(ConsiderationEnvironnementaleArray.PAS_DE_CONSIDÉRATION_ENVIRONNEMENTALE));
        assertThat(considerationsSociales).usingRecursiveComparison().isEqualTo(considerationsSocialesExpected);
        assertThat(considerationsEnvironnementales).usingRecursiveComparison().isEqualTo(considerationsEnvironnementalesExpected);
    }

    @Test
    void test_mapping_contrat_sans_consideration_sociale() {
        //GIVEN
        Contrat contrat = new Contrat();
        contrat.setContratClauses(null);

        //WHEN
        ConsiderationsSociales considerationsSociales = dataGouvMarcheMapper.toConsiderationsSociales(contrat, Map.of("conditionExecution", "CLAUSE_SOCIALE", "critereAttribution", "CRITÈRE_SOCIAL"));

        //THEN
        ConsiderationsSociales considerationsSocialesExpected = new ConsiderationsSociales();
        considerationsSocialesExpected.setConsiderationSociale(Set.of(ConsiderationSocialeArray.PAS_DE_CONSIDÉRATION_SOCIALE));
        assertThat(considerationsSociales).usingRecursiveComparison().isEqualTo(considerationsSocialesExpected);
    }

    @Test
    void test_mapping_taux_avance_null() {
        //GIEVN
        var contrat = new Contrat();
        contrat.setTauxAvance(null);

        //WHEN
        Marche marche = dataGouvMarcheMapper.contratToDataGouvMarche(contrat, mappingDataGouv, "01");

        //THEN
        assertThat(marche.getTauxAvance()).isEqualTo(0.0);
    }

    @Test
    void test_mapping_taux_avance_non_null() {
        //GIEVN
        var contrat = new Contrat();
        contrat.setTauxAvance(new BigDecimal(20));

        //WHEN
        Marche marche = dataGouvMarcheMapper.contratToDataGouvMarche(contrat, mappingDataGouv, "01");

        //THEN
        assertThat(marche.getTauxAvance()).isEqualTo(20.0);
    }

    @Test
    void test_mapping_nautre_contrat() {
        //GIVEN
        var contrat = new Contrat();
        var typeContrat = new TypeContrat();
        typeContrat.setCode("mar");
        contrat.setType(typeContrat);

        //WHEN
        Marche.Nature natureActual = dataGouvMarcheMapper.toNature(contrat, Map.of("mar", "MARCHÉ", "abcmo", "MARCHÉ"));

        //THEN
        assertThat(natureActual).isEqualTo(Marche.Nature.MARCHÉ);
    }
}
