package com.atexo.execution.server.mapper;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.concession.TypeConcession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ContratMapperV2Test {

    @InjectMocks
    private ContratMapperImpl contratMapper;
    @Mock
    ReferentielMapper referentielMapper;
    @Mock
    ClauseMapper clauseMapper;
    @Mock
    ConsultationMapperV2 consultationMapperV2;

    @Test
    void given_contrat_isNotifiable_assert_notifiable_true() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        assertTrue(contratMapper.isNotifiable(contrat));
    }

    @Test
    void given_contrat_non_Notifiable_assert_notifiable_false() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.Clos);
        assertFalse(contratMapper.isNotifiable(contrat));
    }

    @Test
    void given_contrat_with_chorus_isNotifiable_assert_notifiable_false() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setService(new Service());
        contrat.getService().setOrganisme(new Organisme());
        contrat.getService().setEchangesChorus(true);
        contrat.setStatutEJ("pas de statut");
        assertFalse(contratMapper.isNotifiable(contrat));
    }

    @Test
    void given_contrat_with_chorus_commande_isNotifiable_assert_notifiable_true() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setService(new Service());
        contrat.getService().setOrganisme(new Organisme());
        contrat.getService().setEchangesChorus(true);
        contrat.setStatutEJ("échange chorus commandé par ce que ça contient commandé");
        assertTrue(contratMapper.isNotifiable(contrat));
    }

    @Test
    void given_contrat_with_chorus_fen211_isNotifiable_assert_notifiable_true() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setService(new Service());
        contrat.getService().setOrganisme(new Organisme());
        contrat.getService().setEchangesChorus(true);
        contrat.setType(new TypeContrat());
        contrat.getType().setModeEchangeChorus(2);
        var echange = new EchangeChorus();
        echange.setRetourChorus(new RetourChorus());
        echange.getRetourChorus().setIdExterne("COM");
        contrat.getEchangesChorus().add(echange);
        assertTrue(contratMapper.isNotifiable(contrat));
    }

    @Test
    void given_contrat_with_chorus_fen211_isNotifiable_assert_notifiable_false() {
        var contrat = new Contrat();
        contrat.setStatut(StatutContrat.ANotifier);
        contrat.setService(new Service());
        contrat.getService().setOrganisme(new Organisme());
        contrat.getService().setEchangesChorus(true);
        contrat.setType(new TypeContrat());
        contrat.getType().setModeEchangeChorus(1);
        var echange = new EchangeChorus();
        echange.setRetourChorus(new RetourChorus());
        echange.getRetourChorus().setIdExterne("REJ");
        contrat.getEchangesChorus().add(echange);
        assertFalse(contratMapper.isNotifiable(contrat));
    }

    @Test
    void tester_contrat_concession_mapping() {
        //GIVEN
        var contrat = new Contrat();
        var typeContrat = new TypeContrat();
        typeContrat.setParents(Set.of(new TypeConcession()));
        contrat.setType(typeContrat);

        //WHEN
        var contratDto = contratMapper.toDto(contrat);

        //THEN
        assertTrue(contratDto.isConcession());
    }

    @Test
    void tester_contrat_standard_mapping() {
        //GIVEN
        var contrat = new Contrat();
        var typeContrat = new TypeContrat();
        typeContrat.setParents(Set.of(new CategorieConsultation()));
        contrat.setType(typeContrat);

        //WHEN
        var contratDto = contratMapper.toDto(contrat);

        //THEN
        assertFalse(contratDto.isConcession());
    }

    @Test
    void given_contrat_with_chorus_assert_peut_echanger_avec_chorus_false() {
        var contrat = new Contrat();
        contrat.setService(new Service());
        contrat.getService().setEchangesChorus(true);
        var acte = new ActeModificatif();
        acte.setStatut(StatutActe.EN_ATTENTE_VALIDATION);
        contrat.setActes(Set.of(acte));
        assertFalse(contratMapper.peutEchangerAvecChorus(contrat));
    }
    @Test
    void test_recuperation_numej_cas_complexe() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setNumEj("1_1300010540#2_1300010540#3_1300010541#4_1300010541#");

        //WHEN
        var numEj = contratMapper.toNumEj(contrat);

        //THEN
        assertEquals("1300010541", numEj);
    }

    @Test
    void test_recuperation_numej_null() {
        //GIVEN
        var contrat = new Contrat();

        //WHEN
        var numEj = contratMapper.toNumEj(contrat);

        //THEN
        assertEquals(null, numEj);
    }

    @Test
    void test_recuperation_numej_cas_simple_1() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setNumEj("1_1300010540#");
        //WHEN
        var numEj = contratMapper.toNumEj(contrat);

        //THEN
        assertEquals("1300010540", numEj);
    }

    @Test
    void test_recuperation_numej_cas_simple_2() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setNumEj("1_1300010540");
        //WHEN
        var numEj = contratMapper.toNumEj(contrat);

        //THEN
        assertEquals("1300010540", numEj);
    }
    @Test
    void test_recuperation_numej_cas_simple_3() {
        //GIVEN
        var contrat = new Contrat();
        contrat.setNumEj("1300010540");
        //WHEN
        var numEj = contratMapper.toNumEj(contrat);

        //THEN
        assertEquals("1300010540", numEj);
    }

}
