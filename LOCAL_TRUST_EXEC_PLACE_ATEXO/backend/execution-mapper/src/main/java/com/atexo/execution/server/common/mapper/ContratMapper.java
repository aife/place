package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.server.common.mapper.qualifier.LightDTO;
import com.atexo.execution.server.common.mapper.qualifier.WithoutChapeau;
import com.atexo.execution.server.model.Contrat;


public interface ContratMapper {


    @WithoutChapeau
    ContratDTO toDTO(Contrat contrat);


    @LightDTO
    ContratDTO toLightDTO(Contrat contrat);

    Contrat createEntity(ContratDTO contratDTO);


}
