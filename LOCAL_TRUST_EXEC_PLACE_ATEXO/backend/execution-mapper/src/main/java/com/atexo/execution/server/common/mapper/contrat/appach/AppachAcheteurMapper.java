package com.atexo.execution.server.common.mapper.contrat.appach;

import com.atexo.execution.common.appach.model.contrat.titulaire.Acheteur;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.Utilisateur;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AppachAcheteurMapper {

    @Mapping(target = "id", source = "contrat.service.organisme.siret")
    @Mapping(target = "nom", source = "contrat.service.nom")
    @Mapping(target = "idAgent", source = "contrat", qualifiedByName = "toIdAgent")
    @Mapping(target = "nomAgent", source = "contrat.createur.nom")
    @Mapping(target = "prenomAgent", source = "contrat.createur.prenom")
    @Mapping(target = "emailAgent", source = "contrat.createur.email")
    @Mapping(target = "organisme", source = "contrat", qualifiedByName = "toAcronyme")
    @Mapping(target = "libelleOrganisme", source = "service.organisme.nom")
    @Mapping(target = "idOrganisme", source = "contrat", qualifiedByName = "toIdOrganisme")
    @Mapping(target = "idEntiteAchat", source = "contrat", qualifiedByName = "toIdEntiteAchat")
    @Mapping(target = "oldIdEntiteAchat", source = "contrat", qualifiedByName = "toIdEntiteAchat")
    @Mapping(target = "libelleEntiteAchat", source = "contrat.service.nom")
    @Mapping(target = "accesChorus", source = "contrat.service.echangesChorus", qualifiedByName = "toAccesChorus")
    Acheteur toAcheteur(Contrat contrat);

    @Named("toIdAgent")
    default Integer toIdAgent(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(this::extractIdAgent)
                .orElse(null);
    }

    default Integer extractIdAgent(Contrat contrat) {
        var idExterneCreateur = contrat.getIdExterneCreateur();

        if (!StringUtils.isEmpty(idExterneCreateur)) {
            var id = BeanSynchroUtils.extractId(idExterneCreateur);
            return id != null ? Integer.parseInt(id) : null;
        }

        return Optional.ofNullable(contrat.getCreateur())
                .map(Utilisateur::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toOldIdEntiteAchat")
    default String toOldIdEntiteAchat(String idExterne) {
        return Optional.ofNullable(idExterne)
                .map(BeanSynchroUtils::extractId)
                .orElse(null);
    }

    @Named("toAccesChorus")
    default Integer toAccesChorus(boolean accesChorus) {
        return accesChorus ? 1 : 0;
    }

    @Named("toIdOrganisme")
    default Integer toIdOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toAcronyme")
    default String toAcronyme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getAcronyme)
                .map(String::toLowerCase)
                .orElse(null);
    }

    @Named("toIdEntiteAchat")
    default Integer toIdEntiteAchat(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOldIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }
}
