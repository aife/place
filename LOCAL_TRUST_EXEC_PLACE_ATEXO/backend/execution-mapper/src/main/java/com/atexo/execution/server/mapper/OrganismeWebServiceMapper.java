package com.atexo.execution.server.mapper;

import com.atexo.execution.common.exec.mpe.ws.OrganismeType;
import com.atexo.execution.common.exec.mpe.ws.ServiceType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface OrganismeWebServiceMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "acronyme", source = "acronyme")
    @Mapping(target = "denomination", source = "nom")
    @Mapping(target = "sigle", ignore = true)
    @Mapping(target = "description", source = "complement")
    @Mapping(target = "categorieInsee", ignore = true)
    @Mapping(target = "siren", source = "siren")
    @Mapping(target = "nic", ignore = true)
    @Mapping(target = "adresse", ignore = true)
    @Mapping(target = "logo", ignore = true)
    @Mapping(target = "echangesChorus", ignore = true)
    @Mapping(target = "idExterne", source = "idExterne")
    OrganismeType toDto(Organisme organisme);
}
