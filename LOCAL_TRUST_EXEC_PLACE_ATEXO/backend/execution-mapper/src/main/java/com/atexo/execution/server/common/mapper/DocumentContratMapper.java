package com.atexo.execution.server.common.mapper;


import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.server.model.DocumentContrat;
import org.mapstruct.DecoratedWith;

@DecoratedWith(DocumentContratMapperDecorator.class)
public interface DocumentContratMapper {

	DocumentContratDTO toDTO( DocumentContrat document, boolean withEvenement);

	DocumentContrat createEntity( DocumentContratDTO documentsDTO);

}
