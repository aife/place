package com.atexo.execution.server.common.mapper.actes.impl;

import com.atexo.execution.common.def.NatureActe;
import com.atexo.execution.common.def.Variation;
import com.atexo.execution.common.dto.AbstractDTO;
import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.DocumentActeDTO;
import com.atexo.execution.common.mpe.ws.api.*;
import com.atexo.execution.server.common.mapper.DocumentActeMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.referentiels.CodeExterneActeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public abstract class BaseActeMapper {

	@Autowired
	CodeExterneActeRepository codeExterneActeRepository;

	@Autowired
	private DocumentActeMapper documentActeMapper;

	@Autowired
	private EtablissementRepository etablissementRepository;

	@Autowired
	private EtablissementMapper etablissementMapper;

	protected ContactDTO map(Contact contact) {
		if (null == contact) {
			return null;
		}

		var titulaireDTO = new ContactDTO();
		titulaireDTO.setId(contact.getId());
		titulaireDTO.setNom(contact.getNom());
		titulaireDTO.setEmail(contact.getEmail());
		titulaireDTO.setTelephone(contact.getTelephone());
		titulaireDTO.setAdresse(contact.getAdresse());
		titulaireDTO.setEtablissement(etablissementMapper.toDTO(contact.getEtablissement()));

		return titulaireDTO;
	}

	protected Contact map(ContactDTO contactDTO) {
		if (null == contactDTO) {
			return null;
		}

		var titulaire = new Contact();
		titulaire.setId(contactDTO.getId());
		titulaire.setNom(contactDTO.getNom());
		titulaire.setEmail(contactDTO.getEmail());
		titulaire.setTelephone(contactDTO.getTelephone());
		titulaire.setAdresse(contactDTO.getAdresse());

		Optional.ofNullable(contactDTO.getEtablissement())
					.map(AbstractDTO::getId)
					.flatMap(etablissementRepository::findById)
					.ifPresent(titulaire::setEtablissement);

		return titulaire;
	}

	protected CodeFournisseurType getCodeFournisseur(Acte acte) {
		CodeFournisseurType result = null;

		// Fournisseur
		if (null != acte && null != acte.getChorus() && null != acte.getChorus().getTypeFournisseurEntreprise1()) {
			switch (acte.getChorus().getTypeFournisseurEntreprise1()) {
				case "TITULAIRE":
				default:
					result = CodeFournisseurType.TITULAIRE;
					break;
				case "CO-TRAITANT":
					result = CodeFournisseurType.CO_TRAITANT;
					break;
				case "SOUS-TRAITANT":
					result = CodeFournisseurType.SOUS_TRAITANT;
					break;
			}
		}

		return result;

	}

	protected AdresseType toWS(Adresse adresse) {
		if (null == adresse) {
			return null;
		}

		var adresseMPE = new AdresseType();

		var codePostal = adresse.getCodePostal();

		if (null != codePostal) {
			adresseMPE.setCodePostal(codePostal);
		}
		adresseMPE.setAcronymePays(null);
		adresseMPE.setPays(adresse.getPays());
		adresseMPE.setRue(adresse.getAdresse());
		adresseMPE.setVille(adresse.getCommune());

		return adresseMPE;
	}

	protected EtablissementType toWS(Etablissement etablissement) {
		if (null == etablissement) {
			return null;
		}

		var etablissementMPE = new EtablissementType();

		// Adresse
		etablissementMPE.setAdresse(this.toWS(etablissement.getAdresse()));

		// Adresse
		var fournisseur = etablissement.getFournisseur();
		if (null != fournisseur) {
			var fournisseurMPE = new FournisseurType();
			fournisseurMPE.setNom(fournisseur.getNom());
			fournisseurMPE.setSiren(fournisseur.getSiren());
			fournisseurMPE.setRaisonSociale(fournisseur.getRaisonSociale());
			etablissementMPE.setFournisseur(fournisseurMPE);
		}

		etablissementMPE.setDateCreation(MapperUtils.toXmlGregorianCalendar(etablissement.getDateCreation()));
		etablissementMPE.setDateModification(MapperUtils.toXmlGregorianCalendar(etablissement.getDateModification()));
		etablissementMPE.setIdEntreprise(null);
		etablissementMPE.setSiege(etablissement.getSiege());
		etablissementMPE.setSiret(etablissement.getSiret());

		return etablissementMPE;
	}

	protected SousTraitantType toWS(SousTraitant sousTraitant) {
		var result = new SousTraitantType();

		if (null != sousTraitant) {
			result.setNom(sousTraitant.getContact().getNom());
			result.setEmail(sousTraitant.getContact().getEmail());
			result.setTelephone(sousTraitant.getContact().getTelephone());
			result.setAdresse(sousTraitant.getContact().getAdresse());
			result.setEtablissement(toWS(sousTraitant.getContact().getEtablissement()));
			result.setDateNotification(MapperUtils.toXmlGregorianCalendar(sousTraitant.getDateNotification()));
			result.setDateDemande(MapperUtils.toXmlGregorianCalendar(sousTraitant.getDateDemande()));
			result.setSiren(null);
		}

		return result;
	}

	protected VariationType toWS(Variation variation) {
		switch (variation) {
			case FERME:
			default:
				return VariationType.FERME;
			case FERME_ET_ACTUALISABLE:
				return VariationType.FERME_ET_ACTUALISABLE;
			case REVISABLE:
				return VariationType.REVISABLE;
		}
	}

	protected NatureActeType toWS(NatureActe nature) {
		switch (nature) {
			case INITIAL:
			default:
				return NatureActeType.INITIAL;
			case QUITUS:
				return NatureActeType.QUITUS;
			case MODIFICATIF:
				return NatureActeType.MODIFICATIF;
		}
	}

	protected PrestationType toWS(Prestation prestation) {
		var result = new PrestationType();

		result.setCategorie(prestation.getCategorie());
		result.setNature(prestation.getNature());

		return result;
	}

	protected ActeType.Cotraitants toWS(Set<Etablissement> cotraitants) {
		var result = new ActeType.Cotraitants();

		cotraitants.stream().map(this::toWS).forEach(cotraitant -> result.getCotraitant().add(cotraitant));

		return result;
	}

	protected String toWS(TypeActeMPE type) {
		return this.codeExterneActeRepository.findAll().stream().filter(typeActe -> type.getLibelle().equals(typeActe.getLabel())).map(AbstractReferentielEntity::getCode).findFirst().orElseThrow(IllegalArgumentException::new);
	}

	protected List<DocumentActeDTO> documentListToDocumentDTOList(List<DocumentActe> documents) {
		return documents.stream().map(documentActeMapper::toDTO).collect(Collectors.toList());
	}
}
