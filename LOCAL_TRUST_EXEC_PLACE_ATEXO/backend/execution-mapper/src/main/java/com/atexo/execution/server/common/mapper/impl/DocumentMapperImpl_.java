package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.server.common.mapper.DocumentContratMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.FichierMapper;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.DocumentContrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;
import java.util.Optional;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
@Qualifier("delegate")
public class DocumentMapperImpl_ implements DocumentContratMapper {

    @Autowired
    private EtablissementMapper etablissementMapper;
    @Autowired
    private UtilisateurMapper utilisateurMapper;
    @Autowired
    private FichierMapper fichierMapper;

    @Override
    public DocumentContratDTO toDTO( DocumentContrat document, boolean withEvenement) {
        if (document == null) {
            return null;
        }

        DocumentContratDTO documentDTO = new DocumentContratDTO();

        documentDTO.setId(document.getId());
        documentDTO.setEtablissement(etablissementMapper.toDTO(document.getEtablissement()));
        documentDTO.setDocumentType(document.getDocumentType());
        documentDTO.setObjet(document.getObjet());
        documentDTO.setCommentaire(document.getCommentaire());
        documentDTO.setFichier(fichierMapper.toDTO(document.getFichier()));
        documentDTO.setDateCreation(document.getDateCreation());
        documentDTO.setDateModification(document.getDateModification());
        documentDTO.setUtilisateur(utilisateurMapper.toDTO(document.getUtilisateurCreation()));
        documentDTO.setIdExterne(document.getIdExterne());
        documentDTO.setStatut(document.getStatut());
        documentDTO.setDateModificationEdition(document.getDateModificationEdition());
        documentDTO.setOrganismeUid(Optional.ofNullable(document.getContrat()).map(Contrat::getService).map(Service::getOrganisme).map(Organisme::getUuid).orElse(null));

        return documentDTO;
    }

    @Override
    public DocumentContrat createEntity( DocumentContratDTO documentsDTO) {
        if (documentsDTO == null) {
            return null;
        }

        DocumentContrat document = new DocumentContrat();

        document.setId(documentsDTO.getId());
        document.setIdExterne(documentsDTO.getIdExterne());
        document.setObjet(documentsDTO.getObjet());
        document.setCommentaire(documentsDTO.getCommentaire());

        return document;
    }
}
