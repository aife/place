package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;
import com.atexo.execution.server.model.ContratEtablissementGroupement;

public interface ContratEtablissementGroupementMapper {

	ContratEtablissementGroupementDTO toDTO(ContratEtablissementGroupement contratEtablissementGroupement);


}
