package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ContratMapper.class, EtablissementMapperV2.class})
public interface ActeMapperV2 {

    @Mapping(source = "acheteur.nom", target = "acheteur")
    @Mapping(source = "titulaire.etablissement", target = "titulaire")
    ActeType toWS(Acte acte);
}
