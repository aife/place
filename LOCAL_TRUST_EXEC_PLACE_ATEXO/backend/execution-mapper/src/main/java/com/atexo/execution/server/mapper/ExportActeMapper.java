package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.exports.ExportActeDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface ExportActeMapper {

    @Mapping(target = "objet", source = "objet" , defaultValue = "")
    @Mapping(target = "numeroActe", source = "numero", defaultValue = "")
    @Mapping(target = "typeActe", source = "typeActe.code", defaultValue = "")
    @Mapping(target = "acheteur", source = "acheteur.nom", defaultValue = "")
    @Mapping(target = "titulaire", source = "titulaire.nom", defaultValue = "")
    @Mapping(target = "statut", source = "statut", defaultValue = "")
    @Mapping(target = "dateNotification", source = "dateNotification", dateFormat = "dd/MM/yyyy H:mm", defaultValue = "")
    @Mapping(target = "statutPublication", source = "statutPublication", defaultValue = "")
    ExportActeDTO toDto(Acte acte);

}
