package com.atexo.execution.server.mapper.utils;

import com.atexo.execution.common.def.EtatEvenement;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.model.AbstractReferentielEntity;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.actes.ActeModificatif;
import org.mapstruct.Named;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MapperUtils {
    private static Pattern UUID_REGEX =
            Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");


    public static LocalDate convert(LocalDateTime date) {
        if (null == date) {
            return null;
        }
        return date.toLocalDate();
    }

    public static LocalDateTime convert(LocalDate date) {
        if (null == date) {
            return null;
        }
        return date.atStartOfDay();
    }

    public static Date convertToDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date convertToDate(OffsetDateTime localDate) {
        if (localDate == null) {
            return null;
        }
        return convertToDate(localDate.toLocalDateTime());
    }

    public static Date convertToDate(LocalDateTime localDate) {
        if (localDate == null) {
            return null;
        }
        return Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate convertToLocalDate(Date date) {
        if (date == null) {
            return null;
        }
        Instant instant = date.toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime convertToLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        Instant instant = date.toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime convertToLocalDateTime(Integer timestamp) {
        if (timestamp == null) {
            return null;
        }
        Instant instant = Instant.ofEpochSecond(timestamp);
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime convertToLocalDateTime(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        Instant instant = calendar.toGregorianCalendar().getTime().toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDate convertToLocalDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        Instant instant = calendar.toGregorianCalendar().getTime().toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static boolean parseBoolean(Integer value) {
        return value != null && value.equals(1);
    }

    public static XMLGregorianCalendar toXmlGregorianCalendar(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        try {
            GregorianCalendar gregorianCalendar = GregorianCalendar.from(dateTime.atZone(ZoneOffset.UTC));
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static XMLGregorianCalendar toXmlGregorianCalendar(LocalDate dateTime) {
        if (dateTime == null) {
            return null;
        }
        try {
            GregorianCalendar gregorianCalendar = GregorianCalendar.from(dateTime.atStartOfDay().atZone(ZoneOffset.UTC));
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toBooleanString(Boolean value) {
        if (value == null) {
            return null;
        }
        return value ? "Oui" : "Non";
    }

    public static String toReferentiel(Collection<? extends AbstractReferentielEntity> referentiels) {
        return referentiels.stream().map(AbstractReferentielEntity::getCode).collect(Collectors.joining(","));
    }

    public static BigDecimal getMontantTotalAvenants(Contrat contrat) {
        BigDecimal montant = BigDecimal.valueOf(0);
        if (!contrat.getEvenements().isEmpty()) {
            for (var evenementDTO : contrat.getEvenements().stream().filter(evenement -> evenement.getEtatEvenement() == EtatEvenement
                    .VALIDE).collect(Collectors.toSet())) {
                if (evenementDTO.getAvenantType() != null && Boolean.TRUE.equals(evenementDTO.getAvenantType().getIncidence())) {
                    if (Boolean.TRUE.equals(evenementDTO.getAvenantType().getValue())) {
                        montant = montant.add(evenementDTO.getAvenantType().getMontant());
                    } else {
                        montant = montant.subtract(evenementDTO.getAvenantType().getMontant());
                    }
                }
            }
        }
        if (!contrat.getActes().isEmpty()) {
            for (var acte : contrat.getActes()) {
                if (acte instanceof ActeModificatif) {
                    var acteModificatif = (ActeModificatif) acte;
                    if (acte.getStatut() == StatutActe.NOTIFIE && acteModificatif.getMontantTTCChiffre() != null) {
                        montant = montant.add(BigDecimal.valueOf(acteModificatif.getMontantTTCChiffre()));
                    }
                }
            }
        }
        return montant;
    }

    public static String buildIdExterne(Object... params) {
        return Arrays.stream(params).map(Object::toString).collect(Collectors.joining("_"));
    }


    public static Integer toBooleanInteger(Boolean value) {
        if (value == null) {
            return null;
        }
        return value ? 1 : 0;
    }

    public static boolean checkIfExist(String s, List<String> list) {
        return Optional.ofNullable(list)
                .orElse(Collections.emptyList())
                .stream().anyMatch(e -> e.equalsIgnoreCase(s));
    }

    @Named("localDateTimeToZonedDate")
    public static String localDateTimeToZonedDate(LocalDateTime date) {
        return Optional.ofNullable(date)
                .map(e -> e.atOffset(ZoneOffset.ofHours(1)))
                .map(e -> e.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX")))
                .orElse(null);
    }

    @Named("localDateToZonedDate")
    public static String localDateToZonedDate(LocalDate date) {
        return Optional.ofNullable(date)
                .map(LocalDate::atStartOfDay)
                .map(e -> e.atOffset(ZoneOffset.ofHours(1)))
                .map(e -> e.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX")))
                .orElse(null);
    }

    public static Long toLong(String s) {
        // cas d'un UID
        if (s == null) {
            return null;
        }
        if (UUID_REGEX.matcher(s).matches()) {
            return null;
        }
        if (!s.contains("_")) {
            try {
                return Long.parseLong(s);
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return Optional.of(s).filter(id -> id.contains("_")).map(id -> id.substring(id.lastIndexOf("_") + 1)).map(Long::parseLong).orElse(null);
    }

    public static Integer toInteger(String s) {
        // cas d'un UID
        if (s == null) {
            return null;
        }
        if (UUID_REGEX.matcher(s).matches()) {
            return null;
        }
        if (!s.contains("_")) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return Optional.of(s).filter(id -> id.contains("_")).map(id -> id.substring(id.lastIndexOf("_") + 1)).map(Integer::parseInt).orElse(null);

    }

}
