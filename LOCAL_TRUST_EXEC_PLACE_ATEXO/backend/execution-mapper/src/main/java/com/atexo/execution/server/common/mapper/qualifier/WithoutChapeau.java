package com.atexo.execution.server.common.mapper.qualifier;

import org.mapstruct.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Qualifier
@Retention(RetentionPolicy.SOURCE)
public @interface WithoutChapeau {
}

