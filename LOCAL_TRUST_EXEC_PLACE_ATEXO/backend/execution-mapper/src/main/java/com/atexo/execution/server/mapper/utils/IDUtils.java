package com.atexo.execution.server.mapper.utils;

import java.util.Optional;

public class IDUtils {
    public static String extractIdFromURI(String uri) {
        return Optional.ofNullable(uri).filter(u -> u.contains("/")).map(u -> u.substring(u.lastIndexOf("/") + 1)).orElse(null);
    }
}
