package com.atexo.execution.server.mapper;

import com.atexo.execution.common.exec.mpe.ws.ServiceType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring", uses = {MapperUtils.class, OrganismeWebServiceMapper.class})
public interface ServiceWebServiceMapper {

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "idSynchroExec", ignore = true)
	@Mapping(target = "idParent", ignore = true)
	@Mapping(target = "idExterne", source = "idExterne")
	@Mapping(target = "idExterneParent", source = "parent.idExterne")
	@Mapping(target = "libelle", source = "nomCourt")
	@Mapping(target = "acronymeOrganisme", source = "organisme.acronyme")
	@Mapping(target = "sigle", ignore = true)
	@Mapping(target = "email", ignore = true)
	@Mapping(target = "siren", ignore = true)
	@Mapping(target = "complement", ignore = true)
	@Mapping(target = "dateModification", ignore = true)
	@Mapping(target = "dateCreation", ignore = true)
	@Mapping(target = "formeJuridique", ignore = true)
	@Mapping(target = "formeJuridiqueCode", ignore = true)
	@Mapping(target = "organisme", source = "organisme")
	ServiceType toDto(Service service);
}
