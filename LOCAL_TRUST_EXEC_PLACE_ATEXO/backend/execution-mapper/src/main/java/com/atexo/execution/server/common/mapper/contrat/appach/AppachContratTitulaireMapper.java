package com.atexo.execution.server.common.mapper.contrat.appach;

import com.atexo.execution.common.appach.model.contrat.titulaire.*;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.Variation;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.common.mapper.clause.AppachClauseMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Consultation;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {AppachAcheteurMapper.class, AppachTitulaireMapper.class, AppachLieuExecutionMapper.class, AppachContratTitulaireConsultationMapper.class, MapperUtils.class, AppachClauseMapper.class})
public interface AppachContratTitulaireMapper {

    String SIRET = "siret";
    String HORS_UE = "Hors UE";
    String AMO = "AMO";
    String AMU = "AMU";
    String NATURE_PRESTATION = "nature-prestation";
    String TYPE_CONTRAT = "type-contrat";
    String TYPE_PRIX = "type-prix";
    String STATUT_CONTRAT = "statut-contrat";
    String CHAPEAU = "chapeau";
    String SAD = "sad";
    String ABCMO = "abcmo";
    String NATURE_CONTRAT = "nature-contrat";
    String STATUT_DONNEES_CONTRAT_A_SAISIR = "STATUT_DONNEES_CONTRAT_A_SAISIR";
    String STATUT_NOTIFICATION_CONTRAT = "STATUT_NOTIFICATION_CONTRAT";
    String STATUT_NOTIFICATION_CONTRAT_EFFECTUEE = "STATUT_NOTIFICATION_CONTRAT_EFFECTUEE";
    String DATE_NOTIFICATION = "dateNotification";
    String DATE_FIN = "dateFin";
    String DATE_MAX_FIN = "dateMaxFin";
    String MARMU = "marmu";
    String MSA = "msa";
    String VARIATION_PRIX = "variation-prix";

    @Mapping(target = "organisme", source = "contrat", qualifiedByName = "toAcronymeOrganisme")
    @Mapping(target = "horsPassation", source = "contrat.consultation.horsPassation")
    @Mapping(target = "id", source = "contrat", qualifiedByName = "toId")
    @Mapping(target = "uuid", source = "contrat.uuid")
    @Mapping(target = "idMarche", source = "contrat.numero")
    @Mapping(target = "numero", source = "contrat.numero")
    @Mapping(target = "numeroLong", source = "contrat.numeroLong")
    @Mapping(target = "numEj", source = "contrat.numEj")
    @Mapping(target = "referenceLibre", source = "contrat.referenceLibre")
    @Mapping(target = "objet", source = "contrat.objet")
    @Mapping(target = "idService", source = "contrat.service", qualifiedByName = "toIdService")
    @Mapping(target = "idCreateur", source = "createur.idExterne", qualifiedByName = "toIdCreateur")
    @Mapping(target = "formePrix", source = "contrat", qualifiedByName = "toFormePrix")
    @Mapping(target = "typesPrix", source = "contrat.consultation", qualifiedByName = "toTypesPrix")
    @Mapping(target = "modaliteRevisionPrix", source = "contrat", qualifiedByName = "toModaliteRevisionPrix")
    @Mapping(target = "defenseOuSecurite", source = "contrat", qualifiedByName = "toDefenseOuSecurite")
    @Mapping(target = "datePrevisionnelleNotification", source = "contrat.datePrevisionnelleNotification", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "datePrevisionnelleFinMarche", source = "contrat.datePrevisionnelleFinMarche", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "datePrevisionnelleFinMaximaleMarche", source = "contrat.datePrevisionnelleFinMaximaleMarche", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dateDebutExecution", source = "contrat.dateDebutExecution", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dureeMaximaleMarche", source = "contrat.dureeMaximaleMarche")
    @Mapping(target = "lieuExecutions", source = "contrat")
    @Mapping(target = "decisionAttribution", source = "contrat.consultation.decisionAttribution", qualifiedByName = "localDatetoString")
    @Mapping(target = "intitule", source = "contrat.intitule")
    @Mapping(target = "ccagApplicable", source = "contrat", qualifiedByName = "toCcagApplicable")
    @Mapping(target = "typeContrat", source = "contrat", qualifiedByName = "toTypeContrat")
    @Mapping(target = "dateModification", source = "contrat", qualifiedByName = "toDateModification")
    @Mapping(target = "dateCreation", source = "dateCreation", qualifiedByName = "localDateTimeToZonedDate")
    @Mapping(target = "dateNotification", expression = "java(calculDateByStatutContrat(DATE_NOTIFICATION,contrat,mappingPlaceOutput,statuts))")
    @Mapping(target = "statut", source = "contrat", qualifiedByName = "toStatut")
    @Mapping(target = "naturePrestation", source = "contrat.categorie", qualifiedByName = "toNaturePrestation")
    @Mapping(target = "chapeau", source = "contrat", qualifiedByName = "toChapeau")
    @Mapping(target = "idEtablissementTitulaire", source = "contrat.attributaire.etablissement.idExterne")
    @Mapping(target = "montant", source = "contrat.montant")
    @Mapping(target = "idContact", source = "attributaire.contact", qualifiedByName = "toIdContact")
    @Mapping(target = "cpv", source = "contrat")
    @Mapping(target = "contratTransverse", source = "contrat", qualifiedByName = "toContratTransverse")
    @Mapping(target = "nature", source = "contrat", qualifiedByName = "toNature")
    @Mapping(target = "marcheInnovant", source = "contrat.marcheInnovant", qualifiedByName = "toMarcheInnovant")
    @Mapping(target = "idChapeauAcSad", source = "contrat", qualifiedByName = "toIdChapeauAcSad")
    @Mapping(target = "idChapeauMultiAttributaire", source = "contrat", qualifiedByName = "toIdChapeauMultiAttributaire")
    @Mapping(target = "naturePassation", source = "contrat.type.label")
    @Mapping(target = "procedurePassation", source = "contrat.consultation.procedure.label")
    @Mapping(target = "procedure", source = "contrat.consultation.procedure.label")
    @Mapping(target = "nbTotalPropositionsRecu", source = "contrat.offresRecues", defaultValue = "0")
    @Mapping(target = "consultation", source = "contrat")
    @Mapping(target = "lots", source = "contrat")
    @Mapping(target = "oldIdService", source = "contrat", qualifiedByName = "toOldIdService")
    @Mapping(target = "acheteur", source = "contrat")
    @Mapping(target = "titulaire", source = "contrat.attributaire")
    @Mapping(target = "publicationContrat", source = "contrat.donneesEssentielles")
    @Mapping(target = "accordCadreAvecMarcheSubsequent", source = "contrat")
    @Mapping(target = "contratChapeauMultiAttributaires", source = "contrat.contratChapeau", qualifiedByName = "toContratChapeauMultiAttributaires")
    @Mapping(target = "contratChapeauAcSad", source = "contrat.contratChapeau", qualifiedByName = "toContratChapeauAcSad")
    @Mapping(target = "clausesSociales", source = "contrat")
    @Mapping(target = "clausesEnvironnementales", source = "contrat")
    @Mapping(target = "lienAcSad", source = "contrat.lienAcSad.uuid")
    @Mapping(target = "idExec", source = "contrat.id")
    @Mapping(target = "formePrixContrat", source = "contrat.typeFormePrix.code")
    @Mapping(target = "clausesSocialesV2", source = "contrat")
    @Mapping(target = "clausesEnvironnementalesV2", source = "contrat")
    @Mapping(target = "createur", source = "contrat.createur", qualifiedByName = "toCreateur")
    @Mapping(target = "dateSignature", source = "contrat.dateNotification", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dateFin", expression = "java(calculDateByStatutContrat(DATE_FIN,contrat,mappingPlaceOutput,statuts))")
    @Mapping(target = "dateMaxFin", expression = "java(calculDateByStatutContrat(DATE_MAX_FIN,contrat,mappingPlaceOutput,statuts))")
    @Mapping(target = "sousTraitants", ignore = true)
    @Mapping(target = "coTraitants", ignore = true)
    @Mapping(target = "statutEj", source = "contrat.statutEJ")
    @Mapping(target = "actesModificatifs", source = "contrat", qualifiedByName = "toActesModificatifs")
    @Mapping(target = "actesAgrementSousTraitant", source = "contrat", qualifiedByName = "toActesAgrementSousTraitant")
    @Mapping(target = "actesDecisionAffermissementTranche", source = "contrat", qualifiedByName = "toActesDecisionAffermissementTranche")
    @Mapping(target = "actesDecisionReconduction", source = "contrat", qualifiedByName = "toActesDecisionReconduction")
    @Mapping(target = "actesResiliation", source = "contrat", qualifiedByName = "toActesResiliation")
    @Mapping(target = "actesDecisionProlongationDelai", source = "contrat", qualifiedByName = "toActesDecisionProlongationDelai")
    @Mapping(target = "actesOrdreService", source = "contrat", qualifiedByName = "toActesOrdreService")
    @Mapping(target = "actesAutres", source = "contrat", qualifiedByName = "toActesAutres")
    @Mapping(target = "typeGroupementOperateurs", source = "contrat", qualifiedByName = "toGroupementOperateurs")
    @Mapping(target = "attributionAvance", source = "contrat.attributionAvance")
    @Mapping(target = "tauxAvance", source = "contrat.tauxAvance")
    @Mapping(target = "origineUE", source = "contrat.consultation.origineUE")
    @Mapping(target = "origineFrance", source = "contrat.consultation.origineFrance")
    @Mapping(target = "techniquesAchat", source = "contrat", qualifiedByName = "toTechniquesAchats")
    @Mapping(target = "modalitesExecution", source = "contrat", qualifiedByName = "toModalitesExecutions")
    @Mapping(target = "datePublicationDonnees", source = "contrat.donneesEssentielles.datePublication")
    ContratTitulaireAppach contratToAppachContratTitulaire(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput, @Context Set<Integer> statuts);

    @Named("toGroupementOperateurs")
    default String toGroupementOperateurs(Contrat contrat) {
        if (contrat.getConsultation() != null && contrat.getConsultation().getTypeGroupementOperateurs() != null) {
            return contrat.getConsultation().getTypeGroupementOperateurs().getLabel();
        } else {
            return null;
        }
    }

    @Named("toTechniquesAchats")
    default List<String> toTechniquesAchats(Contrat contrat) {
        if (contrat.getConsultation() != null && contrat.getConsultation().getTechniqueAchats() != null) {
            return contrat.getConsultation().getTechniqueAchats().stream().map(TechniqueAchat::getLabel).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Named("toModalitesExecutions")
    default List<String> toModalitesExecutions(Contrat contrat) {
        if (contrat.getConsultation() != null && contrat.getConsultation().getModaliteExecutions() != null) {
            return contrat.getConsultation().getModaliteExecutions().stream().map(ModaliteExecution::getLabel).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Mapping(target = "id", source = "contratChapeau", qualifiedByName = "toId")
    @Mapping(target = "numeroCourt", source = "contratChapeau.numero")
    @Mapping(target = "numeroLong", source = "contratChapeau.numeroLong")
    @Mapping(target = "referenceLibre", source = "contratChapeau.referenceLibre")
    ContratChapeauMultiAttributaires chapeauToContratChapeauMultiAttributaires(Contrat contratChapeau);

    @Mapping(target = "id", source = "contrat", qualifiedByName = "toId")
    @Mapping(target = "numeroCourt", source = "numero")
    @Mapping(target = "numeroLong", source = "numeroLong")
    @Mapping(target = "referenceLibre", source = "referenceLibre")
    @Mapping(target = "numIdUniqueMarchePublic", source = "numeroLong")
    ContratChapeauAcSad chapeauToContratChapeauAcSad(Contrat contrat);

    default Boolean toAccordCadreAvecMarcheSubsequent(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(type -> AMO.equals(type.getCode()) || AMU.equals(type.getCode())).orElse(null);
    }

    @Named("toContratChapeauAcSad")
    default ContratChapeauAcSad toContratChapeauAcSad(Contrat chapeau) {
        return Optional.ofNullable(chapeau)
                .filter(e -> e.getType() != null &&
                        MapperUtils.checkIfExist(e.getType().getCode(), List.of(SAD, AMO, ABCMO)))
                .map(this::chapeauToContratChapeauAcSad)
                .orElse(null);
    }

    @Named("toContratChapeauMultiAttributaires")
    default ContratChapeauMultiAttributaires toContratChapeauMultiAttributaires(Contrat chapeau) {
        return Optional.ofNullable(chapeau)
                .filter(e -> e.getType() != null &&
                        MapperUtils.checkIfExist(e.getType().getCode(), List.of(AMU)))
                .map(this::chapeauToContratChapeauMultiAttributaires)
                .orElse(null);
    }


    @Named("toTypesPrix")
    default Set<String> toTypesPrix(Consultation consultation) {
        return Optional.ofNullable(consultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .map(TypePrix::getCode)
                .collect(Collectors.toSet());
    }

    @Named("toOldIdService")
    default Integer toOldIdService(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOldIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toChapeau")
    default Boolean toChapeau(Contrat contrat) {
        var idChapeauAcSad = toIdChapeauAcSad(contrat);
        var idChapeauMultiAttributaire = toIdChapeauMultiAttributaire(contrat);
        return Optional.of(contrat)
                .filter(contrat1 -> !contrat1.isChapeau())
                .map(contrat1 -> idChapeauAcSad != null || idChapeauMultiAttributaire != null)
                .orElse(Boolean.FALSE);
    }

    @Named("toDefenseOuSecurite")
    default Integer toDefenseOuSecurite(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(c -> c.isDefenseOuSecurite() ? 1 : 0)
                .orElse(null);
    }

    @Named("toId")
    default Integer toId(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(c -> c.getIdExterne() != null && NumberUtils.isNumber(c.getIdExterne()) ? Integer.parseInt(c.getIdExterne()) : c.getId().intValue())
                .orElse(null);
    }

    @Named("toIdContact")
    default Integer toIdContact(Contact contact) {
        return Optional.ofNullable(contact)
                .map(c -> c.getIdExterne() != null && NumberUtils.isNumber(c.getIdExterne()) ? Integer.parseInt(c.getIdExterne()) : c.getId().intValue())
                .orElse(null);
    }

    @Mapping(target = "codePrincipal", source = "codesCpv", qualifiedByName = "toCodePrincipal")
    @Mapping(target = "codeSecondaire1", source = "codesCpv", qualifiedByName = "toCodeSecondaire1")
    @Mapping(target = "codeSecondaire2", source = "codesCpv", qualifiedByName = "toCodeSecondaire2")
    @Mapping(target = "codeSecondaire3", source = "codesCpv", qualifiedByName = "toCodeSecondaire3")
    Cpv toCpv(Contrat contrat);

    @Named("toCcagApplicable")
    default String toCcagApplicable(Contrat contrat) {
        return Optional.of(contrat)
                .map(Contrat::getCcagApplicable)
                .map(CCAGReference::getCode)
                .orElse(null);
    }

    @Named("toNaturePrestation")
    default String toNaturePrestation(CategorieConsultation categorieConsultation, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(categorieConsultation)
                .map(CategorieConsultation::getCode)
                .map(code -> mappingPlaceOutput.get(NATURE_PRESTATION) != null ? mappingPlaceOutput.get(NATURE_PRESTATION).get(code) : null)
                .orElse(null);
    }

    @Named("toCodePrincipal")
    default String toCodePrincipal(Set<CPV> codesCpv) {
        return Optional.ofNullable(codesCpv)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(AbstractReferentielEntity::getCode)
                .orElse(null);
    }

    @Named("toCodeSecondaire1")
    default String toCodeSecondaire1(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 1);
    }

    @Named("toCodeSecondaire2")
    default String toCodeSecondaire2(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 2);
    }

    @Named("toCodeSecondaire3")
    default String toCodeSecondaire3(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 3);
    }

    default String getElementByPosition(Set<CPV> codesCpv, int p) {
        return Optional.ofNullable(codesCpv)
                .orElse(Collections.emptySet())
                .stream()
                .skip(p)
                .map(AbstractReferentielEntity::getCode)
                .findAny().orElse(null);
    }

    default String toNature(CategorieConsultation categorieConsultation) {
        return Optional.ofNullable(categorieConsultation)
                .map(CategorieConsultation::getCode)
                .orElse(null);
    }

    default Lots toLot(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getNumeroLot)
                .filter(numeroLot -> numeroLot > 0)
                .map(numeroLot -> {
                    Lots lot = new Lots();
                    lot.setNumeroLot(numeroLot);
                    lot.setIntituleLot(contrat.getIntitule());
                    return lot;
                })
                .orElse(null);
    }

    default Integer toPublicationContrat(DonneesEssentielles donneesEssentielles) {
        return (donneesEssentielles != null && StatutPublicationDE.A_PUBLIER.equals(donneesEssentielles.getStatut())) ? 0 : 1;
    }

    @Named("toTypeContrat")
    default Integer toTypeContrat(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(TypeContrat::getCode)
                .map(label -> mappingPlaceOutput.get(TYPE_CONTRAT) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_CONTRAT).get(label)) : null)
                .orElse(null);
    }

    @Named("toFormePrix")
    default Integer toFormePrix(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(TypePrix::getCode)
                .map(code -> mappingPlaceOutput.get(TYPE_PRIX) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_PRIX).get(code)) : null)
                .orElse(null);
    }

    @Named("toModaliteRevisionPrix")
    default Integer toModaliteRevisionPrix(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(TypePrix::getCode)
                .map(code -> mappingPlaceOutput.get(TYPE_PRIX) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_PRIX).get(code)) : null)
                .orElse(null);
    }

    @Named("toStatut")
    default String toStatut(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput, @Context Set<Integer> statuts) {
        return Optional.ofNullable(contrat)
                .map(c -> calculStatut(c, statuts, mappingPlaceOutput))
                .orElse(null);
    }

    @Named("toNature")
    default String toNature(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat.getType())
                .map(TypeContrat::getCode)
                .map(code -> mappingPlaceOutput.get(NATURE_CONTRAT) != null ? mappingPlaceOutput.get(NATURE_CONTRAT).get(code) : null)
                .orElse(null);
    }

    @Named("toIdChapeauAcSad")
    default Integer toIdChapeauAcSad(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(c -> {
                    if (checkTypeContrat(c.getContratChapeau(), List.of(SAD, AMO, ABCMO))) {
                        return toId(c.getContratChapeau());
                    } else {
                        return toId(c.getLienAcSad());
                    }
                })
                .orElse(null);
    }

    default boolean checkTypeContrat(Contrat contrat, List<String> types) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(TypeContrat::getCode)
                .map(e -> MapperUtils.checkIfExist(e, types))
                .orElse(false);
    }


    @Named("toIdChapeauMultiAttributaire")
    default Integer toIdChapeauMultiAttributaire(Contrat contrat) {
        return Optional.ofNullable(contrat.getContratChapeau())
                .filter(e -> e.getType() != null &&
                        MapperUtils.checkIfExist(e.getType().getCode(), List.of(AMU, MARMU)) &&
                        e.isChapeau())
                .map(this::toId)
                .orElse(null);
    }

    @Named("toDateModification")
    default String toDateModification(Contrat contrat) {
        List<LocalDateTime> listDateModification = new ArrayList<>();

        Optional.ofNullable(contrat)
                .map(Contrat::getDateModification)
                .ifPresent(listDateModification::add);

        Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .map(Acte::getDateNotification)
                .filter(Objects::nonNull)
                .forEach(listDateModification::add);

        return listDateModification.stream()
                .max(LocalDateTime::compareTo)
                .map(MapperUtils::localDateTimeToZonedDate)
                .orElse(null);
    }

    @Named("toIdService")
    default String toIdService(Service service) {
        return Optional.ofNullable(service)
                .map(Service::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .orElse(null);
    }


    @Mapping(target = "id", source = "utilisateur")
    @Mapping(target = "habilitations", source = "utilisateur", qualifiedByName = "toHabilitations")
    @Mapping(target = "service", source = "utilisateur.service", qualifiedByName = "toService")
    @Named("toCreateur")
    Createur toCreateur(Utilisateur utilisateur);

    @Mapping(target = "id", source = "service")
    @Mapping(target = "organisme", source = "service.organisme")
    @Named("toService")
    com.atexo.execution.common.appach.model.contrat.titulaire.Service toService(Service service);

    @Named("toHabilitations")
    default List<String> toHabilitations(Utilisateur utilisateur) {
        return Optional.ofNullable(utilisateur)
                .map(Utilisateur::getHabilitations)
                .orElse(Collections.emptySet())
                .stream()
                .map(Habilitation::getCode)
                .collect(Collectors.toList());
    }


    default int getIdMPE(AbstractSynchronizedEntity entity) {
        if (entity == null || entity.getIdExterne() == null) {
            return 0;
        }
        String[] splitId = entity.getIdExterne().split("_");
        return (splitId.length > 1 && NumberUtils.isNumber(splitId[1])) ? Integer.parseInt(splitId[1]) : 0;
    }

    @Named("toAcronymeOrganisme")
    default String toAcronymeOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getAcronyme)
                .map(String::toLowerCase)
                .orElse(null);
    }

    @Mapping(target = "id", expression = "java(getIdMPE(organisme))")
    com.atexo.execution.common.appach.model.contrat.titulaire.Organisme organismeToOrganisme(Organisme organisme);

    @Named("toIdCreateur")
    default Integer toIdCreateur(String idExterne) {
        return Optional.ofNullable(idExterne)
                .map(MapperUtils::toInteger)
                .map(Math::toIntExact)
                .orElse(null);
    }

    @Named("localDatetoString")
    default String localDatetoString(LocalDate date) {
        return Optional.ofNullable(date)
                .map(LocalDate::atStartOfDay)
                .map(MapperUtils::localDateTimeToZonedDate)
                .orElse(null);
    }

    @Named("localDateTimetoString")
    default String localDateTimetoString(LocalDateTime date) {
        return Optional.ofNullable(date)
                .map(LocalDateTime::toLocalDate)
                .map(LocalDate::atStartOfDay)
                .map(MapperUtils::localDateTimeToZonedDate)
                .orElse(null);
    }

    @Named("toMarcheInnovant")
    default Integer toMarcheInnovant(boolean marcheInnovant) {
        return marcheInnovant ? 1 : 0;
    }

    @Named("toActesModificatifs")
    default List<ActeModificatifType> toActesModificatifs(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeModificatif.class::isInstance)
                .map(ActeModificatif.class::cast)
                .map(this::toActeModificatifType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "dureeMois", source = "acte.dureeAjoureeContrat", qualifiedByName = "castToInteger")
    @Mapping(target = "idDonneesEssentielles", source = "acte.donneesEssentielles.id")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "nouveauMontantContrat", source = "acte", qualifiedByName = "toNouveauMontantContrat")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "montantActe", source = "acte.montantHTChiffre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "nouvelleDateFinContrat", source = "acte.nouvelleFinContrat", qualifiedByName = "localDatetoString")
    @Mapping(target = "titulaire", source = "acte", qualifiedByName = "toTitulaire")
    ActeModificatifType toActeModificatifType(ActeModificatif acte);

    @Named("toActesOrdreService")
    default List<ActeOrdreServiceType> toActesOrdreService(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(OrdreService.class::isInstance)
                .map(OrdreService.class::cast)
                .map(this::toOrdreServiceType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "reconduction", source = "acte.reconduction")
    @Mapping(target = "maitriseOeuvre", source = "acte.maitreOeuvre")
    @Mapping(target = "maitreOuvrage", source = "acte.maitreOuvrage")
    @Mapping(target = "cotraitants", source = "acte", qualifiedByName = "toCotraitants")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    ActeOrdreServiceType toOrdreServiceType(OrdreService acte);

    @Named("toCotraitants")
    default List<TitulaireType> toCotraitants(OrdreService acte) {
        return Optional.ofNullable(acte)
                .map(OrdreService::getCotraitants)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toTitulaireType)
                .collect(Collectors.toList());
    }

    @Named("toActesResiliation")
    default List<ActeResiliationType> toActesResiliation(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(Resiliation.class::isInstance)
                .map(Resiliation.class::cast)
                .map(this::toActeResiliationType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "fraisTitulaire", source = "acte.fraisEtRisquesPourLeTitulaire")
    @Mapping(target = "motifResiliation", source = "acte.motif")
    @Mapping(target = "typeResiliation", source = "acte.couverture")
    @Mapping(target = "dateFinContrat", source = "acte.finContrat", qualifiedByName = "localDatetoString")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    ActeResiliationType toActeResiliationType(Resiliation acte);

    @Named("toActesAgrementSousTraitant")
    default List<ActeAgrementSousTraitantType> toActesAgrementSousTraitant(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(AgrementSousTraitant.class::isInstance)
                .map(AgrementSousTraitant.class::cast)
                .map(e-> toActeAgrementSousTraitantType(e,mappingPlaceOutput))
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "dureeMois", source = "acte.duree", qualifiedByName = "castToInteger")
    @Mapping(target = "idDonneesEssentielles", source = "acte.donneesEssentielles.id")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "rangSousTraitance", source = "acte.rang")
    @Mapping(target = "variationPrix", source = "acte", qualifiedByName = "toVariationPrix")
    @Mapping(target = "montant", source = "acte.montantHT")
    @Mapping(target = "natureActe", ignore = true)
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "sousTraitant", source = "acte", qualifiedByName = "toSousTraitant")
    ActeAgrementSousTraitantType toActeAgrementSousTraitantType(AgrementSousTraitant acte, @Context Map<String, Map<String, String>> mappingPlaceOutput);

    @Mapping(target = "dureeMois", source = "contratEtablissement.dureeMois")
    @Mapping(target = "dateNotification", source = "contratEtablissement.dateNotification", qualifiedByName = "localDatetoString")
    @Mapping(target = "montant", source = "contratEtablissement.montant", qualifiedByName = "castToDouble")
    @Mapping(target = "variationPrix", source = "contratEtablissement.revisionPrix.label")
    @Mapping(target = "datePublicationDonnees", source = "contratEtablissement.contrat.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "sousTraitant", source = "contratEtablissement", qualifiedByName = "etablissementToTitulaire")
    SousTraitantType toSousTraitantType(ContratEtablissement contratEtablissement);

    @Mapping(target = "dureeMois", source = "contratEtablissement.dureeMois")
    @Mapping(target = "dateNotification", source = "contratEtablissement.dateNotification", qualifiedByName = "localDatetoString")
    @Mapping(target = "montant", source = "contratEtablissement.montant", qualifiedByName = "castToDouble")
    @Mapping(target = "variationPrix", source = "contratEtablissement.revisionPrix.label")
    @Mapping(target = "datePublicationDonnees", source = "contratEtablissement.contrat.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "coTraitant", source = "contratEtablissement", qualifiedByName = "etablissementToTitulaire")
    CoTraitantType toCoTraitantType(ContratEtablissement contratEtablissement);

    @Named("etablissementToTitulaire")
    default TitulaireType etablissementToTitulaire(ContratEtablissement contratEtablissement) {
        return Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(this::toTitulaireType)
                .orElse(null);
    }

    @Named("castToDouble")
    default Double castToDouble(BigDecimal nombre) {
        return Optional.ofNullable(nombre)
                .map(BigDecimal::doubleValue)
                .orElse(null);
    }

    @Named("toActesDecisionProlongationDelai")
    default List<ActeDecisionProlongationDelaiType> toActesDecisionProlongationDelai(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(DecisionProlongationDelai.class::isInstance)
                .map(DecisionProlongationDelai.class::cast)
                .map(this::toActeDecisionProlongationDelaiType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "debutProlongation", source = "acte.debut", qualifiedByName = "localDatetoString")
    @Mapping(target = "finProlongation", source = "acte.fin", qualifiedByName = "localDatetoString")
    @Mapping(target = "dureeProlongation", source = "acte.duree")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    ActeDecisionProlongationDelaiType toActeDecisionProlongationDelaiType(DecisionProlongationDelai acte);


    @Named("toActesDecisionReconduction")
    default List<ActeDecisionReconductionType> toActesDecisionReconduction(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(DecisionReconduction.class::isInstance)
                .map(DecisionReconduction.class::cast)
                .map(this::toActeDecisionReconductionType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "reconduction", source = "acte.reconduction")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "dateDebutReconduction", source = "acte.debut", qualifiedByName = "localDatetoString")
    @Mapping(target = "dateFinReconduction", source = "acte.fin", qualifiedByName = "localDatetoString")
    @Mapping(target = "nouvelleDateFinContrat", source = "acte", qualifiedByName = "toNouvelleDateFinContrat")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    ActeDecisionReconductionType toActeDecisionReconductionType(DecisionReconduction acte);

    @Named("toNouvelleDateFinContrat")
    default String toNouvelleDateFinContrat(DecisionReconduction acte) {
        if (Boolean.FALSE.equals(acte.getReconduction())) {
            return Optional.ofNullable(acte.getContrat())
                    .map(Contrat::getDateFinContrat)
                    .map(LocalDate::atStartOfDay)
                    .map(MapperUtils::localDateTimeToZonedDate)
                    .orElse(null);
        } else {
            return null;
        }
    }

    @Named("toActesDecisionAffermissementTranche")
    default List<ActeDecisionAffermissementTrancheType> toActesDecisionAffermissementTranche(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(DecisionAffermissementTranche.class::isInstance)
                .map(DecisionAffermissementTranche.class::cast)
                .map(this::toActeDecisionAffermissementTrancheType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "tranche", source = "acte", qualifiedByName = "toTranche")
    ActeDecisionAffermissementTrancheType toActeDecisionAffermissementTrancheType(DecisionAffermissementTranche acte);

    @Named("toTranche")
    default TrancheType toTranche(DecisionAffermissementTranche acte) {
        return Optional.ofNullable(acte)
                .map(this::toTrancheType)
                .orElse(null);
    }

    @Mapping(target = "numeroTranche", source = "acte.numeroTranche")
    @Mapping(target = "montant", source = "acte.montantHT")
    @Mapping(target = "dateDebutExecution", source = "acte.debutExecutionTranche", qualifiedByName = "localDatetoString")
    @Mapping(target = "dateFinExecution", source = "acte.finExecutionTranche", qualifiedByName = "localDatetoString")
    @Mapping(target = "duree", source = "acte.dureeExecutionTranche")
    TrancheType toTrancheType(DecisionAffermissementTranche acte);

    @Named("toActesAutres")
    default List<ActeAutreType> toActesAutres(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeAutre.class::isInstance)
                .map(ActeAutre.class::cast)
                .map(this::toActeAutreType)
                .collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "acte.id", qualifiedByName = "castToInteger")
    @Mapping(target = "numeroActe", source = "acte.numero")
    @Mapping(target = "statut", source = "acte", qualifiedByName = "toStatutActe")
    @Mapping(target = "numeroOrdre", source = "acte.numeroOrdre")
    @Mapping(target = "objetActe", source = "acte.objet")
    @Mapping(target = "statutEchange", source = "acte", qualifiedByName = "toStatutEchange")
    @Mapping(target = "dateNotification", source = "acte.dateNotification", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "datePublicationDonnees", source = "acte.donneesEssentielles.datePublication", qualifiedByName = "localDateTimetoString")
    @Mapping(target = "signaturePouvoirAdjudicateur", source = "acte.dateSignaturePouvoirAdjudicateur", qualifiedByName = "localDatetoString")
    ActeAutreType toActeAutreType(ActeAutre acte);

    @Named("castToInteger")
    default Integer castToInteger(Long nombre) {
        return Optional.ofNullable(nombre)
                .map(Math::toIntExact)
                .orElse(null);
    }

    @Named("toSousTraitant")
    default TitulaireType toSousTraitant(AgrementSousTraitant acte) {
        return Optional.ofNullable(acte)
                .map((AgrementSousTraitant::getSousTraitant))
                .map(SousTraitant::getContact)
                .map(Contact::getEtablissement)
                .map(this::toTitulaireType)
                .orElse(null);
    }

    @Named("toStatutEchange")
    default String toStatutEchange(Acte acte) {
        return Optional.ofNullable(acte)
                .map(Acte::getEchangeChorus)
                .map(EchangeChorus::getStatut)
                .map(StatutChorus::getLabel)
                .orElse(null);
    }

    @Named("toStatutActe")
    default String toStatutActe(Acte acte) {
        return Optional.ofNullable(acte)
                .map((Acte::getStatut))
                .map((StatutActe::toString))
                .orElse(null);
    }

    @Named("toVariationPrix")
    default String toVariationPrix(AgrementSousTraitant acte, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(acte)
                .map((AgrementSousTraitant::getVariation))
                .map((Variation::toString))
                .map(code -> mappingPlaceOutput.get(VARIATION_PRIX) != null ? mappingPlaceOutput.get(VARIATION_PRIX).get(code) : null)
                .orElse(null);
    }

    @Named("toEtablissement")
    default String toEtablissement(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map((Etablissement::getId))
                .map((String::valueOf))
                .orElse(null);
    }

    @Named("toEntreprise")
    default String toEntreprise(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map((Etablissement::getFournisseur))
                .map((Fournisseur::getId))
                .map((String::valueOf))
                .orElse(null);
    }

    @Mapping(target = "identifiantTechnique", source = "etablissement.idExterne")
    @Mapping(target = "nom", source = "etablissement.fournisseur.nom")
    @Mapping(target = "prenom", source = "etablissement.fournisseur.prenom")
    @Mapping(target = "email", source = "etablissement.fournisseur.email")
    @Mapping(target = "identifiantNationalEntreprise", source = "etablissement.siret")
    @Mapping(target = "telephone", source = "etablissement.fournisseur.telephone")
    @Mapping(target = "siren", source = "etablissement.fournisseur.siren")
    @Mapping(target = "etablissement", source = "etablissement", qualifiedByName = "toEtablissement")
    @Mapping(target = "entreprise", source = "etablissement", qualifiedByName = "toEntreprise")
    @Mapping(target = "adresse", source = "etablissement", qualifiedByName = "toAdresse")
    @Mapping(target = "typeIdentifiantEntreprise", source = "etablissement", qualifiedByName = "toTypeIdentifiantEntreprise")
    @Mapping(target = "codeEtablissement", source = "etablissement", qualifiedByName = "siretToCodeEtablissement")
    TitulaireType toTitulaireType(Etablissement etablissement);

    @Mapping(target = "rue", source = "etablissement.adresse.adresse")
    @Mapping(target = "codePostal", source = "etablissement.adresse.codePostal")
    @Mapping(target = "ville", source = "etablissement.adresse.commune")
    @Mapping(target = "pays", source = "etablissement.adresse.pays")
    AdresseType toAdresseType(Etablissement etablissement);

    @Named("toTypeIdentifiantEntreprise")
    default String toTypeIdentifiantEntreprise(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map(Etablissement::getSiret)
                .map(e -> SIRET)
                .orElse(HORS_UE);
    }

    @Named("toAdresse")
    default AdresseType toAdresse(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map(this::toAdresseType)
                .orElse(null);
    }

    @Named("siretToCodeEtablissement")
    default String toCodeEtablissement(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map(Etablissement::getSiret)
                .map(e -> StringUtils.right(e, 5))
                .orElse(null);
    }

    @Named("toNouveauMontantContrat")
    default BigDecimal toNouveauMontantContrat(ActeModificatif acte) {
        return acte.getContrat().getMontant().add(BigDecimal.valueOf(acte.getMontantHTChiffre()));
    }

    @Named("toTitulaire")
    default TitulaireType toTitulaire(Acte acte) {
        return Optional.ofNullable(acte)
                .map((Acte::getTitulaire))
                .map(Contact::getEtablissement)
                .map(this::toTitulaireType)
                .orElse(null);
    }

    @Mapping(target = "value", source = "contrat.organismesEligibles", qualifiedByName = "toValue")
    @Mapping(target = "entiteEligible", source = "contrat.organismesEligibles", qualifiedByName = "toEntiteEligibleList")
    @Named("toContratTransverse")
    ContratTransverse toContratTransverse(Contrat contrat);

    @Mapping(target = "id", source = "organisme.idMpe")
    @Mapping(target = "acronyme", source = "organisme.acronyme")
    @Mapping(target = "denomination", source = "organisme.nom")
    EntiteEligible toEntiteEligible(Organisme organisme);

    @Named("toEntiteEligibleList")
    default List<EntiteEligible> toEntiteEligibleList(Set<Organisme> organismesEligibles) {
        var resultat = Optional.ofNullable(organismesEligibles)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toEntiteEligible)
                .collect(Collectors.toList());
        return !resultat.isEmpty() ? resultat : null;
    }

    @Named("toValue")
    default Boolean toValue(Set<Organisme> organismesEligibles) {
        return organismesEligibles != null && !organismesEligibles.isEmpty();
    }



    default String calculStatut(Contrat contrat, Set<Integer> statuts, Map<String, Map<String, String>> mappingPlaceOutput) {
        final Map<String, String> statutMapping = mappingPlaceOutput.get(STATUT_CONTRAT);

        if (contrat.isChapeau() && (CollectionUtils.isEmpty(statuts) || statuts.contains(0))) {
            return statutMapping != null ? statutMapping.getOrDefault(CHAPEAU, null) : null;
        }

        if (StatutContrat.ANotifier.equals(contrat.getStatut())) {
            var modalitesExecution = getModalitesExecution(contrat);
            return modalitesExecution.isEmpty() ? STATUT_DONNEES_CONTRAT_A_SAISIR : STATUT_NOTIFICATION_CONTRAT;
        }

        return (contrat.getStatut() != null && statutMapping != null) ?
                statutMapping.getOrDefault(contrat.getStatut().name(), null) :
                null;
    }


    default String calculDateByStatutContrat(String fieldName, Contrat contrat, Map<String, Map<String, String>> mappingPlaceOutput, Set<Integer> statuts) {
        if (contrat == null) {
            return null;
        }
        String resultDate = null;
        var statutContrat = toStatut(contrat, mappingPlaceOutput, statuts);
        if (STATUT_NOTIFICATION_CONTRAT_EFFECTUEE.equals(statutContrat)) {
            switch (fieldName) {
                case DATE_NOTIFICATION:
                    resultDate = MapperUtils.localDateToZonedDate(contrat.getDateNotification());
                    break;
                case DATE_FIN:
                    resultDate = MapperUtils.localDateToZonedDate(contrat.getDateFinContrat());
                    break;
                case DATE_MAX_FIN:
                    resultDate = MapperUtils.localDateToZonedDate(contrat.getDateMaxFinContrat());
                    break;
                default:
                    break;
            }
        }
        return resultDate;
    }

    default Set<ModaliteExecution> getModalitesExecution(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getModaliteExecutions)
                .orElse(Collections.emptySet());
    }


}
