package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.common.mapper.qualifier.WithoutEvenements;
import com.atexo.execution.server.model.Evenement;
import org.mapstruct.DecoratedWith;
import org.mapstruct.MappingTarget;

@DecoratedWith(EvenementMapperDecorator.class)
public interface EvenementMapper {

	EvenementDTO toDTO(Evenement evenement);

	Evenement createEntity(EvenementDTO evenement);

    Evenement updateEntity(EvenementDTO evenementDTO, @MappingTarget Evenement evenement);

	@WithoutEvenements
	EvenementDTO toDTOWithoutDocument(Evenement evenement);

}
