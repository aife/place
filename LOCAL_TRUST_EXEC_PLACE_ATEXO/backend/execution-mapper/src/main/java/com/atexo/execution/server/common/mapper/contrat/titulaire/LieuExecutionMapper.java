package com.atexo.execution.server.common.mapper.contrat.titulaire;

import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface LieuExecutionMapper {

    String CODE_DEPARTEMENT = "Code département";
    String CODE_PAYS = "Code pays";

    @Mapping(target = "code", source = "code")
    @Mapping(target = "nom", source = "label")
    @Mapping(target = "typeCode", source = "lieuExecution", qualifiedByName = "toTypeCode")
    com.atexo.execution.common.mpe.model.contrat.titulaire.LieuExecution toLieuExecutionContratTitulaire(com.atexo.execution.server.model.LieuExecution lieuExecution);

    default List<com.atexo.execution.common.mpe.model.contrat.titulaire.LieuExecution> toLieuExecutions(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getLieuxExecution)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toLieuExecutionContratTitulaire)
                .collect(Collectors.toList());
    }

    @Named("toTypeCode")
    default String toTypeCode(com.atexo.execution.server.model.LieuExecution lieuExecution) {
        return CODE_DEPARTEMENT;
    }
}
