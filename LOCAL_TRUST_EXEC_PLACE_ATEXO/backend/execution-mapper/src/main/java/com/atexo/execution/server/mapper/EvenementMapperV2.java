package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.EvenementCriteriaDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.repository.paginable.criteres.EvenementCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface EvenementMapperV2 {

    EvenementCriteria toCriteria(EvenementCriteriaDTO criteriaDTO);

    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "contractant", ignore = true)
    @Mapping(target = "destinataires", ignore = true)
    @Mapping(target = "destinatairesLibre", ignore = true)
    @Mapping(target = "nouveauContractant", ignore = true)
    @Mapping(target = "bonCommandeTypeDTO", source = "bonCommandeType")
    @Mapping(target = "avenantType", source = "avenantType")
    EvenementDTO toDTO(Evenement evenement);
}
