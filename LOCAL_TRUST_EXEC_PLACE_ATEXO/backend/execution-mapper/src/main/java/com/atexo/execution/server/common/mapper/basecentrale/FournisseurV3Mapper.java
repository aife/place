package com.atexo.execution.server.common.mapper.basecentrale;

import com.atexo.execution.server.entreprise.v3.model.V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneUnitesLegalesSirenGet200ResponseData;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.CategorieFournisseur;
import com.atexo.execution.server.model.Fournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface FournisseurV3Mapper {

    @Mapping(target = "formeJuridique", source = "fournisseur.formeJuridique.libelle")
    @Mapping(target = "raisonSociale", source = "fournisseur.personneMoraleAttributs.raisonSociale")
    @Mapping(target = "capitalSocial", source = "extraitKbis.capital.montant")
    @Mapping(target = "siren", source = "fournisseur.siren")
    @Mapping(target = "nom", source = "fournisseur.personnePhysiqueAttributs.nomUsage")
    @Mapping(target = "prenom", source = "fournisseur.personnePhysiqueAttributs.prenomUsuel")
    @Mapping(target = "categorie", expression = "java(toCategorie(fournisseur,list))")
    @Mapping(target = "dateCreation", ignore = true)
    Fournisseur toFournisseur(V3InseeSireneUnitesLegalesSirenGet200ResponseData fournisseur, V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData extraitKbis, List<CategorieFournisseur> list);

    default CategorieFournisseur toCategorie(V3InseeSireneUnitesLegalesSirenGet200ResponseData fournisseur, List<CategorieFournisseur> categorieFournisseurList) {
        return Optional.ofNullable(fournisseur)
                .map(V3InseeSireneUnitesLegalesSirenGet200ResponseData::getCategorieEntreprise)
                .map(V3InseeSireneUnitesLegalesSirenGet200ResponseData.CategorieEntrepriseEnum::getValue)
                .flatMap(value -> categorieFournisseurList.stream()
                        .filter(e -> e.getCode().equals(value))
                        .findFirst())
                .orElse(null);
    }

}
