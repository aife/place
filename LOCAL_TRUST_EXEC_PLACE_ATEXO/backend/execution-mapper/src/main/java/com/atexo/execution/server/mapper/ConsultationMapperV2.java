package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.ConsultationDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.mpe.Consultation;
import com.atexo.execution.common.mpe.ws.api.ConsultationType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ContratMapper.class, ReferentielMapper.class, EtablissementMapperV2.class})
public interface ConsultationMapperV2 {

    @Mapping(target = "organisme", source = "contrat.service.organisme.acronyme")
    @Mapping(target = "directionService", source = "contrat.service.idExterne")
    @Mapping(target = "reference", source = "contrat.referenceLibre")
    @Mapping(target = "naturePrestation", source = "contrat.categorie.value")
    @Mapping(target = "typeProcedure", source = "idTypeProcedure")
    @Mapping(target = "typeContrat", source = "idTypeContrat")
    @Mapping(target = "lieuxExecution", expression = "java(toLieuExecution(contrat))")
    @Mapping(target = "codeCpvPrincipal", expression = "java(toCodeCpv(contrat,0))")
    @Mapping(target = "codeCpvSecondaire1", expression = "java(toCodeCpv(contrat,1))")
    @Mapping(target = "codeCpvSecondaire2", expression = "java(toCodeCpv(contrat,2))")
    @Mapping(target = "codeCpvSecondaire3", expression = "java(toCodeCpv(contrat,3))")
    @Mapping(target = "commentaireInterne", constant = "marché subsequent depuis exec")
    @Mapping(target = "valeurEstimee", source = "contrat.montant")
    @Mapping(target = "alloti", constant = "false")
    @Mapping(target = "referenceConsultationInit", source = "contrat.consultation.numero")
    Consultation toWS(ContratDTO contrat, String idTypeProcedure, String idTypeContrat);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "idExterne", source = "id")
    @Mapping(target = "horsPassation", ignore = true)
    @Mapping(target = "categorie", ignore = true)
    @Mapping(target = "numero", source = "reference")
    @Mapping(target = "dateModificationExterne", source = "dateModification")
    @Mapping(target = "procedure", ignore = true)
    @Mapping(target = "typeGroupementOperateurs", ignore = true)
    @Mapping(target = "typesPrix", ignore = true)
    com.atexo.execution.server.model.Consultation toConsultation(ConsultationType consultationType);


    @Mapping(target = "id", source = "id")
    com.atexo.execution.server.model.Consultation fromDTOtoConsultation(ConsultationDTO consultationDTO);

    ConsultationDTO toConsultationDTO(com.atexo.execution.server.model.Consultation consultation);

    default Integer getIdUtilisateur(Contrat contrat) {
        return Optional.ofNullable(contrat.getCreateur()).map(Utilisateur::getIdExterne).map(this::getId).orElse(null);
    }

    default String getIdService(Contrat contrat) {
        return Optional.ofNullable(contrat.getService()).map(Service::getIdExterne).map(this::getId).map(id -> "/api/v2/referentiels/services/" + id).orElse(null);
    }

    default String getIdOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat.getService()).map(Service::getOrganisme).map(Organisme::getAcronyme).map(acronyme -> "/api/v2/referentiels/organismes/" + acronyme).orElse(null);
    }

    default Integer getId(String idExterne) {
        return Optional.ofNullable(idExterne).map(id -> id.substring(id.indexOf("_") + 1)).map(Integer::parseInt).orElse(null);
    }

    default XMLGregorianCalendar now() {
        var c = new GregorianCalendar();
        c.setTime(new Date());
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    default List<Integer> toLieuExecution(Contrat contrat) {
        var le = new ArrayList<Integer>();
        if (contrat.getLieuxExecution() != null && !contrat.getLieuxExecution().isEmpty()) {
            for (var l : contrat.getLieuxExecution()) {
                try {
                    le.add(Integer.parseInt(l.getCode()));
                } catch (Exception e) {

                }
            }
        }
        return le;

    }

    default List<Integer> toLieuExecution(ContratDTO contrat) {
        return Optional.ofNullable(contrat.getLieuxExecution())
                .stream()
                .flatMap(Collection::stream)
                .map(ValueLabelDTO::getValue)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    default String toCodeCpv(ContratDTO contrat, int index) {
        var codesCpv = contrat.getCodesCPV();
        if (codesCpv == null || codesCpv.isEmpty()) {
            return null;
        }
        var list = new ArrayList<>(codesCpv);
        if (index >= list.size()) {
            return null;
        }
        return Optional.ofNullable(list.get(index)).map(ValueLabelDTO::getValue).orElse(null);
    }

}
