package com.atexo.execution.server.common.mapper.contrat.titulaire;

import com.atexo.execution.common.mpe.model.contrat.titulaire.Acheteur;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AcheteurMapper {

    @Mapping(target = "id", source = "contrat.service.organisme.siret")
    @Mapping(target = "nom", source = "contrat.service.nom")
    @Mapping(target = "idAgent", source = "contrat")
    @Mapping(target = "nomAgent", source = "contrat.createur.nom")
    @Mapping(target = "prenomAgent", source = "contrat.createur.prenom")
    @Mapping(target = "emailAgent", source = "contrat.createur.email")
    @Mapping(target = "organisme", source = "contrat", qualifiedByName = "toAcronyme")
    @Mapping(target = "libelleOrganisme", source = "service.organisme.nom")
    @Mapping(target = "idOrganisme", source = "contrat", qualifiedByName = "toIdOrganisme")
    @Mapping(target = "idEntiteAchat", source = "contrat", qualifiedByName = "toIdEntiteAchat")
    @Mapping(target = "oldIdEntiteAchat", source = "contrat.service.organisme.idExterne", qualifiedByName = "toOldIdEntiteAchat")
    @Mapping(target = "libelleEntiteAchat", source = "contrat.service.nom")
    @Mapping(target = "accesChorus", source = "contrat.service.echangesChorus", qualifiedByName = "toAccesChorus")
    Acheteur toAcheteur(Contrat contrat);

    default Integer toIdAgent(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getIdExterneCreateur)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toOldIdEntiteAchat")
    default String toOldIdEntiteAchat(String idExterne) {
        return Optional.ofNullable(idExterne)
                .map(BeanSynchroUtils::extractId)
                .orElse(null);
    }

    @Named("toAccesChorus")
    default Integer toAccesChorus(boolean accesChorus) {
        return accesChorus ? 1 : 0;
    }

    @Named("toIdOrganisme")
    default Integer toIdOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toAcronyme")
    default String toAcronyme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getAcronyme)
                .map(String::toLowerCase)
                .orElse(null);
    }

    @Named("toIdEntiteAchat")
    default Integer toIdEntiteAchat(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOldIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }
}
