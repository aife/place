package com.atexo.execution.server.common.mapper.actes.impl;

import com.atexo.execution.common.dto.actes.ActeModificatifDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.common.mapper.ChorusMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.common.mapper.actes.ActeModificatifMapper;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.DonneesEssentiellesMapper;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.mapper.RejetActeMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class ActeModificatifMapperImpl extends BaseActeMapper implements ActeModificatifMapper {

    private static final String SANS_INCIDENCE = "SANS_INCIDENCE";

    @Autowired
    private EtablissementRepository etablissementRepository;

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private TypeActeRepository typeActeRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private EtablissementMapper etablissementMapper;

    @Autowired
    private UtilisateurMapper utilisateurMapper;

    @Autowired
    private ReferentielMapper referentielMapper;

    @Autowired
    private DonneesEssentiellesMapper donneesEssentiellesMapper;

    @Autowired
    private EchangeChorusMapper echangeChorusMapper;

    @Autowired
    private ContratMapper contratMapper;

    @Autowired
    private RejetActeMapper rejetActeMapper;

    @Autowired
    private ChorusMapper chorusMapper;

    @Autowired
    private ActeRepository acteRepository;

    @Override
    public ActeModificatifDTO toDTO(ActeModificatif acte) {
        if (null == acte) {
            return null;
        }

        // Commun
        var acteDTO = new ActeModificatifDTO();
        acteDTO.setId(acte.getId());
        acteDTO.setUuid(acte.getUuid());
        acteDTO.setVersion(acte.getVersion());
        acteDTO.setObjet(acte.getObjet());
        acteDTO.setStatut(acte.getStatut());
        acteDTO.setNumero(acte.getNumero());
        acteDTO.setAcheteur(utilisateurMapper.toDTO(acte.getAcheteur()));
        acteDTO.setTitulaire(map(acte.getTitulaire()));
        acteDTO.setDateCreation(acte.getDateCreation());
        acteDTO.setDateModification(acte.getDateModification());
        acteDTO.setCommentaire(acte.getCommentaire());
        acteDTO.setEchangeChorus(echangeChorusMapper.toDto(acte.getEchangeChorus()));
        acteDTO.setDateNotification(MapperUtils.convert(acte.getDateNotification()));
        acteDTO.setDocuments(documentListToDocumentDTOList(acte.getDocuments()));
        acteDTO.setDateCreation(acte.getDateCreation());
        acteDTO.setDonneesEssentielles(donneesEssentiellesMapper.toDto(acte.getDonneesEssentielles()));
        acteDTO.setContrat(contratMapper.toDto(acte.getContrat()));
        acteDTO.setTypeNotification(acte.getTypeNotification());
        acteDTO.setValidationChorus(acte.isValidationChorus());
        acteDTO.setType(this.referentielMapper.toDTO(acte.getTypeActe()));
        acteDTO.setClassname(ActeModificatifDTO.class.getCanonicalName());

        // Specifique
        acteDTO.setDureeAjoureeContrat(acte.getDureeAjoureeContrat());
        acteDTO.setNouvelleFinContrat(acte.getNouvelleFinContrat());
        acteDTO.setPublicationDonneesEssentielles(acte.getPublicationDonneesEssentielles());
        acteDTO.setClauseReexamen(acte.getClauseReexamen());
        acteDTO.setTypeModification(acte.getTypeModification());
        acteDTO.setMontantHTChiffre(acte.getMontantHTChiffre());
        acteDTO.setMontantTTCChiffre(acte.getMontantTTCChiffre());
        acteDTO.setTauxTVA(acte.getTauxTVA());

        if (null != acte.getRejet()) {
            acteDTO.setRejet(rejetActeMapper.toDto(acte.getRejet()));
        }

        acteDTO.setChorus(chorusMapper.toDTO(acte.getChorus()));

        return acteDTO;
    }


    @Override
    public ActeModificatif createEntity(ActeModificatifDTO acteDTO) {
        if (null == acteDTO) {
            return null;
        }

        var acte = new ActeModificatif();

        // Commun
        if (null != acteDTO.getVersion()) {
            acte.setVersion(acteDTO.getVersion());
        }

        acte.setId(acteDTO.getId());
        acte.setUuid(acteDTO.getUuid());
        acte.setObjet(acteDTO.getObjet());
        acte.setStatut(acteDTO.getStatut());
        acte.setNumero(acteDTO.getNumero());


        utilisateurRepository.findByUuid(acteDTO.getAcheteur().getUuid()).ifPresent(
                acheteur -> acte.setAcheteur(acheteur)
        );

        acte.setTitulaire(map(acteDTO.getTitulaire()));
        acte.setCommentaire(acteDTO.getCommentaire());
        contratRepository.findById(acteDTO.getContrat().getId()).ifPresent(acte::setContrat);
        acte.setPlateforme(acte.getContrat().getPlateforme());
        acte.setStatut(acteDTO.getStatut());
        acte.getDocuments().forEach(
                documentActe -> documentActe.setActe(acte)
        );

        acte.setDateNotification(MapperUtils.convert(acteDTO.getDateNotification()));
        acte.setEchangeChorus(echangeChorusMapper.createEntity(acteDTO.getEchangeChorus()));
        acte.setTypeNotification(acteDTO.getTypeNotification());
        acte.setValidationChorus(acteDTO.isValidationChorus());

        // Specifique
        acte.setDureeAjoureeContrat(acteDTO.getDureeAjoureeContrat());
        acte.setNouvelleFinContrat(acteDTO.getNouvelleFinContrat());
        acte.setPublicationDonneesEssentielles(acteDTO.getPublicationDonneesEssentielles());
        acte.setClauseReexamen(acteDTO.getClauseReexamen());
        acte.setTypeModification(acteDTO.getTypeModification());
        acte.setMontantHTChiffre(acteDTO.getMontantHTChiffre());
        acte.setMontantTTCChiffre(acteDTO.getMontantTTCChiffre());
        acte.setTauxTVA(acteDTO.getTauxTVA());

        if (null != acteDTO.getRejet()) {
            acte.setRejet(rejetActeMapper.createEntity(acteDTO.getRejet()));
        }

        acte.setChorus(chorusMapper.createEntity(acteDTO.getChorus()));

        return acte;
    }

    @Override
    public ActeType toWS(ActeModificatif acte, EchangeChorus echange) {
        var result = new ActeType();

        String type;

        if (SANS_INCIDENCE.equalsIgnoreCase(acte.getTypeModification())) {
            type = toWS(TypeActeMPE.AVENANT_SANS_INCIDENCE_FINANCIERE);
        } else {
            if (acte.getMontantTTCChiffre() > 0) {
                type = toWS(TypeActeMPE.AVENANT_AUGMENTANT_MONTANT);
            } else {
                type = toWS(TypeActeMPE.AVENANT_DIMINUANT_MONTANT);
            }
        }

        // commun
        result.setType(type);
        result.setObjet(acte.getObjet());
        result.setNumero(acte.getNumero());
        result.setAcheteur(acte.getAcheteur().getIdentifiant());
        result.setStatut(acte.getStatut().name());
        if (null != acte.getTitulaire()) {
            result.setTitulaire(toWS(acte.getTitulaire().getEtablissement()));
        }
        result.setCommentaire(acte.getCommentaire());
        result.setDateNotification(MapperUtils.toXmlGregorianCalendar(acte.getDateNotification()));
        result.setDateNotificationPrevisionnelle(MapperUtils.toXmlGregorianCalendar(echange.getDateNotificationPrevisionnelle()));

        if (null != acte.getChorus()) {
            result.setVisaACCF(acte.getChorus().getVisaACCF());
            result.setVisaPrefet(acte.getChorus().getVisaPrefet());
        }

        result.setTypeFournisseurEntreprise(getCodeFournisseur(acte));

        result.setDateCreation(MapperUtils.toXmlGregorianCalendar(acte.getDateCreation()));
        result.setDateModification(MapperUtils.toXmlGregorianCalendar(acte.getDateModification()));
        result.setDateFinMarche(MapperUtils.toXmlGregorianCalendar(acte.getContrat().getDateFinContrat()));
        // acte modificatif
        result.setDureeAjoureeContrat(acte.getDureeAjoureeContrat());
        result.setNouvelleFinContrat(MapperUtils.toXmlGregorianCalendar(acte.getNouvelleFinContrat()));
        result.setPublicationDonneesEssentielles(acte.getPublicationDonneesEssentielles());
        result.setClauseReexamen(acte.getClauseReexamen());
        result.setTypeModification(acte.getTypeModification());
        result.setMontantHTChiffre(acte.getMontantHTChiffre());
        result.setMontantTTCChiffre(acte.getMontantTTCChiffre());
        result.setTauxTVA(acte.getTauxTVA());

        return result;
    }
}
