package com.atexo.execution.server.common.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;

public abstract class DateTimeMapper {

	public LocalDateTime offsetDateTimetoLocalDateTime(OffsetDateTime offsetDateTime) {
		if (offsetDateTime == null) {
			return null;
		}
		return offsetDateTime.atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
	}

	public LocalDate offsetDateTimetoLocalDate(OffsetDateTime offsetDateTime) {
		if (offsetDateTime == null) {
			return null;
		}
		return offsetDateTime.atZoneSameInstant(ZoneId.systemDefault()).toLocalDate();
	}

	public OffsetDateTime localDateTimetoOffsetDateTime(LocalDateTime localDateTime) {
		if (localDateTime == null) {
			return null;
		}
		return localDateTime.atZone(ZoneId.systemDefault()).toOffsetDateTime();
	}

	public OffsetDateTime localDatetoOffsetDateTime(LocalDate localDate) {
		if (localDate == null) {
			return null;
		}
		return localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toOffsetDateTime();
	}

	public LocalDate dateToLocalDate(Date date) {
		if (date == null) {
			return null;
		}
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate();
	}

}
