package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import org.mapstruct.Mapping;
import org.springframework.core.GenericTypeResolver;

import java.util.stream.Stream;

public interface ActeMappable<T extends Acte, U extends ActeDTO> {

	/**
	 * Le mapper accepte-t-il le type d'acte?
	 * @param type le type
	 * @return vrai si le type d'acte est accepté
	 */
	default boolean accept( Class<?> type ) {
		var generics = GenericTypeResolver.resolveTypeArguments(this.getClass(), ActeMappable.class);

		boolean result = false;

		if (null != generics && generics.length == 2) {
			var typeEntite = generics[0];
			var typeView = generics[1];

			result = Stream.of(typeEntite, typeView).anyMatch(type::isAssignableFrom);
		}

		return result;
	}

	/**
	 * Permet de mapper une entité en DTO
	 * @param acte l'entité
	 * @return le DTO
	 */
	U toDTO( T acte);

	/**
	 * Permet de mapper un DTO en entité
	 * @param acteDTO le DTO
	 * @return l'entité
	 */
	T createEntity(U acteDTO);

	/**
	 * Permet de mapper un acte métier en MPE
	 *
	 * @param acte    l'acte
	 * @param echange l'échange chorus
	 * @return l'entité
	 */

	@Mapping(target = "idExterne", ignore = true)
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "dateCreation", ignore = true)
	@Mapping(target = "dateModification", ignore = true)
	@Mapping(target = "reference", ignore = true)
	ActeType toWS(T acte, EchangeChorus echange);
}
