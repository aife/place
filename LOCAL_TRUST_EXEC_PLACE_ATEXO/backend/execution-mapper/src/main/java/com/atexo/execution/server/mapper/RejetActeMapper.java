package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.dto.RejetActeDTO;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.RejetActe;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface RejetActeMapper {

    RejetActeDTO toDto( RejetActe rejetActe);

    RejetActe createEntity(RejetActeDTO rejetActeDTO);

}
