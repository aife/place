package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.exec.mpe.ws.AdresseType;
import com.atexo.execution.common.exec.mpe.ws.ContactType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Adresse;
import com.atexo.execution.server.model.Contact;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, EtablissementMapper.class})
public interface ContactMapper {

    @Mapping(target = "etablissement", source = "etablissement", qualifiedByName = "toDTO")
    @Mapping(target = "contactReferantDTOS", ignore = true)
	ContactDTO toDTO(Contact contact);

    @Mapping(target = "etablissement", source = "etablissement")
    Contact createEntity(ContactDTO contactDTO);

    @Mapping(target = "raisonSociale", source = "etablissement.fournisseur.raisonSociale")
    @Mapping(target = "adresse", source = "etablissement.adresse")
    ContactType toContactType(Contact contact);

    AdresseType toAdresse(Adresse adresse);
}
