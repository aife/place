package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.def.ReferentielEnum;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.AbstractReferentielEntity;
import com.atexo.execution.server.model.CategorieConsultation;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface ReferentielMapper {

    @Mapping(target = "value", source = "code")
    @Mapping(target = "parentLabel", source = "groupe.label")
    @Mapping(target = "groupeCode", source = "groupe.code")
    @Mapping(target = "parents", source="parents")
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ordre", source = "ordre")
    @Mapping(target = "modeEchangeChorus", source = "referentielEntity", qualifiedByName = "toModeEchangeChorus")
    ValueLabelDTO toDTO(AbstractReferentielEntity referentielEntity);


    List<ValueLabelDTO> toDTO(Set<? extends AbstractReferentielEntity> referentielEntities);

    @Mapping(target = "code", source = "value")
    TypeContrat toTypeContrat(ValueLabelDTO valueLabelDTO);

    @Mapping(target = "code", source = "value")
    @Named("toCategorie")
    CategorieConsultation toCategorie(ValueLabelDTO valueLabelDTO);

    default List<CategorieConsultation> toCategories(List<ValueLabelDTO> valueLabelDTO) {
        if(valueLabelDTO == null) return List.of();
        return valueLabelDTO.stream().map(this::toCategorie).collect(Collectors.toList());
    }

    default ValueLabelDTO toDTO(ReferentielEnum referentielEnum) {
        if (referentielEnum == null) {
            return null;
        }
        ValueLabelDTO valueLabelDTO = new ValueLabelDTO();
        valueLabelDTO.setValue(referentielEnum.getCode());
        valueLabelDTO.setLabel(referentielEnum.getLabelKey());
        return valueLabelDTO;
    }

    default ValueLabelDTO toDTO(Utilisateur utilisateur) {
        var nomService = utilisateur.getService() == null ? null : utilisateur.getService().getNom();
        var label = String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom());
        return ValueLabelDTO.builder().value(utilisateur.getEmail()).label(label).parentLabel(nomService).idExterne(utilisateur.getIdExterne()).build();
    }

    default ValueLabelDTO toDTO(String str) {
        return ValueLabelDTO.builder()
                .value(str)
                .label(str)
                .idExterne(str).build();
    }

    default String map(ValueLabelDTO value) {
        if (value == null) {
            return null;
        }
        return value.getLabel();
    }

    @Named("toModeEchangeChorus")
    default Integer toModeEchangeChorus(AbstractReferentielEntity referentielEntity) {
        if (referentielEntity == null) {
            return null;
        }
        if (referentielEntity instanceof TypeContrat) {
            var typeContrat = (TypeContrat) referentielEntity;
            return typeContrat.getModeEchangeChorus();
        }
        return null;
    }
}
