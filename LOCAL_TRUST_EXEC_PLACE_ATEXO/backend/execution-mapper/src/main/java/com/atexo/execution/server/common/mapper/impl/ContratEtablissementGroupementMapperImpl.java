package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;
import com.atexo.execution.server.common.mapper.ContratEtablissementGroupementMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.model.ContratEtablissementGroupement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class ContratEtablissementGroupementMapperImpl implements ContratEtablissementGroupementMapper {

    @Autowired
    private ReferentielMapper referentielMapper;

    @Override
    public ContratEtablissementGroupementDTO toDTO(ContratEtablissementGroupement contratEtablissementGroupement) {
        if (contratEtablissementGroupement == null) {
            return null;
        }

        ContratEtablissementGroupementDTO contratEtablissementGroupementDTO = new ContratEtablissementGroupementDTO();

        contratEtablissementGroupementDTO.setId(contratEtablissementGroupement.getId());
        contratEtablissementGroupementDTO.setNom(contratEtablissementGroupement.getNom());
        contratEtablissementGroupementDTO.setType(referentielMapper.toDTO(contratEtablissementGroupement.getType()));

        return contratEtablissementGroupementDTO;
    }
}
