package com.atexo.execution.server.common.mapper.data.gouv;

import com.atexo.execution.common.data.gouv.model.marche.LieuExecution;
import com.atexo.execution.common.data.gouv.model.marche.*;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.actes.AgrementSousTraitant;
import org.apache.commons.lang3.EnumUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface DataGouvMarcheMapper {

    String TYPE_PROCEDURE = "type-procedure";
    String NATURE_CONTRAT = "nature-contrat";
    String CCAG_CONTRAT = "ccag-contrat";
    String TYPE_PRIX_CONTRAT = "type-prix-contrat";
    String VARIATION_PRIX_CONTRAT = "variation-prix-contrat";
    String TECHNIQUE_ACHAT = "technique-achat";
    String MODALITE_EXECUTION = "modalite-execution";
    String CONSIDERATION_SOCIALE = "consideration-sociale";
    String CONSIDERATION_ENVIRONNEMENTALE = "consideration-environnementale";
    String TYPE_GROUPEMENT = "type-groupement";

    @Mapping(target = "id", expression = "java(toId(contrat,sequence))")
    @Mapping(target = "type", expression = "java(toType(contrat))")
    @Mapping(target = "acheteur", source = "contrat.service")
    @Mapping(target = "nature", expression = "java(toNature(contrat,mapping.get(NATURE_CONTRAT)))")
    @Mapping(target = "objet", source = "contrat.objet")
    @Mapping(target = "codeCPV", expression = "java(toCodeCPV(contrat))")
    @Mapping(target = "techniques", expression = "java(toTechniques(contrat,mapping.get(TECHNIQUE_ACHAT)))")
    @Mapping(target = "modalitesExecution", expression = "java(toModalitesExecution(contrat,mapping.get(MODALITE_EXECUTION)))")
    @Mapping(target = "idAccordCadre", expression = "java(toIdAccordCadre(contrat))")
    @Mapping(target = "marcheInnovant", source = "contrat.marcheInnovant")
    @Mapping(target = "ccag", expression = "java(toCcag(contrat,mapping.get(CCAG_CONTRAT)))")
    @Mapping(target = "offresRecues", source = "contrat.offresRecues")
    @Mapping(target = "attributionAvance", source = "contrat.attributionAvance")
    @Mapping(target = "tauxAvance", expression = "java(toTauxAvance(contrat))")
    @Mapping(target = "typeGroupementOperateurs", expression = "java(toTypeGroupementOperateurs(contrat,mapping.get(TYPE_GROUPEMENT)))")
    @Mapping(target = "sousTraitanceDeclaree", expression = "java(toSousTraitanceDeclaree(contrat))")
    @Mapping(target = "actesSousTraitance", expression = "java(toActesSousTraitance(contrat,mapping.get(VARIATION_PRIX_CONTRAT)))")
    @Mapping(target = "procedure", expression = "java(toTypeProcedure(contrat, mapping.get(TYPE_PROCEDURE)))")
    @Mapping(target = "lieuExecution", expression = "java(toLieuExecution(contrat))")
    @Mapping(target = "dureeMois", source = "contrat.dureeMaximaleMarche")
    @Mapping(target = "dateNotification", source = "contrat.dateNotification")
    @Mapping(target = "datePublicationDonnees", expression = "java(toDatePublicationDonnees())", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "montant", source = "contrat.montant")
    @Mapping(target = "typesPrix", expression = "java(toTypesPrix(contrat,mapping.get(TYPE_PRIX_CONTRAT)))")
    @Mapping(target = "formePrix", expression = "java(toFormePrix(contrat))")
    @Mapping(target = "origineUE", source = "contrat.consultation.origineUE", qualifiedByName = "toPercent")
    @Mapping(target = "origineFrance", source = "contrat.consultation.origineFrance", qualifiedByName = "toPercent")
    @Mapping(target = "titulaires", expression = "java(toTitulairesList(contrat))")
    @Mapping(target = "considerationsSociales", expression = "java(toConsiderationsSociales(contrat,mapping.get(CONSIDERATION_SOCIALE)))")
    @Mapping(target = "considerationsEnvironnementales", expression = "java(toConsiderationsEnvironnementales(contrat,mapping.get(CONSIDERATION_ENVIRONNEMENTALE)))")
    @Mapping(target = "modificationsActesSousTraitance", expression = "java(toModificationsActesSousTraitance(contrat))")
    @Mapping(target = "modifications", expression = "java(toModifications(contrat))")
    Marche contratToDataGouvMarche(Contrat contrat, Map<String, Map<String, String>> mapping, String sequence);

    @Mapping(target = "id", source = "acteModificatif.id")
    @Mapping(target = "dateNotificationModification", source = "acteModificatif.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "datePublicationDonneesModification", source = "acteModificatif.contrat.donneesEssentielles.datePublication", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "dureeMois", source = "acteModificatif.contrat.dureeMaximaleMarche")
    @Mapping(target = "montant", source = "acteModificatif.montantHTChiffre")
    @Mapping(target = "titulaires", source = "acteModificatif.contrat")
    Modification toModification(ActeModificatif acteModificatif);


    @Mapping(target = "modification", source = "acteModificatif")
    ModificationArray toModifications(ActeModificatif acteModificatif);

    @Mapping(target = "id", source = "service.organisme.siret")
    Acheteur toAcheteur(Service service);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "typeCode", expression = "java(com.atexo.execution.common.data.gouv.model.marche.LieuExecution.TypeCode.CODE_DÉPARTEMENT)")
    LieuExecution toLieuExecutionMarche(com.atexo.execution.server.model.LieuExecution lieuExecution);

    @Mapping(target = "titulaire", source = "attributaire")
    TitulaireArray toTitulaires(ContratEtablissement attributaire);

    @Mapping(target = "id", source = "etablissement.siret")
    @Mapping(target = "typeIdentifiant", expression = "java(com.atexo.execution.common.data.gouv.model.marche.Titulaire.TypeIdentifiant.SIRET)")
    Titulaire toTitulaire(ContratEtablissement attributaire);


    @Mapping(target = "modificationActeSousTraitance", source = "agrementSousTraitant")
    ModificationActesSousTraitanceArray toModificationActesSousTraitance(AgrementSousTraitant agrementSousTraitant);

    @Mapping(target = "id", source = "agrementSousTraitant.id")
    @Mapping(target = "dureeMois", source = "agrementSousTraitant.duree")
    @Mapping(target = "dateNotificationModificationSousTraitance", source = "agrementSousTraitant.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "montant", source = "agrementSousTraitant.montantHT")
    @Mapping(target = "datePublicationDonnees", source = "agrementSousTraitant.datePublication", dateFormat = "yyyy-MM-dd")
    ModificationActeSousTraitance toModificationActeSousTraitance(AgrementSousTraitant agrementSousTraitant);

    @Mapping(target = "acteSousTraitance", expression = "java(toActeSousTraitance(sousTraitant,map))")
    ActeSousTraitanceArray toActeSousTraitanceArray(ContratEtablissement sousTraitant, Map<String, String> map);

    @Mapping(target = "id", source = "sousTraitant.id")
    @Mapping(target = "dureeMois", source = "sousTraitant.dureeMois")
    @Mapping(target = "dateNotification", source = "sousTraitant.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "montant", source = "sousTraitant.montant")
    @Mapping(target = "variationPrix", expression = "java(toVariationPrix(sousTraitant,map))")
    @Mapping(target = "datePublicationDonnees", source = "sousTraitant.contrat.donneesEssentielles.datePublication", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "sousTraitant.id", source = "sousTraitant.etablissement.siret")
    @Mapping(target = "sousTraitant.typeIdentifiant", expression = "java(SousTraitant.TypeIdentifiant.SIRET)")
    ActeSousTraitance toActeSousTraitance(ContratEtablissement sousTraitant, Map<String, String> map);

    @Mapping(target = "technique", expression = "java(toTechnique(contrat,mapTechniques))")
    Techniques toTechniques(Contrat contrat, Map<String, String> mapTechniques);

    @Mapping(target = "modaliteExecution", expression = "java(toModaliteExecution(contrat,mapModaliteExecution))")
    ModalitesExecution toModalitesExecution(Contrat contrat, Map<String, String> mapModaliteExecution);

    @Mapping(target = "typePrix", expression = "java(toTypePrixArray(contrat,mapTypePrix))")
    TypesPrix toTypesPrix(Contrat contrat, Map<String, String> mapTypePrix);

    @Mapping(target = "considerationSociale", expression = "java(toConsiderationSociale(contrat,mapConsiderationsSociales))")
    ConsiderationsSociales toConsiderationsSociales(Contrat contrat, Map<String, String> mapConsiderationsSociales);

    @Mapping(target = "considerationEnvironnementale", expression = "java(toConsiderationEnvironnementale(contrat,mapConsiderationsSociales))")
    ConsiderationsEnvironnementales toConsiderationsEnvironnementales(Contrat contrat, Map<String, String> mapConsiderationsSociales);


    default String toType(Contrat contrat) {
        return Optional.ofNullable(contrat.getType()).map(TypeContrat::getLabel).orElse(null);
    }

    default Marche.Nature toNature(Contrat contrat, Map<String, String> natureContrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(TypeContrat::getCode)
                .map(natureContrat::get)
                .filter(code -> EnumUtils.isValidEnum(Marche.Nature.class, code))
                .map(Marche.Nature::valueOf)
                .orElse(null);
    }

    default String toCodeCPV(Contrat contrat) {
        return Optional.ofNullable(contrat.getCodesCpv())
                .orElse(Collections.emptySet())
                .stream()
                .map(AbstractReferentielEntity::getCode).findFirst().orElse(null);
    }

    default Marche.Procedure toTypeProcedure(Contrat contrat, Map<String, String> typeProcedure) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getProcedure)
                .map(Procedure::getCode)
                .map(typeProcedure::get)
                .filter(code -> EnumUtils.isValidEnum(Marche.Procedure.class, code))
                .map(Marche.Procedure::valueOf).orElse(null);
    }

    default String toIdAccordCadre(Contrat contrat) {
        return Optional.ofNullable(contrat).map(Contrat::getContratChapeau).map(Contrat::getId).map(String::valueOf).orElse(null);
    }

    default Marche.Ccag toCcag(Contrat contrat, Map<String, String> ccagContrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getCcagApplicable)
                .map(CCAGReference::getCode)
                .map(ccagContrat::get)
                .filter(code -> EnumUtils.isValidEnum(Marche.Ccag.class, code))
                .map(Marche.Ccag::valueOf)
                .orElse(null);
    }

    default boolean toSousTraitanceDeclaree(Contrat contrat) {
        return Optional.ofNullable(contrat.getContratEtablissements()).orElse(Collections.emptySet()).stream().anyMatch(contratEtablissement -> contratEtablissement.getCommanditaire() != null);
    }


    default LieuExecution toLieuExecution(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getLieuxExecution)
                .stream().flatMap(Collection::stream)
                .findFirst()
                .map(this::toLieuExecutionMarche).orElse(null);
    }

    default Set<ConsiderationSocialeArray> toConsiderationSociale(Contrat contrat, Map<String, String> mapConsiderationSocaile) {
        var listClause = Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .map(e -> mapConsiderationSocaile.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .map(ConsiderationSocialeArray::valueOf)
                .collect(Collectors.toSet());
        return listClause.isEmpty() ? Collections.singleton(ConsiderationSocialeArray.PAS_DE_CONSIDÉRATION_SOCIALE) : listClause;
    }

    default Set<ConsiderationEnvironnementaleArray> toConsiderationEnvironnementale(Contrat contrat, Map<String, String> mapConsiderationEnvironnementale) {
        var listClause = Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .map(e -> mapConsiderationEnvironnementale.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .map(ConsiderationEnvironnementaleArray::valueOf)
                .collect(Collectors.toSet());
        return listClause.isEmpty() ? Collections.singleton(ConsiderationEnvironnementaleArray.PAS_DE_CONSIDÉRATION_ENVIRONNEMENTALE) : listClause;
    }

    default Set<TechniqueArray> toTechnique(Contrat contrat, Map<String, String> techniqueAchat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTechniqueAchats)
                .orElse(Collections.emptySet())
                .stream()
                .map(TechniqueAchat::getCode)
                .map(techniqueAchat::get)
                .filter(code -> EnumUtils.isValidEnum(TechniqueArray.class, code))
                .map(TechniqueArray::valueOf)
                .collect(Collectors.toSet());
    }

    default Set<ModaliteExecutionArray> toModaliteExecution(Contrat contrat, Map<String, String> mapModaliteExecution) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getModaliteExecutions)
                .orElse(Collections.emptySet())
                .stream()
                .map(com.atexo.execution.server.model.ModaliteExecution::getCode)
                .map(mapModaliteExecution::get)
                .filter(code -> EnumUtils.isValidEnum(ModaliteExecutionArray.class, code))
                .map(ModaliteExecutionArray::valueOf)
                .collect(Collectors.toSet());
    }

    default Set<TypePrixArray> toTypePrixArray(Contrat contrat, Map<String, String> typePrixContrat) {
        return Optional.of(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .map(AbstractReferentielEntity::getCode)
                .map(typePrixContrat::get)
                .filter(code -> EnumUtils.isValidEnum(TypePrixArray.class, code))
                .map(TypePrixArray::valueOf)
                .collect(Collectors.toSet());
    }

    default Set<ModificationActesSousTraitanceArray> toModificationsActesSousTraitance(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(AgrementSousTraitant.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(AgrementSousTraitant.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModificationActesSousTraitance)
                .collect(Collectors.toSet());
    }

    default Set<ActeSousTraitanceArray> toActesSousTraitance(Contrat contrat, Map<String, String> map) {
        return Optional.ofNullable(contrat.getContratEtablissements())
                .orElse(Collections.emptySet())
                .stream()
                .map(agrementSousTraitant -> toActeSousTraitanceArray(agrementSousTraitant, map))
                .collect(Collectors.toSet());
    }

    default ActeSousTraitance.VariationPrix toVariationPrix(AgrementSousTraitant agrementSousTraitant, Map<String, String> mapVariationPrix) {
        return Optional.ofNullable(agrementSousTraitant)
                .map(AgrementSousTraitant::getContrat)
                .map(Contrat::getRevisionPrix)
                .map(RevisionPrix::getCode)
                .map(mapVariationPrix::get)
                .map(ActeSousTraitance.VariationPrix::valueOf)
                .orElse(null);
    }

    default ActeSousTraitance.VariationPrix toVariationPrix(ContratEtablissement sousTraitant, Map<String, String> mapVariationPrix) {
        return Optional.ofNullable(sousTraitant)
                .map(ContratEtablissement::getRevisionPrix)
                .map(RevisionPrix::getCode)
                .map(mapVariationPrix::get)
                .map(ActeSousTraitance.VariationPrix::valueOf)
                .orElse(null);
    }

    default Set<ModificationArray> toModifications(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeModificatif.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(ActeModificatif.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModifications)
                .collect(Collectors.toSet());
    }

    default Marche.TypeGroupementOperateurs toTypeGroupementOperateurs(Contrat contrat, Map<String, String> mapTypeGroupement) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypeGroupementOperateurs)
                .map(TypeGroupement::getCode)
                .filter(code -> EnumUtils.isValidEnum(Marche.TypeGroupementOperateurs.class, code))
                .map(mapTypeGroupement::get)
                .map(Marche.TypeGroupementOperateurs::valueOf).orElse(Marche.TypeGroupementOperateurs.PAS_DE_GROUPEMENT);
    }

    @Named("toPercent")
    default Double toPercent(Integer number) {
        return Optional.ofNullable(number)
                .map(e -> e / 100.0)
                .orElse(null);
    }

    default String toId(Contrat contrat, String sequence) {
        return Optional.ofNullable(contrat)
                .map(e -> MessageFormat.format("{0}{1}{2}",
                        getAcronymeOrganisme(e),
                        e.getNumero() != null ? e.getNumero() : "",
                        sequence)
                )
                .orElse(null);
    }

    default String getAcronymeOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getAcronyme)
                .orElse(null);
    }

    default Marche.FormePrix toFormePrix(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getTypeFormePrix)
                .map(FormePrix::getCode)
                .filter(code -> EnumUtils.isValidEnum(Marche.FormePrix.class, code))
                .map(Marche.FormePrix::valueOf)
                .orElse(null);
    }

    default Set<TitulaireArray> toTitulairesList(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratEtablissements)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toTitulaires)
                .collect(Collectors.toSet());
    }

    default String toDatePublicationDonnees() {
        return String.valueOf(LocalDate.now());
    }

    default Double toTauxAvance(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getTauxAvance)
                .map(BigDecimal::doubleValue)
                .orElse(0.0);
    }
}
