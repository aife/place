package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.ActeModificatifDTO;
import com.atexo.execution.server.model.actes.ActeModificatif;

public interface ActeModificatifMapper extends ActeMappable<ActeModificatif, ActeModificatifDTO> {
}
