package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.BonCommandeTypeDTO;
import com.atexo.execution.server.model.BonCommandeType;


public interface BonCommandeTypeMapper {

    BonCommandeTypeDTO toDTO(BonCommandeType bonCommandeType);

    BonCommandeType createEntity(BonCommandeTypeDTO bonCommandeTypeDTO);
}
