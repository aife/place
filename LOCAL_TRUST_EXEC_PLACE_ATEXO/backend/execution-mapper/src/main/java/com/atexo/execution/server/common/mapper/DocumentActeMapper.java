package com.atexo.execution.server.common.mapper;


import com.atexo.execution.common.dto.DocumentActeDTO;
import com.atexo.execution.server.model.DocumentActe;
import com.atexo.execution.server.model.Fichier;

public interface DocumentActeMapper {

	DocumentActeDTO toDTO( DocumentActe document);

	DocumentActe createEntity(DocumentActeDTO documentActeDTO);
}
