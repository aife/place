package com.atexo.execution.server.common.mapper.basecentrale;

import com.atexo.execution.server.entreprise.v3.model.AdresseDeLTablissement1;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneEtablissementsSiretGet200ResponseData;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Adresse;
import com.atexo.execution.server.model.Etablissement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface EtablissementV3Mapper {

    String ADRESSE_FORMAT = "%s %s %s";

    @Mapping(target = "siret", source = "etablissement.siret")
    @Mapping(target = "siege", source = "etablissement.siegeSocial")
    @Mapping(target = "adresse", source = "etablissement.adresse")
    @Mapping(target = "naf", source = "etablissement.activitePrincipale.code")
    @Mapping(target = "libelleNaf", source = "etablissement.activitePrincipale.libelle")
    @Mapping(target = "fournisseur", ignore = true)
    @Mapping(target = "contacts", ignore = true)
    @Mapping(target = "trusted", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    Etablissement toEtablissement(V3InseeSireneEtablissementsSiretGet200ResponseData etablissement);

    @Mapping(target = "adresse", source = "adresse", qualifiedByName = "toAdresseLibelle")
    @Mapping(target = "adresse2", source = "complementAdresse")
    @Mapping(target = "codePostal", source = "codePostal")
    @Mapping(target = "commune", source = "codeCommune")
    @Mapping(target = "pays", source = "codePaysEtranger")
    @Mapping(target = "codeInseeLocalite", source = "codeCedex")
    Adresse toAdresse(AdresseDeLTablissement1 adresse);


    @Named("toAdresseLibelle")
    default String toAdresseLibelle(AdresseDeLTablissement1 adresse) {
        return Optional.ofNullable(adresse)
                .map(e -> {
                    String typeVoie = e.getTypeVoie() != null ? e.getTypeVoie().getValue() : null;
                    return String.format(ADRESSE_FORMAT, e.getNumeroVoie(), typeVoie, e.getLibelleVoie());
                })
                .orElse(null);
    }
}
