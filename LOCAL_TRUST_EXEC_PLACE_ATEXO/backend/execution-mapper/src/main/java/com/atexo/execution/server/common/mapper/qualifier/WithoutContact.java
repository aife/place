package com.atexo.execution.server.common.mapper.qualifier;

import org.mapstruct.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by qba on 17/06/16.
 */
@Qualifier
@Retention(RetentionPolicy.SOURCE)
public @interface  WithoutContact {
}
