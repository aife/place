package com.atexo.execution.server.common.mapper.exceptions;

import com.atexo.execution.server.common.mapper.actes.ActeMappable;

import java.util.Set;
import java.util.stream.Collectors;

public class TooManyMapperException extends MapperException {

    public TooManyMapperException( final Set<? extends ActeMappable> mappers, final Class<?> type) {
		super(String.format("%d mappers existent (%s) pour le type d'acte '%s'", mappers.size(), mappers.stream()
			.map(mapper->mapper.getClass().getSimpleName())
			.collect(Collectors.joining()),
			type), type);
    }
}
