package com.atexo.execution.server.common.mapper.clause;

import com.atexo.execution.common.appach.model.contrat.titulaire.*;
import com.atexo.execution.common.def.ClauseRSE;
import com.atexo.execution.common.dto.ClauseDto;
import com.atexo.execution.common.exec.mpe.ws.ClauseN2Type;
import com.atexo.execution.common.exec.mpe.ws.ClauseN3Type;
import com.atexo.execution.common.exec.mpe.ws.ClauseN4Type;
import com.atexo.execution.common.exec.mpe.ws.ClauseType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratClause;
import com.atexo.execution.server.model.Habilitation;
import com.atexo.execution.server.model.clause.Clause;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.execution.common.def.ClauseRSE.*;

@Mapper(componentModel = "spring", imports = ClauseRSE.class,uses = MapperUtils.class)
public interface AppachClauseMapper {

    @Mapping(target = "code", source = "clause.code")
    @Mapping(target = "valeur", source = "valeur")
    @Mapping(target = "parent", source = "parent", qualifiedByName = "toClauseDto")
    @Named("toClauseDto")
    ClauseDto toClauseDto(ContratClause contratClause);


    @Mapping(target = CONDITION_EXECUTION, expression = "java(convertToClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION))")
    @Mapping(target = SPECIFICATION_TECHNIQUE, expression = "java(convertToClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE))")
    @Mapping(target = CRITERE_ATTRIBUTION_MARCHE, expression = "java(convertToClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE))")
    @Mapping(target = MARCHE_RESERVE, expression = "java(toMarcheReserve(contrat, ClauseRSE.MARCHE_RESERVE))")
    @Mapping(target = INSERTION, expression = "java(calculClauseN2(contrat, ClauseRSE.INSERTION, ClauseRSE.CLAUSES_SOCIALES,false))")
    ClausesSociales toClausesSociales(Contrat contrat);

    @Mapping(target = SPECIFICATIONS_TECHNIQUES, expression = "java(calculClauseN2(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES, ClauseRSE.CLAUSES_ENVIRONNEMENTALES, false))")
    @Mapping(target = CONDITIONS_EXECUTIONS, expression = "java(calculClauseN2(contrat, ClauseRSE.CONDITIONS_EXECUTIONS, ClauseRSE.CLAUSES_ENVIRONNEMENTALES,false))")
    @Mapping(target = CRITERES_SELECTIONS, expression = "java(calculClauseN2(contrat, ClauseRSE.CRITERES_SELECTIONS, ClauseRSE.CLAUSES_ENVIRONNEMENTALES,false))")
    ClausesEnvironnementales toClausesEnvironnementales(Contrat contrat);


    @Mapping(target = CONDITION_EXECUTION, expression = "java(toConditionExecution(contrat))")
    @Mapping(target = SPECIFICATION_TECHNIQUE, expression = "java(toSpecificationTechnique(contrat))")
    @Mapping(target = CRITERE_ATTRIBUTION_MARCHE, expression = "java(toCritereAttributionMarche(contrat))")
    @Mapping(target = MARCHE_RESERVE, expression = "java(toMarcheReserveV2(contrat))")
    @Mapping(target = INSERTION, expression = "java(calculClauseN2(contrat, ClauseRSE.INSERTION, ClauseRSE.CLAUSES_SOCIALES, true))")
    @Mapping(target = VERIFICATION_NON_RESPECT, expression = "java(calculClauseN2(contrat, ClauseRSE.VERIFICATION_NON_RESPECT, ClauseRSE.CLAUSES_SOCIALES, true))")
    ClausesSocialesV2 toClausesSocialesV2(Contrat contrat);

    @Mapping(target = VERIFICATION_NON_RESPECT, expression = "java(calculClauseN2(contrat, ClauseRSE.VERIFICATION_NON_RESPECT, ClauseRSE.CLAUSES_ENVIRONNEMENTALES, true))")
    @Mapping(target = SPECIFICATIONS_TECHNIQUES, expression = "java(toSpecificationsTechniques(contrat))")
    @Mapping(target = CONDITIONS_EXECUTIONS, expression = "java(toConditionExecutionV2(contrat))")
    @Mapping(target = CRITERES_SELECTIONS, expression = "java(toCriteresSelections(contrat))")
    @Mapping(target = PROTECTION_VALORISATION_ENVIRONNEMENT, expression = "java(calculClauseN2(contrat, ClauseRSE.PROTECTION_VALORISATION_ENVIRONNEMENT, ClauseRSE.CLAUSES_ENVIRONNEMENTALES, true))")
    ClausesEnvironnementalesV2 toClausesEnvironnementalesV2(Contrat contrat);

    @Mapping(target = INSERTION_ACTIVITE_ECONOMIQUE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.INSERTION_ACTIVITE_ECONOMIQUE, false))")
    @Mapping(target = CLAUSE_SOCIALE_FORMATION_SCOLAIRE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.CLAUSE_SOCIALE_FORMATION_SCOLAIRE, false))")
    @Mapping(target = LUTTE_CONTRE_DISCRIMINATIONS, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.LUTTE_CONTRE_DISCRIMINATIONS, false))")
    @Mapping(target = COMMERCE_EQUITABLE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.COMMERCE_EQUITABLE, false))")
    @Mapping(target = ACHATS_ETHIQUES_TRACABILITE_SOCIALE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.ACHATS_ETHIQUES_TRACABILITE_SOCIALE, false))")
    @Mapping(target = CLAUSE_SOCIALE_AUTRE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.CLAUSE_SOCIALE_AUTRE, false))")
    ClauseN3 convertToClauseN3(Contrat contrat, String codeClause);

    @Mapping(target = CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE, expression = "java(calculClauseN3(contrat, codeClause , ClauseRSE.CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE, false))")
    @Mapping(target = CLAUSE_SOCIALE_SIAE, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.CLAUSE_SOCIALE_SIAE, false))")
    @Mapping(target = CLAUSE_SOCIALE_EESS, expression = "java(calculClauseN3(contrat, codeClause ,ClauseRSE.CLAUSE_SOCIALE_EESS, false))")
    MarcheReserve toMarcheReserve(Contrat contrat, String codeClause);


    @Mapping(target = INSERTION_ACTIVITE_ECONOMIQUE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE. INSERTION_ACTIVITE_ECONOMIQUE, true))")
    @Mapping(target = CLAUSE_SOCIALE_FORMATION_SCOLAIRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.CLAUSE_SOCIALE_FORMATION_SCOLAIRE, true))")
    @Mapping(target = CLAUSE_INCITATIVE_FEMMES_HOMMES, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.CLAUSE_INCITATIVE_FEMMES_HOMMES, true))")
    @Mapping(target = CLAUSE_COERCITIVE_FEMMES_HOMMES, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.CLAUSE_COERCITIVE_FEMMES_HOMMES, true))")
    @Mapping(target = COMMERCE_EQUITABLE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.COMMERCE_EQUITABLE, true))")
    @Mapping(target = ACHATS_ETHIQUES_TRACABILITE_SOCIALE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.ACHATS_ETHIQUES_TRACABILITE_SOCIALE, true))")
    @Mapping(target = CLAUSE_SOCIALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITION_EXECUTION ,ClauseRSE.CLAUSE_SOCIALE_AUTRE, true))")
    ConditionExecution toConditionExecution(Contrat contrat);

    @Mapping(target = MOBILISATION_INSERTION_ACTIVITE_ECONOMIQUE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.MOBILISATION_INSERTION_ACTIVITE_ECONOMIQUE, true))")
    @Mapping(target = MOBILISATION_HANDICAP, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.MOBILISATION_HANDICAP, true))")
    @Mapping(target = EGALITE_FEMMES_HOMMES, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.EGALITE_FEMMES_HOMMES, true))")
    @Mapping(target = COMMERCE_EQUITABLE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.COMMERCE_EQUITABLE, true))")
    @Mapping(target = ACHATS_ETHIQUES_TRACABILITE_SOCIALE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.ACHATS_ETHIQUES_TRACABILITE_SOCIALE, true))")
    @Mapping(target = CLAUSE_SOCIALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATION_TECHNIQUE ,ClauseRSE.CLAUSE_SOCIALE_AUTRE, true))")
    SpecificationTechnique toSpecificationTechnique(Contrat contrat);

    @Mapping(target = INSERTION_ACTIVITE_ECONOMIQUE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE ,ClauseRSE.INSERTION_ACTIVITE_ECONOMIQUE, true))")
    @Mapping(target = EGALITE_FEMMES_HOMMES, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE ,ClauseRSE.EGALITE_FEMMES_HOMMES, true))")
    @Mapping(target = COMMERCE_EQUITABLE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE ,ClauseRSE.COMMERCE_EQUITABLE, true))")
    @Mapping(target = ACHATS_ETHIQUES_TRACABILITE_SOCIALE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE ,ClauseRSE.ACHATS_ETHIQUES_TRACABILITE_SOCIALE, true))")
    @Mapping(target = CLAUSE_SOCIALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERE_ATTRIBUTION_MARCHE ,ClauseRSE.CLAUSE_SOCIALE_AUTRE, true))")
    CritereAttributionMarche toCritereAttributionMarche(Contrat contrat);

    @Mapping(target = CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE, expression = "java(calculClauseN3(contrat, ClauseRSE.MARCHE_RESERVE ,ClauseRSE.CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE, true))")
    @Mapping(target = CLAUSE_SOCIALE_SIAE, expression = "java(calculClauseN3(contrat, ClauseRSE.MARCHE_RESERVE ,ClauseRSE.CLAUSE_SOCIALE_SIAE, true))")
    @Mapping(target = CLAUSE_SOCIALE_EESS, expression = "java(calculClauseN3(contrat, ClauseRSE.MARCHE_RESERVE ,ClauseRSE.CLAUSE_SOCIALE_EESS, true))")
    @Mapping(target = CLAUSE_SOCIALE_PRESTATIONS_ETABLISSEMENT_PENITENTIAIRE, expression = "java(calculClauseN3(contrat, ClauseRSE.MARCHE_RESERVE ,ClauseRSE.CLAUSE_SOCIALE_PRESTATIONS_ETABLISSEMENT_PENITENTIAIRE, true))")
    @Mapping(target = CLAUSE_SOCIALE_RESERVE_EA_ESAT_SIAE, expression = "java(calculClauseN3(contrat, ClauseRSE.MARCHE_RESERVE ,ClauseRSE.CLAUSE_SOCIALE_RESERVE_EA_ESAT_SIAE, true))")
    MarcheReserve__1 toMarcheReserveV2(Contrat contrat);

    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECOLABEL, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECOLABEL, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REPARABILITE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REPARABILITE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_BIODIVERSITE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_BIODIVERSITE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_EXCLUSION_PLASTIQUE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_EXCLUSION_PLASTIQUE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.SPECIFICATIONS_TECHNIQUES ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_AUTRE, true))")
    SpecificationsTechniques toSpecificationsTechniques(Contrat contrat);

    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_BEGES, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_BEGES, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REPARABILITE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REPARABILITE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_BIODIVERSITE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_BIODIVERSITE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_MODALITE_EXECUTION, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_MODALITE_EXECUTION, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_GESTION_DECHETS, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_GESTION_DECHETS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CONDITIONS_EXECUTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_AUTRE, true))")
    ConditionsExecutions toConditionExecutionV2(Contrat contrat);

    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECOLABEL, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECOLABEL, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_BEGES, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_BEGES, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_REPARABILITE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_REPARABILITE, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_DEFORESTATION, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_MODALITE_EXECUTION, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_MODALITE_EXECUTION, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_GESTION_DECHETS, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_GESTION_DECHETS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS, true))")
    @Mapping(target = CLAUSE_ENVIRONNEMENTALE_AUTRE, expression = "java(calculClauseN3(contrat, ClauseRSE.CRITERES_SELECTIONS ,ClauseRSE.CLAUSE_ENVIRONNEMENTALE_AUTRE, true))")
    CriteresSelections toCriteresSelections(Contrat contrat);


    default boolean calculClauseN3(Contrat contrat, String codeParentClause, String codeClause, boolean actif) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .filter(e -> isValidClause(e, codeParentClause, actif))
                .map(ContratClause::getClause)
                .map(Clause::getCode)
                .anyMatch(codeClause::equals);
    }

    default boolean calculClauseN2(Contrat contrat, String codeClause, String parentClause, boolean actif) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .filter(e -> e.getParent() != null && parentClause.equals(e.getParent().getClause().getCode()))
                .map(ContratClause::getClause)
                .filter(e -> e.isActif() == actif)
                .map(Clause::getCode)
                .anyMatch(codeClause::equals);
    }


    private boolean isValidClause(ContratClause e, String codeParentClause, boolean actif) {
        return e != null && e.getParent() != null && e.getParent().getClause() != null &&
                codeParentClause.equals(e.getParent().getClause().getCode()) &&
                e.getClause() != null &&
                e.getParent().getClause().isActif() == actif &&
                e.getClause().isActif() == actif;
    }

    default List<ClauseDto> toClausesDto(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toClauseDto)
                .collect(Collectors.toList());
    }


    default List<ClauseType> toClauses(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses) {
        return mapClauses.get(CLAUSES_N_1).stream()
                .filter(clause -> this.existInContratClause(contrat.getContratClauses(), clause.getCode()))
                .map(clause -> {
                    ClauseType clauseDTO = new ClauseType();
                    clauseDTO.setReferentielClauseN1(clause.getCode());
                    clauseDTO.getClausesN2().addAll(toClausesN2(contrat, mapClauses, clause.getCode()));
                    return clauseDTO;
                })
                .collect(Collectors.toList());
    }

    default List<ClauseN2Type> toClausesN2(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses, String parent) {
        return mapClauses.get(CLAUSES_N_2).stream()
                .filter(e -> this.existInContratClause(contrat.getContratClauses(), e.getCode()))
                .filter(e -> e.getGroupe() != null && parent.equals(e.getGroupe().getCode()))
                .map(e -> {
                    ClauseN2Type clauseDTO = new ClauseN2Type();
                    clauseDTO.setReferentielClauseN2(e.getCode());
                    clauseDTO.getClausesN3().addAll(toClausesN3(contrat, mapClauses, e.getCode()));
                    return clauseDTO;
                })
                .collect(Collectors.toList());
    }

    default List<ClauseN3Type> toClausesN3(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses, String parent) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .filter(e -> e.getParent() != null && parent.equals(e.getParent().getClause().getCode()))
                .filter(e -> mapClauses.get(CLAUSES_N_3).stream().anyMatch(c -> c.getCode().equals(e.getClause().getCode())))
                .map(clause -> {
                    ClauseN3Type clauseDTO = new ClauseN3Type();
                    clauseDTO.setReferentielClauseN3(clause.getClause().getCode());
                    clauseDTO.getClausesN4().addAll(toClausesN4(contrat, mapClauses, parent, clause.getClause().getCode()));
                    return clauseDTO;
                })
                .collect(Collectors.toList());
    }

    default List<ClauseN4Type> toClausesN4(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses, String parent, String clauseN3) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .filter(e -> mapClauses.get(CLAUSES_N_4).stream().anyMatch(c -> c.getCode().equals(e.getClause().getCode()) && clauseN3.equals(c.getGroupe().getCode())))
                .filter(e -> e.getParent() != null && parent.equals(e.getParent().getClause().getCode()))
                .map(clause -> {
                    ClauseN4Type clauseDTO = new ClauseN4Type();
                    clauseDTO.setReferentielClauseN4(clause.getClause().getCode());
                    clauseDTO.setValeur(clause.getValeur());
                    return clauseDTO;
                })
                .collect(Collectors.toList());
    }

    default boolean existInContratClause(Set<ContratClause> contratClauses, String code) {
        return contratClauses.stream()
                .map(ContratClause::getClause)
                .map(Clause::getCode)
                .anyMatch(codeClause -> codeClause.equals(code));
    }

    default List<String> map(Set<Habilitation> value) {
        return value.stream().map(Habilitation::getCode).collect(Collectors.toList());
    }
}
