package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.common.mapper.actes.ActeMappable;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;

public interface ActeMapper {
	<T extends Acte, U extends ActeDTO> ActeMappable<T,U> recupererMapper( Class<?> type );

	<T extends Acte, U extends ActeDTO> U toDTO( T acte );

	<T extends Acte, U extends ActeDTO> T createEntity(U acteDTO);

	<T extends Acte> ActeType toWS( T acte, EchangeChorus echange );


}
