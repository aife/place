package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class, ServiceMapper.class, HabilitationMapper.class})
public interface UtilisateurMapper {

    @Mapping(target = "organismeId", source = "service.organisme.id")
    @Mapping(target = "serviceId", source = "service.id")
    @Mapping(target = "plateformeUid", source = "plateforme.mpeUid")
    @Mapping(target = "devise", source = "plateforme.devise")
    @Mapping(target = "organismeUid", source = "service.organisme.uuid")
    UtilisateurDTO toDTO(Utilisateur utilisateur);

    @Mapping(target = "habilitations", ignore = true)
    Utilisateur createEntity(UtilisateurDTO utilisateurDTO);

}
