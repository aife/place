package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.Mpe;
import com.atexo.execution.common.mpe.ws.api.TemplateType;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CustomReponse extends Mpe.Reponse {

    public CustomReponse() {
        super();
    }

    @Override
    @JsonProperty("mail_template")
    public List<TemplateType> getMailTemplate() {
        return super.getMailTemplate();
    }
}
