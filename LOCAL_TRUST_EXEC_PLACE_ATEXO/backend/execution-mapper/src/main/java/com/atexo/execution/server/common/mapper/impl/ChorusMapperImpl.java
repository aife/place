package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.ChorusDTO;
import com.atexo.execution.server.common.mapper.ChorusMapper;
import com.atexo.execution.server.model.Chorus;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class ChorusMapperImpl implements ChorusMapper {

    @Override
    public ChorusDTO toDTO( Chorus chorus ) {
        if (chorus == null) {
            return null;
        }

        var result = new ChorusDTO();

	    result.setActif(chorus.getActif());
	    result.setVisaACCF(chorus.getVisaACCF());
	    result.setVisaPrefet(chorus.getVisaPrefet());
	    result.setTypeFournisseurEntreprise1(chorus.getTypeFournisseurEntreprise1());

        return result;
    }

    @Override
    public Chorus createEntity(ChorusDTO chorus) {
	    if (chorus == null) {
		    return null;
	    }

		var result = new Chorus();

	    result.setActif(chorus.getActif());
	    result.setVisaACCF(chorus.getVisaACCF());
	    result.setVisaPrefet(chorus.getVisaPrefet());
	    result.setTypeFournisseurEntreprise1(chorus.getTypeFournisseurEntreprise1());

	    return result;
    }
}
