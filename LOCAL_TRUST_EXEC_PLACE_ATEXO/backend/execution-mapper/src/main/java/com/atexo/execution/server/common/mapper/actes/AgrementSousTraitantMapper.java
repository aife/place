package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.AgrementSousTraitantDTO;
import com.atexo.execution.server.model.actes.AgrementSousTraitant;

public interface AgrementSousTraitantMapper extends ActeMappable<AgrementSousTraitant, AgrementSousTraitantDTO> {

}
