package com.atexo.execution.server.common.mapper;


import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.dto.FournisseurDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Fournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, EtablissementMapper.class, ReferentielMapper.class})
public interface FournisseurMapper {


    @Mapping(target = "etablissements", source = "etablissements")
    @Mapping(target = "categorie", source = "categorie")
    Fournisseur createEntity(FournisseurDTO fournisseurDTO);


    @Mapping(target = "etablissements", source = "fournisseur.etablissements")
    @Mapping(target = "montantContratsSousTraitants", source = "contratEtablissements", qualifiedByName = "toMontantContratsSousTraitants")
    @Mapping(target = "nbrContratsSousTraitants", source = "contratEtablissements", qualifiedByName = "toNbrContratsSousTraitants")
    @Mapping(target = "montantContratsMandataire", source = "contratEtablissements", qualifiedByName = "toMontantContratsMandataire")
    @Mapping(target = "nbrContratsMandataire", source = "contratEtablissements", qualifiedByName = "toNbrContratsMandataire")
    @Mapping(target = "nbrContratsCotraitants", source = "contratEtablissements", qualifiedByName = "toNbrContratsCotraitants")
    @Mapping(target = "montantContratsCotraitants", source = "contratEtablissements", qualifiedByName = "toMontantContratsCotraitants")
    @Mapping(target = "nbrContratsClos", source = "contratEtablissements", qualifiedByName = "toNbrContratsClos")
    FournisseurDTO toDTO(Fournisseur fournisseur, List<ContratEtablissement> contratEtablissements);

    @Named("toMontantContratsSousTraitants")
    default BigDecimal toMontantContratsSousTraitants(List<ContratEtablissement> contratEtablissements) {
        return Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> contratEtablissement.getMandataire() == null && !Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId()))
                .map(contratEtablissement -> contratEtablissement.getMontant() != null ? contratEtablissement.getMontant() : BigDecimal.ZERO)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Named("toNbrContratsSousTraitants")
    default Integer toNbrContratsSousTraitants(List<ContratEtablissement> contratEtablissements) {
        return Math.toIntExact(Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> contratEtablissement.getMandataire() == null && !Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId()))
                .count());
    }

    @Named("toMontantContratsMandataire")
    default BigDecimal toMontantContratsMandataire(List<ContratEtablissement> contratEtablissements) {
        return Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> contratEtablissement.getMandataire() == Boolean.TRUE || Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId()))
                .map(contratEtablissement -> contratEtablissement.getMontant() != null ? contratEtablissement.getMontant() : BigDecimal.ZERO)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Named("toNbrContratsMandataire")
    default Integer toNbrContratsMandataire(List<ContratEtablissement> contratEtablissements) {
        return Math.toIntExact(Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> contratEtablissement.getMandataire() == Boolean.TRUE || Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId()))
                .count());
    }

    @Named("toMontantContratsCotraitants")
    default BigDecimal toMontantContratsCotraitants(List<ContratEtablissement> contratEtablissements) {
        return Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> !(contratEtablissement.getMandataire() == null && !Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId())))
                .filter(contratEtablissement -> !(contratEtablissement.getMandataire() == Boolean.TRUE || Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId())))
                .map(contratEtablissement -> contratEtablissement.getMontant() != null ? contratEtablissement.getMontant() : BigDecimal.ZERO)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Named("toNbrContratsCotraitants")
    default Integer toNbrContratsCotraitants(List<ContratEtablissement> contratEtablissements) {
        return Math.toIntExact(Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null)
                .filter(contratEtablissement -> !(contratEtablissement.getMandataire() == null && !Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId())))
                .filter(contratEtablissement -> !(contratEtablissement.getMandataire() == Boolean.TRUE || Objects.equals(contratEtablissement.getId(), contratEtablissement.getContrat().getAttributaire().getId())))
                .count());
    }

    @Named("toNbrContratsClos")
    default Integer toNbrContratsClos(List<ContratEtablissement> contratEtablissements) {
        return Math.toIntExact(Optional.ofNullable(contratEtablissements)
                .orElse(Collections.emptyList())
                .stream()
                .filter(contratEtablissement -> !(contratEtablissement.getContrat().getStatut() != StatutContrat.Clos && contratEtablissement.getContrat().getAttributaire() != null))
                .count());
    }
}
