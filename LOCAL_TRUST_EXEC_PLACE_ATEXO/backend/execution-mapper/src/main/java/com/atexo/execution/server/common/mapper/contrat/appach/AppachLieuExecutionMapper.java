package com.atexo.execution.server.common.mapper.contrat.appach;

import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AppachLieuExecutionMapper {

    String CODE_DEPARTEMENT = "Code département";
    String CODE_PAYS = "Code pays";

    @Mapping(target = "code", source = "code")
    @Mapping(target = "nom", source = "label")
    @Mapping(target = "typeCode", source = "lieuExecution", qualifiedByName = "toTypeCode")
    com.atexo.execution.common.appach.model.contrat.titulaire.LieuExecutions toLieuExecutionContratTitulaire(com.atexo.execution.server.model.LieuExecution lieuExecution);

    default com.atexo.execution.common.appach.model.contrat.titulaire.LieuExecutions toLieuExecutions(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getLieuxExecution)
                .orElse(Collections.emptySet())
                .stream().findFirst()
                .map(this::toLieuExecutionContratTitulaire)
                .orElse(null);
    }

    @Named("toTypeCode")
    default String toTypeCode(com.atexo.execution.server.model.LieuExecution lieuExecution) {
        return CODE_DEPARTEMENT;
    }
}
