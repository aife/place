package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.ContratEtablissementCriteriaDTO;
import com.atexo.execution.common.dto.ContratEtablissementDTO;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.repository.paginable.criteres.ContratEtablissementCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Qualifier;


@Qualifier("contratEtablissementMapperV2")
@Mapper(componentModel = "spring", uses = {MapperUtils.class, EtablissementMapper.class, ContactMapper.class})
public interface ContratEtablissementMapperV2 {

    @Named("toDTO")
    @Mapping(target = "id", source = "id")
    @Mapping(target = "etablissement", source = "etablissement", qualifiedByName = "toDTO")
    @Mapping(target = "contactReferantList", ignore = true)
    @Mapping(target = "contrat", ignore = true)
    @Mapping(target = "commanditaire", ignore = true)
    @Mapping(target = "sousTraitants", ignore = true)
    @Mapping(target = "nombreDeContactReferants", expression = "java(contratEtablissement.getContactReferantList().size())")
    @Mapping(target = "organismeUid", source = "contrat.service.organisme.uuid")
    ContratEtablissementDTO toDTO(ContratEtablissement contratEtablissement);

    ContratEtablissementCriteria toCriteria(ContratEtablissementCriteriaDTO contratEtablissementCriteriaDTO);
}
