package com.atexo.execution.server.common.mapper.exceptions;

public abstract class MapperException extends RuntimeException {

	protected final Class<?> type;

    public MapperException( final String message, final Class<?> type) {
		super(message);

		this.type = type;
    }

	public Class<?> getType() {
		return type;
	}
}
