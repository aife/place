package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ChorusDTO;
import com.atexo.execution.server.model.Chorus;

public interface ChorusMapper {

	ChorusDTO toDTO( Chorus chorus);

    Chorus createEntity(ChorusDTO chorusDTO);

}
