package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.DecisionProlongationDelaiDTO;
import com.atexo.execution.server.model.actes.DecisionProlongationDelai;

public interface DecisionProlongationDelaiMapper extends ActeMappable<DecisionProlongationDelai, DecisionProlongationDelaiDTO> {
}
