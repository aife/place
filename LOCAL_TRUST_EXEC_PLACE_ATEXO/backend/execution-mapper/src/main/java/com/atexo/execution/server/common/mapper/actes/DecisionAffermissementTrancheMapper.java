package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.DecisionAffermissementTrancheDTO;
import com.atexo.execution.server.model.actes.DecisionAffermissementTranche;

public interface DecisionAffermissementTrancheMapper extends ActeMappable<DecisionAffermissementTranche, DecisionAffermissementTrancheDTO> {
}
