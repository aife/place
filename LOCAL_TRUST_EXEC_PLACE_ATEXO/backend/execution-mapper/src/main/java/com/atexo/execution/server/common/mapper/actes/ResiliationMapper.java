package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.ResiliationDTO;
import com.atexo.execution.server.model.actes.Resiliation;

public interface ResiliationMapper extends ActeMappable<Resiliation, ResiliationDTO> {
}
