package com.atexo.execution.server.common.mapper.exceptions;

public class NoMapperException extends MapperException {

    public NoMapperException( final Class<?> type) {
		super(String.format("Aucun mapper pour le type d'acte %s", type), type);
    }

}
