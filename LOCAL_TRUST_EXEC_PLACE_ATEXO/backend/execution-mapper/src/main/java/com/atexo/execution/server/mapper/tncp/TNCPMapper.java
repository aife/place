package com.atexo.execution.server.mapper.tncp;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.actes.AgrementSousTraitant;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.model.concession.NatureContratConcession;
import fr.atexo.execution.tncp.envoi.json.SousTraitant;
import fr.atexo.execution.tncp.envoi.json.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface TNCPMapper {
    String SAVE_ESSENTIAL_DATA = "creerDEMP";
    String CONSIDERATION_SOCIALE = "consideration-sociale";
    String CONSIDERATION_ENVIRONNEMENTALE = "consideration-environnementale";
    String NATURE_CONCESSION = "nature-concession";
    String CCAG_CONTRAT = "ccag-contrat";
    String CPV = "cpv";
    String AUCUNE_CLAUSE = "aucune";
    String DATE_FORMAT = "yyyy-MM-dd";


    @Mapping(target = "plateforme.idPlateforme", source = "contract.plateforme.tncpIdentifiant")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "contract.plateforme.tncpIdentifiantTechnique")
    @Mapping(target = "rsDemandeur", expression = "java(toRsDemandeur(contract))")
    @Mapping(target = "idDemandeur", source = "contract.service.siret")
    @Mapping(target = "operation", constant = SAVE_ESSENTIAL_DATA)
    @Mapping(target = "donneeEssentielle", expression = "java(toDonneeEssentielle(contract,map))")
    TNCPContrat toMarche(Contrat contract, Map<String, Map<String, String>> map);

    @Mapping(target = "nature", expression = "java(toNature(contract,map.get(NATURE_CONCESSION)))")
    @Mapping(target = "procedure", source = "contract.consultation.procedure.label")
    @Mapping(target = "statutDE", constant = "A_TRANSMETTRE")
    @Mapping(target = "donneesMP", expression = "java(toDonneesMP(contract,map))")
    @Mapping(target = "considerationsSociales", expression = "java(toConsiderationsSociales(contract,map.get(CONSIDERATION_SOCIALE)))")
    @Mapping(target = "considerationsEnvironnementales", expression = "java(toConsiderationsEnvironnementales(contract,map.get(CONSIDERATION_ENVIRONNEMENTALE)))")
    @Mapping(target = "datePublicationDonnees", expression = "java(getDatePublication())", dateFormat = DATE_FORMAT)
    @Mapping(target = "dureeMois", source = "contract.dureeMaximaleMarche")
    @Mapping(target = "modifications", expression = "java(toModifications(contract))")
    DonneeEssentielle toDonneeEssentielle(Contrat contract, Map<String, Map<String, String>> map);


    @Mapping(target = "codeCPV", expression = "java(toCodeCpv(contract,map.get(CPV)))")
    @Mapping(target = "lieuExecutionCode", expression = "java(toCodeLieuExecution(contract))")
    @Mapping(target = "lieuExecutionTypeCode", constant = "Code département")
    @Mapping(target = "idAcheteur", source = "contract.service.siret")
    @Mapping(target = "ccag", expression = "java(toCcag(contract,map.get(CCAG_CONTRAT)))")
    @Mapping(target = "modaliteExecution", source = "contract.consultation.modaliteExecutions", qualifiedByName = "toReferentielLabel")
    @Mapping(target = "technique", source = "contract.consultation.techniqueAchats", qualifiedByName = "toReferentielLabel")
    @Mapping(target = "typePrix", source = "contract.consultation.typesPrix", qualifiedByName = "toReferentielLabel")
    @Mapping(target = "typeGroupementOperateurs", source = "contract.groupement.type.labelKey", defaultValue = "Pas de groupement")
    @Mapping(target = "offresRecues", source = "contract.offresRecues")
    @Mapping(target = "origineFrance", expression = "java(toTauxFrance(contract))")
    @Mapping(target = "origineUE", expression = "java(toTauxUE(contract))")
    @Mapping(target = "tauxAvance", expression = "java(toMontant(contract.getTauxAvance(), true))")
    @Mapping(target = "titulaires", source = "contract.attributaire.etablissement")
    @Mapping(target = "formePrix", source = "contract.typeFormePrix.label")
    @Mapping(target = "sousTraitanceDeclaree", expression = "java(toSousTraitanceDeclaree(contract))")
    @Mapping(target = "actesSousTraitance", expression = "java(toActesSousTraitance(contract))")
    @Mapping(target = "modificationsActesSousTraitance", expression = "java(toModificationsActesSousTraitance(contract))")
    @Mapping(target = "idAccordCadre", expression = "java(toIdAccordCadre(contract))")
    @Mapping(target = "montant", expression = "java(toMontant(contract.getMontant(), false))")
    DonneesMP toDonneesMP(Contrat contract, Map<String, Map<String, String>> map);

    default String toCodeCpv(Contrat contrat, Map<String, String> map) {
        return Optional.ofNullable(contrat.getCpvPrincipal())
                .map(AbstractReferentielEntity::getCode)
                .map(map::get)
                .orElse(null);
    }

    default StatutPublicationDE toStatutPublicationDE(String statutFlink) {
        if (statutFlink == null) {
            return null;
        }
        switch (statutFlink.toUpperCase()) {
            case "PUBLIE":
            case "FINI":
                return StatutPublicationDE.PUBLIE;
            case "EN_COURS":
            case "EN_ATTENTE":
                return StatutPublicationDE.EN_COURS;
            case "ERREUR":
                return StatutPublicationDE.ERROR;
            default:
                return null;
        }
    }

    @Mapping(target = "id", source = "acteModificatif.id")
    @Mapping(target = "dateNotificationModification", source = "acteModificatif.contrat.dateNotification", dateFormat = DATE_FORMAT)
    @Mapping(target = "datePublicationDonneesModification", expression = "java(getDatePublication())", dateFormat = DATE_FORMAT)
    @Mapping(target = "dureeMois", source = "acteModificatif.contrat.dureeMaximaleMarche")
    @Mapping(target = "montant", expression = "java(toMontantInteger(acteModificatif.getMontantHTChiffre()))")
    @Mapping(target = "titulaires", expression = "java(toTitulaires(acteModificatif))")
    Modification toModification(ActeModificatif acteModificatif);

    @Mapping(target = "id", source = "etablissement.siret")
    @Mapping(target = "typeIdentifiant", expression = "java(toTypeIdentifiant(etablissement))")
    Titulaire toTitulaire(Etablissement etablissement);


    default List<Titulaire> toTitulaires(Etablissement etablissement) {
        if (etablissement == null) {
            return Collections.emptyList();
        }
        return List.of(toTitulaire(etablissement));

    }

    @Mapping(target = "id", source = "etablissement.siret")
    @Mapping(target = "typeIdentifiant", expression = "java(toTypeIdentifiant(etablissement))")
    SousTraitant toSousTraitant(Etablissement etablissement);

    @Mapping(target = "id", source = "agrementSousTraitant.id")
    @Mapping(target = "dureeMois", source = "agrementSousTraitant.contrat.dureeMaximaleMarche")
    @Mapping(target = "dateNotification", source = "agrementSousTraitant.dateNotification", dateFormat = DATE_FORMAT)
    @Mapping(target = "montant", source = "agrementSousTraitant.montantHT")
    @Mapping(target = "datePublicationDonnees", expression = "java(getDatePublication())", dateFormat = DATE_FORMAT)
    ModificationsActesSousTraitance toModificationActesSousTraitance(AgrementSousTraitant agrementSousTraitant);


    @Mapping(target = "id", source = "sousTraitant.id")
    @Mapping(target = "dureeMois", source = "sousTraitant.dureeMois")
    @Mapping(target = "dateNotification", source = "sousTraitant.dateNotification", dateFormat = DATE_FORMAT)
    @Mapping(target = "montant", expression = "java(toMontant(sousTraitant.getMontant(), false))")
    @Mapping(target = "variationPrix", source = "sousTraitant.revisionPrix.label")
    @Mapping(target = "datePublicationDonnees", expression = "java(getDatePublication())", dateFormat = DATE_FORMAT)
    @Mapping(target = "sousTraitant", source = "sousTraitant.etablissement")
    ActesSousTraitance toActeSousTraitance(ContratEtablissement sousTraitant);


    ModificationsActesSousTraitance toModificationActeSoutraitance(Contrat contrat);


    default String toCodeCPV(Contrat contrat) {
        return Optional.ofNullable(contrat.getCodesCpv())
                .orElse(Collections.emptySet())
                .stream()
                .map(AbstractReferentielEntity::getCode).findFirst().orElse(null);
    }

    default String toIdAccordCadre(Contrat contrat) {
        return Optional.ofNullable(contrat).map(Contrat::getLienAcSad).map(Contrat::getNumero).orElse(null);
    }

    default boolean toSousTraitanceDeclaree(Contrat contrat) {
        return Optional.ofNullable(contrat.getSousTraitans()).orElse(Collections.emptySet()).stream().anyMatch(contratEtablissement -> contratEtablissement.getCommanditaire() != null);
    }


    default String toConsiderationsSociales(Contrat contrat, Map<String, String> mapConsiderationSocaile) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(getAucuneClauseSet())
                .stream()
                .map(e -> mapConsiderationSocaile.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.joining(";"));
    }

    default HashSet<ContratClause> getAucuneClauseSet() {
        var emptySet = new HashSet<ContratClause>();
        var emptyClause = new Clause();
        emptyClause.setCode(AUCUNE_CLAUSE);
        var contraClause = new ContratClause();
        contraClause.setClause(emptyClause);
        emptySet.add(new ContratClause());
        return emptySet;
    }

    default String toConsiderationsEnvironnementales(Contrat contrat, Map<String, String> mapConsiderationEnvironnementale) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(getAucuneClauseSet())
                .stream()
                .map(e -> mapConsiderationEnvironnementale.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.joining(";"));
    }

    @Named("toReferentielLabel")
    default String toReferentielLabel(Collection<? extends AbstractReferentielEntity> referentielEntities) {
        return referentielEntities.stream()
                .map(AbstractReferentielEntity::getLabel)
                .collect(Collectors.joining(";"));
    }

    default List<ModificationsActesSousTraitance> toModificationsActesSousTraitance(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(AgrementSousTraitant.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(AgrementSousTraitant.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModificationActesSousTraitance)
                .collect(Collectors.toList());
    }

    default List<ActesSousTraitance> toActesSousTraitance(Contrat contrat) {
        return Optional.ofNullable(contrat.getSousTraitans())
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toActeSousTraitance)
                .collect(Collectors.toList());
    }

    default List<Modification> toModifications(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeModificatif.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(ActeModificatif.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModification)
                .collect(Collectors.toList());
    }

    default String toCodeLieuExecution(Contrat contrat) {
        return contrat.getLieuxExecution().stream()
                .findFirst()
                .map(LieuExecution::getCode)
                .orElse(null);
    }

    default String toTypeIdentifiant(Etablissement etablissement) {
        if (etablissement != null && etablissement.getSiret() != null && etablissement.getSiret().replace(" ", "").length() == 14) {
            return "SIRET";
        }
        var codePays = Optional.ofNullable(etablissement)
                .map(Etablissement::getAdresse)
                .map(Adresse::getPays)
                .filter(Objects::nonNull)
                .map(String::toUpperCase)
                .map(pays -> pays.substring(2))
                .orElse(null);
        if (!"FR".equals(codePays)) {
            return "HORS UE";
        }
        return "SIRET";
    }

    default String toNature(Contrat contrat, Map<String, String> map) {
        String nature = "Marché";
        var typeContrat = Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .orElse(null);
        var codeTypeContrat = Optional.ofNullable(typeContrat)
                .map(TypeContrat::getCode)
                .orElse(null);
        if ("ppp".equalsIgnoreCase(codeTypeContrat)) {
            nature = "Marché de partenariat";
        }
        // cas des concessions
        var natureConcession = Optional.ofNullable(contrat)
                .map(Contrat::getNatureContratConcession)
                .map(NatureContratConcession::getCode)
                .orElse(null);
        if (natureConcession != null) {
            nature = map.get(natureConcession);
        }
        return nature;
    }

    default String toCcag(Contrat contrat, Map<String, String> ccagContrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getCcagApplicable)
                .map(CCAGReference::getCode)
                .map(ccagContrat::get)
                .orElse("Pas de CCAG");
    }

    default double toTauxUE(Contrat contrat) {
        return Optional.ofNullable(contrat.getConsultation()).map(Consultation::getOrigineUE).map(this::toTaux).orElse((double) 0);
    }

    default double toTauxFrance(Contrat contrat) {
        return Optional.ofNullable(contrat.getConsultation()).map(Consultation::getOrigineFrance).map(this::toTaux).orElse((double) 0);
    }

    default double toTaux(double taux) {
        return Optional.of(taux).map(v -> v / 100).orElse((double) 0);
    }

    default String getDatePublication() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    default List<Titulaire> toTitulaires(ActeModificatif acteModificatif) {
        return Optional.ofNullable(acteModificatif)
                .map(ActeModificatif::getContrat)
                .map(Contrat::getContratEtablissements)
                .orElse(Collections.emptySet())
                .stream()
                .map(ContratEtablissement::getEtablissement)
                .map(this::toTitulaire)
                .collect(Collectors.toList());
    }

    default String toRsDemandeur(Contrat contract) {
        return Optional.ofNullable(contract)
                .map(Contrat::getService)
                .map(Service::getRaisonSociale)
                .map(rs -> (rs.length() > 100 ? rs.substring(0, 100) : rs))
                .orElse(null);
    }

    default Double toMontant(BigDecimal montant, boolean taux) {
        if (taux) {
            return Optional.ofNullable(montant).map(v -> v.doubleValue() / 100).orElse((double) 0);
        } else {
            return montant.doubleValue();
        }

    }

    default Integer toMontantInteger(Double montant) {
        return montant != null ? montant.intValue() : null;
    }


}
