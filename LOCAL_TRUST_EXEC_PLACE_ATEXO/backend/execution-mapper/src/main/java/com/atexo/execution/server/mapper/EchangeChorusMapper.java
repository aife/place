package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.common.mpe.ws.api.RetourChorusType;
import com.atexo.execution.common.mpe.ws.api.StatutEchangeType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring", uses = {AgentMapper.class, ActeMapperV2.class, MapperUtils.class, ReferentielMapper.class, ContratMapper.class, EtablissementMapperV2.class})
public interface EchangeChorusMapper {

	@Mapping(target = "utilisateurCreation", ignore = true)
	@Mapping(source = "statut.value", target = "statut.code")
	EchangeChorus createEntity(EchangeChorusDTO echangeChorus);

	Set<EchangeChorus> createEntity(Set<EchangeChorusDTO> echangesChorus);

    @Mapping(source = "utilisateurCreation", target = "agent")
    EchangeChorusDTO toDto(EchangeChorus echangeChorus);

	Set<EchangeChorusDTO> toDto(Set<EchangeChorus> echangesChorus);

    @Mapping(source = "echangeChorus.idExterne", target = "id")
    @Mapping(expression = "java(toStatutEchange(echangeChorus))", target = "statutEchange")
    @Mapping(expression = "java(toRetourChorus(echangeChorus))", target = "retourChorus")
    @Mapping(source = "echangeChorus.dateModification", target = "dateModification")
    @Mapping(source = "echangeChorus.dateNotificationPrevisionnelle", target = "dateNotificationPrevisionnelle")
    @Mapping(source = "acte.utilisateurCreation", target = "agent")
    @Mapping(target = "reference", ignore = true)
    @Mapping(target = "datePublication", ignore = true)
    @Mapping(source = "acte.contrat.dateFinContrat", target = "acte.dateFinMarche")
    @Mapping(target = "acte.type", ignore = true)
    @Mapping(target = "uuidContrat", source = "acte.contrat.uuid")
    @Mapping(target = "messageErreur", source = "echangeChorus.erreurPublication")
    EchangeChorusType toWS( Acte acte, EchangeChorus echangeChorus);

    default StatutEchangeType toStatutEchange(EchangeChorus echangeChorus) {
        if (echangeChorus != null && echangeChorus.getStatut() != null) {
            StatutEchangeType statutEchange = new StatutEchangeType();
            statutEchange.setCode(echangeChorus.getStatut().getCode());
            statutEchange.setLibelle(echangeChorus.getStatut().getLabel());
            return statutEchange;
        }
        else {
            return null;
        }
    }

    default RetourChorusType toRetourChorus(EchangeChorus echangeChorus) {
        if (echangeChorus != null && echangeChorus.getRetourChorus() != null) {
            RetourChorusType retourChorus = new RetourChorusType();
            retourChorus.setCode(echangeChorus.getRetourChorus().getCode());
            retourChorus.setLibelle(echangeChorus.getRetourChorus().getLabel());
            return retourChorus;
        }
        else {
            return null;
        }
    }

}
