package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ContactReferantDTO;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.ContactReferant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring", uses = {MapperUtils.class, ContratEtablissementMapperV2.class, ContactMapper.class})
public interface ContactReferantMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "contratEtablissement", source = "contratEtablissement", qualifiedByName = "toDTO")
    ContactReferantDTO toDTO(ContactReferant contactReferant);
}
