package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.common.mapper.*;
import com.atexo.execution.server.common.mapper.actes.ActeMappable;
import com.atexo.execution.server.common.mapper.exceptions.NoMapperException;
import com.atexo.execution.server.common.mapper.exceptions.TooManyMapperException;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.DonneesEssentiellesMapper;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class ActeMapperImpl implements ActeMapper {

	private static final Logger log = LoggerFactory.getLogger(ActeMapperImpl.class);

	@Autowired
	private EtablissementRepository etablissementRepository;

	@Autowired
	private ContratRepository contratRepository;

	@Autowired
	private TypeActeRepository typeActeRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

    @Autowired
    private DocumentActeMapper documentActeMapper;

    @Autowired
    private EtablissementMapper etablissementMapper;

	@Autowired
	private UtilisateurMapper utilisateurMapper;

    @Autowired
    private ReferentielMapper referentielMapper;

	@Autowired
	private DonneesEssentiellesMapper donneesEssentiellesMapper;

	@Autowired
	private EchangeChorusMapper echangeChorusMapper;

	@Autowired
	private ContratMapper contratMapper;

	@Autowired
	private Set<ActeMappable> delegates;

	/**
	 * Permet de rechercher le mapper d'un acte a partir du type de l'acte
	 * @param acte l'acte
	 * @return le mapper
	 */
	public <T extends Acte, U extends ActeDTO> ActeMappable<T,U> recupererMapper( Class<?> type ) {
		ActeMappable<T,U> result;

		Predicate<? super ActeMappable> match = (ActeMappable mapper)-> mapper.accept(type);

		if ( delegates.stream().filter(match).count() > 1) {
			log.error("{} mappers existent ({}) pour le type d'acte '{}'",
				delegates.stream().filter(match).count(),
				delegates.stream().map(mapper->mapper.getClass().getSimpleName()).collect(Collectors.joining()),
				type
			);

			throw new TooManyMapperException(delegates.stream().filter(match).collect(Collectors.toSet()), type);
		} else {
			result = delegates.stream().filter(match).findFirst().orElseThrow(
				() -> {
					log.error("Aucun mapper pour le type d'acte '{}'", type);

					throw new NoMapperException(type);
				}
			);

			log.debug("Le mapper pour le type '{}' est '{}'", type.getSimpleName(), result.getClass().getCanonicalName());
		}

		return result;
	}

	@Override
	public <T extends Acte, U extends ActeDTO> U toDTO( T acte ) {
		var mapper = this.recupererMapper(acte.getClass());

		return (U)mapper.toDTO(acte);
	}

    @Override
    public <T extends Acte, U extends ActeDTO> T createEntity(U acteDTO) {
	    var mapper = this.recupererMapper(acteDTO.getClass());

	    return (T)mapper.createEntity(acteDTO);
    }

	@Override
	public <T extends Acte> ActeType toWS( T acte, EchangeChorus echange ) {
		var mapper = this.recupererMapper(acte.getClass());

		return mapper.toWS(acte, echange);
	}
}
