package com.atexo.execution.server.common.mapper.contrat.appach;

import com.atexo.execution.common.appach.model.contrat.titulaire.Consultation;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AppachContratTitulaireConsultationMapper {

    @Mapping(target = "id", source = "contrat", qualifiedByName = "toIdConsultation")
    @Mapping(target = "numero", source = "contrat.consultation.numero")
    @Mapping(target = "codeExterne", source = "contrat.consultation.numeroProjetAchat")
    Consultation toConsultation(Contrat contrat);

    @Named("toIdConsultation")
    default Integer toIdConsultation(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(com.atexo.execution.server.model.Consultation::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .filter(Objects::nonNull)
                .map(Integer::parseInt)
                .orElse(null);
    }
}
