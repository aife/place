package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.model.DocumentContrat;
import com.atexo.execution.server.model.DocumentContratUsersEnEdition;
import com.atexo.execution.server.model.Evenement;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class DocumentContratMapperDecorator implements DocumentContratMapper {

    @Autowired
    DocumentContratMapper delegate;

    @Autowired
    DateTimeMapper dateTimeMapper;

    @Autowired
    EvenementMapper evenementMapper;

    @Autowired
    UtilisateurMapper utilisateurMapper;

    @Override
    public DocumentContratDTO toDTO(DocumentContrat document, boolean withEvenement) {
        // Recuperer les documents avec leurs evenement ou pas
        DocumentContratDTO dto = delegate.toDTO(document, withEvenement);
        if (withEvenement) {
            List<EvenementDTO> list = new ArrayList<>();
            for (Evenement evenement : document.getEvenements()) {
                list.add(evenementMapper.toDTO(evenement));
            }
            dto.setEvenements(list);
        }
        if (document.getDateCreationUtilisateur() != null) {
            dto.setDateCreation(document.getDateCreation());
        }
        if (document.getUsersEnEdition() != null) {
            dto.setUtilisateursEnLigne(document.getUsersEnEdition().stream()
                    .map(DocumentContratUsersEnEdition::getUtilisateur)
                    .map(utilisateur -> utilisateur.getPrenom()+" "+utilisateur.getNom())
                    .collect(Collectors.toList()));
        }
        return dto;
    }
}
