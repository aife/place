package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.FichierDTO;
import com.atexo.execution.server.common.mapper.FichierMapper;
import com.atexo.execution.server.model.Fichier;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class FichierMapperImpl implements FichierMapper {

    @Override
    public FichierDTO toDTO(Fichier fichier) {
        if (fichier == null) {
            return null;
        }

        FichierDTO fichierDTO = new FichierDTO();

        fichierDTO.setId(fichier.getId());
        fichierDTO.setNom(fichier.getNom());
        fichierDTO.setTaille(fichier.getTaille());
        fichierDTO.setContentType(fichier.getContentType());

        return fichierDTO;
    }

    @Override
    public Fichier createEntity(FichierDTO fichierDTO) {
        if (fichierDTO == null) {
            return null;
        }

        Fichier fichier = new Fichier();

        fichier.setId(fichierDTO.getId());
        fichier.setNom(fichierDTO.getNom());
        fichier.setNomEnregistre(fichierDTO.getNomEnregistre());
        fichier.setTaille(fichierDTO.getTaille());
        fichier.setContentType(fichierDTO.getContentType());

        return fichier;
    }
}
