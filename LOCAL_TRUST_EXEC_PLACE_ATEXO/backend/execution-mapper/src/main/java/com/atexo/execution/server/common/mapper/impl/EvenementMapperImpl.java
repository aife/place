package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.common.mapper.EvenementMapper;
import com.atexo.execution.server.common.mapper.EvenementMapperDecorator;
import com.atexo.execution.server.model.Evenement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
@Primary
public class EvenementMapperImpl extends EvenementMapperDecorator implements EvenementMapper {

    @Autowired
    @Qualifier("delegate")
    private EvenementMapper delegate;

    @Override
    public EvenementDTO toDTO(Evenement evenement) {
        return delegate.toDTO(evenement);
    }

    @Override
    public EvenementDTO toDTOWithoutDocument(Evenement evenement) {
        return delegate.toDTOWithoutDocument(evenement);
    }
}
