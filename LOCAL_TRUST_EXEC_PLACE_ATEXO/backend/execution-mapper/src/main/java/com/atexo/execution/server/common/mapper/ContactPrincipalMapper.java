package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.ContactPrincipalDTO;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.ContratEtablissement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ContactReferantMapper.class, ReferentielMapper.class, ContratEtablissementMapperV2.class, ContactMapper.class})
public interface ContactPrincipalMapper {

    @Mapping(source = "contratEtablissement.contact", target = "contact")
    @Mapping(target = "contratEtablissement", source = "contratEtablissement", qualifiedByName = "toDTO")
    @Mapping(target = "roleContrat", constant = "Soumissionaire")
    ContactPrincipalDTO toDTO(ContratEtablissement contratEtablissement);
}
