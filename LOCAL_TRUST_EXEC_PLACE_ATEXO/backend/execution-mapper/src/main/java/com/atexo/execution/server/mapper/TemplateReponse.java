package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.Mpe;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TemplateReponse {

    @JsonProperty("mpe")
    private Mpe mpe;

    public Mpe getMpe() {
        return mpe;
    }

    public void setMpe(Mpe mpe) {
        this.mpe = mpe;
    }
}
