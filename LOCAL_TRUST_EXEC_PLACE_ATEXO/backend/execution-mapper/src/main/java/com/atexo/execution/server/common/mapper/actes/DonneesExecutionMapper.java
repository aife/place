package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.DonneesExecutionDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.common.mpe.ws.api.CodeFournisseurType;
import com.atexo.execution.server.common.mapper.*;
import com.atexo.execution.server.common.mapper.actes.impl.BaseActeMapper;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.DonneesEssentiellesMapper;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.mapper.RejetActeMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.actes.DonneesExecution;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DonneesEssentiellesMapper.class, MapperUtils.class, BaseActeMapper.class, RejetActeMapper.class, ChorusMapper.class, UtilisateurMapper.class, ReferentielMapper.class, DonneesEssentiellesMapper.class, EchangeChorusMapper.class, ContratMapper.class, DocumentActeMapper.class, ContactMapper.class})
public interface DonneesExecutionMapper extends ActeMappable<DonneesExecution, DonneesExecutionDTO> {

    @Mapping(target = "contrat", source = "contrat", qualifiedByName = "toFull")
    @Mapping(target = "type", source = "typeActe")
    @Mapping(target = "classname", expression = "java(DonneesExecutionDTO .class.getCanonicalName())")
    DonneesExecutionDTO toDTO(DonneesExecution donneesExecution);

    @Mapping(target = "contrat", source = "contrat")
    @Mapping(target = "documents", source = "documents")
    @Mapping(target = "titulaire", source = "titulaire")
    DonneesExecution createEntity(DonneesExecutionDTO donneesExecutionDTO);

    @Mapping(target = "idExterne", ignore = true)
    @Mapping(target = "statut", source = "acte.statut")
    @Mapping(target = "visaACCF", source = "acte.chorus.visaACCF")
    @Mapping(target = "visaPrefet", source = "acte.chorus.visaPrefet")
    @Mapping(target = "dateCreation", source = "acte.dateCreation")
    @Mapping(target = "dateModification", source = "acte.dateModification")
    @Mapping(target = "dateFinMarche", source = "acte.contrat.dateFinContrat")
    @Mapping(target = "reference", ignore = true)
    @Mapping(target = "titulaire", source = "acte.titulaire.etablissement")
    @Mapping(target = "typeFournisseurEntreprise", expression = "java(getCodeFournisseur(acte))")
    @Mapping(target = "acheteur", source = "acte.acheteur.identifiant")
    @Mapping(target = "type", ignore = true)
    ActeType toWS(DonneesExecution acte, EchangeChorus echange);

    default CodeFournisseurType getCodeFournisseur(Acte acte) {
        CodeFournisseurType result = null;
        // Fournisseur
        if (null != acte && null != acte.getChorus() && null != acte.getChorus().getTypeFournisseurEntreprise1()) {
            switch (acte.getChorus().getTypeFournisseurEntreprise1()) {
                case "TITULAIRE":
                default:
                    result = CodeFournisseurType.TITULAIRE;
                    break;
                case "CO-TRAITANT":
                    result = CodeFournisseurType.CO_TRAITANT;
                    break;
                case "SOUS-TRAITANT":
                    result = CodeFournisseurType.SOUS_TRAITANT;
                    break;
            }
        }
        return result;
    }

}
