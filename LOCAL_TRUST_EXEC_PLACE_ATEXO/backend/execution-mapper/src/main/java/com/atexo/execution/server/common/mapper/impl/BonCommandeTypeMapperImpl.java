package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.BonCommandeTypeDTO;
import com.atexo.execution.server.common.mapper.BonCommandeTypeMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.model.BonCommandeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BonCommandeTypeMapperImpl implements BonCommandeTypeMapper {

    @Autowired
    ReferentielMapper referentielMapper;

    @Override
    public BonCommandeTypeDTO toDTO(BonCommandeType bonCommandeType) {
        BonCommandeTypeDTO bonCommandeTypeDTO = new BonCommandeTypeDTO();
        bonCommandeTypeDTO.setNumero(bonCommandeType.getNumero());
        bonCommandeTypeDTO.setAdresseFacturation(bonCommandeType.getAdresseFacturation());
        bonCommandeTypeDTO.setAdresseLivExec(bonCommandeType.getAdresseLivExec());
        bonCommandeTypeDTO.setDelaiLivExec(bonCommandeType.getDelaiLivExec());
        bonCommandeTypeDTO.setDelaiPaiement(bonCommandeType.getDelaiPaiement());
        bonCommandeTypeDTO.setDevisTitulaireNum(bonCommandeType.getDevisTitulaireNum());
        bonCommandeTypeDTO.setImputationBudgetaire(bonCommandeType.getImputationBudgetaire());
        bonCommandeTypeDTO.setProgrammeNum(bonCommandeType.getProgrammeNum());
        bonCommandeTypeDTO.setEngagementNum(bonCommandeType.getEngagementNum());
        bonCommandeTypeDTO.setMontantTVA(bonCommandeType.getMontantTVA());
        bonCommandeTypeDTO.setMontantHT(bonCommandeType.getMontantHT());
        bonCommandeTypeDTO.setTauxTVA(referentielMapper.toDTO(bonCommandeType.getTauxTVA()));

        return bonCommandeTypeDTO;
    }

    @Override
    public BonCommandeType createEntity(BonCommandeTypeDTO bonCommandeTypeDTO) {
        BonCommandeType bonCommandeType = new BonCommandeType();
        bonCommandeType.setNumero(bonCommandeTypeDTO.getNumero());
        bonCommandeType.setAdresseFacturation(bonCommandeTypeDTO.getAdresseFacturation());
        bonCommandeType.setAdresseLivExec(bonCommandeTypeDTO.getAdresseLivExec());
        bonCommandeType.setDelaiLivExec(bonCommandeTypeDTO.getDelaiLivExec());
        bonCommandeType.setDelaiPaiement(bonCommandeTypeDTO.getDelaiPaiement());
        bonCommandeType.setDevisTitulaireNum(bonCommandeTypeDTO.getDevisTitulaireNum());
        bonCommandeType.setImputationBudgetaire(bonCommandeTypeDTO.getImputationBudgetaire());
        bonCommandeType.setProgrammeNum(bonCommandeTypeDTO.getProgrammeNum());
        bonCommandeType.setEngagementNum(bonCommandeTypeDTO.getEngagementNum());
        bonCommandeType.setMontantTVA(bonCommandeTypeDTO.getMontantTVA());
        bonCommandeType.setMontantHT(bonCommandeTypeDTO.getMontantHT());

        return bonCommandeType;
    }
}
