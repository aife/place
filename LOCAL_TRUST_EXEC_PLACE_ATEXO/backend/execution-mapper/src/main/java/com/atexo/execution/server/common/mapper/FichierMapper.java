package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.FichierDTO;
import com.atexo.execution.server.model.Fichier;

public interface FichierMapper {

	FichierDTO toDTO(Fichier fichier);

    Fichier createEntity(FichierDTO fichierDTO);

}
