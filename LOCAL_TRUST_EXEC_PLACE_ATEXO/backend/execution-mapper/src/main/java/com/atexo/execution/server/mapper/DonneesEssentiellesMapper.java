package com.atexo.execution.server.mapper;


import com.atexo.execution.common.dto.DonneesEssentiellesCriteriaDTO;
import com.atexo.execution.common.dto.DonneesEssentiellesDTO;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.paginable.criteres.DonneesEssentiellesCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface DonneesEssentiellesMapper {

    @Mapping(source = "id", target = "id")
    DonneesEssentiellesDTO toDto(DonneesEssentielles donneesEssentielles);

    List<DonneesEssentiellesDTO> toListDto(Set<DonneesEssentielles> donneesEssentielles);

    @Mapping(target = "utilisateur", source = "utilisateur")
    DonneesEssentiellesCriteria toCriteria(Utilisateur utilisateur, DonneesEssentiellesCriteriaDTO donneesEssentiellesCriteriaDTO);

    @Mapping(target = "statut", source = "contrat.donneesEssentielles.statut")
    @Mapping(target = "datePublication", source = "contrat.donneesEssentielles.datePublication")
    @Mapping(target = "idDonneeEssentielle", source = "contrat.donneesEssentielles.id")
    @Mapping(target = "idContrat", source = "contrat.id")
    @Mapping(target = "typeContrat", source = "contrat.type.label")
    @Mapping(target = "idActe", source = "acte.id")
    @Mapping(target = "typeActe", source = "acte.typeActe.label")
    @Mapping(target = "dateNotificationContrat", source = "contrat.dateNotification")
    @Mapping(target = "dateEnvoi", source = "contrat.donneesEssentielles.datePublication")
    @Mapping(target = "message", source = "contrat.donneesEssentielles.erreurPublication")
    @Mapping(target = "fichier", source = "contrat.donneesEssentielles.nomFichier")
    @Mapping(target = "erreurPublication", source = "contrat.donneesEssentielles.erreurPublication")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "dateNotificationActe", source = "acte.dateNotification")
    @Mapping(target = "modeEnvoiDonneesEssentielles", source = "contrat.donneesEssentielles.modeEnvoiDonneesEssentielles")
    @Mapping(target = "idExterne", source = "acte.idExterne")
    DonneesEssentiellesDTO contratToDonneeEssentielleDTO1(Contrat contrat, Acte acte);
}
