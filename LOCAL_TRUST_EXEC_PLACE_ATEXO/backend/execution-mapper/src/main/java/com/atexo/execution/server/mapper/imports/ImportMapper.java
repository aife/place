package com.atexo.execution.server.mapper.imports;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.common.dto.ModificationContrat;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.model.actes.ActeModificatif;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface ImportMapper {

    String AVEC_INCIDENCE = "AVEC_INCIDENCE";
    String SANS_INCIDENCE = "SANS_INCIDENCE";

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "objet", source = "modificationContrat.objetModification")
    @Mapping(target = "statut", source = "modificationContrat", qualifiedByName = "toStatut")
    @Mapping(target = "numero", ignore = true)
    @Mapping(target = "typeActe", source = "typeActe")
    @Mapping(target = "acheteur", source = "utilisateur")
    @Mapping(target = "titulaire", source = "contact")
    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "commentaire", ignore = true)
    @Mapping(target = "contrat", source = "contrat")
    @Mapping(target = "chorus", ignore = true)
    @Mapping(target = "dateNotification", source = "modificationContrat.dateSignature")
    @Mapping(target = "donneesEssentielles", source = "contrat.donneesEssentielles")
    @Mapping(target = "echangeChorus", ignore = true)
    @Mapping(target = "typeNotification", ignore = true)
    @Mapping(target = "validationChorus", ignore = true)
    @Mapping(target = "statutPublication", source = "modificationContrat", qualifiedByName = "toStatutPublication")
    @Mapping(target = "datePublication", source = "modificationContrat.datePublicationSn")
    @Mapping(target = "numeroOrdre", source = "modificationContrat.numOrdre")
    @Mapping(target = "rejet", ignore = true)
    @Mapping(target = "dureeAjoureeContrat", source = "modificationContrat.dureeMarche")
    @Mapping(target = "nouvelleFinContrat", ignore = true)
    @Mapping(target = "publicationDonneesEssentielles", constant = "true")
    @Mapping(target = "clauseReexamen", ignore = true)
    @Mapping(target = "typeModification", source = "modificationContrat", qualifiedByName = "toTypeModification")
    @Mapping(target = "montantHTChiffre", source = "modificationContrat.montant")
    @Mapping(target = "montantTTCChiffre", ignore = true)
    @Mapping(target = "tauxTVA", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "utilisateurCreation", ignore = true)
    @Mapping(target = "dateModification", source = "modificationContrat.dateModificationSn")
    @Mapping(target = "utilisateurModification", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "idExterne", source = "modificationContrat.id")
    @Mapping(target = "dateModificationExterne", ignore = true)
    @Mapping(target = "plateforme", source = "contrat.plateforme")
    ActeModificatif modificationContratToActeModificatif(ModificationContrat modificationContrat, Contrat contrat, Utilisateur utilisateur, Contact contact, TypeActe typeActe);

    @Named("toStatut")
    default StatutActe toStatut(ModificationContrat modificationContrat) {
        return StatutActe.NOTIFIE;
    }

    @Named("toStatutPublication")
    default StatutAvenant toStatutPublication(ModificationContrat modificationContrat) {
        return Optional.ofNullable(modificationContrat)
                .map(ModificationContrat::getStatutPublicationSn)
                .map(statut -> statut == 1 ? StatutAvenant.PUBLIE : StatutAvenant.A_PUBLIER)
                .orElse(null);
    }

    @Named("toTypeModification")
    default String toTypeModification(ModificationContrat modificationContrat) {
        return Optional.ofNullable(modificationContrat)
                .map(ModificationContrat::getMontant)
                .map(montant -> montant > 0 ? AVEC_INCIDENCE : SANS_INCIDENCE)
                .orElse(null);
    }

}
