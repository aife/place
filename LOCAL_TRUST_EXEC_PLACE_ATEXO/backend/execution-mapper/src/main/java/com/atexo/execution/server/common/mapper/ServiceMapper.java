package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.EntiteServiceDTO;
import com.atexo.execution.common.dto.ServiceDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, OrganismeMapper.class})
public interface ServiceMapper {

	@Mapping(target = "organismeUid", source = "organisme.uuid")
	ServiceDTO toDTO(Service service);

	@Mapping(target = "organismeUid", source = "organisme.uuid")
	EntiteServiceDTO toTreeDTO(Service service);
}
