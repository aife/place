package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.ActeAutreDTO;
import com.atexo.execution.server.model.actes.ActeAutre;

public interface ActeAutreMapper extends ActeMappable<ActeAutre, ActeAutreDTO> {
}
