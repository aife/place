package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.AvenantTypeDTO;
import com.atexo.execution.server.common.mapper.AvenantTypeMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.AvenantType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AvenantTypeMapperImpl implements AvenantTypeMapper {

    @Autowired
    ReferentielMapper referentielMapper;

    @Override
    public AvenantTypeDTO toDTO(AvenantType avenantType) {
        AvenantTypeDTO avenantTypeDTO = new AvenantTypeDTO();
        avenantTypeDTO.setNumero(avenantType.getNumero());
        avenantTypeDTO.setAutreType(avenantType.getAutreType());
        avenantTypeDTO.setDateDemaragePrestation(MapperUtils.convertToDate(avenantType.getDateDemaragePrestation()));
        avenantTypeDTO.setDateDemaragePrestationNew(MapperUtils.convertToDate(avenantType.getDateDemaragePrestationNew()));
        avenantTypeDTO.setDateFin(MapperUtils.convertToDate(avenantType.getDateFin()));
        avenantTypeDTO.setDateFinNew(MapperUtils.convertToDate(avenantType.getDateFinNew()));
        avenantTypeDTO.setDateFinMax(MapperUtils.convertToDate(avenantType.getDateFinMax()));
        avenantTypeDTO.setDateFinMaxNew(MapperUtils.convertToDate(avenantType.getDateFinMaxNew()));
        avenantTypeDTO.setIncidence(avenantType.getIncidence());
        avenantTypeDTO.setAvenantTransfert(avenantType.getAvenantTransfert());
        avenantTypeDTO.setMontant(avenantType.getMontant());
        avenantTypeDTO.setDateModifiable(avenantType.getDateModifiable());
        avenantTypeDTO.setValue(avenantType.getValue());
        if (avenantType.getTypeAvenant() != null) {
            avenantTypeDTO.setTypeAvenant(referentielMapper.toDTO(avenantType.getTypeAvenant()));
        }

        return avenantTypeDTO;
    }

    @Override
    public AvenantType createEntity(AvenantTypeDTO avenantTypeDTO) {
        AvenantType avenantType = new AvenantType();
        avenantType.setNumero(avenantTypeDTO.getNumero());
        avenantType.setAutreType(avenantTypeDTO.getAutreType());
        avenantType.setDateDemaragePrestation(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateDemaragePrestation()));
        avenantType.setDateDemaragePrestationNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateDemaragePrestationNew()));
        avenantType.setDateFin(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFin()));
        avenantType.setDateFinNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinNew()));
        avenantType.setDateFinMax(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinMax()));
        avenantType.setDateFinMaxNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinMaxNew()));
        avenantType.setIncidence(avenantTypeDTO.getIncidence());
        avenantType.setAvenantTransfert(avenantTypeDTO.getAvenantTransfert());
        avenantType.setMontant(avenantTypeDTO.getMontant());
        avenantType.setDateModifiable(avenantTypeDTO.getDateModifiable());
        avenantType.setValue(avenantTypeDTO.getValue());

        return avenantType;
    }

}
