package com.atexo.execution.server.common.mapper.contrat.appach;


import com.atexo.execution.common.appach.model.contrat.titulaire.Titulaire;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Etablissement;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AppachTitulaireMapper {

    String SIRET = "SIRET";
    String HORS_UE = "Hors UE";
    String ENTREPRISE = "entreprises";
    String ETABLISSEMENT = "etablissements";
    String URI_FORMAT = "/api/v2/{0}/{1}";
    String COMMUNICATION_ENQUETE = "COMMUNICATION_ENQUETE";
    String COMMUNICATION_SIA = "COMMUNICATION_SIA";
    String COMMUNICATION_PLACE = "COMMUNICATION_PLACE";


    @Mapping(target = "denominationSociale", source = "etablissement.fournisseur.raisonSociale")
    @Mapping(target = "typeIdentifiantEntreprise", source = "contratEtablissement", qualifiedByName = "toTypeIdentifiantEntreprise")
    @Mapping(target = "nom", source = "contact.nom")
    @Mapping(target = "prenom", source = "contact.prenom")
    @Mapping(target = "email", source = "contact.email")
    @Mapping(target = "telephone", source = "contact.telephone")
    @Mapping(target = "identifiantTechnique", source = "contact", qualifiedByName = "toIdentifiantTechnique")
    @Mapping(target = "adresse", source = "etablissement.adresse.adresse")
    @Mapping(target = "codePostal", source = "etablissement.adresse.codePostal")
    @Mapping(target = "commune", source = "etablissement.adresse.commune")
    @Mapping(target = "pays", source = "etablissement.adresse.pays")
    @Mapping(target = "actif", source = "contact.actif", qualifiedByName = "toActif")
    @Mapping(target = "siren", source = "etablissement.fournisseur.siren")
    @Mapping(target = "codeEtablissement", source = "etablissement", qualifiedByName = "toCodeEtablissement")
    @Mapping(target = "etablissement", source = "contratEtablissement", qualifiedByName = "toEtablissement")
    @Mapping(target = "entreprise", source = "contratEtablissement", qualifiedByName = "toEntreprise")
    @Mapping(target = "identifiantNationalEntreprise", source = "etablissement.siret")
    @Mapping(target = "inscritAnnuaireDefense", source = "contact.inscritAnnuaireDefense", qualifiedByName = "toInscritAnnuaireDefense")
    @Mapping(target = "dateModificationRgpd", source = "contact.dateModificationRgpd")
    @Mapping(target = "rgpd", source = "contact", qualifiedByName = "toRgpd")
    @Mapping(target = "login", source = "contact.login")
    Titulaire toTitulaire(ContratEtablissement contratEtablissement);

    @Named("toActif")
    default Integer toActif(boolean actif) {
        return actif ? 1 : 0;
    }

    @Named("toTypeIdentifiantEntreprise")
    default String toTypeIdentifiantEntreprise(ContratEtablissement contratEtablissement) {
        return Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(Etablissement::getSiret)
                .map(e -> SIRET)
                .orElse(HORS_UE);
    }

    @Named("toEntreprise")
    default String toEntreprise(ContratEtablissement contratEtablissement) {
        return Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(Etablissement::getFournisseur)
                .map(fournisseur -> {
                    if(!StringUtils.isEmpty(fournisseur.getIdExterne())){
                        return fournisseur.getIdExterne();
                    }
                    return String.valueOf(fournisseur.getId());
                })
                .orElse(null);
    }

    @Named("toEtablissement")
    default String toEtablissement(ContratEtablissement contratEtablissement) {
       return Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(etablissement -> {
                    if(!StringUtils.isEmpty(etablissement.getIdExterne())){
                        return etablissement.getIdExterne();
                    }
                    return String.valueOf(etablissement.getId());
                })
                .orElse(null);
    }

    @Named("toInscritAnnuaireDefense")
    default String toInscritAnnuaireDefense(Boolean inscritAnnuaireDefense) {
        return Boolean.TRUE.equals(inscritAnnuaireDefense) ? "1" : "0";
    }

    @Named("toRgpd")
    default List<String> toRgpd(Contact contact) {
        if (contact == null) {
            return null;
        }
        return Stream.of(
                        Boolean.TRUE.equals(contact.getRgpdEnquete()) ? COMMUNICATION_ENQUETE : null,
                        Boolean.TRUE.equals(contact.getRgpdCommunication()) ? COMMUNICATION_SIA : null,
                        Boolean.TRUE.equals(contact.getRgpdCommunicationPlace()) ? COMMUNICATION_PLACE : null
                )
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Named("toCodeEtablissement")
    default String toCodeEtablissement(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map(Etablissement::getSiret)
                .map(e -> StringUtils.right(e, 5))
                .orElse(null);
    }

    @Named("toIdentifiantTechnique")
    default Integer toIdentifiantTechnique(Contact contact) {
        return Optional.ofNullable(contact)
                .map(c -> c.getIdExterne() != null && NumberUtils.isNumber(c.getIdExterne()) ? Integer.parseInt(c.getIdExterne()) : c.getId().intValue())
                .orElse(null);
    }

}
