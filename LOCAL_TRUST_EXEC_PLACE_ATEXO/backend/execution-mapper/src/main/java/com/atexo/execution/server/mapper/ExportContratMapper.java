package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.exports.ExportContratDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import fr.atexo.execution.tncp.commun.ExportEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface ExportContratMapper {

    String CLAUSES_SOCIALES = "clausesSociales";
    String CLAUSE_ENVIRONNEMENTALE = "clauseEnvironnementale";

    @Mapping(target = "numeroContrat", source = "numero", defaultValue = "")
    @Mapping(target = "referenceLibre", source = "referenceLibre", defaultValue = "")
    @Mapping(target = "intitule", source = "intitule", defaultValue = "")
    @Mapping(target = "objetConsultation", source = "consultation.objet", defaultValue = "")
    @Mapping(target = "objetMarche", source = "objet", defaultValue = "")
    @Mapping(target = "typeContrat", source = "type.label", defaultValue = "")
    @Mapping(target = "statutContrat", source = "statut", defaultValue = "")
    @Mapping(target = "accordCadreOUSADLie", source = "lienAcSad.numero", defaultValue = "")
    @Mapping(target = "numeroChapeau", source = "contratChapeau.numero", defaultValue = "")
    @Mapping(target = "procedurePassation", source = "consultation.procedure.label", defaultValue = "")
    @Mapping(target = "referenceConsultation", source = "consultation.numero", defaultValue = "")
    @Mapping(target = "nomEntrepriseAttributaire", source = "attributaire.etablissement.fournisseur.raisonSociale", defaultValue = "")
    @Mapping(target = "siretAttributaire", source = "attributaire.etablissement.siret", defaultValue = "")
    @Mapping(target = "montantContratAttribue", source = "montant", defaultValue = "")
    @Mapping(target = "montantContratFacture", source = "montantFacture", defaultValue = "")
    @Mapping(target = "montantContratMandate", source = "montantMandate", defaultValue = "")
    @Mapping(target = "montantMaximumEstime", ignore = true)
    @Mapping(target = "formeDuPrix", source = "typeFormePrix.label", defaultValue = "")
    @Mapping(target = "categorie", source = "categorie.label", defaultValue = "")
    @Mapping(target = "cpvPrincipal", source = "codesCpv", defaultValue = "")
    @Mapping(target = "lieuPrincipalExecution", source = "lieuxExecution", defaultValue = "")
    @Mapping(target = "dateAttribution", source = "consultation.decisionAttribution", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "datePrevisionnelleNotification", source = "datePrevisionnelleNotification", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "datePrevisionnelleFinContrat", source = "datePrevisionnelleFinMarche", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "datePrevisionnelleMaximaleFinContrat", source = "datePrevisionnelleFinMaximaleMarche", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dateNotification", source = "dateNotification", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dateFinContrat", source = "dateFinContrat", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dateMaximaleFinContrat", source = "dateMaxFinContrat", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dureeMaximaleMarche", source = "dureeMaximaleMarche", defaultValue = "")
    @Mapping(target = "entitePublique", source = "service.organisme.nom", defaultValue = "")
    @Mapping(target = "service", source = "service.nom", defaultValue = "")
    @Mapping(target = "nomAagentGestionnaire", source = "createur.nom", defaultValue = "")
    @Mapping(target = "prenomAagenGestionnaire", source = "createur.prenom", defaultValue = "")
    @Mapping(target = "donneesEssentiellesPublicationContrat", source = "publicationDonneesEssentielles", defaultValue = "")
    @Mapping(target = "ccagApplicable", source = "ccagApplicable.label", defaultValue = "")
    @Mapping(target = "achatResponsable", source = "achatResponsable", defaultValue = "")
    @Mapping(target = "trancheBudgetaire", source = "trancheBudgetaire", defaultValue = "")
    @Mapping(target = "modaliteRevisionPrix", source = "revisionPrix.label", defaultValue = "")
    @Mapping(target = "contratARenouveler", source = "ARenouveler", defaultValue = "")
    @Mapping(target = "createur", expression = "java(toCreateur(contrat))")
    @Mapping(target = "organisme", source = "service.organisme.nom", defaultValue = "")
    @Mapping(target = "dateDemarrage", source = "dateDemaragePrestation", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dateExecution", source = "dateDebutExecution", dateFormat = "dd/MM/yyyy", defaultValue = "")
    @Mapping(target = "dureeRenouvellement", source = "dureePhaseEtudeRenouvellement", defaultValue = "")
    @Mapping(target = "organismesEligibles", expression = "java(toOrganismesEligibles(contrat))")
    @Mapping(target = "montantTotalAvenants", expression = "java(toMontantTotalAvenants(contrat))")
    @Mapping(target = "typeBornes", source = "typeBorne", defaultValue = "")
    @Mapping(target = "borneMinmale", source = "borneMinimale", defaultValue = "")
    @Mapping(target = "borneMaximale", source = "borneMaximale", defaultValue = "")
    @Mapping(target = "montantTotal", expression = "java(toMontantTotal(contrat))")
    @Mapping(target = "typesPrix", expression = "java(toTypesPrix(contrat))")
    @Mapping(target = "pme", expression = "java(verfiePme(contrat))")
    @Mapping(target = "considerationsSociales", expression = "java(verfieClauseSociales(contrat))")
    @Mapping(target = "considerationsEnvironnementales", expression = "java(verfieClauseEnvironementales(contrat))")
    @Mapping(target = "numeroLongContrat", source = "numeroLong")
    @Mapping(target = "numeroCourt", source = "numero")
    @Mapping(target = "statutPublicationDonnesEssentielles", source = "donneesEssentielles.statut")
    ExportContratDTO toDto(Contrat contrat);

    default String toOrganismesEligibles(Contrat contrat) {
        if (contrat.getOrganismesEligibles() == null || contrat.getOrganismesEligibles().isEmpty())
            return "";
        return contrat.getOrganismesEligibles().stream().map(Organisme::getNom).collect(Collectors.joining(","));
    }

    default String toCreateur(Contrat contrat) {
        return Optional.ofNullable(contrat.getCreateur()).map(createur -> createur.getNom() + " " + createur.getPrenom()).orElse("");
    }

    default String toMontantTotalAvenants(Contrat contrat) {
        var montant = MapperUtils.getMontantTotalAvenants(contrat);
        return montant != null ? montant.toString() : "";
    }

    default String toMontantTotal(Contrat contrat) {
        var montant = Optional.ofNullable(contrat.getMontant()).orElse(new BigDecimal(0)).add(MapperUtils.getMontantTotalAvenants(contrat));
        return montant.toString();
    }

    default Set<String> toTypesPrix(Contrat contrat) {
        if (null == contrat || null == contrat.getConsultation() || null == contrat.getConsultation().getTypesPrix()) {
            return new HashSet<>();
        }

        return contrat.getConsultation().getTypesPrix().stream().map(TypePrix::getLabel).collect(Collectors.toSet());
    }

    default String verfieClauseSociales(Contrat contrat) {
        if (contrat == null ||contrat.getContratClauses() == null || contrat.getContratClauses().isEmpty()) {
            return ExportEnum.Non.name();
        }
        for (ContratClause clause : contrat.getContratClauses()) {
            if (clause.getClause().getCode().equalsIgnoreCase(CLAUSES_SOCIALES)) {
                return ExportEnum.Oui.name();
            }
        }
        return ExportEnum.Non.name();
    }

    default String verfieClauseEnvironementales(Contrat contrat) {
        if (contrat == null || contrat.getContratClauses() == null || contrat.getContratClauses().isEmpty()) {
            return ExportEnum.Non.name();
        }
        for (ContratClause clause : contrat.getContratClauses()) {
            if (clause.getClause().getCode().equalsIgnoreCase(CLAUSE_ENVIRONNEMENTALE)) {
                return ExportEnum.Oui.name();
            }
        }
        return ExportEnum.Non.name();
    }

    default String verfiePme(Contrat contrat) {
        if (contrat == null || contrat.getContratEtablissements() == null || contrat.getContratEtablissements().isEmpty()) {
            return ExportEnum.Na.name();
        }
        for (ContratEtablissement contratEtablissement : contrat.getContratEtablissements()) {
            if (contratEtablissement.getEtablissement() != null && contratEtablissement.getEtablissement().getFournisseur() != null && contratEtablissement.getEtablissement().getFournisseur().getCategorie() != null) {
                if (contratEtablissement.getEtablissement().getFournisseur().getCategorie().getCode().equalsIgnoreCase("PME")) {
                    return ExportEnum.Oui.name();
                }
            }
        }
        return ExportEnum.Non.name();
    }


}
