package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.DecisionReconductionDTO;
import com.atexo.execution.server.model.actes.DecisionReconduction;

public interface DecisionReconductionMapper extends ActeMappable<DecisionReconduction, DecisionReconductionDTO> {
}
