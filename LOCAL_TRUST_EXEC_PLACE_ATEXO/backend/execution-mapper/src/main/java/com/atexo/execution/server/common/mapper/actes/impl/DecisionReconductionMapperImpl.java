package com.atexo.execution.server.common.mapper.actes.impl;

import com.atexo.execution.common.dto.actes.DecisionReconductionDTO;
import com.atexo.execution.common.mpe.ws.api.ActeType;
import com.atexo.execution.server.common.mapper.ChorusMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.common.mapper.actes.DecisionReconductionMapper;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.DonneesEssentiellesMapper;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.mapper.RejetActeMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.actes.DecisionReconduction;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class DecisionReconductionMapperImpl extends BaseActeMapper  implements DecisionReconductionMapper {

	@Autowired
	private EtablissementRepository etablissementRepository;

	@Autowired
	private ContratRepository contratRepository;

	@Autowired
	private TypeActeRepository typeActeRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

    @Autowired
    private EtablissementMapper etablissementMapper;

	@Autowired
	private UtilisateurMapper utilisateurMapper;

    @Autowired
    private ReferentielMapper referentielMapper;

	@Autowired
	private DonneesEssentiellesMapper donneesEssentiellesMapper;

	@Autowired
	private EchangeChorusMapper echangeChorusMapper;

	@Autowired
	private ContratMapper contratMapper;

	@Autowired
	private RejetActeMapper rejetActeMapper;

	@Autowired
	private ChorusMapper chorusMapper;

	@Override
	public DecisionReconductionDTO toDTO( DecisionReconduction acte ) {
		if (null == acte) {
			return null;
		}

		// Commun
		var acteDTO = new DecisionReconductionDTO();
		acteDTO.setId(acte.getId());
		acteDTO.setUuid(acte.getUuid());
		acteDTO.setVersion(acte.getVersion());
		acteDTO.setObjet(acte.getObjet());
		acteDTO.setStatut(acte.getStatut());
		acteDTO.setNumero(acte.getNumero());
		acteDTO.setAcheteur(utilisateurMapper.toDTO(acte.getAcheteur()));
		acteDTO.setTitulaire(map(acte.getTitulaire()));
		acteDTO.setCommentaire(acte.getCommentaire());
        acteDTO.setEchangeChorus(echangeChorusMapper.toDto(acte.getEchangeChorus()));
		acteDTO.setDateCreation(acte.getDateCreation());
		acteDTO.setDateModification(acte.getDateModification());
		acteDTO.setDateNotification(MapperUtils.convert(acte.getDateNotification()));
		acteDTO.setDocuments(documentListToDocumentDTOList(acte.getDocuments()));
		acteDTO.setDateCreation(acte.getDateCreation());
		acteDTO.setDonneesEssentielles(donneesEssentiellesMapper.toDto(acte.getDonneesEssentielles()));
        acteDTO.setContrat(contratMapper.toDto(acte.getContrat()));
		acteDTO.setTypeNotification(acte.getTypeNotification());
		acteDTO.setValidationChorus(acte.isValidationChorus());
		acteDTO.setType(this.typeActeRepository.findAll().stream().filter(typeActe -> DecisionReconduction.CODE.equals(typeActe.getCode())).map(referentielMapper::toDTO).findFirst().orElse(null));
		acteDTO.setClassname(DecisionReconductionDTO.class.getCanonicalName());

		// Specifique
		acteDTO.setReconduction(acte.getReconduction());
		acteDTO.setDebut(acte.getDebut());
		acteDTO.setFin(acte.getFin());

		if (null != acte.getRejet()) {
			acteDTO.setRejet(rejetActeMapper.toDto(acte.getRejet()));
		}

		acteDTO.setChorus(chorusMapper.toDTO(acte.getChorus()));

		return acteDTO;
	}


    @Override
    public DecisionReconduction createEntity(DecisionReconductionDTO acteDTO) {
        if (null == acteDTO) {
            return null;
        }

		var acte = new DecisionReconduction();

		// Commun
	    if (null!=acteDTO.getVersion()) {
		    acte.setVersion(acteDTO.getVersion());
	    }

	    acte.setId(acteDTO.getId());
	    acte.setUuid(acteDTO.getUuid());
	    acte.setObjet(acteDTO.getObjet());
	    acte.setStatut(acteDTO.getStatut());
	    acte.setNumero(acteDTO.getNumero());
        acte.setTypeActe(typeActeRepository.findByCode(DecisionReconduction.CODE).orElse(null));
        utilisateurRepository.findByUuid(acteDTO.getAcheteur().getUuid()).ifPresent(
                acheteur -> acte.setAcheteur(acheteur)
        );

	    acte.setTitulaire(map(acteDTO.getTitulaire()));
	    acte.setCommentaire(acteDTO.getCommentaire());
	    contratRepository.findById(acteDTO.getContrat().getId()).ifPresent(acte::setContrat);
		acte.setPlateforme(acte.getContrat().getPlateforme());
		acte.setStatut(acteDTO.getStatut());
	    acte.getDocuments().forEach(
		    documentActe -> documentActe.setActe(acte)
	    );

	    acte.setDateNotification(MapperUtils.convert(acteDTO.getDateNotification()));
        acte.setEchangeChorus(echangeChorusMapper.createEntity(acteDTO.getEchangeChorus()));
	    acte.setTypeNotification(acteDTO.getTypeNotification());
	    acte.setValidationChorus(acteDTO.isValidationChorus());

		// Specifique
	    acte.setReconduction(acteDTO.getReconduction());
	    acte.setDebut(acteDTO.getDebut());
	    acte.setFin(acteDTO.getFin());

	    if (null != acteDTO.getRejet()) {
		    acte.setRejet(rejetActeMapper.createEntity(acteDTO.getRejet()));
	    }

	    acte.setChorus(chorusMapper.createEntity(acteDTO.getChorus()));

        return acte;
    }

	@Override
	public ActeType toWS( DecisionReconduction acte, EchangeChorus echange ) {
		var result = new ActeType();

		var type = toWS(TypeActeMPE.DECISION_RECONDUCTION);
		// commun
		result.setType(type);
		result.setObjet(acte.getObjet());
		result.setNumero(acte.getNumero());
		result.setAcheteur(acte.getAcheteur().getIdentifiant());
		result.setStatut(acte.getStatut().name());
		if (null!=acte.getTitulaire()) {
			result.setTitulaire(toWS(acte.getTitulaire().getEtablissement()));
		}
		result.setCommentaire(acte.getCommentaire());
		result.setDateNotification(MapperUtils.toXmlGregorianCalendar(acte.getDateNotification()));
		result.setDateNotificationPrevisionnelle(MapperUtils.toXmlGregorianCalendar(echange.getDateNotificationPrevisionnelle()));

		if (null!=acte.getChorus()) {
			result.setVisaACCF(acte.getChorus().getVisaACCF());
			result.setVisaPrefet(acte.getChorus().getVisaPrefet());
		}

		result.setTypeFournisseurEntreprise(getCodeFournisseur(acte));

		result.setDateCreation(MapperUtils.toXmlGregorianCalendar(acte.getDateCreation()));
		result.setDateModification(MapperUtils.toXmlGregorianCalendar(acte.getDateModification()));
		result.setDateFinMarche(MapperUtils.toXmlGregorianCalendar(acte.getContrat().getDateFinContrat()));
		// décision reconduction
		result.setReconduction(acte.getReconduction());

		return result;
	}
}
