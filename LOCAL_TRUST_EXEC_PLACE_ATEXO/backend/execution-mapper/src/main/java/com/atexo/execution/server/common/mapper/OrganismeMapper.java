package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Organisme;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface OrganismeMapper {

	OrganismeDTO toDTO(Organisme organisme);
}
