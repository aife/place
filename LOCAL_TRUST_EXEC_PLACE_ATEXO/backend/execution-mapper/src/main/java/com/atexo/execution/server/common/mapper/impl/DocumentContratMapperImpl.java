package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.server.common.mapper.DocumentContratMapper;
import com.atexo.execution.server.common.mapper.DocumentContratMapperDecorator;
import com.atexo.execution.server.model.DocumentContrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
@Primary
public class DocumentContratMapperImpl extends DocumentContratMapperDecorator implements DocumentContratMapper {

    @Autowired
    @Qualifier("delegate")
    private DocumentContratMapper delegate;

    @Override
    public DocumentContrat createEntity( DocumentContratDTO documentsDTO) {
        return delegate.createEntity(documentsDTO);
    }
}
