package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.EtablissementType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Etablissement;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface EtablissementMapperV2 {

    EtablissementType toWS(Etablissement etablissement);
}
