package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.HabilitationDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Habilitation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface HabilitationMapper {

    HabilitationDTO toDTO(Habilitation habilitation);

    Habilitation createEntity(HabilitationDTO habilitationDTO);

}
