package com.atexo.execution.server.mapper.tncp;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratClause;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.model.concession.NatureContratConcession;
import fr.atexo.execution.tncp.envoi.concession.json.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface TNCPConcessionMapper {
    String SAVE_CONCESSION_DATA = "creerDECC";
    String CONSIDERATION_SOCIALE = "consideration-sociale";
    String CONSIDERATION_ENVIRONNEMENTALE = "consideration-environnementale";
    String NATURE_CONCESSION = "nature-concession";
    String CPV = "cpv";
    String AUCUNE_CLAUSE = "aucune";
    String DATE_FORMAT = "yyyy-MM-dd";
    String SIRET = "SIRET";


    @Mapping(target = "plateforme.idPlateforme", source = "contract.plateforme.tncpIdentifiant")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "contract.plateforme.tncpIdentifiantTechnique")
    @Mapping(target = "rsDemandeur", expression = "java(java.util.Optional.ofNullable(contract).map(Contrat::getService).map(Service::getOrganisme).map(Organisme::getNom).map(rs -> (rs !=null && rs.length() > 100 ? rs.substring(0,100) : rs )).orElse(null))")
    @Mapping(target = "idDemandeur", source = "contract.service.organisme.siret")
    @Mapping(target = "operation", constant = SAVE_CONCESSION_DATA)
    @Mapping(target = "donneeEssentielle", expression = "java(toDonneeEssentielle(contract,map))")
    TNCPConcession toConcession(Contrat contract, Map<String, Map<String, String>> map);

    @Mapping(target = "nature", expression = "java(toNature(contract,map.get(NATURE_CONCESSION)))")
    @Mapping(target = "statutDE", constant = "A_TRANSMETTRE")
    @Mapping(target = "id", source = "contract.donneesEssentielles.id")
    @Mapping(target = "objet", source = "contract.objet")
    @Mapping(target = "procedure", source = "contract.consultation.procedure.label")
    @Mapping(target = "dureeMois", source = "contract.dureeMaximaleMarche")
    @Mapping(target = "considerationsSociales", expression = "java(toConsiderationsSociales(contract,map.get(CONSIDERATION_SOCIALE)))")
    @Mapping(target = "considerationsEnvironnementales", expression = "java(toConsiderationsEnvironnementales(contract,map.get(CONSIDERATION_ENVIRONNEMENTALE)))")
    @Mapping(target = "datePublicationDonnees", expression = "java(getDatePublication())", dateFormat = DATE_FORMAT)
    @Mapping(target = "donneesCC", expression = "java(toDonneesCC(contract))")
    @Mapping(target = "modifications", expression = "java(toModifications(contract))")
    fr.atexo.execution.tncp.envoi.concession.json.DonneeEssentielle toDonneeEssentielle(Contrat contract, Map<String, Map<String, String>> map);

    @Mapping(target = "idAutoriteConcedante", source = "contrat.service.organisme.siret")
    @Mapping(target = "dateDebutExecution", source = "contrat.dateDebutExecution", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "dateSignature", source = "contrat.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "valeurGlobale", source = "contrat.valeurGlobale")
    @Mapping(target = "montantSubventionPublique", source = "contrat.montantSubventionPublique")
    @Mapping(target = "concessionnaires", source = "contrat", qualifiedByName = "toConcessionnaires")
    @Mapping(target = "donneesExecution", source = "contrat", qualifiedByName = "toDonneesExecutions")
    DonneesCC toDonneesCC(Contrat contrat);

    @Mapping(target = "id", source = "etablissement.siret")
    @Mapping(target = "typeIdentifiant", constant = SIRET)
    Concessionnaire toConcessionnaire(ContratEtablissement contratEtablissement);

    @Mapping(target = "datePublicationDonneesExecution", source = "donneesExecution.contrat.dateNotification")
    @Mapping(target = "depensesInvestissement", source = "depensesInvestissement")
    @Mapping(target = "tarifs", source = "donneesExecution")
    DonneesExecution toDonneesExecution(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "intituleTarif", source = "intituleTarif")
    @Mapping(target = "tarif", source = "tarif")
    Tarif toTarif(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "id", source = "acteModificatif.id")
    @Mapping(target = "dureeMois", source = "acteModificatif.contrat.dureeMaximaleMarche")
    @Mapping(target = "valeurGlobale", source = "acteModificatif.montantHTChiffre")
    @Mapping(target = "dateSignatureModification", source = "acteModificatif.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "datePublicationDonneesModification", source = "acteModificatif.contrat.donneesEssentielles.datePublication", dateFormat = "yyyy-MM-dd")
    Modification toModification(ActeModificatif acteModificatif);

    default String toConsiderationsSociales(Contrat contrat, Map<String, String> mapConsiderationSocaile) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(getAucuneClauseSet())
                .stream()
                .map(e -> mapConsiderationSocaile.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.joining(";"));
    }

    default HashSet<ContratClause> getAucuneClauseSet() {
        var emptySet = new HashSet<ContratClause>();
        var emptyClause = new Clause();
        emptyClause.setCode(AUCUNE_CLAUSE);
        var contraClause = new ContratClause();
        contraClause.setClause(emptyClause);
        emptySet.add(new ContratClause());
        return emptySet;
    }

    default String toConsiderationsEnvironnementales(Contrat contrat, Map<String, String> mapConsiderationEnvironnementale) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(getAucuneClauseSet())
                .stream()
                .map(e -> mapConsiderationEnvironnementale.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.joining(";"));
    }

    default List<fr.atexo.execution.tncp.envoi.concession.json.Modification> toModifications(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeModificatif.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(ActeModificatif.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModification)
                .collect(Collectors.toList());
    }

    default String toNature(Contrat contrat, Map<String, String> map) {
        String nature = "Marché";
        var typeContrat = Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .orElse(null);
        var codeTypeContrat = Optional.ofNullable(typeContrat)
                .map(TypeContrat::getCode)
                .orElse(null);
        if ("ppp".equalsIgnoreCase(codeTypeContrat)) {
            nature = "Marché de partenariat";
        }
        // cas des concessions
        var natureConcession = Optional.ofNullable(contrat)
                .map(Contrat::getNatureContratConcession)
                .map(NatureContratConcession::getCode)
                .orElse(null);
        if (natureConcession != null) {
            nature = map.get(natureConcession);
        }
        return nature;
    }

    default String getDatePublication() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    @Named("toDonneesExecutions")
    default List<DonneesExecution> toDonneesExecutions(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(com.atexo.execution.server.model.actes.DonneesExecution.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(com.atexo.execution.server.model.actes.DonneesExecution.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toDonneesExecution)
                .collect(Collectors.toList());
    }

    default List<Tarif> toTarifs(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution) {
        return Optional.of(donneesExecution)
                .map(this::toTarif)
                .stream().collect(Collectors.toList());
    }

    @Named("toConcessionnaires")
    default List<fr.atexo.execution.tncp.envoi.concession.json.Concessionnaire> toConcessionnaires(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratEtablissements)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toConcessionnaire)
                .collect(Collectors.toList());
    }

}
