package com.atexo.execution.server.common.mapper.actes.impl;

public enum TypeActeMPE {
	ACTE_SPECIAL_HORS_SOUS_TRAITANCE("Acte Spécial (hors sous-traitance)"),
	ACTE_SPECIAL_SOUS_TRAITANCE("Acte Spécial sous-traitance"),
	AVENANT_SANS_INCIDENCE_FINANCIERE( "Avenant sans incidence financière"),
	AVENANT_AUGMENTANT_MONTANT("Avenant augmentant le montant initial"),
	AVENANT_DIMINUANT_MONTANT("Avenant diminuant le montant initial"),
	CLOTURE( "Clôture"),
	DECISION_AFFERMISSEMENT("Décision d'affermissement"),
	DECISION_POURSUIVRE("Décision de poursuivre"),
	DECISION_PROLONGATION("Décision de prolongation de délai"),
	DECISION_RECONDUCTION( "Décision de reconduction"),
	RESILIATION( "Décision de résiliation"),
	DECISION_SURSIS_EXECUTION("Décision de sursis d'exécution"),
	REOUVERTURE("Réouverture"),
	REVISION_PRIX( "Révision de prix"),
	ANNULATION_RETENUE_GARANTIE( "Annulation de retenue de garantie"),
	AUTRE( "Autre"),
	CHANGEMENT_RAISON_SOCIALE( "Changement de dénomination sociale");
	private final String libelle;

	TypeActeMPE( String libelle ) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}
}
