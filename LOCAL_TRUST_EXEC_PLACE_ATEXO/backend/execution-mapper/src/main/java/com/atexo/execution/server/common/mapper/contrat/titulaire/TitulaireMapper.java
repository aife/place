package com.atexo.execution.server.common.mapper.contrat.titulaire;

import com.atexo.execution.common.mpe.model.contrat.titulaire.Titulaire;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Fournisseur;
import org.apache.commons.lang.math.NumberUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.text.MessageFormat;
import java.util.Optional;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface TitulaireMapper {

    String SIRET = "SIRET";
    String HORS_UE = "Hors UE";
    String ENTREPRISE = "entreprises";
    String ETABLISSEMENT = "etablissements";
    String URI_FORMAT = "/api/v2/{0}/{1}";



    @Mapping(target = "denominationSociale", source = "etablissement.fournisseur.raisonSociale")
    @Mapping(target = "typeIdentifiantEntreprise", source = "contratEtablissement", qualifiedByName = "toTypeIdentifiantEntreprise")
    @Mapping(target = "nom", source = "contact.nom")
    @Mapping(target = "prenom", source = "contact.prenom")
    @Mapping(target = "email", source = "contact.email")
    @Mapping(target = "telephone", source = "contact.telephone")
    @Mapping(target = "identifiantTechnique", source = "contact", qualifiedByName = "toIdContact")
    @Mapping(target = "adresse", source = "etablissement.adresse.adresse")
    @Mapping(target = "codePostal", source = "etablissement.adresse.codePostal")
    @Mapping(target = "commune", source = "etablissement.adresse.commune")
    @Mapping(target = "pays", source = "etablissement.adresse.pays")
    @Mapping(target = "actif", source = "contact.actif", qualifiedByName = "toActif")
    @Mapping(target = "siren", source = "etablissement.fournisseur.siren")
    @Mapping(target = "codeEtablissement", source = "etablissement.adresse.codeInseeLocalite")
    @Mapping(target = "etablissement", source = "contratEtablissement", qualifiedByName = "toEtablissement")
    @Mapping(target = "entreprise", source = "contratEtablissement", qualifiedByName = "toEntreprise")
    Titulaire toTitulaire(ContratEtablissement contratEtablissement);

    @Named("toActif")
    default Integer toActif(boolean actif) {
        return actif ? 1 : 0;
    }

    @Named("toIdContact")
    default Integer toIdContact(Contact contact) {
        return Optional.ofNullable(contact)
                .map(c -> c.getIdExterne() != null && NumberUtils.isNumber(c.getIdExterne()) ? Integer.parseInt(c.getIdExterne()) : c.getId().intValue())
                .orElse(null);
    }

    @Named("toTypeIdentifiantEntreprise")
    default String toTypeIdentifiantEntreprise(ContratEtablissement contratEtablissement) {
        return Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(Etablissement::getSiret)
                .map(e -> SIRET)
                .orElse(HORS_UE);
    }

    @Named("toEntreprise")
    default String toEntreprise(ContratEtablissement contratEtablissement) {
        var idExterne = Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(Etablissement::getFournisseur)
                .map(Fournisseur::getIdExterne)
                .orElse(null);
        return idExterne != null ? MessageFormat.format(URI_FORMAT, ENTREPRISE, idExterne) : null;
    }

    @Named("toEtablissement")
    default String toEtablissement(ContratEtablissement contratEtablissement) {
        var idExterne = Optional.ofNullable(contratEtablissement)
                .map(ContratEtablissement::getEtablissement)
                .map(Etablissement::getFournisseur)
                .map(Fournisseur::getIdExterne)
                .orElse(null);
        return idExterne != null ? MessageFormat.format(URI_FORMAT, ETABLISSEMENT, idExterne) : null;
    }


}
