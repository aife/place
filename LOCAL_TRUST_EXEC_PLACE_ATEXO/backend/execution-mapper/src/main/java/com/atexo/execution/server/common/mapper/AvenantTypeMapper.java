package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.AvenantTypeDTO;
import com.atexo.execution.server.model.AvenantType;


public interface AvenantTypeMapper {
    AvenantTypeDTO toDTO(AvenantType avenantType);

    AvenantType createEntity(AvenantTypeDTO avenantTypeDTO);
}
