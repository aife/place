package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.DocumentActeDTO;
import com.atexo.execution.server.common.mapper.DocumentActeMapper;
import com.atexo.execution.server.common.mapper.FichierMapper;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.model.DocumentActe;
import com.atexo.execution.server.model.Fichier;
import com.atexo.execution.server.repository.crud.DocumentActeRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
		value = "org.mapstruct.ap.MappingProcessor",
		date = "2016-12-15T17:31:23+0100",
		comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
@Qualifier("delegate")
public class DocumentActeMapperImpl implements DocumentActeMapper {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private DocumentActeRepository documentActeRepository;

	@Autowired
	private UtilisateurMapper utilisateurMapper;

	@Autowired
	private FichierMapper fichierMapper;

    @Override
    public DocumentActeDTO toDTO( DocumentActe document) {
        if (null == document) {
            return null;
        }

        var documentDTO = new DocumentActeDTO();

        documentDTO.setId(document.getId());
        documentDTO.setFichier(fichierMapper.toDTO(document.getFichier()));
        documentDTO.setDateCreation(document.getDateCreation());
        documentDTO.setDateModification(document.getDateModification());
        documentDTO.setUtilisateur(utilisateurMapper.toDTO(document.getUtilisateurCreation()));
        documentDTO.setIdExterne(document.getIdExterne());

        return documentDTO;
    }

    @Override
    public DocumentActe createEntity( DocumentActeDTO documentDTO) {
		if (null == documentDTO) {
			return null;
		}

		var document = new DocumentActe();
		document.setId(documentDTO.getId());
		document.setFichier(fichierMapper.createEntity(documentDTO.getFichier()));

		if (document.getId() == null && null != documentDTO.getReference()) {
			return document;
		} else {
			return documentActeRepository.findById(document.getId()).orElse(null);
		}
    }
}
