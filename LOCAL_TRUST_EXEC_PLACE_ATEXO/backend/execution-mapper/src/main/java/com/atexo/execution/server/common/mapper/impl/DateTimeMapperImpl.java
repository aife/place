package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.server.common.mapper.DateTimeMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
public class DateTimeMapperImpl extends DateTimeMapper {
}
