package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.OrganismeType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Organisme;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Qualifier;


@Qualifier("organismeMapperV2")
@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface OrganismeMapperV2 {


    Organisme toOrganisme(OrganismeType organismeType);



}
