package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.AdresseDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Adresse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AdresseMapper {

	AdresseDTO toDTO(Adresse adresse);

    Adresse createEntity(AdresseDTO adresseDTO);

}
