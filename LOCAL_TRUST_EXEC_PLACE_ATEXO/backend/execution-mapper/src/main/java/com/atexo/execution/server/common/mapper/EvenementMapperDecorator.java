package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.server.model.DocumentContrat;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.DocumentContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.TypeEvenementRepository;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sta on 14/09/16.
 */
public abstract class EvenementMapperDecorator implements EvenementMapper {

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    TypeEvenementRepository typeEvenementRepository;

    @Autowired
    EvenementMapper delegate;

    @Autowired
    DocumentContratRepository documentRepository;

    @Override
    public Evenement createEntity(EvenementDTO evenementDTO) {
        Evenement evenement = delegate.createEntity(evenementDTO);
        //fetch contractant
        if (evenementDTO.getContractant() != null) {
            evenement.setContractant(etablissementRepository.findOneById(evenementDTO.getContractant().getId()));
        }
        if (evenementDTO.getNouveauContractant() != null) {
            evenement.setNouveauContractant(etablissementRepository.findOneById(evenementDTO.getNouveauContractant().getId()));
        }
        //fetch destinataires
        List<ValueLabelDTO> destinataireDTOs = evenementDTO.getDestinataires();
        if (destinataireDTOs != null && !destinataireDTOs.isEmpty()) {
            Set<Utilisateur> destinataires = new HashSet<>();
            for (ValueLabelDTO destinataireDTO : destinataireDTOs) {
                List<Utilisateur> utilisateurs = utilisateurRepository.findByEmail(destinataireDTO.getValue());
                if (utilisateurs != null && !utilisateurs.isEmpty())
                    destinataires.addAll(utilisateurs);
                else
                    evenement.addDestinataireLibre(destinataireDTO.getValue());
            }
            evenement.setDestinataires(destinataires);
        }


        List<DocumentContrat> documents = new ArrayList<>();

        if(evenementDTO.getDocuments() != null) {
            for ( DocumentContratDTO documentDTO : evenementDTO.getDocuments()) {
                documentRepository.findById(documentDTO.getId()).ifPresent(documents::add);
            }
            evenement.setDocuments(documents);
        }
        return evenement;
    }

    @Override
    public Evenement updateEntity(EvenementDTO evenementDTO, @MappingTarget Evenement evenement) {
        evenement = delegate.updateEntity(evenementDTO, evenement);
        //fetch contractant (pas de modification du contractant)
        EtablissementDTO contractantAEnregistrer = evenementDTO.getContractant();
        if (contractantAEnregistrer != null) {
            Etablissement contractantActuel = evenement.getContractant();
            if (contractantActuel == null || !contractantActuel.getId().equals(contractantAEnregistrer.getId())) {
                evenement.setContractant(etablissementRepository.findOneById(contractantAEnregistrer.getId()));
            }
        } else {
            evenement.setContractant(null);
        }
        if (evenementDTO.getNouveauContractant() != null) {
            evenement.setNouveauContractant(etablissementRepository.findOneById(evenementDTO.getNouveauContractant().getId()));
        } else {
            evenement.setNouveauContractant(null);
        }
        //fetch destinataires (pas trouvé mieux pour rester synchrnisé avec Hibernate)
        Set<Utilisateur> utilisateursBo = new HashSet<>();
        evenement.setDestinatairesLibre(null);
        for (ValueLabelDTO vl : evenementDTO.getDestinataires()) {
            List<Utilisateur> utilisateurs = utilisateurRepository.findByEmail(vl.getValue());
            if (utilisateurs != null && !utilisateurs.isEmpty()) {
                utilisateursBo.addAll(utilisateurs);
            }
        }
        if (evenementDTO.getDestinatairesLibre() != null) {
            for (ValueLabelDTO vl : evenementDTO.getDestinatairesLibre()) {
                evenement.addDestinataireLibre(vl.getValue());
            }
        }
        evenement.getDestinataires().clear();
        evenement.getDestinataires().addAll(utilisateursBo);

        //rajout des liens préexistant
        List<DocumentContrat> documents = new ArrayList<>();
        if (evenementDTO.getDocuments() != null) {
            for ( DocumentContratDTO documentDTO : evenementDTO.getDocuments()) {
                documentRepository.findById(documentDTO.getId()).ifPresent(documents::add);
            }
        }
        evenement.setDocuments(documents);

        return evenement;
    }


}
