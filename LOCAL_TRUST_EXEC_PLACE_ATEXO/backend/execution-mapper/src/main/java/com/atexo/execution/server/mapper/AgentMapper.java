package com.atexo.execution.server.mapper;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface AgentMapper {

    @Mapping(source = "id", target = "id")
    UtilisateurDTO toDto(Utilisateur utilisateur);
}
