package com.atexo.execution.server.common.mapper.qualifier;

/**
 * qualifier pour charger le DTO avec le minimum de données
 * Created by sta on 13/09/16.
 */
public @interface LightDTO {
}
