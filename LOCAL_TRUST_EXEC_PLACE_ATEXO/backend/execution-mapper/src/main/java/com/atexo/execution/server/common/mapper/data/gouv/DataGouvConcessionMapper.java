package com.atexo.execution.server.common.mapper.data.gouv;


import com.atexo.execution.common.data.gouv.model.marche.*;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.concession.NatureContratConcession;
import org.apache.commons.lang3.EnumUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface DataGouvConcessionMapper {

    String NATURE_CONTRAT = "nature-contrat-concession";
    String TYPE_PROCEDURE = "type-procedure-concession";
    String CONSIDERATION_SOCIALE = "consideration-sociale-concession";
    String CONSIDERATION_ENVIRONNEMENTALE = "consideration-environnementale";

    @Mapping(target = "id", expression = "java(toId(contrat,sequence))")
    @Mapping(target = "autoriteConcedante", source = "contrat.service")
    @Mapping(target = "nature", expression = "java(toNature(contrat,mapping.get(NATURE_CONTRAT)))")
    @Mapping(target = "objet", source = "contrat.objet")
    @Mapping(target = "procedure", expression = "java(toTypeProcedure(contrat, mapping.get(TYPE_PROCEDURE)))")
    @Mapping(target = "dureeMois", source = "contrat.dureeMaximaleMarche")
    @Mapping(target = "dateSignature", source = "contrat.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "datePublicationDonnees", expression = "java(String.valueOf(java.time.LocalDate.now()))", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "dateDebutExecution", source = "contrat.dateDebutExecution", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "valeurGlobale", source = "contrat.valeurGlobale")
    @Mapping(target = "montantSubventionPublique", source = "contrat.montantSubventionPublique")
    @Mapping(target = "donneesExecution", expression = "java(toDonneesExecutionList(contrat))")
    @Mapping(target = "concessionnaires", source = "contrat")
    @Mapping(target = "considerationsSociales", expression = "java(toConsiderationsSociales(contrat,mapping.get(CONSIDERATION_SOCIALE)))")
    @Mapping(target = "considerationsEnvironnementales", expression = "java(toConsiderationsEnvironnementales(contrat,mapping.get(CONSIDERATION_ENVIRONNEMENTALE)))")
    @Mapping(target = "modifications", expression = "java(toModifications(contrat))")
    ContratConcession toContratConcession(Contrat contrat, Map<String, Map<String, String>> mapping, String sequence);

    @Mapping(target = "datePublicationDonneesExecution", source = "datePublication", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "depensesInvestissement", source = "depensesInvestissement")
    @Mapping(target = "tarifs", source = "donneesExecution")
    DonneesExecution toDonneesAnnuelles(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "donneesAnnuelles", source = "donneesExecution")
    DonneesExecutionArray toDonneesExecution(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "intituleTarif", source = "intituleTarif")
    @Mapping(target = "tarif", source = "tarif")
    Tarif toTarif(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "tarif", source = "donneesExecution")
    TarifArray toTarifArray(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution);

    @Mapping(target = "id", source = "etablissement.siret")
    @Mapping(target = "typeIdentifiant", expression = "java(toTypeIdentifiant())")
    Concessionnaire toConcessionnaire(ContratEtablissement contratEtablissement);

    @Mapping(target = "concessionnaire", source = "contratEtablissement")
    ConcessionnaireArray toConcessionnaireArray(ContratEtablissement contratEtablissement);

    @Mapping(target = "id", source = "service.organisme.siret")
    AutoriteConcedante toAutoriteConcedante(Service service);

    @Mapping(target = "considerationSociale", expression = "java(toConsiderationSociale(contrat,mapConsiderationsSociales))")
    ConsiderationsSociales__1 toConsiderationsSociales(Contrat contrat, Map<String, String> mapConsiderationsSociales);

    @Mapping(target = "considerationEnvironnementale", expression = "java(toConsiderationEnvironnementale(contrat,mapConsiderationsSociales))")
    ConsiderationsEnvironnementales__1 toConsiderationsEnvironnementales(Contrat contrat, Map<String, String> mapConsiderationsSociales);

    @Mapping(target = "id", source = "acteModificatif.id")
    @Mapping(target = "datePublicationDonneesModification", source = "acteModificatif.contrat.donneesEssentielles.datePublication", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "dureeMois", source = "acteModificatif.contrat.dureeMaximaleMarche")
    @Mapping(target = "dateSignatureModification", source = "acteModificatif.dateNotification", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "valeurGlobale", source = "acteModificatif.montantHTChiffre")
    Modification__1 toModification(ActeModificatif acteModificatif);

    @Mapping(target = "modification", source = "acteModificatif")
    ModificationArray__1 toModificationArray(ActeModificatif acteModificatif);

    default String toId(Contrat contrat, String sequence) {
        return Optional.ofNullable(contrat)
                .map(e -> MessageFormat.format("{0}{1}",
                        contrat.getNumero() != null ? contrat.getNumero() : "",
                        sequence)
                )
                .orElse(null);
    }

    default ContratConcession.Nature toNature(Contrat contrat, Map<String, String> natureContrat) {
        return Optional.ofNullable(contrat.getNatureContratConcession())
                .map(NatureContratConcession::getCode)
                .map(natureContrat::get)
                .filter(code -> EnumUtils.isValidEnum(ContratConcession.Nature.class, code))
                .map(ContratConcession.Nature::valueOf)
                .orElse(null);
    }

    default ContratConcession.Procedure toTypeProcedure(Contrat contrat, Map<String, String> typeProcedure) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getProcedure)
                .map(Procedure::getCode)
                .map(typeProcedure::get)
                .filter(code -> EnumUtils.isValidEnum(ContratConcession.Procedure.class, code))
                .map(ContratConcession.Procedure::valueOf).orElse(null);
    }

    default Set<DonneesExecutionArray> toDonneesExecutionList(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .filter(com.atexo.execution.server.model.actes.DonneesExecution.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(com.atexo.execution.server.model.actes.DonneesExecution.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toDonneesExecution)
                .collect(Collectors.toSet());
    }

    default List<TarifArray> toTarifs(com.atexo.execution.server.model.actes.DonneesExecution donneesExecution) {
        return Optional.of(donneesExecution)
                .map(this::toTarifArray)
                .stream().collect(Collectors.toList());
    }

    default Set<ConcessionnaireArray> toConcessionnaires(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getContratEtablissements)
                .orElse(Collections.emptySet())
                .stream()
                .map(this::toConcessionnaireArray)
                .collect(Collectors.toSet());
    }

    default Set<ConsiderationSocialeArray_> toConsiderationSociale(Contrat contrat, Map<String, String> mapConsiderationSocaile) {
        var listClause = Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .map(e -> mapConsiderationSocaile.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .map(ConsiderationSocialeArray_::valueOf)
                .collect(Collectors.toSet());
        return listClause.isEmpty() ? Collections.singleton(ConsiderationSocialeArray_.PAS_DE_CONSIDÉRATION_SOCIALE) : listClause;
    }

    default Set<ConsiderationEnvironnementaleArray_> toConsiderationEnvironnementale(Contrat contrat, Map<String, String> mapConsiderationEnvironnementale) {
        var listClause = Optional.ofNullable(contrat)
                .map(Contrat::getContratClauses)
                .orElse(Collections.emptySet())
                .stream()
                .map(e -> mapConsiderationEnvironnementale.get(e.getClause().getCode()))
                .filter(Objects::nonNull)
                .map(ConsiderationEnvironnementaleArray_::valueOf)
                .collect(Collectors.toSet());
        return listClause.isEmpty() ? Collections.singleton(ConsiderationEnvironnementaleArray_.PAS_DE_CONSIDÉRATION_ENVIRONNEMENTALE) : listClause;

    }

    default Set<ModificationArray__1> toModifications(Contrat contrat) {
        return Optional.ofNullable(contrat.getActes())
                .orElse(Collections.emptySet())
                .stream()
                .filter(ActeModificatif.class::isInstance)
                .filter(acte -> StatutActe.NOTIFIE.equals(acte.getStatut()))
                .map(ActeModificatif.class::cast)
                .filter(e -> e.getPublicationDonneesEssentielles() != null && e.getPublicationDonneesEssentielles())
                .map(this::toModificationArray)
                .collect(Collectors.toSet());
    }

    default Concessionnaire.TypeIdentifiant toTypeIdentifiant() {
        return Concessionnaire.TypeIdentifiant.SIRET;
    }
}
