package com.atexo.execution.server.common.mapper.contrat.titulaire;

import com.atexo.execution.common.mpe.model.contrat.titulaire.*;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Consultation;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.*;
import org.apache.commons.lang.math.NumberUtils;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, AcheteurMapper.class, TitulaireMapper.class, LieuExecutionMapper.class, ContratTitulaireConsultationMapper.class, MapperUtils.class, ClauseMapper.class})
public interface ContratTitulaireMapper {

    String SIRET = "siret";

    String AMO = "AMO";
    String AMU = "AMU";
    String NATURE_PRESTATION = "nature-prestation";
    String TYPE_CONTRAT = "type-contrat";
    String TYPE_PRIX = "type-prix";
    String STATUT_CONTRAT = "statut-contrat";
    String CHAPEAU = "chapeau";
    String SAD = "sad";
    String ABCMO = "abcmo";
    String NATURE_CONTRAT = "nature-contrat";

    @Mapping(target = "organisme", source = "contrat", qualifiedByName = "toAcronymeOrganisme")
    @Mapping(target = "horsPassation", source = "contrat.consultation.horsPassation")
    @Mapping(target = "id", source = "contrat", qualifiedByName = "toId")
    @Mapping(target = "uuid", source = "contrat.uuid")
    @Mapping(target = "idMarche", source = "contrat.numero")
    @Mapping(target = "numero", source = "contrat.numero")
    @Mapping(target = "numeroLong", source = "contrat.numeroLong")
    @Mapping(target = "numEj", source = "contrat.numEj")
    @Mapping(target = "referenceLibre", source = "contrat.referenceLibre")
    @Mapping(target = "objet", source = "contrat.objet")
    @Mapping(target = "idService", source = "contrat.service", qualifiedByName = "toIdService")
    @Mapping(target = "idCreateur", source = "createur.idExterne", qualifiedByName = "toIdCreateur")
    @Mapping(target = "formePrix", source = "contrat", qualifiedByName = "toFormePrix")
    @Mapping(target = "typesPrix", source = "contrat.consultation", qualifiedByName = "toTypesPrix")
    @Mapping(target = "modaliteRevisionPrix", source = "contrat", qualifiedByName = "toModaliteRevisionPrix")
    @Mapping(target = "defenseOuSecurite", source = "contrat", qualifiedByName = "toDefenseOuSecurite")
    @Mapping(target = "datePrevisionnelleNotification", source = "contrat.datePrevisionnelleNotification", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "datePrevisionnelleFinMarche", source = "contrat.datePrevisionnelleFinMarche", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "datePrevisionnelleFinMaximaleMarche", source = "contrat.datePrevisionnelleFinMaximaleMarche", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dureeMaximaleMarche", source = "contrat.dureeMaximaleMarche")
    @Mapping(target = "lieuExecutions", source = "contrat")
    @Mapping(target = "decisionAttribution", source = "contrat.consultation.decisionAttribution", qualifiedByName = "toDecisionAttribution")
    @Mapping(target = "intitule", source = "contrat.intitule")
    @Mapping(target = "ccagApplicable", source = "contrat", qualifiedByName = "toCcagApplicable")
    @Mapping(target = "typeContrat", source = "contrat", qualifiedByName = "toTypeContrat")
    @Mapping(target = "dateModification", source = "contrat", qualifiedByName = "toDateModification")
    @Mapping(target = "dateCreation", source = "dateCreation", qualifiedByName = "localDateTimeToZonedDate")
    @Mapping(target = "dateNotification", source = "contrat.dateNotification", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dateFin", source = "contrat.dateFinContrat", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "dateMaxFin", source = "contrat.dateMaxFinContrat", qualifiedByName = "localDateToZonedDate")
    @Mapping(target = "statut", source = "contrat", qualifiedByName = "toStatut")
    @Mapping(target = "naturePrestation", source = "contrat.categorie", qualifiedByName = "toNaturePrestation")
    @Mapping(target = "chapeau", source = "contrat", qualifiedByName = "toChapeau")
    @Mapping(target = "idEtablissementTitulaire", source = "contrat.attributaire.etablissement.idExterne")
    @Mapping(target = "montant", source = "contrat.montant")
    @Mapping(target = "idContact", source = "attributaire.contact.id")
    @Mapping(target = "cpv", source = "contrat")
    @Mapping(target = "contratTransverse", source = "contrat.service")
    @Mapping(target = "nature", source = "contrat", qualifiedByName = "toNature")
    @Mapping(target = "marcheInnovant", source = "contrat.marcheInnovant", qualifiedByName = "toMarcheInnovant")
    @Mapping(target = "idChapeauAcSad", source = "contrat", qualifiedByName = "toIdChapeauAcSad")
    @Mapping(target = "idChapeauMultiAttributaire", source = "contrat", qualifiedByName = "toIdChapeauMultiAttributaire")
    @Mapping(target = "naturePassation", source = "contrat.type.label")
    @Mapping(target = "procedurePassation", source = "contrat.consultation.procedure.label")
    @Mapping(target = "procedure", source = "contrat.consultation.procedure.label")
    @Mapping(target = "nbTotalPropositionsRecu", source = "contrat.offresRecues")
    @Mapping(target = "consultation", source = "contrat")
    @Mapping(target = "lots", source = "contrat")
    @Mapping(target = "oldIdService", source = "contrat", qualifiedByName = "toOldIdService")
    @Mapping(target = "acheteur", source = "contrat")
    @Mapping(target = "titulaires", source = "contrat.contratEtablissements")
    @Mapping(target = "publicationContrat", source = "contrat.donneesEssentielles")
    @Mapping(target = "accordCadreAvecMarcheSubsequent", source = "contrat")
    @Mapping(target = "contratChapeauMultiAttributaires", source = "contrat.contratChapeau")
    @Mapping(target = "contratChapeauAcSad", source = "contrat.contratChapeau")
    @Mapping(target = "clausesSociales", source = "contrat")
    @Mapping(target = "clausesEnvironnementales", source = "contrat")
    @Mapping(target = "lienAcSad", source = "contrat.lienAcSad.uuid")
    @Mapping(target = "idExec", source = "contrat.id")
    @Mapping(target = "formePrixContrat", source = "contrat.typeFormePrix.code")
    @Mapping(target = "clausesSocialesV2", source = "contrat")
    @Mapping(target = "clausesEnvironnementalesV2", source = "contrat")
    @Mapping(target = "createur", source = "contrat.createur", qualifiedByName = "toCreateur")
    @Mapping(target = "idExternesService", source = "contrat", qualifiedByName = "toIdExternesService")
    ContratTitulaire contratToContratTitulaire(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput);

    @Mapping(target = "id", source = "contratChapeau", qualifiedByName = "toId")
    @Mapping(target = "numeroCourt", source = "contratChapeau.numero")
    @Mapping(target = "numeroLong", source = "contratChapeau.numeroLong")
    @Mapping(target = "referenceLibre", source = "contratChapeau.referenceLibre")
    ContratChapeauMultiAttributaires toContratChapeauMultiAttributaires(Contrat contratChapeau);

    @Mapping(target = "id", source = "contrat", qualifiedByName = "toId")
    @Mapping(target = "numeroCourt", source = "contrat.numero")
    @Mapping(target = "numeroLong", source = "contrat.numeroLong")
    @Mapping(target = "referenceLibre", source = "contrat.referenceLibre")
    @Mapping(target = "numIdUniqueMarchePublic", source = "contrat.numeroLong")
    ContratChapeauAcSad toContratChapeauAcSad(Contrat contrat);

    default Boolean toAccordCadreAvecMarcheSubsequent(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(type -> AMO.equals(type.getCode()) || AMU.equals(type.getCode())).orElse(null);
    }


    @Named("toTypesPrix")
    default Set<String> toTypesPrix(Consultation consultation) {
        return Optional.ofNullable(consultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .map(TypePrix::getCode)
                .collect(Collectors.toSet());
    }

    @Named("toOldIdService")
    default Integer toOldIdService(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOldIdExterne)
                .map(BeanSynchroUtils::extractId)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Named("toChapeau")
    default Boolean toChapeau(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::isChapeau)
                .orElse(null);
    }

    @Named("toDefenseOuSecurite")
    default Integer toDefenseOuSecurite(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(c -> c.isDefenseOuSecurite() ? 1 : 0)
                .orElse(null);
    }

    @Named("toId")
    default Integer toId(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(c -> c.getIdExterne() != null && NumberUtils.isNumber(c.getIdExterne()) ? Integer.parseInt(c.getIdExterne()) : c.getId().intValue())
                .orElse(null);
    }

    @Mapping(target = "codePrincipal", source = "codesCpv")
    @Mapping(target = "codeSecondaire1", source = "codesCpv", qualifiedByName = "toCodeSecondaire1")
    @Mapping(target = "codeSecondaire2", source = "codesCpv", qualifiedByName = "toCodeSecondaire2")
    @Mapping(target = "codeSecondaire3", source = "codesCpv", qualifiedByName = "toCodeSecondaire3")
    Cpv toCpv(Contrat contrat);

    @Named("toCcagApplicable")
    default String toCcagApplicable(Contrat contrat) {
        return Optional.of(contrat)
                .map(Contrat::getCcagApplicable)
                .map(CCAGReference::getCode)
                .orElse(null);
    }

    @Named("toNaturePrestation")
    default String toNaturePrestation(CategorieConsultation categorieConsultation, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(categorieConsultation)
                .map(CategorieConsultation::getCode)
                .map(code -> mappingPlaceOutput.get(NATURE_PRESTATION) != null ? mappingPlaceOutput.get(NATURE_PRESTATION).get(code) : null)
                .orElse(null);
    }

    default String toCodePrincipal(Set<CPV> codesCpv) {
        return Optional.ofNullable(codesCpv)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(AbstractReferentielEntity::getCode)
                .orElse(null);
    }

    @Named("toCodeSecondaire1")
    default String toCodeSecondaire1(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 1);
    }

    @Named("toCodeSecondaire2")
    default String toCodeSecondaire2(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 2);
    }

    @Named("toCodeSecondaire3")
    default String toCodeSecondaire3(Set<CPV> codesCpv) {
        return getElementByPosition(codesCpv, 3);
    }

    default String getElementByPosition(Set<CPV> codesCpv, int p) {
        return Optional.ofNullable(codesCpv)
                .orElse(Collections.emptySet())
                .stream()
                .skip(p)
                .map(AbstractReferentielEntity::getCode)
                .findAny().orElse(null);
    }


    default Integer toHorsPassation(boolean horsPassation) {
        return horsPassation ? 1 : 0;
    }

    default ContratTransverse toContratTransverse(Service service) {
        return Optional.ofNullable(service)
                .map(srv -> {
                    ContratTransverse contratTransverse = new ContratTransverse();
                    contratTransverse.setValue(false);
                    return contratTransverse;
                }).orElse(null);


    }

    default String toNature(CategorieConsultation categorieConsultation) {
        return Optional.ofNullable(categorieConsultation)
                .map(CategorieConsultation::getCode)
                .orElse(null);
    }

    default Lots toLot(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .filter(c -> c.getNumeroLot() != null && c.getNumeroLot() > 0)
                .map(c -> {
                    Lots lot = new Lots();
                    lot.setNumeroLot(c.getNumeroLot());
                    lot.setIdExternesLot(c.getIdExternesLot());
                    return lot;
                })
                .orElse(null);
    }

    default Integer toPublicationContrat(DonneesEssentielles donneesEssentielles) {
        return (donneesEssentielles != null && StatutPublicationDE.A_PUBLIER.equals(donneesEssentielles.getStatut())) ? 0 : 1;
    }

    @Named("toTypeContrat")
    default Integer toTypeContrat(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getType)
                .map(TypeContrat::getCode)
                .map(label -> mappingPlaceOutput.get(TYPE_CONTRAT) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_CONTRAT).get(label)) : null)
                .orElse(null);
    }

    @Named("toFormePrix")
    default Integer toFormePrix(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(TypePrix::getCode)
                .map(code -> mappingPlaceOutput.get(TYPE_PRIX) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_PRIX).get(code)) : null)
                .orElse(null);
    }

    @Named("toModaliteRevisionPrix")
    default Integer toModaliteRevisionPrix(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .findFirst()
                .map(TypePrix::getCode)
                .map(code -> mappingPlaceOutput.get(TYPE_PRIX) != null ? MapperUtils.toInteger(mappingPlaceOutput.get(TYPE_PRIX).get(code)) : null)
                .orElse(null);
    }

    @Named("toStatut")
    default String toStatut(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat)
                .map(c -> {
                    if (c.isChapeau()) {
                        return mappingPlaceOutput.get(STATUT_CONTRAT) != null ? mappingPlaceOutput.get(STATUT_CONTRAT).get(CHAPEAU) : null;
                    } else if (c.getStatut() != null) {
                        return mappingPlaceOutput.get(STATUT_CONTRAT) != null ? mappingPlaceOutput.get(STATUT_CONTRAT).get(c.getStatut().name()) : null;
                    }
                    return null;
                })
                .orElse(null);
    }

    @Named("toNature")
    default String toNature(Contrat contrat, @Context Map<String, Map<String, String>> mappingPlaceOutput) {
        return Optional.ofNullable(contrat.getType())
                .map(TypeContrat::getCode)
                .map(code -> mappingPlaceOutput.get(NATURE_CONTRAT) != null ? mappingPlaceOutput.get(NATURE_CONTRAT).get(code) : null)
                .orElse(null);
    }

    @Named("toIdChapeauAcSad")
    default Integer toIdChapeauAcSad(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .filter(e -> e.getType() != null &&
                        MapperUtils.checkIfExist(e.getType().getCode(), List.of(SAD, AMO, ABCMO)) &&
                        e.isChapeau())
                .map(this::toId)
                .orElse(null);
    }


    @Named("toIdChapeauMultiAttributaire")
    default Integer toIdChapeauMultiAttributaire(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .filter(e -> e.getType() != null &&
                        MapperUtils.checkIfExist(e.getType().getCode(), List.of(AMU)) &&
                        e.isChapeau())
                .map(this::toId)
                .orElse(null);
    }

    @Named("toDateModification")
    default String toDateModification(Contrat contrat) {
        List<LocalDateTime> listDateModification = new ArrayList<>();

        Optional.ofNullable(contrat)
                .map(Contrat::getDateModification)
                .ifPresent(listDateModification::add);

        Optional.ofNullable(contrat)
                .map(Contrat::getDateModificationExterne)
                .ifPresent(listDateModification::add);

        Optional.ofNullable(contrat)
                .map(Contrat::getActes)
                .orElse(Collections.emptySet())
                .stream()
                .map(Acte::getDateNotification)
                .filter(Objects::nonNull)
                .forEach(listDateModification::add);

        return listDateModification.stream()
                .max(LocalDateTime::compareTo)
                .map(MapperUtils::localDateTimeToZonedDate)
                .orElse(null);
    }

    @Named("toIdService")
    default String toIdService(Service service) {
        return Optional.ofNullable(service)
                .map(Service::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .orElse(null);
    }


    @Mapping(target = "id", source = "utilisateur")
    @Mapping(target = "habilitations", source = "utilisateur", qualifiedByName = "toHabilitations")
    @Mapping(target = "service", source = "utilisateur.service", qualifiedByName = "toService")
    @Named("toCreateur")
    Createur toCreateur(Utilisateur utilisateur);

    @Mapping(target = "id", source = "service")
    @Mapping(target = "organisme", source = "service.organisme")
    @Named("toService")
    com.atexo.execution.common.mpe.model.contrat.titulaire.Service toService(Service service);

    @Named("toHabilitations")
    default List<String> toHabilitations(Utilisateur utilisateur) {
        return Optional.ofNullable(utilisateur)
                .map(Utilisateur::getHabilitations)
                .orElse(Collections.emptySet())
                .stream()
                .map(Habilitation::getCode)
                .collect(Collectors.toList());
    }


    default int getIdMPE(AbstractSynchronizedEntity entity) {
        if (entity == null || entity.getIdExterne() == null) {
            return 0;
        }
        String[] splitId = entity.getIdExterne().split("_");
        return (splitId.length > 1 && NumberUtils.isNumber(splitId[1])) ? Integer.parseInt(splitId[1]) : 0;
    }

    @Named("toAcronymeOrganisme")
    default String toAcronymeOrganisme(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getAcronyme)
                .map(String::toLowerCase)
                .orElse(null);
    }

    @Mapping(target = "id", expression = "java(getIdMPE(organisme))")
    com.atexo.execution.common.mpe.model.contrat.titulaire.Organisme organismeToOrganisme(Organisme organisme);

    @Named("toIdCreateur")
    default Integer toIdCreateur(String idExterne) {
        return Optional.ofNullable(idExterne)
                .map(MapperUtils::toInteger)
                .map(Math::toIntExact)
                .orElse(null);
    }

    @Named("toDecisionAttribution")
    default String toDecisionAttribution(LocalDate date) {
        return Optional.ofNullable(date)
                .map(LocalDate::atStartOfDay)
                .map(MapperUtils::localDateTimeToZonedDate)
                .orElse(null);
    }

    @Named("toMarcheInnovant")
    default Integer toMarcheInnovant(boolean marcheInnovant) {
        return marcheInnovant ? 1 : 0;
    }

    @Named("toIdExternesService")
    default Integer toIdExternesService(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getService)
                .map(Service::getIdExterne)
                .map(MapperUtils::toInteger)
                .map(Math::toIntExact)
                .orElse(null);
    }
}
