package com.atexo.execution.server.common.mapper.impl;

import com.atexo.execution.common.dto.AvenantTypeDTO;
import com.atexo.execution.common.dto.BonCommandeTypeDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.server.common.mapper.*;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.AvenantType;
import com.atexo.execution.server.model.BonCommandeType;
import com.atexo.execution.server.model.Evenement;
import com.atexo.execution.server.model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2016-12-15T17:31:23+0100",
        comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"
)
@Component
@Qualifier("delegate")
public class EvenementMapperImpl_ implements EvenementMapper {

    @Autowired
    private EtablissementMapper etablissementMapper;
    @Autowired
    private ReferentielMapper referentielMapper;
    @Autowired
    private AvenantTypeMapper avenantTypeMapper;
    @Autowired
    private DocumentContratMapper documentMapper;
    @Autowired
    private BonCommandeTypeMapper bonCommandeTypeMapper;

    @Override
    public EvenementDTO toDTO(Evenement evenement) {
        if (evenement == null) {
            return null;
        }

        EvenementDTO evenementDTO = new EvenementDTO();

        evenementDTO.setId(evenement.getId());
        evenementDTO.setDateDebut(evenement.getDateDebut());
        evenementDTO.setDateFin(evenement.getDateFin());
        evenementDTO.setTypeEvenement(referentielMapper.toDTO(evenement.getTypeEvenement()));
        evenementDTO.setLibelle(evenement.getLibelle());
        evenementDTO.setCommentaire(evenement.getCommentaire());
        evenementDTO.setContractant(etablissementMapper.toDTOWithoutContact(evenement.getContractant()));
        evenementDTO.setNouveauContractant(etablissementMapper.toDTOWithoutContact(evenement.getNouveauContractant()));
        evenementDTO.setPonctuel(evenement.getPonctuel());
        evenementDTO.setAlerte(evenement.getAlerte());
        evenementDTO.setDelaiPreavis(evenement.getDelaiPreavis());
        evenementDTO.setDestinataires(utilisateurListToValueLabelDTOList(evenement.getDestinataires()));
        evenementDTO.setDestinatairesLibre(stringListToValueLabelDTOList(evenement.getDestinatairesLibre()));
        evenementDTO.setDocumentsCount(evenement.getDocumentsCount());
        if(evenement.getDocuments() != null){
            evenementDTO.setDocuments(evenement.getDocuments().stream()
                    .map(document -> {
                        return documentMapper.toDTO(document,false);
                    }).collect(Collectors.toList()));
        }else {
            evenementDTO.setDocuments(new ArrayList<>());
        }

        evenementDTO.setDateDebutReel(evenement.getDateDebutReel());
        evenementDTO.setDateFinReel(evenement.getDateFinReel());
        evenementDTO.setDateRejet(evenement.getDateRejet());
        evenementDTO.setCommentaireEtat(evenement.getCommentaireEtat());
        evenementDTO.setSuiviRealisation(evenement.getSuiviRealisation());
        evenementDTO.setEtatEvenement(evenement.getEtatEvenement());
        if(evenement.getAvenantType() != null){
            evenementDTO.setAvenantType(avenantTypeMapper.toDTO(evenement.getAvenantType()));
        }
        if (evenement.getBonCommandeType() != null) {
            evenementDTO.setBonCommandeTypeDTO(bonCommandeTypeMapper.toDTO(evenement.getBonCommandeType()));
        }

        //evenementDTO.setEnvoiAlerte( evenement.getEnvoiAlerte() );

        return evenementDTO;
    }

    @Override
    public Evenement createEntity(EvenementDTO evenement) {
        if (evenement == null) {
            return null;
        }

        Evenement evenement_ = new Evenement();

        evenement_.setId(evenement.getId());
        evenement_.setDateDebut(evenement.getDateDebut());
        evenement_.setDateFin(evenement.getDateFin());
        evenement_.setPonctuel(evenement.getPonctuel());
        evenement_.setLibelle(evenement.getLibelle());
        evenement_.setCommentaire(evenement.getCommentaire());
        evenement_.setAlerte(evenement.getAlerte());
        evenement_.setDelaiPreavis(evenement.getDelaiPreavis());
        evenement_.setDateDebutReel(evenement.getDateDebutReel());
        evenement_.setDateFinReel(evenement.getDateFinReel());
        evenement_.setDateRejet(evenement.getDateRejet());
        evenement_.setCommentaireEtat(evenement.getCommentaireEtat());
        evenement_.setSuiviRealisation(evenement.getSuiviRealisation());
        evenement_.setEtatEvenement(evenement.getEtatEvenement());

        if(evenement.getAvenantType() != null){
            evenement_.setAvenantType(avenantTypeMapper.createEntity(evenement.getAvenantType()));
        }
        if (evenement.getBonCommandeTypeDTO() != null) {
            evenement_.setBonCommandeType(bonCommandeTypeMapper.createEntity(evenement.getBonCommandeTypeDTO()));
        }
        //evenement_.setEnvoiAlerte( evenement.getEnvoiAlerte() );

        return evenement_;
    }

    @Override
    public Evenement updateEntity(EvenementDTO evenementDTO, Evenement evenement) {
        if (evenementDTO == null) {
            return null;
        }

        evenement.setId(evenementDTO.getId());
        evenement.setDateDebut(evenementDTO.getDateDebut());
        evenement.setDateFin(evenementDTO.getDateFin());
        evenement.setPonctuel(evenementDTO.getPonctuel());
        evenement.setLibelle(evenementDTO.getLibelle());
        evenement.setCommentaire(evenementDTO.getCommentaire());
        evenement.setAlerte(evenementDTO.getAlerte());
        evenement.setDelaiPreavis(evenementDTO.getDelaiPreavis());
        evenement.setDateDebutReel(evenementDTO.getDateDebutReel());
        evenement.setDateFinReel(evenementDTO.getDateFinReel());
        evenement.setDateRejet(evenementDTO.getDateRejet());
        evenement.setCommentaireEtat(evenementDTO.getCommentaireEtat());
        evenement.setSuiviRealisation(evenementDTO.getSuiviRealisation());
        evenement.setEtatEvenement(evenementDTO.getEtatEvenement());
        if(evenementDTO.getAvenantType() != null){
            updateEntity(evenementDTO.getAvenantType(), evenement.getAvenantType());
        }
        if (evenementDTO.getBonCommandeTypeDTO() != null) {
            if (evenement.getBonCommandeType() == null) {
                evenement.setBonCommandeType(new BonCommandeType());
            }
            updateEntity(evenementDTO.getBonCommandeTypeDTO(), evenement.getBonCommandeType());
        }
        //evenement.setEnvoiAlerte( evenementDTO.getEnvoiAlerte() );

        return evenement;
    }

    @Override
    public EvenementDTO toDTOWithoutDocument(Evenement evenement) {
        if (evenement == null) {
            return null;
        }

        EvenementDTO evenementDTO = new EvenementDTO();

        evenementDTO.setId(evenement.getId());
        evenementDTO.setDateDebut(evenement.getDateDebut());
        evenementDTO.setDateFin(evenement.getDateFin());
        evenementDTO.setTypeEvenement(referentielMapper.toDTO(evenement.getTypeEvenement()));
        evenementDTO.setLibelle(evenement.getLibelle());
        evenementDTO.setCommentaire(evenement.getCommentaire());
        evenementDTO.setContractant(etablissementMapper.toDTO(evenement.getContractant()));
        evenementDTO.setNouveauContractant(etablissementMapper.toDTOWithoutContact(evenement.getNouveauContractant()));
        evenementDTO.setPonctuel(evenement.getPonctuel());
        evenementDTO.setAlerte(evenement.getAlerte());
        evenementDTO.setDelaiPreavis(evenement.getDelaiPreavis());
        evenementDTO.setDestinataires(utilisateurListToValueLabelDTOList(evenement.getDestinataires()));
        evenementDTO.setDestinatairesLibre(stringListToValueLabelDTOList(evenement.getDestinatairesLibre()));
        evenementDTO.setDocumentsCount(evenement.getDocumentsCount());
        evenementDTO.setDateDebutReel(evenement.getDateDebutReel());
        evenementDTO.setDateFinReel(evenement.getDateFinReel());
        evenementDTO.setDateRejet(evenement.getDateRejet());
        evenementDTO.setCommentaireEtat(evenement.getCommentaireEtat());
        evenementDTO.setSuiviRealisation(evenement.getSuiviRealisation());
        evenementDTO.setEtatEvenement(evenement.getEtatEvenement());
        if(evenement.getAvenantType() != null){
            evenementDTO.setAvenantType(avenantTypeMapper.toDTO(evenement.getAvenantType()));
        }
        if(evenement.getBonCommandeType() != null){
            evenementDTO.setBonCommandeTypeDTO(bonCommandeTypeMapper.toDTO(evenement.getBonCommandeType()));
        }
        //evenementDTO.setEnvoiAlerte( evenement.getEnvoiAlerte() );

        return evenementDTO;
    }

    protected List<ValueLabelDTO> utilisateurListToValueLabelDTOList(Collection<Utilisateur> list) {
        if (list == null) {
            return null;
        }

        List<ValueLabelDTO> list_ = new ArrayList<ValueLabelDTO>();
        for (Utilisateur utilisateur : list) {
            list_.add(referentielMapper.toDTO(utilisateur));
        }

        return list_;
    }

    protected List<ValueLabelDTO> stringListToValueLabelDTOList(List<String> list) {
        if (list == null) {
            return null;
        }

        List<ValueLabelDTO> list_ = new ArrayList<ValueLabelDTO>();
        for (String string : list) {
            list_.add(referentielMapper.toDTO(string));
        }

        return list_;
    }

    private void updateEntity(AvenantTypeDTO avenantTypeDTO, AvenantType avenantType) {
        avenantType.setNumero(avenantTypeDTO.getNumero());
        avenantType.setAutreType(avenantTypeDTO.getAutreType());
        avenantType.setDateDemaragePrestation(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateDemaragePrestation()));
        avenantType.setDateDemaragePrestationNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateDemaragePrestationNew()));
        avenantType.setDateFin(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFin()));
        avenantType.setDateFinNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinNew()));
        avenantType.setDateFinMax(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinMax()));
        avenantType.setDateFinMaxNew(MapperUtils.convertToLocalDate(avenantTypeDTO.getDateFinMaxNew()));
        avenantType.setIncidence(avenantTypeDTO.getIncidence());
        avenantType.setAvenantTransfert(avenantTypeDTO.getAvenantTransfert());
        avenantType.setMontant(avenantTypeDTO.getMontant());
        avenantType.setDateModifiable(avenantTypeDTO.getDateModifiable());
        avenantType.setValue(avenantTypeDTO.getValue());
    }

    private void updateEntity(BonCommandeTypeDTO bonCommandeTypeDTO, BonCommandeType bonCommandeType) {
        bonCommandeType.setNumero(bonCommandeTypeDTO.getNumero());
        bonCommandeType.setAdresseFacturation(bonCommandeTypeDTO.getAdresseFacturation());
        bonCommandeType.setAdresseLivExec(bonCommandeTypeDTO.getAdresseLivExec());
        bonCommandeType.setDelaiLivExec(bonCommandeTypeDTO.getDelaiLivExec());
        bonCommandeType.setDelaiPaiement(bonCommandeTypeDTO.getDelaiPaiement());
        bonCommandeType.setDevisTitulaireNum(bonCommandeTypeDTO.getDevisTitulaireNum());
        bonCommandeType.setImputationBudgetaire(bonCommandeTypeDTO.getImputationBudgetaire());
        bonCommandeType.setProgrammeNum(bonCommandeTypeDTO.getProgrammeNum());
        bonCommandeType.setEngagementNum(bonCommandeTypeDTO.getEngagementNum());
        bonCommandeType.setMontantTVA(bonCommandeTypeDTO.getMontantTVA());
        bonCommandeType.setMontantHT(bonCommandeTypeDTO.getMontantHT());
    }
}
