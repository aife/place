package com.atexo.execution.server.common.mapper;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Fournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, FournisseurMapper.class, ContactMapper.class, AdresseMapper.class})
public interface EtablissementMapper {

    @Named("toDTO")
    @Mapping(target = "description", expression = "java(toDescription(etablissement))")
    @Mapping(target = "fournisseur.etablissements", ignore = true)
    @Mapping(target = "uuidFournisseur", source = "fournisseur.uuid")
    @Mapping(target = "raisonSociale", source = "fournisseur.raisonSociale")
    @Mapping(target = "contacts", ignore = true)
    EtablissementDTO toDTO(Etablissement etablissement);

    @Mapping(target = "description", expression = "java(toDescription(etablissement))")
    @Mapping(target = "fournisseur.etablissements", ignore = true)
    @Mapping(target = "uuidFournisseur", source = "fournisseur.uuid")
    @Mapping(target = "raisonSociale", source = "fournisseur.raisonSociale")
    @Mapping(target = "nbContacts", expression = "java(toNbContacts(etablissement))")
    EtablissementDTO toDTOWithContacts(Etablissement etablissement);


    @Mapping(target = "contacts", ignore = true)
    EtablissementDTO toDTOWithoutContact(Etablissement etablissement);

    Etablissement createEntity(EtablissementDTO etablissementDTO);

    default List<EtablissementDTO> toEtablissements(List<Etablissement> list) {
        return Optional.ofNullable(list)
                .orElse(Collections.emptyList())
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    default String toDescription(Etablissement etablissement) {
        String raisonSociale = Optional.ofNullable(etablissement.getFournisseur())
                .map(Fournisseur::getRaisonSociale)
                .orElse("");

        String siret = Optional.ofNullable(etablissement.getSiret())
                .map(s -> " (" + s + ") ")
                .orElse("");

        String addressInfo = Optional.ofNullable(etablissement.getAdresse())
                .map(a -> {
                    String postalCode = Optional.ofNullable(a.getCodePostal()).orElse("");
                    String commune = Optional.ofNullable(a.getCommune()).orElse("");
                    return (postalCode.isEmpty() || commune.isEmpty()) ? "" : "-" + postalCode + "-" + commune;
                })
                .orElse("");

        return raisonSociale + siret + addressInfo;
    }

    default Long toNbContacts(Etablissement etablissement) {
        return Optional.ofNullable(etablissement)
                .map(Etablissement::getContacts)
                .map(List::size)
                .map(Integer::longValue)
                .orElse(0L);
    }

}
