package com.atexo.execution.server.mapper;

import com.atexo.execution.common.mpe.ws.api.Mpe;
import com.atexo.execution.common.mpe.ws.api.TemplateType;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Template;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class})
public interface TemplateMapper {

    default TemplateReponse toWS(List<Template> templates) {
        var reponse = new TemplateReponse();
        var mpe = new Mpe();
        mpe.setReponse(new CustomReponse());
        templates.forEach(t -> mpe.getReponse().getMailTemplate().add(toTemplate(t)));
        reponse.setMpe(mpe);
        return reponse;
    }

    @Mapping(source = "id", target = "id")
    @Mapping(source = "code", target = "mailType.code")
    @Mapping(source = "objet", target = "mailType.label")
    TemplateType toTemplate(Template template);
}
