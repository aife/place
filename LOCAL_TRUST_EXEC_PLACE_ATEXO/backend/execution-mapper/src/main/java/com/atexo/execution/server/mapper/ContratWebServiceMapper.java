package com.atexo.execution.server.mapper;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.dto.ContratTitulaireFilter;
import com.atexo.execution.common.exec.mpe.ws.*;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.model.concession.NatureContratConcession;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteriaAppach;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class, ClauseMapper.class})
public interface ContratWebServiceMapper {


    @Mapping(target = "contrats", ignore = true)
    @Mapping(target = "clausesRSE", source = "contrat")
    @Mapping(target = "origineUE", source = "contrat.consultation.origineUE")
    @Mapping(target = "origineFrance", source = "contrat.consultation.origineFrance")
    @Mapping(target = "typeProcedure", source = "contrat.consultation.procedure.code")
    @Mapping(target = "naturePrestation", source = "contrat.categorie.code")
    @Mapping(target = "naturePrestationIdExterne", source = "contrat.categorie.idExterne")
    @Mapping(target = "numeroProjetAchat", source = "contrat.consultation.numeroProjetAchat")
    @Mapping(target = "decisionAttribution", source = "contrat.consultation.decisionAttribution")
    @Mapping(target = "numeroLot", source = "contrat.numeroLot")
    @Mapping(target = "offresRecues", source = "contrat", qualifiedByName = "toOffresRecues")
    @Mapping(target = "typeGroupementOperateurs", source = "contrat.consultation.typeGroupementOperateurs")
    @Mapping(target = "techniques", source = "contrat", qualifiedByName = "toTechniques")
    @Mapping(target = "modalitesExecutions", source = "contrat", qualifiedByName = "toModalitesExecutions")
    @Mapping(target = "typesPrix", source = "contrat", qualifiedByName = "toTypesPrix")
    @Mapping(target = "cpv", source = "contrat")
    @Mapping(target = "idConsultation", source = "contrat", qualifiedByName = "toConsultation")
	@Mapping(target = "referenceConsultation", source = "contrat.consultation.numero")
	@Mapping(target = "idService", source = "contrat.service.id")
	@Mapping(target = "organisme", source = "contrat.service.organisme.acronyme")
	@Mapping(target = "idCreateur", source = "contrat.utilisateurCreation.idExterne")
	@Mapping(target = "formePrix", source = "contrat.typeFormePrix")
	@Mapping(target = "dateFin", source = "contrat.dateFinContrat")
	@Mapping(target = "dateMaxFin", source = "contrat.dateMaxFinContrat")
	@Mapping(target = "idTitulaire", source = "contrat.attributaire.etablissement.idExterne")
	@Mapping(target = "idAttributeur", source = "contrat.attributaire.etablissement.siret")
	@Mapping(target = "idEtablissementTitulaire", source = "contrat.attributaire.etablissement.idExterne")
	@Mapping(target = "etablissementTitulaire", source = "contrat.attributaire.etablissement")
	@Mapping(target = "idChapeau", source = "contrat.contratChapeau.uuid")
	@Mapping(target = "type", source = "contrat")
	@Mapping(target = "idContact", source = "contrat", qualifiedByName = "toIdContact")
	@Mapping(target = "contactTitulaire", source = "contrat.attributaire")
	@Mapping(target = "favoris", source = "contrat")
	@Mapping(target = "lieuExecutions", source = "contrat")
	@Mapping(target = "lieuxExecutions", source = "contrat")
	@Mapping(target = "organismesEligibles", source = "contrat")
	@Mapping(target = "servicesEligibles", source = "contrat")
	@Mapping(target = "modifications", source = "contrat")
	@Mapping(target = "lienAcSad", source = "contrat.lienAcSad.uuid")
	@Mapping(target = "natureContratConcession", source = "contrat", qualifiedByName = "toNatureContratConcession")
	@Mapping(target = "dateSignature", source = "contrat.dateNotification")
	@Mapping(target = "createur", source = "contrat.createur")
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "horsPassation", source = "contrat.consultation.horsPassation")
	ContratType toDto(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses);

	@Mapping(target = "idExterne", source = "id")
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "dateFinContrat", source = "dateFin")
	@Mapping(target = "dateMaxFinContrat", source = "dateMaxFin")
	@Mapping(target = "dateModificationExterne", source = "dateModification")
	@Mapping(target = "synchronisable", constant = "false")
	@Mapping(target = "typeBorne", source = "typeBorne")
	@Mapping(target = "chapeau", ignore = true)
	@Mapping(target = "created", constant = "true")
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "ccagApplicable", ignore = true)
	@Mapping(target = "typeFormePrix", ignore = true)
	@Mapping(target = "modaliteRevisionPrix", ignore = true)
	@Mapping(target = "revisionPrix", ignore = true)
	@Mapping(target = "besoinReccurent", source = "besoinRecurrent")
	@Mapping(target = "numero", ignore = true)
	@Mapping(target = "numeroLong", ignore = true)
	@Mapping(target = "dateNotification", ignore = true)
	@Mapping(target = "contrats", ignore = true)
	@Mapping(target = "lienAcSad", ignore = true)
	@Mapping(target = "favoris", ignore = true)
	@Mapping(target = "natureContratConcession", ignore = true)
	@Mapping(target = "createur", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "offresRecues", source = "contratType.offresRecues")
	Contrat toContrat(ContratType contratType);

	@Mapping(target = "synchronisable", constant = "false")
	@Mapping(target = "chapeau", constant = "true")
	@Mapping(target = "created", constant = "true")
	@Mapping(target = "idExterne", expression = "java(java.util.UUID.randomUUID().toString())")
	@Mapping(target = "ccagApplicable", source = "contrat.ccagApplicable")
	@Mapping(target = "besoinReccurent", source = "contratType.besoinRecurrent")
	@Mapping(target = "defenseOuSecurite", source = "contratType.defenseOuSecurite")
	@Mapping(target = "dureePhaseEtudeRenouvellement", source = "contratType.dureePhaseEtudeRenouvellement")
	@Mapping(target = "marcheInnovant", source = "contratType.marcheInnovant")
	@Mapping(target = "intitule", source = "contratType.intitule")
	@Mapping(target = "ARenouveler", source = "contratType.ARenouveler")
	@Mapping(target = "objet", source = "contratType.objet")
	@Mapping(target = "referenceLibre", source = "contrat.referenceLibre")
	@Mapping(target = "service", source = "contrat.service")
	@Mapping(target = "uuid", ignore = true)
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "dateModificationExterne", ignore = true)
	@Mapping(target = "groupement", ignore = true)
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "typeFormePrix", ignore = true)
	@Mapping(target = "modaliteRevisionPrix", ignore = true)
	@Mapping(target = "revisionPrix", ignore = true)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "dateCreation", ignore = true)
	@Mapping(target = "numero", ignore = true)
	@Mapping(target = "numeroLong", ignore = true)
	@Mapping(target = "numEj", ignore = true)
	@Mapping(target = "montant", ignore = true)
	@Mapping(target = "montantFacture", ignore = true)
	@Mapping(target = "montantMandate", ignore = true)
	@Mapping(target = "dateNotification", ignore = true)
	@Mapping(target = "typeBorne", ignore = true)
	@Mapping(target = "borneMinimale", ignore = true)
	@Mapping(target = "borneMaximale", ignore = true)
	@Mapping(target = "achatResponsable", ignore = true)
	@Mapping(target = "trancheBudgetaire", ignore = true)
	@Mapping(target = "publicationDonneesEssentielles", ignore = true)
	@Mapping(target = "attributionAvance", ignore = true)
	@Mapping(target = "tauxAvance", ignore = true)
	@Mapping(target = "datePrevisionnelleNotification", ignore = true)
	@Mapping(target = "datePrevisionnelleFinMarche", ignore = true)
	@Mapping(target = "datePrevisionnelleFinMaximaleMarche", ignore = true)
	@Mapping(target = "dureeMaximaleMarche", ignore = true)
	@Mapping(target = "dateDebutExecution", ignore = true)
	@Mapping(target = "dateDebutEtudeRenouvellement", ignore = true)
	@Mapping(target = "idOffre", ignore = true)
	@Mapping(target = "statutEJ", source = "contratType.statutEJ")
	@Mapping(target = "lienAcSad", ignore = true)
	@Mapping(target = "contrats", ignore = true)
	@Mapping(target = "numeroLot", source = "contratType.numeroLot")
	@Mapping(target = "type", ignore = true)
	@Mapping(target = "favoris", ignore = true)
	@Mapping(target = "natureContratConcession", ignore = true)
	@Mapping(target = "valeurGlobale", ignore = true)
	@Mapping(target = "montantSubventionPublique", ignore = true)
	@Mapping(target = "createur", ignore = true)
	@Mapping(target = "dateModification", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "offresRecues", source = "contratType.offresRecues")
	@Mapping(target = "idExternesLot", source = "contratType.idExternesLot")
	Contrat toContratChapeau(ContratType contratType, Contrat contrat);

    @Mapping(target = "donneesEssentielles", source = "contrat.modifications")
    ContratType.Modifications toModifications(Contrat contrat);

    @Mapping(target = "organisme", source = "contrat.organismesEligibles")
    ContratType.OrganismesEligibles toOrganismesEligibles(Contrat contrat);

	@Mapping(target = "service", source = "contrat.servicesEligibles")
	ContratType.ServicesEligibles toServicesEligibles(Contrat contrat);

	@Mapping(target = "idAgent", source = "contrat")
	ContratType.Favoris toFavoris(Contrat contrat);

    @Mapping(target = "id", source = "contact.id")
    @Mapping(target = "nom", source = "contact.nom")
    @Mapping(target = "prenom", source = "contact.nom")
    @Mapping(target = "adresse", source = "etablissement.adresse")
    @Mapping(target = "raisonSociale", source = "etablissement.fournisseur.raisonSociale")
    @Mapping(target = "email", source = "contact.email")
    @Mapping(target = "telephone", source = "contact.telephone")
    @Mapping(target = "fax", source = "etablissement.fournisseur.fax")
    @Mapping(target = "idEtablissement", source = "etablissement.idExterne")
    @Mapping(target = "idEntreprise", source = "etablissement.fournisseur.idExterne")
    @Mapping(target = "login", source = "contact.login")
    @Mapping(target = "inscritAnnuaireDefense", source = "contact.inscritAnnuaireDefense")
    @Mapping(target = "dateModificationRgpd", source = "contact.dateModificationRgpd")
    @Mapping(target = "rgpdCommunicationPlace", source = "contact.rgpdCommunicationPlace")
    @Mapping(target = "rgpdEnquete", source = "contact.rgpdEnquete")
    @Mapping(target = "rgpdCommunication", source = "contact.rgpdCommunication")
    ContactType toContactTitulaire(ContratEtablissement contratEtablissement);


    @Mapping(target = "codeExterne", source = "contrat.type.codeExterne")
    @Mapping(target = "libelle", source = "contrat.type.codeExterne")
    @Mapping(target = "abreviation", source = "contrat.type.label")
    @Mapping(target = "code", source = "contrat.type.code")
    @Mapping(target = "associationProcedures", ignore = true)
    ContratReferentielType toTypeContrat(Contrat contrat);

    @Mapping(target = "technique", source = "contrat", qualifiedByName = "toTechnique")
    @Named("toTechniques")
    ContratType.Techniques toTechniques(Contrat contrat);


    @Mapping(target = "modaliteExecution", source = "contrat", qualifiedByName = "toModaliteExecution")
    @Named("toModalitesExecutions")
    ContratType.ModalitesExecutions toModalitesExecutions(Contrat contrat);

    @Named("toOffresRecues")
    default int toOffresRecues(Contrat contrat) {
        if (contrat == null) {
            return 0;
        }
        if (contrat.getContratChapeau() != null && contrat.getContratChapeau().getOffresRecues() != null) {
            return contrat.getContratChapeau().getOffresRecues();
        }
        return contrat.getOffresRecues() != null ? contrat.getOffresRecues() : 0;
    }

    @Mapping(target = "typePrix", source = "contrat", qualifiedByName = "toTypePrix")
    @Named("toTypesPrix")
    ContratType.TypesPrix toTypesPrix(Contrat contrat);

    @Mapping(target = "codePrincipal", source = "contrat.cpvPrincipal.code")
    @Mapping(target = "codeSecondaire1", expression = "java(toCodeSecondaire(contrat,0))")
    @Mapping(target = "codeSecondaire2", expression = "java(toCodeSecondaire(contrat,1))")
    @Mapping(target = "codeSecondaire3", expression = "java(toCodeSecondaire(contrat,2))")
    CPVType toCpv(Contrat contrat);

    @Mapping(target = "clauses", source = "contrat")
    ContratType.ClausesRSE toClausesRSE(Contrat contrat, @Context Map<String, List<? extends Clause>> mapClauses);

    @Mapping(target = "statutContrat", ignore = true)
    @Mapping(target = "statutContrats", expression = "java(toStatutContrats(contratTitulaireFilter,mapStatutContrat))")
    @Mapping(target = "chapeau", expression = "java(toChapeau(contratTitulaireFilter))")
    ContratCriteria contratTitulaireToContratCriteria(ContratTitulaireFilter contratTitulaireFilter, Map<Integer, List<String>> mapStatutContrat);

	@Mapping(target = "statutContrat", ignore = true)
    @Mapping(target = "statutContrats", expression = "java(toStatutContrats(contratTitulaireFilter,mapStatutContrat))")
    ContratCriteria contratTitulaireFiltreMPEToContratCriteria(ContratTitulaireFilter contratTitulaireFilter, Map<Integer, List<String>> mapStatutContrat);

    @Mapping(target = "ville", source = "commune")
    AdresseType toAdresse(Adresse adresse);

    default String toLieuExecutions(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getLieuxExecution)
                .orElse(Collections.emptySet())
                .stream()
                .map(e -> e.getLabel() + e.getCode())
                .reduce(new StringJoiner("; ", "", ""), StringJoiner::add, StringJoiner::merge)
                .toString();
    }

    default LieuExecutionsType toLieuxExecutions(Contrat contrat) {
        if (contrat.getLieuxExecution().isEmpty()) {
            return null;
        }
        var lieuxExecution = new LieuExecutionsType();
        for (var le : contrat.getLieuxExecution()) {
            lieuxExecution.getLieuExecution().add(le.getCode());
        }
        return lieuxExecution;
    }

    default List<Integer> toIdAgent(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getIdExterneCreateur)
                .map(BeanSynchroUtils::extractId)
                .filter(StringUtils::isNumeric)
                .map(Integer::parseInt)
                .stream().collect(Collectors.toList());
    }


    default String toCodeSecondaire(Contrat contrat, int indice) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getCodesCpv)
                .orElse(Collections.emptySet())
                .stream()
                .skip(indice)
                .findFirst()
                .map(CPV::getCode)
                .orElse(null);
    }

    @Named("toTypePrix")
    default List<TypePrixType> toTypePrix(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTypesPrix)
                .orElse(Collections.emptySet())
                .stream()
                .map(TypePrix::getCode)
                .filter(e -> EnumUtils.isValidEnum(TypePrixType.class, e))
                .map(TypePrixType::valueOf)
                .collect(Collectors.toList());
    }

    @Named("toModaliteExecution")
    default List<ModaliteExecutionType> toModaliteExecution(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getModaliteExecutions)
                .orElse(Collections.emptySet())
                .stream()
                .map(ModaliteExecution::getCode)
                .filter(e -> EnumUtils.isValidEnum(ModaliteExecutionType.class, e))
                .map(ModaliteExecutionType::valueOf)
                .collect(Collectors.toList());
    }

    @Named("toTechnique")
    default List<TechniqueAchatType> toTechnique(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getTechniqueAchats)
                .orElse(Collections.emptySet())
                .stream()
                .map(TechniqueAchat::getCode)
                .filter(e -> EnumUtils.isValidEnum(TechniqueAchatType.class, e))
                .map(TechniqueAchatType::valueOf)
                .collect(Collectors.toList());
    }

    @Named("toNatureContratConcession")
    default NatureContratConcessionType toNatureContratConcession(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getNatureContratConcession)
                .map(NatureContratConcession::getCode)
                .filter(e -> EnumUtils.isValidEnum(NatureContratConcessionType.class, e))
                .map(NatureContratConcessionType::valueOf)
                .orElse(null);
    }


    default List<StatutContrat> toStatutContrats(ContratTitulaireFilter contratTitulaireFilter, Map<Integer, List<String>> mapStatutContrat) {
        var statutContratList = Optional.ofNullable(contratTitulaireFilter.getStatutContrat())
                .stream()
                .filter(e -> e != 0)
                .collect(Collectors.toList());
        List<StatutContrat> l1 = extractStatutContratsFromList(contratTitulaireFilter.getStatutContrats(), mapStatutContrat);
        List<StatutContrat> l2 = extractStatutContratsFromList(statutContratList, mapStatutContrat);
        l1.addAll(l2);
        return l1;
    }

    default Boolean toChapeau(ContratTitulaireFilter contratTitulaireFilter) {
        return Optional.ofNullable(contratTitulaireFilter)
                .map(filter -> (filter.getStatutContrat() != null && filter.getStatutContrat() == 0) ||
                        (!CollectionUtils.isEmpty(filter.getStatutContrats()) && filter.getStatutContrats().stream().anyMatch(e -> e == 0)))
                .orElse(null);
    }

    private List<StatutContrat> extractStatutContratsFromList(List<Integer> statutContrats, Map<Integer, List<String>> mapStatutContrat) {
        return Optional.ofNullable(statutContrats)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(e -> e != 0)
                .flatMap(statutContrat -> mapStatutContrat.getOrDefault(statutContrat, Collections.emptyList()).stream())
                .filter(e -> EnumUtils.isValidEnum(StatutContrat.class, e))
                .map(StatutContrat::valueOf)
                .collect(Collectors.toList());
    }

    default CreateurType.Habilitations map(Set<Habilitation> value) {
        var result = new CreateurType.Habilitations();

        result.getHabilitation().addAll(value.stream().map(Habilitation::getCode).collect(Collectors.toList()));

        return result;
    }

    default int getIdMPE(AbstractSynchronizedEntity entity) {
        if(entity == null || entity.getIdExterne() == null){
            return 0;
        }
        int result = 0;

        var split = entity.getIdExterne().split("_");

        if (split.length > 1 && NumberUtils.isNumber(split[1])) {
            result = Integer.parseInt(split[1]);
        }

        return result;
    }

    @Mapping(target = "id", expression = "java(getIdMPE(organisme))")
    OrganismeType organismeToOrganismeType(Organisme organisme);

    @Mapping(target = "id", expression = "java(getIdMPE(service))")
    @Mapping(target = "libelle", source = "nomCourt")
    ServiceType serviceToServiceType(Service service);

    @Mapping(target = "id", expression = "java(getIdMPE(utilisateur))")
    CreateurType utilisateurToCreateurType(Utilisateur utilisateur);

    @Named("toConsultation")
    default Integer toConsultation(Contrat contrat) {
        return Optional.ofNullable(contrat)
                .map(Contrat::getConsultation)
                .map(Consultation::getIdExterne)
                .map(BeanSynchroUtils::extractId)
                .filter(StringUtils::isNumeric)
                .map(Integer::parseInt)
                .orElse(null);
    }

    @Mapping(target = "statutContrat", ignore = true)
    @Mapping(target = "statutContrats", expression = "java(toStatutContrats(contratTitulaireFilter,mapStatutContrat))")
    @Mapping(target = "chapeau", expression = "java(toChapeau(contratTitulaireFilter))")
    @Mapping(target = "statutContratAppach", expression = "java(toStatutContratsAppach(contratTitulaireFilter))")
    ContratCriteriaAppach contratTitulaireToContratAppachCriteria(ContratTitulaireFilter contratTitulaireFilter, Map<Integer, List<String>> mapStatutContrat);


    default List<Integer> toStatutContratsAppach(ContratTitulaireFilter contratTitulaireFilter) {
        if (contratTitulaireFilter == null) {
            return null;
        }

        List<Integer> statutContrats = new ArrayList<>();
        if (contratTitulaireFilter.getStatutContrats() != null) {
            statutContrats.addAll(contratTitulaireFilter.getStatutContrats());
        }
        if (contratTitulaireFilter.getStatutContrat() != null) {
            statutContrats.add(contratTitulaireFilter.getStatutContrat());
        }
        return statutContrats;
    }

    @Named("toIdContact")
    default Integer toIdContact(Contrat contrat) {
        if (contrat != null && contrat.getAttributaire() != null && contrat.getAttributaire().getContact() != null) {
            if (NumberUtils.isNumber(contrat.getAttributaire().getContact().getIdExterne())) {
                return Integer.parseInt(contrat.getAttributaire().getContact().getIdExterne());
            } else if (contrat.getAttributaire().getContact().getId() != null) {
                return contrat.getAttributaire().getContact().getId().intValue();
            }
        }
        return null;
    }
}
