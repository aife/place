package com.atexo.execution.server.mapper;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.mpe.ws.api.ContratType;
import com.atexo.execution.common.mpe.ws.api.CreateurType;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.clause.ClauseMapper;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Habilitation;
import com.atexo.execution.server.model.RetourChorus;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.repository.paginable.criteres.ContratCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Qualifier;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Qualifier("contratMapperV2")
@Mapper(componentModel = "spring", uses = {MapperUtils.class, ReferentielMapper.class, OrganismeMapperV2.class, ConsultationMapperV2.class, ContratEtablissementMapperV2.class, EvenementMapperV2.class, ClauseMapper.class, EtablissementMapper.class, ContactMapper.class})
public interface ContratMapper {
	String STATUT_EJ_COMMAND = "commandé";

	@Mapping(target = "ccagApplicable", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "consultation", ignore = true)
	@Mapping(target = "horsPassation", ignore = true)
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "publicationDonneesEssentielles", ignore = true)
	@Mapping(target = "defenseOuSecurite", ignore = true)
	@Mapping(target = "achatResponsable", ignore = true)
	@Mapping(target = "marcheInnovant", ignore = true)
	@Mapping(target = "lienAcSad", ignore = true)
	@Mapping(target = "favoris", ignore = true)
	ContratType toWS(Contrat contrat);


    @Mapping(target = "attributaire.contrat", ignore = true)
    @Mapping(target = "attributaire.commanditaire", ignore = true)
    @Mapping(target = "attributaire.contact", ignore = true)
    @Mapping(target = "attributaire.contactReferantList", ignore = true)
    @Mapping(target = "attributaire.sousTraitants", ignore = true)
    @Mapping(target = "attributaire.etablissement", source = "attributaire.etablissement", qualifiedByName = "toDTO")
    @Mapping(target = "contrats", ignore = true)
    @Mapping(target = "referenceContratChapeau", source = "contratChapeau.referenceLibre")
    @Mapping(target = "idContratChapeau", source = "contratChapeau.id")
    @Mapping(target = "echangesChorus", ignore = true)
    @Mapping(target = "createur.service", ignore = true)
    @Mapping(target = "montantTotalAvenants", expression = "java(getMontantTotalAvenants(contrat))")
    @Mapping(target = "codesCPV", source = "codesCpv")
    @Mapping(target = "notifiable", expression = "java(isNotifiable(contrat))")
    @Mapping(target = "clausesDto", source = "contrat")
    @Mapping(target = "idLienAcSad", source = "contrat.lienAcSad.id")
    @Mapping(target = "donneesEssentielles", ignore = true)
    @Mapping(target = "organismeUid", source = "service.organisme.uuid")
    ContratDTO toDto(Contrat contrat);

	@Mapping(target = "attributaire.contrat", ignore = true)
	@Mapping(target = "attributaire.contactReferantList", ignore = true)
	@Mapping(target = "attributaire.etablissement.fournisseur.etablissements", ignore = true)
	@Mapping(target = "attributaire.etablissement.contacts", ignore = true)
	@Mapping(target = "contrats", ignore = true)
	@Mapping(target = "referenceContratChapeau", source = "contratChapeau.referenceLibre")
	@Mapping(target = "idContratChapeau", source = "contratChapeau.id")
	@Mapping(target = "createur.service", ignore = true)
	@Mapping(target = "montantTotalAvenants", expression = "java(getMontantTotalAvenants(contrat))")
	@Mapping(target = "codesCPV", source = "codesCpv")
	@Mapping(target = "notifiable", expression = "java(isNotifiable(contrat))")
	@Mapping(target = "peutEchangerAvecChorus", expression = "java(peutEchangerAvecChorus(contrat))")
	@Mapping(target = "clausesDto", source = "contrat")
	@Mapping(target = "attributaire.commanditaire", source = "attributaire.commanditaire", qualifiedByName = "toDTO")
	@Mapping(target = "idLienAcSad", source = "contrat.lienAcSad.id")
	@Mapping(target = "organismeUid", source = "service.organisme.uuid")
	@Mapping(target = "dateMaxFinContratAsString", source = "dateMaxFinContrat", dateFormat = "dd/MM/yyyy")
	@Mapping(target = "numEj", expression = "java(toNumEj(contrat))")
	@Mapping(target = "dureeSupplementaire", expression = "java(getDureeSupplementaireFromActes(contrat))")
	@Named("toFull")
	ContratDTO toFull(Contrat contrat);

	@Mapping(target = "idExterne", source = "id")
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "dateFinContrat", source = "dateFin")
	@Mapping(target = "dateMaxFinContrat", source = "dateMaxFin")
	@Mapping(target = "dateModificationExterne", source = "dateModification")
	@Mapping(target = "synchronisable", constant = "false")
	@Mapping(target = "typeBorne", source = "typeBorne")
	@Mapping(target = "chapeau", ignore = true)
	@Mapping(target = "created", constant = "true")
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "ccagApplicable", ignore = true)
	@Mapping(target = "typeFormePrix", ignore = true)
	@Mapping(target = "modaliteRevisionPrix", ignore = true)
	@Mapping(target = "revisionPrix", ignore = true)
	@Mapping(target = "besoinReccurent", source = "besoinRecurrent")
	@Mapping(target = "numero", ignore = true)
	@Mapping(target = "numeroLong", ignore = true)
	@Mapping(target = "dateNotification", ignore = true)
	@Mapping(target = "lienAcSad", ignore = true)
	@Mapping(target = "favoris", ignore = true)
	@Mapping(target = "createur", ignore = true)
	Contrat toContrat(ContratType contratType);

	@Mapping(target = "idExterne", expression = "java(generateIdExterne(contratDTO))")
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "dateModificationExterne", ignore = true)
	@Mapping(target = "groupement", ignore = true)
	@Mapping(target = "synchronisable", constant = "false")
	@Mapping(target = "chapeau", source = "chapeau")
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "ccagApplicable", ignore = true)
	@Mapping(target = "typeFormePrix", ignore = true)
	@Mapping(target = "modaliteRevisionPrix", ignore = true)
	@Mapping(target = "revisionPrix", ignore = true)
	@Mapping(target = "created", constant = "true")
	@Mapping(target = "listBondeCommandes", ignore = true)
	@Mapping(target = "listAvenantsNonRejete", ignore = true)
	@Mapping(target = "evenements", ignore = true)
	@Mapping(target = "contrats", ignore = true)
	@Mapping(target = "documents", ignore = true)
	@Mapping(target = "attributaire", ignore = true)
	@Mapping(target = "contratEtablissements", ignore = true)
	@Mapping(target = "lienAcSad", ignore = true)
	Contrat fromDTOtoContrat(ContratDTO contratDTO, boolean chapeau);


    default BigDecimal getMontantTotalAvenants(Contrat contrat) {
        return MapperUtils.getMontantTotalAvenants(contrat);
    }

    @Mapping(target = "utilisateur", source = "utilisateur")
    ContratCriteria toCriteria(Utilisateur utilisateur, ContratCriteriaDTO criteriaDTO);

    @Mapping(target = "utilisateur", ignore = true)
    @Mapping(target = "mpePfUid", source = "mpePfUid")
    ContratCriteria toCriteria(ContratCriteriaDTO criteriaDTO, String mpePfUid);

	@Mapping(target = "synchronisable", constant = "false")
	@Mapping(target = "chapeau", constant = "true")
	@Mapping(target = "created", constant = "true")
	@Mapping(target = "idExterne", expression = "java(java.util.UUID.randomUUID().toString())")
	@Mapping(target = "service", source = "contrat.service")
	@Mapping(target = "categorie", source = "contrat.categorie")
	@Mapping(target = "ccagApplicable", source = "contrat.ccagApplicable")
	@Mapping(target = "consultation", source = "contrat.consultation")
	@Mapping(target = "type", source = "contrat.type")
	@Mapping(target = "besoinReccurent", source = "contratType.besoinRecurrent")
	@Mapping(target = "defenseOuSecurite", source = "contratType.defenseOuSecurite")
	@Mapping(target = "dureePhaseEtudeRenouvellement", source = "contratType.dureePhaseEtudeRenouvellement")
	@Mapping(target = "marcheInnovant", source = "contratType.marcheInnovant")
	@Mapping(target = "intitule", source = "contratType.intitule")
	@Mapping(target = "ARenouveler", source = "contratType.ARenouveler")
	@Mapping(target = "objet", source = "contratType.objet")
	@Mapping(target = "referenceLibre", source = "contratType.referenceLibre")
	@Mapping(target = "uuid", ignore = true)
	@Mapping(target = "modifications", ignore = true)
	@Mapping(target = "organismesEligibles", ignore = true)
	@Mapping(target = "servicesEligibles", ignore = true)
	@Mapping(target = "invitesPonctuels", ignore = true)
	@Mapping(target = "dateModificationExterne", ignore = true)
	@Mapping(target = "groupement", ignore = true)
	@Mapping(target = "statut", ignore = true)
	@Mapping(target = "donneesEssentielles", ignore = true)
	@Mapping(target = "typeFormePrix", ignore = true)
	@Mapping(target = "modaliteRevisionPrix", ignore = true)
	@Mapping(target = "revisionPrix", ignore = true)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "dateCreation", ignore = true)
	@Mapping(target = "numero", ignore = true)
	@Mapping(target = "numeroLong", ignore = true)
	@Mapping(target = "numEj", ignore = true)
	@Mapping(target = "montant", ignore = true)
	@Mapping(target = "montantFacture", ignore = true)
	@Mapping(target = "montantMandate", ignore = true)
	@Mapping(target = "dateNotification", ignore = true)
	@Mapping(target = "typeBorne", ignore = true)
	@Mapping(target = "borneMinimale", ignore = true)
	@Mapping(target = "borneMaximale", ignore = true)
	@Mapping(target = "achatResponsable", ignore = true)
	@Mapping(target = "trancheBudgetaire", ignore = true)
	@Mapping(target = "publicationDonneesEssentielles", ignore = true)
	@Mapping(target = "attributionAvance", ignore = true)
	@Mapping(target = "tauxAvance", ignore = true)
	@Mapping(target = "datePrevisionnelleNotification", ignore = true)
	@Mapping(target = "datePrevisionnelleFinMarche", ignore = true)
	@Mapping(target = "datePrevisionnelleFinMaximaleMarche", ignore = true)
	@Mapping(target = "dureeMaximaleMarche", ignore = true)
	@Mapping(target = "dateDebutExecution", ignore = true)
	@Mapping(target = "dateDebutEtudeRenouvellement", ignore = true)
	@Mapping(target = "idOffre", source = "contratType.idOffre")
	@Mapping(target = "statutEJ", source = "contratType.statutEJ")
	@Mapping(target = "lienAcSad", ignore = true)
	@Mapping(target = "favoris", ignore = true)
	@Mapping(target = "createur", ignore = true)
	@Mapping(target = "dateModification", ignore = true)
	Contrat toContratChapeau(ContratType contratType, Contrat contrat);

    @Mapping(target = "synchronisable", constant = "false")
    @Mapping(target = "chapeau", constant = "true")
    @Mapping(target = "created", constant = "true")
    @Mapping(target = "idExterne", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "service", source = "contrat.service")
    @Mapping(target = "categorie", source = "contrat.categorie")
    @Mapping(target = "ccagApplicable", source = "contrat.ccagApplicable")
    @Mapping(target = "consultation", source = "contrat.consultation")
    @Mapping(target = "type", source = "contrat.type")
    @Mapping(target = "besoinReccurent", source = "contratDTO.besoinReccurent")
    @Mapping(target = "defenseOuSecurite", source = "contratDTO.defenseOuSecurite")
    @Mapping(target = "dureePhaseEtudeRenouvellement", source = "contratDTO.dureePhaseEtudeRenouvellement")
    @Mapping(target = "marcheInnovant", source = "contratDTO.marcheInnovant")
    @Mapping(target = "intitule", source = "contratDTO.intitule")
    @Mapping(target = "ARenouveler", source = "contratDTO.ARenouveler")
    @Mapping(target = "objet", source = "contratDTO.objet")
    @Mapping(target = "referenceLibre", source = "contratDTO.referenceLibre")
    @Mapping(target = "offresRecues", source = "contratDTO.offresRecues")
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "modifications", ignore = true)
    @Mapping(target = "organismesEligibles", ignore = true)
    @Mapping(target = "dateModificationExterne", ignore = true)
    @Mapping(target = "groupement", ignore = true)
    @Mapping(target = "statut", ignore = true)
    @Mapping(target = "donneesEssentielles", ignore = true)
    @Mapping(target = "typeFormePrix", ignore = true)
    @Mapping(target = "modaliteRevisionPrix", ignore = true)
    @Mapping(target = "revisionPrix", ignore = true)
    @Mapping(target = "id", ignore = true)
	@Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "numero", ignore = true)
    @Mapping(target = "numeroLong", ignore = true)
    @Mapping(target = "numEj", ignore = true)
    @Mapping(target = "montant", ignore = true)
    @Mapping(target = "montantFacture", ignore = true)
    @Mapping(target = "montantMandate", ignore = true)
    @Mapping(target = "dateNotification", ignore = true)
    @Mapping(target = "typeBorne", ignore = true)
    @Mapping(target = "borneMinimale", ignore = true)
    @Mapping(target = "borneMaximale", ignore = true)
    @Mapping(target = "achatResponsable", ignore = true)
    @Mapping(target = "trancheBudgetaire", ignore = true)
    @Mapping(target = "publicationDonneesEssentielles", ignore = true)
    @Mapping(target = "attributionAvance", ignore = true)
    @Mapping(target = "tauxAvance", ignore = true)
    @Mapping(target = "datePrevisionnelleNotification", ignore = true)
    @Mapping(target = "datePrevisionnelleFinMarche", ignore = true)
    @Mapping(target = "datePrevisionnelleFinMaximaleMarche", ignore = true)
    @Mapping(target = "dureeMaximaleMarche", ignore = true)
    @Mapping(target = "dateDebutExecution", ignore = true)
    @Mapping(target = "dateDebutEtudeRenouvellement", ignore = true)
    @Mapping(target = "createur", source = "contrat.createur")
    @Mapping(target = "attributaire", ignore = true)
    @Mapping(target = "dateFinContrat", ignore = true)
    @Mapping(target = "dateMaxFinContrat", ignore = true)
    @Mapping(target = "contrats", ignore = true)
    @Mapping(target = "dateDemaragePrestation", ignore = true)
    @Mapping(target = "uniteFonctionnelleOperation", ignore = true)
    @Mapping(target = "codeAchat", ignore = true)
    @Mapping(target = "echangesChorus", ignore = true)
    @Mapping(target = "lieuxExecution", ignore = true)
    @Mapping(target = "statutRenouvellement", ignore = true)
    @Mapping(target = "numeroLot", ignore = true)
    @Mapping(target = "considerationsEnvironnementales", ignore = true)
    @Mapping(target = "considerationsSociales", ignore = true)
    @Mapping(target = "listBondeCommandes", ignore = true)
    @Mapping(target = "listAvenantsNonRejete", ignore = true)
    @Mapping(target = "evenements", ignore = true)
    @Mapping(target = "idOffre", ignore = true)
    @Mapping(target = "cpvPrincipal", ignore = true)
    @Mapping(target = "statutEJ", ignore = true)
    @Mapping(target = "lienAcSad", ignore = true)
    @Mapping(target = "natureContratConcession", ignore = true)
    @Mapping(target = "valeurGlobale", ignore = true)
    @Mapping(target = "montantSubventionPublique", ignore = true)
    @Mapping(target = "servicesEligibles", ignore = true)
    @Mapping(target = "invitesPonctuels", ignore = true)
    Contrat dtoToContratChapeau(ContratDTO contratDTO, Contrat contrat);


    @Mapping(target = "attributaire.contactReferantList", ignore = true)
    @Mapping(target = "listBondeCommandes", ignore = true)
    @Mapping(target = "listAvenantsNonRejete", ignore = true)
    @Mapping(target = "donneesEssentielles", source = "donneesEssentielles")
    Contrat toEntity(ContratDTO contratDTO);

    default boolean isNotifiable(Contrat contrat) {
        if (contrat.getService() != null) {
            var chorus = contrat.getService().isEchangesChorus();
            if (chorus) {
                var isFen211 = contrat.getType() != null && contrat.getType().getModeEchangeChorus() == 2;
                if (isFen211) {
                    return contrat.getStatut() == StatutContrat.ANotifier && contrat.getEchangesChorus()
                            .stream().anyMatch(echange -> echange.getRetourChorus() != null && RetourChorus.COMMANDE_ID_EXTERNE.equalsIgnoreCase(echange.getRetourChorus().getIdExterne()));

                }
                return contrat.getStatut() == StatutContrat.ANotifier && contrat.getStatutEJ() != null && contrat.getStatutEJ().toLowerCase().contains(STATUT_EJ_COMMAND);
            }
        }
        return contrat.getStatut() == StatutContrat.ANotifier;
    }

	default boolean peutEchangerAvecChorus(Contrat contrat) {
		// renvoie true si le contrat ne contient aucun acte en statut EN_ATTENTE_VALIDATION
		return contrat.getService() != null && contrat.getService().isEchangesChorus() && contrat.getActes().stream().noneMatch(acte -> acte.getStatut() == StatutActe.EN_ATTENTE_VALIDATION);
	}

	default Long getDureeSupplementaireFromActes(Contrat contrat) {
		if (contrat.getActes() == null) {
			return 0L;
		}
		return contrat.getActes().stream().filter(ActeModificatif.class::isInstance).map(acte -> ((ActeModificatif) acte).getDureeAjoureeContrat()).filter(Objects::nonNull).reduce(0L, Long::sum);
	}

	default CreateurType.Habilitations map(Set<Habilitation> value) {
		var result =  new CreateurType.Habilitations();

        result.getHabilitation().addAll(value.stream().map(Habilitation::getCode).collect(Collectors.toList()));

        return result;
    }

    default String generateIdExterne(ContratDTO contrat) {
        return (contrat.getIdExterne() == null || contrat.getIdExterne().isEmpty()) ? java.util.UUID.randomUUID().toString() : contrat.getIdExterne();
    }

	default String toNumEj(Contrat contrat) {
		return Optional.ofNullable(contrat)
				.map(Contrat::getNumEj)
				.map(numEj -> {
					String[] numEjSplited = numEj.split("_");
					if (numEjSplited.length > 0) {
						var lastNumEj = numEjSplited[numEjSplited.length - 1];
						var position = lastNumEj.indexOf("#");
						return position != -1 ? lastNumEj.substring(0, lastNumEj.indexOf("#")) : lastNumEj;
					}
					return numEj;
				}).orElse(null);
	}
}
