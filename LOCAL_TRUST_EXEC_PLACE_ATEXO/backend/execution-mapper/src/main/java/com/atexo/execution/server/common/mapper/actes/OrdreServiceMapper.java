package com.atexo.execution.server.common.mapper.actes;

import com.atexo.execution.common.dto.actes.OrdreServiceDTO;
import com.atexo.execution.server.model.actes.OrdreService;

public interface OrdreServiceMapper extends ActeMappable<OrdreService, OrdreServiceDTO> {

}
