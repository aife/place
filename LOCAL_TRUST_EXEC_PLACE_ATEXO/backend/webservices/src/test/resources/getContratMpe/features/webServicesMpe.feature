Feature: 1. fichier de test des web services MPE

  Scenario: 1.1 test du ws get contrat MPE avec le filtre chapeau=true
    Given je lance le script SQL "/getContratMpe/data/contrats.sql"
    When je lance le web service "/webservices/api/v2/contrats/mpe" en post avec le body "json/input/post_contrat_et_son_chapeau.json", je stocke le resultat dans "resultatPostContratMpe" et le code retour dans "codeRetourHttpPostContratMpe"
    And je lance le web service "/webservices/api/v2/contrats/mpe?chapeau=true" en get et je stocke le resultat dans "resultatGetContratMpe" et le code de retour dans "codeRetourHttpGetContratMpe"
    Then je compare le code de retour "codeRetourHttpGetContratMpe" avec "200"
    And je compare le resultat "resultatGetContratMpe" avec le JSON attendu "/getContratMpe/output/resultat-1-1.json" et j'ignore les champs suivants
      | [*].dateCreation | [*].dateModification | [*].uuid |

  Scenario: 1.2 tester l'insertion d'un contrat via le web service POST
    Given je lance le script SQL "/getContratMpe/data/contrats.sql"
    When je lance le web service "/webservices/api/v2/contrats/mpe" en post avec le body "getContratMpe/input/post-contrat-1-2.json", je stocke le resultat dans "resultatPostContratMpe" et le code retour dans "codeRetourHttpPostContratMpe"
    And je stocke le resultat de  la requete SQL "select objet, intitule from contrat where id_offre=18867" dans "resQuery"
    Then je compare le resultat de la requete "resQuery" avec les lignes
#      | objet             | intitule          |
      | Achat d'une voiture | Achat d'une voiture |