Feature: 2. fichier de test du web service get-contrat avec plusieurs jdd

  Scenario Outline: "<nomScenario>"
    Given je lance le script SQL "<scriptSql>"
    When je lance le web service "/webservices/api/v2/contrats/mpe" en post avec le body "<reqPost>", je stocke le resultat dans "resultatPostContratMpe" et le code retour dans "codeRetourHttpPostContratMpe"
    And je lance le web service "/webservices/api/v2/contrats/mpe?chapeau=true" en get et je stocke le resultat dans "resultatGetContratMpe" et le code de retour dans "codeRetourHttpGetContratMpe"
    Then je compare le code de retour "codeRetourHttpGetContratMpe" avec "200"
    And je compare le resultat "resultatGetContratMpe" avec le JSON attendu "<jsonAttendu>" et j'ignore les champs suivants
      | [*].dateCreation | [*].dateModification | [*].uuid |

    Examples:
      | nomScenario            | scriptSql                        | reqPost                                     | jsonAttendu                             |
      | Scénario-1 test ws get | /getContratMpe/data/contrats.sql | json/input/post_contrat_et_son_chapeau.json | /getContratMpe/output/resultat-1-1.json |
      | Scénario-2 test ws get | /getContratMpe/data/contrats.sql | json/input/post_contrat_et_son_chapeau.json | /getContratMpe/output/resultat-1-1.json |
      | Scénario-3 test ws get | /getContratMpe/data/contrats.sql | json/input/post_contrat_et_son_chapeau.json | /getContratMpe/output/resultat-1-1.json |
