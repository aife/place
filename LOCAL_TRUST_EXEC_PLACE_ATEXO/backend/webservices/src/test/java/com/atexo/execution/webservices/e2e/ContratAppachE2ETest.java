package com.atexo.execution.webservices.e2e;

import com.atexo.execution.common.appach.model.contrat.titulaire.ContratTitulaireAppach;
import com.atexo.execution.common.exec.mpe.ws.Exec;
import com.atexo.execution.e2e.auth.KeycloakService;
import com.atexo.execution.e2e.sql.SQLService;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.net.URIBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats MPE")
@ActiveProfiles({"e2e", "keycloak"})
class ContratAppachE2ETest extends BaseWebservicesE2ETest {

    @Value("${e2e.contrat.endpoint:/api/v2/contrats/mpe}")
    private String endpoint;
    @Value("${e2e.contrat.endpoint:/api/v2/contrats/appach}")
    private String endpointGet;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.scripts:sql/contrats.sql}")
    private String[] scripts;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private SQLService sqlService;

    @Autowired
    private ContratRepository contratRepository;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement des scripts {}", String.join(", ", scripts));

        super.setup();

        this.sqlService.load(scripts);
    }


    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre statutContrat=1 on le trouve")
    @Order(1)
    void Given_AValidId_When_PostContrat_Then_ContratIsOK_and_statutContrat_1_modalite_execution_isEmpty() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpointGet)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("statutContrat", "1")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_sans_modalites.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaireAppach>>() {
        });

        var statutContrat = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream().findFirst()
                .map(ContratTitulaireAppach::getStatut)
                .orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(statutContrat).isEqualTo("STATUT_DONNEES_CONTRAT_A_SAISIR");
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre statutContrat=1 on le trouve")
    @Order(2)
    void Given_AValidId_When_PostContrat_Then_ContratIsOK_and_statutContrat_1_modalite_execution_isNotEmpty() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpointGet)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("statutContrat", "1")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_dureeMaxMarche1.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaireAppach>>() {
        });

        var statutContrat = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream()
                .filter(e -> e.getDureeMaximaleMarche() != 0)
                .findFirst()
                .map(ContratTitulaireAppach::getStatut)
                .orElse(null);

        assertThat(statutContrat).isEqualTo(null);
    }


    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre statutContrat=3 on le trouve")
    @Order(3)
    void Given_AValidId_When_PostContrat_Then_ContratIsOK_and_statutContrat_3_modalite_execution_isNotEmpty() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpointGet)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("statutContrat", "3")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_dureeMaxMarche2.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaireAppach>>() {
        });

        var statutContrat = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream()
                .filter(e -> e.getDureeMaximaleMarche() == 2)
                .findFirst()
                .map(ContratTitulaireAppach::getStatut)
                .orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(statutContrat).isEqualTo("STATUT_NOTIFICATION_CONTRAT");
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre statutContrat=0 on le trouve")
    @Order(4)
    void Given_AValidId_When_PostContrat_Then_ContratChapeauIsOK_() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpointGet)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("statutContrat", "0")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_chapeau.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaireAppach>>() {
        });

        var statutContrat = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream()
                .filter(e -> "test contrat chapeau".equalsIgnoreCase(e.getObjet()))
                .findFirst()
                .map(ContratTitulaireAppach::getStatut)
                .orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(statutContrat).isEqualTo("STATUT_DECISION_CONTRAT");
    }

}
