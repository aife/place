package com.atexo.execution.webservices.e2e.utils;

import com.atexo.execution.common.dto.ModificationContrat;
import com.atexo.execution.server.model.actes.ActeModificatif;

import java.util.HashMap;
import java.util.Map;

public class ConversionUtils {

    public static final String OBJET = "objet";
    public static final String DATE_NOTIFICATION = "dateNotification";
    public static final String MONTANT = "montant";

    public static Map<String, String> acteToMap(ActeModificatif acteModificatif) {
        Map<String, String> map = new HashMap<>();
        map.put(OBJET, acteModificatif.getObjet());
        map.put(DATE_NOTIFICATION, acteModificatif.getDateNotification() != null ? String.valueOf(acteModificatif.getDateNotification()) : "");
        map.put(MONTANT, acteModificatif.getMontantHTChiffre() != null ? String.valueOf(acteModificatif.getMontantHTChiffre()) : "");
        return map;
    }

    public static Map<String, String> modificationContratToMap(ModificationContrat modificationContrat) {
        Map<String, String> map = new HashMap<>();
        map.put(OBJET, modificationContrat.getObjetModification());
        map.put(DATE_NOTIFICATION, modificationContrat.getDateSignature() != null ? String.valueOf(modificationContrat.getDateSignature()) : "");
        map.put(MONTANT, modificationContrat.getMontant() != null ? String.valueOf(modificationContrat.getMontant()) : "");
        return map;
    }
}
