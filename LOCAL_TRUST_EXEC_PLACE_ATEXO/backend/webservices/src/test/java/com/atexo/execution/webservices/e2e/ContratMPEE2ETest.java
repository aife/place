package com.atexo.execution.webservices.e2e;

import com.atexo.execution.common.def.StatutRenouvellement;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.common.exec.mpe.ws.Exec;
import com.atexo.execution.common.mpe.model.contrat.titulaire.Consultation;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ContratTitulaire;
import com.atexo.execution.common.mpe.model.contrat.titulaire.Lots;
import com.atexo.execution.e2e.auth.KeycloakService;
import com.atexo.execution.e2e.sql.SQLService;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.net.URIBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats MPE")
@ActiveProfiles({"e2e", "keycloak"})
class ContratMPEE2ETest extends BaseWebservicesE2ETest {

    @Value("${e2e.contrat.endpoint:/api/v2/contrats/mpe}")
    private String endpoint;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.scripts:sql/contrats.sql}")
    private String[] scripts;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private SQLService sqlService;

    @Autowired
    private ContratRepository contratRepository;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement des scripts {}", String.join(", ", scripts));

        super.setup();

        this.sqlService.load(scripts);
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton et un contrat valide QUAND on ajoute le contrat ALORS le retour est 401")
    @Order(1)
    void Given_NoToken_When_PostContrat_Then_ReturnUnauthorized() throws URISyntaxException, IOException {
        var headers = new HttpHeaders();

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();

        log.debug("URI = {}", uri);

        var body = this.read("json/input/post_contrat.json", Exec.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.FOUND);
        assertThat(result.getHeaders().getLocation()).isEqualTo(new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + "/sso/login").build());
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on l'ajoute ALORS le contrat est correctement sauvegardé")
    @Order(2)
    void Given_AValidId_When_PostContrat_Then_ContratIsOK() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();

        log.debug("URI = {}", uri);

        var body = this.read("json/input/post_contrat.json", Exec.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre chapeau=true on le trouve")
    @Order(3)
    void Given_AValidId_When_GetContrat_By_Chapeau_Then_ContratIsOK() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("chapeau", "true")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_et_son_chapeau.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaire>>() {
        });
        var numeroConsultation = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream().findFirst()
                .map(ContratTitulaire::getChapeau).orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(numeroConsultation).isEqualTo(true);
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche avec le filtre idExterneConsultation on le trouve")
    @Order(4)
    void Given_AValidId_When_GetContrat_By_IdExterneConsultation_Then_ContratIsOK() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("idExterneConsultation", "20773")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_et_son_chapeau.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaire>>() {
        });
        var numeroConsultation = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream().findFirst()
                .map(ContratTitulaire::getConsultation)
                .map(Consultation::getNumero).orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(numeroConsultation).isEqualTo("sbh_test_id_externe_consultation_26022025");
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche on trouve l'idExternesService dans le json")
    @Order(5)
    void Given_AValidId_When_GetContrat_Then_idExterneServiceIsOK() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("idExterneConsultation", "20773")
                .build();

        log.debug("URI = {}", uriGet);

        var body = this.read("json/input/post_contrat_et_son_chapeau.json", Exec.class);

        testRestTemplate.exchange(uriInsert, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaire>>() {
        });
        var idExterneService = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream().findFirst()
                .map(ContratTitulaire::getIdExternesService).orElse(null);

        assertThat(resultGet).isNotNull();
        assertThat(resultGet.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(idExterneService).isEqualTo(0);
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on l'ajoute ALORS le contrat est correctement sauvegardé avec l'idExterneLot")
    @Order(6)
    void Given_AValidId_When_PostContrat_Then_ContratIsOK_and_idExterneLot() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();

        log.debug("URI = {}", uri);

        var body = this.read("json/input/post_contrat_idExterneLot.json", Exec.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), ContratType.class);

        var idExterneLot = Optional.ofNullable(result.getBody())
                .map(ContratType::getId)
                .flatMap(e -> contratRepository.findById(e.longValue()))
                .map(Contrat::getIdExternesLot).orElse(null);
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(idExterneLot).isEqualTo(123);
    }

    @Test
    @DisplayName("ETANT DONNE un contrat valide QUAND on le cherche on trouve l'idExternesLot dans le json")
    @Order(7)
    void Given_AValidId_When_GetContrat_Then_idExterneLotIsOK() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();

        var uriGet = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint)
                .addParameter("page", "0")
                .addParameter("itemPerPage", "10")
                .addParameter("idExterneConsultation", "1234")
                .build();


        log.debug("URI = {}", uri);

        var body = this.read("json/input/post_contrat_idExterneLot.json", Exec.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), ContratType.class);

        var resultGet = testRestTemplate.exchange(uriGet, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<ContratTitulaire>>() {
        });


        var idExterneLot = Optional.ofNullable(resultGet)
                .map(ResponseEntity::getBody)
                .orElse(Collections.emptyList())
                .stream().findFirst()
                .map(ContratTitulaire::getLots)
                .map(Lots::getIdExternesLot)
                .orElse(null);
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(idExterneLot).isEqualTo(123);
    }

    @Test
    @DisplayName("Insertion d'un contrat et vérifier la colonne <statut_renouvellement>")
    @Order(8)
    void test_insertion_contrat() throws Exception {
        var token = keycloakService.get();

        log.debug("token = {}", token);

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();

        log.debug("URI = {}", uri);

        var body = this.read("json/input/post_contrat-renouvellement.json", Exec.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), ContratType.class);

        var id = Optional.ofNullable(result)
                .map(ResponseEntity::getBody)
                .map(ContratType::getId)
                .map(Integer::longValue)
                .orElse(0L);

        var contrat = contratRepository.findById(id);

        assertThat(contrat).isNotEmpty();
        assertThat(contrat.get().getStatutRenouvellement()).isEqualTo(StatutRenouvellement.EN_ATTENTE);
    }
}
