package com.atexo.execution.webservices.e2e;

import com.atexo.execution.e2e.BaseE2ETest;
import com.atexo.execution.e2e.configuration.DatabaseScanWSConfig;
import com.atexo.execution.webservices.WebserviceApplication;
import liquibase.integration.spring.SpringLiquibase;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@SpringBootTest(classes = {WebserviceApplication.class, DatabaseScanWSConfig.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public abstract class BaseWebservicesE2ETest extends BaseE2ETest {

	@Value("${context-path:/webservices}")
	protected String contextPath;

	@BeforeAll
	protected void setup() throws SQLException, IOException {
		log.debug("Nettoyage de la BDD avant execution des tests");

	}

	@Bean
	public LiquibaseProperties liquibaseProperties() {
		var properties = new LiquibaseProperties();
		properties.setChangeLog("classpath:db/db.changelog-master.xml");
		return properties;
	}

	@Bean
	@DependsOn(value = "entityManagerFactory")
	public SpringLiquibase liquibase(DataSource dataSource) {
		LiquibaseProperties liquibaseProperties = liquibaseProperties();
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setChangeLog(liquibaseProperties.getChangeLog());
		liquibase.setContexts(liquibaseProperties.getContexts());
		liquibase.setDataSource(dataSource);
		liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
		liquibase.setDropFirst(liquibaseProperties.isDropFirst());
		liquibase.setShouldRun(true);
		liquibase.setLabels(liquibaseProperties.getLabels());
		liquibase.setChangeLogParameters(liquibaseProperties.getParameters());
		return liquibase;
	}
}
