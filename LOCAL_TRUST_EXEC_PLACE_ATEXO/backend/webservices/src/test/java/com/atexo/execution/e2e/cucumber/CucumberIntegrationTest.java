package com.atexo.execution.e2e.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/getContratMpe/features", plugin = {"pretty", "json:target/cucumber-reports/cucumber-report.json", "html:target/cucumber-reports/cucumber-report"})
public class CucumberIntegrationTest {
}
