package com.atexo.execution.e2e.cucumber;

import com.atexo.execution.webservices.e2e.BaseWebservicesE2ETest;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"e2e", "keycloak"})
@CucumberContextConfiguration
public class CucumberConfiguration extends BaseWebservicesE2ETest {
}
