package com.atexo.execution.webservices.e2e;

import com.atexo.execution.common.dto.ModificationContrat;
import com.atexo.execution.e2e.auth.KeycloakService;
import com.atexo.execution.e2e.sql.SQLService;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.services.ImportService;
import com.atexo.execution.webservices.e2e.utils.ConversionUtils;
import com.atexo.execution.webservices.e2e.utils.FileUtils;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.net.URIBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats MPE")
@ActiveProfiles({"e2e", "keycloak"})
public class ImportE2ETest extends BaseWebservicesE2ETest {

    @Value("${e2e.contrat.endpoint:/api/v2/import/avenant}")
    private String endpoint;


    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.scripts:sql/contrats.sql}")
    private String[] scripts;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private SQLService sqlService;
    @Autowired
    private ImportService importService;

    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private ActeRepository acteRepository;
    @Autowired
    private PlateformeRepository plateformeRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private EtablissementRepository etablissementRepository;
    @Autowired
    private ContactRepository contactRepository;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement des scripts {}", String.join(", ", scripts));

        super.setup();

        this.sqlService.load(scripts);
    }

    @Test
    @DisplayName("Test de l'insertion d'un acte à partir un fihcier csv")
    @Order(1)
    void test_insertion_acte_depuis_un_csv() throws Exception {
        //GIVEN
        clearAndInsertData();
        var token = keycloakService.get();
        log.debug("token = {}", token);
        var headers = new HttpHeaders();
        headers.setBearerAuth(token);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new ClassPathResource("csv/modification_contrat.csv"));
        body.add("plateformeUid", "MPE_atexo_release");
        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        //WHEN
        testRestTemplate.exchange(uriInsert, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<ModificationContrat>>() {
        });
        var insertedActe = acteRepository.findAll().stream()
                .map(ActeModificatif.class::cast)
                .findFirst()
                .orElse(new ActeModificatif());


        //THEN
        assertThat(insertedActe.getAcheteur()).isNotNull();
        assertThat(insertedActe.getTitulaire()).isNotNull();
        assertThat(insertedActe.getContrat()).isNotNull();
        assertThat(insertedActe.getNumeroOrdre()).isEqualTo(1);
        assertThat(insertedActe.getTypeModification()).isEqualTo("AVEC_INCIDENCE");
        assertThat(insertedActe.getDureeAjoureeContrat()).isEqualTo(6);
        acteRepository.deleteAll();
        contratRepository.deleteAll();
        utilisateurRepository.delete(utilisateurRepository.findByUuid("MPE_atexo_release_5543").orElse(new Utilisateur()));
        contactRepository.deleteAll();
        etablissementRepository.deleteAll();
    }

    @Test
    @DisplayName("Test de l'insertion des acte à partir un fihcier csv")
    @Order(2)
    void test_insertion_acte_depuis_un_csv____() throws Exception {
        //GIVEN
        clearAndInsertData();
        var token = keycloakService.get();
        log.debug("token = {}", token);
        var headers = new HttpHeaders();
        headers.setBearerAuth(token);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var uriInsert = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new ClassPathResource("csv/modification_contrat_list.csv"));
        body.add("plateformeUid", "MPE_atexo_release");
        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        //WHEN
        testRestTemplate.exchange(uriInsert, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<ModificationContrat>>() {
        });
        var insertedActe = acteRepository.findAll();


        //THEN
        List<Map<String, String>> expectedActesSimplified = importService.csvToModificationContrat(FileUtils.getFileFromResourceAsStream("csv/modification_contrat_list.csv"))
                .stream()
                .map(ConversionUtils::modificationContratToMap)
                .collect(Collectors.toList());

        // Extract and compare specific fields
        List<Map<String, String>> insertedActeSimplified = insertedActe.stream()
                .map(ActeModificatif.class::cast)
                .map(ConversionUtils::acteToMap)
                .collect(Collectors.toList());

        assertThat(insertedActe.size()).isEqualTo(22);
        assertThat(insertedActeSimplified).containsExactlyInAnyOrderElementsOf(expectedActesSimplified);

    }


    private void clearAndInsertData() {
        acteRepository.deleteAll();
        contratRepository.deleteAll();
        utilisateurRepository.delete(utilisateurRepository.findByUuid("MPE_atexo_release_5543").orElse(new Utilisateur()));
        contactRepository.deleteAll();
        etablissementRepository.deleteAll();
        var plateforme = plateformeRepository.findByMpeUid("MPE_atexo_release").orElse(null);
        Contrat contrat = new Contrat();
        contrat.setNumero("20240524");
        contrat.setIdExterne("11");
        contrat.setPlateforme(plateforme);
        contratRepository.save(contrat);

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUuid("MPE_atexo_release_5543");
        utilisateur.setPlateforme(plateforme);
        utilisateurRepository.save(utilisateur);

        Etablissement etablissement = new Etablissement();
        etablissement.setIdExterne("12");
        etablissement.setPlateforme(plateforme);
        etablissement = etablissementRepository.save(etablissement);

        Contact contact = new Contact();
        contact.setNom("nomTest");
        contact.setEmail("test@email.com");
        contact.setPlateforme(plateforme);
        contact.setEtablissement(etablissement);
        contactRepository.save(contact);
    }
}
