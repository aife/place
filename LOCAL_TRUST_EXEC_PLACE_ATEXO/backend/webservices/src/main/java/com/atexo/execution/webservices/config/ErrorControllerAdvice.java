package com.atexo.execution.webservices.config;

import com.atexo.execution.common.dto.ErrorDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.common.exception.RessourcesException;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class ErrorControllerAdvice {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<List<ErrorDTO>> handleException(final Exception exception) {
        log.error("Erreur non gérée {}", exception.getMessage());
        return new ResponseEntity<>(List.of(new ErrorDTO(500, "", "erreur générique", exception)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ApplicationTechnicalException.class})
    public ResponseEntity<List<ErrorDTO>> handleApplicationTechnicalException(final ApplicationTechnicalException exception) {
        final ErrorDTO error = initError(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), exception);
        log.error("Erreur technique {}", exception.getMessage());
        return new ResponseEntity<>(Lists.newArrayList(error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ApplicationBusinessException.class})
    public ResponseEntity<List<ErrorDTO>> handleApplicationBusinessException(final ApplicationBusinessException exception) {
        final List<ApplicationBusinessException.BusinessExceptionItem> items = exception.getItems();

        final List<ErrorDTO> result = new ArrayList<>();
        for (final ApplicationBusinessException.BusinessExceptionItem item : items) {
            result.add(initError(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), exception));
        }
        log.error("Erreur métier {}", exception.getMessage());
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({RessourcesException.class})
    public ResponseEntity<List<ErrorDTO>> handleRessourcesException(final RessourcesException exception) {
        final ErrorDTO error = initError(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), exception);
        log.error("Erreur de ressource", exception);
        return new ResponseEntity<>(Lists.newArrayList(error), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ClientAbortException.class)
    public void handleClientAbortException(ClientAbortException e) {
        log.warn("La communication a été interrompu par le client. Message = {}", e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorDTO>> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        final BindingResult bindingResult = ex.getBindingResult();
        final List<ErrorDTO> result = new ArrayList<>();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();

        for (final FieldError fieldError : fieldErrors) {
            final ErrorDTO error = initError(HttpStatus.BAD_REQUEST.value(), fieldError.getDefaultMessage(), ex);
            error.setRejectedValue(fieldError.getRejectedValue());
            result.add(error);
        }
        log.error("Erreur de validation {}", ex.getMessage());
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }

    private ErrorDTO initError(final Integer code, String message, final Throwable t) {
        String exceptionAsString = null;
        if (t != null) {
            exceptionAsString = exceptionToStringWriter(t);
        }
        return new ErrorDTO(code, null, message, t);
    }

    private String exceptionToStringWriter(final Throwable exception) {
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        return stringWriter.toString();
    }
}
