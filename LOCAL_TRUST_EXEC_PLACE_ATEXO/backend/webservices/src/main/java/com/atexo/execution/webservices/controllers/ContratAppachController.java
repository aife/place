package com.atexo.execution.webservices.controllers;


import com.atexo.execution.common.appach.model.contrat.titulaire.ContratTitulaireAppach;
import com.atexo.execution.common.dto.ContratTitulaireFilter;
import com.atexo.execution.services.ContratsService;
import com.atexo.execution.webservices.config.MappingPlace;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/contrats/appach")
@Slf4j
public class ContratAppachController {

    public static final String DEFAULT_PAGE = "0";
    public static final String DEFAULT_ITEM_PER_PAGE = "10";

    private final ContratsService contratsService;

    private final MappingPlace mappingPlace;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ContratTitulaireAppach>> getContrat(
            @RequestParam(defaultValue = "true") boolean pagination,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @RequestParam(required = false, defaultValue = DEFAULT_ITEM_PER_PAGE) Integer itemPerPage,
            @RequestParam(name = "idContratTitulaire[]", required = false) List<Integer> idContratTitulaires,
            @RequestParam(name = "numeroContrat[]", required = false) List<String> numeroContrats,
            @RequestParam(name = "statutContrat[]", required = false) List<Integer> statutContrats,
            @RequestParam(required = false, name = "statutContrat") Integer statutContrat,
            @RequestParam(required = false, name = "objetContrat") String objetContrat,
            @RequestParam(required = false, name = "intitule") String intitule,
            @RequestParam(required = false, name = "numeroContrat") String numeroContrat,
            @RequestParam(required = false, name = "idContratTitulaire") Integer idContratTitulaire,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false, name = "dateModification") Date dateModification,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false, name = "dateModificationActe") Date dateModificationActe,
            Principal principal) {
        try {
            var pfUid = principal.getName();
            log.info("Le contrat sera associé à la plateforme {}, si la PF n'existe pas veuillez la déclarer dans la table Plateforme de exec", pfUid);
            var contratTitulaireFilter = new ContratTitulaireFilter();
            if (idContratTitulaires != null)
                contratTitulaireFilter.setIdContratTitulaires(idContratTitulaires);
            if (numeroContrats != null)
                contratTitulaireFilter.setNumeroContrats(numeroContrats);
            if (statutContrats != null)
                contratTitulaireFilter.setStatutContrats(statutContrats);
            if (statutContrat != null)
                contratTitulaireFilter.setStatutContrat(statutContrat);
            if (objetContrat != null)
                contratTitulaireFilter.setObjetContrat(objetContrat);
            if (intitule != null)
                contratTitulaireFilter.setIntitule(intitule);
            if (numeroContrat != null)
                contratTitulaireFilter.setNumeroContrat(numeroContrat);
            if (idContratTitulaire != null)
                contratTitulaireFilter.setIdContratTitulaire(idContratTitulaire);
            if (dateModification != null)
                contratTitulaireFilter.setDateModification(dateModification);
            if (dateModificationActe != null)
                contratTitulaireFilter.setDateModificationActe(dateModificationActe);

            PageRequest pageable = null;
            if (pagination && page >= 0 && itemPerPage > 0) {
                pageable = PageRequest.of(page, itemPerPage);
            }
            var contrats = contratsService.getContratAppach(pfUid, contratTitulaireFilter, pageable, mappingPlace.getMappingPlaceInput(), mappingPlace.getMappingPlaceOutput());
            return ResponseEntity.ok(contrats);
        } catch (Exception e) {
            log.error("exception au niveau du getContrat:", e);
            return ResponseEntity.internalServerError().build();
        }

    }
}
