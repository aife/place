package com.atexo.execution.webservices.config.security;

import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.Authentication;

@ConditionalOnProperty(value = "keycloak.enabled", matchIfMissing = true)
public class EnhancedKeycloakAuthenticationAgentProvider extends KeycloakAuthenticationProvider {

    @Override
    public Authentication authenticate(final Authentication authentication) {
        var authenticationToken = (KeycloakAuthenticationToken) authentication;
        return new Account(authenticationToken.getAccount(), authenticationToken.isInteractive(), authenticationToken.getAuthorities());
    }

    @Override
    public boolean supports(final Class<?> aClass) {
        return KeycloakAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
