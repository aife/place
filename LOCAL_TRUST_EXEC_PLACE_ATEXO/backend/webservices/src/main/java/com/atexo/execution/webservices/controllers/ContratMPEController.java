package com.atexo.execution.webservices.controllers;


import com.atexo.execution.common.dto.ContratTitulaireFilter;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exec.mpe.ws.ContactType;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.common.exec.mpe.ws.Exec;
import com.atexo.execution.common.mpe.model.contrat.titulaire.ContratTitulaire;
import com.atexo.execution.server.mapper.ContratWebServiceMapper;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.services.ClauseService;
import com.atexo.execution.services.ContactsService;
import com.atexo.execution.services.ContratsService;
import com.atexo.execution.webservices.config.MappingPlace;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/contrats/mpe")
@Slf4j
public class ContratMPEController {

    public static final String DEFAULT_PAGE = "0";
    public static final String DEFAULT_ITEM_PER_PAGE = "10";
    private final PlateformeRepository plateformeRepository;


    private final ContratsService contratsService;
    private final ContratRepository contratRepository;
    private final ClauseService clauseService;
    private final ContratWebServiceMapper contratWebServiceMapper;
    private final ContactsService contactsService;
    Map<String, List<? extends Clause>> mapClauses = new HashMap<>();

    private final MappingPlace mappingPlace;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ContratTitulaire>> getContrat(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @RequestParam(required = false, defaultValue = DEFAULT_ITEM_PER_PAGE) Integer itemPerPage,
            @ParameterObject ContratTitulaireFilter contratTitulaireFilter,
            Principal principal) {
        try {
            var pfUid = principal.getName();
            log.info("Le contrat sera associé à la plateforme {}, si la PF n'existe pas veuillez la déclarer dans la table Plateforme de exec", pfUid);
            var contrats = contratsService.get(pfUid, contratTitulaireFilter, page, itemPerPage, mappingPlace.getMappingPlaceInput(), mappingPlace.getMappingPlaceOutput());
            return ResponseEntity.ok(contrats);
        } catch (Exception e) {
            log.error("exception au niveau du getContrat:", e);
            return ResponseEntity.internalServerError().build();
        }

    }


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ContratType> ajouterContratMPE(HttpEntity<String> httpEntity, Principal principal) {
        try {
            var pfUid = principal.getName();
            var objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            var prettyJson = httpEntity.getBody();
            log.info("Contenu du json envoyé par MPE {}", prettyJson);
            log.info("Le contrat sera associé à la plateforme {}, si la PF n'existe pas veuillez la déclarer dans la table Plateforme de exec", pfUid);
            var mpe = objectMapper.readValue(prettyJson, Exec.class);
            ContratType contratType = Optional.ofNullable(mpe).map(Exec::getEnvoi).map(Exec.Envoi::getContrat).orElseThrow();
            return ResponseEntity.ok(contratsService.save(contratType, pfUid));
        } catch (Exception e) {
            log.error("exception lors de l'ajout d'un contrat MPE:", e);
            return ResponseEntity.internalServerError().build();
        }

    }

    @GetMapping("/{uuid}")
    public ResponseEntity<ContratType> getByUuid(Principal principal, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche du contrat avec l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, mpeUid).orElseThrow(ApplicationBusinessException::new);
        return ResponseEntity.ok(contratWebServiceMapper.toDto(contrat, mapClauses));
    }

    @GetMapping("/{uuid}/contrats-lies")
    public ResponseEntity<List<ContratType>> getContratsLies(Principal principal, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche des contrats liés au contrat l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, mpeUid).orElseThrow(() -> new ApplicationBusinessException(MessageFormat.format("Le contrat avec l''uuid=[{0}] n''existe pas.", uuid)));
        var sousContrat = contrat.getContrats()
                .stream()
                .map(e -> contratWebServiceMapper.toDto(e, mapClauses))
                .collect(Collectors.toList());
        return ResponseEntity.ok(sousContrat);
    }

    @GetMapping("/{uuid}/contacts")
    public ResponseEntity<List<ContactType>> getContacts(Principal principal, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche des contacts liés au contrat l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        return ResponseEntity.ok(contactsService.getContactsByContrat(uuid, mpeUid));
    }

    @PostConstruct
    void initClauses() {
        mapClauses = clauseService.getMapClauses();
    }

}
