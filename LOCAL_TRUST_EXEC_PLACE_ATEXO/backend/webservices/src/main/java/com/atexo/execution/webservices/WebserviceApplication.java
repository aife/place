package com.atexo.execution.webservices;

import com.atexo.execution.webservices.config.YamlPropertySourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.atexo.execution")
@EnableTransactionManagement
@EntityScan(basePackages = "com.atexo.execution.server.model")
@EnableJpaRepositories(basePackages = "com.atexo.execution.server.repository")
@EnableJpaAuditing
@PropertySource(value = {"classpath:config-env.properties"}, encoding = "UTF-8")
@PropertySource(value = {"classpath:mapping-place.yml"}, encoding = "UTF-8", factory = YamlPropertySourceFactory.class)
public class WebserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebserviceApplication.class, args);
    }

}
