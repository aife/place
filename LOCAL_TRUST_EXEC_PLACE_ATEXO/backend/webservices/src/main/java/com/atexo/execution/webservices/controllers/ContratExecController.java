package com.atexo.execution.webservices.controllers;

import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.services.ContratsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/contrats/exec")
@Slf4j
public class ContratExecController {

    private final ContratsService contratsService;
    private final ContratMapper contratMapper;
    private final ContratRepository contratRepository;
    private final PlateformeRepository plateformeRepository;


    @GetMapping("/")
    public ResponseEntity<Page<ContratDTO>> findByGet(Principal principal, @ParameterObject ContratCriteriaDTO criteria, final Pageable pageable) throws ApplicationException {
        var pfUid = criteria.getPfUid();
        log.info("Authentification de l'utilisateur [{}]", principal.getName());
        log.info("Recherche des contrats pour la plateforme [{}]", pfUid);
        var contratPage = contratsService.rechercher(pfUid, criteria, pageable);
        var result = new PageImpl<>(contratPage.getContent().stream().map(contratMapper::toDto).collect(Collectors.toList())
                , pageable, contratPage.getTotalElements());
        return ResponseEntity.ok(result);
    }

    @PostMapping("/")
    public ResponseEntity<Page<ContratDTO>> find(Principal principal, @RequestBody ContratCriteriaDTO criteria, final Pageable pageable) throws ApplicationException {
        var pfUid = criteria.getPfUid();
        log.info("Authentification de l'utilisateur [{}]", principal.getName());
        log.info("Recherche des contrats pour la plateforme [{}]", pfUid);
        var contratPage = contratsService.rechercher(pfUid, criteria, pageable);
        var result = new PageImpl<>(contratPage.getContent().stream().map(contratMapper::toDto).collect(Collectors.toList()), pageable, contratPage.getTotalElements());
        return ResponseEntity.ok(result);
    }

    @PatchMapping("/{uuid}")
    public ResponseEntity<ContratDTO> modify(@PathVariable("uuid") String uuid, @RequestBody ContratDTO contratDTO, @RequestParam String pfUid) throws ApplicationBusinessException {
        log.info("Modification du contrat avec l'uuid=[{}], depuis la plateforme {}", uuid, pfUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, pfUid).orElseThrow(ApplicationBusinessException::new);
        contrat.setDateModificationExterne(LocalDateTime.now());
        contrat.setStatutRenouvellement(contratDTO.getStatutRenouvellement());
        contrat = contratRepository.save(contrat);
        return ResponseEntity.ok(contratMapper.toFull(contrat));
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<ContratDTO> getByUuid(@PathVariable("uuid") String uuid, @RequestParam String pfUid) throws ApplicationBusinessException {
        log.info("Recherche du contrat avec l'uuid=[{}], depuis la plateforme {}", uuid, pfUid);

        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, pfUid).orElseThrow(ApplicationBusinessException::new);
        return ResponseEntity.ok(contratMapper.toFull(contrat));
    }

    @GetMapping("/{uuid}/contrats-lies")
    public ResponseEntity<List<ContratDTO>> getContratsLies(Principal principal, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche des contrats liés au contrat l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, mpeUid).orElseThrow(() -> new ApplicationBusinessException(MessageFormat.format("Le contrat avec l''uuid=[{0}] n''existe pas.", uuid)));
        var sousContrat = contrat.getContrats()
                .stream()
                .map(contratMapper::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(sousContrat);
    }
}
