package com.atexo.execution.webservices.controllers;

import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exec.mpe.ws.ContratType;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;
import com.atexo.execution.server.mapper.ContratWebServiceMapper;
import com.atexo.execution.server.model.clause.Clause;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.services.ClauseService;
import com.atexo.execution.services.ContratsService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/contrats")
@Slf4j
@Deprecated
public class ContratController {

    Map<String, List<? extends Clause>> mapClauses = new HashMap<>();

    private final ContratsService contratsService;
    private final ContratWebServiceMapper contratWebServiceMapper;
    private final ContratRepository contratRepository;
    private final ClauseService clauseService;
    private final PlateformeRepository plateformeRepository;


    @PostConstruct
    void initClauses() {
        mapClauses = clauseService.getMapClauses();
    }

    @GetMapping("/")
    public ResponseEntity<Page<ContratType>> findByGet(Principal principal, @ParameterObject ContratCriteriaDTO criteria, final Pageable pageable) throws ApplicationException {
        var pfUid = principal.getName();
        log.info("Recherche des contrats pour la plateforme [{}]", pfUid);
        var contratPage = contratsService.rechercher(pfUid, criteria, pageable);
        var result = new PageImpl<>(contratPage.getContent().stream().map(e -> contratWebServiceMapper.toDto(e, mapClauses)).collect(Collectors.toList())
                , pageable, contratPage.getTotalElements());
        return ResponseEntity.ok(result);
    }

    @PostMapping("/")
    public ResponseEntity<Page<ContratType>> find(Principal principal,@RequestBody ContratCriteriaDTO criteria, final Pageable pageable) throws ApplicationException {
        var pfUid = principal.getName();
        log.info("Recherche des contrats pour la plateforme [{}]", pfUid);
        var contratPage = contratsService.rechercher(pfUid,criteria, pageable);
        var result = new PageImpl<>(contratPage.getContent().stream().map(e -> contratWebServiceMapper.toDto(e, mapClauses)).collect(Collectors.toList()), pageable, contratPage.getTotalElements());
        return ResponseEntity.ok(result);
    }

    @PatchMapping("/{uuid}")
    public ResponseEntity<ContratType> modify(Principal principal, @PathVariable("uuid") String uuid, @RequestBody ContratDTO contratDTO) throws ApplicationBusinessException {
        log.info("Modification du contrat avec l'uuid=[{}], depuis la plateforme {}", uuid, principal.getName());
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, plateforme.getMpeUid()).orElseThrow(ApplicationBusinessException::new);
        contrat.setDateModificationExterne(LocalDateTime.now());
        contrat.setStatutRenouvellement(contratDTO.getStatutRenouvellement());
        contrat = contratRepository.save(contrat);
        return ResponseEntity.ok(contratWebServiceMapper.toDto(contrat, mapClauses));
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<ContratType> getByUuid(Principal principal,@PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche du contrat avec l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, mpeUid).orElseThrow(ApplicationBusinessException::new);
        return ResponseEntity.ok(contratWebServiceMapper.toDto(contrat, mapClauses));
    }

    @GetMapping("/{uuid}/contrats-lies")
    public ResponseEntity<List<ContratType>> getContratsLies(Principal principal,@PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var plateforme = plateformeRepository.findByMpeUidOrUtilisateurTechnique(principal.getName()).orElseThrow(() -> new ApplicationBusinessException("Plateforme non trouvée " + principal.getName()));
        String mpeUid = plateforme.getMpeUid();
        log.info("Recherche des contrats liés au contrat l'uuid=[{}], depuis la plateforme {}", uuid, mpeUid);
        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, mpeUid).orElseThrow(() -> new ApplicationBusinessException(MessageFormat.format("Le contrat avec l''uuid=[{0}] n''existe pas.", uuid)));
        var sousContrat = contrat.getContrats()
                .stream()
                .map(e -> contratWebServiceMapper.toDto(e, mapClauses))
                .collect(Collectors.toList());
        return ResponseEntity.ok(sousContrat);
    }

    @PatchMapping("/{uuid}/echanges-chorus")
    public ResponseEntity<ContratDTO> updateEchangeChorus(Principal principal, HttpEntity<String> httpEntity) throws ApplicationException, JsonProcessingException {
        var pfUid = principal.getName();
        var objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var prettyJson = httpEntity.getBody();
        log.info("Contenu du json envoyé par MPE {}", prettyJson);
        var echangeChorusType = objectMapper.readValue(prettyJson, EchangeChorusType.class);
        log.info("Mise à jour du contrat {} suite à un échange chorus pour la plateforme [{}]", echangeChorusType.getId(), pfUid);
        return ResponseEntity.ok(contratsService.updateEchangeChorus(pfUid, echangeChorusType));
    }
}
