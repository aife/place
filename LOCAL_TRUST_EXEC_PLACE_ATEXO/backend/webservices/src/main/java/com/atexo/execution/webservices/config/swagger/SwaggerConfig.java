package com.atexo.execution.webservices.config.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition
public class SwaggerConfig {

    private static final String AUTH = "/auth";
    private static final String TOKEN = "/token";
    private static final String OAUTH_SCHEME_NAME = "oAuth";

    @Value("${keycloak.auth-server-url:}")
    private String authServerUrl;
    @Value("${keycloak.realm:}")
    private String realm;
    @Value("${format.protocol.url.keycloak:}")
    private String protocolUrlFormat;

    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI()
                .components(new Components()
                        .addSecuritySchemes(OAUTH_SCHEME_NAME, createOAuthScheme()))
                .addSecurityItem(new SecurityRequirement().addList(OAUTH_SCHEME_NAME));
    }

    private SecurityScheme createOAuthScheme() {
        OAuthFlows flows = createOAuthFlows();
        return new SecurityScheme()
                .type(SecurityScheme.Type.OAUTH2)
                .flows(flows);
    }

    private OAuthFlows createOAuthFlows() {
        OAuthFlow flow = createAuthorizationCodeFlow();
        return new OAuthFlows()
                .authorizationCode(flow);
    }

    private OAuthFlow createAuthorizationCodeFlow() {
        var protocolUrl = String.format(protocolUrlFormat, authServerUrl, realm);
        return new OAuthFlow()
                .authorizationUrl(protocolUrl + AUTH)
                .tokenUrl(protocolUrl + TOKEN);
    }
}