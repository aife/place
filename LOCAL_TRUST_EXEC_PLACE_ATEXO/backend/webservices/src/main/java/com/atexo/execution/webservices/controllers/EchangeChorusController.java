package com.atexo.execution.webservices.controllers;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exec.mpe.ws.EchangeChorusType;
import com.atexo.execution.services.EchangeChorusService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/echanges-chorus")
@Slf4j
public class EchangeChorusController {


    private final EchangeChorusService echangeChorusService;

    @PatchMapping("/")
    public ResponseEntity<EchangeChorusDTO> findByGet(Principal principal, HttpEntity<String> httpEntity) throws ApplicationException, JsonProcessingException {
        var pfUid = principal.getName();
        var objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var prettyJson = httpEntity.getBody();
        log.info("Contenu du json envoyé par MPE {}", prettyJson);
        var echangeChorusType = objectMapper.readValue(prettyJson, EchangeChorusType.class);
        log.info("Mise à jour de l'échnage chorus {} pour la plateforme [{}]", echangeChorusType.getId(), pfUid);
        return ResponseEntity.ok(echangeChorusService.updateEchangeChorus(pfUid, echangeChorusType));
    }
}
