package com.atexo.execution.webservices.controllers;

import com.atexo.execution.common.dto.docgen.ChampsFusionDTO;
import com.atexo.execution.services.ChampsFusionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v2/champs-fusion")
@RequiredArgsConstructor
@Slf4j
public class ChampFusionController {

    private final ChampsFusionService champsFusionService;

    @GetMapping
    ResponseEntity<List<ChampsFusionDTO>> getChampsFusion(@RequestParam(required = false, defaultValue = "10") Integer max, @RequestParam(required = false, defaultValue = "false") boolean eager, @RequestParam(required = false, defaultValue = "false") boolean tree) {
        return ResponseEntity.ok(champsFusionService.getChampsFusion(eager, tree, max));
    }

    @GetMapping(value = "/{domaine}")
    ResponseEntity<List<ChampsFusionDTO>> getChampsFusionByDomaine(@RequestParam(required = false, defaultValue = "10") Integer max, @RequestParam(required = false, defaultValue = "false") boolean eager, @RequestParam(required = false, defaultValue = "false") boolean tree, @PathVariable String domaine) {
        return ResponseEntity.ok(champsFusionService.getChampsFusion(eager, tree, max, domaine));
    }
}
