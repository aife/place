package com.atexo.execution.webservices.controllers;

import com.atexo.execution.common.dto.ModificationContrat;
import com.atexo.execution.services.ImportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v2/import")
@Slf4j
public class ImportController {

    @Autowired
    private ImportService importService;

    @PostMapping("/avenant")
    public List<ModificationContrat> find(@RequestParam("file") MultipartFile file, @RequestParam("plateformeUid") String plateformeUid) {
        return importService.importAvenant(file,plateformeUid);
    }
}
