package com.atexo.execution.webservices.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "place")
public class MappingPlace {
    Map<String, Map<Integer, List<String>>> mappingPlaceInput;
    Map<String, Map<String, String>> mappingPlaceOutput;
}
