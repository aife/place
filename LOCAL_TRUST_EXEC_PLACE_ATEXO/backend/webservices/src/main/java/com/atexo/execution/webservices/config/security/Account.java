package com.atexo.execution.webservices.config.security;

import org.keycloak.adapters.spi.KeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class Account extends KeycloakAuthenticationToken {


    public Account(final KeycloakAccount account, final boolean interactive) {
        super(account, interactive);
    }

    public Account(final KeycloakAccount account, final boolean interactive, final Collection<? extends GrantedAuthority> authorities) {
        super(account, interactive, authorities);
    }

    public String getIdentifiant() {
        return getAccount().getPrincipal().getName();
    }

}
