package com.atexo.execution.webservices.config.security;

import com.auth0.jwt.JWT;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.representations.adapters.config.AdapterConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
@ConditionalOnProperty(value = "keycloak.enabled", matchIfMissing = true)
public class KeyCloakAgentConfiguration extends KeycloakSpringBootConfigResolver {
    private final ConcurrentHashMap<String, KeycloakDeployment> cache = new ConcurrentHashMap<>();
    private final KeycloakDeployment keycloakDeployment;

    public KeyCloakAgentConfiguration(KeycloakSpringBootProperties properties) {
        keycloakDeployment = KeycloakDeploymentBuilder.build(properties);
    }

    @Override
    public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {

        var tokenOpt = Optional.ofNullable(request.getHeader("Authorization")).map(h -> h.substring("Bearer ".length()));
        if (tokenOpt.isPresent()) {
            var token = tokenOpt.get();
            var jwt = JWT.decode(token);
            var issuer = jwt.getIssuer();
            var realm = issuer.substring(issuer.lastIndexOf("/") + 1);
            var clientId = jwt.getClaim("azp").asString();
            var key = realm + clientId;
            if (!cache.containsKey(key)) {
                var adapterConfig = new AdapterConfig();
                adapterConfig.setRealm(realm);
                adapterConfig.setAuthServerUrl(keycloakDeployment.getAuthServerBaseUrl());
                adapterConfig.setSslRequired(adapterConfig.getSslRequired());
                adapterConfig.setResource(clientId);
                adapterConfig.setPublicClient(keycloakDeployment.isPublicClient());
                adapterConfig.setConfidentialPort(keycloakDeployment.getConfidentialPort());
                adapterConfig.setCredentials(keycloakDeployment.getResourceCredentials());
                adapterConfig.setPrincipalAttribute(keycloakDeployment.getPrincipalAttribute());
                var deploymentBuilder = KeycloakDeploymentBuilder.build(adapterConfig);
                cache.put(key, deploymentBuilder);
            }

            return cache.get(key);
        }
        // default config
        return keycloakDeployment;
    }
}
