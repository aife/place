package com.atexo.execution.server.e2e;

import com.atexo.execution.common.dto.actes.ActeModificatifDTO;
import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des actes")
@ActiveProfiles("oauth")
class ActeE2ETest extends BaseServerE2ETest {

    @Value("${e2e.acte.endpoint:/api/actes}")
    private String endpoint;

    @Value("${e2e.acte.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.acte.scripts:sql/actes.sql}")
    private String[] scripts;

    @Autowired
    private OauthService oauthTokenService;

    @Autowired
    private SQLService sqlService;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement des scripts {}", String.join(", ", scripts));

        super.setup();

        this.sqlService.load(scripts);
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton et un identifiant d'acte valide QUAND on récupere l'acte depuis cet id ALORS le retour est 401")
    @Order(1)
    void Given_NoToken_When_GetActe_Then_ReturnUnauthorized() throws URISyntaxException {
        var id = 1;

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + id ).build();

        log.debug("URI = {}", uri);

        var result = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), Object.class);

        assertThat(result).isNotNull();
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("ETANT DONNE un identifiant invalide QUAND on tente de récupérer l'acte depuis cet id sans jeton ALORS rien n'est trouvé")
    @Order(1)
    void Given_InvalidId_When_GetActeById_Then_ReturnNotFoundException() throws URISyntaxException {
        var id = 2;

        var headers = new HttpHeaders();
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + id ).build();

        log.debug("URI = {}", uri);

        var result = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), ActeModificatifDTO.class);

        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("ETANT DONNE un identifiant d'acte valide QUAND on récupere l'acte depuis cet id ALORS l'acte est correct")
    @Order(3)
    void Given_AValidId_When_GetActeById_Then_ActeIsOK() throws Exception {
        var id = 1;

        var headers = new HttpHeaders();
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + id ).build();

        log.debug("URI = {}", uri);

        var result = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), ActeModificatifDTO.class);

        assertThat(result).isNotNull();

        var acte = result.getBody();

        assertThat(acte).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertMatch("json/expected/get_acte_by_id.json", acte);
    }

    @AfterAll
    public void cleanUp() throws SQLException, IOException {
        log.debug("Nottoyage de la BDD");
    }
}
