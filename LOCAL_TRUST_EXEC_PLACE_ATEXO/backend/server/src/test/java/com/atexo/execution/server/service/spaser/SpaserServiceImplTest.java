package com.atexo.execution.server.service.spaser;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.server.clients.oauth2.Oauth2Client;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.crud.ContratRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SpaserServiceImplTest {

    @Mock
    Oauth2Client oauth2Client;

    @Mock
    ContratRepository contratRepository;

    @InjectMocks
    SpaserServiceImpl spaserService;





    @Test
    void Given_NonExistantContrant_Then_ThrowException(){
        UtilisateurDTO utilisateur = new UtilisateurDTO();

        when(contratRepository.findById(67L)).thenReturn(Optional.empty());
        Exception ex = assertThrows(IllegalArgumentException.class, () -> {
            spaserService.initContexte(utilisateur,67L);
        });
        assertTrue(ex.getMessage().contains("Contrat introuvable"));
    }

    @Test
    void Given_NonRealm_Then_ThrowException(){
        UtilisateurDTO utilisateur = new UtilisateurDTO();

        Contrat x = new Contrat();
        when(contratRepository.findById(67L)).thenReturn(Optional.of(x));
        Exception ex = assertThrows(IllegalArgumentException.class, () -> {
            spaserService.initContexte(utilisateur,67L);
        });


        assertTrue(ex.getMessage().contains("Erreur de connexion d'authentification"));
    }


}
