package com.atexo.execution.server.basecentrale;

import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.api.entreprise.v3.service.ApiEntrepriseService;
import com.atexo.execution.server.common.mapper.basecentrale.EtablissementV3MapperImpl;
import com.atexo.execution.server.common.mapper.basecentrale.FournisseurV3MapperImpl;
import com.atexo.execution.server.entreprise.v3.model.V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneEtablissementsSiretGet200ResponseData;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneUnitesLegalesSirenGet200ResponseData;
import com.atexo.execution.server.interf.basecentrale.BaseCentraleInterface;
import com.atexo.execution.server.interf.basecentrale.BaseCentraleInterfaceImpl;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.CategorieFournisseurRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ApiEntrepriseV3Test {


    public static final String DOCUMENTS = "documents";
    public static final String META = "meta";
    private BaseCentraleInterface baseCentraleInterface;

    @Mock
    private ApiEntrepriseService apiEntrepriseService;

    @Mock
    private UtilisateurRepository utilisateurRepository;

    @Mock
    private CategorieFournisseurRepository categorieFournisseurRepository;

    @BeforeEach
    public void setUp() {
        this.baseCentraleInterface = new BaseCentraleInterfaceImpl(new EtablissementV3MapperImpl(), new FournisseurV3MapperImpl(), apiEntrepriseService, new RestTemplate(), utilisateurRepository, categorieFournisseurRepository);
        Utilisateur utilisateur = new Utilisateur();
        Service service = new Service();
        Organisme organisme = new Organisme();
        organisme.setSiren("440909562");
        organisme.setComplement("00041");
        service.setOrganisme(organisme);
        utilisateur.setService(service);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        Mockito.when(baseCentraleInterface.getCurrentSiret()).thenReturn("44090956200041");
        Mockito.when(utilisateurRepository.findOneByIdentifiant(Mockito.anyString())).thenReturn(Optional.of(utilisateur));
    }

    @Test
    public void test_get_entreprise_by_siren() throws ApplicationTechnicalException {
        //GIVEN
        V3InseeSireneUnitesLegalesSirenGet200ResponseData entreprise = new V3InseeSireneUnitesLegalesSirenGet200ResponseData();
        entreprise.setSiren("215301797");
        entreprise.setSiretSiegeSocial("21530179700019");
        V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData kbis = new V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData();
        kbis.setSiren("215301797");
        V3InseeSireneEtablissementsSiretGet200ResponseData etablissement = new V3InseeSireneEtablissementsSiretGet200ResponseData();
        etablissement.setSiret("21530179700019");

        //WHEN
        Mockito.when(apiEntrepriseService.getEntreprise("215301797", "44090956200041")).thenReturn(entreprise);
        Mockito.when(apiEntrepriseService.getExtraitKbis("215301797", "44090956200041")).thenReturn(kbis);
        Mockito.when(apiEntrepriseService.getEtablissement("21530179700019", "44090956200041")).thenReturn(etablissement);
        Optional<Fournisseur> fournisseur = baseCentraleInterface.getEntrepriseBySiren("215301797");

        //THEN
        assertThat(fournisseur.get()).usingRecursiveComparison().isNotEqualTo(new Fournisseur());
    }


    @Test
    public void test_get_etablissement_by_siret() throws ApplicationTechnicalException {
        //GIVEN
        V3InseeSireneEtablissementsSiretGet200ResponseData input = new V3InseeSireneEtablissementsSiretGet200ResponseData();
        input.setSiret("21530179700019");

        //WHEN
        Mockito.when(apiEntrepriseService.getEtablissement("21530179700019", "44090956200041")).thenReturn(input);
        Optional<Etablissement> etablissement = baseCentraleInterface.getEtablissementBySiret("21530179700019");

        //THEN
        assertThat(etablissement.get())
                .usingRecursiveComparison()
                .isNotEqualTo(new Etablissement());
    }


    @Test
    public void test_get_documents_by_siren() {
        //GIVEN
        Mockito.when(apiEntrepriseService.getAttestationCotisationsCongesPayesChomageIntemperies("215301797", "44090956200041")).thenReturn("https://storge/attestation_cotisation.pdf");
        Mockito.when(apiEntrepriseService.getCarteProfessionnelleTravauxPublics("215301797", "44090956200041")).thenReturn("https://storge/caarte_prof.pdf");
        Mockito.when(apiEntrepriseService.getAttestationFiscale("215301797", "44090956200041")).thenReturn("https://storge/attestation_fiscale.pdf");
        Mockito.when(apiEntrepriseService.getAttestationVigilance("215301797", "44090956200041")).thenReturn("https://storge/attestation_vigilance.pdf");

        //WHEN
        Optional<Map<String, Object>> links = baseCentraleInterface.getDocumentsBySiren("215301797");

        //THEN
        assertThat(links.isPresent()).isTrue();
        assertThat(links.get()).containsKeys(DOCUMENTS, META);
        Map<String, Object> documents = (Map<String, Object>) links.get().get(DOCUMENTS);
        Map<String, Object> meta = (Map<String, Object>) links.get().get(META);
        assertThat(documents.values()).allMatch(Objects::nonNull);
        assertThat(meta.values()).allMatch(Objects::nonNull);
    }

    @Test
    public void test_get_documents_by_siret() {
        //GIVEN
        Mockito.when(apiEntrepriseService.getAttestationCotisationsRetraite("21530179700019", "44090956200041")).thenReturn("https://storge/attestation_cotisation_retraite.pdf");

        //WHEN
        Optional<Map<String, Object>> links = baseCentraleInterface.getDocumentsBySiret("21530179700019");

        //THEN
        assertThat(links.isPresent()).isTrue();
        assertThat(links.get()).containsKeys(DOCUMENTS, META);
        Map<String, Object> documents = (Map<String, Object>) links.get().get(DOCUMENTS);
        Map<String, Object> meta = (Map<String, Object>) links.get().get(META);
        assertThat(documents.values()).allMatch(Objects::nonNull);
        assertThat(meta.values()).allMatch(Objects::nonNull);
    }


}
