package com.atexo.execution.server.service;

import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapperImpl;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.model.TypeEvenement;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import com.atexo.execution.server.repository.referentiels.TypeEvenementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.atexo.execution.common.def.ReferentielDiscriminators.TYPE_ACTE;
import static com.atexo.execution.common.def.ReferentielDiscriminators.TYPE_EVENEMENT;
import static com.atexo.execution.server.model.ParametrageApplication.CLE_ACTES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReferentielServiceImplTest {

    @InjectMocks
    ReferentielServiceImpl referentielService;

    @Mock
    ParametrageService parametrageService;

    @Mock
    TypeActeRepository typeActeRepository;

    @Mock
    TypeEvenementRepository typeEvenementRepository;

    @Spy
    ReferentielMapper referentielMapper = new ReferentielMapperImpl();

    @BeforeEach()
    void setUp() {
        // setup actes
        var typeActeModificatif = new TypeActe();
        typeActeModificatif.setCode(TypeActe.CODE_ACTE_MODIFICATIF);
        var typeActeAgrementSousTraitance = new TypeActe();
        typeActeAgrementSousTraitance.setCode(TypeActe.CODE_AGREMENT_SOUS_TRAITANCE);
        typeActeModificatif.setActif(true);
        var autreTypeActe = new TypeActe();
        autreTypeActe.setCode("AUTRE");
        autreTypeActe.setActif(true);
        var allTypesActes = List.of(typeActeModificatif, typeActeAgrementSousTraitance, autreTypeActe);
        lenient().when(typeActeRepository.findByActif(true)).thenReturn(allTypesActes);

        // setup evenements
        var typeEvenementAvenant = new TypeEvenement();
        typeEvenementAvenant.setCode("AVENANT_INCIDENCE");
        typeEvenementAvenant.setActif(true);
        var typeEventementAutre = new TypeEvenement();
        typeEventementAutre.setCode("AUTRE");
        typeEventementAutre.setActif(true);
        lenient().when(typeEvenementRepository.findByActif(true)).thenReturn(List.of(typeEvenementAvenant, typeEventementAutre));
    }


    @Test
    void getReferentielTypeActeWhenActesActifTrue() {
        when(parametrageService.isActif("pfUid", CLE_ACTES)).thenReturn(true);
        var result = referentielService.getReferentiel("pfUid", null, TYPE_ACTE);
        assertEquals(result.size(), 3, "Si le paramètre actes.actif est à true, le référentiel doit contenir tous les types d'actes");
    }

    @Test
    void getReferentielTypeActeWhenActesActifFalse() {
        when(parametrageService.isActif("pfUid", CLE_ACTES)).thenReturn(false);
        var result = referentielService.getReferentiel("pfUid", null,TYPE_ACTE);
        assertEquals(result.size(), 2, "Si le paramètre actes.actif est à false, le référentiel doit contenir tous les types d'actes sauf AUTRE");
        assertTrue(result.stream().allMatch(typeActe -> typeActe.getValue().equals(TypeActe.CODE_ACTE_MODIFICATIF) || typeActe.getValue().equals(TypeActe.CODE_AGREMENT_SOUS_TRAITANCE)), "Si le paramètre actes.actif est à false, le référentiel ne doit pas contenir le type d'acte AUTRE");
    }

    @Test
    void getReferentielTypeEvenement() {
        var result = referentielService.getReferentiel("pfUid",null, TYPE_EVENEMENT);
        assertEquals(result.size(), 1, "Le référentiel doit contenir tous les types d'événements sauf AVENANT");
        assertTrue(result.stream().allMatch(typeEvenement -> typeEvenement.getValue().equals("AUTRE")), "Le référentiel ne doit pas contenir le type d'événement AVENANT");
    }
}
