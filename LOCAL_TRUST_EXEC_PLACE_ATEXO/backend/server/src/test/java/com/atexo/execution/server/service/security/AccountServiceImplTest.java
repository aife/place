package com.atexo.execution.server.service.security;

import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.security.AccountRepository;
import com.atexo.execution.server.service.UtilisateurService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    AccountRepository accountRepository;

    @Mock
    UtilisateurService utilisateurService;

    @Mock
    PlateformeRepository plateformeRepository;

    @InjectMocks
    AccountServiceImpl accountService;

    @Test
    void given_inexistant_account_call_utilisateur_service() {
        var agentTypeSSO = new AgentSSOType();
        agentTypeSSO.setId(1);

        var plateforme = new Plateforme();
        plateforme.setMpeUid("MPE_24");

        when(accountRepository.findFirstByUsernameAndPlateformeMpeUidOrderByIdDesc(any(), any())).thenReturn(Optional.empty());
        when(plateformeRepository.findByMpeUid(any())).thenReturn(Optional.of(plateforme));

        var account = accountService.recuperer(plateforme.getMpeUid(), agentTypeSSO);

        assert(account.isPresent());
        assertEquals(account.get().getUuid(), "MPE_24_1");
        verify(utilisateurService, times(1)).sauvegarder(any(), any());
    }
}
