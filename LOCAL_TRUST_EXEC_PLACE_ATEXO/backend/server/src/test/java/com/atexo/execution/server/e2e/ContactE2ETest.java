package com.atexo.execution.server.e2e;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.EditionContactDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contacts")
@ActiveProfiles("oauth")
class ContactE2ETest extends BaseServerE2ETest {

    @Value("${e2e.contrat.endpoint:/api/contact}")
    private String endpoint;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.script:sql/contacts.sql}")
    private String script;

    @Autowired
    private OauthService oauthTokenService;

    @Autowired
    private SQLService sqlService;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement du script {}", script);

        super.setup();

        this.sqlService.load(script);
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on modifie un contact ALORS le retour est 401")
    @Order(1)
    void Given_NoToken_When_ModifierContact_Then_ReturnUnauthorized() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/modifierContact").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on modifie un contact ALORS le contact est modifié")
    @Order(2)
    void Given_ValidToken_When_ModifierContact_Then_ContactIsUpdated() throws URISyntaxException {
        EtablissementDTO etablissement = new EtablissementDTO();
        etablissement.setSiret("44090956200041");
        etablissement.setId(1L);
        EditionContactDTO updateContact = new EditionContactDTO();
        updateContact.setId(1L);
        updateContact.setNom("DUPONT");
        updateContact.setPrenom("Jean-Louis");
        updateContact.setEmail("mailUpdated@atexo.com");
        updateContact.setTelephone("0650985323");
        updateContact.setEtablissement(etablissement);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/modifierContact").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(updateContact, headers), ContactDTO.class);
        assertThat(response).isNotNull();
        var result  = response.getBody();
        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
        assertThat(result.getEmail()).isEqualTo("mailUpdated@atexo.com");
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on supprime un contact ALORS le retour est 401")
    @Order(3)
    void Given_NoToken_When_SupprimerContact_Then_ReturnUnauthorized() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/1/supprimerContact").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on supprime un contact ALORS le contact est désactivé")
    @Order(4)
    void Given_ValidToken_When_SupprimerContact_Then_ReturnContact() throws URISyntaxException {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/1/supprimerContact").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(headers), ContactDTO.class);
        assertThat(response).isNotNull();
        var result  = response.getBody();
        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
        assertThat(result.getActif()).isEqualTo(false);
    }
}
