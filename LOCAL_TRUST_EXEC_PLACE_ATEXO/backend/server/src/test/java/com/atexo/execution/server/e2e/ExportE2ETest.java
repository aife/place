package com.atexo.execution.server.e2e;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import com.atexo.execution.server.mvc.WorkerController;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats")
@ActiveProfiles("oauth")
 class ExportE2ETest extends BaseServerE2ETest{

    @Value("${e2e.contrat.script:sql/contrats.sql}")
    private String script;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.endpoint:/api/contrat}")
    private String endpoint;

    @Autowired
    private SQLService sqlService;

    @Autowired
    private OauthService oauthTokenService;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement du script {}", script);

        super.setup();

        this.sqlService.load(script);
    }

    @Test
    @Order(1)
    void Given_Token_When_ExportContrats_Then_Return_Ok() throws URISyntaxException, InterruptedException {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/export").build();
        log.debug("URI = {}", uri);

        var criteriaDTO = new ContratCriteriaDTO();
        criteriaDTO.setPerimetre(PerimetreContrat.TOUT);

        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(criteriaDTO, headers), String.class);

        assertThat(response.getBody()).isNotNull();
        var result = response.getBody();
        assertThat(result).isNotNull();

        var uriGetTask = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + "/api/worker/" + result).build();
        WorkerController.Ajax respohseGetAjax = WorkerController.Ajax.emptyResponse();
        while (respohseGetAjax.size()==1){
            respohseGetAjax = testRestTemplate.exchange(uriGetTask, HttpMethod.GET, new HttpEntity<>(headers), WorkerController.Ajax.class).getBody();
        }

        var uriDownload = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + "/api/worker/" + result+ "/download").build();
        var responseDownload = testRestTemplate.exchange(uriDownload, HttpMethod.GET, new HttpEntity<>(headers), byte[].class);

        assertThat(responseDownload.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

}
