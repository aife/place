package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.mpe.AgentMPE;
import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UtilisateurServiceImplTest {

    @Mock
    MpeClient mpeClient;

    @Mock
    UtilisateurRepository utilisateurRepository;

    @Mock
    PlateformeRepository plateformeRepository;

    @Mock
    ServiceRepository serviceRepository;

    @Mock
    UtilisateurMapper utilisateurMapper;

    @InjectMocks
    UtilisateurServiceImpl utilisateurService;

    private AgentMPE agent;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(utilisateurService, "moduleExecName", "moduleExec");
        var utilisateur = new Utilisateur();
        utilisateur.setIdExterne("MPE_24");
        lenient().when(utilisateurRepository.findByUuidAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(utilisateur));
        agent = new AgentMPE();
    }

    @Test
    void exec_actif_false_when_module_present_getUtilisateurByUuidAndPlateforme() {
        agent.setModulesOrganisme(List.of("moduleExec"));
        when(mpeClient.getUtilisateur(any(),any())).thenReturn(agent);
        when(utilisateurMapper.toDTO(any())).thenReturn(new UtilisateurDTO());
        var utilisateurDto = utilisateurService.getUtilisateurByUuidAndPlateforme("pfUid", "login", new HashSet<>());
        assertTrue(utilisateurDto.isModuleExecActif());
    }

    @Test
    void exec_actif_false_when_module_absent_getUtilisateurByUuidAndPlateforme() {
        agent.setModulesOrganisme(List.of("anotherModule"));
        when(mpeClient.getUtilisateur(any(),any())).thenReturn(agent);
        when(utilisateurMapper.toDTO(any())).thenReturn(new UtilisateurDTO());
        var utilisateurDto = utilisateurService.getUtilisateurByUuidAndPlateforme("pfUid", "login", new HashSet<>());
        assertFalse(utilisateurDto.isModuleExecActif());
    }

    @Test
    void test_sauvegarder() {
        agent.setUriOrganisme("uri/uriOrganisme");
        when(mpeClient.getUtilisateur(any(), any())).thenReturn(agent);
        var sso = new AgentSSOType();
        sso.setId(1);
        sso.setIdentifiant("identifiant");
        when(utilisateurMapper.toDTO(any())).thenReturn(new UtilisateurDTO());
        var utilisateurDto = utilisateurService.sauvegarder("pfUid", sso);
        assertNotNull(utilisateurDto);
    }

    @Test
    void test_sauvegarder_user_exists() {
        agent.setUriOrganisme("uri/uriOrganisme");
        when(mpeClient.getUtilisateur(any(), any())).thenReturn(agent);
        var sso = new AgentSSOType();
        sso.setId(1);
        sso.setIdentifiant("identifiant");
        when(utilisateurMapper.toDTO(any())).thenReturn(new UtilisateurDTO());
        when(utilisateurRepository.findByIdExterneAndPlateformeMpeUid(any(), any())).thenReturn(Optional.of(new Utilisateur()));
        var utilisateurDto = utilisateurService.sauvegarder("pfUid", sso);
        assertNotNull(utilisateurDto);
        verify(utilisateurRepository, times(0)).save(any());
    }

    @Test
    void test_sauvegarder_mpe_call_ko() {
        when(mpeClient.getUtilisateur(any(), any())).thenReturn(null);
        var utilisateurDto = utilisateurService.sauvegarder("pfUid", new AgentSSOType());
        assertNull(utilisateurDto);
    }
}
