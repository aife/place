package com.atexo.execution.server.e2e;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.crud.ContratRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats")
@ActiveProfiles("oauth")
class ContratE2ETest extends BaseServerE2ETest {

    @Value("${e2e.contrat.endpoint:/api/contrat}")
    private String endpoint;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.script:sql/contrats.sql}")
    private String script;

    @Autowired
    private OauthService oauthTokenService;

    @Autowired
    private SQLService sqlService;

    private static ContratDTO savedContrat = null;

    @Autowired
    private ContratRepository contratRepository;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement du script {}", script);

        super.setup();

        this.sqlService.load(script);
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on ajoute un contrat ALORS le retour est 401")
    @Order(1)
    void Given_NoToken_When_AjouterContrat_Then_ReturnUnauthorized() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/ajout").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton invalide QUAND on ajoute un contrat ALORS le retour est 401")
    @Order(2)
    void Given_InvalidToken_When_AjouterContrat_Then_ReturnUnauthorized() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth("bearer 123456");
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/ajout").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on ajoute un contrat ALORS le contrat est enregistré")
    @Order(3)
    void Given_ValidToken_When_AjouterContrat_Then_ContratIsSaved() throws URISyntaxException, IOException {
        var contrat = this.read("json/input/post_contrat.json", ContratDTO.class);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/ajout").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(contrat, headers), ContratDTO.class);
        assertThat(response).isNotNull();
        savedContrat = response.getBody();
        assertThat(savedContrat).isNotNull();
        assertThat(savedContrat.getId()).isNotNull();
        assertThat(savedContrat.getObjet()).isEqualTo("objetTEST");
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on récupère un contrat par son id ALORS le retour est 401")
    @Order(4)
    void Given_NoToken_When_GetById_Then_ReturnUnauthorized() throws URISyntaxException {
        var idContrat = 1L;

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + idContrat).build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), Object.class);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("ETANT DONNE jeton invalide QUAND on récupère un contrat par son id ALORS le retour est 401")
    @Order(5)
    void Given_InvalidToken_When_GetById_Then_ReturnUnauthorized() throws URISyntaxException {
        var idContrat = 1L;

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth("bearer 123456");
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + idContrat).build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), Object.class);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on récupère un contrat par son id ALORS le contrat est récupéré")
    @Order(6)
    void Given_ValidToken_When_GetById_Then_ContratIsSaved() throws URISyntaxException {
        var idContrat = 1L;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + idContrat).build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), ContratDTO.class);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getObjet()).isEqualTo("objetTEST");
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on récupère une page de contrats ALORS le retour est 401")
    @Order(7)
    void Given_NoToken_When_Find_Then_ReturnUnauthorized() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton invalide QUAND on récupère une page de contrat ALORS le retour est 401")
    @Order(8)
    void Given_InvalidToken_When_Find_Then_ReturnUnauthorized() throws URISyntaxException {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth("bearer 123456");
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint).build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on récupère une page de contrat ALORS la page de contrats est récupérée")
    @Order(9)
    void Given_ValidToken_When_Find_Then_ContratIsSaved() throws Exception {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint)
                .addParameter("page", "0")
                .addParameter("size", "10")
                .addParameter("sort", "dateCreation,DESC")
                .build();

        var criteriaDTO = new ContratCriteriaDTO();
        criteriaDTO.setPerimetre(PerimetreContrat.TOUT);

        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(criteriaDTO, headers), Object.class);
        assertThat(response.getBody()).isNotNull();
        var page = response.getBody();
        assertThat(page).isNotNull();
        assertMatch("json/expected/get_contrat_page.json", page);
    }

    @Test
    @DisplayName("ETANT DONNE jeton invalide QUAND on update un contrat ALORS le retour est 401")
    @Order(10)
    void Given_InvalidToken_When_UpdateContrat_Then_ReturnUnauthorized() throws URISyntaxException {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth("bearer 123456");
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/updateDonneesPrincipales").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on update un contrat ALORS le contrat est mis à jour")
    @Order(11)
    void Given_ValidToken_When_UpdateContrat_Then_ContratIsSaved() throws Exception {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/updateDonneesPrincipales")
                .build();


        log.debug("URI = {}", uri);
        savedContrat.setOffresRecues(1473);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(savedContrat, headers), ContratDTO.class);
        assertThat(response.getBody()).isNotNull();
        var result = response.getBody();
        assertThat(result).isNotNull();
        assertThat(result.getOffresRecues()).isEqualTo(1473);

    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on exporte les contrats ALORS les contrats sont exportés")
    @Order(12)
    void Given_ValidToken_When_Export_Then_OK() throws Exception {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/export")
                .build();
        ContratCriteriaDTO criteria = new ContratCriteriaDTO();
        criteria.setPerimetre(PerimetreContrat.TOUT);
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(criteria, headers), String.class);
        assertThat(response.getBody()).isNotNull();
        var result = response.getBody();
        assertThat(result).isNotNull();
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on notifie un contrat le statut du contrat chapeau soit égal à NOTIFIE")
    @Order(13)
    void Given_ValidToken_When_Statut_contrat_chapeau_notifie() throws Exception {
        Contrat contratChapeau = new Contrat();
        contratChapeau.setChapeau(true);
        contratChapeau.setNumero("2024T10");
        contratChapeau.setObjet("Achat fournitures");
        contratChapeau = contratRepository.save(contratChapeau);
        Contrat contrat1 = new Contrat();
        contrat1.setNumero("2024T10");
        contrat1.setObjet("Achat fournitures");
        contrat1.setStatut(StatutContrat.ANotifier);
        contrat1.setDateNotification(LocalDate.now().plusDays(1));
        contrat1.setContratChapeau(contratChapeau);
        contrat1 = contratRepository.save(contrat1);
        Contrat contrat2 = new Contrat();
        contrat2.setNumero("2024T10");
        contrat2.setObjet("Achat fournitures");
        contrat2.setStatut(StatutContrat.ANotifier);
        contrat2.setDateNotification(LocalDate.now().plusDays(1));
        contrat2.setContratChapeau(contratChapeau);
        contrat2 = contratRepository.save(contrat2);

        ContratDTO contratDTO = new ContratDTO();
        contratDTO.setId(contrat1.getId());
        contratDTO.setStatut(StatutContrat.ANotifier);
        contratDTO.setDateNotification(Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO.setIdContratChapeau(contratChapeau.getId());

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat1.getId())
                .build();

        log.debug("URI = {}", uri);
        testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(contratDTO, headers), String.class);

        var savedContratChapeau = contratRepository.findById(contrat1.getId()).map(Contrat::getContratChapeau).orElse(null);

        assertThat(savedContratChapeau).isNotNull();
        assertThat(savedContratChapeau.getStatut()).isEqualTo(StatutContrat.Notifie);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on notifie un contrat le statut du contrat chapeau soit égal à EN_COURS")
    @Order(14)
    void Given_ValidToken_When_Statut_contrat_chapeau_en_cours() throws Exception {
        Contrat contratChapeau = new Contrat();
        contratChapeau.setChapeau(true);
        contratChapeau.setNumero("2024T10");
        contratChapeau.setObjet("Achat fournitures");
        contratChapeau = contratRepository.save(contratChapeau);
        Contrat contrat1 = new Contrat();
        contrat1.setNumero("2024T10");
        contrat1.setObjet("Achat fournitures");
        contrat1.setStatut(StatutContrat.ANotifier);
        contrat1.setDateNotification(LocalDate.parse("2024-01-01"));
        contrat1.setDateFinContrat(LocalDate.now().plusDays(1));
        contrat1.setContratChapeau(contratChapeau);
        contrat1 = contratRepository.save(contrat1);

        ContratDTO contratDTO = new ContratDTO();
        contratDTO.setId(contrat1.getId());
        contratDTO.setStatut(StatutContrat.ANotifier);
        contratDTO.setDateNotification(Date.from(LocalDate.parse("2024-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO.setDateFinContrat(Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO.setIdContratChapeau(contratChapeau.getId());

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat1.getId())
                .build();

        log.debug("URI = {}", uri);
        testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(contratDTO, headers), String.class);

        var savedContratChapeau = contratRepository.findById(contrat1.getId()).map(Contrat::getContratChapeau).orElse(null);

        assertThat(savedContratChapeau).isNotNull();
        assertThat(savedContratChapeau.getStatut()).isEqualTo(StatutContrat.EnCours);
    }

    @Test
    @DisplayName("ETANT DONNE jeton valide QUAND on notifie un contrat le statut du contrat chapeau soit égal à CLOS")
    @Order(15)
    void Given_ValidToken_When_Statut_contrat_chapeau_clos() throws Exception {
        Contrat contratChapeau = new Contrat();
        contratChapeau.setChapeau(true);
        contratChapeau.setNumero("2024T10");
        contratChapeau.setObjet("Achat fournitures");
        contratChapeau = contratRepository.save(contratChapeau);
        Contrat contrat1 = new Contrat();
        contrat1.setNumero("2024T10");
        contrat1.setObjet("Achat fournitures");
        contrat1.setStatut(StatutContrat.ANotifier);
        contrat1.setDateNotification(LocalDate.parse("2024-01-01"));
        contrat1.setDateFinContrat(LocalDate.parse("2024-01-02"));
        contrat1.setContratChapeau(contratChapeau);
        contrat1 = contratRepository.save(contrat1);
        Contrat contrat2 = new Contrat();
        contrat2.setNumero("2024T10");
        contrat2.setObjet("Achat fournitures");
        contrat2.setStatut(StatutContrat.ANotifier);
        contrat2.setDateNotification(LocalDate.parse("2024-01-01"));
        contrat2.setDateFinContrat(LocalDate.parse("2024-01-02"));
        contrat2.setContratChapeau(contratChapeau);
        contrat2 = contratRepository.save(contrat2);

        ContratDTO contratDTO1 = new ContratDTO();
        contratDTO1.setId(contrat1.getId());
        contratDTO1.setStatut(StatutContrat.ANotifier);
        contratDTO1.setDateNotification(Date.from(LocalDate.parse("2024-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO1.setDateFinContrat(Date.from(LocalDate.parse("2024-01-02").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO1.setIdContratChapeau(contratChapeau.getId());
        ContratDTO contratDTO2 = new ContratDTO();
        contratDTO2.setId(contrat2.getId());
        contratDTO2.setStatut(StatutContrat.ANotifier);
        contratDTO2.setDateNotification(Date.from(LocalDate.parse("2024-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO2.setDateFinContrat(Date.from(LocalDate.parse("2024-01-02").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO2.setIdContratChapeau(contratChapeau.getId());

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri1 = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat1.getId())
                .build();
        var uri2 = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat2.getId())
                .build();

        log.debug("URI = {}", uri1);
        log.debug("URI = {}", uri2);
        testRestTemplate.exchange(uri1, HttpMethod.POST, new HttpEntity<>(contratDTO1, headers), String.class);
        testRestTemplate.exchange(uri2, HttpMethod.POST, new HttpEntity<>(contratDTO2, headers), String.class);

        var savedContratChapeau = contratRepository.findById(contrat1.getId()).map(Contrat::getContratChapeau).orElse(null);

        assertThat(savedContratChapeau).isNotNull();
        assertThat(savedContratChapeau.getStatut()).isEqualTo(StatutContrat.Clos);
    }

    @Test
    @DisplayName("ETANT DONNE un contrat chapeau qui a 2 fils, QUAND le statut d'au moins un fils = EN_COURS, le statut du contrat chapeau doit être égal à EN_COURS")
    @Order(15)
    void Given_ValidToken_When_Statut_en_cours_contrat_chapeau_encours() throws Exception {
        Contrat contratChapeau = new Contrat();
        contratChapeau.setChapeau(true);
        contratChapeau.setNumero("2024T10");
        contratChapeau.setObjet("Achat fournitures");
        contratChapeau = contratRepository.save(contratChapeau);
        Contrat contrat1 = new Contrat();
        contrat1.setNumero("2024T10");
        contrat1.setObjet("Achat fournitures");
        contrat1.setStatut(StatutContrat.ANotifier);
        contrat1.setDateNotification(LocalDate.parse("2024-01-01"));
        contrat1.setDateFinContrat(LocalDate.now().plusDays(1));
        contrat1.setContratChapeau(contratChapeau);
        contrat1 = contratRepository.save(contrat1);
        Contrat contrat2 = new Contrat();
        contrat2.setNumero("2024T10");
        contrat2.setObjet("Achat fournitures");
        contrat2.setStatut(StatutContrat.ANotifier);
        contrat1.setDateNotification(LocalDate.now().plusDays(1));
        contrat2.setContratChapeau(contratChapeau);
        contrat2 = contratRepository.save(contrat2);

        ContratDTO contratDTO1 = new ContratDTO();
        contratDTO1.setId(contrat1.getId());
        contratDTO1.setStatut(StatutContrat.ANotifier);
        contratDTO1.setDateNotification(Date.from(LocalDate.parse("2024-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO1.setDateFinContrat(Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO1.setIdContratChapeau(contratChapeau.getId());
        ContratDTO contratDTO2 = new ContratDTO();
        contratDTO2.setId(contrat2.getId());
        contratDTO2.setStatut(StatutContrat.ANotifier);
        contratDTO2.setDateNotification(Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        contratDTO2.setIdContratChapeau(contratChapeau.getId());

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri1 = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat1.getId())
                .build();
        var uri2 = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/notification-contrat/" + contrat2.getId())
                .build();

        log.debug("URI = {}", uri1);
        log.debug("URI = {}", uri2);
        testRestTemplate.exchange(uri1, HttpMethod.POST, new HttpEntity<>(contratDTO1, headers), String.class);
        testRestTemplate.exchange(uri2, HttpMethod.POST, new HttpEntity<>(contratDTO2, headers), String.class);

        var savedContratChapeau = contratRepository.findById(contrat1.getId()).map(Contrat::getContratChapeau).orElse(null);

        assertThat(savedContratChapeau).isNotNull();
        assertThat(savedContratChapeau.getStatut()).isEqualTo(StatutContrat.EnCours);
    }
}
