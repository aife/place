package com.atexo.execution.server.oauth.grants;

import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.security.AccountService;
import com.atexo.execution.server.service.security.AuthorityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
class MPEAccountAuthorizerTest {

    @Mock
    MpeClient mpeApiInterface;

    @Mock
    AccountService accountService;

    @Mock
    AuthorityService authorityService;

    @Mock
    PasswordEncoder userPasswordEncoder;

    @Mock
    PasswordBuilder passwordBuilder;

    @Mock
    PlateformeRepository plateformeRepository;

    @Mock
    UtilisateurService utilisateurService;

    @InjectMocks
    MPEAccountAuthorizer mpeAccountAuthorizer;

    @Test
    void authorize_existing_user() {
    }

    @Test
    void authorize_non_existing_user() {
    }
}
