package com.atexo.execution.server.e2e;

import com.atexo.execution.ExecApplication;
import com.atexo.execution.e2e.BaseE2ETest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.sql.SQLException;

@SpringBootTest(classes = ExecApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public abstract class BaseServerE2ETest extends BaseE2ETest {

	@Value("${context-path:/exec-api}")
	protected String contextPath;

	@BeforeAll
	protected void setup() throws SQLException, IOException {
	}
}
