package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.EditionContactDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.ContactReferentRepository;
import com.atexo.execution.server.repository.crud.ContactRepository;
import com.atexo.execution.server.repository.crud.ContratEtablissementRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactServiceImplTest {

    @Mock
    ContactRepository contactRepository;

    @Mock
    EtablissementRepository etablissementRepository;

    @Mock
    ContactReferentRepository contactReferentRepository;

    @Mock
    ContratEtablissementRepository contratEtablissementRepository;;

    @Mock
    ContactMapper contactMapper;

    @InjectMocks
    ContactServiceImpl contactService;

    Plateforme plateforme;

    Etablissement etablissementEntity;

    Contact contact;

    @BeforeEach
    public void setUp() {
        plateforme = new Plateforme();
        plateforme.setId(1L);
        etablissementEntity = new Etablissement();
        etablissementEntity.setId(1L);
        etablissementEntity.setPlateforme(plateforme);
        etablissementEntity.setSiret("44090956200041");
        contact = new Contact();
        contact.setEmail("email");
        contact.setPlateforme(plateforme);
        contact.setActif(true);
        contact.setEtablissement(etablissementEntity);

    }

    @Test
    void when_editionContactDTO_present_modifierContact() throws ApplicationBusinessException {
        List<Etablissement> etablissements = new ArrayList<>();
        etablissements.add(etablissementEntity);
        EtablissementDTO etablissement = new EtablissementDTO();
        etablissement.setSiret("44090956200041");
        etablissement.setId(1L);
        EditionContactDTO updateContact = new EditionContactDTO();
        updateContact.setId(1L);
        updateContact.setNom("DUPONT");
        updateContact.setPrenom("Jean-Louis");
        updateContact.setEmail("mailUpdated@atexo.com");
        updateContact.setTelephone("0650985323");
        updateContact.setEtablissement(etablissement);
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setEmail("mailUpdated@atexo.com");
        List<Contact> contactsToUpdateEtablissement = new ArrayList<>();
        contactsToUpdateEtablissement.add(contact);
        when(contactRepository.findOneById(any())).thenReturn(contact);
        when(etablissementRepository.findAllBySiretAndPlateformeId(any(), any())).thenReturn(etablissements);
        when(contactRepository.findAllByEtablissementIdAndEmailAndPlateformeId(any(), any(), any())).thenReturn(contactsToUpdateEtablissement);
        when(contactRepository.saveAndFlush(any())).thenReturn(contact);
        when(contactMapper.toDTO(any())).thenReturn(contactDTO);
        ContactDTO result = contactService.modifierContact(updateContact);
        assertNotNull(result);
        assertEquals(result.getEmail(), "mailUpdated@atexo.com");
    }

    @Test
    void when_id_contact_present_supprimerContact() {
        List<Etablissement> etablissements = new ArrayList<>();
        etablissements.add(etablissementEntity);
        EtablissementDTO etablissement = new EtablissementDTO();
        etablissement.setSiret("44090956200041");
        etablissement.setId(1L);
        List<Contact> contactsToDeleteEtablissement = new ArrayList<>();
        contactsToDeleteEtablissement.add(contact);
        ContactDTO contactDeleted = new ContactDTO();
        contactDeleted.setEmail("email");
        contactDeleted.setActif(false);
        contactDeleted.setEtablissement(etablissement);
        when(contactRepository.findOneById(any())).thenReturn(contact);
        when(etablissementRepository.findAllBySiretAndPlateformeId(any(), any())).thenReturn(etablissements);
        when(contactRepository.findAllByEtablissementIdAndEmailAndPlateformeId(any(), any(), any())).thenReturn(contactsToDeleteEtablissement);
        when(contactRepository.saveAndFlush(any())).thenReturn(contact);
        when(contactMapper.toDTO(any())).thenReturn(contactDeleted);
        ContactDTO result = contactService.supprimerContact(1L);
        assertNotNull(result);
        assertFalse(result.getActif());
    }

}
