package com.atexo.execution.server.e2e;

import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des échanges chorus")
@ActiveProfiles("oauth")
class EchangeChorusE2ETest extends BaseServerE2ETest {

    @Value("${e2e.contrat.endpoint:/api/echanges-chorus}")
    private String endpoint;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Value("${e2e.contrat.script:sql/chorus.sql}")
    private String script;

    @Autowired
    private OauthService oauthTokenService;

    @Autowired
    private SQLService sqlService;


    @BeforeAll
    protected void setup() throws SQLException, IOException {
        //insertion des données de test
        log.debug("Chargement du script {}", script);
        super.setup();
        this.sqlService.load(script);
    }

    @Test
    @DisplayName("ETANT DONNE pas de jeton QUAND on appelle soumettreEchangeChorus ALORS le retour est 401")
    @Order(1)
    void Given_NoToken_When_SoumettreEchangeChorus_Then_ReturnUnauthorized() throws URISyntaxException {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/soumettre/1").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    @DisplayName("ETANT DONNE un idActe et un echangeChorusDTO pour un contrat avec déjà un acte en cours de validation chorus QUAND on appelle soumettreEchangeChorus ALORS une exception est levée")
    @Order(1)
    void Given_Contrat_with_acte_en_cours_validation_chorus_When_SoumettreEchangeChorus_Then_ThrowException() throws URISyntaxException {
        EchangeChorusDTO echange = new EchangeChorusDTO();
        echange.setReference("refTest");
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + "/soumettre/1").build();
        log.debug("URI = {}", uri);
        ArrayList<LinkedHashMap<String, String>> response = (ArrayList<LinkedHashMap<String, String>>) testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(echange, headers), Object.class).getBody();
        assertThat(response).isNotNull();
        assertThat(response.get(0)).isNotNull();
        assertThat(response.get(0)).containsKeys("exception");
        assertThat(response.get(0)).containsValue("Echange chorus non autorisé = Un échange chorus est déjà en cours pour le contrat");
    }

}
