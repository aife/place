package com.atexo.execution.server.e2e;

import com.atexo.execution.e2e.auth.OauthService;
import com.atexo.execution.e2e.sql.SQLService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DisplayName("Tests E2E des contrats")
@ActiveProfiles("oauth")
public class SpaserE2ETest extends BaseServerE2ETest{

    @Value("${e2e.contrat.endpoint:/api/spaser}")
    private String endpoint;

    @Value("${e2e.contrat.script:sql/contrats.sql}")
    private String script;

    @Autowired
    private SQLService sqlService;

    @Value("${e2e.contrat.user:a666d0c5-fb4d-11ee-a31e-0242ac140003}")
    private String user;

    @Autowired
    private OauthService oauthTokenService;

    @BeforeAll
    protected void setup() throws SQLException, IOException {
        log.debug("Chargement du script {}", script);

        super.setup();

        this.sqlService.load(script);
    }

    @Test
    @DisplayName("ETANT DONNE un idContrat QUAND on appelle initContexte ALORS le lien spaser est correct")
    @Order(1)
    void Given_Ouath_WhenCallingSpaserAPI_ThenReturn_ResponseOk() throws URISyntaxException {

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(USER_UUID, user);
        var idContrat = 1L;
        headers.setBearerAuth(oauthTokenService.get(scheme, host, port));
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(contextPath + endpoint + '/' + idContrat+"/init-contexte").build();
        log.debug("URI = {}", uri);
        var response = testRestTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), String.class);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    }


}
