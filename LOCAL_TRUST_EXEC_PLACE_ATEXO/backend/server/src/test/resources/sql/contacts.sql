INSERT INTO organisme
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, acronyme, complement, data_set, nom, sigle, siren, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_service_racine)
VALUES
    (null,null,null,'facd5e65-cd80-439e-91e6-008abbe6c304',null,null,'pmi-min-1','PMI-MIN-1','03734',null,'Organisme de démonstration ATEXO',null,'216901231',null,null,1,null);
INSERT INTO service
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, actif, actif_lecture, echanges_chorus, niveau, nom, nom_court, nom_court_arborescence, old_id_externe, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_organisme, id_parent)
VALUES
    (null,null,null,'db4c700c-ed29-402a-aa8b-2a41eb492bcf',null,null,'pmi-min-1_0',null,null,false,0,'Organisme de démonstration ATEXO','DEMO',null,null,null,null,1,1,null);

INSERT INTO utilisateur (id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, actif, email, identifiant, nom, prenom, ref_organisme, ref_service, technique, telephone, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_service) VALUES (null, '2024-04-18T12:00:00', null, 'a666d0c5-fb4d-11ee-a31e-0242ac140003', null, null, '2024-04-18T14:00:00', true, null, 'test', null, null, null, 0, 1, null, null, null, 1, 1);
INSERT INTO fournisseur
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, capital_social, code_ape_naf_nace, description, email, fax, forme_juridique, nom, pays, prenom, raison_sociale, siren, site_internet, taille, telephone, trusted, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, categorie_fournisseur)
VALUES
    (null,null,null,'5ca45150-f55e-4f70-a7e2-56c987d1a57a',null,null,'8',null,null,null,null,null,null,'ATEXO',null,null,'ATEXO','440909562',null,null,'0123456789',false,null,null,1,null);
INSERT INTO etablissement
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, adresse_adresse, adresse_adresse_2, adresse_code_insee_localite, ADRESSE_CODE_POSTAL, adresse_commune, adresse_pays, libelle_naf, naf, siege, siret, trusted, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_fournisseur)
VALUES
    (null,null,null,'d4932e13-0d86-4083-9b25-cd0e0871de98',null,null,'31585','63 BD HAUSSMANN',null,null,'75008','PARIS 8','France',null,null,false,'44090956200041',null,null,null,1,1);
insert into contact
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, actif, adresse_postale, date_modification_rgpd, email, fonction, inscrit_annuaire_defense, login, nom, prenom, rgpd_communication, rgpd_communication_place, rgpd_enquete, telephone, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_etablissement)
values
    (null,null,null,'03ce9281-2b86-4c44-a0fc-0991753cb9b4',null,null,'348',true,null,null,'jean.dupont@atexo.com',null,null,null,'DUPONT','Jean',null,null,null,'0637839977',null,null,1,1);
