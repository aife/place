INSERT INTO organisme
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, acronyme, complement, data_set, nom, sigle, siren, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_service_racine)
VALUES
    (null,null,null,'facd5e65-cd80-439e-91e6-008abbe6c304',null,null,'pmi-min-1','PMI-MIN-1','03734',null,'Organisme de démonstration ATEXO',null,'216901231',null,null,1,null);
INSERT INTO service
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, actif, actif_lecture, echanges_chorus, niveau, nom, nom_court, nom_court_arborescence, old_id_externe, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_organisme, id_parent)
VALUES
    (null,null,null,'db4c700c-ed29-402a-aa8b-2a41eb492bcf',null,null,'pmi-min-1_0',null,null,false,0,'Organisme de démonstration ATEXO','DEMO',null,null,null,null,1,1,null);

INSERT INTO utilisateur (id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, actif, email, identifiant, nom, prenom, ref_organisme, ref_service, technique, telephone, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_service) VALUES (null, '2024-04-18T12:00:00', null, 'a666d0c5-fb4d-11ee-a31e-0242ac140003', null, null, '2024-04-18T14:00:00', true, null, 'test', null, null, null, 0, 1, null, null, null, 1, 1);
INSERT INTO fournisseur
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, capital_social, code_ape_naf_nace, description, email, fax, forme_juridique, nom, pays, prenom, raison_sociale, siren, site_internet, taille, telephone, trusted, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, categorie_fournisseur)
VALUES
    (null,null,null,'5ca45150-f55e-4f70-a7e2-56c987d1a57a',null,null,'8',null,null,null,null,null,null,'ATEXO',null,null,'ATEXO','440909562',null,null,'0123456789',false,null,null,null,null);
INSERT INTO etablissement
(id, date_creation, date_modification, uuid, version, date_modification_externe, id_externe, adresse_adresse, adresse_adresse_2, adresse_code_insee_localite, ADRESSE_CODE_POSTAL, adresse_commune, adresse_pays, libelle_naf, naf, siege, siret, trusted, id_utilisateur_creation, id_utilisateur_modification, id_plateforme, id_fournisseur)
VALUES
    (null,null,null,'d4932e13-0d86-4083-9b25-cd0e0871de98',null,null,'31585','63 BD HAUSSMANN',null,null,'75008','PARIS 8','France',null,null,false,'44090956200041',null,null,null,null,1);

INSERT INTO contrat (id,date_creation,date_modification,uuid) values (1,'2024-05-05 12:19:58','2024-05-05 12:19:58','d62293a4-234a-49d6-a111-ee8986b52464');

INSERT INTO acte (id, date_creation, date_modification, uuid, version, actif, type_fournisseur_entreprise1, visa_accf, visa_prefet, commentaire, date_notification, date_publication, numero, numero_ordre, objet, rejet_commentaire, rejet_motif, statut, statut_publication, type_notification, validation_chorus, id_utilisateur_creation, id_utilisateur_modification, id_acheteur, id_contrat, id_donnees_essentielles, id_titulaire_contact, id_type_acte) VALUES (1, '2024-04-18T12:00:00', '2024-04-18T14:00:00', '5b8ccc7a-4b0b-4836-8818-3fe291f3565c', null, null, null, null, null, null, null, null, '2017T00008/0001', null, 'test', null, null, 'BROUILLON', null, null, true, 1, 1, null, 1, null, null, null);

INSERT INTO acte (id, date_creation, date_modification, uuid, version, actif, type_fournisseur_entreprise1, visa_accf, visa_prefet, commentaire, date_notification, date_publication, numero, numero_ordre, objet, rejet_commentaire, rejet_motif, statut, statut_publication, type_notification, validation_chorus, id_utilisateur_creation, id_utilisateur_modification, id_acheteur, id_contrat, id_donnees_essentielles, id_titulaire_contact, id_type_acte) VALUES (2, '2024-04-20T12:00:00', '2024-04-20T14:00:00', '5b8ppp7a-4b0b-4836-8818-3fe291f3565c', null, null, null, null, null, null, null, null, '2017T00008/0002', null, 'test2', null, null, 'EN_ATTENTE_VALIDATION', null, null, true, 1, 1, null, 1, null, null, null);

INSERT INTO acte_modificatif (clause_reexamen, duree_ajouree_contrat, modalite_ht_chiffre, modalite_ttc_chiffre, nouvelle_fin_contrat, publication_de, taux_tva, type_modification, id) VALUES (false, 15, 0, 0, '2017-11-01', true, null, 'SANS_INCIDENCE', 1);

INSERT INTO acte_modificatif (clause_reexamen, duree_ajouree_contrat, modalite_ht_chiffre, modalite_ttc_chiffre, nouvelle_fin_contrat, publication_de, taux_tva, type_modification, id) VALUES (false, 20, 0, 0, '2017-11-01', true, null, 'SANS_INCIDENCE', 2);
