<div style="color: #757575; background-color: #fafafa; box-shadow: 1px 2px 2px #c0c0c0; border: 1px solid #dedede; border-radius: 3px; padding: 15px 15px 0 15px;">
    <h2 style=" font-size: 18px; color: #333333; font-weight: normal; margin-top: 5px;">Contrat concerné par cet
        échange</h2>
    <hr style="border-top: solid 1px #e5e5e5;"/>
    <ul style="list-style-type: none; padding-left: 0px;  font-size: 14px;">
        <li style="margin-bottom: 10px; line-height: 20px;">
            <span style="vertical-align: center"><img style="margin-right: 10px" src="${urlAssets}/tag.png"
                                                      alt="Numéro contrat | Objet"><b>Marché N° | Objet : </b>${reference} | ${objet}</span>
        </li>
        <li>
            <hr style="border-top: solid 1px #e5e5e5;"/>
        </li>
        <li style="margin-bottom: 10px; line-height: 20px;">
            <span style="vertical-align: center"><img style="margin-right: 10px" src="${urlAssets}/bank.png"
                                                      alt="Organisme | Entité d'achat"><b>Organisme | Entité d'achat : </b>${organisme} | ${entiteAchat}</span>
        </li>
    </ul>
</div>
