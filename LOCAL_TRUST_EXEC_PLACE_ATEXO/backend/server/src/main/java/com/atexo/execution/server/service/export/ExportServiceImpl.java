package com.atexo.execution.server.service.export;

import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.common.dto.exports.ExportActeDTO;
import com.atexo.execution.common.dto.exports.ExportContratDTO;
import com.atexo.execution.server.mapper.ActeMapperV2Impl;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.ExportActeMapper;
import com.atexo.execution.server.mapper.ExportContratMapper;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.paginable.ContratPaginableRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ExportServiceImpl implements ExportService {
    private static final Logger LOG = LoggerFactory.getLogger(ExportServiceImpl.class);

    private static final XSSFColor GREY = new XSSFColor(new java.awt.Color(237, 237, 237), null);
    public static final CellType DATE = CellType.NUMERIC;
    public static final CellType MONTANT = CellType.NUMERIC;
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy H:mm");
    private final ContratPaginableRepository contratPaginableRepository;
    private final UtilisateurRepository utilisateurRepository;
    private final ContratMapper contratMapperV2;

    @Autowired
    private ActeRepository acteRepository;

    private final ExportContratMapper exportContratMapper;
    private final ExportActeMapper exportActeMapper;

    String[] excelHeaders = {
            "Référence des contrats",
            "Intitulé du marché",
            "Objet de Consultation",
            "Objet du Marché",
            "Type de Contrat",
            "Statut du Contrat",
            "Accord Cadre ou SAD lié",
            "Numéro de Chapeau",
            "Procédure de Passation",
            "Référence de Consultation",
            "Nom de l'Entreprise Attributaire",
            "SIRET de l'Attributaire",
            "Montant du Contrat Attribué",
            "Montant du Contrat Facturé",
            "Mon Montant du Contrat Mandaté",
            "Forme du Prix",
            "Catégorie",
            "CPV Principal",
            "Lieu Principal d'Exécution",
            "Date d'Attribution",
            "Date Prévisionnelle de Notification",
            "Date Prévisionnelle de Fin de Contrat",
            "Date Prévisionnelle Maximale de Fin de Contrat",
            "Date de Notification",
            "Date de Fin de Contrat",
            "Date Maximale de Fin de Contrat",
            "Durée Maximale du Marché",
            "Entité Publique",
            "Service",
            "Nom de l'Agent Gestionnaire",
            "Prénom de l'Agent Gestionnaire",
            "Données Essentielles de la Publication du Contrat",
            "CCAG Applicable",
            "Achat Responsable",
            "Tranche Budgétaire",
            "Modalité de Révision de Prix",
            "Forme de Prix",
            "Types de Prix",
            "Contrat à Renouveler",
            "PME",
            "Considérations sociales",
            "Considérations environnementales",
            "Numéro long du contrat",
            "Numéro court",
            "Statut publication données essentielles"
    };

    String[] excelHeadersDeuxiemePage = {
            "Référence des contrats",
            "Numéro long du contrat",
            "Numéro court",
            "Objet",
            "Numéro de l'acte",
            "Type d'acte",
            "Acheteur", "Titulaire(s)",
            "Statut",
            "Date de notification",
            "Statut publication données essentielles"
    };
    @Autowired
    private ActeMapperV2Impl acteMapperV2Impl;


    @Override
    public File export(String utilisateurUuid, ContratCriteriaDTO criteriaDTO) throws IOException {
        var utilisateur = utilisateurRepository.findByUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var criteria = contratMapperV2.toCriteria(utilisateur, criteriaDTO);
        Pageable pageable = PageRequest.of(0, 10);
        int totalPage;
        Page<Contrat> allByCriteria;
        var excelFile = Files.createTempFile("contrats", ".xlsx");
        try (var workbook = new XSSFWorkbook(); var outputStream = new FileOutputStream(excelFile.toFile())) {
            XSSFSheet sheet = workbook.createSheet("Contrats");
            XSSFSheet sheetActs = workbook.createSheet("Actes");

            var celleStyle = workbook.createCellStyle();
            celleStyle.setFillForegroundColor(GREY);
            celleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            var font = workbook.createFont();
            font.setBold(true);
            font.setFontName("Calibri");
            font.setFontHeightInPoints((short) 11);
            celleStyle.setFont(font);
            // Créez l'en-tête de la feuille
            var headerRow = sheet.createRow(0);
            for (int i = 0; i < excelHeaders.length; i++) {
                var cell = headerRow.createCell(i);
                cell.setCellValue(excelHeaders[i]);
                cell.setCellStyle(celleStyle);
                sheet.autoSizeColumn(i);
            }
            // Créez l'en-tête de la deuxiéme feuille
            var headerRowPD = sheetActs.createRow(0);
            for (int i = 0; i < excelHeadersDeuxiemePage.length; i++) {
                var cell = headerRowPD.createCell(i);
                cell.setCellValue(excelHeadersDeuxiemePage[i]);
                cell.setCellStyle(celleStyle);
                sheetActs.autoSizeColumn(i);
            }
            // Ajoutez d'autres en-têtes pour les autres champs...

            var format = workbook.createDataFormat();
            var decimalCellStyle = workbook.createCellStyle();
            decimalCellStyle.setDataFormat(format.getFormat("#,##0.00"));

            var dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(format.getFormat("dd/mm/yy"));

            var dateTimeCellStyle = workbook.createCellStyle();
            dateTimeCellStyle.setDataFormat(format.getFormat("dd/mm/yy H:mm"));

            int nextRow = 1;
            do {
                allByCriteria = contratPaginableRepository.rechercher(criteria, pageable);
                totalPage = allByCriteria.getTotalPages();

                exportToExcel(sheet, decimalCellStyle, dateCellStyle, allByCriteria.getContent().stream().map(exportContratMapper::toDto).collect(Collectors.toList()), pageable.getPageNumber() * pageable.getPageSize() + 1);
                nextRow = exportToExcelPD(sheetActs, dateTimeCellStyle, allByCriteria.getContent(), nextRow);

                pageable = pageable.next();
            } while (pageable.getPageNumber() < totalPage);

            workbook.write(outputStream);


            return excelFile.toFile();

        } catch (Exception e) {
            LOG.error("Erreur lors de la création du fichier d'export", e);
            throw e;
        }

    }

    private int exportToExcelPD(XSSFSheet sheet, XSSFCellStyle dateCellStyle, List<Contrat> contrats, int rowNum) {
        XSSFCell cell;
        XSSFRow row;


        for (var contrat : contrats) {
            List<ExportActeDTO> acts = contrat.getActes().stream().map(exportActeMapper::toDto).collect(Collectors.toList());
            for (var act : acts) {
                row = sheet.createRow(rowNum++);
                int cellNum = 0;

                // Colonne A
                cell = row.createCell(cellNum++);
                cell.setCellValue(contrat.getReferenceLibre());

                // Colonne B
                cell = row.createCell(cellNum++);
                cell.setCellValue(contrat.getNumeroLong());

                // Colonne C
                cell = row.createCell(cellNum++);
                cell.setCellValue(contrat.getNumero());

                // Colonne D
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getObjet());

                // Colonne E
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getNumeroActe());

                // Colonne F
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getTypeActe());

                // Colonne G
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getAcheteur());

                // Colonne H
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getTitulaire());

                // Colonne I
                cell = row.createCell(cellNum++);
                cell.setCellValue(act.getStatut());

                // Colonne J
                cell = row.createCell(cellNum++, DATE);
                cell.setCellStyle(dateCellStyle);
                var dateNotification = act.getDateNotification();
                if (StringUtils.isNotBlank(dateNotification)) {
                    cell.setCellValue(LocalDateTime.parse(dateNotification, DATETIME_FORMATTER));
                } else {
                    cell.setCellValue("");
                }

                // Colonne K
                cell = row.createCell(cellNum);
                cell.setCellValue(act.getStatutPublication());
            }


        }

        return rowNum;

    }

    private void exportToExcel(XSSFSheet sheet, XSSFCellStyle decimalCellStyle, XSSFCellStyle dateCellStyle, List<ExportContratDTO> contrats, int rowNum) throws IOException {
        XSSFCell cell;
        XSSFRow row;

        for (var contrat : contrats) {
            row = sheet.createRow(rowNum++);
            int cellNum = 0;

            // Colonne A
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getReferenceLibre());

            // Colonne B
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getIntitule());

            // Colonne C
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getObjetConsultation());

            // Colonne D
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getObjetMarche());

            // Colonne E
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getTypeContrat());

            // Colonne F
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getStatutContrat());

            // Colonne G
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getAccordCadreOUSADLie());

            // Colonne H
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getNumeroChapeau());

            // Colonne I
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getProcedurePassation());

            // Colonne J
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getReferenceConsultation());

            // Colonne K
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getNomEntrepriseAttributaire());

            // Colonne L
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getSiretAttributaire());

            // Colonne M
            cell = row.createCell(cellNum++, MONTANT);
            cell.setCellStyle(decimalCellStyle);
            if (StringUtils.isNotBlank(contrat.getMontantContratAttribue()))
                cell.setCellValue(new BigDecimal(contrat.getMontantContratAttribue()).doubleValue());
            else cell.setCellValue("");

            // Colonne N
            cell = row.createCell(cellNum++, MONTANT);
            cell.setCellStyle(decimalCellStyle);
            if (StringUtils.isNotBlank(contrat.getMontantContratFacture()))
                cell.setCellValue(new BigDecimal(contrat.getMontantContratFacture()).doubleValue());
            else cell.setCellValue("");
            cell.setCellStyle(decimalCellStyle);

            // Colonne O
            cell = row.createCell(cellNum++, MONTANT);
            cell.setCellStyle(decimalCellStyle);
            if (StringUtils.isNotBlank(contrat.getMontantContratMandate()))
                cell.setCellValue(new BigDecimal(contrat.getMontantContratMandate()).doubleValue());
            else cell.setCellValue("");

            // Colonne P
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getFormeDuPrix());

            // Colonne Q
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getCategorie());

            // Colonne R
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getCpvPrincipal());

            // Colonne S
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getLieuPrincipalExecution());

            // Colonne T
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            if (StringUtils.isNotBlank(contrat.getDateAttribution()))
                cell.setCellValue(LocalDate.parse(contrat.getDateAttribution(), DATE_FORMATTER));

            // Colonne U
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var datePrevisionnelleNotification = contrat.getDatePrevisionnelleNotification();
            if (StringUtils.isNotBlank(datePrevisionnelleNotification))
                cell.setCellValue(LocalDate.parse(datePrevisionnelleNotification, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne V
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var datePrevisionnelleFinContrat = contrat.getDatePrevisionnelleFinContrat();
            if (StringUtils.isNotBlank(datePrevisionnelleFinContrat))
                cell.setCellValue(LocalDate.parse(datePrevisionnelleFinContrat, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne W
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var datePrevisionnelleMaximaleFinContrat = contrat.getDatePrevisionnelleMaximaleFinContrat();
            if (StringUtils.isNotBlank(datePrevisionnelleMaximaleFinContrat))
                cell.setCellValue(LocalDate.parse(datePrevisionnelleMaximaleFinContrat, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne X
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var dateNotification = contrat.getDateNotification();
            if (StringUtils.isNotBlank(dateNotification))
                cell.setCellValue(LocalDate.parse(dateNotification, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne Y
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var dateFinContrat = contrat.getDateFinContrat();
            if (StringUtils.isNotBlank(dateFinContrat))
                cell.setCellValue(LocalDate.parse(dateFinContrat, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne Z
            cell = row.createCell(cellNum++, DATE);
            cell.setCellStyle(dateCellStyle);
            var dateMaximaleFinContrat = contrat.getDateMaximaleFinContrat();
            if (StringUtils.isNotBlank(dateMaximaleFinContrat))
                cell.setCellValue(LocalDate.parse(dateMaximaleFinContrat, DATE_FORMATTER));
            else cell.setCellValue("");

            // Colonne AA
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getDureeMaximaleMarche());

            // Colonne AB
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getEntitePublique());

            // Colonne AC
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getService());

            // Colonne AD
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getNomAagentGestionnaire());

            // Colonne AE
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getPrenomAagenGestionnaire());

            // Colonne AF
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getDonneesEssentiellesPublicationContrat());

            // Colonne AG
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getCcagApplicable());

            // Colonne AH
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getAchatResponsable());

            // Colonne AI
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getTrancheBudgetaire());

            // Colonne AJ
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getModaliteRevisionPrix());

            // Colonne AK
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getFormeDuPrix());

            // Colonne AL
            cell = row.createCell(cellNum++);
            if (null != contrat.getTypesPrix()) {
                cell.setCellValue(String.join(", ", contrat.getTypesPrix()));
            } else {
                cell.setCellValue("");
            }

            // Colonne AM
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getContratARenouveler());

            // Colonne AN
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getPme());

            // Colonne AO
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getConsiderationsSociales());

            // Colonne AP
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getConsiderationsEnvironnementales());

            // Colonne AQ
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getNumeroLongContrat());

            // Colonne AR
            cell = row.createCell(cellNum++);
            cell.setCellValue(contrat.getNumeroCourt());

            // Colonne AS
            cell = row.createCell(cellNum);
            cell.setCellValue(contrat.getStatutPublicationDonnesEssentielles());
        }
    }
}
