package com.atexo.execution.server.service.actes;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.dto.ActeCriteriaDTO;
import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.dto.DocumentActeDTO;
import com.atexo.execution.common.dto.FichierDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.manager.FileStoreManager;
import com.atexo.execution.server.common.manager.upload.UploadManager;
import com.atexo.execution.server.common.mapper.ActeMapper;
import com.atexo.execution.server.common.mapper.DocumentActeMapper;
import com.atexo.execution.server.interf.mpe.api.ChorusClient;
import com.atexo.execution.server.model.AbstractSynchronizedEntity;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.DocumentActe;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.model.actes.AgrementSousTraitant;
import com.atexo.execution.server.model.actes.DonneesExecution;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.DocumentActeRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.paginable.ActePaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.ActeCriteria;
import com.atexo.execution.server.repository.referentiels.TypeActeRepository;
import com.atexo.execution.services.DonneesEssentiellesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
@Transactional
public class ActeServiceImpl implements ActeService {

    private final static Logger LOG = LoggerFactory.getLogger(ActeServiceImpl.class);

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private TypeActeRepository typeActeRepository;

    @Autowired
    private ActePaginableRepository actePaginableRepository;

    @Autowired
    private ActeRepository acteRepository;

    @Autowired
    private DocumentActeRepository documentActeRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private ActeMapper acteMapper;

    @Autowired
    private UploadManager uploadManager;

    @Autowired
    private FileStoreManager fileStoreManager;

    @Autowired
    private DocumentActeMapper documentActeMapper;

    @Autowired
    private DonneesEssentiellesService donneesEssentiellesService;

    @Autowired
    private ChorusClient chorusClient;

    @Override
    public void supprimer(final Long utilisateurId, final Long acteId) {
        this.acteRepository.deleteById(acteId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public <T extends ActeDTO> T sauvegarder(final Long id, final T acteDTO) {
        var code = typeActeRepository.findByCode(acteDTO.getType().getValue()).orElse(null);
        var acte = this.acteMapper.createEntity(acteDTO);
        // Les documents doivent etre mappés a part...
        acte.ajouter(this.enregistrer(acteDTO.getDocuments()));
        // ... le type d'acte aussi
        acte.setTypeActe(code);
        updateDeForActe(acte);
        processPlateforme(acte);
        acte = this.acteRepository.save(acte);
        return this.acteMapper.toDTO(acte);
    }

    @Override
    public Optional<? extends ActeDTO> recuperer(final Long utilisateur, final Long id) {
        return this.acteRepository.findById(id).map(this.acteMapper::toDTO);
    }

    @Override
    public Page<? extends ActeDTO> rechercher(final Long utilisateurId, final ActeCriteriaDTO criteria, final Pageable pageable) {

        final var utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(() -> new IllegalArgumentException("Utilisateur " + utilisateurId + " non trouve"));
        final var contrat = contratRepository.findById(criteria.getContrat().getId()).orElseThrow(() -> new IllegalArgumentException("Contrat " + criteria.getContrat() + " non trouve"));

        final var actes = actePaginableRepository.rechercher(
                new ActeCriteria.Builder()
                        .utilisateur(utilisateur)
                        .motsCles(criteria.getMotsCles())
                        .numero(criteria.getNumero())
                        .objet(criteria.getObjet())
                        .contrat(contrat)
                        .statuts(criteria.getStatuts())
                        .build(),
                pageable
        );

        final var result = actes.getContent().stream().<ActeDTO>map(acteMapper::toDTO).collect(Collectors.toList());

        return new PageImpl<>(result, pageable, actes.getTotalElements());
    }

    @Override
    public Set<? extends ActeDTO> rechercher(final String utilisateurUud, final Long contratId) {
        var contrat = contratRepository.findById(contratId).orElseThrow(IllegalArgumentException::new);
        return acteRepository.findByContratId(contrat.getId()).stream().<ActeDTO>map(acteMapper::toDTO).collect(Collectors.toSet());
    }

    @Override
    public <T extends ActeDTO> T notifier(final String uuid) throws ApplicationException {
        LOG.info("Notification de l'acte {}", uuid);
        final var acte = acteRepository.findByUuid(uuid).orElseThrow(() -> new ApplicationException("Acte introuvable " + uuid));
        if (acte.getDateNotification() == null) {
            acte.setDateNotification(LocalDateTime.now());
        } else {
            LOG.warn("l'acte {} a déjà été notifié", uuid);
        }
        acte.setStatut(StatutActe.NOTIFIE);
        updateDeForActe(acte);
        acteRepository.save(acte);
        return acteMapper.toDTO(acte);
    }

    private DocumentActe enregistrer(DocumentActeDTO documentDTO) {
        if (documentDTO.getId() != null) {
            return documentActeRepository.findById(documentDTO.getId()).orElseThrow(() -> new RuntimeException("Le document avec l'id " + documentDTO.getId() + " n'existe pas"));
        }
        return uploadManager.getFile(documentDTO.getReference()).map((uploadFile) -> {
            try {
                final var storeFile = fileStoreManager.storeFile(uploadFile.getFile(), false);

                var fichier = new FichierDTO();

                fichier.setNom(uploadFile.getName());
                fichier.setContentType(uploadFile.getMimeType());
                fichier.setTaille(uploadFile.getSize());
                fichier.setNomEnregistre(storeFile.getName());
                documentDTO.setFichier(fichier);

                var document = documentActeRepository.save(documentActeMapper.createEntity(documentDTO));

                uploadManager.removeFile(documentDTO.getReference());

                return document;
            } catch (ApplicationTechnicalException e) {
                throw new RuntimeException("Le fichier temporaire avec la référence " + documentDTO.getReference() + " ne peut pas etre sauvegardé");
            }

        }).orElseThrow(() -> new RuntimeException("Le fichier temporaire avec la référence " + documentDTO.getReference() + " n'existe pas")
        );
    }


    private List<DocumentActe> enregistrer(List<DocumentActeDTO> documents) {
        return documents.stream().map(this::enregistrer).collect(Collectors.toList());
    }

    private void updateDeForActe(Acte acte) {
        if (acte.getStatut() == StatutActe.NOTIFIE &&
                ((acte instanceof ActeModificatif && ((ActeModificatif) acte).getPublicationDonneesEssentielles()) ||
                        (acte instanceof AgrementSousTraitant && ((AgrementSousTraitant) acte).getPublicationDonneesEssentielles()) ||
                        (acte instanceof DonneesExecution && ((DonneesExecution) acte).getPublicationDonneesEssentielles()))) {
            var de = donneesEssentiellesService.updatePublicationDE(acte.getContrat(), StatutPublicationDE.A_PUBLIER);
            acte.setDonneesEssentielles(de);
        }
    }

    void processPlateforme(Acte acte) {
        var plateforme = Optional.ofNullable(acte)
                .map(Acte::getId)
                .map(e -> acteRepository.findById(e))
                .map(Optional::get)
                .map(Acte::getContrat)
                .map(AbstractSynchronizedEntity::getPlateforme)
                .orElse(null);
        if (plateforme != null) {
            acte.getContrat().setPlateforme(plateforme);
        }
    }


}
