package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.validation.Siren;
import com.atexo.execution.server.interf.basecentrale.BaseCentraleInterface;
import com.atexo.execution.server.service.EtablissementService;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.fournisseur.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/fournisseur")
@Validated

public class FournisseurController {

    @Autowired
    FournisseurService fournisseurService;

    @Autowired
    EtablissementService etablissementService;

    @Autowired
    BaseCentraleInterface baseCentraleInterface;

    @Autowired
    private UtilisateurService utilisateurService;

    @PostMapping(value = "/contratEtablissement/{fournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ContratEtablissementDTO>> findContratEtablissement(
            @RequestBody ContratEtablissementCriteriaDTO contratEtablissementCriteriaDTO,
            @RequestHeader("User-Uuid") final String utilisateurUuid, Pageable pageable) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return new ResponseEntity<>(fournisseurService.findByFournisseur(utilisateur.getOrganismeId(), contratEtablissementCriteriaDTO, pageable), HttpStatus.OK);
    }

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<FournisseurDTO>> find(@RequestBody FournisseurCriteriaDTO criteria, Pageable pageable) {

        return new ResponseEntity<>(fournisseurService.find(criteria, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/{fournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FournisseurDTO> get(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("fournisseur") Long fournisseurId) throws ApplicationBusinessException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return fournisseurService.getFournisseur(utilisateur.getOrganismeId(), fournisseurId)
                .map(fournisseurDTO -> new ResponseEntity<>(fournisseurDTO, HttpStatus.OK))
                .orElseThrow(ApplicationBusinessException::new);
    }

    @PostMapping(value = "/{siren}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FournisseurDTO> creerFournisseur(
            @RequestHeader("User-Uuid") final String utilisateurUuid,
            @PathVariable @Valid @Siren String siren,
            @RequestBody FournisseurDTO fournisseur) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return fournisseurService.creerFournisseur(utilisateur.getOrganismeId(), fournisseur).map(fournisseurDTO -> new ResponseEntity<>(fournisseurDTO, HttpStatus.OK)).orElse(null);
    }

    @PostMapping(value = "/attestations/zip/{sirenOrSiret}")
    public void zipAttestations(@PathVariable String sirenOrSiret, @RequestBody Map<String, String> attestations, HttpServletResponse response) throws IOException {
        File zip = baseCentraleInterface.zipAttestations(sirenOrSiret, attestations);
        String mimetype = Files.probeContentType(zip.toPath());
        if (mimetype == null) {
            mimetype = "application/octet-stream";
        }

        response.setContentType(mimetype);
        response.setHeader("Content-Disposition", MessageFormat.format("inline; filename=\"{0}-{1}.zip\"", "documents", sirenOrSiret));
        response.setContentLength((int) zip.length());
        final BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(zip));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

    @GetMapping(value = "/attestations-siren/{siren}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAttestationsSiren(@PathVariable String siren) {
        Optional<Map<String, Object>> attestations = fournisseurService.getAttestationsSiren(siren);
        if (attestations.isPresent()) {
            if (attestations.get().containsKey("error")) {
                return new ResponseEntity<>(attestations.get(), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(attestations.get(), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/attestations-siret/{siret}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAttestationsSiret(@PathVariable String siret) {
        Optional<Map<String, Object>> attestations = fournisseurService.getAttestationsSiret(siret);
        if (attestations.isPresent()) {
            if (attestations.get().containsKey("error")) {
                return new ResponseEntity<>(attestations.get(), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(attestations.get(), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping(value = "/updateFournisseur", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FournisseurDTO> updateDonneesPrincipales(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody FournisseurDTO fournisseurDTO) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        FournisseurDTO result = this.fournisseurService.updateFournisseur(utilisateur.getService().getOrganisme(), fournisseurDTO);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = "/addFournisseurEtranger", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FournisseurDTO> addFournisseurEtranger(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody FournisseurDTO fournisseurDTO) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        var pairFournisseurDTO = fournisseurService.addFournisseurEtranger(utilisateur.getOrganismeId(), fournisseurDTO);
        return new ResponseEntity<>(pairFournisseurDTO, HttpStatus.OK);
    }


    @GetMapping(value = "/uuid/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FournisseurDTO> getByUuid(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var criteria = FournisseurCriteriaDTO.builder().uuid(uuid).build();
        var result = fournisseurService.find(criteria, PageRequest.of(0, 1));
        var uniqueResult = result.getContent().stream().findFirst().orElse(null);
        if (uniqueResult != null) {
            return ResponseEntity.ok(uniqueResult);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping(value = "/{uuid}/etablissements", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EtablissementDTO>> getEtablissementsByUuid(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return ResponseEntity.ok(etablissementService.findEtablissementsByUUIDFournisseur(uuid));
    }

    @PostMapping(value = "/format-uuid/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValueLabelDTO> formatUrlFicheFournisseur(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("uuid") String uuid) throws ApplicationBusinessException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var url = fournisseurService.formatUrlFicheFournisseur(uuid);
        var dto = new ValueLabelDTO();
        dto.setLabel("lien fiche fournisseur");
        dto.setValue(url);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
