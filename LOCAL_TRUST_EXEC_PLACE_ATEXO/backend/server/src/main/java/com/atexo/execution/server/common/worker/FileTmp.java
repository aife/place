package com.atexo.execution.server.common.worker;

import java.io.File;


public class FileTmp extends File {


    private static final long serialVersionUID = 8710858082379936589L;


    private String nomFichier;

    private String url;

    @Override
    public int compareTo(File fichier) {
        // TODO Auto-generated method stub
        if (fichier instanceof FileTmp) {
            FileTmp fichierTmp = (FileTmp) fichier;
            if (nomFichier != null && fichierTmp.getNomFichier() != null) {
                return nomFichier.compareTo(fichierTmp.getNomFichier());
            }
        }
        return this.compareTo(fichier);
    }


    public FileTmp(String pathname) {
        super(pathname);
    }


    public final String getNomFichier() {
        return nomFichier;
    }


    public final void setNomFichier(final String valeur) {
        this.nomFichier = valeur.replaceAll(" ", "_");
    }


    public final String getUrl() {
        return url;
    }


    public final void setUrl(final String valeur) {
        this.url = valeur;
    }

}
