package com.atexo.execution.server.conf;

import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class JpaAuditingConfig {

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Bean
    public AuditorAware<Utilisateur> auditorAware() {
        return new AuditorAwareImpl();
    }
}
