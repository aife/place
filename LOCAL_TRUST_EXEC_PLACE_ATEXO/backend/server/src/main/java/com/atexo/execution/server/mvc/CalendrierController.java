package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.EvenementCriteriaDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.service.calendrier.CalendrierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/calendrier")
public class CalendrierController {

    @Autowired
    CalendrierService calendrierService;

    @PostMapping(value = "/{contratId}/evenement", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<EvenementDTO>> find(@PathVariable("contratId") Long contratId, @RequestBody EvenementCriteriaDTO criteriaDTO,
                                                   @PageableDefault Pageable pageable) {
        var evenements = calendrierService.rechercher(contratId, criteriaDTO, pageable);
        return new ResponseEntity<>(evenements, HttpStatus.OK);
    }

    @GetMapping(value = "/{contrat}")
    public ResponseEntity<List<EvenementDTO>> findByContractId(@PathVariable("contrat") Long contratId) throws ApplicationBusinessException {
        List<EvenementDTO> evenements = calendrierService.getCalendrierByContrat(contratId);
        return new ResponseEntity<>(evenements, HttpStatus.OK);
    }

    @GetMapping(value = "{contrat}/evenement/{evenement}")
    public ResponseEntity<EvenementDTO> findEvenement(@PathVariable("contrat") Long contratId, @PathVariable("evenement") Long evenementId) {
        EvenementDTO evenement = calendrierService.getEvenement(contratId, evenementId);
        return new ResponseEntity<>(evenement, HttpStatus.OK);
    }

    @GetMapping(value = "/evenement/{evenement}")
    public ResponseEntity<EvenementDTO> findEvenement(@PathVariable("evenement") Long evenementId) {
        EvenementDTO evenement = calendrierService.getEvenement(evenementId);
        return new ResponseEntity<>(evenement, HttpStatus.OK);
    }

    @PostMapping(value = "/{contrat}")
    public ResponseEntity<EvenementDTO> ajouterEvenement(@RequestBody EvenementDTO evenement, @PathVariable("contrat") Long contratId) {
        evenement = calendrierService.enregistrerEvenement(evenement, contratId);
        return new ResponseEntity<>(evenement, HttpStatus.OK);
    }

    @DeleteMapping(value = "{contrat}/evenement/{evenement}")
    public ResponseEntity<Void> supprimerEvenement(@PathVariable("contrat") Long contratId, @PathVariable("evenement") Long evenementId) {
        calendrierService.deleteEvenement(contratId, evenementId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
