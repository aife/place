package com.atexo.execution.server.service.messagerie;

import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.mapper.TemplateReponse;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;

import java.util.List;

public interface MessagerieService {

    ContexteDTO initTokenRedaction(Long utilisateurId, Long contratId) throws ApplicationException;

    ContexteDTO initTokenNotification(Long id, Long contratId, Long acteId) throws ApplicationException;

    ContexteDTO initTokenNotification(Long utilisateurId, Long contratId) throws ApplicationException;

    ContexteDTO initTokenSuivi(Long utilisateurId, Long contratId, Long acteId) throws ApplicationException;

    String[] uploadDocuments(Long organismeId, String token, Long[] documentIds) throws ApplicationException;

    DocumentContratDTO addPieceJointeToDocuments(Long utilisateurId, Long contratId, Long etablissementId, String object, String codeLien, String pieceJointeId) throws ApplicationException;

    List<ContratMessageStatusDTO> countMessageStatusByContrat(Long organismeId, List<String> contrats) throws ApplicationException;

    TemplateReponse getTemplates();

    Long getIdTemplateByCode(String notificationActe);

    ContexteDTO initTokenRedactionByCodeLien(Long utilisateurId, Long contratId, String codeLien);
}
