package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;


public class ZipException extends ApplicationBusinessException {

	public ZipException(String message) {
		super(message);
	}
}
