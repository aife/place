package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.mapper.TemplateReponse;
import com.atexo.execution.server.model.Template;
import com.atexo.execution.server.repository.TemplateRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.messagerie.MessagerieService;
import com.atexo.execution.server.service.spaser.SpaserService;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/spaser")
@Slf4j
public class SpaserController {

    @Autowired
    SpaserService spaserService;

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping(value = "/{idContrat}/init-contexte")
    public ResponseEntity<Object> initContexte(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable final Long idContrat) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contexte = spaserService.initContexte(utilisateur, idContrat);
        return new ResponseEntity<>(contexte, HttpStatus.OK);
    }

}
