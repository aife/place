package com.atexo.execution.server.oauth.grants;

import com.atexo.execution.common.dto.AccountDTO;
import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.oauth.grants.exceptions.AuthenticationException;
import com.atexo.execution.server.oauth.grants.exceptions.InvalideCodeException;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.security.AccountService;
import com.atexo.execution.server.service.security.AuthorityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Profile("!dev")
@Component
@RequiredArgsConstructor
@Slf4j
public class MPEAccountAuthorizer implements AccountCodeAuthorizer {

    private final MpeClient mpeApiInterface;

    private final AccountService accountService;

    private final AuthorityService authorityService;

    private final PasswordEncoder userPasswordEncoder;

    private final PasswordBuilder passwordBuilder;

    private final PlateformeRepository plateformeRepository;

    private final UtilisateurService utilisateurService;

	@Override
	@Transactional
    public AccountDTO authorize(final String pfUid, final String authorizationCode) {
		var password = passwordBuilder.build();

        log.info("Récupération de l'agent");

        AgentSSOType response = mpeApiInterface.getUtilisateurSSO(pfUid, authorizationCode);

		if ( response == null || response.getIdentifiant() == null ) {
            log.info("Code d'autorisation non valide");
			throw new InvalideCodeException(authorizationCode);
		} else {
            log.info("Code d'autorisation valide");
		}

		String login = response.getIdentifiant();
        log.info("Récupération de l'utilisateur depuis le login {}", login);

        var account = accountService.recuperer(pfUid, response).orElseThrow(() -> new AuthenticationException("Impossible de trouver le compte " + login));
        ;
        log.info("Utilisateur {} récupéré", login);
		account.setEnabled(true);
		account.setCredentialsExpired(false);
		account.setPassword(userPasswordEncoder.encode(password));

		if ( null != response.getHabilitations() ) {
			final var habilitationsMpe = response.getHabilitations().getHabilitation();

			var authorities = authorityService.trouver(habilitationsMpe);

			account.setAuthorities(authorities);

            log.info("Définition des habilitations pour {}", login);
		} else {

            log.info("Aucune habilitation pour {}", login);
		}

        log.info("Sauvegarde du compte {}", login);
		accountService.sauvegarder(account);

        log.info("Sauvegarde du compte {} OK", login);

		return new AccountDTO(account.getUuid(), password);
	}
}
