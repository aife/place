package com.atexo.execution.server.service.security;

import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.model.security.Account;
import com.atexo.execution.server.oauth.grants.exceptions.AuthenticationException;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.security.AccountRepository;
import com.atexo.execution.server.service.UtilisateurService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

	private final AccountRepository repository;
	private final UtilisateurService utilisateurService;
	private final PlateformeRepository plateformeRepository;

	@Override
	public Optional<Account> recuperer(String plateformeUid, AgentSSOType agentSSOType) {
		var identifiant = agentSSOType.getIdentifiant();
		var account = repository.findFirstByUsernameAndPlateformeMpeUidOrderByIdDesc(agentSSOType.getIdentifiant(), plateformeUid).orElse(null);
		if (account == null) {
			var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new AuthenticationException("Impossible de trouver la pfUid " + plateformeUid));
			log.warn("Utilisateur {} non trouvé pour la plateforme {}, on le crée", identifiant, plateformeUid);
			account = new Account();
			account.setUuid(BeanSynchroUtils.buildIdExterne(plateforme.getMpeUid(), agentSSOType.getId()));
			account.setUsername(identifiant);
			account.setPlateforme(plateforme);
			account.setIdExterne(identifiant);
			utilisateurService.sauvegarder(plateformeUid, agentSSOType);
			account.setDateCreation(LocalDateTime.now());
		}
		return Optional.of(account);
	}

	@Override
	public Account sauvegarder( Account user ) {
		return this.repository.save(user);
	}

	@Override
	public Account recuperer(String plateformeUid, String login) {
		return repository.findFirstByUsernameAndPlateformeMpeUidOrderByIdDesc(login, plateformeUid).orElseThrow(() -> new AuthenticationException("Impossible de trouver le compte " + login));
	}
}
