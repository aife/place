package com.atexo.execution.server.service.calendrier;

import com.atexo.execution.common.def.EtatEvenement;
import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.common.dto.EvenementCriteriaDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.server.common.mapper.DocumentContratMapper;
import com.atexo.execution.server.common.mapper.EvenementMapper;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.mapper.EvenementMapperV2;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.paginable.DocumentPaginableRepository;
import com.atexo.execution.server.repository.paginable.EvenementPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.DocumentCriteria;
import com.atexo.execution.server.repository.referentiels.TauxTVARepository;
import com.atexo.execution.server.repository.referentiels.TypeAvenantRepository;
import com.atexo.execution.server.repository.referentiels.TypeEvenementRepository;
import com.atexo.execution.server.service.AttributaireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CalendrierServiceImpl implements CalendrierService {

    private final static Logger LOG = LoggerFactory.getLogger(CalendrierServiceImpl.class);

    @Autowired
    AttributaireService attributaireService;

    @Autowired
    ContratRepository contratRepository;

    @Autowired
    EvenementRepository evenementRepository;

    @Autowired
    EvenementPaginableRepository evenementPaginableRepository;

    @Autowired
    TypeEvenementRepository typeEvenementRepository;

    @Autowired
    TypeAvenantRepository typeAvenantRepository;

    @Autowired
    TauxTVARepository tauxTVARepository;

    @Autowired
    DocumentContratRepository documentRepository;

    @Autowired
    DocumentPaginableRepository documentPaginableRepository;

    @Autowired
    EvenementMapper evenementMapper;

    @Autowired
    DocumentContratMapper documentMapper;

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    MpeClient mpeApiInterface;

    @Autowired
    ParametrageApplicationRepository parametrageRepository;

    @Autowired
    EvenementMapperV2 evenementMapperV2;

    @Override
    public List<EvenementDTO> getCalendrierByContrat(final Long contratId) {

        final List<Evenement> evenements = evenementRepository.findByContratId(contratId);

        return evenements.stream().map(
                        evenement -> evenementMapper.toDTO(evenement))
                .collect(Collectors.toList());
    }

    @Override
    public EvenementDTO enregistrerEvenement(final EvenementDTO evenementDTO, final Long idContrat) {
        Evenement evenement;
        final Contrat contrat = contratRepository.findById(idContrat).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        var pfUid = Optional.ofNullable(contrat.getPlateforme()).map(Plateforme::getMpeUid).orElseThrow(() ->
                new IllegalArgumentException("Impossible de trouver la plateforme pour le contrat " + contrat.getId()));
        if (evenementDTO.getId() == null) {
            //ajout d'un nouvel evenement
            evenement = evenementMapper.createEntity(evenementDTO);
            evenement.setContrat(contrat);
            evenement = evenementRepository.save(evenement);
            for (final Long documentId : evenementDTO.getDocumentIds()) {
                final DocumentContrat document = documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Doc ontrouvable"));
                document.getEvenements().add(evenement);
                documentRepository.save(document);
            }
            LOG.info("Evenement #{} créé pour le contrat #{}", evenement.getId(), contrat.getId());
        } else {
            if (evenementDTO.getEtatEvenement() == EtatEvenement.VALIDE && evenementDTO.getAvenantType() != null && isAvenantDateModifie(evenementDTO)) {
                contrat.setDateFinContrat(MapperUtils.convertToLocalDate(evenementDTO.getAvenantType().getDateFinNew()));
                contrat.setDateMaxFinContrat(MapperUtils.convertToLocalDate(evenementDTO.getAvenantType().getDateFinMaxNew()));
                contrat.setDateDemaragePrestation(MapperUtils.convertToLocalDate(evenementDTO.getAvenantType().getDateDemaragePrestationNew()));
                contratRepository.save(contrat);
            }
            evenement = fetchEvenement(idContrat, evenementDTO.getId());
            evenement = evenementMapper.updateEntity(evenementDTO, evenement);
            final Long[] evenementsId = {evenementDTO.getId()};
            final List<DocumentContrat> documentsDejaLie = documentPaginableRepository.rechercher(
                    new DocumentCriteria.Builder()
                            .contrat(idContrat)
                            .evenement(evenementsId)
                            .build());

            //retire les documents delié
            for (final DocumentContrat documentDejaLie : documentsDejaLie) {
                boolean sup = true;
                for (final Long documentId : evenementDTO.getDocumentIds()) {
                    if (Objects.equals(documentDejaLie.getId(), documentId)) {
                        sup = false;
                        break;
                    }
                }
                if (sup) {
                    documentDejaLie.getEvenements().remove(evenement);
                    documentRepository.save(documentDejaLie);
                }
            }

            //ajoute les nouveaux document lié
            if (evenementDTO.getDocumentIds() != null) {
                for (final Long documentId : evenementDTO.getDocumentIds()) {
                    boolean ajout = true;
                    for (final DocumentContrat documentDejaLie : documentsDejaLie) {
                        if (Objects.equals(documentDejaLie.getId(), documentId)) {
                            ajout = false;
                            break;
                        }
                    }
                    if (ajout) {
                        final DocumentContrat document = documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Doc introuvable"));
                        document.getEvenements().add(evenement);
                        documentRepository.save(document);
                    }
                }
            }
        }
        //Bindings complexes et règles de gestion
        //TypeEvenement
        final ValueLabelDTO typeEvenement = evenementDTO.getTypeEvenement();
        if (typeEvenement != null && (evenement.getTypeEvenement() == null || !typeEvenement.getValue().equals(evenement.getTypeEvenement().getCode()))) {
            evenement.setTypeEvenement(typeEvenementRepository.findOneByCode(typeEvenement.getValue()));
        }
        if (evenementDTO.getAvenantType() != null && evenementDTO.getAvenantType().getAutreType()) {
            final ValueLabelDTO typeAvenant = evenementDTO.getAvenantType().getTypeAvenant();
            if (typeAvenant != null && (evenement.getAvenantType().getTypeAvenant() == null || !typeAvenant.getValue().equals(evenement.getAvenantType().getTypeAvenant().getCode()))) {
                evenement.getAvenantType().setTypeAvenant(typeAvenantRepository.findOneByCode(typeAvenant.getValue()));
            }
        }
        if (evenementDTO.getBonCommandeTypeDTO() != null) {
            final ValueLabelDTO TauxTVA = evenementDTO.getBonCommandeTypeDTO().getTauxTVA();
            if (TauxTVA != null && (evenement.getBonCommandeType().getTauxTVA() == null || !TauxTVA.getValue().equals(evenement.getBonCommandeType().getTauxTVA().getCode()))) {
                evenement.getBonCommandeType().setTauxTVA(tauxTVARepository.findOneByCode(TauxTVA.getValue()));
            }
        }


        if (evenementDTO.getEnvoiAlerte() == null) {
            evenement.setEnvoiAlerte(false);
        } else {
            evenement.setEnvoiAlerte(evenementDTO.getEnvoiAlerte());
        }
        // changement d'attributaire
        if (evenement.getEtatEvenement() == EtatEvenement.VALIDE && evenementDTO.getAvenantType() != null && Boolean.TRUE.equals(evenementDTO.getAvenantType().getAvenantTransfert()) && evenementDTO.getNouveauContractant() != null && evenementDTO.getContractant() != null) {
            LOG.info("Changement de titualire de {} à {}", evenementDTO.getContractant().getSiret(), evenementDTO.getNouveauContractant().getSiret());
            ContratEtablissement contratEtablissement = contrat.getContratEtablissements().stream()
                    .filter(ce -> ce.getEtablissement().getId() == evenementDTO.getContractant().getId())
                    .findFirst()
                    .orElse(null);
            if (contratEtablissement != null) {
                contratEtablissement.setEtablissement(etablissementRepository.findOneById(evenementDTO.getNouveauContractant().getId()));
                contratEtablissement.getContrat().setSynchronisable(false);
                contratEtablissementRepository.save(contratEtablissement);
                evenement.setContractant(contratEtablissement.getEtablissement());
                evenement.setNouveauContractant(null);
            }
            // changement du commanditaire
            for (ContratEtablissement commanditaire : contrat.getContratEtablissements().stream()
                    .filter(ce -> ce.getCommanditaire() != null)
                    .filter(ce -> ce.getCommanditaire().getId() == evenementDTO.getContractant().getId())
                    .collect(Collectors.toList())) {
                commanditaire.setCommanditaire(etablissementRepository.findOneById(evenementDTO.getNouveauContractant().getId()));
                contratEtablissementRepository.save(commanditaire);
            }

            // changement du contractant au niveau de l'évènement
            LOG.info("Changement de contractant de l'avenant de {} à {}", evenementDTO.getContractant().getSiret(), evenementDTO.getNouveauContractant().getSiret());
            evenement.setContractant(etablissementRepository.findOneById(evenementDTO.getNouveauContractant().getId()));

        }
        //Contractant
        //Destinataires
        //envoi à MPE
        boolean synchronisationAvenants = parametrageRepository.findByClefAndPlateformeMpeUid(ParametrageApplication.CLE_SYNCHRONISATION_AVENANTS, pfUid)
                .map(ParametrageApplication::getValeur)
                .map(Boolean::parseBoolean)
                .orElse(false);
        if (synchronisationAvenants && evenement.getEtatEvenement() == EtatEvenement.VALIDE && evenement.getAvenantType() != null && evenement.getAvenantType().getDateAcquittement() == null) {
            try {
                var plateformeUid = Optional.ofNullable(contrat.getPlateforme()).map(Plateforme::getMpeUid).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme pour le contrat " + contrat.getId()));
                mpeApiInterface.publierModification(plateformeUid, evenement);
                evenement.getAvenantType().setStatut(StatutAvenant.PUBLIE);
                evenement.getAvenantType().setDateAcquittement(LocalDate.now());
            } catch (MPEApiException ex) {
                evenement.getAvenantType().setStatut(StatutAvenant.A_PUBLIER);
            } finally {
                evenementRepository.save(evenement);
            }
        }
        return evenementMapper.toDTO(evenement);
    }

    @Override
    public EvenementDTO getEvenement(final Long contratId, final Long evenementId) {
        final Evenement evenement = fetchEvenement(contratId, evenementId);
        Double sommePrecedentsAvenants = evenementRepository.calculSommePrecedentsAvenants(contratId, evenement.getDateDebut());
        Double sommeCurrentsAvenants = evenementRepository.calculSommeCurrentsAvenants(contratId, evenement.getDateDebut());
        var evenementDTO = evenementMapper.toDTO(evenement);
        if (evenementDTO != null && evenementDTO.getAvenantType() != null) {
            var avenantType = evenementDTO.getAvenantType();
            avenantType.setCumulPrecedentsAvenants(BigDecimal.valueOf(sommePrecedentsAvenants != null ? sommePrecedentsAvenants : 0));
            avenantType.setCumulAvenantsCourants(BigDecimal.valueOf(sommeCurrentsAvenants != null ? sommeCurrentsAvenants : 0));
        }
        return evenementDTO;
    }

    @Override
    public List<EvenementDTO> getEvenements(final Boolean alerte, final Boolean envoiAlerte) {
        final List<EvenementDTO> evenementDTOs = new ArrayList<>();
        final List<Evenement> evenements = evenementRepository.findByAlerteAndEnvoiAlerte(alerte, envoiAlerte);
        for (final Evenement evenement : evenements) {
            final Date dateDuJour = new Date();
            final Date datePreavis = Date.from(evenement.getDatePreavis().atStartOfDay(ZoneId.systemDefault()).toInstant());
            if (!dateDuJour.before(datePreavis)) {
                evenementDTOs.add(evenementMapper.toDTO(evenement));
            }
        }
        return evenementDTOs;
    }

    @Override
    public EvenementDTO getEvenement(final Long evenementId) {
        final Evenement evenement = evenementRepository.findById(evenementId).orElse(null);
        return evenementMapper.toDTO(evenement);
    }


    @Override
    public void deleteEvenement(final Long contratId, final Long evenementId) {
        final Evenement evenement = fetchEvenement(contratId, evenementId);

        for (final DocumentContrat documentBO : evenement.getDocuments()) {
            documentBO.getEvenements().remove(evenement);
            documentRepository.save(documentBO);
        }
        evenementRepository.delete(evenement);
    }

    private Evenement fetchEvenement(final Long contratId, final Long evenementId) {
        final Evenement evenement = evenementRepository.findById(evenementId).orElseThrow(() -> new IllegalArgumentException("Evenement introuvable"));
        if (!contratId.equals(evenement.getContrat().getId())) {
            throw new IllegalArgumentException(String.format("L'id de l'événement (%d) et celui du contrat (%d) ne correspondent pas.", evenementId, contratId));
        }
        return evenement;
    }

    @Override
    public void modifEnvoiEvenement(final Long evenementId) {
        final Evenement evenement = evenementRepository.findById(evenementId).orElseThrow(() -> new IllegalArgumentException("Evenement introuvable"));
        evenement.setEnvoiAlerte(true);
        evenementRepository.save(evenement);
    }

    public Page<EvenementDTO> rechercher(final Long contratId, EvenementCriteriaDTO criteriaDTO, final Pageable pageable) {
        var criteria = evenementMapperV2.toCriteria(criteriaDTO);
        var page = evenementPaginableRepository.rechercher(criteria, pageable);
        var evenements = page.getContent().stream().map(evenementMapper::toDTO).collect(Collectors.toList());
        return new PageImpl<>(evenements, pageable, page.getTotalElements());
    }

    @Override
    public Map<Long, Long> compterEnAttente(final List<Long> idsContrat) {
        if (idsContrat == null || idsContrat.isEmpty()) {
            return new HashMap<>();
        }
        return evenementPaginableRepository.compterEnAttente(idsContrat);
    }

    private boolean isAvenantDateModifie(final EvenementDTO evenementDTO) {
        final boolean dateFinModifie = evenementDTO.getAvenantType().getDateFin() == null || !evenementDTO.getAvenantType().getDateFin().equals(evenementDTO.getAvenantType().getDateFinNew());
        final boolean dateFinMaxModifie = evenementDTO.getAvenantType().getDateFinMax() == null || !evenementDTO.getAvenantType().getDateFinMax().equals(evenementDTO.getAvenantType().getDateFinMaxNew());


        return evenementDTO.getAvenantType().getDateModifiable() && (dateFinModifie || dateFinMaxModifie
                || evenementDTO.getAvenantType().getDateDemaragePrestation() != null && (!evenementDTO.getAvenantType().getDateDemaragePrestation().equals(evenementDTO.getAvenantType().getDateDemaragePrestationNew())));
    }


}
