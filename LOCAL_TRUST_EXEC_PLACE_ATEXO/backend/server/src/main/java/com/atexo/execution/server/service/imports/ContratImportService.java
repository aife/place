package com.atexo.execution.server.service.imports;

/**
 * Service d'import de contrats via fichier XLS.
 */
public interface ContratImportService extends ImportService {
}
