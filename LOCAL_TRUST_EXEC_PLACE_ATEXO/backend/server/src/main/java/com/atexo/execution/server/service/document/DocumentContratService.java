package com.atexo.execution.server.service.document;

import com.atexo.execution.common.def.DocumentContratType;
import com.atexo.execution.common.dto.CreationDocumentModelDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.dto.DocumentCriteriaDTO;
import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.docgen.DocumentModeleDTO;
import com.atexo.execution.common.dto.edition_en_ligne.FileStatus;
import com.atexo.execution.common.dto.edition_en_ligne.TrackDocumentResponse;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface DocumentContratService {

	Page<DocumentContratDTO> find(DocumentCriteriaDTO criteriaDTO, Pageable pageable);

	void deleteDocument(Long documentId);

	DocumentContratDTO getDocument(Long documentId);

	File getDocumentFile(Long documentId);

	void deleteFile(File file);

    DocumentContratDTO saveDocument(final Long utilisateurId, Long contratId, CreationDocumentModelDTO modelDTO) throws ApplicationTechnicalException;

    DocumentContratDTO generateDocument(final Long utilisateurId, Long contratId, CreationDocumentModelDTO modelDTO) throws ApplicationTechnicalException, IOException;

	DocumentContratDTO saveCommentaire(Long documentId, String commentaire) throws ApplicationTechnicalException;

	String getUrlEditionEnLigne(Long documentId, String utilisateurUuid, String token) throws ApplicationTechnicalException;

	DocumentContratDTO saveStreamToDocument( final Long utilisateurId, Long contratId, String objet, Long etablissementId, DocumentContratType documentType,
	                                         InputStream inputStream, String nom, String contentType, long contentLength, String idExterne)
			throws ApplicationTechnicalException;

	List<DocumentContratDTO> getDocumentByEvenement( Long evenementId);

	File getGuideUtilisateur() throws ApplicationTechnicalException;

	TrackDocumentResponse getDocumentCallback(Long documentId, FileStatus status, String utilisateurUuid);

    void updateDocumentsStatus();

    List<DocumentModeleDTO> getModeleDocumentList(Long organismeId);

	File convertFile(byte[] fileContent, String extension);

	File export(UtilisateurDTO utilisateur, Long contratId) throws IOException, ApplicationTechnicalException;
}
