package com.atexo.execution.server.common.manager;

import com.atexo.execution.common.exception.ApplicationTechnicalException;

import java.io.File;
import java.util.Optional;

public interface FileStoreManager {

	File createFileForStorage(String extension);

	File storeFile(File file, boolean deleteOriginal) throws ApplicationTechnicalException;

	Optional<File> getFile(String name);

	void deleteFile(String name);

}
