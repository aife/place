package com.atexo.execution.server.common.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;


public abstract class Task<T> implements Callable<T> {

    private static Logger logger = LoggerFactory.getLogger(Task.class);

    private String idTask;

    private String resultHref;

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getIdTask() {
        return idTask;
    }

    public String getResultHref() {
        return resultHref;
    }

    public void setResultHref(String resultHref) {
        this.resultHref = resultHref;
    }

    public abstract T work() throws Exception ;

    @Override
    public final T call() throws Exception {
        try {
            T result = work();
            logger.info("Task {} dont id = {} a fini son work avec succes", Thread.currentThread().getName(), idTask);
            return result;
        } catch (Exception ex) {
            logger.error("Task {} dont id = {} a echoue avec erreur {}", Thread.currentThread().getName(), idTask, ex.getMessage());
            throw ex;
        }
    }

}
