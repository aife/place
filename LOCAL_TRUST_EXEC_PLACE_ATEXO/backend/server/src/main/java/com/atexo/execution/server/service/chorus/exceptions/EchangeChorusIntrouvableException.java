package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;
import lombok.Getter;

@Getter
public class EchangeChorusIntrouvableException extends ApplicationBusinessException {

	private final Long id;

	public EchangeChorusIntrouvableException( Long id ) {
		super(String.format("L'echange %s est introuvable", id));

		this.id = id;
	}

}
