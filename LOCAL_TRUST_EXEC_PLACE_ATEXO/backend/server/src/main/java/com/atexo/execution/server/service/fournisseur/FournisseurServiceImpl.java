package com.atexo.execution.server.service.fournisseur;

import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.FournisseurMapper;
import com.atexo.execution.server.interf.basecentrale.BaseCentraleInterface;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.paginable.ContratEtablissementPaginableRepository;
import com.atexo.execution.server.repository.paginable.FournisseurPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.FournisseurCriteria;
import com.atexo.execution.server.repository.referentiels.CategorieFournisseurRepository;
import com.atexo.execution.server.repository.referentiels.CodeApeNafRepository;
import com.atexo.execution.server.repository.referentiels.FormeJuridiqueRepository;
import com.atexo.execution.server.service.fournisseur.exceptions.FournisseurExistantException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service("fournisseurService")
@Transactional
public class FournisseurServiceImpl implements FournisseurService {

    private final static Logger LOG = LoggerFactory.getLogger(FournisseurServiceImpl.class);

    private static final String sgmapErrorCode = "error";
    @Value("${mpe.ficheFournisseur.path}")
    private String ficheFournisseurPath;

    @Autowired
    FournisseurRepository fournisseurRepository;

    @Autowired
    FournisseurPaginableRepository fournisseurRechercheRepository;

    @Autowired
    ContratRepository contratRepository;

    @Autowired
    ContratEtablissementPaginableRepository contratEtablissementParFournisseurRepository;

    @Autowired
    ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    CodeApeNafRepository codeApeNafRepository;

    @Autowired
    FormeJuridiqueRepository formeJuridiqueRepository;

    @Autowired
    FournisseurServiceDelegate fournisseurServiceDelegate;

    @Autowired
    BaseCentraleInterface baseCentraleInterface;

    @Autowired
    FournisseurMapper fournisseurMapper;

    @Autowired
    EtablissementMapper etablissementMapper;

    @Autowired
    OrganismeRepository organismeRepository;

    @Autowired
    ContratEtablissementMapperV2 contratEtablissementMapperV2;

    @Autowired
    CategorieFournisseurRepository categorieFournisseurRepository;

    @Override
    public Page<FournisseurDTO> find(FournisseurCriteriaDTO criteria, final Pageable pageable) {

        final Page<Fournisseur> fournisseurs = fournisseurRechercheRepository.rechercher(
                FournisseurCriteria.builder()
                        .motsCles(criteria.getMotsCles())
                        .siren(criteria.getSiren())
                        .pays(criteria.getPays())
                        .uuid(criteria.getUuid())
                        .build(),
                pageable
        );

        final List<FournisseurDTO> result = fournisseurs.getContent().stream().map(fournisseur -> {
            final List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByEtablissementFournisseurId(fournisseur.getId());
            return fournisseurMapper.toDTO(fournisseur, contratEtablissements);
        }).collect(Collectors.toList());
        return new PageImpl<>(result, pageable, fournisseurs.getTotalElements());
    }

    @Override
    public Optional<FournisseurDTO> getFournisseur(final Long organismeId, final Long fournisseurId) {
        final Optional<Fournisseur> fournisseurOpt = fournisseurServiceDelegate.getFournisseur(fournisseurId);
        if (fournisseurOpt.isEmpty()) {
            return Optional.empty();
        }
        final Optional<Organisme> organismeOpt = organismeRepository.findById(organismeId);
        if (organismeOpt.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(toDTO(organismeOpt.get(), fournisseurOpt.get()));
    }

    public FournisseurDTO toDTO(final Organisme organisme, final Fournisseur fournisseur) {

        final Long organismeId = organisme.getId();
        final List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByEtablissementFournisseurIdAndContratServiceOrganismeId(fournisseur.getId(), organismeId);
        final FournisseurDTO fournisseurDTO;
        fournisseurDTO = fournisseurMapper.toDTO(fournisseur, contratEtablissements);

        return fournisseurDTO;
    }

    public Optional<FournisseurDTO> creerFournisseur(final Long organismeId, final FournisseurDTO fournisseur) throws FournisseurExistantException {
        var siren = fournisseur.getSiren();
        LOG.info("Création de l'entreprise avec le SIREN {} ", siren);

        Organisme organisme = organismeRepository.findById(organismeId).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver l'organisme " + organismeId));

        try {
            var fournisseurs = fournisseurRepository.findBySiren(siren);
            if (!fournisseurs.isEmpty()) {
                throw new FournisseurExistantException(siren);
            }
            Optional<Fournisseur> fournisseurOpt = baseCentraleInterface.getEntrepriseBySiren(siren);
            if (fournisseurOpt.isPresent()) {
                final Fournisseur f = fournisseurOpt.get();

                final Etablissement etablissement = f.getEtablissements().get(0);
                if (etablissement.getSiege() && etablissement.getNaf() != null) {
                    String str = etablissement.getNaf();
                    str = new StringBuilder(str).insert(2, ".").toString();
                    f.setCodeApeNafNace(str + " - " + etablissement.getLibelleNaf());
                }
                f.setPays("FR");
                f.setTrusted(true);
                fournisseurOpt = Optional.of(f);

                final Fournisseur fournisseurBo = fournisseurRepository.save(fournisseurOpt.get());
                etablissementRepository.save(fournisseurOpt.get().getEtablissements().get(0));
                final FournisseurDTO fournisseurDTO = toDTO(organisme, fournisseurBo);
                return Optional.of(fournisseurDTO);
            }

        } catch (final ApplicationTechnicalException applicationException) {
            //Erreur de serveur
            final FournisseurDTO f = new FournisseurDTO();
            f.setSiren("1");
            return Optional.of(f);
        }

        return Optional.empty();
    }

    @Override
    public FournisseurDTO updateFournisseur(final OrganismeDTO organismeDTO, final FournisseurDTO fournisseurDTO) throws ApplicationException {
        final Optional<Fournisseur> fournisseurOpt = fournisseurServiceDelegate.getFournisseur(fournisseurDTO.getId());

        if (fournisseurOpt.isEmpty()) {
            throw new ApplicationTechnicalException();
        }

        Fournisseur fournisseur = fournisseurOpt.get();

        final Optional<Organisme> organismeOpt = organismeRepository.findById(organismeDTO.getId());

        if (organismeOpt.isEmpty()) {
            throw new ApplicationTechnicalException();
        }

        Organisme organisme = organismeOpt.get();

        fournisseur.setTrusted(fournisseurDTO.getTrusted());
        if (fournisseurDTO.getRaisonSociale() != null) {
            fournisseur.setRaisonSociale(fournisseurDTO.getRaisonSociale());
        }
        if (fournisseurDTO.getCapitalSocial() != null) {
            fournisseur.setCapitalSocial(fournisseurDTO.getCapitalSocial());
        }
        fournisseur.setCodeApeNafNace(fournisseurDTO.getCodeApeNafNace());
        fournisseur.setFormeJuridique(fournisseurDTO.getFormeJuridique());
        if (fournisseurDTO.getDescription() != null) {
            fournisseur.setDescription(fournisseurDTO.getDescription());
        }
        if (fournisseurDTO.getSiteInternet() != null) {
            String url = fournisseurDTO.getSiteInternet().toLowerCase();
            if (url.matches("^http://.*$")) {
                url = url.substring(7);
            }
            if (url.matches("^https://.*$")) {
                url = url.substring(8);
            }
            fournisseur.setSiteInternet(url);
        }
        Optional.ofNullable(fournisseurDTO.getCategorie()).map(ValueLabelDTO::getValue).flatMap(categorieFournisseurRepository::findFirstByCode).ifPresent(fournisseur::setCategorie);
        fournisseur = fournisseurRepository.saveAndFlush(fournisseur);

        return toDTO(organisme, fournisseur);
    }

    @Override
    public FournisseurDTO addFournisseurEtranger(final Long organismeId, final FournisseurDTO fournisseurDTO) {
        final Optional<Fournisseur> fournisseurOpt = fournisseurRepository.findOneBySirenAndPays(fournisseurDTO.getSiren(), fournisseurDTO.getPays());
        final Organisme organisme = organismeRepository.findById(organismeId).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver l'organisme avec l'identifiant " + organismeId));

        if (fournisseurOpt.isPresent()) {
            var fournisseur = fournisseurOpt.get();
            return fournisseurMapper.toDTO(fournisseur, new ArrayList<>());
        } else {
            var fournisseur = new Fournisseur();
            fournisseur.setSiren(fournisseurDTO.getSiren());
            fournisseur.setPays(fournisseurDTO.getPays());
            fournisseur.setRaisonSociale(fournisseurDTO.getRaisonSociale());
            fournisseur.setTrusted(false);
            Optional.ofNullable(fournisseurDTO.getCategorie()).map(ValueLabelDTO::getValue).flatMap(categorieFournisseurRepository::findFirstByCode).ifPresent(fournisseur::setCategorie);
            fournisseur = fournisseurRepository.save(fournisseur);
            var etablissement = new Etablissement();
            etablissement.setFournisseur(fournisseur);
            etablissement.setSiret(fournisseurDTO.getSiren() + "00");
            etablissement.setAdresse(new Adresse());
            etablissement.getAdresse().setPays(fournisseurDTO.getPays());
            etablissementRepository.save(etablissement);
            var etablissementDTO = etablissementMapper.toDTO(etablissement);
            var retour = fournisseurMapper.toDTO(fournisseur, new ArrayList<>());
            retour.getEtablissements().add(etablissementDTO);
            return retour;
        }
    }

    @Override
    public Optional<Map<String, Object>> getAttestationsSiret(final String siret) {
        try {
            Optional<Map<String, Object>> documents = baseCentraleInterface.getDocumentsBySiret(siret);
            // S'il y a une erreur, tracer mais retourner quand même le résultat brut
            if (documents.isPresent() && documents.get().containsKey(sgmapErrorCode)) {
                LOG.error("Erreur lors de l'appel au WS de récupération des attestions : " + documents.get().get(sgmapErrorCode).toString());
            }
            return documents;
        } catch (final Exception e) {
            LOG.error("Erreur : ", e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Map<String, Object>> getAttestationsSiren(final String siren) {
        try {
            Optional<Map<String, Object>> documents = baseCentraleInterface.getDocumentsBySiren(siren);
            // S'il y a une erreur, tracer mais retourner quand même le résultat brut
            if (documents.isPresent() && documents.get().containsKey(sgmapErrorCode)) {
                LOG.error("Erreur lors de l'appel au WS de récupération des attestions : " + documents.get().get(sgmapErrorCode).toString());
            }
            return documents;
        } catch (final Exception e) {
            LOG.error("Erreur : ", e);
            return Optional.empty();
        }
    }

    @Override
    public Page<ContratEtablissementDTO> findByFournisseur(final Long organismeId, ContratEtablissementCriteriaDTO contratEtablissementCriteriaDTO, final Pageable pageable) {
        final var plateforme = organismeRepository.findById(organismeId).map(Organisme::getPlateforme).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver l'organisme avec l'identifiant " + organismeId));
        final var fournisseur = fournisseurRepository.findById(contratEtablissementCriteriaDTO.getIdFournisseur()).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver le fournisseur avec l'identifiant " + contratEtablissementCriteriaDTO.getIdFournisseur()));
        var criteria = contratEtablissementMapperV2.toCriteria(contratEtablissementCriteriaDTO);
        criteria.setPfUid(plateforme.getMpeUid());
        criteria.setIdOrganisme(organismeId);
        final Page<ContratEtablissement> contratEtablissements = contratEtablissementParFournisseurRepository.rechercher(
                criteria,
                pageable
        );

        return new PageImpl<>(contratEtablissements.getContent().stream().map(contratEtablissementMapperV2::toDTO).collect(Collectors.toList()), pageable, contratEtablissements.getTotalElements());
    }

    @Override
    public String formatUrlFicheFournisseur(String uuid) {
        return MessageFormat.format(ficheFournisseurPath, uuid);
    }
}
