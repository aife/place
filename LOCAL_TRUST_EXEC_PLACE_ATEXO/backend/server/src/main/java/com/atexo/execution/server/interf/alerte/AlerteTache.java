package com.atexo.execution.server.interf.alerte;

import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.alerte.AlerteService;
import com.atexo.execution.server.service.calendrier.CalendrierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Thread d'envoi d'alertes mail
 * Created by qba on 29/11/16.
 */

@Component("alerteTacheTask")
public class AlerteTache implements AlerteTacheTask {

    final static Logger LOG = LoggerFactory.getLogger(AlerteTache.class);

    @Autowired
    CalendrierService calendrierService;

    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    AlerteService alerteService;

    @Autowired
    ContratRepository contratRepository;

    Boolean active = false;

    /**
     * Envoi des alertes mails
     */
    public final void alerte() {


        if (!active) {

            LOG.info("Début de l'envoi du mail...");
            active = true;
            List<EvenementDTO> evenements = calendrierService.getEvenements(true, false);
            for (EvenementDTO evenementDTO : evenements) {
                try {
                    boolean sent = alerteService.envoiMailAlerte(evenementDTO);
                    if (sent)
                        calendrierService.modifEnvoiEvenement(evenementDTO.getId());
                } catch (Exception e) {
                    LOG.error("Problème lors de la  création du mail alerte de l'évenement : " + evenementDTO.getId(), e);
                }
            }
            for (var contrat : contratRepository.findByDateDebutEtudeRenouvellementAfterAndDateEnvoiAlerteIsNull(LocalDate.now())) {
                try {
                    if (contrat.isARenouveler()) {
                        alerteService.envoiMailAlerte(contrat);
                        contrat.setDateEnvoiAlerte(LocalDateTime.now());
                        contratRepository.save(contrat);
                    }
                } catch (Exception e) {
                    LOG.error("Problème lors de la  création du mail alerte pour le contrat : {}", contrat.getNumero(), e);
                }
            }
            active = false;
            LOG.info("Fin de l'envoi du mail...");

        } else {
            LOG.warn("La precedente tache est toujours en cours d'execution");
        }
    }


}
