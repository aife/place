package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;

public class EchangeNonAutoriseException extends ApplicationBusinessException {
	public EchangeNonAutoriseException(String reason) {
		super(String.format("Echange chorus non autorisé = %s", reason));
	}
}
