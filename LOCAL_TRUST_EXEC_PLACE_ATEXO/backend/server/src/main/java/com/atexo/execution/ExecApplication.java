package com.atexo.execution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
@EnableJpaRepositories(basePackages = {"com.atexo.execution.server.repository"})
@PropertySource(value = {"classpath:config-env.properties"}, encoding = "UTF-8")
@PropertySource(value = {"classpath:config-env-client.properties"}, ignoreResourceNotFound = true)
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableCaching
@EnableConfigurationProperties
public class ExecApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExecApplication.class, args);
    }

}
