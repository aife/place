package com.atexo.execution.server.service.fournisseur;


import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.Optional;

public interface FournisseurService {

    Page<FournisseurDTO> find(FournisseurCriteriaDTO criteria, Pageable pageable);

    Optional<FournisseurDTO> getFournisseur(final Long organismeId, Long contratId);

    Optional<FournisseurDTO> creerFournisseur(final Long organismeId, FournisseurDTO fournisseurDTO) throws ApplicationException;

    FournisseurDTO updateFournisseur( final OrganismeDTO organismeDTO, final FournisseurDTO fournisseur) throws ApplicationException;

    FournisseurDTO addFournisseurEtranger(final Long organismeId, final FournisseurDTO fournisseur);

    Optional<Map<String, Object>> getAttestationsSiret(String sirenOrSiret);

    Optional<Map<String, Object>> getAttestationsSiren(String sirenOrSiret);

    Page<ContratEtablissementDTO> findByFournisseur(final Long organismeId, ContratEtablissementCriteriaDTO contratEtablissementCriteriaDTO, final Pageable pageable);
    String formatUrlFicheFournisseur (final String uuid);
}
