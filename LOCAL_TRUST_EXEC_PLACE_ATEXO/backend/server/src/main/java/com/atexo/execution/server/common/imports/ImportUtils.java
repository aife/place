package com.atexo.execution.server.common.imports;


import com.atexo.execution.common.dto.PairDTO;

import java.util.HashMap;
import java.util.Map;


public class ImportUtils {

    public static final Map<String, PairDTO<String, Boolean>> CHAMPS_CONTRAT = new HashMap<>();
    public static final Map<String, PairDTO<String, Boolean>> CHAMPS_CONTRACTANT = new HashMap<>();

    static {
        initChampsContrat();
        initChampsContractant();
    }

    private static void initChampsContrat() {
        CHAMPS_CONTRAT.put("type", new PairDTO<>("Enum", true));
        CHAMPS_CONTRAT.put("referenceLibre", new PairDTO<>("alphanumeric", true));
        CHAMPS_CONTRAT.put("attributaire.siret", new PairDTO<>("Siret", true));
        CHAMPS_CONTRAT.put("consultation.numero", new PairDTO<>("alphanumeric", false));
        CHAMPS_CONTRAT.put("objet", new PairDTO<>("alphanumeric", true));
        CHAMPS_CONTRAT.put("consultation.categorie", new PairDTO<>("Enum", true));
        CHAMPS_CONTRAT.put("service.organisme.acronyme", new PairDTO<>("Organisme", false));
        CHAMPS_CONTRAT.put("service.nom", new PairDTO<>("service", true));
        CHAMPS_CONTRAT.put("createur.nom", new PairDTO<>("String", true));
        CHAMPS_CONTRAT.put("createur.prenom", new PairDTO<>("String", true));
        CHAMPS_CONTRAT.put("formePrix", new PairDTO<>("Enum", false));
        CHAMPS_CONTRAT.put("typeBorne", new PairDTO<>("Enum", false));
        CHAMPS_CONTRAT.put("borneMinimale", new PairDTO<>("Double", false));
        CHAMPS_CONTRAT.put("borneMaximale", new PairDTO<>("Double", false));
        CHAMPS_CONTRAT.put("montant", new PairDTO<>("Double", true));
        CHAMPS_CONTRAT.put("montantMandate", new PairDTO<>("Double", false));
        CHAMPS_CONTRAT.put("montantFacture", new PairDTO<>("Double", false));
        CHAMPS_CONTRAT.put("dateNotification", new PairDTO<>("Date", true));
        CHAMPS_CONTRAT.put("dateDemaragePrestation", new PairDTO<>("Date", false));
        CHAMPS_CONTRAT.put("dateFinContrat", new PairDTO<>("Date", false));
        CHAMPS_CONTRAT.put("dateMaxFinContrat", new PairDTO<>("Date", false));
        CHAMPS_CONTRAT.put("groupement", new PairDTO<>("Boolean", true));
        CHAMPS_CONTRAT.put("groupement.nom", new PairDTO<>("alphanumeric", false));
        CHAMPS_CONTRAT.put("groupement.type", new PairDTO<>("Enum", false));
    }

    private static void initChampsContractant() {
        CHAMPS_CONTRACTANT.put("contratReferenceLibre", new PairDTO<>("alphanumeric", true));
        CHAMPS_CONTRACTANT.put("etablissement.siret", new PairDTO<>("Siret", true));
        CHAMPS_CONTRACTANT.put("type", new PairDTO<>("typeCotractant", true));
        CHAMPS_CONTRACTANT.put("categorieConsultation", new PairDTO<>("Enum", true));
        CHAMPS_CONTRACTANT.put("role", new PairDTO<>("String", false));
        CHAMPS_CONTRACTANT.put("montant", new PairDTO<>("Double", true));
        CHAMPS_CONTRACTANT.put("dateNotification", new PairDTO<>("Date", true));
        CHAMPS_CONTRACTANT.put("commanditaire.siret", new PairDTO<>("Siret", false));
        CHAMPS_CONTRACTANT.put("contrat.attributaire.etablissement.siret", new PairDTO<>("Siret", false));

    }

    public static boolean isSiretSyntaxValide(String siret) {
        int total = 0;
        int digit = 0;

        for (int i = 0; i < siret.length(); i++) {
            if ((i % 2) == 0) {
                digit = Integer.parseInt(String.valueOf(siret.charAt(i))) * 2;
                if (digit > 9) {
                    digit -= 9;
                }
            } else {
                digit = Integer.parseInt(String.valueOf(siret.charAt(i)));
            }
            total += digit;
        }
        /** Si la somme est un multiple de 10 alors le SIRET est valide */
        return (total % 10) == 0;
    }

}
