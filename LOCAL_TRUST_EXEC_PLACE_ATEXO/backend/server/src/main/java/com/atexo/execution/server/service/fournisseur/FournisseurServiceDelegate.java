package com.atexo.execution.server.service.fournisseur;

import com.atexo.execution.server.model.Fournisseur;

import java.util.Optional;

/**
 * Delegate non transactionnel à utiliser dans des sessions d'autres services
 */
public interface FournisseurServiceDelegate {

    /**
     * Méthode qui retourne un contrat en vérifiant si l'utilisateur connecté appartient au même organisme que le contrat
     *
     * @param fournisseurId
     * @return
     */
    Optional<Fournisseur> getFournisseur(Long fournisseurId);
}
