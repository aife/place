package com.atexo.execution.server.service;

import com.atexo.execution.server.model.ParametrageApplication;

import java.util.List;
import java.util.Map;

public interface ParametrageService {

    List<ParametrageApplication> getAllParametrageExportFrontByUtilisateurUuid(String userUUid);

    boolean isActif(String pfUid, String cle);

    Map<String,String> getParametrageMessagerie(String pfUid);
}
