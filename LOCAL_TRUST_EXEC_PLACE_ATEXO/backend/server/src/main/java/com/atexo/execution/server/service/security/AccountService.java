package com.atexo.execution.server.service.security;

import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.server.model.security.Account;

import java.util.Optional;

public interface AccountService {

	Optional<Account> recuperer(String plateforme, AgentSSOType agentSSOType);
	Account sauvegarder( Account user);

	Account recuperer(String plateformeUid, String login);
}
