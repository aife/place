package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ErrorDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationSecurityException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.common.exception.RessourcesException;
import com.atexo.execution.server.oauth.grants.exceptions.AuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class ErrorControllerAdvice {
    private static final String GENERIC_MESSAGE = "Une erreur est survenue, veuillez contacter votre administrateur.";

    @ExceptionHandler(Exception.class)
    public ResponseEntity<List<ErrorDTO>> handleException(final Exception exception) {
        final var error = new ErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), GENERIC_MESSAGE, exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ApplicationTechnicalException.class)
    public ResponseEntity<List<ErrorDTO>> handleApplicationTechnicalException(final ApplicationTechnicalException exception) {
        final var error = new ErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ApplicationBusinessException.class)
    public ResponseEntity<List<ErrorDTO>> handleApplicationBusinessException(final ApplicationBusinessException exception) {
        final var error = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RessourcesException.class)
    public ResponseEntity<List<ErrorDTO>> handleRessourcesException(final RessourcesException exception) {
        final var error = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ClientAbortException.class)
    public void handleClientAbortException(ClientAbortException e) {
        log.warn("La communication a été interrompu par le client. Message = {}", e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorDTO>> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(ex.getBindingResult().getFieldErrors().stream()
                .map(
                fieldError -> {
                    final var error = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), null != fieldError.getDefaultMessage() ? fieldError.getDefaultMessage() : "Argument non valide", ex);
                    error.setRejectedValue(fieldError.getRejectedValue());
                    return error;
                }
        ).collect(Collectors.toList()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApplicationSecurityException.class)
    public ResponseEntity<List<ErrorDTO>> handleApplicationSecurityException(final ApplicationSecurityException exception) {
        final var error = new ErrorDTO(HttpStatus.UNAUTHORIZED.value(), exception.getMessage(), exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<List<ErrorDTO>> handleAuthenticationException(final AuthenticationException exception) {
        final var error = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), exception);

        return new ResponseEntity<>(List.of(error), HttpStatus.BAD_REQUEST);
    }
}
