package com.atexo.execution.server.oauth.grants;

import com.atexo.execution.common.dto.AccountDTO;

public interface AccountCodeAuthorizer {

    AccountDTO authorize(String code, String plateforme);

}
