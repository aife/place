package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.mpe.ws.api.AgentSSOType;
import com.atexo.execution.server.common.mapper.UtilisateurMapper;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.HabilitationRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.execution.server.mapper.utils.IDUtils.extractIdFromURI;


@org.springframework.stereotype.Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UtilisateurServiceImpl implements UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;
    private final ServiceRepository serviceRepository;
    private final HabilitationRepository habilitationRepository;
    private final PlateformeRepository plateformeRepository;
    private final MpeClient mpeClient;
    @Value("${mpe.exec.module.name:moduleExec}")
    private String moduleExecName;

    @Override
    public UtilisateurDTO getUtilisateurByUuidAndPlateforme(final String pfUid, final String login, Set<String> authorities) {
        var utilisateur = utilisateurRepository.findByUuidAndPlateformeMpeUid(login, pfUid).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver l'utilisateur pour le login " + login));
        utilisateur.setHabilitations(
                authorities.stream().map(authority -> {
                            log.info("Récupération de l'habilitation '{}' pour l'utilisateur {}", authority, utilisateur.getIdentifiant());

                            return habilitationRepository.findByCode(authority)
                                    .map(
                                            habilitation -> {
                                                log.info("L'habilitation '{}' a été trouvé en base = #{}", authority, habilitation.getId());

                                                return habilitation;
                                            }
                                    ).orElseGet(
                                            () -> {
                                                log.info("L'habilitation '{}' N'A PAS été trouvé en base", authority);

                                                return null;
                                            }
                                    );
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet()));
        var utilisateurDTO = utilisateurMapper.toDTO(utilisateur);
        var idAgentMpe = MapperUtils.toLong(utilisateur.getIdExterne());
        if(idAgentMpe != null){
            log.info("L'identifiant externe de l'utilisateur est un identifiant MPE = {}", idAgentMpe);
            log.info("Récupération des modules activés pour l'utilisateur {}", utilisateur.getIdentifiant());
            try {

                var agent = mpeClient.getUtilisateur(pfUid, idAgentMpe);
                if (agent != null && agent.getModulesOrganisme() != null) {
                    utilisateurDTO.setModulesActifs(new HashSet<>(agent.getModulesOrganisme()));
                    utilisateurDTO.setModuleExecActif(agent.getModulesOrganisme().stream().map(String::toLowerCase).collect(Collectors.toList()).contains(moduleExecName.toLowerCase()));
                    log.info("Les modules activés pour l'utilisateur {} sont {}", utilisateur.getIdentifiant(), utilisateurDTO.getModulesActifs());
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération des modules activés pour l'utilisateur", e);
            }
            try {
                var services = mpeClient.getUtilisateurPerimetreVision(pfUid, idAgentMpe);
                if (!CollectionUtils.isEmpty(services)) {
                    utilisateurDTO.setPerimetreVisionServices(services);
                    log.info("Les services  auxquels l’agent {} a accès dans son périmètre de vision sont {}", utilisateur.getIdentifiant(), utilisateurDTO.getPerimetreVisionServices());
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération des habilitations de l'utilisateur", e);
            }

        }
        return utilisateurDTO;
    }

    @Override
    public List<UtilisateurDTO> getUtilisateursByEmailIn(final String[] emails) {
        return utilisateurRepository.findByEmailIn(List.of(emails)).stream().map(utilisateurMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<UtilisateurDTO> getUtilisateursByOrganismeAndService(final Long organismeId, final Long serviceId) {
        var plateforme = serviceRepository.findById(serviceId).map(Service::getPlateforme).orElseThrow(()->new IllegalArgumentException("Le service n'a pas de plateforme"));
        return serviceRepository.findByIdAndOrganismeId(serviceId, organismeId)
                .map(service -> utilisateurRepository.findByServiceIdAndPlateformeIdOrderByNomAsc(serviceId,plateforme.getId()).stream().map(utilisateur -> utilisateurMapper.toDTO(utilisateur)).collect(Collectors.toList()))
                .orElseGet(Lists::newArrayList);
    }

    @Override
    public List<UtilisateurDTO> getAllUtilisateursActifs() {
        return utilisateurRepository.findByActif(true).stream().map(utilisateurMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable("utilisateurs")
    public Optional<UtilisateurDTO> getUtilisateurParUuid(String uuid) {
        return utilisateurRepository.findByUuid(uuid).map(utilisateurMapper::toDTO);
    }

    @Override
    public UtilisateurDTO getUtilisateurParId(Long id) {
        return utilisateurRepository.findById(id).map(utilisateurMapper::toDTO).orElse(null);
    }

    @Override
    public UtilisateurDTO sauvegarder(String pfUid, AgentSSOType response) {
        var agentMPE = mpeClient.getUtilisateur(pfUid, Long.valueOf(response.getId()));
        if (agentMPE != null) {
            var organisme = extractIdFromURI(agentMPE.getUriOrganisme());
            var idExterne = MapperUtils.buildIdExterne(organisme, response.getId());
            var utilisateur = utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterne, pfUid).orElse(null);
            if (utilisateur == null) {
                var idService = extractIdFromURI(agentMPE.getUriService());
                if (idService == null) {
                    log.warn("L'utilisateur {} n'a pas de service on cherche le service racine", response.getIdentifiant());
                    idService = "0";
                }
                var idExterneService = MapperUtils.buildIdExterne(organisme, idService);
                log.warn("Utilisateur {} non trouvé pour la plateforme {}, on le crée", response.getIdentifiant(), pfUid);
                utilisateur = new Utilisateur();
                var uuid = MapperUtils.buildIdExterne(pfUid, response.getId());
                serviceRepository.findByIdExterneAndPlateformeMpeUid(idExterneService, pfUid).ifPresent(utilisateur::setService);
                plateformeRepository.findByMpeUid(pfUid).ifPresent(utilisateur::setPlateforme);
                utilisateur.setIdentifiant(response.getIdentifiant());
                utilisateur.setUuid(uuid);
                utilisateur.setIdExterne(idExterne);
                utilisateur.setNom(agentMPE.getNom());
                utilisateur.setPrenom(agentMPE.getPrenom());
                utilisateur.setEmail(agentMPE.getEmail());
                utilisateur.setRefOrganisme(organisme);
                utilisateur.setActif(true);
                utilisateur = utilisateurRepository.save(utilisateur);
            }
            return utilisateurMapper.toDTO(utilisateur);
        }
        return null;
    }
}
