package com.atexo.execution.server.service;

import com.atexo.execution.common.def.TypeGroupement;
import com.atexo.execution.common.dto.ContratEtablissementDTO;
import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.dto.process.attributaire.AttributaireDTO;
import com.atexo.execution.common.dto.process.attributaire.AttributaireEtablissementDTO;
import com.atexo.execution.common.dto.process.attributaire.AttributaireGroupementDTO;
import com.atexo.execution.common.dto.process.attributaire.EditionAttributaireDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.common.Preconditions;
import com.atexo.execution.server.common.mapper.ContratEtablissementGroupementMapper;
import com.atexo.execution.server.common.mapper.DateTimeMapper;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.referentiels.CategorieConsultationRepository;
import com.atexo.execution.server.repository.referentiels.RevisionPrixRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AttributaireServiceImpl implements AttributaireService {

    private final static Logger LOG = LoggerFactory.getLogger(EtablissementServiceImpl.class);

    @Autowired
    ContratRepository contratRepository;

    @Autowired
    ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    ContratEtablissementGroupementRepository contratEtablissementGroupementRepository;

    @Autowired
    ContratEtablissementMapperV2 contratEtablissementMapperV2;

    @Autowired
    ContratEtablissementGroupementMapper ContratEtablissementGroupementMapper;

    @Autowired
    DateTimeMapper dateTimeMapper;

    @Autowired
    ReferentielMapper referentielMapper;

    @Autowired
    CategorieConsultationRepository categorieConsultationRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    RevisionPrixRepository revisionPrixRepository;

    private Contrat getContrat(Long organismeId, Long contratId) throws ApplicationBusinessException {
        Preconditions.checkArgument(contratId != null);
        return contratRepository.findById(contratId).orElseThrow(ApplicationBusinessException::new);
    }

    private Etablissement getEtablissement(EtablissementDTO etablissementDTO) throws ApplicationBusinessException {
        if (etablissementDTO == null || etablissementDTO.getId() == null) {
            throw new ApplicationBusinessException("Etablissement introuvable");
        }
        return etablissementRepository.findById(etablissementDTO.getId()).orElseThrow(ApplicationBusinessException::new);
    }

    private ContratEtablissement checkRelationExistence(Long contratId, Long etablissementId, boolean throwExceptionifExists)
            throws ApplicationBusinessException {
        var contratEtablissement = contratEtablissementRepository
                .findFirstByContratIdAndEtablissementId(contratId, etablissementId).orElse(new ContratEtablissement());
        contratEtablissement.setContrat(contratRepository.findById(contratId).orElseThrow(() ->
                new ApplicationBusinessException("Contrat introuvable" + contratId)));
        contratEtablissement.setEtablissement(etablissementRepository.findById(etablissementId).orElseThrow(() ->
                new ApplicationBusinessException("Etablissement introuvable" + etablissementId)));
        return contratEtablissement;
    }

    @Override
    public AttributaireDTO getArborescenceAttributaires(Long organismeId, Long contratId) throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        AttributaireDTO attributaire = null;

        List<ContratEtablissement> contratEtablissementsList = contratEtablissementRepository.findByContratId(contratId);

        if (contrat.getGroupement() != null) {

            AttributaireGroupementDTO attributaireGroupement = new AttributaireGroupementDTO();
            attributaire = attributaireGroupement;
            attributaireGroupement.setGroupement(ContratEtablissementGroupementMapper.toDTO(contrat.getGroupement()));

            // Groupement - mandataire ou co-traitants
            contratEtablissementsList.stream().filter(contratEtablissement -> contratEtablissement.getCommanditaire() == null)
                    .forEach(contratEtablissement -> {

                        ContratEtablissementDTO contratEtablissementDTO = contratEtablissementMapperV2.toDTO(contratEtablissement);

                        if (Boolean.TRUE.equals(contratEtablissement.getMandataire())) {
                            attributaireGroupement.setMandataire(contratEtablissementDTO);

                        } else {
                            attributaireGroupement.getCoTraitants().add(contratEtablissementDTO);
                        }

                    });

            // Groupement - sous traints des mandataire ou des co traitants
            contratEtablissementsList.stream().filter(contratEtablissement -> contratEtablissement.getCommanditaire() != null)
                    .forEach(contratEtablissement -> {

                        ContratEtablissementDTO contratEtablissementDTO = contratEtablissementMapperV2.toDTO(contratEtablissement);

                        ContratEtablissementDTO mandataire = attributaireGroupement.getMandataire();

                        if (mandataire.getEtablissement().getId().equals(contratEtablissement.getCommanditaire().getId())) {

                            // rattachement au mandtaiare
                            mandataire.getSousTraitants().add(contratEtablissementDTO);

                        } else {

                            // rattachement à l'un des co traitant
                            Optional<ContratEtablissementDTO> coTraitantOpt = attributaireGroupement.getCoTraitants().stream()
                                    .filter(coTraitant -> {

                                        return coTraitant.getEtablissement().getId()
                                                .equals(contratEtablissement.getCommanditaire().getId());

                                    }).findFirst();

                            coTraitantOpt.ifPresent(etablissementDTO -> etablissementDTO.getSousTraitants().add(contratEtablissementDTO));

                        }

                    });

        } else {

            AttributaireEtablissementDTO attributaireEtablissement = new AttributaireEtablissementDTO();
            attributaire = attributaireEtablissement;
            attributaireEtablissement.setContratEtablissement(contratEtablissementMapperV2.toDTO(contrat.getAttributaire()));

            contratEtablissementsList.stream().filter(contratEtablissement -> contratEtablissement.getCommanditaire() != null)
                    .forEach(contratEtablissement -> {

                        attributaireEtablissement.getContratEtablissement().getSousTraitants()
                                .add(contratEtablissementMapperV2.toDTO(contratEtablissement));
                    });

        }

        return attributaire;
    }

    private void mapContrtEtablissement(EditionAttributaireDTO editionAttributaireDTO, ContratEtablissement contratEtablissement,
                                        boolean isAttributaire) {

        contratEtablissement.setDureeMois(editionAttributaireDTO.getDureeMois());
        Optional.ofNullable(editionAttributaireDTO.getRevisionPrix())
                .map(ValueLabelDTO::getValue)
                .map(revisionPrixRepository::findByCode)
                .map(Optional::get)
                .ifPresent(contratEtablissement::setRevisionPrix);
        if (!isAttributaire) {
            if (editionAttributaireDTO.getCategorieConsultation() != null) {
            contratEtablissement.setCategorieConsultation(
                    categorieConsultationRepository.findOneByCode(editionAttributaireDTO.getCategorieConsultation().getValue()));
            }


            contratEtablissement
                    .setDateNotification(MapperUtils.convertToLocalDate(editionAttributaireDTO.getDateNotification()));
        }

        contratEtablissement.setMontant(editionAttributaireDTO.getMontant());
    }

    @Override
    public ContratEtablissementDTO ajouterSousTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
            throws ApplicationBusinessException {

        Preconditions.checkArgument(editionAttributaireDTO != null);

        Preconditions.checkArgument(editionAttributaireDTO.getCommanditaireId() != null);

        Contrat contrat = getContrat(organismeId, contratId);

        Etablissement etablissement = getEtablissement(editionAttributaireDTO.getEtablissement());

        // exception si le commanditaire n'a pas de relation
        ContratEtablissement contratEtablissementCommanditaire = checkRelationExistence(contratId,
                editionAttributaireDTO.getCommanditaireId(), false);

        // exception si l'établissement a une relation
        try {
            checkRelationExistence(contratId, editionAttributaireDTO.getEtablissement().getId(), true);
        } catch (ApplicationBusinessException e) {
            LOG.error(e.getMessage());
            return null;
        }
        ContratEtablissement contratEtablissement = new ContratEtablissement();
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setEtablissement(etablissement);

        Etablissement etablissementCommanditaire = contratEtablissementCommanditaire.getEtablissement();
        contratEtablissement.setCommanditaire(etablissementCommanditaire);
        contratEtablissement.setPlateforme(contrat.getPlateforme());

        mapContrtEtablissement(editionAttributaireDTO, contratEtablissement, false);

        // enregistrement
        contratEtablissement = contratEtablissementRepository.save(contratEtablissement);

        LOG.info("{} a été rattaché comme sous traitant de {} pour le contrat {}", etablissement.toString(),
                etablissementCommanditaire.toString(), contrat);

        return contratEtablissementMapperV2.toDTO(contratEtablissement);
    }

    @Override
    public void supprimerSousTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionContratEtablissementDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        Etablissement etablissement = getEtablissement(editionContratEtablissementDTO.getEtablissement());

        // exception si l'établissement n'a pas de relation
        ContratEtablissement contratEtablissement = checkRelationExistence(contrat.getId(), etablissement.getId(), false);

        String commanditaireAsString = contratEtablissement.getCommanditaire().toString();
        // Suppression des Contacts Externes du sous-traitant
        for (ContactReferant contactReferant : contratEtablissement.getContactReferantList()) {
            if (contactReferant.getContact().getEtablissement() == null) {
                contactRepository.delete(contactReferant.getContact());
            }
        }

        contratEtablissementRepository.delete(contratEtablissement);

        LOG.info("{} n'est plus rattaché comme sous traitant de {} pour le contrat {}", etablissement,
                commanditaireAsString, contrat);
    }

    @Override
    public AttributaireDTO transformerEnGroupement(Long organismeId, Long contratId, ContratEtablissementGroupementDTO groupementDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        // il y a dèja un groupement;
        if (contrat.getGroupement() != null) {
            throw new ApplicationBusinessException();
        }

        ContratEtablissementGroupement groupement = new ContratEtablissementGroupement();
        groupement.setNom(groupementDTO.getNom());
        groupement.setType(TypeGroupement.valueOf(groupementDTO.getType().getValue()));
        groupement = contratEtablissementGroupementRepository.save(groupement);

        contrat.setGroupement(groupement);
        contratRepository.save(contrat);
        if (contrat.getAttributaire() != null) {
            contrat.getAttributaire().setMandataire(true);
            contratEtablissementRepository.save(contrat.getAttributaire());
        } else {
            LOG.warn("Le contrat {} n'a pas d'attributaire", contratId);
        }
        contratEtablissementRepository.save(contrat.getAttributaire());

        return getArborescenceAttributaires(organismeId, contrat.getId());
    }

    @Override
    public ContratEtablissementDTO ajouterCoTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        Etablissement etablissement = getEtablissement(editionAttributaireDTO.getEtablissement());

        try {
            // exception si l'établissement a une relation
            checkRelationExistence(contrat.getId(), etablissement.getId(), true);
        } catch (ApplicationBusinessException e) {
            LOG.info(e.getMessage());
            return null;
        }
        if (contrat.getGroupement() == null) {
            throw new ApplicationBusinessException();
        }

        ContratEtablissement contratEtablissement = new ContratEtablissement();
        contratEtablissement.setContrat(contrat);
        contratEtablissement.setEtablissement(etablissement);

        mapContrtEtablissement(editionAttributaireDTO, contratEtablissement, false);

        contratEtablissement.setMandataire(false);
        contratEtablissement.setPlateforme(contrat.getPlateforme());

        // enregistrement
        contratEtablissement = contratEtablissementRepository.save(contratEtablissement);

        return contratEtablissementMapperV2.toDTO(contratEtablissement);
    }

    @Override
    public void supprimerCoTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionContratEtablissementDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);
        ContratEtablissement contratEtablissement = contratEtablissementRepository.findById(editionContratEtablissementDTO.getIdContratEtablissement()).orElseThrow(ApplicationBusinessException::new);

        if (contrat.getGroupement() == null) {
            throw new ApplicationBusinessException();
        }
        // Récuperation des ids contacts Externe lié a aux soutraitants du co-traitant
        List<Long> ids = new ArrayList<>();
        List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByCommanditaireId(contratEtablissement.getEtablissement().getId());
        for (ContratEtablissement contratEtablissement1 : contratEtablissements) {
            ids.addAll(contratEtablissement1.getContactReferantList().stream().map(contactReferant -> contactReferant.getContact().getId()).collect(Collectors.toList()));
        }

        // on suprrime la liste des sous traitant
        contratEtablissementRepository.deleteByContratIdAndCommanditaireId(contratId, contratEtablissement.getEtablissement().getId());

        // Suppression des contacts Externe du Co-traitant
        for (ContactReferant contactReferant : contratEtablissement.getContactReferantList()) {
            if (contactReferant.getContact().getEtablissement() == null) {
                ids.add(contactReferant.getContact().getId());
            }
        }

        // Suppression des contacts Externe
        contactRepository.deleteByEtablissementIsNullAndIdIn(ids);

        contratEtablissementRepository.delete(contratEtablissement);
    }

    @Override
    public AttributaireDTO degrouperGroupement(Long organismeId, Long contratId) throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        ContratEtablissementGroupement groupement = contrat.getGroupement();
        contrat.setGroupement(null);
        contratRepository.save(contrat);
        contratEtablissementGroupementRepository.delete(groupement);

        ContratEtablissement attributaire = contrat.getAttributaire();
        // Sans groupement, le montant de l'attributaire doit rester égal au montant du contrat
        attributaire.setMontant(contrat.getMontant());
        attributaire.setDateNotification(contrat.getDateNotification());
        contratEtablissementRepository.save(attributaire);

        List<ContratEtablissement> contratEtablissements = contratEtablissementRepository.findByContratId(contrat.getId());

        for (ContratEtablissement contratEtablissement : contratEtablissements) {
            if (contratEtablissement.getId().equals(attributaire.getId())) {
                contratEtablissement.setMandataire(null);
            } else {
                contratEtablissement.setCommanditaire(attributaire.getEtablissement());
                contratEtablissement.setMandataire(null);
            }
            contratEtablissementRepository.save(contratEtablissement);
        }

        return getArborescenceAttributaires(organismeId, contrat.getId());
    }

    @Override
    public ContratEtablissementGroupementDTO modifierGroupement(Long organismeId, Long contratId, ContratEtablissementGroupementDTO groupementDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);

        ContratEtablissementGroupement groupement = contrat.getGroupement();
        if (groupement == null) {
            throw new ApplicationBusinessException();
        }

        groupement.setNom(groupementDTO.getNom());
        groupement.setType(TypeGroupement.valueOf(groupementDTO.getType().getValue()));
        groupement = contratEtablissementGroupementRepository.save(groupement);

        return ContratEtablissementGroupementMapper.toDTO(groupement);
    }

    @Override
    public ContratEtablissementDTO modifierAttributaire(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
            throws ApplicationBusinessException {

        Contrat contrat = getContrat(organismeId, contratId);
        contrat.setDateModification(LocalDateTime.now());
        Etablissement etablissement = getEtablissement(editionAttributaireDTO.getEtablissement());
        var contratEtablissement = contratEtablissementRepository.findById(editionAttributaireDTO.getIdContratEtablissement()).orElseThrow(ApplicationBusinessException::new);
        boolean isAttributaire = contrat.getAttributaire().getId().equals(contratEtablissement.getId());
        mapContrtEtablissement(editionAttributaireDTO, contratEtablissement, isAttributaire);
        contratEtablissement.setEtablissement(etablissement);
        contratRepository.save(contrat);
        contratEtablissement = contratEtablissementRepository.save(contratEtablissement);
        return contratEtablissementMapperV2.toDTO(contratEtablissement);
    }

    @Override
    public List<ContratEtablissementDTO> modifierAttributaires(Long organismeId, Long contratId, List<EditionAttributaireDTO> editionAttributaireDTOs)
            throws ApplicationBusinessException {
        if (editionAttributaireDTOs == null) {
            return new ArrayList<>();
        }
        List<ContratEtablissementDTO> contratEtablissementDTOs = new ArrayList<>(editionAttributaireDTOs.size());
        for (EditionAttributaireDTO editionAttributaireDTO : editionAttributaireDTOs) {
            contratEtablissementDTOs.add(modifierAttributaire(organismeId, contratId, editionAttributaireDTO));
        }
        return contratEtablissementDTOs;
    }

}
