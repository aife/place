package com.atexo.execution.server.conf;

import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.Optional;

@Component
@Qualifier("auditProvider")
public class AuditorAwareImpl implements AuditorAware<Utilisateur> {

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    EntityManager entityManager;

    @Override
    public Optional<Utilisateur> getCurrentAuditor() {
        try {
            var uuid = Optional.ofNullable(SecurityContextHolder.getContext()).map(SecurityContext::getAuthentication).map(Authentication::getPrincipal).map(p -> (User) p).map(User::getUsername).orElse(null);
            if (uuid == null) {
                return Optional.empty();
            }
            entityManager.setFlushMode(FlushModeType.COMMIT);
            return utilisateurRepository.findByUuid(uuid);
        } catch (Exception e) {
            return Optional.empty();
        }

    }
}
