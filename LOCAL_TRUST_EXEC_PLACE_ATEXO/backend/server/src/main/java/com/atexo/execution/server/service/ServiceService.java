package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.EntiteServiceDTO;
import com.atexo.execution.common.dto.ServiceDTO;

import java.util.List;

public interface ServiceService {

	List<ServiceDTO> getServices( Long organismeId );

	List<EntiteServiceDTO> getServicesTree(Long organismeId);

	List<EntiteServiceDTO> getServicesTree(Long organismeId, Long serviceId);
}
