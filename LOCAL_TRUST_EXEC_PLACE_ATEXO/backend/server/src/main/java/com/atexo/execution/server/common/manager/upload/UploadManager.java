package com.atexo.execution.server.common.manager.upload;

import com.atexo.execution.common.exception.ApplicationTechnicalException;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

public interface UploadManager {

	String saveFile(String name, long size, String mimeType, InputStream inputStream) throws ApplicationTechnicalException;

	Optional<UploadFile> getFile(String reference);

	void removeFile(String reference);

	long getSaveddFilesCount();

	Collection<String> getSavedFilesInfos();

}
