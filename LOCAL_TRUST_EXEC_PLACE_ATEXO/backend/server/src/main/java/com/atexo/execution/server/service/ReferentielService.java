package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.common.dto.ServiceDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;

import java.util.List;

public interface ReferentielService {


	List<ValueLabelDTO> getTypeGroupementList();

	List<ValueLabelDTO> getReferentiel(String plateformeUid, Long organismeId, String typeReferentiel);
}
