package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.EntiteServiceDTO;
import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.dto.docgen.DocumentModeleDTO;
import com.atexo.execution.server.service.ReferentielService;
import com.atexo.execution.server.service.ServiceService;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.document.DocumentContratService;
import com.atexo.execution.server.service.organisme.OrganismeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/referentiel")
public class ReferentielController {

	@Autowired
	ReferentielService referentielService;

	@Autowired
	UtilisateurService utilisateurService;

	@Autowired
	DocumentContratService documentContratService;
	@Autowired
	OrganismeService organismeService;
	@Autowired
	ServiceService serviceService;

	@GetMapping(value = "/modeleDocument")
	public ResponseEntity<List<DocumentModeleDTO>> getModeleDocumentList(@RequestHeader("User-Uuid") final String utilisateurUuid) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return new ResponseEntity<>(documentContratService.getModeleDocumentList(utilisateur.getOrganismeId()), HttpStatus.OK);
	}

	@GetMapping(value = "/{typeReferentiel}")
	public ResponseEntity<List<ValueLabelDTO>> getReferentiel(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("typeReferentiel") final String typeReferentiel) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
		return new ResponseEntity<>(referentielService.getReferentiel(utilisateur.getPlateformeUid(), utilisateur.getOrganismeId(), typeReferentiel), HttpStatus.OK);
	}

	@GetMapping(value = "/typeGroupement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ValueLabelDTO>> getTypeGroupementList() {
		final List<ValueLabelDTO> typeGroupementList = referentielService.getTypeGroupementList();
		return new ResponseEntity<>(typeGroupementList, HttpStatus.OK);
	}

	@GetMapping(value = "/organismes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrganismeDTO>> getOrganismeList(@RequestHeader("User-Uuid") final String utilisateurUuid) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
		final List<OrganismeDTO> organismeDTOS = organismeService.getOrganismeList(utilisateur.getPlateformeUid());
		return new ResponseEntity<>(organismeDTOS, HttpStatus.OK);
	}

	@GetMapping(value = "/services", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EntiteServiceDTO>> getServiceList(@RequestHeader("User-Uuid") final String utilisateurUuid) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
		final List<EntiteServiceDTO> serviceList;
		if (utilisateur.getService() != null)
			serviceList = serviceService.getServicesTree(utilisateur.getOrganismeId(), utilisateur.getService().getId());
		else {
			serviceList = serviceService.getServicesTree(utilisateur.getOrganismeId());
		}
		return new ResponseEntity<>(serviceList, HttpStatus.OK);
	}
}
