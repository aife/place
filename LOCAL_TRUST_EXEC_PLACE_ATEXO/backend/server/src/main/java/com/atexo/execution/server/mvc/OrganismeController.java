package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.organisme.OrganismeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/organismes")
public class OrganismeController {

	@Autowired
	OrganismeService organismeService;

	@Autowired
	private UtilisateurService utilisateurService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrganismeDTO>> getOrganismes(
			@RequestHeader("User-Uuid") final String utilisateurUuid
	) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return new ResponseEntity<>(organismeService.getOrganismes(utilisateur.getOrganismeId()), HttpStatus.OK);
	}
}
