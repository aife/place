package com.atexo.execution.server.service.chorus;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.dto.EchangeChorusCriteriaDTO;
import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.common.mpe.ws.api.RetourChorusType;
import com.atexo.execution.server.common.manager.FileStoreManager;
import com.atexo.execution.server.common.mapper.ActeMapper;
import com.atexo.execution.server.interf.mpe.api.ChorusClient;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.DocumentActe;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.repository.paginable.EchangeChorusPaginableRepository;
import com.atexo.execution.server.repository.referentiels.RetourChorusRepository;
import com.atexo.execution.server.repository.referentiels.StatutChorusRepository;
import com.atexo.execution.server.service.actes.exceptions.ActeIntrouvableException;
import com.atexo.execution.server.service.chorus.exceptions.*;
import com.atexo.execution.server.service.document.DocumentContratService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ChorusServiceImpl implements ChorusService {

	static final Logger LOG = LoggerFactory.getLogger(ChorusServiceImpl.class);

	private static final String VALIDE = "VALIDE";

	private static final String A_ENVOYER = "A_ENVOYER";

	@Autowired
	private EchangeChorusPaginableRepository echangeChorusPaginableRepository;

    @Autowired
    private EchangeChorusMapper echangeChorusMapper;

	@Autowired
	private ContratRepository contratRepository;

    @Autowired
    private EchangeChorusRepository echangeChorusRepository;

    @Autowired
    private ActeRepository acteRepository;

	@Autowired
	private ChorusClient chorusClient;

    @Autowired
    private StatutChorusRepository statutChorusRepository;

	@Autowired
	private RetourChorusRepository retourChorusRepository;

    @Autowired
    private DocumentContratService documentContratService;

    @Autowired
    private FileStoreManager fileStoreManager;

	@Autowired
	private PlateformeRepository plateformeRepository;

    @Autowired
    private ActeMapper acteMapper;


    @Override
    public Page<EchangeChorusDTO> rechercher(Long id, EchangeChorusCriteriaDTO criteria, Pageable pageable, String pfUid) {
        var echanges = echangeChorusPaginableRepository.rechercher(
                criteria,
                pageable
        );

        final List<EchangeChorusDTO> result = echanges.getContent().stream().map(echangeChorusMapper::toDto)
                .map(echangeChorusDTO -> {
                    var acte = acteRepository.findByEchangeChorusIdAndPlateformeMpeUid(echangeChorusDTO.getId(), pfUid);
                    if (acte.isPresent()) {
                        var acteDTO = acteMapper.toDTO(acte.get());
                        echangeChorusDTO.setActeDTO(acteDTO);
                    }
                    return echangeChorusDTO;
                })
                .collect(Collectors.toList());

        return new PageImpl<>(result, pageable, echanges.getTotalElements());
    }

    public File telechargerZip(String plateformeUid, EchangeChorus echangeChorus) throws ApplicationException {
        var echange = echangeChorusRepository.findById(echangeChorus.getId()).orElseThrow(() -> new EchangeChorusIntrouvableException(echangeChorus.getId()));

        try {
            return chorusClient.telechargeZipChorus(plateformeUid, echange.getIdExterne());
        } catch (MPEApiException e) {
            LOG.error("Impossible de télécharger le zip pour l'échange {}", echangeChorus.getId(), e);
			throw new ZipException("Erreur lors de la création du zip");
        }
    }

    @Override
    public File telechargerZip( Contrat contrat) throws ApplicationException {
	    LOG.info("construction du zip chorus a partir des documents du contrat {}", contrat.getNumero());

		var documents = acteRepository.findByContratId(contrat.getId()).stream().map(Acte::getDocuments).flatMap(Collection::stream).collect(Collectors.toList());

	    return construireZip(documents);
    }

	@Override
	public File telechargerZip( Acte acte) throws ApplicationException {
		LOG.info("construction du zip chorus a partir des documents de l'acte {}", acte.getNumero());

		var documents = acte.getDocuments();

		return construireZip(documents);
	}

    private EchangeChorus valider(String plateformeUid, final Acte acte, final EchangeChorus echange, final LocalDateTime dateNotificationPrevisionnelle) throws ApplicationException {
		var statut = statutChorusRepository.findByIdExterne(CodeStatutChorus.FLUX_A_GENERER.getIdExterne()).orElseThrow(()->new StatutChorusIntrouvableException(CodeStatutChorus.FLUX_A_GENERER));

	    echange.setStatut(statut);
		// FIXME nul jusqu'a correction MPE
		LOG.warn("Pas d'envoi de la date de notification {} à CHORUS", dateNotificationPrevisionnelle);
		echange.setDateNotificationPrevisionnelle(null);
		echange.setContrat(acte.getContrat());

		EchangeChorus sauvegarde;

	    try {
            var echangeType = chorusClient.publierEchangeChorus(plateformeUid, acte, echange);

		    var echangeId = String.valueOf(echangeType.getId());

			if (null == echangeId) {
			    throw new MPEApiException("Aucun identifiant externe en retour");
		    }

			LOG.info("Echange CHORUS avec le numéro {} créé avec succès", echangeType.getId());

		    echange.setIdExterne(echangeId);
	    } catch (final MPEApiException e) {
		    LOG.error("Erreur lors de la publication vers chorus {}", echange, e);

		    acte.setStatut(StatutActe.EN_ATTENTE_VALIDATION);

		    throw new PublicationChorusException("Erreur lors de la publication vers CHORUS");
	    } finally {
		    LOG.info("Sauvegarde de l'échange validé {}", echange);

		    sauvegarde = echangeChorusRepository.save(echange);
	    }

	    LOG.info("Sauvegarde de l'acte {}", acte);

	    var statutsKO = retourChorusRepository.findAllByIdExterneIn(
			    RetourStatutChorus.REJETE.getIdExterne(),
			    RetourStatutChorus.IRRECEVABLE.getIdExterne()
	    );

		if (statutsKO.stream().anyMatch(statutKO -> statutKO.equals(sauvegarde.getRetourChorus()))) {
			acte.setStatut(StatutActe.EN_ERREUR);
		}

		acte.setEchangeChorus(sauvegarde);

		acteRepository.save(acte);

        return sauvegarde;
    }

	private EchangeChorus envoyer(String plateformeUid, final Acte acte, EchangeChorus echange) throws ApplicationException {
		var statut = statutChorusRepository.findByIdExterne(CodeStatutChorus.ENVOI_PLANIFIE.getIdExterne()).orElseThrow(()->new StatutChorusIntrouvableException(CodeStatutChorus.ENVOI_PLANIFIE));
        var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme " + plateformeUid));

		LOG.debug("Envoi du ZIP CHORUS avec le statut {}", statut);

		echange.setStatut(statut);
		echange.setActe(acte);
		echange.setDateEnvoi(LocalDateTime.now());
		echange.setPlateforme(plateforme);

		EchangeChorus sauvegarde;

		var documents = acte.getDocuments();
		if (!documents.isEmpty()) {
			LOG.info("Upload de {} documents", documents.size());

			var zip = construireZip(documents);

			try {
                chorusClient.envoyerZipChorus(plateformeUid, echange, zip);

				echange.setDateEnvoiDocuments(LocalDateTime.now());

				acte.setStatut(StatutActe.EN_ATTENTE_VALIDATION);
			} catch (final MPEApiException e) {
				LOG.error("erreur lors de l'envoi du zip vers chorus {}", documents, e);

				acte.setStatut(StatutActe.EN_ERREUR);

				throw new EnvoiZipChorusException("Erreur de l'envoi des documents vers CHORUS");
			} finally {
				documentContratService.deleteFile(zip);

				LOG.info("Sauvegarde de l'échange envoyé {}", echange);
				sauvegarde = echangeChorusRepository.save(echange);
			}
		} else {
			LOG.info("Aucun document a uploader");

			sauvegarde = echange;
		}

		LOG.info("Sauvegarde de l'acte {}", acte);

		acte.setEchangeChorus(sauvegarde);

		acteRepository.save(acte);

		return echange;
	}

	@Override
    public EchangeChorus soumettre(String plateformeUid, final Long idActe, final Long idEchangeChorus, final LocalDateTime dateNotificationPrevisionnelle) throws ApplicationException {
		Acte acte = acteRepository.findById(idActe).orElseThrow(() -> new ActeIntrouvableException(idActe));
		Contrat contrat = acte.getContrat();
		//vérifier qu'aucun autre acte n'est en attente de validation pour ce contrat
		if (contrat.getActes().stream().noneMatch(item -> item.getStatut() == StatutActe.EN_ATTENTE_VALIDATION)) {
			EchangeChorus echange;

			if (null == idEchangeChorus) {
				LOG.info("Validation d'un nouvel échange chorus à la date prévisionnelle {}", dateNotificationPrevisionnelle);

				echange = new EchangeChorus();
			} else {
				LOG.info("Validation de l'échange chorus {} à la date prévisionnelle {}", idEchangeChorus, dateNotificationPrevisionnelle);

				echange = echangeChorusRepository.findById(idEchangeChorus).orElseThrow(()->new EchangeChorusIntrouvableException(idEchangeChorus));
			}

			LOG.info("Validation de l'échange chorus {}", echange);
			echange = this.valider(plateformeUid, acte, echange, dateNotificationPrevisionnelle);

			LOG.info("Envoi de l'échange chorus {}", echange);
			echange = this.envoyer(plateformeUid, acte, echange);

			return echange;
		} else {
			throw new EchangeNonAutoriseException("Un échange chorus est déjà en cours pour le contrat");
		}

	}

	private EchangeChorus revalider(String plateformeUid, final Acte acte, final EchangeChorus echange) throws PublicationChorusException {
		var statut = statutChorusRepository.findByIdExterne(CodeStatutChorus.FLUX_A_GENERER.getIdExterne()).orElseThrow(()->new PublicationChorusException("Le statut FAG n'existe pas"));

		echange.setStatut(statut);
		echange.setContrat(acte.getContrat());

		EchangeChorus sauvegarde;

		try {
			var echangeType = chorusClient.republierEchangeChorus(plateformeUid, acte, echange);

			var echangeId = String.valueOf(echangeType.getId());

			if (null == echangeId) {
				throw new MPEApiException("Aucun identifiant externe en retour");
			}

			LOG.info("Echange CHORUS avec le numéro {} créé avec succès", echangeType.getId());

			echange.setIdExterne(echangeId);
		} catch (final MPEApiException e) {
			LOG.error("Erreur lors de la publication vers chorus {}", echange, e);

			throw new PublicationChorusException("Erreur lors de la republication vers CHORUS");
		} finally {
			LOG.info("Sauvegarde de l'échange validé {}", echange);

			sauvegarde = echangeChorusRepository.save(echange);
		}

		return sauvegarde;
	}

	private EchangeChorus renvoyer(String plateformeUid, final Acte acte, EchangeChorus echange) throws EnvoiZipChorusException {
		var statut = statutChorusRepository.findByIdExterne(CodeStatutChorus.ENVOI_PLANIFIE.getIdExterne()).orElseThrow(()->new EnvoiZipChorusException("Le statut EP n'existe pas"));

		LOG.debug("Actualisation du ZIP CHORUS avec le statut {}", statut);

		echange.setStatut(statut);
		echange.setActe(acte);
		echange.setDateEnvoi(LocalDateTime.now());

		var documents = acte.getDocuments();

		if (documents.isEmpty()) {
			LOG.info("Aucun document a uploader");

			return echange;
		}

		EchangeChorus sauvegarde;

		LOG.info("Upload de {} documents", documents.size());

		File zip = null;

		try {
			zip = construireZip(documents);

			chorusClient.renvoyerZipChorus(plateformeUid, echange, zip);

			echange.setDateEnvoiDocuments(LocalDateTime.now());
		} catch (final MPEApiException | ZipException e) {
			LOG.error("erreur lors de l'envoi du zip vers chorus {}", documents, e);

			throw new EnvoiZipChorusException("Erreur lors du renvoi des documents vers CHORUS");
		} finally {
			if (null!=zip) {
				documentContratService.deleteFile(zip);
			}

			LOG.info("Sauvegarde de l'échange envoyé {}", echange);
			sauvegarde = echangeChorusRepository.save(echange);
		}

		return sauvegarde;
	}

	@Override
	public EchangeChorus ressoumettre(final String plateformeUid, final Long idActe) throws ActeIntrouvableException, PublicationChorusException, EnvoiZipChorusException {
		var acte = acteRepository.findById(idActe).orElseThrow(() -> new ActeIntrouvableException(idActe));
		var echange = acte.getEchangeChorus();

		try {
			LOG.info("Revalidation de l'échange chorus {}", echange);
			echange = this.revalider(plateformeUid, acte, echange);

			LOG.info("Renvoi de l'échange chorus {}", echange);
			echange = this.renvoyer(plateformeUid, acte, echange);

			// Si ok, on passe le statut de l'acte EN_ATTENTE_VALIDATION
			acte.setStatut(StatutActe.EN_ATTENTE_VALIDATION);
		} catch(PublicationChorusException | EnvoiZipChorusException e) {
			LOG.error("Erreur lors de la publication CHORUS", e);

			throw e;
		} finally {
			// Dans tous les cas, on sauvegarde
			acte.setEchangeChorus(echange);
			this.acteRepository.save(acte);
		}

		return echange;
	}

	private File construireZip( List<DocumentActe> documents) throws ZipException {
        try {
            var zip = Files.createTempFile("documents-chorus", ".zip").toFile();
            return remplirZip(documents, zip);
        } catch (Exception e) {
            LOG.error("Erreur lors de la création du fichier temporaire", e);
            throw new ZipException("Erreur lors de la création du zip");
        }
    }

    private File remplirZip( List<DocumentActe> documents, File zipFile) throws ApplicationException {
        try (var out = new ZipOutputStream(new FileOutputStream(zipFile))) {
            for (var document : documents) {
                final var fileOptional = fileStoreManager.getFile(document.getFichier().getNomEnregistre());
                if (fileOptional.isPresent()) {
                    var file = fileOptional.get();
                    var entry = new ZipEntry(document.getFichier().getNom());
                    out.putNextEntry(entry);
                    byte[] data = Files.readAllBytes(file.toPath());
                    out.write(data, 0, data.length);
                    out.closeEntry();
                }
            }
            return zipFile;
        } catch ( IOException e) {
            LOG.error("impossible de générer le zip pour les documents {}", documents);
			throw new ZipException("Erreur lors de la création du zip");

        }
    }
}
