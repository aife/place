package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.AccountDTO;
import com.atexo.execution.server.oauth.grants.AccountCodeAuthorizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/authorization")
@CrossOrigin(origins = "*")
public class AuthorizationController {

	@Autowired
	AccountCodeAuthorizer authorizer;

	@GetMapping(value = "/authorize", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountDTO> authorize(@RequestParam(value = "code") final String code, @RequestParam(value = "plateforme") final String plateforme) {
		return new ResponseEntity<>(authorizer.authorize(plateforme, code), HttpStatus.OK);
	}

}
