package com.atexo.execution.server.common.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component("configurationManager")
public class PathsConfigurationManagerImpl implements PathsConfigurationManager {

	private final static String TMP_DIR = File.separator + "tmp";

	private final static String FILE_STORE_DIR = File.separator + "fileStore";

	private final static String LUCENE_INDEX_DIR = File.separator + "luceneIndex";

	@Value("${application.workDir}")
	String applicationWorkDir;

	String tmpPath;

	String fileStorePath;

	String luceneIndexPath;

	@PostConstruct
	public void init() {

		createFolderIfNotExists(applicationWorkDir);

		tmpPath = applicationWorkDir + TMP_DIR;
		createFolderIfNotExists(tmpPath);

		fileStorePath = applicationWorkDir + FILE_STORE_DIR;
		createFolderIfNotExists(fileStorePath);

		luceneIndexPath = applicationWorkDir + LUCENE_INDEX_DIR;
		createFolderIfNotExists(luceneIndexPath);
	}

	private void createFolderIfNotExists(String path) {
		File tmpPathDir = new File(path);
		if (!tmpPathDir.exists()) {
			tmpPathDir.mkdirs();
		}
	}

	@Override
	public String getApplicationWorkDir() {
		return applicationWorkDir;
	}

	@Override
	public String getFileStorePath() {
		return fileStorePath;
	}

	@Override
	public String getLuceneIndexPath() {
		return luceneIndexPath;
	}

	@Override
	public String getTmpPath() {
		return tmpPath;
	}
}
