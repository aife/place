package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.ContratEtablissementDTO;
import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;
import com.atexo.execution.common.dto.process.attributaire.AttributaireDTO;
import com.atexo.execution.common.dto.process.attributaire.EditionAttributaireDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;

import java.util.List;

public interface AttributaireService {

	AttributaireDTO getArborescenceAttributaires(Long utilisateurId, Long contratId) throws ApplicationBusinessException;

	ContratEtablissementDTO ajouterSousTraitant(Long utilisateurId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
			throws ApplicationBusinessException;

	void supprimerSousTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO) throws ApplicationBusinessException;

	AttributaireDTO transformerEnGroupement(Long organismeId, Long contratId, ContratEtablissementGroupementDTO groupement)
			throws ApplicationBusinessException;

	AttributaireDTO degrouperGroupement(Long organismeId, Long contratId) throws ApplicationBusinessException;

	ContratEtablissementDTO ajouterCoTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
			throws ApplicationBusinessException;

	void supprimerCoTraitant(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO) throws ApplicationBusinessException;

	ContratEtablissementGroupementDTO modifierGroupement(Long organismeId, Long contratId, ContratEtablissementGroupementDTO groupementDTO)
			throws ApplicationBusinessException;

	ContratEtablissementDTO modifierAttributaire(Long organismeId, Long contratId, EditionAttributaireDTO editionAttributaireDTO)
			throws ApplicationBusinessException;

	List<ContratEtablissementDTO> modifierAttributaires(Long organismeId, Long contratId, List<EditionAttributaireDTO> editionAttributaireDTOs)
			throws ApplicationBusinessException;
}
