package com.atexo.execution.server.common.manager.upload;

import java.io.File;

public interface UploadFile {

	String getName();

	long getSize();

	String getMimeType();

	File getFile();

}
