package com.atexo.execution.server.mvc;

import com.atexo.execution.server.model.ParametrageApplication;
import com.atexo.execution.server.service.ParametrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/parametrage")
public class ParametrageController {

    @Autowired
    ParametrageService parametrageService;

    @GetMapping(value = "/findAllExportFront")
    public ResponseEntity<Map<String, String>> findAllValueParametrageFront(Principal principal) {
        var userUUid = principal.getName();
        List<ParametrageApplication> parametrageExportFront = parametrageService.getAllParametrageExportFrontByUtilisateurUuid(userUUid);
        Map<String, String> results = new HashMap<>();
        if (parametrageExportFront != null) {
            for (ParametrageApplication parametrage : parametrageExportFront) {
                results.put(parametrage.getClef(), parametrage.getValeur());
            }
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

}
