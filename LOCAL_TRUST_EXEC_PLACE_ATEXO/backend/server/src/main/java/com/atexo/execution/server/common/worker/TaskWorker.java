package com.atexo.execution.server.common.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Component
public class TaskWorker {
    private static final Logger logger = LoggerFactory.getLogger(TaskWorker.class);

    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    private Map<String, FutureResult<?>> results = new HashMap<>();

    public <T> String executeTask(Task<T> task) {
        String idTask = UUID.randomUUID().toString();
        synchronized (this) {
            task.setIdTask(idTask);
            FutureResult<T> rsemResult = new FutureResult<T>(taskExecutor.submit(task), task.getResultHref());
            results.put(idTask, rsemResult);
        }
        return idTask;
    }

    public <T> T getResult(String idTask) throws Exception {
        if (idTask == null) {
            logger.error("The task id is null");
            throw new Exception("The task id is null");
        }
        if (CollectionUtils.isEmpty(results)) {
            logger.error("La liste des taches est vide");
            throw new Exception("La liste des tâches est vide");
        }


        FutureResult<T> rsemResult = (FutureResult<T>) results.get(idTask);
        if (rsemResult == null)
            throw new Exception("The task was removed for the work id : " + idTask);
        if (rsemResult.isDone())
            return rsemResult.get();
        return null;
    }

    public String getResultHref(String idTask) throws Exception {
        FutureResult<?> rsemResult = results.get(idTask);
        if (rsemResult == null)
            throw new Exception("The task was removed for the work id : " + idTask);
        return rsemResult.getResultHref();
    }

}
