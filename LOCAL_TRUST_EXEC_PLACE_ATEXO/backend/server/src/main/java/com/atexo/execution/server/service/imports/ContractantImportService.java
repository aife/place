package com.atexo.execution.server.service.imports;

/**
 * Service d'import de contractants via fichier XLS.
 */
public interface ContractantImportService extends ImportService {
}
