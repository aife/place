package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.mapper.TemplateReponse;
import com.atexo.execution.server.model.Template;
import com.atexo.execution.server.repository.TemplateRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.messagerie.MessagerieService;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/messagerie")
@Slf4j
public class MessagerieController {

    @Autowired
    MessagerieService messagerieService;

    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    TemplateRepository templateRepository;

    @Value("${messagerie.via.proxypass:false}")
    boolean messagerieViaProxyPass;

    private static final String PROXYPASS = "/execution/";

    @GetMapping(value = "/initTokenRedaction")
    public ResponseEntity<ContexteDTO> initTokenRedaction(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam final Long contratId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contexte = messagerieService.initTokenRedaction(utilisateur.getId(), contratId);
        processContexte(contexte);
        return new ResponseEntity<>(contexte, HttpStatus.OK);
    }

    @GetMapping(value = "/initTokenByCodeLien")
    public ResponseEntity<ContexteDTO> initTokenRedactionByCodeLien(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam final Long contratId, @RequestParam("codeLien") final String codeLien) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contexte = messagerieService.initTokenRedactionByCodeLien(utilisateur.getId(), contratId, codeLien);
        processContexte(contexte);
        return new ResponseEntity<>(contexte, HttpStatus.OK);
    }


    @GetMapping(value = "/initTokenNotification")
    public ResponseEntity<ContexteDTO> initTokenNotification(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam final Long contratId, @RequestParam(required = false) final Long acteId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contexte = new ContexteDTO();
        String idTemplate = null;
        if (acteId != null) {
            log.info("initTokenNotification : acteId = {}", acteId);
            contexte = messagerieService.initTokenNotification(utilisateur.getId(), contratId, acteId);
            idTemplate = Optional.ofNullable(messagerieService.getIdTemplateByCode(Template.NOTIFICATION_ACTE)).map(String::valueOf).orElse(null);

        } else {
            log.info("initTokenNotification : notification du contrat {}", contratId);
            contexte = messagerieService.initTokenNotification(utilisateur.getId(), contratId);
            idTemplate = Optional.ofNullable(messagerieService.getIdTemplateByCode(Template.NOTIFICATION_CONTRAT)).map(String::valueOf).orElse(null);
        }
        contexte.setIdTemplateSelectionne(idTemplate);
        processContexte(contexte);
        return new ResponseEntity<>(contexte, HttpStatus.OK);
    }

    @GetMapping(value = "/initTokenSuivi")
    public ResponseEntity<ContexteDTO> initTokenSuivi(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam final Long contratId, @RequestParam(required = false) final Long acteId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contexte = messagerieService.initTokenSuivi(utilisateur.getId(), contratId, acteId);
        processContexte(contexte);
        return new ResponseEntity<>(contexte, HttpStatus.OK);
    }


    @PostMapping(value = "/uploadDocuments")
    public ResponseEntity<String[]> uploadDocments(
	    @RequestHeader("User-Uuid") final String utilisateurUuid,
    	@RequestParam final String token,
	    @RequestParam final Long[] documentIds
    ) throws ApplicationException {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

	    final String[] references = messagerieService.uploadDocuments(utilisateur.getOrganismeId(), token, documentIds);
        return new ResponseEntity<>(references, HttpStatus.OK);
    }

    @PostMapping(value = "/addPieceJointeToDocuments")
    public ResponseEntity<DocumentContratDTO> addPieceJointeToDocuments(
	    @RequestHeader("User-Uuid") final String utilisateurUuid,
	    @RequestParam final Long contratId,
	    @RequestParam(required = false) final Long etablissementId,
	    @RequestParam final String objet,
	    @RequestParam final String codeLien,
	    @RequestParam final String pieceJointeId
    ) throws ApplicationException {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

	    return new ResponseEntity<>(messagerieService.addPieceJointeToDocuments(utilisateur.getId(), contratId, etablissementId, objet, codeLien,
                pieceJointeId), HttpStatus.OK);
    }

    @PostMapping(value = "/countMessageStatusContrats", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContratMessageStatusDTO>> countMessageStatusContrats(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final List<String> idContrats) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        final List<ContratMessageStatusDTO> messageStatus = messagerieService.countMessageStatusByContrat(utilisateur.getOrganismeId(), idContrats);
        return new ResponseEntity<>(messageStatus, HttpStatus.OK);
    }

    @GetMapping(value = "/templates")
    public ResponseEntity<TemplateReponse> getTemplates() {
        return ResponseEntity.ok(messagerieService.getTemplates());
    }

    private void processContexte(ContexteDTO contexte) {
        if (messagerieViaProxyPass) {
            contexte.setUrlNouveauMessage(PROXYPASS + contexte.getUrlNouveauMessage());
            contexte.setUrlModificationMessage(PROXYPASS + contexte.getUrlModificationMessage());
            contexte.setUrlRetourConsultation(PROXYPASS + contexte.getUrlRetourConsultation());
            contexte.setUrlSuivi(PROXYPASS + contexte.getUrlSuivi());
            contexte.setTemplatesPath("/exec-api/api/messagerie/templates");
        }
    }
}
