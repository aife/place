package com.atexo.execution.server.service.spaser;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;

public interface SpaserService {

    String initContexte(UtilisateurDTO utilisateur, Long idContrat) throws ApplicationTechnicalException, ApplicationBusinessException;
}
