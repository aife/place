package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;

public class PublicationChorusException extends ApplicationBusinessException {

	public PublicationChorusException(String message) {
		super(message);
	}
}
