package com.atexo.execution.server.service.organisme;

import com.atexo.execution.common.dto.OrganismeDTO;

import java.util.List;
import java.util.Optional;

public interface OrganismeService {

    Optional<OrganismeDTO> find(final Long organismeId);

    List<OrganismeDTO> getOrganismes(Long organismeId);

    List<OrganismeDTO> getOrganismeList(String plateformeUid);
}
