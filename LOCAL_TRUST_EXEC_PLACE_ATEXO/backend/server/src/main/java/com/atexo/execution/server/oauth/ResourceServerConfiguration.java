package com.atexo.execution.server.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource-server-rest-api";
    private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    private static final String ALL = "/**";
    private static final String[] WHITE_LIST = new String[]{"/api/authorization/**", "/api/synchronisation/**", "/api/actes/notification/**", "/api/contrat/notification/**", "/api/contrats/notification/**", "/api/contrat/update-status", "/api/messagerie/templates/**", "/api/reindexation/**", "/monitoring", "/actuator/**"};


    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(WHITE_LIST)
                .permitAll()
                .and()
                .requestMatchers()
                .antMatchers(ALL).and().authorizeRequests()
                .anyRequest().access(SECURED_READ_SCOPE);
    }
}
