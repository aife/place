package com.atexo.execution.server.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("applicationAuthenticationProvider")
@PropertySource(value = {"classpath:config-env.properties"}, encoding = "UTF-8")
public class ApplicationAuthenticationProvider implements AuthenticationProvider {

    static final Logger LOG = LoggerFactory.getLogger(ApplicationAuthenticationProvider.class);


    @Value("${authentification.username:root}")
    String username;

    @Value("${authentification.password:root}")
    String password;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String identifiant = authentication.getName();

        String motpasse = authentication.getCredentials().toString();

        if (identifiant == null || password == null) {
            throw new BadCredentialsException("");
        }

        if (!username.equals(identifiant) && !motpasse.equals(password)) {
            throw new UsernameNotFoundException("Utilisateur Inconnu");
        }

        List<GrantedAuthority> grantedAuths = new ArrayList<>();
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

        return new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(OAuth2Authentication.class)
                || authentication.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
    }
}
