package com.atexo.execution.server.common;


import com.atexo.execution.common.exception.ApplicationBusinessException;

public class Preconditions {

	public static void checkArgument(boolean expression) throws ApplicationBusinessException {
		if (!expression) {
			throw new ApplicationBusinessException();
		}
	}

	public static void checkArgument(boolean expression, String messagekey) throws ApplicationBusinessException {
		if (!expression) {
			throw new ApplicationBusinessException();
		}
	}

	public static void checkArgument(boolean expression, String messagekey, Object[] args) throws ApplicationBusinessException {
		if (!expression) {
			throw new ApplicationBusinessException();
		}
	}

	public static boolean isSiren(String siren){
		int total = 0;
		int digit = 0;

		for (int i = 0; i<siren.length(); i++) {
			/** Recherche les positions paires : 2ème, 4ème, 6ème et 8ème chiffre que l'on multiplie par 2


			 petite différence avec la définition ci-dessus car ici on travail de gauche à droite */

			if ((i % 2) == 1) {
				digit = Integer.parseInt(String.valueOf(siren.charAt(i))) * 2;
				/** si le résultat est >9 alors il est composé de deux digits tous les digits devant


				 s'additionner et ne pouvant être >19 le calcule devient : 1 + (digit -10) ou : digit - 9 */

				if (digit > 9) digit -= 9;
			}
			else digit = Integer.parseInt(String.valueOf(siren.charAt(i)));
			total += digit;
		}

		/** Si la somme est un multiple de 10 alors le SIREN est valide */
		return (total % 10) == 0;
	}
}
