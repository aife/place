package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.worker.ContratsExportTaskFactory;
import com.atexo.execution.server.common.worker.Task;
import com.atexo.execution.server.common.worker.TaskWorker;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.calendrier.CalendrierService;
import com.atexo.execution.server.service.export.ExportService;
import com.atexo.execution.services.ContratsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

@RestController
@RequestMapping(value = "/api/contrat")
public class ContratController {

    private static final Logger LOG = LoggerFactory.getLogger(ContratController.class);

    @Autowired
    ContratsService contratService;

    @Autowired
    CalendrierService calendrierService;

    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    ExportService exportService;

    @Autowired
    MpeClient mpeClient;

    @Autowired
    TaskWorker taskWorker;

    @Autowired
    ContratsExportTaskFactory contratsExportTaskFactory;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ContratDTO>> find(
            @RequestHeader("User-Uuid") final String utilisateurUuid,
            @RequestBody final ContratCriteriaDTO criteriaDTO,
            final Pageable pageable
    ) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return new ResponseEntity<>(contratService.rechercher(utilisateur.getId(), criteriaDTO, pageable), HttpStatus.OK);
    }

    @PostMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
    public String export(@RequestHeader("User-Uuid") final String utilisateurUuid, final @RequestBody ContratCriteriaDTO criteriaDTO) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        LOG.info("Export des contrats de l'organisme {} / plateforme {}", utilisateur.getOrganismeId(), utilisateur.getPlateformeUid());
        Task<File> task = contratsExportTaskFactory.newExportContratsTask(utilisateurUuid, criteriaDTO);
        String idTask = taskWorker.executeTask(task);
        LOG.info("Tache {} est lancée", idTask);
        return idTask;
    }

    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ContratDTO>> find(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam(required = false) final String codeAchat, @RequestParam(required = false) final String ufop,
                                                 @PageableDefault(size = Integer.MAX_VALUE) final Pageable pageable) {

        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        final Page<ContratDTO> page = contratService.rechercher(utilisateur.getId(), codeAchat, ufop, pageable);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping(value = "/filtrer/{idExterne}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContratDTO> getByIdexterne(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("idExterne") final String idExterne) throws ApplicationBusinessException {
        LOG.info("Recherche du contrat avec identifiant externe {}", idExterne);

        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return contratService.getContratByIdExterne(utilisateur.getId(), idExterne)
                .map(contratDTO -> {
                    LOG.info("Le contrat avec identifiant externe {} a été trouvé en base pour l'organisme {}", idExterne, utilisateur.getOrganismeId());

                    return new ResponseEntity<>(contratDTO, HttpStatus.OK);
                })
                .orElseThrow(ApplicationBusinessException::new);
    }


    @GetMapping(value = "/{contratId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContratDTO> getById(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contratId") final Long contratId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(contratService.getContratById(utilisateur.getId(), contratId), HttpStatus.OK);
    }

    @GetMapping(value = "/{contratId}/enfants", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContratDTO>> getEnfantByIdParent(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contratId") final Long contratId) throws ApplicationTechnicalException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(contratService.getContratsEnfantsByIdParent(utilisateur.getId(), contratId), HttpStatus.OK);
    }

    @GetMapping(value = "/{contrat}/etablissements", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContratEtablissementDTO> getEtablissementsById(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contrat") final Long contratId) {
        return contratService.getEtablissementsById(contratId);
    }

    @GetMapping(value = "/{contrat}/documents", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentContratDTO> getDocumentsById(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contrat") final Long contratId) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);


        return contratService.getDocumentsById(utilisateur.getId(), contratId);
    }

    @GetMapping(value = "/evenement/{evenement}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContratEtablissementDTO>> getByEvenement(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("evenement") final Long evenementId) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return new ResponseEntity<>(contratService.getContratByEvenement(utilisateur.getId(), evenementId), HttpStatus.OK);
    }

    @PostMapping(value = "/updateDonneesPrincipales", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContratDTO> updateDonneesPrincipales(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ContratDTO contrat) throws ApplicationException {
        LOG.debug("Mise a jour du contrat {} pour l'utilisateur {}", contrat.getNumero(), utilisateurUuid);
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        final ContratDTO result = this.contratService.createOrUpdateContrat(contrat, utilisateur.getUuid());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = "toggleFavori/{contrat}")
    public void fav(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contrat") final Long contratId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        contratService.favorite(utilisateur.getId(), contratId);
    }

    @PostMapping(value = "/ajout", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('CREER_CONTRAT')")
    public ResponseEntity<ContratDTO> ajouterContrat(@RequestHeader("User-Uuid") final String utilisateurId, @RequestBody final ContratDTO contratDTO) throws ApplicationException {
        LOG.debug("Ajout d'un contrat pour l'utilisateur {}", utilisateurId);

        return new ResponseEntity<>(contratService.createOrUpdateContrat(contratDTO, utilisateurId), HttpStatus.OK);
    }

    @PostMapping(value = "/notification/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContratDTO> notification(
            @PathVariable(name = "uuid", required = false) final String uuid
    ) throws ApplicationException {

        var contratNotifie = this.contratService.notifier(uuid, null);

        return new ResponseEntity<>(contratNotifie, HttpStatus.OK);
    }

    @PostMapping(value = "/notification-contrat/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContratDTO> notificationContrat(
            @RequestHeader("User-Uuid") final String utilisateurUuid,
            @PathVariable("id") final Long id
    ) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        ContratDTO contratNotifie = this.contratService.notifierContrat(utilisateur.getId(), id);

        return new ResponseEntity<>(contratNotifie, HttpStatus.OK);
    }

    @PostMapping(value = "/marche-subsequent/format-url/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValueLabelDTO> formatUrlCreationMarcheSubsequent(
            @RequestHeader("User-Uuid") final String utilisateurUuid,
            @PathVariable(name = "id", required = false) final Long id
    ) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var contrat = contratService.getContratById(utilisateur.getId(), id);
        if (!contrat.isChapeau() && contrat.getIdContratChapeau() != null) {
            contrat = contratService.getContratById(utilisateur.getId(), contrat.getIdContratChapeau());
        }
        if (contrat.getUuid() == null) {
            throw new ApplicationException("impossible de générer l'url pour un contrat non synchronisé depuis MPE " + id);
        }
        var url = mpeClient.formatUrlCreationMarcheSubsequent(contrat.getUuid());
        var dto = new ValueLabelDTO();
        dto.setLabel("lien marché subséquent");
        dto.setValue(url);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/echange-chorus/format-url/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValueLabelDTO> formatUrlEchangerChorus(
            @RequestHeader("User-Uuid") final String utilisateurUuid,
            @PathVariable(name = "id", required = false) final Long id
    ) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        final var contrat = contratService.getContratById(utilisateur.getId(), id);
        var idExterne = contrat.getIdExterne();
        if (idExterne == null || contrat.isExNihilo()) {
            idExterne = null;
            LOG.warn("ID MPE indisponible pour le contrat {}, on envoie id vide + uuid du contrat {}", id, contrat.getUuid());
        }
        var url = mpeClient.formatUrlEchangeChorus(idExterne) + "&uuid=" + contrat.getUuid();
        var dto = new ValueLabelDTO();
        dto.setLabel("lien echange chorus");
        dto.setValue(url);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{contratId}")
    @PreAuthorize("hasAuthority('SUPPRIMER_CONTRAT')")
    public ResponseEntity<String> deleteContrat(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("contratId") final Long contratId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        LOG.debug("Suppression du contrat {} pour l'utilisateur {}", contratId, utilisateurUuid);
        contratService.deleteContrat(utilisateur.getService().getOrganisme(), contratId);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @GetMapping(value = "/{id}/contrats-lies", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContratDTO>> getContratsLies(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("id") final Long contratId) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return new ResponseEntity<>(contratService.getContratsLies(utilisateur.getId(), contratId), HttpStatus.OK);
    }

    @PostMapping(value = "/update-status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> addContratsLies() {

        return new ResponseEntity<>(contratService.updateAllStatus(), HttpStatus.OK);
    }

}
