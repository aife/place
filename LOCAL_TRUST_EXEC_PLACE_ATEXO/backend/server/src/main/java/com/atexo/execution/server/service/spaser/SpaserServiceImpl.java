package com.atexo.execution.server.service.spaser;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.clients.oauth2.Oauth2Client;
import com.atexo.execution.server.interf.spaser.SpaserInterface;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SpaserServiceImpl implements SpaserService {


    @Value("${keycloak.enabled:false}")
    boolean keycloakEnabled;

    @Value("${keycloak.auth-server-url}")
    String keycloakUrl;

    @Autowired
    Oauth2Client oauth2Client;

    @Value("${spaser.realm:spaser}")
    private String spaserRealm;

    @Value("${spaser.resource:spaser-api}")
    private String spaserResource;

    @Value("${spaser.username:exec}")
    private String username;

    @Value("${spaser.password:exec}")
    private String password;

    @Autowired
    SpaserInterface spaserInterface;


    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    ContratRepository contratRepository;


    @Override
    public String initContexte(UtilisateurDTO utilisateur, Long idContrat) throws ApplicationBusinessException {
        var contrat = contratRepository.findById(idContrat).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        var accessToken = getAccessToken();
        if (accessToken == null) {
            throw new IllegalArgumentException("Erreur de connexion d'authentification");
        }

        return spaserInterface.initContext(accessToken, utilisateur, contrat);
    }


    private Oauth2TokenDTO getAccessToken() {
        if (keycloakEnabled) {
            return oauth2Client.getOauth2TokenDTO(keycloakUrl, spaserRealm, spaserResource, username, password);
        }
        return null;
    }
}
