package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;

import java.util.List;
import java.util.Optional;

public interface EtablissementService {

	Optional<EtablissementDTO> findById(Long Id);

	Optional<EtablissementDTO> findEtablissementBySiret(String siret);

	EtablissementDTO ajouterEtablissement(EtablissementDTO etablissement) throws ApplicationBusinessException;

	EtablissementDTO modifierEtablissement(EtablissementDTO etablissementDTO) throws ApplicationBusinessException;

	EtablissementDTO ajouterEtablissementEtranger(EtablissementDTO etablissement) throws ApplicationBusinessException;

	List<EtablissementDTO> findEtablissementBymotCles(String motsCles);

	void supprimerEtablissement(Long idEtablissement) throws ApplicationBusinessException;

	EtablissementDTO saveOrUpdate(String siret);

	List<EtablissementDTO> findEtablissementsByUUIDFournisseur(String uuid) throws ApplicationBusinessException;
}
