package com.atexo.execution.server.service;

import com.atexo.execution.common.def.TypeGroupement;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.server.common.mapper.ReferentielMapper;
import com.atexo.execution.server.model.AbstractReferentielEntity;
import com.atexo.execution.server.model.TypeActe;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.repository.crud.DocumentModeleRepository;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.*;
import com.atexo.execution.server.repository.referentiels.clause.ClauseN1Repository;
import com.atexo.execution.server.repository.referentiels.clause.ClauseN2Repository;
import com.atexo.execution.server.repository.referentiels.clause.ClauseN3Repository;
import com.atexo.execution.server.repository.referentiels.clause.ClauseN4Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atexo.execution.common.def.ReferentielDiscriminators.*;
import static com.atexo.execution.server.model.ParametrageApplication.CLE_ACTES;

@Service
@Transactional
@Slf4j
public class ReferentielServiceImpl implements ReferentielService {

    @Autowired
    TypeContratRepository typeContratRepository;
    @Autowired
    OrganismeRepository organismeRepository;

    @Autowired
    TypeEvenementRepository typeEvenementRepository;

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    CategorieConsultationRepository categorieConsultationRepository;

    @Autowired
    ReferentielMapper referentielMapper;

    @Autowired
    FormeJuridiqueRepository formeJuridiqueRepository;

    @Autowired
    CodeApeNafRepository codeApeNafRepository;

    @Autowired
    PaysRepository paysRepository;

    @Autowired
    TypeActeRepository typeActeRepository;

    @Autowired
    CodeExterneActeRepository codeExterneActeRepository;

    @Autowired
    DocumentModeleRepository documentModeleRepository;

    @Autowired
    TypeAvenantRepository typeAvenantRepository;

    @Autowired
    TauxTVARepository tauxTVARepository;

    @Autowired
    CCAGReferenceRepository ccagReferenceRepository;

    @Autowired
    ProcedureRepository procedureRepository;

    @Autowired
    ModaliteExecutionRepository modaliteExecutionRepository;

    @Autowired
    TechniqueAchatRepository techniqueAchatRepository;

    @Autowired
    TypeGroupementRepository typeGroupementRepository;

    @Autowired
    FormePrixRepository formePrixRepository;

    @Autowired
    TypePrixRepository typePrixRepository;

    @Autowired
    RevisionPrixRepository revisionPrixRepository;

    @Autowired
    private ClauseN1Repository clauseN1Repository;

    @Autowired
    private ClauseN2Repository clauseN2Repository;

    @Autowired
    private ClauseN3Repository clauseN3Repository;

    @Autowired
    private ClauseN4Repository clauseN4Repository;

    @Autowired
    private CategorieFournisseurRepository categorieFournisseurRepository;

    @Autowired
    private LieuExecutionRepository lieuExecutionRepository;

    @Autowired
    private ParametrageService parametrageService;
    @Autowired
    private NatureContratConcessionRepository natureContratConcessionRepository;


    private final Function<AbstractReferentielEntity, ValueLabelDTO> referentielToValueLabelDTO = (e) -> {
        if (e != null) {
            return referentielMapper.toDTO(e);
        }
        throw new IllegalArgumentException("Pas de mapper implémenté pour la classe " + e.getClass().getSimpleName());
    };
    private final BiFunction<TypeContrat, Long, ValueLabelDTO> typeContratToValueLabelDTO = (e, idOrganisme) -> {
        if (e != null) {
            ValueLabelDTO dto = referentielMapper.toDTO(e);
            boolean entiteEligibleActif = organismeRepository.existsByIdAndTypeContratsId(idOrganisme, e.getId());
            dto.setEntiteEligibleActif(entiteEligibleActif);

            return dto;
        }
        throw new IllegalArgumentException("Pas de mapper implémenté pour la classe " + e.getClass().getSimpleName());
    };

    @Autowired
    SurchargeLibelleRepository surchargeLibellesRepository;


    @Override
    public List<ValueLabelDTO> getTypeGroupementList() {
        return Arrays.stream(TypeGroupement.values()).map(typeGroupement -> referentielMapper.toDTO(typeGroupement)).collect(Collectors.toList());
    }

    @Override
    public List<ValueLabelDTO> getReferentiel(String plateformeUid, Long organismeId, String typeReferentiel) {
        switch (typeReferentiel) {
            case CATEGORIE_CONSULTATION:
                return categorieConsultationRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CCAG_REFERENCE:
                return ccagReferenceRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case PAYS:
                return paysRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_CONTRAT:
                return typeContratRepository.findByActif(true).stream().map(typeContrat -> typeContratToValueLabelDTO.apply(typeContrat, organismeId)).sorted(Comparator.comparingLong(ValueLabelDTO::getOrdre)).collect(Collectors.toList());
            case FORME_JURIDIQUE:
                return formeJuridiqueRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CODE_APE_NAF:
                return codeApeNafRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_ACTE:
                var actesActif = parametrageService.isActif(plateformeUid, CLE_ACTES);
                log.info("actesActif: {} pour la plateforme {}", actesActif, plateformeUid);
                if (actesActif)
                    return typeActeRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
                else
                    return typeActeRepository.findByActif(true).stream().filter(ta -> (TypeActe.CODE_ACTE_MODIFICATIF.equals(ta.getCode()) || TypeActe.CODE_AGREMENT_SOUS_TRAITANCE.equals(ta.getCode()))).map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CODE_EXTERNE_ACTE:
                return codeExterneActeRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_AVENANT:
                return typeAvenantRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TAUX_TVA:
                return tauxTVARepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case SURCHARGE_LIBELLE:
                return surchargeLibellesRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case PROCEDURE:
                return procedureRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case MODALITE_EXECUTION:
                return modaliteExecutionRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TECHNIQUE_ACHAT:
                return techniqueAchatRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_GROUPEMENT:
                return typeGroupementRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case FORME_PRIX:
                return formePrixRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_PRIX:
                return typePrixRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case REVISION_PRIX:
                return revisionPrixRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case TYPE_EVENEMENT:
                return typeEvenementRepository.findByActif(true).stream().filter(te -> !te.getCode().startsWith("AVENANT_")).map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CLAUSE_N1:
                return clauseN1Repository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CLAUSE_N2:
                return clauseN2Repository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CLAUSE_N3:
                return clauseN3Repository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CLAUSE_N4:
                return clauseN4Repository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CLAUSE_N2N3:
                return clauseN3Repository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case CATEGORIE_FOURNISSEUR:
                return categorieFournisseurRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case LIEU_EXECUTION:
                return lieuExecutionRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            case NATURE_CONTRAT_CONCESSION:
                return natureContratConcessionRepository.findByActif(true).stream().map(referentielToValueLabelDTO).collect(Collectors.toList());
            default:
                throw new IllegalArgumentException("Le type de référentiel est inconnu: " + typeReferentiel);
        }

    }
}
