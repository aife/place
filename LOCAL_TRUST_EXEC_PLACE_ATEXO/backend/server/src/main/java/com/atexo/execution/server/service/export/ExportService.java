package com.atexo.execution.server.service.export;

import com.atexo.execution.common.dto.ContratCriteriaDTO;

import java.io.File;
import java.io.IOException;

public interface ExportService {
    File export(String utilisateurUuid, ContratCriteriaDTO criteriaDTO) throws IOException;
}
