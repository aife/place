package com.atexo.execution.server.mvc;

import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.manager.upload.UploadFile;
import com.atexo.execution.server.common.manager.upload.UploadManager;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/upload")
@RequiredArgsConstructor
public class UploadController {

    final UploadManager uploadManager;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> uploadFile(@RequestParam("uploadfile") MultipartFile file) throws ApplicationTechnicalException {
        String reference = null;
        try {
            if (!file.isEmpty()) {
                reference = uploadManager.saveFile(file.getOriginalFilename(), file.getSize(), file.getContentType(),
                        file.getInputStream());
            }
        } catch (Exception e) {
            throw new ApplicationTechnicalException(e);
        }
        return new ResponseEntity<>("\"" + reference + "\"", HttpStatus.OK);
    }

    @GetMapping(value = "/download/{reference}")
    public ResponseEntity<InputStreamResource> downloadUploadedFile(@PathVariable String reference) throws ApplicationTechnicalException {
        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);

        if (!fileOpt.isPresent()) {
            throw new ApplicationTechnicalException();
        }

        UploadFile uploadFile = fileOpt.get();
        FileInputStream fis = null; // sera fermé par spring mvc
        try {
            fis = new FileInputStream(uploadFile.getFile());
        } catch (Exception e) {
            return new ResponseEntity<>((InputStreamResource) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(uploadFile.getFile().length());
        headers.setContentType(MediaType.parseMediaType(uploadFile.getMimeType()));
        headers.setContentDispositionFormData(uploadFile.getName(), uploadFile.getName());

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);

    }

    @DeleteMapping(value = "/delete/{reference}")
    public ResponseEntity<String> deleteUploadedFile(@PathVariable String reference) throws ApplicationTechnicalException {
        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);

        if ( fileOpt.isEmpty() ) {
            throw new ApplicationTechnicalException();
        }

        uploadManager.removeFile(reference);
        return new ResponseEntity<>((String) null, HttpStatus.OK);
    }
}
