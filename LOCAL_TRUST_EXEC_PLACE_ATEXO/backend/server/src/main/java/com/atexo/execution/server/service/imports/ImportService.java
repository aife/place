package com.atexo.execution.server.service.imports;

import com.atexo.execution.common.dto.PairDTO;

import java.io.File;
import java.util.List;

public interface ImportService {

    PairDTO<Integer, List<String>> importFichier(File fichier, boolean testOnly, String pfUid);
}
