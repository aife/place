package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.ContactPrincipalDTO;
import com.atexo.execution.common.dto.ContactReferantDTO;
import com.atexo.execution.common.dto.EditionContactDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.service.ContactService;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.services.ContratsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/contact")
public class ContactController {

    @Autowired
    ContactService contactService;

    @Autowired
    ContratsService contratsService;

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping(value = "/findByIdIn", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContactDTO>> findByIdIn(@RequestParam final Long[] id) {
        return new ResponseEntity<>(contactService.getContactByIdIn(id), HttpStatus.OK);
    }

    @PostMapping(value = "/ajouterContact", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> ajouterContact(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final EditionContactDTO contact) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        final ContactDTO contactDTO = contactService.ajouterContact(contact, utilisateur.getPlateformeUid());

        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/modifierContact", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> modifierContact(@RequestBody final EditionContactDTO contact) throws ApplicationBusinessException {

        final ContactDTO contactDTO = contactService.modifierContact(contact);

        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{contact}/supprimerContact", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> supprimerContact(@PathVariable("contact") final Long contactId) throws ApplicationBusinessException {

        final ContactDTO contactDTO = contactService.supprimerContact(contactId);

        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/findByContratEtablissement/{contratEtablissementId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContactReferantDTO>> findContactByContratEtablissement(@PathVariable Long contratEtablissementId) {
        return new ResponseEntity<>(contactService.getContactByContratEtablissement(contratEtablissementId), HttpStatus.OK);
    }

    @PostMapping(value = "/ModifierContactReferant", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactReferantDTO> modifierContactReferant(@RequestBody final ContactReferantDTO contactReferant) throws ApplicationBusinessException {
        final ContactReferantDTO contactReferantDTO = contactService.modifierContactReferant(contactReferant);
        return new ResponseEntity<>(contactReferantDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/supprimerContact/{contratEtablissementId}/{contactId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> supprimerContactReferant(@PathVariable final Long contratEtablissementId, @PathVariable final Long contactId) {
        final var response = contactService.supprimerContactReferant(contratEtablissementId, contactId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/ajouterContactReferant", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactReferantDTO> ajouterContactReferant(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ContactReferantDTO contactReferant) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        final ContactReferantDTO contactReferantDTO = contactService.ajouterContactReferant(contactReferant,  utilisateur.getPlateformeUid());
        return new ResponseEntity<>(contactReferantDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/findByEtablissement/{etablissementId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContactDTO>> findByContratEtablissement(@PathVariable Long etablissementId) {
        return new ResponseEntity<>(contactService.getContactsByEtablissement(etablissementId), HttpStatus.OK);
    }

    @PatchMapping(value = "/principal", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactPrincipalDTO> modifier(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ContactPrincipalDTO contactPrincipal) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(contactService.modifier(contactPrincipal,  utilisateur.getPlateformeUid()), HttpStatus.OK);
    }

    @PostMapping(value = "/principal", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactPrincipalDTO> ajouter(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ContactPrincipalDTO contactPrincipal) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(contactService.ajouter(contactPrincipal,  utilisateur.getPlateformeUid()), HttpStatus.OK);
    }

    @GetMapping(value = "/principal", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactPrincipalDTO> recupererContactPrincipal(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam("attributaire") final Long attributaireId) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(contactService.recuperer(attributaireId,  utilisateur.getPlateformeUid()), HttpStatus.OK);
    }

}
