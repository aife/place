package com.atexo.execution.server.oauth.grants.exceptions;

public class InvalideCodeException extends RuntimeException {

	public InvalideCodeException( String code ) {
		super(String.format("Le code %s est invalide. Impossible de récupérer l'utilisateur.", code));
	}
}
