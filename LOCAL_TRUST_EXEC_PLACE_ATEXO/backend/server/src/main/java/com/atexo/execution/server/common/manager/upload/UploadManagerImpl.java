package com.atexo.execution.server.common.manager.upload;

import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.manager.PathsConfigurationManager;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UploadManagerImpl implements UploadManager, RemovalListener<String, UploadFile> {

	final static Logger LOG = LoggerFactory.getLogger(UploadManagerImpl.class);

	@Autowired
	PathsConfigurationManager pathsConfigurationManager;

	Cache<String, UploadFile> cache = CacheBuilder.newBuilder().maximumSize(100).expireAfterAccess(1, TimeUnit.HOURS)
			.removalListener(this).build();

	public UploadManagerImpl() {

	}

	@Override
	public Optional<UploadFile> getFile(String reference) {
		UploadFile uploadFile = cache.getIfPresent(reference);
		if (uploadFile == null) {
			return Optional.empty();
		}
		return Optional.of(uploadFile);
	}

	@Override
	public String saveFile(String name, long size, String mimeType, InputStream inputStream) throws ApplicationTechnicalException {
		String reference = UUID.randomUUID().toString();
		String ext = ".tmp";
		if("application/vnd.ms-excel".equals(mimeType)){
			ext =".xls";
		}
		if("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(mimeType)){
			ext = ".xlsx";
		}
		File file = new File(pathsConfigurationManager.getTmpPath() + File.separator + "UPLOAD_" + reference + ext);
		try (FileOutputStream fos = new FileOutputStream(file)) {
			IOUtils.copyLarge(inputStream, fos);
			BasicUploadFile uploadFile = new BasicUploadFile(name, size, mimeType, file);
			cache.put(reference, uploadFile);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new ApplicationTechnicalException(e);
		}
		return reference;
	}

	@Override
	public void removeFile(String reference) {
		cache.invalidate(reference);
	}

	@Override
	public void onRemoval(RemovalNotification<String, UploadFile> notification) {
		File file = notification.getValue().getFile();
		if (file.exists()) {
			FileUtils.deleteQuietly(file);
			LOG.debug(MessageFormat.format("Le fichier {0} est supprimé du cache des uploads", file.getName()));
		}
	}

	@Override
	public long getSaveddFilesCount() {
		return cache.size();
	}

	@Override
	public List<String> getSavedFilesInfos() {
		return cache.asMap().values().stream().map(Object::toString).collect(Collectors.toList());
	}

	private static class BasicUploadFile implements UploadFile {
		String name;
		long size;
		String mimeType;
		File file;

		public BasicUploadFile(String name, long size, String mimeType, File file) {
			super();
			this.name = name;
			this.size = size;
			this.mimeType = mimeType;
			this.file = file;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public long getSize() {
			return size;
		}

		@Override
		public String getMimeType() {
			return mimeType;
		}

		@Override
		public File getFile() {
			return file;
		}

		@Override
		public String toString() {
			return "[name=" + name + ", size=" + size + ", mimeType=" + mimeType + ", file=" + file + "]";
		}

	}
}
