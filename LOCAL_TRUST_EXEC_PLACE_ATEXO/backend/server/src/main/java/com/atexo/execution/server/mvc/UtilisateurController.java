package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.server.service.UtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/utilisateurs")
public class UtilisateurController {

    private static final Logger LOG = LoggerFactory.getLogger(UtilisateurController.class);

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping(value = "/contexte/{pfUid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UtilisateurDTO> getUtilisateurConnecte(OAuth2Authentication authentication, @PathVariable String pfUid) {
        var uuid = authentication.getName();
        var authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        LOG.info("Liste des habilitations de {} = {}", uuid, authorities);
        return new ResponseEntity<>(utilisateurService.getUtilisateurByUuidAndPlateforme(pfUid, uuid, authorities), HttpStatus.OK);
    }


    @GetMapping(value = "/service/{serviceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UtilisateurDTO>> filter(
	    @RequestHeader("User-Uuid") final String utilisateurUuid,
	    @PathVariable Long serviceId
    ) {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        return new ResponseEntity<>(utilisateurService.getUtilisateursByOrganismeAndService(utilisateur.getOrganismeId(), serviceId), HttpStatus.OK);
    }

}
