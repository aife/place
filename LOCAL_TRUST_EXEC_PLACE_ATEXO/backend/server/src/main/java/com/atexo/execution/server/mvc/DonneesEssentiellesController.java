package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.DonneesEssentiellesCriteriaDTO;
import com.atexo.execution.common.dto.DonneesEssentiellesDTO;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.services.DonneesEssentiellesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/donnees-essentielles")
public class DonneesEssentiellesController {

    @Autowired
    UtilisateurService utilisateurService;
    @Autowired
    DonneesEssentiellesService donneesEssentiellesService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<DonneesEssentiellesDTO>> find(@RequestHeader("User-Uuid") final String utilisateurUuid,
                                                             @RequestBody DonneesEssentiellesCriteriaDTO donneesEssentiellesCriteriaDTO,
                                                             final Pageable pageable) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(donneesEssentiellesService.findDonneesEssentielles(utilisateur.getId(), donneesEssentiellesCriteriaDTO, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/update-statut", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> update(@RequestHeader("User-Uuid") final String utilisateurUuid,
                                          @RequestParam("idsDonneesEssentielles") List<Long> idsDonneesEssentielles) {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(donneesEssentiellesService.updateStaut(idsDonneesEssentielles), HttpStatus.OK);
    }

    @GetMapping(value = "/telecharger/{idDonneesEssentielles}")
    public ResponseEntity<byte[]> telechargerFichierJson(@RequestHeader("User-Uuid") final String utilisateurUuid,
                                                         @PathVariable Long idDonneesEssentielles) {
        var optDe = donneesEssentiellesService.telechargerFichier(idDonneesEssentielles);
        if (optDe.isPresent()) {
            byte[] contentBytes = optDe.get().getJsonFile().getBytes();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDispositionFormData("filename", "contenu_telecharge.json");
            return ResponseEntity.ok()
                    .headers(headers)
                    .body(contentBytes);
        }
        return ResponseEntity.noContent().build();
    }

}
