package com.atexo.execution.server.service.organisme;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.server.common.mapper.OrganismeMapper;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrganismeServiceImpl implements OrganismeService {

    @Autowired
    OrganismeRepository organismeRepository;

    @Autowired
    OrganismeMapper organismeMapper;

    @Override
    public Optional<OrganismeDTO> find(final Long organismeId) {
        return organismeRepository.findById(organismeId).map(organismeMapper::toDTO);
    }

    @Override
    public List<OrganismeDTO> getOrganismes(Long organismeId) {
        var plateforme = organismeRepository.findById(organismeId).map(Organisme::getPlateforme).orElseThrow(()->new IllegalArgumentException("Impossible de trouver la plateforme"));
        return organismeRepository.findAllByPlateformeId(plateforme.getId()).stream().map(organismeMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<OrganismeDTO> getOrganismeList(String plateformeUid) {
        return organismeRepository.findAllByPlateformeMpeUid(plateformeUid).stream().map(organismeMapper::toDTO).collect(Collectors.toList());
    }
}
