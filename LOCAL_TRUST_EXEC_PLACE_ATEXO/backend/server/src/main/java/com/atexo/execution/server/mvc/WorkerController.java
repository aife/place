package com.atexo.execution.server.mvc;

import com.atexo.execution.server.common.worker.FileTmp;
import com.atexo.execution.server.common.worker.TaskWorker;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;

@RestController
@RequestMapping("/api/worker")
public class WorkerController {

    private static final Logger LOG = LoggerFactory.getLogger(WorkerController.class);

    @Autowired
    private TaskWorker taskWorker;


    @GetMapping(value = "/{idTask}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Ajax findTask(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable("idTask") String idTask) {
        LOG.info("/verifierTask.json -> " + idTask);

        try {
            Object result = taskWorker.getResult(idTask);
            if (result == null) {
                LOG.info("La tache {} est encore en train d'execution", idTask);
                return Ajax.emptyResponse(); // tache n'est pas encore finie
            }
            String infoShowResult = result.toString();
            if (infoShowResult.length() > 100)
                infoShowResult = infoShowResult.substring(0, 90) + " ...";
            LOG.info("Result d'execution de la tache {} : {}", idTask, infoShowResult);
            return Ajax.successResponse(taskWorker.getResultHref(idTask));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return Ajax.errorResponse(ex.getMessage());
        }
    }

    @GetMapping(value = "/{idTask}/download")
    public void downloadTaskResult(final HttpServletResponse response, @RequestHeader("User-Uuid") final String utilisateurUuid,
                                   @PathVariable("idTask") String idTask) throws Exception {
        LOG.info("/downloadTaskResult -> " + idTask);

        Object result = taskWorker.getResult(idTask);
        FileTmp fileTmp = (FileTmp) result;

        // Entêtes
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileTmp.getNomFichier());
        response.setContentLength((int) fileTmp.length());

        // Flux de sortie
        OutputStream fluxServlet = response.getOutputStream();
        FileInputStream fluxFichier = new FileInputStream(fileTmp);
        IOUtils.copy(fluxFichier, fluxServlet);
        IOUtils.closeQuietly(fluxServlet);
        IOUtils.closeQuietly(fluxFichier);
        response.flushBuffer();
        Files.deleteIfExists(fileTmp.toPath());
    }

    public static class Ajax extends HashMap<String, Object> {

        private Ajax() {
            super();
        }

        public static Ajax successResponse(Object object) {
            Ajax response = new Ajax();
            response.put("result", "success");
            response.put("data", object);
            return response;
        }

        public static Ajax emptyResponse() {
            Ajax response = new Ajax();
            response.put("result", "success");
            return response;
        }

        public static Ajax errorResponse(String errorMessage) {
            Ajax response = new Ajax();
            response.put("result", "error");
            response.put("message", errorMessage);
            return response;
        }

    }


}
