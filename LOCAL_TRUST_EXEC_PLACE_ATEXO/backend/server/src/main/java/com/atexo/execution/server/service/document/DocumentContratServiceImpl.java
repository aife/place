package com.atexo.execution.server.service.document;

import com.atexo.execution.common.def.DocumentContratType;
import com.atexo.execution.common.dto.*;
import com.atexo.execution.common.dto.docgen.DocumentModeleDTO;
import com.atexo.execution.common.dto.edition_en_ligne.*;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.common.exception.RessourcesException;
import com.atexo.execution.server.clients.oauth2.DocumentModeleClient;
import com.atexo.execution.server.clients.oauth2.Oauth2Client;
import com.atexo.execution.server.common.manager.FileStoreManager;
import com.atexo.execution.server.common.manager.PathsConfigurationManager;
import com.atexo.execution.server.common.manager.UrlsConfigManager;
import com.atexo.execution.server.common.manager.upload.UploadFile;
import com.atexo.execution.server.common.manager.upload.UploadManager;
import com.atexo.execution.server.common.mapper.DocumentContratMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.common.mapper.EvenementMapper;
import com.atexo.execution.server.mapper.ContratMapper;
import com.atexo.execution.server.mapper.ExportContratMapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.paginable.DocumentPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.DocumentCriteria;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.utils.freemarker.Java8DateObjectWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.freemarker.FreemarkerTemplateEngine;
import freemarker.template.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nullable;
import javax.transaction.Transactional;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.execution.common.dto.edition_en_ligne.FileStatusEnum.*;

@org.springframework.stereotype.Service
@Transactional
public class DocumentContratServiceImpl implements DocumentContratService {

    static final Logger LOG = LoggerFactory.getLogger(DocumentContratServiceImpl.class);

    private static final String CONTEXT_CONTRAT = "contrat";
    private static final String CONTEXT_ETABLISSEMENT = "etablissement";
    private static final String CONTEXT_EVENEMENT = "evenement";
    private static final String CONTEXT_DATE_CREATION = "dateCreation";

    @Value("${faq.identifiant-plateforme:''}")
    private String identifiantPlateforme;

    @Value("${keycloak.auth-server-url}")
    String keycloakUrl;

    @Value("${redac.documents-modeles.path}")
    String documentsModelesPath;

    @Value("${redac.documents-modeles.download.path}")
    String documentsModelesDownloadPath;

    @Value("${mpe.module.name:EXEC}")
    private String mpeModuleName;

    @Value("${redac.realm}")
    private String realm;

    @Value("${redac.resource}")
    private String resource;

    @Value("${redac.username}")
    private String username;

    @Value("${redac.password}")
    private String password;

    @Value("${mpe.uid:}")
    private String idPlateformeMPE;

    @Autowired
    private Oauth2Client oauth2Client;

    @Autowired
    OrganismeRepository organismeRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    PathsConfigurationManager pathsConfigurationManager;

    @Autowired
    DocumentPaginableRepository documentPaginableRepository;

    @Autowired
    DocumentContratRepository documentRepository;

    @Autowired
    FichierRepository fichierRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    UploadManager uploadManager;

    @Autowired
    FileStoreManager fileStoreManager;

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    EvenementRepository evenementRepository;

    @Autowired
    ContratRepository contratRepository;

    @Autowired
    DocumentContratMapper documentMapper;

    @Autowired
    EvenementMapper evenementMapper;

    @Autowired
    ContratMapper contratMapper;

    @Autowired
    EtablissementMapper etablissementMapper;

    @Autowired
    UrlsConfigManager urlsConfigManager;

    @Autowired
    DocumentModeleRepository documentModeleRepository;
    @Autowired
    DocumentModeleClient documentModeleClient;

    @Value("${guide_utilisateur_file_name:guide_utilisateur.docx}")
    private String guideUtilisateurFileName;

    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    ExportContratMapper exportContratMapper;

    @Value("classpath:modeledocument/fiche-contrat.docx")
    Resource ficheContrat;

    @Override
    public Page<DocumentContratDTO> find(final DocumentCriteriaDTO criteriaDTO, final Pageable pageable) {
        final Page<DocumentContrat> documents;
        documents = documentPaginableRepository.rechercher(
                new DocumentCriteria.Builder()
                        .contrat(criteriaDTO.getContratId())
                        .motsCles(criteriaDTO.getMotsCles())
                        .evenement(criteriaDTO.getEvenements())
                        .etablissement(criteriaDTO.getEtablissements())
                        .typeEvenement(criteriaDTO.getTypeEvenements())
                        .types(criteriaDTO.getDocumentTypes())
                        .build(),
                pageable
        );
        final List<DocumentContratDTO> result = documents.getContent().stream().map(fournisseur -> documentMapper.toDTO(fournisseur, true)).collect(Collectors.toList());
        return new PageImpl<>(result, pageable, documents.getTotalElements());
    }


    @Override
    public List<DocumentContratDTO> getDocumentByEvenement(final Long evenementId) {
        final List<DocumentContrat> documents;
        final Long[] evenementIds = {evenementId};

        documents = documentPaginableRepository.rechercher(
                new DocumentCriteria.Builder()
                        .evenement(evenementIds)
                        .build()
        );
        return documents.stream().map(fournisseur -> documentMapper.toDTO(fournisseur, true)).collect(Collectors.toList());
    }

    @Override
    public File getGuideUtilisateur() throws ApplicationTechnicalException {
        final File guidUtilisateur;
        try {
            guidUtilisateur = new ClassPathResource(guideUtilisateurFileName).getFile();
        } catch (final Exception ex) {
            throw new ApplicationTechnicalException(ex);
        }
        return guidUtilisateur;
    }

    @Override
    public DocumentContratDTO generateDocument(final Long utilisateurId, final Long contratId, final CreationDocumentModelDTO modelDTO) throws ApplicationTechnicalException, IOException {

        final var documentModele = modelDTO.getModeleDocument();
        final Contrat contrat = contratRepository.findById(contratId).orElseThrow(ApplicationTechnicalException::new);
        final Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(ApplicationTechnicalException::new);
        final Etablissement etablissement = etablissementRepository.findById(modelDTO.getEtablissement().getId()).orElse(null);
        Evenement evenement = null;
        Long evenementId = null;
        if (modelDTO.getEvenement() != null) {
            evenement = evenementRepository.findById(modelDTO.getEvenement().getId()).orElseThrow(ApplicationTechnicalException::new);
            evenementId = evenement.getId();
        }
        final String extension = documentModele.getExtension();
        final File outputFile = fileStoreManager.createFileForStorage(extension);
        generateFromDocgen(evenementId, documentModele, contrat, etablissement, evenement, outputFile);
        final Fichier fichier = new Fichier();
        fichier.setNomEnregistre(outputFile.getName());
        fichier.setTaille(outputFile.length());
        final Path path = outputFile.toPath();
        final String mime = Files.probeContentType(path);
        fichier.setContentType(mime);

        String nomBuilder = documentModele.getLibelle() + "." + extension;
        fichier.setNom(nomBuilder);
        final DocumentContrat document = buildDoocument(utilisateur, contrat, modelDTO.getObjet(), etablissement, DocumentContratType.GENERE, fichier, evenement);
        documentRepository.save(document);

        return documentMapper.toDTO(document, true);
    }

    private void generateFromDocgen(Long evenementId, DocumentModeleDTO documentModele, Contrat contrat, Etablissement etablissement, Evenement evenement, File outputFile) throws ApplicationTechnicalException {
        var acronymeOrganisme = Optional.ofNullable(contrat.getService()).map(Service::getOrganisme).map(Organisme::getAcronyme).orElseThrow(ApplicationTechnicalException::new);
        var path = MessageFormat.format(documentsModelesDownloadPath, documentModele.getCode(), mpeModuleName, idPlateformeMPE, acronymeOrganisme);
        var url = getRedacUrl() + path;
        var accessToken = oauth2Client.getOauth2TokenDTO(keycloakUrl, realm, resource, username, password);
        File template = documentModeleClient.getDocumentModele(url, accessToken.getAccesToken(), documentModele);
        Map<String, Object> context = new HashMap<>();

        context.put(CONTEXT_CONTRAT, contratMapper.toFull(contrat));
        context.put(CONTEXT_ETABLISSEMENT, etablissementMapper.toDTO(etablissement));
        if (evenementId != null) {
            context.put(CONTEXT_EVENEMENT, evenementMapper.toDTO(evenement));
        }
        if (contrat.getDateCreation() != null) {
            context.put(CONTEXT_DATE_CREATION, contrat.getDateCreation());
        }

        Resource resource = this.generateDocument(context, template);
        if (resource == null) {
            throw new RessourcesException("La ressource générée est vide");
        }

        try (FileOutputStream fos = new FileOutputStream(outputFile); InputStream docx = resource.getInputStream()) {
            docx.transferTo(fos);
        } catch (final Exception e) {
            throw new ApplicationTechnicalException(e);
        } finally {
            LOG.info("suppression du fichier temporaire {} ", template.getAbsolutePath());
            if (template.exists()) template.delete();
        }

    }

    private void generateFromFreemarker(Contrat contrat, File outputFile) throws ApplicationTechnicalException, IOException {
        try (FileOutputStream fos = new FileOutputStream(outputFile); InputStream templateInputStream = new FileInputStream(ficheContrat.getFile())) {
            final IXDocReport report = XDocReportRegistry.getRegistry().loadReport(templateInputStream, TemplateEngineKind.Freemarker);
            final Configuration freemarkerConfiguration = ((FreemarkerTemplateEngine) report.getTemplateEngine()).getFreemarkerConfiguration();
            freemarkerConfiguration.setObjectWrapper(new Java8DateObjectWrapper(Configuration.VERSION_2_3_20));
            final IContext context = report.createContext();
            context.put(CONTEXT_CONTRAT, exportContratMapper.toDto(contrat));
            report.process(context, fos);

        } catch (final Exception e) {
            throw new ApplicationTechnicalException(e);
        }
    }

    @Override
    public DocumentContratDTO saveDocument(final Long utilisateurId, final Long contratId, CreationDocumentModelDTO modelDTO) throws ApplicationTechnicalException {
        var reference = Optional.ofNullable(modelDTO).map(CreationDocumentModelDTO::getFichier).map(FichierDTO::getReference).orElseThrow(ApplicationTechnicalException::new);

        final Optional<UploadFile> fileOpt = uploadManager.getFile(reference);
        if (fileOpt.isEmpty()) {
            throw new ApplicationTechnicalException();
        }

        final File storeFile = fileStoreManager.storeFile(fileOpt.get().getFile(), false);
        uploadManager.removeFile(reference);

        final UploadFile uploadFile = fileOpt.get();

        final Fichier fichier = new Fichier();
        fichier.setNomEnregistre(storeFile.getName());
        fichier.setNom(uploadFile.getName());
        fichier.setContentType(uploadFile.getMimeType());
        fichier.setTaille(uploadFile.getSize());

        final Contrat contrat = contratRepository.findById(contratId).orElseThrow(ApplicationTechnicalException::new);
        final Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(ApplicationTechnicalException::new);
        final Etablissement etablissement = etablissementRepository.findById(modelDTO.getEtablissement().getId()).orElse(null);

        Evenement evenement = null;
        Long evenementId = null;
        if (modelDTO.getEvenement() != null) {
            evenementId = modelDTO.getEvenement().getId();
        }
        if (evenementId != null) {
            evenement = evenementRepository.findById(evenementId).orElseThrow(ApplicationTechnicalException::new);
        }
        final DocumentContrat document = buildDoocument(utilisateur, contrat, modelDTO.getObjet(), etablissement, DocumentContratType.LIBRE, fichier, evenement);
        documentRepository.save(document);

        return documentMapper.toDTO(document, true);
    }

    @Override
    public DocumentContratDTO saveStreamToDocument(final Long utilisateurId, final Long contratId, final String objet, @Nullable final Long etablissementId, final DocumentContratType documentType, final InputStream inputStream, final String nom, final String contentType,
                                                   final long contentLength, @Nullable final String idExterne) throws ApplicationTechnicalException {

        final File outputFile = fileStoreManager.createFileForStorage(FilenameUtils.getExtension(nom));
        try {
            FileUtils.copyInputStreamToFile(inputStream, outputFile);
        } catch (final IOException e) {
            throw new ApplicationTechnicalException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        final Fichier fichier = new Fichier();
        fichier.setNomEnregistre(outputFile.getName());
        fichier.setNom(nom);
        fichier.setContentType(contentType);
        fichier.setTaille(contentLength);

        final Contrat contrat = contratRepository.findById(contratId).orElseThrow(ApplicationTechnicalException::new);
        final Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(ApplicationTechnicalException::new);

        Etablissement etablissement = null;
        if (etablissementId != null) {
            etablissement = etablissementRepository.findById(etablissementId).orElseThrow(ApplicationTechnicalException::new);
        }

        final DocumentContrat document = buildDoocument(utilisateur, contrat, objet, etablissement, documentType, fichier, null);
        document.setIdExterne(idExterne);
        documentRepository.save(document);
        return documentMapper.toDTO(document, true);
    }

    private DocumentContrat buildDoocument(final Utilisateur utilisateur, final Contrat contrat, final String objet, final Etablissement etablissement, final DocumentContratType documentType, final Fichier fichier, final Evenement evenement) {
        final DocumentContrat document = new DocumentContrat();
        document.setFichier(fichier);
        document.setObjet(objet);
        document.setDocumentType(documentType);
        if (evenement != null) {
            document.getEvenements().add(evenement);
        }
        document.setContrat(contrat);
        document.setEtablissement(etablissement);
        return document;
    }

    @Override
    public DocumentContratDTO saveCommentaire(final Long documentId, final String commentaire) throws ApplicationTechnicalException {
        DocumentContrat document = documentRepository.findById(documentId).orElseThrow(ApplicationTechnicalException::new);
        document.setCommentaire(commentaire);
        document = documentRepository.save(document);
        return documentMapper.toDTO(document, true);
    }

    @Override
    public DocumentContratDTO getDocument(final Long documentId) {
        return documentRepository.findById(documentId).map(d -> documentMapper.toDTO(d, true)).orElse(null);
    }

    @Override
    public File getDocumentFile(final Long documentId) {
        final DocumentContrat document = documentRepository.findById(documentId).orElse(null);
        if (document != null) {
            final Fichier fichier = document.getFichier();
            final Optional<File> repositoryFileOpt = fileStoreManager.getFile(fichier.getNomEnregistre());
            return repositoryFileOpt.orElse(null);
        }
        return null;
    }

    @Override
    public void deleteFile(File file) {
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException ex) {
            LOG.error("impossible de supprimer le fichier", ex);
        }

    }

    @Override
    public void deleteDocument(final Long documentId) {
        final DocumentContrat document = documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Document introuvable"));
        if (document != null) {
            final Fichier fichier = document.getFichier();
            if (fichier != null) {
                fichierRepository.delete(fichier);
                fileStoreManager.deleteFile(fichier.getNomEnregistre());
            }
            documentRepository.delete(document);
        }
    }

    @Override
    public String getUrlEditionEnLigne(final Long documentId, String utilisateurUuid, String token) {
        final DocumentContrat document = documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Document introuvable"));
        var utilisateur = utilisateurRepository.findByUuid(utilisateurUuid).orElseThrow(() -> new IllegalArgumentException("Utilisateur introuvable"));

        String editionToken = document.getUsersEnEdition().stream().filter(documentContratUsersEnEdition -> documentContratUsersEnEdition.getUtilisateur().getUuid().equals(utilisateurUuid))
                .map(DocumentContratUsersEnEdition::getToken).findAny()
                .orElse(null);
        if (editionToken == null) {
            String name = document.getFichier().getNom();
            String prefix = "EXEC@";
            String accessToken = token.split(" ")[1];
            String endpoint = "/api/document/" + documentId + "/edition-en-ligne/callback?access_token=" + accessToken + "&utilisateur=" + utilisateurUuid;

            DocumentEditorRequest documentEditorRequest = DocumentEditorRequest.builder()
                    .callback(urlsConfigManager.getBackendContext() + endpoint)
                    .documentId(String.valueOf(documentId))
                    .plateformeId(prefix + identifiantPlateforme)
                    .documentTitle(name)
                    .mode("edit")
                    .user(User.builder().id(utilisateur.getUuid()).name(utilisateur.getPrenom() + " " + utilisateur.getNom()).build())
                    .build();
            editionToken = this.getEditionToken(new File(pathsConfigurationManager.getFileStorePath(), document.getFichier().getNomEnregistre()), documentEditorRequest);
            DocumentContratUsersEnEdition usersEnEdition = new DocumentContratUsersEnEdition();
            usersEnEdition.setDocument(document);
            usersEnEdition.setToken(editionToken);
            usersEnEdition.setUtilisateur(utilisateur);
            document.getUsersEnEdition().add(usersEnEdition);
            document.setDateModificationEdition(LocalDateTime.now());
            if (document.getUsersEnEdition().size() == 1)
                document.setStatut(REQUEST_TO_OPEN);
        }

        return urlsConfigManager.getEditionEnLigneUrl() + "?token=" + editionToken;
    }


    public String getEditionToken(File document, DocumentEditorRequest editorRequest) {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();

        bodyMap.add("file", new FileSystemResource(document));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> body = new HttpEntity<>(bodyMap, headers);
        String url = urlsConfigManager.getDocgenUrl() + "/docgen/api/v2/document-editor/request";
        return new RestTemplate().postForObject(url, body, String.class);
    }


    @Override
    public TrackDocumentResponse getDocumentCallback(Long documentId, FileStatus status, String utilisateurUuid) {
        if (status == null || status.getStatus() == null || status.getModificationDate() == null) {
            return TrackDocumentResponse.builder().error("-1").build();
        }
        LOG.info("Réception du statut {} pour le document avec id  {}", status.getStatus(), documentId);
        final DocumentContrat document = documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Document introuvable"));

        DocumentContratUsersEnEdition usersEnEdition = document.getUsersEnEdition().stream().filter(documentContratUsersEnEdition -> documentContratUsersEnEdition.getUtilisateur().getUuid().equals(utilisateurUuid))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Cet utilisateur n'est pas en train de modifier le document"));

        LocalDateTime updatedDateModificationEdition = status.getModificationDate().toLocalDateTime();
        document.setStatut(status.getStatus());
        document.setVersion(status.getVersion());
        document.setDateModificationEdition(updatedDateModificationEdition);
        if (status.getStatus().equals(EDITED_AND_SAVED)
                || status.getStatus().equals(FileStatusEnum.SAVED_AND_CLOSED)) {
            InputStream docx;
            try {
                docx = this.getDocumentFromDocGen(usersEnEdition.getToken()).getInputStream();

                File file = new File(pathsConfigurationManager.getFileStorePath(), document.getFichier().getNomEnregistre());
                Files.copy(docx, file.toPath()
                        , StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                LOG.error("Erreur lors de l'enregistrement du fichier {} : {}", documentId, e.getMessage());
                return TrackDocumentResponse.builder().error("-1").build();
            }
        }
        if (status.getStatus().equals(FileStatusEnum.CLOSED_WITHOUT_EDITING) || status.getStatus().equals(FileStatusEnum.CORRUPTED)
                || status.getStatus().equals(FileStatusEnum.SAVED_AND_CLOSED)) {
            document.getUsersEnEdition().remove(usersEnEdition);
        }
        return TrackDocumentResponse.builder().error("0").build();
    }

    @Override
    public void updateDocumentsStatus() {
        List<FileStatusEnum> statusEnums = Arrays.asList(OPENED, EDITED_AND_SAVED, REQUEST_TO_OPEN);
        List<DocumentContrat> documents = documentRepository.findAllByStatutInAndDateModificationEditionIsNotNull(statusEnums);
        LocalDateTime now = LocalDateTime.now();
        for (DocumentContrat document : documents) {
            long seconds = ChronoUnit.SECONDS.between(document.getDateModificationEdition(), now);
            long hours = ChronoUnit.HOURS.between(document.getDateModificationEdition(), now);
            if (hours > 24 && document.getStatut() != null) {
                FileStatusEnum statusEnum = document.getStatut();
                if (OPENED.equals(statusEnum)) {
                    document.setStatut(FileStatusEnum.CORRUPTED);
                    document.getUsersEnEdition().clear();
                } else if (EDITED_AND_SAVED.equals(statusEnum)) {
                    document.setStatut(FileStatusEnum.SAVED_AND_CLOSED);
                    document.getUsersEnEdition().clear();
                }
            }
            if (seconds > 30 && document.getStatut() != null && REQUEST_TO_OPEN.equals(document.getStatut())) {
                document.setStatut(FileStatusEnum.CLOSED_WITHOUT_EDITING);
                document.getUsersEnEdition().clear();
            }
        }
    }

    @Override
    public List<DocumentModeleDTO> getModeleDocumentList(Long organismeId) {
        var organisme = organismeRepository.findById(organismeId).orElseThrow(() -> new IllegalArgumentException("Organisme introuvable"));
        var accessToken = oauth2Client.getOauth2TokenDTO(keycloakUrl, realm, resource, username, password);
        var acronymeOrganisme = organisme.getAcronyme();
        var path = MessageFormat.format(documentsModelesPath, mpeModuleName, idPlateformeMPE, acronymeOrganisme);
        var url = getRedacUrl() + path;
        return documentModeleClient.getDocumentsModele(url, accessToken.getAccesToken());
    }

    @Override
    public File convertFile(byte[] file, String extension) {
        String url = urlsConfigManager.getDocgenUrl() + MessageFormat.format("/docgen/api/v1/document-convertor/{0}/finalize", extension);
        return documentModeleClient.convertToDocx(file, url, extension);
    }

    /*
     * Génération du contrat à partir de la fiche contrat si elle existe, sinon à partir du template fiche-contrat.docx
     * @param utilisateur utilisateur connecté
     * @param contratId id du contrat
     * @return File
     * */
    @Override
    public File export(UtilisateurDTO utilisateur, Long contratId) throws IOException, ApplicationTechnicalException {
        var idOrganisme = Optional.ofNullable(utilisateur.getService()).map(ServiceDTO::getOrganisme).map(OrganismeDTO::getId).orElseThrow(() -> new IllegalArgumentException("Organisme introuvable"));
        var contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        LOG.info("Recherche de template dans les documents modèles");
        var templateFicheContrat = getModeleDocumentList(idOrganisme).stream().filter(documentModeleDTO -> documentModeleDTO.getCode().toLowerCase().contains("fiche") || documentModeleDTO.getLibelle().toLowerCase().contains("fiche")).findFirst().orElse(null);
        var outputFile = Files.createTempFile("contrat-", ".docx").toFile();
        if (templateFicheContrat != null) {
            LOG.info("Template Fiche contrat trouvé dans les documents modèles");
            generateFromDocgen(null, templateFicheContrat, contrat, null, null, outputFile);
        }
        LOG.info("Export du contrat {} => {}", contrat.getNumero(), contrat.getId());
        generateFromFreemarker(contrat, outputFile);
        return outputFile;
    }


    public Resource getDocumentFromDocGen(String token) {
        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put("token", token);
        return new RestTemplate().getForObject(urlsConfigManager.getDocgenUrl() + "/docgen/api/v1/document-monitoring/status?token={token}", Resource.class, urlParameters);
    }

    public Resource generateDocument(Map<String, Object> map, File ressource) {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("template", new FileSystemResource(ressource));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        try {
            bodyMap.add("keyValues", new HttpEntity<>(objectMapper.writeValueAsString(map), theJsonHeader));
        } catch (JsonProcessingException e) {
            bodyMap.add("keyValues", new HttpEntity<>(map, theJsonHeader));
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        return new RestTemplate().postForObject(urlsConfigManager.getDocgenUrl() + "/docgen/api/v2/document-generator/generate", requestEntity, Resource.class);
    }

    public File downloadTemplate(String ws) {
        if (ws == null) {
            throw new RessourcesException("Le chemin ne doit pas être null");
        }
        try {
            String[] tab = ws.split("\\.");
            String suffix = tab[tab.length - 1];
            String url = ws.startsWith("http") ? ws : urlsConfigManager.getRessourcesUrl() + ws;
            return download(url, File.createTempFile("ressources", "." + suffix));
        } catch (IOException e) {
            throw new RessourcesException("Erreur lors du téléchargement de la fiche", e);
        }
    }

    public File download(String urlInputResource, File path) {
        return new RestTemplate().execute(urlInputResource,
                HttpMethod.GET,
                clientHttpRequest -> {
                },
                clientHttpResponse -> {
                    StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(path));
                    return path;
                });
    }

    private String getRedacUrl() {
        var url = urlsConfigManager.getMpeUrl() + "/redac";
        LOG.info("Construction de l'URL redac via proxypass : {}", url);
        return url;
    }
}
