package com.atexo.execution.server.common.manager;

public interface PathsConfigurationManager {

	String getFileStorePath();

	String getTmpPath();

	String getApplicationWorkDir();

	String getLuceneIndexPath();

}
