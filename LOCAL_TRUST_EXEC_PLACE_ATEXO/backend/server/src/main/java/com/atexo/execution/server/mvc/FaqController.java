package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.faq.reponse.Reponse;
import com.atexo.execution.server.interf.faq.FaqService;
import com.atexo.execution.server.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/faq")
public class FaqController {

    @Autowired
    private FaqService faqService;

    @Autowired
	private UtilisateurService utilisateurService;

    @GetMapping(value = "/{contrat}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reponse> initByContract( @RequestHeader("User-Uuid") final String utilisateurUuid, @RequestHeader("User-Agent") String userAgent, @PathVariable("contrat") Long idContrat) {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return new ResponseEntity<>(faqService.initWithContract(utilisateur, userAgent, idContrat), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reponse> init( @RequestHeader("User-Uuid") final String utilisateurUuid, @RequestHeader("User-Agent") String userAgent) {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return new ResponseEntity<>(faqService.init(utilisateur, userAgent), HttpStatus.OK);
    }
}
