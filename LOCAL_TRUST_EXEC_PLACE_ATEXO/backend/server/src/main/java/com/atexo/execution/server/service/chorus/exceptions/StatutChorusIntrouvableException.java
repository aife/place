package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.service.chorus.CodeStatutChorus;
import lombok.Getter;

@Getter
public class StatutChorusIntrouvableException extends ApplicationBusinessException {

	private final CodeStatutChorus code;

	public StatutChorusIntrouvableException( CodeStatutChorus code ) {
		super(String.format("Le statut %s est introuvable", code));

		this.code = code;
	}

}
