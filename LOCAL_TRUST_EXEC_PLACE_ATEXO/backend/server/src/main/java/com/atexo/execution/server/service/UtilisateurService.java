package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.mpe.ws.api.AgentSSOType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UtilisateurService {

    UtilisateurDTO getUtilisateurByUuidAndPlateforme(String pfUid, String login, Set<String> authorities);

	List<UtilisateurDTO> getUtilisateursByEmailIn(String[] emails);

	List<UtilisateurDTO> getUtilisateursByOrganismeAndService( Long organismeId, Long serviceId );

	List<UtilisateurDTO> getAllUtilisateursActifs();

	Optional<UtilisateurDTO> getUtilisateurParUuid( String uuid );

	UtilisateurDTO getUtilisateurParId( Long utilisateurId );

    UtilisateurDTO sauvegarder(String pfUid, AgentSSOType response);
}
