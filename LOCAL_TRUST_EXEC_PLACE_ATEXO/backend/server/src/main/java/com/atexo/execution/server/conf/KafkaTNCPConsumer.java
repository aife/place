package com.atexo.execution.server.conf;

import com.atexo.execution.server.mapper.tncp.TNCPMapper;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.services.DataGouvService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.execution.tncp.reception.TNCPResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@ConditionalOnProperty(value = "tncp.enabled")
@Component
@RequiredArgsConstructor
@Slf4j
public class KafkaTNCPConsumer {

    private final ContratRepository contratRepository;
    private final PlateformeRepository plateformeRepository;
    private final DataGouvService dataGouvService;
    private final TNCPMapper tncpMapper;
    private final ObjectMapper objectMapper;

    @KafkaListener(topics = "${tncp.kafka.topic.contrat.reception}", groupId = "${tncp.kafka.topic.contrat.reception.groupe:exec}", containerFactory = "tncpKafkaListenerContainerFactory")
    public void consume(String reponseString) {
        log.info("Message reçu du TNCP {}", reponseString);
        try {
            TNCPResponse reponseTNCP = objectMapper.readValue(reponseString, TNCPResponse.class);
            String uuidPlateforme = reponseTNCP.getUuidPlateforme();
            if (!plateformeRepository.existsByMpeUid(uuidPlateforme)) {
                log.error("Plateforme non identifiée pour la réponse {}", reponseTNCP);
                return;
            }
            var uuid = reponseTNCP.getIdObjetSource();
            if (uuid == null) {
                log.error("Contrat non identifié pour la réponse {}", reponseTNCP);
            } else {
                var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuid, uuidPlateforme).orElse(null);
                var message = reponseTNCP.getMessage();
                if (contrat != null) {
                    if (reponseTNCP.getIdObjetDestination() != null) {
                        contrat.setNumeroPublicationTNCP(reponseTNCP.getIdObjetDestination());
                        contratRepository.save(contrat);
                    }
                    var statut = tncpMapper.toStatutPublicationDE(reponseTNCP.getStatut());
                    dataGouvService.miseAjourContratPublication(contrat, false, true, message, statut, LocalDateTime.now());
                }
            }
        } catch (JsonProcessingException e) {
            log.error("Erreur de désérialisation de la réponse TNCP", e);
        }
    }
}
