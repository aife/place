package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.Preconditions;
import com.atexo.execution.server.common.mapper.AdresseMapper;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.common.mapper.EtablissementMapper;
import com.atexo.execution.server.interf.basecentrale.BaseCentraleInterface;
import com.atexo.execution.server.model.Adresse;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Fournisseur;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.crud.FournisseurRepository;
import com.atexo.execution.server.repository.paginable.EtablissementPaginableRepository;
import com.atexo.execution.server.repository.paginable.criteres.EtablissementCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
@Transactional
public class EtablissementServiceImpl implements EtablissementService {

	private static final  Logger LOG = LoggerFactory.getLogger(EtablissementServiceImpl.class);

	@Autowired
	private BaseCentraleInterface baseCentraleInterface;

	@Autowired
	private EtablissementRepository etablissementRepository;

    @Autowired
    private EtablissementPaginableRepository etablissementPaginableRepository;

	@Autowired
	private FournisseurRepository fournisseurRepository;

	@Autowired
	private EtablissementMapper etablissementMapper;

	@Autowired
	private AdresseMapper adresseMapper;

	@Autowired
	private ContactMapper contactMapper;

	@Override
	public Optional<EtablissementDTO> findEtablissementBySiret(String siret) {
		LOG.debug("SIRET de l'entreprise à rechercher {} ", siret);
		if (siret == null) {
            return Optional.empty();
        }

		siret = siret.trim().replace(" ", "");

		Etablissement etablissement = null;
		Optional<Etablissement> etablissementOpt = etablissementRepository.findFirstBySiret(siret);

		if ( etablissementOpt.isEmpty() ) {
			try {
				etablissementOpt = baseCentraleInterface.getEtablissementBySiret(siret);
			} catch (ApplicationTechnicalException applicationException) {
				etablissementOpt = Optional.empty();
				LOG.error(applicationException.getMessage(), applicationException);
			}

			if (etablissementOpt.isPresent()) {
				etablissement = etablissementOpt.get();

				Optional<Fournisseur> fournisseurOpt;
				String siren = siret.substring(0, 9);

				fournisseurOpt = fournisseurRepository.findOneBySiren(siren);
				if( fournisseurOpt.isEmpty() ) {
				fournisseurOpt = getFournisseurBySiren(siren);
				}
				if (fournisseurOpt.isPresent()) {
					Fournisseur fournisseur = fournisseurOpt.get();
					fournisseur = fournisseurRepository.save(fournisseur);
					etablissement.setFournisseur(fournisseur);
				}
				etablissement = etablissementRepository.save(etablissement);
			}
		} else {
			etablissement = etablissementOpt.get();
		}

		if (etablissement == null) {
			return Optional.empty();
		} else {
			return Optional.of(etablissementMapper.toDTO(etablissement));
		}
	}

	@Override
	public Optional<EtablissementDTO> findById(Long etablissementId){
		LOG.debug("id de l'entreprise à rechercher {} ", etablissementId);
		if (etablissementId == null || etablissementId <= 0) {
			return Optional.empty();
		}

		Optional<Etablissement> etablissementOpt = Optional.ofNullable(etablissementRepository.findOneById(etablissementId));
		Etablissement etablissement = new Etablissement();
		if (etablissementOpt.isPresent()) {
			etablissement = etablissementOpt.get();
		}
		return Optional.of(etablissementMapper.toDTO(etablissement));
	}

	private Optional<Fournisseur> getFournisseurBySiren(String siren) {
		LOG.debug("Recherche du fournisseur lié à l'entreprise avec le SIREN {} ", siren);
		Optional<Fournisseur> fournisseurOpt = Optional.empty();
		try {
			fournisseurOpt = baseCentraleInterface.getEntrepriseBySiren(siren);
		} catch (ApplicationTechnicalException applicationException) {
			LOG.error(applicationException.getMessage(), applicationException);
			fournisseurOpt = Optional.empty();
		}
		return fournisseurOpt;
	}

	@Override
	public EtablissementDTO ajouterEtablissement(EtablissementDTO etablissementDTO) throws ApplicationBusinessException {
		Preconditions.checkArgument(etablissementDTO != null);

		Optional<EtablissementDTO> etablissementOpt = findEtablissementBySiret(etablissementDTO.getSiret());
		return etablissementOpt.orElse(null);
	}

	@Override
	public EtablissementDTO modifierEtablissement(EtablissementDTO etablissement) throws ApplicationBusinessException {
		Preconditions.checkArgument(etablissement != null);

		var etablissementNouv = Optional.ofNullable(etablissement.getId()).map(etablissementRepository::findById).orElse(Optional.of(new Etablissement()));
		if (etablissementNouv.isEmpty())
			throw new ApplicationBusinessException();
		var etab = etablissementNouv.get();
		var fournisseur = fournisseurRepository.findById(etablissement.getFournisseur().getId()).orElseThrow(ApplicationBusinessException::new);
		etab.setFournisseur(fournisseur);
		Adresse adresse = new Adresse();
		if (etablissement.getAdresse() != null) {
			adresse.setCodePostal(etablissement.getAdresse().getCodePostal());
			adresse.setPays(etablissement.getAdresse().getPays());
			adresse.setAdresse(etablissement.getAdresse().getAdresse());
			adresse.setCommune(etablissement.getAdresse().getCommune());
			adresse.setAdresse2(etablissement.getAdresse().getAdresse2());
			etab.setAdresse(adresse);
		}
		etab.setSiege(etablissement.isSiege());
		etab.setSiret(etablissement.getSiret());
		etab = etablissementRepository.saveAndFlush(etab);

		return etablissementMapper.toDTO(etab);

	}

	@Override
	public EtablissementDTO ajouterEtablissementEtranger(EtablissementDTO etablissementDTO) throws ApplicationBusinessException {
		Preconditions.checkArgument(etablissementDTO != null);

		Etablissement etablissement = new Etablissement();
		Optional<Fournisseur> optionalFournisseur = fournisseurRepository.findOneById(etablissementDTO.getFournisseur().getId());
		optionalFournisseur.ifPresent(etablissement::setFournisseur);
		var adresse = adresseMapper.createEntity(etablissementDTO.getAdresse());
		etablissement.setAdresse(adresse);
		etablissement.setSiege(etablissementDTO.isSiege());
		etablissement.setSiret(etablissementDTO.getSiret());
		etablissement.setTrusted(false);
		etablissementRepository.save(etablissement);
		return etablissementMapper.toDTO(etablissement);
	}

	@Override
	public List<EtablissementDTO> findEtablissementBymotCles(String motsCles) {
        List<EtablissementDTO> result = new ArrayList<>();
        var criteria = new EtablissementCriteria.Builder();
        criteria.motsCles(motsCles);
        List<Etablissement> etablissements = etablissementPaginableRepository.rechercher(criteria.build());
        for (Etablissement etablissement : etablissements) {
            result.add(etablissementMapper.toDTO(etablissement));
        }
        return result;
    }

	@Override
	public void supprimerEtablissement(Long idEtablissement) throws ApplicationBusinessException {

		var etablissement = etablissementRepository.findById(idEtablissement).orElseThrow(ApplicationBusinessException::new);

		etablissementRepository.delete(etablissement);

	}

    @Override
    public EtablissementDTO saveOrUpdate(String siret) {
        if (siret == null || siret.isEmpty()) {
            return null;
        }
        siret = siret.trim().replace(" ", "");
        var siren = siret;
        if (siret.length() < 14) {
            LOG.warn("SIRET trop court {}", siret);
            if (siret.length() >= 9) {
                siren = siret.substring(0, 9);
            }
        }
        var etablissement = etablissementRepository.findFirstBySiret(siret).orElse(null);
        if (etablissement != null) {
            return etablissementMapper.toDTO(etablissement);
        }
        final var pays = "fr";
        var fournisseur = fournisseurRepository.findFirstBySirenAndPays(siren, pays).orElse(null);
        if (fournisseur == null) {
            fournisseur = new Fournisseur();
            fournisseur.setSiren(siren);
            fournisseur.setRaisonSociale("Inconnu " + siren);
            fournisseur.setPays(pays);
            fournisseur = fournisseurRepository.save(fournisseur);
        }
        etablissement = new Etablissement();
        etablissement.setFournisseur(fournisseur);
        etablissement.setSiret(siret);
        etablissement.setSiege(true);
        etablissement = etablissementRepository.save(etablissement);
        return etablissementMapper.toDTO(etablissement);
    }

	@Override
	public List<EtablissementDTO> findEtablissementsByUUIDFournisseur(String uuid) throws ApplicationBusinessException {
		var fournisseur = fournisseurRepository.findByUuid(uuid).orElseThrow(() -> new ApplicationBusinessException("Fournisseur non trouvé"));
		List<Etablissement> etablissements = fournisseur.getEtablissements();
		//Gestion des doublons d'établissements
		List<Etablissement> etablissementsFiltres = new ArrayList<>();
		for (Etablissement etablissement : etablissements) {
			if (!etablissementsFiltres.contains(etablissement)) {
				etablissementsFiltres.add(etablissement);
			}
		}
		for (Etablissement etablissement : etablissementsFiltres) {
			//Gestion des doublons de contacts
			List<Contact> contactsFiltres = new ArrayList<>();
			for (Contact contact : etablissement.getContacts()) {
				if (!contactsFiltres.contains(contact) && contact.getEmail() != null && !contact.getEmail().isEmpty() && contact.getActif().equals(Boolean.TRUE)) {
					contactsFiltres.add(contact);
				}
			}
			etablissement.setContacts(contactsFiltres);
		}

		return etablissementsFiltres.stream().map(etablissementMapper::toDTOWithContacts).collect(Collectors.toList());
	}
}
