package com.atexo.execution.server.security;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.server.common.mapper.HabilitationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class TokenServiceImpl implements TokenService {

    @Autowired
    HabilitationMapper habilitationMapper;

    @Override
    public PreAuthenticatedAuthenticationToken getTokenAccess(UtilisateurDTO utilisateurDTO, String authorizationCode,Map<String, String> parameters) {
        List<GrantedAuthority> habilitations = utilisateurDTO.getHabilitations().stream().map(habilitationDTO -> habilitationMapper.createEntity(habilitationDTO)).collect(Collectors.toList());

        PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken =
                new PreAuthenticatedAuthenticationToken(utilisateurDTO, authorizationCode, habilitations);

        preAuthenticatedAuthenticationToken.setDetails(parameters);

        preAuthenticatedAuthenticationToken.setAuthenticated(true);

        return preAuthenticatedAuthenticationToken;
    }
}
