package com.atexo.execution.server.service.fournisseur;

import com.atexo.execution.server.model.Fournisseur;
import com.atexo.execution.server.repository.crud.FournisseurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by qba on 24/05/16.
 */
@Component
public class FournisseurServiceDelegateImpl implements FournisseurServiceDelegate {

    @Autowired
    FournisseurRepository fournisseurRepository;

    @Override
    public Optional<Fournisseur> getFournisseur(Long fournisseurId) {
        return fournisseurRepository.findOneById(fournisseurId);
    }

}
