package com.atexo.execution.server.oauth.grants;

import com.atexo.execution.common.dto.AccountDTO;
import com.atexo.execution.server.service.security.AccountService;
import com.atexo.execution.server.service.security.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Profile("dev")
@Component
public class DevAccountAuthorizer implements AccountCodeAuthorizer {

	@Value("${dev.login:adminOrme}")
	private String login;

	@Value("${dev.plateforme:MPE}")
	private String plateformeUid;


	@Autowired
	AccountService accountService;

	@Autowired
	AuthorityService authorityService;

	@Autowired
	PasswordEncoder userPasswordEncoder;

	@Autowired
	PasswordBuilder passwordBuilder;

	@Override
	@Transactional
	public AccountDTO authorize(final String plateformeUid, final String authorizationCode) {
		var password = passwordBuilder.build();
		var oauthAccount = accountService.recuperer(plateformeUid, login);
		oauthAccount.setPassword(userPasswordEncoder.encode(password));
		oauthAccount.setAuthorities(authorityService.recuperer());
		oauthAccount.setEnabled(true);
		oauthAccount.setCredentialsExpired(false);
		accountService.sauvegarder(oauthAccount);
		return new AccountDTO(oauthAccount.getUuid(), password);
    }

}
