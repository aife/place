package com.atexo.execution.server.service.imports;

import com.atexo.execution.common.dto.PairDTO;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Classe contenant la logique d'import par xls partagée par les fonctionnalités d'import.
 */
public abstract class AbstractImportService<T> implements ImportService {

    final static Logger LOG = LoggerFactory.getLogger(AbstractImportService.class);

    protected static final DataFormatter DATA_FORMATTER = new DataFormatter();

    protected List<String> messages = new ArrayList<>();

    protected List<String> messagesRow = new ArrayList<>();

    protected int dtosImportes;

    @Override
    public PairDTO<Integer, List<String>> importFichier(File fichier, boolean testOnly, String pfUid) {

        messages.clear();
        dtosImportes = 0;

        try (Workbook workbook = WorkbookFactory.create(fichier)) {

            List<T> dtoList = parse(workbook, pfUid);
            customDTOCheck(dtoList);
            if (messages.isEmpty() && !testOnly) {
                save(dtoList, pfUid);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
        return new PairDTO<>(dtosImportes, messages);
    }

    /**
     * Parcourt le fichier XLS et extrait les informations pour les transformer en objet.
     *
     * @param workbook Le fichier XLS
     * @return Liste de DTO contenant les informations extraites du fichier XLS
     */
    private List<T> parse(Workbook workbook, String pfUid) {
        Map<Integer, String> headerMap = new HashMap<>();
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        Row headerRow = iterator.next();
        for (Cell headerCell : headerRow) {
            headerCellCheck(headerCell);
            headerMap.put(headerCell.getColumnIndex(), headerCell.getStringCellValue());
        }
        if (!messages.isEmpty()) {
            return new ArrayList<>();
        }
        List<T> dtoList = new ArrayList<>();
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            T instance = getInstance();
            messagesRow.clear();
            Integer referenceLibreCellNumber = null;
            for (Entry<Integer, String> entry : headerMap.entrySet()) {
                Cell cell = currentRow.getCell(entry.getKey());
                String header = entry.getValue();
                setValeur(instance, header, cell, currentRow.getRowNum(), entry.getKey(), pfUid);
                if ("referenceLibre".equalsIgnoreCase(header) || "contratReferenceLibre".equalsIgnoreCase(header)) {
                    referenceLibreCellNumber = entry.getKey();
                }
            }
            if (isReferenceLibreVide(referenceLibreCellNumber, currentRow)) {
                LOG.warn("Aucune référence libre détectée, fin de l'import du fichier");
                break;
            }
            dtoList.add(instance);
            flushMessagesRow(instance);
        }
        return dtoList;
    }

    private boolean isReferenceLibreVide(Integer referenceLibreCellNumber, Row currentRow) {
        if (referenceLibreCellNumber != null) {
            Cell referenceLibre = currentRow.getCell(referenceLibreCellNumber);
            if (referenceLibre == null) {
                return true;
            } else {
                String referenceLibreValue = null;
                if (referenceLibre.getCellType() == CellType.NUMERIC) {
                    referenceLibreValue = String.valueOf(referenceLibre.getNumericCellValue());
                } else {
                    referenceLibreValue = currentRow.getCell(referenceLibreCellNumber).getStringCellValue();
                }
                return referenceLibreValue == null || "".equals(referenceLibreValue.trim());
            }

        }
        return false;
    }

    /**
     * Doit retourner un nouvel objet destiné à contenir une ligne du fichier XLS
     *
     * @return
     */
    protected abstract T getInstance();

    /**
     * Vérifie si l'en-tête est correct (ligne 1). En cas d'erreur, ajouter un message à la liste "messages".
     *
     * @param cell
     */
    protected abstract void headerCellCheck(Cell cell);

    /**
     * Responsable de l'assignation de la valeur extraite d'une cellule à l'objet dto instancié. C'est ici qu'il faut tester
     * si la valeur extraite est conforme (caractère obligatoire, format...). Si erreur il y a, ajouter le message
     * d'erreur à la liste "messagesRow".
     * @param instance Objet dto qui doit recevoir la valeur
     * @param header   nom de l'en-tête
     * @param cell
     * @param rowNum
     * @param colNum
     */
    protected abstract void setValeur(T instance, String header, Cell cell, int rowNum, int colNum, String pfUid);

    /**
     * Doit ajouter tous les messages de "messagesRow" dans la liste "messages". Cela permet également de les modifier
     * après l'extraction complète de la ligne et par exemple d'ajouter un préfixe contenant l'identifiant métier de la
     * ligne.
     *
     * @param dto
     */
    protected abstract void flushMessagesRow(T dto);

    /**
     * Une fois que les dtos sont construits et ont reçu toutes les valeurs des lignes, cette méthode est appelée pour
     * effectuer des contrôles métier sur les dtos.
     *
     * @param dtoList
     */
    protected abstract void customDTOCheck(List<T> dtoList);

    /**
     * Sauvegarde les dtos précédemment construits en base.
     *
     * @param dtoList
     */
    protected abstract void save(List<T> dtoList, String pfUid);

    protected ExecutorService getThreadPool() {
        var nbCpu = Runtime.getRuntime().availableProcessors();
        var threadPool = Executors.newFixedThreadPool(nbCpu);
        return threadPool;
    }
}
