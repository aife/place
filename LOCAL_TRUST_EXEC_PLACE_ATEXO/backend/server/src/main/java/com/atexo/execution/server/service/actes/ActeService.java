package com.atexo.execution.server.service.actes;

import com.atexo.execution.common.dto.ActeCriteriaDTO;
import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.exception.ApplicationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public interface ActeService {

    Page<? extends ActeDTO> rechercher(Long utilisateurId, ActeCriteriaDTO criteria, Pageable pageable);

    Set<? extends ActeDTO> rechercher(final  String utilisateurUud, final Long contratId);

    void supprimer(Long utilisateurId, Long acteId);

    <T extends ActeDTO> T sauvegarder(Long id, T acteDTO);

    Optional<? extends ActeDTO> recuperer(Long utilisateur, Long id);

    <T extends ActeDTO> T notifier(String uuid) throws ApplicationException;
}
