package com.atexo.execution.server.oauth.grants.exceptions;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String message) {
		super(message);
	}

}
