package com.atexo.execution.server.service.alerte;

import com.atexo.execution.common.dto.ContratDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.services.ContratsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by qba on 29/11/16.
 */
@Service
@Transactional
public class AlerteServiceImpl implements AlerteService {

    private final static Logger LOG = LoggerFactory.getLogger(AlerteServiceImpl.class);

    @Autowired
    ContratsService contratService;

    @Value("${messagerie.NomPfEmetteur}")
    private String messagerieNomPfEmetteur;

    @Value("${messagerie.mail.expediteur:nepasrepondre@atexo.com}")
    private String adresseMailExpediteur;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    @Autowired
    JavaMailSender javaMailSender;

    @Override
    public boolean envoiMailAlerte(final EvenementDTO evenementDTO) {

        //les destinataires
        final Set<String> dest = new HashSet<>();
        if (evenementDTO.getDestinataires() != null && !evenementDTO.getDestinataires().isEmpty()) {
            dest.addAll(evenementDTO.getDestinataires().stream().map(ValueLabelDTO::getValue).filter(value -> StringUtils.isNotBlank(value) && value.contains("@"))
                    .collect(Collectors.toSet()));
        }

        if (evenementDTO.getDestinatairesLibre() != null && !evenementDTO.getDestinatairesLibre().isEmpty()) {
            dest.addAll(evenementDTO.getDestinatairesLibre().stream().map(ValueLabelDTO::getValue).filter(value -> StringUtils.isNotBlank(value) && value.contains("@"))
                    .collect(Collectors.toSet()));
        }
        if (dest.isEmpty()) {
            LOG.warn("Pas de destinataire pour l'envoi de l'alerte de l'événement {} : {}", evenementDTO.getId(), evenementDTO.getLibelle());
            return false;
        }

        final String[] mailDestinataireTo = new String[dest.size()];
        dest.toArray(mailDestinataireTo);

        final String dateDebut = formatter.format(evenementDTO.getDateDebut());

        String dateFin = "";
        if (evenementDTO.getDateFin() != null) {
            dateFin = formatter.format(evenementDTO.getDateFin());
        }


        final String dateDebutFin;
        if (evenementDTO.getDateFin() != null) {
            dateDebutFin = dateDebut + " au " + dateFin;
        } else {
            dateDebutFin = dateDebut;
        }


        final String mailObjet = "[Alerte - " + evenementDTO.getTypeEvenement().getLabel() + "] " + evenementDTO.getLibelle() + " " + dateDebutFin;

        //le contenu
        final ContratDTO contrat = contratService.getContratByEvenement(evenementDTO.getId());

        String contractant = "";
        if (evenementDTO.getContractant() != null) {
            contractant = "Contractant : " + evenementDTO.getContractant().getFournisseur().getRaisonSociale() + " - " + evenementDTO.getContractant().getAdresse().getCodePostal() + " " + evenementDTO.getContractant().getAdresse().getCommune() + " \n";
        }

        String documents = "";
        if (evenementDTO.getDocuments() != null) {
            documents = "Nombre de documents liés : " + evenementDTO.getDocuments().size();
        }

        String commentaire = "";
        if (evenementDTO.getCommentaire() != null) {
            commentaire = evenementDTO.getCommentaire();
        }

        final String mailContenu = "Bonjour, \n" +
                "L'événement " + evenementDTO.getLibelle() + " (" + evenementDTO.getTypeEvenement().getLabel() + ") aura lieu le " + dateDebut + ". \n" +
                "L'événement possède les caractéristiques suivantes : \n" +
                "Contrat : " + contrat.getReferenceLibre() + " - " + contrat.getObjet() + " \n" +
                "Nom de l'événement : " + evenementDTO.getLibelle() + " \n" +
                "Type d'événement : " + evenementDTO.getTypeEvenement().getLabel() + " " + commentaire + " \n" +
                "Date : " + dateDebutFin + " \n" + contractant + documents + " \n \n" +
                "Bien cordialement, \n \n" + messagerieNomPfEmetteur;

        LOG.info("envoi du mail :");

        LOG.info("Objet : " + mailObjet + " - Contenu : " + mailContenu);

        final MimeMessagePreparator preparator = getMimeMessagePreparator(mailDestinataireTo, mailObjet, mailContenu);

        javaMailSender.send(preparator);
        return true;
    }

    @Override
    public void envoiMailAlerte(final Contrat contrat) {
        Optional.ofNullable(contrat.getCreateur())
                .map(Utilisateur::getEmail)
                .ifPresent(email -> {
                    var mailDestinataireTo = new String[]{email};
                    //l'objet
                    final String mailObjet = MessageFormat.format("Alerte contrat {0} à renouveler", contrat.getReferenceLibre());
                    final String template = "Le contrat {0} arrive à échéance le {1}.\nObjet: {2}\n\nAfin de préparer son renouvellement une phase d'étude de n mois est prévue.\nVous recevez cette alerte afin d’engager le processus de renouvellement et d’anticiper les actions à réaliser.";
                    var mailContent = MessageFormat.format(template, contrat.getReferenceLibre(), formatter.format(contrat.getDateMaxFinContrat()), contrat.getObjet());
                    LOG.info("envoi du mail :");

                    LOG.info("Objet : " + mailObjet + " - Contenu : " + template);

                    final MimeMessagePreparator preparator = getMimeMessagePreparator(mailDestinataireTo, mailObjet, mailContent);

                    javaMailSender.send(preparator);

                });
    }

    private MimeMessagePreparator getMimeMessagePreparator(String[] mailDestinataireTo, String mailObjet, String mailContent) {
        final MimeMessagePreparator preparator = mimeMessage -> {

            mimeMessage.setHeader("Content-Transfert-Encoding", "bit8");
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");

            message.setTo(mailDestinataireTo);
            message.setFrom(adresseMailExpediteur);
            message.setSubject(mailObjet);
            message.setText(mailContent);
        };
        return preparator;
    }
}
