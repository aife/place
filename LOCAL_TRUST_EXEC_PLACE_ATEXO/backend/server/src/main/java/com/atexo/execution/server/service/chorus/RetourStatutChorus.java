package com.atexo.execution.server.service.chorus;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestAttribute;

@RequiredArgsConstructor
@Getter
public enum RetourStatutChorus {
	REJETE("REJ"),
	IRRECEVABLE("IRR"),
	INTEGRE("COM");

	@NonNull
	final String idExterne;
}
