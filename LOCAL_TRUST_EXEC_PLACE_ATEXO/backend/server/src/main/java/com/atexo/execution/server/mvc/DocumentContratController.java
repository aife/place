package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.CreationDocumentModelDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.dto.DocumentCriteriaDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.dto.edition_en_ligne.FileStatus;
import com.atexo.execution.common.dto.edition_en_ligne.TrackDocumentResponse;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.manager.UrlsConfigManager;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.calendrier.CalendrierService;
import com.atexo.execution.server.service.document.DocumentContratService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Controller
@RequestMapping(value = "/api/document")
public class DocumentContratController {

    @Autowired
    DocumentContratService documentService;

    @Autowired
    CalendrierService calendrierService;

    @Autowired
    UrlsConfigManager urlsConfigManager;

    @Autowired
    UtilisateurService utilisateurService;

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<DocumentContratDTO>> find(@RequestBody DocumentCriteriaDTO criteriaDTO, @PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {

        Page<DocumentContratDTO> list = documentService.find(criteriaDTO, pageable);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }


    @GetMapping(value = "evenement/{evenement}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DocumentContratDTO>> find(@PathVariable("evenement") Long evenementId) {
        return new ResponseEntity<>(documentService.getDocumentByEvenement(evenementId), HttpStatus.OK);
    }

    @PostMapping(value = "/save/{contratId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DocumentContratDTO> saveDocument(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable Long contratId, @RequestBody CreationDocumentModelDTO modelDTO)
            throws ApplicationTechnicalException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        DocumentContratDTO document = documentService.saveDocument(utilisateur.getId(), contratId, modelDTO);
        return new ResponseEntity<>(document, HttpStatus.OK);
    }

    @PostMapping(value = "/{document}/commentaire", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DocumentContratDTO> saveCommentaire(@PathVariable("document") Long documentId, @RequestBody String commentaire) throws ApplicationTechnicalException {
        String newCommentaire = null;
        if (commentaire != null) {
            newCommentaire = commentaire;
        }
        DocumentContratDTO documentDTO = documentService.saveCommentaire(documentId, newCommentaire);
        return new ResponseEntity<>(documentDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{document}/download")
    public ResponseEntity<InputStreamResource> download(@PathVariable("document") Long documentId) {
        DocumentContratDTO document = documentService.getDocument(documentId);
        if (document == null || document.getFichier() == null) {
            return new ResponseEntity<>((InputStreamResource) null, HttpStatus.NOT_FOUND);
        } else {
            File documentFile = documentService.getDocumentFile(documentId);
            if (documentFile != null) {
                FileInputStream fis; // sera fermé par spring mvc
                try {
                    fis = new FileInputStream(documentFile);
                } catch (Exception e) {
                    return new ResponseEntity<>((InputStreamResource) null, HttpStatus.INTERNAL_SERVER_ERROR);
                }
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(documentFile.length());
                headers.setContentType(MediaType.parseMediaType(document.getFichier().getContentType()));
                headers.setContentDispositionFormData(document.getFichier().getNom(), document.getFichier().getNom());
                return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((InputStreamResource) null, HttpStatus.NOT_FOUND);
            }
        }
    }

    @PostMapping(value = "/generate/{contratId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DocumentContratDTO> generateDocument(@RequestHeader("User-Uuid") final String utilisateurUuid,
                                                               @PathVariable Long contratId,
                                                               @RequestBody CreationDocumentModelDTO modelDTO
    ) throws ApplicationTechnicalException, IOException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return new ResponseEntity<>(documentService.generateDocument(utilisateur.getId(), contratId, modelDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{document}")
    public ResponseEntity<Void> deleteDocument(@PathVariable("document") Long documentId) {
        documentService.deleteDocument(documentId);
        return new ResponseEntity<>((Void) null, HttpStatus.OK);
    }

    @GetMapping(value = "/{document}/edition-en-ligne", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValueLabelDTO> getUrlEditionEnLigne(@PathVariable("document") Long documentId,
                                                              @RequestHeader("User-Uuid") final String utilisateurUuid,
                                                              @RequestHeader("Authorization") final String token) throws ApplicationTechnicalException {
        String urlEditionEnLigne = documentService.getUrlEditionEnLigne(documentId, utilisateurUuid, token);
        var result = new ValueLabelDTO();
        result.setValue(urlEditionEnLigne);
        result.setLabel("URL édition en ligne");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @PostMapping("/{documentId}/edition-en-ligne/callback")
    public TrackDocumentResponse trackDocument(@PathVariable Long documentId, @RequestParam String utilisateur, @RequestBody FileStatus status) {
        return documentService.getDocumentCallback(documentId, status, utilisateur);
    }

    @GetMapping(value = "/guide-utilisateur")
    public ResponseEntity<InputStreamResource> getGuideUtilisateur() throws ApplicationTechnicalException {
        File guideUtilisateur = documentService.getGuideUtilisateur();
        if (guideUtilisateur != null) {
            FileInputStream fis; // sera fermé par spring mvc
            try {
                fis = new FileInputStream(guideUtilisateur);
            } catch (Exception e) {
                return new ResponseEntity<>((InputStreamResource) null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(guideUtilisateur.length());
            headers.setContentDispositionFormData(guideUtilisateur.getName(), guideUtilisateur.getName());
            return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>((InputStreamResource) null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/convert/{format}")
    @Operation(method = "POST", description = "Convertir un fichier PDF en DOCX ou autre type")
    public ResponseEntity<InputStreamResource> convert(@RequestPart @NotEmpty @NotNull MultipartFile pdf, @PathVariable String format) throws IOException {

        var file = documentService.convertFile(pdf.getBytes(), format);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(Files.probeContentType(file.toPath())))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName())
                .body(new InputStreamResource(new FileInputStream(file)));

    }

    @GetMapping("/export/{contratId}")
    public ResponseEntity<InputStreamResource> export(@PathVariable("contratId") Long contratId, @RequestHeader("User-Uuid") final String utilisateurUuid) throws ApplicationTechnicalException, IOException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var file = documentService.export(utilisateur, contratId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(Files.probeContentType(file.toPath())))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName())
                .body(new InputStreamResource(new FileInputStream(file)));

    }
}
