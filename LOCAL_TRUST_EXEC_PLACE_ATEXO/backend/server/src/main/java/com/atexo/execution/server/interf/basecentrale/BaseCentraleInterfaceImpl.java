package com.atexo.execution.server.interf.basecentrale;

import com.atexo.execution.common.dto.AttestationModelDTO;
import com.atexo.execution.server.api.entreprise.v3.service.ApiEntrepriseService;
import com.atexo.execution.server.common.mapper.basecentrale.EtablissementV3Mapper;
import com.atexo.execution.server.common.mapper.basecentrale.FournisseurV3Mapper;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.referentiels.CategorieFournisseurRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.atexo.execution.server.entreprise.v3.model.V3InseeSireneUnitesLegalesSirenGet200ResponseData.TypeEnum.MORALE;
import static com.atexo.execution.server.entreprise.v3.model.V3InseeSireneUnitesLegalesSirenGet200ResponseData.TypeEnum.PHYSIQUE;


@Component
@RequiredArgsConstructor
public class BaseCentraleInterfaceImpl implements BaseCentraleInterface {

    static final Logger LOG = LoggerFactory.getLogger(BaseCentraleInterfaceImpl.class);
    private static final String ATTESTATION_COTISATIONS_CONGES_PAYES_CHOMAGE_INTEMPERIES = "AttestationCotisationsCongesPayesChomageIntemperies";
    private static final String CARTE_PROFESSIONNELLE_TRAVAUX_PUBLICS = "CarteProfessionnelleTravauxPublics";
    private static final String ATTESTATION_FISCALE = "AttestationFiscale";
    private static final String ATTESTATION_VIGILANCE = "AttestationVigilance";
    private static final String ATTESTATION_COTISATIONS_RETRAITE = "AttestationCotisationsRetraite";
    private static final String URL = "url";
    private static final String DATE_MISE_A_JOUR = "date_mise_a_jour";
    private static final String ZIP_ARCHIVE = "zip_archive";
    private static final String ZIP_URL = "/execution-server/api/fournisseur/attestations/zip/";
    public static final String META = "meta";
    public static final String DOCUMENTS = "documents";
    public static final String ZIP_FILE_PREFIX = "documents-";
    public static final String ZIP_EXTENSION = ".zip";

    private final EtablissementV3Mapper etablissementV3Mapper;
    private final FournisseurV3Mapper fournisseurV3Mapper;
    private final ApiEntrepriseService apiEntrepriseService;
    private final RestTemplate restTemplate;
    private final UtilisateurRepository utilisateurRepository;
    private final CategorieFournisseurRepository categorieFournisseurRepository;

    @Value("${baseCentrale.recipient:}")
    String recipient;

    @Override
    public Optional<Fournisseur> getEntrepriseBySiren(String siren) {
        var recipient = getCurrentSiret();
        var entreprise = apiEntrepriseService.getEntreprise(siren, recipient);
        var raisonSociale = "";
        if (entreprise.getType() == PHYSIQUE && entreprise.getPersonnePhysiqueAttributs() != null) {
            raisonSociale = entreprise.getPersonnePhysiqueAttributs().getPrenomUsuel() + " " + entreprise.getPersonnePhysiqueAttributs().getNomNaissance();
        }
        if (entreprise.getType() == MORALE && entreprise.getPersonneMoraleAttributs() != null) {
            raisonSociale = entreprise.getPersonneMoraleAttributs().getRaisonSociale();
        }
        var extraitKbis = apiEntrepriseService.getExtraitKbis(siren, recipient);
        Fournisseur fournisseur = fournisseurV3Mapper.toFournisseur(entreprise, extraitKbis, categorieFournisseurRepository.findAll());
        if (fournisseur.getRaisonSociale() == null || fournisseur.getRaisonSociale().isEmpty()) {
            fournisseur.setRaisonSociale(raisonSociale);
        }
        getEtablissementBySiret(entreprise.getSiretSiegeSocial())
                .map(e -> {
                    e.setFournisseur(fournisseur);
                    return e;
                }).ifPresent(etablissement -> fournisseur.setEtablissements(List.of(etablissement)));
        return Optional.of(fournisseur);
    }

    @Override
    public Optional<Etablissement> getEtablissementBySiret(String siret) {
        var recipient = getCurrentSiret();
        var etablissement = etablissementV3Mapper.toEtablissement(apiEntrepriseService.getEtablissement(siret, recipient));
        return Optional.ofNullable(etablissement);
    }

    @Override
    public Optional<Map<String, Object>> getDocumentsBySiren(String siren) {
        var recipient = getCurrentSiret();
        Map<String, String> attestations = new HashMap<>();
        attestations.put(ATTESTATION_COTISATIONS_CONGES_PAYES_CHOMAGE_INTEMPERIES, apiEntrepriseService.getAttestationCotisationsCongesPayesChomageIntemperies(siren, recipient));
        attestations.put(CARTE_PROFESSIONNELLE_TRAVAUX_PUBLICS, apiEntrepriseService.getCarteProfessionnelleTravauxPublics(siren, recipient));
        attestations.put(ATTESTATION_FISCALE, apiEntrepriseService.getAttestationFiscale(siren, recipient));
        attestations.put(ATTESTATION_VIGILANCE, apiEntrepriseService.getAttestationVigilance(siren, recipient));
        return Optional.of(prepareAttestationsToFront(siren, attestations));
    }


    @Override
    public Optional<Map<String, Object>> getDocumentsBySiret(String siret) {
        var recipient = getCurrentSiret();
        Map<String, String> attestations = new HashMap<>();
        attestations.put(ATTESTATION_COTISATIONS_RETRAITE, apiEntrepriseService.getAttestationCotisationsRetraite(siret, recipient));
        return Optional.of(prepareAttestationsToFront(siret, attestations));
    }

    private Map<String, Object> prepareAttestationsToFront(String siren, Map<String, String> attestations) {
        Set<Map<String, String>> urls = new HashSet<>();
        Map<String, Object> meta = new HashMap<>();
        Map<String, Object> documents = new HashMap<>();
        attestations.forEach((k, v) -> {
            if (v != null) {
                meta.put(k, new AttestationModelDTO(k, v));
                Map<String, Object> document = Map.of(URL, v, DATE_MISE_A_JOUR, LocalDateTime.now());
                documents.put(k, document);
                urls.add(Map.of(k, v));
            }
        });
        documents.put(ZIP_ARCHIVE, Map.of(URL, ZIP_URL + siren));
        Map<String, Object> links = Map.of(META, meta, DOCUMENTS, documents);
        return links;
    }

    @Override
    public File zipAttestations(String sirenOrSiret, Map<String, String> attestations) throws IOException {
        File zipFile = File.createTempFile(ZIP_FILE_PREFIX, ZIP_EXTENSION);
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            attestations.forEach((k, v) -> restTemplate.execute(v, HttpMethod.GET, null, clientHttpResponse -> {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                StreamUtils.copy(clientHttpResponse.getBody(), byteArrayOutputStream);
                ZipEntry entry = new ZipEntry(k + " - " + sirenOrSiret + "." + FilenameUtils.getExtension(v));
                zos.putNextEntry(entry);
                byte[] data = byteArrayOutputStream.toByteArray();
                zos.write(data, 0, data.length);
                return null;
            }));
            return zipFile;
        } catch (IOException e) {
            LOG.error("Impossible de générer le zip", e);
        }
        return null;
    }

    public String getCurrentSiret() {
        var securityContext = SecurityContextHolder.getContext();
        if (securityContext == null || securityContext.getAuthentication() == null)
            return recipient;
        var identifiant = SecurityContextHolder.getContext().getAuthentication().getName();
        if (identifiant == null) {
            return recipient;
        }
        return utilisateurRepository.findOneByIdentifiant(SecurityContextHolder.getContext().getAuthentication().getName())
                .map(Utilisateur::getService)
                .map(Service::getOrganisme)
                .map(Organisme::getSiret)
                .orElse(recipient);
    }
}
