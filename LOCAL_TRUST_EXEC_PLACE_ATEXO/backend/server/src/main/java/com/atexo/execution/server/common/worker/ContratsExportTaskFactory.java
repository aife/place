package com.atexo.execution.server.common.worker;

import com.atexo.execution.common.dto.ContratCriteriaDTO;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class ContratsExportTaskFactory extends TaskFactory {

    public Task<File> newExportContratsTask(String utilisateurUuid, ContratCriteriaDTO contratCriteriaDTO) {
        ExportContratsTask task = getContext().getBean(ExportContratsTask.class);
        task.setUtilisateurUuid(utilisateurUuid);
        task.setCriteria(contratCriteriaDTO);
        task.setResultHref("downloadTaskResult");
        return task;
    }



}
