package com.atexo.execution.server.oauth.grants;

import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.stereotype.Component;

import static org.passay.CharacterCharacteristicsRule.ERROR_CODE;

@Component
public class PasswordBuilder {

	public String build() {
		var generator = new PasswordGenerator();
		var lowerCaseChars = EnglishCharacterData.LowerCase;
		var lowerCaseRule = new CharacterRule(lowerCaseChars);
		lowerCaseRule.setNumberOfCharacters(2);

		var upperCaseChars = EnglishCharacterData.UpperCase;
		var upperCaseRule = new CharacterRule(upperCaseChars);
		upperCaseRule.setNumberOfCharacters(2);

		var digitChars = EnglishCharacterData.Digit;
		var digitRule = new CharacterRule(digitChars);
		digitRule.setNumberOfCharacters(2);

		var specialChars = new CharacterData() {
			public String getErrorCode() {
				return ERROR_CODE;
			}

			public String getCharacters() {
				return "!@#$%^&*()_+";
			}
		};
		var splCharRule = new CharacterRule(specialChars);
		splCharRule.setNumberOfCharacters(2);

		return generator.generatePassword(10, splCharRule, lowerCaseRule, upperCaseRule, digitRule);
	}
}
