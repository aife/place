package com.atexo.execution.server.service.chorus.exceptions;

import com.atexo.execution.common.exception.ApplicationBusinessException;

public class EnvoiZipChorusException extends ApplicationBusinessException {

	public EnvoiZipChorusException(String message) {
		super(message);
	}
}
