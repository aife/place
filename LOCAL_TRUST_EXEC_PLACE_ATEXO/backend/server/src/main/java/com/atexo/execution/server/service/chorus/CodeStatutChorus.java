package com.atexo.execution.server.service.chorus;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum CodeStatutChorus {
	REJETE("SC_REJ"),
	REJETE_PROBLEME_FOURNISSEUR("RPF"),
	REJETE_PROBLEME_TECHNIQUE("RPT"),
	IRRECEVABLE("SC_IRR"),
	FLUX_A_GENERER("FAG"),
	ENVOI_PLANIFIE("EP");

	@NonNull
	final String idExterne;
}
