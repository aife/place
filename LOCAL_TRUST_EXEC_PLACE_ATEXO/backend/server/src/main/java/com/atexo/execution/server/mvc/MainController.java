package com.atexo.execution.server.mvc;

import com.atexo.execution.server.common.manager.UrlsConfigManager;
import com.atexo.execution.server.security.TokenService;
import com.atexo.execution.server.service.ParametrageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @Autowired
    UrlsConfigManager urlsConfigManager;

    @Autowired
    ParametrageService parametrageService;

    @GetMapping("/swagger")
    public String getSwaggerUIPage() {
        return "swagger";
    }

    @PostMapping(value = "/api/logout")
    public ResponseEntity<String> logout() {
        OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        OAuth2AuthenticationDetails authenticationDetails = (OAuth2AuthenticationDetails) auth.getDetails();
        if (authenticationDetails != null) {
            String jetonApplicatif = authenticationDetails.getTokenValue();
            //tokenServices.revokeToken(jetonApplicatif);
        }
        String mpeUrl = urlsConfigManager.getMpeUrl();
        return new ResponseEntity<>(mpeUrl + "/?page=agent.AgentHome", HttpStatus.OK);
    }

    @GetMapping(value = "/api/accueilMpe")
    public ResponseEntity<String> accueilMpe() {
        String mpeUrl = urlsConfigManager.getMpeUrl();
        return new ResponseEntity<>(mpeUrl + "/?page=agent.AgentHome", HttpStatus.OK);
    }

    @GetMapping(value = "/api/findProfil")
    public ResponseEntity<String> findProfil() {
        String profilesProperty = System.getProperty("spring.profiles.active");
        return new ResponseEntity<>(profilesProperty, HttpStatus.OK);
    }

}
