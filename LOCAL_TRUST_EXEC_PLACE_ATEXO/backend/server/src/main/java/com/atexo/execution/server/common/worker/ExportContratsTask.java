package com.atexo.execution.server.common.worker;

import com.atexo.execution.common.dto.ContratCriteriaDTO;
import com.atexo.execution.server.service.export.ExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.util.Date;


@Component
@Scope("prototype")
public class ExportContratsTask extends Task<File> {

    private static final Logger logger = LoggerFactory.getLogger(ExportContratsTask.class);

    private String utilisateurUuid;
    private ContratCriteriaDTO criteria;

    @Autowired
    ExportService exportService;

    @Override
    public FileTmp work() throws Exception {
        logger.info("Début de l'export des contrats " + new Date());


        try {
            File file = exportService.export(utilisateurUuid, criteria);
            File tmp = File.createTempFile("edit-", ".xlsx");
            Files.deleteIfExists(tmp.toPath());
            Files.copy(file.toPath(), tmp.toPath());
            FileTmp fileTmp = new FileTmp(tmp.getPath());
            return fileTmp;

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex.fillInStackTrace());
            throw ex;
        }
    }


    public String getUtilisateurUuid() {
        return utilisateurUuid;
    }

    public void setUtilisateurUuid(String utilisateurUuid) {
        this.utilisateurUuid = utilisateurUuid;
    }

    public ContratCriteriaDTO getCriteria() {
        return criteria;
    }

    public void setCriteria(ContratCriteriaDTO criteria) {
        this.criteria = criteria;
    }





}
