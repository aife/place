package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ActeCriteriaDTO;
import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.common.manager.FileStoreManager;
import com.atexo.execution.server.model.Fichier;
import com.atexo.execution.server.repository.crud.DocumentActeRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.actes.ActeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(value = "/api/actes")
public class ActeController {

	@Autowired
	private DocumentActeRepository documentActeRepository;

	@Autowired
	private ActeService acteService;

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private FileStoreManager fileStoreManager;

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<? extends ActeDTO> find(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ActeCriteriaDTO criteria, final Pageable pageable) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return acteService.rechercher(utilisateur.getId(), criteria, pageable);
	}

	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public <T extends ActeDTO> ResponseEntity<T> save(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final T acteDTO) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		var acte = acteService.sauvegarder(utilisateur.getId(), acteDTO);

		return new ResponseEntity<>(acte, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}/download")
	public ResponseEntity<InputStreamResource> download(@PathVariable("id") Long documentId) {
		var optionalDocument = documentActeRepository.findById(documentId);

		if (optionalDocument.isPresent()) {
			final var document = optionalDocument.get();
			final Fichier fichier = document.getFichier();
			final Optional<File> repositoryFileOpt = fileStoreManager.getFile(fichier.getNomEnregistre());

			if (repositoryFileOpt.isPresent()) {
				var documentFile = repositoryFileOpt.get();

				FileInputStream fis; // sera fermé par spring mvc
				try {
					fis = new FileInputStream(documentFile);
				} catch (Exception e) {
					return new ResponseEntity<>((InputStreamResource) null, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				HttpHeaders headers = new HttpHeaders();
				headers.setContentLength(documentFile.length());
				headers.setContentType(MediaType.parseMediaType(document.getFichier().getContentType()));
				headers.setContentDispositionFormData(document.getFichier().getNom(), document.getFichier().getNom());
				return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
			} else {
				return new ResponseEntity<>((InputStreamResource) null, HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>((InputStreamResource) null, HttpStatus.NOT_FOUND);
		}
	}

	@PatchMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ActeDTO> update(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody final ActeDTO acteDTO) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		var acte = acteService.sauvegarder(utilisateur.getId(), acteDTO);

		return new ResponseEntity<>(acte, HttpStatus.OK);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<? extends ActeDTO>> find(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestParam("contrat") final Long contratId) {

		return new ResponseEntity<>(acteService.rechercher(utilisateurUuid, contratId), HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<? extends ActeDTO> get(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable(name = "id") final Long id) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return acteService.recuperer(utilisateur.getId(), id).map(acteDTO -> new ResponseEntity<>(acteDTO, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> delete(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable(name = "id", required = false) final Long acteId) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		this.acteService.supprimer(utilisateur.getId(), acteId);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/notification/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ActeDTO> notification(@PathVariable(name = "uuid", required = false) final String uuid) throws ApplicationException {

		var acteNotifie = this.acteService.notifier(uuid);

		return new ResponseEntity<>(acteNotifie, HttpStatus.OK);
	}
}
