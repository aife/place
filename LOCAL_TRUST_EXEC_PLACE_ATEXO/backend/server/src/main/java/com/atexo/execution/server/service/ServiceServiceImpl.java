package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.EntiteServiceDTO;
import com.atexo.execution.common.dto.ServiceDTO;
import com.atexo.execution.server.common.mapper.ServiceMapper;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
@Transactional
public class ServiceServiceImpl implements ServiceService {

	@Autowired
	OrganismeRepository organismeRepository;

	@Autowired
	ServiceRepository serviceRepository;

	@Autowired
	ServiceMapper serviceMapper;

	@Override
	public List<ServiceDTO> getServices( final Long organismeId ) {
		var plateforme = organismeRepository.findById(organismeId).map(Organisme::getPlateforme).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme"));
		return serviceRepository.findByOrganismeIdAndPlateformeIdOrderByNomAsc(organismeId,plateforme.getId()).stream().map(service -> serviceMapper.toDTO(service)).collect(Collectors.toList());
	}

	@Override
	public List<EntiteServiceDTO> getServicesTree(Long organismeId) {
		var plateforme = organismeRepository.findById(organismeId).map(Organisme::getPlateforme).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme"));
		return serviceRepository.findByOrganismeIdAndPlateformeIdAndParentIsNullOrderByNomAsc(organismeId, plateforme.getId()).stream().map(service -> serviceMapper.toTreeDTO(service)).collect(Collectors.toList());

	}

	@Override
	public List<EntiteServiceDTO> getServicesTree(Long organismeId, Long serviceId) {
		var plateforme = organismeRepository.findById(organismeId).map(Organisme::getPlateforme).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme"));

		return serviceRepository.findByIdAndOrganismeIdAndPlateformeIdOrderByNomAsc(serviceId, organismeId, plateforme.getId()).stream().map(service -> serviceMapper.toTreeDTO(service)).collect(Collectors.toList());
	}
}
