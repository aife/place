package com.atexo.execution.server.common.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


public class FutureResult<T> {
    private static final Logger logger = LoggerFactory.getLogger(FutureResult.class);

    private final Future<T> future;

    private final String resultHref;

    public FutureResult(Future<T> future, String resultHref) {
        this.future = future;
        this.resultHref = resultHref;
    }

    public boolean isDone() {
        if (future == null) {
            logger.error("isDone : The future is null");
            throw new IllegalStateException("The future is null");
        }
        return future.isDone();
    }

    public T get() throws InterruptedException, ExecutionException {
        if (future == null) {
            logger.error("get : The future is null");
            throw new IllegalStateException("The future is null");
        }
        return future.get();
    }

    public String getResultHref() {
        return resultHref;
    }

}
