package com.atexo.execution.server.service.calendrier;

import com.atexo.execution.common.dto.EvenementCriteriaDTO;
import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Service de gestion des évenements du contrat
 * Created by sta on 06/09/16.
 */
public interface CalendrierService {

    List<EvenementDTO> getCalendrierByContrat(Long contratId) throws ApplicationBusinessException;

    Page<EvenementDTO> rechercher(Long contratId, EvenementCriteriaDTO criteriaDTO, Pageable pageable);

    EvenementDTO enregistrerEvenement(EvenementDTO evenement, Long idContrat);

    EvenementDTO getEvenement(Long contratId, Long evenementId);

    void deleteEvenement(Long contratId, Long evenementId);

    EvenementDTO getEvenement(Long evenementId);

    List<EvenementDTO> getEvenements(Boolean alerte, Boolean envoiAlerte);

    void modifEnvoiEvenement(Long evenementId);

    Map<Long, Long> compterEnAttente( List<Long> idsContrat);
}
