package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.ContactPrincipalDTO;
import com.atexo.execution.common.dto.ContactReferantDTO;
import com.atexo.execution.common.dto.EditionContactDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;

import java.util.List;

public interface ContactService {

	List<ContactDTO> getContactByIdIn(Long[] ids);

	ContactDTO ajouterContact(EditionContactDTO editionContactDTO, String plateformeUid)
			throws ApplicationException;

	;

	 ContactDTO modifierContact(EditionContactDTO editionContactDTO)
			throws ApplicationBusinessException;

	ContactDTO supprimerContact(Long contactId)
			throws ApplicationBusinessException;

    List<ContactReferantDTO> getContactByContratEtablissement(Long contratEtablissementId);

    ContactReferantDTO modifierContactReferant(ContactReferantDTO contactReferantDTO) throws ApplicationBusinessException;

    boolean supprimerContactReferant(Long contratEtablissementId, Long ContactId);

    ContactReferantDTO ajouterContactReferant(ContactReferantDTO contactReferantDTO, String plateformeUid) throws ApplicationException;

	List<ContactDTO> getContactsByEtablissement(Long etablissementId);

	ContactPrincipalDTO modifier(ContactPrincipalDTO contactDTO, String plateformeUid) throws ApplicationException;

	ContactPrincipalDTO ajouter(ContactPrincipalDTO contactPrincipalDTO, String plateformeUid) throws ApplicationException;

	ContactPrincipalDTO recuperer(Long attributaireId, String plateformeUid) throws ApplicationException;
}
