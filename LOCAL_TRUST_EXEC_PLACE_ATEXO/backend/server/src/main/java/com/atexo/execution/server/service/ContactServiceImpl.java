package com.atexo.execution.server.service;

import com.atexo.execution.common.dto.ContactDTO;
import com.atexo.execution.common.dto.ContactPrincipalDTO;
import com.atexo.execution.common.dto.ContactReferantDTO;
import com.atexo.execution.common.dto.EditionContactDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.common.Preconditions;
import com.atexo.execution.server.common.mapper.ContactMapper;
import com.atexo.execution.server.common.mapper.ContactPrincipalMapper;
import com.atexo.execution.server.common.mapper.ContactReferantMapper;
import com.atexo.execution.server.mapper.ContratEtablissementMapperV2;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ContactServiceImpl implements ContactService {

    private final static Logger LOG = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    ContactReferentRepository contactReferentRepository;

    @Autowired
    ContactMapper contactMapper;

    @Autowired
    ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    ContactReferantMapper contactReferantMapper;

    @Autowired
    private PlateformeRepository plateformeRepository;

	@Autowired
	private ContactPrincipalMapper contactPrincipalMapper;

    @Override
    public List<ContactDTO> getContactByIdIn(Long[] ids) {
        List<Contact> contacts = contactRepository.findByIdIn(ids);
        if (contacts == null) {
            return null;
        }
        return contacts.stream().map(contact -> contactMapper.toDTO(contact)).collect(Collectors.toList());
    }

    public ContactDTO ajouterContact(EditionContactDTO editionContactDTO, String plateformeUid) throws ApplicationException {
        Plateforme plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new ApplicationException("Plateforme " + plateformeUid + " non trouvée"));

        Preconditions.checkArgument(editionContactDTO != null);

        Contact contact = new Contact();
        contact.setNom(editionContactDTO.getNom().toUpperCase());

        contact.setEtablissement(etablissementRepository.findOneById(editionContactDTO.getEtablissement().getId()));

        if (editionContactDTO.getPrenom() != null) {
            contact.setPrenom(editionContactDTO.getPrenom());
        }
        if (editionContactDTO.getEmail() != null) {
            contact.setEmail(editionContactDTO.getEmail());
        }
        if (editionContactDTO.getFonction() != null) {
            contact.setFonction(editionContactDTO.getFonction());
        }
        if (editionContactDTO.getTelephone() != null) {
            contact.setTelephone(editionContactDTO.getTelephone());
        }
        contact.setActif(true);
        contact.setPlateforme(plateforme);
        // enregistrement
        Contact contactResult = contactRepository.save(contact);

        LOG.info("le contact {} a été ajouté et lié à l'établissement", contact);

        return contactMapper.toDTO(contactResult);
    }

    public ContactDTO modifierContact(EditionContactDTO editionContactDTO) throws ApplicationBusinessException {

        Preconditions.checkArgument(editionContactDTO != null);
        Contact contact = contactRepository.findOneById(editionContactDTO.getId());
        String email = contact.getEmail();
        //récupérer tous les contacts doublons pour tous les etablissements doublons
        List<Etablissement> etablissements = etablissementRepository.findAllBySiretAndPlateformeId(contact.getEtablissement().getSiret(), contact.getPlateforme().getId());
        for (Etablissement etablissement : etablissements) {
            List<Contact> contactsToUpdateEtablissement = contactRepository.findAllByEtablissementIdAndEmailAndPlateformeId(etablissement.getId(), email, contact.getPlateforme().getId());
            for (Contact contactToUpdate : contactsToUpdateEtablissement) {
                contactToUpdate.setNom(editionContactDTO.getNom().toUpperCase());
                contactToUpdate.setEtablissement(etablissement);
                if (editionContactDTO.getPrenom() != null) {
                    contactToUpdate.setPrenom(editionContactDTO.getPrenom());
                }
                if (editionContactDTO.getEmail() != null) {
                    contactToUpdate.setEmail(editionContactDTO.getEmail());
                }
                if (editionContactDTO.getFonction() != null) {
                    contactToUpdate.setFonction(editionContactDTO.getFonction());
                }
                if (editionContactDTO.getTelephone() != null) {
                    contactToUpdate.setTelephone(editionContactDTO.getTelephone());
                }
                contactRepository.saveAndFlush(contactToUpdate);
            }
        }

        contact.setNom(editionContactDTO.getNom().toUpperCase());

        Etablissement etablissement = etablissementRepository.findOneById(editionContactDTO.getEtablissement().getId());
        contact.setEtablissement(etablissement);

        if (editionContactDTO.getPrenom() != null) {
            contact.setPrenom(editionContactDTO.getPrenom());
        }
        if (editionContactDTO.getEmail() != null) {
            contact.setEmail(editionContactDTO.getEmail());
        }
        if (editionContactDTO.getFonction() != null) {
            contact.setFonction(editionContactDTO.getFonction());
        }
        if (editionContactDTO.getTelephone() != null) {
            contact.setTelephone(editionContactDTO.getTelephone());
        }
        // enregistrement
        Contact contactResult = contactRepository.saveAndFlush(contact);

        LOG.info("Le contact {} a été modifié", contact);

        return contactMapper.toDTO(contactResult);
    }

    @Override
    public ContactDTO supprimerContact(Long contactId) {
        Contact contact = contactRepository.findOneById(contactId);
        //suppression du contact et de ses doublons pour tous les etablissements doublons
        List<Etablissement> etablissements = etablissementRepository.findAllBySiretAndPlateformeId(contact.getEtablissement().getSiret(), contact.getPlateforme().getId());
        for (Etablissement etablissement : etablissements) {
            List<Contact> contactsToDeleteEtablissement = contactRepository.findAllByEtablissementIdAndEmailAndPlateformeId(etablissement.getId(), contact.getEmail(), contact.getPlateforme().getId());
            for (Contact contactToDelete : contactsToDeleteEtablissement) {
                contactToDelete.setActif(false);
                contactRepository.saveAndFlush(contactToDelete);
            }
        }
        contact.setActif(false);
        return contactMapper.toDTO(contact);
    }

    @Override
    public List<ContactReferantDTO> getContactByContratEtablissement(Long contratEtablissementId) {
        var contacts = contratEtablissementRepository.findById(contratEtablissementId).map(ContratEtablissement::getContactReferantList).orElse(new HashSet<>());
        return contacts.stream().map(contact -> contactReferantMapper.toDTO(contact)).collect(Collectors.toList());
    }

    @Override
    public ContactReferantDTO modifierContactReferant(ContactReferantDTO contactReferantDTO) throws ApplicationBusinessException {
        Contact contact = contactRepository.findOneById(contactReferantDTO.getContact().getId());
        var contactReferant = contactReferentRepository.findById(new ContactReferantId(contact.getId(), contactReferantDTO.getContratEtablissement().getId())).orElse(null);

        contact.setEmail(contactReferantDTO.getContact().getEmail());
        contact.setNom(contactReferantDTO.getContact().getNom());
        contact.setPrenom(contactReferantDTO.getContact().getPrenom());
        contact.setTelephone(contactReferantDTO.getContact().getTelephone());
        contact.setFonction(contactReferantDTO.getContact().getFonction());

        if (contactReferant != null) {
            contactReferant.setRoleContrat(contactReferantDTO.getRoleContrat());
            contactReferant = contactReferentRepository.save(contactReferant);
        }
        contactRepository.saveAndFlush(contact);
        return contactReferantMapper.toDTO(contactReferant);
    }

    @Override
    public boolean supprimerContactReferant(Long contratEtablissementId, Long contactId) {
        var id = ContactReferantId.builder().idContact(contactId).idContratEtablissement(contratEtablissementId).build();
        contactReferentRepository.findById(id).ifPresent(contactReferentRepository::delete);
        return true;
    }

    @Override
    public ContactReferantDTO ajouterContactReferant(ContactReferantDTO contactReferantDTO, String plateformeUid) throws ApplicationException {
        Plateforme plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new ApplicationException("Plateforme " + plateformeUid + " non trouvée"));

        var contactId = contactReferantDTO.getContact().getId();
        var contact = contactId == null ? null : contactRepository.findById(contactId).orElse(null);
        if (contact == null) {
            contact = contactMapper.createEntity(contactReferantDTO.getContact());
            contact.setActif(Boolean.TRUE);
            if (contactReferantDTO.getContact().getEtablissement() != null) {
                contact.setEtablissement(etablissementRepository.findOneById(contactReferantDTO.getContact().getEtablissement().getId()));
            }
            contact.setPlateforme(plateforme);
            contact = contactRepository.saveAndFlush(contact);
        }


        // contact referent
        ContactReferant contactRef = null;
        if (contactReferantDTO.getContratEtablissement() != null) {
            var id = ContactReferantId.builder().idContact(contact.getId()).idContratEtablissement(contactReferantDTO.getContratEtablissement().getId()).build();
            contactRef = contactReferentRepository.findById(id).orElse(new ContactReferant());
            contactRef.setId(id);
            contactRef.setRoleContrat(contactReferantDTO.getRoleContrat());
            contactRef.setContratEtablissement(contratEtablissementRepository.findById(contactReferantDTO.getContratEtablissement().getId()).orElseThrow(() -> new ApplicationBusinessException()));
            contactRef.setContact(contact);
            contactRef = contactReferentRepository.save(contactRef);

        } else {
            LOG.warn("aucun établissement pour le contact {} => {}", contact.getId(), contact.getEmail());
        }
        return contactReferantMapper.toDTO(contactRef);
    }

    @Override
    public List<ContactDTO> getContactsByEtablissement(Long etablissementId) {
        return contactRepository.findByEtablissementId(etablissementId).stream().map(contact -> contactMapper.toDTO(contact)).collect(Collectors.toList());
    }

    @Override
    public ContactPrincipalDTO modifier(ContactPrincipalDTO contactPrincipalDTO, String plateformeUid) throws ApplicationException {
        var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new ApplicationException("Plateforme " + plateformeUid + " non trouvée"));

        var contact = contactRepository.findOneById(contactPrincipalDTO.getContact().getId());

        if (!plateforme.getUid().equalsIgnoreCase(contact.getPlateforme().getUid())) {
            throw new ApplicationException("Le contact avec l'identifiant "+contact.getId()+" n'appartient pas a la plateforme de l'utilisateur ("+ plateforme.getUid()+")");
        }

        contact.setEmail(contactPrincipalDTO.getContact().getEmail());
        contact.setNom(contactPrincipalDTO.getContact().getNom());
        contact.setPrenom(contactPrincipalDTO.getContact().getPrenom());
        contact.setTelephone(contactPrincipalDTO.getContact().getTelephone());
        contact.setFonction(contactPrincipalDTO.getContact().getFonction());
        contact.setPlateforme(plateforme);
        contactRepository.saveAndFlush(contact);

        var contratEtablissement = contratEtablissementRepository.findById(contactPrincipalDTO.getContratEtablissement().getId()).orElseThrow(ApplicationBusinessException::new);

        return contactPrincipalMapper.toDTO(contratEtablissement);
    }

    @Override
    public ContactPrincipalDTO ajouter(ContactPrincipalDTO contactPrincipalDTO, String plateformeUid) throws ApplicationException {
        var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new ApplicationException("Plateforme " + plateformeUid + " non trouvée"));

        var contactId = contactPrincipalDTO.getContact().getId();
        var contact = contactId == null ? null : contactRepository.findById(contactId).orElse(null);

        if (null == contact) {
            contact = contactMapper.createEntity(contactPrincipalDTO.getContact());
            contact.setActif(Boolean.TRUE);
            if (contactPrincipalDTO.getContact().getEtablissement() != null) {
                contact.setEtablissement(etablissementRepository.findOneById(contactPrincipalDTO.getContact().getEtablissement().getId()));
            }
            contact.setPlateforme(plateforme);
            contactRepository.saveAndFlush(contact);
        }

        var contratEtablissement = contratEtablissementRepository.findById(contactPrincipalDTO.getContratEtablissement().getId()).orElseThrow(ApplicationBusinessException::new);

        contratEtablissement.setContact(contact);

        contratEtablissementRepository.saveAndFlush(contratEtablissement);

        return contactPrincipalMapper.toDTO(contratEtablissement);
    }

    @Override
    public ContactPrincipalDTO recuperer(Long attributaireId, String plateformeUid) throws ApplicationException {
        var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new ApplicationException("Plateforme " + plateformeUid + " non trouvée"));

        var contratEtablissement = contratEtablissementRepository.findById(attributaireId).orElseThrow(ApplicationBusinessException::new);

        if (!plateforme.getUid().equalsIgnoreCase(contratEtablissement.getPlateforme().getUid())) {
            throw new ApplicationException("L'attributaire avec l'identifiant "+contratEtablissement.getId()+" n'appartient pas a la plateforme de l'utilisateur ("+ plateforme.getUid()+")");
        }

        return contactPrincipalMapper.toDTO(contratEtablissement);
    }
}
