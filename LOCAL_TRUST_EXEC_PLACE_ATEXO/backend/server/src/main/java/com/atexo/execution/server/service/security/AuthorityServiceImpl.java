package com.atexo.execution.server.service.security;

import com.atexo.execution.server.model.security.Authority;
import com.atexo.execution.server.repository.security.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	AuthorityRepository repository;

	@Override
	public List<Authority> recuperer() {
		return repository.findAll();
	}

	@Override
	public Optional<Authority> trouver(String name) {
		return repository.findByIdExterne(name);
	}

	@Override
	public Authority sauvegarder(Authority authority) {
		return repository.save(authority);
	}

	@Override
	public List<Authority> trouver(List<String> idsExternes) {
		var authorities = new ArrayList<Authority>();
		for (var idExterne : idsExternes) {
			repository.findByIdExterne(idExterne).ifPresent(authorities::add);
		}
		return authorities;
	}
}
