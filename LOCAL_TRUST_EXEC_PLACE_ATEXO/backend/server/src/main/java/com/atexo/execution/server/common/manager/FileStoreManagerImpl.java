package com.atexo.execution.server.common.manager;

import com.atexo.execution.common.exception.ApplicationTechnicalException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Component
public class FileStoreManagerImpl implements FileStoreManager {

	@Autowired
	PathsConfigurationManager pathsConfigurationManager;

	@Override
    public File createFileForStorage(String extension) {
        StringBuilder dstFileNameBuilder = new StringBuilder();
        dstFileNameBuilder.append(UUID.randomUUID());
        dstFileNameBuilder.append(".");
        if (extension != null) {
            dstFileNameBuilder.append(extension);
        } else {
            dstFileNameBuilder.append("dat");
        }
        File dstFile = new File(getFilePath(dstFileNameBuilder.toString()));
        return dstFile;
	}

	@Override
	public File storeFile(File file, boolean deleteOriginal) throws ApplicationTechnicalException {
		File dstFile = createFileForStorage(FilenameUtils.getExtension(file.getName()));
		try {
			FileUtils.copyFile(file, dstFile);
			if (deleteOriginal) {
				file.delete();
			}
		} catch (IOException e) {
			throw new ApplicationTechnicalException(e);
		}
		return dstFile;
	}

	@Override
	public Optional<File> getFile(String name) {
		File file = new File(getFilePath(name));
		if (file.exists()) {
			return Optional.of(file);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public void deleteFile(String name) {
		File file = new File(getFilePath(name));
		file.delete();
	}

	/**
	 * @param name
	 * @return
	 */
	private String getFilePath(String name) {
		return pathsConfigurationManager.getFileStorePath() + File.separator + name;
	}

}
