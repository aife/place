package com.atexo.execution.server.common.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by MZO on 02/03/2016.
 */
@Component
public class UrlsConfigManagerImpl implements UrlsConfigManager {

    @Value("${application.backendContext}")
    String backendContext;

    @Value("${docgen.url}")
    String docgenUrl;

    @Value("${ressources.url}")
    String ressourcesUrl;

    @Value("${editionEnLigne.url}")
    String editionEnLigneUrl;

    @Value("${editionEnLigneWS.url}")
    String editionEnLigneWSUrl;

    @Value("${mpe.url}")
    String mpeUrl;

    @Override
    public String getEditionEnLigneUrl() {
        return editionEnLigneUrl;
    }

    @Override
    public String getEditionEnLigneWSUrl() {
        return editionEnLigneWSUrl;
    }

    @Override
    public String getBackendContext() {
        return backendContext;
    }

    @Override
    public String getMpeUrl() {
        return mpeUrl;
    }

    @Override
    public String getDocgenUrl() {
        return docgenUrl;
    }

    @Override
    public String getRessourcesUrl() {
        return ressourcesUrl;
    }
}
