package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.PairDTO;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.common.manager.upload.UploadFile;
import com.atexo.execution.server.common.manager.upload.UploadManager;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.imports.ContractantImportService;
import com.atexo.execution.server.service.imports.ContratImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/import")
public class ImportController {

    @Autowired
    UploadManager uploadManager;

    @Autowired
    ContratImportService contratImportService;

    @Autowired
    ContractantImportService contractantImportService;

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping(value = "/testContrat/{reference}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PairDTO<Integer, List<String>>> testImportFichierContrat(@PathVariable String reference, @RequestHeader("User-Uuid") final String utilisateurUuid) throws ApplicationTechnicalException {

        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var pfUid = utilisateur.getPlateformeUid();

        if ( fileOpt.isEmpty() ) {
            throw new ApplicationTechnicalException();
        }

        UploadFile uploadFile = fileOpt.get();
        PairDTO<Integer, List<String>> res = contratImportService.importFichier(uploadFile.getFile(), true, pfUid);
        if (res == null) {
            List<String> messagesError = new ArrayList<>();
            messagesError.add("Une erreur technique est survenue");
            return new ResponseEntity<>(new PairDTO<>(0, messagesError), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (res.getSecond().isEmpty()) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/importContrat/{reference}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PairDTO<Integer, List<String>>> importFichierContrat(@PathVariable String reference, @RequestHeader("User-Uuid") final String utilisateurUuid) throws ApplicationTechnicalException {

        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var pfUid = utilisateur.getPlateformeUid();

        if ( fileOpt.isEmpty() ) {
            throw new ApplicationTechnicalException();
        }

        UploadFile uploadFile = fileOpt.get();
        PairDTO<Integer, List<String>> res = contratImportService.importFichier(uploadFile.getFile(), false, pfUid);
        if (res == null) {
            List<String> messagesError = new ArrayList<>();
            messagesError.add("Une erreur technique est survenue");
            return new ResponseEntity<>(new PairDTO<>(0, messagesError), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (res.getFirst() != 0) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/testContractant/{reference}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PairDTO<Integer, List<String>>> testImportFichierContractant(@PathVariable String reference, @RequestHeader("User-Uuid") final String utilisateurUuid) throws ApplicationTechnicalException {

        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);

        if ( fileOpt.isEmpty() ) {
            throw new ApplicationTechnicalException();
        }
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var pfUid = utilisateur.getPlateformeUid();

        UploadFile uploadFile = fileOpt.get();
        PairDTO<Integer, List<String>> res = contractantImportService.importFichier(uploadFile.getFile(), true, pfUid);
        if (res == null) {
            List<String> messagesError = new ArrayList<>();
            messagesError.add("Une erreur technique est survenue");
            return new ResponseEntity<>(new PairDTO<>(0, messagesError), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (res.getSecond().isEmpty()) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/importContractant/{reference}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PairDTO<Integer, List<String>>> importFichierContractant(@PathVariable String reference, @RequestHeader("User-Uuid") final String utilisateurUuid) throws ApplicationTechnicalException {

        Optional<UploadFile> fileOpt = uploadManager.getFile(reference);

        if ( fileOpt.isEmpty() ) {
            throw new ApplicationTechnicalException();
        }
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        var pfUid = utilisateur.getPlateformeUid();

        UploadFile uploadFile = fileOpt.get();
        PairDTO<Integer, List<String>> res = contractantImportService.importFichier(uploadFile.getFile(), false, pfUid);
        if (res == null) {
            List<String> messagesError = new ArrayList<>();
            messagesError.add("Une erreur technique est survenue");
            return new ResponseEntity<>(new PairDTO<>(0, messagesError), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (res.getFirst() != 0) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }
}
