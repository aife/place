package com.atexo.execution.server.service.fournisseur.exceptions;

import com.atexo.execution.common.exception.ApplicationException;

public class FournisseurExistantException extends ApplicationException {

    private final String siren;

    public FournisseurExistantException(String siren) {
        super("Le siren {0} existe déjà", siren);

        this.siren = siren;
    }

    public String getSiren() {
        return siren;
    }
}
