package com.atexo.execution.server.service.security;

import com.atexo.execution.server.model.security.Authority;

import java.util.List;
import java.util.Optional;

public interface AuthorityService {

	List<Authority> recuperer();

	Optional<Authority> trouver(String name);

	List<Authority> trouver(List<String> idsExternes);

	Authority sauvegarder(Authority authority);
}
