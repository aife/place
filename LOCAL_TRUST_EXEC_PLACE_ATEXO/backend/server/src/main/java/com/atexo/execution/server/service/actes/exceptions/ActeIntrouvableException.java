package com.atexo.execution.server.service.actes.exceptions;

import com.atexo.execution.common.exception.ApplicationException;

public class ActeIntrouvableException  extends ApplicationException {

	private final Long id;

	public ActeIntrouvableException( Long id ) {
		super("L'acte {0} est introuvable", id);

		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
