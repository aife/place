package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.EchangeChorusCriteriaDTO;
import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.actes.exceptions.ActeIntrouvableException;
import com.atexo.execution.server.service.chorus.ChorusService;
import com.atexo.execution.server.service.chorus.exceptions.EchangeChorusIntrouvableException;
import com.atexo.execution.server.service.chorus.exceptions.EnvoiZipChorusException;
import com.atexo.execution.server.service.chorus.exceptions.PublicationChorusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@RestController
@RequestMapping(value = "/api/echanges-chorus")
public class EchangeChorusController {

	private static final Logger LOG = LoggerFactory.getLogger(EchangeChorusController.class);

	public static final String APPLICATION_ZIP = "application/zip";

	@Autowired
	UtilisateurService utilisateurService;

	@Autowired
	ContratRepository contratRepository;

	@Autowired
	EchangeChorusRepository echangeChorusRepository;

	@Autowired
	ActeRepository acteRepository;

	@Autowired
	ChorusService chorusService;

	@Autowired
	EchangeChorusMapper echangeChorusMapper;

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<EchangeChorusDTO>> find(
			@RequestHeader("User-Uuid") final String utilisateurUuid,
			@RequestBody final EchangeChorusCriteriaDTO criteria,
			final Pageable pageable
	) {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		return new ResponseEntity<>(chorusService.rechercher(utilisateur.getId(), criteria, pageable, utilisateur.getPlateformeUid()), HttpStatus.OK);
	}

	@GetMapping("/zip/chorus/{idEchangeChorus}")
    public ResponseEntity<InputStreamResource> downloadEchangeZip(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable Long idEchangeChorus) throws ApplicationException, FileNotFoundException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
		var echangeChorus = this.echangeChorusRepository.findById(idEchangeChorus).orElseThrow(()->new EchangeChorusIntrouvableException(idEchangeChorus));

        var zip = chorusService.telechargerZip(utilisateur.getPlateformeUid(), echangeChorus);
		if (zip == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		var inputStream = new FileInputStream(zip);
		return ResponseEntity.ok().header("Content-Type", "application/zip").body(new InputStreamResource(inputStream));
	}

    @GetMapping("/zip/contrat/{idContrat}")
    public ResponseEntity<InputStreamResource> downloadContratZip(@PathVariable Long idContrat) throws ApplicationException, FileNotFoundException {
		var contrat = this.contratRepository.findById(idContrat).orElseThrow(() -> new ApplicationException("Constrat introuvable " + idContrat));

        return ResponseEntity.ok()
	        .header(CONTENT_TYPE, APPLICATION_ZIP)
	        .header(CONTENT_DISPOSITION, "attachment; filename="+"echanges-chorus-"+contrat.getNumero()+".zip")
	        .body(new InputStreamResource(new FileInputStream(chorusService.telechargerZip(contrat))));
    }

	@GetMapping("/zip/acte/{idActe}")
	public ResponseEntity<InputStreamResource> downloadActeZip(@PathVariable Long idActe) throws ApplicationException, FileNotFoundException {
		var acte = this.acteRepository.findById(idActe).orElseThrow(()->new ActeIntrouvableException(idActe));

		return ResponseEntity.ok()
			.header(CONTENT_TYPE, APPLICATION_ZIP)
			.header(CONTENT_DISPOSITION, "attachment; filename="+"echanges-chorus-"+acte.getNumero()+".zip")
			.body(new InputStreamResource(new FileInputStream(chorusService.telechargerZip(acte))));
	}

	@PostMapping(value = "/soumettre/{idActe}")
	@Transactional(rollbackFor = Exception.class)
    public ResponseEntity<EchangeChorusDTO> soumettre(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable Long idActe, @RequestBody EchangeChorusDTO echangeChorusDTO) throws ApplicationException {
        var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);
        return ResponseEntity.ok(echangeChorusMapper.toDto(chorusService.soumettre(utilisateur.getPlateformeUid(), idActe, echangeChorusDTO.getId(), echangeChorusDTO.getDateNotificationPrevisionnelle())));
	}

	@PostMapping(value = "/ressoumettre/{idActe}")
	public EchangeChorusDTO soumettre(@RequestHeader("User-Uuid") final String utilisateurUuid, @PathVariable Long idActe) throws PublicationChorusException, ActeIntrouvableException, EnvoiZipChorusException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		var echange = chorusService.ressoumettre(utilisateur.getPlateformeUid(), idActe);

		return echangeChorusMapper.toDto(echange);
	}
}
