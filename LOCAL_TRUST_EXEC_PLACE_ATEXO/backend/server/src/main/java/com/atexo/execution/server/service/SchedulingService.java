package com.atexo.execution.server.service;

import com.atexo.execution.server.interf.alerte.AlerteTacheTask;
import com.atexo.execution.server.service.document.DocumentContratService;
import com.atexo.execution.services.NumerotationService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SchedulingService {

    private final AlerteTacheTask alerteTacheTask;
    private final DocumentContratService documentContratService;
    private final NumerotationService numerotationService;

    @Scheduled(cron = "${cron.expression.alerte:0 0 0/1 * * ?}")
    public void envoyerAlteres() {
        alerteTacheTask.alerte();
    }

    @Scheduled(initialDelayString = "${docgen.scheduler.update-document-status.delay:10000}", fixedRateString = "${docgen.scheduler.update-document-status.rate:10000}")
    public void updateDocumentsStatus() {
       documentContratService.updateDocumentsStatus();
    }

    @Scheduled(cron = "1 0 0 * * ?")
    public void reinitialiserCompteurs() {
        numerotationService.reinitialiserCompteurs();
    }

}
