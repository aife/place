package com.atexo.execution.server.service;

import com.atexo.execution.server.model.AbstractParametragelEntity;
import com.atexo.execution.server.model.ParametrageApplication;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.ParametrageApplicationRepository;
import com.atexo.execution.server.repository.crud.ParametrageMessagerieRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ParametrageServiceImpl implements ParametrageService {

    private final ParametrageApplicationRepository parametrageRepository;
    private final ParametrageMessagerieRepository parametrageMessagerieRepository;
    private final UtilisateurRepository utilisateurRepository;

    @Override
    public List<ParametrageApplication> getAllParametrageExportFrontByUtilisateurUuid(String userUud) {
        var plateforme = utilisateurRepository.findByUuid(userUud).map(Utilisateur::getPlateforme).orElseThrow(()->new IllegalArgumentException("La plateforme n'existe pas pour l'utilisateur "+userUud));
        return parametrageRepository.findByExportFrontTrueAndPlateformeMpeUid(plateforme.getMpeUid());
    }

    @Override
    public boolean isActif(String pfUid, String cle) {
        return parametrageRepository.findByClefAndPlateformeMpeUid(cle, pfUid).map(ParametrageApplication::getValeur).map(Boolean::parseBoolean).orElse(false);
    }

    @Override
    public Map<String, String> getParametrageMessagerie(String pfUid) {
        return parametrageMessagerieRepository.findAllByPlateformeUid(pfUid).stream().collect(
                java.util.stream.Collectors.toMap(
                        AbstractParametragelEntity::getClef,
                        AbstractParametragelEntity::getValeur
                )
        );
    }
}
