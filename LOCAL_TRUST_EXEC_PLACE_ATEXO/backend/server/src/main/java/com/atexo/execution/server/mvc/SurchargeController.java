package com.atexo.execution.server.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/surcharge")
public class SurchargeController {

    @Value("${mpe.url}")
    String urlMpe;

    @Value("${mpe.header.path:/header/exec}")
    String headerPath;

    @Value("${mpe.footer.path:/footer/agent/exec}")
    String footerPath;

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping(value = "/header")
    public ResponseEntity<String> getHeader() {
        String pattern = "{url}/{headerPath}";
        String url = buildUrl(pattern);
        return ResponseEntity.ok(restTemplate.getForEntity(url, String.class).getBody());
    }

    @GetMapping(value = "/footer")
    public ResponseEntity<String> getFooter() {
        String pattern = "{url}/{footerPath}";
        String url = buildUrl(pattern);
        return ResponseEntity.ok(restTemplate.getForEntity(url, String.class).getBody());
    }

    private String buildUrl(String pattern) {
        // ajout des url header et footer
        Map<String, Object> map = new HashMap<>();
        map.put("url", urlMpe);
        map.put("headerPath", headerPath);
        map.put("footerPath", footerPath);
        URI url = new UriTemplate(pattern).expand(map).normalize();
        return url.toString();
    }

}
