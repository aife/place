package com.atexo.execution.server.common.manager;

/**
 * Created by MZO on 02/03/2016.
 */
public interface UrlsConfigManager {

    String getDocgenUrl();
    String getRessourcesUrl();

    String getEditionEnLigneUrl();

    String getEditionEnLigneWSUrl();

    String getBackendContext();

    String getMpeUrl();


}
