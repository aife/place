package com.atexo.execution.server.interf.basecentrale;

import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Fournisseur;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public interface BaseCentraleInterface {

    /**
     * Recheche une entreprise à partir du SIREN.
     *
     * @param siren : SIREN de l'entreprise.
     * @return Fournisseur : Infos de l'entreprise recupérées depuis SGMAP.
     */
    Optional<Fournisseur> getEntrepriseBySiren(String siren) throws ApplicationTechnicalException;

    /**
     * Recherche un établissement à partir de son SIRET.
     *
     * @param siret : SIRET de l'établissement.
     * @return Etablissement : Informations sur l'établissement.
     */
    Optional<Etablissement> getEtablissementBySiret(String siret) throws ApplicationTechnicalException;

    /**
     * Récupère la liste des attestations à partir du SIREN ou du SIRET
     *
     * @param id Siren ou Siret de l'entreprise
     * @return JSON seulement désérialisé en map clé-valeur
     * @throws ApplicationTechnicalException
     */
    Optional<Map<String, Object>> getDocumentsBySiren(String id);

    Optional<Map<String, Object>> getDocumentsBySiret(String id);

    File zipAttestations(String sirenOrSiret, Map<String, String> attestations) throws IOException;

    String getCurrentSiret();
}
