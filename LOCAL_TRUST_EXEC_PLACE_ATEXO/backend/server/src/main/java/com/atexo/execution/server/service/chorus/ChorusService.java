package com.atexo.execution.server.service.chorus;

import com.atexo.execution.common.dto.EchangeChorusCriteriaDTO;
import com.atexo.execution.common.dto.EchangeChorusDTO;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.service.actes.exceptions.ActeIntrouvableException;
import com.atexo.execution.server.service.chorus.exceptions.EnvoiZipChorusException;
import com.atexo.execution.server.service.chorus.exceptions.PublicationChorusException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.time.LocalDateTime;

public interface ChorusService {
    Page<EchangeChorusDTO> rechercher(Long id, EchangeChorusCriteriaDTO criteria, Pageable pageable, String pfUid);

    File telechargerZip(String plateformeUid, EchangeChorus echangeChorus) throws ApplicationException;

    File telechargerZip( Contrat contrat) throws ApplicationException;

	File telechargerZip( Acte acte) throws ApplicationException;

    EchangeChorus soumettre(String plateformeUid, Long idActe, Long idEchangeChorus, LocalDateTime dateNotificationPrevisionnelle) throws ApplicationException;

	EchangeChorus ressoumettre(String plateformeUid, Long idActe) throws ActeIntrouvableException, PublicationChorusException, EnvoiZipChorusException;
}
