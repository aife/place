package com.atexo.execution.server.service.imports;

import com.atexo.execution.common.def.TypeBorne;
import com.atexo.execution.common.def.TypeGroupement;
import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.PairDTO;
import com.atexo.execution.common.dto.imports.ContratDTOImport;
import com.atexo.execution.server.common.imports.ImportUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.referentiels.CategorieConsultationRepository;
import com.atexo.execution.server.repository.referentiels.FormePrixRepository;
import com.atexo.execution.server.repository.referentiels.TypeContratRepository;
import com.atexo.execution.server.service.EtablissementService;
import com.atexo.execution.services.ContratsService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atexo.execution.server.common.imports.ImportUtils.CHAMPS_CONTRAT;
import static com.google.common.base.Enums.getIfPresent;

/**
 * Service d'import de contrats via fichier XLS.
 */
@Service
@Transactional
public class ContratImportServiceImpl extends AbstractImportService<ContratDTOImport> implements ContratImportService {

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private FormePrixRepository formePrixRepository;

    @Autowired
    private TypeContratRepository typeContratRepository;

    @Autowired
    private CategorieConsultationRepository categorieConsultationRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private OrganismeRepository organismeRepository;

    @Autowired
    private EtablissementService etablissementService;

    @Autowired
    private EtablissementRepository etablissementRepository;

    @Autowired
    private ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private ContratEtablissementGroupementRepository contratEtablissementGroupementRepository;

    @Autowired
    private ContratsService contratsService;

    @Override
    protected ContratDTOImport getInstance() {
        return new ContratDTOImport();
    }

    @Override
    protected void headerCellCheck(Cell cell) {
        if (!CHAMPS_CONTRAT.containsKey(cell.getStringCellValue())) {
            messages.add("Le champ \"" + cell.getStringCellValue() + "\" n'est pas un champ technique");
        }
    }

    @Override
    protected void setValeur(ContratDTOImport contratDTOImport, String header, Cell cell, int rowNum, int colNum, String pfUid) {
        // check pour les test obligatoire et le type si erreur ajouter le message
        String testValide = testTypeAndObligatoire(cell, header, rowNum, colNum, pfUid);
        if (testValide != null) {
            messagesRow.add(testValide);
        } else {
            setValeurToDTO(contratDTOImport, header, cell);
        }
    }

    @Override
    protected void flushMessagesRow(ContratDTOImport contratDTOImport) {
        for (String messageRow : messagesRow) {
            StringBuilder sb = new StringBuilder("Contrat ");
            if (contratDTOImport.getReferenceLibre() != null) {
                sb.append(contratDTOImport.getReferenceLibre());
                sb.append(" ");
            }
            sb.append(": ");
            messages.add(sb + messageRow);
        }
    }

    @Override
    protected void customDTOCheck(List<ContratDTOImport> contratDTOImports) {
        for (int i = 0; i < contratDTOImports.size(); i++) {
            //Check le couplet siret et reference libre n'existe pas déja la base
            if (isAttributaireAndNumeroContratExist(contratDTOImports.get(i).getReferenceLibre(), contratDTOImports.get(i).getSiret())) {
                messages.add("Ligne " + i + ", Le couple Numéro de contrat (" + contratDTOImports.get(i).getReferenceLibre() +
                        ") et SIRET Attributaire (" + contratDTOImports.get(i).getSiret() + ") existe déjà en base de données");
            }
            // Check le nom et prenom des createur exist dans la base
            if (!isNomAndPrenomExist(contratDTOImports.get(i).getCreateurNom(), contratDTOImports.get(i).getCreateurPrenom())) {
                messages.add("Ligne " + i + ", L'agent " + contratDTOImports.get(i).getCreateurNom() + " "
                        + contratDTOImports.get(i).getCreateurPrenom() + " n'existe pas en base de données");
            }
            // check les tests ObligatoireConditionne
            // messages.addAll(checkObligatoireConditionnel(contratDTOImports.get(i)));
            // Check les doublons
            for (int j = 0; j < contratDTOImports.size(); j++) {
                if (i != j && contratDTOImports.get(i).equals(contratDTOImports.get(j))) {
                    messages.add("Ligne " + i + ", Le couple (Siret : " + contratDTOImports.get(j).getSiret() + ", Numéro de contrat : "
                            + contratDTOImports.get(j).getReferenceLibre() + ") doit être unique");
                }
            }
        }
    }

    @Override
    protected void save(List<ContratDTOImport> dtoList, String pfUid) {
        var threadPool = getThreadPool();
        for (ContratDTOImport contratDTOImport : dtoList) {
            threadPool.submit(() -> {
                try {
                    process(contratDTOImport, pfUid);
                } catch (Exception e) {
                    LOG.error("Erreur lors de l'import des contrats", e);
                    messages.add("Erreur lors de l'import des contrats, contrat : " + contratDTOImport.getReferenceLibre());
                }
            });
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (Exception e) {
            LOG.error("Erreur lors de l'import des contrats", e);
        }
    }

    private ContratDTOImport process(ContratDTOImport contratDTOImport, String pfUid) {
        LOG.info("Import du contrat : " + contratDTOImport.getReferenceLibre());
        Contrat contrat = mapContratDTOImportToContrat(contratDTOImport, pfUid);
        if (contrat != null) {
            Consultation consultation = contrat.getConsultation();
            consultation = consultationRepository.save(consultation);
            contrat.setConsultation(consultation);
            ContratEtablissement attributaire = contrat.getAttributaire();
            contrat.updateContratStatus();

            if (contrat.getGroupement() != null) {
                ContratEtablissementGroupement groupement = contrat.getGroupement();
                groupement = contratEtablissementGroupementRepository.save(groupement);
                contrat.setGroupement(groupement);
            }
            attributaire = contratEtablissementRepository.save(attributaire);
            contrat.setAttributaire(attributaire);
            contrat.setContratEtablissements(new HashSet<>());
            contrat.getContratEtablissements().add(attributaire);
            contrat = contratRepository.save(contrat);
            contratsService.calculStatutContratChapeau(contrat.getContratChapeau());
            return contratDTOImport;
        }
        return null;
    }

    private String testTypeAndObligatoire(Cell cell, String name, int rowNum, int colNum, String pfUid) {
        PairDTO<String, Boolean> regle = CHAMPS_CONTRAT.get(name);
        String prefixMess = "Le champ ";
        // Check si la valeur est obligatoire
        if (regle.getSecond()) {
            if (cell == null) {
                return prefixMess + new CellAddress(rowNum, colNum).formatAsString() + " est obligatoire";
            } else if (cell.getCellTypeEnum() == CellType.BLANK) {
                return prefixMess + new CellAddress(cell).formatAsString() + " est obligatoire";
            }
        }
        // Check la valeur
        if (cell != null && cell.getCellTypeEnum() != CellType.BLANK) {
            if ("String".equalsIgnoreCase(regle.getFirst())) {

                if (cell.getCellTypeEnum() != CellType.STRING) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " doit être au format texte";
                }
            }
            if ("Enum".equalsIgnoreCase(regle.getFirst())) {

                if (cell.getCellTypeEnum() != CellType.STRING || !isEnumOrReferentielExist(cell.getStringCellValue(), name)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas une valeur " +
                            "admissible du référentiel";
                }
            }
            if ("Double".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getCellTypeEnum() != CellType.NUMERIC) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " doit être au format numérique";
                }
            }

            if ("DATE".equalsIgnoreCase(regle.getFirst())) {
                if (!DateUtil.isCellDateFormatted(cell)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas au format date";
                }
            }

            if ("Boolean".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getStringCellValue().length() != 1 || !cell.getStringCellValue().matches("[O|N]")) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " Groupement O/N ne peut contenir que la valeur O ou N";
                }
            }
            if ("Siret".equalsIgnoreCase(regle.getFirst())) {
                String stringCellValue = DATA_FORMATTER.formatCellValue(cell);
                if (stringCellValue.length() != 14) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " SIRET doit être composé de 14 chiffres";
                }
                if (!ImportUtils.isSiretSyntaxValide(stringCellValue)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " SIRET doit respecter l'algorithme de Luhn";
                }

            }
            if ("Organisme".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getCellTypeEnum() != CellType.STRING || !isAcronymeExists(cell.getStringCellValue(), pfUid)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas un acronyme d'un organisme existant en base de données";
                }
            }

            if ("service".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getCellTypeEnum() != CellType.STRING || !isNomServiceExists(cell.getStringCellValue(), pfUid)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas un service existant en base de données";
                }
            }


        }

        return null;
    }

    private boolean isEnumOrReferentielExist(String value, String name) {
        if ("type".equalsIgnoreCase(name) && typeContratRepository.findOneByCode(value) == null) {
            return false;
        }
        if ("groupement.type".equalsIgnoreCase(name) && !getIfPresent(TypeGroupement.class, value).isPresent()) {
            return false;
        }
        return !("consultation.categorie".equalsIgnoreCase(name) && categorieConsultationRepository.findOneByCode(value) == null);

    }

    private boolean isAcronymeExists(String acro, String pfUid) {
        return organismeRepository.findOneByAcronymeAndPlateformeMpeUid(acro, pfUid).isPresent();
    }

    private boolean isNomServiceExists(String nomService, String pfUid) {
        return serviceRepository.findFirstByNomAndPlateformeMpeUid(nomService, pfUid).isPresent();
    }

    private boolean isAttributaireAndNumeroContratExist(String referenceLibre, String siret) {
        return contratRepository.findByReferenceLibreAndAttributaireEtablissementSiret(referenceLibre, siret).isPresent();
    }

    private boolean isNomAndPrenomExist(String nom, String prenom) {
        return utilisateurRepository.findFirstByNomAndPrenom(nom, prenom).isPresent();
    }

    private List<String> checkObligatoireConditionnel(ContratDTOImport contratDTOImport) {
        List<String> conditionalMessages = new ArrayList<>();
        if ("MAXIMALE".equals(contratDTOImport.getTypeBorne()) && contratDTOImport.getBorneMaximale() == null) {
            conditionalMessages.add("Le champ Borne maximale est obligatoire pour le contrat : " + contratDTOImport.getReferenceLibre());
        }
        if ("MINIMALE".equals(contratDTOImport.getTypeBorne()) && contratDTOImport.getBorneMinimale() == null) {
            conditionalMessages.add("Le champ Borne minimale est obligatoire pour le contrat : " + contratDTOImport.getReferenceLibre());
        }

        if ("MINIMALE_MAXIMALE".equals(contratDTOImport.getTypeBorne()) &&
                (contratDTOImport.getBorneMaximale() == null || contratDTOImport.getBorneMinimale() == null)) {
            conditionalMessages.add("Les champs Borne maximale et Borne minimale sont obligatoires pour le contrat : " + contratDTOImport.getReferenceLibre());
        }

        if (("UNITAIRE".equals(contratDTOImport.getFormePrix()) || "MIXTE".equals(contratDTOImport.getFormePrix())) && contratDTOImport.getTypeBorne().isEmpty()) {
            conditionalMessages.add("Le type de borne est obligatoire pour le contrat : " + contratDTOImport.getReferenceLibre());
        }

        if ("O".equals(contratDTOImport.getGroupement()) &&
                (contratDTOImport.getGroupementnom().isEmpty() || contratDTOImport.getGroupementtype().isEmpty())) {
            conditionalMessages.add("Les champs Nom du Groupement et Type de Groupement sont obligatoires pour le contrat : " + contratDTOImport.getReferenceLibre());
        }

        return conditionalMessages;
    }

    private Contrat mapContratDTOImportToContrat(ContratDTOImport contratDTOImport, String pfUid) {

        Contrat contrat = new Contrat();
        contrat.setType(typeContratRepository.findOneByCode(contratDTOImport.getType()));
        contrat.setReferenceLibre(contratDTOImport.getReferenceLibre());
        contrat.setObjet(contratDTOImport.getObjet());

        Consultation consultation = new Consultation();
        consultation.setNumero(contratDTOImport.getConsultationNumero());
        consultation.setIntitule(contrat.getObjet());
        consultation.setObjet(contrat.getObjet());
        consultation.setHorsPassation(true);
        consultation.setDateModificationExterne(null);
        consultation.setIdExterne(null);
        consultation.setCategorie(categorieConsultationRepository.findOneByCode(contratDTOImport.getConsultationCategorie()));
        contrat.setConsultation(consultation);

        Optional<Utilisateur> utilisateurOptional = utilisateurRepository.findFirstByNomAndPrenom(contratDTOImport.getCreateurNom(), contratDTOImport.getCreateurPrenom());
        utilisateurOptional.ifPresent(contrat::setCreateur);

        Optional<com.atexo.execution.server.model.Service> serviceOptional = serviceRepository.findFirstByNomAndPlateformeMpeUid(contratDTOImport.getNomService(), pfUid);
        serviceOptional.ifPresent(contrat::setService);

        if (contratDTOImport.getOrganismeAcronyme() != null && !contratDTOImport.getOrganismeAcronyme().isEmpty()) {
            contrat.getService().setOrganisme(organismeRepository.findOneByAcronymeAndPlateformeMpeUid(contratDTOImport.getOrganismeAcronyme(), pfUid).get());
        }

        com.atexo.execution.server.model.FormePrix formePrix = null;
        if (contratDTOImport.getFormePrix() != null) {
            formePrix = formePrixRepository.findOneByCode(contratDTOImport.getFormePrix());

        }
        contrat.setTypeFormePrix(formePrix);
        if (!contratDTOImport.getTypeBorne().isEmpty()) {
            contrat.setTypeBorne(TypeBorne.valueOf(contratDTOImport.getTypeBorne()));
        }

        if (contratDTOImport.getBorneMaximale() != null) {
            contrat.setBorneMaximale(BigDecimal.valueOf(contratDTOImport.getBorneMaximale()));
        }
        if (contratDTOImport.getBorneMinimale() != null) {
            contrat.setBorneMinimale(BigDecimal.valueOf(contratDTOImport.getBorneMinimale()));
        }

        contrat.setMontant(BigDecimal.valueOf(contratDTOImport.getMontant()));
        if (contratDTOImport.getMontantFacture() != null) {
            contrat.setMontantFacture(BigDecimal.valueOf(contratDTOImport.getMontantFacture()));
        }
        if (contratDTOImport.getMontantMandate() != null) {
            contrat.setMontantMandate(BigDecimal.valueOf(contratDTOImport.getMontantMandate()));
        }

        contrat.setDateDemaragePrestation(MapperUtils.convertToLocalDate(contratDTOImport.getDateDemaragePrestation()));
        contrat.setDateNotification(MapperUtils.convertToLocalDate(contratDTOImport.getDateNotification()));
        contrat.setDateFinContrat(MapperUtils.convertToLocalDate(contratDTOImport.getDateFinContrat()));
        contrat.setDateMaxFinContrat(MapperUtils.convertToLocalDate(contratDTOImport.getDateMaxFinContrat()));

        ContratEtablissement attributaire = new ContratEtablissement();
        EtablissementDTO dtoOptionalEtablissement = etablissementService.saveOrUpdate(contratDTOImport.getSiret());
        if (dtoOptionalEtablissement != null) {
            Etablissement etablissement = etablissementRepository.findById(dtoOptionalEtablissement.getId()).orElse(null);
            attributaire.setEtablissement(etablissement);
        }
        attributaire.setContrat(contrat);
        attributaire.setCategorieConsultation(contrat.getConsultation().getCategorie());
        attributaire.setIdExterne(null);
        attributaire.setMontant(contrat.getMontant());

        contrat.setAttributaire(attributaire);
        contrat.getAttributaire().setDateNotification(contrat.getDateNotification());

        if ("O".equals(contratDTOImport.getGroupement())) {
            ContratEtablissementGroupement groupement = new ContratEtablissementGroupement();
            groupement.setNom(contratDTOImport.getGroupementnom());
            groupement.setType(TypeGroupement.from(contratDTOImport.getGroupementtype()));
            contrat.setGroupement(groupement);
            contrat.getAttributaire().setMandataire(true);
        }

        contrat.setCreated(true);

        return contrat;

    }

    private void setValeurToDTO(ContratDTOImport contratDTOImport, String header, Cell cell) {

        String cellValue = DATA_FORMATTER.formatCellValue(cell);

        switch (header) {
            case "type":
                contratDTOImport.setType(cellValue);
                break;
            case "referenceLibre":
                contratDTOImport.setReferenceLibre(cellValue);
                break;
            case "attributaire.siret":
                contratDTOImport.setSiret(cellValue);
                break;
            case "consultation.numero":
                contratDTOImport.setConsultationNumero(cellValue);
                break;
            case "objet":
                contratDTOImport.setObjet(cellValue);
                break;
            case "consultation.categorie":
                contratDTOImport.setConsultationCategorie(cellValue);
                break;
            case "service.organisme.acronyme":
                contratDTOImport.setOrganismeAcronyme(cellValue);
                break;
            case "service.nom":
                contratDTOImport.setNomService(cellValue);
                break;
            case "createur.nom":
                contratDTOImport.setCreateurNom(cellValue);
                break;
            case "createur.prenom":
                contratDTOImport.setCreateurPrenom(cellValue);
                break;
            case "formePrix":
                contratDTOImport.setFormePrix(cellValue);
                break;
            case "typeBorne":
                contratDTOImport.setTypeBorne(cellValue);
                break;
            case "borneMinimale":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setBorneMinimale(cell.getNumericCellValue());
                }
                break;
            case "borneMaximale":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setBorneMaximale(cell.getNumericCellValue());
                }
                break;
            case "montant":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setMontant(cell.getNumericCellValue());
                }
                break;
            case "montantMandate":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setMontantMandate(cell.getNumericCellValue());
                }
                break;
            case "montantFacture":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setMontantFacture(cell.getNumericCellValue());
                }
                break;
            case "dateNotification":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setDateNotification(cell.getDateCellValue());
                }
                break;
            case "dateDemaragePrestation":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setDateDemaragePrestation(cell.getDateCellValue());
                }
                break;
            case "dateFinContrat":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setDateFinContrat(cell.getDateCellValue());
                }
                break;
            case "dateMaxFinContrat":
                if (cell != null && !cellValue.isEmpty()) {
                    contratDTOImport.setDateMaxFinContrat(cell.getDateCellValue());
                }
                break;
            case "groupement":
                contratDTOImport.setGroupement(cellValue);
                break;
            case "groupement.nom":
                contratDTOImport.setGroupementnom(cellValue);
                break;
            case "groupement.type":
                contratDTOImport.setGroupementtype(cellValue);
                break;
        }
    }
}
