package com.atexo.execution.server.service.messagerie;

import com.atexo.execution.common.def.DocumentContratType;
import com.atexo.execution.common.def.TypeNotification;
import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.DocumentContratDTO;
import com.atexo.execution.common.dto.messagerie.MessecInitialisation;
import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.clients.oauth2.Oauth2Client;
import com.atexo.execution.server.common.Preconditions;
import com.atexo.execution.server.common.manager.FileStoreManager;
import com.atexo.execution.server.interf.messagerie.MessagerieInterface;
import com.atexo.execution.server.mapper.TemplateMapper;
import com.atexo.execution.server.mapper.TemplateReponse;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.TemplateRepository;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.DocumentContratRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.service.ParametrageService;
import com.atexo.execution.server.service.UtilisateurService;
import com.atexo.execution.server.service.document.DocumentContratService;
import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContextException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriTemplate;

import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MessagerieServiceImpl implements MessagerieService {

    private static final Logger LOG = LoggerFactory.getLogger(MessagerieServiceImpl.class);
    private static final String EXEC_API_API_MESSAGERIE_TEMPLATES = "/exec-api/api/messagerie/templates";

    @Autowired
    MessagerieInterface messagerieInterface;

    @Autowired
    UtilisateurService utilisateurService;

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    ContratRepository contratRepository;

    @Autowired
    ActeRepository acteRepository;

    @Autowired
    DocumentContratRepository documentRepository;

    @Autowired
    DocumentContratService documentService;

    @Autowired
    FileStoreManager fileRepositoryManager;

    @Autowired
    TemplateMapper templateMapper;

    @Autowired
    TemplateRepository templateRepository;

    @Autowired
    ParametrageService parametrageService;

    @Value("${keycloak.enabled:false}")
    boolean keycloakEnabled;

    @Value("${keycloak.auth-server-url}")
    String keycloakUrl;
    @Autowired
    Oauth2Client oauth2Client;
    @Value("${messagerie.realm:messec}")
    private String messecRealm;

    @Value("${messagerie.resource:messec-api}")
    private String messecResource;
    @Value("${messagerie.username:dev}")
    private String username;
    @Value("${messagerie.password:dev}")
    private String password;

    @Value("${messagerie.assets.placeholder.path}")
    private String assetsPath;

    @Value("${ressources.url}")
    private String ressourcesURL;

    @Value("${mpe.logoPath")
    private String mpeLogoPath;

    @Value("${mpe.uri.reponse:/entreprise/messagerie/visualisation-contrat}")
    private String mpeUriReponse;

    @Override
    public ContexteDTO initTokenRedaction(final Long utilisateurId, final Long contratId) throws ApplicationException {
        final var utilisateur = utilisateurService.getUtilisateurParId(utilisateurId);
        var contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        var plateforme = contrat.getPlateforme();
        if(plateforme == null) {
            throw new ApplicationException("Plateforme introuvable pour le contrat "+contrat.getReferenceLibre());
        }
        if (utilisateur.getEmail() == null) {
            throw new ApplicationBusinessException();
        }
        var contacts = new ArrayList<Contact>();
        if (contrat.getContratEtablissements() != null) {
            contrat.getContratEtablissements()
                    .stream()
                    .map(ContratEtablissement::getContact)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            contacts.addAll(contrat.getContratEtablissements()
                    .stream()
                    .map(ContratEtablissement::getContactReferantList)
                    .filter(Objects::nonNull)
                    .flatMap(c -> c.stream())
                    .map(ContactReferant::getContact)
                    .collect(Collectors.toSet()));

        }

        final String cartouche = genererCartouche(contrat);
        var numeroLong = contrat.getNumeroLong();
        var accessToken = getAccessToken();
        var token = messagerieInterface.initTokenRedaction(buildInitialisation(plateforme),contratId, numeroLong, cartouche,contacts);
        var contexte = ContexteDTO.builder().templatesPath(EXEC_API_API_MESSAGERIE_TEMPLATES).accessToken(Optional.ofNullable(accessToken).map(Oauth2TokenDTO::getAccesToken).orElse(null)).token(token).peutCreerMessage(true).peutModifierMessage(true).build();
        return contexte;
    }

    @Override
    public ContexteDTO initTokenNotification(Long utilisateurId, Long contratId, Long acteId) throws ApplicationException {
        var contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        var plateforme = contrat.getPlateforme();
        if(plateforme == null) {
            throw new ApplicationException("Plateforme introuvable pour le contrat "+contrat.getReferenceLibre());
        }
        final String cartouche = genererCartouche(contrat);
        var numeroLong = contrat.getNumeroLong();
        var acte = acteRepository.findById(acteId).orElseThrow(() -> new ApplicationContextException("acte introuvable " + acteId));
        List<Contact> contacts = new ArrayList<>();
        if (acte != null && acte.getTitulaire() != null) {
            contacts.add(acte.getTitulaire());
        }
        acte.setTypeNotification(TypeNotification.MESSAGERIE_SECURISEE);
        acteRepository.save(acte);
        var messageSecuriseInit = buildInitialisation(plateforme);
        var lienVisualisation = messageSecuriseInit.getMessagerieUrlPfDestinataireVisualisation();
        messageSecuriseInit.setMessagerieUrlPfDestinataireVisualisation(lienVisualisation.contains("?") ? (lienVisualisation + "&acte=" + acte.getUuid()) : lienVisualisation + "?acte=" + acte.getUuid());
        var token = messagerieInterface.initTokenNotification(messageSecuriseInit,contratId, acteId, numeroLong, cartouche,contacts);
        var accessToken = getAccessToken();
        var contexte = ContexteDTO.builder().templatesPath(EXEC_API_API_MESSAGERIE_TEMPLATES).accessToken(Optional.ofNullable(accessToken).map(Oauth2TokenDTO::getAccesToken).orElse(null)).token(token).peutCreerMessage(true).peutModifierMessage(true).build();
        return contexte;
    }

    @Override
    public ContexteDTO initTokenNotification(Long utilisateurId, Long contratId) throws ApplicationException {
        var contrat = contratRepository.findById(contratId).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        contrat.setTypeNotification(TypeNotification.MESSAGERIE_SECURISEE);
        contratRepository.save(contrat);
        var plateforme = contrat.getPlateforme();
        if(plateforme == null) {
            throw new ApplicationException("Plateforme introuvable pour le contrat "+contrat.getReferenceLibre());
        }
        final String cartouche = genererCartouche(contrat);
        var numeroLong = contrat.getNumeroLong();
        var messageSecuriseInit = buildInitialisation(plateforme);
        var identifiantObjetMetier = contratId;
        var referenceObjetMetier = numeroLong;
        if(contrat.getConsultation() != null){
            var idCosnultation = MapperUtils.toLong(contrat.getConsultation().getIdExterne());
            var referenceConsultation = contrat.getConsultation().getNumero();
            if(idCosnultation != null && referenceConsultation != null){
                identifiantObjetMetier = idCosnultation;
                referenceObjetMetier = referenceConsultation;
                messageSecuriseInit.setMessagerieIdPfDestinataire(plateforme.getMpeUid());
            } else {
                LOG.warn("Consultation introuvable pour le contrat {}", numeroLong);
            }
        }
        var lienVisualisation = messageSecuriseInit.getMessagerieUrlPfDestinataireVisualisation();
        messageSecuriseInit.setMessagerieUrlPfDestinataireVisualisation(lienVisualisation.contains("?") ? (lienVisualisation + "&contrat=" + contrat.getUuid()) : lienVisualisation + "?contrat=" + contrat.getUuid());
        var token = messagerieInterface.initTokenNotification(messageSecuriseInit,identifiantObjetMetier, referenceObjetMetier, cartouche);
        var accessToken = getAccessToken();
        var contexte = ContexteDTO.builder().templatesPath(EXEC_API_API_MESSAGERIE_TEMPLATES).accessToken(Optional.ofNullable(accessToken).map(Oauth2TokenDTO::getAccesToken).orElse(null)).token(token).peutCreerMessage(true).peutModifierMessage(true).build();
        return contexte;
    }

    @Override
    public ContexteDTO initTokenSuivi(final Long utilisateurId, final Long contratId, final Long acteId) throws ApplicationException {

        var contrat = contratRepository.findById(contratId).orElseThrow(ApplicationTechnicalException::new);
        var plateforme = contrat.getPlateforme();
        if(plateforme == null) {
            throw new ApplicationException("Plateforme introuvable pour le contrat "+contrat.getReferenceLibre());
        }
        var token = messagerieInterface.initTokenSuivi(plateforme.getUid(),contratId, acteId, contrat.getNumeroLong());
        var accessToken = getAccessToken();
        var contexte = ContexteDTO.builder().templatesPath(EXEC_API_API_MESSAGERIE_TEMPLATES).accessToken(Optional.ofNullable(accessToken).map(Oauth2TokenDTO::getAccesToken).orElse(null)).token(token).peutCreerMessage(true).peutModifierMessage(true).build();
        return contexte;
    }

    @Override
    @Transactional
    public String[] uploadDocuments(final Long organismeId, final String token, final Long[] documentIds) throws ApplicationException {

        final String[] result = new String[documentIds.length];

        int i = 0;

        for (final Long documentId : documentIds) {

            final Optional<DocumentContrat> documentOpt = documentRepository.findByIdAndContratServiceOrganismeId(documentId, organismeId);

            if (documentOpt.isEmpty()) {
                throw new ApplicationTechnicalException();
            }

            final Fichier fichier = documentOpt.get().getFichier();
            final String nom = fichier.getNomEnregistre();

            final Optional<File> repositoryFileOpt = fileRepositoryManager.getFile(nom);

            if (repositoryFileOpt.isEmpty()) {
                throw new ApplicationTechnicalException();
            }

            result[i++] = messagerieInterface.uploadPieceJointe(token, fichier.getNom(), String.valueOf(documentId),
                    repositoryFileOpt.get());
        }

        return result;
    }

    @Override
    public DocumentContratDTO addPieceJointeToDocuments(final Long utilisateurId, final Long contratId, final Long etablissementId, final String object, final String codeLien, final String pieceJointeId) throws ApplicationException {

        final PieceJointeDTO pieceJointe = messagerieInterface.getPieceJointe(codeLien, pieceJointeId);

        final InputStream inputStream = messagerieInterface.downloadPieceJointe(codeLien, pieceJointeId);

        return documentService.saveStreamToDocument(utilisateurId, contratId, object, etablissementId,

                DocumentContratType.PIECE_JOINTE_MESSAGE, inputStream, pieceJointe.getNom(), pieceJointe.getContentType(),
                pieceJointe.getTaille(), String.valueOf(pieceJointe.getId()));
    }

    @Override
    public List<ContratMessageStatusDTO> countMessageStatusByContrat(final Long organismeId, final List<String> contrats) throws ApplicationException {
        final List<ContratMessageStatusDTO> messageStatus = new ArrayList<>();
        if (contrats != null && !contrats.isEmpty()) {
            var accessToken = getAccessToken();
            for (final String idContrat : contrats) {
                final String tokenSuivi;
                try {
                    ///Valide que le contrat de l'utilisateur est dans le meme organisme.
                    Preconditions.checkArgument(idContrat != null);
                    var contratOpt = contratRepository.findById(Long.valueOf(idContrat)).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
                    var pfUid = Optional.ofNullable(contratOpt).map(Contrat::getPlateforme).map(Plateforme::getUid).orElse(null);
                    tokenSuivi = messagerieInterface.initTokenSuivi(pfUid,contratOpt.getId(), null, contratOpt.getNumeroLong());
                    final ContratMessageStatusDTO contratMessageStatusDTO = messagerieInterface.countMessageStatus(tokenSuivi, idContrat);
                    if (contratMessageStatusDTO != null) {
                        contratMessageStatusDTO.setIdContrat(idContrat);
                    }
                    messageStatus.add(contratMessageStatusDTO);
                } catch (final Exception e) {
                    LOG.error(e.getMessage(), e);
                    throw new ApplicationTechnicalException(e);
                }
            }
        }
        return messageStatus;
    }

    @Override
    public TemplateReponse getTemplates() {
        return templateMapper.toWS(templateRepository.findAll(Sort.by(Sort.Direction.ASC, "ordreAffichage")));
    }

    @Override
    public Long getIdTemplateByCode(String code) {
        return templateRepository.findByCode(code).map(Template::getId).orElse(null);
    }

    @Override
    public ContexteDTO initTokenRedactionByCodeLien(Long utilisateurId, Long contratId, String codeLien) {
        var accessToken = getAccessToken();
        var token = messagerieInterface.initTokenRedactionByCodeLien(codeLien);
        var contexte = ContexteDTO.builder().templatesPath(EXEC_API_API_MESSAGERIE_TEMPLATES).accessToken(Optional.ofNullable(accessToken).map(Oauth2TokenDTO::getAccesToken).orElse(null)).token(token).peutCreerMessage(true).peutModifierMessage(true).build();
        return contexte;
    }

    private String genererCartouche(Contrat contrat) {
        var map = Map.of("url", ressourcesURL, "path", assetsPath);
        var urlAssets = new UriTemplate("{url}/{path}/").expand(map).normalize();
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put("reference", contrat.getReferenceLibre());
        mapFreeMarker.put("objet", contrat.getObjet());
        mapFreeMarker.put("organisme", Optional.ofNullable(contrat.getService()).map(com.atexo.execution.server.model.Service::getOrganisme).map(Organisme::getNom).orElse(""));
        mapFreeMarker.put("entiteAchat", Optional.ofNullable(contrat.getService()).map(com.atexo.execution.server.model.Service::getNom).orElse(""));
        mapFreeMarker.put("urlAssets", urlAssets);
        String xmlFile = "templates/messec/cartouche.ftl";

        try {
            String freeMarkerXML = IOUtils.toString(MessagerieServiceImpl.class.getClassLoader().getResourceAsStream(xmlFile));
            return FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
        } catch (Exception e) {
            return MessageFormat.format("Marché N° {0}-{1}", contrat.getReferenceLibre(), contrat.getObjet());
        }
    }

    private Oauth2TokenDTO getAccessToken() {
        if (keycloakEnabled) {
            return oauth2Client.getOauth2TokenDTO(keycloakUrl, messecRealm, messecResource, username, password);
        }
        return null;
    }

    private String getMessagerieLogo(String messagerieLogoSrc,String mpeUrl) {
        if (messagerieLogoSrc != null && !messagerieLogoSrc.isEmpty()) {
            return messagerieLogoSrc;
        }
        var map = Map.of("mpeUrl", mpeUrl, "mpeLogoPath", mpeLogoPath);
        var logo = new UriTemplate("{mpeUrl}/{mpeLogoPath}").expand(map).normalize();
        return logo.toString();
    }

    private MessecInitialisation buildInitialisation(Plateforme plateforme){
        var initialisation = new MessecInitialisation();
        var parametrageMap = parametrageService.getParametrageMessagerie(plateforme.getUid());
        initialisation.setMessagerieIdPfEmetteur(plateforme.getUid());
        initialisation.setMessagerieIdPfDestinataire(plateforme.getUid());
        initialisation.setMessagerieUrlPfEmetteur(plateforme.getMpeUrl());
        initialisation.setMessagerieNomPfEmetteur(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_NOM_PF_EMETTEUR)).orElse("Exécution des marchés"));
        initialisation.setSignatureAvisPassage(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_SIGNATURE)).orElse("Exécution des marchés"));
        initialisation.setMessagerieUrlPfDestinataire(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_URL_PF_DESTINATAIRE)).orElse(plateforme.getMpeUrl()+"/index.php5?page=entreprise.VisualiserEchangeExterne"));
        initialisation.setMessagerieNomPfDestinataire(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_NOM_PF_DESTINATAIRE)).orElse("Exécution des marchés"));
        initialisation.setMessagerieUrlPfDestinataireVisualisation(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_URL_PF_DESTINATAIRE_VISUALISATION)).orElse(plateforme.getMpeUrl() + mpeUriReponse));
        initialisation.setAdresseMailExpediteur(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_EMAIL_EXPEDITEUR)).orElse(")"));
        initialisation.setMessagerieLogoSrc(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_LOGO_SRC)).orElse(getMessagerieLogo(null,plateforme.getMpeUrl())));
        BigInteger defaultSize = BigInteger.valueOf(10485760L);
        initialisation.setMaxTailleFichiers(Optional.ofNullable(parametrageMap.get(ParametrageMessagerieSecurisee.CLE_TAILLE_MAX_PJ)).map(Long::parseLong).map(BigInteger::valueOf).orElse(defaultSize));
        return initialisation;
    }


}
