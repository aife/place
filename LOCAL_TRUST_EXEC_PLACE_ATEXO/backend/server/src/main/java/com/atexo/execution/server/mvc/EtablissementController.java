package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.validation.Siret;
import com.atexo.execution.server.service.EtablissementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/etablissement")
@Validated
public class EtablissementController {

    private static final Logger LOG = LoggerFactory.getLogger(EtablissementController.class);

    @Autowired
    EtablissementService etablissementService;


    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EtablissementDTO>> getEtablissementByMotCles(@RequestParam final String motsCles) {
        final List<EtablissementDTO> etablissements = etablissementService.findEtablissementBymotCles(motsCles);
        return new ResponseEntity<>(etablissements, HttpStatus.OK);
    }

    @GetMapping(value = "/findByRaisonSociale/{motsCles}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EtablissementDTO>> getEtablissementByRaisonSociale(@PathVariable() final String motsCles) {
        try {
            final List<EtablissementDTO> etablissements = etablissementService.findEtablissementBymotCles(motsCles);
            return new ResponseEntity<>(etablissements, HttpStatus.OK);
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            return new ResponseEntity<>((List<EtablissementDTO>) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/findBySiret/{siret}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EtablissementDTO> get(@PathVariable() @Valid @Siret final String siret) {
        try {
            return etablissementService.findEtablissementBySiret(siret).map(etablissementDTO -> new ResponseEntity<>(etablissementDTO, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>((EtablissementDTO) null, HttpStatus.NOT_FOUND));
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            return new ResponseEntity<>((EtablissementDTO) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/findById/{etablissementId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EtablissementDTO> getById(@PathVariable() final Long etablissementId) {
        return etablissementService.findById(etablissementId).map(etablissementDTO -> new ResponseEntity<>(etablissementDTO, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>((EtablissementDTO) null, HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "/ajouterEtablissement", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EtablissementDTO> ajouterEtablissement(@RequestBody final EtablissementDTO etablissement) {

        final EtablissementDTO etablissementDTO;
        try {
            etablissementDTO = etablissementService.ajouterEtablissement(etablissement);
        } catch (final ApplicationBusinessException e) {
            LOG.error(e.getMessage(), e);
            return new ResponseEntity<>((EtablissementDTO) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (etablissementDTO == null) {
            return new ResponseEntity<>((EtablissementDTO) null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(etablissementDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/modifierEtablissement", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EtablissementDTO> modifierEtablissement(@RequestBody final EtablissementDTO etablissement) throws ApplicationBusinessException {

        final EtablissementDTO etablissementDTO = etablissementService.modifierEtablissement(etablissement);

        return new ResponseEntity<>(etablissementDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/ajouterEtablissementEtranger", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EtablissementDTO> ajouterEtablissementEtranger(@RequestBody final EtablissementDTO etablissement) throws ApplicationBusinessException {

        final EtablissementDTO etablissementDTO = etablissementService.ajouterEtablissementEtranger(etablissement);

        return new ResponseEntity<>(etablissementDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idEtablissement}")
    public void deleteEtablissement(@PathVariable Long idEtablissement) throws ApplicationBusinessException {

        this.etablissementService.supprimerEtablissement(idEtablissement);
    }
}
