package com.atexo.execution.server.service.alerte;

import com.atexo.execution.common.dto.EvenementDTO;
import com.atexo.execution.server.model.Contrat;

/**
 * Created by qba on 29/11/16.
 */
public interface AlerteService {

    boolean envoiMailAlerte(EvenementDTO evenementDTO);

    void envoiMailAlerte(Contrat contrat);

}
