package com.atexo.execution.server.service.imports;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.PairDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import com.atexo.execution.common.dto.imports.ContractantDTOImport;
import com.atexo.execution.common.dto.process.attributaire.EditionAttributaireDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.common.imports.ImportUtils;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.repository.crud.ContratEtablissementRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.EtablissementRepository;
import com.atexo.execution.server.repository.referentiels.CategorieConsultationRepository;
import com.atexo.execution.server.service.AttributaireService;
import com.atexo.execution.server.service.EtablissementService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atexo.execution.server.common.imports.ImportUtils.CHAMPS_CONTRACTANT;

/**
 * Service d'import de contractants via fichier XLS.
 */
@Service
@Transactional
public class ContractantImportServiceImpl extends AbstractImportService<ContractantDTOImport> implements ContractantImportService {


    public static final String TYPE_SOUS_TRAITANT = "SOUSTRAITANT";
    public static final String TYPE_CO_TRAITANT = "COTRAITANT";
    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private CategorieConsultationRepository categorieConsultationRepository;

    @Autowired
    private EtablissementRepository etablissementRepository;

    @Autowired
    private ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    private AttributaireService attributaireService;

    @Autowired
    private EtablissementService etablissementService;

    @Override
    protected ContractantDTOImport getInstance() {
        return new ContractantDTOImport();
    }

    @Override
    protected void headerCellCheck(Cell cell) {
        if (!CHAMPS_CONTRACTANT.containsKey(cell.getStringCellValue())) {
            messages.add("Le champ \"" + cell.getStringCellValue() + "\" n'est pas un champ technique");
        }
    }

    @Override
    protected void setValeur(ContractantDTOImport contractantDTOImport, String header, Cell cell, int rowNum, int colNum, String pfUid) {
        // check pour les test obligatoire et le type si erreur ajouter le message
        String testValide = testTypeAndObligatoire(cell, header, rowNum, colNum);
        if (testValide != null) {
            messagesRow.add(testValide);
        } else {
            setValeurToDTO(contractantDTOImport, header, cell);
        }
    }

    @Override
    protected void flushMessagesRow(ContractantDTOImport contractantDTOImport) {
        for (String messageRow : messagesRow) {
            StringBuilder sb = new StringBuilder("Contrat ");
            if (contractantDTOImport.getContratReferenceLibre() != null) {
                sb.append(contractantDTOImport.getContratReferenceLibre());
            }
            sb.append(", Contractant ");
            if (contractantDTOImport.getEtablissementSiret() != null) {
                sb.append(contractantDTOImport.getEtablissementSiret());
                sb.append(" ");
            }
            sb.append(": ");
            messages.add(sb + messageRow);
        }
    }

    @Override
    protected void customDTOCheck(List<ContractantDTOImport> contractantDTOImport) {

        // numeroContrat: {siretContractant: (est cotraitant ou non)}
        Map<String, Map<String, Boolean>> contractantByContrat = new HashMap<>();
        var i = 0;
        for (ContractantDTOImport dtoImport : contractantDTOImport) {
            i++;
            // Retenir les cotractants ajoutés au fil du fichier
            if (!contractantByContrat.containsKey(dtoImport.getContratReferenceLibre())) {
                contractantByContrat.put(dtoImport.getContratReferenceLibre(), new HashMap<>());
            }

            List<Contrat> contrats = contratRepository.findByReferenceLibre(dtoImport.getContratReferenceLibre());
            Contrat contratBdd = null;

            if (contrats != null && contrats.isEmpty()) {
                // Si le contrat n'existe pas
                messages.add("Ligne " + i + ", Le numéro de contrat (" + dtoImport.getContratReferenceLibre() + ") n'existe pas en base de données");
            } else {
                if (contrats.size() > 1) {
                    // dans le cas d'un contrat chapeau, retrouver le contrat correspondant au bon attributaire
                    if (dtoImport.getContratAttributaireEtablissementSiret() == null || dtoImport.getContratAttributaireEtablissementSiret().isEmpty()) {
                        messages.add("Ligne " + i + ", Plusieurs contrats existent pour le numéro " + dtoImport.getContratReferenceLibre()
                                + ". Le SIRET de l'attributaire est obligatoire");
                    } else {
                        Optional<Contrat> contratOptional =
                                contratRepository.findByReferenceLibreAndAttributaireEtablissementSiret(
                                        dtoImport.getContratReferenceLibre(),
                                        dtoImport.getContratAttributaireEtablissementSiret());
                        if (contratOptional.isPresent()) {
                            contratBdd = contratOptional.get();
                        } else {
                            messages.add("Ligne " + i + ", Le couple numéro de contrat (" + dtoImport.getContratReferenceLibre() +
                                    ") et SIRET Attributaire (" + dtoImport.getContratAttributaireEtablissementSiret() + ") n'existe pas en base de données");
                        }
                    }
                } else {
                    contratBdd = contrats.get(0);
                }
            }

            if (contratBdd != null) {

                // Pour pouvoir l'utiliser plus tard lors de la sauvegarde
                dtoImport.setContratId(contratBdd.getId());

                Etablissement contractant = etablissementRepository.findFirstBySiret(dtoImport.getEtablissementSiret()).orElse(null);

                if (contractant != null) {
                    Optional<ContratEtablissement> contractantOptional =
                            contratEtablissementRepository.findFirstByContratIdAndEtablissementId(contratBdd.getId(), contractant.getId());

                    // Vérifier que le contractant n'existe pas déjà pour le contrat en base
                    if (contractantOptional.isPresent()) {
                        messages.add("Ligne " + i + ", Le contractant (" + dtoImport.getEtablissementSiret()
                                + ") existe déjà pour ce contrat (" + dtoImport.getContratReferenceLibre() + ")");
                    }
                }

                // Vérifier que le contractant n'existe pas déjà pour le contrat dans le fichier
                if (contractantByContrat.get(dtoImport.getContratReferenceLibre()).containsKey(dtoImport.getEtablissementSiret())) {
                    messages.add("Ligne " + i + ", Le contractant (" + dtoImport.getEtablissementSiret()
                            + ") a été ajouté précédemment dans le fichier pour ce contrat (" + dtoImport.getContratReferenceLibre() + ")");
                } else {
                    // S'il n'existe pas déjà, l'ajouter dans la map qui garde la trace des contractants ajoutés via le fichier
                    // Dans le cas d'un groupement, retenir si c'est un cotraitant ou un soustraitant qu'on ajoute
                    contractantByContrat.get(dtoImport.getContratReferenceLibre())
                            .put(dtoImport.getEtablissementSiret(), TYPE_CO_TRAITANT.equals(dtoImport.getType()));
                }


                if (contratBdd.getGroupement() != null) {
                    if (TYPE_SOUS_TRAITANT.equals(dtoImport.getType())) {
                        if (dtoImport.getCommanditaireSiret() == null || dtoImport.getCommanditaireSiret().isEmpty()) {
                            messages.add("Ligne " + i + ", L'attributaire du contrat (" + dtoImport.getContratReferenceLibre()
                                    + ") est un groupement, et le contractant ajouté (" + dtoImport.getEtablissementSiret()
                                    + ") est un sous-traitant, donc le SIRET du titulaire est obligatoire");
                        } else {
                            // Pour un groupement, si on ajoute un sous-traitant, vérifier que son titulaire existe bien sur le contrat

                            // soit ajouté précédemment dans le fichier en tant que sous-traitant au lieu de co-traitant
                            boolean soustraitantInFile = contractantByContrat.get(dtoImport.getContratReferenceLibre()).containsKey(dtoImport.getCommanditaireSiret())
                                    && !contractantByContrat.get(dtoImport.getContratReferenceLibre()).get(dtoImport.getCommanditaireSiret());

                            if (soustraitantInFile) {
                                messages.add("Ligne " + i + ", Le titulaire (" + dtoImport.getCommanditaireSiret()
                                        + ") du sous-traitant (" + dtoImport.getEtablissementSiret()
                                        + ") a été ajouté précédemment dans le fichier en tant que sous-traitant du contrat (" + dtoImport.getContratReferenceLibre() + ")");
                            } else {

                                // soit en base, soit ajouté précédemment dans le fichier en tant que co-traitant
                                boolean cotraitantInFile = contractantByContrat.get(dtoImport.getContratReferenceLibre()).containsKey(dtoImport.getCommanditaireSiret())
                                        && contractantByContrat.get(dtoImport.getContratReferenceLibre()).get(dtoImport.getCommanditaireSiret());

                                Etablissement commanditaire = etablissementRepository.findOneBySiret(dtoImport.getCommanditaireSiret());
                                if (commanditaire != null) {
                                    Optional<ContratEtablissement> contratEtablissementOptional =
                                            contratEtablissementRepository.findFirstByContratIdAndEtablissementId(contratBdd.getId(), commanditaire.getId());
                                    if ((contratEtablissementOptional.isPresent() && contratEtablissementOptional.get().getMandataire() == null)
                                            || (!contratEtablissementOptional.isPresent() && !cotraitantInFile)) {
                                        messages.add("Ligne " + i + ", Le titulaire (" + dtoImport.getCommanditaireSiret()
                                                + ") du sous-traitant (" + dtoImport.getEtablissementSiret()
                                                + ") n'est pas un cotraitant du contrat (" + dtoImport.getContratReferenceLibre() + ")");
                                    }

                                } else if (!cotraitantInFile) {
                                    messages.add("Ligne " + i + ", Le titulaire (" + dtoImport.getCommanditaireSiret()
                                            + ") du sous-traitant (" + dtoImport.getEtablissementSiret()
                                            + ") n'est pas un cotraitant du contrat (" + dtoImport.getContratReferenceLibre() + ")");
                                }
                            }
                        }
                    }
                } else {
                    if (TYPE_CO_TRAITANT.equals(dtoImport.getType())) {
                        messages.add("Ligne " + i + ", L'attributaire du contrat (" + dtoImport.getContratReferenceLibre()
                                + ") n'est pas un groupement, impossible d'ajouter un cotraitant");
                    }
                }
            }

        }

    }

    @Override
    protected void save(List<ContractantDTOImport> dtoList, String pfUid) {

        var threadPool = getThreadPool();
        for (ContractantDTOImport contractantDTOImport : dtoList) {
            threadPool.submit(() -> {
                try {
                    process(contractantDTOImport);
                } catch (Exception e) {
                    LOG.error("Erreur lors de l'import des contractants", e);
                    messages.add("Erreur lors de l'import du contractant, contractant : " + contractantDTOImport.getContratReferenceLibre());
                }
            });
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (Exception e) {
            LOG.error("Erreur lors de l'import des contrats", e);
        }

    }

    private ContractantDTOImport process(ContractantDTOImport contractantDTOImport) {
        LOG.info("Import du contractant : " + contractantDTOImport.getContratReferenceLibre() + " - " + contractantDTOImport.getEtablissementSiret());
        EditionAttributaireDTO editionAttributaireDTO = this.mapContractantDTOImportToEditionAttributaireDTO(contractantDTOImport);
        if (editionAttributaireDTO == null) {
            return null;
        }
        try {
            if (TYPE_CO_TRAITANT.equals(contractantDTOImport.getType())) {
                attributaireService.ajouterCoTraitant(contractantDTOImport.getOrganismeId(), contractantDTOImport.getContratId(), editionAttributaireDTO);
            } else {
                attributaireService.ajouterSousTraitant(contractantDTOImport.getOrganismeId(), contractantDTOImport.getContratId(), editionAttributaireDTO);
            }
        } catch (ApplicationBusinessException e) {
            messages.add("Une erreur est survenue (contrat " + contractantDTOImport.getContratReferenceLibre()
                    + ", contractant " + contractantDTOImport.getEtablissementSiret() + ")");
            LOG.error(e.getMessage(), e);
        }
        return contractantDTOImport;
    }

    private String testTypeAndObligatoire(Cell cell, String name, int rowNum, int colNum) {
        PairDTO<String, Boolean> regle = CHAMPS_CONTRACTANT.get(name);
        String prefixMess = "Le champ ";
        // Check si la valeur est obligatoire
        if (regle.getSecond()) {
            if (cell == null) {
                return prefixMess + new CellAddress(rowNum, colNum).formatAsString() + " est obligatoire";
            } else if (cell.getCellTypeEnum() == CellType.BLANK) {
                return prefixMess + new CellAddress(cell).formatAsString() + " est obligatoire";
            }
        }
        // Check la valeur
        if (cell != null && cell.getCellTypeEnum() != CellType.BLANK) {
            if ("String".equalsIgnoreCase(regle.getFirst())) {

                if (cell.getCellTypeEnum() != CellType.STRING) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " doit être au format texte";
                }
            }
            if ("Enum".equalsIgnoreCase(regle.getFirst())) {

                if (cell.getCellTypeEnum() != CellType.STRING || !isEnumOrReferentielExist(cell.getStringCellValue(), name)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas une valeur " +
                            "admissible du référentiel";
                }
            }
            if ("Double".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getCellTypeEnum() != CellType.NUMERIC) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " doit être au format numérique";
                }
            }

            if ("DATE".equalsIgnoreCase(regle.getFirst())) {
                if (!DateUtil.isCellDateFormatted(cell)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas au format date";
                }
            }

            if ("Siret".equalsIgnoreCase(regle.getFirst())) {
                String stringCellValue = DATA_FORMATTER.formatCellValue(cell);
                if (stringCellValue.length() != 14) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " SIRET doit être composé de 14 chiffres";
                }
                if (!ImportUtils.isSiretSyntaxValide(stringCellValue)) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " SIRET doit respecter l'algorithme de Luhn";
                }

            }

            if ("typeCotractant".equalsIgnoreCase(regle.getFirst())) {
                if (cell.getCellTypeEnum() != CellType.STRING || !isTypeCotractantValid(cell.getStringCellValue())) {
                    return prefixMess + new CellAddress(cell).formatAsString() + " n'est pas un type de cotractant valide";
                }
            }
        }

        return null;
    }

    private boolean isEnumOrReferentielExist(String value, String name) {
        return categorieConsultationRepository.findOneByCode(value) != null;
    }

    private boolean isTypeCotractantValid(String typeCotractant) {
        return TYPE_SOUS_TRAITANT.equals(typeCotractant) || TYPE_CO_TRAITANT.equals(typeCotractant);
    }

    private EditionAttributaireDTO mapContractantDTOImportToEditionAttributaireDTO(ContractantDTOImport contractantDTOImport) {

        EditionAttributaireDTO editionAttributaireDTO = new EditionAttributaireDTO();
        editionAttributaireDTO.setCategorieConsultation(ValueLabelDTO.builder().value(contractantDTOImport.getCategorieConsultation()).build());
        editionAttributaireDTO.setMontant(BigDecimal.valueOf(contractantDTOImport.getMontant()));
        editionAttributaireDTO.setDateNotification(contractantDTOImport.getDateNotification());

        if (contractantDTOImport.getEtablissementSiret() != null && !contractantDTOImport.getEtablissementSiret().isEmpty()) {
            Optional<EtablissementDTO> etablissementOptional = etablissementService.findEtablissementBySiret(contractantDTOImport.getEtablissementSiret());
            if (!etablissementOptional.isPresent()) {
                messages.add("Contrat " + contractantDTOImport.getContratReferenceLibre() + " : le SIRET "
                        + contractantDTOImport.getEtablissementSiret() + " est inconnu des données INSEE");
                return null;
            }
            editionAttributaireDTO.setEtablissement(etablissementOptional.get());
        }

        // Si c'est pas un groupement, il faut retrouver le mandataire et le mettre en tant que commanditaire du sous-traitant
        Contrat contrat = contratRepository.findById(contractantDTOImport.getContratId()).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        if (contrat.getGroupement() == null) {
            editionAttributaireDTO.setCommanditaireId(contrat.getAttributaire().getEtablissement().getId());
        } else {
            if (contractantDTOImport.getCommanditaireSiret() != null && !contractantDTOImport.getCommanditaireSiret().isEmpty()) {
                Etablissement etablissement = etablissementRepository.findOneBySiret(contractantDTOImport.getCommanditaireSiret());
                editionAttributaireDTO.setCommanditaireId(etablissement.getId());
            }
        }

        return editionAttributaireDTO;
    }

    private void setValeurToDTO(ContractantDTOImport contractantDTOImport, String header, Cell cell) {

        String cellValue = DATA_FORMATTER.formatCellValue(cell);

        switch (header) {
            case "contratReferenceLibre":
                contractantDTOImport.setContratReferenceLibre(cellValue);
                break;
            case "etablissement.siret":
                contractantDTOImport.setEtablissementSiret(cellValue);
                break;
            case "type":
                contractantDTOImport.setType(cellValue);
                break;
            case "categorieConsultation":
                contractantDTOImport.setCategorieConsultation(cellValue);
                break;
            case "role":
                contractantDTOImport.setRole(cellValue);
                break;
            case "montant":
                if (cell != null && !cellValue.isEmpty()) {
                    contractantDTOImport.setMontant(cell.getNumericCellValue());
                }
                break;
            case "dateNotification":
                if (cell != null && !cellValue.isEmpty()) {
                    contractantDTOImport.setDateNotification(cell.getDateCellValue());
                }
                break;
            case "commanditaire.siret":
                contractantDTOImport.setCommanditaireSiret(cellValue);
                break;
            case "contrat.attributaire.etablissement.siret":
                contractantDTOImport.setContratAttributaireEtablissementSiret(cellValue);
                break;
        }
    }
}
