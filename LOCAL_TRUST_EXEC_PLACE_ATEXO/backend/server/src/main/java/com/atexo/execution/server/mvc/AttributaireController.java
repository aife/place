package com.atexo.execution.server.mvc;

import com.atexo.execution.common.dto.ContratEtablissementDTO;
import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;
import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.process.attributaire.AttributaireDTO;
import com.atexo.execution.common.dto.process.attributaire.EditionAttributaireDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.service.AttributaireService;
import com.atexo.execution.server.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/attributaire")
public class AttributaireController {

	@Autowired
	AttributaireService attributaireService;

	@Autowired
	UtilisateurService utilisateurService;

	@GetMapping(value = "/{contratId}/arborescence", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttributaireDTO> getArborescenceAttributaires(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@PathVariable Long contratId
	) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		AttributaireDTO arborescenceAttributaires = attributaireService.getArborescenceAttributaires(utilisateur.getOrganismeId(), contratId);

		return new ResponseEntity<>(arborescenceAttributaires, HttpStatus.OK);
	}

	@PostMapping(value = "/{contratId}/ajouterSousTraitant", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContratEtablissementDTO> ajouterSousTraitant(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@PathVariable Long contratId,
		@RequestBody EditionAttributaireDTO editionContratEtablissementDTO
	) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		ContratEtablissementDTO contratEtablissementDTO = attributaireService.ajouterSousTraitant(utilisateur.getOrganismeId(), contratId, editionContratEtablissementDTO);
		if(contratEtablissementDTO == null){
			return new ResponseEntity<>((ContratEtablissementDTO) null,HttpStatus.CONFLICT);
		}

		return new ResponseEntity<>(contratEtablissementDTO, HttpStatus.OK);
	}

	@PostMapping(value = "/{contratId}/transformerEnGroupement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttributaireDTO> transformerEnGroupement(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@PathVariable Long contratId,
		@RequestBody ContratEtablissementGroupementDTO groupementDTO) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		AttributaireDTO arborescnece = attributaireService.transformerEnGroupement(utilisateur.getOrganismeId(), contratId, groupementDTO);

		return new ResponseEntity<>(arborescnece, HttpStatus.OK);
	}

	@PostMapping(value = "/{contratId}/degrouperGroupement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttributaireDTO> degrouperGroupement(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@PathVariable Long contratId
	) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		AttributaireDTO arborescnece = attributaireService.degrouperGroupement(utilisateur.getOrganismeId(), contratId);

		return new ResponseEntity<>(arborescnece, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{contratId}/supprimerSousTraitant", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> supprimerSousTraitant(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@RequestBody EditionAttributaireDTO editionContratEtablissementDTO,
		@PathVariable Long contratId) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		attributaireService.supprimerSousTraitant(utilisateur.getOrganismeId(), contratId, editionContratEtablissementDTO);

		return new ResponseEntity<>((Void) null, HttpStatus.OK);
	}

	@PostMapping(value = "/{contratId}/ajouterCoTraitant", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContratEtablissementDTO> ajouterCoTraitant(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@RequestBody EditionAttributaireDTO editionContratEtablissementDTO,
		@PathVariable Long contratId
	) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		ContratEtablissementDTO contratEtablissementDTO = attributaireService.ajouterCoTraitant(utilisateur.getOrganismeId(), contratId,
				editionContratEtablissementDTO);
		if(contratEtablissementDTO == null){
			return new ResponseEntity<>((ContratEtablissementDTO) null,HttpStatus.CONFLICT);
		}

		return new ResponseEntity<>(contratEtablissementDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/{contratId}/supprimerCoTraitant", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> supprimerCoTraitant(@RequestHeader("User-Uuid") final String utilisateurUuid, @RequestBody EditionAttributaireDTO editionContratEtablissementDTO,
			@PathVariable Long contratId) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		attributaireService.supprimerCoTraitant(utilisateur.getOrganismeId(), contratId, editionContratEtablissementDTO);

		return new ResponseEntity<>((Void) null, HttpStatus.OK);
	}

	@RequestMapping(value = "/{contratId}/modifierAttributaire", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContratEtablissementDTO> modifierAttributaire(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@RequestBody EditionAttributaireDTO editionContratEtablissementDTO,
		@PathVariable Long contratId) throws ApplicationBusinessException
	{
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		ContratEtablissementDTO contratEtablissementDTO = attributaireService.modifierAttributaire(utilisateur.getOrganismeId(), contratId,
				editionContratEtablissementDTO);

		return new ResponseEntity<>(contratEtablissementDTO, HttpStatus.OK);
	}

    @RequestMapping(value = "/{contratId}/modifierAttributaires", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContratEtablissementDTO>> modifierAttributaires(
	    @RequestHeader("User-Uuid") final String utilisateurUuid,
    	@RequestBody List<EditionAttributaireDTO> editionContratEtablissementDTOs,
	    @PathVariable Long contratId) throws ApplicationBusinessException
    {
	    var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

        List<ContratEtablissementDTO> contratEtablissementDTOs = attributaireService.modifierAttributaires(utilisateur.getOrganismeId(), contratId,
                editionContratEtablissementDTOs);

        return new ResponseEntity<>(contratEtablissementDTOs, HttpStatus.OK);
    }

	@RequestMapping(value = "/{contratId}/modifierGroupement", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContratEtablissementGroupementDTO> modifierGroupement(
		@RequestHeader("User-Uuid") final String utilisateurUuid,
		@PathVariable Long contratId,
		@RequestBody ContratEtablissementGroupementDTO groupementDTO
	) throws ApplicationBusinessException {
		var utilisateur = utilisateurService.getUtilisateurParUuid(utilisateurUuid).orElseThrow(IllegalArgumentException::new);

		ContratEtablissementGroupementDTO groupement = attributaireService.modifierGroupement(utilisateur.getOrganismeId(), contratId, groupementDTO);

		return new ResponseEntity<>(groupement, HttpStatus.OK);
	}
}
