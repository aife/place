package com.atexo.execution.server.service;

import org.apache.catalina.connector.ClientAbortException;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceExceptionLogAspect {

	private final static Logger LOG = LoggerFactory.getLogger(ServiceExceptionLogAspect.class);

	@AfterThrowing(pointcut = "execution(* com.atexo.execution.server.service..*.*(..))", throwing = "exception")
	public void logException(Throwable exception) {
		if (exception instanceof ClientAbortException) {
			LOG.warn("La communication a été interrompu par le client. Message = {}", exception.getMessage());
		} else {
			LOG.error(exception.getMessage(), exception);
		}
	}

}
