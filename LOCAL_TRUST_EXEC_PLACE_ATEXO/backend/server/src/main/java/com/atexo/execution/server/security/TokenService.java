package com.atexo.execution.server.security;

import com.atexo.execution.common.dto.UtilisateurDTO;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Map;

public interface TokenService {

    PreAuthenticatedAuthenticationToken getTokenAccess(UtilisateurDTO utilisateurDTO, String authorizationCode, Map<String, String> parameters);
}
