# Envoi des données essentielles au TNCP

## Préparation des données à envoyer

En local, exécuter le script suivant pour obtenir les données à envoyer au TNCP.

```mysql
update donnees_essentielles
set statut='A_PUBLIER'
where statut <> 'A_PUBLIER';
```

## Envoi des données

Exécuter le batch de synchronisation avec le flag __--tncp__.

## Réinitialisation du batch

```mysql
update batch_job_execution
set STATUS='FAILED',
    EXIT_CODE='FAILED',
    END_TIME=now()
where EXIT_CODE = 'UNKNOWN'
```
