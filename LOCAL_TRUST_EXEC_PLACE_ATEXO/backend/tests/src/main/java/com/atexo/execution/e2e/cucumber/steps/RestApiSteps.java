package com.atexo.execution.e2e.cucumber.steps;


import com.atexo.execution.e2e.auth.KeycloakService;
import com.atexo.execution.e2e.cucumber.utils.FileManageUtils;
import com.atexo.execution.e2e.cucumber.utils.TestContext;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.net.URIBuilder;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URISyntaxException;

public class RestApiSteps {

    @Resource
    private KeycloakService keycloakService;
    @Resource
    private TestRestTemplate testRestTemplate;
    @Resource
    private FileManageUtils fileManageUtils;

    @Value("${scheme:http}")
    protected String scheme;
    @Value("${host:localhost}")
    protected String host;
    @LocalServerPort
    protected int port;

    @When("je lance le web service {string} en get et je stocke le resultat dans {string} et le code de retour dans {string}")
    public void jeLanceLeWsAvecCode(String urlWs, String resultatGetContratMpe, String codeRetourHttpGetContratMpe) throws URISyntaxException {
        var token = keycloakService.get();

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).build();
        var uriString = uri.toString() + urlWs;

        var result = testRestTemplate.exchange(uriString, HttpMethod.GET, new HttpEntity<>(headers), String.class);

        TestContext.CONTEXT.set(resultatGetContratMpe, result.getBody());
        TestContext.CONTEXT.set(codeRetourHttpGetContratMpe, Integer.toString(result.getStatusCodeValue()));
    }

    @When("je lance le web service {string} en post avec le body {string}, je stocke le resultat dans {string} et le code retour dans {string}")
    public void jeLanceLeWsEnPost(String urlWs, String restBody, String resultatPostContratMpe, String codeRetourHttpPostContratMpe) throws URISyntaxException, IOException {
        var token = keycloakService.get();

        var headers = new HttpHeaders();
        headers.setBearerAuth(token);

        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(urlWs).build();

        var body = fileManageUtils.read(restBody, Object.class);

        var result = testRestTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), Object.class);

        TestContext.CONTEXT.set(resultatPostContratMpe, result.getBody());
        TestContext.CONTEXT.set(codeRetourHttpPostContratMpe, Integer.toString(result.getStatusCodeValue()));
    }


}
