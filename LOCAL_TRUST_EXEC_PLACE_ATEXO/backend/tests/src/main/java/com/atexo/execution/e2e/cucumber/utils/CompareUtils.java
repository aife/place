package com.atexo.execution.e2e.cucumber.utils;


import org.json.JSONException;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompareUtils {

    public void compareJsonWithIgnore(String JsonAttendu, String jsonResultatDir, List<String> ingnoredAttributs) throws JSONException {
        Customization[] attributsToIngoredArray = new Customization[0];
        attributsToIngoredArray =   ingnoredAttributs.stream().map(e -> new Customization(e, (o1, o2) -> true))
                .collect(Collectors.toList()).toArray(attributsToIngoredArray);
        JSONAssert.assertEquals(JsonAttendu, jsonResultatDir,
                new CustomComparator(JSONCompareMode.LENIENT, attributsToIngoredArray));
    }
}
