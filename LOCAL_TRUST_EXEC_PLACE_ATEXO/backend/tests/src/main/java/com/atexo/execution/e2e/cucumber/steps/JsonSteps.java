package com.atexo.execution.e2e.cucumber.steps;

import com.atexo.execution.e2e.cucumber.utils.CompareUtils;
import com.atexo.execution.e2e.cucumber.utils.FileManageUtils;
import com.atexo.execution.e2e.cucumber.utils.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import org.json.JSONException;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Collections;

public class JsonSteps {

    @Resource
    private FileManageUtils fileManageUtils;
    @Resource
    private CompareUtils compareUtils;

    @Then("je compare le resultat {string} avec le JSON attendu {string} et j'ignore les champs suivants")
    public void jeCompareLeResAttendu(String varName, String filePath, DataTable attributsToIgnored) throws IOException, JSONException {
        var listToIgnored = attributsToIgnored.asLists().stream().findFirst().orElse(Collections.emptyList());
        String fileContent = fileManageUtils.asString(new ClassPathResource(filePath)).trim();
        String resultWs = TestContext.CONTEXT.get(varName);
        compareUtils.compareJsonWithIgnore(fileContent, resultWs, listToIgnored);
    }

    @Then("je compare le resultat {string} avec le JSON attendu {string}")
    public void jeCompareLeResAttendu(String varName, String filePath) throws IOException, JSONException {
        String fileContent = fileManageUtils.asString(new ClassPathResource(filePath)).trim();
        String resultWs = TestContext.CONTEXT.get(varName);
        compareUtils.compareJsonWithIgnore(fileContent, resultWs, Collections.emptyList());
    }
}
