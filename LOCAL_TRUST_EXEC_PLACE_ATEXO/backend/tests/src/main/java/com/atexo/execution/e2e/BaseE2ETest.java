package com.atexo.execution.e2e;

import com.atexo.execution.e2e.sql.MariaDbExtension;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.JsonExpectationsHelper;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(MariaDbExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("e2e")
@Slf4j
@DirtiesContext
public abstract class BaseE2ETest {

    protected static final String USER_UUID = "User-uuid";

    @Value("${scheme:http}")
    protected String scheme;

    @Value("${host:localhost}")
    protected String host;

    @LocalServerPort
    protected int port;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected JsonExpectationsHelper jsonHelper;

    private static String asString(Resource resource) throws IOException {
        try (var reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        }
    }

    protected <T> void assertMatch(String filename, T object) throws Exception {
        var actual = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        var expected = asString(new ClassPathResource(filename));
        jsonHelper.assertJsonEqual(expected, actual, false);
    }

    protected <T> T read(String filename, Class<T> type) throws IOException {
       return this.objectMapper.readValue(new ClassPathResource(filename).getFile(), type);
    }
}
