package com.atexo.execution.e2e.exceptions;

public class OAuthException extends RuntimeException {

	public OAuthException(String message) {
		super(message);
	}
}
