package com.atexo.execution.e2e.sql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@Component
@Slf4j
public class MariaDbExtension implements BeforeAllCallback {
    @Override
    public void beforeAll(ExtensionContext context) {
        var container = SpringExtension.getApplicationContext(context).getBean(MariaDBInstance.class);

        if (!container.isRunning()) {
            log.trace("Starting container {}", container.getContainerName());

            container.start();
        }
    }
}
