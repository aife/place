package com.atexo.execution.e2e.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

@Configuration
@ConfigurationProperties(prefix = "oauth")
@Getter
@Setter
@ActiveProfiles("oauth")
public class OauthConfiguration {
    public static final String GRANT_TYPE = "grant_type";
    public static final String CLIENT_ID = "client_id";
    public static final String SCOPE = "scope";

    private String endpoint;
    private String token;
    private String grantType;
    private String clientId;
    private String scope;
}
