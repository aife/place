package com.atexo.execution.e2e.auth;

import com.atexo.execution.e2e.configuration.KeycloackConfiguration;
import com.atexo.execution.e2e.exceptions.OAuthException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
@RequiredArgsConstructor
public class KeycloakService {
    private static final String GRANT_TYPE = "grant_type";
    private static final String CLIENT_ID = "client_id";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @NonNull
    private KeycloackConfiguration configuration;

    private String token;

    public String get() throws URISyntaxException {
        if (null == token) {
            var restTemplate = new RestTemplate();
            var uri = new URI(configuration.getUrl());

            var headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add(CLIENT_ID, configuration.getClientId());
            map.add(USERNAME, configuration.getUsername());
            map.add(PASSWORD, configuration.getPassword());
            map.add(GRANT_TYPE, configuration.getGrantType());

            var request = new HttpEntity<>(map, headers);

            var result = restTemplate.postForEntity(uri, request, Token.class);
            final var body = result.getBody();

            if (null == body) {
                throw new OAuthException("Impossible de récupéer le jeton KEYCLOAK");
            }

            this.token = body.getAccessToken();
        }

        return token;
    }
}
