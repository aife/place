package com.atexo.execution.e2e.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "identite")
@Getter
@Setter
public class KeycloackConfiguration {
    private String url;
    private String clientId;
    private String username;
    private String password;
    private String grantType;
}
