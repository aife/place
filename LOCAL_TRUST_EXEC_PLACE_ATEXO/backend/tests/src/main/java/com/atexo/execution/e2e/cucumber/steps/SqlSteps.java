package com.atexo.execution.e2e.cucumber.steps;


import com.atexo.execution.e2e.cucumber.utils.TestContext;
import com.atexo.execution.e2e.sql.SQLService;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


public class SqlSteps {

    @Resource
    private SQLService sqlService;

    @Given("je lance le script SQL {string}")
    public void jeLanceScriptSqlStm(String sqlFile) throws SQLException, IOException {
        this.sqlService.load(sqlFile);
    }

    @When("je stocke le resultat de  la requete SQL {string} dans {string}")
    public void jeStockeLeResReqDansObj(String sqlQuery, String varName) {
        List<Map<String, Object>> res = sqlService.execQuery(sqlQuery);
        TestContext.CONTEXT.set(varName, res);
    }

    @Then("je compare le resultat de la requete {string} avec les lignes")
    public void jeCompareLeResReq(String varName, DataTable dataTable) {
        List<Map<String, Object>> listResult = TestContext.CONTEXT.get(varName);
        List<List<String>> listeAttendu = dataTable.asLists();
        for (int i = 0; i < listeAttendu.size(); i++) {
            String ligneAttendu = listeAttendu.get(i)
                    .stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(";"));
            Map<String, Object> mapGenere = listResult.get(i);
            String ligneGenere = mapGenere.entrySet()
                    .stream()
                    .filter(e -> e.getValue() != null)
                    .map(e -> e.getValue().toString().trim())
                    .collect(Collectors.joining(";"));
            Assert.assertEquals(ligneAttendu, ligneGenere);
        }
    }
}

