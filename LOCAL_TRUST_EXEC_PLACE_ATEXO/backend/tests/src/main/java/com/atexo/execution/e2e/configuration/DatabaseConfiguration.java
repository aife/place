package com.atexo.execution.e2e.configuration;

import com.atexo.execution.e2e.audit.TestUtilisateurAuditorAware;
import com.atexo.execution.e2e.exceptions.ConfigurationException;
import com.atexo.execution.e2e.sql.MariaDBInstance;
import com.atexo.execution.server.model.Utilisateur;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.time.ZoneId;
import java.util.TimeZone;

@Configuration
@EnableTransactionManagement
@EntityScan("com.atexo.execution.server.model")
@ComponentScan("com.atexo.execution")
@EnableJpaRepositories(basePackages = "com.atexo.execution.server.repository")
@EnableJpaAuditing(dateTimeProviderRef = "auditingDateTimeProvider", auditorAwareRef = "utilisateurAuditorAware")
@Profile("e2e")
@RequiredArgsConstructor
public class DatabaseConfiguration {

    @NonNull
    private MariaDBInstance container;

    private static final ZoneId ZONE_ID = ZoneId.of("Europe/Paris");

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone(ZONE_ID));
    }

    @Bean
    @Primary
    public DataSource dataSource() {
        // container MUST be running
        if (!container.isRunning()) {
            throw new ConfigurationException("Impossible de créer la datasource car le container mariadb {} n'est pas démarré");
        }

        return DataSourceBuilder.create()
                .url(container.getJdbcUrl())
                .username(container.getUsername())
                .password(container.getPassword())
                .build();
    }

    @Bean
    @Primary
    public AuditorAware<Utilisateur> utilisateurAuditorAware() {
        return new TestUtilisateurAuditorAware();
    }
}
