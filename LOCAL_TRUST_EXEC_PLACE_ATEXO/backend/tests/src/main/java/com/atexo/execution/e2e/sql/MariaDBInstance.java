package com.atexo.execution.e2e.sql;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.MariaDBContainer;

import javax.annotation.PreDestroy;

@Component
public class MariaDBInstance extends MariaDBContainer<MariaDBInstance> {

    public MariaDBInstance(@Value("${mariadb.imageName:mariadb:10.3.6}") String imageName, @Value("${mariadb.database}") String databaseName, @Value("${mariadb.commands:--lower_case_table_names=1,--character-set-server=utf8mb4}") String[] commands) {
        super(imageName);

        super.withCommand(commands).withDatabaseName(databaseName);

        super.start();
    }

    @PreDestroy
    public void destroy() {
        this.stop();
    }
}
