package com.atexo.execution.e2e.sql;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SQLService {

    @NonNull
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void load(String... filenames) throws SQLException, IOException {
        for (String filename : filenames) {
            this.load(filename);
        }
    }

    public void load(String filename) throws IOException, SQLException {
        try (var reader = new FileReader((new ClassPathResource(filename)).getFile()); var connection = dataSource.getConnection()) {
            var sr = new ScriptRunner(connection, true, true);

            sr.runScript(reader);
        }
    }

    public List<Map<String, Object>> execQuery(String query) {
        return jdbcTemplate.queryForList(query);
    }
}
