package com.atexo.execution.e2e.cucumber.utils;


import java.util.HashMap;
import java.util.Map;

/**
 * Permet de gerer un contexte different par scenario (en fait 1 par thread).
 */
public enum TestContext {

    CONTEXT;

    private final ThreadLocal<Map<String, Object>> testContexts = ThreadLocal.withInitial(HashMap::new);

    @SuppressWarnings("unchecked")
    public <T> T get(String name) {
        return (T) testContexts.get().get(name);
    }

    public <T> T set(String name, T object) {
        testContexts.get().put(name, object);
        return object;
    }

    public Map<String, Object> getMap() {
        return testContexts.get();
    }

    public void reset() {
        testContexts.get().clear();
    }
}
