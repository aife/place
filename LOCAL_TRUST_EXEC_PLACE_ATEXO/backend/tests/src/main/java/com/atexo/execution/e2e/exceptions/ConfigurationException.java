package com.atexo.execution.e2e.exceptions;

public class ConfigurationException extends RuntimeException {

	public ConfigurationException(String message) {
		super(message);
	}
}
