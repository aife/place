package com.atexo.execution.e2e.cucumber.steps;

import com.atexo.execution.e2e.cucumber.utils.TestContext;
import io.cucumber.java.en.Then;

import static org.assertj.core.api.Assertions.assertThat;

public class CompareSteps {

    @Then("je compare le code de retour {string} avec {string}")
    public void compare(String varName, String code) {
        var resultWs = TestContext.CONTEXT.get(varName);
        assertThat(resultWs).isEqualTo(code);
    }
}
