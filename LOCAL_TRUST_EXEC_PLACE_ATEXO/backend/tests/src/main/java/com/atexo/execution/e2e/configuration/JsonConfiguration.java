package com.atexo.execution.e2e.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.util.JsonExpectationsHelper;

@Configuration
public class JsonConfiguration {

	@Bean
	public ObjectMapper getObjectMapper() {
		var result = new ObjectMapper();

		result.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		result.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		result.registerModule(new JavaTimeModule());

		return result;
	}

	@Bean
	public JsonExpectationsHelper getJsonExpectationsHelper() {
		return new JsonExpectationsHelper();
	}
}
