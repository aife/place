package com.atexo.execution.e2e.cucumber.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.test.util.JsonExpectationsHelper;
import org.springframework.util.FileCopyUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Component
public class FileManageUtils {

    @Resource
    protected ObjectMapper objectMapper;
    @Resource
    protected JsonExpectationsHelper jsonHelper;

    public String asString(org.springframework.core.io.Resource resource) throws IOException {
        try (var reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        }
    }

    public <T> void assertMatch(String filename, T object) throws Exception {
        var actual = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        var expected = asString(new ClassPathResource(filename));
        jsonHelper.assertJsonEqual(expected, actual, false);
    }

    public <T> T read(String filename, Class<T> type) throws IOException {
        return this.objectMapper.readValue(new ClassPathResource(filename).getFile(), type);
    }
}
