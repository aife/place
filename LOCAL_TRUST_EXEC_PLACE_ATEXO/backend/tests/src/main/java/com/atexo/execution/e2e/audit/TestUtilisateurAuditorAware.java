package com.atexo.execution.e2e.audit;

import com.atexo.execution.server.model.Utilisateur;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

@Configuration
@Profile("e2e")
public class TestUtilisateurAuditorAware implements AuditorAware<Utilisateur> {

    @Override
    public Optional<Utilisateur> getCurrentAuditor() {
        return Optional.empty();
    }
}
