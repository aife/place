package com.atexo.execution.e2e.auth;

import com.atexo.execution.e2e.configuration.KeycloackConfiguration;
import com.atexo.execution.e2e.configuration.OauthConfiguration;
import com.atexo.execution.e2e.exceptions.OAuthException;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.net.URIBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.atexo.execution.e2e.configuration.OauthConfiguration.*;

@Service
@RequiredArgsConstructor
public class OauthService {

    @NonNull
    private OauthConfiguration configuration;

    public String get(String scheme, String host, int port) throws URISyntaxException {
        var restTemplate = new RestTemplate();
        var uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(port).setPath(configuration.getEndpoint()).build();

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(configuration.getToken());
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add(GRANT_TYPE, configuration.getGrantType());
        map.add(CLIENT_ID, configuration.getClientId());
        map.add(SCOPE, configuration.getScope());

        final var body = restTemplate.postForEntity(uri, new HttpEntity<>(map, headers), Token.class).getBody();

        if (null == body) {
            throw new OAuthException("Impossible de récupéer le jeton EXEC OAUTH");
        }

        return body.getAccessToken();
    }
}
