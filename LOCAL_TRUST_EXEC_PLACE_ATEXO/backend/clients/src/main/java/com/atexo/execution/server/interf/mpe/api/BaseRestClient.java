package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TimeZone;

@Component
@Profile("!mock")
@RequiredArgsConstructor
@AllArgsConstructor
@Slf4j
public abstract class BaseRestClient {

    private static final String API_PATH = "/app.php/api/";
    private static final String API_VERSION = "v1";

    protected RestTemplate restTemplate = new RestTemplate();

    protected final TokenManager tokenManager;
    protected final PlateformeRepository plateformeRepository;

    protected URI buildUriForGet(String plateformeUid, String entite, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, entite, page, itemsPerPage);
        var urlTemplate = uri.toString();
        if (null != lastUpdate) {
            var instant = lastUpdate.atZone(TimeZone.getDefault().toZoneId()).toInstant();
	        var timeStamp = instant.getEpochSecond();
            urlTemplate += "&lastUpdateTimestamp=" + timeStamp;
        }
        urlTemplate += "&disableFiltering=true";
        return URI.create(urlTemplate);
    }

    protected URI buildUriForGet(String plateformeUid, String entite, int page, int itemsPerPage) {
        var plateforme = getPlateforme(plateformeUid);
        var map = Map.of(
                "url", plateforme.getMpeUrl(),
		"apiPath", API_PATH,
		"apiVersion", API_VERSION,
		"entite", entite,
		"limit", itemsPerPage,
		"page", page,
                "ticket", tokenManager.getToken(plateforme)
        );

        return new UriTemplate("{url}/{apiPath}/{apiVersion}/{entite}.xml?ticket={ticket}&limit={limit}&page={page}").expand(map).normalize();
    }

    public XMLGregorianCalendar toXMLGregorianCalendar(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
        } catch (DatatypeConfigurationException e) {
            log.error("Impossible de convertir {} en Gregorian calendar", localDate);
        }
        return xmlGregorianCalendar;
    }

    protected Plateforme getPlateforme(String plateformeUid) {
        return plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme " + plateformeUid));
    }


}
