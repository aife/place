package com.atexo.execution.server.interf.faq;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.faq.reponse.Reponse;

public interface FaqService {

    Reponse init(UtilisateurDTO utilisateur, String userAgent);

    Reponse initWithContract(UtilisateurDTO utilisateur, String userAgent, Long idContrat);
}
