package com.atexo.execution.server.api.entreprise.v3.config;

import com.atexo.execution.server.entreprise.v3.api.AttestationsSocialesEtFiscalesApi;
import com.atexo.execution.server.entreprise.v3.api.InformationsGnralesApi;
import com.atexo.execution.server.entreprise.v3.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiClientConfig {

    @Value("${baseCentrale.url:}")
    private String url;

    @Value("${baseCentrale.token:}")
    private String token;

    @Bean
    ApiClient apiClient(){
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(url);
        apiClient.setBearerToken(token);
        return apiClient;
    }

    @Bean
    InformationsGnralesApi informationsGnralesApi(){
       return new InformationsGnralesApi(apiClient());
    }

    @Bean
    AttestationsSocialesEtFiscalesApi attestationsSocialesEtFiscalesApi(){
        return new AttestationsSocialesEtFiscalesApi(apiClient());
    }


}
