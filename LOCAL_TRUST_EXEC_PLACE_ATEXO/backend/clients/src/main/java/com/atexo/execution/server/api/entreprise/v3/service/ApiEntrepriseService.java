package com.atexo.execution.server.api.entreprise.v3.service;


import com.atexo.execution.server.entreprise.v3.model.V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneEtablissementsSiretGet200ResponseData;
import com.atexo.execution.server.entreprise.v3.model.V3InseeSireneUnitesLegalesSirenGet200ResponseData;


public interface ApiEntrepriseService {

    V3InseeSireneUnitesLegalesSirenGet200ResponseData getEntreprise(String siren, String recipient);

    V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData getExtraitKbis(String siren, String recipient);

    V3InseeSireneEtablissementsSiretGet200ResponseData getEtablissement(String siret, String recipient);

    String getAttestationCotisationsCongesPayesChomageIntemperies(String siren, String recipient);

    String getCarteProfessionnelleTravauxPublics(String siren, String recipient);

    String getAttestationCotisationsRetraite(String siret, String recipient);

    String getAttestationFiscale(String siren, String recipient);

    String getAttestationVigilance(String siren, String recipient);

}
