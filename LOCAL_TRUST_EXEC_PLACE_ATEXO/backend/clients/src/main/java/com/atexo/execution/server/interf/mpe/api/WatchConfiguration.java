package com.atexo.execution.server.interf.mpe.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;

@Configuration
public class WatchConfiguration {
    @Bean
    public StopWatch watch() {
        return new StopWatch("Synchronisation");
    }
}
