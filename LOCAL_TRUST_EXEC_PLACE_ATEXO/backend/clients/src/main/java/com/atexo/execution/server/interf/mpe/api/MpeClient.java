package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.common.mpe.AgentMPE;
import com.atexo.execution.common.mpe.ws.api.*;
import com.atexo.execution.server.model.Evenement;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface MpeClient {

    List<OrganismeType> getOrganimes(String plateformeUid,int page, int itemsPerPage, LocalDateTime lastUpdate);

    List<ContratType> getContrats(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate);

    List<ContactType> getContacts(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate);

    List<EntrepriseType> getEntreprises(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate);

    List<EtablissementType> getEtablissements(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate);

    ReferentielsType getReferentiels(String plateformeUid);

    List<ServiceType> getServices(String plateformeUid, int page, int nbItems, LocalDateTime lastUpdate);

    List<AgentType> getUtilisateurs(String plateformeUid, int page, int nbItems, LocalDateTime lastUpdate);

    AgentSSOType getUtilisateurSSO(String plateformeUid, final String sso);

    String formatUrlCreationMarcheSubsequent(String uuid);

    String formatUrlEchangeChorus(String idExterne);

    Mpe publierModification(String plateformeUid, Evenement evenement) throws MPEApiException;

    AgentMPE getUtilisateur(String pfUid, Long idAgentMpe);

    Set<Long> getUtilisateurPerimetreVision(String plateformeUid, Long idAgentMpe);
}
