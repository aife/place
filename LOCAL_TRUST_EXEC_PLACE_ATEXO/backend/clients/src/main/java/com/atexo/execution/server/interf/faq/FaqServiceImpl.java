package com.atexo.execution.server.interf.faq;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.common.dto.ServiceDTO;
import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.faq.*;
import com.atexo.execution.common.dto.faq.reponse.Reponse;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.TypeContrat;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FaqServiceImpl implements FaqService {

    static final Logger LOG = LoggerFactory.getLogger(FaqServiceImpl.class);
    private static final String API_CONTEXT = "/api/temp_contextes";
    private final ContratRepository contratRepository;
    private final PlateformeRepository plateformeRepository;
    @Value("${faq.url:''}")
    private String faqUrl;
    @Value("${faq.utah.url:''}")
    private String utahUrl;
    @Value("${application.version:''}")
    private String applicationVersion;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private Reponse init(Data data) {
        final RestTemplate restTemplate = new RestTemplate();
        Envoi envoi = new Envoi();
        Reponse objectRetour;
        try {
            envoi.setData(objectMapper.writeValueAsString(data));
            String url = faqUrl + API_CONTEXT;
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/ld+json");
            HttpEntity<Envoi> entity = new HttpEntity<>(envoi, headers);

            LOG.info("Initialisation du contexte FAQ sur l'url {} avec les données suivantes:\n{}", url, envoi);
            objectRetour = restTemplate.postForEntity(url, entity, Reponse.class).getBody();
        } catch (JsonProcessingException e) {
            LOG.error("erreur lors de la sérialisation du contexte FAQ", e);
            return null;
        }
        return objectRetour;
    }

    @Override
    public Reponse init(UtilisateurDTO utilisateur, String userAgent) {
        Data data = buildData(userAgent, utilisateur);
        return init(data);
    }

    @Override
    public Reponse initWithContract(UtilisateurDTO utilisateur, String userAgent, Long idContrat) {
        Data data = buildData(userAgent, utilisateur);
        data.setMetier(new Metier("metier", "métier", "Métier"));
        Contrat contrat = contratRepository.findById(idContrat).orElseThrow(() -> new IllegalArgumentException("Contrat introuvable"));
        // champs métier
        ChampsMetier champsMetier = new ChampsMetier();
        champsMetier.setIdContrat(new Champ("ID du contrat", "ID du contrat", String.valueOf(contrat.getId()), true, false));
        champsMetier.setRefContrat(new Champ("Réf. du contrat", "Réf. du contrat", Optional.of(contrat.getNumero())
                .filter(this::isNotEmpty)
                .orElse(contrat.getReferenceLibre())));
        champsMetier.setTypeContrat(new Champ("Type de contrat", "Type de contrat", Optional.of(contrat.getType()).map(TypeContrat::getLabel).orElse("")));
        champsMetier.setStatutContrat(new Champ("Statut du contrat", "Statut du contrat", Optional.of(contrat.getStatut()).map(Enum::toString).orElse("")));
        data.getMetier().setChamps(champsMetier);
        return init(data);
    }

    private boolean isNotEmpty(String s) {
        return s != null && !"".equals(s.trim());
    }

    private Data buildData(String browserAgent, UtilisateurDTO utilisateur) {
        var plateforme = plateformeRepository.findByMpeUid(utilisateur.getPlateformeUid()).orElseThrow(() -> new IllegalArgumentException("Plateforme introuvable pour l'utilisateur "+utilisateur.getUuid()));
        if(plateforme.getFaqUid() == null) {
            throw new IllegalArgumentException("Plateforme "+plateforme.getMpeUid()+" n'a pas de FAQ configurée");
        }
        Data data = new Data();
        data.setApplicatif(new Applicatif("applicatif", "applicatif", "Contexte applicatif de la demande"));
        // champs applicatifs
        ChampsApplicatifs champsApplicatifs = new ChampsApplicatifs();
        champsApplicatifs.setApplication(new Champ("Application", "Application", plateforme.getFaqUid()));
        champsApplicatifs.setTypeActeur(new Champ("Type d'utilisateur", "Type d'utilisateur", "agent_place", true, false));
        champsApplicatifs.setVersion(new Champ("Version", "Version", applicationVersion));
        Champ utilisateurUtah = new Champ("utiliser_utah", "utiliser_utah", "utiliser_utah");
        utilisateurUtah.setTypeValeur("boolean");
        utilisateurUtah.setVisibleSupport(false);
        utilisateurUtah.setVisibleUtilisateur(false);
        champsApplicatifs.setUtiliserUtah(utilisateurUtah);
        champsApplicatifs.setUrlUtah(new Champ("url_utah", "URL Utah", utahUrl));
        champsApplicatifs.getUrlUtah().setVisibleUtilisateur(false);
        champsApplicatifs.getUrlUtah().setVisibleSupport(false);
        champsApplicatifs.setUtilisateurOrganisme(new Champ("Organisme", "Organisme", Optional.ofNullable(utilisateur.getService()).map(ServiceDTO::getOrganisme).map(OrganismeDTO::getNom).orElse("")));
        champsApplicatifs.setUtilisateurEntite(new Champ("Service", "Service", Optional.ofNullable(utilisateur.getService()).map(ServiceDTO::getNom).orElse("")));
        champsApplicatifs.setUtilisateurId(new Champ("ID de l'utilisateur", "ID de l'utilisateur", String.valueOf(utilisateur.getId()), true, false));
        champsApplicatifs.setUtilisateurEmail(new Champ("Courriel de l'utilisateur", "Courriel de l'utilisateur", utilisateur.getEmail()));
        champsApplicatifs.setUtilisateurNom(new Champ("Nom de l'utilisateur", "Nom de l'utilisateur", utilisateur.getPrenom() + " " + utilisateur.getNom()));
        data.getApplicatif().setChampsApplicatifs(champsApplicatifs);

        //champs techniques
        UserAgent userAgent = UserAgent.parseUserAgentString(browserAgent);
        String browser = userAgent.getBrowser().getName() + " " + userAgent.getBrowserVersion().getVersion();
        String operatingSystem = userAgent.getOperatingSystem().getName();
        data.setTechnique(new Technique("technique", "technique", "technique"));
        ChampsTechniques champsTechniques = new ChampsTechniques();
        champsTechniques.setOs(new Champ("Système d'exploitation", "Système d'exploitation", operatingSystem));
        champsTechniques.setNavigateur(new Champ("Navigateur", "Navigateur", browser));
        data.getTechnique().setChamps(champsTechniques);
        return data;
    }
}
