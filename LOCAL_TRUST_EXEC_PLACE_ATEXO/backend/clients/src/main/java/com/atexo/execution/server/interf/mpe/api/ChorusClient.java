package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.actes.ActeModificatif;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Client CHORUS
 */
public interface ChorusClient {

    List<EchangeChorusType> getEchangesChorus(String plateformeUid, int page, int nbItems, LocalDateTime lastSynchronizationDate);

    File telechargeZipChorus(String plateformeUid, String idExterne) throws MPEApiException;

    EchangeChorusType publierEchangeChorus(String plateformeUid, Acte acte, EchangeChorus echange) throws MPEApiException;

    EchangeChorusType republierEchangeChorus(String plateformeUid, Acte acte, EchangeChorus echange) throws MPEApiException;

    void envoyerZipChorus(String plateformeUid, EchangeChorus echange, File zipFile) throws MPEApiException;

    void renvoyerZipChorus(String plateformeUid, EchangeChorus echange, File zipFile) throws MPEApiException;

    void publierModification(ActeModificatif evenement) throws MPEApiException;
}
