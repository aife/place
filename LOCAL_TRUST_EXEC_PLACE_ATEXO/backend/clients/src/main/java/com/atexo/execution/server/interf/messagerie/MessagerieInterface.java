package com.atexo.execution.server.interf.messagerie;

import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.messagerie.MessecInitialisation;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.model.Contact;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

public interface MessagerieInterface {

    String initTokenRedaction(MessecInitialisation initialisation,Long idObjetMetierPfEmetteur, String numeroLong, String cartouche, Collection<Contact> contacts)
            throws ApplicationException;

    String initTokenSuivi(String messagerieIdPfEmetteur,Long idObjetMetierPfEmetteur, Long idSousObjetMetierPfEmetteur, String numeroLong) throws ApplicationException;

    String uploadPieceJointe(String tokenRedaction, String fileName, String identifier, File file)
            throws ApplicationException;

    InputStream downloadPieceJointe(String codeLien, String pieceJointeId) throws ApplicationException;

    PieceJointeDTO getPieceJointe(String codeLien, String pieceJointeId) throws ApplicationException;

    ContratMessageStatusDTO countMessageStatus(String tokenSuivi, String idContrat) throws ApplicationException;

    String initTokenNotification(MessecInitialisation initialisation,Long contratId, String numeroLong, String cartouche) throws ApplicationTechnicalException;

    String initTokenNotification(MessecInitialisation initialisation,Long contratId, Long acteId, String numeroLong, String cartouche,Collection<Contact> contacts) throws ApplicationTechnicalException;

    String initTokenRedactionByCodeLien(String codeLien);
}
