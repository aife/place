package com.atexo.execution.server.interf.messagerie;

import com.atexo.execution.common.dto.ContratMessageStatusDTO;
import com.atexo.execution.common.dto.messagerie.MessecInitialisation;
import com.atexo.execution.common.exception.ApplicationException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.model.Contact;
import com.atexo.execution.server.model.Etablissement;
import com.atexo.execution.server.model.Fournisseur;
import com.atexo.execution.server.repository.TemplateRepository;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.messages.consultation.TypeDestinataire;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

@Service
@Transactional
public class MessagerieInterfaceImpl implements MessagerieInterface {

    final static Logger LOG = LoggerFactory.getLogger(MessagerieInterfaceImpl.class);

    @Value("${messagerie.URI:}")
    private String messagerieURIAsString = null;


    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    TemplateRepository templateRepository;

    private MessageSecuriseInit makeMessageSecuriseInit(MessecInitialisation initialisation) {
        MessageSecuriseInit messageSecuriseInit = new MessageSecuriseInit();

        messageSecuriseInit.setMaxTailleFichiers(initialisation.getMaxTailleFichiers());
        messageSecuriseInit.setIdPfEmetteur(initialisation.getMessagerieIdPfEmetteur());
        messageSecuriseInit.setUrlPfEmetteur(initialisation.getMessagerieUrlPfEmetteur());
        messageSecuriseInit.setNomPfEmetteur(initialisation.getMessagerieNomPfEmetteur());

        messageSecuriseInit.setIdPfDestinataire(initialisation.getMessagerieIdPfDestinataire());
        messageSecuriseInit.setUrlPfDestinataire(initialisation.getMessagerieUrlPfDestinataire());
        messageSecuriseInit.setNomPfDestinataire(initialisation.getMessagerieNomPfDestinataire());

        messageSecuriseInit.setUrlPfDestinataireVisualisation(initialisation.getMessagerieUrlPfDestinataireVisualisation());
        messageSecuriseInit.setUrlPfEmetteurVisualisation(initialisation.getMessagerieUrlPfEmetteur());
        messageSecuriseInit.setUrlPfReponse(initialisation.getMessagerieUrlPfReponse());
        messageSecuriseInit.setEmailExpediteur(initialisation.getAdresseMailExpediteur());
        messageSecuriseInit.setSignatureAvisPassage(initialisation.getSignatureAvisPassage());
        messageSecuriseInit.setNomCompletExpediteur(initialisation.getMessagerieNomPfEmetteur());
        messageSecuriseInit.setLogoSrc(initialisation.getMessagerieLogoSrc());

        return messageSecuriseInit;
    }

    @Override
    public String initTokenRedaction(MessecInitialisation initialisation,Long idObjetMetier, String numeroLong, String cartouche, Collection<Contact> contacts)
            throws ApplicationException {

        MessageSecuriseInit messageSecuriseInit = getMessageSecuriseInit(initialisation,idObjetMetier, numeroLong, cartouche,contacts);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(messagerieURIAsString + "/rest/redaction/initToken",
                messageSecuriseInit, String.class);

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ApplicationTechnicalException();
        } else {
            return responseEntity.getBody();
        }
    }

    private MessageSecuriseInit getMessageSecuriseInit(MessecInitialisation initialisation,Long idObjetMetier, String numeroLong, String cartouche, Collection<Contact> contacts) {
        MessageSecuriseInit messageSecuriseInit = makeMessageSecuriseInit(initialisation);
        messageSecuriseInit.setTypeMessageDefaut(String.valueOf(TypeMessage.TypeMessage1.getIndex()));
        messageSecuriseInit.setCartouche(cartouche);
        messageSecuriseInit.setIdObjetMetierPfEmetteur(idObjetMetier.intValue());
        messageSecuriseInit.setIdObjetMetierPfDestinataire(idObjetMetier.intValue());
        messageSecuriseInit.setRefObjetMetier(numeroLong);
        if (!contacts.isEmpty()) {
            messageSecuriseInit.setDestinatairesPfDestinataire(new MessageSecuriseInit.DestinatairesPfDestinataire());
            for (var contact : contacts) {
                var entreprise = Optional.ofNullable(contact.getEtablissement()).map(Etablissement::getFournisseur).map(Fournisseur::getRaisonSociale).orElse("Contacts");
                var destinataire = new MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire();
                destinataire.setMailContactDestinataire(contact.getEmail());
                destinataire.setNomContactDest(Objects.toString(contact.getNom(), "") + " " + Objects.toString(contact.getPrenom(), ""));
                destinataire.setType(entreprise);
                destinataire.setNomEntrepriseDest(entreprise);
                messageSecuriseInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().add(destinataire);
            }
        }
        return messageSecuriseInit;
    }

    @Override
    public String initTokenSuivi(String messagerieIdPfEmetteur,Long idObjetMetierPfEmetteur, Long idSousObjetMetierPfEmetteur, String numeroLong) throws ApplicationException {
        RechercheMessage rechercheMessage = new RechercheMessage();

        rechercheMessage.setIdPlateformeRecherche(messagerieIdPfEmetteur);
        rechercheMessage.setTypePlateformeRecherche(TypeDestinataire.EMETTEUR);
        rechercheMessage.setIdObjetMetier(idObjetMetierPfEmetteur.intValue());
        rechercheMessage.setIdSousObjetMetier(idSousObjetMetierPfEmetteur == null ? null : idSousObjetMetierPfEmetteur.intValue());
        rechercheMessage.setRefObjetMetier(numeroLong);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(messagerieURIAsString + "/rest/suivi/initToken",
                rechercheMessage, String.class);
        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ApplicationTechnicalException();
        } else {
            return responseEntity.getBody();
        }
    }

    @Override
    public String uploadPieceJointe(String tokenRedaction, String fileName, String identifier, File file)
            throws ApplicationException {
        String reference = null;
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
            parts.add("file", new FileSystemResource(file));

            HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(parts, headers);

            reference = restTemplate.postForEntity(
                    messagerieURIAsString + "/rest/upload/?token={token}&fileName={fileName}&identifier={identifier}", request,
                    String.class, tokenRedaction, fileName, identifier).getBody();

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ApplicationTechnicalException(e);
        }
        if (reference.contains("\"")) {
            reference = reference.replace("\"", "");
        }
        return reference;
    }

    @Override
    public InputStream downloadPieceJointe(String codeLien, String pieceJointeId) throws ApplicationException {
        InputStream openStream = null;
        try {
            openStream = new URL(messagerieURIAsString + "/rest/suivi/downloadPieceJointe?codeLien=" + codeLien
                    + "&pieceJointeId=" + pieceJointeId).openStream();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            IOUtils.closeQuietly(openStream);
            throw new ApplicationTechnicalException(e);
        }
        return openStream;
    }

    @Override
    public PieceJointeDTO getPieceJointe(String codeLien, String pieceJointeId) throws ApplicationException {
        ResponseEntity<PieceJointeDTO> responseEntity = null;
        try {
            responseEntity = restTemplate.getForEntity(
                    messagerieURIAsString + "/rest/suivi/getPieceJointe?codeLien={codeLien}&pieceJointeId={pieceJointeId}",
                    PieceJointeDTO.class, codeLien, pieceJointeId);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ApplicationTechnicalException(e);
        }

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ApplicationTechnicalException();
        } else {
            return responseEntity.getBody();
        }
    }

    @Override
    public ContratMessageStatusDTO countMessageStatus(String tokenSuivi, String idContrat) {
        ResponseEntity<ContratMessageStatusDTO> responseEntity = restTemplate.getForEntity(
                messagerieURIAsString + "/rest/suivi/countMessageStatus?token={token}&idObjetMetier={idObjetMetier}",
                ContratMessageStatusDTO.class, tokenSuivi, idContrat);
        return responseEntity.getBody();
    }

    @Override
    public String initTokenNotification(MessecInitialisation initialisation,Long idObjetMetier, String numeroLong, String cartouche) throws ApplicationTechnicalException {
        List<Contact> contacts = new ArrayList<>();
        var messageSecuriseInit = getMessageSecuriseInit(initialisation,idObjetMetier, numeroLong, cartouche,contacts);
        contacts.forEach(contact -> messageSecuriseInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().add(buildDestinataire(contact)));
        if (contacts.size() == 1) {
            messageSecuriseInit.setDestinatairesPreSelectionnes(new MessageSecuriseInit.DestinatairesPreSelectionnes());
            contacts.stream().findFirst().ifPresent(c -> messageSecuriseInit.getDestinatairesPreSelectionnes().getDestinatairePreSelectionne().add(buildDestinatairePreselectionne(c)));
        }
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(messagerieURIAsString + "/rest/redaction/initToken",
                messageSecuriseInit, String.class);

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ApplicationTechnicalException();
        } else {
            return responseEntity.getBody();
        }
    }

    @Override
    public String initTokenNotification(MessecInitialisation initialisation,Long idObjetMetier, Long acteId, String numeroLong, String cartouche,Collection<Contact> contacts) throws ApplicationTechnicalException {
        var messageSecuriseInit = getMessageSecuriseInit(initialisation,idObjetMetier, numeroLong, cartouche,contacts);
        messageSecuriseInit.setIdSousObjetMetier(acteId.intValue());
        if (!contacts.isEmpty()) {
            messageSecuriseInit.setDestinatairesPfDestinataire(new MessageSecuriseInit.DestinatairesPfDestinataire());
            contacts.forEach(contact -> messageSecuriseInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().add(buildDestinataire(contact)));
            if (contacts.size() == 1) {
                messageSecuriseInit.setDestinatairesPreSelectionnes(new MessageSecuriseInit.DestinatairesPreSelectionnes());
                contacts.stream().findFirst().ifPresent(c -> messageSecuriseInit.getDestinatairesPreSelectionnes().getDestinatairePreSelectionne().add(buildDestinatairePreselectionne(c)));
            }
        }
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(messagerieURIAsString + "/rest/redaction/initToken",
                messageSecuriseInit, String.class);

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new ApplicationTechnicalException();
        } else {
            return responseEntity.getBody();
        }
    }

    @Override
    public String initTokenRedactionByCodeLien(String codeLien) {
        var url = messagerieURIAsString + "/rest/redaction/initTokenByCodeLien?codeLien=" + codeLien;
        return restTemplate.getForEntity(url, String.class).getBody();
    }

    private MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire buildDestinataire(Contact contact) {
        var d = new MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire();
        d.setType("Entrprise à notifier");
        d.setNomContactDest(Objects.toString(contact.getPrenom(), "") + " " + Objects.toString(contact.getNom(), ""));
        d.setMailContactDestinataire(contact.getEmail());
        return d;
    }

    private MessageSecuriseInit.DestinatairesPreSelectionnes.DestinatairePreSelectionne buildDestinatairePreselectionne(Contact contact) {
        var d = new MessageSecuriseInit.DestinatairesPreSelectionnes.DestinatairePreSelectionne();
        d.setType("Entrprise à notifier");
        d.setNomContactDest(Objects.toString(contact.getPrenom(), "") + " " + Objects.toString(contact.getNom(), ""));
        d.setMailContactDestinataire(contact.getEmail());
        return d;
    }
}
