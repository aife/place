package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.common.mpe.TicketType;
import com.atexo.execution.common.mpe.Token;
import com.atexo.execution.common.mpe.TokenRequest;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class TokenManager {

    private final RestTemplate restTemplate = new RestTemplate();
    private final PlateformeRepository plateformeRepository;
    private final Map<Plateforme, String> storedTokens = new HashMap<>();

    @Value("${mpe.api.tokenExpirationHours:1}")
    private int tokenExpirationHours;

    public String getToken(Plateforme plateforme) {
        var token = storedTokens.get(plateforme);
        if (token == null || plateforme.getTokenUpdateDate() == null || Duration.between(plateforme.getTokenUpdateDate(), LocalDateTime.now()).getSeconds() / 3600f > tokenExpirationHours) {
            try {
                var ticketType = getTokenCall(plateforme);
                if (ticketType != null) {
                    token = ticketType.getValue();
                    plateforme.setTokenUpdateDate(LocalDateTime.now());
                    storedTokens.put(plateforme, token);
                }
            } catch (Exception ex) {
                storedTokens.remove(plateforme);
                plateforme.setTokenUpdateDate(null);
                log.error("erreur lors de la récupération du token", ex);
            } finally {
                plateformeRepository.save(plateforme);
            }
        }
        return token;
    }

    TicketType getTokenCall(Plateforme plateforme) {
        var parameters = Map.of(
                "uri", plateforme.getMpeUrl(),
                "login", plateforme.getMpeApiLogin(),
                "password", plateforme.getMpeApiPassword()
        );

        var uri = new UriTemplate("{uri}/api.php/ws/authentification/connexion/{login}/{password}").expand(parameters).normalize();

        try {
            var jeton = restTemplate.getForEntity(uri, TicketType.class).getBody();

            log.info("Appel du webservice d'authentification (pour récuperer le jeton) depuis {}. Résultat = {}", uri, Optional.ofNullable(jeton).map(TicketType::getValue).orElse(null));

            return jeton;
        } catch (RestClientException | NullPointerException ex) {
            log.error("erreur lors de la récupération du token V1", ex);
            return null;
        }
    }

    public Token getTokenV2(Plateforme plateforme) {
        var request = TokenRequest.builder().login(plateforme.getMpeApiLogin()).password(plateforme.getMpeApiPassword()).build();
        Map<String, Object> map = new HashMap<>();
        map.put("url", plateforme.getMpeUrl());
        var url = new UriTemplate("{url}/api/v2/token").expand(map).normalize();
        var httEntity = new HttpEntity<>(request);
        var reponse = restTemplate.exchange(url, HttpMethod.POST, httEntity, Token.class);
        var cookie = reponse.getHeaders().get(HttpHeaders.SET_COOKIE);
        var token = reponse.getBody();
        if (token != null && cookie != null && !cookie.isEmpty()) {
            token.setCookie(String.join(",", cookie));
            log.info("Appel du webservice d'authentification (pour récuperer le jeton) depuis {}. Résultat = {}", url, token);
        }
        return token;
    }

}


