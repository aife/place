package com.atexo.execution.server.interf.mpe.api.v2;


import com.atexo.execution.common.mpe.ws.v2.ContactType;


public interface MpeClient {

    ContactType getContacts(String plateformeUid, int page, int itemsPerPage);
}
