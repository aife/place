package com.atexo.execution.server.interf.mpe.api.v2;

import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Client CHORUS
 */
@Component("ChorusClientV2")
public interface ChorusClient {

    List<EchangeChorusType> getEchangesChorus(String plateformeUid, int page, int nbItems, LocalDateTime lastSynchronizationDate);

}
