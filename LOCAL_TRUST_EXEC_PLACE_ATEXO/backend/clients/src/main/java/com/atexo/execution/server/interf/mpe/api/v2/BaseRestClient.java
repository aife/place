package com.atexo.execution.server.interf.mpe.api.v2;


import com.atexo.execution.server.interf.mpe.api.TokenManager;
import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.util.Map;

@Component("BaseRestClientV2")
@Profile("!mock")
@RequiredArgsConstructor
@AllArgsConstructor
@Slf4j
public abstract class BaseRestClient {

    private static final String API_PATH = "/api/";
    private static final String API_VERSION = "v2";

    protected RestTemplate restTemplate = new RestTemplate();

    protected final TokenManager tokenManager;
    protected final PlateformeRepository plateformeRepository;

    protected URI buildUriForGet(String plateformeUid, String entite, int page, int itemsPerPage) {
        var plateforme = getPlateforme(plateformeUid);
        var map = Map.of(
                "url", plateforme.getMpeUrl(),
                "apiPath", API_PATH,
                "apiVersion", API_VERSION,
                "entite", entite,
                "itemsPerPage", itemsPerPage,
                "page", page
        );

        return new UriTemplate("{url}/{apiPath}/{apiVersion}/{entite}?page={page}&itemsPerPage={itemsPerPage}").expand(map).normalize();
    }

    protected Plateforme getPlateforme(String plateformeUid) {
        return plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new IllegalArgumentException("Impossible de trouver la plateforme " + plateformeUid));
    }
}
