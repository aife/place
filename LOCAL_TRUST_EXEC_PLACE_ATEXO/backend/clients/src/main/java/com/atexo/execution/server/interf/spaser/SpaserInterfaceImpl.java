package com.atexo.execution.server.interf.spaser;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import com.atexo.execution.common.dto.spaser.SpaserContext;
import com.atexo.execution.common.dto.spaser.SpaserInit;
import com.atexo.execution.common.dto.spaser.SpaserReponse;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.model.Contrat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class SpaserInterfaceImpl implements SpaserInterface {


    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${spaser.URI:}")
    private String spaserURIAsString;

    @Value("${spaser.api.init-context:}")
    private String apiInitContextAsString;

    @Override
    public String initContext(Oauth2TokenDTO oauth, UtilisateurDTO utilisateur, Contrat contrat) throws ApplicationBusinessException {

        SpaserInit spaserInit = new SpaserInit();
        spaserInit.setToken(oauth.getAccesToken());
        spaserInit.setRefreshToken(oauth.getRefreshToken());
        spaserInit.setPlateformUid(utilisateur.getPlateformeUid());
        spaserInit.setOrganisme(utilisateur.getService().getOrganisme().getAcronyme());
        SpaserContext spaserContext = new SpaserContext();

        spaserContext.setObjectMetierType("ContratTitulaire");
        spaserContext.setObjetMetierExternalId(contrat.getId());
        ;
        spaserContext.setObjetMetierDateFin(contrat.getDateFinContrat());
        spaserContext.setAgentFirstname(utilisateur.getPrenom());
        spaserContext.setAgentLastname(utilisateur.getNom());
        spaserContext.setTypeRoute("indicateur");
        spaserInit.setSpaserContext(spaserContext);

        try {
            ResponseEntity<SpaserReponse> responseEntity = restTemplate.postForEntity(spaserURIAsString + apiInitContextAsString, spaserInit, SpaserReponse.class);

        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            throw new ApplicationBusinessException();
        }
        if (responseEntity.getBody() == null || responseEntity.getBody().getRedirectUrl() == null)
            throw new ApplicationBusinessException();
        return spaserURIAsString + "/" + responseEntity.getBody().getRedirectUrl();
        } catch (RestClientResponseException e) {
            log.error("Error lors de la récuparation de contexte spaser {}",e.getResponseBodyAsString());
            throw new ApplicationBusinessException("Error lors de la récuparation de contexte spaser ");
        }
    }
}
