package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.common.mpe.AgentMPE;
import com.atexo.execution.common.mpe.ws.api.*;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.lang.Boolean.TRUE;

@Component
public class RestMpeClient extends BaseRestClient implements MpeClient {

    static final Logger LOG = LoggerFactory.getLogger(RestMpeClient.class);
    private static final String API_PATH = "/app.php/api/";
    private static final String API_VERSION = "v1";

    public static final String CONTRAT_ID = "contratId";
    public static final String CONTRAT_UUID = "contratUuid";

    @Value("${mpe.marche.subsequent.path:}")
    private String marcheSubsequentPath;

    @Value("${mpe.echangeChorus.path:}")
    private String echangeChorusPath;


    public RestMpeClient(TokenManager tokenManager, PlateformeRepository plateformeRepository) {
        super(tokenManager, plateformeRepository);
    }


    @Override
    public List<OrganismeType> getOrganimes(String plateformeUid,int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "organismes", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'organismes' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getOrganismes).map(Mpe.Reponse.Organismes::getOrganisme).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des organismes", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ContratType> getContrats(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "contrats", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'contrats' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getContrats).map(Mpe.Reponse.Contrats::getContrat).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des contrats", ex);
            return new ArrayList<>();
        }

    }

    @Override
    public List<ContactType> getContacts(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "contacts", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'contacts' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getContacts).map(Mpe.Reponse.Contacts::getContact).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des contacts", ex);
            return new ArrayList<>();
        }

    }

    @Override
    public List<EntrepriseType> getEntreprises(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "entreprises", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'entreprises' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getEntreprises).map(Mpe.Reponse.Entreprises::getEntreprise).orElse(new ArrayList<>());
        } catch (RestClientException | NullPointerException ex) {
            LOG.error("erreur lors de la récupération des entreprises", ex);
            return new ArrayList<>();
        }

    }

    @Override
    public List<EtablissementType> getEtablissements(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "etablissements", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'etablissements' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getEtablissements).map(Mpe.Reponse.Etablissements::getEtablissement).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des etablissements", ex);
            return new ArrayList<>();
        }

    }


    @Override
    public List<ServiceType> getServices(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "services", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'services' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getServices).map(Mpe.Reponse.Services::getService).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des services", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<AgentType> getUtilisateurs(String plateformeUid, int page, int itemsPerPage, LocalDateTime lastUpdate) {
        var uri = buildUriForGet(plateformeUid, "agents", page, itemsPerPage, lastUpdate);
        try {
            LOG.info("Appel du webservice 'agents' depuis {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getAgents).map(Mpe.Reponse.Agents::getAgent).orElse(new ArrayList<>());
        } catch (RestClientException ex) {
            LOG.error("erreur lors de la récupération des agents", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public AgentSSOType getUtilisateurSSO(String plateformeUid, String sso) {
        var plateforme = getPlateforme(plateformeUid);
        var token = tokenManager.getToken(plateforme);

        LOG.info("Jeton récupéré de MPE = {}", token);

        var map = Map.of(
                "url", plateforme.getMpeUrl(),
                "apiPath", API_PATH,
                "apiVersion", API_VERSION,
                "ticket", token,
                "sso", sso
        );

        LOG.info("Préparation de l'appel de récupération des habilitations. Paramètres = {}", map);

        var expanded = new UriTemplate("{url}/{apiPath}/{apiVersion}/agents.xml/habilitations?ticket={ticket}&sso={sso}").expand(map).normalize();
        try {
            LOG.info("Appel du webservice SSO pour l'utilisateur : {}", expanded);
            var mpe = restTemplate.getForEntity(expanded, Mpe.class).getBody();
            AgentSSOType   result = Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getAgentSSO).orElse(null);
            LOG.info("Utilisateur SSO correctement récupéré = {}", result);
            return result;
        } catch (RestClientException | NullPointerException ex) {
            LOG.error("erreur lors de la récupération des référentiels", ex);
            return null;
        }


    }


    @Override
    public ReferentielsType getReferentiels(String plateformeUid) {
        var plateforme = getPlateforme(plateformeUid);
        var map = Map.of(
                "url", plateforme.getMpeUrl(),
                "apiPath", API_PATH,
                "apiVersion", API_VERSION,
                "token", tokenManager.getToken(plateforme)
        );

        var uri = new UriTemplate("{url}/{apiPath}/{apiVersion}/referentiels/procedureContrats?token={token}&format=xml").expand(map).normalize();
        try {
            LOG.info("Appel du webservice 'referentiels' depuis {}", uri);
            Mpe mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getReferentiels).orElse(null);
        } catch (RestClientException | NullPointerException ex) {
            LOG.error("erreur lors de la récupération des référentiels", ex);
            return null;
        }

    }


    @Override
    public String formatUrlCreationMarcheSubsequent( String uuid) {
        if (!StringUtils.isEmpty(marcheSubsequentPath)) {
            marcheSubsequentPath = marcheSubsequentPath.replace(CONTRAT_ID, CONTRAT_UUID);
        }
        return MessageFormat.format(marcheSubsequentPath, uuid);
    }

    @Override
    public String formatUrlEchangeChorus( String idExterne) {
        return MessageFormat.format(echangeChorusPath, idExterne == null ? "" : Base64.getEncoder().encodeToString(idExterne.getBytes()));
    }

    @Override
    public Mpe publierModification(String plateformeUid, Evenement evenement) throws MPEApiException {
        var plateforme = getPlateforme(plateformeUid);
        LOG.info("Synchronisation de l'avenat : {}", evenement.getId());
        ModificationType modification = buildModification(evenement);
        Map<String, Object> map = new HashMap<>();
        map.put("url", plateforme.getMpeUrl());
        map.put("apiPath", API_PATH);
        map.put("apiVersion", API_VERSION);
        map.put("ticket", tokenManager.getToken(plateforme));
        URI url = new UriTemplate("{url}/{apiPath}/{apiVersion}/contrats/modifications.xml?ticket={ticket}").expand(map).normalize();
        LOG.info("Appel du WS ajout de modification de contrat: {}", url);
        try {
            Mpe mpe = new Mpe();
            Mpe.Envoi envoi = new Mpe.Envoi();
            envoi.setModification(modification);
            mpe.setEnvoi(envoi);
            JAXBContext jaxbContext = JAXBContext.newInstance(Mpe.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(mpe, sw);
            LOG.info("XML envoyé:\n{}", sw);
            return restTemplate.postForEntity(url, mpe, Mpe.class).getBody();
        } catch (RestClientException | JAXBException ex) {
            throw new MPEApiException(ex);
        }
    }

    @Override
    public AgentMPE getUtilisateur(String plateformeUid, Long idAgentMpe) {
        var plateforme = getPlateforme(plateformeUid);
        Map<String, Object> map = new HashMap<>();
        map.put("url", plateforme.getMpeUrl());
        map.put("id", idAgentMpe);
        var expanded = new UriTemplate("{url}/api/v2/agents/{id}").expand(map).normalize();
        final var headers = new HttpHeaders();
        var token = tokenManager.getTokenV2(plateforme);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token.getToken());
        headers.add(HttpHeaders.COOKIE, token.getCookie());
        LOG.info("Appel du WS Agents {} avec le header {}", expanded, headers);
        return restTemplate.exchange(expanded, HttpMethod.GET, new HttpEntity<>(null, headers), AgentMPE.class).getBody();
    }

    @Override
    public Set<Long> getUtilisateurPerimetreVision(String plateformeUid, Long idAgentMpe) {
        var plateforme = getPlateforme(plateformeUid);
        Map<String, Object> map = new HashMap<>();
        map.put("url", plateforme.getMpeUrl());
        map.put("id", idAgentMpe);
        var expanded = new UriTemplate("{url}/api/v2/agents/{id}/perimeter-vision").expand(map).normalize();
        final var headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Authorization", "Bearer " + tokenManager.getTokenV2(plateforme).getToken());
        LOG.info("Appel du WS Périmètre de vision Agents {} avec le header {}", expanded, headers);
        var reponse = restTemplate.exchange(expanded, HttpMethod.GET, new HttpEntity<>(null, headers), Long[].class);
        if (reponse.getBody() == null) {
            return new HashSet<>();
        }
        return new HashSet<>(Arrays.asList(reponse.getBody()));
    }

    private ModificationType buildModification(Evenement evenement) {

        ModificationType modification = new ModificationType();
        modification.setIdContrat(Optional.ofNullable(evenement.getContrat()).map(Contrat::getIdExterne).orElse(null));
        modification.setObjetModification(evenement.getLibelle());
        modification.setIdAgent(Optional.ofNullable(evenement.getUtilisateurModification()).map(Utilisateur::getIdExterne).map(idExterne -> idExterne.substring(idExterne.indexOf("_") + 1)).orElse(null));
        BigDecimal valeurAvenant = Optional.ofNullable(evenement.getAvenantType()).map(AvenantType::getMontant).orElse(new BigDecimal(0));
        BigDecimal valeurContrat = Optional.ofNullable(evenement.getContrat()).map(Contrat::getMontant).orElse(new BigDecimal(0));
        if (TRUE.equals(evenement.getAvenantType().getIncidence())) {
            modification.setMontant(valeurAvenant.add(valeurContrat));
        }

        modification.setDateSignatureModification(toXMLGregorianCalendar(evenement.getDateDebutReel()));
        modification.setDatePublicationDonneesModification(toXMLGregorianCalendar(LocalDate.now()));
        if (TRUE.equals(evenement.getAvenantType().getDateModifiable()) && evenement.getContrat().getDateDemaragePrestation() != null && evenement.getContrat().getDateMaxFinContrat() != null) {
            long months = ChronoUnit.MONTHS.between(evenement.getContrat().getDateDemaragePrestation(), evenement.getContrat().getDateMaxFinContrat());
            if (months != 0) {
                modification.setDureeMois(BigInteger.valueOf(months));
            }
        }
        if (evenement.getContractant() != null && TRUE.equals(evenement.getAvenantType().getAvenantTransfert())) {
            Etablissement contractant = evenement.getContractant();
            ModificationType.Titulaires titulaires = new ModificationType.Titulaires();
            TitulaireType titulaireType = new TitulaireType();
            titulaireType.setId(contractant.getSiret());
            titulaireType.setDenominationSociale(Optional.ofNullable(contractant.getFournisseur()).map(Fournisseur::getRaisonSociale).orElse(null));
            titulaireType.setTypeIdentifiant(contractant.getSiret() != null ? TypeIdentifiantType.SIRET : TypeIdentifiantType.HORS_UE);
            titulaires.getTitulaire().add(titulaireType);
            modification.setTitulaires(titulaires);
        }
        return modification;
    }
}
