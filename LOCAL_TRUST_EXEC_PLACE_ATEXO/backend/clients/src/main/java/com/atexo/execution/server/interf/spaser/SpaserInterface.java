package com.atexo.execution.server.interf.spaser;

import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import com.atexo.execution.server.model.Contrat;

public interface SpaserInterface {

    String initContext(Oauth2TokenDTO oauth, UtilisateurDTO utilisateur, Contrat contrat) throws ApplicationBusinessException;
}
