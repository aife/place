package com.atexo.execution.server.api.entreprise.v3.service.Impl;

import com.atexo.execution.server.api.entreprise.v3.service.ApiEntrepriseService;
import com.atexo.execution.server.entreprise.v3.api.AttestationsSocialesEtFiscalesApi;
import com.atexo.execution.server.entreprise.v3.api.InformationsGnralesApi;
import com.atexo.execution.server.entreprise.v3.model.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApiEntrepriseServiceImpl implements ApiEntrepriseService {

    static final Logger LOG = LoggerFactory.getLogger(ApiEntrepriseServiceImpl.class);
    public static final String NO_CACHE = "no-cache";

    @Value("${baseCentrale.context:context}")
    String context;
    @Value("${baseCentrale.object:object}")
    String objet;

    private final InformationsGnralesApi informationsGnralesApi;
    private final AttestationsSocialesEtFiscalesApi attestationsSocialesEtFiscalesApi;

    @Override
    public V3InseeSireneUnitesLegalesSirenGet200ResponseData getEntreprise(String siren, String recipient) {
        try {
            return Optional.ofNullable(informationsGnralesApi.v3InseeSireneUnitesLegalesSirenGet(siren, context, recipient, objet))
                    .map(V3InseeSireneUnitesLegalesSirenGet200Response::getData)
                    .orElse(null);
        } catch (Exception e) {
            LOG.error("Exception au niveau getEntreprise V3 siren=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }
    }

    @Override
    public V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200ResponseData getExtraitKbis(String siren, String recipient) {
        try {
            return Optional.ofNullable(informationsGnralesApi.v3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet(context, recipient, objet, siren))
                    .map(V3InfogreffeRcsUnitesLegalesSirenExtraitKbisGet200Response::getData)
                    .orElse(null);
        } catch (Exception e) {
            LOG.error("Exception au niveau getExtraitKbis V3 siren=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public V3InseeSireneEtablissementsSiretGet200ResponseData getEtablissement(String siret, String recipient) {
        try {
            return Optional.ofNullable(informationsGnralesApi.v3InseeSireneEtablissementsSiretGet(siret, context, recipient, objet))
                    .map(V3InseeSireneEtablissementsSiretGet200Response::getData)
                    .orElse(null);
        } catch (Exception e) {
            LOG.error("Exception au niveau getEtablissement V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siret, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public String getAttestationCotisationsRetraite(String siret, String recipient) {
        try {
            return Optional.ofNullable(attestationsSocialesEtFiscalesApi.v3ProbtpEtablissementsSiretAttestationCotisationsRetraiteGet(siret, context, recipient, objet))
                    .map(V3CnetpUnitesLegalesSirenAttestationCotisationsCongesPayesChomageIntemperiesGet200Response::getData)
                    .map(V3CnetpUnitesLegalesSirenAttestationCotisationsCongesPayesChomageIntemperiesGet200ResponseData::getDocumentUrl)
                    .orElse("");
        } catch (Exception e) {
            LOG.error("Exception au niveau getAttestationCotisationsRetraite V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siret, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public String getAttestationFiscale(String siren, String recipient) {
        try {
            return Optional.ofNullable(attestationsSocialesEtFiscalesApi.v4DgfipUnitesLegalesSirenAttestationFiscaleGet(siren, context, recipient, objet, NO_CACHE))
                    .map(V4DgfipUnitesLegalesSirenAttestationFiscaleGet200Response::getData)
                    .map(V4DgfipUnitesLegalesSirenAttestationFiscaleGet200ResponseData::getDocumentUrl)
                    .orElse("");
        } catch (Exception e) {
            LOG.error("Exception au niveau getAttestationFiscale V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public String getAttestationVigilance(String siren, String recipient) {
        try {
            return Optional.ofNullable(attestationsSocialesEtFiscalesApi.v4UrssafUnitesLegalesSirenAttestationVigilanceGet(siren, context, recipient, objet, NO_CACHE))
                    .map(V4UrssafUnitesLegalesSirenAttestationVigilanceGet200Response::getData)
                    .map(V4UrssafUnitesLegalesSirenAttestationVigilanceGet200ResponseData::getDocumentUrl)
                    .orElse("");
        } catch (Exception e) {
            LOG.error("Exception au niveau getAttestationVigilance V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public String getAttestationCotisationsCongesPayesChomageIntemperies(String siren, String recipient) {
        try {
            return Optional.ofNullable(attestationsSocialesEtFiscalesApi.v3CnetpUnitesLegalesSirenAttestationCotisationsCongesPayesChomageIntemperiesGet(context, recipient, objet, siren))
                    .map(V3CnetpUnitesLegalesSirenAttestationCotisationsCongesPayesChomageIntemperiesGet200Response::getData)
                    .map(V3CnetpUnitesLegalesSirenAttestationCotisationsCongesPayesChomageIntemperiesGet200ResponseData::getDocumentUrl)
                    .orElse("");
        } catch (Exception e) {
            LOG.error("Exception au niveau getAttestationCotisationsCongesPayesChomageIntemperies V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }

    }

    @Override
    public String getCarteProfessionnelleTravauxPublics(String siren, String recipient) {
        try {
            return Optional.ofNullable(attestationsSocialesEtFiscalesApi.v3FntpUnitesLegalesSirenCarteProfessionnelleTravauxPublicsGet(context, recipient, objet, siren))
                    .map(V3FntpUnitesLegalesSirenCarteProfessionnelleTravauxPublicsGet200Response::getData)
                    .map(V3FntpUnitesLegalesSirenCarteProfessionnelleTravauxPublicsGet200ResponseData::getDocumentUrl)
                    .orElse("");
        } catch (Exception e) {
            LOG.error("Exception au niveau getCarteProfessionnelleTravauxPublics V3 siret=[{}]  context=[{}]  recipient=[{}]  objet=[{}] message=[{}]", siren, context, recipient, objet, e.getMessage());
            return null;
        }
    }
}
