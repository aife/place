package com.atexo.execution.server.interf.mpe.api.v2;

import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.common.mpe.ws.api.Mpe;
import com.atexo.execution.server.interf.mpe.api.BaseRestClient;
import com.atexo.execution.server.interf.mpe.api.TokenManager;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("RestChorusClientV2")
@Profile("!mock")
public class RestChorusClient extends BaseRestClient implements ChorusClient {

    static final Logger LOG = LoggerFactory.getLogger(RestChorusClient.class);

    public RestChorusClient(TokenManager tokenManager, PlateformeRepository plateformeRepository) {
        super(tokenManager, plateformeRepository);
    }

    @Override
    public List<EchangeChorusType> getEchangesChorus(String plateformeUid, int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        var uri = buildUriForGet(plateformeUid, "echanges-chorus", page, nbItems, lastSynchronizationDate);
        LOG.info("Appel du WS Echanges CHORUS: {}", uri);
        var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
        return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getEchangesChorus).map(Mpe.Reponse.EchangesChorus::getEchangeChorus).orElse(new ArrayList<>());
    }
}
