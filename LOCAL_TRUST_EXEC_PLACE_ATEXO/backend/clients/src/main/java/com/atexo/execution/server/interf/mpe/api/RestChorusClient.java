package com.atexo.execution.server.interf.mpe.api;

import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.common.mpe.ws.api.ModificationType;
import com.atexo.execution.common.mpe.ws.api.Mpe;
import com.atexo.execution.server.common.mapper.ActeMapper;
import com.atexo.execution.server.mapper.EchangeChorusMapper;
import com.atexo.execution.server.model.Acte;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ParametrageApplicationRepository;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriTemplate;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@Profile("!mock")
public class RestChorusClient extends BaseRestClient implements ChorusClient {

    static final Logger LOG = LoggerFactory.getLogger(RestChorusClient.class);
    private static final String API_PATH = "/app.php/api/";
    private static final String API_VERSION = "v1";
    private static final String API_VERSION_V2 = "v2";

    private final ActeRepository acteRepository;

    private final EchangeChorusMapper echangeChorusMapper;

    private final ActeMapper acteMapper;

    private final TokenManager tokenManager;

    private Marshaller jaxbMarshaller;

    public RestChorusClient(ActeRepository acteRepository, ParametrageApplicationRepository parametrageRepository, EchangeChorusMapper echangeChorusMapper, ActeMapper acteMapper, TokenManager tokenManager, PlateformeRepository plateformeRepository) {
        super(tokenManager, plateformeRepository);
        this.acteRepository = acteRepository;
        this.echangeChorusMapper = echangeChorusMapper;
        this.acteMapper = acteMapper;
        this.tokenManager = tokenManager;
    }


    @Override
    public List<EchangeChorusType> getEchangesChorus(String plateformeUid, int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        var uri = buildUriForGet(plateformeUid, "echanges-chorus", page, nbItems, lastSynchronizationDate);
        try {
            LOG.info("Appel du WS Echanges CHORUS: {}", uri);
            var mpe = restTemplate.getForEntity(uri, Mpe.class).getBody();
            return Optional.ofNullable(mpe).map(Mpe::getReponse).map(Mpe.Reponse::getEchangesChorus).map(Mpe.Reponse.EchangesChorus::getEchangeChorus).orElse(new ArrayList<>());
        } catch (Exception ex) {
            LOG.error("erreur lors de la récupération des échanges chorus", ex);
            return new ArrayList<>();
        }
    }

    private ModificationType buildModification(ActeModificatif acte) {
        var modification = new ModificationType();
        modification.setIdContrat(Optional.ofNullable(acte.getContrat()).map(Contrat::getIdExterne).orElse(null));
        modification.setObjetModification(acte.getObjet());
        modification.setIdAgent(Optional.ofNullable(acte.getUtilisateurModification()).map(Utilisateur::getIdExterne).map(idExterne -> idExterne.substring(idExterne.indexOf("_") + 1)).orElse(null));
        var avecIncidenceFinanciere = ActeModificatif.AVEC_INCIDENCE.equalsIgnoreCase(acte.getTypeModification());
        LOG.info("Acte avec incidence financière {}", avecIncidenceFinanciere);
        modification.setDateSignatureModification(toXMLGregorianCalendar(acte.getDateNotification().toLocalDate()));
        if (avecIncidenceFinanciere) {
            var montant = Optional.ofNullable(acte.getContrat().getMontant()).map(BigDecimal::doubleValue).orElse(0d) + acte.getMontantHTChiffre();
            var valeurActe = Optional.ofNullable(montant).map(BigDecimal::new).orElse(new BigDecimal(0));
            modification.setMontant(valeurActe);
        } else {
            if (acte.getContrat().getDateDebutExecution() != null && acte.getNouvelleFinContrat() != null) {
                long months = ChronoUnit.MONTHS.between(acte.getContrat().getDateDebutExecution(), acte.getNouvelleFinContrat());
                if (months != 0) {
                    modification.setDureeMois(BigInteger.valueOf(months));
                }
            }
        }
        modification.setDatePublicationDonneesModification(toXMLGregorianCalendar(LocalDate.now()));
        return modification;
    }

    @Override
    public File telechargeZipChorus(String plateformUid, String idExterne) throws MPEApiException {
        var plateforme = getPlateforme(plateformUid);
        var parameters = Map.of(
                "url", plateforme.getMpeUrl(),
		"apiPath", API_PATH,
		"apiVersion", API_VERSION,
                "ticket", tokenManager.getToken(plateforme),
		"idExterne", idExterne
        );

	    byte[] content = null;

        var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/echanges-chorus/download-zip/{idExterne}?ticket={ticket}").expand(parameters).normalize();

		LOG.info("Appel du WS téléchargement zip CHORUS: {}", url);

		try {
            var response = restTemplate.getForEntity(url, byte[].class);


			switch ( response.getStatusCode() ) {
				case OK:
					if (null==response.getBody()) {
						LOG.error("HTTP 200 mais réponse vide. Une erreur s'est produite coté MPE");
					} else {
						LOG.info("Zip CHORUS recu en retour de MPE. Tout est ok");

						content = response.getBody();
					}
					break;
				case NO_CONTENT:
					LOG.warn("HTTP 204 et aucun zip en retour de MPE, le zip est vide cote MPE (ni document, ni fiche). On le construit");
					break;
				default:
					LOG.error("Aucun zip en retour de MPE et status HTTP 204, le zip est inexistant cote MPE. On le construit");

			}
        } catch (RestClientException e) {
	        LOG.error("Erreur lors de l'appel MPE à l'URL '{}'", url, e);
	        throw new MPEApiException(e);
        }

	    try {
		    var file = Files.createTempFile("echange-chorus"+idExterne, ".zip");

			if (null!=content) {
				Files.write(file, content);
			}

            return file.toFile();
		}
		catch (IOException ex) {
			throw new MPEApiException(ex);
		}
    }

    @Override
    public EchangeChorusType publierEchangeChorus(String plateformeUid, Acte acte, EchangeChorus echange) throws MPEApiException {
        var plateforme = getPlateforme(plateformeUid);
        var parameters = Map.of(
                "url", plateforme.getMpeUrl(),
		    "apiPath", API_PATH,
		    "apiVersion", API_VERSION,
                "ticket", tokenManager.getToken(plateforme)
	    );

        var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/echanges-chorus.xml?ticket={ticket}").expand(parameters).normalize();
        LOG.info("Appel du WS publication CHORUS: {}", url);

        var echangeEnvoi = echangeChorusMapper.toWS(acte, echange);
	    echangeEnvoi.setActe(acteMapper.toWS(acte, echange));

        var mpe = new Mpe();
        var envoi = new Mpe.Envoi();
        envoi.setEchangeChorus(echangeEnvoi);
        mpe.setEnvoi(envoi);
        try {
	        var sw = new StringWriter();
            jaxbMarshaller.marshal(mpe, sw);
            LOG.info("XML envoyé:\n{}", sw);

	        var body = restTemplate.postForEntity(url, mpe, Mpe.class).getBody();

	        LOG.info("Retour du XML = {}", body);

	        return body.getReponse().getEchangeChorus();
        } catch (RestClientException | JAXBException ex) {
            throw new MPEApiException(ex);
        }
    }

	@Override
	public EchangeChorusType republierEchangeChorus(String plateformeUid, Acte acte, EchangeChorus echange) throws MPEApiException {
		var plateforme = getPlateforme(plateformeUid);
		var parameters = Map.of(
				"url", plateforme.getMpeUrl(),
				"apiPath", API_PATH, "apiVersion", API_VERSION,
				"ticket", tokenManager.getToken(plateforme)
		);

		var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/echanges-chorus.xml?ticket={ticket}").expand(parameters).normalize();
		LOG.info("Appel du WS publication CHORUS: {}", url);

		var echangeEnvoi = echangeChorusMapper.toWS(acte, echange);
		echangeEnvoi.setActe(acteMapper.toWS(acte, echange));

		var mpe = new Mpe();
		var envoi = new Mpe.Envoi();
		envoi.setEchangeChorus(echangeEnvoi);
		mpe.setEnvoi(envoi);
		try {
			var sw = new StringWriter();
			jaxbMarshaller.marshal(mpe, sw);
			LOG.info("XML envoyé:\n{}", sw);

            var body = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(mpe), Mpe.class).getBody();
            if (body == null) {
                throw new MPEApiException("Erreur lors de la publication de l'échange chorus");
            }

			LOG.info("Retour du XML = {}", body);

			return body.getReponse().getEchangeChorus();
		} catch (RestClientException | JAXBException ex) {
			throw new MPEApiException(ex);
		}
	}

	@Override
    public void envoyerZipChorus(String plateformeUid, final EchangeChorus echange, final File zipFile) throws MPEApiException {
        var plateforme = getPlateforme(plateformeUid);
        var parameters = Map.of(
                "url", plateforme.getMpeUrl(),
		    "apiPath", API_PATH,
		    "apiVersion", API_VERSION,
                "ticket", tokenManager.getToken(plateforme),
		    "idEchange", echange.getIdExterne()
	    );

        var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/echanges-chorus/upload/{idEchange}?ticket={ticket}").expand(parameters).normalize();
        LOG.info("Appel du WS publication CHORUS upload des documents: {}", url);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var fileMap = new LinkedMultiValueMap<String, String>();
        var contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(zipFile.getName())
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        try {
            var fileEntity = new HttpEntity<>(Files.readAllBytes(zipFile.toPath()), fileMap);
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", fileEntity);
            var requestEntity = new HttpEntity<>(body, headers);
            restTemplate.postForEntity(url, requestEntity, Mpe.class);
        } catch (RestClientException | IOException e) {
            throw new MPEApiException(e);
        }
    }

	@Override
	public void renvoyerZipChorus(String plateformeUid, EchangeChorus echange, File zipFile) throws MPEApiException {
		var plateforme = getPlateforme(plateformeUid);
		var parameters = Map.of(
				"url", plateforme.getMpeUrl(),
				"apiPath", API_PATH,
				"apiVersion", API_VERSION,
				"ticket", tokenManager.getToken(plateforme),
				"idEchange", echange.getIdExterne()
		);

		var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/echanges-chorus/upload/{idEchange}?ticket={ticket}").expand(parameters).normalize();
		LOG.info("Appel du WS publication CHORUS upload des documents: {}", url);
		var headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		var fileMap = new LinkedMultiValueMap<String, String>();
		var contentDisposition = ContentDisposition
				.builder("form-data")
				.name("file")
				.filename(zipFile.getName())
				.build();
		fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
		try {
			var fileEntity = new HttpEntity<>(Files.readAllBytes(zipFile.toPath()), fileMap);
			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("file", fileEntity);
			var requestEntity = new HttpEntity<>(body, headers);
			restTemplate.postForEntity(url, requestEntity, Mpe.class);
		} catch (RestClientException | IOException e) {
			throw new MPEApiException(e);
		}
	}

    @Override
    public void publierModification(final ActeModificatif acte) throws MPEApiException {
        LOG.info("Synchronisation de l'avenat : {}", acte.getId());
        var plateforme = acte.getContrat().getPlateforme();
        var modification = buildModification(acte);
        var parameters = Map.of(
                "url", plateforme.getMpeUrl(),
                "apiPath", API_PATH,
                "apiVersion", API_VERSION,
                "ticket", tokenManager.getToken(plateforme)
        );

        var url = new UriTemplate("{url}/{apiPath}/{apiVersion}/contrats/modifications.xml?ticket={ticket}").expand(parameters).normalize();
        LOG.info("Appel du WS ajout de modification de contrat: {}", url);
        try {
            var mpe = new Mpe();
            var envoi = new Mpe.Envoi();
            envoi.setModification(modification);
            mpe.setEnvoi(envoi);
            var sw = new StringWriter();
            jaxbMarshaller.marshal(mpe, sw);
            LOG.info("XML envoyé:\n{}", sw);
            var reponse = restTemplate.postForEntity(url, mpe, Mpe.class).getBody();
            if (reponse != null && reponse.getReponse() != null && reponse.getReponse().getModificationContrat() != null) {
                var modificationMpe = reponse.getReponse().getModificationContrat();
                var idExterne = modificationMpe.getId();
                acte.setIdExterne(idExterne);
                acte.setNumeroOrdre(modificationMpe.getNumOrdre());
            }
            acte.setStatutPublication(StatutAvenant.PUBLIE);
            acte.setDatePublication(LocalDateTime.now());
            acteRepository.save(acte);
        } catch (Exception ex) {
            throw new MPEApiException(ex);
        }
    }


	@PostConstruct
	private void init() throws JAXBException {
		var jaxbContext = JAXBContext.newInstance(Mpe.class);
		this.jaxbMarshaller = jaxbContext.createMarshaller();
	}
}
