package com.atexo.execution.server.clients.oauth2;

import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.stream.Collectors;

@Component
@Slf4j
public class Oauth2Client {

    @Value("${messagerie.grant-type:password}")
    private String grantType;
    private final RestTemplate restTemplate = new RestTemplate();

    public Oauth2TokenDTO getOauth2TokenDTO(String keycloakUrl, String realm, String clientId, String login, String password) {
        var oauthUrl = MessageFormat.format("{0}/realms/{1}/protocol/openid-connect/token", keycloakUrl, realm);
        var paramsMap = new HashMap<String, String>();
        paramsMap.put("grant_type", grantType);
        paramsMap.put("client_id", clientId);
        paramsMap.put("username", login);
        paramsMap.put("password", password);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        var params = paramsMap.keySet().stream()
                .map(key -> key + "=" + URLEncoder.encode(paramsMap.get(key), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));
        var request = new HttpEntity<>(params, headers);
        log.info("appel sécurisé, url oauth => {}", oauthUrl);
        try {
            return restTemplate.postForEntity(oauthUrl, request, Oauth2TokenDTO.class).getBody();
        } catch (Exception e) {
            log.error("Erreur lors de l'authentification {}", e.getMessage());
            return null;
        }
    }
}
