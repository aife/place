package com.atexo.execution.server.clients.oauth2;

import com.atexo.execution.common.dto.docgen.DocumentModeleDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class DocumentModeleClient {
    private final RestTemplate restTemplate = new RestTemplate();
    private final static List<String> VALID_EXTENSIONS = List.of("pptx", "xlsx", "docx");


    public List<DocumentModeleDTO> getDocumentsModele(String url, String accessToken) {
        var headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);
        var entity = new HttpEntity<>(headers);
        try {
            log.info("récupération des documents modèles depuis {}", url);
            return restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<DocumentModeleDTO>>() {
            }).getBody();
        } catch (Exception e) {
            log.error("Erreur lors de la récupération des documents modèles {}", e.getMessage());
            return new ArrayList<>();
        }
    }

    public File getDocumentModele(String url, String accessToken, DocumentModeleDTO modele) {
        log.info("téléchargement du fichier modèle {} depuis {}", modele, url);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        headers.setBearerAuth(accessToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        var response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
        try {
            log.info("récupération du fichier modèle depuis {}", url);
            var tmpFile = Files.createTempFile("modele-", "." + modele.getExtension());
            Files.write(tmpFile, response.getBody());
            return tmpFile.toFile();
        } catch (IOException e) {
            log.error("Erreur lors de la récupération du fichier modèle {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public File convertToDocx(byte[] pdfData, String url, String extension) {
        if (pdfData == null || pdfData.length == 0) {
            throw new IllegalArgumentException("Fichier PDF non fourni");
        }

        if (!VALID_EXTENSIONS.contains(extension)) {
            throw new IllegalArgumentException("Les extensions éligibles pour l'édition sont pptx, xlsx, docx");
        }

        MultiValueMap<String, Resource> bodyMap = new LinkedMultiValueMap<>();
        try {
            File tmp = File.createTempFile("edit-", ".pdf");
            FileOutputStream fos = new FileOutputStream(tmp);
            fos.write(pdfData);
            fos.close();

            bodyMap.add("document", new FileSystemResource(tmp));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<MultiValueMap<String, Resource>> requestEntity = new HttpEntity<>(bodyMap, headers);
            log.info("Conversion pdf vers docx");
            var result = restTemplate.exchange(
                    url.replace("{outputFormat}", "docx"), HttpMethod.POST, requestEntity, byte[].class);
            var docFile = Files.createTempFile("edit-", ".docx");
            Files.write(docFile, result.getBody());
            return docFile.toFile();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
