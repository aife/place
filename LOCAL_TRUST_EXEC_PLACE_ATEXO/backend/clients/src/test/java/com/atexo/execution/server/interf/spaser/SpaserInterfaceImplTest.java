package com.atexo.execution.server.interf.spaser;

import com.atexo.execution.common.dto.OrganismeDTO;
import com.atexo.execution.common.dto.ServiceDTO;
import com.atexo.execution.common.dto.UtilisateurDTO;
import com.atexo.execution.common.dto.oauth.Oauth2TokenDTO;
import com.atexo.execution.common.dto.spaser.SpaserInit;
import com.atexo.execution.common.dto.spaser.SpaserReponse;
import com.atexo.execution.common.exception.ApplicationBusinessException;
import com.atexo.execution.server.clients.oauth2.Oauth2Client;
import com.atexo.execution.server.model.Contrat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SpaserInterfaceImplTest {

    @InjectMocks
    SpaserInterfaceImpl spaserInterface;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void before() throws Exception {
        ReflectionTestUtils.setField(spaserInterface, "spaserURIAsString", "https://spaser-develop.local-trust.com");
        ReflectionTestUtils.setField(spaserInterface, "apiInitContextAsString", "/api/authorization-contexts/init");
        ReflectionTestUtils.setField(spaserInterface, "restTemplate", restTemplate);
    }

    @Test
    void Given_ContratAndUtilisateur_thenReturn_HttpReponse(){


        Oauth2TokenDTO oauth = Oauth2TokenDTO.builder().accesToken("accessToken").refreshToken("refreshToken").build();
        UtilisateurDTO utilisateur = new UtilisateurDTO();
        utilisateur.setPlateformeUid("plateformeUid");
        ServiceDTO service = new ServiceDTO();
        OrganismeDTO organismeDTO = new OrganismeDTO();
        organismeDTO.setAcronyme("acronyme");
        service.setOrganisme(organismeDTO);
        Contrat contrat = new Contrat();
        contrat.setId(123L);
        contrat.setDateFinContrat(LocalDate.now());
        utilisateur.setService(service);

        when(restTemplate.postForEntity(anyString(), any(SpaserInit.class), any()))
                .thenReturn(ResponseEntity.internalServerError().build());
        Exception ex = assertThrows(ApplicationBusinessException.class, () -> {
            spaserInterface.initContext(oauth,utilisateur,contrat);
        });

        assertNotNull(ex);
    }
}
