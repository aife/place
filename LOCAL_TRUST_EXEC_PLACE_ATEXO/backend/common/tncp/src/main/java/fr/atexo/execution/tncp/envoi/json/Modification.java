
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class Modification {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("dureeMois")
    private Integer dureeMois;
    @JsonProperty("montant")
    private Integer montant;
    @JsonProperty("titulaires")
    private List<Titulaire> titulaires;
    @JsonProperty("dateNotificationModification")
    private String dateNotificationModification;
    @JsonProperty("datePublicationDonneesModification")
    private String datePublicationDonneesModification;
}
