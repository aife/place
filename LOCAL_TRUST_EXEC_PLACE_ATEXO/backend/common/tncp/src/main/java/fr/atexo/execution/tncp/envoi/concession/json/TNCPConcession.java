
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class TNCPConcession {

    @JsonProperty("operation")
    public String operation;
    @JsonProperty("rsDemandeur")
    public String rsDemandeur;
    @JsonProperty("idDemandeur")
    public String idDemandeur;
    @JsonProperty("plateforme")
    public Plateforme plateforme;
    @JsonProperty("donneeEssentielle")
    public DonneeEssentielle donneeEssentielle;

}
