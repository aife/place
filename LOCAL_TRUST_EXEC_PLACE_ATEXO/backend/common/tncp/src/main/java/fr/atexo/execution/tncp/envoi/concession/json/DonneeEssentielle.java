
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DonneeEssentielle {

    @JsonProperty("statutDE")
    public String statutDE;
    @JsonProperty("id")
    public String id;
    @JsonProperty("nature")
    public String nature;
    @JsonProperty("objet")
    public String objet;
    @JsonProperty("procedure")
    public String procedure;
    @JsonProperty("dureeMois")
    public Integer dureeMois;
    @JsonProperty("considerationsSociales")
    public String considerationsSociales;
    @JsonProperty("considerationsEnvironnementales")
    public String considerationsEnvironnementales;
    @JsonProperty("datePublicationDonnees")
    public String datePublicationDonnees;
    @JsonProperty("donneesCC")
    public DonneesCC donneesCC;
    @JsonProperty("modifications")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Modification> modifications;

}
