
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class ModificationsActesSousTraitance {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("dureeMois")
    private Integer dureeMois;
    @JsonProperty("montant")
    private Double montant;
    @JsonProperty("dateNotification")
    private String dateNotification;
    @JsonProperty("datePublicationDonnees")
    private String datePublicationDonnees;
}
