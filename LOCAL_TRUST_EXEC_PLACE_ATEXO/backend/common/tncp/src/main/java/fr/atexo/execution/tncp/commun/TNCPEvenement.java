package fr.atexo.execution.tncp.commun;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TNCPEvenement implements Serializable {
    private String uuid;
    private InterfaceEnum flux;
    private TypeEnum type;
    private EtapeEnum etape;
    private String idObjetSource;
    private String idObjetDestination;
    private String uuidPlateforme;
    private String message;
    private String messageDetails;
    private ZonedDateTime dateEnvoi;
    private ZonedDateTime dateReception;
}
