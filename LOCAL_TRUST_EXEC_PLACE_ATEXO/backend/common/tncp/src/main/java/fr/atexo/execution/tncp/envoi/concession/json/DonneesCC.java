
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DonneesCC {

    @JsonProperty("idAutoriteConcedante")
    public String idAutoriteConcedante;
    @JsonProperty("dateDebutExecution")
    public String dateDebutExecution;
    @JsonProperty("dateSignature")
    public String dateSignature;
    @JsonProperty("valeurGlobale")
    public Double valeurGlobale;
    @JsonProperty("montantSubventionPublique")
    public Double montantSubventionPublique;
    @JsonProperty("concessionnaires")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Concessionnaire> concessionnaires;
    @JsonProperty("donneesExecution")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<DonneesExecution> donneesExecution;

}
