
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class ActesSousTraitance {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("sousTraitant")
    private SousTraitant sousTraitant;
    @JsonProperty("dureeMois")
    private Integer dureeMois;
    @JsonProperty("dateNotification")
    private String dateNotification;
    @JsonProperty("montant")
    private Double montant;
    @JsonProperty("variationPrix")
    private String variationPrix;
    @JsonProperty("datePublicationDonnees")
    private String datePublicationDonnees;
}
