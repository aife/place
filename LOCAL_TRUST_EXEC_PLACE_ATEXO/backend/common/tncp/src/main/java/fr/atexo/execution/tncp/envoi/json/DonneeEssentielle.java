
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class DonneeEssentielle {

    @JsonProperty("statutDE")
    private String statutDE;
    @JsonProperty("id")
    private String id;
    @JsonProperty("nature")
    private String nature;
    @JsonProperty("objet")
    private String objet;
    @JsonProperty("procedure")
    private String procedure;
    @JsonProperty("dureeMois")
    private Integer dureeMois;
    @JsonProperty("considerationsSociales")
    private String considerationsSociales;
    @JsonProperty("considerationsEnvironnementales")
    private String considerationsEnvironnementales;
    @JsonProperty("datePublicationDonnees")
    private String datePublicationDonnees;
    @JsonProperty("donneesMP")
    private DonneesMP donneesMP;
    @JsonProperty("modifications")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Modification> modifications;
}
