
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DonneesExecution {

    @JsonProperty("depensesInvestissement")
    public Double depensesInvestissement;
    @JsonProperty("tarifs")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Tarif> tarifs;
    @JsonProperty("datePublicationDonneesExecution")
    public String datePublicationDonneesExecution;

}
