
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Plateforme {

    @JsonProperty("idPlateforme")
    public String idPlateforme;
    @JsonProperty("idTechniquePlateforme")
    public String idTechniquePlateforme;

}
