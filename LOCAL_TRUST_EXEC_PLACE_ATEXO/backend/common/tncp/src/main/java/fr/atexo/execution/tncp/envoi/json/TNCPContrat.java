
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class TNCPContrat {

    @JsonProperty("operation")
    private String operation;
    @JsonProperty("rsDemandeur")
    private String rsDemandeur;
    @JsonProperty("idDemandeur")
    private String idDemandeur;
    @JsonProperty("plateforme")
    private Plateforme plateforme;
    @JsonProperty("donneeEssentielle")
    private DonneeEssentielle donneeEssentielle;

}
