
package fr.atexo.execution.tncp.envoi.concession.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Modification {

    @JsonProperty("id")
    public Integer id;
    @JsonProperty("dureeMois")
    public Integer dureeMois;
    @JsonProperty("valeurGlobale")
    public Integer valeurGlobale;
    @JsonProperty("dateSignatureModification")
    public String dateSignatureModification;
    @JsonProperty("datePublicationDonneesModification")
    public String datePublicationDonneesModification;

}
