package fr.atexo.execution.tncp.envoi;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.atexo.execution.tncp.commun.TNCPEvenement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TNCPEnvoi extends TNCPEvenement {

    private String payload;
}
