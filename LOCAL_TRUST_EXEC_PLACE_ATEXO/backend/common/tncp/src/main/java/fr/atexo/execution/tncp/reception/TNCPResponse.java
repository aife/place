package fr.atexo.execution.tncp.reception;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.atexo.execution.tncp.commun.TNCPEvenement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TNCPResponse extends TNCPEvenement {

    private String statut;
    private String body;
    private String traitementPlateforme;

}
