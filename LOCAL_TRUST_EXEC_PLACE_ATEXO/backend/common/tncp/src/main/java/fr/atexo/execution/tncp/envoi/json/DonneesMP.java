
package fr.atexo.execution.tncp.envoi.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class DonneesMP {

    @JsonProperty("idAcheteur")
    private String idAcheteur;
    @JsonProperty("idAutoriteConcedante")
    private String idAutoriteConcedante;
    @JsonProperty("technique")
    private String technique;
    @JsonProperty("modaliteExecution")
    private String modaliteExecution;
    @JsonProperty("idAccordCadre")
    private String idAccordCadre;
    @JsonProperty("codeCPV")
    private String codeCPV;
    @JsonProperty("lieuExecutionCode")
    private String lieuExecutionCode;
    @JsonProperty("lieuExecutionTypeCode")
    private String lieuExecutionTypeCode;
    @JsonProperty("dateNotification")
    private String dateNotification;
    @JsonProperty("marcheInnovant")
    private Boolean marcheInnovant;
    @JsonProperty("origineUE")
    private Double origineUE;
    @JsonProperty("origineFrance")
    private Double origineFrance;
    @JsonProperty("ccag")
    private String ccag;
    @JsonProperty("offresRecues")
    private Integer offresRecues;
    @JsonProperty("montant")
    private Double montant;
    @JsonProperty("formePrix")
    private String formePrix;
    @JsonProperty("typePrix")
    private String typePrix;
    @JsonProperty("attributionAvance")
    private Boolean attributionAvance;
    @JsonProperty("tauxAvance")
    private Double tauxAvance;
    @JsonProperty("titulaires")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Titulaire> titulaires;
    @JsonProperty("typeGroupementOperateurs")
    private String typeGroupementOperateurs;
    @JsonProperty("sousTraitanceDeclaree")
    private Boolean sousTraitanceDeclaree;
    @JsonProperty("actesSousTraitance")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ActesSousTraitance> actesSousTraitance;
    @JsonProperty("modificationsActesSousTraitance")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ModificationsActesSousTraitance> modificationsActesSousTraitance;
}
