package com.atexo.execution.common.dto;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ContratDTOTest {

    @Test
    void given_montant_avenant_montant_asset_augmentation() {
        var contratDTO = new ContratDTO();
        contratDTO.setMontant(BigDecimal.valueOf(100));
        contratDTO.setMontantTotalAvenants(BigDecimal.valueOf(50));
        assertEquals(50, contratDTO.getAugmentation());
    }

    @Test
    void given_montant_ttc() {
        var contratDTO = new ContratDTO();
        contratDTO.setMontant(BigDecimal.valueOf(100));
        assertEquals(new BigDecimal("120.00"), contratDTO.getMontantTTC());
    }
}
