package com.atexo.execution.common.dto.actes;

import com.atexo.execution.common.dto.ActeDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
public class ActeModificatifDTO extends ActeDTO {

	private Long dureeAjoureeContrat;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate nouvelleFinContrat;
	private Boolean publicationDonneesEssentielles;
	private Boolean clauseReexamen;
	private String typeModification;
	private Double montantHTChiffre;
	private Double montantTTCChiffre;
	private Double tauxTVA;
}

