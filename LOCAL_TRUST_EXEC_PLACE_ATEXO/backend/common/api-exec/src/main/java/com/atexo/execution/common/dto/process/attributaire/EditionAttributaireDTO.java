package com.atexo.execution.common.dto.process.attributaire;

import com.atexo.execution.common.dto.EtablissementDTO;
import com.atexo.execution.common.dto.ValueLabelDTO;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EditionAttributaireDTO {

	EtablissementDTO etablissement;
	Long commanditaireId;
	Date dateNotification;
	BigDecimal montant;
	ValueLabelDTO categorieConsultation;
	private Integer dureeMois;
	private ValueLabelDTO revisionPrix;
	private Long idContratEtablissement;

}
