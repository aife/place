package com.atexo.execution.common.validation.impl;

import com.atexo.execution.common.validation.Siren;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SirenValidator implements ConstraintValidator<Siren, String> {
    @Override
    public void initialize(Siren constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String siren, ConstraintValidatorContext constraintValidatorContext) {
        int total = 0;
        int digit = 0;
        for (int i = 0; i < siren.length(); i++) {
            if ((i % 2) == 1) {
                digit = Integer.parseInt(String.valueOf(siren.charAt(i))) * 2;
                if (digit > 9) digit -= 9;
            } else digit = Integer.parseInt(String.valueOf(siren.charAt(i)));
            total += digit;
        }
        if ((total % 10) == 0) return true;
        else return false;
    }
}
