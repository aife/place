package com.atexo.execution.common.dto.imports;


import java.util.Date;

public class ContractantDTOImport {
    private String contratReferenceLibre;

    private String etablissementSiret;

    private String type;

    private String categorieConsultation;

    private String role;

    private Double montant;

    private Date dateNotification;

    private String commanditaireSiret;

    private String contratAttributaireEtablissementSiret;

	// Utilisé pour stocker l'id du contrat pour être réutilisé dans l'import (entre le moment où on vérifie la validité
	// et où on sauvegarde en base
	private Long organismeId;

    // Utilisé pour stocker l'id du contrat pour être réutilisé dans l'import (entre le moment où on vérifie la validité
    // et où on sauvegarde en base
    private Long contratId;

    public String getContratReferenceLibre() {
        return contratReferenceLibre;
    }

    public void setContratReferenceLibre(String referenceLibre) {
        this.contratReferenceLibre = referenceLibre;
    }

    public String getEtablissementSiret() {
        return etablissementSiret;
    }

    public void setEtablissementSiret(String etablissementSiret) {
        this.etablissementSiret = etablissementSiret;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategorieConsultation() {
        return categorieConsultation;
    }

    public void setCategorieConsultation(String categorieConsultation) {
        this.categorieConsultation = categorieConsultation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Date getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(Date dateNotification) {
        this.dateNotification = dateNotification;
    }

    public String getCommanditaireSiret() {
        return commanditaireSiret;
    }

    public void setCommanditaireSiret(String commanditaireSiret) {
        this.commanditaireSiret = commanditaireSiret;
    }

    public String getContratAttributaireEtablissementSiret() {
        return contratAttributaireEtablissementSiret;
    }

    public void setContratAttributaireEtablissementSiret(String contratAttributaireEtablissementSiret) {
        this.contratAttributaireEtablissementSiret = contratAttributaireEtablissementSiret;
    }

    public Long getContratId() {
        return contratId;
    }

    public void setContratId(Long contratId) {
        this.contratId = contratId;
    }

	public Long getOrganismeId() {
		return organismeId;
	}
}
