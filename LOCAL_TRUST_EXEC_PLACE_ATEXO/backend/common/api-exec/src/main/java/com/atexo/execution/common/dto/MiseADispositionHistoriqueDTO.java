package com.atexo.execution.common.dto;

import java.time.LocalDateTime;

public class MiseADispositionHistoriqueDTO {
    private Long idMiseadisposition;

    private LocalDateTime dateDemande;

    private String typeDemandeMAD;

    private String requette;

    private String reponse;

    private Boolean statut;

    private LocalDateTime dateFin;

    public Long getIdMiseadisposition() {
        return idMiseadisposition;
    }

    public void setIdMiseadisposition(Long idMiseadisposition) {
        this.idMiseadisposition = idMiseadisposition;
    }

    public LocalDateTime getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    public String getTypeDemandeMAD() {
        return typeDemandeMAD;
    }

    public void setTypeDemandeMAD(String typeDemandeMAD) {
        this.typeDemandeMAD = typeDemandeMAD;
    }

    public String getRequette() {
        return requette;
    }

    public void setRequette(String requette) {
        this.requette = requette;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public LocalDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDateTime dateFin) {
        this.dateFin = dateFin;
    }
}
