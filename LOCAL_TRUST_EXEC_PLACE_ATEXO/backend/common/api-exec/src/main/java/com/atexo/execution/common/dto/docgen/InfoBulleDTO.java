package com.atexo.execution.common.dto.docgen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class InfoBulleDTO {
    private String description;
    private LienTypeDTO lien;
    private Boolean actif;
}
