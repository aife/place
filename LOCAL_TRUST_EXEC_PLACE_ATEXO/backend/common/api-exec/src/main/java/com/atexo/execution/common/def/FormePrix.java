package com.atexo.execution.common.def;

import java.util.Arrays;

public enum FormePrix {
    UNITAIRE, FORFAITAIRE, MIXTE;

    public static FormePrix from(String value) {
        if (value == null) {
            return null;
        }
        return Arrays.stream(FormePrix.values()).filter(formePrix -> formePrix.name().equalsIgnoreCase(value)).findFirst().orElse(null);
    }
}
