package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AdresseDTO implements Serializable {

	String adresse;
	String adresse2;
	String codePostal;
	String commune;
    String pays;
	String codeInseeLocalite;
}
