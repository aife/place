package com.atexo.execution.common.def;

public class ReferentielDiscriminators {

    public static final String CATEGORIE_CONSULTATION = "CategorieConsultation";
	public static final String TYPE_ACTE = "TypeActe";
	public static final String CODE_EXTERNE_ACTE = "CodeExterneActe";
    public static final String TYPE_CONTRAT = "TypeContrat";
    public static final String TYPE_EVENEMENT = "TypeEvenement";
    public static final String FORME_JURIDIQUE = "FormeJuridique";
    public static final String CODE_APE_NAF = "CodeApeNaf";
    public static final String PAYS = "Pays";
    public static final String CATEGORIE_TYPE_EVENEMENT = "CategorieTypeEvenement";
    public static final String TYPE_AVENANT = "TypeAvenant";
    public static final String TAUX_TVA = "TauxTVA";
    public static final String CODE_ACHAT = "CodeAchat";
    public static final String UF_OP = "UniteFonctionnelleOperation";
    public static final String APPLICATION_TIERS = "ApplicationTiers";
    public static final String SURCHARGE_LIBELLE = "SurchargeLibelle";
    public static final String CPV = "CPV";
    public static final String LIEU_EXECUTION = "LIEU_EXECUTION";
    public static final String REGION = "REGION";
    public static final String STATUT_CHORUS = "STATUT_CHORUS";
    public static final String CHAMP_FUSION = "ChampFusion";
    public static final String CCAG_REFERENCE = "CCAGReference";
    public static final String TYPE_GROUPEMENT = "TYPE_GROUPEMENT";
    public static final String FORME_PRIX = "FORME_PRIX";
    public static final String TYPE_PRIX = "TYPE_PRIX";

    public static final String REVISION_PRIX = "REVISION_PRIX";
    public static final String PROCEDURE = "PROCEDURE";
    public static final String MODALITE_EXECUTION = "MODALITE_EXECUTION";
    public static final String TECHNIQUE_ACHAT = "TECHNIQUE_ACHAT";
    public static final String CLAUSE_N1 = "ClauseN1";
    public static final String CLAUSE_N2 = "ClauseN2";
    public static final String CLAUSE_N3 = "ClauseN3";
    public static final String CLAUSE_N4 = "ClauseN4";
    public static final String CLAUSE_N2N3 = "ClauseN2N3";
    public static final String CATEGORIE_FOURNISSEUR = "CategorieFournisseur";
    public static final String NATURE_CONTRAT_CONCESSION = "NatureContratConcession";
    public static final String TYPE_CONCESSION = "TypeConcession";
    public static final String RETOUR_CHORUS = "RETOUR_CHORUS";
    public static final String DEVISE = "Devise";
}
