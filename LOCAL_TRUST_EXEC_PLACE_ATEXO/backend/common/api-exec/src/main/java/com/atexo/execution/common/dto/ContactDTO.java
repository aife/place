package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ContactDTO extends AbstractDTO {

	String nom;

	String prenom;

	String email;

	String telephone;

	String fonction;

	String adresse;

	Boolean actif;

	EtablissementDTO etablissement;

    List<ContactReferantDTO> contactReferantDTOS = new ArrayList<>();
}
