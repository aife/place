package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceDTO extends AbstractDTO {

	String nom;
	String nomCourt;
	String nomCourtArborescence;
	OrganismeDTO organisme;
	Integer niveau;
	Boolean actif;
    Long idParent;
	boolean echangesChorus;
}
