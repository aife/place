package com.atexo.execution.common.dto.process.attributaire;

import com.atexo.execution.common.dto.ContratEtablissementDTO;

public class AttributaireEtablissementDTO implements AttributaireDTO {

	ContratEtablissementDTO contratEtablissement;

	public ContratEtablissementDTO getContratEtablissement() {
		return contratEtablissement;
	}

	public void setContratEtablissement(ContratEtablissementDTO contratEtablissement) {
		this.contratEtablissement = contratEtablissement;
	}

	@Override
	public TypeAttributaire getType() {
		return TypeAttributaire.Entreprise;
	}
}
