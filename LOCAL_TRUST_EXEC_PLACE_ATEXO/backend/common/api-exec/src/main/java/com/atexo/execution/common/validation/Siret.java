package com.atexo.execution.common.validation;

import com.atexo.execution.common.validation.impl.SiretValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SiretValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Siret {
    String message() default "Le numéro Siret est invalide";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
