package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.StatutActe;

import java.util.Set;

public class ActeCriteriaDTO {

	private Long id;
	private String numero;
	private String objet;
	private ContratDTO contrat;
	private Set<StatutActe> statuts;
	private String motsCles;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero( String numero ) {
		this.numero = numero;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet( String objet ) {
		this.objet = objet;
	}

	public ContratDTO getContrat() {
		return contrat;
	}

	public void setContrat( ContratDTO contrat ) {
		this.contrat = contrat;
	}

	public Set<StatutActe> getStatuts() {
		return statuts;
	}

	public void setStatuts( Set<StatutActe> statuts ) {
		this.statuts = statuts;
	}

	public String getMotsCles() {
		return motsCles;
	}

	public void setMotsCles( String motsCles ) {
		this.motsCles = motsCles;
	}
}
