package com.atexo.execution.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class EchangeChorusDTO extends AbstractDTO {

    private ValueLabelDTO statut;

    private ValueLabelDTO retourChorus;

    private String erreurPublication;

    private LocalDateTime datePublication;

    private String reference;

    private LocalDateTime dateNotification;

    private LocalDateTime dateEnvoi;

    private String organisationAchat;

    private String groupementAchat;

    private UtilisateurDTO agent;

    private String idExterne;

    private LocalDateTime dateNotificationPrevisionnelle;

    @JsonProperty("acte")
    private ActeDTO acteDTO;
}
