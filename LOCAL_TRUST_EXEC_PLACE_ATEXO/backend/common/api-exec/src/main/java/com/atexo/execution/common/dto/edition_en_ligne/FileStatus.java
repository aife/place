package com.atexo.execution.common.dto.edition_en_ligne;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class FileStatus {
    private String key;
    private String documentId;
    private FileStatusEnum status;
    private String mode;
    private int redacStatus;
    private String redacAction;
    private int version;
    private Timestamp creationDate;
    private Timestamp modificationDate;
    private List<String> users;
}
