package com.atexo.execution.common.dto;

import lombok.Getter;

@Getter
public enum DureeEnum {
    JOUR, SEMAINE, MOIS, ANNEE
}
