package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClauseTypeEnum {
    CLAUSE_SOCIALE("clausesSociales"), CLAUSE_ENVIRONNEMENTALE("clauseEnvironnementale");

    private final String value;
}
