package com.atexo.execution.common.dto;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "value")
public class ValueLabelDTO {
    Long id;
    String value;
    String label;
    String idExterne;
    String codeExterne;
    String parentLabel;
    String groupeCode;
    Set<ValueLabelDTO> parents = new HashSet<>();
    String toolTip;
    Long limitation;
    Long ordre;
    Boolean entiteEligibleActif = false;
    String symbole;
    Integer modeEchangeChorus;

}
