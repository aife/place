package com.atexo.execution.common.exception;

public class MPEApiException extends Exception {


    public MPEApiException() {

    }

    public MPEApiException(String reason) {
        super(reason);
    }

    public MPEApiException(Throwable t) {
        super(t);
    }

}
