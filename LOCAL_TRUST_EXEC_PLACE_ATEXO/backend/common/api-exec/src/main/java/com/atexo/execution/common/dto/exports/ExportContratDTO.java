package com.atexo.execution.common.dto.exports;

import lombok.*;

import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExportContratDTO {
    String numeroContrat;
    String referenceLibre;
    String intitule;
    String objetConsultation;
    String objetMarche;
    String typeContrat;
    String statutContrat;
    String accordCadreOUSADLie;
    String numeroChapeau;
    String procedurePassation;
    String referenceConsultation;
    String nomEntrepriseAttributaire;
    String siretAttributaire;
    String pme;
    String montantContratAttribue;
    String montantContratFacture;
    String montantContratMandate;
    String montantMaximumEstime;
    String formeDuPrix;
    String categorie;
    String cpvPrincipal;
    String considerationsSociales;
    String considerationsEnvironnementales;
    String lieuPrincipalExecution;
    String dateAttribution;
    String datePrevisionnelleNotification;
    String datePrevisionnelleFinContrat;
    String datePrevisionnelleMaximaleFinContrat;
    String dateNotification;
    String dateFinContrat;
    String dateMaximaleFinContrat;
    String dureeMaximaleMarche;
    String entitePublique;
    String service;
    String nomAagentGestionnaire;
    String prenomAagenGestionnaire;
    String donneesEssentiellesPublicationContrat;
    String ccagApplicable;
    String achatResponsable;
    String trancheBudgetaire;
    String modaliteRevisionPrix;
    String contratARenouveler;
    String createur;
    String organisme;
    String dateDemarrage;
    String dateExecution;
    String dureeRenouvellement;
    String organismesEligibles;
    String montantTotalAvenants;
    String typeBornes;
    String borneMinmale;
    String borneMaximale;
    String montantTotal;
    Set<String> typesPrix;
    String numeroLongContrat;
    String numeroCourt;
    String statutPublicationDonnesEssentielles;


}
