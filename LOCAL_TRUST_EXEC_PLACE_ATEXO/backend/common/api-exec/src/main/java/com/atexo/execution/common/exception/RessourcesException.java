package com.atexo.execution.common.exception;

public class RessourcesException extends RuntimeException {

    public RessourcesException(String message) {
        super(message);
    }

    public RessourcesException(String message, Throwable cause) {
        super(message, cause);
    }
}
