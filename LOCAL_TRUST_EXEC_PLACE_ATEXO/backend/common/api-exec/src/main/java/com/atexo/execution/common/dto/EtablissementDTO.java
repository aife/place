package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EtablissementDTO extends AbstractDTO {

	String siret;
	boolean siege;
	AdresseDTO adresse;
	FournisseurDTO fournisseur;
	List<ContactDTO> contacts = new ArrayList<>();
	Long nbContacts;
	String naf;
	String libelleNaf;
	String description;
    String raisonSociale;
    String uuidFournisseur;
}
