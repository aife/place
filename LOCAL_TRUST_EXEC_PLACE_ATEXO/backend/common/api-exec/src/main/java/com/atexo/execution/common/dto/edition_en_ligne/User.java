package com.atexo.execution.common.dto.edition_en_ligne;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    @NotEmpty
    private String id;
    @NotEmpty
    private String name;

    private List<String> roles = new ArrayList<>();
}
