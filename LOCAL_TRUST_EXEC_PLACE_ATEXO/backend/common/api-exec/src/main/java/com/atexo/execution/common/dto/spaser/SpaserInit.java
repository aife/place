package com.atexo.execution.common.dto.spaser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpaserInit {
    private String token;
    private String refreshToken;
    private String agentId;
    private String plateformUid;
    private String organisme;
    @JsonProperty("context")
    private SpaserContext spaserContext;
}
