package com.atexo.execution.common.validation;

import com.atexo.execution.common.validation.impl.SirenValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SirenValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Siren {
    String message() default "Le numéro Siren est invalide";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
