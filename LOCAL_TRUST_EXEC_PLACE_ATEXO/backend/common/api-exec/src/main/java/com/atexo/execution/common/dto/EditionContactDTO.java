package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditionContactDTO {

    String nom;

    long id;

    String prenom;

    String email;

    String telephone;

    String fonction;

    EtablissementDTO etablissement;
}
