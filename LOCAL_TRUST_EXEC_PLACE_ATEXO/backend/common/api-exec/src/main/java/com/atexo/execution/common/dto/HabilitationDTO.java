package com.atexo.execution.common.dto;

public class HabilitationDTO extends AbstractDTO {

    String code;

    String libelle;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
