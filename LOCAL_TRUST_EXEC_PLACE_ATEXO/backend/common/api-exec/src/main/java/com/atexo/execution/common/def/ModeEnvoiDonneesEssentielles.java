package com.atexo.execution.common.def;

public enum ModeEnvoiDonneesEssentielles {

	TNCP, DATA_GOUV, VIA_DUME

}
