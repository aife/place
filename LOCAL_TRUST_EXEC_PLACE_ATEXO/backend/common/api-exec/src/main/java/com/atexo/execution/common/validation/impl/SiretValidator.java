package com.atexo.execution.common.validation.impl;

import com.atexo.execution.common.validation.Siret;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SiretValidator implements ConstraintValidator<Siret, String> {
    @Override
    public void initialize(Siret constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String siret, ConstraintValidatorContext constraintValidatorContext) {
        int total = 0;
        int digit = 0;
        for (int i = 0; i < siret.length(); i++) {
            if ((i % 2) == 0) {
                digit = Integer.parseInt(String.valueOf(siret.charAt(i))) * 2;
                if (digit > 9) digit -= 9;
            } else digit = Integer.parseInt(String.valueOf(siret.charAt(i)));
            total += digit;
        }
        if ((total % 10) == 0) return true;
        else return false;
    }
}
