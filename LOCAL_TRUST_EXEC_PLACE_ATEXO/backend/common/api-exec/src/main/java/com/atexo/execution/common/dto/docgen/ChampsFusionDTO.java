package com.atexo.execution.common.dto.docgen;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString
@EqualsAndHashCode(of = {"champFusion"})
public class ChampsFusionDTO {
    private String libelle;
    private String champFusion;
    private String champsFusionRelatif;
    @Builder.Default
    private List<ChampsFusionDTO> sousChampsFusion = new ArrayList<>();
    private String blocUrl;
    private String type;
    private String sousType;
    private boolean simple;
    @Builder.Default
    private Boolean show = true;
    @Builder.Default
    private boolean collection = false;
    private InfoBulleDTO infoBulle;
    @JsonIgnore
    private String ordre;
    @JsonIgnore
    private String ordreLibelle;
    private String format;
}
