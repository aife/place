package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.StatutRenouvellement;
import com.atexo.execution.common.def.TypeBorne;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContratDTO extends AbstractDTO {

    private String numero;

    private String numEj;

    private String numeroLong;

    private String referenceLibre;

    private StatutContrat statut;

    private String objet;

    private ValueLabelDTO type;

    private BigDecimal montant;

    private BigDecimal montantFacture;

    private BigDecimal montantMandate;

    private boolean attributionAvance;

    private BigDecimal tauxAvance;

    private Date dateNotification;

    private Date dateFinContrat;

    private Date dateMaxFinContrat;
    private String dateMaxFinContratAsString;
    private ServiceDTO service;

    private boolean chapeau;

    private List<EvenementDTO> listBondeCommandes = new ArrayList<>();

    private List<EvenementDTO> listAvenantsNonRejete = new ArrayList<>();

    private int documentsCount;

    private ValueLabelDTO typeFormePrix;

    private TypeBorne typeBorne;

    private BigDecimal borneMinimale;

    private BigDecimal borneMaximale;

    private BigDecimal montantTotalAttributaires;

    private int evenementsEnAttenteCount;

    private Boolean favori = false;

    private BigDecimal montantTotalAvenants;

    ContratEtablissementDTO attributaire;
    ContratEtablissementGroupementDTO groupement;


    private Date dateDemaragePrestation;

    private ValueLabelDTO uniteFonctionnelleOperation;

    private ValueLabelDTO codeAchat;

    private String intitule;

    private List<ValueLabelDTO> lieuxExecution = new ArrayList<>();

    private List<ValueLabelDTO> codesCPV = new ArrayList<>();

    private ValueLabelDTO cpvPrincipal;

    private ValueLabelDTO ccagApplicable;

    private boolean achatResponsable;

    private String trancheBudgetaire;

    private String modaliteRevisionPrix;

    private ValueLabelDTO revisionPrix;

    private boolean defenseOuSecurite;

    private boolean marcheInnovant;

    private boolean publicationDonneesEssentielles;

    private Date datePrevisionnelleNotification;

    private Date datePrevisionnelleFinMarche;

    private Date datePrevisionnelleFinMaximaleMarche;

    private Integer dureeMaximaleMarche;

    private Date dateDebutExecution;

    private List<OrganismeDTO> organismesEligibles = new ArrayList<>();

    private List<ServiceDTO> servicesEligibles = new ArrayList<>();

    private ValueLabelDTO categorie;

    private StatutRenouvellement statutRenouvellement;

    private Date dateDebutEtudeRenouvellement;

    @JsonProperty("aRenouveler")
    private boolean aRenouveler;

    private Integer dureePhaseEtudeRenouvellement;

    private ConsultationDTO consultation;

    private UtilisateurDTO createur; // le créateur peut changer de service
    private DonneesEssentiellesDTO donneesEssentielles;
    private String referenceContratChapeau;
    private Long idContratChapeau;
    private Boolean created;
    private Set<EchangeChorusDTO> echangesChorus = new HashSet<>();
    private List<ContratDTO> contrats = new ArrayList<>();
    private boolean exNihilo;
    private boolean considerationsEnvironnementales;
    private boolean considerationsSociales;

    private Integer numeroLot;

    private boolean besoinReccurent;
    private Integer idOffre;
    private boolean notifiable;

    private List<ClauseDto> clausesDto;
    private String statutEJ;


    @Getter(AccessLevel.NONE)
    private int augmentation = 0;

    private int tauxTVA = 20;

    @Getter(AccessLevel.NONE)
    private BigDecimal montantTTC;

    private ValueLabelDTO natureContratConcession;

    private BigDecimal valeurGlobale;
    private BigDecimal montantSubventionPublique;
    private Date dateExecution;

    public BigDecimal getMontantTTC() {
        if (montant == null || tauxTVA == 0) {
            return BigDecimal.ZERO;
        }
        return montant.add(montant.multiply(new BigDecimal(tauxTVA).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP)));
    }

    public int getAugmentation() {
        if (montant == null || montant.compareTo(BigDecimal.ZERO) == 0 || montantTotalAvenants == null || montantTotalAvenants.compareTo(BigDecimal.ZERO) == 0) {
            return 0;
        }
        return BigDecimal.valueOf(100).multiply(this.montantTotalAvenants.divide(this.montant, 2, RoundingMode.UP)).intValue();
    }
    Integer offresRecues;

    private Long idLienAcSad;
    private List<ContratEtablissementDTO> coTraitants = new ArrayList<>();
    private boolean isConcession;
    private boolean peutEchangerAvecChorus;
    private Long dureeSupplementaire;
}

