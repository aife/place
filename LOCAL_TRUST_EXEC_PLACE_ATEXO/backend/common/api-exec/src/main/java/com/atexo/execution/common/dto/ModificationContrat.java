package com.atexo.execution.common.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModificationContrat {
    private Long id;
    private String idContratTitulaire;
    private Integer numOrdre;
    private LocalDateTime dateCreation;
    private LocalDateTime dateModification;
    private String idAgent;
    private String objetModification;
    private LocalDateTime dateSignature;
    private Double montant;
    private String idEtablissement;
    private Integer dureeMarche;
    private Integer statutPublicationSn;
    private LocalDateTime datePublicationSn;
    private String erreurSn;
    private LocalDateTime dateModificationSn;
}
