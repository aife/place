package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactPrincipalDTO extends AbstractDTO {

    private ContactDTO contact;
    private ContratEtablissementDTO contratEtablissement;
    private String roleContrat;
}
