package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.PerimetreContrat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DonneesEssentiellesCriteriaDTO {
    private Long idDonneeEssentielle;
    private Long idContrat;
    private List<ValueLabelDTO> typesContrat;
    private List<Date> dateNotification;
    private List<Date> dateEnvoi;
    private List<String> statuts;
    private UtilisateurDTO utilisateur;
    private PerimetreContrat perimetre;

}
