
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "os",
        "navigateur",
        "java_ver"
})
public class ChampsTechniques {

    @JsonProperty("os")
    private Champ os;
    @JsonProperty("navigateur")
    private Champ navigateur;
    @JsonProperty("java_ver")
    private Champ javaVer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("os")
    public Champ getOs() {
        return os;
    }

    @JsonProperty("os")
    public void setOs(Champ os) {
        this.os = os;
    }

    @JsonProperty("navigateur")
    public Champ getNavigateur() {
        return navigateur;
    }

    @JsonProperty("navigateur")
    public void setNavigateur(Champ navigateur) {
        this.navigateur = navigateur;
    }

    @JsonProperty("java_ver")
    public Champ getJavaVer() {
        return javaVer;
    }

    @JsonProperty("java_ver")
    public void setJavaVer(Champ javaVer) {
        this.javaVer = javaVer;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
