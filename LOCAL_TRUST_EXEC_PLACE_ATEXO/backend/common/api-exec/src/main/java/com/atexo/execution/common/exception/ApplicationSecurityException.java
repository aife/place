package com.atexo.execution.common.exception;

public class ApplicationSecurityException extends ApplicationException {

    public ApplicationSecurityException(String message) {
        super(message);
    }
}
