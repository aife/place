
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "label",
        "description",
        "valeur",
        "type_valeur",
        "visible_utilisateur",
        "visible_support",
        "extras"
})
public class Champ {

    @JsonProperty("label")
    private String label;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("valeur")
    private String valeur;
    @JsonProperty("type_valeur")
    private String typeValeur;
    @JsonProperty("visible_utilisateur")
    private Boolean visibleUtilisateur;
    @JsonProperty("visible_support")
    private Boolean visibleSupport;
    @JsonProperty("extras")
    private Object extras;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Champ(String label, Object description, String valeur) {
        this.label = label;
        this.description = description;
        this.valeur = valeur;
        this.visibleSupport = true;
        this.visibleUtilisateur = true;
        this.typeValeur = "string";
    }

    public Champ(String label, Object description, String valeur, boolean visibleSupport, boolean visibleUtilisateur) {
        this.label = label;
        this.description = description;
        this.valeur = valeur;
        this.visibleSupport = visibleSupport;
        this.visibleUtilisateur = visibleUtilisateur;
        this.typeValeur = "string";
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Object description) {
        this.description = description;
    }

    @JsonProperty("valeur")
    public String getValeur() {
        return valeur;
    }

    @JsonProperty("valeur")
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @JsonProperty("type_valeur")
    public String getTypeValeur() {
        return typeValeur;
    }

    @JsonProperty("type_valeur")
    public void setTypeValeur(String typeValeur) {
        this.typeValeur = typeValeur;
    }

    @JsonProperty("visible_utilisateur")
    public Boolean getVisibleUtilisateur() {
        return visibleUtilisateur;
    }

    @JsonProperty("visible_utilisateur")
    public void setVisibleUtilisateur(Boolean visibleUtilisateur) {
        this.visibleUtilisateur = visibleUtilisateur;
    }

    @JsonProperty("visible_support")
    public Boolean getVisibleSupport() {
        return visibleSupport;
    }

    @JsonProperty("visible_support")
    public void setVisibleSupport(Boolean visibleSupport) {
        this.visibleSupport = visibleSupport;
    }

    @JsonProperty("extras")
    public Object getExtras() {
        return extras;
    }

    @JsonProperty("extras")
    public void setExtras(Object extras) {
        this.extras = extras;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
