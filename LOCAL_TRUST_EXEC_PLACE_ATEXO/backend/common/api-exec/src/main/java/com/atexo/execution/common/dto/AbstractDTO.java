package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AbstractDTO {

    private Long id;
    private String uuid;
    private String idExterne;
    private String organismeUid;

}
