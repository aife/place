package com.atexo.execution.common.dto;

import java.time.LocalDateTime;

public class DocumentActeDTO extends AbstractDTO {

	String idExterne;

	String reference;

	ActeDTO acte;

	FichierDTO fichier;

	UtilisateurDTO utilisateur;

	LocalDateTime dateCreation;

	LocalDateTime dateModification;

	private Integer version;

	public String getIdExterne() {
		return idExterne;
	}

	public void setIdExterne( String idExterne ) {
		this.idExterne = idExterne;
	}

	public ActeDTO getActe() {
		return acte;
	}

	public void setActe( ActeDTO acte ) {
		this.acte = acte;
	}

	public FichierDTO getFichier() {
		return fichier;
	}

	public void setFichier( FichierDTO fichier ) {
		this.fichier = fichier;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur( UtilisateurDTO utilisateur ) {
		this.utilisateur = utilisateur;
	}

	public LocalDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	public LocalDateTime getDateModification() {
		return dateModification;
	}

	public void setDateModification(LocalDateTime dateModification) {
		this.dateModification = dateModification;
	}

	public String getReference() {
		return reference;
	}

	public void setReference( String reference ) {
		this.reference = reference;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion( Integer version ) {
		this.version = version;
	}
}
