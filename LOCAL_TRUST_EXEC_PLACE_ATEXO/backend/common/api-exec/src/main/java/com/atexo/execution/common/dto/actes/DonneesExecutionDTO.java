package com.atexo.execution.common.dto.actes;

import com.atexo.execution.common.dto.ActeDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DonneesExecutionDTO extends ActeDTO {
    private Double depensesInvestissement;
    private String intituleTarif;
    private Double tarif;
    private Boolean publicationDonneesEssentielles;
}

