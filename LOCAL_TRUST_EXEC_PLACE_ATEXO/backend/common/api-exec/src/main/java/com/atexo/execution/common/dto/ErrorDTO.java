package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@RequiredArgsConstructor
public class ErrorDTO implements Serializable {

    @NonNull
    Integer code;

    String path;

    Object rejectedValue;

    @NonNull
    String message;

    @NonNull
    Throwable exception;

    public ErrorDTO(Integer code, String path, String message, Throwable exception) {
        super();
        this.code = code;
        this.path = path;
        this.message = message;
        this.exception = exception;
    }
}
