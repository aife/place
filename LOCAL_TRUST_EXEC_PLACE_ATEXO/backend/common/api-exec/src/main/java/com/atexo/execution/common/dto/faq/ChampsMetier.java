
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ref_consult",
        "id_consult",
        "etape",
        "allotissement",
        "mps",
        "rdp"
})
public class ChampsMetier {

    @JsonProperty("ref_consult")
    private Champ refConsult;
    @JsonProperty("id_consult")
    private Champ idConsult;
    @JsonProperty("ref_contrat")
    private Champ refContrat;
    @JsonProperty("id_conntrat")
    private Champ idContrat;
    @JsonProperty("etape")
    private Champ etape;
    @JsonProperty("allotissement")
    private Champ allotissement;
    @JsonProperty("mps")
    private Champ mps;
    @JsonProperty("rdp")
    private Champ rdp;
    @JsonProperty("statut_contrat")
    private Champ statutContrat;
    @JsonProperty("type_contrat")
    private Champ typeContrat;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ref_consult")
    public Champ getRefConsult() {
        return refConsult;
    }

    @JsonProperty("ref_consult")
    public void setRefConsult(Champ refConsult) {
        this.refConsult = refConsult;
    }

    @JsonProperty("id_consult")
    public Champ getIdConsult() {
        return idConsult;
    }

    @JsonProperty("id_consult")
    public void setIdConsult(Champ idConsult) {
        this.idConsult = idConsult;
    }

    @JsonProperty("ref_contrat")
    public Champ getRefContrat() {
        return refContrat;
    }

    @JsonProperty("ref_contrat")
    public void setRefContrat(Champ refContrat) {
        this.refContrat = refContrat;
    }

    @JsonProperty("id_contrat")
    public Champ getIdContrat() {
        return idContrat;
    }

    @JsonProperty("id_contrat")
    public void setIdContrat(Champ idContrat) {
        this.idContrat = idContrat;
    }

    @JsonProperty("etape")
    public Champ getEtape() {
        return etape;
    }

    @JsonProperty("etape")
    public void setEtape(Champ etape) {
        this.etape = etape;
    }

    @JsonProperty("allotissement")
    public Champ getAllotissement() {
        return allotissement;
    }

    @JsonProperty("allotissement")
    public void setAllotissement(Champ allotissement) {
        this.allotissement = allotissement;
    }

    @JsonProperty("mps")
    public Champ getMps() {
        return mps;
    }

    @JsonProperty("mps")
    public void setMps(Champ mps) {
        this.mps = mps;
    }

    @JsonProperty("rdp")
    public Champ getRdp() {
        return rdp;
    }

    @JsonProperty("rdp")
    public void setRdp(Champ rdp) {
        this.rdp = rdp;
    }

    @JsonProperty("type_contrat")
    public Champ getTypeContrat() {
        return typeContrat;
    }

    @JsonProperty("type_contrat")
    public void setTypeContrat(Champ typeContrat) {
        this.typeContrat = typeContrat;
    }

    @JsonProperty("statut_contrat")
    public Champ getStatutContrat() {
        return statutContrat;
    }

    @JsonProperty("statut_contrat")
    public void setStatutContrat(Champ statutContrat) {
        this.statutContrat = statutContrat;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
