package com.atexo.execution.common.dto.docgen;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DocumentModeleDTO {
    private String libelle;
    private String code;
    private String produit;
    private String extension;
}
