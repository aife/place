package com.atexo.execution.common.dto.messagerie;

import lombok.*;

import java.math.BigInteger;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessecInitialisation {

    private BigInteger maxTailleFichiers;
    private String messagerieIdPfEmetteur;
    private String messagerieUrlPfEmetteur;
    private String messagerieNomPfEmetteur;
    private String messagerieIdPfDestinataire;
    private String messagerieUrlPfDestinataire;
    private String messagerieNomPfDestinataire;
    private String messagerieUrlPfEmetteurVisualisation;
    private String messagerieUrlPfDestinataireVisualisation;
    private String messagerieUrlPfReponse;
    private String adresseMailExpediteur;
    private String signatureAvisPassage;
    private String mpeUrl;
    private String mpeLogoPath;
    private String messagerieLogoSrc;
}
