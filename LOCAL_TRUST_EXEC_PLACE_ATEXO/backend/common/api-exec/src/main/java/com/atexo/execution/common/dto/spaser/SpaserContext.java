package com.atexo.execution.common.dto.spaser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpaserContext {

    @JsonProperty("objet_metier_type")
    private String objectMetierType;

    @JsonProperty("objet_metier_external_id")
    private Long objetMetierExternalId;

    @JsonProperty("objet_metier_date_fin")
    private LocalDate objetMetierDateFin;

    @JsonProperty("agent_firstname")
    private String agentFirstname;

    @JsonProperty("agent_lastname")
    private String agentLastname;

    @JsonProperty("type_route")
    private String typeRoute;

}
