package com.atexo.execution.common.def;

public enum Variation {
	FERME, FERME_ET_ACTUALISABLE, REVISABLE
}
