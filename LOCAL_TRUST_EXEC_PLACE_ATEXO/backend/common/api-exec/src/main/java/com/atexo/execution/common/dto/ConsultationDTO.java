package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsultationDTO extends AbstractDTO {


	private String numero;

	private String intitule;

	private String objet;

	private ValueLabelDTO categorie;

	private ValueLabelDTO procedure;

	private ValueLabelDTO typeGroupementOperateurs;

	private Boolean horsPassation;

	private String numeroProjetAchat;

	private Date decisionAttribution;

	private List<ValueLabelDTO> modaliteExecutions = new ArrayList<>();
	private List<ValueLabelDTO> techniqueAchats = new ArrayList<>();
	private List<ValueLabelDTO> typesPrix = new ArrayList<>();

	private  Integer origineUE;

	private Integer origineFrance;


}
