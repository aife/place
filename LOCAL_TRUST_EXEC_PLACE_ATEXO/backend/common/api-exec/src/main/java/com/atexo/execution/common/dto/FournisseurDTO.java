package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FournisseurDTO extends AbstractDTO {

	String siren;

	String raisonSociale;

	String email;

	String formeJuridique;

	Integer taille;

	String telephone;

	String fax;

	String siteInternet;

	List<EtablissementDTO> etablissements;

	private String capitalSocial;

	String description;

	String codeApeNafNace;

	String pays;

	String nom;

	String prenom;

	Boolean trusted;

	Date dateModificationExterne;

	BigDecimal montantContratsMandataire = BigDecimal.ZERO;
	BigDecimal montantContratsCotraitants = BigDecimal.ZERO;
	BigDecimal montantContratsSousTraitants = BigDecimal.ZERO;

	Integer nbrContratsMandataire = 0;
	Integer nbrContratsCotraitants = 0;
	Integer	nbrContratsSousTraitants = 0;
	Integer nbrContratsClos = 0;

	ValueLabelDTO categorie;
}
