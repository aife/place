package com.atexo.execution.common.dto;

public class EchangeChorusCriteriaDTO {

    private Long acte;
    private Long contrat;

    public Long getActe() {
        return acte;
    }

    public void setActe(Long acte) {
        this.acte = acte;
    }

    public Long getContrat() {
        return contrat;
    }

    public void setContrat(Long contrat) {
        this.contrat = contrat;
    }
}
