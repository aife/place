package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.StatutContrat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ContratEtablissementCriteriaDTO {
    private Long idFournisseur;
    private String motsCles;
    private Long organisme;
    private StatutContrat[] statuts;
    private List<String> sirets;

    private Integer idExterneContratTitulaire;
    private List<Integer> idExterneContratTitulaires;
    private String numeroContrat;
    private List<String> numeroContrats;
    private StatutContrat statutContrat;
    private List<StatutContrat> statutContrats;
    private LocalDateTime dateModification;
    private String objetContrat;
    private String intitule;
    private Long idConsultation;
    private Integer idLot;
    private boolean defenseOuSecurite;
}
