package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.DocumentContratType;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentCriteriaDTO {

	private Long contratId;
	private String motsCles;
	private Long[] evenements;
	private Long[] etablissements;
	private Long[] contractants;
	private String[] typeEvenements;
	private DocumentContratType[] documentTypes;
}
