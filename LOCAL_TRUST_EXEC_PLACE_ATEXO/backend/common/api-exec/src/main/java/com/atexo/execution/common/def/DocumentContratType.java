package com.atexo.execution.common.def;

public enum DocumentContratType {

	CONTRAT_AUTRES, LIBRE, PIECE_JOINTE_MESSAGE, GENERE
}
