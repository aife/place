package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChorusDTO extends AbstractDTO {;


	private Boolean actif;

	private String visaACCF;

	private String visaPrefet;

	private String typeFournisseurEntreprise1;

	public Boolean getActif() {
		return actif;
	}

	public void setActif( Boolean actif ) {
		this.actif = actif;
	}

	public String getVisaACCF() {
		return visaACCF;
	}

	public void setVisaACCF( String visaACCF ) {
		this.visaACCF = visaACCF;
	}

	public String getVisaPrefet() {
		return visaPrefet;
	}

	public void setVisaPrefet( String visaPrefet ) {
		this.visaPrefet = visaPrefet;
	}

	public String getTypeFournisseurEntreprise1() {
		return typeFournisseurEntreprise1;
	}

	public void setTypeFournisseurEntreprise1( String typeFournisseurEntreprise1 ) {
		this.typeFournisseurEntreprise1 = typeFournisseurEntreprise1;
	}

}

