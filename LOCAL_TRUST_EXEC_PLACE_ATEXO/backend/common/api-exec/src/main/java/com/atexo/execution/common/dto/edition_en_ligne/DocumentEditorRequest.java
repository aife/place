package com.atexo.execution.common.dto.edition_en_ligne;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class DocumentEditorRequest {
    @NotEmpty
    @NotNull
    private String documentId;
    @NotEmpty
    @NotNull
    private String plateformeId;
    private Integer version;
    @NotEmpty
    @NotNull
    private String documentTitle;
    @NotEmpty
    @NotNull
    private String callback;
    private String mode;
    @NotNull
    private User user;
}
