package com.atexo.execution.common.dto;

public class ContratEtablissementGroupementDTO extends AbstractDTO {

	String nom;

	ValueLabelDTO type;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ValueLabelDTO getType() {
		return type;
	}

	public void setType(ValueLabelDTO type) {
		this.type = type;
	}

}
