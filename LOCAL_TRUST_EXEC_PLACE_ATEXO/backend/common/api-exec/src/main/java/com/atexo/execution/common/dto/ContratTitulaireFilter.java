package com.atexo.execution.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class ContratTitulaireFilter {
    private Integer idContratTitulaire;
    private List<Integer> idContratTitulaires = Collections.emptyList();
    private String numeroContrat;
    private List<String> numeroContrats = Collections.emptyList();
    private Integer statutContrat;
    private List<Integer> statutContrats = Collections.emptyList();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateModification;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateModificationActe;
    private String objetContrat;
    private String intitule;
    private Long idExterneConsultation;
    private Integer idLot;
    private Boolean defenseOuSecurite;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNotificationMin;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNotificationMax;
    private String siren;
    private String complement;
    private Integer statutPublicationSn;
    private List<String> exclusionOrganisme;
    private List<Long> idServices;
    private Boolean chapeau;
    private Integer idExternesLot;
    private List<String> acronymesOrganisme;
    private List<Integer> idExternesService;
}
