    package com.atexo.execution.common.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FournisseurCriteriaDTO {
    private String motsCles;
    private String siren;
    private String pays;
    private String uuid;
}
