package com.atexo.execution.common.dto.process.attributaire;

public enum TypeAttributaire {

	Entreprise, Groupement;

}
