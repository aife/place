package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UtilisateurDTO extends AbstractDTO {

    String identifiant;
    String nom;
    String prenom;
    String email;
    String telephone;
    ServiceDTO service;
	Boolean actif;
	String uuid;
    Long organismeId;
    Long serviceId;
    List<HabilitationDTO> habilitations = new ArrayList<>();
    Set<String> modulesActifs = new HashSet<>();
    Set<Long> perimetreVisionServices = new HashSet<>();
    boolean moduleExecActif = false;
    private String plateformeUid;
    private ValueLabelDTO devise;
}
