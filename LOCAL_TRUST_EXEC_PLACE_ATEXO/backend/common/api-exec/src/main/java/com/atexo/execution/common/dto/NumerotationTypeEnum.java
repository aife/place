package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NumerotationTypeEnum {
    CONTRAT_NUMERO("Numéro court"), CONTRAT_NUMERO_LONG("Numéro long"), CONTRAT_REFERENCE_LIBRE("Référence libre"),
    CONTRAT_CHAPEAU_NUMERO("Numéro court chapeau"), CONTRAT_CHAPEAU_NUMERO_LONG("Numéro long chapeau"), CONTRAT_CHAPEAU_REFERENCE_LIBRE("Référence libre chapeau");

    private final String value;
}
