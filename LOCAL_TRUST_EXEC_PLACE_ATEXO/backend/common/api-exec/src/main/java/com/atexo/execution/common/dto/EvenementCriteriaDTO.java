package com.atexo.execution.common.dto;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EvenementCriteriaDTO {
    Long contrat;
    String motsCles;
    Boolean etatNonSuivi;
    Boolean etatValide;
    Boolean etatRejete;
    Boolean etatAVenir;
    Boolean etatEnAttente;
    Long[] contractants;
    Date dateEnvoiDebut;
    Date dateEnvoiFin;
    String[] evenementTypeCode;
    Boolean bonsCommande;
    Boolean avenants;
}
