package com.atexo.execution.common.def;

public class DocumentDiscriminators {

    public static final String CONTRAT = "contrat";
    public static final String ACTE = "acte";
}
