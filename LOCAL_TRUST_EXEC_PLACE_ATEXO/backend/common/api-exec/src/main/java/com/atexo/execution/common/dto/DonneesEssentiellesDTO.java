package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DonneesEssentiellesDTO extends AbstractDTO {
    private String statut;
    private String erreurPublication;
    private LocalDateTime datePublication;
    private Long idDonneeEssentielle;
    private Long idContrat;
    private String typeContrat;
    private Long idActe;
    private String typeActe;
    private LocalDate dateNotificationContrat;
    private LocalDateTime dateEnvoi;
    private String message;
    private String fichier;
    private String statutContrat;
    private LocalDate dateNotificationActe;
    private String modeEnvoiDonneesEssentielles;
}
