package com.atexo.execution.common.dto.actes;

import com.atexo.execution.common.def.Variation;
import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.dto.SousTraitantDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgrementSousTraitantDTO extends ActeDTO {
	private SousTraitantDTO sousTraitant;
	private Integer rang;
	private String reference;
	private Double modaliteSousTraitance;
	private Double montantHT;
	private Double montantTTC;
	private Double tauxTVA;
	private Variation variation;
	private Long duree;
    private Boolean publicationDonneesEssentielles;
}

