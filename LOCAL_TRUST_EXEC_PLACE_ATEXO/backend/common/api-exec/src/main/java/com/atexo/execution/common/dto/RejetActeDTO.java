package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.MotifRejetActe;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.TypeNotification;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RejetActeDTO extends AbstractDTO {;
	private MotifRejetActe motif;
	private String commentaire;
}

