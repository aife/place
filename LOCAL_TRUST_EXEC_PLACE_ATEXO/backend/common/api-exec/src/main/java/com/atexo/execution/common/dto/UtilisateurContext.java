package com.atexo.execution.common.dto;

import java.io.Serializable;

public class UtilisateurContext implements Serializable {

	UtilisateurDTO utilisateur;

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

}
