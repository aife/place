
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "nom",
        "label",
        "description",
        "max_visible",
        "champs"
})
public class Applicatif {

    @JsonProperty("nom")
    private String nom;
    @JsonProperty("label")
    private String label;
    @JsonProperty("description")
    private String description;
    @JsonProperty("max_visible")
    private Integer maxVisible;
    @JsonProperty("champs")
    private ChampsApplicatifs champs;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Applicatif(String nom, String label, String description) {
        this.nom = nom;
        this.label = label;
        this.description = description;
        this.maxVisible = 1000;
    }

    @JsonProperty("nom")
    public String getNom() {
        return nom;
    }

    @JsonProperty("nom")
    public void setNom(String nom) {
        this.nom = nom;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("max_visible")
    public Integer getMaxVisible() {
        return maxVisible;
    }

    @JsonProperty("max_visible")
    public void setMaxVisible(Integer maxVisible) {
        this.maxVisible = maxVisible;
    }

    @JsonProperty("champs")
    public ChampsApplicatifs getChampsApplicatifs() {
        return champs;
    }

    @JsonProperty("champs")
    public void setChampsApplicatifs(ChampsApplicatifs champs) {
        this.champs = champs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
