package com.atexo.execution.common.dto;

import java.math.BigDecimal;


public class BonCommandeTypeDTO {
    private String numero;

    private BigDecimal montantHT;

    private ValueLabelDTO tauxTVA;

    private BigDecimal montantTVA;

    private String delaiLivExec;

    private String programmeNum;

    private String devisTitulaireNum;

    private String adresseLivExec;

    private String delaiPaiement;

    private String imputationBudgetaire;

    private String engagementNum;

    private String adresseFacturation;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(BigDecimal montantHT) {
        this.montantHT = montantHT;
    }

    public ValueLabelDTO getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(ValueLabelDTO tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public BigDecimal getMontantTVA() {
        return montantTVA;
    }

    public void setMontantTVA(BigDecimal montantTVA) {
        this.montantTVA = montantTVA;
    }

    public String getDelaiLivExec() {
        return delaiLivExec;
    }

    public void setDelaiLivExec(String delaiLivExec) {
        this.delaiLivExec = delaiLivExec;
    }

    public String getProgrammeNum() {
        return programmeNum;
    }

    public void setProgrammeNum(String programmeNum) {
        this.programmeNum = programmeNum;
    }

    public String getDevisTitulaireNum() {
        return devisTitulaireNum;
    }

    public void setDevisTitulaireNum(String devisTitulaireNum) {
        this.devisTitulaireNum = devisTitulaireNum;
    }

    public String getAdresseLivExec() {
        return adresseLivExec;
    }

    public void setAdresseLivExec(String adresseLivExec) {
        this.adresseLivExec = adresseLivExec;
    }

    public String getDelaiPaiement() {
        return delaiPaiement;
    }

    public void setDelaiPaiement(String delaiPaiement) {
        this.delaiPaiement = delaiPaiement;
    }

    public String getImputationBudgetaire() {
        return imputationBudgetaire;
    }

    public void setImputationBudgetaire(String imputationBudgetaire) {
        this.imputationBudgetaire = imputationBudgetaire;
    }

    public String getEngagementNum() {
        return engagementNum;
    }

    public void setEngagementNum(String engagementNum) {
        this.engagementNum = engagementNum;
    }

    public String getAdresseFacturation() {
        return adresseFacturation;
    }

    public void setAdresseFacturation(String adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }
}
