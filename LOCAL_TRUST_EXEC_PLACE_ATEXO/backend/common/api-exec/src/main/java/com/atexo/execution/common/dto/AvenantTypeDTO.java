package com.atexo.execution.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AvenantTypeDTO {

    private String numero;
    private Boolean incidence;
    private Boolean avenantTransfert;
    private Boolean dateModifiable;
    private Boolean autreType;
    private ValueLabelDTO typeAvenant;
    private Boolean value;
    private BigDecimal montant;
    private Date dateDemaragePrestation;
    private Date dateDemaragePrestationNew;
    private Date dateFin;
    private Date dateFinMax;
    private Date dateFinNew;
    private Date dateFinMaxNew;
    private BigDecimal cumulPrecedentsAvenants;
    private BigDecimal cumulAvenantsCourants;

}
