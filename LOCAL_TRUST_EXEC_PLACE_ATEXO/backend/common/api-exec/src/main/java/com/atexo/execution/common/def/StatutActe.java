package com.atexo.execution.common.def;

public enum StatutActe {

	BROUILLON,
	EN_ATTENTE_ACCUSE,
	EN_ATTENTE_VALIDATION,
	VALIDE,
	NOTIFIE,
	ANNULE,
	EN_ERREUR;

}
