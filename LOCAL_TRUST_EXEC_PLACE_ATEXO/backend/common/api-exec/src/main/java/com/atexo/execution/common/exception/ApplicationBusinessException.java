package com.atexo.execution.common.exception;

import java.util.ArrayList;
import java.util.List;

public class ApplicationBusinessException extends ApplicationException {

    List<BusinessExceptionItem> items = new ArrayList<>();

    public ApplicationBusinessException() {
        super();
    }

    public ApplicationBusinessException(String reason) {
        super(reason);
        this.items.add(new BusinessExceptionItem(reason));
    }

    public List<BusinessExceptionItem> getItems() {
        return items;
    }

    public static class BusinessExceptionItem {

        public BusinessExceptionItem(String message) {
            this.message = message;
        }

        String path;

        String message;

        String messageKey;

        Object[] messageArgs;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessageKey() {
            return messageKey;
        }

        public void setMessageKey(String messageKey) {
            this.messageKey = messageKey;
        }

        public Object[] getMessageArgs() {
            return messageArgs;
        }

        public void setMessageArgs(Object[] messageArgs) {
            this.messageArgs = messageArgs;
        }
    }

    public void addBusinessExceptionItem(BusinessExceptionItem businessExceptionItem) {
        if (items != null) {
            items = new ArrayList<>();
        }
        items.add(businessExceptionItem);
    }
}
