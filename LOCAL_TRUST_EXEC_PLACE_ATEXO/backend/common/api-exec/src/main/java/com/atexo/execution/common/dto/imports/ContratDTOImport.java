package com.atexo.execution.common.dto.imports;


import java.util.Date;
import java.util.Objects;

public class ContratDTOImport {
    private String type;

    private String referenceLibre;

    private String siret;

    private String consultationNumero;

    private String objet;

    private String consultationCategorie;

    private String organismeAcronyme;

    private String nomService;

    private String createurNom;

    private String createurPrenom;

    private String formePrix;

    private String typeBorne;

    private Double borneMinimale;

    private Double borneMaximale;

    private Double montant;

    private Double montantMandate;

    private Double montantFacture;

    private Date dateNotification;

    private Date dateDemaragePrestation;

    private Date dateFinContrat;

    private Date dateMaxFinContrat;

    private String groupement;

    private String groupementnom;

    private String groupementtype;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReferenceLibre() {
        return referenceLibre;
    }

    public void setReferenceLibre(String referenceLibre) {
        this.referenceLibre = referenceLibre;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getConsultationNumero() {
        return consultationNumero;
    }

    public void setConsultationNumero(String consultationNumero) {
        this.consultationNumero = consultationNumero;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getConsultationCategorie() {
        return consultationCategorie;
    }

    public void setConsultationCategorie(String consultationCategorie) {
        this.consultationCategorie = consultationCategorie;
    }

    public String getOrganismeAcronyme() {
        return organismeAcronyme;
    }

    public void setOrganismeAcronyme(String organismeAcronyme) {
        this.organismeAcronyme = organismeAcronyme;
    }

    public String getNomService() {
        return nomService;
    }

    public void setNomService(String nomService) {
        this.nomService = nomService;
    }

    public String getCreateurNom() {
        return createurNom;
    }

    public void setCreateurNom(String createurNom) {
        this.createurNom = createurNom;
    }

    public String getCreateurPrenom() {
        return createurPrenom;
    }

    public void setCreateurPrenom(String createurPrenom) {
        this.createurPrenom = createurPrenom;
    }

    public String getFormePrix() {
        return formePrix;
    }

    public void setFormePrix(String formePrix) {
        this.formePrix = formePrix;
    }

    public String getTypeBorne() {
        return typeBorne;
    }

    public void setTypeBorne(String typeBorne) {
        this.typeBorne = typeBorne;
    }

    public Double getBorneMinimale() {
        return borneMinimale;
    }

    public void setBorneMinimale(Double borneMinimale) {
        this.borneMinimale = borneMinimale;
    }

    public Double getBorneMaximale() {
        return borneMaximale;
    }

    public void setBorneMaximale(Double borneMaximale) {
        this.borneMaximale = borneMaximale;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getMontantMandate() {
        return montantMandate;
    }

    public void setMontantMandate(Double montantMandate) {
        this.montantMandate = montantMandate;
    }

    public Double getMontantFacture() {
        return montantFacture;
    }

    public void setMontantFacture(Double montantFacture) {
        this.montantFacture = montantFacture;
    }

    public Date getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(Date dateNotification) {
        this.dateNotification = dateNotification;
    }

    public Date getDateDemaragePrestation() {
        return dateDemaragePrestation;
    }

    public void setDateDemaragePrestation(Date dateDemaragePrestation) {
        this.dateDemaragePrestation = dateDemaragePrestation;
    }

    public Date getDateFinContrat() {
        return dateFinContrat;
    }

    public void setDateFinContrat(Date dateFinContrat) {
        this.dateFinContrat = dateFinContrat;
    }

    public Date getDateMaxFinContrat() {
        return dateMaxFinContrat;
    }

    public void setDateMaxFinContrat(Date dateMaxFinContrat) {
        this.dateMaxFinContrat = dateMaxFinContrat;
    }

    public String getGroupement() {
        return groupement;
    }

    public void setGroupement(String groupement) {
        this.groupement = groupement;
    }

    public String getGroupementnom() {
        return groupementnom;
    }

    public void setGroupementnom(String groupementnom) {
        this.groupementnom = groupementnom;
    }

    public String getGroupementtype() {
        return groupementtype;
    }

    public void setGroupementtype(String groupementtype) {
        this.groupementtype = groupementtype;
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(this.getSiret(), ((ContratDTOImport) obj).getSiret()) &&
                Objects.equals(this.getReferenceLibre(), ((ContratDTOImport) obj).getReferenceLibre());
    }

}
