package com.atexo.execution.common.def;

import java.util.Arrays;

public enum TypeBorne {
    AUCUNE, MINIMALE, MAXIMALE, MINIMALE_MAXIMALE;

    public static TypeBorne from(String value) {
        if (value == null) {
            return null;
        }
        return Arrays.stream(TypeBorne.values()).filter(typeBorne -> typeBorne.name().equalsIgnoreCase(value)).findFirst().orElse(null);
    }
}
