package com.atexo.execution.common.dto;

import java.time.LocalDateTime;

public class ApplicationVersionDTO {
    private String version;
    private LocalDateTime installationDate;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public LocalDateTime getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(LocalDateTime installationDate) {
        this.installationDate = installationDate;
    }
}
