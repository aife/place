package com.atexo.execution.common.dto;

import java.time.LocalDateTime;

public class EchangeDTO {

    String statut;
    String erreurPublication;
    LocalDateTime datePublication;
    private Long id;
    private String idExterne;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdExterne() {
        return idExterne;
    }

    public void setIdExterne(String idExterne) {
        this.idExterne = idExterne;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getErreurPublication() {
        return erreurPublication;
    }

    public void setErreurPublication(String erreurPublication) {
        this.erreurPublication = erreurPublication;
    }

    public LocalDateTime getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDateTime datePublication) {
        this.datePublication = datePublication;
    }
}
