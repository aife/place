package com.atexo.execution.common.dto.exports;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExportActeDTO {

    String objet;
    String numeroActe;
    String typeActe;
    String acheteur;
    String titulaire;
    String statut;
    String dateNotification;
    String statutPublication;

}
