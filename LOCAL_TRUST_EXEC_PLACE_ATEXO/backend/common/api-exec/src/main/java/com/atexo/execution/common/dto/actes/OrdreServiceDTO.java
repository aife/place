package com.atexo.execution.common.dto.actes;

import com.atexo.execution.common.dto.ActeDTO;
import com.atexo.execution.common.dto.EtablissementDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class OrdreServiceDTO extends ActeDTO {
	private Set<EtablissementDTO> cotraitants = new HashSet<>();
	private Boolean reconduction;
	private String maitreOeuvre;
	private String maitreOuvrage;
}

