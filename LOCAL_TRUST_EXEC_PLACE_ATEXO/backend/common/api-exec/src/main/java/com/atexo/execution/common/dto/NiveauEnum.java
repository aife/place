package com.atexo.execution.common.dto;

import lombok.Getter;

@Getter
public enum NiveauEnum {
    PLATEFORME, ORGANISME, SERVICE
}
