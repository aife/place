
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "application",
        "version",
        "type_acteur",
        "utilisateur_nom",
        "utilisateur_id",
        "utilisateur_email",
        "utilisateur_organisme",
        "utilisateur_entite",
        "utiliser_utah",
        "url_utah"
})
public class ChampsApplicatifs {

    @JsonProperty("application")
    private Champ application;
    @JsonProperty("version")
    private Champ version;
    @JsonProperty("type_acteur")
    private Champ typeActeur;
    @JsonProperty("utilisateur_nom")
    private Champ utilisateurNom;
    @JsonProperty("utilisateur_id")
    private Champ utilisateurId;
    @JsonProperty("utilisateur_email")
    private Champ utilisateurEmail;
    @JsonProperty("utilisateur_organisme")
    private Champ utilisateurOrganisme;
    @JsonProperty("utilisateur_entite")
    private Champ utilisateurEntite;
    @JsonProperty("utiliser_utah")
    private Champ utiliserUtah;
    @JsonProperty("url_utah")
    private Champ urlUtah;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("application")
    public Champ getApplication() {
        return application;
    }

    @JsonProperty("application")
    public void setApplication(Champ application) {
        this.application = application;
    }

    @JsonProperty("version")
    public Champ getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Champ version) {
        this.version = version;
    }

    @JsonProperty("type_acteur")
    public Champ getTypeActeur() {
        return typeActeur;
    }

    @JsonProperty("type_acteur")
    public void setTypeActeur(Champ typeActeur) {
        this.typeActeur = typeActeur;
    }

    @JsonProperty("utilisateur_nom")
    public Champ getUtilisateurNom() {
        return utilisateurNom;
    }

    @JsonProperty("utilisateur_nom")
    public void setUtilisateurNom(Champ utilisateurNom) {
        this.utilisateurNom = utilisateurNom;
    }

    @JsonProperty("utilisateur_id")
    public Champ getUtilisateurId() {
        return utilisateurId;
    }

    @JsonProperty("utilisateur_id")
    public void setUtilisateurId(Champ utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    @JsonProperty("utilisateur_email")
    public Champ getUtilisateurEmail() {
        return utilisateurEmail;
    }

    @JsonProperty("utilisateur_email")
    public void setUtilisateurEmail(Champ utilisateurEmail) {
        this.utilisateurEmail = utilisateurEmail;
    }

    @JsonProperty("utilisateur_organisme")
    public Champ getUtilisateurOrganisme() {
        return utilisateurOrganisme;
    }

    @JsonProperty("utilisateur_organisme")
    public void setUtilisateurOrganisme(Champ utilisateurOrganisme) {
        this.utilisateurOrganisme = utilisateurOrganisme;
    }

    @JsonProperty("utilisateur_entite")
    public Champ getUtilisateurEntite() {
        return utilisateurEntite;
    }

    @JsonProperty("utilisateur_entite")
    public void setUtilisateurEntite(Champ utilisateurEntite) {
        this.utilisateurEntite = utilisateurEntite;
    }



    @JsonProperty("utiliser_utah")
    public Champ getUtiliserUtah() {
        return utiliserUtah;
    }

    @JsonProperty("utiliser_utah")
    public void setUtiliserUtah(Champ utiliserUtah) {
        this.utiliserUtah = utiliserUtah;
    }

    @JsonProperty("url_utah")
    public Champ getUrlUtah() {
        return urlUtah;
    }

    @JsonProperty("url_utah")
    public void setUrlUtah(Champ urlUtah) {
        this.urlUtah = urlUtah;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
