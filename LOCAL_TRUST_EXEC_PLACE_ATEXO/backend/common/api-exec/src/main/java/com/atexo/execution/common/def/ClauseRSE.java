package com.atexo.execution.common.def;

public class ClauseRSE {

    public static final String INSERTION_ACTIVITE_ECONOMIQUE = "insertionActiviteEconomique";
    public static final String CLAUSE_SOCIALE_FORMATION_SCOLAIRE = "clauseSocialeFormationScolaire";
    public static final String LUTTE_CONTRE_DISCRIMINATIONS = "lutteContreDiscriminations";
    public static final String COMMERCE_EQUITABLE = "commerceEquitable";
    public static final String ACHATS_ETHIQUES_TRACABILITE_SOCIALE = "achatsEthiquesTracabiliteSociale";
    public static final String CLAUSE_SOCIALE_AUTRE = "clauseSocialeAutre";
    public static final String CONDITION_EXECUTION = "conditionExecution";
    public static final String SPECIFICATION_TECHNIQUE = "specificationTechnique";
    public static final String CRITERE_ATTRIBUTION_MARCHE = "critereAttributionMarche";
    public static final String MARCHE_RESERVE = "marcheReserve";
    public static final String CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE = "clauseSocialeReserveAtelierProtege";
    public static final String CLAUSE_SOCIALE_SIAE = "clauseSocialeSIAE";
    public static final String CLAUSE_SOCIALE_EESS = "clauseSocialeEESS";
    public static final String INSERTION = "insertion";
    public static final String SPECIFICATIONS_TECHNIQUES = "specificationsTechniques";
    public static final String CONDITIONS_EXECUTIONS = "conditionsExecutions";
    public static final String CRITERES_SELECTIONS = "criteresSelections";
    public static final String CLAUSES_N_1 = "clausesN1";
    public static final String CLAUSES_N_2 = "clausesN2";
    public static final String CLAUSES_N_3 = "clausesN3";
    public static final String CLAUSES_N_4 = "clausesN4";
    public static final String VERIFICATION_NON_RESPECT = "verificationNonRespect";
    public static final String CLAUSES_SOCIALES = "clausesSociales";
    public static final String CLAUSES_ENVIRONNEMENTALES = "clauseEnvironnementale";
    public static final String PROTECTION_VALORISATION_ENVIRONNEMENT = "protectionValorisationEnvironnement";
    public static final String CLAUSE_INCITATIVE_FEMMES_HOMMES = "clauseIncitativeFemmesHommes";
    public static final String CLAUSE_COERCITIVE_FEMMES_HOMMES = "clauseCoercitiveFemmesHommes";
    public static final String MOBILISATION_INSERTION_ACTIVITE_ECONOMIQUE = "mobilisationInsertionActiviteEconomique";
    public static final String MOBILISATION_HANDICAP = "mobilisationHandicap";
    public static final String EGALITE_FEMMES_HOMMES = "egaliteFemmesHommes";
    public static final String CLAUSE_SOCIALE_PRESTATIONS_ETABLISSEMENT_PENITENTIAIRE = "clauseSocialePrestationsEtablissementPenitentiaire";
    public static final String CLAUSE_SOCIALE_RESERVE_EA_ESAT_SIAE = "clauseSocialeReserveEaEsatSiae";
    public static final String CLAUSE_ENVIRONNEMENTALE_EXCLUSION_PLASTIQUE = "clauseEnvironnementaleExclusionPlastique";
    public static final String CLAUSE_ENVIRONNEMENTALE_BEGES = "clauseEnvironnementaleBEGES";
    public static final String CLAUSE_ENVIRONNEMENTALE_ECOLABEL = "clauseEnvironnementaleEcolabel";
    public static final String CLAUSE_ENVIRONNEMENTALE_ECONOMIE_ENERGIE = "clauseEnvironnementaleEconomieEnergie";
    public static final String CLAUSE_ENVIRONNEMENTALE_PERFORMANCE_ENERGIE = "clauseEnvironnementalePerformanceEnergie";
    public static final String CLAUSE_ENVIRONNEMENTALE_REDUCTION_POLLUANTS = "clauseEnvironnementaleReductionPolluants";
    public static final String CLAUSE_ENVIRONNEMENTALE_ECONOMIE_CIRCULAIRE = "clauseEnvironnementaleEconomieCirculaire";
    public static final String CLAUSE_ENVIRONNEMENTALE_REPARABILITE = "clauseEnvironnementaleReparabilite";
    public static final String CLAUSE_ENVIRONNEMENTALE_DEFORESTATION = "clauseEnvironnementaleDeforestation";
    public static final String CLAUSE_ENVIRONNEMENTALE_MODALITE_EXECUTION = "clauseEnvironnementaleModaliteExecution";
    public static final String CLAUSE_ENVIRONNEMENTALE_COMPOSITION_PRODUITS = "clauseEnvironnementaleCompositionProduits";
    public static final String CLAUSE_ENVIRONNEMENTALE_GESTION_DECHETS = "clauseEnvironnementaleGestionDechets";
    public static final String CLAUSE_ENVIRONNEMENTALE_AUTRE = "clauseEnvironnementaleAutre";
    public static final String CLAUSE_ENVIRONNEMENTALE_BIODIVERSITE = "clauseEnvironnementaleBiodiversite";

}
