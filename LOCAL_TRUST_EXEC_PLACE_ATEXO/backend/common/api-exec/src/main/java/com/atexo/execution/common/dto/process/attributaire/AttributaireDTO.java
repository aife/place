package com.atexo.execution.common.dto.process.attributaire;

public interface AttributaireDTO {

	TypeAttributaire getType();

}
