package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.def.TypeNotification;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "classname")
public class ActeDTO extends AbstractDTO {
	private String classname;
	private String objet;
	private ValueLabelDTO type;
	private StatutActe statut;
	private String numero;
	private UtilisateurDTO acheteur;
	private ContactDTO titulaire;
	private List<DocumentActeDTO> documents = new ArrayList<>();
	private String commentaire;
	private ContratDTO contrat;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateNotification;
	private DonneesEssentiellesDTO donneesEssentielles;
	private EchangeChorusDTO echangeChorus;
	private TypeNotification typeNotification;
	private boolean validationChorus;
	private Integer version;
	private LocalDateTime dateCreation;
	private LocalDateTime dateModification;
	private RejetActeDTO rejet;
	private ChorusDTO chorus;
}

