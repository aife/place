package com.atexo.execution.common.def;

import java.util.Arrays;

public enum TypeGroupement implements ReferentielEnum {

    SOLIDAIRE("Solidaire"),
    CONJOINT("Conjoint"),
    AUCUN("Aucun");

    String labelKey;

    private TypeGroupement(String labelKey) {
        this.labelKey = labelKey;
    }

	public static TypeGroupement from(String value) {
		if (value == null) {
			return null;
		}
		return Arrays.stream(TypeGroupement.values()).filter(typeGroupement -> typeGroupement.name().equalsIgnoreCase(value)).findFirst().orElse(null);
	}

	@Override
	public String getLabelKey() {
		return labelKey;
	}

    @Override
    public String getCode() {
        return name();
    }
}
