package com.atexo.execution.common.def;

public enum StatutRenouvellement {
    EN_ATTENTE, NON_RENOUVELE, BESOIN_CREE, PROJET_ACHAT_CREE
}
