package com.atexo.execution.common.dto.edition_en_ligne;

import lombok.Getter;

@Getter
public enum FileStatusEnum {
    REQUEST_TO_OPEN(0), OPENED(1), SAVED_AND_CLOSED(2), CORRUPTED(3),
    CLOSED_WITHOUT_EDITING(4), EDITED_AND_SAVED(6), ERROR_WHILE_SAVING(7), NOT_FOUND(8),
    DOWNLOADED(9), GENERATED(10), IMPORTED(11);
    private final int code;

    FileStatusEnum(int code) {
        this.code = code;
    }
}
