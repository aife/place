package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichierDTO extends AbstractDTO {

    String nom;
    String nomEnregistre;
    Long taille;
    String contentType;
    String reference;
}
