package com.atexo.execution.common.dto;

import java.io.Serializable;

public class ContratMessageStatusDTO implements Serializable {

    private Integer nombreMessageNonLu;
    private Integer nombreMessageNonDelivre;
    private Integer nombreMessageEnAttenteReponse;
    private String idContrat;


    public Integer getNombreMessageNonLu() {
        return nombreMessageNonLu;
    }

    public void setNombreMessageNonLu(Integer nombreMessageNonLu) {
        this.nombreMessageNonLu = nombreMessageNonLu;
    }

    public Integer getNombreMessageNonDelivre() {
        return nombreMessageNonDelivre;
    }

    public void setNombreMessageNonDelivre(Integer nombreMessageNonDelivre) {
        this.nombreMessageNonDelivre = nombreMessageNonDelivre;
    }

    public void setIdContrat(String idContrat) {
        this.idContrat = idContrat;
    }

    public String getIdContrat() {
        return idContrat;
    }

    public Integer getNombreMessageEnAttenteReponse() {
        return nombreMessageEnAttenteReponse;
    }

    public void setNombreMessageEnAttenteReponse(Integer nombreMessageEnAttenteReponse) {
        this.nombreMessageEnAttenteReponse = nombreMessageEnAttenteReponse;
    }
}
