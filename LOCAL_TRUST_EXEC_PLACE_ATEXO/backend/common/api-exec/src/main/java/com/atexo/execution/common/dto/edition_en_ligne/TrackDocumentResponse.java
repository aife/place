package com.atexo.execution.common.dto.edition_en_ligne;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrackDocumentResponse {
    private String error;
}
