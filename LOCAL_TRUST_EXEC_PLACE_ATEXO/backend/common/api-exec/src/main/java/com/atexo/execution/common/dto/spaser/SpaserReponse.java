package com.atexo.execution.common.dto.spaser;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpaserReponse {

    private String redirectUrl;

}
