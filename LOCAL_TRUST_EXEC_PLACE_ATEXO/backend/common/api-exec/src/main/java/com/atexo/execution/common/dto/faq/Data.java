
package com.atexo.execution.common.dto.faq;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "applicatif",
        "metier",
        "technique"
})
public class Data {

    @JsonProperty("applicatif")
    private Applicatif applicatif;
    @JsonProperty("metier")
    private Metier metier;
    @JsonProperty("technique")
    private Technique technique;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("applicatif")
    public Applicatif getApplicatif() {
        return applicatif;
    }

    @JsonProperty("applicatif")
    public void setApplicatif(Applicatif applicatif) {
        this.applicatif = applicatif;
    }

    @JsonProperty("metier")
    public Metier getMetier() {
        return metier;
    }

    @JsonProperty("metier")
    public void setMetier(Metier metier) {
        this.metier = metier;
    }

    @JsonProperty("technique")
    public Technique getTechnique() {
        return technique;
    }

    @JsonProperty("technique")
    public void setTechnique(Technique technique) {
        this.technique = technique;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
