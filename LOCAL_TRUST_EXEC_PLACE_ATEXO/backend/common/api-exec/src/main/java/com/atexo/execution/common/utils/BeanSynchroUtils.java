package com.atexo.execution.common.utils;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Ensemble des méthodes utilitaires de copie des beans lors de la
 * synchronisation.
 */
public class BeanSynchroUtils {

    private static String PATTERN = ".*_(\\d+)$";

    public static String buildIdExterne(Object... params) {
        return Arrays.stream(params).map(Object::toString).collect(Collectors.joining("_"));
    }

    public static String extractId(String input) {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(input);
        return matcher.find() ? matcher.group(1) : null;
    }
}
