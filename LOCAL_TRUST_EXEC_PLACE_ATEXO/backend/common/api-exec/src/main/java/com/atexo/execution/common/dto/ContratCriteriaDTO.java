package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.PerimetreContrat;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.common.def.StatutRenouvellement;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContratCriteriaDTO {
    private List<String> identifiantsAgent;
    private List<String> libellesService;
    private List<String> acronymesOrganisme;
    private String siretAttributaire;
    private List<StatutRenouvellement> statutsRenouvellement;
    private String pfUid;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateDebutRenouvellementMin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateDebutRenouvellementMax;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateMaxFinContratMin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateMaxFinContratMax;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateModificationMin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateModificationMax;

    private Date[] dateNotification;

    private Date[] datePrevisionnelleNotification;

    private Date[] dateFinContrat;

    private Date[] dateMaxFinContrat;

    private String motsCles;

    private PerimetreContrat perimetre;

    private StatutContrat[] statuts;

    private Boolean favoris;

    private Boolean transverse;

    private String codeAchat;

    private String uniteFonctionnelleOperation;
    private List<ValueLabelDTO> typesContrat;
    private List<String> codesExternesTypeContrat;
    private List<ValueLabelDTO> categories;
    private List<String> codesCategorie;
    private Boolean chapeau;
    private Boolean considerationsEnvironnementales;
    private Boolean considerationsSociales;
    private List<String> idExternesConsultation;
    private List<Integer> idExternesOffre;
    private Integer numeroLot;
}
