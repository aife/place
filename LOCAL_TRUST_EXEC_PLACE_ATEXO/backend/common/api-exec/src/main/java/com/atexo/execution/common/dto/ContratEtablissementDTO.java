package com.atexo.execution.common.dto;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ContratEtablissementDTO extends AbstractDTO {

    EtablissementDTO etablissement;

    ContratDTO contrat;

    ContactDTO contact;

    String role;

    ValueLabelDTO categorieConsultation;

    BigDecimal montant;

    Date dateNotification;

    Boolean mandataire;

    EtablissementDTO commanditaire; // pour lequel l'etablissement est sous
    // traitant

    List<ContratEtablissementDTO> sousTraitants = new ArrayList<>();

    Date dateModificationExterne;

    List<ContactDTO> contactReferantList = new ArrayList<>();

    int nombreDeContactReferants;

    private Integer dureeMois;

    private ValueLabelDTO revisionPrix;
}
