package com.atexo.execution.common.dto;

import com.atexo.execution.common.dto.docgen.DocumentModeleDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreationDocumentModelDTO {
    private String objet;
    private EtablissementDTO etablissement;
    private EvenementDTO evenement;
    private DocumentModeleDTO modeleDocument;
    private FichierDTO fichier;
    private String reference;
}
