package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.DocumentContratType;
import com.atexo.execution.common.dto.edition_en_ligne.FileStatusEnum;

import java.time.LocalDateTime;
import java.util.List;

public class DocumentContratDTO extends AbstractDTO {

    String idExterne;

    ContratDTO contrat;

    EtablissementDTO etablissement;

    DocumentContratType documentType;

    String objet;

    String commentaire;

    FichierDTO fichier;

    UtilisateurDTO utilisateur;

    LocalDateTime dateCreation;

    LocalDateTime dateModification;

    List<EvenementDTO> evenements;

    List<String> utilisateursEnLigne;

    private FileStatusEnum statut;

    private LocalDateTime dateModificationEdition;


    public ContratDTO getContrat() {
        return contrat;
    }

    public void setContrat(ContratDTO contrat) {
        this.contrat = contrat;
    }

    public EtablissementDTO getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EtablissementDTO etablissement) {
        this.etablissement = etablissement;
    }

    public DocumentContratType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentContratType documentType) {
        this.documentType = documentType;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public FichierDTO getFichier() {
        return fichier;
    }

    public void setFichier(FichierDTO fichier) {
        this.fichier = fichier;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LocalDateTime getDateModification() {
        return dateModification;
    }

    public void setDateModification(LocalDateTime dateModification) {
        this.dateModification = dateModification;
    }

    public UtilisateurDTO getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(UtilisateurDTO utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getIdExterne() {
        return idExterne;
    }

    public void setIdExterne(String idExterne) {
        this.idExterne = idExterne;
    }

    public List<EvenementDTO> getEvenements() {
        return evenements;
    }

    public void setEvenements(List<EvenementDTO> evenements) {
        this.evenements = evenements;
    }

    public List<String> getUtilisateursEnLigne() {
        return utilisateursEnLigne;
    }

    public void setUtilisateursEnLigne(List<String> utilisateursEnLigne) {
        this.utilisateursEnLigne = utilisateursEnLigne;
    }

    public FileStatusEnum getStatut() {
        return statut;
    }

    public void setStatut(FileStatusEnum statut) {
        this.statut = statut;
    }

    public LocalDateTime getDateModificationEdition() {
        return dateModificationEdition;
    }

    public void setDateModificationEdition(LocalDateTime dateModificationEdition) {
        this.dateModificationEdition = dateModificationEdition;
    }
}
