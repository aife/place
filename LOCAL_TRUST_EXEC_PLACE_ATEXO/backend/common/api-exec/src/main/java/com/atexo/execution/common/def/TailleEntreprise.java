package com.atexo.execution.common.def;

/**
 * Created by qba on 26/05/16.
 */
public enum TailleEntreprise {
    TPE, PME, ETI, GE
}
