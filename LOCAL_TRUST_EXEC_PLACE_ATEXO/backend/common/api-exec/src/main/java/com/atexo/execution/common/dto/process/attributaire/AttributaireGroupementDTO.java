package com.atexo.execution.common.dto.process.attributaire;

import java.util.ArrayList;
import java.util.List;

import com.atexo.execution.common.dto.ContratEtablissementDTO;
import com.atexo.execution.common.dto.ContratEtablissementGroupementDTO;

public class AttributaireGroupementDTO implements AttributaireDTO {

	ContratEtablissementGroupementDTO groupement;

	ContratEtablissementDTO mandataire;

	List<ContratEtablissementDTO> coTraitants;

	public ContratEtablissementGroupementDTO getGroupement() {
		return groupement;
	}

	public void setGroupement(ContratEtablissementGroupementDTO groupement) {
		this.groupement = groupement;
	}

	public ContratEtablissementDTO getMandataire() {
		return mandataire;
	}

	public void setMandataire(ContratEtablissementDTO mandataire) {
		this.mandataire = mandataire;
	}

	public List<ContratEtablissementDTO> getCoTraitants() {
		if (coTraitants == null) {
			coTraitants = new ArrayList<>();
		}
		return coTraitants;
	}

	public void setCoTraitants(List<ContratEtablissementDTO> coTraitants) {
		this.coTraitants = coTraitants;
	}

	@Override
	public TypeAttributaire getType() {
		return TypeAttributaire.Groupement;
	}

}
