package com.atexo.execution.common.dto;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class AttestationModelDTO {

    private String libelle;

    private LocalDateTime date = LocalDateTime.now();

    private String url;

    private Map<String, Object> documents = new HashMap<>();

    public AttestationModelDTO(String libelle, String url) {
        this.libelle = libelle;
        this.url = url;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getDocuments() {
        return documents;
    }

    public void setDocuments(Map<String, Object> documents) {
        this.documents = documents;
    }
}
