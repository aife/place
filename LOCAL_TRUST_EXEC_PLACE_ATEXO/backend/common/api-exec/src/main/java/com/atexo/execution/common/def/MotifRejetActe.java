package com.atexo.execution.common.def;

public enum MotifRejetActe {

	ERREUR_SAISIE,
	ABSENCE_ACCUSE_ENTREPRISE,
	REFUS_CHORUS,
	AUTRE
}
