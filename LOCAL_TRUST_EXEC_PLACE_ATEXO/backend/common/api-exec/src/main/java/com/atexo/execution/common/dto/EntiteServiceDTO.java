package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EntiteServiceDTO extends AbstractDTO {

    String nom;
    String nomCourt;
    String nomCourtArborescence;
    Integer niveau;
    List<EntiteServiceDTO> services;
}
