package com.atexo.execution.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrganismeDTO extends AbstractDTO {

    String nom;

    String acronyme;

    String sigle;

}
