package com.atexo.execution.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.Locale;

public class AccountDTO {

    String login;

    String password;

	public AccountDTO( String login, String password ) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
}
