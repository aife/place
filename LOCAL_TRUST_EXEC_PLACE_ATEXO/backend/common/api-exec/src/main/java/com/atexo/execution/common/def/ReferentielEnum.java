package com.atexo.execution.common.def;

public interface ReferentielEnum {

	public String getLabelKey();

	public String getCode();

}
