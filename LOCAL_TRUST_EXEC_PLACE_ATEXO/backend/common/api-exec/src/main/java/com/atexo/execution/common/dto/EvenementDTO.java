package com.atexo.execution.common.dto;

import com.atexo.execution.common.def.EtatEvenement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Représente un événement dans le calendrier d'exécution
 * Created by sta on 06/09/16.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EvenementDTO extends AbstractDTO {

    //@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="GMT")
    LocalDate dateDebut;

    LocalDate dateFin;

    private LocalDate dateDebutReel;

    private LocalDate dateFinReel;

    private LocalDate dateRejet;

    private String commentaireEtat;

    private Boolean suiviRealisation;

    private EtatEvenement etatEvenement;

    /**
     * Si l'évenement est ponctuel, on ne stocke pas la date de fin
     */
    Boolean ponctuel;

    EtablissementDTO contractant;

    EtablissementDTO nouveauContractant;

    ValueLabelDTO typeEvenement;

    String libelle;

    String commentaire;

    Boolean alerte;

    Integer delaiPreavis;

    private List<ValueLabelDTO> destinataires = new ArrayList<>();
    private List<ValueLabelDTO> destinatairesLibre = new ArrayList<>();

    Boolean envoiAlerte;

    private List<DocumentContratDTO> documents = new ArrayList<>();

    int documentsCount;

    private AvenantTypeDTO avenantType;

    private BonCommandeTypeDTO bonCommandeTypeDTO;

    private List<Long> documentIds = new ArrayList<>();
}
