const concat = require('concat');
const fs = require('fs');
const path = require('path');
const execWebComponent = '../target/dist/front/';
const absolutPath = path.resolve(__dirname, execWebComponent);

let jsFiles = fs.readdirSync(absolutPath).filter(file => file.endsWith('.js') && !file.endsWith('.min.js'));
jsFiles = jsFiles.map(file => `${absolutPath}/${file}`);
console.log(`Found files to concatenate ${jsFiles}:`);
build = async () => {
  await concat(jsFiles, `${absolutPath}/exec.js`);
}

console.log("build des web components exec")
build();

