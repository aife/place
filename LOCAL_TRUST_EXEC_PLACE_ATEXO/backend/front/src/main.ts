import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from 'app/app.module';
import * as numeral from 'numeral';
import * as $ from 'jquery';
import 'numeral/locales/fr';

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));

numeral.locale( 'fr' );
$(function () {
    console.log('JQuery ready!');
});
