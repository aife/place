import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes, RoutesRecognized} from '@angular/router';
import {ContratsComponent} from '@views/contrats/contrats';
import {FicheContratComponent} from '@views/contrats/ficheContrat';
import {FournisseursComponent} from '@views/fournisseurs/fournisseurs';
import {CreeFournisseurComponent} from '@views/fournisseurs/creeFournisseur';
import {FicheFournisseurComponent} from '@views/fournisseurs/ficheFournisseur';
import {ContratTableauBordComponent} from '@views/contrats/contratTableauBord';
import {MessagesHolderComponent} from '@views/messagerie/suivi/messages';
import {EnvoiMessageComponent} from '@views/messagerie/envoi/envoiMessage';
import {AttributairesComponent} from '@views/attributaires/attributaires';
import {DocumentsComponent} from '@views/documents/documents';
import {CalendrierExecutionComponent} from '@views/execution/calendrier';
import {EvenementComponent} from '@views/execution/evenement';
import {GuideComponent} from '@views/aide/guide';
import {ContactsReferantsComponent} from '@views/contact/contactsReferants';
import {CanDeactivateGuard} from '@common/canDeactivateGuard';
import {ImportContratComponent} from '@views/import/importContrat';
import {ImportContractantComponent} from '@views/import/importContractant';
import {APPLICATION_CONTEXT} from '@common/applicationContext';
import {ActeTableauDeBordComponent} from '@views/actes/acte-tableau-bord/acte-tableau-bord.component';
import {ActeCreationComponent} from '@views/actes/acte-creation/acte-creation.component';
import {ContratVisualisationComponent} from '@views/contrats/contratVisualisation';
import {SsoGuard} from './interceptors/sso.guard';
import {NouveauContratComponent} from "@views/contrats/nouveau-contrat/nouveau-contrat.component";
import {
    SupervisionDonneesEssentiellesComponent
} from "@views/supervision-donnees-essentielles/supervision-donnees-essentielles";

interface RouteData {
    fournisseur: boolean;
}

const routes: Routes = [

    {path: 'contrats', component: ContratsComponent, canActivate: [ SsoGuard ] },
    {path: 'actes/:contratId', component: ActeTableauDeBordComponent, canActivate: [ SsoGuard ]},
    {path: 'actes/ajouter/:contratId', component: ActeCreationComponent, canActivate: [ SsoGuard ]},
    {path: 'actes/modifier/:contratId/:acteId', component: ActeCreationComponent, data: {mode: 'update'}, canActivate: [ SsoGuard ]},
    {path: 'actes/afficher/:contratId/:acteId', component: ActeCreationComponent, data: {mode: 'lock'}, canActivate: [ SsoGuard ]},
    {path: 'actes/notification/:contratId/:acteId', component: EnvoiMessageComponent, canActivate: [ SsoGuard ]},
    {path: 'ficheContrat/:contratId', component: FicheContratComponent, canDeactivate: [CanDeactivateGuard], canActivate: [ SsoGuard ]},
    {path: 'newContrat', component: NouveauContratComponent, canActivate: [ SsoGuard ]},
    {path: 'fournisseurs', component: FournisseursComponent, data: <RouteData>{fournisseur: true}, canActivate: [ SsoGuard ]},
    {path: 'creeFournisseur', component: CreeFournisseurComponent, data: <RouteData>{fournisseur: true}, canActivate: [ SsoGuard ]},
    {
        path: 'ficheFournisseur/:fournisseurId',
        component: FicheFournisseurComponent,
        data: <RouteData>{fournisseur: true},
        canActivate: [ SsoGuard ]
    },
    {
        path: 'ficheFournisseur/uuid/:uuid',
        component: FicheFournisseurComponent,
        data: <RouteData>{fournisseur: true},
        canActivate: [ SsoGuard ]
    },
    {path: 'tableauDeBord/:contratId', component: ContratTableauBordComponent, canActivate: [ SsoGuard ]},
    {path: 'contrats/visualiser/:idExterne', component: ContratVisualisationComponent, canActivate: [ SsoGuard ]},
    {path: 'messages/:contratId', component: MessagesHolderComponent, canActivate: [ SsoGuard ]},
    {path: 'messages/:contratId/:acteId', component: MessagesHolderComponent, canActivate: [ SsoGuard ]},
    {path: 'envoiMessage/:contratId', component: EnvoiMessageComponent, canActivate: [ SsoGuard ]},
    {path: 'envoiMessage/:contratId/:codeLien', component: EnvoiMessageComponent, canActivate: [ SsoGuard ]},
    {path: 'editionMessage/:contratId/:codeLien', component: EnvoiMessageComponent, canActivate: [ SsoGuard ]},
    {path: 'attributaires/:contratId', component: AttributairesComponent, canActivate: [ SsoGuard ]},
    {path: 'documents/:contratId', component: DocumentsComponent, canActivate: [ SsoGuard ]},
    {path: 'evenement/documents/:contratId/:evenementId', component: DocumentsComponent, canActivate: [SsoGuard]},
    {path: 'calendrier-execution/:contratId', component: CalendrierExecutionComponent, canActivate: [ SsoGuard ]},
    {path: 'calendrier-execution/:contratId/ajouterEvenement', component: EvenementComponent, canActivate: [ SsoGuard ]},
    {path: 'calendrier-execution/:contratId/evenement/:evenementId', component: EvenementComponent, canActivate: [ SsoGuard ]},
    {path: 'contactsReferants/:contratId/:etablissementId', component: ContactsReferantsComponent, canActivate: [ SsoGuard ]},
    {path: 'importContrat', component: ImportContratComponent, canActivate: [ SsoGuard ]},
    {path: 'importContractant', component: ImportContractantComponent, canActivate: [ SsoGuard ]},
    {path: 'guide', component: GuideComponent, canActivate: [ SsoGuard ]},
    {path: '', component: ContratsComponent, canActivate: [SsoGuard]},
    {
        path: 'SupervisionDonneesEssentiellesComponent',
        component: SupervisionDonneesEssentiellesComponent,
        canActivate: [SsoGuard]
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})

export class AppRoutingModule {
    constructor(private router: Router) {

        this.router.events.subscribe((event) => {
            if (event instanceof RoutesRecognized) {
                let activated = event.state.root;
                while (activated.firstChild != null) {
                    activated = activated.firstChild;
                }
                if (APPLICATION_CONTEXT.modeSelectionContact) {
                    if (!activated.data || !(<RouteData> activated.data).fournisseur) {
                        APPLICATION_CONTEXT.modeSelectionContact = false;
                        APPLICATION_CONTEXT.callBack = null;
                    }
                }
            }
        });

    }


}
