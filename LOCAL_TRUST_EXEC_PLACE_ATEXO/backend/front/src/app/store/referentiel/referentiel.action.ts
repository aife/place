import {createAction, props} from '@ngrx/store';
import {OrganismeDTO, ServiceDTO, ValueLabelDTO} from '@dto';
import {TypeReferentiel} from '@dto/backendEnum';


export const getReferentiel = createAction('[REFERENTIEL] Get Referentiel List', props<{ refType: TypeReferentiel }>());

export const successGetReferentiel = createAction('[REFERENTIEL] Success Get Referentiel', props<{ list: Array<ValueLabelDTO> ,
    refType: TypeReferentiel}>());
export const errorGetReferentiel = createAction('[REFERENTIEL] Error Get Referentiel', props<{ error: string ,
    refType: TypeReferentiel }>());

export const getServices = createAction('[REFERENTIEL] Get Services List');

export const successGetServices = createAction('[REFERENTIEL] Success Get Services', props<{
    list: Array<ServiceDTO>
}>());
export const errorGetServices = createAction('[REFERENTIEL] Error Get Services', props<{ error: string }>());


export const getOrganismes = createAction('[REFERENTIEL] Get Organismes List');

export const successGetOrganismes = createAction('[REFERENTIEL] Success Get Organismes', props<{
    list: Array<OrganismeDTO>
}>());
export const errorGetOrganismes = createAction('[REFERENTIEL] Error Get Organismes', props<{ error: string }>());



