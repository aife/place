import {
    errorGetOrganismes,
    errorGetReferentiel,
    errorGetServices,
    getOrganismes,
    getReferentiel,
    getServices,
    successGetOrganismes,
    successGetReferentiel,
    successGetServices
} from './referentiel.action';
import {createReducer, on} from '@ngrx/store';
import {OrganismeDTO, ServiceDTO, ValueLabelDTO} from '@dto';
import {FetchStatusModel} from '@common/fetch-status.model';


export interface ReferentielState {
    referentiels: Map<string, FetchStatusModel<Array<ValueLabelDTO>>>;
    organismes: FetchStatusModel<Array<OrganismeDTO>>;
    services: FetchStatusModel<Array<ServiceDTO>>;

}

const initialState: ReferentielState = {
    referentiels: new Map(),
    organismes: new FetchStatusModel<Array<OrganismeDTO>>(),
    services: new FetchStatusModel<Array<ServiceDTO>>()
};


const _referentielReducer = createReducer(
    initialState,

    on(getReferentiel, (state, props) => {
        let ref: FetchStatusModel<Array<ValueLabelDTO>> = state.referentiels.get(props.refType);
        if (!ref) {
            ref = new FetchStatusModel();
        }
        ref.content = [];
        ref.error = null;
        ref.loading = true;
        state.referentiels.set(props.refType, ref);
        return {
            ...state
        };
    }),
    on(successGetReferentiel, (state, props) => {
        let ref: FetchStatusModel<Array<ValueLabelDTO>> = state.referentiels.get(props.refType);
        if (!ref) {
            ref = new FetchStatusModel();
        }
        ref.content = props.list;
        ref.error = null;
        ref.loading = false;
        state.referentiels.set(props.refType, ref);
        return {
            ...state
        };
    }),
    on(errorGetReferentiel, (state, props) => {
        let ref: FetchStatusModel<Array<ValueLabelDTO>> = state.referentiels.get(props.refType);
        if (!ref) {
            ref = new FetchStatusModel();
        }
        ref.content = [];
        ref.error = props.error;
        ref.loading = false;
        state.referentiels.set(props.refType, ref);
        return {
            ...state
        };
    }),
    on(getServices, (state, props) => {
        let ref: FetchStatusModel<Array<ServiceDTO>> = new FetchStatusModel();

        ref.content = [];
        ref.error = null;
        ref.loading = true;
        return {
            ...state,
            services: ref
        };
    }),
    on(successGetServices, (state, props) => {
        let ref: FetchStatusModel<Array<ServiceDTO>> = new FetchStatusModel();

        ref.content = props.list;
        ref.error = null;
        ref.loading = false;
        return {
            ...state,
            services: ref
        };
    }),
    on(errorGetServices, (state, props) => {
        let ref: FetchStatusModel<Array<ServiceDTO>> = new FetchStatusModel();

        ref.content = [];
        ref.error = props.error;
        ref.loading = false;
        return {
            ...state,
            services: ref
        };
    }),
    on(getOrganismes, (state, props) => {
        let ref: FetchStatusModel<Array<OrganismeDTO>> = new FetchStatusModel();

        ref.content = [];
        ref.error = null;
        ref.loading = true;
        return {
            ...state,
            organismes: ref
        };
    }),
    on(successGetOrganismes, (state, props) => {
        let ref: FetchStatusModel<Array<OrganismeDTO>> = new FetchStatusModel();

        ref.content = props.list;
        ref.error = null;
        ref.loading = false;
        return {
            ...state,
            organismes: ref
        };
    }),
    on(errorGetOrganismes, (state, props) => {
        let ref: FetchStatusModel<Array<OrganismeDTO>> = new FetchStatusModel();

        ref.content = [];
        ref.error = props.error;
        ref.loading = false;
        return {
            ...state,
            organismes: ref
        };
    }),
);



export function referentielReducer(state, action) {
    return _referentielReducer(state, action);
}
