import {Injectable} from '@angular/core';

import {
    errorGetOrganismes,
    errorGetReferentiel,
    errorGetServices,
    getOrganismes,
    getReferentiel,
    getServices,
    successGetOrganismes,
    successGetReferentiel,
    successGetServices
} from './referentiel.action';
import {catchError, mergeMap} from 'rxjs/operators';
import {ReferentielService} from '@services';
import {Actions, createEffect, ofType} from '@ngrx/effects';


@Injectable()
export class ReferentielEffect {

    loadReferentielListFromType$ = createEffect(() => this.actions$.pipe(
        ofType(getReferentiel),
        mergeMap((action) =>
            this.referentielService.getReferentielAsync(action.refType.toString()).pipe(mergeMap(data => {
                return [successGetReferentiel({
                    list: data,
                    refType: action.refType
                })];
            }), catchError((error) => [errorGetReferentiel({
                error,
                refType: action.refType
            })]))
        )));
    loadServicesListFromType$ = createEffect(() => this.actions$.pipe(
        ofType(getServices),
        mergeMap((action) =>
            this.referentielService.getServicesAsync().pipe(mergeMap(data => {
                return [successGetServices({
                    list: data
                })];
            }), catchError((error) => [errorGetServices({
                error
            })]))
        )));
    loadOrganismesListFromType$ = createEffect(() => this.actions$.pipe(
        ofType(getOrganismes),
        mergeMap((action) =>
            this.referentielService.getOrganismesAsync().pipe(mergeMap(data => {
                return [successGetOrganismes({
                    list: data
                })];
            }), catchError((error) => [errorGetOrganismes({
                error
            })]))
        )));

    constructor(
        private readonly actions$: Actions,
        private readonly referentielService: ReferentielService

    ) {
    }
}
