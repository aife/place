import {ActionReducerMap} from '@ngrx/store';
import * as fromReferentielReducer from 'app/store/referentiel/referentiel.reducer';

export interface State {
    referentielReducer: fromReferentielReducer.ReferentielState;
}

export const reducers: ActionReducerMap<State> = {
    referentielReducer: fromReferentielReducer.referentielReducer

};
