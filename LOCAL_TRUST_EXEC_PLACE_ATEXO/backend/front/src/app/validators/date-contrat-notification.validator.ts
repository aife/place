import { ContratDTO } from '@dto';
import { Validator } from './validator';
import { isBlank } from '@common/validation';

export class DateContratNotificationValidator implements Validator<ContratDTO> {

    validate(contrat: ContratDTO): Error[] {
        let errors: Error[] = [];

        if (contrat?.concession) {
            if (isBlank(contrat?.dateNotification)) {
                errors.push({ name: 'contrat.dateNotification', message: 'Date de signature'});
            }
        } else {
            if (isBlank(contrat?.dateNotification)) {
                errors.push({ name: 'contrat.dateNotification', message: 'Date réelle de notification du contrat'});
            }

            if (isBlank(contrat?.dateFinContrat)) {
                errors.push({ name: 'contrat.dateFinContrat', message: 'Date réelle de fin du contrat'});
            }

            if (isBlank(contrat?.dateMaxFinContrat)) {
                errors.push({ name: 'contrat.dateMaxFinContrat', message: 'Date réelle de fin maximale du contrat'});
            }
        }

        return errors;

    }
}
