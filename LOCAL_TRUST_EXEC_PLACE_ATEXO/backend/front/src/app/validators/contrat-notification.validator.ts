import { ContratDTO } from '@dto';
import { Validator } from './validator';
import { isBlank, isEmpty } from '@common/validation';

export class ContratNotificationValidator implements Validator<ContratDTO> {

    validate(contrat: ContratDTO): Error[] {
        let errors: Error[] = [];

        if (contrat.concession) {
            if (isBlank(contrat.natureContratConcession)) {
                errors.push({ name: 'contrat.natureContratConcession', message: 'Nature contrat concession'});
            }
            if (isBlank(contrat.objet)) {
                errors.push({ name: 'contrat.objet', message: 'Objet'});
            }
            if (isBlank(contrat?.consultation?.procedure)) {
                errors.push({ name: 'contrat.consultation.procedure', message: 'Procédure de passation'});
            }
            if (isBlank(contrat.dateDebutExecution)) {
                errors.push({ name: 'contrat.dateDebutExecution', message: `Date de début d'exécution`});
            }
            if (isBlank(contrat.valeurGlobale)) {
                errors.push({ name: 'contrat.valeurGlobale', message: 'Valeur globale'});
            }
            if (isBlank(contrat.montantSubventionPublique)) {
                errors.push({ name: 'contrat.montantSubventionPublique', message: 'Montant de la subvention publique'});
            }
        } else {
            if (isBlank(contrat.type)) {
                errors.push({ name: 'contrat.type', message: 'Type'});
            }
            if (isBlank(contrat.numero)) {
                errors.push({ name: 'contrat.numero', message: 'Numéro'});
            }
            if (isBlank(contrat.objet)) {
                errors.push({ name: 'contrat.objet', message: 'Objet'});
            }
            if (isBlank(contrat.service)) {
                errors.push({ name: 'contrat.service', message: 'Direction/Service'});
            }
            if (isBlank(contrat.createur)) {
                errors.push({ name: 'contrat.createur', message: 'Agent gestionnaire'});
            }
            if (isBlank(contrat.typeFormePrix)) {
                errors.push({ name: 'contrat.typeFormePrix', message: 'Forme de prix'});
            }
            if (isBlank(contrat.montant)) {
                errors.push({ name: 'contrat.montant', message: `Montant ${contrat?.typeFormePrix?.value === 'UNITAIRE' ? 'attribué' : 'forfaitaire initial'}` });
            }
            if (isBlank(contrat.dureeMaximaleMarche)) {
                errors.push({ name: 'contrat.dureeMaximaleMarche', message: 'Durée du marché (mois)'});
            }
            if (isEmpty(contrat.consultation.modaliteExecutions)) {
                errors.push({ name: 'contrat.consultation.modaliteExecutions', message: `Modalité(s) d'exécution`});
            }
            if (isEmpty(contrat.consultation.techniqueAchats)) {
                errors.push({ name: 'contrat.consultation.techniqueAchats', message: `Technique(s) d'achat`});
            }
            if (isBlank(contrat.offresRecues)) {
                errors.push({ name: 'contrat.offresRecues', message: `Nombre d'offres reçues`});
            }
            if (isBlank(contrat.consultation.procedure)) {
                errors.push({ name: 'contrat.consultation.procedure', message: 'Procédure de passation'});
            }
            if (isBlank(contrat.ccagApplicable)) {
                errors.push({ name: 'contrat.ccagApplicable', message: 'CCAG applicable'});
            }
            if ((contrat.groupement?.type?.value == 'SOLIDAIRE' || contrat.groupement?.type?.value == 'CONJOINT') && contrat.coTraitants.length <= 1) {
                errors.push({ name: 'contrat.coTraitants', message: 'Le contrat a un groupement solidaire ou conjoint mais aucun co-traitant de renseigné.'});
            }
        }

        return errors;
    }
}
