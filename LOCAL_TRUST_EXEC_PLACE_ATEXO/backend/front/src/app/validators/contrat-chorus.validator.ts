import { ContratDTO } from '@dto';
import { Validator } from './validator';
import { isBlank, isEmpty } from '@common/validation';

export class ContratChorusValidator implements Validator<ContratDTO> {

    validate(contrat: ContratDTO): Error[] {
        let errors: Error[] = [];

        if (isEmpty(contrat?.objet) ) {
            errors.push({ name: 'contrat.objet', message: 'Objet' });
        }

        if (isEmpty(contrat?.lieuxExecution)) {
            errors.push({ name: 'contrat.lieuxExecution', message: `Lieu d'exécution` });
        }

        if (isBlank(contrat?.categorie)) {
            errors.push({ name: 'contrat.categorie', message: 'Catégorie principal' });
        }

        if (isBlank(contrat?.cpvPrincipal)) {
            errors.push({ name: 'contrat.cpvPrincipal', message: 'CPV' });
        }

        if (isBlank(contrat?.montant)) {
            errors.push({ name: 'contrat.montant', message: 'Montant' });
        }

        if (isBlank(contrat?.typeFormePrix)) {
            errors.push({ name: 'contrat.typeFormePrix', message: 'Forme de prix' });
        }

        if (isBlank(contrat?.datePrevisionnelleNotification)) {
            errors.push({ name: 'contrat.datePrevisionnelleNotification', message: 'Date prévisionnelle de notification' });
        }

        if (isBlank(contrat?.datePrevisionnelleFinMarche)) {
            errors.push({ name: 'contrat.datePrevisionnelleFinMarche', message: 'Date prévisionnelle de fin de marché' });
        }

        if (isBlank(contrat?.datePrevisionnelleFinMaximaleMarche)) {
            errors.push({ name: 'contrat.datePrevisionnelleFinMaximaleMarche', message: 'Date prévisionnelle de fin maximale de marché' });
        }

        if (isBlank(contrat?.dureeMaximaleMarche)) {
            errors.push({ name: 'contrat.dureeMaximaleMarche', message: 'Durée (mois)' });
        }

        return errors;
    }
}
