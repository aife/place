import {Directive, ElementRef, forwardRef, HostListener, Renderer2} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import * as numeral from 'numeral';


@Directive({
    selector: '[money]',
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MoneyDirective), multi: true}
    ]
})
export class MoneyDirective implements ControlValueAccessor {

    propagateChange: any = () => {
    }
    @HostListener('blur')
    onTouched: any = () => {
    }

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    writeValue(obj: any): void {
        if (obj) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatMoney(obj));
        } else {
            this.renderer.setProperty(this.el.nativeElement, 'value', '');
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    @HostListener('change', ['$event.target.value'])
    onChanges(value: string) {
        const val = this.parseMoney(value);
        if (!isNaN(val) && val != null) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatMoney(val));
        }
        this.propagateChange(val);
    }

    formatMoney(inputMontant: number): string {
        return numeral(inputMontant).format('0,0.00');
    }

    parseMoney(inputString: string): any {
        const parsedMontant = inputString.replace(',', '.').replace(/\s+/g, '');
        if (parsedMontant == '') {
            return null;
        }
        return Number(parsedMontant);
    }
}
