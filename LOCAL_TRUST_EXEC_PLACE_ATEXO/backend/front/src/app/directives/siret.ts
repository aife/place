import {Directive, ElementRef, forwardRef, HostListener, Renderer2} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Directive({
    selector: '[siret]',
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SiretDirective), multi: true}
    ]
})
export class SiretDirective implements ControlValueAccessor {

    propagateChange: any = () => {
    }
    @HostListener('blur') onTouched: any = () => {
    }

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    writeValue(obj: any): void {
        if (obj) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatSiret(obj));
        } else {
            this.renderer.setProperty(this.el.nativeElement, 'value', '');
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    @HostListener('change', ['$event.target.value'])
    @HostListener('input', ['$event.target.value'])
    onChanges(value: string) {
        const val = this.parseSiret(value);
        if (val != null) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatSiret(val));
        }
        this.propagateChange(val);
    }

    parseSiret(inputSiret: string): string {
        return inputSiret.replace(/ /g, '').substring(0, 14);
    }

    formatSiret(inputSiret: string): string {
        return inputSiret.replace(/(.{3})(.{3})(.{3})(.{5})/, '$1 $2 $3 $4');
    }
}
