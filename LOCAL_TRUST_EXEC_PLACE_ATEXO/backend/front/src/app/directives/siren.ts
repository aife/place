import {Directive, ElementRef, forwardRef, HostListener, Renderer2} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Directive({
    selector: '[siren]',
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SirenDirective), multi: true}
    ]
})
export class SirenDirective implements ControlValueAccessor {

    propagateChange: any = () => {
    }
    @HostListener('blur')
    onTouched: any = () => {
    }

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    writeValue(obj: any): void {
        if (obj) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatSiren(obj));
        } else {
            this.renderer.setProperty(this.el.nativeElement, 'value', '');
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    @HostListener('change', ['$event.target.value'])
    onChanges(value: string) {
        const val = this.parseSiren(value);
        if (val != null) {
            this.renderer.setProperty(this.el.nativeElement, 'value', this.formatSiren(val));
        }
        this.propagateChange(val);
    }

    parseSiren(inputSiren: string): string {
        return inputSiren.replace(/ /g, '').substring(0, 9);
    }

    formatSiren(inputSiren: string): string {
        return inputSiren.replace(/(.{3})(.{3})(.{3})/, '$1 $2 $3');
    }
}
