import {USER_CONTEXT} from './userContext';

export function downloadUrl(url: string) {

    if (url.indexOf('?') > -1) {
        window.open(url + '&access_token=' + USER_CONTEXT.authentification.token, "_blank");
    } else {
        window.open(url + '?access_token=' + USER_CONTEXT.authentification.token, "_blank");
    }
}
