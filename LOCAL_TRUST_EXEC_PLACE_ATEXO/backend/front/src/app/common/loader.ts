export function showLoading() {
    $('#bloc-loader').show();
}

export function hideLoading() {
    $('#bloc-loader').fadeOut(100);
}



