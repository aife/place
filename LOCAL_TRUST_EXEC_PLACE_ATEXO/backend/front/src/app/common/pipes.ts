import {Injectable, Pipe, PipeTransform} from '@angular/core';
import * as numeral from 'numeral';
import * as moment from 'moment';
import {DomSanitizer} from '@angular/platform-browser';
import {DestinataireDTO, EmailDTO} from '@dto/messagerie-dto';
import {Store} from '@ngrx/store';
import {State} from '../store';
import {ContratDTO, ValueLabelDTO} from '@dto';
import {TypeReferentiel} from '@dto/backendEnum';

@Pipe({name: 'limitTo'})
@Injectable()
export class LimitToPipe implements PipeTransform {
    // FROM  http://scratch99.com/web-development/javascript/convert-bytes-to-mb-kb/
    transform(value: string, args: Array<any> = null): string {
        if (value == null) {
            return null;
        }

        const maxLength = args[0];
        if (value.length <= maxLength) {
            return value;
        }

        value = value.substring(0, maxLength);
        return value + '...';
    }
}

@Pipe({name: 'truncateMinMax', pure: true})
@Injectable()
export class TruncateMinMaxPipe implements PipeTransform {
    transform(value: string, minLength: number, maxLength: number): string {
        if (value == null) {
            return null;
        }
        if (value.length <= maxLength) {
            return value;
        }
        value = value.substr(minLength, maxLength);
        return value;
    }
}


@Pipe({name: 'fileSize'})
@Injectable()
export class FileSizePipe implements PipeTransform {
    // FROM  http://scratch99.com/web-development/javascript/convert-bytes-to-mb-kb/
    transform(value: number, args: Array<any> = null): string {
        return numeral(value).format('0.0 b');
    }
}

@Pipe({name: 'stringISODateToDate'})
@Injectable()
export class StringISODateToDatePipe implements PipeTransform {
    transform(value: string, args: Array<any> = null): Date {
        const date = null;
        try {
            if (value) {
                return moment(value).toDate();
            }
        } catch (e) {
        }
        return date;
    }
}

@Pipe({name: 'formatDate'})
@Injectable()
export class FormatDatePipe implements PipeTransform {
    transform(value: Date, args): string {
        let date = null;
        try {
            if (value) {
                const pattern: string = args != null ? args : 'DD/MM/YYYY';
                date = moment(value).format(pattern);
            }
        } catch (e) {
        }
        return date;
    }
}


@Pipe({name: 'currencyEuro'})
export class CurrencyEuroPipe implements PipeTransform {
    transform(value: number, args: Array<any> = null): string {
        if (value) {
            return numeral(value).format('0,0.00');
        }
        return '0,00';
    }
}

@Pipe({name: 'formatSiren'})
export class FormatSirenPipe implements PipeTransform {
    transform(value: string, args: Array<any> = null): string {
        value = value + '';
        if (value) {
            return value.replace(/(.{3})(.{3})(.{3})/, '$1 $2 $3');
        }
        return '0,00 €';
    }
}

@Pipe({name: 'replaceSpace'})
export class ReplaceSpacePipe implements PipeTransform {
    transform(value: string, args: Array<any> = null): string {
        value = value + '';
        if (value) {
            return value.replace('%20', ' ');
        }
        return '';
    }
}
@Pipe({name: 'replacePoint'})
export class ReplacePointPipe implements PipeTransform {
    transform(value: string, args: Array<any> = null): string {
        value = value + '';
        if (value) {
            return value.replace('.', ',');
        }
        return '';
    }
}

@Pipe({name: 'timestampToDate'})
@Injectable()
export class TimestampToDatePipe implements PipeTransform {
    transform(value: number, args: Array<any> = null): Date {
        let date = null;
        try {
            date = new Date(value);
        } catch (e) {
        }
        return date;
    }
}

@Pipe({name: 'delayToDate'})
@Injectable()
export class DelayToDatePipe implements PipeTransform {
    transform(value: Date, days: number): Date {
        if (value) {
            return moment(value).add(days, 'days').toDate();
        }
        return null;
    }
}

const momentConstructor: (value?: any) => moment.Moment = (<any>moment).default || moment;

@Pipe({ name: 'amDifference' })
export class DifferencePipe implements PipeTransform {
    transform(value: Date | moment.Moment,
              otherValue: Date | moment.Moment,
              unit?: any,
              precision?: boolean): number {

        const date = momentConstructor(value);
        const date2 = (otherValue !== null) ? momentConstructor(otherValue) : momentConstructor();

        return date.diff(date2, unit, precision);
    }
}


@Pipe({name: 'newlineToBreakline'})
@Injectable()
export class NewlineToBreaklinePipe implements PipeTransform {
    transform(value: string, args: Array<any> = null): string {
        if (value != null) {
            return value.replace(/(\r\n|\n|\r)/gm, '<br/>');
        }
        return null;
    }
}

@Pipe({
    name: 'stripHtml'
})

export class StripHtmlPipe implements PipeTransform {
    transform(value: string): any {
        return value.replace(/<.*?>/g, ''); // replace tags
    }
}

@Pipe({
    name: 'destinataireLabel'
})
export class DestinataireLabelPipe implements PipeTransform {
    transform(item: DestinataireDTO): any {
        let label = '';
        if (item.nomContactDest) {
            label = label += item.nomContactDest + ' | ';
        }
        if (item.nomEntrepriseDest) {
            label = label += item.nomEntrepriseDest + ' | ';
        }
        if (item.mailContactDestinataire) {
            label = label += item.mailContactDestinataire;
        }
        return label;
    }
}

@Pipe({
    name: 'emailLabel'
})
export class EmailLabelPipe implements PipeTransform {
    transform(item: EmailDTO): any {
        let label = '';
        if (item.nomContact) {
            label = label += item.nomContact + ' | ';
        }
        if (item.email) {
            label = label += item.email;
        }
        return label;
    }
}

@Pipe({name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform {
    constructor(private sanitized: DomSanitizer) {
    }

    transform(value) {
        return this.sanitized.bypassSecurityTrustHtml(value);
    }
}

@Pipe({name: 'surchargeLibelle'})
export class SurchargeLibellePipe implements PipeTransform {
    surchargeLibelleList: Array<ValueLabelDTO> = [];
    constructor(private store: Store<State>) {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.SURCHARGE_LIBELLE)?.content).subscribe(data => {
            this.surchargeLibelleList = data;
        });
    }

    transform(cle, valeurParDefaut) {
        const value = this.surchargeLibelleList?.find(vl => vl.value === cle);
        if (value) {
            return value.label;
        }
        return valeurParDefaut;
    }
}

@Pipe({name: 'cpv'})
export class CPVPipe implements PipeTransform {
    constructor(private _sanitizer: DomSanitizer) {
    }

    transform(contrat: ContratDTO) {
        if (contrat?.codesCPV) {
            for (const cpv of contrat.codesCPV) {
                cpv.principal = cpv.value === contrat?.cpvPrincipal?.value;
            }
        }
        const spans = contrat.codesCPV.map(cpv => `<span class="${cpv.principal ? 'cpv-principal' : ''}" title="${cpv.label}">${cpv.principal ? '<i class="fas fa-star"></i>' : ''}${cpv.value}</span>`).join(',');
        return this._sanitizer.bypassSecurityTrustHtml(spans);
    }
}

export let APPLICATION_PIPES = [DifferencePipe, LimitToPipe, TruncateMinMaxPipe, FileSizePipe, StringISODateToDatePipe,
    FormatDatePipe, CurrencyEuroPipe, FormatSirenPipe, ReplaceSpacePipe, TimestampToDatePipe, DelayToDatePipe, ReplacePointPipe,
    SafeHtmlPipe, EmailLabelPipe, DestinataireLabelPipe, StripHtmlPipe, NewlineToBreaklinePipe, SurchargeLibellePipe, CPVPipe];



