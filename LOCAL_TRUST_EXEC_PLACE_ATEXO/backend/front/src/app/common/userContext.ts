import {UtilisateurDTO} from '@dto';
import {Habilitation, PerimetreContrat} from '@dto/backendEnum';
import {Account} from '@services';

export enum Statut {
    KO, OK
}

export class Authentification {
    token?:  string;
    status: Statut = Statut.KO;
    message?: string;
    contexte?: any;
    date?: Date;
    url?: string;

    public isOK(): boolean {
        return this.status === Statut.OK;
    }

    public isKO(): boolean {
        return this.status === Statut.KO;
    }

    public ko(message?: string, contexte?: any) {
        this.status = Statut.KO;
        this.message = message;
        this.contexte = contexte;
        this.date = new Date();
        this.url = window.location.href;
    }
}


export class UserContext {
    authToken: string;
    authentification: Authentification = new Authentification();
    utilisateur: UtilisateurDTO;
    habilitations: String[] = [];
    account: Account;

    public hasHabilitation( habilitation: Habilitation ): boolean {
        return this.habilitations.indexOf( Habilitation[habilitation] ) > -1;
    }

    public isHabiliteExec(): boolean {
        return this?.utilisateur?.moduleExecActif;
    }

    getDefaultPerimetre(perimetre: string) {
        if (perimetre) {
            return PerimetreContrat[perimetre];
        }
        if (this.hasHabilitation(Habilitation.VOIR_TOUS_CONTRATS)) {
            return PerimetreContrat.TOUT;
        } else if (this.hasHabilitation(Habilitation.VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES)) {
            return PerimetreContrat.SERVICE_ET_SOUS_SERVICES;
        } else if (this.hasHabilitation(Habilitation.VOIR_CONTRATS_SERVICE)) {
            return PerimetreContrat.SERVICE;
        }

        return PerimetreContrat.MOI;
    }
}

export let USER_CONTEXT: UserContext = new UserContext();
