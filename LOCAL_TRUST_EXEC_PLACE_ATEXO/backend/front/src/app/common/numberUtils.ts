export function getMontantValid(value: any): string {

    if (value == null || isNaN(value)) {
        return null;
    }

    if (!(value instanceof String)) {
        value = value + '';
    }

    value = value.replace(/\s+/g, '');
    value = value.replace(',', '.');

    if (isNaN(value)) {
        return null;
    }

    return value;
}
