import * as moment from 'moment';
export function isoDateToDate(isoDateAsString: string) {
    if (isoDateAsString == null || isoDateAsString.trim().length === 0) {
        return null;
    }
    return new Date(isoDateAsString.trim());
}

export function formatIsoDate(date: Date, format: string): string {
    if (date == null) {
        return null;
    }
    return moment(date).format(format);
}

export function formatIsoDateFromDate(isoDateAsDate: Date, format: string): string {
    return moment(isoDateAsDate).format(format);
}

export function getDateFromInput(inputSelector: string, addTime: boolean): string {
    let dateAsString = $(inputSelector).val();
    if (dateAsString != null && (dateAsString + '').trim().length > 0) {
        dateAsString = moment((dateAsString + '').trim(), 'DD/MM/YYYY').format('YYYY-MM-DD');
        if (addTime) {
            const timeAsString = moment().format('HH:mm:ssZ');
            dateAsString = dateAsString + 'T' + timeAsString;
        }
        return dateAsString;

    } else {
        return null;
    }
}

export function isMoment(date: string | Date | moment.Moment): date is moment.Moment {
    return (<moment.Moment> date).toDate !== undefined;
}

export function momentToDate(date: string | Date | moment.Moment): string | Date {
    if (isMoment(date)) {
        return date.toDate();
    } else {
        return date;
    }
}

export function stringOrDateToDate(date: string | Date): Date {
    if (date instanceof Date) {
        return date;
    } else {
        return isoDateToDate(date);
    }
}

export function stringify(value: Date): string {
    return value ? moment(value).format('DD/MM/YYYY') : null;
}

export function datify(value: string): Date {
    return value ? moment(value, 'DD/MM/YYYY').toDate() : null;
}
