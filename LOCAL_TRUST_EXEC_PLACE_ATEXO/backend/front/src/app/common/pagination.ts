export interface PaginationPage<T> {
    content?: Array<T>;
    last?: boolean;
    first?: boolean;
    number: number;
    size: number;
    totalPages?: number;
    itemsPerPage?: number;
    totalElements?: number;
    numberOfElements?: number;
    order?: Array<PaginationPropertySort>;
}

export enum Order {
    ASC = 'ASC',
    DESC = 'DESC'
}

export interface PaginationPropertySort {
    direction: Order;
    property: string;
}
