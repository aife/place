import {Component, Directive, Host, Input} from '@angular/core';
import {NG_VALIDATORS, NgControl, NgForm} from '@angular/forms';
import * as NumberUtils from '../common/numberUtils';
import * as moment from 'moment';

/**
 * Money validation
 *
 * @param c
 * @returns {any}
 */
export function moneyValidator(c): { [key: string]: boolean } {
    let value = c.value;
    if (value === null || value === undefined) {
        return null;
    }
    value = NumberUtils.getMontantValid(value);
    if (value === null) {
        return {'invalidMoney': true};
    } else {
        return null;
    }
}
@Directive({
    selector: '[money]', providers: [
        {provide: NG_VALIDATORS, useValue: moneyValidator, multi: true}
    ]
})
export class MoneyValidator {
}

/**
 * Email Validation
 * @param c
 * @returns {any}
 */
export function emailValidator(email: NgControl): {[key: string]: boolean} {
    const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (email.value === null || email.value === undefined || email.value.length === 0) {
        return null;
    }
    if (!re.test(email.value)) {
        return {'invalidEmail': true};
    } else {
        return null;
    }
}
@Directive({
    selector: '[email][ngModel]', providers: [
        {provide: NG_VALIDATORS, useValue: emailValidator, multi: true}]
})
export class EmailValidator {
}

/**
 * URL Validator
 * @param url
 * @returns {any}
 */
export function urlValidator(url): {[key: string]: boolean} {
    const re = /^[^ "]+$/i;
    const value = url.value;
    if (value === null || value === undefined || value.length === 0) {
        return null;
    }
    if (value != null && !(re.test(value) )) {
        return {'invalidURL': true};
    }
    return null;

}
@Directive({
    selector: '[siteInternet]', providers: [
        {provide: NG_VALIDATORS, useValue: urlValidator, multi: true}
    ]
})
export class UrlValidator {
}

/**
 * Telephone validator
 * @param tel
 * @return {any}
 */
export function telephoneValidator(tel): {[key: string]: boolean} {
    const re = /^(0|\+33)[1-9][0-9]{8}$/i;
    const value = tel.value;
    if (value === null || value === undefined || value.length === 0) {
        return null;
    }
    if (value != null && (isNaN(value) || !(re.test(value)))) {
        return {'invalidPhoneNumber': true};
    }
    return null;
}
@Directive({
    selector: '[telephone]', providers: [
        {provide: NG_VALIDATORS, useValue: telephoneValidator, multi: true}
    ]
})
export class TelephoneValidator {
}

/**
 * Date Validation
 * @param c
 * @returns {any}
 */
export function dateValidator(c): { [key: string]: boolean } {
    const value = c.value;
    if (value === null || value === undefined || value.length === 0) {
        return null;
    }
    if (!moment(value, 'DD/MM/YYYY').isValid()) {
        return {'invalidDate': true};
    } else {
        return null;
    }
}
@Directive({
    selector: '[date]', providers: [
        {provide: NG_VALIDATORS, useValue: dateValidator, multi: true}
    ]
})
export class DateValidator {
}

/**
 * IsCodeEtablissement
 * @param obj
 * @returns {any}
 */
export function isCodeEtablissement(obj): {[key: string]: boolean} {
    const value = obj.value;
    if (!(value != null && value.length === 5) || isNaN(value)) {
        return {'invalidCodeEtablissement': true};
    } else {
        return null;
    }
}
@Directive({
    selector: '[etablissementCode]',
    providers: [
        {provide: NG_VALIDATORS, useValue: isCodeEtablissement, multi: true}
    ]
})
export class CodeEtablissementValidator {
}

export function isoDateValidator(c): { [key: string]: boolean } {
    const value = c.value;
    if (value == null || value.length === 0) {
        return null;
    }
    if (!moment(value, 'YYYY-MM-DD').isValid()) {
        return {'invalidIsoDate': true};
    } else {
        return null;
    }
}

@Directive({
    selector: '[isoDate]', providers: [
        {provide: NG_VALIDATORS, useValue: isoDateValidator, multi: true}
    ]
})
export class IsoDateValidator {

}

/**
 * Is Siren
 * @param siren
 * @returns {any}
 */
export function isSiren(siren): { [key: string]: boolean } {
    let estValide = null;
    let value = siren.value;

    if (!(value instanceof String)) {
        value = value + '';
        value = value.replace(/ /g, '');
    }

    if (!(value != null && value.length === 9) || isNaN(value)) {
        return {'invalidSirenNbChiffres': true};
    } else {
        // Donc le SIREN est un numérique à 9 chiffres
        let somme = 0;
        let tmp;
        for (let cpt = 0; cpt < value.length; cpt++) {
            if ((cpt % 2) === 1) { // Les positions paires : 2ème, 4ème, 6ème et 8ème chiffre
                tmp = value.charAt(cpt) * 2; // On le multiplie par 2
                if (tmp > 9) {
                    tmp -= 9;	// Si le résultat est supérieur à 9, on lui soustrait 9
                }
            } else {
                tmp = value.charAt(cpt);
            }
            somme += parseInt(tmp, 10);
        }
        if ((somme % 10) === 0) {
            estValide = null;	// Si la somme est un multiple de 10 alors le SIREN est valide
        } else {
            return {'invalidSiren': true};
        }
    }
    return estValide;
}

@Directive({
    selector: '[siren]', providers: [
        {provide: NG_VALIDATORS, useValue: isSiren, multi: true}
    ]
})
export class SirenValidator {
}

/**
 * Is Siret
 * @param siret
 * @returns {any}
 */
export function isSiretValid(siret): any {
    let value = siret.value;

    if (!(value instanceof String)) {
        value = value + '';
        value = value.replace(/ /g, '');
    }

    if (!(value != null && value.length === 14) || isNaN(value)) {
        return {'invalidSiretNbChiffres': true};
    } else {
        // Donc le SIRET est un numérique à 14 chiffres
        // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4 suivants
        // correspondent au numéro d'établissement
        // et enfin le dernier chiffre est une clef de LUHN.
        let somme = 0;
        let tmp;
        for (let cpt = 0; cpt < value.length; cpt++) {
            if ((cpt % 2) === 0) { // Les positions impaires : 1er, 3è, 5è, etc...
                tmp = value.charAt(cpt) * 2; // On le multiplie par 2
                if (tmp > 9) {
                    tmp -= 9;   // Si le résultat est supérieur à 9, on lui soustrait 9
                }
            } else {
                tmp = value.charAt(cpt);
            }
            somme += parseInt(tmp, 10);
        }
        if ((somme % 10) !== 0) {
            return {'invalidSiret': true};
        }
    }
}

@Directive({
    selector: '[siret]', providers: [
        {provide: NG_VALIDATORS, useValue: isSiretValid, multi: true}
    ]
})
export class SiretValidator {
}

@Component({
    selector: 'show-error',
    template: `
    <span  *ngIf="errorMessage !== null" class="help-block with-errors">{{errorMessage}}</span>
  `
})
export class ShowError {
    formDir;
    @Input('control') control: string;
    @Input('errors') errors: string[];

    constructor(@Host() formDir: NgForm) {
        this.formDir = formDir;
    }

    get errorMessage(): string {
        const control = this.formDir.controls[this.control];

        if (isPresent(control) && control.touched) {
            for (let i = 0; i < this.errors.length; ++i) {
                if (control.hasError(this.errors[i])) {
                    return this._errorMessage(this.errors[i]);
                }
            }
        }
        return null;
    }

    _errorMessage(code: string): string {
        const config = {
            'required': 'Ce champ est obligatoire',
            'invalidMoney': 'Le montant est invalide',
            'invalidDate': 'La date est invalide',
            'invalidIsoDate': 'La date est invalide',
            'invalidEmail': 'Le mail est invalide',
            'invalidCodeEtablissement': 'Le code doit contenir 5 chiffres',
            'invalidURL': 'L\'URL ne doit pas contenir d\'espaces.',
            'invalidPhoneNumber': 'Le numéro de téléphone est invalide',
            'invalidSirenNbChiffres': 'Le SIREN doit être composé de 9 chiffres',
            'invalidSiretNbChiffres': 'Le SIRET doit être composé de 14 chiffres',
            'invalidSiret': 'Le format du SIRET est invalide',
            'invalidSiren': 'Le format du SIREN est invalide',
            'max': 'Le montant max du sous-traitant est supérieur à celui du co-traitant.'
        };
        return config[code];
    }
}

export function isEmpty(field: any) {
    return isBlank(field) || field.length === 0;
}

export function isValidEmail(email: string): boolean {
    const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (!re.test(email)) {
        return false;
    } else {
        return true;
    }
}


export function isPresent(obj: any): boolean {
    return obj !== undefined && obj !== null;
}

export function isBlank(obj: any): boolean {
    return obj === undefined || obj === null;
}

export function isString(obj: any): boolean {
    return typeof obj === 'string';
}

export function isFunction(obj: any): boolean {
    return typeof obj === 'function';
}

export function isType(obj: any): boolean {
    return isFunction(obj);
}

export function isStringMap(obj: any): boolean {
    return typeof obj === 'object' && obj !== null;
}

export function isArray(obj: any): boolean {
    return Array.isArray(obj);
}

export function isNumber(obj): boolean {
    return typeof obj === 'number';
}

export function isDate(obj): boolean {
    return obj instanceof Date && !isNaN(obj.valueOf());
}

export let APPLICATION_VALIDATORS = [ShowError, DateValidator, IsoDateValidator, CodeEtablissementValidator,
    EmailValidator, TelephoneValidator, MoneyValidator, UrlValidator, SirenValidator, SiretValidator];
