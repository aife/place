import {Observable} from 'rxjs';
import {Subject} from 'rxjs/Subject';

export interface Parametrage {
    contact: {
        externe: {
            actif: boolean;
        }
    };
    contrat: {
        numero: {
            modifiable: boolean;
        },
        creation: {
            actif: boolean;
        },
        attributs: {
            visible: string;
        },
        statuts: {
            modifiables: string;
        }
    };
    fournisseur: {
        actif: boolean;
    };
    faq: {
        actif: boolean;
    };
    surcharge: {
        footer: boolean;
        header: boolean;
    };
    imports: {
        actif: boolean;
    };
    actes: {
        actif: boolean;
    };
    js: {
        footer: string;
        header: string;
    };
    edition: {
        document: {
            actif: boolean
        }
    };
    marches: {
        subsequents: {
            actif: boolean
        }
    };
    document: {
        libre: {
            taille: number
        }
    };
    projet: {
        achat: {
            actif: boolean
        }
    };
    ac_sad: {
        transversaux: {
            actif: boolean
        }
    };
    exec:{
        spaser: boolean;
    }
}

export class Application {
    public parametrage?: Parametrage;

    private state: Subject<boolean> = new Subject<boolean>();

    public loading(): Observable<boolean> {
        return this.state.asObservable();
    }

    public loaded(statut: boolean): boolean {
        this.state.next(statut);

        return statut;
    }
}


export let APPLICATION: Application = new Application();


