import {AfterViewInit, Component} from '@angular/core';
import {SurchargeService} from "@services";
import {APPLICATION} from "@common/parametrage";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {document} from "ngx-bootstrap/utils";

@Component( {
    selector: 'ltexec-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
} )
export class FooterComponent implements AfterViewInit {

    surchargeFooter = false;
    footerContent: SafeHtml

    constructor( private surchargeService: SurchargeService, private sanitizer: DomSanitizer ) {
    }

    ngAfterViewInit() {
        APPLICATION.loading().subscribe(
            _ => {
                this.surchargeFooter = APPLICATION?.parametrage?.surcharge?.footer;
                if ( this.surchargeFooter ) {
                    this.surchargeService.getFooter().subscribe( content => this.footerContent = this.sanitizer.bypassSecurityTrustHtml( content ) );
                }
                const js = APPLICATION.parametrage?.js?.footer;
                if (js) {
                    console.debug(`injection du js => ${js}`);
                    const jsNode = document.getElementById('js-footer') ?? document.createElement('script');
                    jsNode.id = "js-footer";
                    jsNode.type = 'text/javascript';
                    jsNode.innerHTML = js;
                    jsNode.text = js;
                    document.getElementById('footer').appendChild(jsNode);
                }
            }
        )


    }

}
