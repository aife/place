import {AfterViewInit, Component} from '@angular/core';
import {APPLICATION} from '@common/parametrage';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {SurchargeService} from '@services';
import {document} from 'ngx-bootstrap/utils';

@Component( {
    selector: 'ltexec-header',
    templateUrl: 'header.component.html',
    styleUrls: ['./header.component.scss']
} )
export class HeaderComponent implements AfterViewInit {

    surchargeHeader = false;
    headerContent: SafeHtml;

    constructor( private surchargeService: SurchargeService, private sanitizer: DomSanitizer ) {
    }

    ngAfterViewInit() {
        APPLICATION.loading().subscribe(
            _ => {
                this.surchargeHeader = APPLICATION.parametrage?.surcharge?.header;
                if ( this.surchargeHeader ) {
                    this.surchargeService.getHeader().subscribe( content => this.headerContent = this.sanitizer.bypassSecurityTrustHtml( content ) );
                }
                const js = APPLICATION.parametrage?.js?.header;
                if (js) {
                    console.debug(`injection du js => ${js}`);
                    const jsNode = document.getElementById('js-header') ?? document.createElement('script');
                    jsNode.id = "js-header";
                    jsNode.type = 'text/javascript';
                    jsNode.innerHTML = js;
                    jsNode.text = js;
                    document.head.appendChild(jsNode);
                }
            }
        )
    }
}
