import {AfterViewInit, Component, Input} from '@angular/core';
import {UtilisateurDTO} from "@dto/backend";


@Component({
    selector: 'ltexec-utilisateur-link',
    templateUrl: 'utilisateurLink.html'
})
export class UtilisateurLinkComponent implements AfterViewInit {

    static identifier_ = 0;

    @Input() utilisateur: UtilisateurDTO;

    identifier = 0;

    constructor() {
        UtilisateurLinkComponent.identifier_++;
        this.identifier = UtilisateurLinkComponent.identifier_;
    }

    ngAfterViewInit() {

        $(`#detail_agent_${this.identifier}`).popover({
            html: true,
            content: function () {
                const content = $(this).attr('id') + '_popover';
                return $('#' + content).children('.popover-body').html();
            },
            title: function () {
                const content = $(this).attr('id') + '_popover';
                return $('#' + content).children('.popover-heading').html();
            }

        }).on('click', function (e) {
            e.preventDefault();
        });
    }
}
