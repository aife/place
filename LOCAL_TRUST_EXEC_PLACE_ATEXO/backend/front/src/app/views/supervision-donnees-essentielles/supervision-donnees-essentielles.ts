import {Component, Injector, Input, OnInit} from '@angular/core';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {StatutPublicationDE, TypeReferentiel} from "@dto/backendEnum";
import {Store} from "@ngrx/store";
import {DonneesEssentiellesDTO, ValueLabelDTO} from "@dto";
import {State} from 'app/store';
import {Order, PaginationPage, PaginationPropertySort} from "@common/pagination";
import {defaultItemsCountPerPage} from "../../constants";
import {Observable} from "rxjs";
import {DonneesEssentiellesCriteriaDTO, DonneesEssentiellesService} from "@services/donnneesEssentielles.service";
import {ActivatedRoute} from "@angular/router";
import * as Notification from "@common/notification";
import {saveAs} from 'file-saver';

@Component({
    selector: 'ltexec-supervision-donnees-essentielles',
    templateUrl: './supervision-donnees-essentielles.html',
    styleUrls: ['./supervision-donnees-essentielles.scss']
})
export class SupervisionDonneesEssentiellesComponent extends AbstractViewComponent implements OnInit {

    typesContrat: ValueLabelDTO[];
    donneesEssentielles: PaginationPage<DonneesEssentiellesDTO>;
    sort: PaginationPropertySort[] = [{direction: Order.ASC, property: 'id'}];
    self: any;
    filtres: DonneesEssentiellesCriteriaDTO = {
        statutEnCours: false,
        statutAPublier: false,
        statutPublie: false,
        statutError: false,
        statutNonPublie: false
    };
    idsDonneesEssentielles: number[] = [];
    selectAllBoolean: boolean = false;
    @Input() habilitationRepublication: boolean;

    constructor(private injector: Injector, private store: Store<State>, private donneesEssentiellesService: DonneesEssentiellesService) {
        super(injector);
        this.self = this;
    }

    ngOnInit() {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_CONTRAT).content).subscribe(data => {
            this.typesContrat = data;
        });
        this.fetchPage(0, defaultItemsCountPerPage, this.sort);

    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any> {
        const observable: Observable<PaginationPage<DonneesEssentiellesDTO>> = this.donneesEssentiellesService.findDonneesEssentielles(this.filtres, pageNumber, pageSize, sort);
        observable.subscribe(donneesEssentielles => {
            this.donneesEssentielles = donneesEssentielles;
        });
        return observable;
    }

    changePageSize(page: PaginationPage<any>) {
        this.donneesEssentielles = page;
        this.selectAllBoolean = false;
        this.idsDonneesEssentielles = [];
    }

    statutEnCoursClick() {
        this.filtres.statutEnCours = !this.filtres.statutEnCours;
        this.calculateStatut();
        this.fetchPage(0, this.donneesEssentielles.size, this.sort);
    }

    statutPublieClick() {
        this.filtres.statutPublie = !this.filtres.statutPublie;
        this.calculateStatut();
        this.fetchPage(0, this.donneesEssentielles.size, this.sort);
    }

    statutAPublierClick() {
        this.filtres.statutAPublier = !this.filtres.statutAPublier;
        this.calculateStatut();
        this.fetchPage(0, this.donneesEssentielles.size, this.sort);
    }

    statutErrorClick() {
        this.filtres.statutError = !this.filtres.statutError;
        this.calculateStatut();
        this.fetchPage(0, this.donneesEssentielles.size, this.sort);
    }

    statutNonPublieClick() {
        this.filtres.statutNonPublie = !this.filtres.statutNonPublie;
        this.calculateStatut();
        this.fetchPage(0, this.donneesEssentielles.size, this.sort);
    }

    private calculateStatut() {
        const statuts = [];
        if (this.filtres.statutEnCours) {
            statuts.push(StatutPublicationDE.EN_COURS);
        }
        if (this.filtres.statutAPublier) {
            statuts.push(StatutPublicationDE.A_PUBLIER);
        }
        if (this.filtres.statutPublie) {
            statuts.push(StatutPublicationDE.PUBLIE);
        }
        if (this.filtres.statutError) {
            statuts.push(StatutPublicationDE.ERROR);
        }
        if (this.filtres.statutNonPublie) {
            statuts.push(StatutPublicationDE.NON_PUBLIE);
        }

        this.filtres.statuts = statuts;
    }

    selectDonneeEssentielle($event, idDonneeEssentielle: number) {
        if ($event?.target?.checked) {
            this.idsDonneesEssentielles.push(idDonneeEssentielle);
        } else {
            this.deleteElement(idDonneeEssentielle);
        }
    }

    deleteElement(element: number) {
        const index: number = this.idsDonneesEssentielles.indexOf(element);
        if (index !== -1) {
            this.idsDonneesEssentielles.splice(index, 1);
        }
    }

    republierDonneesEssentielles() {
        this.donneesEssentiellesService.updateStatut(this.idsDonneesEssentielles).subscribe(ok => {
            if (ok) {
                Notification.messageNotification('La mise à jour a réussi');
            } else {
                Notification.errorNotification('Une erreur est survenue');
            }
            this.fetchPage(0, defaultItemsCountPerPage, this.sort);
        });
    }

    selectAllDonneesEssentielles() {
        if (this.selectAllBoolean) {
            this.donneesEssentielles.content.forEach(e => this.idsDonneesEssentielles.push(e.idDonneeEssentielle));
        } else {
            this.idsDonneesEssentielles = [];
        }
    }

    telechargerJson(donneesEssentiellesDTO: DonneesEssentiellesDTO) {
        this.donneesEssentiellesService.telechargerFihcier(donneesEssentiellesDTO.idDonneeEssentielle).subscribe(blob => {
            saveAs(blob, (donneesEssentiellesDTO?.modeEnvoiDonneesEssentielles != null ? donneesEssentiellesDTO?.modeEnvoiDonneesEssentielles : "") + "-" + donneesEssentiellesDTO?.idDonneeEssentielle + ".json");
        });
    }
}

