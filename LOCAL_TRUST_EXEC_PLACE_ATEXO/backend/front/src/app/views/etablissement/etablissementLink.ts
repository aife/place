import {Component, ElementRef, Inject, Input} from '@angular/core';
import {ContactDTO, EtablissementDTO} from '@dto';
import {Router} from '@angular/router';

@Component( {
    selector: 'ltexec-etablissement-link',
    templateUrl: 'etablissementLink.html'
} )
export class EtablissementLinkComponent {
    static identifier_ = 0;
    @Input() etablissement: EtablissementDTO;
    contact: ContactDTO;
    elementRef: ElementRef;
    identifier = 0;
    isModaleFicheForunisseurShown = false;


    constructor( @Inject( ElementRef ) elementRef: ElementRef, private router: Router) {
        this.elementRef = elementRef;
        EtablissementLinkComponent.identifier_++;
        this.identifier = EtablissementLinkComponent.identifier_;
    }

    public ouvrirFicheFournisseur(uuid: string): void{
        this.isModaleFicheForunisseurShown = true;
    }

}
