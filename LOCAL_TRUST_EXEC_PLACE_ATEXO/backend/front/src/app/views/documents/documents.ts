import {AfterViewInit, Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {filter} from 'rxjs/operators/filter';
import {debounceTime} from 'rxjs/operators/debounceTime';
import {distinctUntilChanged} from 'rxjs/operators/distinctUntilChanged';
import {CalendrierService, ContratService, DocumentService, FaqService} from '@services';
import {ActivatedRoute, Router} from '@angular/router';
import {downloadUrl} from '@common/download';
import {messageNotification} from '@common/notification';
import {isPresent} from '@common/validation';
import {Table} from '@components/table/table';
import {
    CommentaireModel,
    ContratDTO,
    ContratEtablissementDTO,
    DocumentContratDTO,
    DocumentCriteriaDTO,
    EtablissementDTO,
    EvenementDTO,
    ValueLabelDTO
} from '../../external/backend';
import {DocumentContratTypeEnum} from '@dto/backendEnum';
import {APPLICATION} from '@common/parametrage';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {AbstractViewComponent} from '@views/abstract.view.component';

import * as $ from 'jquery';

export class BaseDocumentsComponent extends AbstractViewComponent {
    isModalCreationDocumentShown = false;
    documentsLibres: Array<DocumentContratDTO> = [];
    documentsGeneres: Array<DocumentContratDTO> = [];
    baseDocumentService: DocumentService;
    flag: boolean = false;
    showConfirmationSuppressionModal: boolean = false;


    constructor(injector: Injector) {
        super(injector);
        this.baseDocumentService = injector.get(DocumentService);
    }

    updateRepartitionDocuments(documents: Array<DocumentContratDTO>) {

        if (documents != null) {
            this.documentsLibres = documents.filter(document => document.documentType == 'LIBRE' || document.documentType == 'PIECE_JOINTE_MESSAGE');
            this.documentsGeneres = documents.filter(document => document.documentType == 'GENERE');
        } else {
            this.documentsLibres = [];
            this.documentsGeneres = [];
        }
    }

    onDownloadDocumentClick(documentId) {
        downloadUrl(this.baseDocumentService.getDownloadURL(documentId));
    }
}

@Component({
    selector: 'ltexec-documents',
    templateUrl: 'documents.html'
})
export class DocumentsComponent extends BaseDocumentsComponent implements AfterViewInit, Table, OnInit, OnDestroy {

    contratId: number;
    evenementId: number;

    contrat: ContratDTO;

    evenement: EvenementDTO;

    evenements: Array<EvenementDTO> = [];

    currentDocument: DocumentContratDTO;
    documentToDelete: DocumentContratDTO;
    defaultSort: PaginationPropertySort[] = [{property: 'dateModification', direction: Order.DESC}];

    documentsGeneresPage: PaginationPage<DocumentContratDTO> = {size: 10, number: 0, order: this.defaultSort};

    documentsLibresPage: PaginationPage<DocumentContratDTO> = {size: 10, number: 0, order: this.defaultSort};

    documentsGeneresCriteriaDTO: DocumentCriteriaDTO = {documentTypes: ['GENERE']};

    documentsLibresCriteriaDTO: DocumentCriteriaDTO = {documentTypes: ['LIBRE']};

    commentaireModel: CommentaireModel;

    etablissements: Array<EtablissementDTO> = [];

    documentsGeneresTable: Table;
    documentsLibresTable: Table;

    searchTerms = new Subject<string>();
    interval: any;
    editionEnLigneActivee = false;
    contratEtablissements: ContratEtablissementDTO[] = [];
    isModalCommentaireShown = false;
    private modalRef: BsModalRef;

    constructor(private contratService: ContratService, private calendrierService: CalendrierService,
                private injector: Injector,
                router: Router, private faqService: FaqService) {
        super(injector);
        this.documentsGeneresTable = this;

        this.documentsLibresTable = this;
        this.editionEnLigneActivee = APPLICATION.parametrage?.edition?.document?.actif;
    }

    ngOnInit() {
        const self = this;
        this.activatedRoute.params.subscribe(params => {

            this.contratId = params['contratId'];
            this.faqService.currentContratId = this.contratId;
            this.evenementId = params['evenementId'];
            let etablissementObservable: Observable<ContratEtablissementDTO[]> = null;

            this.documentsLibresCriteriaDTO = {
                motsCles: isPresent(params['motsCles']) ? params['motsCles'] : '',
                documentTypes: [DocumentContratTypeEnum[DocumentContratTypeEnum.LIBRE], DocumentContratTypeEnum[DocumentContratTypeEnum.PIECE_JOINTE_MESSAGE]]
            };

            this.documentsGeneresCriteriaDTO = {
                motsCles: isPresent(params['motsCles']) ? params['motsCles'] : '',
                documentTypes: [DocumentContratTypeEnum[DocumentContratTypeEnum.GENERE]]
            };

            if (this.contratId != null) {


                if (!!this.interval) {
                    clearInterval(this.interval);
                }
                this.contratService.findContratById(this.contratId).subscribe(value => {
                    this.contrat = value;
                })
                this.interval = setInterval(() => {
                    this.getDocumentsGeneres();
                }, 5000)
                etablissementObservable = this.contratService.findEtablissementsById(this.contratId);
            }
            if (this.evenementId != null) {
                etablissementObservable = this.contratService.findContratByEvenement(this.evenementId);

                this.documentsGeneresCriteriaDTO.evenements = [this.evenementId];
                this.documentsLibresCriteriaDTO.evenements = [this.evenementId];

                $('#collapseFiltresPlusLabel').attr('aria-expanded', 'true');
                $('#collapseFiltresPlusLabel').attr('class', '');

                $('#collapseFiltresPlus').attr('aria-expanded', 'true');
                $('#collapseFiltresPlus').attr('class', 'panel-collapse panel-indent collapse in');

            }
            etablissementObservable.subscribe(contratEtablissements => {

                this.contratEtablissements = contratEtablissements;
                if (this.contratId == null) {
                    this.contratId = this.contrat?.id;
                }
                const attributaire = 'Attributaires du contrat';
                const autre = 'Autres fournisseurs';
                contratEtablissements.forEach(ce => {
                    const etablissement = ce.etablissement;
                    if (ce.commanditaire == null) {
                        etablissement.type = attributaire;
                    } else {
                        etablissement.type = autre;
                    }
                    this.etablissements = this.etablissements.concat(etablissement);
                })
            });
            const evenementsObservable = this.calendrierService.findCalendrierByContrat(this.contratId);
            evenementsObservable.subscribe(evenements => {
                this.evenements = evenements;
                if (this.evenementId != null) {
                    this.evenement = evenements.find(e => e.id == this.evenementId);
                    this.evenements = [].concat(this.evenement)
                }
            });
            this.baseDocumentService.find(this.contratId, this.documentsGeneresCriteriaDTO, 0, null, this.defaultSort)
                .subscribe(documents => {
                    this.documentsGeneresPage = documents;
                    this.documentsGeneres = documents.content;

                }, e => {
                });

            this.baseDocumentService.find(this.contratId, this.documentsLibresCriteriaDTO, 0, null, null)
                .subscribe(documents => {
                    this.documentsLibresPage = documents;
                    this.documentsLibres = documents.content;

                }, _ => {
                });

            this.searchTerms.pipe(
                filter((term: string) => term.length === 0 || term.length > 2),
                debounceTime(300),
                distinctUntilChanged())
                .subscribe((term: string) => {
                    this.documentsGeneresCriteriaDTO.motsCles = term;
                    this.fetchPage(0, this.documentsGeneresPage.size,
                        this.documentsGeneresPage.order ? this.documentsGeneresPage.order : []);
                    this.documentsLibresCriteriaDTO.motsCles = term;
                    this.fetchPageDocumentsLibres(0, this.documentsLibresPage.size,
                        this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
                });
        });
    }


    private getDocumentsGeneres() {
        this.baseDocumentService.find(this.contratId, this.documentsGeneresCriteriaDTO, this.documentsGeneresPage?.number, this.documentsGeneresPage?.size, this.documentsGeneresPage?.order ? this.documentsGeneresPage?.order : this.defaultSort)
            .subscribe(documents => {
                this.documentsGeneresPage = documents;
                this.documentsGeneres = documents.content;

            }, e => {
            });
    }

    evenementsSelectionChanges(evenements: EvenementDTO[]) {
        const ids = evenements.map(e => e.id);
        this.documentsGeneresCriteriaDTO.evenements = ids;
        this.fetchPage(0, this.documentsGeneresPage.size,
            this.documentsGeneresPage.order ? this.documentsGeneresPage.order : []);
        this.documentsLibresCriteriaDTO.evenements = ids;
        this.fetchPageDocumentsLibres(0, this.documentsLibresPage.size,
            this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
    }

    typeEvenementSelectionChanges(typeEvenements: ValueLabelDTO[]) {
        const ids = typeEvenements.map(e => e.valueAsLong);
        this.documentsGeneresCriteriaDTO.typeEvenements = ids;
        this.fetchPage(0, this.documentsGeneresPage.size,
            this.documentsGeneresPage.order ? this.documentsGeneresPage.order : []);
        this.documentsLibresCriteriaDTO.typeEvenements = ids;
        this.fetchPageDocumentsLibres(0, this.documentsLibresPage.size,
            this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
    }

    contratantSelectionChanges(contractants: EtablissementDTO[]) {
        const ids = contractants.map(c => c.id);
        this.documentsGeneresCriteriaDTO.contractants = ids;
        this.fetchPage(0, this.documentsGeneresPage.size,
            this.documentsGeneresPage.order ? this.documentsGeneresPage.order : []);
        this.documentsLibresCriteriaDTO.contractants = ids;
        this.fetchPageDocumentsLibres(0, this.documentsLibresPage.size,
            this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
    }

    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        this.searchTerms.next(motsCles);
    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any> {
        const observable: Observable<PaginationPage<DocumentContratDTO>>
            = this.baseDocumentService.find(this.contratId, this.documentsGeneresCriteriaDTO, pageNumber, pageSize, sort);
        observable.subscribe(documents => {
            this.documentsGeneresPage = documents;
            this.documentsGeneres = documents.content;

        }, _ => {
        });
        return observable;
    }

    fetchPageDocumentsLibres(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any> {
        const observable: Observable<PaginationPage<DocumentContratDTO>>
            = this.baseDocumentService.find(this.contratId, this.documentsLibresCriteriaDTO, pageNumber, pageSize, sort);
        observable.subscribe(documents => {
            this.documentsLibresPage = documents;
            this.documentsLibres = documents.content;

        }, _ => {
        });
        return observable;
    }

    ngAfterViewInit() {

    }

    onAjouterDocumentLibreClick() {
        this.flag = true;
        this.isModalCreationDocumentShown = true;

    }

    onHidden(): void {
        this.isModalCreationDocumentShown = false;
        if (this.flag) {
            this.fetchPageDocumentsLibres(this.documentsLibresPage.number, this.documentsLibresPage.size,
                this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
        }
    }

    onAjouterDocumentGenereClick() {
        this.flag = false;
        this.isModalCreationDocumentShown = true;
    }


    setCurrentDocument(document) {
        this.currentDocument = document;
        this.commentaireModel = {commentaire: document.commentaire};
    }


    saveCommentaire() {

        this.baseDocumentService.saveCommentaire(this.currentDocument.id, this.commentaireModel.commentaire).subscribe((document) => {
            this.documentsLibresPage.content.forEach(doc => {
                if (doc.id === document.id) {
                    doc.commentaire = document.commentaire;
                }
            });
            this.documentsGeneresPage.content.forEach(doc => {
                if (doc.id === document.id) {
                    doc.commentaire = document.commentaire;
                }
            });
            this.currentDocument = document;
            this.isModalCommentaireShown = false;
        });
    }

    supprimerDocument(document: DocumentContratDTO) {
        this.baseDocumentService.deleteMessage(document.id).subscribe(() => {
            this.contrat.documentsCount--;
            if (document.documentType == DocumentContratTypeEnum[DocumentContratTypeEnum.GENERE]) {
                this.fetchPage(this.documentsGeneresPage.number, this.documentsGeneresPage.size,
                    this.documentsGeneresPage.order ? this.documentsGeneresPage.order : []);
            } else if (document.documentType == DocumentContratTypeEnum[DocumentContratTypeEnum.LIBRE]
                || document.documentType == DocumentContratTypeEnum[DocumentContratTypeEnum.PIECE_JOINTE_MESSAGE]) {
                this.fetchPageDocumentsLibres(this.documentsLibresPage.number, this.documentsLibresPage.size,
                    this.documentsLibresPage.order ? this.documentsLibresPage.order : []);
            }
            messageNotification(`Document '${document.fichier.nom}' supprimé`);
        }, () => {
        }, () => {
        });
        this.showConfirmationSuppressionModal = false;
    }

    onEditionEnLigneClick(documentId) {
        this.baseDocumentService.getUrlEditionOnlyOffice(documentId).subscribe(dto => {
            window.open(dto.value)

        })
    }

    ngOnDestroy() {
        if (!!this.interval) {
            clearInterval(this.interval);
        }
    }

    getTitle(document: DocumentContratDTO) {
        let title = "Lien du document";
        if (document.utilisateursEnLigne?.length > 0)
            title += ". En cours d'édition par : " + document.utilisateursEnLigne.join(", ")
        return title;

    }

    editerCommentaire() {
        this.isModalCommentaireShown = true;
    }

}
