import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {
    CreationDocumentGenereModel,
    DocumentContratDTO,
    DocumentModeleDTO,
    EtablissementDTO,
    EvenementDTO,
    FichierDTO
} from '@dto';
import {DocumentService, UploadService} from '@services';
import * as Dropzone from 'dropzone';
import {DropzoneFile} from 'dropzone';
import {webServiceEndpoint} from 'app/constants';
import {USER_CONTEXT} from '@common/userContext';
import {genericErrorNotification} from '@common/notification';
import {downloadUrl} from '@common/download';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {APPLICATION} from '@common/parametrage';
import {DropzoneConfigInterface, DropzoneDirective} from 'ngx-dropzone-wrapper';

@Component({
    selector: 'ltexec-creation-document',
    templateUrl: './creation-document.component.html',
    styleUrls: ['./creation-document.component.scss']
})
export class CreationDocumentComponent implements OnInit, AfterViewInit, OnDestroy {

    dropzone: Dropzone;

    creationDocumentLibreModel: CreationDocumentGenereModel = {};

    creationDocumentGenereModel: CreationDocumentGenereModel = {};

    modelesDocuments: Array<DocumentModeleDTO> = [];
    @Input() contratId: number
    @Input() evenements: Array<EvenementDTO> = [];
    @Input() etablissements: Array<EtablissementDTO> = [];
    @Output() documentCree = new EventEmitter<DocumentContratDTO[]>()
    @Output() hideModal = new EventEmitter()
    @ViewChild(DropzoneDirective, {static: false}) dropzdropzoneDirective: DropzoneDirective;
    options: DropzoneConfigInterface;

    constructor(public modalRef: BsModalRef, private uploadService: UploadService, private documentService: DocumentService, private modalservice: BsModalService) {
    }

    private _documentsLibres: boolean = false;

    get documentsLibres() {
        return this._documentsLibres;

    }

    @Input() set documentsLibres(value: boolean) {
        this._documentsLibres = value;
        if (value) {
            if (this.dropzone == null) {
                const self = this;
                setTimeout(function () {
                    self.renderDropZone(self);
                }, 500);
            } else {
                this.dropzone.enable();
            }
        }
    }

    renderDropZone(self) {
        self.options = {
            url: `${webServiceEndpoint}/upload/`,
            headers: {'Authorization': 'Bearer ' + USER_CONTEXT.authentification.token},
            dictDefaultMessage: 'Déposer les fichiers ici pour les joindre',
            dictFallbackMessage: 'Votre navigateur ne supports pas le drag\'n\'drop.',
            dictFileTooBig: 'La taille totale des pièces jointes associées au message dépasse la taille totale autorisée : {{maxFilesize}} Mo',
            dictResponseError: 'Le serveur a répond  avec le statut : {{statusCode}}.',
            dictCancelUpload: 'Annuler',
            dictCancelUploadConfirmation: 'Voulez vous vraiment annuler cet upload?',
            dictRemoveFile: '<i class=\'fa fa-trash\'></i>',
            dictRemoveFileConfirmation: null,
            dictInvalidFileType: 'You can not upload any more files.',
            dictMaxFilesExceeded: 'You can not upload any more files.',
            previewTemplate: '<div style="display:none"></div>',
            addRemoveLinks: false,
            paramName: 'uploadfile',
            maxFilesize: APPLICATION?.parametrage?.document?.libre?.taille ?? 4,
            maxThumbnailFilesize: 5,
            timeout: -1,
            uploadMultiple: false,
            maxFiles: 1,
        }
    }

    ngOnDestroy(): void {
        if (this.dropzone) {
            this.dropzone.disable();
        }
    }

    ngOnInit(): void {
        // Récupération des types d'événements depuis réferentielService (déjà chargé si tout va bien) vers Choosen
        this.documentService.findModeleDocument().subscribe(modelesDocuments => {
            this.modelesDocuments = modelesDocuments;
        });
    }

    ngAfterViewInit(): void {

    }

    isCreationDocumentGenereModelValid() {
        return this.creationDocumentGenereModel.modeleDocument != null
            && this.creationDocumentGenereModel.etablissement != null
            && this.creationDocumentGenereModel.objet != null
            && this.creationDocumentGenereModel.objet.length > 1;
    }

    isCreationDocumentLibreModelValid() {
        return this.creationDocumentLibreModel.etablissement != null
            && this.creationDocumentLibreModel.objet != null
            && this.creationDocumentLibreModel.objet.length > 1
            && this.creationDocumentLibreModel.fichier != null
            && this.creationDocumentLibreModel.fichier.reference != null;
    }

    createDocumentGenere() {

        const documentObservable =
            this.documentService.generateDocument(this.contratId, this.creationDocumentGenereModel);


        documentObservable.subscribe(document => {
            this.documentCree.emit();
            this.closeModal();
        }, _ => {

            genericErrorNotification();
        }, () => {

        });
    }

    createDocumentLibre() {

        const documentObservable =
            this.documentService.saveDocument(this.contratId, this.creationDocumentLibreModel);

        documentObservable.subscribe(document => {
            this.documentCree.emit([document]);
            this.closeModal();
        }, _ => {

        }, () => {

        });
    }

    deleteFichier(fichier: FichierDTO) {
        if (fichier.reference != null) {
            this.uploadService.delete(fichier.reference);
        }
        this.creationDocumentLibreModel.fichier = null;
        this.dropzone.removeAllFiles(true);
    }

    cancelDocumentCreationLibre() {
        if (this.creationDocumentLibreModel.fichier != null) {
            this.deleteFichier(this.creationDocumentLibreModel.fichier);
        }
        this.closeModal();
    }

    downlaodFichier(fichier: FichierDTO) {
        downloadUrl(this.documentService.getDownloadURL(fichier.id))
    }


    onAddedFile(file: DropzoneFile) {
        this.creationDocumentLibreModel.fichier = {nom: file.name, progress: 0};
    }

    onError = (files: DropzoneFile) => {
        let file = files[0];
        if (this.creationDocumentLibreModel.fichier == null) {
            this.creationDocumentLibreModel.fichier = {nom: file.name, taille: file.size, progress: 100};
        }
        const reponseServeur = file?.xhr?.response;
        const erreurDropZone = files[1];
        this.creationDocumentLibreModel.fichier.erreur = reponseServeur == null ? erreurDropZone : reponseServeur;
        this.creationDocumentLibreModel.fichier.progress = 100;
    }

    onSuccess(files: DropzoneFile) {
        let file = files[0];
        if (this.creationDocumentLibreModel.fichier != null) {
            this.creationDocumentLibreModel.fichier.reference = files[1];
        }
    }

    onUploadprogress(files: DropzoneFile) {
        let file = files[0];
        this.creationDocumentLibreModel.fichier.progress = 100;
        this.creationDocumentLibreModel.fichier.taille = file.size;
    }

    onQueuecomplete($event: any, dropzone: any) {
        this.dropzdropzoneDirective.reset(false);
    }

    closeModal() {
        this.hideModal.emit();
    }

}
