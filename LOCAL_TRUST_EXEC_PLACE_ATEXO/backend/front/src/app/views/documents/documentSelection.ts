import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DocumentService} from '@services';
import {DocumentContratDTO, DocumentCriteriaDTO} from '@dto';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Table} from '@components/table/table';
import {Observable} from 'rxjs';
import {downloadUrl} from '@common/download';
import {defaultItemsCountPerPage} from 'app/constants';

@Component({
    selector: 'ltexec-document-selection',
    templateUrl: 'documentSelection.html'
})

export class DocumentSelectionComponent implements Table, OnInit {

    @Output() selection: EventEmitter<DocumentContratDTO[]>;
    @Output() onClodeModal: EventEmitter<void> = new EventEmitter();
    @Input() contratId: number;
    documents: DocumentContratDTO[] = [];

    documentPage: PaginationPage<DocumentContratDTO> = {
        number: 0,
        size: defaultItemsCountPerPage,
        order: [{property: 'dateCreation', direction: Order.DESC}]
    };
    documentCriteria: DocumentCriteriaDTO = {};
    documentTable: Table;
    filtrerDocumentsGeneres = false;
    filtrerDocumentsLibres = false;

    constructor(private documentService: DocumentService) {
        this.selection = new EventEmitter();
    }

    ngOnInit(): void {
        this.documentTable = this;
        this.fetchPage(0, this.documentPage.size, this.documentPage.order);
    }

    fetchPage(pageNumber: number, pageSize: number, order: PaginationPropertySort[]): Observable<any> {
        this.documentPage.number = pageNumber;
        this.documentPage.size = pageSize;
        this.documentPage.order = order;
        const observable: Observable<any> = this.documentService.find(this.contratId, this.documentCriteria, this.documentPage.number, this.documentPage.size, this.documentPage.order ? this.documentPage.order : []);
        observable.subscribe(page => {
            this.documentPage = page;
            this.documents = page.content;
        })
        return observable;
    }

    onDownloadDocumentClick(documentId) {
        downloadUrl(this.documentService.getDownloadURL(documentId));
    }

    onAjouterDocuments() {
        const selectedDocuments = this.documents.filter(doc => doc.selected);
        selectedDocuments.forEach(doc => doc.preSelected = true);
        this.selection.emit(selectedDocuments);
        this.closeModal();
    }

    selectAll(check) {
        this.documents.forEach(doc => {
            doc.selected = check.target.checked;
        })
    }

    closeModal() {
        this.onClodeModal.emit();
    }

    filtrer(typeDoc: string) {
        const types = [];
        if (this.filtrerDocumentsGeneres) {
            types.push('GENERE');
        }
        if (this.filtrerDocumentsLibres) {
            types.push('LIBRE');
        }
        this.documentCriteria.documentTypes = types;
        this.fetchPage(0, this.documentPage.size, this.documentPage.order ? this.documentPage.order : []);

    }
}
