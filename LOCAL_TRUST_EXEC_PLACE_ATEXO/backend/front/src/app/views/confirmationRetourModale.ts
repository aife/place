import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

import * as $ from 'jquery';

@Component({
    selector: 'ltexec-confirmation-retour-modale',
    templateUrl: 'confirmationRetourModale.html'
})
export class ConfirmationRetourModaleComponent implements OnChanges, OnInit {
    @Output() onResolve: EventEmitter<boolean> = new EventEmitter();

    @Input() display: boolean;

    ngOnInit(): void {
        this.displayModale();
    }

    onAnnuler() {
        this.onResolve.emit(false);
    }

    onConfirm() {
        this.onResolve.emit(true);
    }

    ngOnChanges(): void {
        this.displayModale();
    }

    displayModale(): void {
        if (this.display) {
            $('.modal-confirmation-retour').modal('show');
        }
    }

}
