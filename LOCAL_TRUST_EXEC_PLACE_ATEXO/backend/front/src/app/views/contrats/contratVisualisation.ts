import { Component, OnInit } from '@angular/core';
import { ContratService } from '@services';
import { ActivatedRoute, Router } from '@angular/router';
import { ContratDTO } from '@dto';

export enum State {
    KO, OK
}

export interface Result {
    state: State;
    message: string;
    contrat?: ContratDTO;
}

@Component( {
    selector: 'ltexec-contrat-visualisation',
    templateUrl: 'contratVisualisation.html'
} )
export class ContratVisualisationComponent implements OnInit {

    public idExterne: string;
    public resultat: Result;

    constructor(
        private activatedRoute: ActivatedRoute,
        private contratService: ContratService,
        private router: Router
    ) {
    }

    public ngOnInit(): void {
        console.log( 'initialisation de la visualisation du contrat' );

        this.activatedRoute.params.subscribe( params => {
            this.idExterne = params['idExterne'];

            console.debug( `Recherche du contrat ${this.idExterne}` );

            this.contratService.findContratByIdExterne( this.idExterne ).subscribe(
                contrat => {
                    this.resultat = { state: State.OK, contrat: contrat, message: `<p class="text-success">Le contrat n<sup>o</sup>${this.idExterne} a bien été trouvé.</p>` };
                    console.debug( `Récupération du contrat ${contrat.id} depuis l'identifiant externe ${this.idExterne}` );

                    this.router.navigate( ['/tableauDeBord', contrat.id], { state: { contrat: contrat } } ).then( () => {
                        console.debug( 'Redirection vers le tableau de bord du contrat' );
                    } );
                },
                _ => this.resultat = {
                    state: State.KO,
                    message: `<p class="text-danger "><i class="fas fa-exclamation-triangle txt-undelivered m-r-10"></i>Le contrat n<sup>o</sup>${this.idExterne} n'a pas été trouvé.</p><p>Il est certainement actuellement en cours de synchronisation dans le module de gestion administrative des contrats.</p><p>Merci de réessayer ultérieurement.</p>`
                }
            )
        } );
    }
}
