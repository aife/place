import {Component, Injector, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {defaultItemsCountPerPage, FILTRE_CONTRAT, MOTS_CLES} from 'app/constants';
import {ContratCriteriaDTO, ContratService, FaqService} from '@services';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Table} from '@components/table/table';
import {ActivatedRoute} from '@angular/router';
import {MessagerieService} from '@services/messagerie.service';
import {isBlank, isPresent} from '@common/validation';
import {USER_CONTEXT} from '@common/userContext';
import {ContratDTO, ValueLabelDTO} from '@dto';
import {Habilitation, PerimetreContrat, StatutContrat, TypeReferentiel} from '@dto/backendEnum';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {Store} from '@ngrx/store';

import {State} from 'app/store';

@Component({
    selector: 'ltexec-contrats',
    templateUrl: 'contrats.html'
})
export class ContratsComponent extends AbstractViewComponent implements Table, OnInit {

    contrats: PaginationPage<ContratDTO>;
    showCustomEmptyMessage = false;

    @Input() filtres: ContratCriteriaDTO = {
        motsCles: '',
        statutANotifier: false,
        statutNotifie: false,
        statutEnCours: false,
        statutClos: false,
        statutArchive: false,
        favoris: false,
        transverses: false
    };
    @Input("rechercheavanceeouverte")
    rechercheavanceeouverte = false;

    sort: PaginationPropertySort[] =[{direction: Order.DESC, property: 'dateCreation'}];
    params: any;
    perimetre: string = null;
    monPerimetre = true;
    hideCheckBox = true;
    typesContrat: ValueLabelDTO[] = [];
    categories: ValueLabelDTO[] = []

    constructor(private contratService: ContratService,
                public messagerieService: MessagerieService,
                private faqService: FaqService,
                private store: Store<State>,
                private injector: Injector) {

        super(injector);
        this.messagerieService = messagerieService;
        this.contratService = contratService;
        this.contrats = {number: 0, size: defaultItemsCountPerPage};

    }

    ngOnInit() {
        if (this.params && isPresent(this.params['perimetre'])) {
            this.perimetre = this.params['perimetre'];
        }
        this.filtres.perimetre = USER_CONTEXT.getDefaultPerimetre(this.perimetre);
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_CONTRAT).content).subscribe(data => {
            this.typesContrat = data
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CATEGORIE).content).subscribe(data => {
            this.categories = data
        });
        this.hideCheckBox = this.hasPerimetre("SERVICE") || this.hasPerimetre("SERVICE_ET_SOUS_SERVICES") || this.hasPerimetre("TOUT");
        this.faqService.currentContratId = null;
        let filter = localStorage.getItem(FILTRE_CONTRAT);
        if (isPresent(filter)) {
            this.filtres = JSON.parse(filter);
        }

        this.activatedRoute.queryParams.subscribe(params => {

            this.params = params;

            if (this.params['motsCles']) {
                console.debug(`Récuperation des mots cles en paramètre dans l'URL`, this.params['motsCles']);
            }
            this.bindQueryParams();


            const pageParam = this.params['page'];
            const page_ = isBlank(pageParam) ? 0 : parseInt(pageParam, 10);

            const pageSizeParam = this.params['pageSize'];
            const pageSize_ = isBlank(pageSizeParam) ? defaultItemsCountPerPage : parseInt(pageSizeParam, 10);

            const propertyParam = this.params['property'];
            const directionParam = this.params['direction'];

            let sort: PaginationPropertySort[] = this.sort;
            if (!isBlank(propertyParam) && !isBlank(directionParam)) {
                sort = [{property: propertyParam, direction: directionParam}];
            }
            this.fetchPage(page_, pageSize_, sort, false);
        });
        this.mappingLabelTypeContrat();
    }

    hasPerimetre(perimetre: string) {
        switch (perimetre.toUpperCase()) {
            case 'SERVICE':
                return USER_CONTEXT.hasHabilitation(Habilitation.VOIR_CONTRATS_SERVICE);
            case 'SERVICE_ET_SOUS_SERVICES':
                return USER_CONTEXT.hasHabilitation(Habilitation.VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES);
            case 'TOUT':
                return USER_CONTEXT.hasHabilitation(Habilitation.VOIR_TOUS_CONTRATS);
            case 'MOI':
                return true;
            default:
                return false;
        }

    }

    public isPerimetreActive(perimetre: string) {
        return perimetre == PerimetreContrat[this.filtres.perimetre];
    }

    perimetreClick() {
        if (!this.hideCheckBox) {
            if (this.monPerimetre) {
                this.perimetre = "MOI";
            } else {
                this.perimetre = null;
            }
        }
        this.filtres.perimetre = PerimetreContrat[this.perimetre];
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    statutANotifierClick() {
        this.filtres.statutANotifier = !this.filtres.statutANotifier;
        this.calculateStatut();
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    statutNotifieClick() {
        this.filtres.statutNotifie = !this.filtres.statutNotifie;
        this.calculateStatut();
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    statutEnCoursClick() {
        this.filtres.statutEnCours = !this.filtres.statutEnCours;
        this.calculateStatut();
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    statutClosClick() {
        this.filtres.statutClos = !this.filtres.statutClos;
        this.calculateStatut();
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    statutArchiveClick() {
        this.filtres.statutArchive = !this.filtres.statutArchive;
        this.calculateStatut();
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    favorisClick() {
        this.filtres.favoris = !this.filtres.favoris;
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    transversesClick() {
        this.filtres.transverses = !this.filtres.transverses;
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    updateSearch() {
        this.storeFilter();
        this.fetchPage(0, this.contrats.size, this.sort, true);
    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[],
              pushState?: boolean): Observable<PaginationPage<ContratDTO>> {

        const observable: Observable<PaginationPage<ContratDTO>> =
            this.contratService.findContrats(this.filtres, pageNumber, pageSize, sort);

        observable.subscribe(contrats => {
            this.contrats = contrats;

            const idContrats = this.contrats.content.map((contrat) => contrat.id);

            const messageStatusObservable = this.messagerieService.countMessageStatusContrats(idContrats);
            messageStatusObservable.subscribe((messageStatus) => {
                for (let i = 0; i < this.contrats.content.length; i++) {
                    if (messageStatus[i].idContrat && this.contrats.content[i].id === parseInt(messageStatus[i].idContrat, 10)) {
                        this.contrats.content[i].messageStatus = messageStatus[i];
                    }
                }
            });
        }, _ => {
        });

        return observable;
    }


    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        if (motsCles.length === 0 || motsCles.length > 2) {
            this.filtres.motsCles = motsCles;
            this.storeFilter();
            this.fetchPage(0, this.contrats.size, null);
        }
        this.showCustomEmptyMessage = false;
    }



    storeFilter() {
        localStorage.setItem(FILTRE_CONTRAT, JSON.stringify(this.filtres));
    }

    checkPresent(param: string) {
        return this.params[param] == 'true'
    }

    onSelectContrat(contrat: ContratDTO) {
        localStorage.setItem(MOTS_CLES, contrat.referenceLibre);
        this.bindQueryParams();
    }

    private calculateStatut() {
        const statuts = [];
        if (this.filtres.statutANotifier) {
            statuts.push(StatutContrat.ANotifier);
        }
        if (this.filtres.statutNotifie) {
            statuts.push(StatutContrat.Notifie);
        }
        if (this.filtres.statutEnCours) {
            statuts.push(StatutContrat.EnCours);
        }
        if (this.filtres.statutClos) {
            statuts.push(StatutContrat.Clos);
        }
        if (this.filtres.statutArchive) {
            statuts.push(StatutContrat.Archive);
        }

        this.filtres.statuts = statuts;
    }

    private mappingLabelTypeContrat() {
        if (this.filtres.typesContrat) {
            this.filtres.typesContrat = this.filtres.typesContrat.map((t: ValueLabelDTO) => {
                t.label = this.findLabel(t.value);
                return t;
            });
        }
    }

    private findLabel(value) {
        const typeContrat = this.typesContrat.find(t => t.value.toUpperCase() === value.toUpperCase());
        return typeContrat ? typeContrat.label : "";
    }

    private bindQueryParams() {
        const motsCles = localStorage.getItem(MOTS_CLES);
        if ((!isPresent(this.filtres.motsCles) || this.filtres.motsCles === '') && isPresent(motsCles)) {
            this.filtres.motsCles = motsCles;
            this.showCustomEmptyMessage = true;
        }
        if (isPresent(this.params['motsCles'])) {
            if (this.params['filter'] === 'none') {
                this.filtres.motsCles = ''
            } else {
                this.filtres.motsCles = this.params['motsCles']
            }
            if (this.params['motsCles'] !== motsCles) {
                this.showCustomEmptyMessage = false;
            }
        }
        if (isPresent(this.params['statutANotifier'])) {
            this.filtres.statutANotifier = this.checkPresent('statutANotifier');
        }
        if (isPresent(this.params['statutNotifie'])) {
            this.filtres.statutNotifie = this.checkPresent('statutNotifie');
        }
        if (isPresent(this.params['statutEnCours'])) {
            this.filtres.statutEnCours = this.checkPresent('statutEnCours')
        }
        if (isPresent(this.params['statutClos'])) {
            this.filtres.statutClos = this.checkPresent('statutClos')
        }
        if (isPresent(this.params['statutArchive'])) {
            this.filtres.statutArchive = this.checkPresent('statutArchive')
        }
        if (isPresent(this.params['favoris'])) {
            this.filtres.favoris = this.checkPresent('favoris')
        }
        if (isPresent(this.params['transverse'])) {
            this.filtres.transverses = this.checkPresent('transverse')
        }
        if (isPresent(this.params['perimetre'])) {
            this.filtres.perimetre = PerimetreContrat[this.params['perimetre']];
            this.perimetre = this.params['perimetre'];
        }
        if (isPresent(this.params['chapeau'])) {
            this.filtres.chapeau = ('true' == this.params['chapeau'])
        }
        let typeContratList: Array<ValueLabelDTO>;
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_CONTRAT).content).subscribe(data => {
            typeContratList = data;
        });

        if (isPresent(this.params['type'])) {
            const codes = this.params['type'];
            let types: ValueLabelDTO[] = [];
            if (codes instanceof Array) {
                codes.forEach(c => types.push(typeContratList.find(type => type.value == c)));
            } else {
                types = typeContratList.filter(type => type.value == codes);
            }
            console.log(this.params['type'])
            this.filtres.typesContrat = types;
        }
    }
}
