import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {ClauseDto, ValueLabelDTO} from "@dto";
import {Store} from "@ngrx/store";
import {State} from "../../../store";
import {TypeReferentiel} from "@dto/backendEnum";
import {ReferentielService} from "@services";

@Component({
    selector: 'ltexec-affichage-clause',
    templateUrl: 'affichage-clause.html'
})
export class AffichageClauseComponent implements OnInit {

    @Input() clausesDto: ClauseDto[];
    @Input() isDisplay: boolean;
    clausesN1: ValueLabelDTO[];
    clausesN2: ValueLabelDTO[];
    clausesN3: ValueLabelDTO[];
    clausesN4: ValueLabelDTO[];
    listeN1: ValueLabelDTO[] = [];
    listeN2: ValueLabelDTO[] = [];
    clauseN2ModelMap: Map<string, ValueLabelDTO[]> = new Map<string, ValueLabelDTO[]>();
    @Output() editionModeEvent: EventEmitter<boolean>;

    constructor(private store: Store<State>, @Inject(ReferentielService) private referentielService: ReferentielService) {
    }

    ngOnInit(): void {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N1).content).subscribe(data => {
            this.clausesN1 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N2).content).subscribe(data => {
            this.clausesN2 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N3).content).subscribe(data => {
            this.clausesN3 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N4).content).subscribe(data => {
            this.clausesN4 = data;
        });
        this.prepareClauseToDisplay();
    }

    prepareClauseToDisplay() {
        this.prepareN1();
        this.prepareN2();
    }


    prepareN1() {
        this.clausesN1.forEach(e => {
            if (this.findByCode(e.value)) {
                this.listeN1.push({value: e.value, label: e.label});
            }
        })
    }


    prepareN2() {
        this.listeN1.forEach(e => {
            let clausesN2 = this.clausesN2.filter(element => element.groupeCode == e.value);
            clausesN2.forEach(clauseN2 => {
                if (this.findByCodeAndParent(clauseN2.value, e.value)) {
                    this.listeN2.push({value: clauseN2.value, label: clauseN2.label});
                }
            })
            this.clauseN2ModelMap.set(e.value, this.listeN2);
            this.listeN2 = [];
        });
    }

    findByCodeAndParent(code: string, parent: string) {
        return this.clausesDto.filter(e => e.code === code && e.parent.code === parent).length != 0;
    }

    findByCode(code: string) {
        return this.clausesDto.filter(e => e.code === code).length != 0;
    }

    getClauseN3FromN2(codeClauseN2) {
        return this.clausesDto.filter(e => this.clausesN3.find(c=>c.value == e.code))
            .filter(e => e.parent.code == codeClauseN2)
            .map(e => {
                let clause = this.clausesN3.find(el => el.value == e.code);
                return {value: e.code, label: clause.label, valeur: null}
            });
    }
    getClauseN4FromN2(codeClauseN2,codeClauseN3) {
        return this.clausesDto.filter(e =>  this.clausesN4.find(c=>c.value == e.code && c.groupeCode == codeClauseN3))
            .filter(e => e.parent.code == codeClauseN2)
            .map(e => {
                let clause = this.clausesN4.find(el => el.value == e.code);
                return {value: e.code, label: clause.label, valeur: e.valeur}
            });
    }
}
