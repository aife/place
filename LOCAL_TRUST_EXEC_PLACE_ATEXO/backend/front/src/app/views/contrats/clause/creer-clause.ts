import {Component, Inject, Input, OnInit} from '@angular/core';
import {ClauseDto, ValueLabelDTO} from "@dto";
import {Store} from "@ngrx/store";
import {State} from "../../../store";
import {TypeReferentiel} from "@dto/backendEnum";
import {ReferentielService} from "@services";
import * as Notification from "@common/notification";

@Component({
    selector: 'ltexec-creer-clause',
    templateUrl: 'creer-clause.html'
})
export class CreerClauseComponent implements OnInit {

    clausesN1: ValueLabelDTO[];
    clausesN2: ValueLabelDTO[];
    clausesN3: ValueLabelDTO[];
    clausesN4: ValueLabelDTO[];
    clausesN2N3: ValueLabelDTO[];
    referentielClauseN2: ValueLabelDTO[];
    clauseOption: Map<string, ValueLabelDTO[]> = new Map<string, ValueLabelDTO[]>();
    clauseN1ModelMap: Map<string, boolean> = new Map<string, boolean>();
    clauseN2ModelMap: Map<string, boolean> = new Map<string, boolean>();
    clauseN3ModelMap: Map<string, string[]> = new Map<string, string[]>();
    clauseN4ModelMap: Map<string, string> = new Map<string, string>();
    clausesToSave: ClauseDto[];
    @Input() isSubmitted: boolean;
    @Input() editMode: boolean;
    @Input() clausesDto: ClauseDto[];
    @Input() typeContrat: string;


    constructor(private store: Store<State>, @Inject(ReferentielService) private referentielService: ReferentielService) {
    }

    ngOnInit(): void {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N1).content).subscribe(data => {
            this.clausesN1 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N2).content).subscribe(data => {
            this.clausesN2 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N3).content).subscribe(data => {
            this.clausesN3 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N4).content).subscribe(data => {
            this.clausesN4 = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CLAUSE_N2N3).content).subscribe(data => {
            this.clausesN2N3 = data;
        });
        if (this.editMode) {
            this.prepareEditClause();
        }
    }

    getClauseN2FromN1(codeClauseN1) {
        return this.clausesN2.filter(element => element.groupeCode == codeClauseN1);
    }

    getClauseN4FromN3(codeClauseN3, codeClauseN2) {
        return this.clausesN4.filter(element => element.groupeCode == codeClauseN3 &&
            this.checkClauseN4hasParent(element.value, codeClauseN2)
        );
    }

    checkIfN3HasN4(arr: string[], code: string) {
        if (arr == undefined) {
            return false;
        }
        return arr.filter(el => el == code).length != 0;
    }

    prepareClausesToSave() {
        this.clausesToSave = [];
        this.prepareClauseN1();
        this.prepareAutresClause();
        return this.clausesToSave;
    }

    prepareClauseN1() {
        this.clauseN1ModelMap.forEach((value: boolean, key: string) => {
            if (value == true) {
                this.clausesToSave.push({code: key, valeur: null, parent: null});
            }
        });
    }

    prepareAutresClause() {
        this.clauseN2ModelMap.forEach((value: boolean, key: string) => {
            if (value) {
                const keys: string[] = key.split('/');
                this.clausesToSave.push({
                    code: keys[1],
                    valeur: null,
                    parent: {code: keys[0], valeur: null, parent: null}
                });
                let listClauseN3 = this.getSelectedClauseN3(keys[1]);
                this.prepareClauseN3(listClauseN3, keys[0], keys[1])
            }
        });
    }

    prepareClauseN3(listClauseN3: string[], clauseN1: string, clauseN2: string) {
        listClauseN3.forEach(el => {
                this.clausesToSave.push({
                    code: el,
                    valeur: null,
                    parent: {code: clauseN2, valeur: null, parent: null}
                })
                let listClauseN4 = this.getClauseN4FromN3(el, clauseN2);
                this.prepareClauseN4(listClauseN4, clauseN1, clauseN2, el)
            }
        )
    }

    prepareClauseN4(listClauseN4: ValueLabelDTO[], clauseN1: string, clauseN2: string, clauseN3: string) {
        listClauseN4.forEach(el => {
                this.clausesToSave.push({
                    code: el.value,
                    valeur: this.clauseN4ModelMap.get(clauseN1 + '/' + clauseN2 + '/' + el.value),
                    parent: {code: clauseN2, valeur: null, parent: null}
                })
            }
        )
    }

    getSelectedClauseN3(code: string) {
        let arr: string[] = [];
        this.clauseN3ModelMap.forEach((value: string[], key: string) => {
            const keys: string[] = key.split('/');
            if (keys[1] == code) {
                value.map(el => arr.push(el))
            }
        });
        return arr;
    }

    validerClause(): boolean {
        let verif = true;
        for (const [key, value] of this.clauseN1ModelMap) {
            if (value) {
                if (this.validerN2(key)) {
                    if (!this.validerN3(key)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return verif;
    }

    validerN2(clauseN1) {
        if (!this.checkIfOneClauseN2IsSelected(this.getClauseN2FromN1(clauseN1), clauseN1)) {
            Notification.errorNotification('Sélectionnez au moins une Clause');
            return false;
        }
        return true;
    }

    validerN3(clauseN1) {
        let selectedClauseN2 = this.clausesN2.filter(e => this.clauseN2ModelMap.get(clauseN1 + '/' + e.value) && this.checkIfN3ExistForN2(e.value));
        for (var clauseN2 of selectedClauseN2) {
            let selectedClauseN3 = this.clauseN3ModelMap.get(clauseN1 + '/' + clauseN2.value);
            if (!selectedClauseN3) {
                Notification.errorNotification('Sélectionnez au moins une Clause');
                return false;
            } else if (selectedClauseN3.length == 0) {
                Notification.errorNotification('Sélectionnez au moins une Clause');
                return false;
            }
        }
        return true;
    }

    validerN4() {
        console.log(this.clauseN4ModelMap);
        let clauseN3 = "insertionActiviteEconomique";
        let clauseN4 = "heureInsertionActiviteEconomique";
        for (const [key, value] of this.clauseN3ModelMap) {
            let exist = value.find(e => e === clauseN3);
            if (exist) {
                let selectedClauseN4 = this.clauseN4ModelMap.get(key + clauseN4);
                if (!selectedClauseN4) {
                    Notification.errorNotification("Remplir Le nombre d'heures");
                    return false;
                }
            }
        }
        return true;
    }

    checkIfOneClauseN2IsSelected(clausesN2: ValueLabelDTO[], clauseN1: string): boolean {
        return clausesN2.filter(e => this.clauseN2ModelMap.get(clauseN1 + '/' + e.value)).length > 0;
    }

    prepareEditClausesN1() {
        this.clausesN1.forEach(e => {
            if (this.findByCode(e.value)) {
                this.clauseN1ModelMap.set(e.value, true);
            }
        })
    }

    prepareEditClausesN2() {
        this.clausesDto.filter(e => this.clausesN2.find(c => c.value == e.code))
            .map(e => {
                this.clauseN2ModelMap.set(e.parent.code + '/' + e.code, true);
                this.getClauseN3FromN2(e.code);
            });
    }

    prepareEditClausesN3() {
        this.clausesDto.filter(e => this.clausesN3.find(c => c.value == e.code))
            .map(e => {
                if (this.clauseN3ModelMap.get(e?.parent?.parent.code + '/' + e?.parent?.code)) {
                    let listN3 = this.clauseN3ModelMap.get(e?.parent?.parent.code + '/' + e?.parent?.code);
                    listN3.push(e.code);
                    this.clauseN3ModelMap.set(e?.parent?.parent.code + '/' + e?.parent?.code, listN3);
                } else {
                    this.clauseN3ModelMap.set(e?.parent?.parent.code + '/' + e?.parent?.code, [e.code]);
                }
            });
    }

    prepareEditClausesN4() {
        this.clausesDto.filter(e => this.clausesN4.find(c => c.value == e.code))
            .map(e => {
                this.clauseN4ModelMap.set(e.parent.parent.code + '/' + e.parent.code + '/' + e.code, e.valeur);
            });
    }

    prepareEditClause() {
        this.prepareEditClausesN1();
        this.prepareEditClausesN2();
        this.prepareEditClausesN3();
        this.prepareEditClausesN4();
    }

    checkIfOneClauseN3IsSelected(clausesN2: ValueLabelDTO[]): boolean {
        return clausesN2.filter(e => this.clauseN2ModelMap.get(e.value)).length > 0;
    }

    findByCode(code: string) {
        return this.clausesDto.filter(e => e.code === code).length != 0;
    }

    getClauseN3FromN2(codeClauseN2: string): void {
        const clausesN3FromN2 = this.clausesN2N3.filter(e => e.parents.find(p => p.value === codeClauseN2));
        this.clauseOption.set(codeClauseN2, clausesN3FromN2);
    }

    checkIfN3ExistForN2(clauseN2) {
        return this.clausesN2N3.filter(e => e.parents.find(p => p.value === clauseN2)).length != 0;
    }

    checkClauseN4hasParent(codeClause: string, parent: string) {
        return this.clausesN4
            .filter(e => e.value == codeClause)
            .filter(element => element.parents.find(e => e.value === parent))
            .length != 0;
    }
}
