import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {isPresent} from '@common/validation';
import {ContratService, MessagerieService} from '@services';
import {USER_CONTEXT} from '@common/userContext';
import {ContratDTO} from '@dto';
import {Habilitation} from '@dto/backendEnum';
import {APPLICATION} from '@common/parametrage';
import {ActivatedRoute, Router} from "@angular/router";

@Component( {
    selector: 'ltexec-contrat-infos-references',
    templateUrl: 'contratInfos-references.html'
} )
export class ContratInfosReferencesComponent {
    @Input() contrat: ContratDTO;
    @Output() onSelect = new EventEmitter<ContratDTO>();
}


@Component( {
    selector: 'ltexec-contrat-infos-description',
    templateUrl: 'contratInfos-description.html'
} )
export class ContratInfosDescriptionComponent implements AfterViewInit, OnInit {
    @Input() contrat: ContratDTO;
    @Input() updateMessagesStatus = true;
    @Input() attributaires = true;
    @Input() documents = true;
    @Input() messages = true;
    @Input() calendrier = true;
    @Input() actes = true;
    contratsLies: ContratDTO[] = [];
    gestionActes: boolean = false;
    isHabiliteExec = false;

    constructor(private activatedRoute: ActivatedRoute, private messagerieService: MessagerieService, private contratService: ContratService,private router: Router ) {
        this.isHabiliteExec = USER_CONTEXT.isHabiliteExec();
    }

    public ngOnInit(): void {
        this.gestionActes = APPLICATION.parametrage?.actes?.actif;
    }

    public ngAfterViewInit(): void {
        if ( isPresent( this.contrat ) && isPresent( this.contrat.id ) && this.updateMessagesStatus ) {
            const tokenObservable = this.messagerieService.initTokenSuivi( this.contrat.id );
            tokenObservable.subscribe( contexte => {
                this.countMessageStatus( contexte?.token );
            } );
        }
        if (this.contrat.chapeau) {
            this.contratService.getContratsLies(this.contrat.id).subscribe(contratsLies => {
                this.contratsLies = contratsLies;
            });
        }
    }

    countMessageStatus( token ) {
        const _countMessageStatusObservable = this.messagerieService.countMessageStatus( token, this.contrat.id + '' );
        _countMessageStatusObservable.subscribe( ( messageStatusDTO ) => {
            this.contrat.messageStatus = messageStatusDTO;
        }, error => {
            console.error( 'Erreur lors de la récupération des messages', error );
        } );
    }

    currentUrl() {
        return this.router.url;
    }
}


@Component( {
    selector: 'ltexec-contrat-infos-dates',
    templateUrl: 'contratInfos-dates.html'
} )
export class ContratInfosDatesComponent {
    @Input() contrat: ContratDTO;

    tousContrats: boolean = USER_CONTEXT.hasHabilitation( Habilitation.VOIR_TOUS_CONTRATS );
}


@Component( {
    selector: 'ltexec-contrat-infos-montants',
    templateUrl: 'contratInfos-montants.html'
} )
export class ContratInfosMontantsComponent {
    @Input() contrat: ContratDTO;

    tousContrats: boolean = USER_CONTEXT.hasHabilitation( Habilitation.VOIR_TOUS_CONTRATS );
}


export let CONTRAT_INFOS_DIRECTIVES: Array<any> = [
    ContratInfosReferencesComponent,
    ContratInfosDescriptionComponent,
    ContratInfosDatesComponent,
    ContratInfosMontantsComponent
];
