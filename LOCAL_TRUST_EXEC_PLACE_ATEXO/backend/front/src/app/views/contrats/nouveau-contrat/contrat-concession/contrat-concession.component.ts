import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {Order, PaginationPropertySort} from '@common/pagination';
import {ContratDTO, EtablissementDTO, OrganismeDTO, ServiceDTO, UtilisateurDTO, ValueLabelDTO} from '@dto';
import {EtablissementService, OrganismeService, ServiceService, UtilisateurService} from '@services';
import * as Notification from '@common/notification';
import {messageNotification} from '@common/notification';
import {Observable} from 'rxjs/Observable';
import {NgSelectConfig} from '@ng-select/ng-select';
import {NgForm} from '@angular/forms';
import {Habilitation} from '@dto/backendEnum';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {USER_CONTEXT} from '@common/userContext';
import {APPLICATION} from '@common/parametrage';
import {merge} from 'rxjs/observable/merge';
import {AbstractFicheContrat} from '@views/contrats/nouveau-contrat/AbstractFicheContrat';

declare var $: any;

@Component({
    selector: 'ltexec-saisie-contrat-concession',
    templateUrl: './contrat-concession.component.html',
    styleUrls: ['../nouveau-contrat.component.scss']
})
export class ContratConcessionComponent extends AbstractFicheContrat implements OnInit {


    @Input() isCreation: boolean;
    page: number = 0;
    @Input() masquerNavigation = false;
    contrats: Array<ContratDTO> = [];
    sort: PaginationPropertySort[] = [{direction: Order.DESC, property: 'dateCreation'}];
    organismes: Array<OrganismeDTO> = [];
    utilisateur: UtilisateurDTO;
    utilisateurs: UtilisateurDTO[] = [];
    typeContratOptions: Array<ValueLabelDTO> = [];
    procedureOptions: Array<ValueLabelDTO> = [];
    modalitesExecutionOptions: Array<ValueLabelDTO> = [];
    formePrixOptions: Array<ValueLabelDTO> = [];
    typePrixOptions: Array<ValueLabelDTO> = [];
    ccagOptions: Array<ValueLabelDTO> = [];
    codesCpv: ValueLabelDTO[] = [];
    lieuxExecution: ValueLabelDTO[] = [];
    natureContratConcessionOptions: ValueLabelDTO[] = [];
    services: ServiceDTO[] = [];
    categorieConsultationOptions: Array<ValueLabelDTO> = [];
    raisonSociale: string;
    disabledDE: boolean = false;
    dateFinReelleOK: boolean = true;
    dateFinMaxReelleOK: boolean = true;
    termLieu: string;
    isUtilisateurChorus: boolean = false;
    showAlerteDate: boolean = true;
    //pour l'écran fiche contrat ou édition contrat
    editionMode = false;
    modifiable: boolean = USER_CONTEXT.hasHabilitation(Habilitation.MODIF_DONNEES_CONTRAT);
    @Output() onExport: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() onSave: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() editionModeEvent: EventEmitter<boolean>;
    numContratModif = false;
    premierContratNotifie = false;
    parametrageProjetAchat: boolean = true;
    procedureOptionsFiltred: Array<ValueLabelDTO> = [];
    dateExecutionOk: boolean = true;
    private readonly _procedureConcession = ['PNO', 'PNNO', 'PNR', 'PNNR'];


    constructor(injector: Injector, private etablissementService: EtablissementService,
                private organismeService: OrganismeService, private serviceSercice: ServiceService,
                private utilisateurService: UtilisateurService, private config: NgSelectConfig) {
        super(injector);
        this.editionModeEvent = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.loadReferentiel();
        this.parametrageProjetAchat = APPLICATION.parametrage?.projet?.achat?.actif;
        this.config.notFoundText = "Aucun résultat";
        this.organismeService.getOrganismes().subscribe(organismes => this.organismes = organismes);
        if (this.isCreation) {
            this.dureeMarche = 0;
            this.initContrat();
        } else {
            if (!!this.contrat.dateFinContrat && !!this.contrat.dateNotification) {
                this.dureeMarche = Math.ceil(moment(this.contrat.dateFinContrat).diff(moment(this.contrat.dateNotification), 'months', true));
            } else {
                this.dureeMarche = 0;
            }
            if (!!this.contrat.idLienAcSad) {
                this.contratService.findContratById(this.contrat.idLienAcSad).subscribe(contrat => {
                    this.lienAcSad = contrat;
                });
            }
        }

        this.serviceSercice.getServices().subscribe(services => {
            this.services = services;
        });

        this.utilisateur = USER_CONTEXT.utilisateur;
        this.isUtilisateurChorus = this.utilisateur?.service?.echangesChorus;
        this.contrat.service = this.utilisateur.service;
        this.contrat.createur = this.utilisateur;
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['idFournisseur']) {
                console.log("params ", params['idFournisseur']);
                this.etablissementService.findEtablissementById(params['idFournisseur']).subscribe(etablissement => {
                    this.contrat.attributaire.etablissement = etablissement;
                });
            }
        });
        this.onChangeTypeContrat();
    }


    getUtilisateurs(serviceId: any): Observable<Array<UtilisateurDTO>> {
        const utilisateursObservable = this.utilisateurService.getUtilisateursByServiceId(serviceId);
        utilisateursObservable.subscribe(utilisateurs => {
            this.utilisateurs = utilisateurs;
        });
        return utilisateursObservable;
    }




    cancelClick() {
        this.router.navigate(['/']);
    }


    getDateEnvoiAlerte() {
        return (this.contrat.dateMaxFinContrat && this.contrat.dureePhaseEtudeRenouvellement)
            ? moment(this.contrat.dateMaxFinContrat).subtract(this.contrat.dureePhaseEtudeRenouvellement, "months").toDate()
            : null;
    }


    checkDateFinPrevisionnelle() {
        if (!!this.contrat.datePrevisionnelleFinMarche && !!this.contrat.datePrevisionnelleNotification) {
            this.dateFinPrevisionnelleOK = this.contrat.datePrevisionnelleFinMarche > this.contrat.datePrevisionnelleNotification;
        }
    }

    checkDateFinMaxPrevisionnelle() {
        if (!!this.contrat.datePrevisionnelleFinMaximaleMarche && !!this.contrat.datePrevisionnelleFinMarche) {
            this.dateFinMaxPrevisionnelleOK = this.contrat.datePrevisionnelleFinMaximaleMarche > this.contrat.datePrevisionnelleFinMarche;
        }
    }

    checkDateReelleFinContrat() {
        if (!!this.contrat.dateFinContrat && !!this.contrat.dateNotification) {
            this.dateFinReelleOK = this.contrat.dateFinContrat > this.contrat.dateNotification;
        }
    }

    checkDateReelleFinMaxContrat() {
        this.updateShowAlert();
        if (!!this.contrat.dateMaxFinContrat && !!this.contrat.dateFinContrat) {
            console.log(this.contrat.dateMaxFinContrat, this.contrat.dateFinContrat);
            this.dateFinMaxReelleOK = this.contrat.dateMaxFinContrat >= this.contrat.dateFinContrat;
        }
    }

    getBindLabelLieu() {
        return isNaN(Number(this.termLieu?.charAt(0))) ? "label" : "value";
    }

    updateDureeMarche() {
        this.dureeMarche = 0;
        if (!!this.contrat.dateMaxFinContrat && !!this.contrat.dateNotification) {
            this.dureeMarche = Math.ceil(moment(this.contrat.dateMaxFinContrat).diff(moment(this.contrat.dateNotification), 'months', true));
        }
    }


    public update(etablissement: EtablissementDTO): void {
        this.contrat.attributaire.etablissement = etablissement;
    }

    updateShowAlert() {
        if (this.contrat.dateMaxFinContrat && this.contrat.dureePhaseEtudeRenouvellement) {
            let date = moment(this.contrat.dateMaxFinContrat).subtract(this.contrat.dureePhaseEtudeRenouvellement, "months").toDate();
            let now = new Date();
            this.showAlerteDate = date > now;
        }
    }


    creerMarcheSubsequent() {
        this.contratService.creerMarcheSubsequent(this.contrat);
    }

    peutCreerMarcheSubsequentOuOrganismes() {
        return this.contratService.peutCreerOrganismesOuMarcheSubsequent(this.contrat);
    }

    exporter() {
        this.onExport.emit(this.contrat);
    }

    favContrat(contrat: ContratDTO) {
        this.contratService.toggleFavoris(contrat.id).subscribe((_) => contrat.favori = !contrat.favori);
    }

    editContrat() {
        this.editionMode = true;
        const datesToConvert = [
            'dateNotification',
            'dateFinContrat',
            'dateMaxFinContrat',
            'datePrevisionnelleFinMaximaleMarche',
            'datePrevisionnelleFinMarche',
            'datePrevisionnelleNotification',
            'dateExecution'
        ];
        datesToConvert.forEach(dateField => {
            if (this.contrat[dateField]) {
                this.contrat[dateField] = new Date(this.contrat[dateField]);
            }
        });


        const servicesObservable = this.serviceSercice.getServices();
        APPLICATION.loading().subscribe(
            _ => this.numContratModif = APPLICATION.parametrage?.contrat?.numero?.modifiable
        );
        servicesObservable.subscribe(services => {
            this.services = services;
        });

        const utilisateursObservable = this.getUtilisateurs(this.contrat.service.id);


        merge(servicesObservable, utilisateursObservable).subscribe(() => {

        }, () => {
            this.editionMode = true;
            this.editionModeEvent.emit(true);
        });
        this.bindCPVPrincipal();
    }

    getValeurMultiple(propriete: string, proprieteMultiple: string) {
        let objects = this.getValeur(propriete) as Array<any>;
        if (objects && proprieteMultiple) {
            return objects.map(o => o[proprieteMultiple]).join(", ");
        }
        return null;
    }

    getValeur(propriete: string) {
        if (propriete) {
            let objet: Object = this.contrat;
            for (let prop of propriete.split('.')) {
                objet = objet[prop]
                if (objet == null) {
                    return null;
                }
                if (objet && objet.hasOwnProperty(prop)) {
                    return objet[prop];
                }
            }
            if (propriete === 'ccagApplicable') {
                return objet['label']
            }
            return objet;
        }
        return null;
    }

    cancelEditContrat() {
        this.editionMode = false;
        this.editionModeEvent.emit(false);
        this.getContrat();
    }

    getContrat() {
        if (this.contrat) {
            this.contratService.findContratById(this.contrat.id).subscribe(contrat => {
                this.contrat = contrat;
                this.bindCPVPrincipal();
            });
        }
    }

    valider(form: NgForm) {
        this.isSubmitted = true;
        if (!this.contrat.attributionAvance) {
            this.contrat.tauxAvance = 0;
        }
        this.checkTauxAvance();
        this.contrat.dureeMaximaleMarche = this.dureeMarche;
        if (form.form.valid && this.dateFinPrevisionnelleOK && this.dateFinMaxPrevisionnelleOK && this.isOKAvance && this.creerClauseComponent.validerClause()) {
            this.contrat.clausesDto = this.creerClauseComponent.prepareClausesToSave();
            this.contratService.updateDonneesPrincipales(this.contrat).subscribe(contrat => {
                this.contrat = contrat;
                this.cancelEditContrat();
                messageNotification('Informations du contrat modifiées.');
                this.isSubmitted = false;
                this.onSave.emit(contrat);
            }, _ => {

            }, () => {

            });
        } else {
            Notification.errorNotification('Le formulaire est invalide.');
        }
    }

    onChangeTypeContrat() {
        this.updateProcedure();
    }

    updateProcedure() {
        this.procedureOptionsFiltred = this.procedureOptions.filter(procedure => this._procedureConcession.find(e => e == procedure.value))
    }

    checkDateExecutionContrat() {
        this.dateExecutionOk = this.contrat?.dateDebutExecution > this.contrat?.dateNotification;
    }

    validerAjoutContrat(form: NgForm) {
        this.isSubmitted = true;
        if (!!this.lienAcSad) {
            this.contrat.idLienAcSad = this.lienAcSad.id
        }
        if (!this.contrat.attributionAvance) {
            this.contrat.tauxAvance = 0;
        }
        this.checkTauxAvance();
        this.contrat.dureeMaximaleMarche = this.dureeMarche;
        if (!!this.contrat.attributaire.etablissement && form.form.valid && this.dateFinPrevisionnelleOK && this.dateFinMaxPrevisionnelleOK && this.isOKAvance && this.creerClauseComponent.validerClause()) {
            this.contrat.clausesDto = this.creerClauseComponent.prepareClausesToSave();
            this.contratService.ajouterContrat(this.contrat).subscribe(contratDTO => {
                Notification.messageNotification('Le contrat n°' + contratDTO.referenceLibre + ' a été créé');
                this.router.navigate(['/ficheContrat', contratDTO.id]);
            });
        } else {
            Notification.errorNotification('Le formulaire est invalide.');
        }

    }
}

