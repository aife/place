import {Component, Injector, Input, OnInit} from '@angular/core';
import {ContratDTO, OrganismeDTO, ServiceDTO} from '@dto';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {USER_CONTEXT} from '@common/userContext';

declare var $: any;

@Component({
    selector: 'ltexec-contrat-entite-eligible',
    templateUrl: './contrat-entite-eligible.component.html',
    styleUrls: ['./contrat-entite-eligible.component.scss']
})
export class ContratEntiteEligibleComponent extends AbstractViewComponent implements OnInit {

    @Input() set contrat(contrat: ContratDTO) {
        this._contrat = contrat;
        if (this._contrat && this._contrat.servicesEligibles && this._contrat.servicesEligibles.length > 0) {
            this.selectOption = 'services';
            this.selectedServicesOptions = this._contrat.servicesEligibles;
        } else if (this._contrat && this._contrat.organismesEligibles && this._contrat.organismesEligibles.length > 0) {
            this.selectOption = 'organismes';
            this.selectedOrganismesOptions = this._contrat.organismesEligibles;
        } else {
            this.selectOption = 'defaut';
        }
        if (this._contrat)
            this.loadReferentiel();
    }

    @Input() editionMode = true;
    @Input() isCreation = true;


    get contrat(): ContratDTO {
        return this._contrat;
    }

    private _contrat: ContratDTO = {} as ContratDTO;
    organismesOptions: Array<OrganismeDTO> = [];
    userOrganisme: OrganismeDTO = null;
    servicesOptions: Array<ServiceDTO> = [];

    isModaleEntiteEligibleShown: boolean = false;

    selectOption = 'defaut';
    confirmedOption = 'defaut';
    selectedServicesOptions: ServiceDTO[] = [];
    selectedOrganismesOptions: OrganismeDTO[] = [];

    constructor(private injector: Injector, private store: Store<State>) {
        super(injector);
    }

    ngOnInit() {
    }

    loadReferentiel() {

        this.store.select(state => state.referentielReducer.organismes.content).subscribe(data => {
            this.organismesOptions = JSON.parse(JSON.stringify(data));
            this.userOrganisme = this.organismesOptions.filter(organisme => USER_CONTEXT.utilisateur.organismeId === organisme.id).pop();
            if (this.selectOption === 'organismes') {
                this.selectedOrganismesOptions = this.organismesOptions.filter(organisme => this._contrat.organismesEligibles.map(organismeEligible => organismeEligible.id).includes(organisme.id));
                this.organismesOptions.forEach(organisme => {
                    if (this.selectedOrganismesOptions.map(organismeEligible => organismeEligible.id).includes(organisme.id)) {
                        organisme.selected = true;
                    }
                });
            }

        });
        this.store.select(state => state.referentielReducer.services.content).subscribe(data => {
            this.servicesOptions = JSON.parse(JSON.stringify(data));
            if (this.selectOption === 'services') {
                this.selectedServicesOptions = this.servicesOptions.filter(service => this._contrat.servicesEligibles.map(serviceEligible => serviceEligible.id).includes(service.id));
                this.setServicesSelected(this.servicesOptions);
            }
        });
    }

    setServicesSelected(services: ServiceDTO[]) {
        services.forEach(service => {
            if (this.selectedServicesOptions.map(serviceEligible => serviceEligible.id).includes(service.id)) {
                service.selected = true;
            }
            if (service.services?.length > 0) {
                this.setServicesSelected(service.services);
            }
        });
    }

    getLibelleEntiteEligible(option: string) {
        if (option === 'services') {
            return 'Les agents invités permanents sur le service de rattachement de ce contrat, et les agents rattachés aux Directions / Services suivants';
        }
        if (option === 'organismes') {
            return 'Les agents rattachés aux Entités publiques / Organismes suivants';
        }
        return 'Les agents invités permanents sur le service de rattachement de ce contrat (périmètre de visibilité par défaut)';
    }

    updateContrat() {
        this.confirmedOption = this.selectOption;
        if (this.confirmedOption === 'defaut') {
            this.contrat.organismesEligibles = [];
            this.contrat.servicesEligibles = [];
        } else if (this.confirmedOption === 'services') {
            this.contrat.organismesEligibles = [];
            this.contrat.servicesEligibles = this.selectedServicesOptions;
        } else {
            this.contrat.servicesEligibles = [];
            this.contrat.organismesEligibles = this.selectedOrganismesOptions;
        }
    }

    updateOrganismesEligibles() {
        this.selectOption = 'organismes';
        this.selectedOrganismesOptions = this.organismesOptions.filter(value => value.selected);
    }

    updateServicesEligibles(service: ServiceDTO) {
        this.selectOption = 'services';
        if (service.selected) {
            this.selectedServicesOptions.push(service);
        } else {
            this.selectedServicesOptions = this.selectedServicesOptions.filter(value => value.id !== service.id);
        }
    }
}

