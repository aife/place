import {Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ContratDTO, EtablissementDTO, OrganismeDTO, ServiceDTO, ValueLabelDTO} from '@dto';
import {ContratService, EtablissementService} from '@services';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {TypeReferentiel} from '@dto/backendEnum';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {ActivatedRoute} from '@angular/router';
import {APPLICATION} from '@common/parametrage';
import {ContratComponent} from '@views/contrats/nouveau-contrat/contrat/contrat.component';

declare var $: any;

@Component({
    selector: 'ltexec-fiche-contrat-nouveau',
    templateUrl: './nouveau-contrat.component.html',
    styleUrls: ['./nouveau-contrat.component.scss']
})
export class NouveauContratComponent extends AbstractViewComponent implements OnInit {

    @Input() isCreation: boolean;
    @Input() contrat: ContratDTO = {};
    @Input() masquerNavigation = false;
    @Output() onExport: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() onSave: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() editionModeEvent: EventEmitter<boolean>;
    typeContratOptions: Array<ValueLabelDTO> = [];
    organismesOptions: Array<OrganismeDTO> = [];
    userOrganisme: OrganismeDTO = null;
    servicesOptions: Array<ServiceDTO> = [];
    parametrageProjetAchat: boolean = true;
    isConcession: boolean = false;
    isModaleEntiteEligibleShown: boolean = false;
    @ViewChild(ContratComponent) contratComponent: ContratComponent;
    etablissements: Array<EtablissementDTO> = [{id: null}];
    isMultiattributaire: boolean = false;
    selectOption = 'defaut';
    confirmedOption = 'defaut';
    selectedServicesOptions: ServiceDTO[] = [];
    selectedOrganismesOptions: OrganismeDTO[] = [];

    constructor(private injector: Injector, private etablissementService: EtablissementService, private store: Store<State>, private contratService: ContratService) {
        super(injector);
        this.editionModeEvent = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.loadReferentiel();
        this.parametrageProjetAchat = APPLICATION.parametrage?.projet?.achat?.actif;
        this.isCreation = !this.contrat?.id;
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['idFournisseur']) {
                this.etablissementService.findEtablissementById(params['idFournisseur']).subscribe(etablissement => {
                    this.contrat.attributaire.etablissement = etablissement;
                });
            }
        });
        this.calculateType();
    }

    public update(etablissement: EtablissementDTO): void {
        this.contrat.attributaire.etablissement = etablissement;
    }

    onSelectAttributaire(etablissement: EtablissementDTO, index: number) {
        this.contrat.attributaire = {};
        this.contrat.attributaire.etablissement = etablissement;
        this.etablissements[index] = etablissement == null ? {} : etablissement;
    }

    loadReferentiel() {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_CONTRAT).content).subscribe(data => {
            this.typeContratOptions = data;
        });
    }

    exporter() {
        this.onExport.emit(this.contrat);
    }

    onBlocEditionMode(editMode: boolean) {
        this.editionModeEvent.emit(editMode);
    }

    calculateType() {
        this.isConcession = this.contratService.isConcession(this.contrat);
        if (!this.isConcession) {
            this.contratComponent?.onChangeTypeContrat();
        }
        this.calculIsMultiAttributaire();
        if (!this.isMultiattributaire) {
            this.etablissements = [{id: null}];
        }
    }

    ajoutAttributaire() {
        this.etablissements.push({id: null});
    }

    supprimerAttributaire(index) {
        this.etablissements.splice(index, 1);
    }

    calculIsMultiAttributaire() {
        this.isMultiattributaire = ['sad', 'amu', 'marmu'].indexOf(this.contrat?.type?.value) != -1;
    }

    isEtablissementSelected() {
        //Au minimum un établissement est selectionné
        return this.etablissements.filter(e => e?.id != null).length > 0;
    }

    deleteEmptyElementFromEtablissements() {
        return this.etablissements.filter(e => e?.id != null);
    }

    getLibelleEntiteEligible(option: string) {
        if (option === 'services') {
            return 'Les agents invités permanents sur le service de rattachement de ce contrat, et les agents rattachés aux Directions / Services suivants';
        }
        if (option === 'organismes') {
            return 'Les agents rattachés aux Entités publiques / Organismes suivants';
        }
        return 'Les agents invités permanents sur le service de rattachement de ce contrat (périmètre de visibilité par défaut)';
    }

    updateContrat() {
        this.confirmedOption = this.selectOption;
        if (this.confirmedOption === 'defaut') {
            this.contrat.organismesEligibles = [];
            this.contrat.servicesEligibles = [];
        } else if (this.confirmedOption === 'services') {
            this.contrat.organismesEligibles = [];
            this.contrat.servicesEligibles = this.selectedServicesOptions;
        } else {
            this.contrat.servicesEligibles = [];
            this.contrat.organismesEligibles = this.selectedOrganismesOptions;
        }
    }

    updateOrganismesEligibles() {
        this.selectOption = 'organismes';
        this.selectedOrganismesOptions = this.organismesOptions.filter(value => value.selected);
    }

    updateServicesEligibles(service: ServiceDTO) {
        this.selectOption = 'services';
        if (service.selected) {
            this.selectedServicesOptions.push(service);
        } else {
            this.selectedServicesOptions = this.selectedServicesOptions.filter(value => value.id !== service.id);
        }
    }
}

