import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {Order, PaginationPropertySort} from '@common/pagination';
import {ContratDTO, EtablissementDTO, OrganismeDTO, ServiceDTO, UtilisateurDTO, ValueLabelDTO} from '@dto';
import {
    EtablissementService,
    OrganismeService,
    ParametrageService,
    ServiceService,
    UtilisateurService
} from '@services';
import * as Notification from '@common/notification';
import {messageNotification} from '@common/notification';
import {Observable} from 'rxjs/Observable';
import {NgSelectConfig} from '@ng-select/ng-select';
import {NgForm} from '@angular/forms';
import {FormePrix, Habilitation, PerimetreContrat, TypeBorne} from '@dto/backendEnum';
import * as moment from 'moment';
import {USER_CONTEXT} from '@common/userContext';
import {APPLICATION} from '@common/parametrage';
import {merge} from 'rxjs/observable/merge';
import {AbstractFicheContrat} from '@views/contrats/nouveau-contrat/AbstractFicheContrat';

declare var $: any;

@Component({
    selector: 'ltexec-saisie-contrat',
    templateUrl: './contrat.component.html',
    styleUrls: ['../nouveau-contrat.component.scss']
})
export class ContratComponent extends AbstractFicheContrat implements OnInit {

    @Input() isCreation: boolean;
    @Input() masquerNavigation = false;
    @Output() onExport: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() onSave: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();
    @Output() editionModeEvent: EventEmitter<boolean>;
    @Input() etablissements: Array<EtablissementDTO>;
    page: number = 0;
    contrats: Array<ContratDTO> = [];
    @Input()
    sort: PaginationPropertySort[] = [{direction: Order.DESC, property: 'dateCreation'}];
    organismes: Array<OrganismeDTO> = [];
    utilisateur: UtilisateurDTO;
    utilisateurs: UtilisateurDTO[] = [];
    services: ServiceDTO[] = [];
    raisonSociale: string;
    disabledDE: boolean = false;
    typeBorneOptionsNonAC: string[] = ['Sans minimum ni maximum',
        'Borne minimum',
        'Borne maximum',
        'Bornes minimum et maximum'];
    typeBorneOptionsAC: string[] = [
        'Borne maximum',
        'Bornes minimum et maximum'];
    typeBorneSelected: string;
    datesPrevisionnellesOK: boolean = true;
    datesReellesOK: boolean = true;
    parametrageSAD: boolean = true;
    termLieu: string;
    isUtilisateurChorus: boolean = false;
    showAlerteDate: boolean = true;
    //pour l'écran fiche contrat ou édition contrat
    editionMode = false;
    modifiable: boolean = USER_CONTEXT.hasHabilitation(Habilitation.MODIF_DONNEES_CONTRAT);
    numContratModif = false;
    premierContratNotifie = false;
    parametrageProjetAchat: boolean = true;
    protected readonly formePrix = FormePrix;
    procedureOptionsFiltred: Array<ValueLabelDTO> = [];
    private readonly _procedureConcession = ['PNO', 'PNNO', 'PNR', 'PNNR'];
    editContratChapeau = false;
    attributs: string;
    statutsModifables: string;

    constructor(injector: Injector, private etablissementService: EtablissementService,
                private organismeService: OrganismeService, private serviceSercice: ServiceService,
                private utilisateurService: UtilisateurService,
                private parametrageService: ParametrageService,
                private config: NgSelectConfig) {
        super(injector);
        this.editionModeEvent = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.loadReferentiel();

        this.statutsModifables = APPLICATION.parametrage?.contrat?.statuts?.modifiables;
        this.attributs = APPLICATION.parametrage?.contrat?.attributs?.visible;
        this.parametrageSAD = APPLICATION.parametrage?.ac_sad?.transversaux?.actif;
        this.parametrageProjetAchat = APPLICATION.parametrage?.projet?.achat?.actif;
        this.config.notFoundText = "Aucun résultat";
        this.organismeService.getOrganismes().subscribe(organismes => this.organismes = organismes);
        this.serviceSercice.getServices().subscribe(services => {
            this.services = services;
        });

        this.utilisateur = USER_CONTEXT.utilisateur;
        this.isUtilisateurChorus = this.utilisateur?.service?.echangesChorus;

        if (this.isCreation) {
            this.dureeMarche = 0;
            this.initContrat();
            this.contrat.service = this.utilisateur.service;
            this.contrat.createur = this.utilisateur;
            this.contrat.typeFormePrix = this.formePrixOptions.find(e => e.value == 'FORFAITAIRE');
        } else {
            if (!!this.contrat.dateMaxFinContrat && !!this.contrat.dateNotification) {
                this.dureeMarche = Math.ceil(moment(this.contrat.dateMaxFinContrat).diff(moment(this.contrat.dateNotification), 'months', true));
            } else {
                this.dureeMarche = 0;
            }
            if (!!this.contrat.idLienAcSad) {
                this.contratService.findContratById(this.contrat.idLienAcSad).subscribe(contrat => {
                    this.lienAcSad = contrat;
                });
            }
        }


        this.activatedRoute.queryParams.subscribe(params => {
            if (params['idFournisseur']) {
                console.log("params ", params['idFournisseur']);
                this.etablissementService.findEtablissementById(params['idFournisseur']).subscribe(etablissement => {
                    this.contrat.attributaire.etablissement = etablissement;
                });
            }
        });
        this.onChangeTypeContrat();
    }


    getUtilisateurs(serviceId: any): Observable<Array<UtilisateurDTO>> {
        const utilisateursObservable = this.utilisateurService.getUtilisateursByServiceId(serviceId);
        utilisateursObservable.subscribe(utilisateurs => {
            this.utilisateurs = utilisateurs;
        });
        return utilisateursObservable;
    }


    cancelClick() {
        this.router.navigate(['/']);
    }

    trackByFn(item: any) {
        return item.id;
    }

    private loadLienAcSad() {
        if (this.contrat?.type?.value === 'marsp') {
            this.contratService.findContrats({
                perimetre: PerimetreContrat.SERVICE_ET_SOUS_SERVICES,
                typesContrat: this.typeContratOptions.filter(type => type.value == 'sad'),
            }, 0, 5000, this.sort).subscribe(value => {
                if (!!value && !!value.content) {
                    this.contrats = value.content.filter(e => !e.chapeau);
                } else {
                    this.contrats = [];
                }
            })
        } else if (this.contrat?.type?.value === 'msa') {
            this.contratService.findContrats({
                perimetre: PerimetreContrat.SERVICE_ET_SOUS_SERVICES,
                typesContrat: this.typeContratOptions.filter(type => type.value == 'amu' || type.value == 'abcmo' || type.value == 'amo'),
            }, 0, 5000, this.sort).subscribe(value => {
                if (!!value && !!value.content) {
                    this.contrats = value.content.filter(e => !e.chapeau);
                } else {
                    this.contrats = [];
                }
            })
        }
    }

    getDateEnvoiAlerte() {
        if (this.contrat.dateMaxFinContrat && this.contrat.dureePhaseEtudeRenouvellement) {
            return moment(this.contrat.dateMaxFinContrat).subtract(this.contrat.dureePhaseEtudeRenouvellement, "months").toDate();
        }
        return null;
    }

    updateTechniqueAchat() {
        if (this.contrat?.consultation?.techniqueAchats && this.contrat.consultation.techniqueAchats.length > 0) {
            return;
        }
        if (this.contrat?.type?.parentLabel) {
            this.tecniquesAchatOptions.forEach(tc => {
                if (tc.label === this.contrat?.type?.parentLabel) {
                    this.contrat.consultation.techniqueAchats.push(tc);
                }
            });
        } else if (this.contrat?.type?.parents) {
            this.tecniquesAchatOptions.forEach(tc => {
                if (this.contrat?.type?.parents?.find(e => e.label == tc.label)) {
                    this.contrat.consultation.techniqueAchats.push(tc);
                }
            });
        }
    }


    getBindLabelLieu() {
        return isNaN(Number(this.termLieu?.charAt(0))) ? "label" : "value";
    }

    updateDureeMarche() {
        if (!!this.contrat.dateMaxFinContrat && !!this.contrat.dateNotification) {
            this.dureeMarche = Math.ceil(moment(this.contrat.dateMaxFinContrat).diff(moment(this.contrat.dateNotification), 'months', true));
        }
    }


    public update(etablissement: EtablissementDTO): void {
        console.log('Update etablissement = ', etablissement);

        this.contrat.attributaire.etablissement = etablissement;
    }

    updateShowAlert() {
        if (this.contrat.dateMaxFinContrat && this.contrat.dureePhaseEtudeRenouvellement) {
            let date = moment(this.contrat.dateMaxFinContrat).subtract(this.contrat.dureePhaseEtudeRenouvellement, "months").toDate();
            let now = new Date();
            this.showAlerteDate = date > now;
        }
    }

    getItemsTypeBorne() {
        if (this.contrat?.type?.value === 'amu' || this.contrat?.type?.value === 'abcmo' || this.contrat?.type?.value === 'amo') {
            return this.typeBorneOptionsAC;
        } else {
            return this.typeBorneOptionsNonAC;
        }

    }

    resetBornes() {
        this.contrat.borneMinimale = 0;
        this.contrat.borneMaximale = 0;
        if (this.typeBorneSelected === 'Sans minimum ni maximum') {
            this.contrat.typeBorne = TypeBorne.AUCUNE;
        } else if (this.typeBorneSelected === 'Borne minimum') {
            this.contrat.typeBorne = TypeBorne.MINIMALE;
        } else if (this.typeBorneSelected === 'Borne maximum') {
            this.contrat.typeBorne = TypeBorne.MAXIMALE;
        } else if (this.typeBorneSelected === 'Bornes minimum et maximum') {
            this.contrat.typeBorne = TypeBorne.MINIMALE_MAXIMALE;
        }
    }

    creerMarcheSubsequent(contrat: ContratDTO) {
        this.contratService.creerMarcheSubsequent(this.contrat);
    }

    peutCreerMarcheSubsequentOuOrganismes() {
        return this.contratService.peutCreerOrganismesOuMarcheSubsequent(this.contrat);
    }

    exporter() {
        this.onExport.emit(this.contrat);
    }

    favContrat(contrat: ContratDTO) {
        this.contratService.toggleFavoris(contrat.id).subscribe((_) => contrat.favori = !contrat.favori);
    }

    editContrat() {
        if (this.contrat.consultation == null) {
            this.contrat.consultation = {
                modaliteExecutions: [],
                techniqueAchats: [],
            };
        }
        if (!!this.contrat.dateNotification) {
            this.contrat.dateNotification = new Date(this.contrat.dateNotification);
        }
        if (!!this.contrat.dateFinContrat) {
            this.contrat.dateFinContrat = new Date(this.contrat.dateFinContrat);
        }
        if (!!this.contrat.dateMaxFinContrat) {
            this.contrat.dateMaxFinContrat = new Date(this.contrat.dateMaxFinContrat);
        }
        if (!!this.contrat.datePrevisionnelleFinMaximaleMarche) {
            this.contrat.datePrevisionnelleFinMaximaleMarche = new Date(this.contrat.datePrevisionnelleFinMaximaleMarche);
        }
        if (!!this.contrat.datePrevisionnelleFinMarche) {
            this.contrat.datePrevisionnelleFinMarche = new Date(this.contrat.datePrevisionnelleFinMarche);
        }
        if (!!this.contrat.datePrevisionnelleNotification) {
            this.contrat.datePrevisionnelleNotification = new Date(this.contrat.datePrevisionnelleNotification);
        }
        if (!!this.contrat.dateDebutEtudeRenouvellement) {
            this.contrat.dateDebutEtudeRenouvellement = new Date(this.contrat.dateDebutEtudeRenouvellement);
        }

        if (!!this.contrat.typeBorne) {
            if (this.contrat.typeBorne === "AUCUNE") {
                this.typeBorneSelected = 'Sans minimum ni maximum';
            } else if (this.contrat.typeBorne === "MINIMALE") {
                this.typeBorneSelected = 'Borne minimum';
            } else if (this.contrat.typeBorne === "MAXIMALE") {
                this.typeBorneSelected = 'Borne maximum';
            } else if (this.contrat.typeBorne === "MINIMALE_MAXIMALE") {
                this.typeBorneSelected = 'Bornes minimum et maximum';
            }
        }
        this.editionMode = !this.contrat.chapeau;
        const servicesObservable = this.serviceSercice.getServices();

        APPLICATION.loading().subscribe(
            _ => this.numContratModif = APPLICATION.parametrage?.contrat?.numero?.modifiable
        );

        servicesObservable.subscribe(services => {
            this.services = services;
        });

        const utilisateursObservable = this.getUtilisateurs(this.contrat.service.id);


        merge(servicesObservable, utilisateursObservable).subscribe(() => {

        }, () => {
            this.editionMode = !this.contrat.chapeau;
            this.editionModeEvent.emit(true);

        });
        if (this.contrat.chapeau) {
            this.contratService.findEnfantsByContratId(this.contrat.id).subscribe(contrats => {
                if (contrats.length > 0) {
                    const premierContrat = contrats[0];
                    if (premierContrat.dateNotification != null) {
                        this.premierContratNotifie = true;
                        this.contrat.dateNotification = !this.contrat.dateNotification ? moment(premierContrat.dateNotification).toDate() : this.contrat.dateNotification;
                        this.contrat.dateFinContrat = !this.contrat.dateFinContrat ? moment(premierContrat.dateFinContrat).toDate() : this.contrat.dateFinContrat;
                        this.contrat.dateMaxFinContrat = !this.contrat.dateMaxFinContrat ? moment(premierContrat.dateMaxFinContrat).toDate() : this.contrat.dateMaxFinContrat;
                    } else {
                        console.warn(`Le premier contrat n'est pas notifié , id contrat enfant => ${premierContrat.id}`)
                    }
                }
            });
        }
        this.bindCPVPrincipal();
        this.editContratChapeau = this.contrat.chapeau;
    }

    getValeurMultiple(propriete: string, proprieteMultiple: string) {
        let objects = this.getValeur(propriete) as Array<any>;
        if (objects && proprieteMultiple) {
            return objects.map(o => o[proprieteMultiple]).join(", ");
        }
        return null;
    }

    getValeur(propriete: string) {
        if (propriete) {
            let objet: Object = this.contrat;
            for (let prop of propriete.split('.')) {
                objet = objet[prop]
                if (objet == null) {
                    return null;
                }
                if (objet && objet.hasOwnProperty(prop)) {
                    return objet[prop];
                }
            }
            if (propriete === 'ccagApplicable') {
                return objet['label']
            }
            return objet;
        }
        return null;
    }

    haveIncidence(): boolean {
        return this.contrat.listAvenantsNonRejete?.length > 0
            && (this.contrat.listAvenantsNonRejete?.find(ave => ave.avenantType?.incidence) != null);
    }

    getLibelleTypeBorne(typeBorne: string | TypeBorne) {
        if (typeBorne === "AUCUNE") {
            return 'Sans minimum ni maximum';
        } else if (typeBorne === "MINIMALE") {
            return 'Borne minimum';
        } else if (typeBorne === "MAXIMALE") {
            return 'Borne maximum';
        } else if (typeBorne === "MINIMALE_MAXIMALE") {
            return 'Bornes minimum et maximum';
        } else {
            return "non renseigné";
        }
    }

    cancelEditContrat() {
        this.editionMode = false;
        this.editionModeEvent.emit(false);
        this.getContrat();
        this.editContratChapeau = false;
    }

    getContrat() {
        if (this.contrat) {
            this.contratService.findContratById(this.contrat.id).subscribe(contrat => {
                this.contrat = contrat;
                this.bindCPVPrincipal();
            });
        }
    }

    valider(form: NgForm) {
        if (this.contrat.chapeau) {
            this.isSubmitted = true;
            this.contratService.updateDonneesPrincipales(this.contrat).subscribe(contrat => {
                this.contrat = contrat;
                this.cancelEditContrat();
                messageNotification('Informations du contrat modifiées.');
                this.isSubmitted = false;
                this.onSave.emit(contrat);
            }, _ => {

            }, () => {

            });
        } else {
            this.isSubmitted = true;
            if (!this.contrat.attributionAvance) {
                this.contrat.tauxAvance = 0;
            }
            this.checkTauxAvance();
            this.contrat.dureeMaximaleMarche = this.dureeMarche;
            if (form.form.valid && this.dateFinPrevisionnelleOK && this.dateFinMaxPrevisionnelleOK && this.isOKAvance && this.creerClauseComponent.validerClause()) {
                this.contrat.clausesDto = this.creerClauseComponent.prepareClausesToSave();
                console.log ("clauses", this.contrat.clausesDto);
                let hasClauseSociale: boolean = false;
                let hasClauseEnvironnementale: boolean = false;
                if ( this.contrat.clausesDto.length > 0) {
                    this.contrat.clausesDto.forEach(clause => {
                        if (clause.code === 'clausesSociales') {
                            hasClauseSociale = true;
                        }
                        if (clause.code === 'clauseEnvironnementale') {
                            hasClauseEnvironnementale = true;
                        }
                    });
                    this.contrat.considerationsEnvironnementales = hasClauseEnvironnementale;
                    this.contrat.considerationsSociales = hasClauseSociale;
                    this.contrat.achatResponsable = hasClauseSociale || hasClauseEnvironnementale;
                }
                this.contratService.updateDonneesPrincipales(this.contrat).subscribe(contrat => {
                    this.contrat = contrat;
                    this.cancelEditContrat();
                    messageNotification('Informations du contrat modifiées.');
                    this.isSubmitted = false;
                    this.onSave.emit(contrat);
                }, _ => {

                }, () => {

                });
            } else {
                Notification.errorNotification('Le formulaire est invalide.');
            }
        }
    }

    onChangeTypeContrat() {
        this.lienAcSad = null;
        this.loadLienAcSad();
        this.updateTechniqueAchat();
        this.updateProcedure();
    }

    updateProcedure() {
        this.procedureOptionsFiltred = this.procedureOptions.filter(procedure => !this._procedureConcession.find(e => e == procedure.value))
    }

    visible(attribut: string): boolean {
        return this.attributs === '' || this.attributs === '*' || this.attributs.split(',').includes(attribut);
    }

    customSearchFn(term: string, item: any) {
        return item?.objet?.toLocaleLowerCase().indexOf(term?.toLowerCase()) > -1 || item?.numero?.toLocaleLowerCase().indexOf(term?.toLowerCase()) > -1;
    }

    searchByNom(term: string, item: any) {
        term = term.toLocaleLowerCase();
        return item?.nom?.toLocaleLowerCase().indexOf(term) > -1
            || item?.prenom?.toLocaleLowerCase().indexOf(term) > -1;
    }

    validerAjoutContrat(form: NgForm) {
        this.isSubmitted = true;
        if (!!this.lienAcSad) {
            this.contrat.idLienAcSad = this.lienAcSad.id
        }
        if (!this.contrat.attributionAvance) {
            this.contrat.tauxAvance = 0;
        }
        this.checkTauxAvance();
        this.contrat.dureeMaximaleMarche = this.dureeMarche;
        if (this.etablissements?.length > 0 && form.form.valid && this.dateFinPrevisionnelleOK && this.dateFinMaxPrevisionnelleOK && this.isOKAvance && this.creerClauseComponent.validerClause()) {
            this.ajouterContrat();
        } else {
            Notification.errorNotification('Le formulaire est invalide.');
        }
    }

    ajouterContrat() {
        this.contrat.clausesDto = this.creerClauseComponent.prepareClausesToSave();
        this.contrat.attributaire.etablissement = this.etablissements[0];
        this.contratService.ajouterContrat(this.contrat).subscribe(contratDTO => {
            this.contrat.idContratChapeau = contratDTO.idContratChapeau;
            this.enregistrerContratsRestants();
            Notification.messageNotification('Le contrat n°' + contratDTO.referenceLibre + ' a été créé');
            this.router.navigate(['/ficheContrat', contratDTO.id]);
        });
    }

    enregistrerContratsRestants() {
        for (let i = 1; i < this.etablissements.length; i++) {
            this.saveContrat(this.etablissements[i]);
        }
    }

    saveContrat(etablissement: EtablissementDTO) {
        this.contrat.attributaire.etablissement = etablissement;
        this.contratService.ajouterContrat(this.contrat).subscribe(contratDTO => {
            Notification.messageNotification('Le contrat n°' + contratDTO.referenceLibre + ' a été créé');
            this.router.navigate(['/ficheContrat', contratDTO.id]);
        });
    }

    checkDatesPrevisionnelles() {
        if (!!this.contrat.datePrevisionnelleFinMarche && !!this.contrat.datePrevisionnelleNotification && !!this.contrat.datePrevisionnelleFinMaximaleMarche) {
            this.datesPrevisionnellesOK = (this.contrat.datePrevisionnelleFinMarche > this.contrat.datePrevisionnelleNotification) && (this.contrat.datePrevisionnelleFinMaximaleMarche >= this.contrat.datePrevisionnelleFinMarche);
        }
    }

    checkDatesReelles() {
        if (!!this.contrat.dateFinContrat && !!this.contrat.dateNotification && !!this.contrat.dateMaxFinContrat) {
            this.datesReellesOK = (this.contrat.dateFinContrat > this.contrat.dateNotification) && (this.contrat.dateFinContrat <= this.contrat.dateMaxFinContrat);
        } else if (!!this.contrat.dateFinContrat && !!this.contrat.dateNotification) {
            this.datesReellesOK = this.contrat.dateFinContrat > this.contrat.dateNotification;
        } else if (!!this.contrat.dateMaxFinContrat && !!this.contrat.dateNotification) {
            this.datesReellesOK = this.contrat.dateMaxFinContrat > this.contrat.dateNotification;
        }
    }

    possedeStatutModifiable(): boolean {
        return this.statutsModifables === '' || this.statutsModifables === '*' || this.statutsModifables.split(',').includes(this.contrat.statut);

    }
}

