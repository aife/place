import {Component, Injector, Input, ViewChild} from '@angular/core';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {ContratDTO, ValueLabelDTO} from '@dto';
import {ContratService, CpvService} from '@services';
import {TypeReferentiel} from '@dto/backendEnum';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {CreerClauseComponent} from '@views/contrats/clause/creer-clause';
import {NgForm} from '@angular/forms';
import * as Notification from '@common/notification';
import {USER_CONTEXT} from "@common/userContext";


@Component({
    template: ''
})
export abstract class AbstractFicheContrat extends AbstractViewComponent {
    @Input() contrat: ContratDTO;
    private cpvService: CpvService
    public termCPV: string;
    public codesCpv: ValueLabelDTO[] = [];
    private store: Store<State>
    protected typeContratOptions: Array<ValueLabelDTO> = [];
    protected procedureOptions: Array<ValueLabelDTO> = [];
    protected modalitesExecutionOptions: Array<ValueLabelDTO> = [];
    protected tecniquesAchatOptions: Array<ValueLabelDTO> = [];
    protected formePrixOptions: Array<ValueLabelDTO> = [];
    protected typePrixOptions: Array<ValueLabelDTO> = [];
    protected ccagOptions: Array<ValueLabelDTO> = [];
    protected lieuxExecution: ValueLabelDTO[] = [];
    protected lieuxExecutionOrdonne: ValueLabelDTO[] = [];
    protected lieuxExecutionDepartRegion: ValueLabelDTO[] = [];
    protected lieuxExecutionPays: ValueLabelDTO[] = [];
    protected categorieConsultationOptions: Array<ValueLabelDTO> = [];
    protected natureContratConcessionOptions: ValueLabelDTO[] = [];
    protected displayOrigines: boolean = false;
    protected contratService: ContratService
    protected isSubmitted: boolean = false;
    protected lienAcSad: ContratDTO;
    protected dureeMarche: number;
    protected dateFinPrevisionnelleOK: boolean = true;
    protected dateFinMaxPrevisionnelleOK: boolean = true;
    protected isOKAvance: boolean = true;
    protected devise: ValueLabelDTO;
    @ViewChild(CreerClauseComponent) creerClauseComponent: CreerClauseComponent;

    constructor(private injector: Injector) {
        super(injector);
        this.cpvService = injector.get(CpvService);
        this.cpvService.getToken();
        this.store = injector.get(Store);
        this.contratService = injector.get(ContratService);
    }

    getBindLabelCpv() {
        return isNaN(Number(this.termCPV?.charAt(0))) ? "label" : "value";
    }

    public searchCpv(evt: { term: string; items: any[] }) {
        this.termCPV = evt.term;
        this.cpvService.fetch(evt.term).subscribe(results => {
            this.codesCpv = results.referentielList?.map(res => {
                const valueLabel: ValueLabelDTO = {value: res.code, label: res.label};
                return valueLabel;
            })
        })
    }

    public loadReferentiel() {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_CONTRAT).content).subscribe(data => {
            this.typeContratOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CATEGORIE).content).subscribe(data => {
            this.categorieConsultationOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.FORME_PRIX).content).subscribe(data => {
            this.formePrixOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.PROCEDURE).content).subscribe(data => {
            this.procedureOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_PRIX).content).subscribe(data => {
            this.typePrixOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.MODALITE_EXECUTION).content).subscribe(data => {
            let dataReversed = [...data].reverse();
            this.modalitesExecutionOptions = dataReversed;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TECHNIQUE_ACHAT).content).subscribe(data => {
            let dataReversed = [...data].reverse();
            this.tecniquesAchatOptions = dataReversed;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CCAG).content).subscribe(data => {
            this.ccagOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.LIEU_EXECUTION).content).subscribe(data => {
            this.lieuxExecution = data;
            this.lieuxExecution.forEach(lieu => {
                if (!!lieu.parentLabel) {
                    this.lieuxExecutionDepartRegion.push(lieu);
                } else {
                    this.lieuxExecutionPays.push(lieu);
                }
            });
            this.lieuxExecutionDepartRegion.sort((a, b) => a.parentLabel.localeCompare(b.parentLabel));
            this.lieuxExecutionPays.sort((a, b) => a.label.localeCompare(b.label));
            this.lieuxExecutionOrdonne = this.lieuxExecutionDepartRegion.concat(this.lieuxExecutionPays);
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.NATURE_CONTRAT_CONCESSION).content).subscribe(data => {
            this.natureContratConcessionOptions = data;
        });
        this.devise = USER_CONTEXT.utilisateur.devise;
    }

    updateOrigines(cpvs: ValueLabelDTO[]) {
        this.contrat.cpvPrincipal = cpvs[0];
        this.contrat.codesCPV = cpvs;
        this.bindCPVPrincipal();
        this.displayOrigines = false;
        if (!!this.contrat.codesCPV && this.contrat.codesCPV.length > 0) {
            let cpvList = this.contrat.codesCPV.map(item => item.value);
            cpvList.forEach(cpv => {
                let numero = parseInt(cpv);
                if ((numero <= 15982200 && numero >= 15100000) || (numero <= 34144910 && numero >= 34100000)
                    || (numero <= 34522700 && numero >= 34510000) || (numero <= 34622500 && numero >= 34600000)
                    || (numero <= 34722200 && numero >= 34710000) || (numero <= 33198200 && numero >= 33100000)
                    || (numero <= 33698300 && numero >= 33600000) || (numero <= 18453000 && numero >= 18100000)
                    || (numero <= 18843000 && numero >= 18800000)
                ) {
                    this.displayOrigines = true;
                    return;
                }
            })

        } else {
            this.displayOrigines = false;
        }
    }

    bindCPVPrincipal() {
        if (this.contrat.cpvPrincipal && this.contrat.codesCPV) {
            const cpvPrincipal = this.contrat.codesCPV.find(cpv => cpv.value === this.contrat.cpvPrincipal.value);
            if (cpvPrincipal) {
                cpvPrincipal.principal = true;
            }
        }
    }

    initContrat() {
        this.contrat.chapeau = false;
        this.contrat.consultation = {
            numero: '',
            categorie: null,
            horsPassation: true,
            techniqueAchats: []
        };
        this.contrat.favori = false;
        this.contrat.marcheInnovant = false;
        this.contrat.defenseOuSecurite = false;
        this.contrat.publicationDonneesEssentielles = true;
    }

    validerAjoutContrat(form: NgForm) {
        this.isSubmitted = true;
        if (!!this.lienAcSad) {
            this.contrat.idLienAcSad = this.lienAcSad.id
        }
        if (!this.contrat.attributionAvance) {
            this.contrat.tauxAvance = 0;
        }
        this.checkTauxAvance();
        this.contrat.dureeMaximaleMarche = this.dureeMarche;
        if (!!this.contrat.attributaire.etablissement && form.form.valid && this.dateFinPrevisionnelleOK && this.dateFinMaxPrevisionnelleOK && this.isOKAvance && this.creerClauseComponent.validerClause()) {
            this.contrat.clausesDto = this.creerClauseComponent.prepareClausesToSave();
            console.log("creation avec clause : ", this.contrat.clausesDto)
            let hasClauseSociale: boolean = false;
            let hasClauseEnvironnementale: boolean = false;
            if ( this.contrat.clausesDto.length > 0) {
                this.contrat.clausesDto.forEach(clause => {
                    if (clause.code === 'clausesSociales') {
                        hasClauseSociale = true;
                    }
                    if (clause.code === 'clauseEnvironnementale') {
                        hasClauseEnvironnementale = true;
                    }
                });
                this.contrat.considerationsEnvironnementales = hasClauseEnvironnementale;
                this.contrat.considerationsSociales = hasClauseSociale;
                this.contrat.achatResponsable = hasClauseSociale || hasClauseEnvironnementale;
            }
            this.contratService.ajouterContrat(this.contrat).subscribe(contratDTO => {
                Notification.messageNotification('Le contrat n°' + contratDTO.referenceLibre + ' a été créé');
                this.router.navigate(['/ficheContrat', contratDTO.id]);
            });
        } else {
            Notification.errorNotification('Le formulaire est invalide.');
        }

    }

    checkTauxAvance() {
        if (this.contrat.attributionAvance && !this.contrat.tauxAvance) {
            this.isOKAvance = false;
        } else this.isOKAvance = !(this.contrat.attributionAvance && !!this.contrat.tauxAvance && (this.contrat.tauxAvance < 0 || this.contrat.tauxAvance > 100));
    }
}
