import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {ContratDTO, ValueLabelDTO} from '@dto';
import {defaultItemsCountPerPage} from 'app/constants';
import {Table} from '@components/table/table';
import {ContratCriteriaDTO, ContratService, MessagerieService} from '@services';
import {Observable} from 'rxjs';
import {USER_CONTEXT} from '@common/userContext';
import {errorNotification, messageNotification} from '@common/notification';
import {APPLICATION} from '@common/parametrage';
import {Habilitation} from '@dto/backendEnum';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {SpaserService} from '@services/spaser.service';
import {
    ContratChorusValidator,
    ContratNotificationValidator,
    DateContratNotificationValidator
} from '../../../validators';
import {isEmpty} from '@common/validation';


interface ModaleVerificationChorus {
    erreurs: Error[];
}

@Component({
    selector: 'ltexec-tableau-contrats',
    templateUrl: './tableau-contrats.component.html',
    styleUrls: ['./tableau-contrats.component.scss']
})
export class TableauContratsComponent implements OnInit, Table {

    @Input() filtres: ContratCriteriaDTO = {};

    @Input() contrats: PaginationPage<ContratDTO> = {
        number: 0,
        size: defaultItemsCountPerPage,
        order: [{direction: Order.DESC, property: 'dateCreation'}]
    };
    @Input() standalone = false;
    @Input() masquerNavigation = false;
    @Input() sort: PaginationPropertySort[];
    @Output() onSelectContrat = new EventEmitter<ContratDTO>();
    @Output() onDeleteContrat = new EventEmitter<ContratDTO>();

    pageSize: number;
    self: any;
    marchesSubsequentsActif: boolean = false;
    isHabiliteExec: boolean = false;
    currentContrat: ContratDTO;
    isModalFicheContratShown = false;
    isModalSuppressionContratShown = false;
    isModalMessecShown = false;
    isModalNotificationErrorContratShown = false;
    isModalNotificationOKContratShown = false;
    errorNotificationMessage: string;
    isNotificationConfirmee: boolean = false;
    messageNotificationOK: string;
    navigateMessec: boolean = false;
    devise!: ValueLabelDTO;
    spaserActif: boolean;
    modaleVerificationChorus: ModaleVerificationChorus;

    constructor(
        public router: Router,
        private contratService: ContratService,
        private messagerieService: MessagerieService,
        private spaserService: SpaserService,
        private contratNotificationValidator: ContratNotificationValidator,
        private dateContratNotificationValidator: DateContratNotificationValidator,
        private contratChorusValidator: ContratChorusValidator
    ) {
        this.self = this;
    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any> {
        const observable: Observable<PaginationPage<ContratDTO>> = this.contratService.findContrats(this.filtres, pageNumber, pageSize, sort);

        observable.subscribe(contrats => {
            this.contrats = contrats;
            if (USER_CONTEXT.isHabiliteExec()) {
                const idContrats = this.contrats.content.map((contrat) => contrat.id);
                this.messagerieService.countMessageStatusContrats(idContrats).subscribe(
                    (messageStatus) => {
                        for (let i = 0; i < this.contrats.content.length; i++) {
                            if (messageStatus[i].idContrat && this.contrats.content[i].id === parseInt(messageStatus[i].idContrat, 10)) {
                                this.contrats.content[i].messageStatus = messageStatus[i];
                            }
                        }
                    }
                );
            }
        });

        return observable;
    }

    favContrat(contrat: ContratDTO) {
        this.contratService.toggleFavoris(contrat.id).subscribe((_) => contrat.favori = !contrat.favori);
    }

    isChorus(contrat: ContratDTO) {
        return contrat?.service?.echangesChorus && !contrat.chapeau && !(contrat.type?.modeEchangeChorus == 0);
    }


    verifierEchangeChorus(contrat: ContratDTO): void {
        let erreurs: Error[] = this.contratChorusValidator.validate(contrat);

        if (isEmpty(erreurs)) {
            this.echangerChorus(contrat);
        } else {
            this.modaleVerificationChorus = {
                erreurs: erreurs
            };
        }
    }

    echangerChorus(contrat: ContratDTO): void {
        this.contratService.echangerChorus(contrat);
    }

    notifierContrat(contrat: ContratDTO, navigateMessec: boolean, checkDateNotification: boolean) {
        this.navigateMessec = navigateMessec;
        this.currentContrat = contrat;
        const errors: Error[] = this.contratNotificationValidator.validate(contrat);

        if (checkDateNotification) {
            errors.push(...this.dateContratNotificationValidator.validate(contrat));
        }

        if (isEmpty(errors)) {
            this.isModalNotificationOKContratShown = true;
            let rappel = `Rappel des dates réelles renseignées :<br/>`;
            rappel += `<ul>`;
            rappel += `<li>Date réelle de notification du contrat : ${contrat.dateNotification ? moment(contrat.dateNotification).format('DD/MM/YYYY') : 'Non renseignée'}</li>`;
            rappel += `<li>Date réelle de fin du contrat : ${contrat.dateFinContrat ? moment(contrat.dateFinContrat).format('DD/MM/YYYY') : 'Non renseignée'}</li>`;
            rappel += `<li>Date de fin maximale du contrat : ${contrat.dateMaxFinContrat ? moment(contrat.dateMaxFinContrat).format('DD/MM/YYYY') : 'Non renseignée'}</li>`;
            rappel += `</ul>`;
            this.messageNotificationOK = `${rappel}La notification du contrat est immédiate et irréversible. Êtes vous sûr de vouloir notifier ce contrat ?`;

        } else {
            this.errorNotificationMessage = `
                <div class="errors">
                    <div>
                        <span>Le contrat ne peut pas être notifié.</span>
                        <br/>
                        <span>${errors.length > 1 ? `Les champs suivants sont obligatoires` : `Le champ suivant est obligatoire`} :</span>
                        <br/>
                        <ol class="m-t-1">
                            <li>${errors.map((error: Error) => error.message).join('</li><li>')}</li>
                        </ol>
                    </div>
                </div>
                <div class="please">
                    <span>Merci de corriger avant de continuer.</span>
                </div>`;

            this.isModalNotificationErrorContratShown = true;
        }
    }

    confirmer() {
        this.isNotificationConfirmee = true;
        this.onSelectContrat.emit(this.currentContrat);
        if (this.navigateMessec) {
            if (this.masquerNavigation) {
                this.isModalMessecShown = true;
                this.navigateMessec = false;
            } else {
                this.router.navigate(['/envoiMessage', this.currentContrat.id], {queryParams: {notification: true}});
                this.navigateMessec = false;
            }

        } else {
            this.contratService.notifierContrat(this.currentContrat).subscribe(
                (ret) => {
                    this.fetchPage(0, this.contrats.size, this.sort);
                    messageNotification('Contrat notifié.');
                },
                error => {
                    errorNotification('Erreur suppression contrat:');
                }
            );
        }
        this.isModalNotificationOKContratShown = false;
        this.messageNotificationOK = null;
    }


    ouvrirSuppressionContratModale(contrat: ContratDTO): void {
        this.currentContrat = contrat;
        this.isModalSuppressionContratShown = true;
    }

    fermerModaleSuppression(): void {
        this.currentContrat = undefined;
        this.isModalSuppressionContratShown = false;
    }


    supprimerContrat(contrat: ContratDTO): void {
        this.isModalSuppressionContratShown = false;

        this.contratService.deleteContrat(contrat.id).subscribe(
            () => {
                this.fetchPage(0, this.contrats.size, this.sort);
                messageNotification('Contrat supprimé.');
                this.onDeleteContrat.emit(contrat);
            },
            error => {
                errorNotification('Erreur suppression contrat:');
            }
        );

    }

    creerMarcheSubsequent(contrat: ContratDTO) {
        this.contratService.creerMarcheSubsequent(contrat);
    }

    peutCreerMarcheSubsequent(contrat: ContratDTO) {
        return this.marchesSubsequentsActif && this.contratService.peutCreerOrganismesOuMarcheSubsequent(contrat);
    }

    hasHabilitation(habilitation: string) {
        const habilitationEnum = Habilitation[habilitation];
        if (habilitationEnum) {
            return USER_CONTEXT.hasHabilitation(habilitationEnum);
        }
        return false;

    }

    ngOnInit() {
        this.marchesSubsequentsActif = APPLICATION.parametrage?.marches?.subsequents?.actif;
        this.spaserActif = APPLICATION.parametrage?.exec?.spaser;
        this.isHabiliteExec = USER_CONTEXT.isHabiliteExec();
        if (this.standalone) {
            if (this.filtres.perimetre == null) {
                this.filtres.perimetre = USER_CONTEXT.getDefaultPerimetre(null);
            }
            this.fetchPage(0, this.contrats.size, this.sort);
        }
        this.devise = USER_CONTEXT.utilisateur.devise;


    }

    openFicheContrat(contrat: ContratDTO) {
        if (this.masquerNavigation) {
            this.currentContrat = contrat;
            this.isModalFicheContratShown = true;
        } else {
            this.router.navigate(['/ficheContrat', contrat.id]).then(() => console.debug('Navigation vers fiche contrat'));
        }

    }

    onSaveFicheContrat(contrat: ContratDTO) {
        this.isModalFicheContratShown = false;
        this.currentContrat = null;
        this.contrats.content = this.contrats.content.map(c => c.id === contrat.id ? contrat : c);
    }

    changePageSize(page: PaginationPage<any>) {
        this.contrats = page;
    }

    trier($event: any) {
        this.sort = $event;
    }

    redirigerVersSpaser(contrat: ContratDTO) {
        this.spaserService.initContext(contrat.id).subscribe(value => window.open(value, '_blank'));
    }

    fermerModaleNotification(): void {
        this.isModalNotificationErrorContratShown = false;
        this.currentContrat = null;
        this.errorNotificationMessage = null;
    }

    fermerModaleModification(): void {
        this.isModalNotificationOKContratShown = false;
        this.currentContrat = null;
        this.messageNotificationOK = null;
    }

    fermerModaleFiche(): void {
        this.isModalFicheContratShown = false;
        this.currentContrat = null;
    }

    fermerModaleVerificationChorus(): void  {
        this.modaleVerificationChorus = null;
    }

    canDelete(contrat: ContratDTO) {
        const candDeleteChorus = !contrat.statutEJ || contrat.statutEJ.endsWith(';Supprimé#') || contrat.statutEJ.endsWith(';Clôturé#');
        return this.hasHabilitation('SUPPRIMER_CONTRAT') && !contrat.chapeau && contrat.statut === 'ANotifier' && candDeleteChorus;
    }
}
