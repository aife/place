import {Component, Input} from '@angular/core';


@Component({
    selector: 'ltexec-categorie-consultation',
    templateUrl: 'categorieConsultation.html'
})
export class CategorieConsultationComponent {
    @Input('categorie-consultation') categorieConsultation: any;

    constructor() {

    }
}
