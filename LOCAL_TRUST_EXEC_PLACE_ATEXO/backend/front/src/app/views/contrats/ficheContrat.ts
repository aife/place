import {Component, EventEmitter, HostListener, Injector, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {ActivatedRoute} from '@angular/router';
import {ActeService, CalendrierService, ContratService, DocumentService, FaqService} from '@services';
import {CanComponentDeactivate} from '@common/canDeactivateGuard';
import {ActeDTO, ContratDTO} from '@dto';
import {EtatDateEvenement} from '@dto/backendEnum';
import {Order} from '@common/pagination';
import {saveAs} from 'file-saver';
import {AbstractViewComponent} from '@views/abstract.view.component';


@Component({
    selector: 'ltexec-contrat-fiche',
    templateUrl: 'ficheContrat.html'
})
export class FicheContratComponent extends AbstractViewComponent implements CanComponentDeactivate, OnInit {

    @Input() contratId: any;
    contrat: ContratDTO;
    editionModeCount = 0;
    actes: ActeDTO[] = [];

    // Modale pour confirmer le changement d'écran
    displayConfirm = false;
    confirmObservable = new Subject<boolean>();
    etatDateEvenement = EtatDateEvenement;
    @Input() idExterne: string;
    @Input() masquerNavigation = false;
    @Output() onSave: EventEmitter<ContratDTO> = new EventEmitter<ContratDTO>();

    constructor(
        private contratService: ContratService,
        private documentService: DocumentService,
        private calendrierService: CalendrierService,
        private faqService: FaqService,
        private acteServices: ActeService,
        private injecor: Injector) {
        super(injecor);
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            if (params['contratId']) {
                this.contratId = params['contratId'];
            }
            this.loadContrat();
        });
    }

    loadContrat() {
        this.faqService.currentContratId = this.contratId;

        const contratObs = this.idExterne != null ? this.contratService.findContratByIdExterne(this.idExterne) : this.contratService.findContratById(this.contratId);
        contratObs.subscribe(contrat => {
            this.contrat = contrat;
            this.contrat.listAvenantsNonRejete = this.contrat.listAvenantsNonRejete.map(this.calendrierService.calculerEtatDate);
        });
            this.acteServices.rechercher({contrat: {id: this.contratId}}, 0, 1000,[ {
                property: 'id',
                direction: Order.ASC
            }]).subscribe(actes => {
                this.actes = actes.content.filter(a => a.publicationDonneesEssentielles);

                this.actes.sort((acteA: ActeDTO, acteB: ActeDTO) => acteA.id - acteB.id);
            });
    }


    onBackClick() {
        this.router.navigate(['/']).then(() => {
            console.log('navigated');
        });
    }

    onBlocEditionMode(editonMode) {
        if (editonMode) {
            this.editionModeCount++;
        } else {
            this.editionModeCount--;
        }
    }

    checkEditionMode(): boolean {
        return this.editionModeCount > 0;
    }

    canDeactivate(): Observable<boolean> | boolean {
        if (this.checkEditionMode()) {
            this.displayConfirm = true;
            return this.confirmObservable;
        } else {
            return true;
        }
    }

    onConfirmResolve(result: boolean) {
        this.displayConfirm = false;
        this.confirmObservable.next(result);
    }

    // Pour la navigation en dehors de la page (fermeture de l'onglet, changement d'URL, etc)
    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.checkEditionMode()) {
            // Message pour IE
            $event.returnValue = 'Êtes-vous sûr de vouloir quitter cette page sans enregistrer ?';
        }
    }

    exporter(contrat: ContratDTO) {
        this.documentService.export(this.contratId).subscribe(blob => {
            saveAs(blob, 'contrat_' + contrat.referenceLibre + '.docx');
        });
    }

    transform(value: number, args: Array<any> = null): string {
        if (value) {
            return numeral(value).format('0,0.00');
        }
        return '0,00';
    }

    goToAttributaires() {
        this.router.navigate(['/attributaires', this.contrat.id], {queryParams: {from: 'ficheContrat'}});
    }
}
