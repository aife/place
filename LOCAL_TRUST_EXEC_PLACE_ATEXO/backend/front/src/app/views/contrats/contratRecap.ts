import {Component, Input, OnInit} from '@angular/core';
import {ContratDTO} from '@dto';
import {APPLICATION} from '@common/parametrage';
import {USER_CONTEXT} from '@common/userContext';

@Component({
    selector: 'ltexec-contrat-recap',
    templateUrl: 'contratRecap.html',
})
export class ContratRecapComponent implements OnInit {
    @Input() contrat: ContratDTO;
    isOpen: boolean = false;
    moduleExecution: boolean = true;

    ngOnInit() {
        this.moduleExecution = USER_CONTEXT?.isHabiliteExec();
    }

}
