import {ChangeDetectorRef, Component, Injector, OnInit} from '@angular/core';
import {
    ActeService,
    CalendrierService,
    ContratService,
    DocumentService,
    EvenementCriteriaDTO,
    FaqService
} from '@services';
import {downloadUrl} from '@common/download';
import {Order} from '@common/pagination';
import {ActeDTO, ContratDTO, EchangeChorusDTO, EvenementDTO} from '../../external/backend';
import {EtatEvenement, StatutActe} from '@dto/backendEnum';
import {EchangeChorusService} from '@services/echange-chorus.service';
import {saveAs} from 'file-saver';
import {
    CreationEchangeChorusComponent
} from '@views/actes/chorus/creation-echanges-chorus/creation-echange-chorus.component';
import {APPLICATION} from '@common/parametrage';
import {AbstractViewComponent} from '@views/abstract.view.component';

@Component({
    selector: 'ltexec-contrat-tableau-bord',
    templateUrl: 'contratTableauBord.html'
})
export class ContratTableauBordComponent extends AbstractViewComponent implements OnInit {

    contratId: any;

    contrat: ContratDTO;
    listBonCommandes: EvenementDTO[] = [];
    montantTotalHT: number;
    montantTotalTVA: number;
    actes: ActeDTO[];
    nbActes = 0;
    nbEvenements = 8;
    echangesCHORUS: EchangeChorusDTO[] = [];
    listEvenementEnAttente: EvenementDTO[] = [];
    actesActif = false;

    // Pour que l'enum soit accessible dans le template
    etatEvenement = EtatEvenement;

    constructor(
        private acteService: ActeService,
        private contratService: ContratService,
        private injector: Injector,
        private documentService: DocumentService,
        private calendrierService: CalendrierService,
        private faqService: FaqService,
        private echangeChorusService: EchangeChorusService,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        super(injector);
    }

    ngOnInit() {
        this.actesActif = APPLICATION.parametrage?.actes?.actif;
        this.activatedRoute.params.subscribe(params => {
            let contrat = history.state.contrat;
            if (contrat) {
                this.contratId = contrat.id;
            } else {
                this.contratId = params['contratId'];
            }
            this.gererContrat(this.contratId);
            this.getEvenements(this.contratId);
        });
    }

    private getEvenements(contratId: number) {
        const eventementCriteria: EvenementCriteriaDTO = {contrat: this.contratId, etatEnAttente: true};
        let eventObservable = this.calendrierService.findEvenements(eventementCriteria, this.contratId, 0, this.nbEvenements, [{property: 'dateDebut', direction: Order.DESC}]);
        eventObservable.subscribe(page => {
            this.listEvenementEnAttente = page.content;
        });
    }

    private gererContrat(contratId: number) {

        this.getActes();
        this.getEchangesChorus();
        this.contratService.findContratById(contratId).subscribe(contrat => {
            this.faqService.currentContratId = contratId;
            this.contrat = contrat;

        });
        this.calendrierService.findEvenements({contrat: contratId,bonsCommande: true,etatValide: true}, contratId, 0, 10, [{direction: Order.DESC, property: 'id'}]).subscribe(page => {
            this.listBonCommandes = page.content.filter(eve => eve.bonCommandeTypeDTO != null);
            this.montantTotalHT = this.listBonCommandes.map(eve => eve.bonCommandeTypeDTO.montantHT).reduce((a, b) => a + b, 0);
            this.montantTotalTVA = this.listBonCommandes.map(eve => eve.bonCommandeTypeDTO.montantTVA).reduce((a, b) => a + b, 0);
        });
    }

    onBackClick() {
        this.router.navigate(['/']).then(() => {
            console.log('navigated');
        });
    }

    onClickDownloadFile(documentId: number) {
        downloadUrl(this.documentService.getDownloadURL(documentId));
    }

    afficheLabelEvenement(evenement: EvenementDTO) {
        let label = `${evenement.typeEvenement.label}`;
        if (evenement.avenantType != null) {
            label = label + ` n° ${evenement.avenantType.numero}`;
        }
        if (evenement.bonCommandeTypeDTO != null) {
            label = label + ` n° ${evenement.bonCommandeTypeDTO.numero}`;
        }
        return label;
    }

    getLibelleStatut(acte: ActeDTO) {
        let result;

        switch (acte.statut) {
            case StatutActe.BROUILLON:
            case 'BROUILLON':
                result = 'Brouillon';
                break;
            case StatutActe.EN_ATTENTE_VALIDATION:
            case 'EN_ATTENTE_VALIDATION':
                result = 'En attente de validation par CHORUS';
                break;
            case StatutActe.EN_ATTENTE_ACCUSE:
            case 'EN_ATTENTE_ACCUSE':
                result = 'En attente de l\'accusé de lecture';
                break;
            case StatutActe.VALIDE:
            case 'VALIDE':
                result = 'Validé par CHORUS';
                break;
            case StatutActe.EN_ERREUR:
            case 'EN_ERREUR':
                result = 'Erreur';
                break;
            case StatutActe.NOTIFIE:
            case 'NOTIFIE':
                result = 'Notifié';
                break;
            case StatutActe.ANNULE:
            case 'ANNULE':
                result = 'Annulé';
                break;
            default:
                result = 'Statut non connu';
        }

        return result;
    }

    downloadChorusZip(echangeChorus: EchangeChorusDTO) {
        this.echangeChorusService.downloadChorusZip(echangeChorus).subscribe(result => {
            const blob = new Blob([result])
            saveAs(blob, `echange-chorus-${echangeChorus?.acte?.numero}.zip`)
        })
    }

    creerEchangeChorus(acte: ActeDTO) {
        console.debug(`Edition de l'échange chorus ${acte}`);

        let modale = this.echangeChorusService.openModal(CreationEchangeChorusComponent);
        modale.content.acte = acte;
        modale.content.echangeChorus = {dateNotificationPrevisionnelle: acte.dateNotification ? new Date(acte.dateNotification) : null};
        modale.content.soumission.subscribe(result => {
            this.echangesCHORUS.push(result);
            this.changeDetectorRef.detectChanges();
        });
    }

    private getActes(): void {
        this.acteService.rechercher({contrat: {id: this.contratId}}, 0, 3,[ {
            direction: Order.DESC,
            property: 'id'
        }]).subscribe(actes => {
            this.nbActes = actes.totalElements;
            this.actes = actes.content;
            console.debug(`${this.actes.length} actes trouvés pour le contrat ${this.contratId} = `, this.actes);
        })
    }

    private getEchangesChorus(): void {
        this.echangeChorusService.rechercher({contrat: this.contratId})
            .subscribe(echanges => this.echangesCHORUS = echanges.content)
    }

    goToAttributaires() {
        this.router.navigate(['/attributaires', this.contrat.id], {queryParams: {from: 'tableauBord'}});
    }

    currentUrl() {
        return this.router.url;
    }
}
