import {Component, Injector, Input, ViewEncapsulation} from '@angular/core';
import {AbstractExecWebComponent} from '@views/web-components/abstract.web.component';

@Component({
    selector: 'ltexec-wc-supervision-donnees-essentielles',
    templateUrl: './wc-supervision-donnees-essentielles.html',
    styleUrls: ['./wc-supervision-donnees-essentielles.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class WcSupervisionDonneesEssentiellesComponent extends AbstractExecWebComponent {
    @Input() habilitationRepublication: boolean;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    get baseNavigationURL(): string {
        return "";
    }

    @Input()
    public set sso(sso: string) {
        super.sso = sso;
    }

    @Input("pf-uid")
    public set pfUid(value: string) {
        super.pfUid = value
    }

    @Input("client")
    public set client(value: string) {
        super.client = value;
    }

    @Input("theme")
    public set theme(value: string) {
        super.theme = value;
    }
}
