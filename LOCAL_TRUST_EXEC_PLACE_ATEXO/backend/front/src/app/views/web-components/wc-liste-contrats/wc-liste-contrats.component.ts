import {Component, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractExecWebComponent} from '@views/web-components/abstract.web.component';
import {ContratCriteriaDTO} from '@services';
import {PerimetreContrat} from '@dto/backendEnum';

@Component({
    selector: 'ltexec-wc-liste-contrats',
    templateUrl: './wc-liste-contrats.component.html',
    styleUrls: ['./wc-liste-contrats.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class WcListeContratsComponent extends AbstractExecWebComponent implements OnInit {

    constructor(injector: Injector) {
        super(injector);
    }

    _filtres: ContratCriteriaDTO = {
        perimetre: PerimetreContrat.MOI
    }

    @Input()
    public set sso(sso: string) {
        super.sso = sso;
    }

    @Input("pf-uid")
    public set pfUid(value: string) {
        super.pfUid = value
    }

    @Input("client")
    public set client(value: string) {
        super.client = value;
    }

    @Input("theme")
    public set theme(value: string) {
        super.theme = value;
    }

    @Input("filtres")
    public set filtres(value: any) {
        this._filtres = super.transposeFilters(value);
    }

    @Input("rechercheavanceeouverte")
    rechercheavanceeouverte: boolean = false;


    ngOnInit(): void {
        super.ngOnInit();
    }

    get baseNavigationURL(): string {
        return `/`;
    }

}
