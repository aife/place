import {
    AuthentificationService,
    ContratCriteriaDTO,
    ParametrageService, PF_UID,
    ReferentielService,
    SSO_TOKEN,
    UtilisateurService
} from '@services';
import {mergeMap} from 'rxjs/operators';
import {Statut, USER_CONTEXT} from '@common/userContext';
import {map} from 'rxjs/operators/map';
import {APPLICATION} from '@common/parametrage';
import {Component, Injector, OnInit, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {ValueLabelDTO} from '@dto';
import {ScrollComponent} from '@components/scroll/scroll.component';
import {FILTRE_CONTRAT, MOTS_CLES} from 'app/constants';


@Component({
    template: ''
})
export abstract class AbstractExecWebComponent implements OnInit {

    loaded = false;
    protected router: Router;
    private authentificationService: AuthentificationService;
    private utilisateurService: UtilisateurService;
    private parametrageService: ParametrageService;
    private referentielService: ReferentielService;
    private listCss: string[];
    private viewContainerRef: ViewContainerRef;

    constructor(private injector: Injector) {
        this.authentificationService = injector.get(AuthentificationService);
        this.utilisateurService = injector.get(UtilisateurService);
        this.parametrageService = injector.get(ParametrageService);
        this.referentielService = injector.get(ReferentielService);
        this.router = injector.get(Router);
        this.viewContainerRef = injector.get(ViewContainerRef);
        this.router.events.subscribe((val) => {
            this._navigate = true;
        })
        this.listCss = [
            "/execution/assets/css/bs-datepicker.css",
            "/execution/assets/css/jquery.growl.css",
            "/execution/assets/css/loader.css"
        ];
        this.listCss.forEach(obj => {
            console.log("Ajout de la feuille de style : " + obj + " via proxypass");
            const font = document.createElement("link");
            font.href = obj;
            font.rel = "stylesheet"
            document.head.appendChild(font);
        });

        var newDiv = document.createElement("div");
        newDiv.className = "modal";
        newDiv.id = "bloc-loader";
        newDiv.setAttribute("data-backdrop", "static");
        newDiv.setAttribute("data-keyboard", "false");
        var loaderDiv = document.createElement("div");
        loaderDiv.className = "loader";
        loaderDiv.textContent = "Chargement...";
        newDiv.appendChild(loaderDiv);
        document.body.appendChild(newDiv);
        this.viewContainerRef.createComponent(ScrollComponent)
    }

    private _navigate = false;

    public get navigate() {
        return this._navigate;
    }

    private _pfUid: string;

    public set pfUid(value: string) {
        this._pfUid = value;
        if (value) {
            localStorage.setItem(PF_UID, value);
        }
    }

    _sso: string;


    public set sso(sso: string) {
        this._sso = sso;
        localStorage.setItem(SSO_TOKEN, sso);
    }

    private _client: string;

    public set client(value: string) {
        this._client = value;
    }

    private _theme: string;

    public set theme(value: string) {
        this._theme = value;
    }

    private flatten(data) {
        if (Object(data) !== data || Array.isArray(data)) {
            return data;
        }
        const regex = /\.?([^.\[\]]+)|\[(\d+)]/g,
            resultholder = {};
        for (const p in data) {
            let cur = resultholder,
                prop = '',
                m;
            while (m = regex.exec(p)) {
                cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}));
                prop = m[2] || m[1];
            }
            cur[prop] = data[p] == 'false' ? false : (data[p] == 'true' ? true : data[p]);
        }
        return resultholder[''] || resultholder;
    }

    authentifier() {
        console.info(`Authentification avec le jeton SSO = ${this._sso} , pfUid = ${this._pfUid} , client = ${this._client}`)
        console.info(`Theme utilisé = ${this._theme}`)
        const observable = this.authentificationService.authorize(this._pfUid, this._sso)
            .pipe(mergeMap(account => {
                USER_CONTEXT.account = account;

                console.debug(`Récupération OK du compte MPE = `, USER_CONTEXT.account);

                return this.authentificationService.login()
            }))
            .pipe(mergeMap(token => {
                console.debug(`Jeton = ${token.access_token}`);

                USER_CONTEXT.authentification.token = token.access_token;
                return this.utilisateurService.getUtilisateurConnecte(this._pfUid);
            }))
            .pipe(mergeMap(utilisateur => {
                console.info(`Récupération OK de l'utilisateur = `, utilisateur);

                USER_CONTEXT.utilisateur = utilisateur;

                if (utilisateur.habilitations) {
                    USER_CONTEXT.habilitations = utilisateur.habilitations.map(habilitation => habilitation.code);
                } else {
                    USER_CONTEXT.habilitations = [];
                }

                USER_CONTEXT.authentification.status = Statut.OK;

                return this.parametrageService.getClientParametrage()
            }))
            .pipe(map(parametrage => this.flatten(parametrage)))
            .pipe(mergeMap(parametrage => {
                APPLICATION.parametrage = parametrage;

                return this.referentielService.chargerTousReferentiels()
            }))
            .pipe(map(_ => {
                console.debug('Remplacement du jeton SSO');

                localStorage.setItem(SSO_TOKEN, this._sso);
                this.applyTheme();
            }));
        observable.subscribe(result => {
            APPLICATION.loaded(true);
            this.loaded = true;
        });
    }

    public ngOnInit(): void {
        this.authentifier();
        AbstractViewComponent.history.push(this.baseNavigationURL);
        // nettoyage du store
        localStorage.removeItem(FILTRE_CONTRAT);
        localStorage.removeItem(MOTS_CLES);
    }

    private applyTheme() {
        // thème place
        let theme = {
            "color-primary": this.getBackgroundColor('navbar-horizontal'),
            "color-secondary": this.getCssProperty('--secondary')
        }
        /*
        theme= {
         "color-link": "#2660a4",
            "color-primary": "#5FC3F0"
            }
                */
        for (const key in theme) {
            document.documentElement.style.setProperty(`--${key}`, theme[key]);
        }
    }

    private getCssProperty(variable: string) {
        return getComputedStyle(document.documentElement).getPropertyValue(variable);
    }

    private getBackgroundColor(cssClass: string) {
        const element = document.getElementsByClassName(cssClass)[0];
        if (element)
            return getComputedStyle(element).getPropertyValue('background-color');
        else
            return '#0E3A5A';

    }

    abstract get baseNavigationURL(): string;

    transposeFilters(value: any) {
        let filtres: ContratCriteriaDTO = {};
        console.log(`injection des filtres depuis le web component => ${value}`);
        if (value != null && typeof value === 'string' && value.length > 0) {
            const filtresMPE = JSON.parse(value);
            if (Array.isArray(filtresMPE)) {
                console.warn(`filtres is an array, it should be an object, please check your code`);
                return filtres;
            }
            // transposer les types de contrats en ValueLabelDTO
            this.transposer(filtresMPE);
            console.log(`injection des filtres dans le composant EXEC => ${JSON.stringify(filtresMPE)}`);
            filtres = filtresMPE;
        }
        if (value != null && typeof value === 'object' && !Array.isArray(value)) {
            this.transposer(value);
            filtres = value;
        }
        return filtres;
    }

    private transposer(filtresMPE: any) {
        if (filtresMPE.typesContrat && typeof filtresMPE.typesContrat === 'object') {
            filtresMPE.typesContrat = filtresMPE.typesContrat.map((t: string) => {
                const vl: ValueLabelDTO = {value: t, label: t};
                return vl;
            });
        }
    }
}
