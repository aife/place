import {Component, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractExecWebComponent} from "@views/web-components/abstract.web.component";

@Component({
    selector: 'ltexec-wc-liste-fournisseurs',
    templateUrl: './wc-liste-fournisseurs.component.html',
    styleUrls: ['./wc-liste-fournisseurs.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class WcListeFournisseursComponent extends AbstractExecWebComponent implements OnInit {
    constructor(injector: Injector) {
        super(injector);
    }

    @Input()
    public set sso(sso: string) {
        super.sso = sso;
    }

    @Input("pf-uid")
    public set pfUid(value: string) {
        super.pfUid = value
    }

    @Input("client")
    public set client(value: string) {
        super.client = value;
    }

    @Input("theme")
    public set theme(value: string) {
        super.theme = value;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.router.navigate(["/fournisseurs"]);
    }

    get baseNavigationURL(): string {
        return `/fournisseurs`;
    }

}
