import {Component, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractExecWebComponent} from "@views/web-components/abstract.web.component";

@Component({
    selector: 'ltexec-wc-fiche-fournisseur',
    templateUrl: './wc-fiche-fournisseur.component.html',
    styleUrls: ['./wc-fiche-fournisseur.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class WcFicheFournisseurComponent extends AbstractExecWebComponent implements OnInit{

    @Input("uuid") uuid: string;

    constructor(injector: Injector) {
        super(injector);
    }

    @Input()
    public set sso(sso: string) {
        super.sso = sso;
    }

    @Input("pf-uid")
    public set pfUid(value: string) {
        super.pfUid = value
    }

    @Input("client")
    public set client(value: string) {
        super.client = value;
    }

    @Input("theme")
    public set theme(value: string) {
        super.theme = value;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    get baseNavigationURL(): string {
        return "ficheFournisseur/uuid/" + this.uuid;
    }
}
