import {Component, EventEmitter, Injector, Input, Output, ViewEncapsulation} from '@angular/core';
import {AbstractExecWebComponent} from '@views/web-components/abstract.web.component';
import {ContratCriteriaDTO} from '@services';
import {PerimetreContrat} from '@dto/backendEnum';
import {ContratDTO} from '@dto';
import {Order, PaginationPropertySort} from '@common/pagination';

@Component({
    selector: 'ltexec-wc-tableau-contrats',
    templateUrl: './wc-tableau-contrats.component.html',
    styleUrls: ['./wc-tableau-contrats.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class WcTableauContratsComponent extends AbstractExecWebComponent {
    constructor(injector: Injector) {
        super(injector);
    }

    sort: PaginationPropertySort[] = [{
        property: 'consultation.idExterne',
        direction: Order.ASC
    }, {property: 'numeroLot', direction: Order.ASC}, {
        property: 'chapeau',
        direction: Order.DESC
    }, {
        property: 'referenceLibre',
        direction: Order.ASC
    }];

    _filtres: ContratCriteriaDTO = {
        perimetre: PerimetreContrat.MOI
    };

    @Input('filtres')
    public set filtres(value: any) {
        this._filtres = super.transposeFilters(value);
    }

    @Input()
    public set sso(sso: string) {
        super.sso = sso;
    }

    @Input('pf-uid')
    public set pfUid(value: string) {
        super.pfUid = value;
    }

    @Input('client')
    public set client(value: string) {
        super.client = value;
    }

    @Input('theme')
    public set theme(value: string) {
        super.theme = value;
    }

    @Output("ondeletecontrat")
    onDeleteContrat = new EventEmitter<ContratDTO>

    get baseNavigationURL(): string {
        return `/`;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}
