import {ContratEtablissementDTO, EtablissementDTO, ValueLabelDTO} from '@dto';

export enum EditionScope {
    Attributaire,
    CoTraitant,
    SousTraitant
}

export enum EditionMode {
    Creation,
    Modification,
    Delete
}

export interface EditionContratEtablissementModel {
    scope: EditionScope;
    mode: EditionMode;
    siret?: string;
    contratEtablissementCommanditaire?: ContratEtablissementDTO;
    contratEtablissement?: ContratEtablissementDTO;
    position?: number;
    etablissement?: EtablissementDTO;
    categorieConsultation?: ValueLabelDTO;
    dateNotification?: Date;
    montant?: number;
    dureeMois?: number;
    revisionPrix?: ValueLabelDTO;
    etablissementFromBase: boolean ;
    idContratEtablissement?: number;
}

export interface EditionGroupementModel {
    mode: EditionMode;

    nomGroupement?: string;
    typeGroupementCode?: any;
}
