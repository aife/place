import {AfterViewChecked, Component, Injector, Input, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AttributaireService, ContratService} from '@services';
import * as Notification from '../../common/notification';
import {EditionContratEtablissementModel, EditionGroupementModel, EditionMode, EditionScope} from './editionDtos';
import {RepartitionMontantsModaleComponent} from './repartitionMontantsModale';
import {
    AttributaireDTO,
    AttributaireEtablissementDTO,
    AttributaireGroupementDTO,
    ContratDTO,
    ContratEtablissementDTO,
    ContratEtablissementGroupementDTO,
    EditionAttributaireDTO,
    ValueLabelDTO
} from '@dto';
import {TypeAttributaire, TypeReferentiel} from '@dto/backendEnum';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {ConfirmationModalComponent} from '@components/modals/ConfirmationModal';
import * as moment from 'moment';

@Component({
    selector: 'ltexec-arborescence-attributaires',
    templateUrl: 'arborescenceAttributaires.html',
    styleUrls: ['./arborescenceAttributaires.scss']
})
export class ArborescenceAttributairesComponent extends AbstractViewComponent implements AfterViewChecked, OnInit {

    @Input() contrat: ContratDTO;

    @Input() visualisation = true;
    @Input() contratId: any;

    editionContratEtablissementModel: EditionContratEtablissementModel;
    editionGroupementModel: EditionGroupementModel;

    attributaire: any = null;

    categorieConsultationOptions: ValueLabelDTO[] = [];
    typeGroupementOptions: ValueLabelDTO[] = [];
    revisionPrixOptions: ValueLabelDTO[] = [];
    editionScope = EditionScope;
    editionMode = EditionMode;
    @ViewChild("repartitionMontantsModaleComponent", {static: false})
    private repartitionModale: RepartitionMontantsModaleComponent;
    private modalRef: BsModalRef;
    isModalAjoutAttributaireShown = false;
    isModalModifierAttributaireShown = false;
    isModalEditionRepartitionShown = false;
    isModifierGroupementShown = false;
    showSuppressionSousTraitant = false;
    showSuppressionCoTraitant = false;

    constructor(
        private store: Store<State>,
        private attributaireService: AttributaireService,
        private contratService: ContratService,
        private injector: Injector
    ) {
        super(injector);
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CATEGORIE).content).subscribe(data => {
            this.categorieConsultationOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_GROUPEMENT).content).subscribe(data => {
            this.typeGroupementOptions = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.REVISION_PRIX).content).subscribe(data => {
            this.revisionPrixOptions = data;
        });
    }

    ngOnInit() {
        this.contratService.findContratById(this.contratId).subscribe(contrat => {
            this.contrat = contrat;
            this.getAroborescenceAttributaires();

        });
    }

    ngAfterViewChecked() {
    }

    isAttributaireGroupement(attributaire: AttributaireDTO): boolean {
        return attributaire.type == TypeAttributaire[TypeAttributaire.Groupement];
    }


    mapModelToViewModel(contratEtablissement: ContratEtablissementDTO, editionContratEtablissementModel: EditionContratEtablissementModel) {
        if (contratEtablissement.categorieConsultation != null) {
            editionContratEtablissementModel.categorieConsultation = contratEtablissement.categorieConsultation;
        }
        editionContratEtablissementModel.dureeMois = contratEtablissement.dureeMois;
        editionContratEtablissementModel.revisionPrix = contratEtablissement.revisionPrix;
        editionContratEtablissementModel.dateNotification = moment(contratEtablissement.dateNotification).toDate();
        editionContratEtablissementModel.montant = contratEtablissement.montant;
        editionContratEtablissementModel.etablissement = contratEtablissement.etablissement;
        editionContratEtablissementModel.contratEtablissement = contratEtablissement;
    }

    mapViewModelToModel(editionContratEtablissementModel: EditionContratEtablissementModel,
                        editionAttributaireDTO: EditionAttributaireDTO) {
        editionAttributaireDTO.dureeMois = editionContratEtablissementModel.dureeMois;
        editionAttributaireDTO.revisionPrix = editionContratEtablissementModel.revisionPrix;
        editionAttributaireDTO.categorieConsultation = editionContratEtablissementModel.categorieConsultation;
        editionAttributaireDTO.dateNotification = moment(editionContratEtablissementModel.dateNotification).toDate();
        editionAttributaireDTO.montant = editionContratEtablissementModel.montant;
        editionAttributaireDTO.etablissement = editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.idContratEtablissement = editionContratEtablissementModel?.contratEtablissement?.id;
    }

    onAjouterSousTraitantClick(contratEtablissementCommanditaire: ContratEtablissementDTO) {
        this.editionContratEtablissementModel = null;
        if (contratEtablissementCommanditaire.montant == null || contratEtablissementCommanditaire.montant === 0) {
            contratEtablissementCommanditaire.montant = this.contrat.montant;
        }
        setTimeout(() => {
            this.editionContratEtablissementModel = {
                etablissementFromBase: true,
                scope: EditionScope.SousTraitant,
                mode: EditionMode.Creation,
                categorieConsultation: null,
                contratEtablissementCommanditaire: contratEtablissementCommanditaire,
                dateNotification: this.contrat.dateNotification
            };
            this.isModalModifierAttributaireShown = true;
        });
    }

    onValiderAjouterSousTraitantClick() {

        const editionAttributaireDTO: EditionAttributaireDTO = {};
        editionAttributaireDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        editionAttributaireDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.commanditaireId = this.editionContratEtablissementModel.contratEtablissementCommanditaire.etablissement.id;
        this.mapViewModelToModel(this.editionContratEtablissementModel, editionAttributaireDTO);
        if(this.etablissementExists(editionAttributaireDTO)){
            return;
        }

        this.attributaireService.ajouterSousTraitant(this.contrat.id, editionAttributaireDTO).subscribe(contratEtablissement => {

            // on ajoute l'objet crée au model view
            this.editionContratEtablissementModel.contratEtablissementCommanditaire.sousTraitants.push(contratEtablissement);
            this.isModalModifierAttributaireShown = false;
            this.isModalAjoutAttributaireShown = false;

        }, e => {
            if (e.status === 409) {
                Notification.warningNotification('L\'établissement est déjà ajouté en tant que contractant sur ce contrat.');
            } else {
                Notification.genericErrorNotification();
            }


        });
    }

    onModifierSousTraitantClick(contratEtablissementCommanditaire: ContratEtablissementDTO,
                                contratEtablissement: ContratEtablissementDTO, position: number) {
        this.editionContratEtablissementModel = {
            etablissementFromBase: false,
            scope: EditionScope.SousTraitant,
            mode: EditionMode.Modification,
            categorieConsultation: null,
            etablissement: contratEtablissement.etablissement,
            contratEtablissement: contratEtablissement,
            contratEtablissementCommanditaire: contratEtablissementCommanditaire,
            position: position
        };
        this.mapModelToViewModel(contratEtablissement, this.editionContratEtablissementModel);
        this.isModalModifierAttributaireShown = true;
    }

    onValiderModifierSousTraitantClick(f: NgForm) {

        const editionAttributaireDTO: EditionAttributaireDTO = {};
        editionAttributaireDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        this.mapViewModelToModel(this.editionContratEtablissementModel, editionAttributaireDTO);


        this.attributaireService.modifierAttributaire(this.contrat.id, editionAttributaireDTO).subscribe(contratEtablissement => {

            // on ajoute l'objet crée au model view
            this.editionContratEtablissementModel.contratEtablissementCommanditaire
                .sousTraitants[this.editionContratEtablissementModel.position] = contratEtablissement;
            this.isModalModifierAttributaireShown = false;

        }, e => {
            Notification.genericErrorNotification();

        });
    }

    onSupprimerSousTraitantClick(contratEtablissementCommanditaire: ContratEtablissementDTO,
                                 contratEtablissement: ContratEtablissementDTO, position: number) {
        this.showSuppressionSousTraitant = true;
        this.editionContratEtablissementModel = null;
        setTimeout(() => {
            this.editionContratEtablissementModel = {
                etablissementFromBase: false,
                scope: EditionScope.SousTraitant,
                mode: EditionMode.Delete,
                contratEtablissementCommanditaire: contratEtablissementCommanditaire,
                contratEtablissement: contratEtablissement,
                etablissement: contratEtablissement.etablissement,
                position: position
            };
        });
    }

    onValiderSuppressionSousTraitantClick() {

        const editionContratEtablissementDTO: EditionAttributaireDTO = {};
        editionContratEtablissementDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionContratEtablissementDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        this.attributaireService.supprimerSousTraitant(this.contrat.id, editionContratEtablissementDTO).subscribe(() => {

            // suppression du model view
            this.editionContratEtablissementModel.contratEtablissementCommanditaire.sousTraitants
                .splice(this.editionContratEtablissementModel.position, 1);
            Notification.warningNotification('Le sous-traitant a été supprimé ');

        }, e => {
            Notification.genericErrorNotification();

        });
        this.showSuppressionSousTraitant = false;
    }

    onAnnulerSelectionEtablissementClick() {
        this.editionContratEtablissementModel.etablissement = null;
        this.editionContratEtablissementModel.siret = null;
    }

    onTransformerEnGroupementClick(contratEtablissement: ContratEtablissementDTO) {
        this.isModifierGroupementShown = true;
        this.editionGroupementModel = null;
        setTimeout(() => {
            this.editionGroupementModel = {mode: EditionMode.Creation, typeGroupementCode: null};
        });
    }

    onValiderTransformerEnGroupementClick() {
        const groupementDTO: ContratEtablissementGroupementDTO = {};
        groupementDTO.nom = this.editionGroupementModel.nomGroupement;
        groupementDTO.type = {value: this.editionGroupementModel.typeGroupementCode};


        this.attributaireService.transformerEnGroupement(this.contrat.id, groupementDTO).subscribe(attributaire => {

            this.attributaire = attributaire;
            this.isModalModifierAttributaireShown = false;

        }, e => {
            Notification.genericErrorNotification();

        });
        this.isModifierGroupementShown = false;
    }

    onModifierGroupementClick() {
        this.editionGroupementModel = null;
        setTimeout(() => {
            this.editionGroupementModel = {mode: EditionMode.Modification};
            this.editionGroupementModel.nomGroupement = (<AttributaireGroupementDTO>this.attributaire).groupement.nom;
            this.editionGroupementModel.typeGroupementCode = (<AttributaireGroupementDTO>this.attributaire).groupement.type.value;
            this.isModifierGroupementShown = true;
        });
    }

    onValiderModifierGroupementClick(f) {
        const groupementDTO: ContratEtablissementGroupementDTO = {};
        groupementDTO.nom = this.editionGroupementModel.nomGroupement;
        groupementDTO.type = {value: this.editionGroupementModel.typeGroupementCode};


        this.attributaireService.modifierGroupement(this.contrat.id, groupementDTO).subscribe(groupement => {
            (<AttributaireGroupementDTO>this.attributaire).groupement = groupement;
        }, e => {
            Notification.genericErrorNotification();

        });
        this.isModifierGroupementShown = false;
    }

    onModifierAttributaireClick() {
        this.editionContratEtablissementModel = {
            scope: EditionScope.Attributaire,
            mode: EditionMode.Modification,
            etablissementFromBase: false
        };

        if (this.isAttributaireGroupement(this.attributaire)) {
            this.editionContratEtablissementModel.contratEtablissement = (<AttributaireGroupementDTO> this.attributaire).mandataire;
        } else {
            // traiter ce cas ?
            this.editionContratEtablissementModel.contratEtablissement =
                (<AttributaireEtablissementDTO> this.attributaire).contratEtablissement;
        }

        this.editionContratEtablissementModel.etablissement = this.editionContratEtablissementModel.contratEtablissement.etablissement;
        this.mapModelToViewModel(this.editionContratEtablissementModel.contratEtablissement, this.editionContratEtablissementModel);
        this.isModalModifierAttributaireShown = true;
    }

    onValiderModifierAttributaireClick() {

        const editionAttributaireDTO: EditionAttributaireDTO = {};
        editionAttributaireDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        this.mapViewModelToModel(this.editionContratEtablissementModel, editionAttributaireDTO);
        if (this.etablissementExists(editionAttributaireDTO)) {
            return;
        }

        this.attributaireService.modifierAttributaire(this.contrat.id, editionAttributaireDTO)
            .subscribe((contratEtablissement: ContratEtablissementDTO) => {
            this.getAroborescenceAttributaires();
            this.isModalModifierAttributaireShown = false;
            this.isModalAjoutAttributaireShown = false;

        }, e => {
            Notification.genericErrorNotification();

            });
    }

    onDegrouperGroupementClick() {
        const modale = this.openModal(ConfirmationModalComponent, {});
        modale.content.title = 'Supprimer un groupement';
        modale.content.message = 'Etes-vous sûr de vouloir supprimer ce groupement ?';
        modale.content.yes.subscribe((e) => {
            this.onValiderDegrouperGroupementClick();
            modale.hide();
        });
    }

    onValiderDegrouperGroupementClick() {

        this.attributaireService.degrouperGroupement(this.contrat.id).subscribe(attributaire => {
            this.attributaire = attributaire;

        }, e => {
            Notification.genericErrorNotification();

        });
    }

    onAjouterCoTraitantClick() {
        this.editionContratEtablissementModel = {
            etablissementFromBase: true,
            scope: EditionScope.CoTraitant,
            mode: EditionMode.Creation,
            categorieConsultation: null,
            montant: 0,
            dateNotification: this.contrat.dateNotification
        };
        this.isModalModifierAttributaireShown = true;
    }

    onValiderAjoutCoTraitantClick() {

        const editionAttributaireDTO: EditionAttributaireDTO = {};
        editionAttributaireDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.dureeMois = this.editionContratEtablissementModel.dureeMois;
        editionAttributaireDTO.revisionPrix = this.editionContratEtablissementModel.revisionPrix;
        editionAttributaireDTO.categorieConsultation = this.editionContratEtablissementModel.categorieConsultation;
        editionAttributaireDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        this.mapViewModelToModel(this.editionContratEtablissementModel, editionAttributaireDTO);
        if(this.etablissementExists(editionAttributaireDTO)){
            return;
        }

        this.attributaireService.ajouterCoTraitant(this.contrat.id, editionAttributaireDTO).subscribe(contratEtablissement => {

            // on ajoute l'objet crée au model view
            (<AttributaireGroupementDTO>this.attributaire).coTraitants.push(contratEtablissement);
            this.repartitionModale.initModale();
            this.isModalAjoutAttributaireShown=false;
            this.isModalModifierAttributaireShown = false;
            this.isModalEditionRepartitionShown=true;

        }, e => {
                Notification.genericErrorNotification();
            this.isModalAjoutAttributaireShown = false;
        });
    }

    onModifierCoTraitantClick(contratEtablissement: ContratEtablissementDTO, position: number) {
        this.editionContratEtablissementModel = {
            etablissementFromBase: false,
            scope: EditionScope.CoTraitant,
            mode: EditionMode.Modification,
            etablissement: contratEtablissement.etablissement,
            contratEtablissement: contratEtablissement,
            position: position
        };
        this.mapModelToViewModel(contratEtablissement, this.editionContratEtablissementModel);
        this.isModalModifierAttributaireShown = true;
    }

    onValiderModifierCoTraitantClick() {

        const editionAttributaireDTO: EditionAttributaireDTO = {};
        editionAttributaireDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionAttributaireDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;
        this.mapViewModelToModel(this.editionContratEtablissementModel, editionAttributaireDTO);
        if(this.etablissementExists(editionAttributaireDTO)){
            return;
        }

        this.attributaireService.modifierAttributaire(this.contrat.id, editionAttributaireDTO)
            .subscribe((contratEtablissement: ContratEtablissementDTO) => {

            // on copie les sous traitants
                contratEtablissement.sousTraitants =
                    (<AttributaireGroupementDTO>this.attributaire)
                        .coTraitants[this.editionContratEtablissementModel.position].sousTraitants;

            // on ajoute l'objet crée au model view
                (<AttributaireGroupementDTO>this.attributaire)
                    .coTraitants[this.editionContratEtablissementModel.position] = contratEtablissement;
                this.isModalModifierAttributaireShown = false;

        }, e => {
            Notification.genericErrorNotification();

            });
    }

    onSupprimerCoTraitantClick(contratEtablissement: ContratEtablissementDTO, position: number) {
        this.showSuppressionCoTraitant = true;
        this.editionContratEtablissementModel = null;
        this.editionContratEtablissementModel = {
            etablissementFromBase: false,
            scope: EditionScope.CoTraitant,
            mode: EditionMode.Delete,
            contratEtablissement: contratEtablissement,
            position: position
        };
    }

    onValiderSuppressionCoTraitantClick() {

        const editionContratEtablissementDTO: EditionAttributaireDTO = {};
        editionContratEtablissementDTO.etablissement = this.editionContratEtablissementModel.etablissement;
        editionContratEtablissementDTO.idContratEtablissement = this.editionContratEtablissementModel?.contratEtablissement?.id;

        this.attributaireService.supprimerCoTraitant(this.contrat.id, editionContratEtablissementDTO).subscribe(contratEtablissement => {
            // suppression du model view
            (<AttributaireGroupementDTO>this.attributaire).coTraitants.splice(this.editionContratEtablissementModel.position, 1);
            this.repartitionModale.initModale();
            this.isModalEditionRepartitionShown = true;
            Notification.warningNotification('Le co-traitant '
                + this.editionContratEtablissementModel.contratEtablissement.etablissement.fournisseur.raisonSociale + ' a été supprimé ');

        }, e => {
            Notification.genericErrorNotification();


        });
        this.showSuppressionCoTraitant = false;
    }

    isEditionGroupementModelValid(): boolean {
        if (this.editionGroupementModel.nomGroupement == null || this.editionGroupementModel.nomGroupement.length === 0) {
            return false;
        }
        if (this.editionGroupementModel.typeGroupementCode == null || this.editionGroupementModel.typeGroupementCode == null) {
            return false;
        }
        return true;
    }

    isEditionContratEtablissementModelValid(f: NgForm): boolean {
        if (this.editionContratEtablissementModel.etablissement == null) {
            return false;
        }
        let valid = false;
        if (this.editionContratEtablissementModel.scope === EditionScope.SousTraitant) {
            if (this.editionContratEtablissementModel.mode === EditionMode.Creation) {
                if (this.editionContratEtablissementModel.etablissement == null) {
                    valid = false;
                } else {
                    valid = true;
                }
            } else if (this.editionContratEtablissementModel.mode === EditionMode.Modification) {
                if (this.editionContratEtablissementModel.etablissement == null) {
                    valid = false;
                } else {
                    valid = true;
                }
            }
        } else if (this.editionContratEtablissementModel.scope === EditionScope.CoTraitant) {
            if (this.editionContratEtablissementModel.mode === EditionMode.Creation) {
                if (this.editionContratEtablissementModel.etablissement == null) {
                    valid = false;
                } else {
                    valid = true;
                }
            } else if (this.editionContratEtablissementModel.mode === EditionMode.Modification) {
                if (this.editionContratEtablissementModel.etablissement == null) {
                    valid = false;
                } else {
                    valid = true;
                }
            }
        } else if (this.editionContratEtablissementModel.scope === EditionScope.Attributaire) {
            if (this.editionContratEtablissementModel.mode === EditionMode.Modification) {
                if (this.editionContratEtablissementModel.etablissement == null) {
                    valid = false;
                } else {
                    valid = true;
                }
            }
        }
        return valid && f.form.valid;
    }
    onModifierRepartitionClick(): void {
        this.repartitionModale.initModale();
        this.isModalEditionRepartitionShown = true;
    }

    getMontantMax(contratEtablissement:ContratEtablissementDTO) {
        const max = this.getValue(contratEtablissement?.montant) - contratEtablissement?.sousTraitants.reduce((accumulator:number, obj:ContratEtablissementDTO) => {
           return this.getValue(accumulator) + this.getValue(obj.montant);
       }, 0);
        console.log(`Montant max = ${max}`)
        return max;
    }
    getValue(n: number): number {
        if(Number.isNaN(n)) return 0;
        return n;
    }

    private getAroborescenceAttributaires() {
        this.attributaireService.getArborescenceAttributaires(this.contratId).subscribe(attributaire => {
            this.attributaire = attributaire;
        });
    }

    private etablissementExists(editionAttributaireDTO: EditionAttributaireDTO) {
        const cotraitantExists = this.attributaire.coTraitants && this.attributaire.coTraitants.find(ce => ce?.etablissement?.id === editionAttributaireDTO?.etablissement?.id);
        const cotraitantMandataireExists = this.attributaire.mandataire && this.attributaire.mandataire.sousTraitants.find(ce => ce?.etablissement?.id === editionAttributaireDTO?.etablissement?.id);
        const exists = cotraitantExists || cotraitantMandataireExists;
        if (exists) {
            Notification.warningNotification('L\'établissement est déjà ajouté en tant que co-traitant/sous-traitant sur ce contrat.');
        }
        return exists;
    }
}
