import {Component, Injector, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContratService, FaqService} from '@services';
import {AttributaireDTO, ContratDTO} from '@dto';
import {AbstractViewComponent} from '@views/abstract.view.component';

@Component({
    selector: 'ltexec-attributaires',
    templateUrl: 'attributaires.html'
})
export class AttributairesComponent extends AbstractViewComponent implements OnInit {

    from: string;
    contratId: any;
    contrat: ContratDTO;
    attributaire: AttributaireDTO = null;
    constructor(private contratService: ContratService, private faqService: FaqService, private injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.activatedRoute.queryParams.subscribe(params => {
                if (params['from']) {
                    this.from = params['from'];
                }
            });
            this.contratId = params['contratId'];
            this.faqService.currentContratId = this.contratId;

            this.contratService.findContratById(this.contratId).subscribe(contrat => {
                this.contrat = contrat;
            });
        });
    }

    retour() {
        if (!!this.from && this.from === 'ficheContrat') {
            return this.router.navigate(['/ficheContrat', this.contratId]);
        }
        else if (!!this.from && this.from === 'tableauBord') {
            return this.router.navigate(['/tableauDeBord', this.contratId]);
        }
        else {
            return this.router.navigate(['/contrats']);
        }
    }

}
