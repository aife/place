import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {from} from 'rxjs/observable/from';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import {reduce} from 'rxjs/operators/reduce';
import {EditionContratEtablissementModel, EditionMode, EditionScope} from './editionDtos';
import * as Notification from '../../common/notification';
import {NgForm} from '@angular/forms';
import {AttributaireService} from '@services';
import {
    AttributaireDTO,
    AttributaireGroupementDTO,
    ContratDTO,
    ContratEtablissementDTO,
    EditionAttributaireDTO
} from '@dto';

@Component({
    selector: 'ltexec-repartition-montants-modale',
    templateUrl: 'repartitionMontantsModale.html'
})
export class RepartitionMontantsModaleComponent implements OnInit {

    @Input() contrat: ContratDTO;
    @Input() attributaire: AttributaireDTO;
    @Input() isModalShown = false;
    @Output() onHidden = new EventEmitter();

    editionScope = EditionScope;
    repartitionObservable: Subject<Array<EditionContratEtablissementModel>>;
    editionRepartitionContratEtablissement: Array<EditionContratEtablissementModel>;
    montantRestant: number;

    constructor(private attributaireService: AttributaireService) {
    }

    ngOnInit(): void {

    }

    mapViewModelToModel(editionContratEtablissementModel: EditionContratEtablissementModel,
                        editionAttributaireDTO: EditionAttributaireDTO) {
        editionAttributaireDTO.dureeMois = editionContratEtablissementModel.dureeMois;
        editionAttributaireDTO.revisionPrix = editionContratEtablissementModel.revisionPrix;
        editionAttributaireDTO.categorieConsultation = editionContratEtablissementModel.categorieConsultation;
        editionAttributaireDTO.dateNotification = editionContratEtablissementModel.dateNotification;
        editionAttributaireDTO.montant = editionContratEtablissementModel.montant;
        editionAttributaireDTO.idContratEtablissement = editionContratEtablissementModel.idContratEtablissement;
    }

    initModale(): void {

        this.editionRepartitionContratEtablissement = [];
        const attributaireGroupement = <AttributaireGroupementDTO>this.attributaire;
        // Ajout du mandataire
        this.editionRepartitionContratEtablissement.push({
            etablissementFromBase: false,
            scope: EditionScope.Attributaire,
            mode: EditionMode.Modification,
            etablissement: attributaireGroupement.mandataire.etablissement,
            dateNotification: attributaireGroupement.mandataire.dateNotification,
            montant: attributaireGroupement.mandataire.montant,
            dureeMois: attributaireGroupement.mandataire.dureeMois,
            revisionPrix: attributaireGroupement.mandataire.revisionPrix,
            categorieConsultation: attributaireGroupement.mandataire.categorieConsultation,
            contratEtablissement: attributaireGroupement.mandataire,
            idContratEtablissement: attributaireGroupement?.mandataire?.id
        });
        // Et des cotraitants
        for (const coTraitant of attributaireGroupement.coTraitants) {
            this.editionRepartitionContratEtablissement.push({
                etablissementFromBase: false,
                scope: EditionScope.CoTraitant,
                mode: EditionMode.Modification,
                etablissement: coTraitant.etablissement,
                dateNotification: coTraitant.dateNotification,
                montant: coTraitant.montant,
                dureeMois: coTraitant.dureeMois,
                revisionPrix: coTraitant.revisionPrix,
                categorieConsultation: coTraitant.categorieConsultation,
                contratEtablissement: coTraitant,
                idContratEtablissement: coTraitant?.id
            });
        }

        this.repartitionObservable = new Subject<Array<EditionContratEtablissementModel>>();
        // On map les array des objets sur un observable de montant
        this.repartitionObservable.pipe(
            startWith(this.editionRepartitionContratEtablissement),
            switchMap((array: Array<EditionContratEtablissementModel>) => from(array).pipe(
                reduce((sum: number, contractant: EditionContratEtablissementModel) => sum + contractant.montant, 0))))
            .subscribe((sum) => {
                this.montantRestant = this.contrat.montant + this.contrat.montantTotalAvenants - Math.round(sum);
            });
    }

    onValiderRepartitionClick(): void {

        const editionAttributaireDTOs: Array<EditionAttributaireDTO> = [];

        for (const editionRepartition of this.editionRepartitionContratEtablissement) {
            const editionAttributaireDTO: EditionAttributaireDTO = {};
            editionAttributaireDTO.etablissement = editionRepartition.etablissement;
            this.mapViewModelToModel(editionRepartition, editionAttributaireDTO);
            editionAttributaireDTOs.push(editionAttributaireDTO);
        }


        this.attributaireService.modifierAttributaires(this.contrat.id, editionAttributaireDTOs)
            .subscribe((contratEtablissements: Array<ContratEtablissementDTO>) => {

            // Mise à jour des données des Contrats Établissement dans la vue
            for (const editionRepartition of this.editionRepartitionContratEtablissement) {
                editionRepartition.contratEtablissement.montant = editionRepartition.montant;
            }
            this.onHidden.emit();

        }, _ => {
            Notification.genericErrorNotification();

            }, () => {

                this.onHidden.emit();
            const attributaireGroupement = <AttributaireGroupementDTO>this.attributaire;
            this.contrat.montantTotalAttributaires = attributaireGroupement.mandataire.montant + attributaireGroupement.coTraitants
                    .filter(contratEtablissement => contratEtablissement.commanditaire == null)
                    .map(a => a.montant)
                    .reduce((a, b) => a + b, 0);
        });


    }

    updateMontantRestant(): void {
        this.repartitionObservable.next(this.editionRepartitionContratEtablissement);
    }

    isRepartitionValid(form: NgForm): boolean {
        return Math.round(this.montantRestant) === 0 && form.form.valid;
    }

}
