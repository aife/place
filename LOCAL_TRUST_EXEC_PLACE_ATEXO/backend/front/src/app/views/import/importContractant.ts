import {AfterViewInit, Component} from '@angular/core';
import {webServiceEndpoint} from '../../constants';
import {USER_CONTEXT} from '@common/userContext';
import {ImportService, UploadService} from '@services';
import {downloadUrl} from '@common/download';
import {genericErrorNotification} from '@common/notification';
import * as Dropzone from 'dropzone';
import {FichierDTO} from '@dto';

@Component({
    selector: 'ltexec-import-contractant',
    templateUrl: 'importContractant.html'
})

export class ImportContractantComponent implements AfterViewInit {
    static dropzone: Dropzone;
    fichier: FichierDTO;
    messageValide: string;
    messageError: string;
    messages: any;

    constructor(private uploadService: UploadService, private importService: ImportService) {

    }

    ngAfterViewInit() {
        const self = this;
        ImportContractantComponent.dropzone = new Dropzone(`#piecesJointes-dropzone`, {
            url: `${webServiceEndpoint}/upload/`,
            headers: {'Authorization': 'Bearer ' + USER_CONTEXT.authentification.token},
            clickable: '#dz-button',
            dictDefaultMessage: 'Déposer les fichiers ici pour les joindre',
            dictFallbackMessage: 'Votre navigateur ne supporte pas le drag-and-drop.',
            dictFallbackText: 'Please use the fallback form below to upload your files like in the olden days.',
            dictFileTooBig: 'Le fichier est trop volumineux ({{filesize}} MiB). Taille maximale: {{maxFilesize}} MiB.',
            dictInvalidFileType: 'Le format du fichier n\'est pas valide. Les formats acceptés sont : xls ou xlsx.',
            dictResponseError: 'Server responded with {{statusCode}} code.',
            dictCancelUpload: 'Annuler',
            dictCancelUploadConfirmation: 'Êtes vous sûr de vouloir annuler ?',
            dictRemoveFile: '<i class=\'fa fa-trash\'></i>',
            dictRemoveFileConfirmation: null,
            dictMaxFilesExceeded: 'Un seul fichier peut être déposé.',
            previewTemplate: '<div style="display:none"></div>',
            previewsContainer: '#dummyPreviewsContainer',
            addRemoveLinks: false,
            maxFilesize: 4,
            paramName: 'uploadfile',
            maxThumbnailFilesize: 5,
            acceptedFiles: '.xls,.xlsx',
            maxFiles: 1
        });
        ImportContractantComponent.dropzone.enable();


        ImportContractantComponent.dropzone.on('success', function (file, response: string) {
            if (self.fichier != null) {
                self.fichier.reference = response;
            }
        });


        ImportContractantComponent.dropzone.on('addedfile', function (file) {
            self.fichier = {nom: file.name, progress: 0};

        });

        ImportContractantComponent.dropzone.on('uploadprogress', function (file, progress, bytesSent) {
            self.fichier.progress = progress;
            self.fichier.taille = bytesSent;
        });
    }

    downlaodPieceJointe(fichier: FichierDTO) {
        downloadUrl(this.uploadService.download(fichier.reference));
    }

    deletePieceJointe(fichier: FichierDTO) {
        this.uploadService.delete(fichier.reference).subscribe(
            () => {
                this.fichier = null;
                this.messageValide = null;
                this.messageError = null;
                ImportContractantComponent.dropzone.removeAllFiles();
            }
        );
    }

    testImport(fichier: FichierDTO) {

        this.importService.testImportContractant(fichier.reference).subscribe(res => {
                this.messageValide = 'Tous les champs ont été validés lors du test d\'import. Le fichier peut être importé.';
            },
            e => {
                if (e.status === 400) {
                    this.messageError = 'Au moins un champ n\'a pas validé le test d\'import. Les erreurs sont détaillées ci-dessous :';
                    this.messages = e.error.second;
                } else {
                    genericErrorNotification();
                }

            });
    }

    importFichier(fichier: FichierDTO) {

        this.importService.importFichierContractant(fichier.reference).subscribe(res => {
                this.messageValide = res.first + ' contractant(s) importé(s) avec succès';
                if (res.second.length !== 0) {
                    this.messageError = 'Des erreurs se sont produites pour certaines lignes :';
                    this.messages = res.second;
                }
            },
            e => {
                if (e.status === 400) {
                    this.messageError = 'Au moins un champ n\'a pas validé le test d\'import. Les erreurs sont détaillées ci-dessous :';
                    this.messages = e.error.second;
                } else {
                    genericErrorNotification();
                }

            });
    }

    downloadModel() {
        downloadUrl('assets/model/Import Contractants - Modele.xlsx');
    }

}
