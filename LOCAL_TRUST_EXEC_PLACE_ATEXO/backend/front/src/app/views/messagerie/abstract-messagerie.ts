import {MessagerieContexte} from "@dto/backend";
import {Component, OnDestroy, OnInit} from "@angular/core";
import {document} from "ngx-bootstrap/utils";
import {AbstractViewComponent} from "@views/abstract.view.component";
import {WEB_COMPONENTS} from "app/constants";

@Component({
    selector: 'ltexec-abstract-messagerie',
    template: '',
})
export abstract class AbstractMessagerie extends AbstractViewComponent implements OnInit, OnDestroy {

    msContexteId = 'ms-contexte';
    jsId = 'messec-js';
    styleMessecId = 'style-messec';
    private _contexte: MessagerieContexte;

    public get contexte() {
        return this._contexte;
    }

    public set contexte(value: MessagerieContexte) {
        this._contexte = value;
    }

    getMessecHtml(balise: string) {
        return `<${balise} id="messec-wc" contexte='${JSON.stringify(this._contexte)}'></${balise}>`
    }

    cleanUp() {
        this.removeById(this.jsId);
        this.removeById(this.msContexteId);
        this.removeById(this.styleMessecId)
    }

    loadAssets() {


        const messecJs = document.getElementById(this.jsId) ?? document.createElement('script');
        messecJs.id = this.jsId;
        messecJs.type = 'text/javascript';
        messecJs.src = '/messagerieSecurisee/front/messec.js?key=' + this.getRandomInt();
        messecJs.text = ``;
        document.body.appendChild(messecJs);
    }

    removeById(id: string) {
        if (document.getElementById(id)) {
            let element = document.getElementById(id);
            element.remove();
            delete document.body[id];
        }
    }

    ngOnInit() {
        this.cleanUp();
    }

    ngOnDestroy() {
        this.cleanUp();
    }

    getRandomInt() {
        const min = Math.ceil(100000);
        const max = Math.floor(min * 10);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    getWebComponent() {
        for (const wc of WEB_COMPONENTS) {
            const webComponent = document.getElementsByTagName(wc);
            if(webComponent.length > 0) {
                const execWC = webComponent[0];
                console.log(`Mode web component détecté: ${wc}`);
                const messecWC = execWC.shadowRoot.getElementById('messec-wc');
                return messecWC;
            }

        }
        return document.getElementById('messec-wc');
    }
}
