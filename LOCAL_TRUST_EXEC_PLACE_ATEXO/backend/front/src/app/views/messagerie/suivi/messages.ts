import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ContratService, FaqService, MessagerieService} from '@services';
import {ActivatedRoute} from '@angular/router';
import {genericErrorNotification} from '@common/notification';
import {ContratDTO} from '@dto';
import {AbstractMessagerie} from "app/views/messagerie/abstract-messagerie";
import {AbstractViewComponent} from "@views/abstract.view.component";

@Component({
    selector: 'ltexec-messages',
    templateUrl: 'messages.html',
    styleUrls: ['messages.scss']
})
export class MessagesHolderComponent extends AbstractMessagerie implements OnInit, AfterViewInit {

    contratId: any;
    acteId: any;
    contrat: ContratDTO;
    params: any;
    from: string;

    constructor(public messagerieService: MessagerieService, public contratService: ContratService,
                private injector: Injector,
                private faqService: FaqService) {
        super(injector);
    }

    ngOnInit() {
        super.ngOnInit();
        this.activatedRoute.queryParams.subscribe(params => {
            this.from = params?.from;
        });
        this.activatedRoute.params.subscribe(params => {

            this.params = params;
            this.contratId = params['contratId'];
            this.acteId = params['acteId']
            this.faqService.currentContratId = this.contratId;
            const contratObservable: Observable<ContratDTO> = this.contratService.findContratById(this.contratId);

            contratObservable.subscribe(contrat => {
                if (this.contratId) {
                    let contexteObservable = this.messagerieService.initTokenSuivi(this.contratId);

                    if (this.acteId) {
                        contexteObservable = this.messagerieService.initTokenSuiviNotifications(this.contratId, this.acteId);
                    }

                    // Init résultats à l'affichage de la page
                    contexteObservable.subscribe(t => {
                        this.contexte = t;
                        this.loadAssets();
                    });

                    this.contrat = contrat;
                }

            }, e => {
                genericErrorNotification();

            });

        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            const messecNE = this.getWebComponent();
            if (messecNE) {
                messecNE.addEventListener('onnouveaumessage', (event: any) => {
                    this.nouveauMessage();
                });
                messecNE.addEventListener('onmodifiermessage', (event: any) => {
                    console.log(`onModifierMessage ${event}`)
                    if(typeof event === 'string'){
                        this.editerMessage(event);
                    }else {
                        this.editerMessage(event.detail);
                    }
                });
            }
        }, 2000);

    }


    nouveauMessage() {
        this.router.navigate(['/envoiMessage', this.contratId]);
    }

    editerMessage(codeLien) {
        this.router.navigate(['/envoiMessage', this.contratId, codeLien]);
    }

    getScript() {
        return `<script type="javascript"> let ms_contexte = ${JSON.stringify(this.contexte)} </script>`
    }

    onBackClick() {
        super.goBack();
    }
}
