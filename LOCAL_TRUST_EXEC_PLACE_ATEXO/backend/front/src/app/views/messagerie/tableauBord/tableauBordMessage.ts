import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContratDTO, MessagerieContexte} from '../../../external/backend';
import {switchMap} from 'rxjs/operators/switchMap';
import {MessagerieService} from '@services/messagerie.service';

@Component({
    selector: 'ltexec-message-tableau-bord',
    templateUrl: 'tableauBordMessage.html'
})
export class TableauBordMessageComponent implements OnInit {

    contratId: any;

    @Input() contrat: ContratDTO;

    token: string;

    favoriActive = false;

    constructor(private messagerieServiceProvider: MessagerieService, private router: Router, private activatedRoute: ActivatedRoute) {

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.contratId = params['contratId'];
            this.getMessageStatusAll();
        });
    }


    nouveauMessage() {
        this.router.navigate(['/envoiMessage', this.contratId], { queryParams: { from: this.currentUrl() } });
    }


    getMessageStatusAll() {
        this.favoriActive = false;
        if (this.contratId) {
            this.messagerieServiceProvider.initTokenSuivi(this.contratId).pipe(switchMap((contexte: MessagerieContexte) => {
                this.token = contexte?.token;
                return this.messagerieServiceProvider.countMessageStatus(this.token, this.contratId);
            })).subscribe((messageStatus) => {
                this.contrat.messageStatus = messageStatus;
            });
        }
    }

    currentUrl() {
        return this.router.url;
    }
}
