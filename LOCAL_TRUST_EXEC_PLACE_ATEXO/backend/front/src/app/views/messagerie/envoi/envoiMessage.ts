import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {AfterViewInit, Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MessagerieService} from '@services/messagerie.service';
import {ActeService, ContratService, DocumentService, FaqService} from '@services';
import {ActivatedRoute} from '@angular/router';
import {ActeDTO, ContratDTO, DocumentContratDTO, DocumentMessecDTO} from '@dto';
import {AbstractMessagerie} from 'app/views/messagerie/abstract-messagerie';
import {USER_CONTEXT} from '@common/userContext';
import {StatutActe} from '@dto/backendEnum';
import {AbstractViewComponent} from "@views/abstract.view.component";

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-envoi-message',
    templateUrl: 'envoiMessage.html'
})
export class EnvoiMessageComponent extends AbstractMessagerie implements OnInit, AfterViewInit {

    contratId: any;
    acteId: any;
    acte: ActeDTO;
    contrat: ContratDTO;
    documents: DocumentContratDTO[] = [];
    notification = false;
    showEspaceDocumentaire = false;

    @Input() idContratInput: number = null;
    @Input() notificationInput: boolean = false;
    @Output() closeModal = new EventEmitter<ContratDTO>();
    @Input() masquerNavigation: boolean = false;

    constructor(public messagerieService: MessagerieService, public contratService: ContratService, private acteService: ActeService,
                private faqService: FaqService, private baseDocumentService: DocumentService, private injector: Injector) {
        super(injector);
    }

    ngOnInit() {

        super.ngOnInit();
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['notification'] || this.notificationInput) {
                this.notification = true;
                console.log(`notification du contrat ${this.contratId} via messagerie sécurisée`);
            }
        });
        this.activatedRoute.params.subscribe(params => {

            this.contratId = this.idContratInput == null ? params['contratId'] : this.idContratInput;
            this.acteId = params['acteId'];
            if (this.acteId) {
                this.acteService.recuperer(this.acteId).subscribe(acte => {
                    this.acte = acte;
                });
            }
            this.faqService.currentContratId = this.contratId;
            let contexteObservable: Observable<any> = null;

            if (params['codeLien'] != null) { // edition
                contexteObservable = this.messagerieService.initTokenRedactionByCodeLien(this.contratId, params['codeLien']);
            } else if (params['acteId']) { // création
                contexteObservable = this.messagerieService.initTokenNotification(this.contratId, this.acteId);
            } else {
                if (this.notification) {
                    contexteObservable = this.messagerieService.initTokenNotification(this.contratId, null);
                } else {
                    contexteObservable = this.messagerieService.initTokenRedaction(this.contratId);
                }
            }

            const contratObservable = this.contratService.findContratById(this.contratId);
            contratObservable.subscribe(contrat => {
                this.contrat = contrat;
            });
            const documentsObservable = this.baseDocumentService.find(this.contratId, {}, 0, 1000, null);

            documentsObservable.subscribe(documentsPage => {
                this.documents = documentsPage.content;
                contexteObservable.subscribe(t => {
                    this.contexte = t;
                    if (this.documents.length > 0) {
                        this.contexte.id_espace_doc_modal = 'espace-documentaire';
                    }
                    this.loadAssets();
                });
            }, e => {
            });

        });

    }

    onSelectedDocs(docs: DocumentContratDTO[]) {
        const selectionEvent = 'espace-documentaire-documents-selectionnes';
        console.debug(`documents sélectionnés => ${JSON.stringify(docs)}`);
        const docsMessec: DocumentMessecDTO[] = docs.map(d => {
            let docMessec: DocumentMessecDTO = {
                nom: d.fichier.nom,
                id: d.id,
                poids: d.fichier.taille,
                url_telechargement: this.baseDocumentService.getDownloadURL(d.id) + '?access_token=' + USER_CONTEXT.authentification.token
            };
            return docMessec;
        });
        localStorage.setItem(selectionEvent, JSON.stringify(docsMessec));
    }

    ngAfterViewInit() {
        setTimeout(() => {
            const messecNE = this.getWebComponent();
            if (messecNE) {
                messecNE.addEventListener('onquitterredaction', (event: any) => {
                    const message = event.detail;
                    if (message && message.brouillon === true) {
                        this.redirect();
                    } else if (this.acte) {
                        this.acte.statut = StatutActe.EN_ATTENTE_ACCUSE;
                        this.acteService.modifier(this.acte).subscribe((acte) => {
                            console.log('acte modifié, uuid du message de notification : ' + message.uuid);
                            this.fermerMessec();
                        });
                    }
                    if (!message || !message.brouillon) {
                        this.fermerMessec();
                    }
                });
                messecNE.addEventListener('onpiecejointesupprimee', (event: any) => {
                    console.log(`onpiecejointesupprimee ${event}`);
                    let pjAsupprimer = event;
                    if (event.detail && typeof event.detail === 'object') {
                        pjAsupprimer = event.detail;
                    }
                    console.log(`onpiecejointesupprimee ${JSON.stringify(pjAsupprimer)}`);
                    for (const doc of this.documents) {
                        if (doc?.fichier?.nom === pjAsupprimer.nom) {
                            doc.preSelected = false;
                        }
                    }
                });
                messecNE.addEventListener('onopenespacedocumentaire', () => {
                    this.showEspaceDocumentaire = true;
                });
            }
        }, 2000);

    }

    fermerMessec() {
        if (this.idContratInput != null) {
            this.closeModal.emit();
        } else {
            this.redirect();
        }
    }

    private redirect() {
        if (this.acteId) {
            this.router.navigate(['/messages', this.contratId, this.acteId]).then(() => {
                console.log('redirection vers acte');
            });
        } else {
            this.router.navigate(['/messages', this.contratId]).then(() => {
                console.log('redirection vers contrat');
            });
        }
    }

}
