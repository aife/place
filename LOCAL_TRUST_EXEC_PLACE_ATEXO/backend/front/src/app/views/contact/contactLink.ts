import {AfterViewInit, Component, Input} from '@angular/core';
import {ContactDTO} from "@dto/backend";


@Component({
    selector: 'ltexec-contact-link',
    templateUrl: 'contactLink.html'
})
export class ContactLinkComponent implements AfterViewInit {

    static identifier_ = 0;

    @Input() contact: ContactDTO;

    identifier = 0;

    constructor() {
        ContactLinkComponent.identifier_++;
        this.identifier = ContactLinkComponent.identifier_;
    }

    ngAfterViewInit() {

        $(`#detail_contact_${this.identifier}`).popover({
            html: true,
            content: function () {
                const content = $(this).attr('id') + '_popover';
                return $('#' + content).children('.popover-body').html();
            },
            title: function () {
                const content = $(this).attr('id') + '_popover';
                return $('#' + content).children('.popover-heading').html();
            }

        }).on('click', function (e) {
            e.preventDefault();
        });
    }
}
