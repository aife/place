import { Component, Injector, OnInit } from '@angular/core';
import * as Notification from '../../common/notification';
import { AttributaireService, ContactService, ContratService } from '@services';
import { APPLICATION } from '@common/parametrage';
import {
    AttributaireDTO,
    AttributaireEtablissementDTO,
    AttributaireGroupementDTO,
    ContactDTO, ContactPrincipalDTO,
    ContactReferantDTO,
    ContratDTO,
    ContratEtablissementDTO,
    EtablissementDTO
} from '@dto';
import { TypeAttributaire } from '@dto/backendEnum';
import { AbstractViewComponent } from '@views/abstract.view.component';
import { Params } from '@angular/router';
import { v4 as uuid } from 'uuid';
import { NgForm } from '@angular/forms';

enum TypeContact {
    Nouveau= 'NOUVEAU', Libre = 'LIBRE', Existant = 'EXISTANT'
}

interface AjoutModale {
    type?: TypeContact;
    id?: number;
    nom?: string;
    prenom?: string;
    fonction?: string;
    email?: string;
    telephone?: string;
    role?: string;
}

interface ModificationModale {
    id?: number;
    nom?: string;
    prenom?: string;
    fonction?: string;
    email?: string;
    telephone?: string;
    role?: string;
    contactReferent?: ContactReferantDTO;
    contactPrincipal?: ContactPrincipalDTO;
}

interface SuppressionModale {
    contactReferent?: ContactReferantDTO;
}

@Component({
    selector: 'ltexec-contacts-referents',
    templateUrl: 'contactsReferents.html',
    styleUrls: ['./contactsReferents.scss']
})


export class ContactsReferantsComponent extends AbstractViewComponent implements OnInit {
    protected readonly TypeContact = TypeContact;
    protected readonly DEFAULT_ROLE: string = 'Soumissionaire';

    contrat: ContratDTO;
    etablissement: EtablissementDTO;
    contratEtablissements: ContratEtablissementDTO[];
    contratEtablissement: ContratEtablissementDTO;
    attributaire: AttributaireDTO;
    contactsReferents: ContactReferantDTO[] = [];
    ajoutModale: AjoutModale;
    modificationModale: ModificationModale;
    suppressionModale: SuppressionModale;
    contactExterneActif: Boolean = true;
    contactPrincipal: ContactPrincipalDTO;
    contratEtablissementId : number;
    contactsExistants: ContactDTO[];

    constructor(
        private contactService: ContactService,
        private contratService: ContratService,
        private attributaireService: AttributaireService,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.activatedRoute.params.subscribe((params: Params): void => {
            const contratId = params['contratId'];
            this.contratEtablissementId = params['etablissementId'];

            this.contratService.findContratById(contratId)
                .subscribe(
                    ( contrat: ContratDTO ): void => {
                        this.contrat = contrat;

                        this.contratService.findEtablissementsById(this.contrat.id).subscribe((contratEtablissements: ContratEtablissementDTO[]): void => {
                                this.contratEtablissements = contratEtablissements;
                                this.contratEtablissement = contratEtablissements.find((contratEtablissement: ContratEtablissementDTO): boolean => contratEtablissement.id == this.contratEtablissementId);

                                if (!this.contratEtablissement) {
                                    this.retour();
                                }

                                this.etablissement = this.contratEtablissement.etablissement;

                                this.contactService.findContactsByEtablissement(this.etablissement.id).subscribe(
                                    (contacts: ContactDTO[]) => this.contactsExistants = contacts
                                );
                            }
                        );

                        this.contactService.recupererContactPrincipal(this.contrat.attributaire).subscribe(
                            (contratPrincipal: ContactPrincipalDTO | undefined): void => {
                                if (contratPrincipal && contratPrincipal.contact) {
                                    this.contactPrincipal = contratPrincipal;
                                }
                            }
                        )

                        this.attributaireService.getArborescenceAttributaires(this.contrat.id).subscribe((attributaire: AttributaireDTO) => this.attributaire = attributaire);

                        this.contactService.findContactByContratEtablissement(this.contratEtablissementId).subscribe((contacts: ContactDTO[]): void => {
                            this.contactsReferents = contacts;

                            this.contactsReferents.forEach((contactReferent: ContactReferantDTO) => contactReferent.uuid = uuid());

                            this.contactsReferents.sort((c1: ContactReferantDTO, c2: ContactReferantDTO): number => c1.contact.id < c2.contact.id ? 1 : 0);
                        });

                        APPLICATION.loading().subscribe(_ => this.contactExterneActif = APPLICATION.parametrage?.contact?.externe?.actif);


                    }
                );

        });
    }


    recupererTypeEtablissement(): string {
        if (this.attributaire.type === TypeAttributaire[TypeAttributaire.Groupement]) {
            const groupement: AttributaireGroupementDTO = this.attributaire as AttributaireGroupementDTO;

            if (groupement.mandataire.id === this.contratEtablissementId) {
                return 'Mandataire :';
            }
            if (groupement.coTraitants.some((coTraitant: ContratEtablissementDTO): boolean => coTraitant.id == this.contratEtablissementId)) {
                return 'Co-traitant :';
            }

            return 'Sous-traitant :';
        } else {
            const etablissement: AttributaireEtablissementDTO = this.attributaire as AttributaireEtablissementDTO;

            if (etablissement.contratEtablissement.id === this.contratEtablissementId) {
                return 'Titulaire :';
            }
            return 'Sous-traitant :';
        }
    }

    existe(contact: ContactDTO): boolean {
        return this.contactsReferents != null && this.contactsReferents.some((contactReferent: ContactReferantDTO): boolean => contactReferent.contact.id === contact.id);
    }

    demanderModificationContactPrincipal(contactPrincipal: ContactPrincipalDTO): void {
        this.modificationModale = {
            contactPrincipal: contactPrincipal,
            // Remplissage des valeurs par défaut
            email: contactPrincipal.contact.email,
            prenom: contactPrincipal.contact.prenom,
            nom: contactPrincipal.contact.nom,
            id: contactPrincipal.contact.id,
            fonction: contactPrincipal.contact.fonction,
            telephone: contactPrincipal.contact.telephone,
            role: contactPrincipal.roleContrat
        };
    }

    demanderModificationContactReferent(contactReferent: ContactReferantDTO): void {
        this.modificationModale = {
            contactReferent: contactReferent,
            // Remplissage des valeurs par défaut
            email: contactReferent.contact.email,
            prenom: contactReferent.contact.prenom,
            nom: contactReferent.contact.nom,
            id: contactReferent.contact.id,
            fonction: contactReferent.contact.fonction,
            telephone: contactReferent.contact.telephone,
            role: contactReferent.roleContrat
        };

    }

    modifier(): void {
        if (this.modificationModale.contactPrincipal) {
            let contactPrincipal: ContactPrincipalDTO = this.modificationModale.contactPrincipal;

            contactPrincipal.contact.email = this.modificationModale.email;
            contactPrincipal.contact.prenom = this.modificationModale.prenom;
            contactPrincipal.contact.nom = this.modificationModale.nom;
            contactPrincipal.contact.fonction = this.modificationModale.fonction;
            contactPrincipal.contact.telephone = this.modificationModale.telephone;

            this.contactService.modifierContactPrincipal(contactPrincipal)
                .subscribe( (ContactPrincipal: ContactPrincipalDTO) => {
                    Notification.messageNotification(`Le contact référent principal ${ContactPrincipal.contact.prenom} ${ContactPrincipal.contact.nom} a été modifié avec succès`);

                    this.modificationModale = undefined;
                });
        } else if (this.modificationModale.contactReferent) {
            let contactReferent: ContactReferantDTO = this.modificationModale.contactReferent;

            contactReferent.contact.email = this.modificationModale.email;
            contactReferent.contact.prenom = this.modificationModale.prenom;
            contactReferent.contact.nom = this.modificationModale.nom;
            contactReferent.contact.fonction = this.modificationModale.fonction;
            contactReferent.contact.telephone = this.modificationModale.telephone;

            contactReferent.roleContrat = this.modificationModale.role;
            this.contactService.modifierContactReferant(contactReferent)
                .subscribe( (contactReferent: ContactReferantDTO) => {
                    Notification.messageNotification(`Le contact référent ${contactReferent.contact.prenom} ${contactReferent.contact.nom} a été modifié avec succès`);

                    this.modificationModale = undefined;
                });
        }

    }

    demanderSuppression(contactReferent: ContactReferantDTO): void {
        this.suppressionModale = {
            contactReferent: contactReferent
        };
    }

    supprimer(): void {
        this.contactService.supprimerContactReferant(this.contratEtablissementId, this.suppressionModale.contactReferent.contact.id)
            .subscribe(_ => {
                this.contactsReferents = this.contactsReferents.filter((contactReferent: ContactReferantDTO): boolean => contactReferent.uuid !== this.suppressionModale.contactReferent.uuid)
                Notification.messageNotification(`Le contact référent ${this.suppressionModale.contactReferent.contact.prenom} ${this.suppressionModale.contactReferent.contact.nom} a été supprimé`);
                this.suppressionModale = undefined;
            });
    }

    demanderAjout(): void {
        this.ajoutModale = {
            type: TypeContact.Existant
        };

    }

    ajouter(): void {
        let contact: ContactDTO = {};

        if (this.ajoutModale.type === TypeContact.Existant) {
            contact = this.contactsExistants.find((contact: ContactDTO): boolean => contact.id === this.ajoutModale.id);
        } else {
            contact.email = this.ajoutModale.email;
            contact.nom = this.ajoutModale.nom;
            contact.prenom = this.ajoutModale.prenom;
            contact.telephone = this.ajoutModale.telephone;
            contact.prenom = this.ajoutModale.prenom;
            contact.fonction = this.ajoutModale.fonction;
            contact.etablissement = this.etablissement;
        }

        if (!this.contactPrincipal && this.contactsReferents.length === 0) {
            console.debug('Aucun contact, ni référent ni principal. Ajout du contact principal');

            const contactPrincipal: ContactPrincipalDTO = {
                contact: contact,
                contratEtablissement: this.contratEtablissement,
                roleContrat: this.DEFAULT_ROLE
            };

            this.contactService.ajouterContactPrincipal(contactPrincipal).subscribe((nouveauContactPrincipal: ContactPrincipalDTO): void => {
                    this.contactPrincipal = nouveauContactPrincipal;

                    Notification.messageNotification(`Le contact référent principal ${nouveauContactPrincipal.contact.prenom} ${nouveauContactPrincipal.contact.nom} a été ajouté`);
                    this.ajoutModale = undefined;
                }
            );
        } else {
            if (this.ajoutModale.type === TypeContact.Libre) {
                contact.etablissement = null;
            }

            const contactReferant: ContactReferantDTO = {
                contact: contact,
                contratEtablissement: this.contratEtablissement,
                roleContrat: this.ajoutModale.role
            };

            this.contactService.ajouterContactReferant(contactReferant)
                .subscribe((nouveauContact: ContactReferantDTO): void => {
                    this.contactsReferents.push(nouveauContact);
                    if (this.ajoutModale.type === TypeContact.Nouveau) {
                        this.etablissement.contacts.push(nouveauContact.contact);
                    }
                    Notification.messageNotification(`Le contact référent ${nouveauContact.contact.prenom} ${nouveauContact.contact.nom} a été ajouté`);
                    this.ajoutModale = undefined;
                });
        }
    }

    retour() {
        return this.router.navigate(['/attributaires', this.contrat.id]);
    }

    changeTypeContact(type: TypeContact): void {
        this.ajoutModale = {
            type: type
        };
    }

    selectionContactExistant(target: any) {
        const i = parseInt(target.value);
        const contact: ContactDTO = this.contactsExistants.find((contact: ContactDTO): boolean => contact.id === i);

        if (contact) {
            this.ajoutModale.id = contact.id;
            this.ajoutModale.email = contact.email;
            this.ajoutModale.prenom = contact.prenom;
            this.ajoutModale.nom = contact.nom;
            this.ajoutModale.fonction = contact.fonction;
            this.ajoutModale.telephone = contact.telephone;
            this.ajoutModale.role = contact.fonction;
        }

    }

    formulaireAjoutEstOK(ngForm: NgForm): boolean {
        return ngForm.form.valid && ( this.ajoutModale.type !== TypeContact.Existant || !!this.ajoutModale.id);
    }

    formulaireModificationEstOK(ngForm: NgForm): boolean {
        return ngForm.form.valid;
    }
}
