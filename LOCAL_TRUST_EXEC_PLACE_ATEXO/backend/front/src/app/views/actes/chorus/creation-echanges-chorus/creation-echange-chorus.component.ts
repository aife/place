import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActeDTO, EchangeChorusDTO} from '@dto';
import {EchangeChorusService} from '@services/echange-chorus.service';

export enum Etat {
    INITIAL,
    SOUMISSION
}

@Component({
    selector: 'ltexec-creation-echange-chorus',
    templateUrl: './creation-echange-chorus.component.html',
    styleUrls: ['./creation-echange-chorus.component.scss']
})
export class CreationEchangeChorusComponent implements OnInit {

    @Input() public acte: ActeDTO;
    @Input() public echangeChorus: EchangeChorusDTO;
    @Output() public soumission: EventEmitter<EchangeChorusDTO> = new EventEmitter<EchangeChorusDTO>();
    @Output() public onClose = new EventEmitter<any>();
    public erreur: string;
    public etat: Etat;

    constructor(private echangeChorusService: EchangeChorusService) {
    }

    soumettre(): void {
        this.etat = Etat.SOUMISSION;

        this.echangeChorusService.soumettre(this.acte, this.echangeChorus).subscribe( echange => {
            console.log(`Soumission de l'échange CHORUS`);

            this.etat = Etat.INITIAL;

            this.soumission.emit(echange);

            this.fermer();
        }, error => {
            console.error('Erreur lors de la validation CHORUS', error);

            this.erreur = `Une erreur s'est produite lors de la validation`;
        });
    }

    fermer(): void {
        this.onClose.emit();
    }

    estValide() {
        return this.echangeChorus?.dateNotificationPrevisionnelle && this.etat == Etat.INITIAL;
    }

    ngOnInit(): void {
        this.etat = Etat.INITIAL;
    }
}
