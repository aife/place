import {Component, ComponentFactoryResolver, Injector, Input, OnInit, ViewChild} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ActeService, ContratService} from '@services';
import {APPLICATION_CONTEXT} from '@common/applicationContext';
import {ActeDTO, ContratDTO, FournisseurDTO, ValueLabelDTO} from '@dto';
import {CreationActeComponent, FormulaireActeDirective, TypeFormulaireActe} from '@views/actes/forms';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {TypeReferentiel} from '@dto/backendEnum';
import {AbstractViewComponent} from '@views/abstract.view.component';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-acte-creation',
    templateUrl: 'acte-creation.component.html',
    styleUrls: ['./acte-creation.component.scss']
})

export class ActeCreationComponent extends AbstractViewComponent implements OnInit {
    mode: string;
    siren: String;
    fournisseur: FournisseurDTO;
    ajoutEtranger: boolean;
    idNational: String;
    typesActe: ValueLabelDTO[] = [];
    pays: string;
    raisonSociale: String;
    editionModeCount = 0;
    fromContrat: boolean;
    selectionMode: boolean = APPLICATION_CONTEXT.modeSelectionContact;
    contrat: ContratDTO = {};
    actes: ActeDTO[] = [];
    acte: ActeDTO;
    type: string;

    @Input() formulairesCreationActe: TypeFormulaireActe[] = [];

    @ViewChild(FormulaireActeDirective, {static: true}) formulaireHost!: FormulaireActeDirective;

    constructor(
        private contratService: ContratService,
        private store: Store<State>,
        private componentFactoryResolver: ComponentFactoryResolver,
        private acteService: ActeService,
        private injector: Injector
    ) {
        super(injector);
    }

    chargerFormulaire(code: string) {
        console.log(`Chargement du formulaire depuis le code ${code}`);

        const formulaire = this.recuperer(code);

        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formulaire.component);

        const viewContainerRef = this.formulaireHost.viewContainerRef;
        viewContainerRef.clear();
        const component = viewContainerRef.createComponent<CreationActeComponent>(componentFactory);
        component.instance.contrat = this.contrat;
        component.instance.actes = this.actes;
        component.instance.acte = this.acte;
        component.instance.mode = this.mode;
        component.instance.type = formulaire;
    }

    ngOnInit(): void {
        this.activatedRoute
            .data
            .subscribe(data => {
                this.mode = data.mode;
            });

        this.activatedRoute.params.subscribe(params => {
            this.contratService.findContratById(params['contratId']).subscribe(
                (contrat: ContratDTO) => {
                    this.contrat = contrat;

                    this.acteService.recupererPourContrat(this.contrat).subscribe(
                        actes => {
                            this.actes = actes;

                            console.debug('Chargement des formulaires des actes');

                            this.acteService.recupererFormulairesActe().subscribe(
                                formulaires => {
                                    this.formulairesCreationActe = formulaires;

                                    this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_ACTE).content).subscribe(data => {
                                        this.typesActe = this.contrat.concession ?
                                            data.filter(acte => this.existe(acte.value) && acte.value !== 'ACT_AST')
                                            : data.filter(acte => this.existe(acte.value) && acte.value !== 'ACT_DE');

                                        console.debug('Chargement des types d\'actes =', this.typesActe);

                                        if (params['acteId']) {
                                            this.acteService.recuperer(params['acteId']).subscribe(
                                                (acte: ActeDTO) => {
                                                    this.acte = acte;

                                                    this.type = this.acte.type.value;

                                                    this.majTypeActe();
                                                }
                                            );
                                        }
                                    });
                                }
                            );
                        }
                    );
                }
            );
        });
    }

    private recuperer(code: string): TypeFormulaireActe {
        return this.formulairesCreationActe.filter((formulaire: TypeFormulaireActe) => formulaire.code === code).pop();
    }

    public majTypeActe(): void {
        console.debug(`Changement du type d'acte pour ${this.type}`);

        this.chargerFormulaire(this.type);
    }

    public retour(): void {
        this.goBack();
    }

    private existe(code: string): boolean {
        return this.formulairesCreationActe.filter((formulaire: TypeFormulaireActe) => formulaire.code === code).length > 0;
    }
}
