import {ChangeDetectorRef, Component, Injector, OnInit} from '@angular/core';
import {Table} from '@components/table/table';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {ActeDTO, ContratDTO, DocumentActeDTO, EchangeChorusDTO} from '@dto';
import {StatutActe} from '@dto/backendEnum';
import {defaultItemsCountPerPage, EXEC_SORT} from '../../../constants';
import {isBlank} from '@common/validation';
import {Observable} from 'rxjs/Observable';
import {ActeCriteriaDTO, ActeService, ContratService} from '@services';
import {saveAs} from 'file-saver';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeAnnulationComponent} from '@views/actes/acte-annulation/acte-annulation.component';
import {ConfirmationModalComponent} from '@components/modals/ConfirmationModal';
import {AbstractViewComponent} from '@views/abstract.view.component';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-acte-tableau-bord',
    templateUrl: 'acte-tableau-bord.component.html',
    styleUrls: ['./acte-tableau-bord.component.scss']
})
export class ActeTableauDeBordComponent extends AbstractViewComponent implements Table, OnInit {

    criteria: ActeCriteriaDTO = {};
    actes: PaginationPage<ActeDTO>;

    sort: PaginationPropertySort[] = [{direction: Order.ASC, property: 'numero'}];
    params: any;
    self: any;
    contrat: ContratDTO;
    selectedActe: ActeDTO;
    echnageChorus: EchangeChorusDTO;
    isNotificationModalShown = false;
    isEnvoiChorusShown = false;

    constructor(
        private acteService: ActeService,
        private contratService: ContratService,
        private changeDetectorRef: ChangeDetectorRef,
        injector: Injector
    ) {
        super(injector);
        this.actes = {number: 0, size: defaultItemsCountPerPage};
        this.self = this;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            const contratId = params['contratId'];
            this.criteria.contrat = {id: contratId};
            this.contratService.findContratById(contratId).subscribe(contrat => this.contrat = contrat);

            console.debug(`Recherche des actes pour le contrat ${this.criteria.contrat.id}`);

            this.activatedRoute.queryParams.subscribe(params => {

                this.params = params;
                const pageParam = this.params['page'];
                const page_ = isBlank(pageParam) ? 0 : parseInt(pageParam, 10);

                const pageSizeParam = this.params['pageSize'];
                const pageSize_ = isBlank(pageSizeParam) ? defaultItemsCountPerPage : parseInt(pageSizeParam, 10);

                const propertyParam = this.params['property'];
                const directionParam = this.params['direction'];

                let sort: PaginationPropertySort[] = this.sort;
                if (!isBlank(propertyParam) && !isBlank(directionParam)) {
                    sort = [{property: propertyParam, direction: directionParam}];
                }


                this.fetchPage(page_, pageSize_, sort, false).subscribe(
                    (actes: PaginationPage<ActeDTO>) => {
                        this.actes = actes;
                    }, () => {


                    }, () => {


                    });
            });
        });

    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[], pushState?: boolean): Observable<PaginationPage<ActeDTO>> {
        if (!sort) {
            sort = JSON.parse(localStorage.getItem(EXEC_SORT));
        }

        let observable = this.acteService.rechercher(this.criteria, pageNumber, pageSize, sort);

        observable.subscribe(actes => {
            this.actes = actes;

            console.debug('Liste des actes = ', actes.content);
        }, _ => {
        });

        return observable;
    }

    supprimerActe(numero: string): void {
        var acte: ActeDTO = this.actes.content.filter(acte => acte.numero === numero).pop();

        this.acteService.supprimer(acte).subscribe(
            () => {
                this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                this.router.onSameUrlNavigation = 'reload';
                this.router.navigate(['/actes', acte.contrat.id]);
            }
        );
    }

    afficherModaleSuppressionActe(acte: ActeDTO) {
        const modale = this.modalService.openModal(ConfirmationModalComponent, {});
        modale.content.title = `Suppression de l'acte n°${acte?.numero}`;
        modale.content.message = 'Êtes-vous sûr de vouloir supprimer cet acte?';
        modale.content.yes.subscribe(_ => this.supprimerActe(acte.numero));

    }

    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        if (motsCles.length === 0 || motsCles.length > 2) {
            this.criteria.motsCles = motsCles;
            this.fetchPage(0, this.actes.size, null);
        }
    }

    getLibelleStatut(acte: ActeDTO) {
        let result: string;

        switch (acte.statut) {
            case StatutActe.BROUILLON:
            case 'BROUILLON':
                result = 'Brouillon';
                break;
            case StatutActe.EN_ATTENTE_VALIDATION:
            case 'EN_ATTENTE_VALIDATION':
                result = 'En attente de validation par CHORUS';
                break;
            case StatutActe.EN_ATTENTE_ACCUSE:
            case 'EN_ATTENTE_ACCUSE':
                result = 'En attente de l\'accusé de lecture';
                break;
            case StatutActe.VALIDE:
            case 'VALIDE':
                result = 'Validé par CHORUS';
                break;
            case StatutActe.EN_ERREUR:
            case 'EN_ERREUR':
                result = 'Erreur';
                break;
            case StatutActe.NOTIFIE:
            case 'NOTIFIE':
                result = 'Notifié';
                break;
            case StatutActe.ANNULE:
            case 'ANNULE':
                result = 'Annulé';
                break;
            default:
                result = 'Statut non connu';
        }

        return result;
    }

    getInfobulleStatut(acte: ActeDTO) {
        let result: string;

        switch (acte.statut) {
            case StatutActe.BROUILLON:
            case 'BROUILLON':
                result = 'Brouillon';
                break;
            case StatutActe.EN_ATTENTE_ACCUSE:
            case 'EN_ATTENTE_ACCUSE':
                result = 'En attente de l\'accusé de lecture';
                break;
            case StatutActe.EN_ATTENTE_VALIDATION:
            case 'EN_ATTENTE_VALIDATION':
                result = 'En attente de validation par CHORUS';
                break;
            case StatutActe.NOTIFIE:
            case 'NOTIFIE':
                result = 'Notifié';
                break;
            case StatutActe.ANNULE:
            case 'ANNULE':
                result = `Annulé au motif ${acte.rejet?.motif.toUpperCase()} ${acte.rejet?.commentaire ? `( avec le commentaire '${acte.rejet?.commentaire}' )` : ''}`;
                break;
            default:
                result = 'Statut non connu';
        }

        return result;
    }

    download(document: DocumentActeDTO) {

        this.acteService.download(document.id).subscribe(blob => {
            saveAs(blob, document?.fichier?.nom);
        });
    }

    notifier(acte: ActeDTO) {
        this.selectedActe = acte;
        this.isNotificationModalShown = true;
    }

    updateActe(acte: ActeDTO) {
        const index: number = this.actes.content.findIndex((a: ActeDTO) => a.id === acte.id);
        if (index !== -1) {
            this.actes.content[index] = acte;
        }
    }

    echangerChorus(acte: ActeDTO) {
        console.debug(`Edition de l'échange chorus ${acte}`);
        this.selectedActe = acte;
        this.isEnvoiChorusShown = true;
        this.echnageChorus = {dateNotificationPrevisionnelle: acte.dateNotification ? new Date(acte.dateNotification) : null};
    }

    updateEchangeChorus(result: EchangeChorusDTO) {
        this.selectedActe.echangeChorus = result;
        this.changeDetectorRef.detectChanges();
    }

    isChorus() {
        return this.contrat?.service?.echangesChorus;
    }

    rejeter(acte: ActeDTO) {
        const modalRef = this.modalService.openModal(ActeAnnulationComponent, {class: 'modal-lg'});
        modalRef.content.acte = acte;
    }

    existeEchangeChorusOK(acte: ActeDTO): boolean {
        return acte?.echangeChorus?.statut?.value === 'EN_COURS';
    }

    avecEtablissement(): boolean {
        return this.contrat?.attributaire?.etablissement != null;
    }
}
