import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ActeDTO, RejetActeDTO } from '@dto';
import { ActeService } from '@services';
import { Router } from '@angular/router';
import { Option } from '@views/actes/forms';
import { MotifRejetActeDTO } from '@dto/backendEnum';

@Component({
  selector: 'ltexec-acte-annulation',
  templateUrl: './acte-annulation.component.html',
  styleUrls: ['./acte-annulation.component.scss']
})
export class ActeAnnulationComponent {

    @Input() acte: ActeDTO;
    public rejet: RejetActeDTO = {};

    public MOTIFS_REJET: Option<string, string>[] = [
        {code : MotifRejetActeDTO.ERREUR_SAISIE, label: `Erreur de saisie`},
        {code : MotifRejetActeDTO.ABSENCE_ACCUSE_ENTREPRISE, label: `Je n'ai jamais recu d'accusé de lecture de la part de l'entreprise`},
        {code : MotifRejetActeDTO.REFUS_CHORUS, label: `J'ai recu un refus de la part de CHORUS`},
        {code : MotifRejetActeDTO.AUTRE, label: `Autre`},
    ];

    constructor(public modalRef: BsModalRef, private acteService: ActeService, private router:Router) {
    }

    public valider(): void {
        this.acteService.annuler(this.acte, this.rejet).subscribe( _ => {
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate(['/actes', this.acte?.contrat?.id]).then(() => {
                console.debug('Retour a la liste des actes');
            });
        }, error => {
            console.log(`Erreur lors de l'annulation de l'acte`, error);
        }, () => {
            this.modalRef.hide();
        });
    }

    public annuler(): void {
        this.modalRef.hide();
    }

    estValide() {
        return this.rejet?.motif && ( this.rejet?.motif !== MotifRejetActeDTO.AUTRE || this.rejet?.commentaire );
    }
}
