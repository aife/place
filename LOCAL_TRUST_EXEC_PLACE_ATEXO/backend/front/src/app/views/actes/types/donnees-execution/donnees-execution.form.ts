import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {ContratService} from '@services';
import {DonneesExecutionDTO} from "@views/actes/types/donnees-execution/donnees-execution.dto";

export class DonneesExecutionForm extends BaseForm<DonneesExecutionDTO> {
    public tarif?: number;
    public intituleTarif?: string;
    public depensesInvestissement?: number;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: DonneesExecutionDTO,
    ) {
        super(contratService, contrat, numero, acheteur);
        this.publicationDonneesEssentielles = true;
        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): DonneesExecutionDTO {
        const acte: DonneesExecutionDTO = super.toDTO();
        acte.depensesInvestissement = this.depensesInvestissement;
        acte.tarif = this.tarif;
        acte.intituleTarif = this.intituleTarif;
        acte.depensesInvestissement = this.depensesInvestissement;
        acte.publicationDonneesEssentielles = this.publicationDonneesEssentielles;
        return acte;
    }

    public fromDTO(acte: DonneesExecutionDTO): void {
        super.fromDTO(acte);
        this.publicationDonneesEssentielles = acte?.publicationDonneesEssentielles;
        this.tarif = acte?.tarif;
        this.intituleTarif = acte?.intituleTarif;
        this.depensesInvestissement = acte?.depensesInvestissement;
    }
}
