import { ActeDTO, date } from '@dto';

export interface DecisionProlongationDelaiDTO extends ActeDTO {
    debut?: date;
    fin?: date;
    dureeEnJours?: number;
}

