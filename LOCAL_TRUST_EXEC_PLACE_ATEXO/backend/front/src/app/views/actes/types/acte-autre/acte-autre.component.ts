import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {ActeAutreForm} from '@views/actes/types/acte-autre/acte-autre.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-acte-autre',
    templateUrl: './acte-autre.component.html',
    styleUrls: ['./acte-autre.component.scss']
})
export class ActeAutreComponent extends BaseCreationActeComponent<ActeAutreForm> implements OnInit {
    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new ActeAutreForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }


}
