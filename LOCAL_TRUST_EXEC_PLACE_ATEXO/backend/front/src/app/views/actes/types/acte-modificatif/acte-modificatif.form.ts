import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {ActeModificatifDTO} from '@views/actes/types/acte-modificatif/acte-modificatif.dto';
import {ContratService} from '@services';
import {datify, stringify} from '@common/dateUtils';

export class ActeModificatifForm extends BaseForm<ActeModificatifDTO> {
    public dureeAjoureeContrat: number;
    public clauseReexamen: boolean;
    public typeModification: string;
    public montantHTChiffre?: number = 0;
    public montantTTCChiffre?: number = 0;
    public tauxTVA?: number;
    public nouvelleFinContrat: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: ActeModificatifDTO,
    ) {
        super(contratService, contrat, numero, acheteur);
        this.publicationDonneesEssentielles = true;
        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): ActeModificatifDTO {
        const acte: ActeModificatifDTO = super.toDTO();

        acte.montantHTChiffre = this.montantHTChiffre;
        acte.montantTTCChiffre = this.montantTTCChiffre;
        acte.tauxTVA = this.tauxTVA;
        acte.typeModification = this.typeModification;
        acte.clauseReexamen = this.clauseReexamen;
        acte.publicationDonneesEssentielles = this.publicationDonneesEssentielles;
        acte.nouvelleFinContrat = stringify(this.nouvelleFinContrat);
        acte.dureeAjoureeContrat = this.dureeAjoureeContrat;

        return acte;
    }

    public fromDTO(acte: ActeModificatifDTO): void {
        super.fromDTO(acte);

        this.montantHTChiffre = acte?.montantHTChiffre;
        this.montantTTCChiffre = acte?.montantTTCChiffre;
        this.tauxTVA = acte?.tauxTVA;
        this.typeModification = acte?.typeModification;
        this.clauseReexamen = acte?.clauseReexamen;
        this.publicationDonneesEssentielles = acte?.publicationDonneesEssentielles;
        this.nouvelleFinContrat = datify(acte?.nouvelleFinContrat);
        this.dureeAjoureeContrat = acte?.dureeAjoureeContrat;
    }
}
