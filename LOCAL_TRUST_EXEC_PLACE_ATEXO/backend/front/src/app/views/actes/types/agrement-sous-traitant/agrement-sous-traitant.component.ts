import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent, Option} from '@views/actes/forms';
import {AgrementSousTraitantForm} from '@views/actes/types/agrement-sous-traitant/agrement-sous-traitant.form';
import {EtablissementDTO} from '@dto';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-agrement-sous-traitant',
    templateUrl: './agrement-sous-traitant.component.html',
    styleUrls: ['./agrement-sous-traitant.component.scss']
})
export class AgrementSousTraitantComponent extends BaseCreationActeComponent<AgrementSousTraitantForm> implements OnInit {

    public RANGS_SOUS_TRAITANCE?: Option<number, string>[] = [
        {code: 1, label: '1'},
        {code: 2, label: '2'},
        {code: 3, label: '3'},
        {code: 4, label: '4'},
        {code: 5, label: '5'},
        {code: null, label: 'Aucun'},
    ];

    public VARIATIONS?: Option<string, string>[] = [
        {code: 'FERME', label: 'Ferme'},
        {code: 'FERME_ET_ACTUALISABLE', label: 'Ferme et actualisable'},
        {code: 'REVISABLE', label: 'Révisable'}
    ];

    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);
        console.debug('Acte = ', this.acte);

        this.formulaire = new AgrementSousTraitantForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }
    public modifierSousTraitant(etablissement: EtablissementDTO): void {
        console.log(`Mise a jour du sous-traitant avec l'établissement = `, etablissement);

        if (!this.formulaire.sousTraitant) {
            this.formulaire.sousTraitant = {};
        }

        this.formulaire.sousTraitant.etablissement = etablissement;
        if ( etablissement.fournisseur.raisonSociale ) {
            this.formulaire.sousTraitant.nom = etablissement.fournisseur.raisonSociale;
        }
        this.formulaire.sousTraitant.email = etablissement.fournisseur.email;
        this.formulaire.sousTraitant.telephone = etablissement.fournisseur.telephone;
        this.formulaire.sousTraitant.adresse = (etablissement.adresse ? `${etablissement.adresse?.adresse} ${etablissement.adresse?.codePostal} ${etablissement.adresse?.commune}` : '');
    }

    public round(value: number): number {
        return Math.round((value + Number.EPSILON) * 100) / 100;
    }

    public actualiserMontantsApresHTChiffre(): void {
        if (this.formulaire.montantHT && (this.formulaire.tauxTVA || this.formulaire.montantTTC)) {
            if (this.formulaire.tauxTVA ) {
                this.formulaire.montantTTC = this.round(this.formulaire.montantHT *  ( 1 + ( this.formulaire.tauxTVA ) / 100));
            } else if (this.formulaire.montantTTC ) {
                this.formulaire.tauxTVA = this.round(((this.formulaire.montantTTC / this.formulaire.montantHT) - 1) * 100);
            }
        }
    }

    public actualiserMontantsApresTTCChiffre(): void {
        if (this.formulaire.montantTTC && (this.formulaire.tauxTVA || this.formulaire.montantHT)) {
            if (this.formulaire.montantHT ) {
                this.formulaire.tauxTVA = this.round(((this.formulaire.montantTTC / this.formulaire.montantHT) - 1) * 100);
            } else if (this.formulaire.tauxTVA ) {
                this.formulaire.montantHT = this.round(this.formulaire.montantTTC / (1 + (this.formulaire.tauxTVA / 100)));
            }
        }
    }

    public actualiserMontantsApresTauxTVA(): void {
        if (this.formulaire.tauxTVA && (this.formulaire.montantHT || this.formulaire.montantTTC)) {
            if (this.formulaire.montantHT ) {
                this.formulaire.montantTTC = this.round(this.formulaire.montantHT *  ( 1 + ( this.formulaire.tauxTVA ) / 100));
            } else if (this.formulaire.montantTTC ) {
                this.formulaire.montantHT = this.round(this.formulaire.montantTTC / (1 + (this.formulaire.tauxTVA / 100)));
            }
        }
    }

}
