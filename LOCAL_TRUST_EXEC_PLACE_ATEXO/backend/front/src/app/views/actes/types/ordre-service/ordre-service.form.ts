import {ActeDTO, ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {OrdreServiceDto} from '@views/actes/types/ordre-service/ordre-service.dto';
import {ContratService} from "@services";

export class OrdreServiceForm extends BaseForm<OrdreServiceDto> {
    public cotraitants?: number[] = [];
    public maitreOeuvre?: string;
    public maitreOuvrage?: string;
    public reconduction: boolean;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: ActeDTO,
    ) {
        super(contratService, contrat, numero, acheteur);

        this.contrat = contrat;

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): OrdreServiceDto {
        const acte: OrdreServiceDto = super.toDTO();

        acte.cotraitants = this.etablissements.filter(etablissement => this.cotraitants.includes(etablissement.id));
        acte.reconduction = this.reconduction;
        acte.maitreOeuvre = this.maitreOeuvre;
        acte.maitreOuvrage = this.maitreOuvrage;

        return acte;
    }

    public fromDTO(acte: OrdreServiceDto): void {
        super.fromDTO(acte);

        this.cotraitants = acte?.cotraitants?.map(cotraitant => cotraitant.id);
        this.reconduction = acte?.reconduction;
        this.maitreOeuvre = acte?.maitreOeuvre;
        this.maitreOuvrage = acte?.maitreOuvrage;
    }

}
