import { ActeDTO, date } from '@dto';

export interface DecisionReconductionDTO extends ActeDTO {
    reconduction?: boolean;
    debut?: date;
    fin?: date;
}
