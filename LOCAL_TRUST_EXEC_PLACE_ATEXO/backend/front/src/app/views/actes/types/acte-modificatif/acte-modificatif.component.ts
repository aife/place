import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent, Option} from '@views/actes/forms';
import {ActeModificatifForm} from '@views/actes/types/acte-modificatif/acte-modificatif.form';
import {ValueLabelDTO} from "@dto";
import * as moment from 'moment';
import {USER_CONTEXT} from "@common/userContext";

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-acte-modificatif',
    templateUrl: './acte-modificatif.component.html',
    styleUrls: ['./acte-modificatif.component.scss']
})
export class ActeModificatifComponent extends BaseCreationActeComponent<ActeModificatifForm> implements OnInit {

    devise: ValueLabelDTO;

    public TYPES_MODIFICATION?: Option<string, string>[] = [
        {code: 'AVEC_INCIDENCE', label: 'Modifications avec incidence financière'},
        {code: 'SANS_INCIDENCE', label: 'Modifications sans incidence financière'}
    ];

    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new ActeModificatifForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
        this.devise = USER_CONTEXT.utilisateur.devise;

    }

    public round(value: number): number {
        return Math.round((value + Number.EPSILON) * 100) / 100;
    }

    public actualiserMontantsApresHTChiffre(): void {
        if (this.formulaire.montantHTChiffre && (this.formulaire.tauxTVA || this.formulaire.montantTTCChiffre)) {
            if (this.formulaire.tauxTVA ) {
                this.formulaire.montantTTCChiffre = this.round(this.formulaire.montantHTChiffre *  ( 1 + ( this.formulaire.tauxTVA ) / 100));
            } else if (this.formulaire.montantTTCChiffre ) {
                this.formulaire.tauxTVA = this.round(((this.formulaire.montantTTCChiffre / this.formulaire.montantHTChiffre) - 1) * 100);
            }
        }
    }

    public actualiserMontantsApresTTCChiffre(): void {
        if (this.formulaire.montantTTCChiffre && (this.formulaire.tauxTVA || this.formulaire.montantHTChiffre)) {
            if (this.formulaire.montantHTChiffre ) {
                this.formulaire.tauxTVA = this.round(((this.formulaire.montantTTCChiffre / this.formulaire.montantHTChiffre) - 1) * 100);
            } else if (this.formulaire.tauxTVA ) {
                this.formulaire.montantHTChiffre = this.round(this.formulaire.montantTTCChiffre / (1 + (this.formulaire.tauxTVA / 100)));
            }
        }
    }

    public actualiserMontantsApresTauxTVA(): void {
        if (this.formulaire.tauxTVA && (this.formulaire.montantHTChiffre || this.formulaire.montantTTCChiffre)) {
            if (this.formulaire.montantHTChiffre ) {
                this.formulaire.montantTTCChiffre = this.round(this.formulaire.montantHTChiffre *  ( 1 + ( this.formulaire.tauxTVA ) / 100));
            } else if (this.formulaire.montantTTCChiffre ) {
                this.formulaire.montantHTChiffre = this.round(this.formulaire.montantTTCChiffre / (1 + (this.formulaire.tauxTVA / 100)));
            }
        }
    }

    public updateNouvelleFinContrat(): void {
        let dateFinContrat = Date.parse(this.contrat.dateFinContrat as any as string);
        this.formulaire.nouvelleFinContrat = new Date(dateFinContrat + this.formulaire.dureeAjoureeContrat * 24 * 60 * 60 * 1000);
    }

    public updateDureeAjoureeContrat(event: any): void {
        this.formulaire.nouvelleFinContrat = event;
        let momentStartDate = moment(Date.parse(this.contrat.dateFinContrat as any as string));
        let momentEndDate = moment(this.formulaire.nouvelleFinContrat);
        if (this.contrat.dateFinContrat && this.formulaire.nouvelleFinContrat) {
            this.formulaire.dureeAjoureeContrat = (momentEndDate.diff(momentStartDate, 'days'));
        }

    }

    public getMaxDateFinContrat(): Date {
        return this.contrat.dateMaxFinContrat ? new Date(Date.parse(this.contrat.dateMaxFinContrat as any as string)) : undefined;
    }

}
