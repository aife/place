import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {DonneesExecutionForm} from "@views/actes/types/donnees-execution/donnees-execution.form";

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-donnees-execution',
    templateUrl: './donnees-execution.component.html',
    styleUrls: ['./donnees-execution.component.scss']
})
export class DonneesExecutionComponent extends BaseCreationActeComponent<DonneesExecutionForm> implements OnInit {

    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);
        console.debug('Acte = ', this.acte);

        this.formulaire = new DonneesExecutionForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }

}
