import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {
    DecisionAffermissementTrancheDTO
} from '@views/actes/types/decision-affermissement-tranche/decision-affermissement-tranche.dto';
import {ContratService} from "@services";
import * as moment from 'moment';
import { datify, stringify } from '@common/dateUtils';

export class DecisionAffermissementTrancheForm extends BaseForm<DecisionAffermissementTrancheDTO> {
    public numeroTranche: string;
    public dureeEnJoursExecutionTranche: number;
    public montantHT?: number = 0;
    public montantTTC?: number = 0;
    public tauxTVA?: number;
    public debutExecutionTranche?: Date;
    public finExecutionTranche?: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: DecisionAffermissementTrancheDTO,
    ) {
        super(contratService, contrat, numero, acheteur);

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public mettreAJourDureeTranche(): void {
        if (this.debutExecutionTranche && this.finExecutionTranche) {

            const debut = new Date(this.finExecutionTranche);
            const fin = new Date(this.debutExecutionTranche);

            var diff = moment(debut.getTime()).unix() - moment(fin.getTime()).unix();
            this.dureeEnJoursExecutionTranche = moment.duration(diff * 1000).asDays();
        }
    }

    public toDTO(): DecisionAffermissementTrancheDTO {
        const acte: DecisionAffermissementTrancheDTO = super.toDTO();

        acte.numeroTranche = this.numeroTranche;
        acte.debutExecutionTranche = stringify(this.debutExecutionTranche);
        acte.finExecutionTranche = stringify(this.finExecutionTranche);
        acte.dureeEnJoursExecutionTranche = this.dureeEnJoursExecutionTranche;
        acte.montantHT = this.montantHT;
        acte.montantTTC = this.montantTTC;
        acte.tauxTVA = this.tauxTVA;

        return acte;
    }

    public fromDTO(acte: DecisionAffermissementTrancheDTO): void {
        super.fromDTO(acte);

        this.numeroTranche = acte?.numeroTranche;
        this.debutExecutionTranche = datify(acte?.debutExecutionTranche);
        this.finExecutionTranche = datify(acte?.finExecutionTranche);
        this.dureeEnJoursExecutionTranche = acte?.dureeEnJoursExecutionTranche;
        this.montantHT = acte?.montantHT;
        this.montantTTC = acte?.montantTTC;
        this.tauxTVA = acte?.tauxTVA;
    }
}
