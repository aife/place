import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {
    DecisionAffermissementTrancheForm
} from '@views/actes/types/decision-affermissement-tranche/decision-affermissement-tranche.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-decision-affermissement-tranche',
    templateUrl: './decision-affermissement-tranche.component.html',
    styleUrls: ['./decision-affermissement-tranche.component.scss']
})
export class DecisionAffermissementTrancheComponent extends BaseCreationActeComponent<DecisionAffermissementTrancheForm> implements OnInit {
    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new DecisionAffermissementTrancheForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }
}
