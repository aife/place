import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {DecisionReconductionDTO} from '@views/actes/types/decision-reconduction/decision-reconduction.dto';
import {ContratService} from "@services";
import { datify, stringify } from '@common/dateUtils';

export class DecisionReconductionForm extends BaseForm<DecisionReconductionDTO> {
    public reconduction: boolean;
    public debut?: Date;
    public fin?: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: DecisionReconductionDTO,
    ) {
        super(contratService, contrat, numero, acheteur);

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): DecisionReconductionDTO {
        const acte: DecisionReconductionDTO = super.toDTO();

        acte.reconduction = this.reconduction;

        if (this.reconduction) {
            acte.debut = stringify(this.debut);
        } else {
            acte.debut = null;
        }

        acte.fin = stringify(this.fin);

        return acte;
    }

    public fromDTO(acte: DecisionReconductionDTO): void {
        super.fromDTO(acte);

        this.reconduction = acte?.reconduction;
        this.debut = datify(acte?.debut);
        this.fin = datify(acte?.fin);
    }
}
