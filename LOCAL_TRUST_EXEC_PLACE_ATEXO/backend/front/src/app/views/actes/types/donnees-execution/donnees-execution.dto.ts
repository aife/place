import {ActeDTO} from '@dto';

export interface DonneesExecutionDTO extends ActeDTO {
    depensesInvestissement?: number;
    tarif?: number;
    intituleTarif?: string;
    publicationDonneesEssentielles?: boolean;
}
