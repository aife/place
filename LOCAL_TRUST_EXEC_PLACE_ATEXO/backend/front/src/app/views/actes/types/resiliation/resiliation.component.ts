import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent, Option} from '@views/actes/forms';
import {ResiliationForm} from '@views/actes/types/resiliation/resiliation.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-resiliation',
    templateUrl: './resiliation.component.html',
    styleUrls: ['./resiliation.component.scss']
})
export class ResiliationComponent extends BaseCreationActeComponent<ResiliationForm> implements OnInit {
    public COUVERTURES_RESILIATION?: Option<string, string>[] = [
        {code: 'PARTIELLE', label: 'Partielle'},
        {code: 'TOTALE', label: 'Totale'}
    ];

    public MOTIFS_RESILIATION?: Option<string, string>[] = [
        {code: 'ACTES_EXTERIEURS', label: 'pour actes extérieurs au marché'},
        {code: 'ACTES_LIES', label: 'pour actes liés au marché'},
        {code: 'FAUTE_TITULAIRE', label: 'pour faute du titulaire pour motif d’intérêt général'}
    ];

    constructor(
        router: Router,
        public contratService: ContratService,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new ResiliationForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }

}
