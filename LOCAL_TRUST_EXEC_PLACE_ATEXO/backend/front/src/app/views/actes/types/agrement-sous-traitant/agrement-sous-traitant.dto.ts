import {ActeDTO, SousTraitantDTO} from '@dto';

export interface AgrementDuSousTraitantDTO extends ActeDTO {
    sousTraitant?: SousTraitantDTO;
    rang?: number;
    reference?: string;
    modaliteSousTraitance?: number;
    montantHT?: number;
    montantTTC?: number;
    tauxTVA?: number;
    montantOuPourcentageHT?: number;
    autoliquidation?: boolean;
    paiementDirect?: boolean;
    variation?: string;
    duree?: number;
    publicationDonneesEssentielles?: boolean;
}
