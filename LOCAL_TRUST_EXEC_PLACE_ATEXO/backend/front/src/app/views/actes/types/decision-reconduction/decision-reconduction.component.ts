import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {DecisionReconductionForm} from '@views/actes/types/decision-reconduction/decision-reconduction.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-decision-reconduction',
    templateUrl: './decision-reconduction.component.html',
    styleUrls: ['./decision-reconduction.component.scss']
})
export class DecisionReconductionComponent extends BaseCreationActeComponent<DecisionReconductionForm> implements OnInit {
    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new DecisionReconductionForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }

}
