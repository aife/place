import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {OrdreServiceForm} from '@views/actes/types/ordre-service/ordre-service.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-ordre-service',
    templateUrl: './ordre-service.component.html',
    styleUrls: ['./ordre-service.component.scss']
})
export class OrdreServiceComponent extends BaseCreationActeComponent<OrdreServiceForm> implements OnInit {
    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new OrdreServiceForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }
}
