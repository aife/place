import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {ActeAutreDto} from '@views/actes/types/acte-autre/acte-autre.dto';
import { DecisionProlongationDelaiDTO } from '@views/actes/types/decision-prolongation-delai/decision-prolongation-delai.dto';
import {ContratService} from "@services";
import * as moment from 'moment';
import { datify, stringify } from '@common/dateUtils';

export class DecisionProlongationDelaiForm extends BaseForm<DecisionProlongationDelaiDTO> {
    public dureeEnJours: number;
    public debut?: Date;
    public fin?: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: ActeAutreDto,
    ) {
        super(contratService, contrat, numero, acheteur);

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): ActeAutreDto {
        const acte: DecisionProlongationDelaiDTO = super.toDTO();

        acte.debut = stringify(this.debut);
        acte.fin = stringify(this.fin);
        acte.dureeEnJours = this.dureeEnJours;

        return acte;
    }

    public fromDTO(acte: DecisionProlongationDelaiDTO): void {
        super.fromDTO(acte);

        this.debut = datify(acte?.debut);
        this.fin = datify(acte?.fin);
        this.dureeEnJours = acte?.dureeEnJours;
    }

    public mettreAJourDureeTranche(): void {
        if (this.debut && this.fin) {

            var diff = moment(this.fin.getTime()).unix() - moment(this.debut.getTime()).unix();
            this.dureeEnJours = moment.duration(diff * 1000).asDays();
        }
    }
}
