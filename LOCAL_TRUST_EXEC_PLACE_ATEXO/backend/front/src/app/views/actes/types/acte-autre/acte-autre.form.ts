import {
    ContratDTO,
    EtablissementDTO,
    UtilisateurDTO
} from '@dto';
import { BaseForm, TypeFormulaireActe } from '@views/actes/forms';
import { ActeAutreDto } from '@views/actes/types/acte-autre/acte-autre.dto';
import {ContratService} from "@services";
import { datify, stringify } from '@common/dateUtils';

export class ActeAutreForm extends BaseForm<ActeAutreDto> {
    public dateSignaturePouvoirAdjudicateur: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: ActeAutreDto,
    ) {
        super(contratService,contrat, numero, acheteur);

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): ActeAutreDto {
        const acte: ActeAutreDto = super.toDTO();

        acte.dateSignaturePouvoirAdjudicateur = stringify(this.dateSignaturePouvoirAdjudicateur);

        return acte;
    }

    public fromDTO(acte: ActeAutreDto): void {
        super.fromDTO(acte);

        this.dateSignaturePouvoirAdjudicateur = datify(acte?.dateSignaturePouvoirAdjudicateur);
    }
}
