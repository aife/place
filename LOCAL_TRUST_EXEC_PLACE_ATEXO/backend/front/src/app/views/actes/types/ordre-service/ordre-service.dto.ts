import { ActeDTO, EtablissementDTO } from '@dto';

export interface OrdreServiceDto extends ActeDTO {
    cotraitants?: EtablissementDTO[];
    reconduction?: boolean;
    maitreOeuvre?: string;
    maitreOuvrage?: string;
}
