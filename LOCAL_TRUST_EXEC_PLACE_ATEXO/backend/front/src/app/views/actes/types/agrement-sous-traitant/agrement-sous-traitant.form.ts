import { ContratDTO, EtablissementDTO, UtilisateurDTO } from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {AgrementDuSousTraitantDTO} from '@views/actes/types/agrement-sous-traitant/agrement-sous-traitant.dto';
import {ContratService} from '@services';
import { datify, stringify } from '@common/dateUtils';

export class TitulaireForm {
    public id?: number;
    public etablissement?: EtablissementDTO;
    public adresse?: string = '';
    public email?: string = '';
    public telephone?: string = '';
    public nom?: string = '';
}

export class SousTraitantForm {
    public etablissement?: EtablissementDTO;
    public adresse?: string = '';
    public email?: string = '';
    public telephone?: string = '';
    public nom?: string = '';
    public dateDemande?: Date;
    public dateNotification?: Date;
}

export class AgrementSousTraitantForm extends BaseForm<AgrementDuSousTraitantDTO> {
    public rang?: number;
    public variation?: string;
    public reference?: string;
    public sousTraitant?: SousTraitantForm = new SousTraitantForm();
    public modaliteSousTraitance?: number = 0;
    public montantHT?: number = 0;
    public montantTTC?: number = 0;
    public tauxTVA?: number;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: AgrementDuSousTraitantDTO,
    ) {
        super(contratService, contrat, numero, acheteur);
        this.publicationDonneesEssentielles = true;
        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): AgrementDuSousTraitantDTO {
        const acte: AgrementDuSousTraitantDTO = super.toDTO();

        acte.rang = this.rang;
        acte.reference = this.reference;
        acte.sousTraitant = {
            etablissement: this.sousTraitant.etablissement,
            adresse: this.sousTraitant.adresse,
            email: this.sousTraitant.email,
            telephone: this.sousTraitant.telephone,
            nom: this.sousTraitant.nom,
            dateDemande: stringify(this.sousTraitant.dateDemande),
            dateNotification: stringify(this.sousTraitant.dateNotification)
        };
        acte.modaliteSousTraitance = this.modaliteSousTraitance;
        acte.montantHT = this.montantHT;
        acte.montantTTC = this.montantTTC;
        acte.tauxTVA = this.tauxTVA;
        acte.variation = this.variation;
        acte.duree = this.duree;
        acte.publicationDonneesEssentielles = this.publicationDonneesEssentielles;
        return acte;
    }

    public fromDTO(acte: AgrementDuSousTraitantDTO): void {
        super.fromDTO(acte);

        this.rang = acte?.rang;
        this.reference = acte?.reference;
        this.sousTraitant = {
            etablissement: acte?.sousTraitant?.etablissement,
            adresse: acte?.sousTraitant?.adresse,
            email: acte?.sousTraitant?.email,
            telephone: acte?.sousTraitant?.telephone,
            nom: acte?.sousTraitant?.nom,
            dateDemande: datify(acte?.sousTraitant?.dateDemande),
            dateNotification: datify(acte?.sousTraitant?.dateNotification)
        };
        this.modaliteSousTraitance = acte?.modaliteSousTraitance;
        this.montantHT = acte?.montantHT;
        this.montantTTC = acte?.montantTTC;
        this.tauxTVA = acte?.tauxTVA;
        this.variation = acte?.variation;
        this.duree = acte?.duree;
        this.publicationDonneesEssentielles = acte?.publicationDonneesEssentielles;
    }
}
