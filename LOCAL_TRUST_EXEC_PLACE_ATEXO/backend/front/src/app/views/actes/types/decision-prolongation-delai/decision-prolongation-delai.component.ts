import {Component, OnInit} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {BaseCreationActeComponent} from '@views/actes/forms';
import {
    DecisionProlongationDelaiForm
} from '@views/actes/types/decision-prolongation-delai/decision-prolongation-delai.form';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-decision-prolongation-delai',
    templateUrl: './decision-prolongation-delai.component.html',
    styleUrls: ['./decision-prolongation-delai.component.scss']
})
export class DecisionProlongationDelaiComponent extends BaseCreationActeComponent<DecisionProlongationDelaiForm> implements OnInit {
    constructor(
        public contratService: ContratService,
        router: Router,
        acteService: ActeService,
        etablissementService: EtablissementService,
        echangeChorusService: EchangeChorusService
    ) {
        super(contratService, router, acteService, etablissementService, echangeChorusService);
    }

    public ngOnInit(): void {
        console.debug('Contrat = ', this.contrat);

        this.formulaire = new DecisionProlongationDelaiForm(this.contratService, this.contrat, this.numero, this.utilisateur, this.type, this.acte);
    }

}
