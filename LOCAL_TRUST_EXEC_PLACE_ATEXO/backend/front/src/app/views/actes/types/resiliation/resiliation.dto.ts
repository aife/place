import { ActeDTO, date } from '@dto';

export enum CouvertureDTO {
    PARTIELLE = "PARTIELLE",
    TOTALE = "TOTALE"
}

export interface ResiliationDto extends ActeDTO {
    couverture?: CouvertureDTO;
    motif?: string;
    fraisEtRisquesPourLeTitulaire?: boolean;
    miseEnOeuvre?: string;
    finContrat?: date;
}
