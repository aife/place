import { ActeDTO, date } from '@dto';

export interface ActeAutreDto extends ActeDTO {
    dateSignaturePouvoirAdjudicateur?: date;
}

