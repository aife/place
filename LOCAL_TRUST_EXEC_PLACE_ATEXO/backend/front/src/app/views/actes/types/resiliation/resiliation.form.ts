import {ContratDTO, UtilisateurDTO} from '@dto';
import {BaseForm, TypeFormulaireActe} from '@views/actes/forms';
import {CouvertureDTO, ResiliationDto} from '@views/actes/types/resiliation/resiliation.dto';
import {ContratService} from "@services";
import { datify, stringify } from '@common/dateUtils';

export class ResiliationForm extends BaseForm<ResiliationDto> {
    public couverture: CouvertureDTO;
    public motif: string;
    public fraisEtRisquesPourLeTitulaire: boolean;
    public miseEnOeuvreResiliation?: string;
    public finContrat: Date;

    constructor(
        public contratService: ContratService,
        contrat: ContratDTO,
        numero: string,
        acheteur: UtilisateurDTO,
        public type: TypeFormulaireActe,
        public acte?: ResiliationDto,
    ) {
        super(contratService, contrat, numero, acheteur);

        if (acte) {
            this.fromDTO(acte);
        }
    }

    public toDTO(): ResiliationDto {
        const acte: ResiliationDto = super.toDTO();

        acte.couverture = this.couverture;
        acte.motif = this.motif;
        acte.fraisEtRisquesPourLeTitulaire = this.fraisEtRisquesPourLeTitulaire;
        acte.miseEnOeuvre = this.miseEnOeuvreResiliation;
        acte.finContrat = stringify(this.finContrat);

        return acte;
    }

    public fromDTO(acte: ResiliationDto): void {
        super.fromDTO(acte);

        this.couverture = acte?.couverture;
        this.motif = acte?.motif;
        this.fraisEtRisquesPourLeTitulaire = acte?.fraisEtRisquesPourLeTitulaire;
        this.miseEnOeuvreResiliation = acte?.miseEnOeuvre;
        this.finContrat = datify(acte?.finContrat);
    }
}
