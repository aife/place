import {ActeDTO, date} from '@dto';


export interface ActeModificatifDTO extends ActeDTO {
    dureeAjoureeContrat?: number;
    nouvelleFinContrat?: date;
    publicationDonneesEssentielles?: boolean;
    clauseReexamen?: boolean;
    typeModification?: string;
    montantHTChiffre?: number;
    montantTTCChiffre?: number;
    tauxTVA?: number;
    valeurGlobale?: number;
    dateSignatureModification?: date;
}
