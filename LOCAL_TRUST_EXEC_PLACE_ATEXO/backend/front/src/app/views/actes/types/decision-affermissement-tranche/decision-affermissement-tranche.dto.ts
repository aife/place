import { ActeDTO, date } from '@dto';

export interface DecisionAffermissementTrancheDTO extends ActeDTO {
    numeroTranche?: string;
    debutExecutionTranche?: date;
    finExecutionTranche?: date;
    dureeEnJoursExecutionTranche?: number;
    montantHT?: number;
    montantTTC?: number;
    tauxTVA?: number;

}
