// tslint:disable: directive-selector
import { Directive, Input, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[formulaire-acte]',
})
export class FormulaireActeDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
