import { Type } from '@angular/core';

export class TypeFormulaireActe {
    constructor(public code: string, public libelle: string, public classname, public component: Type<any>) {}
}
