import {ActeDTO, ContratDTO, DonneesEssentiellesDTO, EchangeChorusDTO, EtablissementDTO, UtilisateurDTO} from '@dto';
import {TypeFormulaireActe} from './type-formulaire-acte';
import {StatutActe} from '@dto/backendEnum';
import {NgForm} from '@angular/forms';
import {ActeService, ContratService, EchangeChorusService, EtablissementService} from '@services';
import {Router} from '@angular/router';
import {PieceJointeDTO} from '@dto/messagerie-dto';
import {USER_CONTEXT} from '@common/userContext';
import {TitulaireForm} from '@views/actes/types/agrement-sous-traitant/agrement-sous-traitant.form';
import {datify, stringify} from '@common/dateUtils';

class EchangeChorusForm {
    actif?: boolean = true;
    visaACCF?: string;
    visaPrefet?: string;
    typeFournisseurEntreprise1?: string;
}

export abstract class BaseForm<T extends ActeDTO> {
    public DATE_FORMAT = 'DD/MM/YYYY';
    public DATEPICKER_CONFIGURATION = {
        dateInputFormat: this.DATE_FORMAT,
        displayOneMonthRange: true,
        containerClass: 'theme-dark-blue',
    };

    public id?: number;
    public objet?: string;
    public version?: number;
    public statut?: string | StatutActe = StatutActe.BROUILLON;
    public piecesJointes?: PieceJointeDTO[] = [];
    public type: TypeFormulaireActe;
    public notification?: boolean = false;
    public commentaire?: string;
    public chorus: EchangeChorusForm = {};
    public echangeChorus: EchangeChorusDTO;
    public titulaire?: TitulaireForm = {};
    public etablissements?: EtablissementDTO[] = [];
    public dateNotification?: Date;
    public duree?: number;
    public dateCreation?: Date;
    public dateModification?: Date;
    public donneesEssentielles?: DonneesEssentiellesDTO;
    public publicationDonneesEssentielles: boolean;

    protected constructor(
        public contratService: ContratService,
        public contrat: ContratDTO,
        public numero: string,
        public acheteur: UtilisateurDTO
    ) {
        contratService.findEtablissementsById(contrat.id).subscribe(etablissements => {
            console.log(etablissements);
            this.etablissements = etablissements.map(etablissement => etablissement?.etablissement);
            if (this.etablissements.length === 1) {
                if (!this.titulaire.etablissement) {
                    this.titulaire.etablissement = this.etablissements[0];
                }

                if (!this.titulaire.nom) {
                    this.titulaire.nom = this.titulaire.etablissement.fournisseur?.raisonSociale;
                }

                if (!this.titulaire.email) {
                    this.titulaire.email = this.titulaire.etablissement.fournisseur?.email;
                }

                if (!this.titulaire.telephone) {
                    this.titulaire.telephone = this.titulaire.etablissement.fournisseur?.telephone;
                }

                if (!this.titulaire.adresse) {
                    this.titulaire.adresse = (this.titulaire.etablissement.adresse ? `${this.titulaire.etablissement.adresse?.adresse} ${this.titulaire.etablissement.adresse?.codePostal} ${this.titulaire.etablissement.adresse?.commune}` : '');
                }
            }
        });
    }

    public toDTO(): ActeDTO {
        const acte: ActeDTO = {};

        acte.objet = this.objet;
        acte.contrat = this.contrat;
        acte.statut = this.statut;
        acte.numero = this.numero;
        acte.acheteur = this.acheteur;
        acte.type = {
            value: this.type.code
        };
        acte.titulaire = {
            id: this?.titulaire?.id,
            etablissement: this?.titulaire?.etablissement,
            adresse: this?.titulaire?.adresse,
            email: this?.titulaire?.email,
            telephone: this?.titulaire?.telephone,
            nom: this?.titulaire?.nom
        };
        acte.id = this.id;
        acte.version = this.version;
        acte.statut = this.statut;
        acte.dateCreation = this.dateCreation;
        acte.dateModification = this.dateModification;
        if (this.notification) {
            acte.statut = StatutActe.NOTIFIE;
        }
        acte.dateNotification = stringify(this.dateNotification);
        acte.chorus = this.chorus;
        acte.echangeChorus = this.echangeChorus;
        acte.donneesEssentielles = this?.donneesEssentielles;
        acte.commentaire = this.commentaire;
        acte.classname = this.type.classname;
        acte.documents = this.piecesJointes.map(
            pj => {
                return {
                    id: parseInt(pj.id),
                    idExterne: pj.idExterne,
                    reference: pj?.reference?.toString(),
                    utilisateur: USER_CONTEXT.utilisateur,
                    dateCreation: new Date(pj.dateCreation),
                };
            }
        );

        console.log('Liste des documents = ', acte.documents);

        return acte;
    }

    public fromDTO(acte: T): void {
        this.id = acte.id;
        this.objet = acte?.objet;
        this.contrat.id = acte?.contrat?.id;
        this.acheteur = acte?.acheteur;
        this.numero = acte?.numero;
        this.statut = acte?.statut;
        this.version = acte?.version;
        this.titulaire = {
            id: acte?.titulaire?.id,
            etablissement: acte?.titulaire?.etablissement,
            adresse: acte?.titulaire?.adresse,
            email: acte?.titulaire?.email,
            telephone: acte?.titulaire?.telephone,
            nom: acte?.titulaire?.nom
        };
        this.dateCreation = acte?.dateCreation;
        this.dateModification = acte?.dateModification;
        this.dateNotification = datify(acte?.dateNotification);
        this.notification = acte?.statut === 'NOTIFIE';
        this.commentaire = acte?.commentaire;
        this.chorus = {...acte?.chorus};
        this.echangeChorus = acte?.echangeChorus;
        this.donneesEssentielles = acte?.donneesEssentielles;
        this.piecesJointes = acte?.documents.map(
            document => {
                return {
                    id: document.id.toString(),
                    idExterne: document.idExterne,
                    reference: document.reference,
                    nom: document.fichier.nom,
                    taille: document.fichier.taille,
                    dateCreation: new Date().getTime(),
                    contentType: document?.fichier?.contentType,
                    progress: 100
                };
            }
        );

        console.log('Liste des pièces jointes = ', this.piecesJointes);

    }
}

export interface Option<U, T> {
    code: U;
    label: T;
}

export interface CreationActeComponent {
    acte?: ActeDTO;
    actes?: ActeDTO[];
    contrat?: ContratDTO;
    mode?: string;
    type?: TypeFormulaireActe;
}

export abstract class BaseCreationActeComponent<T extends BaseForm<any>> implements CreationActeComponent {

    public VISAS_ACCF: Option<string, string>[] = [
        {code: 'OUI', label: 'Oui'},
        {code: 'NON', label: 'Non'},
        {code: 'INCONNU', label: 'Non renseigné'}
    ];

    public VISAS_PREFET: Option<string, string>[] = [
        {code: 'OUI', label: 'Oui'},
        {code: 'NON', label: 'Non'},
        {code: 'INCONNU', label: 'Non renseigné'}
    ];

    public TYPES_FOURNISSEUR_ENTREPRISE: Option<string, string>[] = [
        {code: 'TITULAIRE', label: 'Titulaire'},
        {code: 'CO-TRAITANT', label: 'Co-traitant'},
        {code: 'SOUS-TRAITANT', label: 'Sous-traitant'}
    ];

    public acte?: ActeDTO;
    public actes: ActeDTO[] = [];
    public contrat: ContratDTO = {};
    public mode?: string;
    public type: TypeFormulaireActe;
    public taille: number = 0;
    public formulaire: T;
    public utilisateur: UtilisateurDTO;


    protected constructor(
        public contratService: ContratService,
        protected router: Router,
        protected acteService: ActeService,
        protected etablissementService: EtablissementService,
        protected echangeChorusService: EchangeChorusService
    ) {
        this.utilisateur = USER_CONTEXT?.utilisateur;
    }

    public actualiser(event): void {
        this.taille = event;
    }

    public estValide(form: NgForm): boolean {
        const invalidControls = [];
        const controls = form.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                invalidControls.push(name);
            }
        }

        return form.valid && this.acte?.statut !== 'NOTIFIE';
    }

    public get numero(): string {
        return `${this.contrat.numero}/${(this.actes.length + 1).toString().padStart(4, '0')}`;
    }

    public retour() {
        return this.router.navigate(['/actes', this.contrat.id]);
    }

    public supprimer(acte: ActeDTO): void {
        this.acteService.supprimer(acte).subscribe(
            () => {
                this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                this.router.onSameUrlNavigation = 'reload';
                this.router.navigate(['/actes', acte.contrat.id]).then(
                    () => console.debug('Retour au tableau de bord')
                );
            }
        );
    }

    public getLibelleStatut(): string {
        let result;

        switch (this.formulaire.statut) {
            case StatutActe.BROUILLON:
            case 'BROUILLON':
                result = 'Brouillon';
                break;
            case StatutActe.EN_ATTENTE_ACCUSE:
            case 'EN_ATTENTE_ACCUSE':
                result = 'En attente de l\'accusé de lecture';
                break;
            case StatutActe.EN_ATTENTE_VALIDATION:
            case 'EN_ATTENTE_VALIDATION':
                result = 'En attente de validation';
                break;
            case StatutActe.VALIDE:
            case 'VALIDE':
                result = 'Validé';
                break;
            case StatutActe.NOTIFIE:
            case 'NOTIFIE':
            default:
                result = 'Notifié';
        }

        return result;
    }

    public getStatut(): string {
        let result;

        switch (this.formulaire.statut) {
            case StatutActe.BROUILLON:
            case 'BROUILLON':
                result = 'BROUILLON';
                break;
            case StatutActe.EN_ATTENTE_ACCUSE:
            case 'EN_ATTENTE_ACCUSE':
                result = 'EN_ATTENTE_ACCUSE';
                break;
            case StatutActe.EN_ATTENTE_VALIDATION:
            case 'EN_ATTENTE_VALIDATION':
                result = 'EN_ATTENTE_VALIDATION';
                break;
            case StatutActe.VALIDE:
            case 'VALIDE':
                result = 'VALIDE';
                break;
            case StatutActe.NOTIFIE:
            case 'NOTIFIE':
            default:
                result = 'NOTIFIE';
        }

        return result;
    }

    public creer() {
        console.log(`Ajout d'un nouvel acte`);

        const acte: ActeDTO = this.formulaire.toDTO();

        this.acteService.creer(acte).subscribe(_ => this.retour());
    }

    public modifier(): void {
        console.log(`Modification de l'acte ${this.formulaire.id}`);

        const acte: ActeDTO = this.formulaire.toDTO();

        if ('EN_ERREUR' === this.acte.statut) {
            this.acteService.modifier(acte).subscribe(_ => {
                this.echangeChorusService.ressoumettre(this.acte).subscribe((echange: EchangeChorusDTO): void => {
                    console.log(`Ressoumission de l'échange CHORUS = `, echange);
                    acte.echangeChorus = echange;
                    this.retour();
                }, error => {
                    console.error('Erreur lors de la ressoumission CHORUS', error);
                });
            });

        } else {
            this.acteService.modifier(acte).subscribe(_ => this.retour());
        }
    }

    public get verrouille(): boolean {
        return this.mode === 'lock';
    }

    public estEligibileCHORUS(): boolean {
        return this.contrat?.service?.echangesChorus;
    }

    public modifierTitulaire(etablissement: EtablissementDTO): void {
        console.log(`Mise a jour du titulaire avec l'établissement = `, etablissement);

        if (!this.formulaire.titulaire) {
            this.formulaire.titulaire = {};
        }

        if (etablissement) {
            this.formulaire.titulaire.etablissement = etablissement;
            this.formulaire.titulaire.nom = etablissement.fournisseur.raisonSociale;
            this.formulaire.titulaire.email = etablissement.fournisseur.email;
            this.formulaire.titulaire.telephone = etablissement.fournisseur.telephone;
            this.formulaire.titulaire.adresse = (etablissement.adresse ? `${etablissement.adresse?.adresse} ${etablissement.adresse?.codePostal} ${etablissement.adresse?.commune}` : '');
        }


    }
}
