import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {PieceJointeDTO} from '@dto/messagerie-dto';
import {Utils} from '@components/shared/utils';
import {ActeService} from '@services';
import {webServiceEndpoint} from 'app/constants';
import {USER_CONTEXT} from '@common/userContext';
import {saveAs} from 'file-saver';
import {DropzoneFile} from 'dropzone';
import {DropzoneConfigInterface, DropzoneDirective} from 'ngx-dropzone-wrapper';

declare var Dropzone: any

@Component({
    selector: 'ltexec-acte-document-upload',
    templateUrl: './acte-document-upload.component.html',
    styleUrls: ['./acte-document-upload.component.scss']
})


export class ActeDocumentUploadComponent implements AfterViewInit {

    @Output() filesupdated: EventEmitter<PieceJointeDTO[]>;
    currentUploadedFilesSize: number = 0;
    @Output() sizeChange: EventEmitter<number> = new EventEmitter();
    @Output() onSelectionDocuments = new EventEmitter();
    @ViewChild(DropzoneDirective, {static: false}) dropzdropzoneDirective: DropzoneDirective;
    options: DropzoneConfigInterface;

    constructor(private acteService: ActeService, private utils: Utils) {
        this.filesupdated = new EventEmitter();

    }

    private _piecesJointes: PieceJointeDTO[] = [];

    get piecesJointes() {
        return this._piecesJointes;
    }

    @Input() set piecesJointes(_piecesJointes: PieceJointeDTO[]) {
        this._piecesJointes = _piecesJointes;
        if (_piecesJointes && _piecesJointes.length > 0) {
            this.currentUploadedFilesSize = _piecesJointes.map(pc => pc.taille).reduce((sum, current) => sum += current);
            this.sizeChange.emit(this.currentUploadedFilesSize);
        }
    }

    ngAfterViewInit() {
        const self = this;
        setTimeout(function () {
            self.generateDropZone(self);
        }, 500);
    }

    generateDropZone(self) {
        self.options = {
            url: `${webServiceEndpoint}/upload`,
            dictDefaultMessage: 'Déposer les fichiers ici pour les joindre',
            dictFallbackMessage: 'Votre navigateur ne supports pas le drag\'n\'drop.',
            dictFileTooBig: 'La taille totale des pièces jointes associées au message dépasse la taille totale autorisée : {{maxFilesize}} Mo',
            dictResponseError: 'Le serveur a répond  avec le statut : {{statusCode}}.',
            dictCancelUpload: 'Annuler',
            dictCancelUploadConfirmation: 'Voulez vous vraiment annuler cet upload?',
            dictRemoveFile: '<i class=\'fa fa-trash\'></i>',
            dictRemoveFileConfirmation: null,
            dictInvalidFileType: 'You can not upload any more files.',
            dictMaxFilesExceeded: 'You can not upload any more files.',
            previewTemplate: '<div style="display:none"></div>',
            addRemoveLinks: false,
            paramName: 'uploadfile',
            maxFilesize: 60,
            maxThumbnailFilesize: 5,
            timeout: -1,
            error: this.onError,
            headers: {
                'Authorization': `Bearer ${USER_CONTEXT?.authentification.token}`
            }
        };
    }

    deletePieceJointe(pieceJointe: PieceJointeDTO) {
        this.currentUploadedFilesSize -= pieceJointe.taille;
        const deleteFileCallBack = () => {
            this.piecesJointes.splice(this.piecesJointes.indexOf(pieceJointe), 1);
            this.filesupdated.next(this.piecesJointes);
        };

        // si la pièce jointe est correctement renseigné
        if (pieceJointe.file) {
            this.dropzdropzoneDirective.dropzone().removeFile(pieceJointe.file);
        } else {
            // si la pièce jointe est mal renseigné.
            let file = this.dropzdropzoneDirective.dropzone().getAcceptedFiles().find(af => af.name == pieceJointe.nom && af.size == pieceJointe.taille);
            if (file) {
                this.dropzdropzoneDirective.dropzone().removeFile(file);
            }
        }

        if (pieceJointe.reference != null) { // upload récent
            this.acteService.delete(pieceJointe).subscribe(deleteFileCallBack);
        } else {
            deleteFileCallBack();
        }
        this.sizeChange.emit(this.currentUploadedFilesSize);
    }

    download( pieceJointe: PieceJointeDTO) {
        if (pieceJointe.reference) {
            this.acteService.draft(pieceJointe.reference).subscribe(blob => {
                saveAs(blob, pieceJointe.nom);
            });
        } else {
            this.acteService.download(pieceJointe.id).subscribe(blob => {
                saveAs(blob, pieceJointe.nom);
            });
        }


    }

    onError = (file: DropzoneFile) => {
        const pj = this.piecesJointes.find(pj => pj.file === file);
        if (pj) {
            pj.erreur = file.xhr.response;
            pj.progress = 100;
        }

    }

    onAddedFile(file: DropzoneFile) {
        const pieceJointe: PieceJointeDTO = {
            nom: file.name,
            progress: 0,
            taille: file.size,
            file: file,
            dateCreation: new Date().getTime()
        };

        if (!this._piecesJointes) {
            this._piecesJointes = [];
        }
        this._piecesJointes.push(pieceJointe);
        this.currentUploadedFilesSize += file.size;
        this.sizeChange.emit(this.currentUploadedFilesSize);
    }

    onSuccess(files: DropzoneFile) {
        const file = files[0];
        const pieceJointe = this._piecesJointes.find((pj: PieceJointeDTO) => pj.file === file);
        pieceJointe.reference = file.xhr.response.replace('"', '').replace('"', '');
        pieceJointe.file = null;
        this.filesupdated.next(this._piecesJointes);
    }

    onUploadprogress(files: DropzoneFile) {
        const file = files[0];
        const uploadFile = this._piecesJointes.find(pj => pj.file === file);
        uploadFile.progress = 100;
        uploadFile.taille = file.size;
    }

    onQueuecomplete($event: any, dropzone: any) {
        this.dropzdropzoneDirective.reset(false);
    }
}
