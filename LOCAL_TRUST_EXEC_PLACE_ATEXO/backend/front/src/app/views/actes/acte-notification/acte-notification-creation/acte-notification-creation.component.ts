import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ActeDTO} from '@dto/backend';
import {Router} from '@angular/router';
import {ActeService} from '@services';
import {StatutActe, TypeNotification} from '@dto/backendEnum';
import {datify, stringify} from '@common/dateUtils';

@Component({
    selector: 'ltexec-acte-notification-creation',
    templateUrl: './acte-notification-creation.component.html',
    styleUrls: ['./acte-notification-creation.component.scss']
})
export class ActeNotificationCreationComponent {

    acte: ActeDTO;
    dateNotification: Date;

    @Input()
    public set idActe(id: number) {
        this.acteService.recuperer(id).subscribe(acte => {
            this.acte = acte;
            this.dateNotification = datify(this.acte.dateNotification);
        });
    }

    @Output() onClose: EventEmitter<void> = new EventEmitter<void>();
    @Output() onUpdate: EventEmitter<ActeDTO> = new EventEmitter<ActeDTO>();

    constructor(private acteService: ActeService, private router: Router) {
    }

    isValid(): boolean {
        return this.acte && ((this.isChorus() && this.acte.validationChorus) || !this.isChorus());
    }

    suivant(): void {
        this.acte.dateNotification = stringify(this.dateNotification);

        this.acteService.modifier(this.acte).subscribe(acte => {
            if (this.acte.typeNotification === 'MESSAGERIE_SECURISEE') {
                this.onClose.emit();
                this.router.navigate(['/actes/notification', this.acte?.contrat?.id, this.acte.id]);
            } else {
                this.acte.typeNotification = TypeNotification.MANUELLE;
                this.acte.statut = StatutActe.NOTIFIE;
                this.acteService.modifier(this.acte).subscribe(acte => {
                    this.onClose.emit();
                    this.onUpdate.emit(acte);
                });
            }
        })

    }

    isChorus(): boolean {
        return this.acte && this.acte?.contrat?.service?.echangesChorus;
    }
}
