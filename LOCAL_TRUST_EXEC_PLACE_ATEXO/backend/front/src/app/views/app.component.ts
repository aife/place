import {AfterViewInit, Component, OnInit} from '@angular/core';
import {USER_CONTEXT, UserContext} from '@common/userContext';
import {APPLICATION} from '@common/parametrage';
import {environment} from '../../environments/environment';


@Component( {
    selector: 'ltexec-application',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
} )
export class AppComponent implements AfterViewInit, OnInit {
    userContext: UserContext = USER_CONTEXT;
    surchargeHeader = false;

    ngAfterViewInit() {
        APPLICATION.loading().subscribe(
            _ => this.surchargeHeader = APPLICATION.parametrage?.surcharge?.header
        );
    }

    public ngOnInit() {
        if ( environment.production ) {
            this.disableLog();
        }
    }

    private disableLog() {
        ['log', 'debug', 'warn', 'info', 'group', 'error', 'groupEnd', 'alert'].forEach(
            method => console[method] = () => {
            }
        );
    }



}
