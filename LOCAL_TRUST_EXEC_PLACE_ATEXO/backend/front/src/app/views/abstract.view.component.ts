import {Component, Injector, OnInit, TemplateRef} from '@angular/core';
import {ModalService} from '@services';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators/filter';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {ValueLabelDTO} from '@dto';

@Component({
    template: ``,
    selector: 'ltexec-abstract-view'
})
export class AbstractViewComponent implements OnInit {
    protected modalService: ModalService;
    protected router: Router;
    private previousUrl: string;
    public instanceModale: BsModalRef;
    public static history: string[] = [];
    from: string;
    protected activatedRoute: ActivatedRoute;

    constructor(injector: Injector) {
        this.modalService = injector.get(ModalService);
        this.router = injector.get(Router);
        this.activatedRoute=injector.get(ActivatedRoute);
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                console.debug('prev:', event.url);
                if (AbstractViewComponent.history.length === 0) {
                    AbstractViewComponent.history.push(event.url);
                }
                if (AbstractViewComponent.history.length > 0 && AbstractViewComponent.history[AbstractViewComponent.history.length - 1] !== event.url) {
                    AbstractViewComponent.history.push(event.url);
                }
            });
    }

    public goBack() {
        if(this.from){
            this.router.navigate([this.from]);
        }else if(AbstractViewComponent.history[AbstractViewComponent.history.length - 1] === this.router.url){
            this.router.navigate(['/']);
        }else if(AbstractViewComponent.history.length > 1){
            this.router.navigate([this.getPreviousUrl()] );
        }else{
            this.router.navigate(['/']);
        }
    }





    private getPreviousUrl() {
        const length = AbstractViewComponent.history.length;
        if (length - 2 >= 0) {
            return AbstractViewComponent.history[length - 2];
        } else return "/";
    }

    public openModal(component: any, data: any) {
        return this.modalService.openModal(component, data);
    }

    public openModalFromTemplate(template: TemplateRef<any>) {
        this.instanceModale = this.openModal(template, null);
    }

    searchReferentiel(term: string, ref: ValueLabelDTO) {
        const wholeText = ref.value + ' ' + ref.label + ' ' + ref.parentLabel;
        return wholeText.toLowerCase().indexOf(term.toLowerCase()) > -1;
    }

    ngOnInit(): void {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                console.debug('prev:', event.url);
                this.previousUrl = event.url;
            });
        this.activatedRoute.queryParams.subscribe(params => {
            this.from = params?.from;
        });
    }

}
