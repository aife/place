import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Statut, USER_CONTEXT, UserContext} from '@common/userContext';
import {AuthentificationService} from '@services';
import {APPLICATION} from '@common/parametrage';
import {Habilitation} from '@dto/backendEnum';

@Component( {
    selector: 'ltexec-navigation',
    templateUrl: 'navigation.html'

} )
export class NavigationComponent implements OnInit {

    userContext: UserContext = USER_CONTEXT;

    motsCles: String;

    profilRSEM: boolean;

    modeFournisseur: boolean;

    creationContrat: boolean;
    tousContrats: boolean;
    imports: boolean = false;

    constructor( private router: Router, private authentificationService: AuthentificationService ) {
        this.motsCles = '';

    }

    ngOnInit() {
        APPLICATION.loading().subscribe(
            _ => {
                this.modeFournisseur = APPLICATION.parametrage?.fournisseur?.actif;
                this.imports = APPLICATION.parametrage?.imports?.actif;
                this.creationContrat = USER_CONTEXT.hasHabilitation( Habilitation.CREER_CONTRAT ) && APPLICATION.parametrage?.contrat?.creation?.actif;
                this.tousContrats = USER_CONTEXT.hasHabilitation( Habilitation.VOIR_TOUS_CONTRATS );
            }
        );
    }

    onAccueilClick() {

        this.authentificationService.accueilMpe().subscribe( ( redirectionUrl ) => {
            USER_CONTEXT.authentification.token = null;
            USER_CONTEXT.authentification.status = Statut.KO;
            USER_CONTEXT.utilisateur = null;

            window.location.href = redirectionUrl;

        });
    }

    findByMotsCles() {
        if ( this.isMotsClesValid() ) {
            this.router.navigate( ['/contrats'], { queryParams: { motsCles: this.motsCles } } ).then(_ => console.log('Redirection vers /contrats'));
        }
    }

    isMotsClesValid(): boolean {
        return this.motsCles != null && this.motsCles.length > 0;
    }

}
