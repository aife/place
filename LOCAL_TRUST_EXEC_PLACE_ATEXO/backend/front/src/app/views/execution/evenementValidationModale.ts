import {Component, EventEmitter, Input, Output} from '@angular/core';
import {EvenementDTO} from '../../external/backend';

@Component({
    selector: 'ltexec-evenement-validation-modale',
    templateUrl: 'evenementValidationModale.html'
})
export class EvenementValidationModaleComponent {

    @Output() onSubmit: EventEmitter<any> = new EventEmitter();

    @Input() evenementValidation: EvenementDTO;

    @Input() showEvenementValidationModale: boolean;

    @Output() onCancel: EventEmitter<any> = new EventEmitter();

    onConfirmValidation() {
        this.onSubmit.emit();
    }

    onClose() {
        this.showEvenementValidationModale = false;
        this.onCancel.emit();
    }

}
