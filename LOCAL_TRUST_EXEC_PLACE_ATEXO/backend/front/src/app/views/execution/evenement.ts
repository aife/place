import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Observable} from 'rxjs/Observable';
import {from} from 'rxjs/observable/from';
import {mergeAll} from 'rxjs/operators/mergeAll';
import {
    AttributaireService,
    CalendrierService,
    ContratService,
    DocumentService,
    EtablissementService,
    ReferentielService,
    ServiceService,
    UtilisateurService
} from '@services';
import {ActivatedRoute} from '@angular/router';
import {downloadUrl} from '@common/download';
import {
    errorNotification,
    genericErrorNotification,
    messageNotification,
    serverErrorNotification,
    warningNotification
} from '@common/notification';
import * as DateUtils from '@common/dateUtils';
import * as Validation from '@common/validation';
import {isPresent} from '@common/validation';
import {NgForm} from '@angular/forms';
import {RepartitionMontantsModaleComponent} from '../attributaires/repartitionMontantsModale';
import {USER_CONTEXT} from '@common/userContext';
import {
    AttributaireDTO,
    ContratDTO,
    ContratEtablissementDTO,
    DocumentContratDTO,
    DocumentCriteriaDTO,
    EtablissementDTO,
    EvenementDTO,
    ValueLabelDTO
} from '../../external/backend';
import {EtatDateEvenement, EtatEvenement, FormePrix, TypeReferentiel} from '@dto/backendEnum';
import {CompleterItem, CompleterService, RemoteData} from 'ng2-completer';
import {EditionContratEtablissementModel, EditionMode, EditionScope} from '../attributaires/editionDtos';
import {webServiceEndpoint} from 'app/constants';
import {Store} from '@ngrx/store';
import {State} from 'app/store';
import {AbstractViewComponent} from '@views/abstract.view.component';
import * as moment from 'moment';

class CommentaireModel {
    commentaire: string;
}

@Component({
    selector: 'ltexec-evenement',
    templateUrl: 'evenement.html'
})
export class EvenementComponent extends AbstractViewComponent implements OnInit {

    contratId: any;

    ajoutAvenant: boolean;

    ajoutBonCommande: boolean;

    ajoutEvenement: boolean;

    montantAvenant: number;

    evenement: EvenementDTO;

    contrat: ContratDTO;

    documentsLie: Array<DocumentContratDTO>;
    documentDeLie: Array<DocumentContratDTO>;

    documents: Array<DocumentContratDTO>;

    documentsLibres: Array<DocumentContratDTO> = [];
    documentsGeneres: Array<DocumentContratDTO> = [];

    documentsLibresLie: Array<DocumentContratDTO> = [];
    documentsGeneresLie: Array<DocumentContratDTO> = [];

    documentCriteriaDTO: DocumentCriteriaDTO;

    destinataires: ValueLabelDTO[] = [];
    commentaireModel: CommentaireModel;

    typesEvenement: ValueLabelDTO[] = [];

    tauxTVAOptions: Array<ValueLabelDTO> = [];

    etablissementsOptions: Array<EtablissementDTO> = [];

    evenementsOptions: Array<ValueLabelDTO> = [];

    avenantTypes: ValueLabelDTO[] = [];

    numberAvenant: number;

    montantBondeCommandesHT: number = 0;

    montantBondeCommandesTVA: number = 0;

    // Pour que l'enum soit accessible dans le template
    etatDateEvenement = EtatDateEvenement;

    edition: boolean;

    devise: ValueLabelDTO;

    // Utilisés par les modales
    evenementValidation: EvenementDTO = {};

    evenementRejet: EvenementDTO = {};
    montantEvenementAvenant = 0;
    attributaire: AttributaireDTO;
    isSpecial: boolean;
    nouvelEtablissement = false;
    editionContratEtablissementModel: EditionContratEtablissementModel = {
        scope: EditionScope.Attributaire,
        mode: EditionMode.Modification,
        etablissementFromBase: true
    };

    dataRemote: RemoteData;
    searchStr: string = '';
    emailRegexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    @ViewChild(RepartitionMontantsModaleComponent, {static: false})
    private repartitionModale: RepartitionMontantsModaleComponent;
    etablissements: EtablissementDTO[];
    isModalSelectionDocumentsShown: boolean = false;
    showEvenementValidationModale: boolean = false;
    showEvenementRejetModale: boolean = false;
    afficherPopinDepassement: boolean = false;

    constructor(private completerService: CompleterService, private contratService: ContratService, private calendrierService: CalendrierService,
                private store: Store<State>,
                private documentService: DocumentService, private utilisateurService: UtilisateurService,
                private referentielService: ReferentielService, private injector: Injector,
                private attributaireService: AttributaireService, private serviceService: ServiceService, private etablissementService: EtablissementService) {
        super(injector);
        this.montantAvenant = 0;

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_EVENEMENT).content).subscribe(data => {
            this.typesEvenement = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TAUX_TVA).content).subscribe(data => {
            this.tauxTVAOptions = data;
        });

        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_AVENANT).content).subscribe(data => {
            this.avenantTypes = data;
        });

        this.devise = USER_CONTEXT.utilisateur.devise;

    }


    ngOnInit() {
        this.dataRemote = this.completerService.remote(`${webServiceEndpoint}/etablissement/?motsCles=${this.searchStr}`, 'fournisseur.raisonSociale,siret', 'description');

        this.dataRemote.requestOptions({
            'headers': {'Authorization': `Bearer ${USER_CONTEXT.authentification.token}`}
        });

        this.activatedRoute.queryParams.subscribe(params => {
            this.from = params?.from;
        });

        this.activatedRoute.params.subscribe(params => {

            this.documentsLie = [];
            this.documents = [];
            this.contratId = params['contratId'];

            const evenementId = params['evenementId'];

            if (params['avenant']) {
                this.ajoutAvenant = true;
            } else {
                this.ajoutAvenant = false;
            }
            if (params['bonCommand']) {
                this.ajoutBonCommande = true;
            } else {
                this.ajoutBonCommande = false;
            }

            this.documentCriteriaDTO = {
                motsCles: isPresent(params['motsCles']) ? params['motsCles'] : ''
            };

            this.contratService.findEtablissementsById(this.contratId).subscribe(value => {
                this.etablissements = value.map(ce => ce.etablissement);
            })
            const contratObservable = this.contratService.findContratById(this.contratId);
            contratObservable.subscribe(contrat => {
                this.contrat = contrat;
                this.montantBondeCommandesHT = this.contrat.listBondeCommandes
                    .filter(eve => eve.bonCommandeTypeDTO != null && EtatEvenement[eve.etatEvenement] !== EtatEvenement.REJETE)
                    .map(eve => eve.bonCommandeTypeDTO.montantHT).reduce((a, b) => a + b, 0);
                this.montantBondeCommandesTVA = this.contrat.listBondeCommandes
                    .filter(eve => eve.bonCommandeTypeDTO != null && EtatEvenement[eve.etatEvenement] !== EtatEvenement.REJETE)
                    .map(eve => eve.bonCommandeTypeDTO.montantTVA).reduce((a, b) => a + b, 0);
                const attributaireObservable = this.attributaireService.getArborescenceAttributaires(this.contrat.id)
                    .subscribe(attributaire => {
                        this.attributaire = attributaire;
                    });
                // Initialisation de la liste des fournisseurs liés au contrat (contractants)
                const mapContatcsOptions = (ce: ContratEtablissementDTO, group) => {
                    return {
                        value: ce.etablissement.id,
                        label: `${ce.etablissement.fournisseur.raisonSociale} -  ${ce.etablissement.adresse.codePostal} ${ce.etablissement.adresse.commune}`,
                        group: group
                    };
                };

                // Ajout direct d'avenant
                if (this.ajoutAvenant) {
                    this.evenement.typeEvenement = this.typesEvenement.find(e => e.value === 'AVENANT_INCIDENCE_FINANCIERE');
                    this.onTypeChange(this.evenement.typeEvenement);
                }
                if (this.ajoutBonCommande) {
                    this.evenement.typeEvenement = this.typesEvenement.find(e => e.value === 'BON_COMMANDE_FINANCIER');
                    this.onTypeChange(this.evenement.typeEvenement);
                }


                this.fetchPage(0, null, null);

            });


            const evenementsObservable = this.calendrierService.findCalendrierByContrat(this.contratId);
            this.evenementsOptions = [];
            this.numberAvenant = 0;
            const evenementsOptions: Array<ValueLabelDTO> = [];
            evenementsObservable.subscribe(evenements => {
                evenements.forEach(evenement => {
                    if (evenement.avenantType) {
                        this.numberAvenant++;
                    }
                    if (evenement.dateFin != null) {
                        evenementsOptions.push({
                            label: evenement.libelle + ' - ' + DateUtils.formatIsoDate(evenement.dateDebut, 'DD/MM/YYYY')
                                + ' au ' + DateUtils.formatIsoDate(evenement.dateFin, 'DD/MM/YYYY')
                                + ' (' + evenement?.typeEvenement?.label + ')',
                            value: evenement.id + ''
                        });
                    } else {
                        evenementsOptions.push({
                            label: evenement.libelle + ' - ' + DateUtils.formatIsoDate(evenement.dateDebut, 'DD/MM/YYYY')
                                + ' (' + evenement?.typeEvenement?.label + ')',
                            value: evenement.id + ''
                        });
                    }
                });
            }, e => {
            }, () => {
                this.evenementsOptions = evenementsOptions;
            });

            const servicesObservable = this.serviceService.getServices();
            servicesObservable.subscribe(services => {

                // Map entre l'id du service et l'id de l'organisme auquel il appartient
                const serviceToOrganisme: { [serviceId: string]: number } = {};

                services.forEach(service => {
                    serviceToOrganisme[service.id + ''] = service.organisme ? service.organisme.id : null;
                });

                // Récupération des utilisateurs depuis réferentielService (déjà chargé si tout va bien) vers Choosen
                var idService = this.contrat?.service?.id;
                if(idService){
                    this.utilisateurService.getUtilisateursByServiceId(idService).subscribe(utilisateurs => {
                        for (let utilisateur of utilisateurs) {
                            this.destinataires.push({
                                value: utilisateur.email,
                                label: utilisateur.nom + ' ' + utilisateur.prenom,
                                parentLabel: utilisateur?.service?.nom
                            });
                        }
                    })
                }
            });

            let rxObservable;
            this.isSpecial = false;
            if (evenementId) {
                let evenementObservable = this.calendrierService.fetchEvenement(evenementId, this.contratId);
                evenementObservable = this.calendrierService.determineEtatDateEvenement(evenementObservable);
                evenementObservable.subscribe(evenement => {
                    this.evenement = evenement;
                    if (this.evenement.avenantType != null) {
                        this.isSpecial = true;
                        this.montantEvenementAvenant = this.evenement.avenantType.montant;
                    }
                    if (this.evenement.bonCommandeTypeDTO != null) {
                        this.isSpecial = true;
                        this.evenement.bonCommandeTypeDTO.tauxTVA = this.tauxTVAOptions.find(taux => {
                            return taux.value === this.evenement.bonCommandeTypeDTO.tauxTVA.value;
                        });
                    }
                    this.edition = evenement.etatDateEvenement !== EtatDateEvenement.VALIDE
                        && evenement.etatDateEvenement !== EtatDateEvenement.REJETE;
                    // Utilisé par les modales
                    if (evenement.etatDateEvenement === EtatDateEvenement.A_VENIR
                        || evenement.etatDateEvenement === EtatDateEvenement.EN_ATTENTE) {
                        this.evenementValidation = this.evenement;
                        this.evenementRejet = this.evenement;
                        this.evenementValidation.dateDebutReel = this.evenementValidation.dateDebut;
                        this.evenementValidation.dateFinReel = this.evenementValidation.dateFin;
                        this.evenementRejet.dateRejet = new Date();
                    }
                    if (this.evenement.destinatairesLibre) {
                        this.evenement.destinatairesLibre.forEach(d => {
                            const destinataireLibre: ValueLabelDTO = {
                                value: d.value,
                                label: d.label,
                                parentLabel: 'Libre'
                            }
                            this.destinataires.push(destinataireLibre);
                            this.evenement.destinataires.push(destinataireLibre)
                        })

                    }
                });
                rxObservable = from([contratObservable, evenementObservable, servicesObservable]);
            } else {
                this.evenement = {};
                // Par défaut, l'élément a "suivi realisation" à Oui
                this.evenement.suiviRealisation = true;
                this.edition = true;
                rxObservable = from([contratObservable, servicesObservable]);
            }
            rxObservable.pipe(mergeAll()).subscribe(() => {
            }, e => {

                serverErrorNotification(e);
            }, () => {
                // mappings complexes (choosen)

                // Actualisation des montants des bond de commandes
                if (this.evenement && this.evenement.bonCommandeTypeDTO != null
                    && EtatEvenement[this.evenement.etatEvenement] !== EtatEvenement.REJETE) {
                    this.montantBondeCommandesHT -= this.evenement.bonCommandeTypeDTO.montantHT;
                    this.montantBondeCommandesTVA -= this.evenement.bonCommandeTypeDTO.montantTVA;
                }
                // Actualisation des montants des avenants
                if (this.evenement && this.evenement.avenantType && EtatEvenement[this.evenement.etatEvenement] !== EtatEvenement.REJETE) {
                    this.montantAvenant = this.contrat.montantTotalAvenants
                        + ((this.evenement.avenantType.value ? -1 : 1) * this.evenement.avenantType.montant);
                } else {
                    this.montantAvenant = this.contrat?.montantTotalAvenants;
                }


            });

        });
    }

    updateRepartitionDocuments(documents: Array<DocumentContratDTO>) {

        if (documents != null) {
            this.documentsLibres = documents.filter(document => document.documentType === 'LIBRE'
                || document.documentType === 'PIECE_JOINTE_MESSAGE');
            this.documentsGeneres = documents.filter(document => document.documentType === 'GENERE');
        } else {
            this.documentsLibres = [];
            this.documentsGeneres = [];
        }
    }

    updateRepartitionDocumentsLie(documents: Array<DocumentContratDTO>) {

        if (documents != null) {
            this.documentsLibresLie = documents.filter(document => document.documentType === 'LIBRE'
                || document.documentType === 'PIECE_JOINTE_MESSAGE');
            this.documentsGeneresLie = documents.filter(document => document.documentType === 'GENERE');
        } else {
            this.documentsLibresLie = [];
            this.documentsGeneresLie = [];
        }
    }

    onBackClick() {
        this.goBack();
    }

    onDownloadDocumentClick(documentId) {
        downloadUrl(this.documentService.getDownloadURL(documentId));
    }

    evenementsSelectionChanges(evenements: ValueLabelDTO[]) {
        this.documentCriteriaDTO.evenements = evenements.map(e => parseInt(e.value));
        this.fetchPage(0, null, null);
    }

    contratantSelectionChanges(contractants: EtablissementDTO[]) {
        this.documentCriteriaDTO.contractants = contractants.map(c => c.id);
        this.fetchPage(0, null, null);
    }

    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        if (motsCles.length === 0 || motsCles.length > 2) {
            this.documentCriteriaDTO.motsCles = motsCles;
            this.fetchPage(0, null, null);
        }
    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[],
              pushState?: boolean): Observable<PaginationPage<DocumentContratDTO>> {
        const observable: Observable<PaginationPage<DocumentContratDTO>> =
            this.documentService.find(this.contratId, this.documentCriteriaDTO, null, null, null);
        observable.subscribe(documents => {
            documents.content.forEach(document => {

                // initialisation des différentes liste
                document.evenements.forEach(evenement => {
                    if (evenement.id === this.evenement.id) {
                        document.preSelected = true;
                        let index = true;
                        this.documentsLie.forEach(documentLie => {
                            if (documentLie.id === document.id) {
                                index = false;
                            }
                        });
                        if (index) {
                            this.documentsLie.push(document);
                        }
                    } else {
                        document.preSelected = false;
                    }
                });
                this.documentsLie.forEach(documentLie => {
                    if (documentLie.id === document.id) {
                        document.preSelected = true;
                    }
                });
            });
            this.documents = documents.content;
            this.updateRepartitionDocumentsLie(this.documentsLie);
            this.updateRepartitionDocuments(this.documents);
        }, e => {
        });
        return observable;
    }

    onDocumenstSelection($even) {

        this.documents.filter(document => document.selected === true).forEach((document: DocumentContratDTO) => {

            document.preSelected = true;
            document.selected = null;

            this.documentsLie.push(document);
            this.updateRepartitionDocumentsLie(this.documentsLie);
        });
    }

    onDocumenstDeSelection(document) {
        document.preSelected = null;
        const index = this.documentsLie.indexOf(document);
        this.documentsLie.splice(index, 1);
        this.updateRepartitionDocuments(this.documents);
        this.updateRepartitionDocumentsLie(this.documentsLie);
    }

    checkDepassement() {
        let depassement: boolean = this.edition && this.contrat != null;
        if (depassement && this.evenement.avenantType != null) {
            depassement = this.evenement.avenantType.incidence && this.evenement.avenantType.value &&
                (((this.evenement.avenantType.montant + this.montantAvenant) / this.contrat.montant) * 100) > 5;
        } else if (depassement && this.evenement.bonCommandeTypeDTO != null) {
            depassement = FormePrix[this.contrat?.typeFormePrix?.value] !== FormePrix.FORFAITAIRE && this.contrat.borneMaximale && (
                (this.montantBondeCommandesHT + this.evenement.bonCommandeTypeDTO.montantHT) > this.contrat.borneMaximale);

        } else {
            depassement = false;
        }
        this.afficherPopinDepassement = depassement;
        return depassement;
    }

    onSave() {

        // mappings complexes (choosen)
        // destinataires
        if (this.destinataires && this.evenement.destinataires) {
            const knownDestinataires = this.destinataires.filter(d => d.parentLabel?.toLowerCase() != 'libre').map(d => d.value);
            this.evenement.destinatairesLibre = this.evenement.destinataires.filter(c => knownDestinataires.indexOf(c.value) === -1);
            this.evenement.destinataires = this.evenement.destinataires.filter(c => knownDestinataires.indexOf(c.value) > -1);

        } else {
            this.evenement.destinataires = [];
            this.evenement.destinatairesLibre = [];
        }

        if (this.evenement.avenantType && this.evenement.avenantType.autreType) {
            this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_AVENANT).content).subscribe(data => {
                data.forEach(ref => {
                    if (ref.value === this.evenement.avenantType.typeAvenant.value) {
                        this.evenement.avenantType.typeAvenant = ref;
                    }
                });
            });
        }


        // ajout des documents lié a l'évènement
        this.evenement.documents = null;
        this.evenement.documentIds = this.documentsLie?.map(item=>item.id);

        // enregistrement asynchrone
        this.calendrierService.saveEvenement(this.evenement, this.contratId).subscribe(evenement => {
            if (evenement != null) {
                const incidence = {incident: false};
                if (this.contrat.groupement != null && this.evenement.avenantType != null && this.evenement.avenantType.incidence &&
                    this.evenement.avenantType.montant !== this.montantEvenementAvenant) {
                    const montantTotal = this.montantAvenant + this.contrat.montantTotalAvenants +
                        ((this.evenement.avenantType.value ? 1 : -1) * this.evenement.avenantType.montant);
                    if (montantTotal !== this.contrat.montantTotalAvenants) {
                        incidence.incident = true;
                    }
                }

                this.router.navigate(['/calendrier-execution', this.contratId]);

            }
        }, e => {
            errorNotification('Erreur technique lors de l\'enregistrement d\'un événement');
            console.error(e.responseText);

        });


    }

    isModelValid(form: NgForm): boolean {
        this.ajoutEvenement = false;
        if (this.evenement != null && this.evenement.contractant != null && this.evenement.dateDebut != null && this.evenement.libelle != null
            && this.evenement.typeEvenement != null) {
            this.ajoutEvenement = true;
            if ((this.evenement.ponctuel === false || this.evenement.ponctuel == null) && this.evenement.dateFin == null) {
                this.ajoutEvenement = false;
            }
            if (this.evenement.alerte === true && (this.evenement.destinataires == null || this.evenement.destinataires.length === 0
                || this.evenement.delaiPreavis == null)) {
                this.ajoutEvenement = false;
            }
        }

        return this.ajoutEvenement && form.valid;
    }

    emailValidator(email: string): boolean {
        if (email == null || email.length === 0) {
            return false;
        }
        return Validation.isValidEmail(email);
    }

    onConfirmRejet(evenement: EvenementDTO) {

        this.evenement.etatEvenement = EtatEvenement.REJETE;
        this.evenement.dateRejet = evenement.dateRejet;
        this.evenement.commentaireEtat = evenement.commentaireEtat;
        this.calendrierService.saveEvenement(this.evenement, this.contratId).subscribe(() => {
            messageNotification('L\'événement "' + this.evenement.libelle + '" a été mis à jour.');
            if (this.contrat.groupement != null && this.evenement.avenantType && this.evenement.avenantType.incidence) {
                const index = this.contrat.listAvenantsNonRejete.indexOf(evenement, 0);
                if (index > -1) {
                    this.contrat.listAvenantsNonRejete.splice(index, 0);
                }
                this.contrat.montantTotalAvenants -= (this.evenement.avenantType.value ? 1 : -1) * this.evenement.avenantType.montant;
                this.repartitionModale.initModale();
                $('.modal-edition-repartition').modal('show');
            }
            this.evenement.etatDateEvenement = EtatDateEvenement.REJETE;
            this.edition = false;
        }, e => {
            genericErrorNotification();

        });
    }

    onConfirmValidation(evenement: EvenementDTO) {

        this.evenement.etatEvenement = EtatEvenement.VALIDE;
        this.evenement.dateDebutReel = evenement.dateDebutReel;
        this.evenement.dateFinReel = evenement.dateFinReel;
        this.evenement.commentaireEtat = evenement.commentaireEtat;
        this.calendrierService.saveEvenement(this.evenement, this.contratId).subscribe(() => {
            messageNotification('L\'événement "' + this.evenement.libelle + '" a été mis à jour.');
            this.evenement.etatDateEvenement = EtatDateEvenement.VALIDE;
            this.edition = false;
        }, e => {
            genericErrorNotification();

        });
    }

    onTypeChange(codeEvenement: ValueLabelDTO) {
        if (codeEvenement.value.toUpperCase().startsWith('AVENANT')) {
            this.evenement.avenantType = {montant: 0};
            this.evenement.ponctuel = true;
            this.evenement.suiviRealisation = true;
            this.evenement.avenantType.dateDemaragePrestation = this.contrat.dateDemaragePrestation;
            this.evenement.avenantType.dateFin = this.contrat.dateFinContrat;
            this.evenement.avenantType.dateFinMax = this.contrat.dateMaxFinContrat;
            this.evenement.avenantType.typeAvenant = {value: '', label: ''};
            this.evenement.avenantType.numero = this.contrat.referenceLibre + '-' + (this.numberAvenant + 1 > 9 ? '' : '0')
                + (this.numberAvenant + 1);
            this.evenement.avenantType.incidence = false;
            this.evenement.avenantType.autreType = false;
            this.evenement.avenantType.avenantTransfert = false;
            this.evenement.bonCommandeTypeDTO = null;
        } else if (codeEvenement.value.toUpperCase().startsWith('BON_COMMAND')) {
            this.evenement.bonCommandeTypeDTO = {};
            this.evenement.bonCommandeTypeDTO.numero = this.contrat.referenceLibre + '-'
                + (this.contrat.listBondeCommandes.length + 1 > 9 ? '' : '0') +
                (this.contrat.listBondeCommandes.length + 1);
            this.evenement.suiviRealisation = true;
            this.evenement.ponctuel = true;
            this.evenement.bonCommandeTypeDTO.tauxTVA = this.tauxTVAOptions[1];
            this.evenement.bonCommandeTypeDTO.montantTVA = 0;
            this.evenement.bonCommandeTypeDTO.montantHT = 0;
            this.evenement.avenantType = null;
        } else {
            this.evenement.avenantType = null;
            this.evenement.bonCommandeTypeDTO = null;
            this.evenement.ponctuel = null;
            this.evenement.suiviRealisation = null;
        }
    }

    onChangeDate() {
        if (this.evenement.avenantType.dateModifiable) {
            this.evenement.avenantType.dateDemaragePrestationNew = this.evenement.avenantType.dateDemaragePrestation;
            this.evenement.avenantType.dateFinNew = this.evenement.avenantType.dateFin;
            this.evenement.avenantType.dateFinMaxNew = this.evenement.avenantType.dateFinMax;
        } else {
            this.evenement.avenantType.dateDemaragePrestationNew = null;
            this.evenement.avenantType.dateFinNew = null;
            this.evenement.avenantType.dateFinMaxNew = null;
        }
    }

    onChangeIncidence() {
        this.evenement.avenantType.value = true;
        this.evenement.avenantType.montant = 0;
    }

    afficheLabelNom() {
        if (this.evenement.avenantType) {
            return 'Objet de l\'avenant';
        }
        if (this.evenement.bonCommandeTypeDTO) {
            return 'Objet du bon de commande';
        }

        return 'Nom';
    }

    afficheLabelDate() {
        if (this.evenement.avenantType) {
            return 'Date de notification';
        }
        if (this.evenement.bonCommandeTypeDTO) {
            return 'Date de commande';
        }

        return 'Date';
    }

    onClickAutreType() {
        if (this.evenement.avenantType.autreType) {
            this.evenement.avenantType.typeAvenant = {};
        } else {
            this.evenement.avenantType.typeAvenant = null;
        }
    }

    hasIncidence(evenement: EvenementDTO): boolean {
        if (this.contrat != null) {
            const list: Array<EvenementDTO> = this.contrat.listAvenantsNonRejete;
            if (evenement.id != null && evenement.avenantType != null && evenement.avenantType.incidence) {
                const index = this.deepIndexOf(list, evenement);
                if (index > -1) {
                    list.splice(index, 1);
                }

            }
            return list.length > 0 && list.find(evenement_ => {
                return evenement_.avenantType.incidence;
            }) != null;
        }
        return false;
    }

    deepIndexOf(arr: Array<EvenementDTO>, obj: EvenementDTO) {
        return arr.findIndex(function (cur) {
            return cur.id === obj.id;
        });
    }

    testTwoDate(d1: any, d2: any): number {
        if (d1 == null || d2 == null) {
            return 0;
        }
        const date1 = moment(d1).toDate();
        const date2 = moment(d2).toDate();
        if (date1.getTime() > date2.getTime()) {
            return 1;
        } else if (date1.getTime() < date2.getTime()) {
            return -1;
        } else {
            return 0;
        }
    }

    rejetText(evenement: EvenementDTO) {
        return `Cet événément a été rejeté le ${DateUtils.formatIsoDateFromDate(<Date>evenement.dateRejet, 'DD/MM/YYYY')}.`;
    }

    onSelectTauxTVA(code: string) {
        if (code.localeCompare('AUTRE') !== 0) {
            const taux = Number(code);
            this.evenement.bonCommandeTypeDTO.montantTVA = this.evenement.bonCommandeTypeDTO.montantHT * taux;
        }
    }

    setCurrentDocument(document: DocumentContratDTO) {

    }

    onSelectedEtablissement(selected: CompleterItem) {
        if (selected) {
            if (!this.exists(selected.originalObject.siret)) {
                this.editionContratEtablissementModel.etablissement = selected.originalObject;
                this.evenement.nouveauContractant = this.editionContratEtablissementModel.etablissement;
            } else {
                warningNotification('L\'établissement est déjà ajouté en tant que contractant sur ce contrat.');
                this.editionContratEtablissementModel.etablissement = null;
            }

        } else {
            this.editionContratEtablissementModel.etablissement = null;
        }
    }

    exists(siret: string): boolean {
        return this.etablissements.find(etablissement => etablissement.siret === siret) != null;
    }

    onRechercherEtablissementBySiretClick() {
        this.editionContratEtablissementModel.etablissement = null;

        if (this.exists(this.editionContratEtablissementModel.siret)) {
            let message = 'L\'établissement est déjà ajouté en tant que contractant sur ce contrat.';
            if (this.contrat.groupement != null) {
                message = 'Modification refusée : le nouveau titulaire ne peut pas être déjà cotraitant.'
            }
            warningNotification(message);
        } else {

            this.etablissementService.findEtablissementBySiret(this.editionContratEtablissementModel.siret).subscribe(etablissement => {
                this.editionContratEtablissementModel.etablissement = etablissement;
                this.evenement.nouveauContractant = etablissement;
            }, e => {
                if (e.status === 404) {
                    errorNotification('Le SIRET est inconnu des données INSEE');
                } else {
                    errorNotification('Services d’identification automatique indisponibles.' +
                        ' Merci de réessayer ultérieurement');
                }


            });
        }
    }

    onAddDestinataire = (term) => {
        if (!this.emailRegexp.test(term) || this.destinataires.find(d => d.value === term) != null) {
            return null;
        } else {
            const ret: ValueLabelDTO = {parentLabel: "Libre", value: term, label: term};
            return ret;
        }
    }

    onCloseEvenementValidationModal() {
        this.showEvenementValidationModale = false;
    }

    onCloseRejet() {
        this.showEvenementRejetModale = false;
    }
}
