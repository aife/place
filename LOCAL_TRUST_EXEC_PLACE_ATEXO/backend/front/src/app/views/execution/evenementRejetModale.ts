import {Component, EventEmitter, Input, Output} from '@angular/core';
import {EvenementDTO} from '../../external/backend';

@Component({
    selector: 'ltexec-evenement-rejet-modale',
    templateUrl: 'evenementRejetModale.html'
})
export class EvenementRejetModaleComponent {

    @Output() onSubmit: EventEmitter<any> = new EventEmitter();

    @Input() evenementRejet: EvenementDTO;

    @Input() showEvenementRejetModale: boolean;

    @Output() onCancel: EventEmitter<any> = new EventEmitter();

    onConfirmRejet() {
        this.onSubmit.emit();
    }

    onClose() {
        this.showEvenementRejetModale = false;
        this.onCancel.emit();
    }

}
