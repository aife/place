import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {merge} from 'rxjs/observable/merge';
import {filter} from 'rxjs/operators/filter';
import {debounceTime} from 'rxjs/operators/debounceTime';
import {distinctUntilChanged} from 'rxjs/operators/distinctUntilChanged';
import {
    AttributaireService,
    CalendrierService,
    ContratService,
    EvenementCriteriaDTO,
    FaqService,
    ReferentielService
} from '@services';
import {ActivatedRoute} from '@angular/router';
import {genericErrorNotification, messageNotification, serverErrorNotification} from '@common/notification';
import {RepartitionMontantsModaleComponent} from '../attributaires/repartitionMontantsModale';
import {Table} from '@components/table/table';
import {EtatDateEvenement, EtatEvenement, TypeReferentiel} from '@dto/backendEnum';
import {AttributaireDTO, ContratDTO, EtablissementDTO, EvenementDTO, ValueLabelDTO} from '@dto';
import {Store} from '@ngrx/store';
import {State} from '../../store';

import * as $ from 'jquery';
import {AbstractViewComponent} from '@views/abstract.view.component';

// Enrichit le DTO du backend avec un état tenant compte de la date de début de l'événement
declare module '../../external/backend' {
    export interface EvenementDTO {
        etatDateEvenement?: EtatDateEvenement;
    }
}

@Component({
    selector: 'ltexec-calendrier-execution',
    templateUrl: 'calendrier.html'
})
export class CalendrierExecutionComponent extends AbstractViewComponent implements Table, OnInit {


    findEvenementsDTO: EvenementCriteriaDTO = {
        motsCles: '',
        etatNonSuivi: false,
        etatAVenir: false,
        etatEnAttente: false,
        etatValide: false,
        etatRejete: false,
        dateEnvoiDebut: null,
        dateEnvoiFin: null,
        evenementTypeCode: [],
        contractants: []
    };

    motsCles: string;

    contratId: any;

    evenements: Array<EvenementDTO>;

    evenementSuppression: EvenementDTO;

    commentaireEdition: EvenementDTO;

    contrat: ContratDTO;

    searchTerms = new Subject<string>();

    // Pour que l'enum soit accessible dans le template
    etatDateEvenement = EtatDateEvenement;

    evenementValidation: EvenementDTO;

    evenementRejet: EvenementDTO;

    evenementAnnulation: EvenementDTO;

    from: string;

    @ViewChild(RepartitionMontantsModaleComponent, {static: false})
    private repartitionModale: RepartitionMontantsModaleComponent;

    attributaire: AttributaireDTO;

    // Permet de l'utiliser avec le composant table-sort pour garder une trace du tri en cours. N'a pas d'incidence sur la pagination.
    evenementPage: PaginationPage<EvenementDTO> = {
        number: 0,
        size: 10,
        order: [{property: 'dateDebut', direction: Order.DESC}]
    };
    evenementTable: Table;

    self: Table;
    etablissements: EtablissementDTO[] = [];
    typesEvenements: ValueLabelDTO[] = [];
    showEvenementValidationModale = false;
    showEvenementRejetModale = false;
    showEvenementAnnulationModal = false;
    showEvenementSuppressionModale = false;

    constructor(private contratService: ContratService, private calendrierService: CalendrierService,
                 private attributaireService: AttributaireService, private store: Store<State>,
                private referentielService: ReferentielService, private faqService: FaqService, private injector: Injector) {
        super(injector);
        this.evenements = [];
        this.self = this;
        this.evenementTable = this;
    }

    ngOnInit() {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.TYPE_EVENEMENT).content).subscribe(data => {
            this.typesEvenements = data;
        });

        this.activatedRoute.queryParams.subscribe(params => {
            this.from = params?.from;
        });

        this.activatedRoute.params.subscribe(params => {

            this.contratId = params['contratId'];
            this.faqService.currentContratId = this.contratId;
            const repartitionIncident = params['incident'];


            const contratObservable = this.contratService.findContratById(this.contratId);
            contratObservable.subscribe(contrat => {
                this.contrat = contrat;
                if (this.contrat.groupement != null) {
                    this.attributaireService.getArborescenceAttributaires(contrat.id)
                        .subscribe(attributaire => {
                            this.attributaire = attributaire;
                            if (repartitionIncident != null && repartitionIncident == 'true') {
                                this.repartitionModale.attributaire = attributaire;
                                this.repartitionModale.contrat = contrat;
                                this.repartitionModale.initModale();
                                $('.modal-edition-repartition').modal('show');
                            }

                        });
                }
                const attributaire = 'Attributaires du contrat';
                const autre = 'Autres fournisseurs';
                this.contratService.findEtablissementsById(this.contratId).subscribe(contratEtablissements => {
                    contratEtablissements.forEach(ce => {
                        const etablissement = ce.etablissement;
                        if (etablissement != null) {
                            if (ce.commanditaire == null) {
                                etablissement.type = attributaire;
                            } else {
                                etablissement.type = autre;
                            }
                        }
                        this.etablissements = this.etablissements.concat(etablissement);
                    })
                });

            });

            const calendrierObservable = this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);

            merge(contratObservable, calendrierObservable).subscribe(() => {
            }, e => {

                serverErrorNotification(e);
            }, () => {


            });
        });

        this.searchTerms.pipe(
            filter((term: string) => term.length === 0 || term.length > 2),
            debounceTime(300),
            distinctUntilChanged())
            .subscribe((term: string) => {
                this.findEvenementsDTO.motsCles = term;
                this.fetchPage(0, this.evenementPage.size,
                    this.evenementPage.order ? this.evenementPage.order : []);
            });
    }

    onBackClick() {
        super.goBack();
    }

    onConfirmSuppression(evenement: EvenementDTO) {
        this.closeModalSuppression();

        this.calendrierService.deleteEvenement(evenement.id, this.contratId).subscribe(() => {
            // suppression du model view
            if (this.contrat.groupement != null && evenement.avenantType != null && evenement.avenantType.incidence) {
                this.contrat.listAvenantsNonRejete.splice(this.contrat.listAvenantsNonRejete.indexOf(evenement, 0), 1);
                this.contrat.montantTotalAvenants -= (evenement.avenantType.value ? 1 : -1) * evenement.avenantType.montant;
                this.repartitionModale.initModale();
                $('.modal-edition-repartition').modal('show');
            }
            const index = this.evenements.indexOf(evenement, 0);
            if (index > -1) {
                this.evenements.splice(index, 1);
            }
            messageNotification('L\'événement "' + evenement.libelle + '" a été supprimé.');

        }, _ => {
            genericErrorNotification();

        });
    }

    onConfirmRejet(evenement: EvenementDTO) {

        evenement.etatEvenement = EtatEvenement.REJETE;
        this.calendrierService.saveEvenement(evenement, this.contratId).subscribe(() => {
            messageNotification('L\'événement "' + evenement.libelle + '" a été mis à jour.');
            evenement.etatDateEvenement = EtatDateEvenement.REJETE;
            if (this.contrat.groupement != null && evenement.avenantType != null && evenement.avenantType.incidence) {
                this.contrat.listAvenantsNonRejete.splice(this.contrat.listAvenantsNonRejete.indexOf(evenement, 0), 1);
                this.contrat.montantTotalAvenants -= (evenement.avenantType.value ? 1 : -1) * evenement.avenantType.montant;
                this.repartitionModale.initModale();
                $('.modal-edition-repartition').modal('show');
            }
            this.evenementRejet = null;

        }, _ => {
            genericErrorNotification();

        });
        this.showEvenementRejetModale = false;
    }

    onConfirmValidation(evenement: EvenementDTO) {

        evenement.etatEvenement = EtatEvenement.VALIDE;
        this.calendrierService.saveEvenement(evenement, this.contratId).subscribe(() => {
            messageNotification('L\'événement "' + evenement.libelle + '" a été mis à jour.');
            evenement.etatDateEvenement = EtatDateEvenement.VALIDE;
            this.evenementValidation = null;

        }, _ => {
            genericErrorNotification();

        });
        this.showEvenementValidationModale = false;
    }

    onConfirmAnnulation(evenement: EvenementDTO) {

        const isRejet: boolean = evenement.etatDateEvenement === EtatDateEvenement.REJETE;
        evenement.etatEvenement = null;
        evenement.dateDebutReel = null;
        evenement.dateFinReel = null;
        evenement.dateRejet = null;
        evenement.commentaireEtat = null;
        this.calendrierService.saveEvenement(evenement, this.contratId).subscribe(() => {
            messageNotification('L\'événement "' + evenement.libelle + '" a été mis à jour.');
            this.calendrierService.calculerEtatDate(evenement);
            if (evenement.etatDateEvenement === EtatDateEvenement.EN_ATTENTE) {
                this.contrat.evenementsEnAttenteCount++;
            }
            if (this.contrat.groupement != null && evenement.avenantType != null && evenement.avenantType.incidence && isRejet) {
                this.contrat.listAvenantsNonRejete.push(evenement);
                this.contrat.montantTotalAvenants += (evenement.avenantType.value ? 1 : -1) * evenement.avenantType.montant;
                this.repartitionModale.initModale();
                $('.modal-edition-repartition').modal('show');
            }
            this.evenementAnnulation = null;

        }, _ => {
            genericErrorNotification();

        });
        this.showEvenementAnnulationModal = false;
    }

    onValidCommentaire(evenement: EvenementDTO) {
        this.closeModalCommentaire();
        this.calendrierService.saveEvenement(evenement, this.contratId).subscribe(() => {
            // mise à jour du model view
            const index = this.evenements.indexOf(evenement, 0);
            if (index > -1) {
                this.evenements[index].commentaire = evenement.commentaire;
            }
            $('[data-toggle="tooltip"]').tooltip('show');
            messageNotification('L\'événement "' + evenement.libelle + '" a été mis à jour.');
        }, _ => {
            genericErrorNotification();
        });
    }

    closeModalSuppression() {
        this.showEvenementSuppressionModale = false;
    }

    closeModalCommentaire() {
        $('#myModal_commentaire').modal('hide');
    }


    setEvenementSuppression(value: EvenementDTO) {
        this.evenementSuppression = value;
        this.showEvenementSuppressionModale = true;
    }

    setEvenementValidation(value: EvenementDTO) {
        this.showEvenementValidationModale = true;
        this.evenementValidation = value;
        this.evenementValidation.dateDebutReel = this.evenementValidation.dateDebut;
        this.evenementValidation.dateFinReel = this.evenementValidation.dateFin;
    }

    setEvenementRejet(value: EvenementDTO) {
        this.showEvenementRejetModale = true;
        this.evenementRejet = value;
        this.evenementRejet.dateRejet = new Date();
    }

    setEvenementAnnulation(value: EvenementDTO) {
        this.evenementAnnulation = value;
        this.showEvenementAnnulationModal = true;
    }

    setCommentaireEdition(value: EvenementDTO) {
        this.commentaireEdition = value;
    }

    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        this.searchTerms.next(motsCles);
    }

    statutNonSuiviClick() {
        this.findEvenementsDTO.etatNonSuivi = !this.findEvenementsDTO.etatNonSuivi;
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    statutEnAttenteClick() {
        this.findEvenementsDTO.etatEnAttente = !this.findEvenementsDTO.etatEnAttente;
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    statutAVenirClick() {
        this.findEvenementsDTO.etatAVenir = !this.findEvenementsDTO.etatAVenir;
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    statutValideClick() {
        this.findEvenementsDTO.etatValide = !this.findEvenementsDTO.etatValide;
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    statutRejeteClick() {
        this.findEvenementsDTO.etatRejete = !this.findEvenementsDTO.etatRejete;
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    afficheLabelEvenement(evenement: EvenementDTO) {
        let label = `${evenement.typeEvenement.label}`;
        if (evenement.avenantType != null) {
            label = label + ` n° ${evenement.avenantType.numero}`;
        }
        if (evenement.bonCommandeTypeDTO != null) {
            label = label + ` n° ${evenement.bonCommandeTypeDTO.numero}`;
        }
        return label;
    }

    /**
     * Est appelé par le composant table-sort. Normalement, le tri/pagination est assuré côté serveur mais ici
     * on doit trier selon dateDebut ou dateDebutReel, donc on le fait côté client.
     * @param pageNumber Ne sert à rien ici
     * @param pageSize Ne sert à rien ici
     * @param order Paramètres de tri
     * @returns {null}
     */
    fetchPage(pageNumber: number, pageSize: number, order: PaginationPropertySort[]): Observable<any> {

        this.evenementPage.number = pageNumber;
        this.evenementPage.size = pageSize;
        this.evenementPage.order = order;
        let observable = this.calendrierService.findEvenements(this.findEvenementsDTO, this.contratId, this.evenementPage.number, this.evenementPage.size, order);
        observable.subscribe(page => {
            this.evenements = page.content;
            this.evenementPage = page;
            this.calendrierService.determineEtatDateEvenements(this.evenements)

        });
        return observable;
    }

    contratantSelectionChanges(contratctants: EtablissementDTO[]) {
        this.findEvenementsDTO.contractants = contratctants.map(e => e.id);
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    evenementTypeSelectionChanges(types: ValueLabelDTO[]) {
        this.findEvenementsDTO.evenementTypeCode = types.map(vl => vl.value);
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    onChangeDate(dates: Date []) {
        if (dates && dates.length === 2) {
            this.findEvenementsDTO.dateEnvoiDebut = dates[0];
            this.findEvenementsDTO.dateEnvoiFin = dates[1];
        } else {
            this.findEvenementsDTO.dateEnvoiDebut = null;
            this.findEvenementsDTO.dateEnvoiFin = null;
        }
        this.fetchPage(0, this.evenementPage.size, this.evenementPage.order ? this.evenementPage.order : []);
    }

    onCloseRejet() {
        this.showEvenementRejetModale = false;
    }

    onCloseEvenementValidationModal() {
        this.showEvenementValidationModale = false;
    }

    currentUrl() {
        return this.router.url;
    }
}
