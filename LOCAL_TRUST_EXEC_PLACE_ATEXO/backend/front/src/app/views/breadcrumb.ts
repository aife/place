import {Component, Input} from '@angular/core';

@Component({
    selector: 'ltexec-breadcrumb',
    templateUrl: 'breadcrumb.html'
})
export class BreadcrumbComponent {

    @Input() items: Array<BreadcrumbItem>;

    constructor() {

    }
}

interface BreadcrumbItem {

    routeAlias: string ;
    label: string ;
}
