import {Component, Inject} from '@angular/core';
import {downloadUrl} from '../../common/download';
import {Router} from '@angular/router';
import {webServiceEndpoint} from "../../constants";

@Component({
    selector: 'ltexec-guide',
    templateUrl: 'guide.html',
})
export class GuideComponent {

    router: Router;

    constructor(@Inject(Router) router: Router) {
        this.router = router;
    }

    downloadGuide() {
        downloadUrl(webServiceEndpoint + '/document/guide-utilisateur');
    }
}
