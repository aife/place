import {Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {onErrorResumeNext} from 'rxjs/observable/onErrorResumeNext';
import * as Notification from '@common/notification';
import {ActivatedRoute} from '@angular/router';
import {
    ContactService,
    ContratService,
    DocumentExterneService,
    EtablissementService,
    FournisseurService
} from '@services';
import {NgForm} from '@angular/forms';
import {APPLICATION_CONTEXT} from '@common/applicationContext';
import {AdresseDTO, ContactDTO, EditionContactDTO, EtablissementDTO, FournisseurDTO, ValueLabelDTO} from '@dto';
import {DocumentsExternesDTO} from '@dto/documentExterne';
import {isSiretValid} from '@common/validation';
import {SyntheseContratComponent} from './synthese-contrat/synthese-contrat.component';
import {ConfirmationModalComponent} from '@components/modals/ConfirmationModal';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {saveAs} from 'file-saver';
import {AbstractViewComponent} from '@views/abstract.view.component';
import {State} from 'app/store';
import {Store} from '@ngrx/store';
import * as moment from 'moment';
import {TypeReferentiel} from '@dto/backendEnum';

enum EditionScope {
    Contact, Etablissement
}

enum EditionMode {
    Creation, Modification, Delete
}

interface EditionContactModel {
    scope?: EditionScope;
    mode?: EditionMode;
    id?: number;
    etablissement?: EtablissementDTO;
    etablissementComanditaire?: EtablissementDTO;
    nom?: string;
    prenom?: string;
    fonction?: string;
    email?: string;
    telephone?: string;
    position?: number;
}

interface EditionEtablissementModel {
    scope?: EditionScope;
    mode?: EditionMode;
    etablissementCode?: string;
    fournisseur?: FournisseurDTO;
    siege?: boolean;
    voie?: string;
    codePostal?: string;
    ville?: string;
    pays?: string;
    id?: number;
}

interface AttestationModel {
    libelle?: string;
    date?: string;
    url?: string;
}

interface EtablissementAttestationsModel {
    etablissement: EtablissementDTO;
    attestations?: Array<AttestationModel>;
    error?: boolean;
    urlZip?: string;
    collapsed?: boolean;
}

@Component({
    selector: 'ltexec-fournisseur-fiche',
    templateUrl: 'ficheFournisseur.html'
})

export class FicheFournisseurComponent extends AbstractViewComponent implements OnInit {
    // Fournisseur
    fournisseurId: any;
    fournisseur: FournisseurDTO;
    editionEtablissementModel: EditionEtablissementModel = {};
    codeApeNafOptions: Array<string> = [];
    formeJuridiqueOptions: Array<string> = [];
    @Input() codeApeNafNace: string;
    @Input() formeJuridique: string;
    @Input() uuid: string;
    @Input() hideNavigation = false;
    editionFournisseur = false;
    selectionMode: boolean = APPLICATION_CONTEXT.modeSelectionContact;
    editionMode = EditionMode;

    @Output() editionModeEvent: EventEmitter<boolean> = new EventEmitter();
    editionModeCount = 0;

    pays: string;

    editionEtranger = false;
    ficheEtrangere = false;

    // Documents
    attestationsSiegeSocial: Array<AttestationModel>;
    errorAttestationsSiegeSocial = false;
    urlAttestationsSiegeSocialZip: string;
    attestationsEtablissements: Array<EtablissementAttestationsModel> = [];
    nombreDocuments: number = null;

    // Contacts
    editionContactModel: EditionContactModel = {};
    etablissementOptions: Array<EtablissementDTO> = [];
    paysOptions: Array<string> = [];

    @ViewChild(SyntheseContratComponent, {static: false})
    syntheseContratModal: SyntheseContratComponent;

    modalRef: BsModalRef;
    etablissements: EtablissementDTO[] = [];
    isModalAjouEtablissementShown = false;
    isModalSupprimerContactShown = false;
    isModalModifierContactShown = false;
    categorieFournisseur: ValueLabelDTO[];
    private FRANCE: string = 'France';
    displayAllContact: boolean = false;
    constructor(
        private fournisseurService: FournisseurService,
        private contratService: ContratService,
        private contactService: ContactService,
        private store: Store<State>,
        private etablissementService: EtablissementService,
        private documentExterneService: DocumentExterneService,
        private injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.PAYS).content).subscribe(data => {
            this.paysOptions = data.map(pays => pays.label);
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CATEGORIE_FOURNISSEUR).content).subscribe(data => {
            this.categorieFournisseur = data;
        });
        if (this.uuid != null) {
            this.loadFournisseur(this.fournisseurService.findByUuid(this.uuid));
            return;
        } else {
            this.activatedRoute.params.subscribe(params => {
                if (!!params['uuid'] && params['uuid'] !== 'undefined' && params['uuid'] !== 'null') {
                    this.uuid = params['uuid'];
                    this.loadFournisseur(this.fournisseurService.findByUuid(this.uuid));
                }
                else if (!!params['fournisseurId'] && params['fournisseurId'] !== 'undefined' && params['fournisseurId'] !== 'null') {
                    this.fournisseurId = params['fournisseurId'];
                    this.loadFournisseur(this.fournisseurService.findFournisseur(this.fournisseurId));
                }
                else {
                    //récupérer le fournisseur à partir de l id de contrat dans l'url sinon accès à ce composant KO depuis la fiche contrat
                    this.activatedRoute.paramMap.subscribe(params => {
                        if (!!params.get('contratId') && params['contratId'] !== 'undefined' && params['contratId'] !== 'null') {
                            let idContrat = params.get('contratId');
                            this.contratService.findContratById(parseInt(idContrat)).subscribe(value => {
                                this.loadFournisseur(this.fournisseurService.findByUuid(value.attributaire.etablissement.fournisseur.uuid));
                            })
                        } else {
                            console.log ("Problème aucun moyen de récupérer l'uuid du fournisseur")
                        }
                    });
                }
            });
        }

    }

    loadFournisseur(fournisseurObservable: Observable<FournisseurDTO>) {
        fournisseurObservable.subscribe(fournisseur => {
            this.fournisseur = fournisseur;
            // EXEC-137 Fiche étrangère
            if (this.fournisseur.trusted == false) {
                this.ficheEtrangere = true;
                this.pays = this.paysOptions.find(p => p == this.fournisseur.pays);
                this.editionEtablissementModel.pays = this.paysOptions.filter(this.startsWith(this.fournisseur.pays)).find(pays => pays === this.fournisseur.pays);
                this.changeEtablissements();
            }

            // EXEC-142 Options Etablissement - Modification d'un contact
            this.refreshOptionEtablissement();
            // EXEC-143 Suppression d'un contact
            // Mise à jour du nombre de contact des établissement en fonction de ceux qui sont actifs
            this.majNbContacts();
            this.loadEtablissements(fournisseur.uuid);


        });
    };

    private loadEtablissements(uuid: string) {
        this.fournisseurService.findEtablissementsByUuid(uuid).subscribe(etablissements => {
            this.etablissements = etablissements;
            // Charger les attestations dans le bloc Documents
            this.loadAttestations();
        });
    }

    loadAttestations() {
        const siegeSocialObservable = this.documentExterneService.findDocumentsBySiren(this.fournisseur.siren);

        siegeSocialObservable.subscribe(documentsBySiren => {
            // Parser la réponse
            this.attestationsSiegeSocial = this.parseDocumentsExternesDTO(documentsBySiren);
            // Trier le tableau dans l'ordre alphabétique
            this.attestationsSiegeSocial = this.attestationSort(this.attestationsSiegeSocial);
            // Récupérer l'url du zip global si des documents existent
            if (this.countDocuments(this.attestationsSiegeSocial) !== 0) {
                this.urlAttestationsSiegeSocialZip = this.getUrlAllDocuments(documentsBySiren);
            }
        }, _ => {
            this.errorAttestationsSiegeSocial = true;
        });

        // Les documents des établissements + ceux du siège social
        const allDocumentsObservable: Array<Observable<DocumentsExternesDTO>> = [];
        allDocumentsObservable.push(siegeSocialObservable);

        this.etablissements.forEach(etablissement => {

            const attestationsEtablissement: EtablissementAttestationsModel = {
                etablissement: etablissement,
                attestations: null,
                collapsed: false
            };

            this.attestationsEtablissements.push(attestationsEtablissement);

            const documentObservable = this.documentExterneService.findDocumentsBySiret(etablissement.siret);
            allDocumentsObservable.push(documentObservable);
            documentObservable.subscribe(documentsBySiret => {

                attestationsEtablissement.attestations = [];

                // Parser la réponse
                attestationsEtablissement.attestations = this.parseDocumentsExternesDTO(documentsBySiret);
                // Trier le tableau dans l'ordre alphabétique
                attestationsEtablissement.attestations = this.attestationSort(attestationsEtablissement.attestations);
                // Récupérer l'url du zip global si des documents existent
                if (this.countDocuments(attestationsEtablissement.attestations) !== 0) {
                    attestationsEtablissement.urlZip = this.getUrlAllDocuments(documentsBySiret);
                }

            }, _ => {
                attestationsEtablissement.error = true;
            }, () => {
            });
        });

        // Calculer le nombre de documents
        onErrorResumeNext(allDocumentsObservable).subscribe(() => {
        }, _ => {
        }, () => {
            this.majNbDocuments();
        });
    }

    parseDocumentsExternesDTO(documentsExternesDTO: DocumentsExternesDTO): Array<AttestationModel> {
        const attestations: Array<AttestationModel> = [];
        for (const metadata in documentsExternesDTO.meta) {
            if (documentsExternesDTO.meta[metadata]) {
                const document: AttestationModel = {
                    libelle: documentsExternesDTO.meta[metadata].libelle,
                };
                if (documentsExternesDTO.documents[metadata]) {
                    document.url = documentsExternesDTO.documents[metadata].url;
                    document.date = documentsExternesDTO.documents[metadata].date_mise_a_jour;
                }
                attestations.push(document);
            }
        }
        return attestations;
    }

    attestationSort(attestations: Array<AttestationModel>): Array<AttestationModel> {
        return attestations.sort((a, b) => {
            if (a.libelle > b.libelle) {
                return 1;
            }
            if (a.libelle < b.libelle) {
                return -1;
            }
            return 0;
        });
    }

    getUrlAllDocuments(documentsExternesDTO: DocumentsExternesDTO): string {
        if (documentsExternesDTO && documentsExternesDTO.documents && documentsExternesDTO.documents.zip_archive) {
            return documentsExternesDTO.documents.zip_archive.url;
        } else {
            return null;
        }
    }

    majNbDocuments(): void {
        this.nombreDocuments = 0;
        this.attestationsEtablissements
            .filter(etablissementAttestations => !!etablissementAttestations.attestations)
            .forEach(etablissementAttestations => {
                this.nombreDocuments += this.countDocuments(etablissementAttestations.attestations);
            });
        if (this.attestationsSiegeSocial) {
            this.nombreDocuments += this.countDocuments(this.attestationsSiegeSocial);
        }
    }

    countDocuments(documents: Array<AttestationModel>): number {
        let nb = 0;
        documents.filter(attestation => !!attestation.url)
            .forEach(_ => {
                nb++;
            });
        return nb;
    }

    majNbContacts() {
        for (let i = 0; i < this.fournisseur.etablissements.length; i++) {
            this.fournisseur.etablissements[i].nbContacts = (this.fournisseur.etablissements[i].contacts.filter(function (contact) {
                return contact.actif;
            })).length;
        }
    }

    refreshOptionEtablissement() {
        this.etablissementOptions = this.fournisseur.etablissements;
    }

    changeEtablissements() {
        this.fournisseur.etablissements.forEach(etablissement => {
            console.debug('Etablissement = ', etablissement);
            if (!etablissement?.adresse?.pays) {
                console.debug(`Aucun pays trouvé pour le fournisseur '${this.fournisseur}'`);

                etablissement.adresse.pays = this.FRANCE;
            }
        });
    }

    // Change le siege social pour qu'il n'y en ait qu'un.
    changeSiegeSocial(id: number) {
        this.fournisseur.etablissements.forEach(etablissement => {
            if (etablissement.id !== id && etablissement.siege) {
                etablissement.siege = false;
                const etablissementDTO: EtablissementDTO = {};
                etablissementDTO.id = etablissement.id;
                etablissementDTO.siege = false;
                etablissementDTO.fournisseur = this.fournisseur;
                etablissementDTO.siret = etablissement.siret;
                this.etablissementService.modifierEtablissement(etablissementDTO);
            }
        });
    }

    // Fonction qui permet d'utiliser un paramètre dans le map()
    startsWith(pays: String) {
        return function (item) {
            return (item.value === pays);
        };
    }

    /**
     * Fonctions FOURNISSEUR
     */
    editFournisseur() {
        this.editionFournisseur = true;
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CODE_APE_NAF).content).subscribe(data => {
            this.codeApeNafOptions = data.map(codeApeNaf => codeApeNaf.value + ' - ' + codeApeNaf.label);
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.FORME_JURIDIQUE).content).subscribe(data => {
            this.formeJuridiqueOptions = data.map(formeJuridique => formeJuridique.value + ' - ' + formeJuridique.label);
        });
        if (this.ficheEtrangere) {
            this.editionEtranger = true;
        }
        this.editionModeEvent.emit(true);

    }

    cancelEditFournisseur() {
        this.editionFournisseur = false;
        this.editionModeEvent.emit(false);
        this.editionEtranger = false;
    }

    onBackClick() {
        if (this.editionModeCount > 0) {
            $('.modal-confirmation-retour').modal('show');
            console.log('retour');
        } else {
            this.onConfirmBackClick();
        }
    }

    onConfirmBackClick() {
        super.goBack();
    }

    isModelValid(form: NgForm): boolean {
        return form.form.valid && (this.fournisseur.raisonSociale != null || this.fournisseur.nom != null);
    }

    valider() {

        this.fournisseurService.updateDonneesPrincipales(this.fournisseur).subscribe(fournisseur => {
            this.fournisseur = fournisseur;
            this.majNbContacts();
            if (this.ficheEtrangere) {
                this.changeEtablissements();
            }
            this.cancelEditFournisseur();
            Notification.messageNotification('Informations du fournisseur modifiées.');
        }, _ => {

        }, () => {

        });
    }


    /**
     * Fonctions ETABLISSEMENT
     */
    onAfficherTousContactsClick(flag: boolean) {
        this.displayAllContact = flag;
        this.etablissements.forEach(etablissement => {
            etablissement.collapsed = flag;
        });
    }

    mapViewModelToModelEtablissement(editionEtablissementModel: EditionEtablissementModel, etablissementDTO: EtablissementDTO) {
        etablissementDTO.fournisseur = this.fournisseur;
        etablissementDTO.siege = editionEtablissementModel.siege;
        etablissementDTO.siret = this.fournisseur.siren + editionEtablissementModel.etablissementCode;
        if (editionEtablissementModel.mode === EditionMode.Modification || this.ficheEtrangere) {
            etablissementDTO.id = editionEtablissementModel.id;
            const adresseDTO: AdresseDTO = {};
            adresseDTO.adresse = this.editionEtablissementModel.voie;
            adresseDTO.codePostal = this.editionEtablissementModel.codePostal;
            adresseDTO.commune = this.editionEtablissementModel.ville;
            adresseDTO.pays = this.editionEtablissementModel.pays;
            etablissementDTO.adresse = adresseDTO;
            etablissementDTO.siege = this.editionEtablissementModel.siege;
        }
    }

    onAjouterEtablissementClick(fournisseur: FournisseurDTO) {
        this.editionEtablissementModel = {};
        this.isModalAjouEtablissementShown = true;
        if (this.ficheEtrangere) {
            this.editionEtablissementModel.pays = this.pays;
        }
        setTimeout(() => {
            this.editionEtablissementModel = {
                scope: EditionScope.Etablissement,
                mode: EditionMode.Creation,
                fournisseur: fournisseur
            };
        });
    }

    exists(etablissement: EtablissementDTO) {
        let exists = false;
        for (let i = 0; i < this.editionEtablissementModel.fournisseur.etablissements.length; i++) {
            if (etablissement.siret === this.editionEtablissementModel.fournisseur.etablissements[i].siret) {
                exists = true;
            }
        }
        return exists;
    }

    onValiderAjouterEtablissementClick() {
        const etablissementDTO: EtablissementDTO = {};
        etablissementDTO.fournisseur = this.fournisseur;
        this.mapViewModelToModelEtablissement(this.editionEtablissementModel, etablissementDTO);


        if (this.ficheEtrangere) {
            this.etablissementService.ajouterEtablissementEtranger(etablissementDTO).subscribe(etablissement => {
                const code = etablissement.siret.substr(etablissement.fournisseur.siren.length, etablissement.siret.length);
                if (this.exists(etablissement)) {
                    Notification.messageNotification('L\'établissement ' + code + ' a déjà été ajouté.');

                } else {
                    etablissement.contacts = [];
                    etablissement.nbContacts = 0;
                    this.editionEtablissementModel.fournisseur.etablissements.push(etablissement);
                    this.etablissementOptions.push(etablissement);
                    Notification.messageNotification('L\'établissement a été ajouté.');
                    if (etablissement.siege) {
                        this.changeSiegeSocial(etablissement.id);
                    }
                    this.isModalAjouEtablissementShown = false;
                }
            }, _ => {
                Notification.genericErrorNotification();

            });

        } else {
            const testValidite = isSiretValid({value: (this.fournisseur.siren + this.editionEtablissementModel.etablissementCode)});
            if (testValidite != null && testValidite['invalidSiret']) {
                Notification.errorNotification('Le format du SIRET est invalide.');

            } else {
                if (this.exists({siret: etablissementDTO.siret})) {
                    Notification.warningNotification('L\'établissement ' + etablissementDTO.siret + ' a déjà été ajouté.');

                } else {
                    this.etablissementService.ajouterEtablissement(etablissementDTO).subscribe(etablissement => {
                        etablissement.contacts = [];
                        etablissement.nbContacts = 0;
                        this.editionEtablissementModel.fournisseur.etablissements.push(etablissement);
                        this.etablissementOptions.push(etablissement);
                        Notification.messageNotification('L\'etablissement ' + etablissement.fournisseur.raisonSociale + ' a été ajouté.');
                        this.isModalAjouEtablissementShown = false;
                    }, e => {
                        if (e.status === 404) {
                            Notification.errorNotification('Le SIRET est inconnu des données INSEE.');
                        } else {
                            Notification.errorNotification('Services d’identification automatique indisponibles.' +
                                ' Merci de réessayer ultérieurement.');
                        }


                    });
                }
            }
        }
        this.isModalAjouEtablissementShown = false;
    }

    onSelectionnerContactClick(contact: ContactDTO) {
        eval('opener.postMessage(contact, \'' + APPLICATION_CONTEXT.callBack + '\')');
        window.close();
    }

    onModifierEtablissementClick(etablissement: EtablissementDTO) {
        this.editionEtablissementModel = {};
        if (etablissement.adresse.pays == null) {
            etablissement.adresse.pays = this.FRANCE;
        }
        setTimeout(() => {
            this.editionEtablissementModel = {
                scope: EditionScope.Etablissement,
                mode: EditionMode.Modification,
                etablissementCode: etablissement.siret.substr(etablissement.fournisseur.siren.length, etablissement.siret.length),
                fournisseur: etablissement.fournisseur,
                voie: etablissement.adresse.adresse,
                ville: etablissement.adresse.commune,
                codePostal: etablissement.adresse.codePostal,
                pays: etablissement.adresse.pays,
                siege: etablissement.siege,
                id: etablissement.id
            };
        });
    }

    onValiderModifierEtablissementClick() {

        const etablissementDTO: EtablissementDTO = {};
        etablissementDTO.fournisseur = this.fournisseur;
        this.mapViewModelToModelEtablissement(this.editionEtablissementModel, etablissementDTO);


        this.etablissementService.modifierEtablissement(etablissementDTO).subscribe(etablissement => {
            for (let i = 0; i < this.fournisseur.etablissements.length; i++) {
                if (etablissement.siret === this.fournisseur.etablissements[i].siret) {
                    this.fournisseur.etablissements[i] = etablissement;
                }
            }
            const code = etablissement.siret.substr(etablissement.siret.length - 5, etablissement.siret.length);
            this.majNbContacts();
            this.refreshOptionEtablissement();
            if (etablissement.siege) {
                this.changeSiegeSocial(etablissement.id);
            }
            Notification.messageNotification('L\'établissement ' + code + ' a été modifié.');
            this.isModalAjouEtablissementShown = false;
        }, _ => {
            Notification.genericErrorNotification();

        });
    }

    validateEditionEtablissementModelValid(f: NgForm): boolean {
        return this.fournisseur != null && this.editionEtablissementModel.etablissementCode != null && f.form.valid;
    }


    /**
     * Fonctions CONTACT
     */
    mapViewModelToModelContact(editionContactModel: EditionContactModel, editioncontactDTO: EditionContactDTO) {
        editioncontactDTO.etablissement = this.editionContactModel.etablissementComanditaire;
        if (this.editionContactModel.mode === EditionMode.Modification) {
            editioncontactDTO.etablissement = this.editionContactModel.etablissement;
        }

        editioncontactDTO.nom = editionContactModel.nom;
        editioncontactDTO.prenom = editionContactModel.prenom;
        editioncontactDTO.email = editionContactModel.email;
        editioncontactDTO.fonction = editionContactModel.fonction;
        editioncontactDTO.telephone = editionContactModel.telephone;
        editioncontactDTO.id = editionContactModel.id;
    }

    onAjouterContactClick(etablissementComanditaire: EtablissementDTO) {
        this.editionContactModel = {};
        this.isModalModifierContactShown = true;
        this.editionContactModel = {
            scope: EditionScope.Contact,
            mode: EditionMode.Creation,
            etablissementComanditaire: etablissementComanditaire
        };
    }

    onValiderAjouterContactClick() {
        const editioncontactDTO: EditionContactDTO = {};
        this.mapViewModelToModelContact(this.editionContactModel, editioncontactDTO);

        this.contactService.ajouterContact(editioncontactDTO).subscribe(contact => {
            // on ajoute l'objet crée au model view

            if (this.editionContactModel.etablissementComanditaire.contacts == null) {
                this.editionContactModel.etablissementComanditaire.contacts = [];
            }

            this.editionContactModel.etablissementComanditaire.contacts.push(contact);
            this.editionContactModel.etablissementComanditaire.nbContacts++;
            Notification.messageNotification('Contact ajouté.');
            this.isModalModifierContactShown = false;
        }, _ => {
            Notification.genericErrorNotification();

        });
    }

    onModifierContactClick(etablissementComanditaire: EtablissementDTO, contact: ContactDTO) {
        this.isModalModifierContactShown = true;
        this.editionContactModel = {};
        setTimeout(() => {
            this.editionContactModel = {
                scope: EditionScope.Contact,
                mode: EditionMode.Modification,
                etablissementComanditaire: etablissementComanditaire,
                etablissement: contact.etablissement,
                nom: contact.nom,
                prenom: contact.prenom,
                fonction: contact.fonction,
                email: contact.email,
                telephone: contact.telephone,
                id: contact.id,
            };
        });
    }

    onValiderModifierContactClick() {
        const editioncontactDTO: EditionContactDTO = {};
        this.mapViewModelToModelContact(this.editionContactModel, editioncontactDTO);

        this.contactService.modifierContact(editioncontactDTO).subscribe(contact => {

            // Si on veut changer l'établissement du contact
            if (this.editionContactModel.etablissementComanditaire.id != contact.etablissement.id) {
                // On le supprime de la liste de contacts de l'établissement commanditaire
                const index = this.editionContactModel.etablissementComanditaire.contacts.map(function (e) {
                    return e.id;
                }).indexOf(contact.id);
                this.editionContactModel.etablissementComanditaire.contacts.splice(index, 1);
                this.editionContactModel.etablissementComanditaire.nbContacts--;
                // On change l'établissement commanditaire
                for (let j = 0; j < this.fournisseur.etablissements.length; j++) {
                    if (this.fournisseur.etablissements[j].id === contact.etablissement.id) {
                        this.editionContactModel.etablissementComanditaire = this.fournisseur.etablissements[j];
                        this.editionContactModel.etablissementComanditaire.nbContacts++;
                    }
                }
                this.editionContactModel.etablissementComanditaire.contacts.push(contact);
            } else {
                // Sinon on modifie le contact dans la liste (affichage dynamique)
                for (let i = 0; i < this.editionContactModel.etablissementComanditaire.contacts.length; i++) {
                    if (contact.id === this.editionContactModel.etablissementComanditaire.contacts[i].id) {
                        this.editionContactModel.etablissementComanditaire.contacts[i] = contact;
                    }
                }
            }
            Notification.messageNotification('Le contact a été modifié.');
            this.isModalModifierContactShown = false;

        }, _ => {
            Notification.genericErrorNotification();

        });
        this.isModalModifierContactShown = false;
    }

    onSupprimerContactClick(contact: ContactDTO, etablissement: EtablissementDTO, position: number) {
        this.isModalSupprimerContactShown = true;
        this.editionContactModel = {};
        this.editionContactModel = {
            scope: EditionScope.Contact,
            mode: EditionMode.Delete,
            position: position,
            nom: contact.nom,
            prenom: contact.prenom,
            id: contact.id,
        };
        this.editionContactModel.etablissementComanditaire = etablissement;
        this.editionContactModel.etablissementComanditaire.contacts = etablissement.contacts;
    }

    onValiderSuppressionContactClick() {

        this.contactService.supprimerContact(this.editionContactModel.id).subscribe(() => {
            // suppression du model view
            this.editionContactModel.etablissementComanditaire.contacts.splice(this.editionContactModel.position, 1);
            this.editionContactModel.etablissementComanditaire.nbContacts--;
            Notification.messageNotification('Le contact ' + this.editionContactModel.nom
                + ' ' + this.editionContactModel.prenom + ' a été supprimé.');

        }, _ => {
            Notification.genericErrorNotification();

        });
        this.isModalSupprimerContactShown = false;
    }

    validateEditionContactModelValid(form: NgForm): boolean {
        return this.editionContactModel.nom != null && this.editionContactModel.email != null && form.form.valid;
    }

    initModalSyntheseContrat(clos: boolean) {
        this.syntheseContratModal.initModal(clos);
        //  $('.modal-contrats').modal('show');
    }

    downloadAll(sirenOrSiret, listAttestations) {
        const mapAttestations = {};
        listAttestations.forEach(obj => {
            mapAttestations[obj.libelle] = obj.url;
        });
        this.documentExterneService.getAttestation(sirenOrSiret, mapAttestations).subscribe((response) => {
            const blob = new Blob([response.body as BlobPart], {type: 'application/zip'});
            const date = new Date();
            const fichier = `Attestations-${sirenOrSiret}-${moment(date).format('yyyyMMDD HHmmss')}.zip`
            saveAs(blob, fichier);
        });
    }

    supprimerEtablissement(etablissement: EtablissementDTO) {
        this.openModal(ConfirmationModalComponent, {"class": "msv2"});
        this.modalRef.content.title = 'Demande de confirmation';
        this.modalRef.content.message = 'Êtes-vous sûr de vouloir supprimer cet établissement ?';
        this.modalRef.content.yes.subscribe(_ => {
            this.etablissementService.supprimerEtablissement(etablissement).subscribe(() => {
                const index = this.fournisseur.etablissements.indexOf(etablissement, 0);
                if (index > -1) {
                    this.fournisseur.etablissements.splice(index, 1);
                }
            });
        });
    }


}
