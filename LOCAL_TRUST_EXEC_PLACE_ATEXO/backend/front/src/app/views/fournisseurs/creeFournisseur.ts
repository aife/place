import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {NgForm, UntypedFormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {FournisseurCriteriaDTO, FournisseurService} from '@services';
import {errorNotification, infosNotification, messageNotification, warningNotification} from '@common/notification';
import {APPLICATION_CONTEXT} from '@common/applicationContext';
import {EtablissementDTO, FournisseurDTO, ValueLabelDTO} from '@dto';
import {Store} from '@ngrx/store';
import {State} from 'app/store';

import * as $ from 'jquery';
import {TypeReferentiel} from '@dto/backendEnum';
import {AbstractViewComponent} from '@views/abstract.view.component';

@Component({
    viewProviders: [UntypedFormBuilder, NgForm],
    selector: 'ltexec-cree-fournisseur',
    templateUrl: 'creeFournisseur.html',
    styleUrls: ['./creeFournisseur.scss']
})

export class CreeFournisseurComponent extends AbstractViewComponent implements OnInit {
    siren: String;
    fournisseur: FournisseurDTO = {raisonSociale: '', siren: '', etablissements: [], pays: null};
    ajoutEtranger: boolean;
    idNational: String;
    paysOptions: Array<ValueLabelDTO> = [];
    categorieFournisseur: ValueLabelDTO[];
    pays: ValueLabelDTO;
    raisonSociale: String;
    editionModeCount = 0;
    fromContrat: boolean;
    selectionMode: boolean = APPLICATION_CONTEXT.modeSelectionContact;
    isFirstOpenEntrepriseFrance = true;
    openOnePanelAtATime = true;
    @Output() onSave = new EventEmitter<EtablissementDTO>();
    @Input() hideBackButton = false;
    @Input() navigateOnNewEtablissement = false;

    constructor(private fournisseurService: FournisseurService, private store: Store<State>,
                private injector: Injector) {
        super(injector);
        this.activatedRoute.params.subscribe(params => {
            if (params['etranger']) {
                this.fromContrat = true;
                this.ajoutEtranger = true;
            } else {
                this.fromContrat = false;
            }
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.PAYS).content).subscribe(data => {
            this.paysOptions = data;
        });
        this.store.select(state => state.referentielReducer.referentiels.get(TypeReferentiel.CATEGORIE_FOURNISSEUR).content).subscribe(data => {
            this.categorieFournisseur = data;
        });
    }

    onBackClick() {
        if (this.editionModeCount > 0) {
            $('.modal-confirmation-retour').modal('show');
        } else {
            this.onConfirmBackClick();
        }
    }

    onConfirmBackClick() {
        super.goBack();
    }

    isModelValid(form: NgForm): boolean {
        return this.ajoutEtranger ?
            (this.fournisseur.siren != null && this.fournisseur.pays != null && this.fournisseur.raisonSociale != null && form.form.valid)
            : (this.siren != null && form.form.valid);
    }

    valider() {
        const siren = this.siren.replace(/ /g, '');
        const criteria: FournisseurCriteriaDTO = {siren: siren};
        this.fournisseurService.findFournisseurs(criteria, 0, 10, null).subscribe(fournisseurPage => {
            if (fournisseurPage.totalElements > 0) {

                if (fournisseurPage.totalElements == 1) {
                    this.onSave.emit(fournisseurPage.content.pop().etablissements[0]);
                 } else {
                    warningNotification(`${fournisseurPage.totalElements} entreprise(s) avec le SIREN ${siren}<br> ${fournisseurPage.content.map(f => '<b>' + f.raisonSociale + '</b><br>')}`);
                }

            } else {
                const fournisseur: FournisseurDTO = {siren: siren};
                this.fournisseurService.creerFournisseur(fournisseur).subscribe(fournisseur => {
                    if (fournisseur != null) {
                        if (fournisseur.siren === '') {
                            errorNotification('Le SIREN renseigné est inconnu des données INSEE');
                        } else if (fournisseur.siren === '1') {
                            errorNotification('Services d’identification automatique indisponibles. \nMerci de réessayer ultérieurement.');
                        } else {
                            messageNotification('La Fiche du fournisseur a été créée');
                        }
                        this.onSave.emit(fournisseur.etablissements[0]);
                    }
                }, e => {
                    if (e.status === 302) {
                        console.log(e.error.message);
                        warningNotification(e.error.message);
                    } else {
                        errorNotification('Services d’identification automatique indisponibles. \nMerci de réessayer ultérieurement.');
                    }
                }, () => {

                });

            }
        })

    }

    validerEtranger() {
        this.fournisseur.siren = this.fournisseur.siren.replace(/ /g, '');
        this.fournisseurService.addFournisseurEtranger(this.fournisseur).subscribe(fournisseur => {
            messageNotification('La Fiche du fournisseur a été créée');
            this.onSave.emit(fournisseur.etablissements[0]);
            if (!this.hideBackButton) {
                this.router.navigate(['/ficheFournisseur', fournisseur.id]);
            }
        }, e => {
            if (e.status === 302) {
                const fournisseur = e.responseJSON;
                infosNotification('La Fiche Fournisseur existe déjà.');
            } else {
                errorNotification('Services d’identification automatique indisponibles. \nMerci de réessayer ultérieurement.');
            }

        }, () => {

        });
    }


}
