import {Component, Input, OnInit} from '@angular/core';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {ContratEtablissementDTO, ContratFournisseurCriteriaDTO, EtablissementDTO, FournisseurDTO} from '@dto';
import {FournisseurService} from '@services';
import {genericErrorNotification} from '@common/notification';
import {Observable} from 'rxjs/Observable';
import {Table} from '@components/table/table';
import {Router} from '@angular/router';

@Component({
    selector: 'ltexec-synthese-contrat',
    templateUrl: './synthese-contrat.component.html',
    styleUrls: ['./synthese-contrat.component.scss']
})
export class SyntheseContratComponent implements Table, OnInit {

    contratEtablissemnts: PaginationPage<ContratEtablissementDTO>;
    @Input() fournisseur: FournisseurDTO;
    criteria: ContratFournisseurCriteriaDTO;

    page: number;
    pageSize: number;
    falseModel: any;
    falseMotsCle: any;
    etablissements: EtablissementDTO[] = [];
    isModalSyntheseContratShown: boolean = false;

    constructor(private fournisseurService: FournisseurService, private router: Router) {
    }

    ngOnInit() {
        this.defaultValueForPagination();
    }

    initModal(clos: boolean) {
        this.isModalSyntheseContratShown = true;
        this.criteria = {
            statutProvisoire: false,
            statutClos: false,
            statutEnCours: false,
            motsCles: null,
            sirets: []
        };
        this.falseMotsCle = null;
        if (clos === false) {
            this.criteria.statutEnCours = true;
            this.criteria.statutProvisoire = true;

        } else {
            this.criteria.statutClos = true;
        }
        this.getResult();

    }

    defaultValueForPagination() {
        const empty: ContratEtablissementDTO[] = [];
        this.contratEtablissemnts = {
            content: empty,
            first: true,
            last: false,
            itemsPerPage: 0,
            numberOfElements: 0,
            totalElements: 0,
            size: 0,
            number: 0
        };
    }

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any> {
        return this.fournisseurService.findContrats(this.criteria, pageNumber, this.fournisseur.id, pageSize, sort);
    }

    isGroupement(contratEtablissement: ContratEtablissementDTO): boolean {
        return contratEtablissement.mandataire !== undefined;
    }

    closeandredirect(id: number, link: string) {
        this.isModalSyntheseContratShown = false;
        this.router.navigate([`/${link}`, id]);
    }

    checkStatutsProvisoire() {
        this.criteria.statutProvisoire = !this.criteria.statutProvisoire;
        this.getResult();
    }

    checkStatutsEnCours() {
        this.criteria.statutEnCours = !this.criteria.statutEnCours;
        this.getResult();
    }

    checkStatutsEnCLos() {
        this.criteria.statutClos = !this.criteria.statutClos;
        this.getResult();
    }

    selectSiretByType(etablissements: EtablissementDTO[]) {
        this.criteria.sirets = etablissements.map(etablissement => etablissement.siret);
        this.getResult();
    }

    onSearchKeyUp($event) {
        $event.preventDefault();
        if ($event && $event.target.value.length > 2) {
            this.criteria.motsCles = $event.target.value;
        } else {
            this.criteria.motsCles = null;
        }

        this.getResult();
    }

    private getResult() {
        this.fetchPage(0, this.pageSize,[{property: 'id', direction: Order.ASC}]).subscribe(value => {
            this.contratEtablissemnts = value;
        }, error1 => {
            genericErrorNotification();
            this.defaultValueForPagination();
        });
    }

}
