import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {EtablissementDTO} from '@dto';
import {Subject} from 'rxjs/Subject';
import {catchError} from 'rxjs/operators';
import {switchMap} from 'rxjs/operators/switchMap';
import {of} from 'rxjs';
import {EtablissementService} from '@services';
import {Router} from '@angular/router';

@Component({
    selector: 'ltexec-recherche-fournisseur',
    templateUrl: './recherche.component.html',
    styleUrls: ['./recherche.component.scss']
})
export class RechercheComponent implements OnInit {
    protected etablissements?: Observable<Array<EtablissementDTO>>;
    protected input: Subject<string> = new Subject<string>();
    private value?: EtablissementDTO;
    @Output() public onSelected: EventEmitter<EtablissementDTO> = new EventEmitter<EtablissementDTO>();
    @Input() navigateOnNewEtablissement = false;
    isModaleCreationForunisseurShown: boolean = false;


    public constructor(
        protected etablissementService: EtablissementService, private router: Router
    ) {
    }

    public ngOnInit(): void {
        this.etablissements = this.input.pipe(
            switchMap((term: string) => this.etablissementService.findEtablissementsByRaisonSociale(term)
                .pipe(
                    catchError(() => of([]))
                )
            )
        );
    }

    @Input()
    public set etablissement(value: EtablissementDTO) {
        console.debug('Etablissement choisi = ', value);

        this.value = value;
    }

    public get etablissement(): EtablissementDTO {
        return this.value;
    }

    onNouveeauFournisseur() {
        this.isModaleCreationForunisseurShown = true;
    }
}
