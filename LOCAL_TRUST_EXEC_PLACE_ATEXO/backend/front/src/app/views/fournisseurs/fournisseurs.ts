import {Component, Injector, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {defaultItemsCountPerPage} from 'app/constants';
import {FournisseurCriteriaDTO, FournisseurService} from '@services';
import {PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Table} from '@components/table/table';
import {ActivatedRoute} from '@angular/router';
import {isBlank, isPresent} from '@common/validation';
import * as Notification from '../../common/notification';
import {APPLICATION_CONTEXT} from '@common/applicationContext';
import {FournisseurDTO} from '@dto';
import {AbstractViewComponent} from "@views/abstract.view.component";

@Component({
    selector: 'ltexec-fournisseurs',
    templateUrl: 'fournisseurs.html'
})
export class FournisseursComponent extends AbstractViewComponent implements Table, OnInit {

    fournisseurs: PaginationPage<FournisseurDTO>;

    findFournisseursDTO: FournisseurCriteriaDTO = {
        motsCles: ''
    };

    self: any;

    motsCles: string;
    selectionMode: boolean = APPLICATION_CONTEXT.modeSelectionContact;

    filter: string;


    constructor(private fournisseurService: FournisseurService, private injector: Injector) {
        super(injector);
        this.fournisseurs = {number: 0, size: defaultItemsCountPerPage};


        this.self = this;
    }


    ngOnInit() {
        this.router.routerState.root.queryParams.subscribe(queryParams => {
            this.motsCles = queryParams['motsCles'];
        }, e => {
            Notification.errorNotification('Error : ' + e);
        });

        this.router.routerState.root.queryParams.subscribe(queryParams => {
            this.filter = queryParams['filter'];
        }, e => {
            Notification.errorNotification('Error : ' + e);
        });

        this.activatedRoute.params.subscribe(params => {

            this.findFournisseursDTO = {
                motsCles: isPresent(this.motsCles) ? this.filter === 'none' ? '' : this.motsCles : '',
            };

            this.findFournisseursDTO = {
                motsCles: this.motsCles === 'null' ? '' : this.motsCles
            };

            const pageParam = params['page'];
            const page_ = isBlank(pageParam) ? 0 : parseInt(pageParam, 10);

            const pageSizeParam = params['pageSize'];
            const pageSize_ = isBlank(pageSizeParam) ? defaultItemsCountPerPage : parseInt(pageSizeParam, 10);

            const propertyParam = params['property'];
            const directionParam = params['direction'];

            let sort: PaginationPropertySort[] = [];
            if (!isBlank(propertyParam) && !isBlank(directionParam)) {
                sort = [{property: propertyParam, direction: directionParam}];
            }

            const fournisseursObservable: Observable<PaginationPage<FournisseurDTO>> = this.fetchPage(page_, pageSize_, sort, false);


            fournisseursObservable.subscribe(() => {
            }, () => {

            }, () => {

            });


        });
    }


    /*private pushState(params) {
     this.locationStrategy.pushState(this.routeParams, '', "/fournisseurs", $.param(params));
     }*/

    fetchPage(pageNumber: number, pageSize: number,
              sort: PaginationPropertySort[], pushState?: boolean): Observable<PaginationPage<FournisseurDTO>> {

        const observable: Observable<PaginationPage<FournisseurDTO>> =
            this.fournisseurService.findFournisseurs(this.findFournisseursDTO, pageNumber, pageSize, sort);

        observable.subscribe(fournisseurs => {
            this.fournisseurs = fournisseurs;
            this.fournisseurs.content.map((fournisseur) => fournisseur.id);

        }, _ => {
        });

        return observable;
    }

    updateSearchByMotsCles($event) {
        $event.preventDefault();
        const motsCles = $event.target.value;
        if (motsCles.length === 0 || motsCles.length > 2) {
            this.findFournisseursDTO.motsCles = motsCles;
            this.fetchPage(0, this.fournisseurs.size, null);
        }
    }

    getSiegeSocialCodePostalCommune(fournisseur: FournisseurDTO): string {
        for (const etablissement of fournisseur.etablissements) {
            if (etablissement.siege && etablissement.adresse) {
                return ' – ' + etablissement.adresse.codePostal + ' ' + etablissement.adresse.commune;
            }
        }
        return null;
    }


}
