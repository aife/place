import {
    ApplicationRef,
    CUSTOM_ELEMENTS_SCHEMA,
    forwardRef,
    Inject,
    Injector,
    LOCALE_ID,
    NgModule,
    NO_ERRORS_SCHEMA,
    Type
} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from '@views/app.component';
import {NavigationComponent} from '@views/navigation';
import {BreadcrumbComponent} from '@views/breadcrumb';
import {
    ActeService,
    AttributaireService,
    AuthentificationService,
    CalendrierService,
    ContactService,
    ContratService,
    DocumentExterneService,
    DocumentService,
    EtablissementService,
    FaqService,
    FournisseurService,
    ImportService,
    ParametrageService,
    ReferentielService,
    ServiceService,
    SurchargeService,
    UploadService,
    UtilisateurService
} from '@services';

import {GuideComponent} from '@views/aide/guide';
import {AttributairesComponent} from '@views/attributaires/attributaires';
import {ArborescenceAttributairesComponent} from '@views/attributaires/arborescenceAttributaires';
import {ContactLinkComponent} from '@views/contact/contactLink';
import {CategorieConsultationComponent} from '@views/contrats/categorieConsultation';
import {CONTRAT_INFOS_DIRECTIVES} from '@views/contrats/contratInfos';
import {ContratsComponent} from '@views/contrats/contrats';
import {ContratRecapComponent} from '@views/contrats/contratRecap';
import {ContratTableauBordComponent} from '@views/contrats/contratTableauBord';
import {FicheContratComponent} from '@views/contrats/ficheContrat';
import {DocumentsComponent} from '@views/documents/documents';
import {DocumentSelectionComponent} from '@views/documents/documentSelection';
import {EtablissementLinkComponent} from '@views/etablissement/etablissementLink';
import {CalendrierExecutionComponent} from '@views/execution/calendrier';
import {EvenementComponent} from '@views/execution/evenement';
import {CreeFournisseurComponent} from '@views/fournisseurs/creeFournisseur';
import {FicheFournisseurComponent} from '@views/fournisseurs/ficheFournisseur';
import {FournisseursComponent} from '@views/fournisseurs/fournisseurs';
import {DateTimePicker, DateTimePickerControlValueAccessor} from '@components/datetimepicker/datetimepicker';
import {TABLE_DIRECTIVES} from '@components/table/table';
import {AsteriskComponent} from '@components/asterisk';
import {MoneyDirective} from './directives/money';
import {SirenDirective} from './directives/siren';
import {APPLICATION_PIPES} from '@common/pipes';
import {EnvoiMessageComponent} from '@views/messagerie/envoi/envoiMessage';
import {MessagesHolderComponent} from '@views/messagerie/suivi/messages';
import {UtilisateurLinkComponent} from '@views/utilisateurs/utilisateurLink';
import {ContactsReferantsComponent} from '@views/contact/contactsReferants';
import {APPLICATION_VALIDATORS} from '@common/validation';

import {SiretDirective} from './directives/siret';
import {Ng2CompleterModule} from 'ng2-completer';
import {AlertPanelComponent} from '@components/alertpanel/alertpanel';
import {EvenementValidationModaleComponent} from '@views/execution/evenementValidationModale';
import {EvenementRejetModaleComponent} from '@views/execution/evenementRejetModale';
import {CanDeactivateGuard} from '@common/canDeactivateGuard';
import {ConfirmationRetourModaleComponent} from '@views/confirmationRetourModale';
import {RepartitionMontantsModaleComponent} from '@views/attributaires/repartitionMontantsModale';
import {InlineAddressComponent} from '@components/inlineAddress';
import {GaugeComponent} from '@components/gauge-exec/gauge-exec';
import {ShowHideTextComponent} from '@components/showHide/showHide';
import {ImportContratComponent} from '@views/import/importContrat';
import {ImportContractantComponent} from '@views/import/importContractant';
import {SyntheseContratComponent} from '@views/fournisseurs/synthese-contrat/synthese-contrat.component';
import {AssistanceComponent} from '@components/assistance/assistance.component';
import {HeaderComponent} from '@views/layout/header/header.component';
import {FooterComponent} from '@views/layout/footer/footer.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap/datepicker';
import {ModalModule} from 'ngx-bootstrap/modal';
import {NgSelectModule} from '@ng-select/ng-select';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {SucessModalComponent} from '@components/modals/SuccessModal';
import {ErrorModalComponent} from '@components/modals/ErrorModal';
import {ConfirmationModalComponent} from '@components/modals/ConfirmationModal';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {
    CreationEchangeChorusComponent
} from '@views/actes/chorus/creation-echanges-chorus/creation-echange-chorus.component';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {frLocale} from 'ngx-bootstrap/locale';
import {
    ActeModificatifComponent,
    AgrementSousTraitantComponent,
    DecisionAffermissementTrancheComponent,
    DecisionReconductionComponent,
    OrdreServiceComponent,
    ResiliationComponent
} from '@views/actes/types';
import {FormulaireActeDirective} from '@views/actes/forms';
import {ActeDocumentUploadComponent} from '@views/actes/acte-document-upload/acte-document-upload.component';
import {TableauBordMessageComponent} from 'app/views/messagerie/tableauBord/tableauBordMessage';
import {ActeCreationComponent} from '@views/actes/acte-creation/acte-creation.component';
import {ActeTableauDeBordComponent} from '@views/actes/acte-tableau-bord/acte-tableau-bord.component';
import {
    ActeNotificationCreationComponent
} from '@views/actes/acte-notification/acte-notification-creation/acte-notification-creation.component';
import {ActeAutreComponent} from '@views/actes/types/acte-autre/acte-autre.component';
import {
    DecisionProlongationDelaiComponent
} from '@views/actes/types/decision-prolongation-delai/decision-prolongation-delai.component';
import {ActeAnnulationComponent} from '@views/actes/acte-annulation/acte-annulation.component';
import {ContratVisualisationComponent} from '@views/contrats/contratVisualisation';
import {setTheme} from 'ngx-bootstrap/utils';
import {DOCUMENT, Location, registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {SessionExpiredComponent} from '@components/session-expired/session-expired.component';
import {ErrorInterceptor} from './interceptors/error.interceptor';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {CreationDocumentComponent} from '@views/documents/creation-document/creation-document.component';
import {
    ExportContratsButtonComponent
} from '@components/export/export-contrats-button/export-contrats-button.component';
import {WcListeContratsComponent} from '@views/web-components/wc-liste-contrats/wc-liste-contrats.component';
import {WcFicheContratComponent} from '@views/web-components/wc-fiche-contrat/wc-fiche-contrat.component';
import {createCustomElement} from '@angular/elements';
import {Router} from '@angular/router';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {AccordionModule} from 'ngx-bootstrap/accordion';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {BsDropdownConfig, BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {WcSaisirContratComponent} from '@views/web-components/wc-saisir-contrat/wc-saisir-contrat.component';
import {
    WcListeFournisseursComponent
} from '@views/web-components/wc-liste-fournisseurs/wc-liste-fournisseurs.component';
import {UiSwitchModule} from 'ngx-ui-switch';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store';
import {EffectsModule} from '@ngrx/effects';
import {ReferentielEffect} from './store/referentiel/referentiel.effect';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {NouveauContratComponent} from '@views/contrats/nouveau-contrat/nouveau-contrat.component';
import {RechercheComponent} from '@views/fournisseurs/recherche/recherche.component';
import {DropzoneModule} from 'ngx-dropzone-wrapper';
import {WcFicheFournisseurComponent} from '@views/web-components/wc-fiche-fournisseur/wc-fiche-fournisseur.component';
import {LoaderInterceptor} from './interceptors/loader.interceptor';
import {TableauContratsComponent} from '@views/contrats/tableau-contrats/tableau-contrats.component';
import {WcTableauContratsComponent} from '@views/web-components/wc-tableau-contrats/wc-tableau-contrats.component';
import {AffichageClauseComponent} from '@views/contrats/clause/affichage-clause';
import {CreerClauseComponent} from '@views/contrats/clause/creer-clause';
import {FILTRE_CONTRAT} from 'app/constants';
import {ScrollComponent} from '@components/scroll/scroll.component';
import {DonneesExecutionComponent} from '@views/actes/types/donnees-execution/donnees-execution.component';
import {ContratComponent} from '@views/contrats/nouveau-contrat/contrat/contrat.component';
import {
    ContratConcessionComponent
} from '@views/contrats/nouveau-contrat/contrat-concession/contrat-concession.component';
import {ServiceArbreComponent} from '@components/service-arbre/service-arbre.component';
import {
    ContratEntiteEligibleComponent
} from '@views/contrats/nouveau-contrat/contrat-entite-eligible/contrat-entite-eligible.component';
import {
    WcSupervisionDonneesEssentiellesComponent
} from '@views/web-components/wc-supervision-donnees-essentielles/wc-supervision-donnees-essentielles';
import {
    SupervisionDonneesEssentiellesComponent
} from '@views/supervision-donnees-essentielles/supervision-donnees-essentielles';
import {DonneesEssentiellesService} from '@services/donnneesEssentielles.service';
import {WorkerService} from '@services/worker.service';
import { ContratChorusValidator, ContratNotificationValidator, DateContratNotificationValidator } from './validators';


defineLocale('fr', frLocale);
registerLocaleData(localeFr, 'fr');

@NgModule({
    declarations: [
        ServiceArbreComponent,
        SessionExpiredComponent,
        AppComponent,
        NavigationComponent,
        BreadcrumbComponent,
        GuideComponent,
        AttributairesComponent,
        ArborescenceAttributairesComponent,
        RepartitionMontantsModaleComponent,
        ContactLinkComponent,
        CategorieConsultationComponent,
        CONTRAT_INFOS_DIRECTIVES,
        ContratRecapComponent,
        ContratsComponent,
        ContratTableauBordComponent,
        FicheContratComponent,
        DocumentsComponent,
        ActeTableauDeBordComponent,
        ActeCreationComponent,
        DocumentSelectionComponent,
        EtablissementLinkComponent,
        CalendrierExecutionComponent,
        EvenementComponent,
        CreeFournisseurComponent,
        FicheFournisseurComponent,
        FournisseursComponent,
        ImportContratComponent,
        ImportContractantComponent,

        EnvoiMessageComponent,
        UtilisateurLinkComponent,

        ContactsReferantsComponent,
        NouveauContratComponent,
        GaugeComponent,
        ShowHideTextComponent,
        DateTimePicker,
        TABLE_DIRECTIVES,
        AsteriskComponent,
        InlineAddressComponent,
        AlertPanelComponent,
        EvenementValidationModaleComponent,
        EvenementRejetModaleComponent,
        ConfirmationRetourModaleComponent,
        MoneyDirective,
        SirenDirective,
        SiretDirective,
        APPLICATION_PIPES,
        APPLICATION_VALIDATORS,
        SyntheseContratComponent,
        AssistanceComponent,
        HeaderComponent,
        FooterComponent,
        MessagesHolderComponent,
        TableauBordMessageComponent,
        SucessModalComponent,
        ErrorModalComponent,
        ConfirmationModalComponent,
        CreationEchangeChorusComponent,
        FormulaireActeDirective,
        ActeModificatifComponent,
        OrdreServiceComponent,
        ActeDocumentUploadComponent,
        ActeNotificationCreationComponent,
        AgrementSousTraitantComponent,
        DecisionReconductionComponent,
        DecisionAffermissementTrancheComponent,
        ResiliationComponent,
        ActeAutreComponent,
        DecisionProlongationDelaiComponent,
        ActeAnnulationComponent,
        ContratVisualisationComponent,
        CreationDocumentComponent,
        ExportContratsButtonComponent,
        WcListeContratsComponent,
        WcFicheContratComponent,
        WcSaisirContratComponent,
        WcListeFournisseursComponent,
        RechercheComponent,
        WcListeFournisseursComponent,
        WcFicheFournisseurComponent,
        TableauContratsComponent,
        WcTableauContratsComponent,
        AffichageClauseComponent,
        CreerClauseComponent,
        ScrollComponent,
        DonneesExecutionComponent,
        ContratComponent,
        ContratConcessionComponent,
        ContratEntiteEligibleComponent,
        WcSupervisionDonneesEssentiellesComponent,
        SupervisionDonneesEssentiellesComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        Ng2CompleterModule.forRoot(),
        AppRoutingModule,
        HttpClientModule, BsDatepickerModule.forRoot(), ModalModule.forRoot(), TooltipModule.forRoot(), AccordionModule.forRoot(), PopoverModule.forRoot(),
        FormsModule, NgSelectModule, HttpClientModule, ButtonsModule, CollapseModule.forRoot(), TabsModule.forRoot(),
        BrowserAnimationsModule,
        StoreModule.forRoot(reducers),
        StoreRouterConnectingModule.forRoot({
            stateKey: 'router'
        }),
        EffectsModule.forRoot([
            ReferentielEffect
        ]),
        StoreDevtoolsModule.instrument({
            name: '[EXEC-APP]',
            maxAge: 100
        }),
        NgSelectModule, HttpClientModule, ButtonsModule, CollapseModule.forRoot(), ReactiveFormsModule, BrowserModule, FontAwesomeModule, BsDropdownModule.forRoot(), UiSwitchModule.forRoot({size: 'small'}),
        DropzoneModule
    ],
    providers: [
        ActeService,
        AuthentificationService,
        UtilisateurService,
        ContactService,
        ContratService,
        FournisseurService,
        UploadService,
        EtablissementService,
        AttributaireService,
        ReferentielService,
        DocumentService,
        DocumentExterneService,
        CalendrierService,
        WorkerService,
        ServiceService,
        ParametrageService,
        CanDeactivateGuard,
        ImportService,
        FaqService,
        SurchargeService,
        BsDropdownConfig,
        DonneesEssentiellesService,
        ContratChorusValidator,
        ContratNotificationValidator,
        DateContratNotificationValidator,
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DateTimePickerControlValueAccessor),
            multi: true
        },
        {provide: LOCALE_ID, useValue: "fr-FR"},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptor,
            multi: true
        }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})

export class AppModule {
    private browserDocument;

    constructor(@Inject(DOCUMENT) private document: any, private injector: Injector, private router: Router, private location: Location, private bsLocaleService: BsLocaleService, private library: FaIconLibrary) {
        frLocale.invalidDate = 'Date invalide';
        defineLocale('custom locale', frLocale);
        this.bsLocaleService.use('custom locale');
        // nettoyage du store
        localStorage.removeItem(FILTRE_CONTRAT);

        setTheme('bs4');
        this.browserDocument = document;
        // Exec APP Web Component
        this.registerWebComponent('ltexec-application', AppComponent, injector);

        // Liste contrats Web Component
        this.registerWebComponent('exec-liste-contrats', WcListeContratsComponent, injector);

        // Fiche contrat Web Component
        this.registerWebComponent('exec-fiche-contrat', WcFicheContratComponent, injector);
        // Saisir contrat Web Component
        this.registerWebComponent('exec-saisir-contrat', WcSaisirContratComponent, injector);
        // Liste fournisseurs Web Component
        this.registerWebComponent('exec-liste-fournisseurs', WcListeFournisseursComponent, injector);
        // Fiche founisseur Web Component
        this.registerWebComponent('exec-fiche-fournisseur', WcFicheFournisseurComponent, injector);
        // Tableau contrats Web Component
        this.registerWebComponent('exec-tableau-contrats', WcTableauContratsComponent, injector);
        // Supervision données essentielles Web Component
        this.registerWebComponent('exec-supervision-donnees-essentielles', WcSupervisionDonneesEssentiellesComponent, injector);
        library.addIconPacks(fas, far);
    }

    ngDoBootstrap(appRef: ApplicationRef) {
        if (document.getElementsByTagName('ltexec-application').length > 0) {
            console.log(`Bootstrap !`);
            appRef.bootstrap(AppComponent);
        }
    }

    private registerWebComponent(tagName: string, component: Type<any>, injector: Injector) {
        if (!customElements.get(tagName)) {
            console.log(`registring ${tagName}`)
            const webComponent = createCustomElement(component, {injector: injector});
            customElements.define(tagName, webComponent);
        } else {
            console.log(`${tagName} already registred`)
        }
    }
}
