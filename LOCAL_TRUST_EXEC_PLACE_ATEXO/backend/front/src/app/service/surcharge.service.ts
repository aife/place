import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {webServiceEndpoint} from "../constants";

@Injectable()
export class SurchargeService {

    constructor(private httpClient: HttpClient) {
    }

    getHeader() {
        const url = `${webServiceEndpoint}/surcharge/header`;
        return this.httpClient.get(url, {headers: this.buildHeaders(), responseType: 'text'});
    }

    getFooter() {
        const url = `${webServiceEndpoint}/surcharge/footer`;
        return this.httpClient.get(url, {headers: this.buildHeaders(), responseType: 'text'});
    }

    buildHeaders() {
        return new HttpHeaders({"Content-Type": "text/html"});
    }
}
