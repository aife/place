import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {webServiceEndpoint} from "app/constants";

@Injectable({
    providedIn: 'root'
})
export class SpaserService {

    constructor(private http: HttpClient) {
    }

    initContext(contratId: number): Observable<any> {
        const url = `${webServiceEndpoint}/spaser/${contratId}/init-contexte`;
        // @ts-ignore
        return this.http.get<any>(url, {responseType:'text'});
    }


}
