import {defaultItemsCountPerPage} from '../constants';
import {Order, PaginationPropertySort} from '@common/pagination';


interface Orderable {
    page?: number;
    size?: number;
    sort?: string[];
}

export class PaginationRequestParamsBuilder {

    page = 0;
    pageSize: number = defaultItemsCountPerPage;
    order: PaginationPropertySort[];

    constructor() {
        this.order = [{direction: Order.ASC, property: 'id'}];
    }

    setPage(page: number): PaginationRequestParamsBuilder {
        this.page = page;
        return this;
    }

    setPageSize(pageSize: number): PaginationRequestParamsBuilder {
        this.pageSize = pageSize;
        return this;
    }

    setSort(sorts: PaginationPropertySort[]): PaginationRequestParamsBuilder {
        if (sorts) {
            this.order = sorts.filter(sort => {
                return sort.property != null;
            });
        }
        return this;
    }

    build(): Orderable {
        return {
            size: this.pageSize,
            page: this.page,
            sort: this.order ? this.order.map(sort => {
                return sort.property + ',' + sort.direction;
            }) : []
        };
    }
}

