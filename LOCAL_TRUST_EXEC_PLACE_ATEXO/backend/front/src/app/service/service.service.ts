import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {ServiceDTO} from '@dto';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class ServiceService {
    constructor(private http: HttpClient) {
    }
    getServices(): Observable<Array<ServiceDTO>> {
        const url = `${webServiceEndpoint}/service/`;
        return this.http.get<any>(url);

    }
}



