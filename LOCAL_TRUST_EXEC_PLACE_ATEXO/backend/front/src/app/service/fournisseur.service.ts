import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {PaginationPage, PaginationPropertySort} from '@common/pagination';
import {PaginationRequestParamsBuilder} from '@services/common.service';
import {
    ContratEtablissementDTO,
    ContratFournisseurCriteriaDTO,
    EtablissementDTO,
    FournisseurDTO,
    ValueLabelDTO
} from '@dto';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class FournisseurService {
    constructor(private http: HttpClient) {
    }

    findFournisseurs(fournisseurCriteriaDTO: FournisseurCriteriaDTO, page: number, pageSize: number,
                     sort: PaginationPropertySort[]): Observable<PaginationPage<FournisseurDTO>> {
        const url = `${webServiceEndpoint}/fournisseur/`;

        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};

        return this.http.post<any>(url, fournisseurCriteriaDTO, {params: params});
    }

    findFournisseur(id: number): Observable<FournisseurDTO> {
        const url = `${webServiceEndpoint}/fournisseur/${id}`;
        return this.http.get<any>(url);
    }

    creerFournisseur(fournisseur: FournisseurDTO): Observable<FournisseurDTO> {
        const url = `${webServiceEndpoint}/fournisseur/${fournisseur.siren}`;
        return this.http.post<FournisseurDTO>(url, fournisseur);
    }

    updateDonneesPrincipales(fournisseurDTO: FournisseurDTO): Observable<FournisseurDTO> {
        const url = `${webServiceEndpoint}/fournisseur/updateFournisseur`;
        return this.http.put<any>(url, fournisseurDTO);
    }

    addFournisseurEtranger(fournisseurDTO: FournisseurDTO): Observable<FournisseurDTO> {
        const url = `${webServiceEndpoint}/fournisseur/addFournisseurEtranger`;
        return this.http.post<any>(url, fournisseurDTO);
    }

    findContrats(contratFournisseurCriteria: ContratFournisseurCriteriaDTO, page: number, idFournisseur: number,
                 pageSize: number, sort: PaginationPropertySort[]): Observable<PaginationPage<ContratEtablissementDTO>> {
        const url = `${webServiceEndpoint}/fournisseur/contratEtablissement/${idFournisseur}`;
        contratFournisseurCriteria.idFournisseur = idFournisseur;

        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};

        return this.http.post<any>(url, contratFournisseurCriteria, {params: params});
    }
    findByUuid(uuid: string) {
        const url = `${webServiceEndpoint}/fournisseur/uuid/${uuid}`;
        return this.http.get<FournisseurDTO>(url);
    }

    findEtablissementsByUuid(uuid: string) {
        const url = `${webServiceEndpoint}/fournisseur/${uuid}/etablissements`;
        return this.http.get<EtablissementDTO[]>(url);
    }

    formatUrlFicheFournisseur(uuid: string){
        const url = `${webServiceEndpoint}/fournisseur/format-uuid/${uuid}`;
        return this.http.post<ValueLabelDTO>(url, null);
    }
}

export interface FournisseurCriteriaDTO {
    motsCles?: string;
    siren?: string;
}
