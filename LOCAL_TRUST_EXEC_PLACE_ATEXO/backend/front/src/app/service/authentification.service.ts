import {authenticationEndpoint, authorizationEndpoint, webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {USER_CONTEXT} from '@common/userContext';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

export interface Token {
    access_token: string;
    expires_in?: number;
    refresh_token?: string;
    scope?: string;
    token_type?: string;
}

export interface Account {
    login: string;
    password: string;
}


interface AuthenticationUrlParams {
    sso_mode: string;
    sso: string;
    motsCles?: string;
}

export const SSO_TOKEN: string = 'sso';
export const PF_UID: string = 'pfUid';

@Injectable()
export class AuthentificationService {
    constructor(private http: HttpClient) {
    }

    public getCode(): string {
        const url = new Url<AuthenticationUrlParams>(window.location.href);

        console.debug(`URL d'accès = ${url}`);

        // Récupération dans les parametres
        let code = url?.query?.sso;

        if (code) {
            console.debug(`Récupération du code depuis le parametre SSO = ${code} et stockage dans le local storage`);

            return code;
        }
        // Recherche manuelle dans l'URL
        code = this.rechercherParametre(SSO_TOKEN);

        if (code) {
            console.debug(`Récupération du code SSO par recherche dans l'URL = ${code} et stockage dans le local storage`);

            return code;
        }

        return localStorage.getItem(SSO_TOKEN);
    }


    private rechercherParametre(name, url = window.location.href) {
        name = name.replace(/[\[\]]/g, '\\$&');
        let results = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)').exec(url);

        if (!results) {
            return null;
        }
        if (!results[2]) {
            return '';
        }

        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    public authorize(plateforme: string, code: string): Observable<Account> {
        let url = code ? `${authorizationEndpoint}?code=${code}&plateforme=${plateforme}` : authorizationEndpoint;
        return this.http.get<any>(url);
    }

    public login(): Observable<Token> {
        const clientId = 'lt-execution-webapp';
        const clientSecret = 'lt-execution-webapp-s3cr3t';
        const headers = {'authorization': `Basic ${btoa(`${clientId}:${clientSecret}`)}`}

        // ---------------------
        // GRANT TYPE = password
        // ---------------------
        const username = USER_CONTEXT.account.login;
        const password = USER_CONTEXT.account.password;

        const form = new FormData();
        form.append('grant_type', 'password');
        form.append('username', username);
        form.append('password', password);
        form.append('client_id', clientId);

        return this.http.post<any>(authenticationEndpoint, form, {headers: headers});
    }

    public logout(): Observable<string> {
        const url = `${webServiceEndpoint}/logout`;
        localStorage.removeItem(SSO_TOKEN);
        return this.http.post<any>(url, null);
    }

    public accueilMpe(): Observable<string> {
        const url = `${webServiceEndpoint}/accueilMpe`;
        return this.http.get(url, {responseType: "text"});
    }
}


