import {Injectable, isDevMode} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class LieuExecutionService {

    constructor(private httpClient: HttpClient) {
    }

    getLieuxExecution() {
        if (isDevMode()) {
            return this.httpClient.get<any>("assets/json/map_france.json")
        } else {
            return this.httpClient.get<any>("/execution/assets/json/map_france.json")
        }

    }
}
