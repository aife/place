import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {
    ContactDTO,
    ContactPrincipalDTO,
    ContactReferantDTO,
    ContratDTO,
    ContratEtablissementDTO,
    ContratEtablissementGroupementDTO
} from '@dto';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class ContactService {
    constructor(private http: HttpClient) {
    }

    findContactByContratEtablissement(id: any): Observable<Array<ContactDTO>> {
        const url = `${webServiceEndpoint}/contact/findByContratEtablissement/${id}`;
        return this.http.get<any>(url);
    }

    modifierContactReferant(contactReferantDTO: ContactReferantDTO): Observable<ContactReferantDTO> {
        const url = `${webServiceEndpoint}/contact/ModifierContactReferant`;
        return this.http.post<any>(url, contactReferantDTO);
    }

    supprimerContactReferant(contratEtablissementId: any, contactId: any): Observable<boolean> {
        const url = `${webServiceEndpoint}/contact/supprimerContact/${contratEtablissementId}/${contactId}`;
        return this.http.delete<any>(url);
    }

    ajouterContactReferant(contactReferantDTO: ContactReferantDTO): Observable<ContactReferantDTO> {
        const url = `${webServiceEndpoint}/contact/ajouterContactReferant`;
        return this.http.post<any>(url, contactReferantDTO);
    }

    findContactById(ids: Array<any>): Observable<Array<ContactDTO>> {

        const url = `${webServiceEndpoint}/contact/findByIdIn`;
        const params = {id: ids.toString()};
        return this.http.get<any>(url, {params: params});
    }

    ajouterContact(contactDTO: ContactDTO): Observable<ContactDTO> {
        const url = `${webServiceEndpoint}/contact/ajouterContact`;
        return this.http.post<any>(url, contactDTO);
    }

    modifierContact(contactDTO: ContactDTO): Observable<ContactDTO> {
        const url = `${webServiceEndpoint}/contact/modifierContact`;
        return this.http.post<any>(url, contactDTO);
    }

    supprimerContact(contactId: number) {
        const url = `${webServiceEndpoint}/contact/${contactId}/supprimerContact`;
        return this.http.delete<any>(url);
    }

    findContactsByEtablissement(id: any): Observable<ContactDTO[]> {
        const url = `${webServiceEndpoint}/contact/findByEtablissement/${id}`;
        return this.http.get<any>(url);
    }

    modifierContactPrincipal(contactPrincipalDTO: ContactPrincipalDTO): Observable<ContactPrincipalDTO> {
        const url: string = `${webServiceEndpoint}/contact/principal`;
        return this.http.patch<ContactPrincipalDTO>(url, contactPrincipalDTO);
    }

    ajouterContactPrincipal(contactPrincipalDTO: ContactPrincipalDTO): Observable<ContactPrincipalDTO> {
        const url: string = `${webServiceEndpoint}/contact/principal`;
        return this.http.post<ContactPrincipalDTO>(url, contactPrincipalDTO);
    }

    recupererContactPrincipal(contratEtablissementDTO: ContratEtablissementDTO): Observable<ContactPrincipalDTO | undefined> {
        const url: string = `${webServiceEndpoint}/contact/principal?attributaire=${contratEtablissementDTO.id}`;
        return this.http.get<any>(url);
    }
}

