import {Injectable, Renderer2} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
    EmailDTO,
    PaginationPage,
    PaginationPropertySort,
    PieceJointeDTO,
    SerializableRechercheMessage
} from '@dto/messagerie-dto';
import {DATE_TIME_ISO_FORMAT, MS_WEBSERVICE_ENDPOINT} from '@dto/messagerie-constants';
import * as moment from 'moment';
import {messagerieWebServiceEndpoint, webServiceEndpoint} from "app/constants";
import {ContratMessageStatusDTO, MessagerieContexte} from "@dto/backend";

@Injectable({
    providedIn: 'root'
})
export class MessagerieService {


    private renderer: Renderer2;

    constructor(private http: HttpClient) {

    }

    findEmails(token: string, emailCriteriaDTO: SerializableRechercheMessage, page: number, pageSize: number,
               sort: PaginationPropertySort[]): Observable<PaginationPage<EmailDTO>> {
        const httpParams = {size: '' + pageSize, page: '' + page, sort: []};
        if (sort) {
            httpParams.sort = sort.map(s => `${s.property},${s.direction}`);
        }
        const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/messages/?token=${token}`;
        console.log(`Appel de l'url ${url}, avec les paramètres: size : ${pageSize}, page: ${page}, sort : ${sort}`);

        const requestParams: SerializableRechercheMessage = emailCriteriaDTO;

        // TODO revoir la sérialisation des dates ...
        if (emailCriteriaDTO.dateEnvoiDebut != null) {
            requestParams.dateEnvoiDebut = moment(emailCriteriaDTO.dateEnvoiDebut).format(DATE_TIME_ISO_FORMAT);
        }
        if (emailCriteriaDTO.dateEnvoiFin != null) {
            requestParams.dateEnvoiFin = moment(emailCriteriaDTO.dateEnvoiFin).format(DATE_TIME_ISO_FORMAT);
        }
        requestParams.inclureReponses = false;
        // fixme: workaround RSEM & EXEC
        requestParams.exclureReponses = true;
        return this.http.post<PaginationPage<EmailDTO>>(url, requestParams, {params: httpParams});
    }

    getPieceJointeDownloadURL(email: EmailDTO, pieceJointe: PieceJointeDTO): string {
        if (pieceJointe.id != null) {
            return `${MS_WEBSERVICE_ENDPOINT}/suivi/downloadPieceJointe?codeLien=${email.codeLien}&pieceJointeId=${pieceJointe.id}`;
        } else if (pieceJointe.reference != null) {
            return `${MS_WEBSERVICE_ENDPOINT}/upload/download/${pieceJointe.reference}`;
        }
        return null;
    }


    initTokenRedaction(contratId: number): Observable<MessagerieContexte> {
        const url = `${webServiceEndpoint}/messagerie/initTokenRedaction?contratId=${contratId}`;
        return this.http.get<any>(url);
    }

    initTokenRedactionByCodeLien(contratId: number, codeLien: string): Observable<any> {
        const url = `${webServiceEndpoint}/messagerie/initTokenByCodeLien?contratId=${contratId}&codeLien=${codeLien}`;
        return this.http.get<any>(url);
    }

    initTokenNotification(contratId: any, acteId: any) {
        let url: string = `${webServiceEndpoint}/messagerie/initTokenNotification?contratId=${contratId}`;
        if (acteId) {
           url = url.concat(`&acteId=${acteId}`);
        }
        return this.http.get<any>(url);
    }

    initTokenSuivi(contratId: number): Observable<MessagerieContexte> {
        const url = `${webServiceEndpoint}/messagerie/initTokenSuivi?contratId=${contratId}`;
        return this.http.get<any>(url);
    }

    initTokenSuiviNotifications(contratId: number, acteId: number): Observable<MessagerieContexte> {
        const url = `${webServiceEndpoint}/messagerie/initTokenSuivi?contratId=${contratId}&acteId=${acteId}`;
        return this.http.get<any>(url);
    }

    countMessageStatusContrats(idContrats: Array<any>): Observable<Array<ContratMessageStatusDTO>> {
        const url = `${webServiceEndpoint}/messagerie/countMessageStatusContrats`;
        return this.http.post<any>(url, idContrats);
    }

    countMessageStatus(token: string, idObjetMetier: string) {
        const url = `${messagerieWebServiceEndpoint}/suivi/count`;
        const params = {
            token: token,
            idObjetMetier: idObjetMetier,
        };
        return this.http.get<any>(url, {params: params});
    }

}
