import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {webServiceEndpoint} from "../constants";

@Injectable()
export class WorkerService {

    constructor(public readonly http: HttpClient) {
    }

    findTask(idTask: string): Observable<any> {
        const url = `${webServiceEndpoint}/worker/${idTask}`;
        return this.http.get<any>(url);
    }

    download(idTask: string) {
        const url = `${webServiceEndpoint}/worker/${idTask}/download`;
        return this.http.get<any>(url, {
            params: null,
            // @ts-ignore
            responseType: 'blob'
        })
    }

}
