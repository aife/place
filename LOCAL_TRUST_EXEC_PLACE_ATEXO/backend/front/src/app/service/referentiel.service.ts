import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {from} from 'rxjs/observable/from';
import {merge} from 'rxjs/observable/merge';
import {OrganismeDTO, ReferentielParentDto, ServiceDTO, ValueLabelDTO} from '@dto';
import {mergeAll} from 'rxjs/operators/mergeAll';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../store';
import {getOrganismes, getReferentiel, getServices} from '../store/referentiel/referentiel.action';
import {TypeReferentiel} from '@dto/backendEnum';

@Injectable()
export class ReferentielService {

    // statiques
    typeGroupementList: Array<ValueLabelDTO> = [];
    clauseN2N3List: Array<ReferentielParentDto> = [];

    constructor(private http: HttpClient, private store: Store<State>,) {
    }

    chargerTousReferentiels(): Observable<any> {
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CATEGORIE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.PAYS}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CCAG}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_CONTRAT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.FORME_JURIDIQUE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CPV}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_AVENANT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_ACTE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CODE_EXTERNE_ACTE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TAUX_TVA}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.PROCEDURE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.MODALITE_EXECUTION}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TECHNIQUE_ACHAT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_GROUPEMENT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.FORME_PRIX}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_PRIX}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.REVISION_PRIX}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.SURCHARGE_LIBELLE}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_CONTRAT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.TYPE_EVENEMENT}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CLAUSE_N1}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CLAUSE_N2}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CLAUSE_N3}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CLAUSE_N4}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CLAUSE_N2N3}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.CATEGORIE_FOURNISSEUR}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.LIEU_EXECUTION}));
        this.store.dispatch(getReferentiel({refType: TypeReferentiel.NATURE_CONTRAT_CONCESSION}));
        this.store.dispatch(getOrganismes());
        this.store.dispatch(getServices());
        const staticObservable = this.loadStatic();
        return merge(staticObservable);
    }


    loadStatic(): Observable<Array<ValueLabelDTO>> {
        const typeGroupementListAsync = this.getTypeGroupementListAsync();
        typeGroupementListAsync.subscribe(typeGroupementList => this.typeGroupementList = typeGroupementList, _ => {
        });
        return from([typeGroupementListAsync]).pipe(mergeAll());
    }


    public getReferentielAsync(type: string): Observable<Array<ValueLabelDTO>> {
        const url = `${webServiceEndpoint}/referentiel/${type}`;
        return this.http.get<any>(url);
    }

    public getOrganismesAsync(): Observable<Array<OrganismeDTO>> {
        const url = `${webServiceEndpoint}/referentiel/organismes`;
        return this.http.get<any>(url);
    }

    public getServicesAsync(): Observable<Array<ServiceDTO>> {
        const url = `${webServiceEndpoint}/referentiel/services`;
        return this.http.get<any>(url);
    }

    public getTypeGroupementListAsync(): Observable<Array<ValueLabelDTO>> {
        const url = `${webServiceEndpoint}/referentiel/typeGroupement`;
        return this.http.get<any>(url);
    }
}

