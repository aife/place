import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Injectable} from '@angular/core';
import {DonneesEssentiellesDTO, ValueLabelDTO} from '@dto';
import {HttpClient} from '@angular/common/http';
import {PaginationRequestParamsBuilder} from "@services/common.service";
import {PerimetreContrat, StatutContrat} from "@dto/backendEnum";

@Injectable()
export class DonneesEssentiellesService {

    constructor(private http: HttpClient) {
    }

    findDonneesEssentielles(donneesEssentiellesCriteriaDTO: DonneesEssentiellesCriteriaDTO, page: number,
                            pageSize: number, sort: PaginationPropertySort[]): Observable<PaginationPage<DonneesEssentiellesDTO>> {

        const url = `${webServiceEndpoint}/donnees-essentielles/`;

        if (sort == null) {
            sort = [{direction: Order.DESC, property: "id"}]
        }

        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};

        return this.http.post<any>(url, donneesEssentiellesCriteriaDTO, {params: params});
    }

    updateStatut(idsDonneesEssentielles: number[]) {
        const url = `${webServiceEndpoint}/donnees-essentielles/update-statut?idsDonneesEssentielles=${idsDonneesEssentielles}`;
        return this.http.get<string>(url);
    }

    telechargerFihcier(idDonneesEssentielles: number): Observable<any> {
        const url = `${webServiceEndpoint}/donnees-essentielles/telecharger/${idDonneesEssentielles}`;
        return this.http.get(url, {
            responseType: 'blob'
        });
    }
}

export interface DonneesEssentiellesCriteriaDTO {
    idDonneeEssentielle?: number;
    idContrat?: number;
    typesContrat?: ValueLabelDTO[];
    dateNotification?: Date[];
    dateEnvoi?: Date[];
    statuts?: StatutContrat[];
    perimetre?: PerimetreContrat | string;
    statutEnCours?: boolean;
    statutAPublier?: boolean;
    statutPublie?: boolean;
    statutError?: boolean;
    statutNonPublie?: boolean;
}

