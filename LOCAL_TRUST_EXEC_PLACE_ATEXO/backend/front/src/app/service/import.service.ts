import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {PairDTO} from "@dto/backend";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class ImportService {

    constructor(private http: HttpClient) {
    }
    testImportContrat(reference: string): Observable<PairDTO> {
        const url = `${webServiceEndpoint}/import/testContrat/${reference}`;
        return this.http.get<any>(url);
    }

    importFichierContrat(reference: string): Observable<PairDTO> {
        const url = `${webServiceEndpoint}/import/importContrat/${reference}`;
        return this.http.get<any>(url);
    }

    testImportContractant(reference: string): Observable<PairDTO> {
        const url = `${webServiceEndpoint}/import/testContractant/${reference}`;
        return this.http.get<any>(url);
    }

    importFichierContractant(reference: string): Observable<PairDTO> {
        const url = `${webServiceEndpoint}/import/importContractant/${reference}`;
        return this.http.get<any>(url);
    }
}
