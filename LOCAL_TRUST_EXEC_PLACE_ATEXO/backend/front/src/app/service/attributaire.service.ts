import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {
    AttributaireDTO,
    ContratEtablissementDTO,
    ContratEtablissementGroupementDTO,
    EditionAttributaireDTO
} from '@dto';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class AttributaireService {
    constructor(private http: HttpClient) {
    }

    getArborescenceAttributaires(contratId: number): Observable<AttributaireDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/arborescence`;
        return this.http.get<any>(url)
    }

    ajouterSousTraitant(contratId: number, editionContratEtablissementDTO: EditionAttributaireDTO): Observable<ContratEtablissementDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/ajouterSousTraitant`;
        return this.http.post<any>(url, editionContratEtablissementDTO);
    }

    supprimerSousTraitant(contratId: number, editionContratEtablissementDTO: EditionAttributaireDTO): Observable<any> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/supprimerSousTraitant`;
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}), body: editionContratEtablissementDTO
        };
        return this.http.delete(url, httpOptions);
    }

    ajouterCoTraitant(contratId: number, editionContratEtablissementDTO: EditionAttributaireDTO): Observable<ContratEtablissementDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/ajouterCoTraitant`;
        return this.http.post<any>(url, editionContratEtablissementDTO);
    }

    degrouperGroupement(contratId: number): Observable<AttributaireDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/degrouperGroupement`;
        return this.http.post<any>(url, null);
    }

    transformerEnGroupement(contratId: number, groupement: ContratEtablissementGroupementDTO): Observable<AttributaireDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/transformerEnGroupement`;
        return this.http.post<any>(url, groupement);
    }

    supprimerCoTraitant(contratId: number, editionContratEtablissementDTO: EditionAttributaireDTO) {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/supprimerCoTraitant`;
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}), body: editionContratEtablissementDTO
        };
        return this.http.delete<any>(url, httpOptions);
    }

    modifierAttributaire(contratId: number, editionAttributaireDTO: EditionAttributaireDTO): Observable<ContratEtablissementDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/modifierAttributaire`;
        return this.http.put<any>(url, editionAttributaireDTO);
    }

    modifierAttributaires(contratId: number,
                          editionAttributaireDTOs: Array<EditionAttributaireDTO>): Observable<Array<ContratEtablissementDTO>> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/modifierAttributaires`;
        return this.http.put<any>(url, editionAttributaireDTOs);
    }

    modifierGroupement(contratId: number, groupement: ContratEtablissementGroupementDTO): Observable<ContratEtablissementGroupementDTO> {
        const url = `${webServiceEndpoint}/attributaire/${contratId}/modifierGroupement`;
        return this.http.put<any>(url, groupement);
    }
}


