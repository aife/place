import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {EtablissementDTO} from '@dto';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class EtablissementService {

    constructor(private http: HttpClient) {
    }

    findEtablissementsByRaisonSociale(raisonSociale: string): Observable<Array<EtablissementDTO>> {
        const url = `${webServiceEndpoint}/etablissement/findByRaisonSociale/${raisonSociale}`;
        return this.http.get<any>(url);
    }

    findEtablissementById(id: number): Observable<EtablissementDTO> {
        const url = `${webServiceEndpoint}/etablissement/findById/${id}`;
        return this.http.get<any>(url);

    }

    findEtablissementBySiret(siret: string): Observable<EtablissementDTO> {
        const url = `${webServiceEndpoint}/etablissement/findBySiret/${siret}`;
        return this.http.get<any>(url);
    }

    ajouterEtablissement(etablissementDTO: EtablissementDTO): Observable<EtablissementDTO> {
        const url = `${webServiceEndpoint}/etablissement/ajouterEtablissement`;
        return this.http.post<any>(url, etablissementDTO);
    }

    modifierEtablissement(etablissement: EtablissementDTO): Observable<EtablissementDTO> {
        const url = `${webServiceEndpoint}/etablissement/modifierEtablissement`;
        return this.http.post<any>(url, etablissement);
    }

    ajouterEtablissementEtranger(etablissementDTO: EtablissementDTO): Observable<EtablissementDTO> {
        const url = `${webServiceEndpoint}/etablissement/ajouterEtablissementEtranger`;
        return this.http.post<any>(url, etablissementDTO);
    }

    supprimerEtablissement(etablissement: EtablissementDTO) {
        const url = `${webServiceEndpoint}/etablissement/${etablissement.id}`;
        return this.http.delete<any>(url);


    }
}

