import {Injectable} from '@angular/core';
import {webServiceEndpoint} from 'app/constants';
import {HttpClient} from '@angular/common/http';
import {ActeDTO, ContratDTO, EchangeChorusDTO} from '@dto';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Observable} from 'rxjs/Observable';
import {PaginationPage} from "@common/pagination";

@Injectable({
    providedIn: 'root'
})
export class EchangeChorusService {
    modalRef: BsModalRef;

    constructor(
        private http: HttpClient,
        private modalService: BsModalService
    ) {
    }

    public rechercher(criteria: EchangeChorusCriteriaDTO): Observable<PaginationPage<EchangeChorusDTO>> {
        const url = `${webServiceEndpoint}/echanges-chorus?number=0&size=10000&sort=id,ASC`;

        return this.http.post<PaginationPage<EchangeChorusDTO>>(url, criteria);

    }

    downloadChorusZip(echangeChorus: EchangeChorusDTO) {
        const url = `${webServiceEndpoint}/echanges-chorus/zip/chorus/${echangeChorus.id}`;
        return this.http.get(url, {responseType: 'blob'});
    }

    downloadContratZip(contratId: ContratDTO): Observable<Blob> {
        const url = `${webServiceEndpoint}/echanges-chorus/zip/contrat/${contratId}`;
        return this.http.get(url, {responseType: 'blob'});
    }

    openModal(component, initialState?: any): BsModalRef {
        this.modalRef = this.modalService.show(component, {initialState, class: 'msv2'});

        return this.modalRef;
    }

    soumettre(acte: ActeDTO, echangeChorus: EchangeChorusDTO): Observable<EchangeChorusDTO> {
        const url: string = `${webServiceEndpoint}/echanges-chorus/soumettre/${acte.id}`;

        return this.http.post<EchangeChorusDTO>(url, echangeChorus, {responseType: 'json'});
    }

    ressoumettre(acte: ActeDTO): Observable<EchangeChorusDTO> {
        const url: string = `${webServiceEndpoint}/echanges-chorus/ressoumettre/${acte.id}`;

        return this.http.post<EchangeChorusDTO>(url, null, {responseType: 'json'});
    }
}

export interface EchangeChorusCriteriaDTO {
    acte?: number;
    contrat?: number;
}
