import {defaultItemsCountPerPage, webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {PaginationRequestParamsBuilder} from '@services/common.service';
import {PaginationPage, PaginationPropertySort} from '@common/pagination';
import {
    CreationDocumentGenereModel,
    DocumentContratDTO,
    DocumentCriteriaDTO,
    DocumentModeleDTO,
    ValueLabelDTO
} from '@dto';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class DocumentService {
    constructor(private http: HttpClient) {
    }

    findModeleDocument() {
        const url = `${webServiceEndpoint}/referentiel/modeleDocument`;
        return this.http.get<Array<DocumentModeleDTO>>(url)

    }

    findByEvenement(evenementId): Observable<Array<DocumentContratDTO>> {
        const url = `${webServiceEndpoint}/document/evenement/${evenementId}`;
        return this.http.get<Array<DocumentContratDTO>>(url)

    }

    find(contratId, documentCriteriaDTO: DocumentCriteriaDTO, page: number, pageSize: number,
         sort: PaginationPropertySort[]): Observable<PaginationPage<DocumentContratDTO>> {

        const url = `${webServiceEndpoint}/document/`;

        let params: any = {};

        if (page != null || pageSize != null) {
            const orderable = new PaginationRequestParamsBuilder().setPage(page)
                .setPageSize(pageSize == null ? defaultItemsCountPerPage : pageSize)
            if (sort != null) {
                orderable.setSort(sort).build();
            }
            params = {...orderable.build()};
        }

        documentCriteriaDTO.contratId = contratId;
        return this.http.post<PaginationPage<DocumentContratDTO>>(url, documentCriteriaDTO, {params: params});
    }

    saveDocument(contratId, model: CreationDocumentGenereModel): Observable<DocumentContratDTO> {
        const url = `${webServiceEndpoint}/document/save/${contratId}`;
        return this.http.post<DocumentContratDTO>(url, model);
    }

    generateDocument(contratId, model: CreationDocumentGenereModel): Observable<Array<DocumentContratDTO>> {
        const url = `${webServiceEndpoint}/document/generate/${contratId}`;
        return this.http.post<Array<DocumentContratDTO>>(url, model);

    }

    saveCommentaire(documentId, commentaire): Observable<any> {
        const url = `${webServiceEndpoint}/document/${documentId}/commentaire`;
        return this.http.post<any>(url, commentaire)
    }

    deleteMessage(id: any): Observable<any> {
        const url = `${webServiceEndpoint}/document/${id}`;
        return this.http.delete(url)

    }

    getDownloadURL(documentId): string {
        return `${webServiceEndpoint}/document/${documentId}/download`;
    }

    getUrlEditionOnlyOffice(documentId): Observable<ValueLabelDTO> {
        const url = `${webServiceEndpoint}/document/${documentId}/edition-en-ligne`;
        return this.http.get(url);
    }

    convert(file: any, extension: string) {
        const url = `${webServiceEndpoint}/document/convert/${extension}`;
        const formData = new FormData();
        formData.append("pdf", new Blob([file], {
            type: "application/pdf"
        }))
        return this.http.post(url, formData, {responseType: 'blob'});
    }

    export(contratId: any) {
        const url = `${webServiceEndpoint}/document/export/${contratId}`;
        return this.http.get(url, {responseType: 'blob'});
    }
}

