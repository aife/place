import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {Injectable} from '@angular/core';
import {ActeDTO, ContratDTO, RejetActeDTO} from '@dto';
import {StatutActe} from '@dto/backendEnum';
import {TypeFormulaireActe} from '@views/actes/forms/type-formulaire-acte';
import {of} from 'rxjs';
import {PieceJointeDTO} from '@dto/messagerie-dto';
import {HttpClient} from '@angular/common/http';
import {
    ActeModificatifComponent,
    AgrementSousTraitantComponent,
    DecisionAffermissementTrancheComponent,
    DecisionReconductionComponent,
    OrdreServiceComponent,
    ResiliationComponent
} from '@views/actes/types';
import {ActeAutreComponent} from '@views/actes/types/acte-autre/acte-autre.component';
import {
    DecisionProlongationDelaiComponent
} from '@views/actes/types/decision-prolongation-delai/decision-prolongation-delai.component';
import {DonneesExecutionComponent} from '@views/actes/types/donnees-execution/donnees-execution.component';
import {PaginationRequestParamsBuilder} from '@services/common.service';

export interface ActeCriteriaDTO {
    id?: number;
    numero?: string;
    objet?: string;
    contrat?: ContratDTO;
    statuts?: StatutActe[];
    motsCles?: string;
}

@Injectable()
export class ActeService {

    constructor(private http: HttpClient) {
    }

    rechercher(criteria: ActeCriteriaDTO, page: number, pageSize: number, sort: PaginationPropertySort[]): Observable<PaginationPage<ActeDTO>> {
        if (!sort) {
            sort = [{direction: Order.ASC, property: 'numero'}];
        }

        const url = `${webServiceEndpoint}/actes`;

        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};
        return this.http.post<PaginationPage<ActeDTO>>(url, criteria, {params: params});
    }

    recupererPourContrat(contrat: ContratDTO): Observable<Array<ActeDTO>> {
        const url = `${webServiceEndpoint}/actes?contrat=${contrat.id}`;
        return this.http.get <Array<ActeDTO>>(url);
    }

    recuperer(id: number): Observable<ActeDTO> {
        const url = `${webServiceEndpoint}/actes/${id}`;
        return this.http.get <ActeDTO>(url);
    }

    recupererFormulairesActe(): Observable<Array<TypeFormulaireActe>> {
        return of([
            new TypeFormulaireActe(
                'ACT_AST',
                'Agrément du sous-traitant',
                'com.atexo.execution.common.dto.actes.AgrementSousTraitantDTO',
                AgrementSousTraitantComponent
            ),
            new TypeFormulaireActe(
                'ACT_MOD',
                'Acte modificatif',
                'com.atexo.execution.common.dto.actes.ActeModificatifDTO',
                ActeModificatifComponent
            ),
            new TypeFormulaireActe(
                'ACT_DAT',
                'Décision d\'affermissement de tranche',
                'com.atexo.execution.common.dto.actes.DecisionAffermissementTrancheDTO',
                DecisionAffermissementTrancheComponent
            ),
            new TypeFormulaireActe(
                'ACT_DRN',
                'Décision de reconduction ou non reconduction',
                'com.atexo.execution.common.dto.actes.DecisionReconductionDTO',
                DecisionReconductionComponent
            ),
            new TypeFormulaireActe(
                'ACT_RES',
                'Résiliation',
                'com.atexo.execution.common.dto.actes.ResiliationDTO',
                ResiliationComponent
            ),
            new TypeFormulaireActe(
                'ACT_DPD',
                'Décision de prolongation de délai',
                'com.atexo.execution.common.dto.actes.DecisionProlongationDelaiDTO',
                DecisionProlongationDelaiComponent
            ),
            new TypeFormulaireActe(
                'ACT_ODS',
                'Ordre de service',
                'com.atexo.execution.common.dto.actes.OrdreServiceDTO',
                OrdreServiceComponent
            ),
            new TypeFormulaireActe(
                'ACT_AUT',
                'Autre',
                'com.atexo.execution.common.dto.actes.ActeAutreDTO',
                ActeAutreComponent
            ),
            new TypeFormulaireActe(
                'ACT_DE',
                "Données d'exécution",
                'com.atexo.execution.common.dto.actes.DonneesExecutionDTO',
                DonneesExecutionComponent
            )
        ]);
    }

    supprimer(acte: ActeDTO) {
        const url = `${webServiceEndpoint}/actes/${acte.id}`;
        return this.http.delete(url);
    }

    creer(acte: ActeDTO): Observable<ActeDTO> {
        const url = `${webServiceEndpoint}/actes`;
        return this.http.put<ActeDTO>(url, acte);
    }

    modifier(acte: ActeDTO): Observable<ActeDTO> {
        const url = `${webServiceEndpoint}/actes`;
        return this.http.patch<ActeDTO>(url, acte);
    }

    delete(pieceJointe: PieceJointeDTO): Observable<any> {
        const url = `${webServiceEndpoint}/upload/delete/${pieceJointe.reference}`;
        return this.http.delete(url);
    }

    draft(reference: string | object): Observable<any> {
        const url = `${webServiceEndpoint}/upload/download/${reference}`;
        return this.http.get(url, {
            responseType: 'blob'
        });
    }

    download(id: string | number): Observable<any> {
        const url = `${webServiceEndpoint}/actes/${id}/download`;
        return this.http.get(url, {
            responseType: 'blob'
        });
    }

    annuler(acte: ActeDTO, rejet: RejetActeDTO): Observable<ActeDTO> {
        acte.statut = 'ANNULE';
        acte.rejet = rejet;

        return this.modifier(acte);
    }
}

