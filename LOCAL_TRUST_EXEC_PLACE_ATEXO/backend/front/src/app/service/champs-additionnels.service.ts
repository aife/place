import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ChampDTO} from '@dto';

@Injectable({
    providedIn: 'root'
})
export class ChampsAdditionnelsService {

    _jsonPath = "assets/json/champs_additionnels.json"

    constructor(private http: HttpClient) {
    }

    getChampsAdditionnels(): Observable<ChampDTO[]> {
        return this.http.get<ChampDTO[]>(this._jsonPath);
    }
}
