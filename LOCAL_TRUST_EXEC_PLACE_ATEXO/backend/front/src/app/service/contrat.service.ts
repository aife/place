import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {PaginationRequestParamsBuilder} from '@services/common.service';
import {Injectable} from '@angular/core';
import {PerimetreContrat, StatutContrat} from '@dto/backendEnum';
import {ContratDTO, ContratEtablissementDTO, DocumentContratDTO, PairDTO, ValueLabelDTO} from '@dto';
import {ConfirmationModalComponent} from '@components/modals/ConfirmationModal';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {HttpClient} from '@angular/common/http';
import {ModalService} from '@services/modal.service';
import * as moment from 'moment';
import {USER_CONTEXT} from '@common/userContext';

@Injectable()
export class ContratService {

    modalRef: BsModalRef;
    private readonly CONCESSION = 'Concession';

    constructor(private modalService: ModalService, private http: HttpClient) {
    }

    findContrats(contratCriteriaDTO: ContratCriteriaDTO, page: number,
                 pageSize: number, sort: PaginationPropertySort[]): Observable<PaginationPage<ContratDTO>> {

        if (USER_CONTEXT?.utilisateur?.perimetreVisionServices?.length > 0) {
            contratCriteriaDTO.idServices = USER_CONTEXT?.utilisateur?.perimetreVisionServices;
        }
        const url = `${webServiceEndpoint}/contrat/`;

        if (sort == null) {
            sort = [{direction: Order.DESC, property: "dateCreation"}];
        }

        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};

        return this.http.post<any>(url, contratCriteriaDTO, {params: params});
    }

    findContratById(id: number): Observable<ContratDTO> {
        const url = `${webServiceEndpoint}/contrat/${id}`;
        return this.http.get<any>(url);
    }

    findEnfantsByContratId(id: number) {
        const url = `${webServiceEndpoint}/contrat/${id}/enfants`;
        return this.http.get<ContratDTO[]>(url);
    }

    findEtablissementsById(id: number): Observable<ContratEtablissementDTO[]> {
        const url = `${webServiceEndpoint}/contrat/${id}/etablissements`;
        return this.http.get<ContratEtablissementDTO[]>(url);
    }

    findDocumentsById(id: number): Observable<DocumentContratDTO[]> {
        const url = `${webServiceEndpoint}/contrat/${id}/documents`;
        return this.http.get<any>(url);
    }

    findContratByIdExterne(id: string): Observable<ContratDTO> {
        const url = `${webServiceEndpoint}/contrat/filtrer/${id}`;
        return this.http.get<any>(url);
    }

    findContratByEvenement(id: number): Observable<ContratEtablissementDTO[]> {
        const url = `${webServiceEndpoint}/contrat/evenement/${id}`;
        return this.http.get<any>(url);
    }

    toggleFavoris(contratId: number): Observable<any> {
        const url = `${webServiceEndpoint}/contrat/toggleFavori/${contratId}`;
        return this.http.post<any>(url, null);
    }

    updateDonneesPrincipales(contratDTO: ContratDTO): Observable<ContratDTO> {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        const url = `${webServiceEndpoint}/contrat/updateDonneesPrincipales`;
        return this.http.post<any>(url, contratDTO);
    }

    ajouterContrat(contratDTO: ContratDTO): Observable<ContratDTO> {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        const url = `${webServiceEndpoint}/contrat/ajout`;
        return this.http.post<any>(url, contratDTO);
    }

    creerMarcheSubsequent(contratDTO: ContratDTO) {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        if (contratDTO.dateMaxFinContrat != null && moment(new Date()).isAfter(contratDTO.dateMaxFinContrat)) {
            this.modalRef = this.modalService.openModal(ConfirmationModalComponent, {class: 'msv2'});
            this.modalRef.content.title = 'Demande de confirmation';
            this.modalRef.content.message = 'Attention : Le contrat a une date définitive de fin de marché dépassée. Veuillez vous référer au contrat concerné avant de confirmer le lancement de cette consultation.';
            this.modalRef.content.yes.subscribe(_ => this.processCreationMarcheSubsequent(contratDTO));
        } else {
            this.processCreationMarcheSubsequent(contratDTO);
        }

    }

    processCreationMarcheSubsequent(contratDTO: ContratDTO) {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        const url = `${webServiceEndpoint}/contrat/marche-subsequent/format-url/${contratDTO.id}`;

        const obs = this.http.post<ValueLabelDTO>(url, null);
        obs.subscribe(dto => {
            window.open(window.location.origin + dto.value, '_self');
        }, err => console.log(err));

    }

    echangerChorus(contratDTO: ContratDTO) {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        const url = `${webServiceEndpoint}/contrat/echange-chorus/format-url/${contratDTO.id}`;

        const obs = this.http.post<ValueLabelDTO>(url, null);
        obs.subscribe(dto => {
            window.open(window.location.origin + dto.value, '_self');
        }, err => console.log(err));

    }

    notifierContrat(contratDTO: ContratDTO) {
        if (!contratDTO.attributionAvance) {
            contratDTO.tauxAvance = undefined;
        }

        const url = `${webServiceEndpoint}/contrat/notification-contrat/${contratDTO.id}`;
        return this.http.post<any>(url, contratDTO, {responseType: 'blob' as 'json'});
    }

    peutCreerOrganismesOuMarcheSubsequent(contrat: ContratDTO) {
        return contrat && contrat.statut && contrat.statut !== StatutContrat[StatutContrat.ANotifier] && contrat.type && ['sad', 'amo', 'amu'].indexOf(contrat.type.value) != -1 && (!contrat.exNihilo || true);
    }


    exportContrats(contratCriteriaDTO: ContratCriteriaDTO): Observable<string> {

        const url = `${webServiceEndpoint}/contrat/export`;
        if (USER_CONTEXT?.utilisateur?.perimetreVisionServices?.length > 0) {
            contratCriteriaDTO.idServices = USER_CONTEXT?.utilisateur?.perimetreVisionServices;
        }

        return this.http.post<any>(url, contratCriteriaDTO, {responseType: 'text' as 'json'});
    }

    deleteContrat(contratId: number): Observable<any> {
        const url = `${webServiceEndpoint}/contrat/${contratId}`;
        return this.http.delete<any>(url);
    }

    getContratsLies(id: number) {
        const url = `${webServiceEndpoint}/contrat/${id}/contrats-lies`;
        return this.http.get<ContratDTO[]>(url);
    }

    isConcession(contrat: ContratDTO) {
        return contrat?.type?.parents?.find(e => e.value == this.CONCESSION) != null;
    }
}

export interface ContratCriteriaDTO {
    motsCles?: string;
    favoris?: boolean;
    transverses?: boolean;
    perimetre?: PerimetreContrat | string;
    statutANotifier?: boolean;
    statutNotifie?: boolean;
    statutEnCours?: boolean;
    statutClos?: boolean;
    statutArchive?: boolean;
    statuts?: StatutContrat[];
    typesContrat?: ValueLabelDTO[];
    categories?: ValueLabelDTO[];
    chapeau?: boolean;
    considerationsEnvironnementales?: boolean;
    considerationsSociales?: boolean;
    dateNotification?: Date[];
    datePrevisionnelleNotification?: Date[];
    dateFinContrat?: Date[];
    dateMaxFinContrat?: Date[];
    idServices?: number[];
}
