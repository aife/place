import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class ParametrageService {
    constructor(private http: HttpClient) {
    }
    getClientParametrage(): Observable<any> {
        const url = `${webServiceEndpoint}/parametrage/findAllExportFront`;
        return this.http.get<any>(url);

    }

}
