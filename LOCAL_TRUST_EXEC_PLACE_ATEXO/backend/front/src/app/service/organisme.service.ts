import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {webServiceEndpoint} from 'app/constants';

@Injectable({
    providedIn: 'root'
})
export class OrganismeService {

    constructor(private http: HttpClient) {
    }

    getOrganismes() {
        const url = `${webServiceEndpoint}/organismes/`;
        return this.http.get<any>(url);
    }
}
