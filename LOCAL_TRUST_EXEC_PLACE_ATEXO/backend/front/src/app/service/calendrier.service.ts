import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {Order, PaginationPage, PaginationPropertySort} from '@common/pagination';
import {EvenementDTO} from '@dto';
import {EtatDateEvenement} from '@dto/backendEnum';
import {map} from 'rxjs/operators/map';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {PaginationRequestParamsBuilder} from "@services/common.service";
import * as moment from 'moment';

@Injectable()
export class CalendrierService {
    constructor(private http: HttpClient) {
    }

    findCalendrierByContrat(contratId): Observable<Array<EvenementDTO>> {
        const url = `${webServiceEndpoint}/calendrier/${contratId}`;
        return this.http.get<any>(url);

    }

    saveEvenement(evenement: EvenementDTO, contratId): Observable<EvenementDTO> {
        const url = `${webServiceEndpoint}/calendrier/${contratId}`;
        return this.http.post<any>(url, evenement);
    }

    fetchEvenement(evenementId, contratId): Observable<EvenementDTO> {
        const url = `${webServiceEndpoint}/calendrier/${contratId}/evenement/${evenementId}`;
        return this.http.get<any>(url);
    }

    fetchEvenementById(evenementId): Observable<EvenementDTO> {
        const url = `${webServiceEndpoint}/calendrier/evenement/${evenementId}`;
        return this.http.get<any>(url);
    }

    deleteEvenement(evenementId, contratId): Observable<EvenementDTO> {
        const url = `${webServiceEndpoint}/calendrier/${contratId}/evenement/${evenementId}`;
        return this.http.delete<any>(url);
    }


    findEvenements(evenementCriteriaDTO: EvenementCriteriaDTO, contratId, page: number, pageSize: number,
                   sort: PaginationPropertySort[]) {
        const url = `${webServiceEndpoint}/calendrier/${contratId}/evenement/`;
        evenementCriteriaDTO.contrat = contratId;

        if (sort == null) {
            sort = [{direction: Order.DESC, property: "id"}]
        }
        const orderable = new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build();

        const params: any = {...orderable};

        // Pour l'instant pas de pagination
        // new PaginationRequestParamsBuilder().setPage(page).setPageSize(pageSize).setSort(sort).build(params);

        if (evenementCriteriaDTO != null) {

            if (evenementCriteriaDTO.motsCles != null && evenementCriteriaDTO.motsCles.length > 0) {
                params.motsCles = evenementCriteriaDTO.motsCles;
            }

            const etats: Array<string> = [];

            if (evenementCriteriaDTO.etatNonSuivi) {
                etats.push('NON_SUIVI');
            }
            if (evenementCriteriaDTO.etatAVenir) {
                etats.push(EtatDateEvenement[EtatDateEvenement.A_VENIR]);
            }
            if (evenementCriteriaDTO.etatEnAttente) {
                etats.push(EtatDateEvenement[EtatDateEvenement.EN_ATTENTE]);
            }
            if (evenementCriteriaDTO.etatValide) {
                etats.push(EtatDateEvenement[EtatDateEvenement.VALIDE]);
            }
            if (evenementCriteriaDTO.etatRejete) {
                etats.push(EtatDateEvenement[EtatDateEvenement.REJETE]);
            }
            if (etats.length > 0) {
                params.etats = etats.toString();
            }
            if (evenementCriteriaDTO.contractants != null && evenementCriteriaDTO.contractants.length > 0) {
                params.contractants = evenementCriteriaDTO.contractants.toString();
            }
            if (evenementCriteriaDTO.evenementTypeCode != null && evenementCriteriaDTO.evenementTypeCode.length > 0) {
                params.evenementTypeCode = evenementCriteriaDTO.evenementTypeCode.toString();
            }
            if (evenementCriteriaDTO.dateEnvoiDebut != null) {
                params.dateEnvoiDebut = moment(evenementCriteriaDTO.dateEnvoiDebut).format('YYYY-MM-DD');
            }
            if (evenementCriteriaDTO.dateEnvoiFin != null) {
                params.dateEnvoiFin = moment(evenementCriteriaDTO.dateEnvoiFin).format('YYYY-MM-DD');
            }

        }

        return this.http.post<PaginationPage<EvenementDTO>>(url, evenementCriteriaDTO, {params: params});

    }

    determineEtatDateEvenements(evenements: EvenementDTO[]): void {
        evenements.forEach(evenement => this.calculerEtatDate(evenement));
    }

    determineEtatDateEvenement(evenObservable: Observable<EvenementDTO>): Observable<EvenementDTO> {
        return evenObservable.pipe(map(this.calculerEtatDate));
    }

    calculerEtatDate(evenement: EvenementDTO): EvenementDTO {
        if (evenement.suiviRealisation) {
            if (evenement.etatEvenement != null) {
                // Cas VALIDE / REJETE
                evenement.etatDateEvenement = EtatDateEvenement[evenement.etatEvenement];
            } else {
                // Si la date début est dans le futur, alors A VENIR
                if (new Date() < evenement.dateDebut) {
                    evenement.etatDateEvenement = EtatDateEvenement.A_VENIR;
                } else {
                    // Sinon EN ATTENTE
                    evenement.etatDateEvenement = EtatDateEvenement.EN_ATTENTE;
                }
            }
        }
        return evenement;
    }

}

export interface EvenementCriteriaDTO {
    contrat?: number;
    motsCles?: string;
    etatNonSuivi?: boolean;
    etatValide?: boolean;
    etatRejete?: boolean;
    etatAVenir?: boolean;
    etatEnAttente?: boolean;
    contractants?: any[];
    dateEnvoiDebut?: Date | string;
    dateEnvoiFin?: Date | string;
    evenementTypeCode?: string[];
    bonsCommande?: boolean;
}

export interface EchangeCriteriaDTO {
    motsCles?: string;
    statut?: string;
    typeEchange?: string;
    applicationCible?: string;
    documents?: string;
    dateEnvoiDebut?: Date | string;
    dateEnvoiFin?: Date | string;
}
