import {webServiceEndpoint} from '../constants';
import {Observable} from 'rxjs/Observable';
import {UtilisateurDTO} from '@dto';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class UtilisateurService {

    constructor(private http: HttpClient) {
    }

    getUtilisateurConnecte(pfUid: string): Observable<UtilisateurDTO> {
        const url = `${webServiceEndpoint}/utilisateurs/contexte/${pfUid}`;
        return this.http.get<any>(url);
    }

    getUtilisateursByServiceId(serviceId: any): Observable<Array<UtilisateurDTO>> {
        const url = `${webServiceEndpoint}/utilisateurs/service/${serviceId}`;
        return this.http.get<any>(url);
    }
}



