import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {DocumentsExternesDTO} from '@dto/documentExterne';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class DocumentExterneService {

    constructor(private http: HttpClient) {
    }

    findDocumentsBySiren(siren): Observable<DocumentsExternesDTO> {
        const url = `${webServiceEndpoint}/fournisseur/attestations-siren/${siren}`;
        return this.http.get<any>(url);
    }

    findDocumentsBySiret(siret): Observable<DocumentsExternesDTO> {
        const url = `${webServiceEndpoint}/fournisseur/attestations-siret/${siret}`;
        return this.http.get<any>(url);
    }

    getAttestation(siretOrSiren, attestations): Observable<HttpResponse<Blob>> {
        const url = `${webServiceEndpoint}/fournisseur/attestations/zip/${siretOrSiren}`;
        return this.http.post<Blob>(url, attestations, {
            observe: 'response',
            responseType: 'blob' as 'json',
        });
    }


}
