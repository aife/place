import {Injectable} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    modalRef: BsModalRef;

    constructor(private bsModalService: BsModalService) {
    }

    openModal(component, options?: any) {
        this.modalRef = this.bsModalService.show(component, options);
        return this.modalRef;
    }
}
