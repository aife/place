import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {ReponseFAQ} from "@dto/backend";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class FaqService {

    _currentContractId: number;

    constructor(private http: HttpClient) {
    }

    public set currentContratId(value: number) {
        this._currentContractId = value;
    }

    init(): Observable<ReponseFAQ> {
        const url = `${webServiceEndpoint}/faq/${this._currentContractId ? this._currentContractId : ''}`;
        return this.http.get<any>(url);
    }
}
