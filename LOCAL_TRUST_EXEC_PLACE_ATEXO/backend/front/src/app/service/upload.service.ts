import {Observable} from 'rxjs/Observable';
import {webServiceEndpoint} from '../constants';
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";


@Injectable()
export class UploadService {
    constructor(private http: HttpClient) {
    }

    delete(reference: string): Observable<any> {
        const url = `${webServiceEndpoint}/upload/delete/${reference}`;
        return this.http.delete<any>(url);
    }

    download(reference: string): string {
        const url = `${webServiceEndpoint}/upload/download/${reference}`;
        return url;
    }
}
