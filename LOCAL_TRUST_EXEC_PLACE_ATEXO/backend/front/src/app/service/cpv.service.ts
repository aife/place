import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
    providedIn: 'root'
})
export class CpvService {

    _token: string;

    constructor(private httpClient: HttpClient) {
    }

    public getToken() {
        const endpoint = '/referentiels/ws/bootstrapConfiguration?locale=fr&configPath=/resources/config/cpv-config.xml';
        this.httpClient.get<any>(endpoint).subscribe(source => this._token = source.token);
    }

    public fetch(request): Observable<any> {
        const endpoint = `/referentiels/ws/searchReferentiel?token=${this._token}&userQuery=${request}&locale=fr`
        return this.httpClient.get(endpoint);
    }
}
