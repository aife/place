import {Component} from '@angular/core';
import {AuthentificationService} from "@services";

@Component({
    selector: 'session-expired-errors',
    templateUrl: './session-expired.component.html',
    styleUrls: ['./session-expired.component.scss']
})
export class SessionExpiredComponent {

    constructor(private readonly authService: AuthentificationService) {
        this.authService.logout();
    }

}
