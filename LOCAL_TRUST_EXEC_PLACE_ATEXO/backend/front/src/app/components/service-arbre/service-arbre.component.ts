import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ServiceDTO} from '@dto';

@Component({
    selector: 'ltexec-service-arbre',
    templateUrl: './service-arbre.component.html',
    styleUrls: ['./service-arbre.component.scss']
})
export class ServiceArbreComponent {

    @Input() services: ServiceDTO[] = [];
    @Input() readOnly = false;
    @Input() selectedOnly = false;
    @Output() onServiceSelected: EventEmitter<ServiceDTO> = new EventEmitter<ServiceDTO>();

    constructor() {

    }

    onServiceChange(service: ServiceDTO) {
        this.onServiceSelected.emit(service);
    }
}
