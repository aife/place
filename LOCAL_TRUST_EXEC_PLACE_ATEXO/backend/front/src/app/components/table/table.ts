import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {Order, PaginationPage, PaginationPropertySort} from '../../common/pagination';
import {Observable} from 'rxjs/Observable';
import {EXEC_SORT} from 'app/constants';

export interface Table {

    fetchPage(pageNumber: number, pageSize: number, sort: PaginationPropertySort[]): Observable<any>;

}

@Component({
    selector: 'ltexec-table-elements-count',
    templateUrl: 'elements-count.html'
})
export class TableElementsCountComponent<T> {
    @Input() page: PaginationPage<T>;
}

@Component({
    selector: 'ltexec-table-pagination',
    templateUrl: 'pagination.html'
})
export class TablePaginationComponent<T> {
    @Input() table: Table;
    @Input() sort: PaginationPropertySort[];
    @Input() page: PaginationPage<T>;
    @Output() change: EventEmitter<PaginationPage<T>> = new EventEmitter<PaginationPage<T>>();

    fetchPageNumber($event) {

        if (isNaN($event.target.value)) {
            return;
        }
        this.page.number = +$event.target.value - 1;
        if (this.page.number > this.page.totalPages) return;
        this.table.fetchPage(this.page.number, this.page.size, this.sort).subscribe(() => {
            this.change.emit(this.page);

        });
    }

    fetchPageSize() {
        this.page.number = 0;
        this.reload();
    }

    fetchNextPage() {
        this.page.number = this.page.number + 1;
        this.reload();
    }

    fetchPreviousPage() {
        this.page.number = this.page.number - 1;
        this.reload();
    }

    reload(): void {
        this.table.fetchPage(this.page.number, this.page.size, this.sort).subscribe(() => {
            this.change.emit(this.page);
        });
    }

    public check(event): boolean {
        const code = (event.which) ? event.which : event.keyCode;
        return !(code > 31 && (code < 48 || code > 57));



    }
}

@Component({
    selector: 'ltexec-table-sort',
    templateUrl: 'sort.html'
})
export class TableSortComponent<T> implements OnInit {

    @Input() label: string;
    @Input() property: string;
    @Input() table: Table;
    @Input() page: PaginationPage<T>;
    @Output() order: EventEmitter<PaginationPropertySort[]> = new EventEmitter<PaginationPropertySort[]>();

    sortDirection: Order;
    sortClass = false;
    sortAscClass = false;
    sortDescClass = false;

    ngOnInit() {
        if (!this.page.order) {
            this.sortClass = true;
            this.sortAscClass = false;
            this.sortDescClass = false;

            return;
        }
    }

    sortByProperty() {
        if (localStorage.getItem(EXEC_SORT)) {
            let save: PaginationPropertySort = JSON.parse(localStorage.getItem(EXEC_SORT));

            if (save.property !== this.property) {
                this.sortDirection = Order.ASC;
            } else {
                this.sortDirection = save.direction ===  Order.ASC ?  Order.DESC:  Order.ASC;
            }
        }

        let sort: PaginationPropertySort[] = [{property: this.property, direction: this.sortDirection ? this.sortDirection : Order.ASC}];

        console.debug(`Tri = ${JSON.stringify(sort)}`);

        localStorage.setItem(EXEC_SORT, JSON.stringify(sort));

        let pageNumber = this.page.number - 1;
        if (pageNumber < 0) {
            pageNumber = 0;
        }
        this.order.emit([{property: this.property, direction: this.sortDirection}]);
        console.log("sortByProperty called", this.property)
        const observable: Observable<any> = this.table.fetchPage(pageNumber, this.page.size, sort);

        if (observable != null) {

            observable.subscribe(() => {
            }, () => {

            });
        }
    }
}

export let TABLE_DIRECTIVES: Array<any> = [
    TableElementsCountComponent,
    TablePaginationComponent,
    TableSortComponent,
];
