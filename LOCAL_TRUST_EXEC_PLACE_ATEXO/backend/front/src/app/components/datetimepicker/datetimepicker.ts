import {
    AfterViewChecked,
    AfterViewInit,
    Component,
    Directive,
    ElementRef,
    EventEmitter,
    Host,
    HostListener,
    Inject,
    Input,
    OnDestroy,
    Output
} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {DateComparable} from 'eonasdan-bootstrap-datetimepicker';
import * as moment from 'moment';

@Component({
    selector: 'ltexec-datetimepicker',
    template: `
        <div  class="input-group datepicker-group">
            <input type="text" class="form-control" (change)="onInputChange($event)" [disabled]="disabled"/>
            <span class="input-group-addon">
             <fa-icon [icon]="['fas', 'calendar']"></fa-icon>
         </span>
        </div>
    `
})
export class DateTimePickerComponent implements AfterViewInit, OnDestroy, AfterViewChecked {


    static DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss';
    static DATE_FORMAT = 'YYYY-MM-DD';

    @Output() change: EventEmitter<any>;
    @Output() blur: EventEmitter<any>;
    @Output() paste: EventEmitter<any>;

    // Pour activer le choix de l'heure, il faut mettre un format avec heure (DD/MM/YYYY HH:mm) et pickTime à true
    @Input() format = 'DD/MM/YYYY';
    @Input() minDate: DateComparable | boolean | undefined;
    @Input() maxDate: DateComparable | boolean | undefined;
    @Input() pickTime = false;

    @Input() disabled = false;

    elementRef: ElementRef;
    inputElement: JQuery;

    value: string;

    constructor(@Inject(ElementRef) elementRef: ElementRef) {
        this.elementRef = elementRef;
        this.change = new EventEmitter();
        this.blur = new EventEmitter();
        this.paste = new EventEmitter();

        const el: any = this.elementRef.nativeElement;
        this.inputElement = $(el).find('input');
    }

    onInputChange($event) {
        event.stopPropagation();
    }

    ngAfterViewInit() {
        const el: any = this.elementRef.nativeElement;
        this.inputElement = $(el).find('input');

        setTimeout(() => {
            this.inputElement.datetimepicker({
                minDate: this.minDate,
                maxDate: this.maxDate,
                format: this.format,
                useCurrent: true,
                defaultDate: this.value === '' ? '' : moment(this.value, this.format),
                keepInvalid: true
            });
        });

        this.inputElement.on('blur', (ev, e) => {
            this.blur.emit(null);
        });

        this.inputElement.on('dp.hide change paste input', (ev, e) => {
            const value = this.inputElement.val();
            if (value == null || value === '') {
                this.change.emit(null);
            } else {
                const momentDate = moment(value, this.format);
                if (!momentDate.isValid()) {
                    this.change.emit('null');
                    return;
                }
                const valueParsed = momentDate.format(this.pickTime ? DateTimePickerComponent.DATE_TIME_FORMAT
                    : DateTimePickerComponent.DATE_FORMAT);
                this.change.emit(valueParsed);
            }
        });
    }

    routerCanReuse() {
        return false;
    }

    ngAfterViewChecked() {

    }

    ngOnDestroy() {
        this.inputElement.val('');
        // this.inputElement.data("DateTimePicker").destroy();
    }
}

@Directive({
    selector: 'ltexec-datetimepicker[formControlName],ltexec-datetimepicker[formControl],ltexec-datetimepicker[ngModel]'
})
export class DateTimePickerControlValueAccessor implements ControlValueAccessor {

    el: any;
    inputElement: JQuery;

    @HostListener('change', ['$event'])
    onChange = (_) => {

    }

    @HostListener('blur')
    onTouched = () => {

    }

    constructor( private _elementRef: ElementRef,
                @Host() private dateTimePickerComponent: DateTimePickerComponent, private control: NgControl) {
        this.el = this._elementRef.nativeElement;
        this.inputElement = $(this.el).find('input');
        this.control.valueAccessor = this;
    }

    writeValue(value: any): void {
        if (value != null && value !== '') {
            const propertyValue = moment(new Date(value)).format(this.dateTimePickerComponent.format);
            this.dateTimePickerComponent.value = propertyValue;
        } else {
            this.dateTimePickerComponent.value = '';
        }
    }

    registerOnChange(fn: (_: any) => {}): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => {}): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.dateTimePickerComponent.disabled = isDisabled;
    }
}

export let DateTimePicker: Array<any> = [
    DateTimePickerComponent,
    DateTimePickerControlValueAccessor
];
