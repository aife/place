export function isEmpty(field: any) {
    return isBlank(field) || field.length === 0;
}

export function isPresent(obj: any): boolean {
    return obj !== undefined && obj !== null;
}

export function isBlank(obj: any): boolean {
    return obj === undefined || obj === null;
}
