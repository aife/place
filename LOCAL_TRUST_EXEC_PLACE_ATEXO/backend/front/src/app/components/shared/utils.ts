import {Injectable, isDevMode} from '@angular/core';
import {DestinataireDTO, TypeDestinataire} from '@dto/messagerie-dto';

@Injectable({
    providedIn: 'root'
})
export class Utils {
    constructor() {
    }

    downloadUrl(url: string) {
        if (isDevMode()) {
            console.log(`ouverture de ${url} dans une nouvelle fenêtre`);
        }
        window.open(url, '_blank');
    }

    /**
     * Enrechir la liste des contacts (utilisée dans le filtre de recherche
     */
    concatContacts(sourceContacts: DestinataireDTO[], destination: DestinataireDTO[], groupe: string) {
        if (sourceContacts != null) {
            sourceContacts.forEach(contact => {
                contact.type = groupe;
                contact.typeDestinataire = TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
                if (!contact.nomContactDest) {
                    contact.nomContactDest = contact.mailContactDestinataire;
                }
                destination.push(contact);
            });
        }
    }

    contientEmail(liste: DestinataireDTO[], mail: string): boolean {
        if (liste == null || liste.length === 0) {
            return false;
        }
        const found = liste.find(e => e.mailContactDestinataire.toLowerCase() === mail.toLowerCase());
        return found !== undefined;
    }

    isValidEmail(email: string): boolean {
        const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!re.test(email)) {
            return false;
        } else {
            return true;
        }
    }


}
