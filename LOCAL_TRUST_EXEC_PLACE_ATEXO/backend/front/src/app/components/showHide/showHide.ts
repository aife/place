import {Component, Input} from '@angular/core';

@Component({
    selector: 'ltexec-show-hide',
    template: `
                <div *ngIf="isMax()" class="box desc-activite form-control-static">
                         <div *ngIf="highlightedDiv == 1">
                            <div>{{text}}<a (click)="toggleHighlight(1);" style='cursor: pointer' title="Afficher moins"> [ - ]</a></div>
                         </div>
                        <div *ngIf="highlightedDiv !=1">
                             <div>{{text.substr(0,taille)}}
                             <a (click)="toggleHighlight(1);" title="Afficher plus" style="cursor: pointer">[ + ]</a></div>
                        </div>
                </div>
                <div *ngIf="!isMax()" class="box form-control-static">
                    {{text}}
                </div>

                `
})

export class ShowHideTextComponent {
    @Input() text: string;
    @Input() taille: number = 250;

    highlightedDiv = 0;
    long: number;

    toggleHighlight(newValue: number) {
        if (this.highlightedDiv === newValue) {
            this.highlightedDiv = 0;
        } else {
            this.highlightedDiv = newValue;
        }
    }

    isMax(): boolean {
        if (this.text) {
            this.long = this.text.length;
            if (this.long >= this.taille) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

}
