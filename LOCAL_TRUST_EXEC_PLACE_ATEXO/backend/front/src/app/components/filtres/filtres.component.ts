import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MessagerieService} from '@services/messagerie.service';
import {DestinataireDTO, SerializableRechercheMessage, Template, TypeStatutEmail} from '@dto/messagerie-dto';
import {CRITERE_TEMPLATE, LISTE_SAISIE_LIBRE} from '@dto/messagerie-constants';
import {Utils} from '../shared/utils';
import {BsDatepickerConfig} from "ngx-bootstrap/datepicker";

@Component({
    selector: 'messagerie-filtres',
    templateUrl: './filtres.component.html',
    styleUrls: ['./filtres.component.scss']
})
export class FiltresComponent implements OnInit {
    statutsReception: TypeStatutEmail[] = [TypeStatutEmail.ACQUITTE, TypeStatutEmail.ACQUITTEMENT_PARTIEL, TypeStatutEmail.ENVOYE, TypeStatutEmail.ECHEC_ENVOI, TypeStatutEmail.EN_COURS_ENVOI, TypeStatutEmail.EN_ATTENTE_ENVOI, TypeStatutEmail.ECHEC_PARTIEL, TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT];
    emailCriteriaDTO: SerializableRechercheMessage = {statuts: this.statutsReception};
    @Input() templates: Template[] = [];
    @Output() filterChange = new EventEmitter<SerializableRechercheMessage>();
    selectedTemplate: Template;
    selectedDestinataires: DestinataireDTO[] = [];
    destinataires: DestinataireDTO[] = [];
    dateRange: Date[];
    statutEnCourEnvoi: boolean;
    statutNonDelivre: boolean;
    statutDelivre: boolean;
    statutAcquitte: boolean;
    dateEnvoiStatut: boolean;
    reponseAttendue: boolean;

    bsConfig: Partial<BsDatepickerConfig>;

    constructor(private messagerieService: MessagerieService, private utils: Utils) {
        this.bsConfig = {containerClass: 'theme-dark-blue', isAnimated: true};
    }

    ngOnInit() {
        this.messagerieService.getDestinataires().subscribe(d => this.destinataires = d);
    }

    onCriteresChanges() {
        this.emailCriteriaDTO.criteres = {critere: []};
        this.emailCriteriaDTO.statuts = this.getStatuts();
        this.emailCriteriaDTO.nomOuMailDestList = [];
        if (this.selectedTemplate) {
            this.emailCriteriaDTO.criteres.critere.push({
                nom: CRITERE_TEMPLATE,
                valeur: '' + this.selectedTemplate.id
            });
        }
        if (this.selectedDestinataires && this.selectedDestinataires.length > 0) {
            const mails = this.selectedDestinataires.map(d => d.mailContactDestinataire);
            this.emailCriteriaDTO.nomOuMailDestList = mails;
        }

        if (this.reponseAttendue) {
            this.emailCriteriaDTO.reponseAttendue = this.reponseAttendue;
            console.log(this.emailCriteriaDTO.reponseAttendue)
        } else {
            this.emailCriteriaDTO.reponseAttendue = null;
        }

        if (this.dateRange && this.dateRange.length === 2) {
            this.emailCriteriaDTO.dateEnvoiDebut = this.dateRange[0];
            this.emailCriteriaDTO.dateEnvoiFin = this.dateRange[1];
        } else {
            this.emailCriteriaDTO.dateEnvoiDebut = null;
            this.emailCriteriaDTO.dateEnvoiFin = null;
        }
        this.filterChange.emit(this.emailCriteriaDTO);
    }

    getStatuts() {
        let retour: TypeStatutEmail [] = [];
        if (this.statutEnCourEnvoi) {
            retour.push(TypeStatutEmail.EN_COURS_ENVOI);
        }
        if (this.statutNonDelivre) {
            retour.push(TypeStatutEmail.ECHEC_ENVOI);
        }
        if (this.statutDelivre) {
            retour.push(TypeStatutEmail.ENVOYE);
            retour.push(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT);
        }
        if (this.statutAcquitte) {
            retour.push(TypeStatutEmail.ACQUITTE);
        }

        if (retour.length === 0) {
            retour = this.statutsReception;
        }
        return retour;
    }

    onAddFreeText = (term) => {
        if (!this.utils.isValidEmail(term) || this.destinataires.find(d => term === d) != null) {
            return null;
        } else {
            const ret: DestinataireDTO = {mailContactDestinataire: term, type: LISTE_SAISIE_LIBRE};
            return ret;
        }
    }

    onStatutChange(event) {
        this.reponseAttendue = event;
        this.onCriteresChanges();
    }

    onKeyWordChange(event) {
        event.preventDefault();
        const keyWords = event.target.value;
        if (keyWords.length === 0 || keyWords.length > 2) {
            this.emailCriteriaDTO.motsCles = keyWords;
            this.onCriteresChanges();
        }
    }
}
