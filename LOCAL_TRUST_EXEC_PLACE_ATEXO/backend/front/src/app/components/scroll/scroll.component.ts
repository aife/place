import {Component, inject, OnInit} from '@angular/core';
import {DOCUMENT, ViewportScroller} from '@angular/common';
import {fromEvent, Observable} from 'rxjs';
import {map} from 'rxjs/operators/map';

@Component({
    selector: 'app-scroll',
    templateUrl: './scroll.component.html',
    styleUrls: ['./scroll.component.scss'],
})
export class ScrollComponent implements OnInit {

    private document = inject(DOCUMENT);
    private viewport = inject(ViewportScroller);

    ngOnInit(): void {
    }

    readonly showScroll$: Observable<boolean> = fromEvent(
        this.document,
        'scroll'
    ).pipe(
        map(() => this.viewport.getScrollPosition()?.[1] > 0)
    );

    onScrollToTop(): void {
        window.scrollTo({top: 0, behavior: 'smooth'});
    }

}
