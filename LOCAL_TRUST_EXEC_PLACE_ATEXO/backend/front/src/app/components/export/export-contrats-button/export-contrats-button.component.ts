import {Component, Input, OnInit} from '@angular/core';
import {ContratCriteriaDTO, ContratService} from "@services";
import {genericErrorNotification} from "@common/notification";
import {saveAs} from "file-saver";
import * as moment from 'moment';
import {WorkerService} from "@services/worker.service";

@Component({
    selector: 'ltexec-export-contrats-button',
    templateUrl: './export-contrats-button.component.html',
    styleUrls: ['./export-contrats-button.component.scss']
})
export class ExportContratsButtonComponent implements OnInit {

    exportEnCours: boolean = false;
    checkWorkerInterval: any;
    @Input() findContratDTO: ContratCriteriaDTO;

    constructor(private contratService: ContratService, private workerService: WorkerService) {
    }



    ngOnInit(): void {
    }

    exporter() {
        this.exportEnCours = true;
        this.contratService.exportContrats(this.findContratDTO).subscribe(idTask => {
            console.log("idTask", idTask);
            this.checkWorkerInterval = setInterval(() => {
                this.workerService.findTask(idTask).subscribe(value => {
                    if (value.result === 'success' && value.data) {
                        this.exportEnCours = false;
                        clearInterval(this.checkWorkerInterval);
                        this.workerService.download(idTask).subscribe(file => {
                            const date = new Date();
                            const fichier = `Contrats - ${moment(date).format('yyyyMMDD HHmmss')}.xlsx`
                            saveAs(file, fichier);
                            clearInterval(this.checkWorkerInterval);
                        }, () => this.exportEnCours = false)
                    }
                    if (value.result === 'error') {
                        clearInterval(this.checkWorkerInterval);
                        genericErrorNotification();
                        this.exportEnCours = false;
                    }
                })
            }, 10000);

        }, () => {
            console.log("error export")
            this.exportEnCours = false;
            genericErrorNotification();
            clearInterval(this.checkWorkerInterval);
        })




        /*this.contratService.exportContrats(this.findContratDTO).subscribe(blob => {
            ExportContratsButtonComponent.exportEnCours = false;
            const date = new Date();
            const fichier = `Contrats - ${moment(date).format('yyyyMMDD HHmmss')}.xlsx`
            saveAs(blob, fichier);
        }, error => {
            genericErrorNotification();
            ExportContratsButtonComponent.exportEnCours = false;
        });*/
    }

}
