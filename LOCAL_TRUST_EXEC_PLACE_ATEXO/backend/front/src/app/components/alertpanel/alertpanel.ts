import {Component, Input} from '@angular/core';

@Component({
    selector: 'ltexec-alertpanel',
    template: `
        <div *ngIf="content" class="panel {{configuration[type].panelClass}} {{configuration[type].alertClass}}">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#accordion_{{type}}" aria-expanded="false">
                        <i class="fa {{configuration[type].iconClass}} fa-2x"></i>{{title}} <span class="normal read-more">Plus de détail</span>
                    </a>
                </h4>
            </div>
            <div id="accordion_{{type}}" class="panel-collapse collapse">
                <div class="panel-body">
                    <div [ngSwitch]="comment" class="state-list">
                        <div *ngSwitchCase="true">
                            <div class="bloc-commentaire">
                                {{content}}
                            </div>
                        </div>
                        <div *ngSwitchCase="false">
                            {{content}}
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div *ngIf="!content" class="alert {{configuration[type].alertClass}}" role="alert">
            <i class="fa {{configuration[type].iconClass}} fa-2x"></i><p>{{title}}</p>
        </div>
    `
})
export class AlertPanelComponent {

    static CONFIGURATION = {
        info: {
            panelClass: 'panel-info',
            alertClass: 'alert-info',
            iconClass: 'fa-info-circle'
        },
        warning: {
            panelClass: 'panel-warning',
            alertClass: 'alert-warning',
            iconClass: 'fa-warning'
        },
        danger: {
            panelClass: 'panel-danger',
            alertClass: 'alert-danger',
            iconClass: 'fa-exclamation-circle'
        },
        success: {
            panelClass: 'panel-success',
            alertClass: 'alert-success',
            iconClass: 'fa-check'
        }
    };

    @Input() title: string;
    @Input() content: string;
    @Input() type: 'info' | 'warning' | 'danger' | 'success' = 'info';
    @Input() comment = false;

    configuration = AlertPanelComponent.CONFIGURATION;

}
