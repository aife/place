import {Component, EventEmitter, Output} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
    selector: 'messagerie-confirmation-modal',
    templateUrl: './ConfirmationModal.html',
    styleUrls: ['./ConfirmationModal.scss']
})
export class ConfirmationModalComponent {
    title: string;
    message: string;
    @Output() yes = new EventEmitter<string>();

    constructor(public modalRef: BsModalRef) {
    }

    conifrmer() {
        this.modalRef.hide();
        this.yes.emit('confirmé');
    }

}
