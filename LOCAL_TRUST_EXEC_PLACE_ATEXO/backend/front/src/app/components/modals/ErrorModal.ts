import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
    selector: 'messagerie-error-modal',
    templateUrl: './ErrorModal.html',
    styleUrls: ['./ErrorModal.scss']
})
export class ErrorModalComponent {
    title: string;
    message: string;
    icon: IconProp;

    constructor(public modalRef: BsModalRef) {
    }

}
