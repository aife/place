import {Component, Input, OnInit} from '@angular/core';
import {ContratDTO, EvenementDTO, ValueLabelDTO} from '../../external/backend';
import {EtatEvenement} from '@dto/backendEnum';
import {USER_CONTEXT} from "@common/userContext";

@Component({
    selector: 'ltexec-gauge',
    template: `   <div class="control-label control-label-gauge">Détail des montants commandés  </div>
                        <div class="gauge-frame">

                       <div [ngClass] ="{'gauge-depassement' :  borneMax != null, 'gauge-other' : borneMax == null}">
                        <div [ngClass]="{'in': isMax(),  'out': !isMax()}">
                            <div *ngIf="borneMax" class = "border_max" [style.width]="getBorneMaxPercent()" >
                                <div class="value">Maxi {{contrat.borneMaximale | currencyEuro}}</div>
                            </div>
                            <div *ngIf="borneMin" class = "border_min" [style.width]="getBorneMinPercent()" >
                                <div class="value">Mini {{borneMin | currencyEuro}}</div>
                            </div>
                            <div class="amount_total "  [style.width]="getPercentForAmountTotal()" [ngClass]="{'border-radius-none': evenement.bonCommandeTypeDTO.montantHT}" >
                                <div class="value" title="Bons de commande précédents - {{montantCommandeHT | currencyEuro}} {{ devise.value }} HT">{{montantCommandeHT | currencyEuro}} {{ devise.value }} HT
                                   </div>
                            </div>
                            <div class="amount_current" [style.width]="getPercentForAmountCurrent()" [style.left]="getPercentForAmountTotal()">
                            <div class="value" title="Montant du bon de commande - {{evenement.bonCommandeTypeDTO.montantHT | currencyEuro}} {{ devise.value }} HT">{{evenement.bonCommandeTypeDTO.montantHT | currencyEuro}} {{ devise.value }} HT </div>
                            </div>
                        </div>
                        <div class="axis"> </div>
                        <div class="ladder">Montant {{ devise.value }} HT</div>
                     </div>
                    </div>
                `
})

export class GaugeComponent implements OnInit {

    devise: ValueLabelDTO;

    @Input() contrat: ContratDTO;
    @Input() evenement: EvenementDTO;

    public borneMin: number;
    public borneMax: number;
    public montantCommandeHT: number;


    private borneMaxPercent = '80%';
    private borneMinPercent = '20%';
    private amountTotalPercent = '0%';
    private amountCurrentPercent = '0%';
    private unit = 1;
    private totalCommand = 0;
    private totalCommandPercent = '0%';
    @Input() listBondeCommandes: EvenementDTO[] = [];


    constructor() {

    }

    ngOnInit(): void {
        if (this.evenement == null) {
            this.evenement = {bonCommandeTypeDTO: {montantHT: 0}};
        }
        this.getUnit();


        if (this.contrat.borneMaximale) {
            this.borneMax = this.contrat.borneMaximale;
        }
        if (this.contrat.borneMinimale) {
            this.borneMin = this.contrat.borneMinimale;
        }


        this.montantCommandeHT = this.listBondeCommandes.filter(eve => EtatEvenement[eve.etatEvenement] !== EtatEvenement.REJETE)
            .map(eve => eve.bonCommandeTypeDTO.montantHT).reduce((a, b) => a + b, 0);
        this.totalCommand = this.montantCommandeHT;

        this.devise = USER_CONTEXT.utilisateur.devise;


    }


    isMax(): boolean {
        if (this.contrat.borneMaximale) {
            return (this.montantCommandeHT + this.evenement.bonCommandeTypeDTO.montantHT <= this.contrat.borneMaximale);
        }
        if (!this.contrat.borneMaximale) {
            return (this.montantCommandeHT + this.evenement.bonCommandeTypeDTO.montantHT === this.totalCommand);
        }
        return false;
    }

    getUnit(): number {
        if (this.borneMax) {
            this.unit = this.borneMax / 80;
            return this.unit;
        }
        if (this.borneMin) {
            this.unit = this.borneMin / 20;
            return this.unit;

        }
        if (this.totalCommand > 0) {
            this.unit = (this.totalCommand + this.evenement.bonCommandeTypeDTO.montantHT) / 75;
            return this.unit;
        } else {
            if (this.evenement.bonCommandeTypeDTO.montantHT === 0) {
                this.unit = 1;
            } else {
                this.unit = this.evenement.bonCommandeTypeDTO.montantHT / 75;
            }

            return this.unit;
        }
    }

    getPercentForAmount(): void {
        const totalCommandPercent = this.totalCommand;
        const amountCurrentPercent = this.evenement.bonCommandeTypeDTO.montantHT / this.getUnit();
        const amountTotalPercent = this.montantCommandeHT / this.getUnit();
        const borneMinPercent = this.borneMin / this.getUnit();
        const borneMaxPercent = this.borneMaxPercent;


        this.borneMaxPercent = borneMaxPercent;
        if (amountTotalPercent > 100) {
            this.amountTotalPercent = 100 + '%';
        } else {
            this.amountTotalPercent = amountTotalPercent + '%';
        }
        this.borneMinPercent = borneMinPercent + '%';
        this.amountCurrentPercent =
            ((amountCurrentPercent + amountTotalPercent > 100) ? (100 - amountTotalPercent) : amountCurrentPercent) + '%';
    }


    getPercentForAmountTotal(): string {
        this.getPercentForAmount();
        return this.amountTotalPercent;
    }

    getPercentForAmountCurrent(): string {
        this.getPercentForAmount();
        return this.amountCurrentPercent;
    }

    getBorneMinPercent(): string {
        this.getPercentForAmount();
        return this.borneMinPercent;
    }

    getBorneMaxPercent(): string {
        this.getPercentForAmount();
        return this.borneMaxPercent;
    }

}
