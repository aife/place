import {Component, Input} from '@angular/core';
import {AdresseDTO} from '@dto';

@Component({
    selector: 'ltexec-inline-address',
    template: `
<ng-template [ngIf]="adresse">
    <span *ngIf="adresse.adresse != null">{{adresse.adresse}},</span>
    {{adresse.codePostal}} {{adresse.commune}}
    <span *ngIf='ficheEtrangere && (adresse.pays != null && adresse.pays != "")'>({{ adresse.pays}})</span>
    <span *ngIf='!ficheEtrangere && (adresse.pays != null && adresse.pays != "")'>({{ adresse.pays}})</span>
</ng-template>
    `
})
export class InlineAddressComponent {
    @Input() adresse: AdresseDTO;
    @Input() ficheEtrangere = false;
}
