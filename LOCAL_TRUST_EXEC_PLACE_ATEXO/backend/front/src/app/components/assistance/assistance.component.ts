import { Component, OnInit } from '@angular/core';
import { APPLICATION } from '@common/parametrage';
import { FaqService } from '@services';

@Component( {
    selector: 'ltexec-assistance',
    templateUrl: './assistance.component.html',
    styleUrls: ['./assistance.component.scss']
} )
export class AssistanceComponent implements OnInit {
    enableFaq = false;
    token: string;
    contratId: number;

    constructor( private faqService: FaqService ) {

    }

    ngOnInit() {
        this.enableFaq = APPLICATION.parametrage?.faq?.actif;
    }

    initFaq() {
        this.faqService.init().subscribe( reponse => {
            this.token = reponse.token;
            window.open( `/faq/?token=${this.token}`, '_blank' );
        } );
    }

}
