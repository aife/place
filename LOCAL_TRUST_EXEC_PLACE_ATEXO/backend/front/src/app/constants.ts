import {environment} from '../environments/environment';

export const backendContext = environment.API_PATH;

// export const messagerieSecuriseeContext = `https://ms-int.local-trust.com/bleu`;


export const authenticationEndpoint = `${backendContext}/oauth/token`;
export const authorizationEndpoint = `${backendContext}/api/authorization/authorize`;

export const webServiceEndpoint = `${environment.API_PATH}/api`;

export const defaultItemsCountPerPage = 10;

export const messagerieWebServiceEndpoint = `/messagerieSecurisee/rest`;

export const DATE_TIME_ISO_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZZ';
export const WEB_COMPONENTS = ['exec-liste-contrats', 'exec-fiche-contrat', 'exec-liste-fournisseurs', 'exec-saisir-contrat', 'exec-tableau-contrats', 'exec-supervision-donnees-essentielles']

export const LOADER_TIMEOUT = 2000;
//local storage
export const FILTRE_CONTRAT = 'filtre-contrat';
export const MOTS_CLES = 'motsCles';
export const EXEC_SORT = 'exec.sort';
