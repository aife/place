import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs";
import {USER_CONTEXT} from "@common/userContext";
import {authenticationEndpoint, backendContext} from '../constants';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(request.url.includes(backendContext) && !request.url.includes(authenticationEndpoint)) {
            if (!!USER_CONTEXT?.authentification?.token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${USER_CONTEXT?.authentification.token}`
                    }
                });
            }
            if (!!USER_CONTEXT?.utilisateur?.uuid) {
                request = request.clone({
                    setHeaders: {
                        'User-Uuid': `${USER_CONTEXT?.utilisateur?.uuid}`
                    }
                });
            }
        }
        return next.handle(request);
    }
}
