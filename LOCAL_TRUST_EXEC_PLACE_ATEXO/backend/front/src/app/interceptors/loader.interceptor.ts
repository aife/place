import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {hideLoading, showLoading} from '@common/loader';
import {catchError, finalize} from 'rxjs/operators';
import {LOADER_TIMEOUT, webServiceEndpoint} from '../constants';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

    URL_EXCLUDED_LIST = ['/fournisseur/attestations']

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.isApiUrl(httpRequest.url) && !this.isExcludedUrl(httpRequest.url)) {
            const loader = setTimeout(() => {
                showLoading();
            }, LOADER_TIMEOUT);
            return next.handle(httpRequest).pipe(catchError(err => {
                clearTimeout(loader);
                hideLoading();
                return throwError(err);
            })).pipe(
                finalize(() => {
                    clearTimeout(loader);
                    hideLoading();
                })
            );
        }
        return next.handle(httpRequest);
    }

    private isApiUrl(requestUrl: string): boolean {
        return requestUrl.indexOf(webServiceEndpoint) != -1;
    }

    private isExcludedUrl(requestUrl: string) {
        return this.URL_EXCLUDED_LIST.filter(e => requestUrl.indexOf(e) != -1)?.length > 0;
    }
}
