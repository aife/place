import {Injectable, isDevMode} from '@angular/core';
import {Observable, OperatorFunction, throwError} from 'rxjs';
import {Statut, USER_CONTEXT} from '@common/userContext';
import {APPLICATION} from '@common/parametrage';
import {
    Account,
    AuthentificationService,
    ParametrageService,
    PF_UID,
    ReferentielService,
    SSO_TOKEN,
    Token,
    UtilisateurService
} from '@services';
import {catchError, mergeMap} from 'rxjs/operators';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {map} from 'rxjs/operators/map';
import {UtilisateurDTO} from '@dto';

@Injectable( {
    providedIn: 'root'
} )
export class SsoGuard implements CanActivate {

    constructor(
        private authentificationService: AuthentificationService,
        private utilisateurService: UtilisateurService,
        private parametrageService: ParametrageService,
        private referentielService: ReferentielService
    ) {
        if (isDevMode) {
            localStorage.setItem(SSO_TOKEN, new Date().getTime().toString());
            if (localStorage.getItem(PF_UID) === null) {
                localStorage.setItem(PF_UID, 'MPE_atexo_release');
            }
        }

    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        console.log( 'Garde de sécurité activé' );

        let param = this.authentificationService.getCode();
        let storage = localStorage.getItem( SSO_TOKEN );
        let pfUid = localStorage.getItem(PF_UID);

        console.debug( `Parametre = ${param}, stockage local = ${storage}` );

        if ( param && (param !== storage) ) {
            console.log( 'Authentification par code SSO' );

            console.debug( 'Suppression du jeton sso précédent' );

            localStorage.removeItem( SSO_TOKEN );

            return this.authentifier(pfUid, param);
        } else if ( storage ) {
            if ( USER_CONTEXT.utilisateur ) {
                console.info( 'Authentification déjà faite' );

                return true;
            } else {
                console.log( `Tentative d'authentification par le jeton SSO dans le local storage` );

                return this.authentifier(pfUid, storage);
            }
        } else {
            console.error( 'Aucune authentification possible' );

            USER_CONTEXT.authentification.ko( `Aucun jeton SSO n'a été fourni a EXEC. Impossible donc de vous authentifier.`, { code: 'EXEC-AUTH-ERR-001' } );

            return false;
        }
    }

    private authentifier(pfUid: string, code: string): Observable<any> {
        return this.authentificationService.authorize(pfUid, code)
            .pipe( this.handleError<Account>( `Impossible de récuperer l'utilisateur depuis MPE a partir du jeton SSO : celui-ci ne semble plus valide.`, { code: 'EXEC-AUTH-ERR-002', sso: code } ) )
            .pipe( mergeMap( account => {
                USER_CONTEXT.account = account;

                console.debug( `Récupération OK du compte MPE = `, USER_CONTEXT.account );

                return this.authentificationService.login()
                    .pipe( this.handleError<Token>( `Impossible de réaliser l'authentification OAUTH dans EXEC`, { code: 'EXEC-AUTH-ERR-003', utilisateur: USER_CONTEXT.account.login } ) );
            } ) )
            .pipe( mergeMap( token => {
                console.debug( `Jeton = ${token.access_token}` );

                USER_CONTEXT.authentification.token = token.access_token;
                return this.utilisateurService.getUtilisateurConnecte(pfUid)
                    .pipe( this.handleError<UtilisateurDTO>( `Impossible de récupérer l'utilisateur dans EXEC. Cela peut venir d'un délai de synchronisation`, { code: 'EXEC-AUTH-ERR-004', jeton: USER_CONTEXT.authentification.token } ) );
            } ) )
            .pipe( mergeMap( utilisateur => {
                console.info( `Récupération OK de l'utilisateur = `, utilisateur );

                USER_CONTEXT.utilisateur = utilisateur;

                if ( utilisateur.habilitations ) {
                    USER_CONTEXT.habilitations = utilisateur.habilitations.map( habilitation => habilitation.code );
                } else {
                    USER_CONTEXT.habilitations = [];
                }

                USER_CONTEXT.authentification.status = Statut.OK;

                return this.parametrageService.getClientParametrage()
                    .pipe( this.handleError( `Impossible de récupérer le paramétrage depuis EXEC.`, { code: 'EXEC-AUTH-ERR-005' } ) );
            } ) )
            .pipe( map( parametrage => this.flatten( parametrage ) ) )

            .pipe( mergeMap( parametrage => {
    APPLICATION.parametrage = parametrage;

    return this.referentielService.chargerTousReferentiels()
            .pipe( this.handleError( `Impossible de charger les référentiels depuis EXEC.`, { code: 'EXEC-AUTH-ERR-006' } ) );
} ) )

            .pipe( map( _ => {
                console.debug( 'Remplacement du jeton SSO' );

                localStorage.setItem( SSO_TOKEN, code );
            } ) )
            .pipe( map( _ => APPLICATION.loaded( true ) ) );
    }

    private handleError<T>( message: string, contexte?: any ): OperatorFunction<T, T> {
        return catchError( error => {
                USER_CONTEXT.authentification.ko( message, contexte );

                APPLICATION.loaded( false );

                return throwError( message );
            }
        );
    }

    // Vient d'Internet, très légèrement modifié
    private flatten( data ) {
        if ( Object( data ) !== data || Array.isArray( data ) ) {
            return data;
        }
        const regex = /\.?([^.\[\]]+)|\[(\d+)]/g,
            resultholder = {};
        for ( const p in data ) {
            let cur = resultholder,
                prop = '',
                m;
            while ( m = regex.exec( p ) ) {
                cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}));
                prop = m[2] || m[1];
            }
            cur[prop] = data[p] == 'false' ? false : (data[p] == 'true' ? true : data[p]);
        }
        return resultholder[''] || resultholder;
    }

}


