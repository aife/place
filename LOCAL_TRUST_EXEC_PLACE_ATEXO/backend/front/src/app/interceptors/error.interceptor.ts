import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {SessionExpiredComponent} from '@components/session-expired/session-expired.component';
import {backendContext} from '../constants';
import * as Notification from '@common/notification';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    bsModalRef: BsModalRef;

    constructor(private modalService: BsModalService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.startsWith(backendContext)) {
            return next.handle(request).pipe(catchError(err => {
                console.error(err)
                if ([401].indexOf(err.status) !== -1) {
                    let config = {
                        backdrop: true,
                        ignoreBackdropClick: true,
                        class: "modal-semi-full-width"
                    };
                    this.bsModalRef = this.modalService.show(SessionExpiredComponent, config);
                }
                if (err.error && Array.isArray(err.error)) {
                    let message = err.error.map((error: any) => error.message).join('. ');
                    Notification.errorNotification(message);
                } else {
                    Notification.errorNotification('Une erreur est survenue, veuillez contacter votre administrateur.');
                }

                return throwError(err);
            }))
        }
        return next.handle(request);
    }
}
