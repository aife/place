export const defaultItemsCountPerPage = 10;

export const DATE_TIME_ISO_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZZ';
export const CRITERE_TEMPLATE = 'template';
export const TEMPLATES_PATH = '/app.php/api/v1/messagerie/templates';
export const MS_WEBSERVICE_ENDPOINT = '/messagerieSecurisee/rest';
export const LISTE_SAISIE_LIBRE = 'Destinataires libres saisis précédemment';
export const TEMPLATE_AVEC_AR = 'templateMPEAvecAR.ftl';
export const TEMPLATE_SANS_AR = 'templateMPESansAR.ftl';
