export interface MetaDocumentExterneDTO {
    code?: string;
    libelle: string;
    uri?: string;
    params_uri?: string;
}

export interface DownloadDocumentExterneDTO {
    url: string;
    date_mise_a_jour?: string;
}

export interface DocumentsExternesDTO {

    meta: {
        [key: string]: MetaDocumentExterneDTO;
    };
    documents: {
        [key: string]: DownloadDocumentExterneDTO;
        zip_archive: DownloadDocumentExterneDTO;
    };
    error: string;

}
