import {TypeDestinataire, TypeMessage, TypeStatutEmail} from './backendEnum';
import {ContactDTO, DocumentContratDTO, UtilisateurDTO} from './backend';

export interface CritereRechercheMessage extends FormulaireRecherche {
    critere?: SerializableRechercheMessage;
}

export interface SerializableRechercheMessage {
    token?: string;
    typePlateformeRecherche?: string | TypeDestinataire;
    idPlateformeRecherche?: string;
    idObjetMetier?: string;
    idContactDestList?: number[];
    idEntrepriseDest?: string;
    mailDestinataire?: string;
    refObjetMetier?: string;
    objetMessage?: string;
    nomOuMailDestList?: string[];
    nomOuMailExp?: string;
    dateEnvoiDebut?: Date | string;
    dateEnvoiFin?: Date | string;
    dateARDebut?: Date | string;
    dateARFin?: Date | string;
    ids?: any;
    statuts?: TypeStatutEmail[];
    criteres?: Criteres;
    reponseAttendue?: boolean;
    typeMessage?: string;
    dateDemandeEnvoiDebut?: Date | string;
    dateDemandeEnvoiFin?: Date | string;
    pieceJointe?: string;
    pieceJointeNom?: string;
    idMessageDestinataire?: string;
    motsCles?: string;
    reponseNonLue?: boolean;
    enAttenteReponse?: boolean;
}

export interface CritereDTO {
    id?: number;
    nom?: string;
    valeur?: string;
}

export interface DestinataireDTO {
    typeDestinataire?: string | TypeDestinataire;
    idEntrepriseDest?: string;
    idContactDest?: number;
    nomEntrepriseDest?: string;
    nomContactDest?: string;
    mailContactDestinataire?: string;
}

export interface EmailDTO {
    id?: number;
    typeDestinataire?: string | TypeDestinataire;
    identifiantObjetMetier?: string;
    identifiantEntreprise?: string;
    identifiantContact?: number;
    nomEntreprise?: string;
    nomContact?: string;
    email?: string;
    statutEmail?: string | TypeStatutEmail;
    codeLien?: string;
    dateDemandeEnvoi?: number;
    dateEnvoi?: number;
    dateAR?: number;
    nombreEssais?: number;
    erreur?: string;
    identifiantEntrepriseAR?: string;
    identifiantContactAR?: string;
    nomEntrepriseAR?: string;
    nomContactAR?: string;
    emailContactAR?: string;
    message?: MessageDTO;
    reponse?: EmailDTO;
    dateModificationBrouillon?: number;
    dateDemandeEnvoiAsDate?: Date;
    dateEnvoiAsDate?: Date;
    dateARAsDate?: Date;
    dateModificationBrouillonAsDate?: Date;
    contact?: ContactDTO;
    utilisateur?: UtilisateurDTO;
    favori: boolean;
    toutesLesReponses?: EmailDTO[];
}

export interface MessageDTO extends MessagePlateformeInfosDTO {
    id?: number;
    referenceObjetMetier?: string;
    typeMessage?: string | TypeMessage;
    masquerOptionsEnvoi?: boolean;
    maxTailleFichiers?: number;
    cartouche?: string;
    signatureAvisPassage?: string;
    nomCompletExpediteur?: string;
    emailExpediteur?: string;
    emailReponseExpediteur?: string;
    emailContactDestinatairePfEmetteur?: string;
    idMsgInitial?: number;
    contenu?: string;
    contenuHTML?: boolean;
    contenuText?: string;
    objet?: string;
    objetText?: string;
    reponseAttendue?: boolean;
    criteres?: CritereDTO[];
    piecesJointes?: PieceJointeDTO[];
    statut?: string | TypeStatutEmail;
    typesInterdits?: TypeMessage[];
    destsPfEmettreur?: DestinataireDTO[];
    destsPfDestinataire?: DestinataireDTO[];
    typeMessageCritere?: any;
}

export interface MessagePlateformeInfosDTO {
    identifiantPfEmetteur?: string;
    identifiantPfDestinataire?: string;
    nomPfEmetteur?: string;
    nomPfDestinataire?: string;
    urlPfEmetteur?: string;
    urlPfDestinataire?: string;
    urlPfEmetteurVisualisation?: string;
    urlPfDestinataireVisualisation?: string;
    urlPfReponse?: string;
    identifiantObjetMetierPlateFormeDestinataire?: string;
    identifiantObjetMetierPlateFormeEmetteur?: string;
}

export interface MessageStatusDTO {
    nombreMessageNonLu?: number;
    nombreMessageNonDelivre?: number;
    nombreMessageEnAttenteReponse?: number;
}

export interface PieceJointeDTO {
    id?: number;
    idExterne?: string;
    reference?: string;
    nom?: string;
    chemin?: string;
    taille?: number;
    dateCreation?: number;
    contentType?: string;
    progress?: number;
    file?: any;
    document?: DocumentContratDTO;
    erreur?: string;
}

export interface SuiviMessageDTO {
    id?: number;
    nomEntreprise?: string;
    nomContact?: string;
    email?: string;
    dateEnvoi?: string;
    dateAR?: string;
    message?: string;
}


export interface ResultatRecherche {
    dateRecherche?: Date | string;
    listeDesMessages?: EmailDTO[];
}

export interface FormulaireRecherche extends AbstractFormulaireRecherche {
    abstractFormulaireRecherche?: AbstractFormulaireRecherche;
}

export interface Criteres {
    critere?: Critere[];
}

export interface CleValeur {
    cle?: string;
    valeur?: string;
}

export interface AbstractFormulaireRecherche {
    parametres?: { [index: string]: string };
    champTri?: string;
    asc?: boolean;
    resultatsParPage?: number;
    page?: number;
    codeParametreResultatsParPage?: string;
    codeParametrePage?: string;
    codeParametreChampTri?: string;
    codeParametreAsc?: string;
    codeFormulaire?: string;
}

export interface Critere {
    nom?: string;
    valeur?: any;
}
