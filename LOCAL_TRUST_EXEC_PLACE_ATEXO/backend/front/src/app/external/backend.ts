import {
    DocumentContratTypeEnum,
    EtatEvenement,
    MotifRejetActeDTO,
    StatutActe,
    TypeAttributaire,
    TypeBorne,
    TypeChampDTO,
    TypeNotification
} from './backendEnum';

export type date = string;

export interface AbstractDTO {
    id?: number;
    uuid?: string;
    idExterne?: string;
}

export interface AdresseDTO {
    adresse?: string;
    adresse2?: string;
    codePostal?: string;
    commune?: string;
    pays?: string;
    codeInseeLocalite?: string;
}

export interface ConsultationDTO extends AbstractDTO {
    numero?: string;
    intitule?: string;
    objet?: string;
    categorie?: ValueLabelDTO;
    procedure?: ValueLabelDTO;
    typeGroupementOperateurs?: ValueLabelDTO;
    modaliteExecutions?: Array<ValueLabelDTO>;
    techniqueAchats?: Array<ValueLabelDTO>;
    typesPrix?: Array<ValueLabelDTO>;
    horsPassation?: boolean;
    numeroProjetAchat?: string;
    decisionAttribution?: Date;
    origineUE?: number;
    origineFrance?: number;

}

export interface ContactReferantDTO extends AbstractDTO {
    contact?: ContactDTO;
    contratEtablissement?: ContratEtablissementDTO;
    roleContrat?: string;
}

export interface ContactPrincipalDTO extends AbstractDTO {
    contact?: ContactDTO;
    contratEtablissement?: ContratEtablissementDTO;
    roleContrat?: string;
}

export interface ContactDTO extends AbstractDTO {
    nom?: string;
    prenom?: string;
    email?: string;
    telephone?: string;
    fonction?: string;
    actif?: boolean;
    etablissement?: EtablissementDTO;
    contratEtablissementReferantList?: ContactReferantDTO[];
}

export interface ContratDTO extends AbstractDTO {
    numero?: string;
    numEj?: string;
    numeroLong?: string;
    referenceLibre?: string;
    statut?: string;
    objet?: string;
    type?: ValueLabelDTO;
    montant?: number;
    montantFacture?: number;
    montantMandate?: number;
    attributionAvance?: boolean;
    tauxAvance?: number;
    dateNotification?: Date;
    dateFinContrat?: Date;
    dateMaxFinContrat?: Date;
    service?: ServiceDTO;
    chapeau?: boolean;
    listBondeCommandes?: EvenementDTO[];
    listAvenantsNonRejete?: EvenementDTO[];
    documentsCount?: number;
    typeFormePrix?: ValueLabelDTO;
    typeBorne?: string | TypeBorne;
    borneMinimale?: number;
    borneMaximale?: number;
    montantTotalAttributaires?: number;
    evenementsEnAttenteCount?: number;
    favori?: boolean;
    offresRecues?: number;
    montantTotalAvenants?: number;
    attributaire?: ContratEtablissementDTO;
    groupement?: ContratEtablissementGroupementDTO;
    dateDemaragePrestation?: Date;
    uniteFonctionnelleOperation?: ValueLabelDTO;
    codeAchat?: ValueLabelDTO;
    intitule?: string;
    lieuxExecution?: ValueLabelDTO[];
    codesCPV?: ValueLabelDTO[];
    ccagApplicable?: ValueLabelDTO;
    achatResponsable?: boolean;
    trancheBudgetaire?: string;
    modaliteRevisionPrix?: string;
    revisionPrix?: ValueLabelDTO;
    defenseOuSecurite?: boolean;
    marcheInnovant?: boolean;
    publicationDonneesEssentielles?: boolean;
    datePrevisionnelleNotification?: Date;
    datePrevisionnelleFinMarche?: Date;
    datePrevisionnelleFinMaximaleMarche?: Date;
    dureeMaximaleMarche?: number;
    dateDebutExecution?: Date;
    organismesEligibles?: OrganismeDTO[];
    servicesEligibles?: ServiceDTO[];
    categorie?: ValueLabelDTO;
    statutRenouvellement?: string;
    dateDebutEtudeRenouvellement?: Date;
    aRenouveler?: boolean;
    dureePhaseEtudeRenouvellement?: number;
    consultation?: ConsultationDTO;
    createur?: UtilisateurDTO;
    donneesEssentielles?: DonneesEssentiellesDTO;
    referenceContratChapeau?: string;
    idContratChapeau?: number;
    created?: boolean;
    exNihilo?: boolean;
    numeroLot?: number;
    messageStatus?: ContratMessageStatusDTO;
    besoinReccurent?: boolean;
    cpvPrincipal?: ValueLabelDTO;
    // champs calculés dans le backend
    notifiable?: boolean;
    clausesDto?: ClauseDto[];
    idLienAcSad?: number;
    natureContratConcession?: ValueLabelDTO;
    coTraitants?: ContratEtablissementDTO[];
    valeurGlobale?: number;
    montantSubventionPublique?: number;
    concession?: boolean;
    considerationsEnvironnementales?: boolean;
    considerationsSociales?: boolean;
    peutEchangerAvecChorus?: boolean;
    statutEJ?: string;
}

export interface ContratEtablissementDTO extends AbstractDTO {
    etablissement?: EtablissementDTO;
    contrat?: ContratDTO;
    contact?: ContactDTO;
    role?: string;
    categorieConsultation?: ValueLabelDTO;
    montant?: number;
    dateNotification?: Date;
    mandataire?: boolean;
    commanditaire?: EtablissementDTO;
    sousTraitants?: ContratEtablissementDTO[];
    dateNotification_R?: Date;
    dateModificationExterne?: Date;
    contacts?: ContactDTO[];
    collapsed?: boolean;
    revisionPrix?: ValueLabelDTO;
    dureeMois?: number;
}

export interface ContratEtablissementGroupementDTO extends AbstractDTO {
    nom?: string;
    type?: ValueLabelDTO;
}

export interface ContratMessageStatusDTO {
    nombreMessageNonLu?: number;
    nombreMessageNonDelivre?: number;
    nombreMessageEnAttenteReponse?: number;
    idContrat?: string;
}

export interface DocumentActeDTO extends AbstractDTO {
    idExterne?: string;
    reference?: string;
    acte?: ActeDTO;
    fichier?: FichierDTO;
    utilisateur?: UtilisateurDTO;
    dateCreation?: Date;
    dateModification?: Date;
}

export interface DocumentContratDTO extends AbstractDTO {
    idExterne?: string;
    contrat?: ContratDTO;
    etablissement?: EtablissementDTO;
    documentType?: string | DocumentContratTypeEnum;
    objet?: string;
    commentaire?: string;
    fichier?: FichierDTO;
    utilisateur?: UtilisateurDTO;
    dateCreation?: Date;
    dateModification?: Date;
    evenements?: EvenementDTO[];
    selected?: boolean;
    preSelected?: boolean;
    statut?: string;
    dateModificationEdition?: Date;
    utilisateursEnLigne?: string[];
}

export interface DocumentMessecDTO extends AbstractDTO {
    base?: string;
    extension?: string;
    fichier?: string;
    nom?: string;
    poids?: number;
    type?: string;
    url_telechargement?: string;
}

export interface EditionContactDTO {
    nom?: string;
    id?: number;
    prenom?: string;
    email?: string;
    telephone?: string;
    fonction?: string;
    etablissement?: EtablissementDTO;
}

export interface ErrorDTO {
    code?: number;
    path?: string;
    rejectedValue?: any;
    message?: string;
    exception?: string;
}

export interface ChorusDTO extends AbstractDTO {
    actif?: boolean;
    visaACCF?: string;
    visaPrefet?: string;
    typeFournisseurEntreprise1?: string;
}

export interface EtablissementDTO extends AbstractDTO {
    siret?: string;
    siege?: boolean;
    adresse?: AdresseDTO;
    fournisseur?: FournisseurDTO;
    contacts?: ContactDTO[];
    nbContacts?: number;
    naf?: string;
    libelleNaf?: string;
    description?: string;
    collapsed?: boolean;
    type?: string;
    raisonSociale?: string;
    uuidFournisseur?: string;
}

export interface AvenantTypeDTO {
    incidence?: boolean;
    avenantTransfert?: boolean;
    dateModifiable?: boolean;
    autreType?: boolean;
    value?: boolean;
    montant?: number;
    numero?: string;
    typeAvenant?: ValueLabelDTO;
    dateDemaragePrestation?: Date;
    dateDemaragePrestationNew?: Date;
    dateFin?: Date;
    dateFinMax?: Date;
    dateFinNew?: Date;
    dateFinMaxNew?: Date;
    cumulPrecedentsAvenants?: number;
    cumulAvenantsCourants?: number;
}

export interface BonCommandeTypeDTO {
    numero?: string;
    montantHT?: number;
    tauxTVA?: ValueLabelDTO;
    montantTVA?: number;
    delaiLivExec?: string;
    programmeNum?: string;
    devisTitulaireNum?: string;
    adresseLivExec?: string;
    delaiPaiement?: string;
    imputationBudgetaire?: string;
    engagementNum?: string;
    adresseFacturation?: string;
}

export interface EvenementDTO extends AbstractDTO {
    dateDebut?: Date;
    dateFin?: Date;
    dateDebutReel?: Date;
    dateFinReel?: Date;
    dateRejet?: Date;
    commentaireEtat?: string;
    suiviRealisation?: boolean;
    etatEvenement?: string | EtatEvenement;
    ponctuel?: boolean;
    contractant?: EtablissementDTO;
    nouveauContractant?: EtablissementDTO;
    typeEvenement?: ValueLabelDTO;
    libelle?: string;
    commentaire?: string;
    alerte?: boolean;
    delaiPreavis?: number;
    destinataires?: ValueLabelDTO[];
    destinatairesLibre?: ValueLabelDTO[];
    envoiAlerte?: boolean;
    documents?: DocumentContratDTO[];
    documentsCount?: number;
    avenantType?: AvenantTypeDTO;
    bonCommandeTypeDTO?: BonCommandeTypeDTO;
    documentIds?: number[];
}

export interface FichierDTO extends AbstractDTO {
    nom?: string;
    taille?: number;
    contentType?: string;
    reference?: string;
    progress?: number;
    erreur?: string;
}

export interface FournisseurDTO extends AbstractDTO {
    siren?: string;
    raisonSociale?: string;
    email?: string;
    formeJuridique?: string;
    taille?: number;
    telephone?: string;
    fax?: string;
    siteInternet?: string;
    etablissements?: EtablissementDTO[];
    capitalSocial?: number;
    description?: string;
    codeApeNafNace?: string;
    pays?: string;
    nom?: string;
    prenom?: string;
    trusted?: boolean;
    montantContratsMandataire?: number;
    montantContratsCotraitants?: number;
    montantContratsSousTraitants?: number;
    nbrContratsMandataire?: number;
    nbrContratsCotraitants?: number;
    nbrContratsSousTraitants?: number;
    nbrContratsClos?: number;
    categorie?: ValueLabelDTO;
}

export interface ModeleDocumentDTO {
    code?: string;
    nom?: string;
}

export interface OrganismeDTO extends AbstractDTO {
    nom?: string;
    acronyme?: string;
    sigle?: string;
    selected?: boolean;
}

export interface PairDTO {
    first?: any;
    second?: any;
}

export interface ServiceDTO extends AbstractDTO {
    nom?: string;
    nomCourt?: string;
    nomCourtArborescence?: string;
    organisme?: OrganismeDTO;
    niveau?: number;
    actif?: boolean;
    idParent?: number;
    echangesChorus?: boolean;
    services?: ServiceDTO[];
    selected?: boolean;
    expanded?: boolean;
}

export interface UtilisateurContext {
    utilisateur?: UtilisateurDTO;
}

export interface UtilisateurDTO extends AbstractDTO {
    identifiant?: string;
    nom?: string;
    prenom?: string;
    email?: string;
    telephone?: string;
    service?: ServiceDTO;
    organismeId?: number;
    serviceId?: number;
    habilitations?: HabilitationDTO[];
    perimetreVisionServices?: number[];
    moduleExecActif?: boolean;
    devise: ValueLabelDTO;
}

export interface HabilitationDTO {
    code?: string;
    libelle?: string;
}

export interface ValueLabelDTO {
    id?: number;
    value?: string;
    label?: string;
    valueAsLong?: number;
    parentLabel?: string;
    principal?: boolean;
    groupeCode?: string;
    parents?: ValueLabelDTO[];
    toolTip?: string;
    limitation?: number;
    entiteEligibleActif?: boolean;
    symbole?: string;
    modeEchangeChorus?: number;

}

export interface AttributaireDTO {
    type?: string | TypeAttributaire;
}

export interface AttributaireEtablissementDTO {
    contratEtablissement?: ContratEtablissementDTO;
    type?: string | TypeAttributaire;
}

export interface AttributaireGroupementDTO {
    groupement?: ContratEtablissementGroupementDTO;
    mandataire?: ContratEtablissementDTO;
    coTraitants?: ContratEtablissementDTO[];
    type?: string | TypeAttributaire;
}

export interface EditionAttributaireDTO {
    idContratEtablissement?: number;
    etablissement?: EtablissementDTO;
    commanditaireId?: number;
    dateNotification?: Date;
    montant?: number;
    categorieConsultation?: ValueLabelDTO;
    revisionPrix?: ValueLabelDTO;
    dureeMois?: number;
}

export interface ReferentielEnum {
    labelKey?: string;
    code?: string;
}

export interface DocumentCriteriaDTO {
    contratId?: number;
    motsCles?: string;
    typeEvenements?: Array<number>;
    evenements?: Array<number>;
    etablissements?: Array<number>;
    contractants?: Array<number>;
    documentTypes?: Array<string>;
}

export interface CreationDocumentGenereModel {
    objet?: string;
    etablissement?: EtablissementDTO;
    evenement?: EvenementDTO;
    modeleDocument?: DocumentModeleDTO;
    fichier?: FichierDTO;
}

export interface CommentaireModel {
    commentaire: string;
}

export interface ContratFournisseurCriteriaDTO {
    motsCles?: string;
    statutProvisoire?: boolean;
    statutEnCours?: boolean;
    statutClos?: boolean;
    statutArchive?: boolean;
    sirets?: string[];
    idFournisseur?: number;
}

export interface ReponseFAQ {
    id?: string;
    token?: string;
}

export interface DonneesEssentiellesDTO {
    id?: number;
    idExterne?: string;
    statut?: string;
    erreurPublication?: string;
    datePublication?: Date;
    idDonneeEssentielle?: number;
    idContrat?: number;
    typeContrat?: string;
    idActe?: number;
    typeActe?: string;
    dateNotificationContrat?: Date;
    dateEnvoi?: Date;
    message?: string;
    fichier?: string;
    dateNotificationActe?: Date;
    modeEnvoiDonneesEssentielles?: string
}

export interface EchangeChorusDTO {
    id?: number;
    acte?: ActeDTO;
    reference?: string;
    statut?: ValueLabelDTO;
    retourChorus?: ValueLabelDTO;
    erreurPublication?: string;
    dateEnvoi?: date;
    datePublication?: date;
    dateNotification?: date;
    dateNotificationPrevisionnelle?: Date;
    organisationAchat?: string;
    groupementAchat?: string;
    agent?: UtilisateurDTO;
}

export interface TitulaireDTO {
    id?: number;
    etablissement?: EtablissementDTO;
    adresse?: string;
    email?: string;
    telephone?: string;
    nom?: string;
}

export interface SousTraitantDTO {
    etablissement?: EtablissementDTO;
    adresse?: string;
    email?: string;
    telephone?: string;
    nom?: string;
    dateDemande?: date;
    dateNotification?: date;
}

export interface CoTraitantDTO {
    coordonnees?: string;
    siret?: string;
    nom?: string;
}

export interface RejetActeDTO {
    motif?: MotifRejetActeDTO;
    commentaire?: string;
}

export interface ActeDTO extends AbstractDTO {
    classname?: string;
    type?: ValueLabelDTO;
    objet?: string;
    version?: number;
    numero?: string;
    statut?: string | StatutActe;
    rejet?: RejetActeDTO;
    acheteur?: UtilisateurDTO;
    contrat?: ContratDTO;
    publicationDonneesEssentielles?: boolean;
    donneesEssentielles?: DonneesEssentiellesDTO;
    echangeChorus?: EchangeChorusDTO;
    chorus?: ChorusDTO;
    validationChorus?: boolean;
    typeNotification?: string | TypeNotification;
    dateCreation?: Date;
    dateModification?: Date;
    dateNotification?: date;
    documents?: DocumentActeDTO[];
    commentaire?: string;
    titulaire?: TitulaireDTO;
}

export interface ChampDTO extends AbstractDTO {
    propriete?: string;
    propriete_multiple?: string;
    libelle?: string;
    infoBulle?: string;
    type?: TypeChampDTO;
    readonly?: boolean;
}

export interface MessagerieContexte {
    token?: string;
    id_template_selectionne?: string;
    id_espace_doc_modal?: string;
    templates_path?: string;
    url_suivi?: string;
    url_retour_consultation?: string;
    url_nouveau_message?: string;
    url_visualisation_message?: string;
}

export interface SpaserContext {
    url?: string;
}

export interface DocumentModeleDTO {
    libelle?: string;
    code?: string;
    produit?: string;
    extension?: string;
}

export interface ClauseDto {
    code?: string;
    valeur?: string;
    parent?: ClauseDto;

}

export interface ReferentielParentDto {
    value?: string;
    parentValue?: string;
}
