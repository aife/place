export enum TypeAttributaire {
    Entreprise, Groupement
}

export enum ContratEtablissementRole {
    ChantierComplet
}

export enum DocumentContratTypeEnum {
    CONTRAT_AUTRES, LIBRE, PIECE_JOINTE_MESSAGE, GENERE
}

export enum PerimetreContrat {
    MOI, SERVICE, SERVICE_ET_SOUS_SERVICES, TOUT
}

export enum StatutContrat {
    ANotifier, Notifie, EnCours, Clos, Archive
}

export enum TailleEntreprise {
    TPE, PME, ETI, GE
}

export enum TypeGroupement {
    groupementSolidaire, groupementConjoint
}

export enum TypeReferentiel {
CATEGORIE = "CategorieConsultation", PAYS = "Pays", CCAG = "CCAGReference", TYPE_CONTRAT = "TypeContrat", FORME_JURIDIQUE = "FormeJuridique",
CPV = "CodeApeNaf", TYPE_AVENANT = "TypeAvenant", TYPE_ACTE = "TypeActe", CODE_EXTERNE_ACTE = "CodeExterneActe", TAUX_TVA = "TauxTVA",
PROCEDURE = "PROCEDURE", MODALITE_EXECUTION = "MODALITE_EXECUTION", TECHNIQUE_ACHAT = "TECHNIQUE_ACHAT", TYPE_GROUPEMENT = "TYPE_GROUPEMENT", FORME_PRIX = "FORME_PRIX",
TYPE_PRIX = "TYPE_PRIX",
REVISION_PRIX = "REVISION_PRIX",
TYPE_EVENEMENT = "TypeEvenement",
SURCHARGE_LIBELLE = "SurchargeLibelle",
CLAUSE_N1 = "ClauseN1",
CLAUSE_N2 = "ClauseN2",
CLAUSE_N3 = "ClauseN3",
CLAUSE_N4 = "ClauseN4",
CLAUSE_N2N3 = "ClauseN2N3",
CODE_APE_NAF = 'CodeApeNaf',
CATEGORIE_FOURNISSEUR = 'CategorieFournisseur',
LIEU_EXECUTION = 'LIEU_EXECUTION',
NATURE_CONTRAT_CONCESSION = 'NatureContratConcession'
}

export enum EtatEvenement {
    VALIDE, REJETE
}

export enum FormePrix {
    UNITAIRE, FORFAITAIRE, MIXTE
}

export enum TypeBorne {
    AUCUNE, MINIMALE, MAXIMALE, MINIMALE_MAXIMALE
}

export enum Habilitation {
    VOIR_TOUS_CONTRATS, MODIF_DONNEES_CONTRAT, CREER_CONTRAT, SUPPRIMER_CONTRAT, VOIR_CONTRATS_SERVICE, VOIR_CONTRATS_SERVICE_ET_SOUS_SERVICES
}

// Messagerie sécurisée

export enum TypeDestinataire {
    DESTINATAIRE_PLATEFORME_EMETTRICE, DESTINATAIRE_PLATEFORME_DESTINATAIRE
}

export enum TypeMessage {
    Unknown, TypeMessage1, TypeMessage2, TypeMessage3, TypeMessage4
}

export enum TypeStatutEmail {
    BROUILLON, EN_ATTENTE_ENVOI, EN_COURS_ENVOI, ECHEC_ENVOI, ENVOYE, EN_ATTENTE_ACQUITTEMENT, ACQUITTE, ECHEC_PARTIEL, ACQUITTEMENT_PARTIEL
}

// Ne provient pas du backend ; utilisé pour augmenter le DTO du backend

export enum EtatDateEvenement {
    A_VENIR, EN_ATTENTE, VALIDE, REJETE
}

export enum StatutActe {
    BROUILLON = 'BROUILLON',
    EN_ATTENTE_ACCUSE = 'EN_ATTENTE_ACCUSE',
    EN_ATTENTE_VALIDATION = 'EN_ATTENTE_VALIDATION',
    VALIDE = 'VALIDE',
    EN_ERREUR = 'EN_ERREUR',
    NOTIFIE= 'NOTIFIE',
    ANNULE = 'ANNULE'
}

export enum TypeNotification {
    MESSAGERIE_SECURISEE,
    MANUELLE
}

export enum MotifRejetActeDTO {
    ERREUR_SAISIE = 'ERREUR_SAISIE',
    ABSENCE_ACCUSE_ENTREPRISE = 'ABSENCE_ACCUSE_ENTREPRISE',
    REFUS_CHORUS = 'REFUS_CHORUS',
    AUTRE = 'AUTRE'
}

export enum TypeChampDTO {
    TEXTE = 'TEXTE', BOOLEN = 'BOOLEEN', DATE = 'DATE', MULTIPLE = 'MULTIPLE'
}

export enum  StatutPublicationDE {
    EN_COURS='EN_COURS',A_PUBLIER='A_PUBLIER',PUBLIE='PUBLIE',ERROR='ERROR',NON_PUBLIE='NON_PUBLIE'
}
