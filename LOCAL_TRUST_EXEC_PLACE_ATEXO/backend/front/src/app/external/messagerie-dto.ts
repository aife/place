import {DropzoneFile} from "dropzone";

export interface SerializableRechercheMessage {
    token?: string;
    typePlateformeRecherche?: string | TypeDestinataire;
    idPlateformeRecherche?: string;
    idObjetMetier?: string;
    idSousObjetMetier?: string;
    idContactDestList?: number[];
    idEntrepriseDest?: string;
    mailDestinataire?: string;
    refObjetMetier?: string;
    objetMessage?: string;
    nomOuMailDest?: string;
    nomOuMailExp?: string;
    dateEnvoiDebut?: Date | string;
    dateEnvoiFin?: Date | string;
    dateARDebut?: Date;
    dateARFin?: Date;
    ids?: any;
    statuts?: TypeStatutEmail[];
    criteres?: Criteres;
    reponseAttendue?: boolean;
    typeMessage?: string;
    dateDemandeEnvoiDebut?: Date | string;
    dateDemandeEnvoiFin?: Date | string;
    pieceJointe?: string;
    pieceJointeNom?: string;
    idMessageDestinataire?: string;
    nomOuMailDestList?: string[];
    motsCles?: string;
    enAttenteReponse?: boolean;
    reponseAttendu?: boolean;
    reponseNonLue?: boolean;
    reponseLue?: boolean;
    inclureReponses?: boolean;
    exclureReponses?: boolean;
    favoris?: boolean;

}

export interface CritereDTO {
    id?: number;
    nom?: string;
    valeur?: string;
}

export interface DestinataireDTO {
    typeDestinataire?: string | TypeDestinataire;
    idEntrepriseDest?: string;
    idContactDest?: number;
    nomEntrepriseDest?: string;
    nomContactDest?: string;
    mailContactDestinataire?: string;
    type?: string;
    disabled?: boolean;
    ordre?: number;
}

export interface EmailDTO {
    id?: number;
    typeDestinataire?: string | TypeDestinataire;
    identifiantObjetMetier?: string;
    identifiantEntreprise?: string;
    identifiantContact?: number;
    nomEntreprise?: string;
    nomContact?: string;
    email?: string;
    statutEmail?: string | TypeStatutEmail;
    codeLien?: string;
    dateDemandeEnvoi?: number;
    dateEnvoi?: number;
    dateAR?: number;
    nombreEssais?: number;
    erreur?: string;
    identifiantEntrepriseAR?: string;
    identifiantContactAR?: string;
    nomEntrepriseAR?: string;
    nomContactAR?: string;
    emailContactAR?: string;
    message?: MessageDTO;
    dateDemandeEnvoiAsDate?: Date;
    dateEnvoiAsDate?: Date;
    dateARAsDate?: Date;
    reponse?: EmailDTO;
    dateModificationBrouillonAsDate?: Date;
    toutesLesReponses: Array<EmailDTO>;
    template?: Template;
    connecte?: boolean;
    dateRelance?: Date;
    dateEchec?: Date;
    favori?: boolean;
    reponseMasquee?: boolean;
}

export interface MessageDTO {
    id?: number;
    typeMessage?: string | TypeMessage;
    cartouche?: string;
    contenu?: string;
    contenuHTML?: boolean;
    objet?: string;
    reponseAttendue?: boolean;
    criteres?: CritereDTO[];
    dateModification?: Date;
    piecesJointes?: PieceJointeDTO[];
    statut?: string | TypeStatutEmail;
    destsPfEmettreur?: DestinataireDTO[];
    destsPfDestinataire?: DestinataireDTO[];
    destinatairesPreSelectionnes?: DestinataireDTO[];
    templateCritere?: string;
    emailsAlerteReponse?: string;
    dateReponseAttendue?: boolean;
    dateNow?: Date;
    dateLimiteReponseAsDate?: Date;
    dateLimiteReponseAttendue?: boolean;
    reponseBloque?: boolean;
    reponseLectureBloque?: boolean;
    nbDestinataires?: number;
    dateReponseAsDate?: Date;
    destinatairesNotifies?: DestinataireDTO[];
    maxTailleFichiers?: number;
    templateMail?: string;
    templateFige?: boolean;
    nomCompletExpediteur?: string;
    limitationTailleFichier?: any;
    codeLien?: string;
    brouillon?: boolean;
    dossiersVolumineux?: DossierVolumineuxDTO[];
    dossierVolumineux?: DossierVolumineuxDTO;
}

export interface PieceJointeDTO {
    id?: string;
    idExterne?: string;
    reference?: string | Object;
    nom?: string;
    chemin?: string;
    taille?: number;
    dateCreation?: number;
    contentType?: string;
    file?: DropzoneFile;
    progress?: number;
    erreur?: string;
}

export interface DocumentDTO {
    base?: string;
    extension?: string;
    fichier?: string;
    id?: string
    nom?: string;
    poids?: number;
    type?: string;
    url_telechargement?: string;
}

export enum TypeDestinataire {
    DESTINATAIRE_PLATEFORME_EMETTRICE, DESTINATAIRE_PLATEFORME_DESTINATAIRE
}

export enum TypeMessage {
    Unknown, TypeMessage1, TypeMessage2, TypeMessage3, TypeMessage4
}

export enum TypeStatutEmail {
    BROUILLON, EN_ATTENTE_ENVOI, EN_COURS_ENVOI, ECHEC_ENVOI, ENVOYE, EN_ATTENTE_ACQUITTEMENT, ACQUITTE, ECHEC_PARTIEL, ACQUITTEMENT_PARTIEL
}


export interface Criteres {
    critere?: Critere[];
}

export interface Critere {
    nom?: string;
    valeur?: string;
}

export class PaginationPage<T> {
    content?: Array<T>;
    last?: boolean;
    first?: boolean;
    number: number;
    size: number;
    totalPages?: number;
    totalElements?: number;
    sort?: Array<PaginationPropertySort>;
}

export interface PaginationPropertySort {
    direction: string;
    property: string;
}

// templates
export interface MailType {
    label?: string;
    code?: string;
}

export interface Template {
    id: number;
    mailType?: MailType;
    code?: string;
    objet?: string;
    corps?: string;
    ordreAffichage: number;
    envoiModalite?: string;
    envoiModaliteFigee?: boolean;
    reponseAttendue?: boolean;
    reponseAttendueFigee?: boolean;
}

export interface ContexteDTO {
    token?: string;
    url_suivi?: string;
    url_retour_consultation?: string;
    url_nouveau_message?: string;
    url_modification_message?: string;
    id_template_selectionne?: string;
    url_visualisation_message?: string;
    url_callback_reponse_a_question?: string;
    id_espace_doc_modal?: string;
    templates_path?: string;
}

export interface PageChangeEvent {
    currentPage: number;
    pageSize: number;
}

export interface Evenement {
    nom: string;
    date: Date;
}

export interface DossierVolumineuxDTO {
    id?: number;
    dateCreationAsDate?: Date;
    nom?: string;
    taille?: number;
    uuidReference?: string;
    uuidTechnique?: string;
}
