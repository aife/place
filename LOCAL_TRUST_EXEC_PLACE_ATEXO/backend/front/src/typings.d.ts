/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
    id: string;
}

/**
 * Plugin bootstrap-wisywyg
 * Pas de package @types disponible pour l'instant
 */
interface JQuery {
    wysiwyg(settings?: any): JQuery;
}
