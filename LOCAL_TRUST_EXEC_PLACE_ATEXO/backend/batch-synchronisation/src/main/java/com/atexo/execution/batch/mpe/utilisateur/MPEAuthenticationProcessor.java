package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.mpe.ws.api.AgentType;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.model.security.Account;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import com.atexo.execution.server.repository.security.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class MPEAuthenticationProcessor extends MPEAbstractProcessor implements ItemProcessor<AgentType, AgentType> {

	final static Logger LOG = LoggerFactory.getLogger(MPEAuthenticationProcessor.class);

	@Autowired
	UtilisateurRepository utilisateurRepository;

    @Autowired
    AccountRepository accountRepository;

	@Autowired
	PlateformeRepository plateformeRepository;

	@Override
	public AgentType process(AgentType agentType) throws Exception {
		LOG.info("Plateforme {} , Création ou modification de l'utilisateur OAUTH {}", mpePfUid, agentType.getIdentifiant());
		var account = accountRepository.findFirstByUsernameAndPlateformeMpeUidOrderByIdDesc(agentType.getIdentifiant(), mpePfUid).orElseGet(Account::new);
		account.setUuid(BeanSynchroUtils.buildIdExterne(mpePfUid, agentType.getId()));
		account.setUsername(agentType.getIdentifiant());
		account.setPassword(null);
		account.setAuthorities(null);
		account.setAccountExpired(false);
		account.setAccountLocked(false);
		account.setCredentialsExpired(true);
		account.setEnabled(false);
		account.setIdExterne(agentType.getIdentifiant());
		plateformeRepository.findByMpeUid(mpePfUid).ifPresent(account::setPlateforme);

        accountRepository.save(account);

		return agentType;
	}
}
