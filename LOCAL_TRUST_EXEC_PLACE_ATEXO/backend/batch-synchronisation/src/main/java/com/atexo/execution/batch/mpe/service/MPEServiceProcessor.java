package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.mpe.ws.api.ServiceType;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@StepScope
@Slf4j
public class MPEServiceProcessor extends MPEAbstractProcessor implements ItemProcessor<ServiceType, Service> {

	@Autowired
	OrganismeRepository organismeRepository;

	@Autowired
	ServiceRepository serviceRepository;

	@Override
	public Service process(ServiceType serviceType) {
		var serviceId = 0 != serviceType.getIdSynchroExec() ? serviceType.getIdSynchroExec() : serviceType.getId();

		String idExterne = BeanSynchroUtils.buildIdExterne(serviceType.getAcronymeOrganisme(), String.valueOf(serviceId));
		// y compris le cas des services racine car la valeur par defaut d'un int est 0
		String idExterneParent = BeanSynchroUtils.buildIdExterne(serviceType.getAcronymeOrganisme(), String.valueOf(serviceType.getIdParent()));
		var organisme = organismeRepository.findByIdExterneAndPlateformeMpeUid(serviceType.getAcronymeOrganisme(), mpePfUid).orElse(null);
		var parent = Optional.ofNullable(idExterneParent).map(id->serviceRepository.findByIdExterneAndPlateformeMpeUid(id, mpePfUid)).orElse(null).orElse(null);
        if(parent == null && idExterneParent != null){
			parent = new Service();
			parent.setIdExterne(idExterneParent);
		}
		if(parent !=null){
			parent.setPlateforme(getPlateforme());
			parent.setOrganisme(organisme);
			parent = serviceRepository.save(parent);
		}

		Service service = serviceRepository.findByIdExterneAndPlateformeMpeUid(idExterne, mpePfUid).orElse(new Service());
		service.setPlateforme(getPlateforme());
		service.setOrganisme(organisme);
		service.setIdExterne(idExterne);
		service.setSiren(serviceType.getSiren());
		service.setComplement(serviceType.getComplement());
		service.setRaisonSociale(serviceType.getLibelle());
		service.setSigle(serviceType.getSigle());
		// pré-enregistrement
		service = serviceRepository.save(service);
        organismeRepository.findByIdExterneAndPlateformeMpeUid(serviceType.getAcronymeOrganisme(), mpePfUid).ifPresent(service::setOrganisme);
		service.setNom(serviceType.getLibelle());
		service.setNomCourt(serviceType.getSigle());
		service.setDateCreation(MapperUtils.convertToLocalDateTime(serviceType.getDateCreation()));
		LocalDateTime dateModification = MapperUtils.convertToLocalDateTime(serviceType.getDateModification());
		if (dateModification == null) {
			dateModification = LocalDateTime.now();
		}
		if (service.getDateModification() == null)
			service.setDateModification(dateModification);
		service.setDateModificationExterne(dateModification);
		service.setParent(parent);
		if (serviceType.getAccesChorus() != null) {
			service.setEchangesChorus("1".equals(serviceType.getAccesChorus()));
		}

		var oldServiceId = 0 != serviceType.getIdSynchroExec() && serviceType.getIdSynchroExec() != serviceType.getId() ? serviceType.getId() : null;
		if (null != oldServiceId) {
			service.setOldIdExterne(BeanSynchroUtils.buildIdExterne(serviceType.getAcronymeOrganisme(), String.valueOf(oldServiceId)));

		}

		return service;
	}
}
