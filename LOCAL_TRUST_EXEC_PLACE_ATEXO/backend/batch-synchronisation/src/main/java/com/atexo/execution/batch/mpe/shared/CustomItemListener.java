package com.atexo.execution.batch.mpe.shared;

import org.springframework.stereotype.Component;

@Component
public interface CustomItemListener {

	String getTaskname();
}
