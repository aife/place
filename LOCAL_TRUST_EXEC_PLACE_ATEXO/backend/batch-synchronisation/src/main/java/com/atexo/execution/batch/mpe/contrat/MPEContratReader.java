package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.RestPaginatedReader;
import com.atexo.execution.common.mpe.ws.api.ContratType;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@StepScope
@Slf4j
public class MPEContratReader extends RestPaginatedReader<ContratType> {

    @Autowired
    MpeClient mpeApiInterface;
    public static long totalItems = 0;
    public static long lastItems = 0;

    public MPEContratReader() {
        super(SynchronisationExterneDate.TypeModification.CONTRAT_ETABLISSEMENT);
    }

    @Override
    public List<ContratType> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        List<ContratType> contrats = mpeApiInterface.getContrats(plateformeUid, page, nbItems, lastSynchronizationDate);
        int size = contrats.size();
        log.info("Fetched {} contrats from MPE", size);
        if (!contrats.isEmpty()) {
            lastItems = size;
            totalItems += size;
            log.info("Total contrats fetched from MPE: {}", totalItems);
        } else {
            totalItems += size;
            log.info("finished fetching contrats from MPE: {}/{}", totalItems, ((long) (page - 2) * nbItems) + lastItems);
        }
        return contrats;
    }

}

