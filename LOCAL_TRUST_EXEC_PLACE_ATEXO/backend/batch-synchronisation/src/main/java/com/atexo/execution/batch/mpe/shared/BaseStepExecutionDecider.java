package com.atexo.execution.batch.mpe.shared;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public abstract class BaseStepExecutionDecider implements JobExecutionDecider {

	private static final Logger LOG = LoggerFactory.getLogger(BaseStepExecutionDecider.class);

	public static final String A_EXECUTER = "YES";

	public static final String NE_PAS_EXECUTER = "NO";

	@Value("${mpe.synchro:fournisseurs,etablissements,contacts,contrats,acquittement-avenants,echanges-chorus,organismes,services,utilisateurs}")
	protected List<String> synchros = new ArrayList<>();

	@Override
	public FlowExecutionStatus decide( JobExecution jobExecution, StepExecution stepExecution ) {
		FlowExecutionStatus result;

		List<String> synchroActives;

		var parameters = jobExecution.getJobParameters().getString("mpe.synchro");

		if (null!=parameters) {
			synchroActives = Arrays.stream(parameters.split(",")).collect(Collectors.toList());
		} else {
			synchroActives = synchros;
		}

		if (synchroActives.contains(this.getStepname())) {
			LOG.info("Le step '{}' va être executé", this.getStepname());
			result = new FlowExecutionStatus(A_EXECUTER);
		} else {
			LOG.info("Le step '{}' NE DOIT PAS ETRE executé", this.getStepname());

			result = new FlowExecutionStatus(NE_PAS_EXECUTER);
		}

		return result;
	}

	public abstract String getStepname();
}
