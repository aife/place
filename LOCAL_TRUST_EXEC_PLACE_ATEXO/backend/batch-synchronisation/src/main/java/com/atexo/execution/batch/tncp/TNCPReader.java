package com.atexo.execution.batch.tncp;

import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.ContratRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class TNCPReader {

    public static final String TNCP_REPO_ITEM_READER = "tncpRepositoryItemReader";
    public static final String FIND_CONTRAT_TO_PUBLISH = "findByContratToPublish";
    public static final String ID = "id";
    private final ContratRepository contratRepository;
    @Value("${data.gouv.page.size:50}")
    private int pageSize;

    @Bean
    public RepositoryItemReader<Contrat> tncpRepositoryItemReader() {
        return new RepositoryItemReaderBuilder<Contrat>()
                .name(TNCP_REPO_ITEM_READER)
                .repository(this.contratRepository)
                .methodName(FIND_CONTRAT_TO_PUBLISH)
                .arguments(StatutPublicationDE.A_PUBLIER, StatutContrat.ANotifier, ModeEnvoiDonneesEssentielles.VIA_DUME)
                .sorts(Map.of(ID, Sort.Direction.ASC))
                .pageSize(pageSize)
                .saveState(false)
                .build();
    }
}
