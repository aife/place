package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.mpe.ws.api.OrganismeType;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@StepScope
@Slf4j
public class MPEOrganismeProcessor extends MPEAbstractProcessor implements ItemProcessor<OrganismeType, Organisme> {

    @Autowired
    OrganismeRepository organismeRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Override
    public Organisme process(OrganismeType organismeType) throws Exception {
        log.info("Plateforme {} , Création ou modification de l'organisme {}", mpePfUid, organismeType.getAcronyme());
        Organisme organisme = organismeRepository.findByIdExterneAndPlateformeMpeUid(organismeType.getAcronyme(), mpePfUid).orElse(new Organisme());
        String acronyme = organismeType.getAcronyme();
        String identifiantSynchroRacine = acronyme + "_0";
        var serviceRacine = serviceRepository.findByIdExterneAndPlateformeMpeUid(identifiantSynchroRacine, mpePfUid).orElse(new Service());
        organisme.setAcronyme(acronyme.toUpperCase());
        organisme.setIdExterne(acronyme);
        serviceRacine.setIdExterne(identifiantSynchroRacine);
        serviceRacine.setNiveau(0);
        organisme.setServiceRacine(serviceRacine);
        organisme.setPlateforme(getPlateforme());
        serviceRacine.setPlateforme(getPlateforme());
        organisme.setNom(organismeType.getDenomination());
        organisme.setSiren(organismeType.getSiren());
        organisme.setComplement(organismeType.getNic());
        organisme.setIdMpe(organismeType.getId());
        if (null != serviceRacine) {
            serviceRacine.setNomCourt(organismeType.getSigle());
            serviceRacine.setNom(organismeType.getDenomination());
            serviceRacine.setOrganisme(organisme);
            serviceRacine.setPlateforme(getPlateforme());
        }
        return organisme;
    }
}
