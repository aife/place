package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.shared.BaseItemProcessorListener;
import com.atexo.execution.common.mpe.ws.api.AgentType;
import com.atexo.execution.server.model.Utilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class UtilisateurProcessorListener extends BaseItemProcessorListener<AgentType, Utilisateur> {

	private static final Logger LOG = LoggerFactory.getLogger(UtilisateurProcessorListener.class);

	@Override
	public void beforeProcess( AgentType from ) {
		super.beforeProcess(from);

		LOG.trace("{} : traitement de l'utilisateur avec identifiant externe (identifiant MPE) = {}", this.getTaskname(), from.getId());
	}

	@Override
	public void afterProcess( AgentType from, Utilisateur to ) {
		super.afterProcess(from, to);

		LOG.debug("{} : utilisateur avec identifiant externe {} intégré en {}ms", this.getTaskname(), from.getId(), watch.getTotalTimeMillis());
	}

	@Override
	public String getTaskname() {
		return "[synchro des utilisateurs] traitement des données";
	}
}
