package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.shared.BaseItemReaderListener;
import com.atexo.execution.common.mpe.ws.api.OrganismeType;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class OrganismeReaderListener extends BaseItemReaderListener<OrganismeType> {

	@Override
	public String getTaskname() {
		return "[synchro des organismes] récupération des données via API MPE";
	}
}
