package com.atexo.execution.batch.conf;

import fr.atexo.execution.tncp.envoi.TNCPEnvoi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@ConditionalOnProperty(value = "tncp.enabled")
@Configuration
@EnableKafka
@RequiredArgsConstructor
@Slf4j
public class KafkaConfig {

    @Value(value = "${tncp.kafka.broker}")
    private String bootstrapAddress;

    private final KafkaSecurityConf kafkaSecurityConf;
    @Value(value = "${tncp.kafka.security.enabled}")
    private boolean securityEnabled;

    @Bean
    public ProducerFactory<String, TNCPEnvoi> producerFactory() {
        var configProps = new HashMap<String, Object>();
        if (securityEnabled) {
            log.info("Kafka Security is enabled");
            configProps.putAll(kafkaSecurityConf.getKafkaSecurityProperties());
            log.info("Kafka Security Properties pour le producteur: {}", configProps);
        }
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public KafkaTemplate<String, TNCPEnvoi> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

}
