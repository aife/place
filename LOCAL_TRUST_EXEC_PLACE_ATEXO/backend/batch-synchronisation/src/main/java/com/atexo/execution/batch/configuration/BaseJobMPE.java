package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.batch.core.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public abstract class BaseJobMPE {

	@Autowired
	private Set<Step> steps;
	@Autowired
	private Set<BaseStepExecutionDecider> deciders;

    @Value("${mpe.skip.limit:100}")
    int skipLimit = 100;

	/**
	 * Permet de récuperer un step depuis son nom
	 * @param nom le nom du step
	 * @return le step
	 */
	protected Step recupererStep( String nom ) {
		return steps.stream().filter(step -> step.getName().equals(nom)).findFirst().orElseThrow(() -> new IllegalArgumentException("Le step '" + nom + "' n'existe pas. Veuillez vérifier la configuration du job"));
	}

	/**
	 * Permet de récuperer un decider depuis le step
	 * @param nom le nom du step
	 * @return le step
	 */
	protected BaseStepExecutionDecider recupererDecider( String nom ) {
		return deciders.stream().filter(decider -> decider.getStepname().equals(nom)).findFirst().orElseThrow(() -> new IllegalArgumentException("Le decider pour le step '" + nom + "' n'existe pas. Veuillez vérifier la configuration du job"));
	}
}
