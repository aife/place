package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.RestPaginatedReader;
import com.atexo.execution.common.mpe.ws.api.ServiceType;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@StepScope
public class MPEServiceReader extends RestPaginatedReader<ServiceType> {

	@Autowired
	MpeClient mpeApiInterface;

	public MPEServiceReader() {
		super(SynchronisationExterneDate.TypeModification.SERVICE);
	}

	@Override
	public List<ServiceType> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        return mpeApiInterface.getServices(plateformeUid, page, nbItems, lastSynchronizationDate);
	}


}
