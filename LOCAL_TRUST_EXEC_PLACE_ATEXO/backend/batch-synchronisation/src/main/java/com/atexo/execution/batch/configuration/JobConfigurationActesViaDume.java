package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.dume.ActeProcessor;
import com.atexo.execution.batch.dume.ActeWriter;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.actes.ActeModificatif;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JobConfigurationActesViaDume {

    private final StepBuilderFactory stepBuilderFactory;
    private final RepositoryItemReader<ActeModificatif> acteRepositoryItemReader;
    private final ActeProcessor acteProcessor;
    private final ActeWriter acteWriter;
    @Value("${chunk.size:100}")
    int chunkSize;

    Step dataGouvStep() {
        return stepBuilderFactory.get("ACTES-VIA-DUME")
                .<ActeModificatif, DonneesEssentielles>chunk(chunkSize)
                .reader(acteRepositoryItemReader)
                .processor(acteProcessor)
                .writer(acteWriter)
                .faultTolerant()
                .skip(Exception.class)
                .build();
    }

    @Bean
    public Job actesViaDumeJob(JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory.get("Envoi des données essentielles des actes VIA DUME")
                .incrementer(new RunIdIncrementer())
                .flow(dataGouvStep()).end().build();

    }
}
