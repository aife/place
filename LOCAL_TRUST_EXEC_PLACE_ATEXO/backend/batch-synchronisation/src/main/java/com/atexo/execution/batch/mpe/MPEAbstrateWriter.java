package com.atexo.execution.batch.mpe;

import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.crud.SynchronisationExterneDateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.util.Collection;

public class MPEAbstrateWriter {

    private static final Logger LOG = LoggerFactory.getLogger(MPEAbstrateWriter.class);

    @Value("#{jobParameters['plateformeUid']}")
    public String plateformeUid;

    @Autowired
    SynchronisationExterneDateRepository synchronisationExterneDateRepository;

    @Autowired
    PlateformeRepository plateformeRepository;

    private final SynchronisationExterneDate.TypeModification typeModification;

    public MPEAbstrateWriter() {
        throw new IllegalArgumentException("ce constructeur n'est pas autorisé");
    }

    public MPEAbstrateWriter(SynchronisationExterneDate.TypeModification typeModification) {
        this.typeModification = typeModification;
    }

    public void updateSynchronizationDate(Collection elements) {
        if (elements != null && !elements.isEmpty()) {
            var plateforme = plateformeRepository.findByMpeUid(plateformeUid).orElseThrow(() -> new IllegalArgumentException("plateforme non trouvée"));
            SynchronisationExterneDate synchronisationExterneDate = synchronisationExterneDateRepository.findByTypeModificationAndPlateformeMpeUid(typeModification, plateformeUid).orElse(new SynchronisationExterneDate());
            synchronisationExterneDate.setPlateforme(plateforme);
            synchronisationExterneDate.setTypeModification(typeModification);
            synchronisationExterneDate.setDateDerniereModification(LocalDateTime.now());
            synchronisationExterneDateRepository.save(synchronisationExterneDate);
        } else {
            LOG.info("aucun élément synchronisé pour {}", typeModification);
        }

    }
}
