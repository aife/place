package com.atexo.execution.batch.mpe;

import com.atexo.execution.server.model.Plateforme;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public abstract class MPEAbstractProcessor {

    @Value("#{jobParameters['plateformeUid']}")
    public String mpePfUid;

    @Autowired
    private PlateformeRepository plateformeRepository;

    public Plateforme getPlateforme() {
        return plateformeRepository.findByMpeUid(mpePfUid).orElse(null);
    }

}
