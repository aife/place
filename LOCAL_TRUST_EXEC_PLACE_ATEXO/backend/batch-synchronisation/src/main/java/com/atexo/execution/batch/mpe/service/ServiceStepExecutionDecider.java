package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class ServiceStepExecutionDecider extends BaseStepExecutionDecider {

	@Override
	public String getStepname() {
		return "services";
	}
}
