package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.mpe.echangesChorus.*;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.model.EchangeChorus;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JobConfigurationEchangeChorus {

    @Value("${mpe.skip.limit:100}")
    int skipLimit = 100;

    private final StepBuilderFactory stepBuilderFactory;
    private final MPEChorusReader mpeChorusReader;
    private final MPEChorusProcessor mpeChorusProcessor;
    private final MPEChorusWriter mpeChorusWriter;
    private final ChorusReaderListener chorusReaderListener;
    private final ChorusProcessorListener chorusProcessorListener;

    Step mpeEchangesChorus() {
        return stepBuilderFactory.get("echanges-chorus")
                .<EchangeChorusType, EchangeChorus>chunk(1)
                .reader(mpeChorusReader)
                .processor(mpeChorusProcessor)
                .writer(mpeChorusWriter)
                .listener(chorusReaderListener)
                .listener(chorusProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    public Job echangeChorusJob(JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory.get("Synchronisation des échanges chorus")
                .incrementer(new RunIdIncrementer())
                .flow(mpeEchangesChorus()).end().build();

    }
}
