package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class ContratStepExecutionDecider extends BaseStepExecutionDecider {

	@Override
	public String getStepname() {
		return "contrats";
	}
}
