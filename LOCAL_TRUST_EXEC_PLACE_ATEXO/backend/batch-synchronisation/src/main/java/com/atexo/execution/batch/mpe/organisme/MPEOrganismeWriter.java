package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.MPEAbstrateWriter;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@StepScope
public class MPEOrganismeWriter extends MPEAbstrateWriter implements ItemWriter<Organisme> {

	@Autowired
	OrganismeRepository organismeRepository;

	public MPEOrganismeWriter() {
		super(SynchronisationExterneDate.TypeModification.ORGANISME);
	}

	@Override
	public void write(List<? extends Organisme> list) throws Exception {
		organismeRepository.saveAll(list);
		updateSynchronizationDate(list);
	}

}
