package com.atexo.execution.batch.data.gouv;

import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.services.DataGouvService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataGouvProcessorListenner implements ItemProcessListener<Organisme, DataGouv> {

    public static final String FILE = "tmpFile";
    private final DataGouvService dataGouvService;
    private final ContratRepository contratRepository;

    @Override
    public void beforeProcess(Organisme organisme) {
        log.info("Début DataGouvProcessor pour l'organisme, id={}, nom={}", organisme.getId(), organisme.getNom());
    }

    @Override
    public void afterProcess(Organisme organisme, DataGouv dataGouv) {
        log.info("Fin DataGouvProcessor pour l'organisme, id={}, nom={}", organisme.getId(), organisme.getNom());
    }

    @Override
    public void onProcessError(Organisme organisme, Exception e) {
        log.error("erreur au niveau DataGouvProcessor pour l'organisme, id={}, nom={}", organisme.getId(), organisme.getNom(), e);
        dataGouvService.miseAjourContratPublication(contratRepository.findContratByOrganisme(organisme, StatutPublicationDE.A_PUBLIER, StatutContrat.Notifie)
                , true
                , false
                , e.getMessage()
                , StatutPublicationDE.NON_PUBLIE
                , LocalDateTime.now()
                , StatutAvenant.ERREUR_PUBLICATION, null);
        StepExecution stepExecution = Objects.requireNonNull(StepSynchronizationManager.getContext()).getStepExecution();
        Optional.ofNullable((String) stepExecution.getExecutionContext().get(FILE))
                .map(Path::of)
                .ifPresent(dataGouvService::supprimerFichier);
    }
}
