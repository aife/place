package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.RestPaginatedReader;
import com.atexo.execution.common.mpe.ws.api.OrganismeType;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@StepScope
public class MPEOrganismeReader extends RestPaginatedReader<OrganismeType> {

	@Autowired
	MpeClient mpeApiInterface;

	public MPEOrganismeReader() {
		super(SynchronisationExterneDate.TypeModification.ORGANISME);
	}

	@Override
	public List<OrganismeType> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        return mpeApiInterface.getOrganimes(plateformeUid, page, nbItems, lastSynchronizationDate);
	}

}
