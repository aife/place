package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class UtilisateurStepExecutionDecider extends BaseStepExecutionDecider {

	@Override
	public String getStepname() {
		return "utilisateurs";
	}
}
