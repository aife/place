package com.atexo.execution.batch.argument;

import com.beust.jcommander.Parameter;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public class Args {

    @Parameter(names = {"--synchro", "-s"}, description = "Lancement du job de synchronisation")
    private boolean synchro;
    @Parameter(names = {"--synchro-update", "-su"}, description = "Lancement du job de synchronisation")
    private boolean synchroUpdate;

    @Parameter(names = {"--data-gouv", "-dg"}, description = "Lancement du job DataGouv")
    private boolean dataGouv;

    @Parameter(names = {"--tncp", "-tncp"}, description = "Lancement du job TNCP")
    private boolean tncp;

    @Parameter(names = {"--echange-chorus", "-ec"}, description = "Lancement du job EchangeChorus")
    private boolean echangeChorus;

    @Parameter(names = {"--actes-via-dume", "-actes-via-dume"}, description = "Lancement du job EchangeChorus")
    private boolean viaDume;

    public static String[] filterSpringBootArgs(String[] args) {
        // Filtrer les arguments
        List<String> allowedPrefixes = List.of("--data-gouv", "-dg", "--synchro", "-s", "--tncp", "-tncp", "--synchro-update", "-su", "--echange-chorus", "-ec", "--actes-via-dume", "-actes-via-dume");
        return Arrays.stream(args)
                .filter(arg -> allowedPrefixes.stream().anyMatch(arg::startsWith))
                .toArray(String[]::new);
    }


}
