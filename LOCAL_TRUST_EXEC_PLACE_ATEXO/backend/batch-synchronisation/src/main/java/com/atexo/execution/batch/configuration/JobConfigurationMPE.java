package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.listeners.StopWatchStepListener;
import com.atexo.execution.batch.listeners.SynchronisationJobListener;
import com.atexo.execution.batch.mpe.contrat.*;
import com.atexo.execution.batch.mpe.echangesChorus.*;
import com.atexo.execution.batch.mpe.organisme.*;
import com.atexo.execution.batch.mpe.service.*;
import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import com.atexo.execution.batch.mpe.utilisateur.*;
import com.atexo.execution.common.mpe.ws.api.*;
import com.atexo.execution.server.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class JobConfigurationMPE extends BaseJobMPE {

    static final Logger LOG = LoggerFactory.getLogger(JobConfigurationMPE.class);

    @Value("${mpe.retry.limit:10}")
    int retryLimit = 10;


    @Value("${mpe.synchro}")
    protected List<String> synchros = new ArrayList<>();

    @PostConstruct
    public void echo() {
        LOG.info("Liste des synchros actives : {}", synchros);
    }


    @Bean(name = "synchronisationJob")
    public Job synchronizationJob(JobBuilderFactory jobBuilderFactory, SynchronisationJobListener synchronisationJobListener) {
        var jobBuilder = jobBuilderFactory.get("Synchronisation MPE")
                .incrementer(new RunIdIncrementer())
                .listener(synchronisationJobListener);

        var flowUtilisateurs = new FlowBuilder<Flow>("flow-utilisateurs")
                // Utilisateurs
                .start(recupererDecider("utilisateurs"))
                .on(BaseStepExecutionDecider.A_EXECUTER)
                .to(recupererStep("utilisateurs"))
                .from(recupererDecider("utilisateurs"))
                .on(BaseStepExecutionDecider.NE_PAS_EXECUTER)
                .end()
                .build();

        var flowServices = new FlowBuilder<Flow>("flow-services")
                // Services
                .start(recupererDecider("services"))
                .on(BaseStepExecutionDecider.A_EXECUTER)
                .to(recupererStep("services"))
                .next(flowUtilisateurs)
                .from(recupererDecider("services"))
                .on(BaseStepExecutionDecider.NE_PAS_EXECUTER)
                .to(flowUtilisateurs)
                .build();

        var flowOrganisme = new FlowBuilder<Flow>("flow-organismes")
                // Organismes
                .start(recupererDecider("organismes"))
                .on(BaseStepExecutionDecider.A_EXECUTER)
                .to(recupererStep("organismes"))
                .next(flowServices)
                .from(recupererDecider("organismes"))
                .on(BaseStepExecutionDecider.NE_PAS_EXECUTER)
                .to(flowServices)
                .build();

        var flowSocle = new FlowBuilder<Flow>("socle")
                // Organismes
                .start(flowOrganisme)
                .build();


        var flowChorus = new FlowBuilder<Flow>("echanges-chorus")
                // CHORUS
                .start(recupererDecider("echanges-chorus"))
                .on(BaseStepExecutionDecider.A_EXECUTER)
                .to(recupererStep("echanges-chorus"))
                .from(recupererDecider("echanges-chorus"))
                .on(BaseStepExecutionDecider.NE_PAS_EXECUTER)
                .end()
                .build();

        var flowContrats = new FlowBuilder<Flow>("flow-contrats")
                // contrats
                .start(recupererDecider("contrats"))
                .on(BaseStepExecutionDecider.A_EXECUTER)
                .to(recupererStep("contrats"))
                .from(recupererDecider("contrats"))
                .on(BaseStepExecutionDecider.NE_PAS_EXECUTER)
                .end()
                .build();

        var flowExtras = new FlowBuilder<Flow>("extras")
                // fournisseurs
                .start(flowContrats)
                .next(flowChorus)
                .build();

        return jobBuilder
                .start(flowSocle)
                .next(flowExtras)
                .end()
                .build();
    }

    @Bean
    Step mpeContrats(
            StepBuilderFactory stepBuilderFactory,
            MPEContratReader mpeContratReader,
            MPEContratProcessor mpeContratProcessor,
            MPEContratWriter mpeContratWriter,
            StopWatchStepListener stopWatchStepListener,
            ContratReaderListener contratReaderListener,
            ContratProcessorListener contratProcessorListener,
            @Value("${mpe.chunk.size:200}") int chunkSize
    ) {
        return stepBuilderFactory.get("contrats")
                .listener(stopWatchStepListener)
                .<ContratType, ContratEtablissement>chunk(chunkSize)
                .reader(mpeContratReader)
                .processor(mpeContratProcessor)
                .writer(mpeContratWriter)
                .listener(contratReaderListener)
                .listener(contratProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    Step mpeEchangesChorus(
            StepBuilderFactory stepBuilderFactory,
            MPEChorusReader mpeChorusReader,
            MPEChorusProcessor mpeChorusProcessor,
            MPEChorusWriter mpeChorusWriter,
            ChorusReaderListener chorusReaderListener,
            ChorusProcessorListener chorusProcessorListener,
            @Value("${mpe.chunk.size:200}") int chunkSize
    ) {
        return stepBuilderFactory.get("echanges-chorus")
                .<EchangeChorusType, EchangeChorus>chunk(chunkSize)
                .reader(mpeChorusReader)
                .processor(mpeChorusProcessor)
                .writer(mpeChorusWriter)
                .listener(chorusReaderListener)
                .listener(chorusProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    Step mpeOrganismes(
            StepBuilderFactory stepBuilderFactory,
            MPEOrganismeReader mpeOrganismeReader,
            MPEOrganismeProcessor mpeOrganismeProcessor,
            MPEOrganismeWriter mpeOrganismeWriter,
            OrganismeReaderListener organismeReaderListener,
            OrganismeProcessorListener organismeProcessorListener,
            @Value("${mpe.chunk.size:200}") int chunkSize) {
        return stepBuilderFactory.get("organismes")
                .<OrganismeType, Organisme>chunk(chunkSize)
                .reader(mpeOrganismeReader)
                .processor(mpeOrganismeProcessor)
                .writer(mpeOrganismeWriter)
                .listener(organismeReaderListener)
                .listener(organismeProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    Step mpeServices(
            StepBuilderFactory stepBuilderFactory,
            MPEServiceReader mpeServiceReader,
            MPEServiceProcessor mpeServiceProcessor,
            MPEServiceWriter mpeServiceWriter,
            ServiceReaderListener serviceReaderListener,
            ServiceProcessorListener serviceProcessorListener,
            @Value("${mpe.chunk.size:200}") int chunkSize
    ) {
        return stepBuilderFactory.get("services")
                .<ServiceType, Service>chunk(chunkSize)
                .reader(mpeServiceReader)
                .processor(mpeServiceProcessor)
                .writer(mpeServiceWriter)
                .listener(serviceReaderListener)
                .listener(serviceProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    Step mpeUtilisateurs(
            StepBuilderFactory stepBuilderFactory,
            MPEUtilisateurReader mpeUtilisateurReader,
            ItemProcessor<AgentType, Utilisateur> utilisateurProcessor,
            MPEUtilisateurWriter mpeUtilisateurWriter,
            UtilisateurReaderListener utilisateurReaderListener,
            UtilisateurProcessorListener utilisateurProcessorListener,
            @Value("${mpe.chunk.size:200}") int chunkSize
    ) {
        return stepBuilderFactory.get("utilisateurs")
                .<AgentType, Utilisateur>chunk(chunkSize)
                .reader(mpeUtilisateurReader)
                .processor(utilisateurProcessor)
                .writer(mpeUtilisateurWriter)
                .listener(utilisateurReaderListener)
                .listener(utilisateurProcessorListener)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    ItemProcessor<AgentType, Utilisateur> utilisateurProcessor(
            MPEAuthenticationProcessor authenticationProcessor,
            MPEUtilisateurProcessor utilisateurProcessor
    ) {
        var compositeProcessor = new CompositeItemProcessor<AgentType, Utilisateur>();

        compositeProcessor.setDelegates(List.of(
                authenticationProcessor,
                utilisateurProcessor
        ));


        return compositeProcessor;
    }

}
