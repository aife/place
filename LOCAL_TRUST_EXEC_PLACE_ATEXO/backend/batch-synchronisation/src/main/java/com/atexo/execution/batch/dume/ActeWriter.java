package com.atexo.execution.batch.dume;


import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class ActeWriter implements ItemWriter<DonneesEssentielles> {

    private final DonneesEssentiellesRepository donneesEssentiellesRepository;

    @Override
    public void write(List<? extends DonneesEssentielles> donneesEssentielles) {
        log.debug("écriture des données essentielles \n{}", donneesEssentielles);
        donneesEssentiellesRepository.saveAll(donneesEssentielles);
    }
}

