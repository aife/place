package com.atexo.execution.batch.services;

import com.atexo.execution.batch.argument.Args;
import com.atexo.execution.server.repository.crud.PlateformeRepository;
import com.atexo.execution.server.repository.crud.SynchronisationExterneDateRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SynchronisationServiceImpl implements SynchronisationService {

    static final Logger LOG = LoggerFactory.getLogger(SynchronisationServiceImpl.class);

    final Job synchronisationJob;

    final Job dataGouvJob;

    final Job tncpJob;

    final Job actesViaDumeJob;

    final Job echangeChorusJob;

    final SynchronisationExterneDateRepository synchronisationExterneDateRepository;

    final JobLauncher jobLauncher;

    final JobExplorer jobExplorer;

    final PlateformeRepository plateformeRepository;

    @Value("${tncp.enabled:false}")
    boolean tncpEnabled;

    @Override
    public void jobDispatcher(Args main) {
        if (main.isDataGouv()) {
            LOG.info("Début du job DataGouv");
            runDataGouvJob();
            LOG.info("On sort !! \uD83D\uDCA8");
        } else if (main.isTncp()) {
            LOG.info("Début du job TNCP");
            runTncpJob();
            LOG.info("On sort !! \uD83D\uDCA8");
        } else if (main.isSynchroUpdate()) {
            LOG.info("Début de synchronisation update");
            synchronise(true);
            LOG.info("On sort !! \uD83D\uDCA8");
        } else if (main.isEchangeChorus()) {
            LOG.info("Début du job EchangeChorus");
            synchroniseEchangeChorus();
            LOG.info("On sort !! \uD83D\uDCA8");
        } else if (main.isViaDume()) {
            LOG.info("Début du job Actes via DUME");
            runActesViaDume();
            LOG.info("On sort !! \uD83D\uDCA8");
        } else {
            LOG.info("Début de synchronisation");
            synchronise(false);
            LOG.info("On sort !! \uD83D\uDCA8");
        }
    }

    public void runDataGouvJob() {
        final var builder = new JobParametersBuilder();
        builder.addLong("at", Instant.now().toEpochMilli()).toJobParameters();
        runJob(dataGouvJob, dataGouvJob.getName(), builder.toJobParameters());
    }

    public void runTncpJob() {
        if (tncpEnabled) {
            final var builder = new JobParametersBuilder();
            builder.addLong("at", Instant.now().toEpochMilli()).toJobParameters();
            runJob(tncpJob, tncpJob.getName(), builder.toJobParameters());
        } else {
            LOG.warn("Le job TNCP est désactivé");
        }
    }

    public void runActesViaDume() {
        final var builder = new JobParametersBuilder();
        builder.addLong("at", Instant.now().toEpochMilli()).toJobParameters();
        runJob(actesViaDumeJob, actesViaDumeJob.getName(), builder.toJobParameters());
    }


    @Override
    public synchronized Map<String, String> synchronise(boolean update) {
        var map = new HashMap<String, String>();
        for (var plateforme : plateformeRepository.findAllByActiveTrue()) {
            var mpePfUid = plateforme.getMpeUid();
            var jobName = synchronisationJob.getName() + "_" + plateforme.getMpeUid();
            var jobParameters = new JobParametersBuilder().addString("plateformeUid", mpePfUid).addDate("at", new Date())
                    .addString("update", String.valueOf(update))
                    .toJobParameters();
            LOG.info("Lancement de la synchronisation pour la plateforme {}", mpePfUid);
            map.put(jobName, runJob(synchronisationJob, jobName, jobParameters));
        }
        return map;
    }

    public String runJob(final Job job, String jobName, JobParameters jobParameters) {
        final var runnings = jobExplorer.findRunningJobExecutions(jobName);

        if (!runnings.isEmpty()) {
            LOG.warn("{} batch en cours d'exécution \uD83D\uDE45 pour le job {}", runnings.size(), jobName);
            return String.format("le job %s est en cours d'exécution", job.getName());
        } else {
            LOG.info("🚧 3...2...1... Lancement du batch : {}", job.getName());
            launchJob(job, jobParameters);
            return String.format("lancement du batch %s", job.getName());
        }
    }

    @Async
    public void launchJob(Job job, JobParameters jobParameters) {
        try {
            jobLauncher.run(job, jobParameters);

        } catch (final JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException |
                       JobInstanceAlreadyCompleteException e) {
            LOG.error("Error in executing batch", e);
        }
    }

    public void synchroniseEchangeChorus() {
        for (var plateforme : plateformeRepository.findAllByActiveTrue()) {
            var mpePfUid = plateforme.getMpeUid();
            var jobName = echangeChorusJob.getName() + "_" + plateforme.getMpeUid();
            var jobParameters = new JobParametersBuilder().addString("plateformeUid", mpePfUid).addDate("at", new Date())
                    .toJobParameters();
            LOG.info("Lancement de la synchronisation des échanges chorus pour la plateforme {}", mpePfUid);
            runJob(echangeChorusJob, jobName, jobParameters);
        }
    }

}
