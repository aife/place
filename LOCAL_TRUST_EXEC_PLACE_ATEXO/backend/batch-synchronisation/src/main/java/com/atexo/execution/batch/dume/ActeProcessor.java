package com.atexo.execution.batch.dume;

import com.atexo.execution.common.exception.MPEApiException;
import com.atexo.execution.server.interf.mpe.api.ChorusClient;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.actes.ActeModificatif;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ActeProcessor implements ItemProcessor<ActeModificatif, DonneesEssentielles> {

    private final ChorusClient chorusClient;

    @Override
    public DonneesEssentielles process(ActeModificatif acte) {
        try {
            chorusClient.publierModification(acte);
            if (acte.getDonneesEssentielles() != null) {
                acte.getDonneesEssentielles().setStatut(StatutPublicationDE.EN_COURS);
            }
        } catch (MPEApiException e) {
            log.error("Erreur lors de la publication de l'acte", e);
        }
        return acte.getDonneesEssentielles();
    }


}
