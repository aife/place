package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class OrganismeStepExecutionDecider extends BaseStepExecutionDecider {

	@Override
	public String getStepname() {
		return "organismes";
	}
}
