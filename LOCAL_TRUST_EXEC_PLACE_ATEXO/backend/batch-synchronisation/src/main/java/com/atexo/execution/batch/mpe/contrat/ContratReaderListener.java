package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.shared.BaseItemReaderListener;
import com.atexo.execution.common.mpe.ws.api.ContratType;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ContratReaderListener extends BaseItemReaderListener<ContratType> {

	@Override
	public String getTaskname() {
		return "[synchro des contrats] récupération des données via API MPE";
	}
}
