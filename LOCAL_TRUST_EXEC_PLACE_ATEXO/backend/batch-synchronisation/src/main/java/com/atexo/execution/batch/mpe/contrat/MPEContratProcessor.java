package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.def.StatutRenouvellement;
import com.atexo.execution.common.def.TypeBorne;
import com.atexo.execution.common.mpe.ws.api.*;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.*;
import com.atexo.execution.server.repository.crud.*;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import com.atexo.execution.server.repository.referentiels.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@StepScope
@Slf4j
public class MPEContratProcessor extends MPEAbstractProcessor implements ItemProcessor<ContratType, ContratEtablissement> {

    private static final Pattern lieuExecutionPattern = Pattern.compile("\\s*\\((?<code>[^\\)]*)\\)\\s*(?<libelle>\\w*)");

    @Autowired
    private ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private TypeContratRepository typeContratRepository;

    @Autowired
    private FormePrixRepository formePrixRepository;

    @Autowired
    private ProcedureRepository procedureRepository;

    @Autowired
    private CategorieConsultationRepository categorieConsultationRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private EtablissementRepository etablissementRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private DonneesEssentiellesRepository donneesEssentiellesRepository;

    @Autowired
    private OrganismeRepository organismeRepository;

    @Autowired
    private ActeRepository acteRepository;

    @Autowired
    private LieuExecutionRepository lieuExecutionRepository;

    @Autowired
    private CPVRepository cpvRepository;

    @Autowired
    CCAGReferenceRepository ccagReferenceRepository;

    @Autowired
    private RevisionPrixRepository revisionPrixRepository;

    @Autowired
    private FournisseurRepository fournisseurRepository;


    @Value("#{jobParameters['update']}")
    public String update;



    @Override
    public ContratEtablissement process(ContratType contratType) throws Exception {
        boolean isUpdate = Boolean.parseBoolean(update);
        String organisme = contratType.getOrganisme();
        var serviceId = 0 != contratType.getIdServiceSynchroExec() ? contratType.getIdServiceSynchroExec() : contratType.getIdService();
        Integer contratTypeId = contratType.getId();
        String numero = contratType.getNumero();
        log.debug("Plateforme {} => traitement du contrat {}/{} => [chapeau ou mono ={}][service={}][organisme={}]", mpePfUid, numero, contratTypeId, contratType.isChapeau(), serviceId, organisme);
        String idExterne = String.valueOf(contratTypeId);
        Contrat contrat = contratRepository.findByIdExterneAndPlateformeMpeUid(idExterne, mpePfUid).orElse(null);
        if (contrat == null && StringUtils.hasText(numero)) {
            contrat = getContratFromNumero(numero, mpePfUid, serviceId, organisme);
        }
        if (isUpdate) {
            return update(contrat, contratType, idExterne, numero, organisme, serviceId);
        }
        return createOrUpdate(contrat, contratType, idExterne, numero, organisme, serviceId);
    }

    private ContratEtablissement createOrUpdate(Contrat contrat, ContratType contratType, String idExterne, String numero, String organisme, int serviceId) {
        if (contrat == null) {
            log.info("Plateforme {} => création du contrat {}", mpePfUid, numero);
            contrat = new Contrat(idExterne, true, StatutRenouvellement.EN_ATTENTE);
            contrat.setChapeau(false);
        } else {
            boolean chapeau = !CollectionUtils.isEmpty(contrat.getContrats());
            contrat.setChapeau(chapeau);
            log.info("Plateforme {} => mise à jour du contrat {}/{} [chapeau]={}", mpePfUid, numero, contrat.getId(), chapeau);
        }
        contrat.setPlateforme(getPlateforme());
        ContratEtablissement contratEtablissement = contratEtablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(idExterne, mpePfUid).orElse(new ContratEtablissement());
        contratEtablissement.setPlateforme(getPlateforme());
        contratEtablissement.setIdExterne(idExterne);
        // on ne synchronise pas les contrats flagués à synchronisable = false
        if (Boolean.FALSE.equals(contrat.getSynchronisable())) {
            log.debug("Plateforme {} => le contrat {}/{} n'est pas synchronisable", mpePfUid, numero, contrat.getId());
            return contratEtablissement;
        }
        CategorieConsultation categorieContrat = Optional.ofNullable(contratType.getNaturePrestation())
                .map(NaturePrestationType::value)
                .map(categorieConsultationRepository::findOneByCode).orElse(null);
        contrat = contratRepository.save(contrat);
        // remplissage du contrat
        var service = serviceRepository.findByIdExterneAndPlateformeMpeUid(BeanSynchroUtils.buildIdExterne(organisme, String.valueOf(serviceId)), mpePfUid).orElse(null);
        contrat.setCategorie(categorieContrat);
        contrat.setNumEj(contratType.getNumEj());
        contrat.setNumero(numero);
        contrat.setNumeroLong(contratType.getNumeroLong());
        contrat.setReferenceLibre(contratType.getReferenceLibre());
        contrat.setObjet(contratType.getObjet());
        contrat.setIntitule(contratType.getIntitule());
        contrat.setService(service);
        contrat.setDateCreation(MapperUtils.convertToLocalDateTime(contratType.getDateCreation()));
        LocalDateTime dateModificationExterne = MapperUtils.convertToLocalDateTime(contratType.getDateModification());
        if (dateModificationExterne == null) {
            dateModificationExterne = LocalDateTime.now();
        }
        contrat.setDateModificationExterne(dateModificationExterne);
        if (contrat.getDateModification() == null) {
            contrat.setDateModification(dateModificationExterne);
        }
        contrat.setType(Optional.ofNullable(contratType.getTypeContrat()).map(tc -> typeContratRepository.findOneByIdExterne(tc.getCodeExterne())).orElse(null));
        contrat.setMontant(Optional.ofNullable(contratType.getMontant()).map(BigDecimal::valueOf).orElse(BigDecimal.ZERO));

        var idExterneCreateur = BeanSynchroUtils.buildIdExterne(organisme, String.valueOf(contratType.getIdCreateur()));

        contrat.setIdExterneCreateur(idExterneCreateur);
        utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterneCreateur, mpePfUid).ifPresent(contrat::setCreateur);
        if (contratType.getDonneesEssentielles() != null) {
            log.debug("Plateforme {} => traitement des données essentielles du contrat {}", mpePfUid, numero);
            // données essentielles
            var deIdExterne = "de_" + idExterne;
            var de = donneesEssentiellesRepository.findByIdExterneAndPlateformeMpeUid(deIdExterne, mpePfUid).stream().findFirst().orElse(contrat.getDonneesEssentielles());
            if (de == null) {
                de = new DonneesEssentielles();
                bindDonneesEssentielles(contratType.getDonneesEssentielles(), de);
            }
            de.setPlateforme(getPlateforme());
            de.setIdExterne(deIdExterne);
            de = donneesEssentiellesRepository.save(de);
            contrat.setDonneesEssentielles(de);
            contrat.setPublicationDonneesEssentielles(true);
        } else {
            contrat.setDonneesEssentielles(null);
        }
        if (contratType.getModifications() != null) {
            bindModifications(contratType, contrat);
        }
        var numeroLot = Optional.ofNullable(contratType.getConsultation()).map(ConsultationType::getNumeroLot).orElse(null);
        if (numeroLot != null) {
            contrat.setNumeroLot(numeroLot == 0 ? null : numeroLot);
        }

        Consultation consultation = processConsultation(contratType, categorieContrat, numeroLot, contrat);
        contrat.setConsultation(consultation);


        Contrat contratChapeau = getContratChapeau(contratType, numero, contrat, service, consultation, categorieContrat, numeroLot);
        contrat.setContratChapeau(contratChapeau);
        contrat.setDateNotification(MapperUtils.convertToLocalDate(contratType.getDateNotification()));
        contrat.setDateFinContrat(MapperUtils.convertToLocalDate(contratType.getDateFin()));
        contrat.setDateMaxFinContrat(MapperUtils.convertToLocalDate(contratType.getDateMaxFin()));
        contrat.setTypeBorne(TypeBorne.AUCUNE);
        FormePrix formePrix = null;
        if (contratType.getFormePrix() != null) {
            formePrix = formePrixRepository.findOneByCode(contratType.getFormePrix());
            if (formePrix == null) {
                log.error("la forme de prix {} n'a pas d'équivalence dans le module exécution", contratType.getFormePrix());
                formePrix = formePrixRepository.findOneByCode("FORFAITAIRE");
            }
        }
        contrat.setTypeFormePrix(formePrix);

        // champs additionnels PLACE
        bindAdditionalFields(contrat, contratType);
        // remplissage du contrat établissement
        contratEtablissement.setCategorieConsultation(categorieContrat);
        etablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(contratType.getIdEtablissementTitulaire(), mpePfUid).ifPresent(contratEtablissement::setEtablissement);
        var fournisseur = saveFournisseur(contratType.getEntreprise());
        Etablissement etablissement = saveEtablissement(contratType.getEtablissement(), fournisseur);
        contratEtablissement.setEtablissement(etablissement);
        Contact contact = contratEtablissement.getContact();
        contratEtablissement.setContact(saveContact(contratType.getIdContact(), etablissement, contact));
        createLienAcSad(contratType, contrat);
        // gestion des favoris
        if (contratType.getFavoris() != null) {
            for (Integer idAgent : contratType.getFavoris().getIdAgent()) {
                String idExterneAgent = organisme + "_" + idAgent;
                Utilisateur utilisateur = utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterneAgent, mpePfUid).orElse(null);
                if (utilisateur != null) {
                    contrat.getFavoris().add(utilisateur);
                }
            }
        }

        contrat = contratRepository.save(contrat);
        // assemblage
        contratEtablissement.setContrat(contrat);
        contrat.setAttributaire(contratEtablissement);
        return contratEtablissement;
    }

    private Contrat getContratChapeau(ContratType contratType, String numero, Contrat contrat, Service service, Consultation consultation, CategorieConsultation categorieContrat, Integer numeroLot) {
        Contrat contratChapeau = null;
        // rattachement au contrat parent
        if (contratType.getIdChapeau() != null && !contratType.getIdChapeau().isEmpty()) {
            contratChapeau = contratRepository.findByIdExterneAndPlateformeMpeUid(contratType.getIdChapeau(), mpePfUid).orElse(null);
            if (contratChapeau == null) {
                contratChapeau = contrat.getContratChapeau();
            }
            // cas où les contrats ne sont pas triés dans le bon ordre
            if (contratChapeau == null) {
                log.debug("Plateforme {} => création du contrat chapeau {}", mpePfUid, contratType.getIdChapeau());
                contratChapeau = new Contrat();
                contratChapeau.setSynchronisable(true);
                contratChapeau.setService(service);
                contratChapeau.setReferenceLibre(contratType.getReferenceLibre());
                contratChapeau.setConsultation(consultation);
                contratChapeau.setCategorie(categorieContrat);
                contratChapeau.setType(contrat.getType());
                contratChapeau.setObjet(contratType.getObjet());
            } else {
                log.debug("Plateforme {} => mise à jour du contrat chapeau {}/{}", mpePfUid, contratType.getIdChapeau(), contratChapeau.getId());
            }
            if (contratChapeau.getNumero() != null && !contratChapeau.getNumero().endsWith("-00")) {
                log.debug("Plateforme {} => mise à jour du numéro du contrat chapeau {}/{}", mpePfUid, contratChapeau.getNumero(), contratChapeau.getId());
                contratChapeau.setNumero(numero + "-00");
            }
            contratChapeau.setPlateforme(getPlateforme());
            contratChapeau.setChapeau(true);
            if (numeroLot != null) {
                contratChapeau.setNumeroLot(numeroLot == 0 ? null : numeroLot);
            }

            contratChapeau.setIdExterne(contratType.getIdChapeau());
            contratChapeau = contratRepository.save(contratChapeau);


        }
        return contratChapeau;
    }

    private ContratEtablissement update(Contrat contrat, ContratType contratType, String idExterne, String numero, String organisme, int serviceId) {
        if (contrat == null) {
            log.debug("Aucun contrat trouvé pour l'id externe {}/{}", idExterne, mpePfUid);
            return createOrUpdate(null, contratType, idExterne, numero, organisme, serviceId);
        }
        boolean chapeau = !CollectionUtils.isEmpty(contrat.getContrats());
        contrat.setChapeau(chapeau);
        log.debug("Plateforme {} => mise à jour du contrat {}/{} [chapeau]={}", mpePfUid, numero, contrat.getId(), chapeau);

        contrat.setPlateforme(getPlateforme());
        ContratEtablissement contratEtablissement = contratEtablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(idExterne, mpePfUid).orElse(contrat.getAttributaire());
        if (contratEtablissement == null) {
            contratEtablissement = new ContratEtablissement();
        }
        contratEtablissement.setPlateforme(getPlateforme());
        contratEtablissement.setIdExterne(idExterne);


        contrat.setNumEj(contratType.getNumEj());
        contrat.setNumero(numero);
        contrat.setNumeroLong(contratType.getNumeroLong());
        contrat.setDateCreation(MapperUtils.convertToLocalDateTime(contratType.getDateCreation()));
        LocalDateTime dateModificationExterne = MapperUtils.convertToLocalDateTime(contratType.getDateModification());
        if (dateModificationExterne == null) {
            dateModificationExterne = LocalDateTime.now();
        }
        contrat.setDateModificationExterne(dateModificationExterne);
        if (contrat.getDateModification() == null) {
            contrat.setDateModification(dateModificationExterne);
        }

        var idExterneCreateur = BeanSynchroUtils.buildIdExterne(organisme, String.valueOf(contratType.getIdCreateur()));

        contrat.setIdExterneCreateur(idExterneCreateur);
        utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterneCreateur, mpePfUid).ifPresent(contrat::setCreateur);
        if (contratType.getDonneesEssentielles() != null) {
            log.debug("Plateforme {} => traitement des données essentielles du contrat {}", mpePfUid, numero);
            // données essentielles
            var deIdExterne = "de_" + idExterne;
            DonneesEssentielles donneesEssentielles = contrat.getDonneesEssentielles();
            var de = donneesEssentiellesRepository.findByIdExterneAndPlateformeMpeUid(deIdExterne, mpePfUid).stream().findFirst().orElse(donneesEssentielles);
            if (de == null) {
                de = new DonneesEssentielles();
                bindDonneesEssentielles(contratType.getDonneesEssentielles(), de);
            } else if (donneesEssentielles != null && !Objects.equals(donneesEssentielles.getId(), de.getId())) {
                donneesEssentielles.setContrat(contrat);
                donneesEssentiellesRepository.save(donneesEssentielles);
            }
            de.setPlateforme(getPlateforme());
            de.setContrat(null);
            de.setIdExterne(deIdExterne);
            de = donneesEssentiellesRepository.save(de);
            contrat.setDonneesEssentielles(de);
            contrat.setPublicationDonneesEssentielles(true);
        }
        var numeroLot = Optional.ofNullable(contratType.getConsultation()).map(ConsultationType::getNumeroLot).orElse(null);
        if (numeroLot != null) {
            contrat.setNumeroLot(numeroLot == 0 ? null : numeroLot);
        }

        Consultation consultation = processConsultation(contratType, contrat.getCategorie(), numeroLot, contrat);
        contrat.setConsultation(consultation);

        Contrat contratChapeau = getContratChapeau(contratType, numero, contrat, contrat.getService(), consultation, contrat.getCategorie(), numeroLot);

        contrat.setContratChapeau(contratChapeau);

        contrat.setIdOffre(contratType.getIdOffre());

        // remplissage du contrat établissement
        contratEtablissement.setCategorieConsultation(contrat.getCategorie());
        etablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(contratType.getIdEtablissementTitulaire(), mpePfUid).ifPresent(contratEtablissement::setEtablissement);
        if (contratEtablissement.getEtablissement() == null || contratEtablissement.getEtablissement().getFournisseur() == null) {
            var fournisseur = saveFournisseur(contratType.getEntreprise());
            Etablissement etablissement = saveEtablissement(contratType.getEtablissement(), fournisseur);
            contratEtablissement.setEtablissement(etablissement);
        }
        contratEtablissement.setContact(saveContact(contratType.getIdContact(), contratEtablissement.getEtablissement(), contratEtablissement.getContact()));
        createLienAcSad(contratType, contrat);

        contrat = contratRepository.save(contrat);
        // assemblage
        contratEtablissement.setContrat(contrat);
        contrat.setAttributaire(contratEtablissement);
        return contratEtablissement;
    }

    private Contrat getContratFromNumero(String numero, String mpePfUid, int serviceId, String organisme) {
        var service = serviceRepository.findByIdExterneAndPlateformeMpeUid(BeanSynchroUtils.buildIdExterne(organisme, String.valueOf(serviceId)), mpePfUid).orElse(null);
        if (service != null) {
            List<Contrat> contratList = contratRepository.findByNumeroAndServiceIdAndPlateformeMpeUid(numero, service.getId(), mpePfUid).stream()
                    .filter(c -> c.getIdExterne() == null || !org.apache.commons.lang3.StringUtils.isNumeric(c.getIdExterne())).collect(Collectors.toList());
            if (contratList.size() == 1) {
                return contratList.get(0);
            }
            if (contratList.size() > 1)
                log.warn("Plusieurs contrats trouvés pour le numéro {} et le service {} et la plateforme {}", numero, service.getId(), mpePfUid);
            return null;
        } else {
            List<Contrat> contratList = contratRepository.findByNumeroAndPlateformeMpeUid(numero, mpePfUid).stream().filter(c -> c.getIdExterne() == null || !org.apache.commons.lang3.StringUtils.isNumeric(c.getIdExterne())).collect(Collectors.toList());
            if (contratList.size() == 1) {
                return contratList.get(0);
            }
            if (contratList.size() > 1)
                log.warn("Plusieurs contrats trouvés pour le numéro {}  et la plateforme {}", numero, mpePfUid);
            return null;
        }

    }

    private Fournisseur saveFournisseur(EntrepriseType entrepriseType) {
        if (entrepriseType == null) {
            return null;
        }
        Fournisseur fournisseur = fournisseurRepository.findByIdExterneAndPlateformeMpeUid(String.valueOf(entrepriseType.getId()), mpePfUid).orElse(null);
        if (fournisseur == null) {
            fournisseur = new Fournisseur();
            fournisseur.setIdExterne(String.valueOf(entrepriseType.getId()));
            fournisseur.setIdExterne(String.valueOf(entrepriseType.getId()));
            fournisseur.setNom(entrepriseType.getRaisonSociale());
            fournisseur.setRaisonSociale(entrepriseType.getRaisonSociale());
            fournisseur.setSiren(entrepriseType.getSiren());
            fournisseur.setPays(entrepriseType.getPays());
            fournisseur.setTelephone(entrepriseType.getTelephone());
            fournisseur.setFax(entrepriseType.getFax());

        }
        fournisseur.setPlateforme(getPlateforme());
        return fournisseurRepository.save(fournisseur);
    }

    private Etablissement saveEtablissement(EtablissementType etablissementType, Fournisseur fournisseur) {
        if (etablissementType == null) {
            return null;
        }
        Etablissement etablissement = etablissementRepository.findFirstByIdExterneAndPlateformeMpeUid(String.valueOf(etablissementType.getId()), mpePfUid).orElse(null);
        if (etablissement == null) {
            etablissement = new Etablissement();
            etablissement.setIdExterne(String.valueOf(etablissementType.getId()));
            etablissement.setFournisseur(fournisseur);
            var siret = etablissementType.getSiret();
            if (siret != null && siret.replace(" ", "").length() != 14) {
                siret = fournisseur.getSiren() + etablissementType.getSiret();
            }
            etablissement.setSiret(siret);
            var adresse = new Adresse();
            if (etablissementType.getAdresse() != null) {
                adresse.setAdresse(etablissementType.getAdresse().getRue());
                adresse.setCodePostal(etablissementType.getAdresse().getCodePostal());
                adresse.setCommune(etablissementType.getAdresse().getVille());
                adresse.setPays(etablissementType.getAdresse().getPays());
                etablissement.setAdresse(adresse);
            }
        }
        etablissement.setPlateforme(getPlateforme());
        return etablissementRepository.save(etablissement);
    }

    private Contact saveContact(Integer idContact, Etablissement etablissement, Contact saved) {
        if (idContact == null) {
            return null;
        }
        Contact contact = contactRepository.findFirstByIdExterneAndPlateformeMpeUid(String.valueOf(idContact), mpePfUid).orElse(saved);
        if (contact == null) {
            contact = new Contact();
        }
        contact.setIdExterne(String.valueOf(idContact));
        contact.setEtablissement(etablissement);
        contact.setPlateforme(getPlateforme());
        return contactRepository.save(contact);
    }

    private void createLienAcSad(ContratType contratType, Contrat contrat) {
        Contrat lienAcSad = null;
        if (contratType.getLienAcSad() != null && contratType.getLienAcSad() != 0) {
            lienAcSad = contratRepository.findByIdExterneAndPlateformeMpeUid(String.valueOf(contratType.getLienAcSad()), mpePfUid).orElse(null);
            if (lienAcSad == null) {
                lienAcSad = contrat.getLienAcSad();
            }
            if (lienAcSad == null) {
                lienAcSad = new Contrat();
                lienAcSad.setSynchronisable(true);
            }
            lienAcSad.setPlateforme(getPlateforme());
            lienAcSad.setIdExterne(String.valueOf(contratType.getLienAcSad()));
            lienAcSad = contratRepository.save(lienAcSad);
            contrat.setLienAcSad(lienAcSad);
        }
    }

    private static String getAutresCpv(ContratType contratType) {
        String autresCpv = "";
        if (contratType.getCpv().getCodeSecondaire1() != null) {
            autresCpv += "#" + contratType.getCpv().getCodeSecondaire1();
        }
        if (contratType.getCpv().getCodeSecondaire2() != null) {
            autresCpv += "#" + contratType.getCpv().getCodeSecondaire2();
        }
        if (contratType.getCpv().getCodeSecondaire3() != null) {
            autresCpv += "#" + contratType.getCpv().getCodeSecondaire3();
        }
        return autresCpv;
    }

    private void bindAdditionalFields(Contrat contrat, ContratType contratType) {
        contrat.getLieuxExecution().clear();
        contrat.getLieuxExecution().addAll(processLieuExecution(contratType.getLieuExecutions()));
        contrat.getCodesCpv().clear();
        Optional.ofNullable(contratType.getCpv())
                .map(CPVType::getCodePrincipal)
                .ifPresent(cpv -> {
                    var refCpv = cpvRepository.findByCode(cpv).orElseGet(() -> cpvRepository.save(new CPV(cpv, cpv, cpv)));
                    contrat.setCpvPrincipal(refCpv);
                    contrat.getCodesCpv().add(refCpv);
                });
        if (contratType.getCpv() != null) {
            String autresCpv = getAutresCpv(contratType);
            for (var cpvSecondaire : autresCpv.split("#")) {
                if (cpvSecondaire != null && !cpvSecondaire.isEmpty()) {
                    var refCpvSecondaire = cpvRepository.findByCode(cpvSecondaire).orElseGet(() -> cpvRepository.save(new CPV(cpvSecondaire, cpvSecondaire, cpvSecondaire)));
                    contrat.getCodesCpv().add(refCpvSecondaire);
                }

            }
        }

        Optional.ofNullable(contratType.getCcagApplicable()).map(String::trim).flatMap(ccagReferenceRepository::findFirstByLabel).ifPresent(contrat::setCcagApplicable);

        contrat.setAchatResponsable(MapperUtils.parseBoolean(contratType.getAchatResponsable()));
        contrat.setTrancheBudgetaire(contratType.getTrancheBudgetaire());
        contrat.setModaliteRevisionPrix(contratType.getModaliteRevisionPrix());
        if (contratType.getModaliteRevisionPrix() != null) {
            RevisionPrix revisionPrix = revisionPrixRepository.findFirstByCode(contratType.getModaliteRevisionPrix()).orElse(null);
            contrat.setRevisionPrix(revisionPrix);
        }
        contrat.setDefenseOuSecurite(contratType.isDefenseOuSecurite());
        contrat.setMarcheInnovant(contratType.isMarcheInnovant());
        contrat.setPublicationDonneesEssentielles(contratType.isPublicationDonneesEssentielles());
        contrat.setDatePrevisionnelleNotification(MapperUtils.convertToLocalDate(contratType.getDatePrevisionnelleNotification()));
        contrat.setDatePrevisionnelleFinMarche(MapperUtils.convertToLocalDate(contratType.getDatePrevisionnelleFinMarche()));
        contrat.setDatePrevisionnelleFinMaximaleMarche(MapperUtils.convertToLocalDate(contratType.getDatePrevisionnelleFinMaximaleMarche()));
        contrat.setDureeMaximaleMarche(Optional.ofNullable(contratType.getDureeMaximaleMarche()).map(BigInteger::intValue).orElse(null));
        contrat.setDateDebutExecution(MapperUtils.convertToLocalDate(contratType.getDateDebutExecution()));
        contrat.getOrganismesEligibles().clear();
        if (contratType.getOrganismesEligibles() != null) {
            contratType.getOrganismesEligibles().getOrganisme().forEach(organismeType -> {
                organismeRepository.findByIdExterneAndPlateformeMpeUid(organismeType.getAcronyme(), mpePfUid).ifPresent(organisme -> contrat.getOrganismesEligibles().add(organisme));
            });
        }
        contrat.setIdOffre(contratType.getIdOffre());
        if (contratType.getUuid() != null) {
            contrat.setUuid(contratType.getUuid());
        }
        contrat.setStatutEJ(contratType.getStatutEJ());
    }

    private void bindDonneesEssentielles(DonneesEssentiellesType donneesEssentiellesType, DonneesEssentielles de) {
        de.setDatePublication(MapperUtils.convertToLocalDateTime(donneesEssentiellesType.getDatePublication()));
        de.setStatut(Arrays.stream(StatutPublicationDE.values()).filter(e -> e.name().equalsIgnoreCase(donneesEssentiellesType.getStatut())).findFirst().orElse(null));
        de.setErreurPublication(donneesEssentiellesType.getErreurPublication());
        LocalDateTime dateModification = MapperUtils.convertToLocalDateTime(donneesEssentiellesType.getDateModification());
        if (dateModification == null) {
            dateModification = LocalDateTime.now();
        }
        de.setDateModificationExterne(dateModification);
        de.setDateModification(dateModification);
    }

    private Contrat bindModifications(ContratType contratType, Contrat contrat) {
        // modfications
        contrat.getModifications().clear();
        if (contratType.getModifications() != null) {
            for (var donneesEssentiellesModif : contratType.getModifications().getDonneesEssentielles()) {
                var idExterneModification = String.valueOf(donneesEssentiellesModif.getIdModification());
                var modification = donneesEssentiellesRepository.findFirstByIdExterneAndPlateformeMpeUid(idExterneModification, getPlateforme().getMpeUid()).orElse(new DonneesEssentielles());
                modification.setIdExterne(String.valueOf(donneesEssentiellesModif.getIdModification()));
                bindDonneesEssentielles(donneesEssentiellesModif, modification);
                modification.setContrat(contrat);
                modification = donneesEssentiellesRepository.save(modification);
                contrat.getModifications().add(modification);
                contrat = contratRepository.save(contrat);
                var acte = acteRepository.findFirstByIdExterneAndPlateformeMpeUid(idExterneModification, getPlateforme().getMpeUid()).orElse(null);
                if (acte != null) {
                    acte.setDonneesEssentielles(modification);
                    acteRepository.save(acte);
                }
            }
        }
        return contrat;
    }


    private Collection<? extends LieuExecution> processLieuExecution(String lieuExecutions) {
        var lieuxExecution = new HashSet<LieuExecution>();
        if (StringUtils.hasText(lieuExecutions)) {
            var keyValues = lieuExecutions.split(",|;");
            if (keyValues != null) {
                for (var kv : keyValues) {
                    Matcher matcher = lieuExecutionPattern.matcher(kv);
                    if (matcher.find()) {
                        String code = matcher.group("code");
                        lieuExecutionRepository.findByCode(code).ifPresent(lieuxExecution::add);
                    }
                }
            }
        }
        return lieuxExecution;
    }

    private Consultation processConsultation(ContratType contratType, CategorieConsultation categorieContrat, Integer numeroLot, Contrat contrat) {
        // remplissage de la consultation
        String idConsultation = (BeanSynchroUtils.buildIdExterne("hors_passation", contratType.getOrganisme(), String.valueOf(contratType.getId())));
        if ((contratType.isHorsPassation() == null || !contratType.isHorsPassation()) && contratType.getConsultation() != null) {
            idConsultation = BeanSynchroUtils.buildIdExterne(contratType.getOrganisme(), String.valueOf(contratType.getConsultation().getId()));
        }

        log.info("Plateforme {} => traitement de la consultation {}", mpePfUid, idConsultation);
        if (numeroLot != null) {
            log.debug("Plateforme {} , Le contrat {} est associé au lot {}", mpePfUid, contratType.getNumero(), numeroLot);
        }
        Consultation consultation = consultationRepository.findByIdExterneAndPlateformeMpeUid(idConsultation, mpePfUid).orElse(contrat.getConsultation());
        if (consultation == null) {
            consultation = new Consultation();
        }
        consultation.setPlateforme(getPlateforme());
        consultation.setIdExterne(idConsultation);
        consultation.setHorsPassation(contratType.isHorsPassation());
        if (contratType.getConsultation() != null) {
            ConsultationType consultationType = contratType.getConsultation();
            consultation.setNumero(consultationType.getReference());
            consultation.setIntitule(consultationType.getIntitule());
            consultation.setObjet(consultationType.getObjet());
            LocalDateTime localDateTime = MapperUtils.convertToLocalDateTime(consultationType.getDateModification());
            if (localDateTime == null) {
                localDateTime = LocalDateTime.now();
            }
            consultation.setDateModificationExterne(localDateTime);
            consultation.setDateModification(localDateTime);
            CategorieConsultation categorieConsultation = Optional.ofNullable(consultationType.getNaturePrestation()).map(np -> categorieConsultationRepository.findOneByCode(np.value())).orElse(null);
            consultation.setCategorie(categorieConsultation);
            consultation.setNumeroProjetAchat(consultationType.getNumeroProjetAchat());
            consultation.setDecisionAttribution(MapperUtils.convertToLocalDate(consultationType.getDecisionAttribution()));
            Optional.ofNullable(consultationType.getTypeProcedure())
                    .flatMap(procedureRepository::findByCode)
                    .ifPresent(consultation::setProcedure);
        } else {
            if (Boolean.TRUE.equals(consultation.getHorsPassation())) {
                consultation.setNumero(contratType.getReferenceLibre());
                consultation.setCategorie(categorieContrat);
            } else {
                log.warn("le contrat {} n'est rattaché à aucune consultation", contratType.getNumero());
            }
        }
        return consultationRepository.save(consultation);
    }


}
