package com.atexo.execution.batch.data.gouv;

import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.OrganismeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class DataGouvReader {

    public static final String DATA_GOUV_REPO_ITEM_READER = "dataGouvRepositoryItemReader";
    public static final String FIND_ORGANISMES_BY_CONTRAT_TO_PUBLISH = "findOrganismesByContratToPublish";
    public static final String ID = "id";

    @Value("${data.gouv.page.size:50}")
    private int pageSize;

    private final OrganismeRepository organismeRepository;


    @Bean
    public RepositoryItemReader<Organisme> dataGouvRepositoryItemReader() {
        return new RepositoryItemReaderBuilder<Organisme>()
                .name(DATA_GOUV_REPO_ITEM_READER)
                .repository(this.organismeRepository)
                .methodName(FIND_ORGANISMES_BY_CONTRAT_TO_PUBLISH)
                .arguments(StatutPublicationDE.A_PUBLIER, StatutContrat.ANotifier)
                .sorts(Map.of(ID, Sort.Direction.ASC))
                .pageSize(pageSize)
                .saveState(false)
                .build();
    }
}
