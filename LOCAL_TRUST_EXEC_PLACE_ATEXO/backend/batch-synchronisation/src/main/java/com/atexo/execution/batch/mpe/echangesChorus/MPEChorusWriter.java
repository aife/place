package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.MPEAbstrateWriter;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@StepScope
public class MPEChorusWriter extends MPEAbstrateWriter implements ItemWriter<EchangeChorus> {

    static final Logger LOG = LoggerFactory.getLogger(MPEChorusWriter.class);

    @Autowired
    EchangeChorusRepository echangeChorusRepository;

    public MPEChorusWriter() {
        super(SynchronisationExterneDate.TypeModification.ECHANGES_CHORUS);
    }

    @Override
    public void write(List<? extends EchangeChorus> list) throws Exception {
        echangeChorusRepository.saveAll(list);
        updateSynchronizationDate(list);
    }

}
