package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.shared.BaseItemProcessorListener;
import com.atexo.execution.common.mpe.ws.api.ServiceType;
import com.atexo.execution.server.model.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ServiceProcessorListener extends BaseItemProcessorListener<ServiceType, Service> {

	private static final Logger LOG = LoggerFactory.getLogger(ServiceProcessorListener.class);

	@Override
	public void beforeProcess( ServiceType from ) {
		super.beforeProcess(from);

		LOG.trace("{} : traitement du service avec identifiant externe (identifiant MPE) = {}", this.getTaskname(), from.getId());
	}

	@Override
	public void afterProcess( ServiceType from, Service to ) {
		super.afterProcess(from, to);

		LOG.debug("{} : service avec identifiant externe {} sauvegardé en {}ms", this.getTaskname(), from.getId(), watch.getTotalTimeMillis());
	}

	@Override
	public String getTaskname() {
		return "[synchro des services] traitement des données";
	}
}
