package com.atexo.execution.batch.data.gouv;

import com.atexo.execution.batch.conf.MappingDataGouv;
import com.atexo.execution.common.data.gouv.model.marche.Marches;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Organisme;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.services.DataGouvService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataGouvProcessor implements ItemProcessor<Organisme, DataGouv> {

    public static final String FILE = "tmpFile";
    private final ContratRepository contratRepository;
    private final DataGouvService dataGouvService;
    private final MappingDataGouv mappingDataGouv;

    @Override
    public DataGouv process(Organisme organisme) throws IOException {
        //Récupération des contrats de l'organisme
        var contrats = contratRepository.findContratByOrganisme(organisme, StatutPublicationDE.A_PUBLIER, StatutContrat.ANotifier);
        log.info("[{}] contrats récupérés pour l'organisme: id=[{}], nom=[{}]", contrats.size(), organisme.getId(), organisme.getNom());

        //Récupération du dataSet de l'organisme
        String dataSet = dataGouvService.getOrCreateDataSet(organisme);
        log.info("le DataSet de l'organisme: id=[{}], nom=[{}] est: [{}]", organisme.getId(), organisme.getNom(), dataSet);

        //Préparation de l'objet dataGouv
        log.info("Mapping Exec/DataGouv pour l'organisme: id=[{}], nom=[{}]", organisme.getId(), organisme.getNom());
        Marches marches = dataGouvService.mappingExecDataGouv(contrats, mappingDataGouv.getMappingDataGouv(), organisme);

        //Préparation du fichier
        Path tmpFile = dataGouvService.preparationJsonFile(marches, organisme);
        log.info("Création du fichier=[{}] pour l'organisme: id=[{}], nom=[{}]", tmpFile.getFileName(), organisme.getId(), organisme.getNom());

        //Stocker le Path dans StepExecution
        StepExecution stepExecution = Objects.requireNonNull(StepSynchronizationManager.getContext()).getStepExecution();
        stepExecution.getExecutionContext().put(FILE, tmpFile.toString());

        //Appel du l'api dataGouv upload
        dataGouvService.uploadFile(tmpFile, dataSet);
        log.info("Le fichier=[{}] est bien chargé. organisme, id={}, nom={}", tmpFile.getFileName(), organisme.getId(), organisme.getNom());

        //Suppressioàn du fichier temporaire
        dataGouvService.supprimerFichier(tmpFile);
        log.info("Suppression du fichier. organisme, id={}, nom={}", organisme.getId(), organisme.getNom());

        var nomFichier = Optional.of(tmpFile).map(Path::getFileName).map(Path::toString).orElse(null);
        return new DataGouv.DataGouvBuilder().contrats(contrats).nomFihier(nomFichier).build();
    }
}
