package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.mpe.ws.api.AgentType;
import com.atexo.execution.common.utils.BeanSynchroUtils;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@StepScope
public class MPEUtilisateurProcessor extends MPEAbstractProcessor implements ItemProcessor<AgentType, Utilisateur> {

	final static Logger LOG = LoggerFactory.getLogger(MPEUtilisateurProcessor.class);

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	ServiceRepository serviceRepository;

	@Override
	public Utilisateur process(AgentType agentType) throws Exception {
		String idExterne = buildIdUtilisateur(agentType);

		LOG.info("Plateforme {} , Création ou modification de l'utilisateur {}", mpePfUid, agentType.getIdentifiant());

        Utilisateur utilisateur = utilisateurRepository.findByIdExterneAndPlateformeMpeUid(idExterne, mpePfUid).orElseGet(Utilisateur::new);
        utilisateur.setUuid(BeanSynchroUtils.buildIdExterne(mpePfUid, agentType.getId()));

        serviceRepository.findByIdExterneAndPlateformeMpeUid(buildIdService(agentType), mpePfUid).ifPresentOrElse(
			service -> {
				utilisateur.setActif(agentType.isActif());
				utilisateur.setService(service);
			},
			() -> {
				utilisateur.setActif(false);
				utilisateur.setService(null);
			}
		);
		utilisateur.setPlateforme(getPlateforme());
		utilisateur.setIdExterne(idExterne);
		if (null != agentType.getService()) {
			utilisateur.setRefService(agentType.getService().getId());
		}
		utilisateur.setRefOrganisme(agentType.getAcronymeOrganisme());
		utilisateur.setEmail(agentType.getEmail());
		utilisateur.setIdentifiant(agentType.getIdentifiant());
		utilisateur.setNom(agentType.getNom());
		utilisateur.setPrenom(agentType.getPrenom());
		utilisateur.setDateCreation(MapperUtils.convertToLocalDateTime(agentType.getDateCreation()));
		LocalDateTime localDateTime = MapperUtils.convertToLocalDateTime(agentType.getDateModification());
		if (null == localDateTime) {
			localDateTime = LocalDateTime.now();
		}
		utilisateur.setDateModificationExterne(localDateTime);
		if (utilisateur.getDateModification() == null)
			utilisateur.setDateModification(localDateTime);
		return utilisateur;
	}

	private static String buildIdUtilisateur( AgentType agentType ) {
        return BeanSynchroUtils.buildIdExterne(agentType.getAcronymeOrganisme(), agentType.getId());
	}

	private static String buildIdService( AgentType agentType ) {
		int idService = 0;

		if (null != agentType.getService()) {
			idService = agentType.getService().getId();
		}

        return BeanSynchroUtils.buildIdExterne(agentType.getAcronymeOrganisme(), idService);
	}
}
