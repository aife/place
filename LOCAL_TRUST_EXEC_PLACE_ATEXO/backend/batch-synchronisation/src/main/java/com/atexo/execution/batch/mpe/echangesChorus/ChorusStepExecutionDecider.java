package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.shared.BaseStepExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class ChorusStepExecutionDecider extends BaseStepExecutionDecider {

	@Override
	public String getStepname() {
		return "echanges-chorus";
	}
}
