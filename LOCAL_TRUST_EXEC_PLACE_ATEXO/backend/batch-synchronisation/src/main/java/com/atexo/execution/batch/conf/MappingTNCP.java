package com.atexo.execution.batch.conf;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "tncp")
public class MappingTNCP {
    Map<String, Map<String, String>> mappingTNCP;
}
