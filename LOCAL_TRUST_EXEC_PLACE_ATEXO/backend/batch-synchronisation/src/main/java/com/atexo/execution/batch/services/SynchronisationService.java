package com.atexo.execution.batch.services;

import com.atexo.execution.batch.argument.Args;
import com.atexo.execution.common.exception.ApplicationTechnicalException;
import org.springframework.batch.core.JobParameters;

import java.util.Map;

public interface SynchronisationService {

    Map<String, String> synchronise(boolean update) throws ApplicationTechnicalException;

    void jobDispatcher(Args main) throws ApplicationTechnicalException;

}
