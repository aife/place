package com.atexo.execution.batch.data.gouv;


import com.atexo.execution.common.def.StatutAvenant;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.services.DataGouvService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataGouvWriter implements ItemWriter<DataGouv> {

    private final DataGouvService dataGouvService;

    @Override
    public void write(List<? extends DataGouv> list) throws Exception {
        if (!CollectionUtils.isEmpty(list)) {
            var dataGouv = list.get(0);
            dataGouvService.miseAjourContratPublication(dataGouv.getContrats(), true, false, StatutPublicationDE.PUBLIE.name(), StatutPublicationDE.PUBLIE, LocalDateTime.now(), StatutAvenant.PUBLIE, dataGouv.getNomFihier());
        }
    }
}

