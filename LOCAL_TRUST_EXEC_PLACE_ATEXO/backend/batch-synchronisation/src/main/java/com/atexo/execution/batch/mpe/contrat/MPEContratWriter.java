package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.MPEAbstrateWriter;
import com.atexo.execution.common.def.StatutContrat;
import com.atexo.execution.server.model.Contrat;
import com.atexo.execution.server.model.ContratEtablissement;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.crud.ContratEtablissementRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@StepScope
public class MPEContratWriter extends MPEAbstrateWriter implements ItemWriter<ContratEtablissement> {

    @Autowired
    ContratEtablissementRepository contratEtablissementRepository;

    @Autowired
    ContratRepository contratRepository;

    public MPEContratWriter() {
        super(SynchronisationExterneDate.TypeModification.CONTRAT_ETABLISSEMENT);
    }

    @Override
    public void write(List<? extends ContratEtablissement> list) throws Exception {
        // on sauvegarde que les contrats synchronisables et les contrats établissements pré-persistés
        for (ContratEtablissement contratEtablissement : list) {
            if (contratEtablissement.getContrat() != null && (!Boolean.TRUE.equals(contratEtablissement.getContrat().getSynchronisable()))) {
                continue;
            }
            ContratEtablissement savedContratEtablissement = contratEtablissementRepository.save(contratEtablissement);
            Contrat contrat = contratEtablissement.getContrat();
            if (contrat != null) {
                if (!contrat.isChapeau()) {
                    contrat.setAttributaire(savedContratEtablissement);
                    // on met à jour le statut du contrat
                    if(contrat.getDateNotification() != null) {
                        contrat.setStatut(StatutContrat.Notifie);
                    }
                    contrat.updateContratStatus();
                } else {
                    contrat.setAttributaire(null);
                    contrat.setStatut(null);
                }
                contratRepository.save(contrat);
            }
        }
        updateSynchronizationDate(list);
    }
}
