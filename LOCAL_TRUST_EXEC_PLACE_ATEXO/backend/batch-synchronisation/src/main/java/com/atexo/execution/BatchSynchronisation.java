package com.atexo.execution;

import com.atexo.execution.batch.argument.Args;
import com.atexo.execution.batch.conf.YamlPropertySourceFactory;
import com.atexo.execution.batch.services.SynchronisationService;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.atexo.execution.server.repository"})
@PropertySource(value = {"classpath:config-env.properties", "classpath:bdd.properties"}, encoding = "UTF-8")
@PropertySource(value = {"classpath:mapping-data-gouv.yml", "classpath:mapping-tncp.yml"}, encoding = "UTF-8", factory = YamlPropertySourceFactory.class)
@EnableBatchProcessing
@EnableConfigurationProperties
@Slf4j
public class BatchSynchronisation implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public BatchSynchronisation(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    public static void main(String[] args) {
        SpringApplication.run(BatchSynchronisation.class, args).close();
    }

    @Override
    public void run(String... args) throws Exception {
        var main = new Args();
        JCommander.newBuilder().addObject(main).build().parse(Args.filterSpringBootArgs(args));
        var synchronisationService = applicationContext.getBean(SynchronisationService.class);
        synchronisationService.jobDispatcher(main);
        SpringApplication.exit(applicationContext, () -> 0);
    }
}
