package com.atexo.execution.batch.mpe.contrat;

import com.atexo.execution.batch.mpe.shared.BaseItemProcessorListener;
import com.atexo.execution.common.mpe.ws.api.ContratType;
import com.atexo.execution.server.model.ContratEtablissement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ContratProcessorListener extends BaseItemProcessorListener<ContratType, ContratEtablissement> {

	private static final Logger LOG = LoggerFactory.getLogger(ContratProcessorListener.class);

	@Override
	public void beforeProcess( ContratType from ) {
		super.beforeProcess(from);

		LOG.trace("{} : traitement du contrat avec identifiant externe (identifiant MPE) = {}", this.getTaskname(), from.getId());
	}

	@Override
	public void afterProcess( ContratType from, ContratEtablissement to ) {
		super.afterProcess(from, to);

		LOG.debug("{} : contrat avec identifiant externe {} intégré en {}ms", this.getTaskname(), from.getId(), watch.getTotalTimeMillis());
	}

	@Override
	public String getTaskname() {
		return "[synchro des contrats] traitement des données";
	}
}
