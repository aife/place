package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.shared.BaseItemReaderListener;
import com.atexo.execution.common.mpe.ws.api.AgentType;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class UtilisateurReaderListener extends BaseItemReaderListener<AgentType> {

	@Override
	public String getTaskname() {
		return "[synchro des utilisateurs] récupération des données via API MPE";
	}
}
