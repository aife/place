package com.atexo.execution.batch.mpe;

import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;

import java.util.LinkedList;
import java.util.List;

public abstract class RestReader<T> implements ItemReader<T> {
    static final Logger LOG = LoggerFactory.getLogger(RestReader.class);
    private LinkedList<T> data = new LinkedList<>();
    private final SynchronisationExterneDate.TypeModification typeModification;
    private int totalItems = 0;

    @Value("#{jobParameters['plateformeUid']}")
    public String plateformeUid;


    public RestReader() {
        throw new IllegalArgumentException("ce constructeur n'est pas autorisé");
    }

    public RestReader(SynchronisationExterneDate.TypeModification typeModification) {
        this.typeModification = typeModification;
    }

    public abstract List<T> fetchData();

    @BeforeStep
    private void retrieve() {
        totalItems = 0;
        this.data = new LinkedList<>(fetchData());
    }

    @Override
    public T read() {
        totalItems++;
        return data.poll();
    }

    @AfterStep
    private void traceAfterStep() {
        LOG.info("{} : nombre d'éléments lus : {}", typeModification, totalItems);
    }
}
