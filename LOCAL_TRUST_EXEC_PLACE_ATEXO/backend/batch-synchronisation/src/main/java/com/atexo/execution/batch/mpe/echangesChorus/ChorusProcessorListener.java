package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.shared.BaseItemProcessorListener;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.model.EchangeChorus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ChorusProcessorListener extends BaseItemProcessorListener<EchangeChorusType, EchangeChorus> {

	private static final Logger LOG = LoggerFactory.getLogger(ChorusProcessorListener.class);

	@Override
	public void beforeProcess( EchangeChorusType from ) {
		super.beforeProcess(from);

		LOG.trace("{} : traitement de l'échange CHORUS avec identifiant externe (identifiant MPE) = {}", this.getTaskname(), from.getId());
	}

	@Override
	public void afterProcess( EchangeChorusType from, EchangeChorus to ) {
		super.afterProcess(from, to);

		LOG.debug("{} : échange CHORUS avec identifiant externe {} intégré en {}ms", this.getTaskname(), from.getId(), watch.getTotalTimeMillis());
	}

	@Override
	public String getTaskname() {
		return "[synchro CHORUS] traitement des données";
	}
}
