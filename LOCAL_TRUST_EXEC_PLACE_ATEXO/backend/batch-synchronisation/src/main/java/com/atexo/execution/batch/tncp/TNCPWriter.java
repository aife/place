package com.atexo.execution.batch.tncp;


import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.server.model.DonneesEssentielles;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.echanges.DonneesEssentiellesRepository;
import com.atexo.execution.services.DataGouvService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.execution.tncp.envoi.TNCPEnvoi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class TNCPWriter implements ItemWriter<TNCPEnvoi> {

    private final DataGouvService dataGouvService;
    private final ObjectMapper objectMapper;
    private final ContratRepository contratRepository;
    private final DonneesEssentiellesRepository donneesEssentiellesRepository;
    private final KafkaTemplate<String, TNCPEnvoi> kafkaTemplate;

    @Value("${tncp.kafka.topic.contrat.envoi}")
    private String contratTopicEnvoi;

    @Override
    public void write(List<? extends TNCPEnvoi> contrats) {
        contrats.forEach(tncpEnvoi -> {
            var contrat = contratRepository.findByUuidAndPlateformeMpeUid(tncpEnvoi.getIdObjetSource(), tncpEnvoi.getUuidPlateforme()).orElseThrow(IllegalArgumentException::new);
            log.info("Envoi via KAFKA du contrat Numéro {} ID => {} sur le topic {}", contrat.getNumero(), contrat.getId(), contratTopicEnvoi);
            log.debug("JSON envoyé");
            if (log.isDebugEnabled()) {
                try {
                    log.debug(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tncpEnvoi));
                } catch (JsonProcessingException e) {
                    log.error("Impossible de serialize", e);
                }
            }
            var future = kafkaTemplate.send(contratTopicEnvoi, tncpEnvoi);
            DonneesEssentielles donneesEssentielles = contrat.getDonneesEssentielles();
            donneesEssentielles.setModeEnvoiDonneesEssentielles(ModeEnvoiDonneesEssentielles.TNCP);
            future.addCallback(stringTNCPEnvoiSendResult -> {
                log.info("Envoi du contrat Numéro {} ID => {} avec succès", contrat.getNumero(), contrat.getId());
                dataGouvService.miseAjourContratPublication(contrat, false, true, "", StatutPublicationDE.PUBLIE, LocalDateTime.now());
                donneesEssentielles.setStatut(StatutPublicationDE.EN_COURS);
                donneesEssentielles.setErreurPublication(null);
                donneesEssentiellesRepository.save(donneesEssentielles);
            }, throwable -> {
                log.error("Erreur lors de l'envoi du contrat Numéro {} ID => {}", contrat.getNumero(), contrat.getId(), throwable);
                dataGouvService.miseAjourContratPublication(contrat, false, true, throwable.getMessage(), StatutPublicationDE.ERROR, LocalDateTime.now());
                donneesEssentielles.setStatut(StatutPublicationDE.ERROR);
                donneesEssentielles.setErreurPublication(throwable.getMessage());
                try {
                    donneesEssentielles.setJsonFile(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tncpEnvoi));
                } catch (JsonProcessingException e) {
                    log.error("Impossible de serialize", e);
                }
                donneesEssentiellesRepository.save(donneesEssentielles);
            });

        });
    }
}

