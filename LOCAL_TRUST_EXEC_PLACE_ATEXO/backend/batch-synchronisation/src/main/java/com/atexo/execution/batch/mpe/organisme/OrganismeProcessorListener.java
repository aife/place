package com.atexo.execution.batch.mpe.organisme;

import com.atexo.execution.batch.mpe.shared.BaseItemProcessorListener;
import com.atexo.execution.common.mpe.ws.api.OrganismeType;
import com.atexo.execution.server.model.Organisme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class OrganismeProcessorListener extends BaseItemProcessorListener<OrganismeType, Organisme> {

	private static final Logger LOG = LoggerFactory.getLogger(OrganismeProcessorListener.class);

	@Override
	public void beforeProcess( OrganismeType from ) {
		super.beforeProcess(from);

		LOG.trace("{} : traitement de l'organisme avec identifiant externe (identifiant MPE) = {}", this.getTaskname(), from.getId());
	}

	@Override
	public void afterProcess( OrganismeType from, Organisme to ) {
		super.afterProcess(from, to);

		LOG.debug("{} : organisme avec identifiant externe {} intégré en {}ms", this.getTaskname(), from.getId(), watch.getTotalTimeMillis());
	}

	@Override
	public String getTaskname() {
		return "[synchro des organismes] traitement des données";
	}
}
