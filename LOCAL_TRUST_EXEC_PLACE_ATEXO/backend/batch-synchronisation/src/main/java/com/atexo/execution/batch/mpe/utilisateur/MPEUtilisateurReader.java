package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.RestPaginatedReader;
import com.atexo.execution.common.mpe.ws.api.AgentType;
import com.atexo.execution.server.interf.mpe.api.MpeClient;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@StepScope
public class MPEUtilisateurReader extends RestPaginatedReader<AgentType> {

	@Autowired
	MpeClient mpeApiInterface;

	public MPEUtilisateurReader() {
		super(SynchronisationExterneDate.TypeModification.UTILISATEUR);
	}

	@Override
	public List<AgentType> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        return mpeApiInterface.getUtilisateurs(plateformeUid, page, nbItems, lastSynchronizationDate);
	}


}
