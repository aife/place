package com.atexo.execution.batch.mpe;


import com.atexo.execution.batch.listeners.StopWatchListener;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.crud.SynchronisationExterneDateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public abstract class RestPaginatedReader<T> implements ItemReader<T> {
    static final Logger LOG = LoggerFactory.getLogger(RestPaginatedReader.class);

    @Autowired
    SynchronisationExterneDateRepository synchronisationExterneDateRepository;
    @Value("${mpe.pagination.size:1000}")
    private int paginationSize;
	@Value("${mpe.limit:0}")
	private Integer limit;

	@Value("#{jobParameters['mpe.limit']}")
	public void setLimit(final Integer limit) {
		if (null!=limit) {
			this.limit = limit;
		}
	}

    @Value("#{jobParameters['plateformeUid']}")
    protected String plateformeUid;

    private int currentPage = 0;
    private int totalItems = 0;
    private LinkedList<T> data = new LinkedList<>();
    private LocalDateTime lastSynchronizationDate;
    private final SynchronisationExterneDate.TypeModification typeModification;

	@Autowired
	private StopWatchListener watch;

    public RestPaginatedReader() {
        throw new IllegalArgumentException("ce constructeur n'est pas autorisé");
    }

    public RestPaginatedReader(SynchronisationExterneDate.TypeModification typeModification) {
        this.typeModification = typeModification;
    }

    @Override
    public T read() {
        if (data.isEmpty()) {
	        currentPage++;

			if (null==limit || 0 == limit || currentPage*paginationSize <=limit) {
				data = new LinkedList<>(fetchData(currentPage, paginationSize, lastSynchronizationDate));
			} else {
				var restant = limit - ( ( currentPage - 1) * paginationSize );

				if ( restant > 0 ) {
					data = new LinkedList<>(fetchData(currentPage, restant , lastSynchronizationDate));
				}
			}
        }

        if (data.isEmpty()) {
            return null;
        }
        totalItems++;
        return data.poll();
    }

	public abstract List<T> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate);

    @BeforeStep
    private void initStep() {
        this.lastSynchronizationDate = synchronisationExterneDateRepository
                .findByTypeModificationAndPlateformeMpeUid(typeModification, plateformeUid)
                .map(SynchronisationExterneDate::getDateDerniereModification)
                .orElse(null);
        currentPage = 0;
        totalItems = 0;
    }

    @AfterStep
    private void traceAfterStep() {
        LOG.info("{} : nombre d'éléments lus : {}", typeModification, totalItems);
    }
}
