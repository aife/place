package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.MPEAbstractProcessor;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.mapper.utils.MapperUtils;
import com.atexo.execution.server.model.EchangeChorus;
import com.atexo.execution.server.model.RetourChorus;
import com.atexo.execution.server.repository.crud.ActeRepository;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.echanges.EchangeChorusRepository;
import com.atexo.execution.server.repository.referentiels.RetourChorusRepository;
import com.atexo.execution.server.repository.referentiels.StatutChorusRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@StepScope
@Slf4j
@RequiredArgsConstructor
public class MPEChorusProcessor extends MPEAbstractProcessor implements ItemProcessor<EchangeChorusType, EchangeChorus> {

    private final EchangeChorusRepository echangeChorusRepository;

    private final ContratRepository contratRepository;

    private final StatutChorusRepository statutChorusRepository;

    private final RetourChorusRepository retourChorusRepository;

    private final ActeRepository acteRepository;

    @Override
    public EchangeChorus process(EchangeChorusType echangeContratType) {
        var idEchange = String.valueOf(echangeContratType.getId());
        var uuidContrat = echangeContratType.getUuidContrat();
        var plateforme = getPlateforme();

        log.info("Création ou modification de le l'echange chorus {} pour la plateforme {}", idEchange, plateforme.getMpeUid());

        var echange = echangeChorusRepository.findByIdExterneAndPlateformeMpeUid(idEchange, plateforme.getMpeUid()).orElse(new EchangeChorus());

        echange.setPlateforme(plateforme);
        echange.setReference(echangeContratType.getReference());

        var contrat = contratRepository.findByUuidAndPlateformeMpeUid(uuidContrat, plateforme.getMpeUid()).orElse(null);

        if (null == contrat) {
            log.warn("Aucun contrat n'a été trouvé pour l'uuid {} et la plateforme {}", uuidContrat, plateforme.getMpeUid());
        }

        var statut = echangeContratType.getStatutEchange();

        if (null == statut) {
            log.warn("L'échange CHORUS {} pour le contrat avec l'uuid {} (plateforme {}) n'a aucun statut", idEchange, uuidContrat, plateforme.getMpeUid());
        } else {
            log.debug("L'échange CHORUS {} pour le contrat avec l'uuid {} (plateforme {}) a maintenant le statut {}", idEchange, uuidContrat, plateforme.getMpeUid(), statut.getCode());

            statutChorusRepository.findByIdExterne(statut.getCode()).ifPresentOrElse(
                    echange::setStatut,
                    () -> log.warn("L'échange CHORUS {} pour le contrat avec l'uuid {} (plateforme {}) a bien un statut {} mais il est introuvable dans EXEC", idEchange, uuidContrat, plateforme.getMpeUid(), echangeContratType.getRetourChorus().getCode())
            );

            echange.setErreurPublication(echangeContratType.getMessageErreur());
        }

        var retour = echangeContratType.getRetourChorus();

        if (null == retour) {
            log.warn("L'échange CHORUS {} pour le contrat avec l'uuid {} (plateforme {}) n'a aucun code de retour de fourni.", idEchange, uuidContrat, plateforme.getMpeUid());
        } else {
            retourChorusRepository.findByIdExterne(retour.getCode()).ifPresentOrElse(
                    echange::setRetourChorus,
                    () -> log.warn("L'échange CHORUS {} pour le contrat avec l'uuid {} (plateforme {}) a bien un code retour {} mais il est introuvable dans EXEC", idEchange, uuidContrat, plateforme.getMpeUid(), retour.getCode())
            );

            echange.setErreurPublication(echangeContratType.getMessageErreur());
        }

        echange.setDatePublication(MapperUtils.convertToLocalDateTime(echangeContratType.getDatePublication()));
        echange.setDateModificationExterne(ObjectUtils.defaultIfNull(MapperUtils.convertToLocalDateTime(echangeContratType.getDateModification()), LocalDateTime.now()));
        if (null == echange.getDateModification()) {
            echange.setDateModification(echange.getDateModificationExterne());
        }
        echange.setIdExterne(idEchange);
        echange.setContrat(contrat);
        echange.setGroupementAchat(echangeContratType.getGroupementAchat());
        echange.setOrganisationAchat(echangeContratType.getOrganisationAchat());

        //Mise à jour de l'acte
        updateStatutActeByRetourChorus(echange.getIdExterne(), plateforme.getMpeUid(), echangeContratType);

        return echange;
    }

    private void updateStatutActeByRetourChorus(String idExterneEchange, String pfUid, EchangeChorusType echangeContratType) {
        var acte = acteRepository.findByEchangeChorusIdExterneAndPlateformeMpeUid(idExterneEchange, pfUid).orElse(null);
        if (acte != null && echangeContratType.getRetourChorus() != null && echangeContratType.getRetourChorus().getCode() != null) {
            var statutActe = StatutActe.EN_ATTENTE_VALIDATION;
            switch (echangeContratType.getRetourChorus().getCode().toUpperCase()) {
                case RetourChorus.COMMANDE_ID_EXTERNE:
                    statutActe = StatutActe.VALIDE;
                    break;
                case RetourChorus.IRR:
                case RetourChorus.REJ:
                    statutActe = StatutActe.EN_ERREUR;
                    break;
                default:
                    break;
            }
            log.info("L'échange chorus lié à l'acte est au statut=[{}], on passe l'acte au statut=[{}]", echangeContratType.getRetourChorus().getCode(), statutActe);
            acte.setStatut(statutActe);
            acteRepository.save(acte);
        }
    }

}
