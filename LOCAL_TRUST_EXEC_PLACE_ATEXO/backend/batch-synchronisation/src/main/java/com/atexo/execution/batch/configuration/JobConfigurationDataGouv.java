package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.data.gouv.DataGouv;
import com.atexo.execution.batch.data.gouv.DataGouvProcessor;
import com.atexo.execution.batch.data.gouv.DataGouvProcessorListenner;
import com.atexo.execution.batch.data.gouv.DataGouvWriter;
import com.atexo.execution.server.model.Organisme;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JobConfigurationDataGouv {

    @Value("${mpe.skip.limit:100}")
    int skipLimit = 100;

    private final StepBuilderFactory stepBuilderFactory;
    private final RepositoryItemReader<Organisme> dataGouvRepositoryItemReader;
    private final DataGouvProcessor dataGouvProcessor;
    private final DataGouvWriter dataGouvWriter;
    private final DataGouvProcessorListenner dataGouvProcessorListenner;

    Step dataGouvStep() {
        return stepBuilderFactory.get("data-gouv")
                .<Organisme, DataGouv>chunk(1)
                .reader(dataGouvRepositoryItemReader)
                .processor(dataGouvProcessor)
                .writer(dataGouvWriter)
                .listener(dataGouvProcessorListenner)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE)
                .skip(Exception.class)
                .build();
    }

    @Bean
    public Job dataGouvJob(JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory.get("Envoi des données essentielles à DataGouv")
                .incrementer(new RunIdIncrementer())
                .flow(dataGouvStep()).end().build();

    }
}
