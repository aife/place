package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.shared.BaseItemReaderListener;
import com.atexo.execution.common.mpe.ws.api.ServiceType;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ServiceReaderListener extends BaseItemReaderListener<ServiceType> {

	@Override
	public String getTaskname() {
		return "[synchro des services] récupération des données via API MPE";
	}
}
