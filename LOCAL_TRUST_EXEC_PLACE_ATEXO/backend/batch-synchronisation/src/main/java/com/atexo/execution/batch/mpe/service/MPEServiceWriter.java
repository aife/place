package com.atexo.execution.batch.mpe.service;

import com.atexo.execution.batch.mpe.MPEAbstrateWriter;
import com.atexo.execution.server.model.Service;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.repository.crud.ContratRepository;
import com.atexo.execution.server.repository.crud.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@StepScope
public class MPEServiceWriter extends MPEAbstrateWriter implements ItemWriter<Service> {

	private static final Logger LOG = LoggerFactory.getLogger(MPEServiceWriter.class);

	@Autowired
	ServiceRepository serviceRepository;

	@Autowired
	ContratRepository contratRepository;

	public MPEServiceWriter() {
		super(SynchronisationExterneDate.TypeModification.SERVICE);
	}

	@Override
	public void write(List<? extends Service> services) {
		serviceRepository.saveAll(services);

		// Mise a jour des contrats pour les services qui ont changés d'id MPE.
		services.stream()
				.filter(service -> null != service.getOldIdExterne())
				.forEach(newService ->
					serviceRepository.findOneByIdExterneAndPlateformeMpeUid(newService.getOldIdExterne(), newService.getPlateforme().getMpeUid()).ifPresentOrElse(
							oldService -> {
								LOG.info("Mise a jour de tous les contrats liés au service {} qui change d'id pour {}", oldService.getIdExterne(), newService.getIdExterne());

								contratRepository.mettreAJourService(oldService, newService);
							}, () -> {
								LOG.info("Ancien service introuvable pour l'd externe {}. Rien a faire.", newService.getOldIdExterne());
						})
				);

		updateSynchronizationDate(services);
	}

}
