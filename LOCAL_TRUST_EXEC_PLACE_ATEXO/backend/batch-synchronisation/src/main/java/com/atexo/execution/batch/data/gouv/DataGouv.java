package com.atexo.execution.batch.data.gouv;

import com.atexo.execution.server.model.Contrat;
import lombok.*;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DataGouv {
    private List<Contrat> contrats;
    private String nomFihier;
}
