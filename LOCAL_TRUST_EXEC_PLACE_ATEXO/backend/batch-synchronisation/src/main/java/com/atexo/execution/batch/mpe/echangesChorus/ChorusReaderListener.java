package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.shared.BaseItemReaderListener;
import com.atexo.execution.common.mpe.ws.api.EtablissementType;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class ChorusReaderListener extends BaseItemReaderListener<EtablissementType> {

	@Override
	public String getTaskname() {
		return "[synchro CHORUS] récupération des données via API MPE";
	}
}
