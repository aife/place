package com.atexo.execution.batch.listeners;

import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class StopWatchListener {


	public static class WatchInfo {
		private final String nom;
		private Long duree;
		private final StopWatch chrono;

		public String getNom() {
			return nom;
		}

		public Long getDuree() {
			return duree;
		}

		public StopWatch getChrono() {
			return chrono;
		}

		public WatchInfo( String nom ) {
			this.nom = nom;
			this.duree = 0L;
			this.chrono = new StopWatch();
			this.chrono.start();
		}
	}

	public void reset() {
		this.tasks = new HashMap<>();
	}

	private Map<UUID, WatchInfo> tasks = new HashMap<>();

	public WatchInfo get( UUID uuid ) {
		return tasks.get(uuid);
	}

	public UUID start(String nom) {

		var uuid= tasks.entrySet().stream()
			.filter((entry) -> entry.getValue().getNom().equals(nom)).findFirst().map(Map.Entry::getKey).orElse(null);

		if ( null == uuid ) {
			uuid = UUID.randomUUID();
			tasks.put(uuid, new WatchInfo(nom));
		} else {
			if (tasks.get(uuid).getChrono().isRunning()) {
				tasks.get(uuid).getChrono().stop();
			}
			tasks.get(uuid).getChrono().start();
		}

		return uuid;
	}

	public Long getDuree(UUID uuid) {
		return tasks.get(uuid).getDuree();
	}

	public void stop(UUID uuid) {
		var info = tasks.get(uuid);

		info.chrono.stop();
		info.duree += info.chrono.getLastTaskTimeMillis();
	}

	public String print() {
		var total = tasks.values().stream().map(WatchInfo::getDuree).reduce(0L, Long::sum);

		return tasks.values().stream().map(info -> MessageFormat.format("''{0}'' en {1}ms ({2}%)", info.getNom(), info.getDuree(), Math.round(info.duree * 100.0) / Double.valueOf(total)))
			.collect(Collectors.joining("\n"));
	}
}
