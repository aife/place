package com.atexo.execution.batch.mpe.shared;

import com.atexo.execution.batch.listeners.StopWatchListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.UUID;

@Component
@StepScope
public abstract class BaseItemReaderListener<U> implements ItemReadListener<U>, CustomItemListener {

	private static final Logger LOG = LoggerFactory.getLogger(BaseItemReaderListener.class);

	@Autowired
	private StopWatchListener listener;

	private StopWatch watch;

	private UUID uuid;

	@Override
	public void beforeRead() {
		uuid = listener.start(this.getTaskname());

		watch = new StopWatch();
		watch.start();
	}

	@Override
	public void afterRead( U from ) {
		watch.stop();

		var time = watch.getTotalTimeMillis();

		if (time>0) {
			LOG.debug("{} en {}ms", this.getTaskname(), time);
		}

		listener.stop(uuid);
	}

	@Override
	public void onReadError( Exception exception ) {
	}
}
