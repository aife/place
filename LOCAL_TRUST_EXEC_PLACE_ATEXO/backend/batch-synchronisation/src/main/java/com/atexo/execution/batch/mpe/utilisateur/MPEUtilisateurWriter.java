package com.atexo.execution.batch.mpe.utilisateur;

import com.atexo.execution.batch.mpe.MPEAbstrateWriter;
import com.atexo.execution.server.model.AbstractSynchronizedEntity;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import com.atexo.execution.server.model.Utilisateur;
import com.atexo.execution.server.repository.crud.UtilisateurRepository;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

@Component
@StepScope
public class MPEUtilisateurWriter extends MPEAbstrateWriter implements ItemWriter<Utilisateur> {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	public MPEUtilisateurWriter() {
		super(SynchronisationExterneDate.TypeModification.UTILISATEUR);
	}

	@Override
	public void write(List<? extends Utilisateur> utilisateurs ) throws Exception {
		utilisateurs.stream().filter( distinct(AbstractSynchronizedEntity::getIdExterne) ).forEach(
			utilisateurRepository::save
		);

		updateSynchronizationDate(utilisateurs);
	}

	private static <T> Predicate<T> distinct( Function<? super T, Object> keyExtractor)
	{
		var map = new ConcurrentHashMap<Object, Boolean>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

}
