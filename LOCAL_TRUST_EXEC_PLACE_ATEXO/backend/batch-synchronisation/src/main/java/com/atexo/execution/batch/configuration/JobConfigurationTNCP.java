package com.atexo.execution.batch.configuration;

import com.atexo.execution.batch.tncp.TNCPProcessor;
import com.atexo.execution.batch.tncp.TNCPWriter;
import com.atexo.execution.server.model.Contrat;
import fr.atexo.execution.tncp.envoi.TNCPEnvoi;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JobConfigurationTNCP {

    private final StepBuilderFactory stepBuilderFactory;
    private final RepositoryItemReader<Contrat> tncpRepositoryItemReader;
    private final TNCPProcessor tncpProcessor;
    private final TNCPWriter tncpWriter;
    @Value("${chunk.size:100}")
    int chunkSize;

    Step dataGouvStep() {
        return stepBuilderFactory.get("TNCP")
                .<Contrat, TNCPEnvoi>chunk(chunkSize)
                .reader(tncpRepositoryItemReader)
                .processor(tncpProcessor)
                .writer(tncpWriter)
                .faultTolerant()
                .skip(Exception.class)
                .build();
    }

    @Bean
    public Job tncpJob(JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory.get("Envoi des données essentielles au TNCP")
                .incrementer(new RunIdIncrementer())
                .flow(dataGouvStep()).end().build();

    }
}
