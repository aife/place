package com.atexo.execution.batch.tncp;

import com.atexo.execution.batch.conf.MappingTNCP;
import com.atexo.execution.server.mapper.tncp.TNCPConcessionMapper;
import com.atexo.execution.server.mapper.tncp.TNCPMapper;
import com.atexo.execution.server.model.Contrat;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.execution.tncp.commun.EtapeEnum;
import fr.atexo.execution.tncp.commun.InterfaceEnum;
import fr.atexo.execution.tncp.commun.TypeEnum;
import fr.atexo.execution.tncp.envoi.TNCPEnvoi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Base64Util;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class TNCPProcessor implements ItemProcessor<Contrat, TNCPEnvoi> {

    private final TNCPMapper tncpMapper;
    private final TNCPConcessionMapper tncpConcessionMapper;
    private final MappingTNCP mappingTNCP;
    private final ObjectMapper customMapper;

    @Override
    public TNCPEnvoi process(Contrat contrat) throws IOException {
        log.info("tranformation du contrat {} en contrat à envoyer au TNCP", contrat.getNumero());
        String jsonEnvoye = null;
        if (contrat.isConcession()) {
            log.info("Le contrat {} est une concession", contrat.getNumero());
            var tncpConcession = tncpConcessionMapper.toConcession(contrat, mappingTNCP.getMappingTNCP());
            jsonEnvoye = customMapper.writeValueAsString(tncpConcession);
        } else {
            var tncpContrat = tncpMapper.toMarche(contrat, mappingTNCP.getMappingTNCP());
            jsonEnvoye = customMapper.writeValueAsString(tncpContrat);
        }
        var tncpEnvoi = new TNCPEnvoi();
        tncpEnvoi.setUuid(UUID.randomUUID().toString());
        tncpEnvoi.setIdObjetSource(contrat.getUuid());
        tncpEnvoi.setDateEnvoi(ZonedDateTime.now());
        tncpEnvoi.setEtape(EtapeEnum.PUBLICATION_DE_CONTRAT);
        tncpEnvoi.setFlux(InterfaceEnum.TNCP_SORTANTE);
        tncpEnvoi.setType(TypeEnum.PUBLICATION_DE_CONTRAT);
        if (contrat.getPlateforme() != null) {
            tncpEnvoi.setUuidPlateforme(contrat.getPlateforme().getMpeUid());
        } else {
            log.warn("La plateforme n'est pas renseignée pour le contrat {}", contrat.getUuid());
        }
        log.debug("JSON à envoyer au TNCP : {}", jsonEnvoye);
        String payload = Base64Util.encode(jsonEnvoye);
        tncpEnvoi.setPayload(payload);
        return tncpEnvoi;
    }


}
