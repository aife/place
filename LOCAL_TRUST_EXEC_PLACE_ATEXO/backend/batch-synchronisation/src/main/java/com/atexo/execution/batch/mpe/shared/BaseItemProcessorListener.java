package com.atexo.execution.batch.mpe.shared;

import com.atexo.execution.batch.listeners.StopWatchListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.UUID;

@Component
public abstract class BaseItemProcessorListener<U, V> implements ItemProcessListener<U, V>, CustomItemListener {

	private static final Logger LOG = LoggerFactory.getLogger(BaseItemProcessorListener.class);

	@Autowired
	protected StopWatchListener listener;

	protected StopWatch watch;

	private UUID uuid;

	@Override
	public void beforeProcess( U from ) {
		uuid = listener.start(this.getTaskname());

		watch = new StopWatch();
		watch.start();
	}

	@Override
	public void afterProcess( U from , V to ) {
		watch.stop();

		listener.stop(uuid);
	}

	@Override
	public void onProcessError( U from, Exception exception ) {
	}
}
