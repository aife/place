package com.atexo.execution.batch.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SynchronisationJobListener implements JobExecutionListener {

    static final Logger LOG = LoggerFactory.getLogger(SynchronisationJobListener.class);

	@Autowired
	private StopWatchListener listener;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        LOG.info("\uD83C\uDFC1 lancement du job => {}", jobExecution.getJobInstance().getJobName());

	    listener.reset();
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        LOG.info("\uD83D\uDD1A fin du job => {}", jobExecution.getJobInstance().getJobName());
    }
}
