package com.atexo.execution.batch.dume;

import com.atexo.execution.common.def.ModeEnvoiDonneesEssentielles;
import com.atexo.execution.common.def.StatutActe;
import com.atexo.execution.server.model.StatutPublicationDE;
import com.atexo.execution.server.model.actes.ActeModificatif;
import com.atexo.execution.server.repository.crud.ActeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class ActeReader {

    public static final String ACTE_REPOSITORY_ITEM_READER = "acteRepositoryItemReader";
    public static final String FIND_BY_ACTES_TO_PUBLISH = "findByActesToPublish";
    public static final String ID = "id";
    private final ActeRepository acteRepository;
    @Value("${data.gouv.page.size:50}")
    private int pageSize;

    @Bean
    public RepositoryItemReader<ActeModificatif> acteRepositoryItemReader() {
        return new RepositoryItemReaderBuilder<ActeModificatif>()
                .name(ACTE_REPOSITORY_ITEM_READER)
                .repository(this.acteRepository)
                .methodName(FIND_BY_ACTES_TO_PUBLISH)
                .arguments(StatutPublicationDE.A_PUBLIER, StatutActe.NOTIFIE, ModeEnvoiDonneesEssentielles.VIA_DUME)
                .sorts(Map.of(ID, Sort.Direction.ASC))
                .pageSize(pageSize)
                .saveState(false)
                .build();
    }
}
