package com.atexo.execution.batch.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class StopWatchStepListener implements StepExecutionListener {

	private static final Logger LOG = LoggerFactory.getLogger(StopWatchStepListener.class);

	@Autowired
	private StopWatchListener listener;

	@Override
	public void beforeStep( StepExecution stepExecution ) {
		LOG.info("Début du step {}", stepExecution.getStepName());
	}

	@Override
	public ExitStatus afterStep( StepExecution stepExecution ) {
		LOG.debug("Fin du step {} avec le statut {}", stepExecution.getStepName(), stepExecution.getExitStatus());

		LOG.debug(listener.print());

		return stepExecution.getExitStatus();
	}
}
