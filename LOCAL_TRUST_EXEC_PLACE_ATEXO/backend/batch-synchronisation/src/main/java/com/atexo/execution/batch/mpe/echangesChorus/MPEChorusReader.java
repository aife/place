package com.atexo.execution.batch.mpe.echangesChorus;

import com.atexo.execution.batch.mpe.RestPaginatedReader;
import com.atexo.execution.common.mpe.ws.api.EchangeChorusType;
import com.atexo.execution.server.interf.mpe.api.ChorusClient;
import com.atexo.execution.server.model.SynchronisationExterneDate;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@StepScope
public class MPEChorusReader extends RestPaginatedReader<EchangeChorusType> {

	@Autowired
	private ChorusClient chorusClient;

    public MPEChorusReader() {
        super(SynchronisationExterneDate.TypeModification.ECHANGES_CHORUS);
    }

    @Override
    public List<EchangeChorusType> fetchData(int page, int nbItems, LocalDateTime lastSynchronizationDate) {
        return chorusClient.getEchangesChorus(plateformeUid, page, nbItems, lastSynchronizationDate);
    }

}
