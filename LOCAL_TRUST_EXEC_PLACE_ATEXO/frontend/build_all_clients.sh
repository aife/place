#!/bin/bash
for client in `ls`
do
  if [[ -d "$client" ]] && [[ "$client" != "src" ]] && [[ "$client" != "node" ]] && [[ "$client" != "node_modules" ]]&& [[ "$client" != "commun" ]] && [[ "$client" != "target" ]]; then
      echo "build du client => $client"
      mvn clean install deploy -Dclient=$client -s ~/.m2/settings_nexus.xml
  fi
done
