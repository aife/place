# Local Trust MPE

## Dématérialisation des marchés publics ##

Premier module de la solution progicielle LOCAL TRUST, LOCAL TRUST MPE (Marchés Publics Electroniques) est une solution d’e-procurement public répondant aux exigences réglementaires des Directives européennes et du Code des marchés publics.

La solution gère tous les types de marchés au-dessus et en-dessous des seuils. Elle offre les fonctionnalités suivantes :

* **Couverture fonctionnelle complète** : e-publicité, e-consultation, e-enchère, e-attribution, base fournisseur
* **Forte adaptabilité aux diversités de fonctionnement des grands comptes publics**(organisation centralisée, décentralisée, etc)
* **Facilité de paramétrage**

## Documentation sur Confluence
* [Espace MPE](https://confluence.local-trust.com/display/ltmpe/Espace+MPE)
* [Versions de MPE](https://confluence.local-trust.com/display/ltmpe/VERSIONS+MPE)
* [Déterminer la branche Git pour un ticket et une MR](https://confluence.local-trust.com/pages/viewpage.action?pageId=32605104)
* [Plate-formes d'intégration continue](https://confluence.local-trust.com/pages/viewpage.action?pageId=49742606)

Pour en savoir plus sur un sujet, penser à faire des recherches
sur Confluence.

Si une documentation n'est pas à jour, prenez l'initiative de la
mettre à jour :)

## La branche Develop/Sprint:
si vous travaillez sur cette branche directement, vous n'aurez pas besoin de faire l'étape 'ansible-init' + 'ansible-build' car au moment du 'make docker-run' on va télécharger l'image MPE et on va la builder :)

## Références sur Jira
* [Kanban évolutif et applicatif](https://jira.local-trust.com/secure/RapidBoard.jspa?rapidView=189&useStoredSettings=true)
* [Suivi des versions MPE en cours de préparation](https://jira.local-trust.com/projects/MPE?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page)

## Installation environnement de dev MPE
Cf. [docker/README.md](docker/README.md)

## Les commandes les plus utiles
Cf. [COMMANDS.md](COMMANDS.md)

## Le thème Modern Admin pour la partie Agent (SF)
Cf. [mpe_sf/assets/modern-admin/README.md](README.md)
