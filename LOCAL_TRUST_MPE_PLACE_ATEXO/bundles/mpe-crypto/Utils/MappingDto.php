<?php

/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 29/09/2016
 * Time: 12:21.
 */

namespace Atexo\CryptoBundle\Utils;

use App\Entity\CertificatChiffrement;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use AtexoCrypto\Dto\Certificat;
use AtexoCrypto\Dto\Chiffrement;
use AtexoCrypto\Dto\Consultation as ConsultationDto;
use AtexoCrypto\Dto\ConsultationAlloti;
use AtexoCrypto\Dto\Dechiffrement;
use AtexoCrypto\Dto\Enveloppe as EnveloppeDto;
use AtexoCrypto\Dto\EnveloppeAlloti;
use AtexoCrypto\Dto\Fichier;
use AtexoCrypto\Dto\Offre as OffreDto;
use AtexoCrypto\Dto\SignatureFichier;
use AtexoCrypto\Dto\VerifFichier;
use AtexoCrypto\Dto\VerifHash;
use AtexoCrypto\Dto\VerifPdf;

class MappingDto
{
    /**
     * Map consultation to chiffrement.
     *
     * @return Chiffrement
     */
    public function getChiffrement(Consultation $consultationObject, Offre $offreObject, array $params)
    {
        $chiffrement = new Chiffrement();
        $chiffrement->setServeurCryptoURLPublic($params['serveur_crypto_url_public']);
        $chiffrement->setMpeURLPrivee($params['mpe_url_privee']);
        $chiffrement->setMpeFilePath($params['mpe_file_path']);
        $chiffrement->setMpeLogin($params['mpe_login']);
        $chiffrement->setMpePassword($params['mpe_password']);
        $chiffrement->setFonction($params['fonction']);

        $chiffrement->setRaisonSocialEntreprise($offreObject->getNomEntrepriseInscrit());

        if ($consultationObject->getAlloti()) {
            $consultation = new ConsultationAlloti();
            $consultation->setNombreLots(count($consultationObject->getLots()));
        } else {
            $consultation = new ConsultationDto();
        }

        $consultation->setReference($consultationObject->getReferenceUtilisateur());
        $consultation->setId($consultationObject->getId());
        $consultation->setOrganisme($consultationObject->getAcronymeOrg());
        $consultation->setChiffrement(('1' ==$consultationObject->getChiffrementOffre() ));
        $consultation->setAlloti(('1' ==$consultationObject->getAlloti() ));
        $consultation->setIntitule($consultationObject->getIntitule());
        $consultation->setObjet($consultationObject->getObjet());
        $consultation->setCmsActif($consultationObject->getCmsActif());

        $chiffrement->setConsultation($consultation);

        $offre = self::getOffre($offreObject, $consultationObject, true, ('1' ==$consultationObject->getChiffrementOffre() ), $consultationObject->getAlloti());

        $chiffrement->setOffre($offre);

        return $chiffrement;
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param $idAgent
     *
     * @return Dechiffrement
     */
    public function getDechiffrement(array $offres, $idAgent, Consultation $consultationObject = null, array $params = [])
    {
        $dechiffrement = new Dechiffrement();

        $dechiffrement->setServeurCryptoURLPublic($params['ServeurCryptoURLPublic']);
        $dechiffrement->setMpeURLPrivee($params['MpeURLPrivee']);
        $dechiffrement->setMpeFilePath($params['MpeFilePath']);
        $dechiffrement->setMpeLogin($params['MpeLogin']);
        $dechiffrement->setMpePassword($params['MpePassword']);
        $dechiffrement->setFormat($params['Format']);
        $dechiffrement->setEnLigne($params['EnLigne']);
        if ($consultationObject) {
            $dechiffrement->setDateLimiteRemisePlis($consultationObject->getDatefin());
            $dechiffrement->setReference($consultationObject->getReferenceUtilisateur());
            $dechiffrement->setOrganisme($consultationObject->getAcronymeOrg());
        }

        $dechiffrement->setIdAgent($idAgent);

        foreach ($offres as $offre) {
            $dechiffrement->addOffre(self::getOffre($offre));
        }

        return $dechiffrement;
    }

    /**
     * Map fichier to signature.
     *
     * @return VerifHash
     */
    public function getVerifHash(string $hash, string $signatureXml, string $typeSignature, string $hash256 = '')
    {
        $signature = new VerifHash();

        $signature->addHash($hash);
        $signature->addHash($hash256);

        $signature->setSignatureBase64($signatureXml);
        $signature->setSignatureType($typeSignature);

        return $signature;
    }

    /**
     * Map fichier to signature.
     *
     * @param string $hash
     * @param string $sigantureXml
     *
     * @return InfosSignature
     */
    public function getVerifFichier(string $path, int $idBlob, string $contenuSignature, string $typeSignature)
    {
        $signature = new VerifFichier();

        $signature->setIdFileSystem($idBlob);
        $signature->setMpeFilePath($path);
        $signature->setSignatureBase64($contenuSignature);
        $signature->setSignatureType($typeSignature);

        return $signature;
    }

    /**
     * Map fichier to signature.
     *
     * @return VerifPdf
     */
    public function getVerifPdf(string $path, string $typeSignature)
    {
        $signature = new VerifPdf();

        $signature->setPdfPath($path);
        $signature->setSignatureType($typeSignature);

        return $signature;
    }

    /**
     * Map Offre to chiffrement.
     *
     * @param bool $alloti
     *
     * @return Offre
     */
    public function getOffre(Offre $offreObject, Consultation $consultationObject = null, bool $chiffrement = false, bool $withCertificat = true, bool $allouti = true)
    {
        $offre = new OffreDto();
        $offre->setIdOffre($offreObject->getId());

        if (!$chiffrement) {
            $offre->setReponseAnnonceBase64(base64_encode($offreObject->getXmlString()));
            if (0 ==$offreObject->getVerificationHorodatage() ) {
                $offre->setJetonHorodatage($offreObject->getHorodatage());
            }
        }
        foreach ($offreObject->getEnveloppes() as $enveloppe) {
            if ($enveloppe instanceof Enveloppe && '0' == $enveloppe->getEnveloppeFictive()) {
                $offre->addEnveloppe(self::getEnveloppe($enveloppe, $chiffrement, $allouti));
            }
        }

        if ($chiffrement && $withCertificat) {
            foreach ($consultationObject->getCertificatChiffrementsNonDouble() as $certificat) {
                $offre->addCertificate(self::getCertificat($certificat));
            }
        }

        return $offre;
    }

    /**
     * Map Enveloppe to chiffrement.
     *
     * @return Enveloppe
     */
    public function getEnveloppe(Enveloppe $enveloppeObject, bool $chiffrement = false, bool $alloti = false)
    {

        if ($alloti) {
            $enveloppe = new EnveloppeAlloti();
            $enveloppe->setNumeroLot($enveloppeObject->getSousPli());
        } else {
            $enveloppe = new EnveloppeDto();
        }
        $enveloppe->setIdEnveloppe($enveloppeObject->getIdEnveloppeElectro());
        $enveloppe->setType($enveloppeObject->getTypeEnv());
        if ($chiffrement) {
            foreach ($enveloppeObject->getFichierEnveloppes() as $fichierObject) {
                $fichier = self::getFichier($fichierObject);
                $enveloppe->addFichier($fichier);
            }
        }

        return $enveloppe;
    }

    /**
     * Map fichier to chiffrement.
     *
     * @return Fichier
     */
    public function getFichier(EnveloppeFichier $fichierObject)
    {
        $fichier = new Fichier();
        $fichier->setIdFichier($fichierObject->getIdFichier());
        $fichier->setTypeFichier($fichierObject->getTypeFichier());
        $fichier->setName($fichierObject->getNomFichier());
        $fichier->setNumeroOrdre($fichierObject->getNumOrdreFichier());
        $fichier->setTailleFichier($fichierObject->getTailleFichier());
        $fichier->setHashFichier($fichierObject->getHash256());

        if (empty($fichierObject->getIdBlobSignature())) {
            $fichier->setFichierSignatureBase64($fichierObject->getSignatureFichier());
        }

        return $fichier;
    }

    /**
     * Map certificat to chiffrement.
     *
     * @return Certificat
     */
    public function getCertificat(CertificatChiffrement $certificatObject)
    {
        $certificat = new Certificat();
        $certificat->setTypeEnveloppe($certificatObject->getTypeEnv());

        $certificat->setCertificatBase64($certificatObject->getCertificat());

        return $certificat;
    }

    /**
     * @param $pathFichier
     * @param null $pathCertificat
     * @param null $password
     *
     * @return SignatureFichier
     */
    public function getSignatureFichier($pathFichier, $pathCertificat = null, $password = null)
    {
        $signatureFichier = new SignatureFichier();
        $signatureFichier->setCertificat($pathCertificat);
        $signatureFichier->setPassword($password);
        $signatureFichier->setContenu($pathFichier);

        return $signatureFichier;
    }
}
