<?php

namespace Atexo\CryptoBundle;

use App\Entity\Consultation;
use App\Entity\Offre;
use Atexo\CryptoBundle\Utils\MappingDto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Exception\ServiceException;

/**
 * Classe contenant les fonctions crypto.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2016
 *
 * @version 0
 *
 * @since   0
 */
class AtexoCrypto
{
    private $url;
    private $logger = null;
    private $proxy = null;
    private $platform;
    private $enableSignatureServerV2;
    private $webServicesCrypto = null;

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return mixed
     */
    public function getEnableSignatureServerV2()
    {
        return $this->enableSignatureServerV2;
    }

    /**
     * @param mixed $enableSignatureServerV2
     */
    public function setEnableSignatureServerV2($enableSignatureServerV2)
    {
        $this->enableSignatureServerV2 = $enableSignatureServerV2;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return null
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * @param null $proxy
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * Constructeur de la classe.
     */
    public function __construct($url, $cryptoLogger, $enableSignatureServerV2, $proxyUrl = null, $proxyPort = null, $platform = null, $webServicesCrypto = null)
    {
        $this->setUrl($url);
        $this->setLogger($cryptoLogger);
        $this->setEnableSignatureServerV2($enableSignatureServerV2);

        if (!empty($proxyUrl)) {
            $proxy = $proxyUrl.(empty($proxyPort) ? '' : ':'.$proxyPort);
            $this->setProxy($proxy);
        }

        if (null != $platform) {
            $this->setPlatform($platform);
        }

        $this->webServicesCrypto = $webServicesCrypto;
    }

    /**
     * Demande de chiffrement.
     *
     * @return \AtexoCrypto\Service\jeton
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function demandeChiffrement(Consultation $consultation, Offre $offre, array $params)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();
            $chiffrement = $mappingDto->getChiffrement($consultation, $offre, $params);

            return $c->chiffrementService()->demandeChiffrement($chiffrement);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Demande de dechiffrement.
     *
     * @param int $idAgent
     *
     * @return string $jnlp
     */
    public function demandeDechiffrement(array $offres, $idAgent, Consultation $consultation = null, array $params = [])
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();
            $dechiffrement = $mappingDto->getDechiffrement($offres, $idAgent, $consultation, $params);

            return $c->dechiffrementService()->demandeDechiffrement($dechiffrement);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Verification de signature.
     *
     * @return AtexoCrypto\Dto\InfosSignature infosSignature
     */
    public function verifierSignatureHash(string $hash, string $signatureXml, string $typeSignature, string $hash256 = '')
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();
            $signature = $mappingDto->getVerifHash($hash, $signatureXml, $typeSignature, $hash256);

            return $c->validationService()->verifierSignature($signature);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Verification de signature.
     *
     * @param string $idBlob
     *
     * @return AtexoCrypto\Dto\InfosSignature infosSignature
     */
    public function verifierSignatureFichier(string $path, int $idBlob, string $contenuSignature, string $typeSignature)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();
            $signature = $mappingDto->getVerifFichier($path, $idBlob, $contenuSignature, $typeSignature);

            return $c->validationService()->verifierSignature($signature);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Verification de signature.
     *
     * @return AtexoCrypto\Dto\InfosSignature infosSignature
     */
    public function verifierSignaturePdf(string $pdfPath, string $typeSignature)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                $this->platform,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();

            $signatureFichier = $mappingDto->getSignatureFichier(
                $pdfPath
            );

            $signature = $mappingDto->getVerifPdf($pdfPath, $typeSignature);
            $eSSV2 = 0;

            if ($this->enableSignatureServerV2) {
                $eSSV2 = 1;
            }

            return $c->validationService()->verifierSignature($signature, $eSSV2, $signatureFichier);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Signature Pades.
     *
     * @param string $pathCertificat
     * @param string $password
     * @param string $pathFichier
     *
     * @return string fichierPades
     */
    public function signerPades($pathCertificat, $password, $pathFichier)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            $mappingDto = new MappingDto();
            $signatureFichier = $mappingDto->getSignatureFichier($pathFichier, $pathCertificat, $password);

            return $c->signatureService()->signerPades($signatureFichier);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Signature Pades.
     *
     * @param string $urlServeurCrypto
     *
     * @return string jnlp
     */
    public function jnlpSignature($urlServeurCrypto)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            return $c->signatureService()->jnlpSignature($urlServeurCrypto);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Validation horodatage.
     *
     * @param string $jetonTSR
     * @param string $hashSha256
     *
     * @return string json
     */
    public function verifierHorodatage($jetonTSR, $hashSha256)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            return $c->validationService()->verifierHorodatage($jetonTSR, $hashSha256);
        } catch (ServiceException $e) {
            throw $e;
        }
    }

    /**
     * Vérification de disponibilité du service de chiffrement.
     *
     * @param array
     *
     * @return \stdClass
     */
    public function verifierDisponibilite($params)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                null,
                $this->webServicesCrypto
            );

            return $c->chiffrementService()->verifierDisponibilite($params);
        } catch (ServiceException $e) {
        }
    }

    /**
     * @return AtexoCrypto\Dto\InfosHorodatage
     *
     * @throws ServiceException
     */
    public function demandeHorodatage(string $xmlHash256)
    {
        try {
            $c = new Client(
                $this->url,
                null,
                null,
                $this->logger,
                $this->proxy,
                $this->platform,
                $this->webServicesCrypto
            );

            return $c->horodatageService()->demandeHorodatage($xmlHash256);
        } catch (ServiceException $e) {
            throw $e;
        }
    }
}
