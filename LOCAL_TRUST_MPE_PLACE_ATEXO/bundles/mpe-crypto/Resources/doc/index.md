AtexoCryptoBundle
==================

Ajouter au niveau du composer.json du projet qui utilise cette lib :

"autoload": {
        "psr-4": {
            "Atexo\\CryptoBundle\\": "vendor/atexo/cryptobundle"
        },
        "psr-0": {
            "AtexoCrypto\\": "vendor/atexo/api-crypto/src"
        },
    },

"repositories": [
        {
            "type":"package",
            "package": {
                "name": "atexo/api-crypto",
                "version":"1.0",
                "source": {
                    "url": "https://gitlab.local-trust.com/php-libraries/AtexoCrypto.git",
                    "type": "git",
                    "reference":"develop"
                }
            }
        },
        {
            "type":"package",
            "package": {
                "name": "atexo/cryptobundle",
                "version":"1.0",
                "source": {
                    "url": "https://gitlab.local-trust.com/bundle/AtexoCryptoBundle.git",
                    "type": "git",
                    "reference":"develop"
                }
            }
        }
    ],
    "require": {
            "atexo/api-crypto": "1.0",
            "atexo/cryptobundle": "1.0"
        },