<?php

namespace Atexo\CryptoBundle\Controller;

use App\Entity\EnveloppeFichier;
use Atexo\CryptoBundle\AtexoCrypto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    public function __construct(private AtexoCrypto $atexoCrypto)
    {
    }

    /**
     * @Route("/crypto/chiffrement/{idConsultation}/{idOffre}", name="test_crypto_chiffrement")
     */
    public function chiffrementAction(Request $request, $idConsultation, $idOffre)
    {
        $consultation = $this->getDoctrine()->getRepository('App:Consultation')->find($idConsultation);

        $offre = $this->getDoctrine()->getRepository('App:Offre')->find($idOffre);

        $params = [];
        $params['MpeURLPrivee'] = 'http://127.0.0.1:8080/crypto/mpeMock/';
        $params['ServeurCryptoURLpublique'] = 'https://atx11.local-trust.com/cryptographie-serveur/';
        $params['MpeFilePath'] = '/opt/MPE/';
        $params['MpeLogin'] = 'login';
        $params['MpePassword'] = 'password';

        $token = $this->atexoCrypto->demandeChiffrement($consultation, $offre, $params);
        echo "Jeton d'horodatage récupéré : ".$token;
        exit;
    }

    /**
     * @Route("/crypto/dechiffrement/{idOffre}/{idAgent}", name="test_crypto_dechiffrement")
     */
    public function dechiffrementAction(Request $request, $idOffre, $idAgent)
    {
        $offre = $this->getDoctrine()->getRepository('App:Offre')->find($idOffre);

        $params = [];
        $params['MpeURLPrivee'] = 'http://127.0.0.1:8080/crypto/mpeMock/';
        $params['ServeurCryptoURLpublique'] = 'https://atx11.local-trust.com/cryptographie-serveur/';
        $params['MpeFilePath'] = '/opt/MPE/';
        $params['MpeLogin'] = 'login';
        $params['MpePassword'] = 'password';
        $params['format'] = 'JNLP'; //"APPLET|JNLP"

        $jnlp = $this->atexoCrypto->demandeDechiffrement([$offre], $idAgent, $offre->getConsultation(), $params);
        header('Content-type: application/x-java-jnlp-file');
        echo $jnlp;
        exit;
    }

    /**
     * @Route("/crypto/validation/{idFichier}", name="test_crypto_validation")
     */
    public function validationAction(Request $request, $idFichier)
    {
        $fichier = $this->getDoctrine()->getRepository(EnveloppeFichier::class)->find($idFichier);

        $verification = $this->atexoCrypto->verifierSignature($fichier, '/opt/MPE/');
        print_r($verification);
        exit;
    }
}
