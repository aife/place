<?php

namespace Atexo\SuperviseurServicesBundle\Controller;

use Atexo\CryptoBundle\AtexoCrypto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    public function __construct(private AtexoCrypto $atexoCrypto)
    {
    }

    /**
     * @Route("/", name="supervisor-home")
     */
    public function indexAction()
    {
        $facade = $this->get('atexo_superviseur_services.facade');

        $result = $facade->getResult();

        $json = new JsonResponse($result);

        return $json;
    }

    /**
     * @Route("/215-09!#crypto", name="supervisor-test-crypto")
     */
    public function cryptoTest()
    {
        $params = [];
        $params['mpeURLPrivee'] = 'http://127.0.0.1:8080/crypto/mpeMock/';
        $params['serveurCryptoURLpublic'] = 'http://mpe-sf3-int.local-trust.com:8080';
        $params['mpeFilePath'] = '/opt/MPE/';
        $params['mpeLogin'] = 'login';
        $params['mpePassword'] = 'password';

        $result = $this->atexoCrypto->availabilityCheck($params);

        $json = new JsonResponse();
        $json->setContent($result);

        return $json;
    }
}
