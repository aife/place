SuperviseurServiceBundle est une API REST exploitée par le serveur de monitoring zabbix pour la superision des services faisant partie de l'ecosysteme MPE

Pour l'installer, vérifier le json de votre projet il doit contenir. sinon ajouter la ligne le script suivant:

"require": {
        "atexo/superviseurServiceBundle": "1.0",
        }
        
Puis, faire la commande suivante:

composer require "atexo/superviseurServiceBundle".

Pour étendre le bundle (contribuer au développement du bundle) (Exemple : vouloir supervsier un nouveau service)

Etape-1 : Créer la classe Service de type AbsractService et dedans une méthode qui redéfinit la "public function verify()", et ymettre le traitement approprié dedans.
(NB la méthode verify() ne retourne rien)

Etape-2 : Déclarer la classe Service récemment créee en tant que service auprès du conteneur symfony.

Etape-3 : Souscire le service en question à l'annuaire des service réseaux.