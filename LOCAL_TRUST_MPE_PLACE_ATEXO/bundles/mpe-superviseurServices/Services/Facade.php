<?php

namespace Atexo\SuperviseurServicesBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * @author Mohamed Blal
 * Class Facade
 */
class Facade
{
    /**
     * @var ContainerAwareInterface
     */
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getResult()
    {
        $services = new NetworkServiceDirectory();
        $s = $services->getServices();
        foreach ($s as $service) {
            $srv = $this->container->get($service);
            $srv->ping();
        }

        $fileService = $this->container->get('atexo_superviseur_services.file_service');
        $accessRules = $fileService->verify();

        $list = array_merge(AbstractService::getState(), $accessRules);

        $output = $this->getStackedStates($list);

        return $output;
    }

    /**
     * @param array $list
     *
     * @return array
     */
    protected function getStackedStates($list = [])
    {
        $report = [];
        foreach ($list as $key => $item) {
            $report[$key][Constant::SERVICE_NAME_TITLE] = $item->getName();
            $report[$key][Constant::SERVICE_STATUS_TITLE] = $item->getStatus();
            $report[$key][Constant::SERVICE_DESCRIPTION] = $item->getDescription();
        }
        $output['data'] = $report;

        return $output;
    }
}
