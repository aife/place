<?php

namespace Atexo\SuperviseurServicesBundle\Services;

use Atexo\AppBundle\Service\CurlWrapper;

/**
 * @author Mohamed Blal
 * Class CryptoService
 */
class CryptoService extends AbstractService
{
    /**
     * @var CurlWrapper
     */
    protected $curl;

    /**
     * @var
     */
    protected $serveurCrypto;

    /**
     * @var string
     */
    protected $mpeURLPrivee;

    /**
     * @var string
     */
    protected $mpeFilePath;
    /**
     * @var string
     */
    protected $mpeLogin;

    /**
     * @var string
     */
    protected $mpePassword;

    /**
     * @var string
     */
    protected $format;
    /**
     * @var bool
     */
    private $enLigne;

    /**
     * CryptoService constructor.
     *
     * @param $crypto
     */
    public function __construct(CurlWrapper $curl, string $url, int $timeout, $crypto, string $mpeURLPrivee, string $mpeFilePath, string $mpeLogin, string $mpePassword, string $format, bool $enLigne)
    {
        //parent::__construct();
        $this->curl = $curl;
        $this->host = $url;
        $this->timeout = $timeout;
        $this->serveurCrypto = $crypto;
        $this->mpeURLPrivee = $mpeURLPrivee;
        $this->mpeFilePath = $mpeFilePath;
        $this->mpeLogin = $mpeLogin;
        $this->mpePassword = $mpePassword;
        $this->format = $format;
        $this->enLigne = $enLigne;
    }

    public function verify()
    {
        $params = [];

        $params['MpeURLPrivee'] = $this->mpeURLPrivee;
        $params['ServeurCryptoURLPublic'] = $this->host;
        $params['MpeFilePath'] = $this->mpeFilePath;
        $params['MpeLogin'] = $this->mpeLogin;
        $params['MpePassword'] = $this->mpePassword;
        $params['Format'] = $this->format;
        $params['EnLigne'] = $this->enLigne;

        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $this->timeout);
        $result = $this->serveurCrypto->verifierDisponibilite($params);
        ini_set('default_socket_timeout', $defaultTimeout);

        $this->name = Constant::PLATEFORME_CRYPTO;

        $returnObject = json_decode($result, false);
        if (null === $returnObject) {
            $this->status = Constant::STATUS_KO;
            $this->description = Constant::MSG_ERROR_CRYPTO;

            return;
        }
        if (201 !== $returnObject->code) {
            $this->status = Constant::STATUS_KO;
        }
        $this->description = $returnObject->message;

        //trigger_error(serialize($this),E_USER_ERROR);
    }
}
