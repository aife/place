<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed Blal
 * Class Constant
 */
class Constant
{
    const DB_SERVICE_NAME = 'SERVEUR DE BASE DE DONNEES';
    const MAIL_SERVICE_NAME = 'SERVEUR DE MAIL';
    const PLATEFORME_MARCHES_ONLINE = 'Marchés on line (MOL)';
    const PLATEFORME_CHORUS = 'CHORUS';
    const PLATEFORME_CRYPTO = 'SERVEUR CRYPTO SIGNATURE';
    const FILE_READ_RIGHTS = 'DROIT DE LECTURE';
    const FILE_WRITE_RIGHTS = 'DROIT D\'ECRITURE';

    const SERVICE_NAME_TITLE = '#SERVICENAME';
    const SERVICE_STATUS_TITLE = '#SERVICESTATUT';
    const SERVICE_DESCRIPTION = '#SERVICESTATUTDESCRIPTION';

    const STATUS_KO = 'KO';
    const STATUS_OK = 'OK';

    const MSG_ERROR_CRYPTO = 'Le SEREVUR NE REPOND PAS';
}
