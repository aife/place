<?php

namespace Atexo\SuperviseurServicesBundle\Services;

use Atexo\AppBundle\Service\CurlWrapper;

/**
 * @author Mohamed Blal
 *
 * Class CHORUSService
 */
class CHORUSService extends AbstractService
{
    /**
     * @var CurlWrapper
     */
    protected $curl;

    public function __construct(CurlWrapper $curl, string $url, int $timeout)
    {
        //parent::__construct();
        $this->curl = $curl;
        $this->host = $url;
        $this->timeout = $timeout;
    }

    public function verify()
    {
        $result = $this->curl->get($this->host, $this->timeout);
        $this->name = Constant::PLATEFORME_CHORUS;
        if (!$result) {
            $this->curl->getLastError();
            $this->status = Constant::STATUS_KO;
            $this->description = $this->curl->getLastError();
        }
        //trigger_error(serialize($this),E_USER_ERROR);
    }
}
