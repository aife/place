<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed Blal
 *
 * Class ServiceDirectory
 */
class NetworkServiceDirectory
{
    public function getServices()
    {
        return [
            'atexo_superviseur_services.smtp_service',
            'atexo_superviseur_services.database_service',
            'atexo_superviseur_services.mol_service',
            'atexo_superviseur_services.chorus_service',
            'atexo_superviseur_services.crypto_service',
            //'atexo_superviseur_services.file_service'
        ];
    }
}
