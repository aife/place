<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed Blal
 *
 * Class ExceptionHandler
 */
trait ControlHandler
{
    /**
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     */
    public function handler($errno, $errstr, $errfile, $errline)
    {
        $list = unserialize($errstr);

        if (!empty($list)) {
            static::$state[] = $list;
        }
    }
}
