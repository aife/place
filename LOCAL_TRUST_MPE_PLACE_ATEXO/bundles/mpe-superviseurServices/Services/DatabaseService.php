<?php

namespace Atexo\SuperviseurServicesBundle\Services;

use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @author Mohamed BLAL
 *
 * Class DatabaseService
 */
class DatabaseService extends AbstractService
{
    /**
     * @var RegistryInterface
     */
    protected $doctrine;

    /**
     * DatabaseService constructor.
     *
     * @param $timeout
     */
    public function __construct(RegistryInterface $doctrine, int $timeout)
    {
        //parent::__construct();
        $this->doctrine = $doctrine;
        $this->timeout = $timeout;
    }

    public function verify()
    {
        $this->name = Constant::DB_SERVICE_NAME;
        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $this->timeout);
        try {
            $this->doctrine->getConnection()->connect();
            $this->doctrine->getConnection()->close();
        } catch (\Exception $e) {
            $this->status = Constant::STATUS_KO;
            $this->description = $e->getMessage();
        }
        ini_set('default_socket_timeout', $defaultTimeout);
        //trigger_error(serialize($this), E_USER_ERROR);
    }
}
