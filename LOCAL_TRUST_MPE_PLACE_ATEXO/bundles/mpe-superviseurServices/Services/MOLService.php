<?php

namespace Atexo\SuperviseurServicesBundle\Services;

use Atexo\AppBundle\Service\CurlWrapper;

/**
 * @author Mohamed Blal
 *
 * Class MOLService
 */
class MOLService extends AbstractService
{
    protected $curl;

    /**
     * MOLService constructor.
     *
     * @param $url
     * @param $timeout
     */
    public function __construct(CurlWrapper $curl, string $url, int $timeout)
    {
        parent::__construct();
        $this->curl = $curl;
        $this->host = $url;
        $this->timeout = $timeout;
    }

    public function verify()
    {
        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $this->timeout);
        $result = $this->curl->get($this->host);
        $this->name = Constant::PLATEFORME_MARCHES_ONLINE;
        if (!$result) {
            $this->curl->getLastError();
            $this->status = Constant::STATUS_KO;
            $this->description = $this->curl->getLastError();
        }
        ini_set('default_socket_timeout', $defaultTimeout);
        //trigger_error(serialize($this),E_USER_ERROR);
    }
}
