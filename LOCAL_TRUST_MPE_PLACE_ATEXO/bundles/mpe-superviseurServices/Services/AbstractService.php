<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed BLAL
 *
 * Class AbstractService
 */
abstract class AbstractService //implements \Serializable
{
    //use ControlHandler;

    protected static $state = [];

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $port;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $status = Constant::STATUS_OK;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $timeout;

    /**
     * AbstractService constructor.
     */
    public function __construct()
    {
        //set_error_handler(array($this, 'handler'));
    }

    /**
     * Test the server connection.
     *
     * @return mixed
     */
    abstract public function verify();

    public function ping()
    {
        $this->verify();
        static::$state[] = $this;
    }

    /**
     * @return array
     */
    public static function getState()
    {
        return static::$state;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /*
    public function serialize()
    {
        return serialize([
            $this->name,
            $this->status,
            $this->description,
        ]);
    }

    public function unserialize($serialized)
    {
        return list(
            $this->name,
            $this->status,
            $this->description
            ) = unserialize($serialized);
    }*/
}
