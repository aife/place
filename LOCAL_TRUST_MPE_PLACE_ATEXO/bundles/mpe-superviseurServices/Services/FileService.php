<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed Blal
 *
 * Class FileService
 */
class FileService extends AbstractService
{
    /**
     * @var array
     */
    protected $files;

    public function __construct($files = [])
    {
        //parent::__construct();
        $this->files = $files;
    }

    public function verify()
    {
        $rules = [];
        foreach ($this->files as $file) {
            // Lecture
            $isReadable = is_readable($file) ? Constant::STATUS_OK : Constant::STATUS_KO;
            $this->status = $isReadable;
            $this->description = ''; //$file;
            $this->name = Constant::FILE_READ_RIGHTS.': '.$file;
            //trigger_error(serialize($this),E_USER_ERROR);
            //throw new ReadableException($isReadable);
            $rules[] = clone $this;

            // Ecriture

            $isWritable = is_writable($file) ? Constant::STATUS_OK : Constant::STATUS_KO;
            $this->status = $isWritable;
            $this->description = ''; //$file;
            $this->name = Constant::FILE_WRITE_RIGHTS.': '.$file;
            //throw new WritableException($isWritable);
            //trigger_error(serialize($this),E_USER_ERROR);
            $rules[] = clone $this;
        }

        return $rules;
    }
}
