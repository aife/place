<?php

namespace Atexo\SuperviseurServicesBundle\Services;

/**
 * @author Mohamed Blal
 * Class SMTPService
 */
class SMTPService extends AbstractService
{
    /**
     * SMTPService constructor.
     */
    public function __construct(string $host, int $port = null, int $timeout)
    {
        //parent::__construct();
        $fields = [];

        // traitement valable pour l'ipv4
        if (empty($port)) {
            $fields = explode(':', $host);
        } else {
            $this->host = $host;
            $this->port = $port;
        }
        if (2 === count($fields)) {
            $this->host = $fields[0];
            $this->port = (int) $fields[1];
        }
        $this->timeout = $timeout;
    }

    public function verify()
    {
        $connection = @fsockopen($this->host, $this->port, $errno, $errstr, $this->timeout);
        $this->name = Constant::MAIL_SERVICE_NAME;
        if (!$connection) {
            $this->status = Constant::STATUS_KO;
            $this->description = $errstr;
        }
        //trigger_error(serialize($this), E_USER_ERROR);

        if (is_resource($connection)) {
            fclose($connection);
        }
    }
}
