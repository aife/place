/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import Promise from 'es6-promise';
Promise.polyfill();

import '../css/fonts.scss';

import './app-assets/fonts/feather/style.css';
import './app-assets/fonts/flag-icon-css/sass/flag-icon.scss';
import './app-assets/fonts/line-awesome/css/line-awesome.css';

import './scss/bootstrap.scss';
import './scss/bootstrap-extended.scss';
import './scss/components.scss';
import './scss/colors.scss';

import './scss/core/menu/menu-types/horizontal-menu.scss';
import './scss/core/colors/palette-gradient.scss';

import './scss/pages/login-register.scss';

import './mpe.scss';
import  './scss/plugins/pickers/daterange/daterange.scss';
import  './js/scripts/pickers/daterange/daterangepicker';
