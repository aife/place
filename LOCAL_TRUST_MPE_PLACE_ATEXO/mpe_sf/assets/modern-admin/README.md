
# Le thème Modern Admin pour la partie Agent (SF)

## Documentation de thème
La documentation est accessible en ligne :
[https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/documentation/](https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/documentation/)


## Fichiers à ne pas modifier
Afin d'assurer une éventuelle mise à jour du thème Modern Admin,
il est **interdit** de modifier les fichiers se trouvant dans le
répertoire `mpe_sf/assets/modern-admin/` à l'exception des fichiers
suivants :

* mpe.scss
* indexElaboration.js
* scss/core/variables/_bootstrap-variables.scss
* scss/core/variables/_components-variables.scss
* scss/core/variables/_material-variables.scss
* scss/core/colors/palette-variables.scss
* scss/core/colors/material-palette-variables.scss

Cette recommendation est clairement indiquée dans la documentation 
du thème :
https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/documentation/documentation-color-customization.html#scss-way

Un hook de pre-commit Git a été créé pour empêcher la modification
de ces fichiers.

## PurgeCss

### Présentation
Un plugin Webpack a été ajouté à l'occasion de la mise en place du thème
Modern Admin : [PurgeCss](https://purgecss.com/).

Documentation : https://purgecss.com/

Ce plugin permet de réduire fortement la taille des feuilles de style
lors du build tout en offrant toutes les possibilités du thème Modern Admin.

Pour cela, il scanne les fichiers `*.html.twig` et `*.vue` des
répertoires `templates/` et `assets/components/`
et supprime toutes les classes CSS non utilisées.

La configuration de ce plugin se trouve dans le fichier `webpack.config.js`

Cette cure de minceur est appliquée sur **toutes** les feuilles de styles
**générées par Webpack**.

### Points d'attention / risques
Certaines classes peuvent être supprimées à tort par le plugin.

Il est important dans les fichiers Twig de ne pas construire les classes
avec des conditions. Par ex:

