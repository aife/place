import $ from "jquery";

$('.nav-link').on('click', function(event){
    if ($(this).parent().hasClass('open')){
        $(this).parent().removeClass('open');

        return false;
    }

    $('.nav-link').each(function(){
        $(this).parent().removeClass('open');
    });

    $(this).parent().addClass('open');
    event.stopPropagation();
});


$(document).click(function(event){
    var $navMPE = $('#main-nav-MPE');

    if (!$navMPE.is(event.target) && !$navMPE.has(event.target).length){
        $navMPE.find('.nav-item').removeClass('open');
    }
});
