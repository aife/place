import { createApp } from 'vue';
import header from './header-statistics';

const agentStatistics = createApp({});

agentStatistics.component('header-statistics', header);
agentStatistics.mount('#agentHeaderStatistics');
