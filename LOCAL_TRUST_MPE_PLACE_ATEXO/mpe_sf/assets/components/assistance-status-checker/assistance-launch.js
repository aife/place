/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { createApp } from 'vue';
import axios from 'axios';
import {Base64} from 'js-base64';

var elem = document.getElementById('assistance-launch');
var addPersonal = document.getElementById('add-personal');
var addRelief = document.getElementById('add-relief');

var assistanceLocalhostPort = 11992;
var pathAssistance = 'http://localhost:' + assistanceLocalhostPort + '/health';
var pathAssistanceLaunch = 'http://localhost:' + assistanceLocalhostPort + '/run-local-application';


if (addPersonal !== null && elem !== null) {
    var sessionId = elem.getAttribute('data-session-id');
    var callBackUrl = elem.getAttribute('data-callback-url');
    var pathCrypto = window.location.origin + '/crypto'
    var otherChecked = document.getElementById('ctl0_CONTENU_PAGE_ChoixEntiteAchat_autreEntite');
    var serviceId = document.getElementById('ctl0_CONTENU_PAGE_ChoixEntiteAchat_listeAutreEntites');

    createApp({
        el: '#add-personal',
        methods: {
            verifyConnection: function (isTest = 0) {
                if (null !== otherChecked && null !== serviceId && true === otherChecked.checked) {
                    callBackUrl = callBackUrl + '&serviceId=' + serviceId.value;
                }
                axios
                    .get(pathAssistance)
                    .then(response => {
                        if (isTest > 0) {
                            this.assistanceTest();
                        } else {
                            let contextJsonString = {
                                estCleChiffrementSecours: false,
                                ticket: sessionId,
                                callbackUrl: callBackUrl,
                            };
                            axios
                                .post(pathCrypto + '/cryptographie/local-server/dual-key', contextJsonString)
                                .then(response => {
                                    this.assistanceLaunch(response.data);
                                    });
                        }
                        },
                        (error) => {
                            showModalAssistance();
                        });

            },
            assistanceLaunch: function (contextJsonString) {
                axios
                    .post(pathAssistanceLaunch, contextJsonString)
                    .then(function (response) {
                    });
            },
            assistanceTest: function (contextJsonString) {
                axios
                    .post(pathAssistanceLaunch, contextJsonString)

                    .then(function (response) {

                    });
            }
        }
    }).mount('#add-personal');
}


if (addRelief !== null && elem !== null) {
    var pathCrypto = window.location.origin + '/crypto'
    var sessionId = elem.getAttribute('data-session-id');
    var callBackUrl = elem.getAttribute('data-callback-url');
    var otherChecked = document.getElementById('ctl0_CONTENU_PAGE_ChoixEntiteAchat_autreEntite');
    var serviceId = document.getElementById('ctl0_CONTENU_PAGE_ChoixEntiteAchat_listeAutreEntites');

    createApp({
        el: '#add-relief',
        methods: {
            verifyConnection: function (isTest = 0) {
                if (null !== otherChecked && null !== serviceId && true === otherChecked.checked) {
                    callBackUrl = callBackUrl + '&serviceId=' + serviceId.value;
                }
                axios
                    .get(pathAssistance)
                    .then(response => {
                        if (isTest > 0) {
                            this.assistanceTest();
                        } else {
                            let contextJsonString = {
                                estCleChiffrementSecours: true,
                                ticket: sessionId,
                                callbackUrl: callBackUrl,
                            };
                            axios
                                .post(pathCrypto + '/cryptographie/local-server/dual-key', contextJsonString)
                                .then(response => {
                                    this.assistanceLaunch(response.data);
                                    });
                        }
                        },
                        (error) => {
                            showModalAssistance();
                        });
            },
            assistanceLaunch: function (contextJsonString) {
                axios
                    .post(pathAssistanceLaunch, contextJsonString)
                    .then(function (response) {
                    });
            },
            assistanceTest: function (contextJsonString) {
                axios
                    .post(pathAssistanceLaunch, contextJsonString)

                    .then(function (response) {

                    });
            }
        }
    }).mount('#add-relief');
}

if (elem !== null) {
    var pathCrypto = window.location.origin + '/crypto'
    var platform = elem.getAttribute('data-plateform');
    createApp({
        el: '#assistance-launch',
        methods: {
            verifyConnection: function (isTest = 0) {
                axios
                    .get(pathAssistance)
                    .then(response => {
                        if (isTest > 0) {
                            this.assistanceTest();
                        } else {
                            let contextJsonString = {
                                appId: 'APPLICATION_SIGNATURE',
                                plateforme: platform,
                                urlPubliqueServeurCrypto: pathCrypto,
                            };

                            if (document.getElementById('assistance-launch')) {
                                if (document.getElementById('assistance-launch').getAttribute('data-context')) {
                                    const context = document.getElementById('assistance-launch').getAttribute('data-context');
                                    contextJsonString = Base64.decode(context);
                                    const contextJson = JSON.parse(contextJsonString);
                                    contextJson.urlServeurUpload = pathCrypto + '/cryptographie/'
                                    contextJsonString = JSON.stringify(contextJson);
                                }
                            }
                            this.assistanceLaunch(contextJsonString);
                        }
                        },
                        (error) => {
                            showModalAssistance();
                        });
            },
            assistanceLaunch: function (contextJsonString) {
                axios
                    .post(pathAssistanceLaunch, contextJsonString)
                    .then(function (response) {
                    });
            },
            assistanceTest: function () {
                axios
                    .post(pathAssistanceLaunch, {
                        appId: 'APPLICATION_TEST_CONFIGURATION',
                        urlPubliqueServeurCrypto: pathCrypto,
                        useragent: navigator.userAgent
                    })
                    .then(function (response) {

                    });
            }
        }
    }).mount('#assistance-launch');
}

