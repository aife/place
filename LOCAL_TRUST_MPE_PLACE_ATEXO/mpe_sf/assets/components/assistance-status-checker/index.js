/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { createApp } from 'vue';
import AssistanceStatusChecker from './assistance-status-checker.vue';

// Créez une application Vue
const assistanceStatusChecker = createApp(AssistanceStatusChecker);


assistanceStatusChecker.mount('#assistance-status-checker');

