/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'bootstrap';

jQuery("body").append('<div id="popinRgpd" ></div>');
jQuery('#popinRgpd').load(pfUrl + (isAgent ? 'agent':'entreprise') + '/rgpd', function () {
    jQuery('#verificationRgpdModal').modal('show')
});

function rgpdSaveChoice()
{
    jQuery.ajax({
        url: pfUrl + (isAgent ? 'agent':'entreprise') + '/rgpd/save',
        type: "POST",
        data: {
            communicationPlaceRgpd: jQuery('#communicationPlaceRgpd').is(':checked'),
            enqueteRgpd: jQuery('#enqueteRgpd').is(':checked'),
            communicationRgpd: jQuery('#communicationRgpd').is(':checked')
        }
    }).done(function () {
        jQuery('#verificationRgpdModal').modal('hide');
        jQuery('#popinRgpd').remove();
        jQuery('.modal-backdrop').remove();
    });
}

function rgpdDisableAllChoicesAndSave()
{
    const elementIds = ['communicationPlaceRgpd', 'enqueteRgpd', 'communicationRgpd'];

    for (const element of elementIds) {
        let elem = jQuery('#' + element);
        elem.prop('checked', false);
        elem.parent().removeClass('btn-success');
        elem.parent().addClass('btn-default');
        elem.parent().addClass('off');
    }
    rgpdSaveChoice();
}

window.rgpdSaveChoice = rgpdSaveChoice;
window.rgpdDisableAllChoicesAndSave = rgpdDisableAllChoicesAndSave;
