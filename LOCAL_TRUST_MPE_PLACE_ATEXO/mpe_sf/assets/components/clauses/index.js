/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { createApp } from 'vue';
import ClausesN1 from "./ClausesN1.vue";
import axios from "axios";

const Emitter = require('tiny-emitter');

createApp({
    components: {
        'clauses-n1': ClausesN1
    },
    data() {
        return {
            tinyEmitter: new Emitter(),
            disabled: false,
            page: '',
            valuesSave: [],
            values: [],
            clauses: [],
            token: '',
            url: '',
            isValidClauses: false
        }
    },
    methods: {
        getData() {
            const options = {
                headers: { Authorization: `Bearer ${this.token}` }, params: { actif : true }
            };
            axios.get("/api/v2/referentiels/clauses-n1s", options).then(response => {
                if (response.data['hydra:totalItems'] > 0) {
                    this.values = response.data['hydra:member'];
                }
            });
        },
        getToken() {
            axios.get('/agent/token-user-connected').then(response => {
                this.token = response.data;
            });
        },
        add(payload) {
            if (!this.exists(payload.data.referentielClauseN1)) {
                this.clauses.push(payload.data);
            }
        },
        remove(payload) {
            this.clauses = this.clauses.filter(element => element.referentielClauseN1 !== payload.referentielClauseN1);
        },
        exists(id) {
            return this.clauses.find((element => element.referentielClauseN1 === id)) instanceof Object;
        },
        validate() {
            this.tinyEmitter.emit('send-validate-clause-n2');
        },
        disable() {

            let queryString = window.location.search;
            let urlParams = new URLSearchParams(queryString);
            let locationFilter = '';
            if ( urlParams.has('page') ) {
                locationFilter = urlParams.get('page');
                if (locationFilter === 'Agent.PopupDetailLot') {
                    this.disabled = true;
                }
            }
        },
    },
    computed: {
        formValueToJson() {
            return JSON.stringify(this.clauses);
        },
    },
    watch: {
        token() {
            this.getData();
        }
    },
    created() {
        this.getToken();
        this.tinyEmitter.on('send-validate-clauses', (value) => {
            this.isValidClauses = value;
        });
    },
    mounted: function () {
        window.achatResponsable = this;
        this.disable()

    },
}).component('clauses-n1', ClausesN1).mount('#achat-responsable');
