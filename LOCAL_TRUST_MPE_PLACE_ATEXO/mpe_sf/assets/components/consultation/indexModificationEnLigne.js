import { createApp } from 'vue';
import ModificationConsultationEnLigne from './ModificationConsultationEnLigne';
import Notifications from '@kyvg/vue3-notification';

createApp({
    components: {
        'modification-consultation': ModificationConsultationEnLigne,
    }
}).use(Notifications).mount('#modification-consultation');
