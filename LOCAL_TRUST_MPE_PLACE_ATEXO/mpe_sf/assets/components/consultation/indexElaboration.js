import { createApp } from 'vue';
import ElaborationConsultation from './ElaborationConsultation';
import Notifications from '@kyvg/vue3-notification';

createApp({
    components: {
        'elaboration-consultation': ElaborationConsultation,
    }
}).use(Notifications).mount('#elaboration-consultation');
