import axios from 'axios';

export default {
    name: "DecisionConsultation",
    props: [
        'id',
        'labelNumberOfResults',
        'labelDisplay',
        'labelGoToTheFirstPage',
        'labelGoToPreviousPage',
        'labelGoToNextPage',
        'labelGoToTheLastPage',
        'labelLotsAttributes',
        'labelStatus',
        'labelResultsPerPage',
        'labelPageNumber',
        'labelDateOfNotification',
        'labelLinkToTheContract',
        'labelNumberOfResultsPerPage',
        'labelContractDataToBeEntered',
        'labelNotificationStep',
        'labelStepNotificationPerformed',
        'labelDatePrevisionnelleNotification',
        'labelNumeroContrat'
    ],
    data() {
        return {
            contrats: [],
            entreprises: [],
            limit: 10,
            start: 0,
            end: 10,
            page: 1,
            contrat: null,
            styleNotification: 'display: flex; flex-direction: column; align-items: flex-start; padding-left: 15px;'
        }
    },
    methods: {
        getContrats: function () {
            let path = "agent/contrat/lot/" + this.id;
            let $vm = this;
            axios.get(path).then(resp => {
                $vm.getData(resp.data);
            });
        },
        onSubmit: function () {
            if(isNaN(this.page)){
                this.page = 1;
            }
            if(this.page <= 1) {
                this.start = 0;
                this.end = this.limit;
            } else {
                this.start = this.limit + this.start;
                this.end = this.end * this.page;
            }
        },
        onChange: function () {
            this.start = 0;
            this.end = this.limit;
        },
        getNumberOfPages: function () {
            let numberOfPages = 1;
            if(this.entreprises.length > 0) {
                numberOfPages = Math.ceil(this.entreprises.length / this.limit);
            }
            return numberOfPages;
        },
        getStyle: function (index) {
            if (index % 2 === 0) {
                return 'background-color: #edebeb';
            }
            return 'background-color: #fff';
        },
        getLink: function (contrats) {
            let contrat = this.getContrat(contrats);

            return '/?page=Agent.ResultatRechercheContrat&uuid=' + contrat.uuid;
        },
        getFormatDate: function (date) {
            var today  = new Date(date);
            return today.toLocaleDateString("fr-FR");
        },
        getData: function (contrats) {
            let data = [];
            console.log(contrats)
            contrats.forEach((item) => {
                if(!this.entreprises.includes(item.entrepriseId)){
                    this.entreprises.push(item.entrepriseId);
                }

                if(typeof data[item.entrepriseId] == "undefined") {
                    data[item.entrepriseId] = [];
                }

                data[item.entrepriseId].push(item);
            });
            this.contrats = data;
        },
        getContrat: function (contrats) {
            return [...contrats].shift();
        },
        goLastPage: function () {
            this.page = this.getNumberOfPages();
            this.start = this.limit * (this.page -1);
            this.end = this.end * this.page;
        },
        goFirstPage: function () {
            this.page = 1;
            this.onSubmit();
        },
        goNextPage: function () {
            if (this.page < this.getNumberOfPages()) {
                this.page++;
                this.onSubmit();
            }
        },
        goPrevPage: function () {
            if (this.page > 1) {
                --this.page;
                this.start = this.start - this.limit;
                this.end = this.limit * this.page;
            }
        }
    },
    mounted() {
        this.getContrats();
    }
}