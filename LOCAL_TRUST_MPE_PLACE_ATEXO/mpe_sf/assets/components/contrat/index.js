import { createApp } from 'vue';
import DecisionConsultation from "./decision-consultation.vue";

createApp({
    el: '#decision-consultation',
    data: {
    },
    components: {
        "decision-consultation": DecisionConsultation
    },
    methods: {

    }
}).mount('#decision-consultation');
