import { createApp } from 'vue';
import DefineCriteria from './define-criteria.vue';
import DefineBaremeAllotie from './define-bareme-allotie.vue'
import axios from 'axios';

createApp({
    components: {
        'define-criteria': DefineCriteria,
        'define-bareme-allotie': DefineBaremeAllotie
    },
    data() {
        return {
            isAllotie: null,
            criteriaList: [],
            criteriaListFinalAllotie: [],
            totalRateNumber: 0,
            serverError: false,
            identical: null,
            lots: [],
            showNextButton: true,
            showSaveButton: false,
            lotCriteriaReady: false
        }
    },
    methods: {
        setDataAllotie(data) {
            if (data == '[]') {
                this.isAllotie = false;
            } else {
                this.isAllotie = true;
            }
        },
        setShowSaveButton: function (payload) {
            this.showSaveButton = payload.showSaveButton;
        },
        setLotCriteriaReady: function (payload) {
            this.lotCriteriaReady = payload.lotCriteriaReady;
        },
        saveCriterias: function () {
            let vueModel = this
            let data = '';
            if (this.isAllotie) {
                if (this.identical) {
                    for (let i = 0; i < this.lots.length; i++) {
                        this.createFinalCriteriaListAllotie(this.lots[i].id)
                    }
                    data = JSON.stringify({'criteria': this.criteriaListFinalAllotie});
                } else {
                    data = JSON.stringify({'criteria': this.criteriaList});
                }
            } else {
                data = JSON.stringify({'criteria': this.criteriaList});
            }

            let url = '/agent/analyse-offre/critere-attribution/definir';
            let config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            axios.post(
                url,
                {'dataAjax': data},
                config
            )
            .then(response => {
                location.reload();
            })
            .catch(function (error) {
                vueModel.serverError = true;
            });
        },
        setTotalRate: function (payload) {
            this.totalRateNumber = payload.total

        },
        setIdentical: function (payload) {
            this.identical = payload.identical;
        },
        goNextStepDefineCriteriaAllotie: function (lots) {
            for (let i = 0; i < lots.length; i++) {
                lots[i].criteria = null;
                lots[i].total = 0;
                this.lots.push(lots[i])
            }
            this.showNextButton = false;
        },
        createFinalCriteriaListAllotie: function (idLot) {
            let cloneCriteriaList = JSON.parse(JSON.stringify((this.criteriaList)));
            for (let i = 0; i < cloneCriteriaList.length; i++) {
                let criteria = cloneCriteriaList[i]
                criteria.id_lot = idLot;
                this.criteriaListFinalAllotie.push(criteria) ;
            }
        }
    }
}).mount('#modalCriteriaAttributionToDefine');