/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.min';
import '../../css/espace-documentaire.scss';

import $ from 'jquery';
import { createApp } from 'vue';
import Promise from 'es6-promise';
Promise.polyfill();
import axios from 'axios';
import fileSize from "filesize";

import PieceDCE from "./piece-dce";
import PliAttributaire from "../espace-documentaire/pli-attributaire";
import PieceDepot from "../espace-documentaire/piece-depot";
import PiecePub from "../espace-documentaire/piece-pub";
import PieceLibre from "../espace-documentaire/piece-libre";
import PieceModele from "../espace-documentaire/piece-modele";
import Notifications from '@kyvg/vue3-notification';

// Vue.filter('truncate', function (text, length, clamp) {
//     clamp = clamp || '...';
//     var node = document.createElement('div');
//     node.innerHTML = text;
//     var content = node.textContent;
//     return content.length > length ? content.slice(0, length) + clamp : content;
// });

const espaceDoc = createApp({
    data() {
        return {
            locale: '',
            downloadOK: false,
            noFilesSelected: false,
            errorCreateDownload: false
        }
    },
    components: {
        'piece-dce': PieceDCE,
        'pli-attributaire': PliAttributaire,
        'piece-depot': PieceDepot,
        'piece-pub': PiecePub,
        'piece-libre': PieceLibre,
        'piece-modele': PieceModele
    },
    methods: {
        getSize(octets) {
            return fileSize(octets);
        },
        isArray(data) {
            return Array.isArray(data);
        },
        hideAlert() {
            this.downloadOK = false;
            this.noFilesSelected = false;
            this.errorCreateDownload = false;
        },
        download(baseUrl, id) {
            var myArray = [];

            $("input:checkbox:checked").each(function () {
                if ($(this).val() !== 'checkboxManager') {
                    myArray.push($(this).val());
                }
            });
            if (myArray.length > 0) {
                var data = JSON.stringify(myArray);

                var apiUrl = baseUrl + '/agent/espace-documentaire/create-download/' + id + '.json';
                axios.post(apiUrl, {'data': myArray}).then(response => {
                    var wsResponse = response.data.mpe.reponse.data;
                    if (wsResponse) {
                        if (wsResponse.message == 'OK') {
                            this.downloadOK = true;
                        } else {
                            this.errorCreateDownload = true;
                        }
                    }
                });
            } else {
                this.noFilesSelected = true;
            }
            $('#modalInformation').modal('show');
        }
    },
    beforeMount: function () {
        // this.locale = this.$el.attributes['data-locale'].value;
    }
});

espaceDoc.use(Notifications).mount('#root_espace_doc');

$(document).ready(function () {
    $("#search-lot").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#select-lots div").filter(function () {
            if (value != '') {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            } else {
                $(this).css('display', 'block');
            }
        });
    });

    $("#search-entreprise").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#select-entreprise div").filter(function () {
            if (value != '') {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            } else {
                $(this).css('display', 'block');
            }
        });
    });
});
