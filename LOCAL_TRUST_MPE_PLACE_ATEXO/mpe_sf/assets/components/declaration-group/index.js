/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import Vue from 'vue2/dist/vue';
import axios from 'axios';
import LayerGroupementType from './layer-groupement-type.vue';
import LayerGroupementSousTraitant from './layer-groupement-sous-traitant';
import $ from "jquery";

var uid = document.getElementById('form_uid_response').getAttribute('value');
var idGroupeEntreprise = document.getElementById('id_groupe_entreprise');


var $groupement = new Vue({
    el: '#groupement',
    data: {
        picked: 0,
        display: 'none',
        uid: 0,
        displayPanelMessage: 'none',
        displayCotraitant: 'block',
        modalAlertDelete: false
    },
    methods: {
        createGroupement()
        {
            let contextJsonString = {
                groupementOui: this.picked,
                uid_response: uid,
                refConsultation: consulation_ref
            };

            axios
                .post(Routing.generate('atexo_groupement_oui'), contextJsonString)
                .then(function (response) {
                    idGroupeEntreprise.value = response.data;
                    $groupement.setIdGroupeEntreprise();
                });
        },
        showModalAlertDeleteGroupement()
        {
            this.modalAlertDelete = true;
        },
        closeModalAlertDelete()
        {
            this.modalAlertDelete = false;
            this.setIdGroupeEntreprise();
        },
        //delete Groupement
        confirmDelete()
        {
            let contextJsonString = {
                idGroupementEntreprise: idGroupeEntreprise.getAttribute('value')
            };

            let $vm = this;
            axios
                .post(Routing.generate('atexo_groupement_supprimer_groupement'), contextJsonString)
                .then(function (response) {
                    idGroupeEntreprise.value = 0;
                    $vm.closeModalAlertDelete();
                });
        },
        setIdGroupeEntreprise()
        {
            if (idGroupeEntreprise.getAttribute('value') > 0) {
                this.picked = 1;
                this.display = 'block';
            } else {
                this.display = 'none';
            }
        },
        showErrorInvalidSiret(value)
        {
            this.displayPanelMessage = value;
        },
        showCoTraitant(value)
        {
            this.displayCotraitant = value;
        },
        viewResponse(value)
        {
            initVueCreationGroupement();
        }
    },
    mounted()
    {
        this.uid = uid;
        this.setIdGroupeEntreprise();
    },
    components: {
        'layer-groupement-type': LayerGroupementType,
        'layer-groupement-sous-traitant': LayerGroupementSousTraitant
    }
});

function initVueCreationGroupement() {
    new Vue({
        el: '#groupement-tableau-dynamique',
        data: {
            displayCotraitant: $groupement.displayCotraitant
        },
        methods: {
            viewResponse(value)
            {
                initVueCreationGroupement();
            }
        },
        components: {
            'layer-groupement-sous-traitant': LayerGroupementSousTraitant,
        }
    });
}

var $btn = null;

$('body').on('click', 'button.add-ligne-sous-traitant', function () {
    let id = $(this).attr('id');
    $(this).prop("disabled",true);
    $btn = $(this);
    let $line = $('.' + id);
    if ($line.css('display') == 'none') {
        $line.css('display', 'table-row');
    }
});

$('body').on('click', 'button.del-ligne-sous-traitant', function () {
    let $gparent = $(this).parents('tr');
    $($btn).removeAttr('disabled');

    if ($gparent.css('display') == 'table-row') {
        $gparent.css('display', 'none');
    }
});


