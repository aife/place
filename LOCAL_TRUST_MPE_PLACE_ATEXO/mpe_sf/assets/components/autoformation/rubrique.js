import './styles/rubrique.css';
import RubriqueForm from "./rubrique.vue";

new Vue({
    el: '#main-part',
    components: {
        "rubrique-form": RubriqueForm
    }
});