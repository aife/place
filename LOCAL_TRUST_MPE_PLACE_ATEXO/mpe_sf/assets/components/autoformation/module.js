import './styles/module.css';
import ModuleForm from "./module.vue";
import $ from 'jquery';

new Vue({
    el: '#main-part',
    components: {
        "module-form": ModuleForm
    }
});

$('.image-upload-wrap').bind('dragover', function () {
    let id = "#" + $(this).parent().parent().attr( "id" );
    $(id + ' .image-upload-wrap').addClass('image-dropping');
});
$('.image-upload-wrap').bind('dragleave', function () {
    let id = "#" + $(this).parent().parent().attr( "id" );
    $(id + ' .image-upload-wrap').removeClass('image-dropping');
});