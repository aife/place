import './styles/index.css';
import axios from 'axios';

new Vue({
    el: '#main-part',
    data: {
        idModule: '',
        page: ''
    },
    methods: {
        confirmDelete: function () {
            if( this.page && this.idModule) {
                let redirectUrl = '?page=Agent.ModuleAutoformation&access=' + this.page;
                let url =  '/' + this.page + '/module-autoformation/module/supprimer/' + this.idModule;

                axios
                    .post(url)
                    .then(response => {
                        if (response.data.success) {
                            window.location.href = redirectUrl
                        }
                    });
            }

        },
        setData: function (id, page) {
            this.idModule = id;
            this.page = page;
        }
    }
});