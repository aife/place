/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'bootstrap';

$(document).ready(function () {
    let modal = $('#modalImport');
    let selected;
    let urlDocumentGenere = '#';
    let btnAnalyze;
    if (modal.data('force')) {
        modal.modal('show');
    }

    $("body #close-modal").on("click", function() {
        $('div.modal-backdrop').remove();
        $('#modalImport').hide();
        $('body').removeClass('modal-open');
    });

    $('#form_importFichier').on('change',function () {
        let fileName = $(this).val();

        $(this).next('.custom-file-label').html(fileName);
    });

    $("#analyse-offre-list .iCheck-helper").on('click', function () {
         btnAnalyze = $('#open-modal');
         selected = [];

        $("#analyse-offre-list input[type='checkbox']").each(function () {
            let value = $(this).attr('value');
            if ($(this).is(":checked") && value != null) {
                selected.push(value);
            }
        });

        if (selected.length > 0) {
            btnAnalyze.removeAttr('disabled');
        } else {
            btnAnalyze.attr("disabled", true);
        }
    });

    $('.btn-import').on('click', function(e) {
        $('#modalImport').toggleClass('show');
        e.stopPropagation();
    });

    $('#open-modal').on('click', function () {
        let filename = 'Analyse_AF_' + getDateNowToString() + '.xlsx';
        let url = $(this).attr('data-url');
        Swal.mixin({
            progressSteps: ['1', '2', '3'],
        }).queue([
            {
                input: 'text',
                title: 'Nom du document',
                text: 'Veuillez saisir le nom du document à générer :',
                buttonsStyling: false,
                showCancelButton: true,
                cancelButtonClass: "btn btn-danger mr-1",
                cancelButtonText: "Annuler",
                confirmButtonText: 'Suivant &rarr;',
                confirmButtonClass: 'btn btn-primary',
                inputValue: filename,
                reverseButtons: true,
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                        if (value) {
                            resolve();
                            generateAnalyse(url, value);
                        } else {
                            resolve('Le nom du document est obligatoire');
                        }
                    })
                }
            },
            {
                title:'Génération en cours...',
                html: '<div class="loader-wrapper"><div class="loader-container"><div class="ball-pulse loader-primary"><div></div><div></div><div></div></div></div></div>',
                confirmButtonText: 'Suivant &rarr;',
                confirmButtonClass: 'btn btn-primary etape2 d-none'
            },
            {
                title:'Génération terminée',
                type: "success",
                text: "Le document généré peut désormais être consulté et édité depuis l'espace documentaire de la consultation.",
                showCancelButton: true,
                cancelButtonClass: "btn btn-secondary",
                cancelButtonText: "Fermer",
                confirmButtonClass: 'btn btn-primary etape3',
                confirmButtonText: 'Accéder au document généré &rarr;',
                reverseButtons: true,
            }

        ]).then(function (result) {
            if (result.value) {
                window.open(urlDocumentGenere, '_blank');
            }
        })
    });

    function getDateNowToString()
    {
        let now = new Date();

        let annee   = now.getFullYear();
        let mois    = ('0' + (now.getMonth() + 1)).slice(-2);
        let jour    = now.getDate();
        let heure   = now.getHours();
        let minute  = now.getMinutes();

        return annee + '-' + mois + '-' + jour + '_' + heure + 'h' + minute;
    }

    function generateAnalyse(url, name)
    {
        if (selected.length > 0) {
            $.ajax({
                url: url,
                type: "GET",
                data: {'blobIds': selected.join('-'), 'fileName': name},
                success: function (response) {
                    $('button.etape2').trigger("click");
                    urlDocumentGenere = response;
                },
                error: function (xhr) {
                    Swal.fire({
                        title: "Erreur",
                        text: "Une erreur est survenue lors de la génération du document.",
                        type: "error",
                        confirmButtonText: 'Fermer',
                        confirmButtonClass: 'btn btn-secondary',
                        buttonsStyling: false,
                    });
                }
            });
        }
    }
});
