/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import '../css/scss/recherche-organisme.scss';

new DataTable('#organismes', {
    order: [[3, 'desc']]
});