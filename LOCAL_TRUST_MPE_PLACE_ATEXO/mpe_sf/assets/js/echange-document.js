import '../css/styles.scss';
import '../css/echange-documentaire.scss';
import '../css/espace-documentaire-jeton.scss';

import 'bootstrap';

import J from 'jquery';
import 'jqueryui';

import "bootstrap-table";
import "bootstrap-table/dist/bootstrap-table-locale-all";




J(document).ready(function () {
    var listFiles = [];
    var listPrincipal = [];
    var listPrincipaux = [];
    var jeton = [];
    var selectTypageDocument = '';
    var pastell = {};
    var trFichierJeton;
    var trIndex = 0;
    var typagePrincipal = {};
    var typageAnnexe = {};
    var idClickPrimo = '';

    J('#closeModalEspaceDoc, #closeModalEspaceDocBtn').click(function () {
        J.each(J(".etape3"), function (key, value) {
            var list = J(this);
            if (list.prop('checked')) {
                list.prop('checked', false);
            }
        });

        J('#modalEspaceDoc').hide();
    });
    J('body').on('click', 'button.openModalEspaceDoc', function () {
        trFichierJeton = J(this).parent().parent();
        trIndex = J(trFichierJeton).attr('data-index');
        J('#modalEspaceDoc').show();
    });

    J('#closeModalEspaceDocJeton, #closeModalEspaceDocBtnJeton').click(function () {
        J.each(J(".jeton"), function (key, value) {
            var list = J(this);
            if (list.prop('checked')) {
                list.prop('checked', false);
            }
        });

        J('#modalEspaceDocJeton').hide();
    });

    J('body').on('click', 'button.openModalJeton', function () {
        idClickPrimo = J(this).attr('id');
        trFichierJeton = J(this).parent().parent();
        trIndex = J(trFichierJeton).attr('data-index');
        J('#modalEspaceDocJeton').show();
    });

    J(document).on("click", ".jetonSelect", function () {
        var elemClick = J(this);
        var id = elemClick.attr('id');
        J.each(J(".jetonSelect"), function (key, value) {
            var list = J(this);
            if (list.prop('checked')) {
                if (id != list.attr('id')) {
                    list.prop("checked", false);
                }
            }
        });
    });

    J('#btn-modal-jeton').click(function (event) {
        if (J("input:checkbox").is(':checked')) {
            J.each(J("#modalEspaceDocJeton input:checkbox"), function (key, value) {
                if (J(this).is(':checked')) {
                    var valueCheck = J(this).val();

                    cleanJeton();

                    if (valueCheck != "checkboxManager") {
                        jeton = [];
                        listPrincipaux[trIndex]['jetons'] = [];
                        jeton.push(valueCheck);

                        var element = J(this).parent().children("label").children("a");
                        var documentJoint = element[0].cloneNode(true);
                        var link =  documentJoint;

                        let uniqueId = Math.random().toString(36).substring(2) + Date.now().toString(36)
                        var id = 'etape2_' + uniqueId;

                        let icon = '<i class="fa fa-lg fa-trash" aria-hidden="true"></i>';
                        var elFichierPrincipal = '<tr id="delete_' + idClickPrimo + '" data-index-annexe="' + trIndex + '"><td><span class="display-tree"></span><span class="fichiers-annexes-jeton"></span><span id="' +
                            id + '"></span><span class="typageFichier hide">' + jetonSignatureElectronique + '</span></td><td><span id="type-jeton' +
                            id + '">' + jetonSignatureElectronique + '</span></td><td><button class="btn btn-danger delete-jeton" value="' + valueCheck + '">' +
                            icon + '</button></div></td></tr>';


                        J(trFichierJeton).after(elFichierPrincipal);
                        J('#' + id).html(link);

                        J(trFichierJeton).next().removeClass('hide')

                        J('.delete-jeton').on('click', function () {
                            let valueDelete = J(this).attr('value');
                            var trDelete = J(this).parent().parent();
                            let indexValue = J(this).parent().parent().attr('data-index-annexe');

                            if (listPrincipaux[indexValue]['jetons'].indexOf(valueDelete)  > -1) {
                                listPrincipaux[indexValue]['jetons'].splice(listPrincipaux.indexOf(valueDelete), 1);
                            }
                            J(trDelete).remove();
                        });

                        listPrincipaux[trIndex]['jetons'].push(valueCheck);
                    }

                    J(this).prop('checked',false);
                }
            });
        }

        J.each(J(".jeton"), function (key, value) {
            var list = J(this)
            if (list.prop('checked')) {
                list.prop('checked', false)
            }
        });

        J('#modalEspaceDocJeton').hide();
        closeAccordion();
    });

    J('#btn-modal').click(function () {
        if (J("input:checkbox").is(':checked')) {
            typageAnnexe = {};
            J.each(J("#modalEspaceDoc input:checkbox"), function (key, value) {
                if (J(this).is(':checked')) {
                    var valueCheck = J(this).val();
                    if (valueCheck != "checkboxManager") {
                        var doublon = false
                        J.each(J(".annexe"), function (key, data) {
                            if (J(this).attr('data-index-annexe') == trIndex && J(this).attr('data-value-annexe') == valueCheck) {
                                doublon = true;
                            }
                        });

                        if (doublon == false) {
                            listFiles.push(valueCheck);
                            var element = J(this).parent().children("label").children("a");
                            var documentJoint = element[0].cloneNode(true);
                            var link = documentJoint;

                            let uniqueId = Math.random().toString(36).substring(2) + Date.now().toString(36)
                            var id = 'etape2_' + uniqueId;

                            let icon = '<i class="fa fa-lg fa-trash" aria-hidden="true"></i>';
                            var elFichierPrincipal = '<tr class="annexe" id="annexe_' +
                            id + '" data-value-annexe="' + valueCheck + '" data-index-annexe="' + trIndex + '"><td><span class="display-tree"></span><span class="fichiers-annexes-file"></span><span id="' +
                            id + '"></span><span class="typageFichier hide"></span></td><td><span data-index="' + trIndex + '" id="type' +
                            id + '"></span></td><td><button class="btn btn-danger delete-annexe" value="' + valueCheck + '" >' + icon + '</button></div></td></tr>';
                            J(trFichierJeton).after(elFichierPrincipal);
                            J('#' + id).html(link);

                            let idSelect = Math.random().toString(36).substring(2) + Date.now().toString(36)
                            var replaceId = selectTypageDocument.replace('typageDoc-a-remplacer', idSelect)
                            replaceId = replaceId.replace('value-fichier-selected', valueCheck)


                            J('#type' + id).html(replaceId);

                            J('.delete-annexe').on('click', function () {
                                let idRemove = J(this).parent().parent().attr('id');
                                let indexValue = J(this).parent().parent().attr('data-index-annexe');
                                let valueDelete = J(this).attr('value');
                                if (listFiles.indexOf(valueDelete) > -1) {
                                    listFiles.splice(listFiles.indexOf(valueDelete), 1);
                                    listPrincipaux[indexValue]['annexes'].splice(listFiles.indexOf(valueDelete), 1);
                                    J.each(listPrincipaux[indexValue]['typage_annexe'], function (key, data) {
                                        if (typeof data != 'undefined' && typeof data[valueDelete] != 'undefined') {
                                            listPrincipaux[indexValue]['typage_annexe'].splice(key, 1);
                                        }
                                    });
                                }
                                J('#' + idRemove).remove();
                            });

                            listPrincipaux[trIndex]['annexes'].push(valueCheck);
                        }
                        J(this).prop('checked', false)
                    }
                }
            });
        }

        J.each(J(".etape3"), function (key, value) {
            var list = J(this)
            if (list.prop('checked')) {
                list.prop('checked', false)
            }
        });

        J('#modalEspaceDoc').hide();
        closeAccordion();
    });

    function loadDataTable()
    {
        J.extend(J.fn.bootstrapTable.defaults, J.fn.bootstrapTable.locales['fr-FR']);
        var $table = J('#tableDashboardEchange');
        $table.bootstrapTable({
            locale: 'fr-FR'
        });
        actionTable();
        J('.search-input').hide();
        J('#motsClesEchange').keyup(function () {
            var text = J(this).val();
            var lenghtText = text.length;

            if (lenghtText > 2 ) {
                J('.search-input').val(text);
                J('.search input').trigger("blur");
            } else {
                J('.search-input').val('');
                J('.search input').trigger("blur");
            }
        });

        $table.on('page-change.bs.table', function (number, size) {
            actionTable();
        });


    }

    if (typeof(urlAjax) !== 'undefined') {
        J.ajax({
            type: "GET",
            url: urlAjax
        }).done(function (data) {
            J('#table-echange-ajax').html(data);
            loadDataTable()
        });

        var elementSelect = [];
        var checkBox = J('.statut');
        checkBox.click(function () {
            var text = J(this).val();

            if (J(this).prop('checked')) {
                elementSelect.push(J(this).val());
                J(this).attr('checked', 'checked');
                J(this).parent().addClass('active');
            } else {
                J(this).removeAttr('checked');
                J(this).parent().removeClass('active');
                elementSelect.splice(elementSelect.indexOf(J(this).val()), 1);
            }
            J.ajax({
                type: "GET",
                url: urlAjax,
                data: {
                    'search': text,
                    'listeStatut': elementSelect
                }
            }).done(function (data) {
                J('#table-echange-ajax').html(data);
                loadDataTable();
            });
        });
    }

    J('.warning-no-doc-selectionne').hide();
    var navListItems = J('div.setup-panel div a'),
        allWells = J('.setup-content'),
        allNextBtn = J('.nextBtn'),
        allPreviousBtn = J('.previousBtn');

    allWells.hide();
    J('#objetError').hide();
    J('#descriptionError').hide();
    J('#applicationError').hide();
    J('#cheminementError').hide();
    J('#ContratError').hide();
    J('#bloc-sous-type-parapheur').hide();
    J('#sous-type-parapheur-error').hide();

    J('#pastell').hide();
    if (typeof(pastel) !== 'undefined') {
        J('#application').on('change', function () {
            var appsDist = J(this).val();
            if (appsDist === '') {
                appsDist = 0;
            }
            var urlPastel = pastel.replace('appsDist', appsDist);
            J.ajax({
                type: "GET",
                url: urlPastel
            }).done(function (data) {
                if (data) {
                    J('#pastell').show();
                    J('#pastell').html(data);
                    J('#cheminementError').hide();

                    hideResetBlocSousTypeParapheur();
                    displayBlocSousTypeParapheurByPastelParapheur();
                } else {
                    J('#pastell').hide();
                    hideResetBlocSousTypeParapheur();
                }
            });
        })
    }

    J('#closeModal').click(function () {
        J('#modalErreur').hide();
    });



    navListItems.click(function (e) {
        e.preventDefault();
        var Jtarget = J(J(this).attr('href')),
            Jitem = J(this);
        J('#warningNoDocSelectionne').hide();
        if (!Jitem.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            Jitem.addClass('btn-primary');
            allWells.hide();
            Jtarget.show();
            Jtarget.find('input:eq(0)').focus();
        }
    });

    //Gestion des évènements du bouton suivant
    allNextBtn.click(function () {
        J('.warning-no-doc-selectionne').hide();
        var step = J(this).data('step'),
            curStep = J("#" + step),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = J('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            isValid = true;

        J('#step1Error').hide();
        if (step === "step-1") {
            var contrat = '';
            var cheminementSignature = false;
            var cheminementTdt = false;
            var cheminementGed = false;
            var cheminementSae = false;
            J('.bloc-contrat').hide();



            isValid = checkStep1();
            if (isValid) {
                J('#step1').removeClass('btn-default').addClass('btn-default-pass');
                J('#step2-text').removeClass('stepwizard-text-inactif').addClass('stepwizard-text-actif');
                selectTypageDocument =  getTypageDocument(J('#application').val(), 'typageFichierPrincipal');

                contrat =  J("#contrat-echange option:selected").html();
                if (J('#concerne-contrat-oui').prop('checked') && J('#contrat-echange').val() != '') {
                    contrat =  J("#contrat-echange option:selected").html();
                    J('#syntheseContrat').html(contrat);
                    J('.bloc-contrat').show();
                }

                J('#fichier-jeton').addClass('hide');
                J('#fichier-jeton').html('');

                cheminementSignature = showPastelle('cheminement_signature', cheminementSignature)
                cheminementTdt = showPastelle('cheminement_tdt', cheminementTdt)
                cheminementGed = showPastelle('cheminement_ged', cheminementGed)
                cheminementSae = showPastelle('cheminement_sae', cheminementSae)

                if (isApplicationEnvoiDic()) {
                    setStep2ForEnvoiDic();
                } else {
                    J('#echangeDocumentaireStep2Body').show();
                    J('#step-2 .section-texte').removeClass("alert alert-primary");
                }

                J('#pastell').hide();
                J('#bloc-sous-type-parapheur').hide();

                J('.pastell').hide();
                if (cheminementSignature == true || cheminementTdt == true || cheminementGed == true || cheminementSae == true) {
                    J('#pastell').show();
                    J('.pastell').show();
                }
            }
        }

        if (curStepBtn == 'step-2') {
            if (isApplicationEnvoiDic()) {
                isValid = true;
                setStep4ForEnvoiDic();
                nextStepWizard.removeAttr('disabled');
                nextStepWizard = nextStepWizard.parent().next().children("a");
            } else {
                isValid = checkStep2();
                if (isValid) {
                    selectTypageDocument =  getTypageDocument(J('#application').val(), 'typageFichierAnnexe');

                    J('#step2').removeClass('btn-default').addClass('btn-default-pass');
                    J('#step3-text').removeClass('stepwizard-text-inactif').addClass('stepwizard-text-actif');
                }
            }
        }

        J('#step3Error').hide();
        if (curStepBtn == 'step-3') {
            if (isApplicationEnvoiDic()) {
                isValid = true;
                setStep4ForEnvoiDic();
            } else {
                isValid = checkStep3();
                if (isValid) {
                    J('#step3').removeClass('btn-default').addClass('btn-default-pass');
                    J('#step4-text').removeClass('stepwizard-text-inactif').addClass('stepwizard-text-actif');
                    var ligneTd = '';
                    J.each(J('#listFileEtape3 > tr > td:first-child'), function (key, data) {
                        var select = J(this).next('td').children('span').children('select');
                        var selectedTypage = select.attr('id');
                        var td = data.innerHTML;
                        let valueSelect = J('#' + selectedTypage + ' option:selected').html()
                        if (typeof valueSelect == 'undefined') {
                            valueSelect = jetonSignatureElectronique
                        }
                        ligneTd = ligneTd + '<tr><td>' +  td  + ' <span class="badge badge-primary badge-pill"><i class="fa fa-tag"></i> ' + valueSelect + '</span></td></tr>'
                    })
                    J('#listFileSynthese').html(ligneTd)
                }
            }
        }


        if (curStepBtn == 'step-4') {
            isValid = false
            if (isValid) {
                J('#step4').removeClass('btn-default').addClass('btn-default-pass');
            }
            envoyerEchange();
        }

        if (isValid) {
            nextStepWizard.removeAttr('disabled').trigger('click');
        }
        closeAccordion();
    });

    J('#concerne-contrat-non').on('click', function () {
        J('#contrat-echange').val('').change();
        J('#etape1-list-contrat').addClass('hide');
    })

    J('#concerne-contrat-oui').on('click', function () {
        J('#etape1-list-contrat').removeClass('hide');
    })

    function checkStep3()
    {
        var isValid = true;
        J.each(J('.typageDoc'), function (key, value) {
            J(this).removeClass('alert-danger');
            if (J(this).val() == '') {
                isValid = false
                J(this).addClass('alert-danger')
            }
        })

        if (!isValid) {
            J('#step3Error').show();
        }

        return isValid;
    }

    function checkStep1()
    {
        J('#objetError').hide();
        J('#descriptionError').hide();
        J('#applicationError').hide();
        J('#cheminementError').hide();
        J('#ContratError').hide();
        J('#sous-type-parapheur-error').hide();

        var isValid = true;

        if (!J('#application').val()) {
            J('#applicationError').show();
            isValid = false;
        } else {
            J('#syntheseApplication').text(J('#application option:selected').text());
        }

        var checkedElement = false;
        var i = 0;
        J(".checkbox_pastell").each(function ( index ) {
            var elementId = J(this).attr('id');
            if (J('#' + elementId).prop('checked')) {
                checkedElement = true;
                return checkedElement;
            }
            i++;
        });

        if (!checkedElement && i > 0) {
            J('#cheminementError').show();
            isValid = false;
        }

        if (!J('#objet').val()) {
            J('#objetError').show();
            isValid = false;
        } else {
            J('#syntheseObjet').text(J('#objet').val());
        }

        J('#syntheseDescription').text(J('#description').val());

        if (J('#concerne-contrat-oui').prop('checked') && J('#contrat-echange').val() == '') {
            isValid = false;
            J('#ContratError').show();
        }

        if (J('#cheminement_signature').prop('checked') && J('#sous-type-parapheur').val() === '') {
            isValid = false;
            J('#sous-type-parapheur-error').show();
        }

        if (!isValid) {
            J('#step1Error').show();
        }

        return isValid;
    }

    J(document).on("click", ".etape2", function () {
        let obj = J('#stepwizard-echange-doc');
        let step2 = J('#step-2');
        if (
            (typeof obj != 'undefined' && J(obj).attr('data-multi-docs-principaux') == 0) &&
            (typeof step2 != 'undefined' && J(step2).css('display') != 'none')
        ) {
            monoSelect(J(this));
        }
    });

    J(document).on("change", ".typageDoc" , function () {
        var elemChange = J(this);
        var valueFile = elemChange.attr('valuefile');
        var valueid = elemChange.attr('id');
        const regex = /^typage-principale/g;
        var elementParent = elemChange.parent();

        if (elementParent.attr('id').match(regex)) {
            J.each(listPrincipal, function (key, data) {
                if (data == valueFile) {
                    var index = elementParent.attr('data-index');
                    listPrincipaux[index]['typage'] = elemChange.val();
                    typagePrincipal[data] = elemChange.val();
                }
            })
        } else {
            var index = elementParent.attr('data-index');
            var arrayTypage = {};
            var keyRef =   valueFile.toString();
            arrayTypage[keyRef] = elemChange.val();
            var add = true;
            J.each(listPrincipaux[index]['typage_annexe'], function (key, data) {
                if ( listPrincipaux[index]['typage_annexe'][key][keyRef] != undefined) {
                    add = false
                    listPrincipaux[index]['typage_annexe'][key] = arrayTypage;
                }
            });
            if (add == true) {
                listPrincipaux[index]['typage_annexe'].push(arrayTypage);
            }
        }

    });

    function monoSelect(elemClick)
    {
        J.each(J(".etape2"), function (key, value) {
            let list = J(this)
            if (list.prop('checked')) {
                list.prop("checked", false);
            }
        });
        elemClick.prop("checked", true);
    }

    function checkStep2()
    {
        var isValid = false;
        let index = 0;
        listFiles = [];
        var links = [];
        var valuesCheck = [];
        var elFichierPrincipal = '';
        var multiDocsPrincipaux = J('#stepwizard-echange-doc').attr('data-multi-docs-principaux');

        if (J(".etape2").is(':checked')) {
            listPrincipal = [];
            listPrincipaux = [];
            J.each(J(".etape2"), function (key, value) {

                var btnPrimo = '';
                var btnPrimoId = getRandomString(20);
                if (primoSignature) {
                    btnPrimo = '   <button type="button" id="' + btnPrimoId + '" data-primo="" class="btn nextStep nextBtn openModalJeton">\n' +
                        '      <i class="fa fa-certificate"></i>\n' +
                        '   </button>';
                }

                if (J(this).is(':checked')) {
                    var valueCheck = J(this).val();
                    listPrincipal.push(valueCheck);
                    if (valueCheck != "checkboxManager") {
                        var idPrincipal = 'href-principal-' + index;
                        var idTypagePrincipal = 'typage-principale-' + index;
                        listPrincipaux.push({'principal': valueCheck, 'typage': '', 'annexes': [], 'jetons': [], 'typage_annexe': []});
                        var element = J(this).parent().children("label").children("a");
                        var documentJoint = element[0].cloneNode(true);
                        links[index] = documentJoint;
                        valuesCheck[index] = valueCheck;

                        if (multiDocsPrincipaux > 0) {
                            elFichierPrincipal +=
                                '<tr class="fichier-principal" data-index="' + index + '" data-value="' + valueCheck + '">' +
                                '<td><span class="classIconStar"></span><span id="' + idPrincipal + '"></span><span class="typageFichier hide"></span></td>' +
                                '<td><span id="' + idTypagePrincipal + '" data-index="' + index + '"></span></td>' +
                                '<td>  ' +
                                '   <button type="button" class="btn nextStep nextBtn openModalEspaceDoc">\n' +
                                '       <i class="fa fa-paperclip"></i>\n' +
                                '   </button>\n' +
                                btnPrimo +
                                '</td>' +
                                '</tr>  ' +
                                '<tr class="hide fichier-jeton"></tr>';
                        } else {
                            elFichierPrincipal +=
                                '<tr class="fichier-principal" data-index="' + index + '" data-value="' + valueCheck + '">' +
                                '<td><span class="classIconStar"></span><span id="' + idPrincipal + '"></span><span class="typageFichier hide"></span></td>' +
                                '<td><span id="' + idTypagePrincipal + '" data-index="' + index + '"></span></td>' +
                                '<td>  ' +
                                '   <button type="button" class="btn nextStep nextBtn openModalEspaceDoc">\n' +
                                '       <i class="fa fa-paperclip"></i>\n' +
                                '   </button>\n' +
                                btnPrimo +
                                '</td>' +
                                '</tr>  ';
                        }
                        index++;
                    }

                    J(this).prop("checked", false);
                }
            });

            J("#listFileEtape3").empty();
            J("#listFileEtape3").append(elFichierPrincipal);

            for (let i = 0; i < index; i++) {
                var idPrincipal = 'href-principal-' + i;
                var idTypagePrincipal = 'typage-principale-' + i;

                J('#' + idPrincipal).html(links[i]);

                let idSelect = Math.random().toString(36).substring(2) + Date.now().toString(36);
                var replaceId = selectTypageDocument.replace('typageDoc-a-remplacer', idSelect);
                replaceId = replaceId.replace('value-fichier-selected',  valuesCheck[i]);
                J('#' + idTypagePrincipal).html(replaceId);
            }
            isValid = true;
        } else {
            J('.warning-no-doc-selectionne').show()
            window.scrollTo(0, 500);
        }

        return isValid;
    }

    function envoyerEchange()
    {
        let contrat = '';
        if (J('#concerne-contrat-oui').prop('checked')) {
            contrat = J('#contrat-echange').val();
        }
        var params = {
            'application': J('#application').val(),
            'pastell': pastell,
            'objet': J('#objet').val(),
            'description': J('#description').val(),
            'contrat': contrat,
            'fichierPrincipaux': listPrincipaux,
            'sousTypeParapheur': J('#sous-type-parapheur').val()
        };

        J.ajax({
            method: "POST",
            url: saveUrl,
            data: {'data': params},
            dataType: "json"
        }).done(function (data) {
            J('#modalSucces').show();
        }).fail(function () {
            J('#modalErreur').show();
        });

    }

    //Gestion des évènements du bouton précédent
    allPreviousBtn.click(function () {
        var step = J(this).data('step'),
            curStep = J("#" + step),
            curStepBtn = curStep.attr("id"),
            previousStepWizard = J('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        if (step === "step-1") {
            J('#step1').removeClass('btn-default-pass').addClass('btn-default');
        }

        if (curStepBtn == 'step-2') {
            J('#step2').removeClass('btn-default-pass').addClass('btn-default').attr('disabled', 'disabled');
            J('#step2-text').removeClass('stepwizard-text-actif').addClass('stepwizard-text-inactif');
        }

        if (curStepBtn == 'step-3') {
            J('#step3').removeClass('btn-default-pass').addClass('btn-default').attr('disabled', 'disabled');
            J('#step3-text').removeClass('stepwizard-text-actif').addClass('stepwizard-text-inactif');
        }

        if (curStepBtn == 'step-4') {
            J('#step4').removeClass('btn-default-pass').addClass('btn-default').attr('disabled', 'disabled');
            J('#step4-text').removeClass('stepwizard-text-actif').addClass('stepwizard-text-inactif');
        }

        previousStepWizard.removeAttr('disabled').trigger('click');
    });

    J('div.setup-panel div a.btn-primary').trigger('click');


    /**
     * Script sur la HOME
     */

    function actionTable()
    {
        J('.relance-echange').on('click', function () {
            var idEchange = J(this).attr('data-id-echange');
            var elm = J(this);
            J.ajax({
                type: "PUT",
                url: urlPutEchange,
                data: {
                    'idEchange': idEchange
                }
            }).done(function () {
                J('#badge-echange-' + idEchange).removeClass('alert-color-danger')
                    .removeClass('question-mark-picto')
                    .addClass('alert-color-progress');
                J('#date-echange-' + idEchange).addClass('progress');
                J('#badge-echange-' + idEchange).text(TEXT_A_ENVOYER)
                elm.hide()
                J('#relance-echange-' + idEchange).hide()
                J("ul li:not(:first-child)").remove();
            });
        });

        J('.detail-echange').click(function () {
            J('#detail-echange-ajax').html('')
            var url = J(this).attr('data-id-echange');
            J.ajax({
                type: "GET",
                url: url
            }).done(function (data) {
                J('#detail-echange-ajax').html(data)
                J('.detail-echange-fonctionnel').hide();
                J('.show-detail-echange').on('click', function () {
                    let elemDetail = J(this);
                    let dataId = elemDetail.attr('data-id')
                    if (J("#" + dataId).is(":visible")) {
                        J("#" + dataId).hide();
                        elemDetail.html(
                            '<i class="fa fa-plus-circle"></i> ' + afficherEchangeFonctionnel
                        )
                    } else {
                        J("#" + dataId).show();
                        elemDetail.html(
                            '<i class="fa fa-minus-circle"></i> ' + masquerEchangeFonctionnel
                        )
                    }
                })
            })
            J('#modalSucces').modal();
        });

        J('#modal-detail-echange').click(function () {
            J('#modalSucces').modal('hide');
        });
    }

    function getTypageDocument(idEchangeDocApplicationClient, classTypage = null)
    {
        let selectTypage = '<select name="typageDoc[]" id="typageDoc-a-remplacer" valueFile="value-fichier-selected" class="typageDoc form-control ' + classTypage + '" required>';
        var optionTypage = '<option value="">' + typeNonRenseigne + '</option>';
        var url = ajaxTypageDocument.replace('idEchangeDocApplicationClient', idEchangeDocApplicationClient)
        var retrunAjax = J.ajax({
            type: "GET",
            url: url,
            async: false,
            dataType: 'json'
        }).done(function (data) {
            return data;
        });

        J.each(retrunAjax.responseJSON, function (key, value) {
            optionTypage = optionTypage + '<option value="' + value.id + '">' + value.libelle + '</option>'
        })

        selectTypage = selectTypage + optionTypage + "</select>"

        return selectTypage;
    }

    J(document).on("change", ".typageDoc" , function () {
        J(this).removeClass('alert-danger')
    });

    function showPastelle(elementId, cheminement)
    {
        J('#' + elementId + '_synthese').hide()
        if (J('#' + elementId).length) {
            cheminement = true;
            J('#' + elementId + '_synthese').show()

            J('#' + elementId + '_oui_synthese').hide()
            J('#' + elementId + '_non_synthese').show()
            pastell[elementId] = false;
            if (J('#' + elementId).prop('checked')) {
                J('#' + elementId + '_oui_synthese').show()
                J('#' + elementId + '_non_synthese').hide()
                pastell[elementId] = true;
            }
        }
        return cheminement
    }

    function closeAccordion()
    {
        J('button.accordion-toggle').attr('aria-expanded', 'false');
        J('div.show').removeClass('show');
    }


    function getRandomString(length)
    {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        return result;
    }

    function cleanJeton()
    {
        if (J('#delete_' + idClickPrimo).length  > 0) {
            J('#delete_' + idClickPrimo).remove()
        }
    }

    function isApplicationEnvoiDic()
    {
        return J('#application option:selected').data("envoiDic") == 1;
    }

    function setStep2ForEnvoiDic()
    {
        J('#echangeDocumentaireStep2Body').hide();

        J('#step-2 .section-texte').html(informationsEnvoiDicStep2);
        J('#step-2 .section-texte').addClass("alert alert-primary");
    }

    function setStep4ForEnvoiDic()
    {
        J('#step2').removeClass('btn-default').addClass('btn-default-pass');
        J('#step3').removeClass('btn-default').addClass('btn-default-pass');
        J('#step3-text').removeClass('stepwizard-text-inactif').addClass('stepwizard-text-actif');
        J('#step4-text').removeClass('stepwizard-text-inactif').addClass('stepwizard-text-actif');

        var documentsLine = '<tr><td><span id="href-principal-0" style="color: #1183ff">' + informationsEnvoiDicStep4 + '</span> <span class="badge badge-primary badge-pill"><i class="fa fa-tag"></i> DAC</span></td></tr>';
        J('#listFileSynthese').html(documentsLine);
    }

    function hideResetBlocSousTypeParapheur()
    {
        J('#sous-type-parapheur').val("");
        J('#bloc-sous-type-parapheur').hide();
    }

    function displayBlocSousTypeParapheurByPastelParapheur()
    {
        J('#cheminement_signature').change(function () {
            if (this.checked) {
                J('#bloc-sous-type-parapheur').show();
            } else {
                hideResetBlocSousTypeParapheur();
            }
        });
    }
});