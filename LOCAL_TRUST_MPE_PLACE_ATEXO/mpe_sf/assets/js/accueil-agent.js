/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import '../css/accueil-agent.scss';

import 'block-ui/jquery.blockUI';
import Chart from "chart.js/auto";

$(document).ready(function () {

    if ($('#stats-courriers').length > 0) {
        getDataStatMessec();
    }

    // Masquer le bouton collapse lorsque le bloc de texte est plus petit que 200 px
    if ($('#collapseBlocMessageAccueilAgent').height() < 200) {
        $('#buttonCollapse').hide();
    }

    getDataStatStatutCalcule();
})

function getDataStatStatutCalcule() {
    var ctx = $("#simple-doughnut-chart");
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        plugins: {
            legend: {
                display: false
            }
        }
    };
    var chartData = {
        labels: labels,
        datasets: [{
            label: null,
            data: data,
            backgroundColor: backgroundColor,
        }]
    };
    var config = {
        type: 'doughnut',
        options : chartOptions,
        data : chartData
    };

    return new Chart(ctx, config);
}

const cardMessec = $('#stats-courriers');

function getDataStatMessec()
{
    $.get({
        url: '/agent/home/messec/dashboard',
    }).done(function (data) {
        $('#nombre-messages h3').text(data['nombreMessages']);
        $('#nombre-message-non-delivre h3').text(data['nombreMessageNonDelivre']);
        $('#nombre-message-en-attente-reponse h3').text(data['nombreMessageEnAttenteReponse']);
        $('#nombre-message-non-lu h3').text(data['nombreMessageNonLu']);

        cardMessec.unblock();
    });
}

$('#stats-courriers').ready(function() {
    cardMessec.block({
        message: '<div class="ft-refresh-cw la-spin font-medium-2"></div>',
        timeout: 5000,
        overlayCSS: {
            backgroundColor: '#FFF',
            cursor: 'wait',
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
});
