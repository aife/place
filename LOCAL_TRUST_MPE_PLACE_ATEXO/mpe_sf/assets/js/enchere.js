/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import '../css/enchere.scss';

import * as Chartist from 'chartist';
import moment from "moment";
import * as ChartistLegend from 'chartist-plugin-legend';
import 'chartist/dist/chartist.min.css';

$(document).ready(function () {
    new Chartist.Line('#enchere', {
        series: series
    }, {
        low: 0,
        axisX: {
            type: Chartist.FixedScaleAxis,
            divisor: 5,
            labelInterpolationFnc: function (value) {
                return moment(value).format('MMM D');
            }
        },plugins: [
            Chartist.plugins.legend({
                legendNames: legend,
                position: 'bottom'
            })
        ]
    });
})