import axios from "axios";

let currentPosition = "";
let currentTabId = "";
let currentSelectedId = "";
//init checkEdit
$('#currentCkeditor').val('message_accueil_agent_connecte_agentAuthenticated');
$('#currentMessageType').val($("#message_accueil_agent_connecte_typeMessage").val());
$('#destinataire').val('agent');
$('#authentifier').val(1);

$('a[href*="#show_"]').on('click', function (event) {
    currentPosition = event.currentTarget.getAttribute("data-position");
    $('#destinataire').val(event.currentTarget.getAttribute("data-destinataire"));
    $('#authentifier').val(event.currentTarget.getAttribute("data-authentifier"));
    currentTabId = $('textarea')[currentPosition].id;
    currentSelectedId = $('select')[currentPosition].id;
    $('#currentMessageType').val($("#"+currentSelectedId).val());
    $('#currentCkeditor').val(currentTabId);
})

$( "[id*='message_accueil_']").change(function () {
    $('#currentMessageType').val($(this).val());
});

$('#validateButton').click(function () {
    let currentCkeditorName = $('#currentCkeditor').val();
    let messageType =  $('#currentMessageType').val();
    const url = '/agent/message/accueil/save';
    axios.post(decodeURI(url), {
        destinataire: $('#destinataire').val(),
        authentifier: $('#authentifier').val(),
        data: CKEDITOR.instances[currentCkeditorName].getData(),
        selectedValueTypeMessage: messageType,
    })
        .then(response => {
            const alertMessage = $(".alert");
            $("#messageAccueilModal").modal('hide');
            alertMessage.css("display", "block");
            setTimeout(function () {
                alertMessage.fadeOut(1000, function () {
                    $(this).css("display", "none");
                });
            }, 3000);
        })
        .catch(error => {
            console.log(error.response)
        });
});