/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import '../css/consultation-simplifiee.scss';
import '@uppy/core/dist/style.css';
import '@uppy/status-bar/dist/style.css';

import Promise from 'es6-promise';
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import 'bootstrap';

import '../css/lieux-execution.scss';
import './lieux-execution';

import Uppy from "@uppy/core";
import Tus from "@uppy/tus";
import StatusBar from "@uppy/status-bar";

Promise.polyfill();

const locale = $('html').attr('lang') || 'fr'
const routes = require('../../public/js/fos_js_routes.json');
Routing.setRoutingData(routes);
global.Routing = Routing;
const filesize = require('filesize')

const UppyLocaleFr = require('@uppy/locales/lib/fr_FR')
const UppyLocaleEn = require('@uppy/locales/lib/en_US')
const UppyLocale = (locale === 'fr') ? UppyLocaleFr : UppyLocaleEn
const uppyOne = new Uppy({
    debug: false,
    autoProceed: true,
    locale: UppyLocale,
    restrictions: {
        maxFileSize: null,
        minFileSize: null,
        maxTotalFileSize: null,
        maxNumberOfFiles: 1,
        minNumberOfFiles: null,
        allowedFileTypes: ['.zip']
    }
})

$(document).ready(function () {
    updateTypeProcedureList($('#divTypeProcedureOrganisme').data('selected'));
    $("#form_typeMarche").on("change", function () {
        updateTypeProcedureList();
    });

    $(document).on("change", "#form_typeProcedureOrganisme", function () {
        updateTypeAcces();
    });

    $("input[name='form[typeAcces]']").on("change", function () {
        updateCodeAcces();
    });

    updateCodeAcces();
    $(document).on("change", "#form_codeProcedureInput", function () {
        $("#form_codeProcedure").val($(this).val());
    });

    /**
     * @description : variable qui sert a faire l'affichage de la modale
     * @type {boolean}
     * */
    let submit = false;

    /**
     * Action déclencher lors du clique.
     */
    $('#form_saveAndValidate').on('click', function (event) {
        if (submit === false) {
            event.preventDefault();
            $('#modaleSucces').modal('show');

            return false;
        }
    });

    /**
     * Action déclencher lors du clique "annuler" sur la modale pour annuler la soumission
     */
    $('#modaleAnnuler').on('click', function (event) {
        $('#modaleSucces').modal('hide')
    })

    /**
     * Action déclencher lors du clique "Confirmer" sur la modale pour annuler la soumission
     */
    $('#modaleConfirmer').on('click', function (event) {
        $('#modaleSucces').modal('hide');
        submit = true;
        $('#form_saveAndValidate').trigger('click');
    })

    uppyOne
        .use(Tus, {
            endpoint: Routing.generate('agent_dce_tus_upload'),
            removeFingerprintOnSuccess: true,
            chunkSize: 1024 * 1024 * 20
        })
        .use(StatusBar, {target: '.uploader', hideAfterFinish: false})
        .on('upload', () => {
            $('#uploaded-dce').closest('form').find('button').attr('disabled', 'disabled')
        })
        .on('upload-success', (file, response) => {
            fileInput.value = null;

            $('#uploaded-dce ul li').addClass('list-group-item-danger')

            $('#uploaded-dce ul')
                .append('<li class="list-group-item list-group-item-success">'
                    + '<i class="fa fa-folder"></i> '
                    + file.name + ' ('
                    + filesize(file.size, {locale: locale})
                    + ')</li>')

            $('#uploaded-dce').show()

            let filepath = response.uploadURL.split('/').pop()
                + '/' + file.name
            $('#form_dce_filepath').val(filepath)

            $('#uploaded-dce').closest('form').find('button').attr('disabled', false)
        })
        .on('upload-error', () => {
            $('#uploaded-dce').closest('form').find('button').attr('disabled', false)
        })
        .on('file-removed', () => {
            fileInput.value = null
        })

    const fileInput = document.querySelector('#form_dce')

    fileInput.addEventListener('change', (event) => {
        const files = Array.from(event.target.files);
        uppyOne.resetProgress();

        files.forEach((file) => {
            try {
                uppyOne.addFile({
                    source: 'file input',
                    name: file.name,
                    type: file.type,
                    data: file
                })
            } catch (err) {
                console.error(err)
            }
        })
    })

    $('#form_objet,#form_intitule,#form_champSuppInvisible').each(function () {
        $(this).on("change", function () {
            replaceCaracSpecial(this);
        });
    });

});

// Mise à jour des type de procédures si on change le type de contrat
function updateTypeProcedureList(typeProcedureSelected = '', updateTypeAccess = true)
{
    let typeContratId = $('SELECT[data-ajax-custom="typeContrat"]').val();
    let actionType = $('#form_actionType').val();
    $.post(Routing.generate('atexo_consultation_simplifie_update_type_procedure'), {
        typeContratId: typeContratId,
        actionType: actionType,
    }, function (response) {
        $("#divTypeProcedureOrganisme").html(response);
        if (typeProcedureSelected !== '') {
            $('SELECT[data-ajax-custom="procedureType"]').val(typeProcedureSelected);
            if (updateTypeAccess) {
                updateTypeAcces();
            }
        }
    });
}

// Mise à jour du type d'accès si on change le type de procedure
function updateTypeAcces()
{
    let typeProcedure = $('SELECT[data-ajax-custom="procedureType"]').val();
    $.post(Routing.generate('atexo_consultation_simplifie_update_type_acces'), {
        typeProcedure: typeProcedure,
    }, function (response) {
        if (
            (response.procedure_public.indexOf("-") == -1 && response.procedure_public.indexOf("+") == -1)
            || response.procedure_public.indexOf("-") >= 0
        ) {
            $('#form_typeAcces_0').prop("disabled", true);
            $('#form_typeAcces_0').prop("readonly", true);
        }

        if (
            (response.procedure_restreint.indexOf("-") == -1 && response.procedure_restreint.indexOf("+") == -1)
            || response.procedure_restreint.indexOf("-") >= 0
        ) {
            $('#form_typeAcces_1').prop("disabled", true);
            $('#form_typeAcces_1').prop("readonly", true);
        }

        if (response.type_acces) {
            if (!$('#form_actionType').val() || $('#form_actionType').val() !== "update") {
                 $('input[name="form[typeAcces]"][value="' + response.type_acces + '"]').prop("checked", true);
            }
            updateCodeAcces();
        }
    });
}

// Dynamisation du code d'accès en fonction du type d'accès
function updateCodeAcces()
{
    let typeAcces = $("input[name='form[typeAcces]']:checked").val();
    let hiddenField = $('#form_codeProcedure');
    let field = $('#form_codeProcedureInput');
    if (typeAcces === '2') {
        field.val(hiddenField.val());
        field.parent().show();
    } else if (field.val() == '' || typeAcces === '1') {
        hiddenField.val('');
        field.val(hiddenField.val());
        field.parent().hide();
    }
}

function replaceCaracSpecial(element)
{
    const pattern = {
        "<":"< ",
        ">":" >",
        "-":"-",
        "'":"'",
        "_":"_",
        "\u2264":"<=",
        "\u2265":">=",
        "\u00A9":"",
        "\u201D":"\"",
        "\u201C":"\"",
        "\u0152":"OE",
        "\x0133":"..",
        "\x243":"<=",
        "\x242":">=",
        "\x0156":"oe",
        "\x8212":"--",
        "\x0145":"'",
        "\x0146":"'",
        "\x0147":"\"",
        "\x0148":"\"",
        "\x174":"\"",
        "\x175":"\"",
        "\x0192":"A",
        "\x0193":"A",
        "\x0194":"A",
        "\x0195":"A",
        "\x0196":"A",
        "\x0197":"A",
        "\x0200":"E",
        "\x0201":"E",
        "\x0202":"E",
        "\x0203":"E",
        "\x0204":"I",
        "\x0205":"I",
        "\x0206":"I",
        "\x0207":"I",
        "\x0210":"O",
        "\x0211":"O",
        "\x0212":"O",
        "\x0213":"O",
        "\x0214":"O",
        "\x0217":"U",
        "\x0218":"U",
        "\x0219":"U",
        "\x0220":"U",
    }

    for (let key in pattern) {
        element.value = element.value.replace(new RegExp(key, 'g'),pattern[key]);
    }
}
