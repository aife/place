/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
import $ from "jquery";
import '../css/prerequis-techniques.scss';

$(document).ready(function() {
    $('.nav-link').on('click', function() {
        var panToOpenID = $(this).attr('href');
    
        $('.tab-pane').removeClass('active');
        $('.nav-link').removeClass('active');
        $(panToOpenID).addClass('active');
        $(this).addClass('active');
    });
});
