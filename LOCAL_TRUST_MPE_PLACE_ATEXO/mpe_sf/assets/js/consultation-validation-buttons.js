/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import Routing from 'fos-router';
import axios from "axios";

$('#show-modal-consultation').click(function () {
    $('#vailder-consultation-modal').show()
});

$('#hide-modal-consultation').click(function () {
    $('#vailder-consultation-modal').hide()
});

$('#back-to-dashboard').click(function () {
    window.location.href =  Routing.generate('consultation_search_index')
        + '?search=true&id=' + $('#consultation-id').val();
});

$('#save-stay-page').click(function () {
    window.location.reload();
})

$('#valider-consultation').click(function () {
    let consultationId = $('#consultation-id').val();
    axios.get('/agent/token-user-connected').then((response) => {
        const options = {
            headers: {Authorization: `Bearer ${response.data}`}
        };
        axios.put('/api/v2/consultations/' + consultationId + '/attente-validation', {}, options)
            .then(() => {
                window.location.href =  Routing.generate('consultation_search_index')
                    + '?search=true&id=' + consultationId;
            })
            .catch((error) => {
                if (error.response.status === 400 && error.response.data) {
                    $('#error-container').empty();
                    error.response.data.forEach((itemError) => {
                        $('#error-container').append(
                            '<div style="font-weight: bold;color: #dd6060;margin-left: 20px">' +
                            '- ' + itemError +
                            '</div>'
                        )
                    });
                    $('#error-list').show();
                }
                $('#vailder-consultation-modal').hide()
            });
    });
})
