// assets/js/espace-documentaire.js

import '../css/espace-documentaire-jeton.scss';

import $ from 'jquery';
import Vue from 'vue';
import Promise from 'es6-promise';
Promise.polyfill();
import axios from 'axios';
import 'bootstrap';
import fileSize from "filesize";

Vue.filter('truncate', function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
});

var DceJeton = Vue.component('dce-jeton', {
    template: '#dce-template-jeton',
    props: {
        node: Object,
        depth: Number,
        masterId: String
    },
    data: function () {
        return {
            composantType: 'PIECESDCE##DCE'
        }
    },
    computed: {
        isDirectory() {
            return this.node.type === 'dossier';
        },
        getStyle() {
            return 'padding-left:' + (this.depth) + 'em;';
        }
    },
    methods: {
        getName(data) {
            return 'DCE_' + data.id + '_' + data.nom;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data) {
            return 'DCE_' + data.id + '_' + data.nom+'-jeton';
        },
        getValue(data) {
            return this.composantType + '##' + this.masterId + '##' + data.fichier;
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,encryptedPath,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+this.masterId+'-'+encryptedPath;
        }
    }
});

var PiecesDceJeton = Vue.component('pieces-dce-jeton', {
    template: '#pieces-dce-template-jeton',
    data: function () {
        return {
            isLoaded: false,
            rc: "",
            autresPieces: "",
            dumeAcheteur: "",
            dce: "",
            composantType: 'PIECESDCE'
        }
    },
    methods: {
        getName(data) {
            return data.type + '_' + data.id;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data, type = null) {
            if (type) {
                return type + '_' + data.id+'-jeton';
            }
            return data.type + '_' + data.id+'-jeton';
        },
        getValue(data, type = null) {
            if (type) {
                data.type = type;
            }
            return this.composantType + '##' + data.type + '##' + data.id;
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="checkboxDCE"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var urlRc = apiUrl + 'rc/' + id + '.json'
                var urlOther = apiUrl + 'dce/autres-pieces/' + id + '.json';
                var urLDumeAcheteur = apiUrl + 'dume/dume-acheteur/' + id + '.json';
                var urlDce = apiUrl + 'dce/' + id + '.json';
                axios.get(urlRc).then(response => {
                    if (response.data.mpe.reponse.data.id) {
                        this.rc = response.data.mpe.reponse.data;
                    }
                });
                axios.get(urlOther).then(response => {
                    if (response.data.mpe.reponse.data.id) {
                        this.autresPieces = response.data.mpe.reponse.data;
                    }
                });
                axios.get(urLDumeAcheteur).then(response => {
                    if (response.data.mpe.reponse.data.fichiers) {
                        this.dumeAcheteur = response.data.mpe.reponse.data;
                    }
                });
                axios.get(urlDce).then(response => {
                    console.log(response.data.mpe)
                    if (response.data.mpe.reponse.data) {
                        this.dce = response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded = true;
            }
        }
    }
});

var PiecesAutresJeton = Vue.component('pieces-autres-jeton', {
    template: '#pieces-autres-template-jeton',
    data: function () {
        return {
            isLoaded: false,
            autres: {
                fichiers: []
            },
            file: ''
        }
    },
    methods: {
        getName(data) {
            return data.type + '_' + data.id;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data) {
            return data.type + '_' + data.id+'-jeton';
        },
        getValue(data) {
            return this.autres.type + '##' + data.id;
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="'+event.target.dataset.targetCheckbox+'"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var url = apiUrl + 'autres-pieces-consultation/' + id + '.json'
                axios.get(url).then(response => {
                    if (response.data.mpe.reponse.data.fichiers) {
                        this.autres = response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded = true;
            }
        },
        uploadNewFile() {
            this.file = this.$refs.file.files[0];

            let data = {
                'id': '',
                'nom': this.file.name,
                'poids': this.file.size,
                'created_at': '',
                'uploaded': false
            }
            this.autres.fichiers.push(data);
            let dataIndex = this.autres.fichiers.length - 1;

            let formData = new FormData();
            formData.append('file[]', this.file);

            let apiUrl = this.$refs.file.dataset.uploadBaseUrl +
                '/agent/espace-documentaire/autres-pieces-consultation/' +
                this.$refs.file.dataset.consultationId +
                '.json';

            axios.post(apiUrl,
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
                .then(function (response) {
                    if (response.data.mpe.reponse.data.fichier) {
                        this.autres.fichiers[dataIndex].id = response.data.mpe.reponse.data.fichier.id;
                        this.autres.fichiers[dataIndex].uploaded = response.data.mpe.reponse.data.fichier.uploaded;
                        this.autres.fichiers[dataIndex].created_at = response.data.mpe.reponse.data.fichier.created_at;
                    }
                }.bind(this))
                .catch(function () {
                    console.log('Error while uploading new file');
                    console.log(response);
                });

        },
        deleteFile(index, consultationId, baseUrl, message) {
            if (confirm(message)) {

                let data = this.autres.fichiers[index];

                let apiUrl = baseUrl +
                    '/agent/espace-documentaire/autres-pieces-consultation/' +
                    consultationId + '/' +
                    data.id +
                    '.json';

                axios.delete(apiUrl)
                    .then(function () {
                        this.autres.fichiers.splice(index, 1);
                    }.bind(this))
                    .catch(function (response) {
                        console.log('Error while deleting file');
                        console.log(response);
                    });

                if (this.autres.fichiers.length == 0) {
                    this.autres.fichiers = null;
                }
            }
        }
    }
});

var PiecesPubJeton = Vue.component('pieces-pub-jeton', {
    template: '#pieces-pub-template-jeton',
    data: function () {
        return {
            isLoaded: false,
            pub: ""
        }
    },
    methods: {
        getName(data) {
            return data.type + '_' + data.id;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data) {
            return data.type + '_' + data.id+'-jeton';
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getValue(data) {
            return this.pub.type + '##' + data.id;
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="'+event.target.dataset.targetCheckbox+'"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var url = apiUrl + 'publicite/' + id + '.json'
                axios.get(url).then(response => {
                    if (response.data.mpe.reponse.data.fichiers) {
                        this.pub = response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded = true;
            }
        }
    }
});

var PiecesDepotJeton = Vue.component('pieces-depot-jeton', {
    template: '#pieces-depot-template-jeton',
    data: function () {
        return {
            isLoaded: false,
            depots: ""
        }
    },
    methods: {
        isArray(data) {
            return this.$parent.isArray(data);
        },
        getName(data) {
            return data.type + '_' + data.id;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data) {
            return data.type + '_' + data.id+'-jeton';
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        loadData(baseUrl, id) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var url = apiUrl + 'plis/' + id + '.json'
                axios.get(url).then(response => {
                    if (response.data.mpe.reponse.data.plis) {
                        this.depots = response.data.mpe.reponse.data.plis;
                    }
                });
                this.isLoaded = true;
            }
        }
    }
});

var DepotJeton = Vue.component('depot-jeton', {
    template: '#depot-template-jeton',
    props: {
        node: Object,
    },
    data: function () {
        return {
            isLoaded: false,
            enveloppes: "",
            composantType: 'PLI'
        }
    },
    methods: {
        getIdDiv(offre, idx, type) {
            return offre + type + idx+'-jeton';
        },
        isArray(data) {
            return this.$parent.isArray(data);
        },
        getIndex(idx) {
            return idx.toString();
        },
        getName(data) {
            return data.type + '_' + data.id;
        },
        getId(data) {
            return data.type + '_' + data.id+'-jeton';
        },
        getExtension(data) {
            return data.extension;
        },
        getValueForDumeOE(data) {
            return this.composantType + '##DUMEOE##' + data.type + '##' + data.id_blob;
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getNomPli(tradEntreprise, tradPli) {

            return tradEntreprise + ' ' + this.node.entreprise + ' | ' + tradPli + ' : ' + this.node.pli;
        },
        isAlloti() {
            if (this.enveloppes.alloti && this.enveloppes.alloti === 1) {
                return true;
            }
            return false;
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="'+event.target.dataset.targetCheckbox+'"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id, idOffre) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var url = apiUrl + 'pieces-du-depots/' + idOffre + '/' + id + '.json'
                axios.get(url).then(response => {
                    if (!response.data.mpe.reponse.data.message) {
                        this.enveloppes = response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded = true;
            }
        }
    }
});

var EnveloppeJeton = Vue.component('enveloppe-jeton', {
    template: '#enveloppe-template-jeton',
    props: ['node', 'indexNode', 'alloti', 'titre', 'color', 'idDiv', 'nomEnveloppe'],
    data: function () {
        return {
            isLoaded: false,
            elements: "",
        }
    },
    methods: {
        isArray(data) {
            return this.$parent.isArray(data);
        },
        decoder(str) {
            var textArea = document.createElement('textarea');
            textArea.innerHTML = str;
            return textArea.value;
        },
        getName(data) {
            return 'checkbox'+data.type;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data) {
            return data.type + '_' + data.id_blob+'-jeton';
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getValue(data) {
            var parent = this.$parent.node;
            var value = this.$parent.composantType + '##' + parent.pli + '-' + parent.entreprise + '##' + this.nomEnveloppe + '##';

            if(this.alloti == 1){
                value+=this.node.labelLot+ '##';
            }
            value+=data.id_blob;
            return value;
        },
        isAlloti() {
            if (this.elements.alloti && this.elements.alloti == 1) {
                return true;
            }
            return false;
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="'+event.target.dataset.targetCheckbox+'"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id, idOffre) {
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if (!this.isLoaded) {
                var url = apiUrl + 'pieces-du-depots/' + idOffre + '/' + id + '.json'
                axios.get(url).then(response => {
                    if (!response.data.mpe.reponse.data.message) {
                        this.elements = response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded = true;
            }
        }
    }
});

var PliAttributaireJeton = Vue.component('pli-attributaire-jeton', {
    template: '#pli-attributaire-template-jeton',
    data: function () {
        return {
            isLoaded: false,
            attributaires: ""
        }
    },
    methods: {
        isArray(data){
            return this.$parent.isArray(data);
        },
        getName(data){
            return data.type+'_'+data.id;
        },
        getExtension(data) {
            return data.extension;
        },
        getId(data){
            return data.type+'_'+data.id+'-jeton';
        },
        getSize(octet){
            return this.$parent.getSize(octet);
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        loadData(baseUrl, id){
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if(!this.isLoaded){
                var url= apiUrl+ 'plis-attributaires/'+ id +'.json'
                axios.get(url).then(response => {
                    if(response.data.mpe.reponse.data.plis){
                        this.attributaires=response.data.mpe.reponse.data.plis;
                    }
                });
                this.isLoaded=true;
            }
        }
    }
});

var AttributaireJeton = Vue.component('attributaire-jeton', {
    template: '#attributaire-template-jeton',
    props: {
        node: Object
    },
    data: function () {
        return {
            isLoaded: false,
            elements: "",
            composantType: 'ATTRIBUTAIRE'
        }
    },
    computed: {
        displayCandidature: function () {
            let candidature = false;
            if (this.elements.listes.enveloppe_candidature != undefined) {
                candidature = this.elements.listes.enveloppe_candidature.length == undefined
            }

            return candidature;

        }
    },
    methods: {
        getIdDiv(offre,idx,type){
            return offre + type + idx+'-jeton';
        },
        isArray(data){
            return this.$parent.isArray(data);
        },
        getIndex(idx){
            return idx.toString();
        },
        getName(data){
            return data.type+'_'+data.id;
        },
        getId(data){
            return data.type+'_'+data.id+'-jeton';
        },
        getValueForDumeOE(data) {
            return this.composantType + '##DUMEOE##' + data.type + '##' + data.id_blob;
        },
        getSize(octet) {
            return this.$parent.getSize(octet);
        },
        getNomPli(tradEntreprise, tradPli) {

            return tradEntreprise + ' ' + this.node.entreprise + ' | ' + tradPli + ' : ' + this.node.pli;
        },
        isAlloti() {
            if (this.elements.alloti && this.elements.alloti == 1) {
                return true;
            }
            return false;
        },
        getDownloadUrl(url,consultationId,encryptedAgentId,id,type){
            return url+ '/agent/direct-download/'+encryptedAgentId+'/'+consultationId+'/'+type+'-'+id;
        },
        checkboxManager(event){
            $('input[name ="'+event.target.dataset.targetCheckbox+'"]').prop('checked', event.target.checked);
        },
        loadData(baseUrl, id,idOffre){
            var apiUrl = baseUrl + '/agent/espace-documentaire/';

            if(!this.isLoaded){
                var url= apiUrl+ 'pieces-plis-attributaires/'+ idOffre +'/'+ id +'.json'
                axios.get(url).then(response => {
                    if(!response.data.mpe.reponse.data.message){
                        this.elements=response.data.mpe.reponse.data;
                    }
                });
                this.isLoaded=true;
            }
        }
    }
});

/**
 * Create a fresh Vue Application instance
 */
new Vue({
        el: '#consultation-jeton',
        data: {
            locale: '',
            downloadOK: false,
            noFilesSelected: false,
            errorCreateDownload: false
        },
        components: {
            PiecesDceJeton,
            PiecesPubJeton,
            PiecesAutresJeton,
            PiecesDepotJeton,
            DceJeton,
            DepotJeton,
            EnveloppeJeton,
            PliAttributaireJeton
        },
        methods: {
            getSize(octets) {
                return fileSize(octets);
            },
            isArray(data) {
                return Array.isArray(data);
            },
            hideAlert() {
                this.downloadOK = false;
                this.noFilesSelected = false;
                this.errorCreateDownload = false;
            },
            download(baseUrl, id) {
                var myArray = [];

                $("input:checkbox:checked").each(function () {
                    if($(this).val() !== 'checkboxManager'){
                        myArray.push($(this).val());
                    }
                });
                if (myArray.length > 0) {
                    var data = JSON.stringify(myArray);

                    var apiUrl = baseUrl + '/agent/espace-documentaire/create-download/' + id + '.json';
                    axios.post(apiUrl, {'data': myArray}).then(response => {
                        var wsResponse = response.data.mpe.reponse.data;
                        if (wsResponse) {
                            if (wsResponse.message == 'OK') {
                                this.downloadOK = true;
                            } else {
                                this.errorCreateDownload = true;
                            }
                        }
                    });
                } else {
                    this.noFilesSelected = true;
                }
                $('#modalInformation').modal('show');
            }
        },
        beforeMount: function () {
            this.locale = this.$el.attributes['data-locale'].value;
        }
    });
