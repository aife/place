import axios from "axios";

function getToken()
{
    return axios.get('/agent/token-user-connected');
}

function patchConsultation(consultationId, data, config)
{
    return axios.patch('/api/v2/consultations/' + consultationId, data, config);
}

function handleEventValidation(event)
{
    const details = event.detail;

    if (details['facturation'] && details['facturation']['idConsultation']) {
        getToken().then((responseToken) => {
            const token = responseToken.data;
            const options = {
                headers: {Authorization: `Bearer ${token}`, "Content-Type": "application/merge-patch+json"}
            };

            const patchObject = {
                "dateMiseEnLigneCalcule" : details['facturation']['dateMiseEnLigneCalcule']
            };
            patchConsultation(details['facturation']['idConsultation'], patchObject, options).then().catch((error) => {
                console.log(error);
            });
        });
    } else {
        console.log('Error : Objet facturation vide ou incomplet');
    }
}

window.addEventListener("update-facturation-validation", handleEventValidation);
