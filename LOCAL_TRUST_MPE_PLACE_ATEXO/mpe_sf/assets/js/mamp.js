J('.object-example-container').on('click', function () {
    const objectMampOData = J(this).data('bodyMampObjects');
    const mampLocalhostPort = 11992;
    const pathMamp = 'http://localhost:' + mampLocalhostPort + '/run-local-application';

    J.ajax({
        type: "POST",
        url: pathMamp,
        data: JSON.stringify(objectMampOData),
    });
})
