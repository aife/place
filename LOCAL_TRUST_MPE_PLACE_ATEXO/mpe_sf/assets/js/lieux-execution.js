/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

$(document).ready(function () {

    $('#selectLieuxExecution').click(function (e) {
        let selected = '';
        let lieux = '';
        if (document.getElementById('form_lieuxExecutionSelect').options.length > 0) {
            for (let i = 0; i < document.getElementById('form_lieuxExecutionSelect').options.length; i++) {
                let denominationOneAndTwo = document.getElementById('form_lieuxExecutionSelect').options[i].value.split('-');
                selected += denominationOneAndTwo[0] + ',';
                lieux += denominationOneAndTwo[1] + '_';
            }
        }
        e.preventDefault();
        popUpSetSize('/index.php?page=Commun.LieuxExecutionCarte&lieux=' + lieux + '&selected=' + selected + '&sf', '850px', '850px', 'yes');
    });

    let lieuxExec = $('#form_lieuxExecutionSelect option');
    if (lieuxExec.length > 0) {
        $('#form_lieuxExecutionSelect').css("height", lieuxExec.last().offset().top - lieuxExec.first().offset().top + 44);
    }
});

global.returnLieuxExecution = function (idSelectedGeo) {
    $('#form_lieuxExecution').val(idSelectedGeo);
    $.post(Routing.generate('lieux_execution_update_liste_lieux'), {
        ids: idSelectedGeo,
    }, function (response) {
        $('#form_lieuxExecutionSelect').replaceWith(response);
        let lieuxExec = $('#form_lieuxExecutionSelect option');
        if (lieuxExec.length > 0) {
            $('#form_lieuxExecutionSelect').css("height", lieuxExec.last().offset().top - lieuxExec.first().offset().top + 44);
        }
    });
}

let newWinSetSize = null;

function popUpSetSize(strURL, strWidth, strHeight, strScrollbars, popupName)
{
    popupName = popupName || 'newWin';
    if (newWinSetSize !== null) {
        if (!newWinSetSize.closed) {
            newWinSetSize.close();
        }
    }

    let strOptions = "";
    strOptions = "toolbar=no,menubar=no,scrollbars=" + strScrollbars + ",resizable,location=no,height=" + strHeight + ",width=" + strWidth;
    newWinSetSize = window.open(strURL, popupName, strOptions);
    newWinSetSize.focus();
}
