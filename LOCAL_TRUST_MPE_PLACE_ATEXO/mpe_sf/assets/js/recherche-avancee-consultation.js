/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'bootstrap';
import '../css/recherche-avancee-consultation.scss';
import '../css/lieux-execution.scss';

import 'block-ui/jquery.blockUI';
import * as ApexCharts from 'apexcharts';
import Chart from 'chart.js/auto';

import './lieux-execution';
import './select2';

import fr from "apexcharts/dist/locales/fr.json";

$('.consultation-block .details-info').on('click', function () {
    if ($(this).find('.hiddenDetails').is(':visible')) {
        $(this).find('.collapsedDetails').show();
        $(this).find('.hiddenDetails').hide();
    } else {
        $(this).find('.collapsedDetails').hide();
        $(this).find('.hiddenDetails').show();
    }
});

$('#heading21').find('.showPlusCriterias').on('click', function () {
    $(this).hide();
    $('.showMinusCriterias').show();
});

$('#heading21').find('.showMinusCriterias').on('click', function () {
    $(this).hide();
    $('.showPlusCriterias').show();
});

$('.nav-link').on('click', function (event) {
    event.stopPropagation();
    if ($(this).hasClass('showDetails')) {
        $('.showDetails').addClass('active');
        $('.hideDetails').removeClass('active');
        localStorage.setItem('showDetailsConsultationLists', 'true');

        $('.consultation-block').each(function () {
            if ($(this).find('.hiddenDetails').is(':visible')) {
                $(this).find('.details-info').trigger('click');
            }
        });
    } else {
        $('.showDetails').removeClass('active');
        $('.hideDetails').addClass('active');
        localStorage.setItem('showDetailsConsultationLists', 'false');

        $('.consultation-block').each(function () {
            if ($(this).find('.collapsedDetails').is(':visible')) {
                $(this).find('.details-info').trigger('click');
            }
        });
    }
});

$(document).on('click', '.consultation-suivie', function () {
    var consultationId = $(this).attr('data-consultation-id');
    var $this = $(this);
    $.ajax({
        method: "PUT",
        url: "../consultations/" + consultationId + "/favori",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            favori : false
        })
    }).done(function (_msg) {
        $this.hide();
        $this.next(".consultation-non-suivie").show();
    });
});
$(document).on('click', '.consultation-non-suivie', function () {
    var consultationId = $(this).attr('data-consultation-id');
    var $this = $(this);
    $.ajax({
        method: "PUT",
        url: "../consultations/" + consultationId + "/favori",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            favori : true
        })
    }).done(function (_msg) {
        $this.hide();
        $this.prev(".consultation-suivie").show();
    });
});

$(document).on('change', '.input-sm', function () {
    var page = Number($(this).val());
    var lastPage = Number($(this).attr('data-last-page'));

    if (page > lastPage) {
        page = lastPage;
    }
    if (page < 1) {
        page = 1;
    }
    var goto = $(this).attr('data-link') + page;
    location.href = goto;
});

$(window).on('load', function () {
    var isDetailsShow = localStorage.getItem('showDetailsConsultationLists');

    if (isDetailsShow == "true") {
        $('.showDetails').trigger('click');
    } else {
        $('.hideDetails').trigger('click');
    }

    if ($('#no-result-message').length == 0) {
        getDataStatCategorie();
        getDataStatStatutCalcule();
        getDataStatTypeProcedure();
    }

    $('[data-toggle="tooltip"]').tooltip();

    getAgentCreateurList();
});

var $primary = "#666ee8",
    $secondary = "#6B6F82",
    $success = "#1C9066",
    $info = "#1E9FF2",
    $warning = "#FF9149",
    $danger = "#FF4961"

var $themeColor = [$primary, $success, $info, $warning, $danger, $secondary];

var ctx = $("#simple-doughnut-chart");
var chartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    responsiveAnimationDuration:500,
};
var chartData = {
    labels: [],
    datasets: [{
        label: null,
        data: [],
        backgroundColor: [],
    }]
};
var config = {
    type: 'doughnut',
    options : chartOptions,
    data : chartData
};


var columnBasicChart = {
    chart: {
        height: 350,
        type: 'bar',
        locales: [fr],
        defaultLocale: 'fr'
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '55%'
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Catégorie principale',
        data: []
    }],
    xaxis: {
        categories: [],
    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val + " consultation(s)"
            }
        }
    },
    fill: {
        opacity: 1,
        colors: ["#1E9FF2", "#1E9FF2", "#1E9FF2", "#1E9FF2", "#1E9FF2", "#1E9FF2"]
    }
}

var barBasicChart = {
    chart: {
        height: 350,
        type: 'bar',
        locales: [fr],
        defaultLocale: 'fr'
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        data: []
    }],
    xaxis: {
        categories: [],
    },
    fill: {
        colors: $themeColor
    }
}

const currentUrl = window.location.href;
const url = currentUrl.replace('/recherche', '/recherche/statistiques');

function getDataStatStatutCalcule()
{
    $.get({
        url: url,
        data: {
            'groupBy': 'statutCalcule'
        }
    }).done(function (data) {
        chartData.datasets[0].data = data.totals
        chartData.datasets[0].backgroundColor = data.colors;
        chartData.labels = data.names

        new Chart(ctx, config);
    });
}

function getDataStatCategorie()
{
    $.get({
        url: url,
        data: {
            'groupBy': 'categorie'
        }
    }).done(function (data) {
        if (Object.keys(data).length > 0) {
            columnBasicChart.series[0].data = data.totals
            columnBasicChart.xaxis.categories = data.names
            var column_basic_chart = new ApexCharts(
                document.querySelector("#column-basic-chart"),
                columnBasicChart
            );
            column_basic_chart.render();
        }
    });
}

function getDataStatTypeProcedure()
{
    $.get({
        url: url,
        data: {
            'groupBy': 'typeProcedure'
        }
    }).done(function (data) {
        if (Object.keys(data).length > 0) {
            barBasicChart.series[0].data = data.totals
            barBasicChart.xaxis.categories = data.names
            var bar_basic_chart = new ApexCharts(
                document.querySelector("#bar-basic-chart"),
                barBasicChart
            );
            bar_basic_chart.render();
        }
    });
}

$('.card--chart').ready(function () {
    var $cardChart = $('.card--chart');

    $cardChart.block({
        message: '<div class="ft-refresh-cw la-spin font-medium-2"></div>',
        timeout: 2000,
        overlayCSS: {
            backgroundColor: '#FFF',
            cursor: 'wait',
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
});

// Activate tooltip
$('[data-toggle="tooltip"]').tooltip();

function getAgentCreateurList()
{
    // Initialize Select2
    $('.selectAgentReferent').select2({ disabled: true, width: "100%" });

    // Make an Ajax request
    $.ajax({
        url: searchAgentReferentPath,
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            // Process the received data and populate Select2
            const selectorAgent  = $('.selectAgentReferent');
            let options = [];

            // Assuming the data is an array of objects with 'id' and 'text' properties
            data.forEach(function(item) {
                options.push({
                    id: item.id,
                    text: item.text
                });
            });

            // Set the data in Select2
            selectorAgent.select2(
                {
                    data: options,
                    language: {
                        noResults: function () {
                            return notFoundMessage;
                        },
                        searching: function () {
                            return searchingMessage;
                        },
                    },
                    width: "100%",
                }
            );

            // Enable the Select2 dropdown
            selectorAgent.prop('disabled', false);
        }
    });
}

$('#saveRechercheModal .alerteValidation [data-dismiss=modal]').on('click', function (e) {
    var searchName = document.getElementById('search-name').value;
    if (searchName.trim() !== '') {
        var xhr = new XMLHttpRequest();
        var url = Routing.generate('agent_recherches_sauvegardees_save');

        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        var data = {
            'searchName': searchName,
            'queryStringToJSON': QueryStringToJSON(window.location.search)
        };
        xhr.onreadystatechange = function () {};

        var jsonData = JSON.stringify(data);
        xhr.send(jsonData);
    }else {
        e.stopPropagation();
        document.getElementById('alertEmptySearchName').style.display = 'block'
    }
})

function QueryStringToJSON(url) {
    const params = new URLSearchParams(url);
    const json = {};

    params.forEach((value, key) => {
        if (key.includes("[]")) {
            const cleanKey = key.replace("[]", "");
            if (!json[cleanKey]) {
                json[cleanKey] = [];
            }
            json[cleanKey].push(value);
        } else {
            json[key] = value;
        }
    });

    return json;
}

document.addEventListener("DOMContentLoaded", function() {
    var buttons = document.querySelectorAll(".deleteRechecheBtn");

    buttons.forEach(function(button) {
        button.addEventListener("click", function() {
            var id = button.getAttribute("data-id");
            var confirmed = window.confirm("Êtes-vous sûr de bien vouloir supprimer cette alerte ?");
            
            if (confirmed) {
                fetch(Routing.generate('agent_recherches_sauvegardees_delete', {'id': id}), {
                method: 'DELETE'
                })
                .then(result => {
                    var elementToRemove = document.getElementById('lineToHide' + id);
                    if (elementToRemove) {
                        elementToRemove.remove();
                    }
                })
                .catch(error => console.error('Error:', error));
            }
        });
    });
})