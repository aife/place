/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import $ from "jquery";
import '../css/interface-suivi.scss';

$('#flux').select2();
$('#action').select2();
$('#statut').select2();
$('#etape').select2();

$('.icheck').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
});

$('#dateEnvoi').daterangepicker({
    locale: {
        "format": 'DD/MM/YYYY',
        "separator": " - ",
        "applyLabel": "Valider",
        "cancelLabel": "Annuler",
        "fromLabel": "De",
        "toLabel": "à",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Dim",
            "Lun",
            "Mar",
            "Mer",
            "Jeu",
            "Ven",
            "Sam"
        ],
        "monthNames": [
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
        ],
        "firstDay": 1
    },
    opens: 'left'
});

var checkAll = $('.checkAll');
var checkboxes = $('.icheck');

checkAll.on('ifChecked ifUnchecked', function(event) {
    if (event.type == 'ifChecked') {
        checkboxes.iCheck('check');
    } else {
        checkboxes.iCheck('uncheck');
    }
});

checkboxes.on('ifChanged', function(event){
    if(checkboxes.filter(':checked').length == checkboxes.length) {
        checkAll.prop('checked', 'checked');
    } else {
        checkAll.removeProp('checked');
    }
    checkAll.iCheck('update');
});

$(document).on('change', '.goto-page', function () {
    var page = Number($(this).val());
    var lastPage = Number($(this).attr('data-last-page'));

    if (page > lastPage) {
        page = lastPage;
    }
    if (page < 1) {
        page = 1;
    }
    var goto = $(this).attr('data-link') + page;
    location.href = goto;
});

$('.calendarTriggerEvent').on('click', function() {
    $('#dateEnvoi').trigger('click');
});
