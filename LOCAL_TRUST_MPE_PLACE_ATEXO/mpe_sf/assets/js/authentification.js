import $ from 'jquery';

$(document).ready(function() {
    $('#showPasswordAction').on('click', function(){
        if ($(this).find('.showPassword').is(':visible')) {
            $('#form__password').attr('type', 'text');

            $(this).find('.showPassword').hide();
            $(this).find('.hidePassword').show();
        } else {
            $('#form__password').attr('type', 'password');

            $(this).find('.showPassword').show();
            $(this).find('.hidePassword').hide();
        }
    });
});