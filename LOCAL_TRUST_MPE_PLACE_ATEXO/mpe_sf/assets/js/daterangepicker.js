/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'daterangepicker/daterangepicker.css'
import 'daterangepicker/daterangepicker.js'
