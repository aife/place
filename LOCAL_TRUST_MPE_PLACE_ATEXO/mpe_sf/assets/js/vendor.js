// assets/js/vendor.js
import '../css/vendor.scss';
import $ from 'jquery';
import Vue from 'vue';
import Promise from 'es6-promise';
Promise.polyfill();
import axios from 'axios';
import 'bootstrap';

global.$ = global.jQuery = $;
