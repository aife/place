import $ from 'jquery';
import './bootstrap-filestyle.min.js';
import '../css/verification-signature.scss';

$('.filestyle').filestyle({
    btnClass: 'btn-primary',
    buttonBefore: true,
    htmlIcon: '<i class="ft-file"></i>'
});

$('.buttonText').each(function () {
    let textReplaceAttr = $(this).closest('.col-12').find('.filestyle').attr('data-text');

    $(this).html(textReplaceAttr);
});

$(document).ready(function () {
    var ref = $('.tooltip-evt');
    var $tooltip = $('.tooltip');

    $tooltip.hide();

    ref.mouseover(function () {
        $(this).parent().next($tooltip).show();
    });
    ref.mouseout(function () {
        $(this).parent().next($tooltip).hide();
    });
});
