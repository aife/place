/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

var habilitationSectionHeaderSelector = '.js-habilitation-section-header';
var habilitationSectionSelector = '.js-habilitation-section';
var habilitationSectionExpandedClass = 'habilitation-section--expanded';
var habilitationSectionCheckboxSelector = '.js-habilitation-section-checkbox';
var closeWindowSelector = '.js-close-window';
var treeToggleSelector = '.js-services-tree-toggle';
var treeParentSelector = '.js-services-tree-parent';
var treeItemExpandedClass = 'services-tree__item--expanded';
var serviceTreeCheckboxSelector = '.js-service-tree-checkbox';
var serviceTreeSelector = '.js-service-tree';

$(document).ready(function () {
    var sectionNames = [
        'gestionConsultationSection',
        'gestionContratSection',
        'recensementSection',
        'gestionPerimetreInterventionSection',
        'redactionPieceMarcheSection',
        'annuaireSection',
        'gestionAutresAnnoncesSection',
        'gestionOperationsSection',
        'administrationMetierSection',
        'archiveSection',
        'parametrageServiceSection',
        'newsletterSection',
        'societesExcluesSection',
        'espaceCollaboratifSection',
        'responsableMinisterielAchatSection',
        'spaserSection',
        'documentsModelesSection'
    ];

    function loadSections(names) {
        if (names.length === 0) {
            $(document).trigger('atexo.habilitations.loaded');
            return;
        }

        var sectionName = names.shift();

        $.ajax({
            url: '/agent/habilitations/' + agentId + '/load-section/' + sectionName + '?mode=' + mode,
            method: 'GET',
            success: function (data) {
                $('.js-sections').append(data);
                loadSections(names);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error('Erreur lors du chargement de la section:', errorThrown);
                loadSections(names);
            }
        });
    }

    function loadCreerConsultation() {
        var sectionName = 'creerConsultationSection';

        $.ajax({
            url: '/agent/habilitations/' + agentId + '/load-section/' + sectionName + '?mode=' + mode,
            method: 'GET',
            success: function (data) {
                $('.js-section-creer-consultation').append(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error('Erreur lors du chargement de la section:', errorThrown);
            },
            complete: function () {
                loadCreerSuiteConsultation()
            }
        });
    }

    function loadCreerSuiteConsultation() {
        var sectionName = 'creerSuiteConsultationSection';

        $.ajax({
            url: '/agent/habilitations/' + agentId + '/load-section/' + sectionName + '?mode=' + mode,
            method: 'GET',
            success: function (data) {
                $('.js-section-creer-suite-consultation').append(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error('Erreur lors du chargement de la section:', errorThrown);
            },
            complete: function () {
                $(document).trigger('atexo.subhabilitations.loaded');
            }
        });
    }

    function updateCheckboxN2(selector) {
        var $parentSection = selector.closest( '.habilitation-section.habilitation-section--n2');
        var $checkboxes = $parentSection.find('input[type="checkbox"]:not(.js-habilitation-section-checkbox)');
        var $checkedCheckboxes = $parentSection.find('input[type="checkbox"]:not(.js-habilitation-section-checkbox):checked');
        if ($checkedCheckboxes.length === 0) {
            $parentSection.find('.js-habilitation-section-checkbox:first').prop('checked', false);
            $parentSection.find('.js-habilitation-section-checkbox:first').prop('indeterminate', false);
        } else if ($checkedCheckboxes.length === $checkboxes.length) {
            $parentSection.find('.js-habilitation-section-checkbox:first').prop('checked', true);
            $parentSection.find('.js-habilitation-section-checkbox:first').prop('indeterminate', false);
        } else {
            $parentSection.find('.js-habilitation-section-checkbox:first').prop('indeterminate', true);
        }
    }

    function addEventListeners() {
        $(habilitationSectionHeaderSelector).off('click').on('click', function (e) {
            if ($(e.target).hasClass('js-habilitation-section-checkbox')) {
                return;
            }
            $(this).parent(habilitationSectionSelector).toggleClass(habilitationSectionExpandedClass);
        });

        $(habilitationSectionCheckboxSelector).off('click').on('click', function (e) {
            var $parentSection = $(this).closest(habilitationSectionSelector);
            if ($(this).is(':checked')) {
                $parentSection.find('input[type="checkbox"]').prop('checked', true);
            } else {
                $parentSection.find('input[type="checkbox"]').prop('checked', false);
            }
        });

        $(habilitationSectionCheckboxSelector).off('refresh').on('refresh', function (e) {
            var $parentSection = $(this).closest(habilitationSectionSelector);
            var $checkboxes = $parentSection.find('input[type="checkbox"]:not(' + habilitationSectionCheckboxSelector + ')');
            var $checkedCheckboxes = $parentSection.find('input[type="checkbox"]:not(' + habilitationSectionCheckboxSelector + '):checked');

            if ($checkedCheckboxes.length === 0) {
                $(this).prop('checked', false);
                $(this).prop('indeterminate', false);
            } else if ($checkedCheckboxes.length === $checkboxes.length) {
                $(this).prop('checked', true);
                $(this).prop('indeterminate', false);
            } else {
                $(this).prop('checked', false);
                $(this).prop('indeterminate', true);
            }
        });

        $('.habilitation-section.habilitation-section--n3 .js-habilitation-section-checkbox').on('refresh', function (e) {
            updateCheckboxN2($(this));
        });

        $('.habilitation-section.habilitation-section--n3 .js-habilitation-section-checkbox').off('change').on('change', function (e) {
            updateCheckboxN2($(this));
        });

        $('input[type="checkbox"]:not(' + habilitationSectionCheckboxSelector + ')').off('change').on('change', function (e) {
            var $parentSection = $(this).closest(habilitationSectionSelector);
            var $sectionCheckbox = $parentSection.find(habilitationSectionCheckboxSelector);

            if ($sectionCheckbox) {
                $sectionCheckbox.trigger('refresh');
            }
        });

        $(treeToggleSelector).click(function () {
            $(this).parent(treeParentSelector).toggleClass(treeItemExpandedClass);
        });

        $(serviceTreeCheckboxSelector+':checked').each(function () {
            $(this).parents(treeParentSelector).addClass(treeItemExpandedClass);
        });

        if ($(serviceTreeCheckboxSelector+':checked').length) {
            $(serviceTreeSelector)
                .closest(habilitationSectionSelector)
                .addClass(habilitationSectionExpandedClass);
        }

        $(closeWindowSelector).off('click').on('click', function () {
            window.close();
        });

        $(habilitationSectionCheckboxSelector).trigger('refresh');
    }

    function loaded() {
        addEventListeners();

        $('.js-loader').fadeOut(300, function () {
            $('.habilitations').removeClass('habilitations--loading');
        });
    }

    loadSections(sectionNames);

    $(document).on('atexo.habilitations.loaded', function () {
        loadCreerConsultation();
    });
    $(document).on('atexo.subhabilitations.loaded', function () {
        loaded();
    });
});
