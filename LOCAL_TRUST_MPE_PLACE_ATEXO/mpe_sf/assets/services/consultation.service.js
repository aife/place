/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    postConsultation(data, config) {
        return axios.post('/api/v2/consultations', data, config);
    },
    getConsultationById(consultationId, config) {
        return axios.get('/api/v2/consultations/' + consultationId, config);
    },
    patchConsultation(consultationId, data, config) {
        return axios.patch('/api/v2/consultations/' + consultationId, data, config);
    },
    getConsultationFilesById(consultationId, config) {
        return axios.get('/api/v2/consultations/' + consultationId + '/dce', config);
    },
    postConsultationDCE(consultationId, data, config) {
        return axios.post('/api/v2/consultations/' + consultationId + '/documents/dce', data, config);
    },
    postConsultationRC(consultationId, data, config) {
        return axios.post('/api/v2/consultations/' + consultationId + '/documents/rc', data, config);
    },
    postConsultationAutrePiece(consultationId, data, config) {
        return axios.post('/api/v2/consultations/' + consultationId + '/documents/autre-piece', data, config);
    },
    putConsultationAttenteValidation(consultationId, config) {
        return axios.put('/api/v2/consultations/' + consultationId + '/attente-validation', {}, config);
    },
    putDuplicateParent(consultationId, data, config) {
        return axios.put('/api/v2/consultations/' + consultationId + '/duplicate-parent', data, config);
    },
    getParametrageModules(config) {
        return axios.get('/api/v2/parameters-modules', config);
    },
    getParametrageProcedure(typeProcedureId, config) {
        return axios.get('/api/v2/parameters-procedures/' + typeProcedureId, config);
    },
    getParametrageProcedureDume(config) {
        return axios.get('/api/v2/parameters-procedures-dumes', config);
    },
    getGeolocalisationN2(config) {
        return axios.get('/api/v2/geolocalisation-n2s', config);
    },
    getDefaultParameters() {
        return axios.get('/agent/consultation/parameters');
    },
    getLieuMap() {
        return axios.get('/agent/consultation/lieu/map');
    }
};
