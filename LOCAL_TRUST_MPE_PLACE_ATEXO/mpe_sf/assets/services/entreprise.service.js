/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getEntrepriseById(entrepriseId, config) {
        return axios.get('/api/v2/entreprises/' + entrepriseId, config);
    },
    getEtablissementById(etablissementId, config) {
        return axios.get('/api/v2/etablissements/' + etablissementId, config);
    }
};
