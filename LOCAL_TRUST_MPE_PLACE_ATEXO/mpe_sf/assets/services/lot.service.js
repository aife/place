/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getLots(config) {
        return axios.get('/api/v2/lots', config);
    },
};
