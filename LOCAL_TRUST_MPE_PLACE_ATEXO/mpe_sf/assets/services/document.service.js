/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getDCEFileDescriptor(config) {
        return axios.get('/agent/document/dce/file-descriptor', config);
    }
};
