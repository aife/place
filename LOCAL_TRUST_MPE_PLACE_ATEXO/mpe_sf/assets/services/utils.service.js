/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

export default {
    thousandSeparator(number) {
        number += '';
        number = number.replace(/[^\d.,]/g, '');
        if (!number) {
            return '0';
        }
        return new Intl.NumberFormat('fr-FR', {
            style: 'decimal',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        }).format(parseFloat(number));
    },
    generateRandomCode(length = 9) {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        let code = '';
        for (let i = 0; i < length; i++) {
            code += characters.charAt(Math.floor(Math.random() * characters.length));
        }

        return code;
    },
    getFormatedSize(size) {
        let i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    },
    getLastSplitFromIri(iri) {
        if (!iri) {
            return null;
        }
        let splitIri = iri.split('/');
        return splitIri[splitIri.length - 1];
    },
    replaceCharacters(inputField) {
        const replacements = [
            { search: "<", replace: "< " },
            { search: ">", replace: " >" },
            { search: "-", replace: "-" },
            { search: "'", replace: "'" },
            { search: "_", replace: "_" },
            { search: '\u2264', replace: "<=" },
            { search: '\u2265', replace: ">=" },
            { search: '\u00A9', replace: "" },
            { search: '\u201D', replace: "\"" },
            { search: '\u201C', replace: "\"" },
            { search: '\u0152', replace: "OE" },
            { search: '\x0133', replace: ".." },
            { search: '\x243', replace: "<=" },
            { search: '\x242', replace: ">=" },
            { search: '\x0156', replace: "oe" },
            { search: '\x8212', replace: "--" },
            { search: '\x0145', replace: "'" },
            { search: '\x0146', replace: "'" },
            { search: '\x0147', replace: "\"" },
            { search: '\x0148', replace: "\"" },
            { search: '\x174', replace: "\"" },
            { search: '\x175', replace: "\"" },
            { search: '\x0192', replace: "A" },
            { search: '\x0193', replace: "A" },
            { search: '\x0194', replace: "A" },
            { search: '\x0195', replace: "A" },
            { search: '\x0196', replace: "A" },
            { search: '\x0197', replace: "A" },
            { search: '\x0200', replace: "E" },
            { search: '\x0201', replace: "E" },
            { search: '\x0202', replace: "E" },
            { search: '\x0203', replace: "E" },
            { search: '\x0204', replace: "I" },
            { search: '\x0205', replace: "I" },
            { search: '\x0206', replace: "I" },
            { search: '\x0207', replace: "I" },
            { search: '\x0210', replace: "O" },
            { search: '\x0211', replace: "O" },
            { search: '\x0212', replace: "O" },
            { search: '\x0213', replace: "O" },
            { search: '\x0214', replace: "O" },
            { search: '\x0217', replace: "U" },
            { search: '\x0218', replace: "U" },
            { search: '\x0219', replace: "U" },
            { search: '\x0220', replace: "U" },
        ];

        replacements.forEach(replacement => {
            inputField = inputField.replace(new RegExp(replacement.search, 'g'), replacement.replace);
        });

        return inputField;
    },
    resize(element, maximum) {
        if (element.scrollHeight <= maximum) {
            element.style.height = element.scrollHeight + "px";
        }
    },
    truncate(text, stop, clamp) {
        return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
    },
    countDaysDifference(dateStart, dateEnd) {
        const timeDifference = dateEnd.getTime() - dateStart.getTime();

        return Math.floor(timeDifference / (1000 * 60 * 60 * 24));
    },
};
