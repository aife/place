/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

export default {
    getAlreadySelectedCpv(consultation) {
        let alreadySelectedReferences = [];

        if (consultation.codeCpvPrincipal) {
            alreadySelectedReferences.push(consultation.codeCpvPrincipal);
        }
        if (consultation.codeCpvSecondaire1) {
            alreadySelectedReferences.push(consultation.codeCpvSecondaire1);
        }
        if (consultation.codeCpvSecondaire2) {
            alreadySelectedReferences.push(consultation.codeCpvSecondaire2);
        }
        if (consultation.codeCpvSecondaire3) {
            alreadySelectedReferences.push(consultation.codeCpvSecondaire3);
        }

        return alreadySelectedReferences;
    }
};
