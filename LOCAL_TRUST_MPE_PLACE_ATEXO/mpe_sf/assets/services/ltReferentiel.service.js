import axios from "axios";

export default {
    getReferentiel(consultationId, config) {
        return axios.get('/api/v2/consultations/' + consultationId + '/referentiels', config);
    },
    postReferentiel(consultationId, data, config) {
        return axios.post('/api/v2/consultations/' + consultationId + '/referentiels', data, config);
    },
    putReferentiel(consultationId, data, config) {
        return axios.put('/api/v2/consultations/' + consultationId + '/referentiels', data, config);
    },
    preProcessCodesNacres(token, consultationId) {
        const options = {
            headers: {Authorization: `Bearer ${token}`, 'Accept': 'application/json'}
        };

        return this.getReferentiel(consultationId, options).then((response) => {
            let selectedReferentiels = [];
            if (response.data.list && response.data.list['referentiels'] && response.data.list['referentiels']['NOMENCLATURE_ACHAT']) {
                selectedReferentiels.push(response.data.list['referentiels']['NOMENCLATURE_ACHAT']['valeurPrincipale']);
                if (response.data.list['referentiels']['NOMENCLATURE_ACHAT']['valeurSecondaire'] !== '') {
                    let splitSecondaire = response.data.list['referentiels']['NOMENCLATURE_ACHAT']['valeurSecondaire'].split('#');
                    splitSecondaire.forEach((element) => {
                        if (element !== '') {
                            selectedReferentiels.push(element);
                        }
                    });
                }
            }
            return selectedReferentiels;
        });
    },
    handleCodesNacres(token, consultationId, selectedReferentiels, creation) {
        let valeurPrincipale = '';
        let valeurSecondaire = '';

        selectedReferentiels.forEach((element, index) => {
            if (index === 0) {
                valeurPrincipale = element['code'];
            } else {
                valeurSecondaire += '#' + element['code'];
            }
        });

        if (valeurPrincipale !== '') {
            const options = {
                headers: {Authorization: `Bearer ${token}`, 'Content-Type': 'application/json'}
            };

            let body = {
                'NOMENCLATURE_ACHAT' : {
                    'valeurPrincipale' : valeurPrincipale,
                    'valeurSecondaire' : valeurSecondaire
                }
            };

            if (creation) {
                return this.postReferentiel(consultationId, body, options);
            } else {
                return this.putReferentiel(consultationId, body, options);
            }
        }
    }
}
