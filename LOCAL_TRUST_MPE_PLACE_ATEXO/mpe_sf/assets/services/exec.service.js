/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getContrats(config) {
        return axios.get('/execution/webservices/api/v2/contrats/', config);
    },
    getContratByUuid(uuid, config) {
        return axios.get('/execution/webservices/api/v2/contrats/' + uuid, config);
    },
    getContratsLies(uuid, config) {
        return axios.get('/execution/webservices/api/v2/contrats/' + uuid + '/contrats-lies', config);
    }
};
