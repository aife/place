/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getReferentielsN1(config) {
        return axios.get('/api/v2/referentiels/clauses-n1s', config);
    },
    getReferentielsN2(config) {
        return axios.get('/api/v2/referentiels/clauses-n2s', config);
    },
    getReferentielsN3(config) {
        return axios.get('/api/v2/referentiels/clauses-n3s', config);
    },
    getReferentielsN4(config) {
        return axios.get('/api/v2/referentiels/clauses-n4s', config);
    }
};
