/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
import axios from "axios";

export default {
    getAll() {
        return axios.get('/agent/translation/messages');
    }
};
