/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getToken() {
        return axios.get('/agent/token-user-connected');
    },
    getExecToken() {
        return axios.get('/agent/exec/token-user-connected');
    }
};
