/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getAgentById(agentId, config) {
        return axios.get('/api/v2/agents/' + agentId, config);
    },
    patchAgent(agentId, data, config) {
        return axios.patch('/api/v2/agents/' + agentId, data, config);
    }
};
