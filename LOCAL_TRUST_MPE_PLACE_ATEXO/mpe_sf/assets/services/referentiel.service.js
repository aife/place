/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getOrganismeById(organismeId, config) {
        return axios.get('/api/v2/referentiels/organismes/' + organismeId, config);
    },
    getServiceById(serviceId, config) {
        return axios.get('/api/v2/referentiels/services/' + serviceId, config);
    },
    getCategories(config) {
        return axios.get('/api/v2/referentiels/nature-prestations', config);
    },
    getCategorieById(categorieId, config) {
        return axios.get('/api/v2/referentiels/nature-prestations/' + categorieId, config);
    },
    getTypeContrats(config) {
        return axios.get('/api/v2/referentiels/contrats', config);
    },
    getTypeContratById(typeId, config) {
        return axios.get('/api/v2/referentiels/contrats/' + typeId, config);
    },
    getTypeProcedures(config) {
        return axios.get('/api/v2/referentiels/procedures', config);
    },
    getTypeProceduresForSuite(config) {
        return axios.get('/api/v2/referentiels/procedures-suite', config);
    },
    getTypeProceduresForCreation(config) {
        return axios.get('/api/v2/referentiels/procedures-consultation-creation', config);
    },
    getTypeProcedureById(procedureId, config) {
        return axios.get('/api/v2/referentiels/procedures/' + procedureId, config);
    }
};
