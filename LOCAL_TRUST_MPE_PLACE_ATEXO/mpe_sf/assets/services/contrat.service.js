/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getContrats(config) {
        return axios.get('/api/v2/contrat-titulaires', config);
    },
    getContratById(contratId, config) {
        return axios.get('/api/v2/contrat-titulaires/' + contratId, config);
    },
    getContratContractants(contratId, config) {
        return axios.get('/api/v2/contrat-titulaires/' + contratId + '/contractants', config);
    }
};
