/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getConsultationTags(config) {
        return axios.get('/api/v2/consultation-tags', config);
    },
    postConsultationTags(data, config) {
        return axios.post('/api/v2/consultation-tags', data, config);
    },
    deleteConsultationTags(tagId, config) {
        return axios.delete('/api/v2/consultation-tags/' + tagId, config);
    }
};
