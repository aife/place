/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import axios from 'axios';

export default {
    getRedacAuthToken() {
        return axios.get('/agent/redaction/auth');
    },
    getRedacListFiles(data) {
        return axios.post('/agent/redaction/list-files', data);
    },
    downloadRedacFile(data) {
        return axios.post('/agent/redaction/download', data);
    }
};
