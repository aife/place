
let Encore = require('@symfony/webpack-encore');
const PurgeCssPlugin = require('purgecss-webpack-plugin');
const glob = require('glob-all');
const path = require('path');
const FosRouting = require('fos-router/webpack/FosRouting');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    .autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        moment: 'moment'
    })
    // public path used by the web server to access the output path
    .setPublicPath('/build/')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('vendor', './assets/js/vendor.js')
    .addEntry('app', './assets/js/app.js')
    .addEntry('modern-admin', './assets/modern-admin/index.js')
    .addEntry('accueil-agent', './assets/js/accueil-agent.js')
    .addEntry('espace-documentaire', './assets/components/espace-documentaire/index.js')
    .addEntry('echange-document', './assets/js/echange-document.js')
    .addEntry('assistance-status-checker', './assets/components/assistance-status-checker/index.js')
    .addEntry('assistance-launch', './assets/components/assistance-status-checker/assistance-launch.js')
    .addEntry('declaration-group', './assets/components/declaration-group/index.js')
    .addEntry('enchere', './assets/js/enchere.js')
    .addEntry('consultation-simplifiee', './assets/js/consultation-simplifiee.js')
    .addEntry('prerequis-techniques', './assets/js/prerequis-techniques.js')
    .addEntry('annexe-financiere', './assets/js/annexe-financiere.js')
    .addEntry('error-page', './assets/js/error-page.js')
    .addEntry('criteria-attribution-analyse-offre', './assets/components/analyse-offre-critere-attribution/index.js')
    .addEntry('recherche-avancee-consultation', './assets/js/recherche-avancee-consultation.js')
    .addEntry('agent-header-statistics', './assets/components/agent-statistics/index.js')
    .addEntry('menu-agent-action', './assets/components/menu-agent/index.js')
    //.addEntry('page-decision-contrat', './assets/components/contrat/index.js')
    .addEntry('module-autoformation-rubrique', './assets/components/autoformation/rubrique.js')
    .addEntry('module-autoformation-module', './assets/components/autoformation/module.js')
    .addEntry('module-autoformation', './assets/components/autoformation/index.js')
    .addEntry('rgpd', './assets/components/rgpd/modal.js')
    .addEntry('clauses', './assets/components/clauses/index.js')
    .addEntry('elaboration-consultation', './assets/components/consultation/indexElaboration.js')
    .addEntry('modification-consultation', './assets/components/consultation/indexModificationEnLigne.js')
    .addEntry('interface-suivi', './assets/js/interface-suivi.js')
    .addEntry('daterangepicker', './assets/js/daterangepicker.js')
    .addEntry('icheck', './assets/js/icheck.js')
    .addEntry('select2', './assets/js/select2.js')
    .addEntry('entreprise-search-multiselect-cpv', './assets/components/search-entreprise/index.js')
    .addEntry('authentification', './assets/js/authentification.js')
    .addEntry('verification-signature', './assets/js/verification-signature.js')
    .addEntry('hinclude', './assets/js/hinclude.js')
    .addEntry('mamp', './assets/js/mamp.js')
    .addEntry('message-accueil', './assets/js/message-accueil.js')
    .addEntry('validation-eco-sip', './assets/js/validation-eco-sip.js')
    .addEntry('consultation-validation-buttons', './assets/js/consultation-validation-buttons.js')
    .addEntry('recherche-organisme', './assets/js/recherche-organisme.js')
    .addEntry('habilitations', './assets/js/habilitations.js')
    .copyFiles([
    {from: './node_modules/ckeditor4/', to: 'ckeditor/[path][name].[ext]', pattern: /\.(js|css)$/, includeSubdirectories: false},
    {from: './node_modules/ckeditor4/adapters', to: 'ckeditor/adapters/[path][name].[ext]'},
    {from: './node_modules/ckeditor4/lang', to: 'ckeditor/lang/[path][name].[ext]'},
    {from: './node_modules/ckeditor4/plugins', to: 'ckeditor/plugins/[path][name].[ext]'},
    {from: './node_modules/ckeditor4/skins', to: 'ckeditor/skins/[path][name].[ext]'},
    {from: './node_modules/ckeditor4/vendor', to: 'ckeditor/vendor/[path][name].[ext]'}
])


    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // uncomment if you use TypeScript
    // .enableTypeScriptLoader()

    // enables Sass/SCSS support
    .enableSassLoader()

    .enableVueLoader()

    .addStyleEntry('layout-agent-fonts', './assets/css/fonts.scss')
    .addStyleEntry('layout-agent-prado', [
        './legacy/protected/themes/css/bootstrap-custom.css',
        './legacy/protected/themes/css/font-awesome.min.css',
        './assets/modern-admin/app-assets/fonts/line-awesome/css/line-awesome.css',
        './legacy/protected/themes/css/mpe-menu/bootstrap-mpe-nav.css',
        './legacy/protected/themes/css/mpe-menu/bootstrap-mpe-nav-extended.css',
        './legacy/protected/themes/css/mpe-menu/components.css',
        './legacy/protected/themes/css/mpe-menu/horizontal-menu.css',
    ])
    .addStyleEntry('layout-agent-shared', [
        './assets/css/mpe-new.scss',
        './assets/css/footer.scss',
        './assets/css/assistance.scss',
    ])

    .addStyleEntry('modern-admin-entreprise', './assets/css/modern-admin-entreprise/modern-admin-entreprise.scss')

    .addStyleEntry('outilsSignatures', './assets/css/outils-signatures.scss')

    .addStyleEntry('critere-attribution-analyse-offre', './assets/css/critere-attribution-analyse-offre.scss')

    .addPlugin(new PurgeCssPlugin({
        paths: glob.sync([
            path.join(__dirname, 'templates/**/*.html.twig'),
            path.join(__dirname, 'src/**/*.php'),
            path.join(__dirname, 'assets/components/**/*.vue'),
            path.join(__dirname, 'node_modules/daterangepicker/daterangepicker.js'),
            path.join(__dirname, 'node_modules/vue-multiselect/dist/vue-multiselect.js'),
            path.join(__dirname, 'node_modules/@vuepic/vue-datepicker/dist/vue-datepicker.js'),
            path.join(__dirname, 'node_modules/@kyvg/vue3-notification/dist/index.es.js'),
        ]),
        safelist: {
            deep: [
                /menu_level_[0-9]/,
                /dropdown-custom/,
                /^ct-/,             // préfixe des classes CSS de Chartist
                /^fa-/,
                /^select2-/,
                /^ft-/,
                /^bg-/,
                /^la-/,
                /^tooltip/,
                /^bs-tooltip-/,
                /^alert-color-/,
                /statisticsReports/, // Iframe module BI
                /uppy-/,
            ]
        },
        defaultExtractor: (content) => {
            return content.match(/[\w-/:]+(?<!:)/g) || [];
        }
    }))
    .addPlugin(new FosRouting())

// uncomment if you're having problems with a jQuery plugin
//.autoProvidejQuery();

config = Encore.getWebpackConfig();
config.module.rules.push({
    test: /\.mjs$/,
    include: /node_modules/,
    type: 'javascript/auto'
});
/*
config.resolve.alias = {
    // Force all modules to use the same jquery version.
    'jquery': path.join(__dirname, 'node_modules/jquery/src/jquery')
};*/
module.exports = config;
