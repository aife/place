#!/usr/bin/env bash
#
# Copyright (C) Atexo - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
#
SCRIPTDIR="$( cd "$(dirname "$0")" ; pwd -P )"
theme_source="themes"
theme_destination="mpe_sf/public/themes"

cd $SCRIPTDIR/../..

tempTarFile="/tmp/temp.theme.tar"

find ${theme_destination} -regex '.*\.\(fr\|en\|es\|du\|ar\|it\|su\)\.js' -exec tar -rvf ${tempTarFile} {} \; > /dev/null
rm -rf ${theme_destination}/*
tar -xf ${tempTarFile}
rm ${tempTarFile}

cp -R ${theme_source}/portal/* ${theme_destination}/
cp -Rn ${SCRIPTDIR}/protected/themes/* ${theme_destination}/

if [ -e ${theme_source}/portal/images/favicon.ico ]
 then
   cp -f ${theme_source}/portal/images/favicon.ico ${SCRIPTDIR}/../public/
 else
   cp -n ${SCRIPTDIR}/protected/themes/mobile/images/favicon.ico ${SCRIPTDIR}/../public/
fi

ls ${theme_source}/

for i in $(ls ${theme_source}/)
do
  if [ "$i" != 'portal' ] && [[ -d "${theme_source}/$i" ]]
  then
    echo "$i"
    mkdir -p "${theme_destination}/$i"
    cp -R ${theme_source}/$i/* "${theme_destination}/$i/"
    cp -Rn ${theme_source}/portal/* "${theme_destination}/$i/"
    cp -Rn ${SCRIPTDIR}/protected/themes/* "${theme_destination}/$i/"
  fi
done
