#!/bin/bash
extractLibelleFromCodeToFile()
{
#	echo 'Begin script:'
	egrep -ir "(prado::localize|Prado::localize|Prado::localize|<com:TTranslate>|<com:TTranslate )" $1 | grep -v "/config/messages/"  > /tmp/localize
	iconv -f iso-8859-1 -t UTF-8 -o /tmp/localize8 /tmp/localize
	for ((i=1; i <= 5; i++)) do
	    sed -i 's/Prado::localize/\nPrado::localize/ig' /tmp/localize8
	    sed -i 's/<com:TTranslate>/\n<com:TTranslate>/ig' /tmp/localize8
            sed -i 's/prado::localize/\nPrado::localize/ig' /tmp/localize8
            sed -i 's/<com:TTranslate >/\n<com:TTranslate>/ig' /tmp/localize8
            sed -i 's/prado::localize/\nPrado::localize/ig' /tmp/localize8
	done
	cat /tmp/localize8 | grep -i localize | sed 's/ *//g' | sed 's/.*[Pp]rado.*::.*localize(\([^)]\+\))*.*/\1/gi' | sed "s/'//g" | sed 's/,.*//' | sed 's/"//g' > /tmp/libelle
	cat /tmp/localize8 | grep -i TTrans | sed 's/.*<com:TTranslate.*>\(.*\)<\/com:TTranslate>.*/\1/ig' >> /tmp/libelle
	if [ "$3" == "add" ]; then
		sort /tmp/libelle | uniq >> /tmp/total_$2
		cat /tmp/total_$2 > /tmp/total_$2_tmp
		sort /tmp/total_$2_tmp | uniq > /tmp/total_$2 
	else
		sort /tmp/libelle | uniq > /tmp/total_$2
	fi
#	echo "$1 ($3)"
	wc -l /tmp/total_$2	
#	echo 'End script'
}

extractLibelleFromCodeToFile "../" "protected"
