<?php

require_once __DIR__ . '/../Cli/PropelPostGeneratorTools.php';

use \Application\Cli\PropelPostGeneratorTools;

$arguments = $_SERVER['argv'];

$tableName = $arguments[1];

PropelPostGeneratorTools::copyOnlyOurModifiedClass($tableName, true);
