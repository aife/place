<?php

namespace Application\Bin;

use Application\Service\Atexo\Atexo_Util;

function openLabelFile($file)
{
    $tmp = file($file);
    $tmp2 = [];
    foreach ($tmp as $node) {
        if ('' != trim($node)) {
            $tmp2[] = trim($node);
        }
    }

    return $tmp2;
}

// Affiche le contenu de $a1 qi n'est pas dans $a2
function stripArray($a1, $a2, $a3 = [])
{
    sort($a1);
    sort($a2);

    return array_diff($a1, $a2, $a3);
}

function generateCSV($listLabels, $domv1, $domv2)
{
    foreach ($listLabels as $source) {
        if (isset($domv1[$source])) {
            echo '"'.$source.'","'.htmlspecialchars($domv1[$source]).'","';
        } else {
            echo '"'.$source.'","'.'**************** NOT DEFINED **************** '.'","';
        }
        if (isset($domv2[$source])) {
            echo htmlspecialchars($domv2[$source])."\"\n";
        } else {
            echo '**************** NOT DEFINED (en) **************** '."\"\n";
        }
    }
}

$domv1 = Atexo_Util::XliffToArray(__DIR__.'/../config/messages/messages.fr.xml');
$domv2 = Atexo_Util::XliffToArray(__DIR__.'/../config/messages/messages.en.xml');

$diff_commun_entreprise = stripArray(openLabelFile('/tmp/total_commun'), openLabelFile('/tmp/total_entreprise'));
$diff_agent_commun_entreprise = stripArray(openLabelFile('/tmp/total_agent'), openLabelFile('/tmp/total_commun'), openLabelFile('/tmp/total_entreprise'));
//echo count($diff_commun_entreprise) ."\n";
//echo count($diff_agent_commun_entreprise) . "\n";

generateCSV(openLabelFile('/tmp/total_entreprise'), $domv1, $domv2);
generateCSV($diff_commun_entreprise, $domv1, $domv2);
generateCSV($diff_agent_commun_entreprise, $domv1, $domv2);
