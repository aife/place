<?php

use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Controller\Cli;
use Symfony\Component\Dotenv\Dotenv;
use App\Kernel;

require __DIR__.'/../../../vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->usePutenv(true);
$dotenv->load(__DIR__.'/../../../.env');

$kernel = new Kernel('prod', false);
$kernel->boot();

Atexo_Util::setSfKernel($kernel);

Cli::run();
