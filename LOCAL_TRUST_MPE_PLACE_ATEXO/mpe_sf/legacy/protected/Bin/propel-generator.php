<?php

require_once __DIR__ . '/../Service/Atexo/Mapping/PropelSchema.php';

use Application\Service\Atexo\Mapping\Atexo_Mapping_PropelSchema;

$schemaFilePath = '/data/apache2/mpe-docker.local-trust.com/htdocs/mpe_sf/legacy/protected/Library/Propel/generator/MPE/schema.xml';

(new Atexo_Mapping_PropelSchema())->replaceCapsChars($schemaFilePath);
(new Atexo_Mapping_PropelSchema())->addPhpName($schemaFilePath, 'Common');
