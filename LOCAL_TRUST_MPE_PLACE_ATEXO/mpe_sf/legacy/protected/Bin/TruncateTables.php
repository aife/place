<?php

namespace Application\Bin;

use Application\Service\Atexo\Atexo_TruncateTables;

 /**
  * commentaires.
  *
  * @author adil El Kanabi <adil.alkanabi@atexo.com>
  * @copyright Atexo 2008
  *
  * @version 1.0
  *
  * @since MPE-3.0
  */
 $truncateTables = new Atexo_TruncateTables();
 $truncateTables->run();
