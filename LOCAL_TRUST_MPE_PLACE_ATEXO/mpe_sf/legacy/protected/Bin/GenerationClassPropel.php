<?php

namespace Application\Bin;

use Prado\Prado;
use Prado\Util\TLogger;

/*
 * Created on 31 mars 2008
 * @author:adil el kanabi
 * @package:bin

 */
class GenerationClassPropel
{
    /**
     * cette fonction ajoute, pour chaque balise "table", dans le fichier xml transmis en paramètre l'attribut "phpName"
     * tel que attribut.
     *
     * @author : adil el kanabi
     *
     * @param : $shemaFilePath : chemin du fichier shema.xml
     */
    public function addPhpName($shemaFilePath)
    {
        //si le fichier schema.xml transmis en paramètre existe
        if (file_exists($shemaFilePath)) {
            //le fichiers xml est chargé
            $xml = simplexml_load_file($shemaFilePath);
            $indiceTabXml = 0;
            //pour chaque balise "<table>"
            $table = '';
            foreach ($xml->table as $table) {
                //on ajoute un attribut "phpName" composé du mot "Common" concatener avec la valeur de l'attribut de name après avoir
                //supprimer les caractères "_" et transformer en majuscule le caractère suivant l'underscore
                $phpName = 'Common';
                $valueOfPhpNameAttributes = explode('_', $xml->table[$indiceTabXml]['name']);
                foreach ($valueOfPhpNameAttributes as $elementOfPhpNameAttributes) {
                    $phpName .= strtoupper(substr($elementOfPhpNameAttributes, 0, 1)).substr($elementOfPhpNameAttributes, 1);
                }
                $xml->table[$indiceTabXml]->addAttribute('phpName', $phpName);
                ++$indiceTabXml;
            }
            $contentXml = $xml->asXML();
            //crée enchère, permettre l'utilisation d'une valeur dans le champ auto incrément lors de l'insertion
            $contentXml = str_replace('<table name="EnchereReference" ', '<table name="EnchereReference" allowPkInsert="true" ', $contentXml);
            //on ecrase le contenu de l'ancien schema.xm par le nouveau
            if (!$handle = fopen($shemaFilePath, 'w')) {
                echo "Impossible d'ouvrir le fichier (".$shemaFilePath.')';

                return;
            }
            if (false === fwrite($handle, $contentXml)) {
                echo "Impossible d'écrire dans le fichier  (".$shemaFilePath.')';

                return;
            }
        } else {
            Prado::log("Echec lors de l\'ouverture du fichier".$shemaFilePath, TLogger::INFO, 'Info');
        }
    }
}
