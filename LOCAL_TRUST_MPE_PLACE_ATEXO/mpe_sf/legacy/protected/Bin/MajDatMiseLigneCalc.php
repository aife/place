<?php

namespace Application\Bin;

use Application\Service\Atexo\Atexo_UpdateDateMiseEnLigneCalculeOrganisme;

 /**
  * commentaires.
  *
  * @author adil El Kanabi <adil.alkanabi@atexo.com>
  * @copyright Atexo 2008
  *
  * @version 1.0
  *
  * @since MPE-3.0
  */
 include __DIR__.'/../library/Atexo/UpdateDateMiseEnLigneCalculeOrganisme.php';
 $truncateTables = new Atexo_UpdateDateMiseEnLigneCalculeOrganisme();
 $truncateTables->run();
