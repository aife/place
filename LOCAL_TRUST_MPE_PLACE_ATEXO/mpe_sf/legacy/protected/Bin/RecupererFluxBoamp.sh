#!/bin/bash
# Définition des variables
FILE_OWNER="apache"
CURL_PATH="$(which curl)"
CURL_OPTIONS="--silent --connect-timeout 30 --max-time 60"
YEAR="$(date +%Y)"
FTP_URL="ftp://echanges.dila.gouv.fr/BOAMP/${YEAR}/"
BASE_ROOT_DIR="$1"
WORK_DIR="${BASE_ROOT_DIR}/logs/annonces_boamp"
WORK_DIR_TODO="${WORK_DIR}/fichiers_a_traiter"
WORK_DIR_DONE="${WORK_DIR}/fichiers_traites"
VERBOSE="1"
DEBUG="0"

# On vérifie que les dossiers existent bien
if [[ -d ${WORK_DIR} && -d ${WORK_DIR_TODO} && -d ${WORK_DIR_DONE} ]]; then
    FTP_LIST=($(${CURL_PATH} ${CURL_OPTIONS} --list-only ${FTP_URL}))
    [[ "${DEBUG}" -ge "2" ]] && echo "FTP_LIST = ${FTP_LIST[@]}"
    for FTP_FILE_NAME in ${FTP_LIST[@]}; do
        if [[ ! -s "${WORK_DIR_DONE}/${FTP_FILE_NAME}" ]]; then
            [[ "${VERBOSE}" -ge "1" ]] && echo "On récupère le fichier ${FTP_URL}/${FTP_FILE_NAME} dans ${WORK_DIR_TODO}/"
            [[ "${DEBUG}" -ge "1" ]] && echo "su ${FILE_OWNER} --login --shell /bin/bash --command \"${CURL_PATH} --silent ${FTP_URL}/${FTP_FILE_NAME} --output ${WORK_DIR_TODO}/${FTP_FILE_NAME}\""
            su ${FILE_OWNER} --login --shell /bin/bash --command "${CURL_PATH} ${CURL_OPTIONS} ${FTP_URL}/${FTP_FILE_NAME} --output ${WORK_DIR_TODO}/${FTP_FILE_NAME}"
        else
            [[ "${DEBUG}" -ge "1" ]] && echo "Le fichier ${FTP_URL}/${FTP_FILE_NAME} est déjà présent dans ${WORK_DIR_DONE}/"
        fi
    done
else
    echo "Erreur - Le(s) dossier(s) ${WORK_DIR}, ${WORK_DIR_TODO} et/ou ${WORK_DIR_DONE} n'existe(nt) pas."
fi
