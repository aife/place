<?php

namespace Application\Bin;

$csv = 'messages.csv';

echo '<?xml version="1.0" encoding="utf-8"?>
  <xliff version="1.0">
    <file datatype="plaintext" date="'.date('c').'" original="I18N Example IndexPage" source-language="FR" target-language="FR-FR">
      <body>'."\n";
$handle = fopen($csv, 'r');
while (($data = fgetcsv($handle, 8000, ',')) !== false) {
    ++$row;
    if ('label' == $argv[1]) {
        $data[1] = $data[0];
    }
    echo "<trans-unit id=\"$row\"><source>$data[0]</source><target>$data[1]</target></trans-unit>\n";
}
fclose($handle);

echo "</body></file></xliff>\n";
