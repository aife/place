    <com:TRepeater id="repeaterDirigeants"
        AllowPaging="true"
        PageSize="10"
        EnableViewState="true"
        DataKeyField="Id"
        >
            <prop:HeaderTemplate>
                <div class="panel-body">
                    <h4><com:TTranslate>MANDATAIRES_LEGAUX</com:TTranslate></h4>
                    <table class="table bordered-table zebra-striped table-highlight table-contacts">
                <caption class="sr-only"><com:TTranslate>LISTE_MANDATAIRES_LEGAUX</com:TTranslate></caption>
                <thead>
                <tr>
                    <th class="col-md-6" scope="col"><com:TTranslate>DEFINE_PRENOM</com:TTranslate> <com:TTranslate>DEFINE_NOM</com:TTranslate></th>
                    <th class="col-contact" scope="col"><com:TTranslate>EMAIL</com:TTranslate></th>
                    <th scope="col"><com:TTranslate>DEFINE_TEL</com:TTranslate></th>
                </tr>
                </thead>
                <tbody>
        </prop:HeaderTemplate>
        <prop:ItemTemplate>
                <tr>
                    <td class="col-user">
                        <div>
                            <i title=""  class="glyphicon glyphicon-user" data-original-title="Contact"><span class="sr-only"><com:TTranslate>CONTACT</com:TTranslate></span></i>
                            <%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom())%>&nbsp;<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom())%>
                            <com:TConditional condition="<%#(($this->Data instanceof Application\Propel\Mpe\CommonResponsableengagement)&& $this->Data->getQualite())%>">
                                <prop:trueTemplate>
                                    <i class="text-muted"><%#$this->Data->getQualite()%></i>
                                </prop:trueTemplate>
                            </com:TConditional>

                        </div>
                    </td>
                    <td class="col-contact col-break">
                        <div>
                            <i title="" class="fa fa-envelope" data-original-title="Email"><span class="sr-only"><com:TTranslate>EMAIL</com:TTranslate></span></i>
                            <%#($this->Data->getEmail())?$this->Data->getEmail():'-'%>
                        </div>
                    </td>
                    <td class="col-tel col-break">
                        <div>
                            <i title="" class="glyphicon glyphicon-phone" data-original-title="Téléphone"><span class="sr-only"><com:TTranslate>DEFINE_TEL</com:TTranslate></span></i>
                            <%#($this->Data->getTelephone())?$this->Data->getTelephone():'-'%>
                        </div>
                    </td>
                </tr>
        </prop:ItemTemplate>
        <prop:FooterTemplate>
                </tbody>
            </table>
            </div>
        </prop:FooterTemplate>

    </com:TRepeater>
