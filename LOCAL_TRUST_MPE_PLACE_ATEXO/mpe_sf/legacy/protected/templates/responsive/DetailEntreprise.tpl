<!--Debut Main-content-->
	<h2 class="text-primary"><com:TTranslate>FICHE_FOURNISSEUR</com:TTranslate></h2>
	<!--Debut Panel Recap-->
	<div class="panel panel-default bloc-toggle bloc-recap recap-fournisseur">
		<div class="panel-heading" id="headingRecap">
			<h3 class="panel-title"><a data-toggle="collapse" href="#collapseRecap" aria-expanded="true" aria-controls="collapseRecap"><i class="fa"></i><com:TLabel id="idRaisonSociale" /> <com:TLabel id="paysSiren" /></a></h3>
		</div>
		<div id="collapseRecap" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingRecap">
			<com:IdentificationEntreprise id="idIdentificationEntreprise"  responsive="1"/>
			<com:DirigeantsEntreprise id="idDirigeantsEntreprise" visible="<%=($this->getCalledFrom() == 'agent')? true: false%>" responsive="1" />
			<com:AtexoEtablissements ID="listeEtablissments" responsive="true" pagination="false"/>
		</div>
	</div>
	<!--// Fin Panel Recap -->
    <!-- Debut Panel Contacts demande de devis-->
    <com:AtexoContactDevis responsive="true" visible="<%=($this->getCalledFrom() == 'agent')? true: false%>"/>
    <!-- // Fin Panel Contacts demande de devis-->
	<!-- Debut Panel Documents-->
	<div class="panel panel-default bloc-toggle">
		<div class="panel-heading" role="tab" id="headingDocuments">
			<h3 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocuments" aria-expanded="true" aria-controls="collapseDocuments"><i class="fa"></i><com:TTranslate>TEXT_DOCUMENTS</com:TTranslate></a> </h3>
		</div>
		<div id="collapseDocuments" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingDocuments">
			<com:AtexoDisponibiliteAttestation
					id="dispoAttestationsReponse"
					responsive="true"
					idEntreprise="<%=$_GET['idEntreprise']%>"
					Visible="<%=$this->Page->gestionAffichageBlocAttestations()%>"
					organisme="<%=$_GET['organisme']%>"
					refCons="<%=$_GET['id']%>"
					callFromOA="<%=$_GET['callFromOA']%>"
					idOffre="<%=$_GET['idO']%>"
					typeEnv="<%=$_GET['type']%>"
			/>

			<com:AtexoTableauDocCfe ID="tableauDocumentsDuCoffreFort" responsive="true" />
		</div>
	</div>
	<!-- // Fin Panel Documents-->

<com:TActiveButton ID="buttonRefresh" Display="None" oncallBack="refreshAttestations"/>
<!--Fin Main-content-->
<com:TPanel visible="false">
	<com:TLabel id="panelActiviteDomaineDefense" />
	<com:TLabel id="panelDocumentsComerciaux" />
	<com:TLabel id="panelQualification" />
	<com:TLabel id="panelAgrements" />
	<com:TLabel id="panelMoyensTechnique" />
	<com:TLabel id="panelMoyensHumains" />
	<com:TLabel id="panelPrestation" />
	<com:TLabel id="labelSiteInternet" />
	<com:TLabel id="labelDescriptionActivite" />
	<com:TLabel id="labelActiviteDefense" />
	<com:TLabel id="labelQualifications" />
	<com:TLabel id="labelAgrements" />
	<com:TLabel id="labelleDocCommercial" />
	<com:TLabel id="labelMoyensHumains" />
	<com:TLabel id="labelMoyensTechniques" />
	<com:TLabel id="panelEtablissements" />
	<com:TLabel id="panelLinkDownloadPj" />
	<com:TLabel id="sizeDocCommercial" />
	<com:TLabel id="aucunDocCoffreFort" />

	<com:ActiviteEntreprise id="idActiviteEntreprise"  visible="<%=($this->getCalledFrom() == 'agent')? true : false %>" />
	<com:InfoDonneesComplementaires id="idDonneesComplementaires" Visible="<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && ($this->getCalledFrom() == 'agent')))?true:false %>" />
	<com:PrestationEntreprise ID="prestationEntreprise"/>
	<com:AtexoLtReferentiel id="ltrefEntr" mode="0" entreprise="1" cssClass="intitule-250 bold"/>
</com:TPanel>
