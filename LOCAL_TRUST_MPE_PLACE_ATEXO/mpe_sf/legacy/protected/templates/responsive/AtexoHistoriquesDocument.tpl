<!--Debut Modal Message nouvelle version non disponible-->
<div class="modal-historique" style="display:none;">

	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="document.getElementById('<%=$this->Page->buttonRefresh->getClientId()%>').click();J('.modal-historique').dialog('close');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="titreDialogue_historiqueAttestations"><com:TActiveLabel ID="titrePopin" /></h4>
			</div>
			<div class="modal-body">
				<com:TActivePanel cssClass="alert alert-info" Attributes.role="alert" id="panelInfoVersion" >
					<p><i class="fa fa-info-circle"></i>&nbsp;<com:TActiveLabel ID="msgInfo"/></p>
				</com:TActivePanel>
				<com:TActiveRepeater ID="repeaterHistoriques" >
					<prop:HeaderTemplate>
						<table class="table bordered-table zebra-striped table-historique-attestations table-hover">
						<caption class="sr-only"><com:TTranslate>ICONE_HISTORIQUE</com:TTranslate></caption>
						<thead>
						<tr>
							<th scope="col" class="col-md-11" id="historique_attestionFiscale_date">
								<com:TActiveLinkButton
										CssClass="on"
										OnCallback="SourceTemplateControl.sortDataSource"
										Text="<%=Prado::localize('DATE')%>"
										ActiveControl.CallbackParameter="DateRecuperation"
										/>
								<com:TActiveLinkButton
										CssClass="fa fa-sort"
										OnCallback="SourceTemplateControl.sortDataSource"
										ActiveControl.CallbackParameter="DateRecuperation"
										/>
							</th>
							<th scope="col" class="col-md-1 col-actions" id="historique_attestionFiscale_actions"><com:TTranslate>DEFINE_TELECHARGER</com:TTranslate></th>
						</tr>
						</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<tr>
							<td>
								<com:TLabel ID="dateRecuperation" Text="<%#$this->Data->getDateRecuperation('d/m/Y')%>" />
							</td>
							<td class="col-actions">


								<com:TActiveLinkButton
										id="pictoDownload"
										CssClass="btn btn-primary btn-xs btn-action"
										onCommand="SourceTemplateControl.downloadVersion"
										CommandParameter="<%#$this->Data->getIdVersionDocument() .'#'. $this->Data->getIdDocument()%>"
								>
									<span class="fa fa-download"></span>
								</com:TActiveLinkButton>
							</td>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
						</table>
					</prop:FooterTemplate>
				</com:TActiveRepeater>
				<div class="text-right">
					<com:TActiveLinkButton
							CssClass="btn btn-primary btn-xs"
							ID="linkDemanderNouvelleVersion"
							Attributes.title="<%=Prado::localize('DEMANDER_UNE_NOUVELLE_VERSION')%>"
							onClick="addNewVersion"
							>
						<span class="fa fa-download"></span>
						<com:TTranslate>DEMANDER_UNE_NOUVELLE_VERSION</com:TTranslate>
						<prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
						<prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
						<prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>
					</com:TActiveLinkButton>
				</div>

			</div>
			<div class="modal-footer">
				<input
						type="button"
						class="btn btn-sm btn-default pull-left"
						value="<%=Prado::localize('FERMER')%>"
						title="<%=Prado::localize('FERMER')%>"
						onclick="document.getElementById('<%=$this->Page->buttonRefresh->getClientId()%>').click();J('.modal-historique').dialog('close');"
						/>

			</div>
		</div>
	</div>
</div>
<!-- // Fin Modal historique Attestations -->