<com:TActivePanel cssClass="panel-body" id="bloc_documents_entreprise">
	<!-- Debut Tableau justificatifs d’immatriculation -->
	<table class="table bordered-table zebra-striped table-documents table-hover">
		<caption class="sr-only">
			<com:TTranslate>IMMATRICULATION_JUSTIFICATION</com:TTranslate>
		</caption>
		<thead>
			<tr>
				<th scope="col"><com:TTranslate>IMMATRICULATION_JUSTIFICATION</com:TTranslate></th>
				<th scope="col" class="col-md-2 col-actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<com:TActiveLabel Text="<%=Prado::localize('RCNS_INPI_ATTESTATION_TITLE')%>" />
					<com:TActiveLabel cssclass="text-muted" Text="<%=Prado::localize('RCNS_INPI_ATTESTATION_SUBTITLE')%>" />
				</td>
				<td>
					<a href='<%=$this->getNationalRegisterTradeCompanyPdf()%>' class='btn btn-primary btn-sm btn-action' target='_blank'><i class='fa fa-download'></i></a>
				</td>
			</tr>
			<tr>
				<td>
					<com:TActiveLabel Text="<%=Prado::localize('CMA_RNM_ATTESTATION_TITLE')%>" />
					<com:TActiveLabel cssclass="text-muted" Text="<%=Prado::localize('CMA_RNM_ATTESTATION_SUBTITLE')%>" />
				</td>
				<td>
					<a href='<%=$this->getAvisSituationPdf()%>' class='btn btn-primary btn-sm btn-action' target='_blank'><i class='fa fa-download'></i></a>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- //Fin Tableau justificatifs d’immatriculation -->

	<!-- Debut Tableau Attestations SGMAP - Siège social -->
	<com:TActiveRepeater ID="listeTypesDocsEntreprise" DataSource="<%=$this->remplirListeTypesDocumentsEntreprise()%>">
		<prop:HeaderTemplate>
			<table class="table bordered-table zebra-striped table-documents table-hover">
				<caption class="sr-only">
					<com:TTranslate>DEFINE_LISTE_DES_ATTESTATIONS_SGMAP_SIEGE_SOCIAL</com:TTranslate>
				</caption>
				<thead>
				<tr>
					<th scope="col"><com:TTranslate>DEFINE_LISTE_DES_ATTESTATIONS_SGMAP_SIEGE_SOCIAL</com:TTranslate></th>
					<th scope="col" class="col-md-2 col-dates"><com:TTranslate>MIS_A_JOUR_LE</com:TTranslate></th>
					<th scope="col" class="col-md-2 col-actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
				</tr>
				</thead>
				<tbody>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
				<tr>
					<td>
						<com:TActiveLabel ID="titreAttestation" Text="<%#$this->SourceTemplateControl->getTitreAttestation($this->Data)%> -" />

						<com:TActiveLinkButton
								visible = "<%#$this->SourceTemplateControl->hasVersion($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise()) && $this->Data->getSynchroActif()%>"
								id="linkDownload"
								attributes.title="<%# Prado::localize('DOWNLOAD_DOC') . ' ' . $this->SourceTemplateControl->getExtentionAttestation($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise()) . ' ' . $this->SourceTemplateControl->getTailleAttestation($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise())%>"
								onCommand="SourceTemplateControl.downloadVersion"
								CommandParameter="<%#$this->SourceTemplateControl->getIdEntreprise() .'#'. $this->Data->getIdTypeDocument().'#' .Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')%>"
						>
							<i class="fa fa-download"></i> <com:TTranslate>DEFINE_TELECHARGER</com:TTranslate> <small><com:TActiveLabel id="labelTailleDoc" Text="<%=$this->SourceTemplateControl->getTailleAttestation($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise())%>" /></small>
						</com:TActiveLinkButton>
						<com:TActiveLabel cssclass="text-muted"visible = "<%# !$this->SourceTemplateControl->hasVersion($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise()) || !$this->Data->getSynchroActif()%>"  text="<%=$this->Data->getMessageDisponibilite()%>" />
					</td>
					<td><com:TActiveLabel id="dateMajDoc" Text="<%#$this->SourceTemplateControl->getDateMAJAttestation($this->Data->getIdTypeDocument(), $this->SourceTemplateControl->getIdEntreprise())%>" /></td>
					<td class="col-actions">
						<com:TActiveLinkButton
								cssClass="btn btn-primary btn-sm btn-action"
								Attributes.title="<%=Prado::localize('ICONE_HISTORIQUE')%>"
								onCommand="SourceTemplateControl.afficherHistoriqueAttestation"
								CommandParameter="<%#$this->Data->getIdTypeDocument()%>-<%#$this->SourceTemplateControl->getSirenEntreprise()%>"
						>
							<i class="fa fa-clock-o"></i>
						</com:TActiveLinkButton>
					</td>
				</tr>
		</prop:ItemTemplate>
		<prop:FooterTemplate>
				</tbody>
			</table>
			<script>initialiserAffichageTableauDocumentsEntreprise();</script>
		</prop:FooterTemplate>
	</com:TActiveRepeater>
	<!-- //Fin Tableau Attestations SGMAP - Siège social -->

	<!-- Debut Tableau Attestations SGMAP - Etablissements -->
	<com:TActiveRepeater ID="listeEtablissements" DataSource="<%=$this->remplirListeEtablissements()%>">
		<prop:HeaderTemplate>
			<table class="table bordered-table table-striped table-hover treeview table-attestations">
				<caption class="sr-only"><com:TTranslate>DEFINE_ATTESTATIONS_SGMAP_ETABLISSEMENT</com:TTranslate></caption>
				<thead>
				<tr class="row-attributaire">
					<th scope="col"><com:TTranslate>DEFINE_ATTESTATIONS_SGMAP_ETABLISSEMENT</com:TTranslate> </th>
					<th scope="col" class="col-md-2 col-dates"><com:TTranslate>MIS_A_JOUR_LE</com:TTranslate></th>
					<th scope="col" class="col-md-2 col-actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
				</tr>
				</thead>
				<tbody>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
			<!-- Etablissement -->
			<tr class="indent-n1 treegrid-<%#$this->ItemIndex+1%> docs-etablissement" id="docs-etablissement-<%#$this->Data->getIdEtablissement()%>">
				<td colspan="3"><%#$this->Data->getCodeEtablissement()%> <%# ($this->Data->getAdresse() || $this->Data->getAdresse2() || $this->Data->getCodePostal() || $this->Data->getVille())?'-' : ''%> <%#$this->Data->getAdresse()%> <%#$this->Data->getAdresse2()%> <%#$this->Data->getCodePostal()%> <%#$this->Data->getVille()%>
					<i <%#$this->Data->getEstSiege()? '' : 'style="display:none"'%> class="fa fa-building siege-social" data-toggle="tooltip" data-original-title="<%=Prado::localize('TEXT_SIEGE_SOCIAL')%>"><span class="sr-only"><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate></span></i>
				</td>
			</tr>

			<!-- Debut liste documents etablissement -->
			<com:TRepeater ID="listeTypesDocsEtablissement" EnableViewState="true" DataSource="<%#$this->SourceTemplateControl->remplirListeTypesDocumentsEtablissement()%>">
				<prop:ItemTemplate>
					<tr class="child treegrid-parent-<%#$this->TemplateControl->ItemIndex+1%> etablissement-type-attestation etablissement-type-attestation-id-<%#$this->TemplateControl->Data->getIdEtablissement()%>" id="type-doc-etablissement-id-<%#$this->TemplateControl->Data->getIdEtablissement()%>">
						<td>
							<com:TActiveLabel ID="titreAttestation" Text="<%#$this->SourceTemplateControl->getTitreAttestation($this->Data)%> -" />
							<com:TActiveLinkButton
									visible = "<%#$this->SourceTemplateControl->hasVersion($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'))%>"
									id="linkDownload"
									attributes.title="<%# Prado::localize('DOWNLOAD_DOC') . ' ' . $this->SourceTemplateControl->getExtentionAttestation($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) . ' ' . $this->SourceTemplateControl->getTailleAttestation($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'))%>"
									onCommand="SourceTemplateControl.downloadVersion"
									CommandParameter="<%#$this->TemplateControl->Data->getIdEtablissement() .'#'. $this->Data->getIdTypeDocument().'#' .Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')%>"
							>
								<i class="fa fa-download"></i> <com:TTranslate>DEFINE_TELECHARGER</com:TTranslate>
								<small>
									<com:TActiveLabel id="labelTailleDocEtab" Text="<%=$this->SourceTemplateControl->getTailleAttestation($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'))%>" />
								</small>
							</com:TActiveLinkButton>
							<com:TActiveLabel cssclass="text-muted"visible = "<%# !$this->SourceTemplateControl->hasVersion($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'))%>"  text="<%=Prado::localize('TEXT_NON_DISPONIBLE')%>" />
						</td>
						<td><com:TActiveLabel Text="<%#$this->SourceTemplateControl->getDateMAJAttestation($this->Data->getIdTypeDocument(), $this->TemplateControl->Data->getIdEtablissement(), Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'))%>" /></td>
						<td class="col-actions">
							<com:TActiveLinkButton
									cssClass="btn btn-primary btn-sm btn-action"
									Attributes.title="<%=Prado::localize('ICONE_HISTORIQUE')%>"
									onCommand="SourceTemplateControl.afficherHistoriqueAttestation"
									CommandParameter="<%#$this->Data->getIdTypeDocument() . '-' . $this->SourceTemplateControl->getSirenEntreprise() . $this->Parent->Parent->Data->getCodeEtablissement() . '-' . $this->TemplateControl->Data->getIdEtablissement()%>"
							>
								<i class="fa fa-clock-o"></i>
							</com:TActiveLinkButton>
						</td>
					</tr>
				</prop:ItemTemplate>
			</com:TRepeater>
			<!-- Fin liste documents etablissement -->

			<!-- // Fin Etablissement -->
		</prop:ItemTemplate>
		<prop:FooterTemplate>
			</tbody>
			</table>
		</prop:FooterTemplate>
	</com:TActiveRepeater>
	<!-- //Fin Tableau Attestations SGMAP - Etablissements -->

	<!-- Debut Modal historique Attestations -->
	<com:THiddenField ID="titrePopin" Attributes.title="<%=Prado::localize('ICONE_HISTORIQUE')%>"  value="<%=Prado::localize('ICONE_HISTORIQUE')%>" />
	<com:TActiveLabel id="javascriptLabel" />
	<com:TButton id="buttonDownload" onClick="downloadFichier" style="display:none;"/>
	<com:AtexoHistoriquesDocument ID="atexoHistoriquesDocument" responsive="true"/>
	<!-- // Fin Modal historique Attestations -->
	<input ID="initialisationAttestationTreeGrid" value="<%=$this->recupererCasInitialisationAttestationTreeGrid()%>" style="display: none"/>
	<input ID="idEtablissementAyantDeposeReponse" value="<%=$this->recupererIdEtablissementDepotReponse()%>" style="display: none"/>
</com:TActivePanel>

