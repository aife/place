<!--Debut Panel Recap-->
        <com:TActivePanel cssClass="panel-body" ID="panelInfoEntreprise">
            <div class="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate> </strong>:</div>
                <div class="col-md-8"><i class="fa fa-building"></i> <com:TLabel id="siegeSocial" /></div>
            </div>
            <com:TPanel id="panelCodeApe"  cssclass="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>TEXT_NAF</com:TTranslate> </strong> :</div>
                <div class="col-md-8"><div class="read-more box desc-naf"><com:TLabel id="codeApeNafNace" /></div></div>
            </com:TPanel>
            <div class="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>TEXT_FJ</com:TTranslate> </strong> :</div>
                <div class="col-sm-8"><com:TLabel id="formeJuridique" /></div>
            </div>
            <div class="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate> </strong> :</div>
                <div class="col-sm-8"><com:TLabel id="categorieEntreprise" /></div>
            </div>
            <com:TPanel id="panelDescription" cssclass="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>DEFINE_DESCRIPTION</com:TTranslate> :</strong></div>
                <div class="col-md-8">
                    <div class="read-more box desc-activite"><com:TLabel id="description" /></div>
                </div>
            </com:TPanel>
            <com:TPanel id="panelSiteInternet" cssclass="row container-fluid" visible="false">
                <div class="col-xs-3 col-md-3"><strong><com:TTranslate>TEXT_DEFINE_SITE_INTERNET</com:TTranslate> :</strong></div>
                <div class="col-md-8"><com:THyperLink id="siteInternet" attributes.target="_blank"> <com:TLabel id="siteInternetLabel" /> <i class="fa fa-external-link"></i></com:THyperLink> </div>
            </com:TPanel>
        </com:TActivePanel>
        <com:AtexoSoumissionnaire id="atexoSoumissionnaire" visible="false" affichage="1"/>

<com:TPanel visible="false">
    <com:TLabel id="idRaisonSociale" />
    <com:TLabel id="region" />
    <com:TLabel id="province" />
    <com:TLabel id="telephone" />
    <com:TLabel id="telephone2" />
    <com:TLabel id="telephone3" />
    <com:TLabel id="panelTelEntreprise2" />
    <com:TLabel id="panelTelEntreprise3" />
    <com:TLabel id="fax" />
    <com:TLabel id="siretSiege" />
    <com:TLabel id="idNational" />
    <com:TLabel id="idtaxeprof" />
    <com:TLabel id="numRc" />
    <com:TLabel id="villeRc" />
    <com:TLabel id="idcnss" />
    <com:TLabel id="idcapital" />
    <com:TLabel id="ifu" />
    <com:TLabel id="panelCnss" />
    <com:TLabel id="panelNumRc" />
    <com:TLabel id="panelIdTaxProf" />
    <com:TLabel id="panelRegion" />
    <com:TLabel id="panelProvince" />
    <com:TLabel id="panelSiren" />
    <com:TLabel id="panelIdNational" />
    <com:TLabel id="panelDomaineActivite" />
    <com:TLabel id="panelCapital" />
    <com:TLabel id="panelIfu" />
    <com:THiddenField id="idsDomaines" />
    <com:TLabel id="adresse" /> <com:TLabel id="adresseSuite" /> - <com:TLabel id="codePostal" /> <com:TLabel id="ville" /> - <com:TLabel id="pays" />
    <com:TLabel id="paysSiren" />
    <com:AtexoReferentiel ID="atexoReferentielCodeNace" cas="cas9"/>
</com:TPanel>
<!--// Fin Panel Recap -->