    <!-- Debut Tableau Coffre-fort électronique -->
    <com:TRepeater ID="piecesCoffreFort" EnableViewState="true">
            <prop:HeaderTemplate>
                <div class="panel-body">
            <table class="table bordered-table zebra-striped table-documents table-hover">
                <caption class="sr-only"><com:TTranslate>LISTE_DOCUMENTS_COFFRE_FORT_ELECTRONIQUE</com:TTranslate></caption>
                <thead>
                <tr>
                    <th scope="col"><com:TTranslate>COFFRE_FORT_ELECTRONIQUE_ENTREPRISE</com:TTranslate></th>
                    <th scope="col" class="col-md-2 col-dates"><com:TTranslate>FIN_DE_VALIDITE</com:TTranslate></th>
                </tr>
                </thead>
                <tbody>
             </prop:HeaderTemplate>
             <prop:ItemTemplate>
                    <tr>
                        <td><%#$this->Data->getNom()%> -
                            <com:TLinkButton onCommand="SourceTemplateControl.downloadPiece" CommandParameter="<%#$this->Data->getIntituleJustificatif()%>" CommandName="<%#$this->Data->getJustificatif()%>">

                            </com:TLinkButton>

                            <com:THyperLink
                                    ID="linkDownload"
                                    NavigateUrl="?page=Agent.FileDownLoad&idFichier=<%# $this->Data->getJustificatif().'&org=' .Application\Service\Atexo\Atexo_Config::getParameter('COMMON_BLOB').'&nomFichier='.$this->Data->getIntituleJustificatif()%>"
                                    attributes.title="<%# Prado::localize('DOWNLOAD_DOC') . ' ' . (Application\Service\Atexo\Atexo_Util::getExtension($this->Data->getIntituleJustificatif())) . ' ' . ($this->Data->getTaille()/1000)%>">
                                <i class="fa fa-download"></i> <%#$this->Data->getIntituleJustificatif()%>
                                <small>(<%#Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1000)%>)</small>
                            </com:THyperLink>
                        </td>
                        <td><%#Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data->getDateFinValidite())%></td>
                    </tr>
             </prop:ItemTemplate>
            <prop:FooterTemplate>
                </tbody>
                </table>
            </div>
            </prop:FooterTemplate>
    </com:TRepeater>
    <com:TLabel ID="aucunePiece" visible="false"/>

<!-- // Fin Tableau Coffre-fort électronique -->
