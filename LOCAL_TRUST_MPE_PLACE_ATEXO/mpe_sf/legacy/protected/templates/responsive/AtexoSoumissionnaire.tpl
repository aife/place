<div class="panel-body">
    <div class="row container-fluid">
        <div class="col-xs-3 col-md-3"><strong>
            <com:TTranslate>SOUMISSIONNAIRE</com:TTranslate>
        </strong>:
        </div>
        <div class="col-md-8">
            <div><i class="glyphicon glyphicon-user"  title="Contact"><span class="sr-only"><%=Prado::localize('CONTACT')%></span></i>
                <com:TLabel id="contact"/>
            </div>
            <div>
                <com:TLabel id="adresse"/> <com:TLabel id="estSiege"><i class="fa fa-building" data-original-title="<%=Prado::localize('TEXT_SIEGE_SOCIAL')%>"><span class="sr-only"><%=Prado::localize('TEXT_SIEGE_SOCIAL')%></span></i></com:TLabel>&nbsp;&nbsp;[ <com:TTranslate>TEXT_SIRET</com:TTranslate> : <com:TLabel id="siret"/> ]
            </div>
            <div>
                <com:TLabel id="imgEmail"><i class="fa fa-envelope" title="<%=Prado::localize('EMAIL')%>"><span class="sr-only"><%=Prado::localize('EMAIL')%></span></i>
                    <com:TLabel id="email"/>
                </com:TLabel> &nbsp;<com:TLabel id="imgTelephone" ><i class="glyphicon glyphicon-phone" title="<%=Prado::localize('TEXT_TELEPHONE')%>"><span
                    class="sr-only"><%=Prado::localize('TEXT_TELEPHONE')%></span></i> <com:TLabel id="telephone"/></com:TLabel>
                <com:TActiveImage id="imgFax" attributes.alt="<%=Prado::localize('TEXT_FAX')%>" cssclass="margin-left-15"
                                  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" />
                <com:TActiveLabel id="faxe"/>
            </div>
        </div>
    </div>
</div>