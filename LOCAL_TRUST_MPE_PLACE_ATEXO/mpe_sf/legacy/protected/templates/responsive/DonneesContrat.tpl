<div class="popup-moyen" id="container">
    <h1><com:TTranslate>TEXT_DONNEES_CONTRAT</com:TTranslate></h1>
    <div class="spacer-small"></div>
    <com:ActivePanelMessageAvertissement ID="warningSiret"
                                         Message="<%=Prado::localize('MSG_CONTROLE_SIRET_ACHETEUR_CONTRAT')%>" />

    <!--Debut message de confirmation-->
    <com:TActivePanel id="panelBlocMessages">
        <com:PanelMessageErreur  id="panelMessageErreur"  />
        <com:PanelMessageConfirmation ID="messageConfirmationEnregistrement" />
    </com:TActivePanel>
    <!--Fin message de confirmation-->
    <com:TActivePanel ID="panelPopupDecisionContrat">
        <!--Debut Bloc Recap Lot-->
        <com:AtexoBlocRecapConsultation ID="recapConsultation" />
        <!--Fin Bloc Recap Lot-->
        <com:AtexoValidationSummary  ValidationGroup="Validation" ID="divValidationSummary" />
        <com:AtexoValidationSummary  ValidationGroup="validerNotification" ID="divValidationSummaryEnregistrement" />
        <com:IdentificationEntreprise id="idIdentificationEntreprise"  responsive="2"/>
        <!--Debut Bloc Formulaire Infos AC,Infos SAD -->

        <!--Fin Bloc Formulaire Infos AC,Infos SAD -->


        <!--Debut Bloc formulaire-->
        <!-- Attributaire-->
        <com:AttributaireComposant id="attributaire"/>
        <!-- Attributaire-->
        <!--Debut Bloc Dates définitives-->
        <com:TActivePanel id="datesDefinitives" cssclass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <div class="panel">
                    <h3>Dates définitives</h3>
                    <!--Debut Ligne Date définitive de notification-->
                    <div class="line">
                        <div class="intitule-250"><label for="dateDefinitiveNotification"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_NOTIFICATION</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="calendar">
                            <com:TTextBox ID="dateDefinitiveNotification"  Attributes.title="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_NOTIFICATION')%>" CssClass="heure"  />
                            <com:TImage id="calendarDateNotification" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_dateDefinitiveNotification'),'dd/mm/yyyy','');"  />
                            <com:TRequiredFieldValidator
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveNotification"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_NOTIFICATION')%>"
                                    Text="<span title='<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_NOTIFICATION')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                            <com:TDateValidator
                                    Display="Dynamic"
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveNotification"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_NOTIFICATION')%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TDateValidator>
                            <com:TCustomValidator
                                    ControlToValidate="dateDefinitiveNotification"
                                    ValidationGroup="validerNotification"
                                    Display="Dynamic"
                                    ClientValidationFunction="ValidateDateReelleNotificationFinMarche"
                                    ErrorMessage="<%=Prado::localize('MESSAGE_ERREUR_CONTROLE_DATE_REELLE_NOTIFICATION_FIN_MARCHE_DECLARATION_CONTRAT')%>"
                                    EnableClientScript="true"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleDateReelle')" onmouseover="afficheBulle('bulleDateReelle', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    </div>
                    <!--Fin Ligne Date définitive de notification-->
                    <!--debut Ligne Date définitive de fin de marché-->
                    <div class="line">
                        <div class="intitule-250"><label for="dateDefinitiveFinMarche"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_FIN_MARCHE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="calendar">
                            <com:TTextBox ID="dateDefinitiveFinMarche"  Attributes.title="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MARCHE')%>" CssClass="heure"  />
                            <com:TImage id="calendarDateFin" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_dateDefinitiveFinMarche'),'dd/mm/yyyy','');"  />
                            <com:TRequiredFieldValidator
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveFinMarche"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MARCHE')%>"
                                    Text="<span title='<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MARCHE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                            <com:TDateValidator
                                    Display="Dynamic"
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveFinMarche"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MARCHE')%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TDateValidator>
                            <com:TCustomValidator
                                    ControlToValidate="dateDefinitiveFinMarche"
                                    ValidationGroup="validerNotification"
                                    Display="Dynamic"
                                    ClientValidationFunction="ValidateDateReelleFinMarcheFinMarcheMax"
                                    ErrorMessage="<%=Prado::localize('MESSAGE_ERREUR_CONTROLE_DATE_REELLE_FIN_MARCHE_MAXIMALE_DECLARATION_CONTRAT')%>"
                                    EnableClientScript="true"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleDateReelle')" onmouseover="afficheBulle('bulleDateReelle', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    </div>
                    <!--fin Ligne Date définitive de fin de marché-->
                    <!--Fin Ligne Date définitive de fin maximale de marché-->
                    <div class="line">
                        <div class="intitule-250"><label for="dateDefinitiveFinMaxMarche"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="calendar">
                            <com:TTextBox ID="dateDefinitiveFinMaxMarche"  Attributes.title="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE')%>" CssClass="heure"  />
                            <com:TImage id="calendarDateFinMax" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_dateDefinitiveFinMaxMarche'),'dd/mm/yyyy','');"  />
                            <com:TRequiredFieldValidator
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveFinMaxMarche"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE')%>"
                                    Text="<span title='<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                            <com:TDateValidator
                                    Display="Dynamic"
                                    ValidationGroup="validerNotification"
                                    ControlToValidate="dateDefinitiveFinMaxMarche"
                                    ErrorMessage="<%=Prado::localize('CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE')%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryEnregistrement').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TDateValidator>
                        </div>
                        <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleDateReelle')" onmouseover="afficheBulle('bulleDateReelle', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    </div>
                    <!--Fin Ligne Date définitive de fin maximale de marché-->
                    <div class="breaker"></div>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
        <!--Fin Bloc Bloc Dates définitives-->
        <!--Début Bloc Durée initiale du contrat-->
        <com:TActivePanel id="dureeInitialeMarche" cssclass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <div class="panel">
                    <h3><com:TTranslate>DEFINE_DUREE_MARCHE</com:TTranslate></h3>
                    <div class="line">
                        <div class="intitule-250"><label for="dureeMarche"><com:TTranslate>DEFINE_DUREE_INITIALE_MARCHE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TTextBox ID="dureeMarche"  Attributes.title="<%=Prado::Localize('DEFINE_DUREE_INITIALE_MARCHE')%>"
                                      CssClass="input-100 float-left-fix"
                        />
                        <div class="intitule-bloc"><com:TTranslate>MONTHS</com:TTranslate></div>
                        <div id="bulleDureeInitialeContrat" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_DUREE_CONTRAT_INFO_BULLE_MODIFIEE</com:TTranslate></div></div>
                        <com:TRequiredFieldValidator
                                ValidationGroup="Validation"
                                ControlToValidate="dureeMarche"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_DUREE_INITIALE_MARCHE')%>"
                                Text="<span title='<%=Prado::localize('DEFINE_DUREE_INITIALE_MARCHE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ControlToValidate="dureeMarche"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                ClientValidationFunction="isDureeMarcheValid"
                                ErrorMessage="<%=Prado::localize('DUREE_MARCHE_NON_VALIDE')%>"
                                EnableClientScript="true"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <img class="picto-info-intitule" onmouseout="cacheBulle('bulleDureeInitialeContrat')" onmouseover="afficheBulle('bulleDureeInitialeContrat', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
        <!--Fin Bloc Durée initiale du contrat-->
        <!--Début Bloc DATE_DEBUT_EXECUTION-->
        <com:TActivePanel id="dateDebutExecution" cssclass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <div class="panel">
                    <h3><com:TTranslate>DATE_DEBUT_EXECUTION</com:TTranslate></h3>
                    <div class="line">
                        <div class="intitule-250"><label for="dureeMarche"><com:TTranslate>DATE_DEBUT_EXECUTION</com:TTranslate></label> :</div>
                        <div class="calendar">
                            <com:TTextBox
                                    ID="dateExecution"
                                    Attributes.title="<%=Prado::localize('DATE_DEBUT_EXECUTION')%>"
                                    CssClass="heure"
                            />
                            <com:TImage id="debutExecutionImg" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->dateExecution->getClientId()%>'),'dd/mm/yyyy','');" />
                        </div>
                        <div class="intitule-bloc"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
        <!--Fin Bloc DATE_DEBUT_EXECUTION-->
        <com:DecisionEntitesEligibles ID="decisionEntitesEligibles"/>
        <!--Fin Bloc formulaire-->
        <com:THiddenField ID="donneeDecision" />
        <com:TCustomValidator
                ValidationGroup="Validation"
                ControlToValidate="donneeDecision"
                Display="Dynamic"
                ErrorMessage=""
                onServerValidate="validerDonneesDecision"
                Text="" >
        </com:TCustomValidator>
        <!--Debut line boutons-->
        <div class="boutons-line">
            <input name="annuler" value="Fermer" id="Fermer" class="bouton-moyen float-left" onclick="self.close();" title="<%=Prado::Localize('FERMER_DONNEES_SERONT_PERDUES')%>" type="submit" />
            <div class="right">
                <com:TActiveButton
                        id="buttonSave"
                        CssClass="bouton-moyen notice"
                        Attributes.value="<%=Prado::localize('TEXT_ENREGISTER')%>"
                        Attributes.title="<%=Prado::localize('DEFINE_ENREGISTRER_RESTER_PAGE')%>"
                        ValidationGroup="Validation"
                        onClick="saveContrat" />
                <com:TActiveButton
                        id="buttonValidate"
                        CssClass="bouton-moyen"
                        Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>"
                        Attributes.title="<%=Prado::localize('VALIDER_PASSER_AU_STATUT_SUIVANT')%>"
                        ValidationGroup="Validation"
                        onClick="validateContrat" />
            </div>
        </div>
        <!--Fin line boutons-->
    </com:TActivePanel>
</div>
<com:TActiveLabel id="script"/>
<com:TActiveLabel id="scriptJS"/>
<div id="bulleDateReelle" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_DATE_REELLE</com:TTranslate></div></div>
<script type="text/javascript">popupResize();</script>