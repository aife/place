<div class="panel panel-default bloc-toggle">
    <div class="panel-heading" role="tab" id="headingContactDevis">
        <h3 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseContactDevis" aria-expanded="true" aria-controls="collapseContactDevis"><i class="fa"></i><com:TTranslate>TEXT_CONTACTS_DEMANDE_DEVIS</com:TTranslate></a> </h3>
    </div>
    <div id="collapseContactDevis" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingContactDevis">
        <div class="panel-body">
            <div class="row container-fluid">
                <div class="col-xs-3 col-md-3"><strong>
                    <com:TTranslate>TEXT_ADRESSES_ELECTRONIQUE</com:TTranslate>
                </strong>:
                </div>
                <div class="col-md-8">
                    <div>
                        <com:TLabel id="imgEmail1"><i class="fa fa-envelope" title="<%=Prado::localize('EMAIL')%>"><span class="sr-only"><%=Prado::localize('EMAIL')%></span></i>
                            <com:TLabel id="emailDevis"/>
                        </com:TLabel> &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>