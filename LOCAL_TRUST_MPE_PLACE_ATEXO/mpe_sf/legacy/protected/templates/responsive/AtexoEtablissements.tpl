
	<com:TRepeater
			ID="repeaterEtablissements"
			EnableViewState="true"
			>
		<prop:HeaderTemplate>
			<div class="panel-body">
	<table class="table bordered-table table-striped table-hover treeview table-contacts">
		<caption><i class="glyphicon glyphicon-user"></i> <com:TTranslate>ETABLISSEMENTS_ENREGISTRES</com:TTranslate> <a href="#" class="toggle"><com:TTranslate>VOIR_TOUS_LES_CONTACTS_PARENTHESE</com:TTranslate></a></caption>
		<thead>
		<tr class="row-attributaire">
			<th colspan="3" scope="col"><com:TTranslate>CODE_ADRESSE</com:TTranslate></th>
			<th class="col-actions" scope="col"><com:TTranslate>CONTACTS</com:TTranslate></th>
		</tr>
		</thead>
		<tbody>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
		<!-- Etablissement -->
		<tr class="indent-n1 treegrid-<%#$this->ItemIndex+1%> treegrid-collapsed ">
			<td colspan="3"><span <%#$this->Data->getStatutActif() ? 'class="radie"' : '' %> ><%#$this->Data->getCodeEtablissement()%> <%# ($this->Data->getAdresse() || $this->Data->getAdresse2() || $this->Data->getCodePostal() || $this->Data->getVille())?'-' : ''%> <%#$this->Data->getAdresse()%> <%#$this->Data->getAdresse2()%> <%#$this->Data->getCodePostal()%> <%#$this->Data->getVille()%>
				<com:TLabel visible="<%#!$this->Data->getStatutActif()%>" text="<%#Prado::localize('ETABLISSEMENT_FERME_PARENTHESE')%>" /></span>
				<i <%#$this->Data->getEstSiege()? '' : 'style="display:none"'%> class="fa fa-building" data-original-title="Siège social" >
					<span class="sr-only"><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate></span>
				</i>
			</td>
			<td class="col-actions"><%# $this->TemplateControl->getInscritByEtablissement($this->Data->getIdEntreprise(),$this->Data->getIdEtablissement(),true)%></td>
		</tr>
			<com:TRepeater DataSource="<%#$this->TemplateControl->getInscritByEtablissement($this->Data->getIdEntreprise(),$this->Data->getIdEtablissement())%>"					>
				<prop:ItemTemplate>
						<tr class="child treegrid-parent-<%#$this->TemplateControl->ItemIndex+1%>">
							<td colspan="2" class="col-user"><div><i class="glyphicon glyphicon-user" title="<%=Prado::localize('CONTACT')%>"><span class="sr-only"><com:TTranslate>CONTACT</com:TTranslate></span></i><%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom())%>&nbsp;<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom())%> </div></td>
							<td class="col-contact col-break"><div><i class="fa fa-envelope" title="<%=Prado::localize('EMAIL')%>"><span class="sr-only"><com:TTranslate>EMAIL</com:TTranslate></span></i> <%#$this->Data->getEmail()%></div></td>
							<td class="col-tel col-break">
								<div><i class="glyphicon glyphicon-phone" title="<%=Prado::localize('TEXT_TELEPHONE')%>"><span class="sr-only"><com:TTranslate>TEXT_TELEPHONE</com:TTranslate></span></i> <%#$this->Data->getTelephone()%></div>
							</td>
						</tr>
					</prop:ItemTemplate>
				</com:TRepeater>
		<!-- // Fin Etablissement -->
			</prop:ItemTemplate>
			<prop:FooterTemplate>
				</tbody>
				</table>
				</div>
			</prop:FooterTemplate>
	</com:TRepeater>


<com:TPanel visible="false">
	<com:TActiveLabel ID="nombreElement"/>
	<com:TLabel ID="labelSlashTop"/>
	<com:TLabel ID="nombrePageTop"/>
	<com:TLabel ID="labelSlashBottom" />
	<com:TLabel ID="nombrePageBottom"/>
	<com:TPager ID="PagerTop"></com:TPager>
	<com:TPager ID="PagerBottom"></com:TPager>
	<com:TTextBox ID="pageNumberBottom" />
	<com:TTextBox ID="pageNumberTop"/>
	<com:TDropDownList ID="nbResultsTop" />
	<com:TDropDownList ID="nbResultsBottom" />
	<com:TLabel ID="labelAfficherTop"/>
	<com:TLabel ID="labelAfficherBottom"/>
	<com:TLabel id="resultParPageBottom" />
	<com:TLabel id="resultParPageTop" />
	<com:TPanel ID="panelBouttonGotoPageBottom"/>
	<com:TPanel ID="panelBouttonGotoPageTop"/>
	<com:TRegularExpressionValidator ID="formatCp"/>
	<com:TRegularExpressionValidator ID="formatCpEtrangere"/>
</com:TPanel>