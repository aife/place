<div class="sign-document">
    <div class="main-part" id="main-part">

        <div class="clearfix  hide-agent">
            <ul class="breadcrumb">
                <li>
                    <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                </li>
                <li>
                    <com:TTranslate>DEFINE_TEXT_OUTILS_DE_SIGNATURE</com:TTranslate>
                </li>
                <li>
                    <com:TTranslate>SIGNER_UN_DOCUMENT</com:TTranslate>
                </li>
            </ul>
        </div>
        <div class="breadcrumbs">
            <a href="/entreprise">
                Accueil &gt;
            </a>
            <span class="normal"><com:TTranslate>DEFINE_TEXT_OUTILS_DE_SIGNATURE</com:TTranslate> &gt; </span>
            <com:TTranslate>SIGNER_UN_DOCUMENT</com:TTranslate>

        </div>

        <com:MessageStatusChecker></com:MessageStatusChecker>

        <!--Debut Bloc Signature document-->
        <div>
            <com:PanelMessageErreur ID="panelMessageErreur"/>
            <com:TPanel id="panelInfo" visible="<%=(Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])=='entreprise')?true:false%>">
                <com:PanelMessageInformation ID="panelMessageInfo" message="<%=Prado::Localize('TEXT_SIGNATURE_DOC_ENTREPRISE')%>"/>
            </com:TPanel>
        </div>


        <div class="panel panel-default">

            <div class="panel-body">
                <p>
                    <strong><com:TTranslate>TEXT_SIGNATURE_DOC</com:TTranslate></strong>
                </p>

                <ul class="diagnostic-liste sign-document-list">
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>APPLICATION_SIGNATURE_DOC</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>JETON_SIGNATURE_DOC</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>VERFIER_CERTIFICATS_POUR_SIGNATURE_DOC</com:TTranslate>
                    </li>
                </ul>

            </div>
            <div id="cms-actif" style="display:<%= $this->Page->isCmsActif()%>">
                <div class="panel-footer clearfix">
                    <div class="clearfix" id="assistance-launch"
                         data-plateform="<%=Application\Service\Atexo\Atexo_Config::getParameter('UID_PF_MPE')%>"
                         data-url-crypto="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_CRYPTO')%>">
                        <a href="#"
                           v-on:click.prevent="verifyConnection(0)"
                           class="btn btn-primary btn-sm btn-footer pull-right bouton"
                           style="border:0;"><%=Prado::Localize('LANCER_OUTIL_SIGNATURE')%></a>
                    </div>

                    <script>
                        function showModalAssistance() {
                            openModal('demander-validation', 'popup-moyen1', document.getElementById('panelValidationNt'), 'Demande de validation');
                        }
                        J(document).ready(function () {
                            J('#assistance-launch').attr('data-url-crypto', window.location.origin + '/crypto');
                        });
                    </script>
                    <script src="./themes/js/assistance-marche-public-prado.js?k=<%=Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
                </div>
            </div>
            <div id="cms-inactif" style="display:<%= $this->Page->isCmsInactif()%>">
                <div class="panel-footer clearfix">
                    <com:TButton id="lancerJWS"
                                 cssClass="btn btn-primary btn-sm btn-footer pull-right bouton"
                                 Attributes.style="border:0"
                                 Attributes.title="<%=Prado::Localize('LANCER_OUTIL_SIGNATURE')%>" Onclick="lancerJWS"
                                 Text="<%=Prado::Localize('LANCER_OUTIL_SIGNATURE')%>">
                    </com:TButton>
                </div>
            </div>
        </div>
        <!--Fin Bloc Signature document-->
    </div>
</div>
