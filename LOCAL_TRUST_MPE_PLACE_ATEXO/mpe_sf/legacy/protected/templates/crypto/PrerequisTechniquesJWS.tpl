<!--Debut colonne droite-->
<div class="main-part testPrerequis" id="main-part">
    <div class="clearfix">
        <div class="breadcrumbs">
            <nav role="navigation">
            <span class="normal">
                <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
            </span>
            &gt;
                <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>
            </nav>
        </div>
    </div>
    <!--Debut Bloc Conditions utilisation-->
    <div class="form-field">
        <!--BEGIN BTN CONFIGURATION ET CONSULTATION-->
        <div class="row text-justify">
            <!--BEGIN CONFIGURATION-->
            <div class="col-md-12">
                <div class="list-group-item clearfix">
                    <h1 class="m-t-0 m-b-1">
                        <com:TTranslate>CONFIG_POSTE</com:TTranslate>
                    </h1>
                    <div>
                        <p>
                            <com:TTranslate>OUTIL_MIS_A_DISPOSITION_POUR_TESTER_CONFIGURATION</com:TTranslate>
                        </p>
 
                        <com:TConditional condition="$_GET['calledFrom'] == 'entreprise'">
                            <prop:trueTemplate>
                            <a href="index.php?page=Entreprise.DiagnosticPoste&callFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['calledFrom'])%>"
                               class="m-t-4 pull-right"
                               title="<%=Prado::localize('TESTER_MA_CONFIGURATION')%>">
                                <i class="fa fa-cogs p-r-1"></i>
                                <com:TTranslate>TESTER_MA_CONFIGURATION</com:TTranslate>
                            </a>
                            </prop:trueTemplate>
                        </com:TConditional>

                        <com:TConditional condition="$_GET['calledFrom'] == 'agent'">
                            <prop:trueTemplate>
                                <a href="index.php?page=Agent.DiagnosticPoste&callFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['calledFrom'])%>"
                                   class="m-t-4 pull-right"
                                   title="<%=Prado::localize('TESTER_MA_CONFIGURATION')%>">
                                    <i class="fa fa-cogs p-r-1"></i>
                                    <com:TTranslate>TESTER_MA_CONFIGURATION</com:TTranslate>
                                </a>
                            </prop:trueTemplate>
                        </com:TConditional>
                    </div>
                </div>
            </div>
            <!--END CONFIGURATION-->
            <!--BEGIN CONSULTATION DE TEST-->
            <div class="col-md-12">
                <com:TPanel cssClass="list-group-item clearfix" visible="<%= ! ($this->isAgent())%>">
                    <div class="content">
                        <h1 class="m-t-0 m-b-1">
                            <com:TTranslate>CONSULTATION_DE_TEST</com:TTranslate>
                        </h1>
                        <div>
                            <com:TTranslate>CONSULTATION_DE_TEST_MISE_A_DISPOSITION</com:TTranslate>
                            <com:TTranslate>TEST_CYCLE_ENTIER_REPONSE</com:TTranslate>
                            <a href="?page=Entreprise.EntrepriseAdvancedSearch&AllCons&orgTest"
                               class="btn btn-primary btn-sm m-t-3 pull-right"
                               title="<%=Prado::localize('CONSULTATION_DE_TEST')%>">
                                <i class="fa fa-arrow-right p-r-1" aria-hidden="true"></i>
                                <com:TTranslate>CONSULTATION_DE_TEST</com:TTranslate>
                            </a>
                        </div>
                    </div>
                </com:TPanel>
            </div>
            <!--END CONSULTATION DE TEST-->
        </div>
        <!--END BTN CONFIGURATION ET CONSULTATION-->
        <!--BEGIN NAV-->
        <div class="clearfix nav-prerequis">
            <div data-example-id="togglable-tabs">
                <div class="p-0">
                    <ul class="list-group list-unstyled prerequis-tabs m-b-0" id="tabTest"  role="tablist">
                        <li class="list-group-item kt-vertical-align p-0 active">
                            <a class="p-2 col" id="rubrique_2_1" href="#rubrique_2_paragraphe_1" role="tab" data-toggle="tab" aria-expanded="true">
                                <h4><com:TTranslate>CONNECTIQUE_CONFIGURATION_RESEAU</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_2" href="#rubrique_2_paragraphe_2" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>CONFIGURATION_POSTE_TRAVAIL</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_4" href="#rubrique_2_paragraphe_3" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>SYSTEM_EXPLOITATION_NAVIGATEUR</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_5" href="#rubrique_2_paragraphe_4" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>FORMAT_CERTIFICAT_NUMERIQUE</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_6" href="#rubrique_2_paragraphe_5" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>VERSION_ENV_JAVA</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_7" href="#rubrique_2_paragraphe_6" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>DEFINE_PROBLEME_INCOMPATIBILITES</com:TTranslate></h4>
                            </a>
                        </li>
                        <li class="list-group-item kt-vertical-align p-0">
                            <a class="p-2 col" id="rubrique_2_8" href="#rubrique_2_paragraphe_7" role="tab" data-toggle="tab">
                                <h4><com:TTranslate>DEFINE_RECOMMANDATIONS_AUX_ENTREPRISES</com:TTranslate></h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 p-l-0 m-t-18">
                    <div class="tab-content p-2">
                        <div class="tab-pane fade in active" role="tabpanel" id="rubrique_2_paragraphe_1">
                            <h2 class="m-t-0">
                                <com:TTranslate>CONNECTIQUE_CONFIGURATION_RESEAU</com:TTranslate>
                            </h2>
                            <ul>
                                <li><com:TTranslate>ACCES_INTERNET_HAUT_DEBIT_REQUIS</com:TTranslate></li>
                                <li><com:TTranslate>ENVIRONNEMENT_RESEAU</com:TTranslate>
                                    <ul class="p-t-1">
                                        <li><com:TTranslate>ENVIRONNEMENT_RESEAU_AUTORISER_HTTPS</com:TTranslate></li>
                                        <li><com:TTranslate>ENVIRONNEMENT_RESEAU_AUTORISER_TELECHARGEMENT_FICHIER_JWS</com:TTranslate></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_2">
                            <h2 class="m-t-0">
                                <com:TTranslate>CONFIGURATION_POSTE_TRAVAIL</com:TTranslate>
                            </h2>
                            <ul class="liste-actions">
                                <li><com:TTranslate>GESTION_COOKIES_SESSION</com:TTranslate></li>
                                <li><com:TTranslate>ENVIRONNEMENT_JAVA_INSTALLE</com:TTranslate></li>
                                <li><com:TTranslate>GESTION_FONCTION_CRYPTOGRAPHIQUE</com:TTranslate>
                                    <ul>
                                        <li><com:TTranslate>GESTION_CERTIFICAT_NUMERIQUE_AUTORISEE</com:TTranslate></li>
                                        <li><com:TTranslate>UTILISATION_NAVIGATEUR_STANDARD_JWS</com:TTranslate></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_3">
                            <h2 class="m-t-0">
                                <com:TTranslate>SYSTEM_EXPLOITATION_NAVIGATEUR</com:TTranslate>
                            </h2>
                            <div>
                                <p><com:TTranslate>AVOIR_UN_DES_OS_SUIVANTS</com:TTranslate></p>
                                <ul class="liste-actions">
                                    <li class="no-float"><com:TTranslate>MICROSOFT_WINDOWS_SEVEN</com:TTranslate></li>
                                    <li class="no-float"><com:TTranslate>MICROSOFT_WINDOWS_8</com:TTranslate></li>
                                    <li class="no-float"><com:TTranslate>MICROSOFT_WINDOWS_10</com:TTranslate></li>
                                    <li class="no-float"><com:TTranslate>APPLE_MAC_OSX</com:TTranslate></li>
                                    <li class="no-float"><com:TTranslate>LINUX_UBUNTU</com:TTranslate></li>
                                </ul>
                                <p class="m-t-8"><com:TTranslate>POSTE_TRAVAIL_UTILISANT_NAVIGATEUR_SUIVANT</com:TTranslate> :</p>
                                <ul class="liste-actions">
                                    <li ><com:TTranslate>NAVIGATEUR_PRE_REQUIS</com:TTranslate></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_4">
                            <h2 class="m-t-0">
                                <com:TTranslate>FORMAT_CERTIFICAT_NUMERIQUE</com:TTranslate>
                            </h2>
                            <p><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES</com:TTranslate></p>
                            <ul class="liste-actions">
                                <li><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES_DANS_MAGASIN_CERTIFICAT</com:TTranslate></li>
                                <li><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES_AU_FORMAT_P12</com:TTranslate></li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_5">
                            <h2 class="h4 page-header m-t-0">
                                <com:TTranslate>VERSION_ENV_JAVA</com:TTranslate>
                            </h2>
                            <p>
                                <com:TTranslate>JWS_REQUIERT_JRE</com:TTranslate>
                                <br/>
                                <br />
                                <com:TTranslate>POSTE_TRAVAIL_EQUIPE_JRE_POUR_JWS</com:TTranslate>
                                <br/>
                                <com:TTranslate>POSTE_TRAVAIL_EQUIPE_JRE_POUR_JWS_MISE_A_JOUR</com:TTranslate>
                                <br/>
                                <br/>
                                <com:TTranslate>CONDITION_VERSION_JAVA</com:TTranslate>
                                <br/>
                            </p>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_6">
                            <h2 class="m-t-0">
                                <com:TTranslate>DEFINE_PROBLEME_INCOMPATIBILITES</com:TTranslate>
                            </h2>
                            <p>
                                <com:TTranslate>DEFINE_PROBLEME_JAVA_CACHE</com:TTranslate>
                            </p>
                            <p>
                                <com:TTranslate>DEFINE_FICHIER_RAR_DECOMPRESSION</com:TTranslate>
                            </p>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="rubrique_2_paragraphe_7">
                            <h2 class="m-t-0">
                                <com:TTranslate>DEFINE_RECOMMANDATIONS_AUX_ENTREPRISES</com:TTranslate>
                            </h2>
                            <ul class="liste-actions">
                                <li><com:TTranslate>RECOMMANDATION_TESTER_CONFIG_POSTE</com:TTranslate></li>
                                <li><com:TTranslate>RECOMMANDATION_VERFIER_CERTIFICAT</com:TTranslate></li>
                                <li><com:TTranslate>RECOMMANDATION_POUR_SIGNATURE</com:TTranslate></li>
                                <li><com:TTranslate>RECOMMANDATION_NOMMAGE_FICHIERS</com:TTranslate></li>
                                <li><com:TTranslate>RECOMMANDATION_POUR_COMPRESSER_FICHIER</com:TTranslate></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END NAV-->
    </div>
    <!--Fin bloc Conditions utilisation-->
</div>
<!--Fin colonne droite-->