<!--Debut colonne droite-->
<div class="configuration-poste">
    <div class="main-part" id="main-part">
        <div class="clearfix hide-agent">
            <ul class="breadcrumb">
                <li>
                    <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                </li>
                <li>
                    <com:THyperLink ID="linkAnnonces"></com:THyperLink>
                    <com:TTranslate>TESTER_CONFIGURATION_MON_POSTE</com:TTranslate>
                </li>
            </ul>
        </div>
        <div class="breadcrumbs"><span class="normal"><com:TTranslate>SE_PREPARER_A_REPONDRE</com:TTranslate> &gt; </span><com:TTranslate>TESTER_CONFIGURATION_MON_POSTE</com:TTranslate></div>

        <com:MessageStatusChecker></com:MessageStatusChecker>

        <!--Debut Bloc Message Important-->
        <com:TPanel id="msgCertificatImportant" visible="false">
            <div class="form-bloc bloc-message-certificat">
                <div class="content">
                    <div class="msg-important">
                        <h3>
                            <com:TTranslate>DEFINE_IMPORTANT</com:TTranslate>
                        </h3>
                        <p><strong>
                            <com:TTranslate>CERTAINES_CONSULTATIONS_NECESSITENT_SIGNATURE</com:TTranslate>
                            <br/>
                            <com:TTranslate>SIGNALEES_PAR_PICTO</com:TTranslate>
                            : </strong></p>

                        <div class="message-certificat">
                            <img alt="<com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE</com:TTranslate>"
                                 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig.gif"/>
                            <div class="intitule-legende"> :
                                <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE</com:TTranslate>
                            </div>
                            <div class="breaker"></div>
                            <img alt=" <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE</com:TTranslate> "
                                 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig-avec-signature.gif"/>
                            <div class="intitule-legende">:
                                <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE</com:TTranslate>
                            </div>
                        </div>
                        <div class="spacer-small"></div>

                        <p><strong>
                            <com:TTranslate>POUR_SIGNER_VOS_DOCS_VERIFIER_SI</com:TTranslate>
                            :</strong></p>
                        <ul class="default-list">
                            <li>
                                <com:TTranslate>SIGNATURE_ELECTRONIQUE_REQUISE_POUR_REPONDRE</com:TTranslate>
                            </li>
                            <li>
                                <com:TTranslate>VOUS_DISPOSEZ_DUN_CERTIFICAT_ELECTRONIQUE_CONFORME</com:TTranslate>
                                : <a
                                    href="index.php?page=Entreprise.ConditionsUtilisation&calledFrom=entreprise#rubrique_1_paragraphe_3_2"
                                    target="_blank">
                                <com:TTranslate>EN_SAVOIR_PLUS</com:TTranslate>
                            </a></li>
                        </ul>
                    </div>
                    <div class="breaker"></div>

                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
        </com:TPanel>
        <!--Fin Bloc Message Important-->

        <!--Debut Entete -->
        <div class="panel panel-default">

            <h1 class="h4 panel-title" style="color: #3B83C3;">
                <com:TTranslate>TEST_CONFIGURATION_POSTE</com:TTranslate>
            </h1>

            <div class="panel-body">
                <p>
                    <com:TActiveLabel ID="testConfiguration"
                                      text="<%=(Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])=='agent')?Prado::localize('TEXT_TEST_CONFIGURATION_AGENT_CRYPTO'):Prado::localize('TEXT_TEST_CONFIGURATION_CRYPTO')%>"/>
                </p>

                <ul class="diagnostic-liste">
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>SYSTEME_EXPLOITATION</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>PRESENCE_ENVIRONNEMENT_PRESENT</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>ACCES_MAGASIN_CERTIFICATS</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>VERSION_NAVIGATEUR_WEB</com:TTranslate>
                    </li>
                    <li>
                        <span class="fa fa-chevron-right"></span>
                        <com:TTranslate>COMMUNICATION_AVEC_SERVEUR</com:TTranslate>
                    </li>
                </ul>


                <com:TLabel ID="infoCertif"/>
            </div>
            <div id="cms-actif" style="display:<%= $this->Page->isCmsActif()%>">
                <div class="clearfix">

                    <script language="JavaScript">
                    J(document).ready(function () {
                        J('#assistance-launch').attr('data-url-crypto', window.location.origin + '/crypto');
                    });
                </script>

            <div class="clearfix" id="assistance-launch"
                         data-plateform="<%=Application\Service\Atexo\Atexo_Config::getParameter('UID_PF_MPE')%>"
                         data-url-crypto="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_CRYPTO')%>">
                        <a href="#"
                           v-on:click.prevent="verifyConnection(1)"
                           class="btn btn-primary btn-sm btn-footer pull-right bouton"
                           style="border:0; width: 250px;"><%=Prado::Localize('LANCER_TEST_CONFIG')%></a>
                    </div>

                    <script>
                        function showModalAssistance() {
                            openModal('demander-validation', 'popup-moyen1', document.getElementById('panelValidationNt'), 'Demande de validation');
                        }
                    </script>
                </div>
                <script src="./themes/js/assistance-marche-public-prado.js?k=<%=Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
        </div>


                <div class="clearfix" id="assistance-launch"
                     data-plateform="<%=Application\Service\Atexo\Atexo_Config::getParameter('UID_PF_MPE')%>"
                     data-url-crypto="<%=Application\Service\Atexo\Atexo_MultiDomaine::replaceDomain(Application\Service\Atexo\Atexo_Config::getParameter('URL_CRYPTO'))%>"
                >

                    <a href="javascript:window.history.back();" class="btn btn-default btn-sm btn-footer bouton-retour"
                       title="<%=Prado::localize('TEXT_RETOUR')%>">
                        <com:TTranslate>TEXT_RETOUR</com:TTranslate>
                    </a>
                </div>
            </div>
            <div id="cms-inactif" style="display:<%= $this->Page->isCmsInactif()%>">
                <div class="panel-footer clearfix">
                    <a class="btn btn-primary btn-sm btn-footer pull-right bouton"
                       href="<%=Application\Service\Atexo\Atexo_Config::getParameter('PF_URL')%>agent/footer/test-config" title="<com:TTranslate>LANCER_TEST_CONFIG</com:TTranslate>">
                        <i class="fa fa-cogs m-r-1 hide-agent"></i>
                        <com:TTranslate>LANCER_TEST_CONFIG</com:TTranslate>
                    </a>
                </div>
            </div>
        </div>

        <!--Fin Bloc Resultat de la configuration-->
    </div>
</div>