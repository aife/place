	<!--Debut Bloc Identification de l'entreprise-->
	<com:TActivePanel cssClass="panel panel-default p-0" ID="panelInfoEntreprise">
		<div class="panel-heading" id="indentEntreprise" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseIndentEntreprise" aria-expanded="false" aria-controls="collapseIndentEntreprise">
			<h2 class="panel-title h4">
					<com:TTranslate>TEXT_IDENTIF_COMPTE_ENTREPRISE</com:TTranslate>
			</h2>
		</div>
		<div id="collapseIndentEntreprise" class="panel-collapse collapse" role="tabpanel" aria-labelledby="indentEntreprise">
			<div class="panel-body">
				<div id="indentifactionEntreprise" class="small clearfix">
					<div class="col-md-6 col-xs-12">
						<!--Debut Bloc gauche-->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate> : </label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="idRaisonSociale" />
							</div>
						</div>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_ADRESSE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="adresse" />
							</div>
						</div>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="adresseSuite" />
							</div>
						</div>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>DEFINE_CODE_POSTAL</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="codePostal" />
							</div>
						</div>
						<!-- -->
						<com:TPanel ID="panelRegion" Display="None" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_REGION</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="region" />
							</div>
						</com:TPanel>
						<!-- -->
						<com:TPanel ID="panelProvince" Display="None" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_PROVINCE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="province" />
							</div>
						</com:TPanel>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_VILLE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="ville" />
							</div>
						</div>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_PAYS</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="pays" />
							</div>
						</div>
						<!-- -->
						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_TELEPHONE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="telephone" />
							</div>
						</div>
						<!--Fin Bloc gauche-->
					</div>
					<div class="col-md-6 col-xs-12">
						<!--Debut Bloc droite-->
						<com:TPanel  Display="None" ID="panelTelEntreprise2" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_TEL_2</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="telephone2" />
							</div>
						</com:TPanel>

						<com:TPanel  Display="None" ID="panelTelEntreprise3" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_TEL_3</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="telephone3" />
							</div>
						</com:TPanel>

						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_FAX</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="fax" />
							</div>
						</div>

						<com:TPanel Display="None" ID="panelCnss" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_CNSS</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel cssClass="<%=(new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->entreprise,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" id="idcnss" />
							</div>
						</com:TPanel>

						<com:TPanel Display="None" ID="panelCapital" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_CAPITAL_SICIAL</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel cssClass="<%=(new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->entreprise,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" id="idcapital" />
							</div>
						</com:TPanel>

						<com:TPanel Display="None" ID="panelIdTaxProf" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_NUM_TAX_PROFESSIONNELLE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel cssClass="<%=(new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->entreprise,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" id="idtaxeprof"  />
							</div>
						</com:TPanel>

						<com:TPanel Display="None" ID="panelNumRc"  cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_RC_NUM_VILLE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="numRc" /> /
								<com:TLabel cssClass="<%=(new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->entreprise,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" id="villeRc"  />
							</div>
						</com:TPanel>

						<com:TPanel Display="None" ID="panelIfu"  cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_IDENTIFIANT_FISCAL_UNIQUE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel cssClass="<%=(new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->entreprise,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" id="ifu" />
							</div>
						</com:TPanel>

						<com:TPanel ID="panelSiren" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>SIREN_SIRET_SIEGE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="siretSiege" />
							</div>
						</com:TPanel>

						<com:TPanel ID="panelIdNational" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate>:</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="idNational" />
							</div>
						</com:TPanel>

						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_FJ</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="formeJuridique" />
							</div>
						</div>

						<div class="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="categorieEntreprise" />
							</div>
						</div>

						<com:TPanel cssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel'))? true:false %>">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_CODE_NACE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:AtexoReferentiel ID="atexoReferentielCodeNace" cas="cas9"/>
							</div>
						</com:TPanel>

						<com:TPanel ID="panelDomaineActivite" Display="None" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>:</label>
							<span class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:THiddenField ID="idsDomaines" />
								<div>
									<%=Application\Service\Atexo\Atexo_Util::truncateTexte($this->TemplateControl->idIdentificationEntreprise->displayDomainesActivites(),80)%>
									<span
											onmouseover="afficheBulle('ctl0_CONTENU_PAGE_idIdentificationEntreprise_infosDomaines', this)"
											onmouseout="cacheBulle('ctl0_CONTENU_PAGE_idIdentificationEntreprise_infosDomaines')"
											class="info-suite"><com:TLabel visible="<%=(Application\Service\Atexo\Atexo_Util::truncateTexte($this->TemplateControl->idIdentificationEntreprise->displayDomainesActivites(),80)!='') ? true:false%>" Text="..."/>
									</span>
								</div>
							</span>
							<com:TPanel ID="infosDomaines" CssClass="info-bulle" >
								<div><%=$this->TemplateControl->idIdentificationEntreprise->displayDomainesActivites()%></div>
							</com:TPanel>

						</com:TPanel>

						<com:TPanel ID="panelCodeApe" Display="Dynamic" cssClass="clearfix">
							<label class="col-md-5 col-xs-12 "><com:TTranslate>CODE_APE_NAF_NACE</com:TTranslate> :</label>
							<div class="col-md-7 col-md-offset-0 col-xs-11 col-xs-offset-1 ">
								<com:TLabel id="codeApeNafNace" />
							</div>
						</com:TPanel>
						<!--Fin Bloc droite-->
					</div>
				</div>
			</div>
		</div>
	</com:TActivePanel>
	<!--Fin Bloc Identification de l'entreprise-->

	<com:TPanel visible="false" >
		<com:TLabel id="description" />
		<com:TLabel id="panelSiteInternet" />
		<com:TLabel id="siteInternetLabel" />
		<com:THyperLink id="siteInternet" />
		<com:TLabel id="siegeSocial" />
		<com:TLabel id="paysSiren" />
		<com:TPanel id="panelDescription" />
		<com:AtexoSoumissionnaire id="atexoSoumissionnaire" visible="false"/>
	</com:TPanel>