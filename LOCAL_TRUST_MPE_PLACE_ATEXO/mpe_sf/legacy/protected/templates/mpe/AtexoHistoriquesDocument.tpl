<!--Debut Modal Message nouvelle version non disponible-->
<div class="modal-historique" style="display:none;">
    <!--Debut message infos-->
    <com:TActivePanel ID="panelInfoVersion" cssClass="bloc-message form-bloc-conf msg-info" >
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <div class="message bloc-700">
                <span><com:TActiveLabel ID="msgInfo" /></span>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </com:TActivePanel>
    <!--Fin  message infos-->
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
		
			<com:TActiveRepeater ID="repeaterHistoriques" >
				<prop:HeaderTemplate>	
	            <table class="table-results table-contenus" summary="Attestation fiscale" id="tableau_historique">
	                <caption><com:TTranslate>ICONE_HISTORIQUE</com:TTranslate></caption>
	                <thead>
	                    <tr>
	                        <th class="top" colspan="2"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
	                    </tr>
	                    <tr>
	                        <th class="col-700" id="historique_attestionFiscale_date">
	                        	<com:TActiveLinkButton 
   									CssClass="on"
									OnCallback="SourceTemplateControl.sortDataSource" 
									Text="<%=Prado::localize('DATE')%>"
									ActiveControl.CallbackParameter="DateRecuperation"
								/>
								<com:TActiveImageButton 
									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" 
									OnCallback="SourceTemplateControl.sortDataSource"
									ActiveControl.CallbackParameter="DateRecuperation"
									AlternateText="<%=Prado::localize('DATE')%>" 
									Attributes.title="<%=Prado::localize('DEFINE_TRIER_PAR_DATE')%>" 
								/>
	                        </th>
	                        <th class="actions" id="historique_attestionFiscale_actions"><com:TTranslate>DEFINE_TELECHARGER</com:TTranslate></th>
	                    </tr>
	                </thead>
	                </prop:HeaderTemplate>
					<prop:ItemTemplate>
	                <tr>
	                    <td class="col-700" headers="historique_attestionFiscale_date"><com:TLabel ID="dateRecuperation" Text="<%#$this->Data->getDateRecuperation('d/m/Y')%>" /></td>
	                    <td class="actions" headers="historique_attestionFiscale_actions">
	                    	<com:TActiveImageButton
								id="pictoDownload"
								cssClass="btn-action"
								ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-telechargement.png" 
								Attributes.alt="<%=Prado::localize('DEFINE_TELECHARGER')%>"
								Attributes.title="<%=Prado::localize('DEFINE_TELECHARGER')%>"
								onCommand="SourceTemplateControl.downloadVersion"
								CommandParameter="<%#$this->Data->getIdVersionDocument() .'#'. $this->Data->getIdDocument()%>"
							/>
	                    </td>
	                </tr>
	                </prop:ItemTemplate>
	            <prop:FooterTemplate>
							</table>
					    </prop:FooterTemplate>
            </com:TActiveRepeater>
            <div class="line-actions">
                <com:TActiveLinkButton
					CssClass="bouton float-right"
					ID="linkDefinirActionFormePrix"
					Attributes.title="<%=Prado::localize('DEMANDER_UNE_NOUVELLE_VERSION')%>"
					onClick="addNewVersion"
				>
					<span class="left">&nbsp;</span>
                	<img alt="" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-telechargement.png" />
					<com:TTranslate>DEMANDER_UNE_NOUVELLE_VERSION</com:TTranslate>
					<prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
					<prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
					<prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>
   				</com:TActiveLinkButton>
            </div>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="<%=Prado::localize('FERMER')%>" title="<%=Prado::localize('FERMER')%>" onclick="document.getElementById('<%=$this->Page->buttonRefresh->getClientId()%>').click();J('.modal-historique').dialog('close');" />
	</div>
	<!--Fin line boutons-->
</div>
<com:TActiveLabel id="javascriptLabel" />
<com:TButton id="buttonDownload" onClick="downloadFichier" style="display:none;"/>

<com:TActivePanel visible="false" >
	<com:TActiveLabel ID="titrePopin"  />
</com:TActivePanel>
<!--Fin Modal Message nouvelle version non disponible-->