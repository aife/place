
<div class="popup-moyen" id="container">
	<h1><com:TTranslate>TEXT_DETAIL_ENTREPRISE</com:TTranslate></h1>
	<div class="spacer-small"></div>
	   <!--Bloc message-->
        <com:TPanel id="messageSaisieManuel" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSGMAP') && ($this->getCalledFrom() == 'agent'))? true:false %>"  cssClass="form-bloc margin-0">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <p class="infos-line margin-0"><%=Prado::localize('TEXT_MESSAGE_INFO_SAISIE_MANUEL')%></p>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
        <div class="spacer-small"></div>
        <!--Fin Bloc message-->
	<com:IdentificationEntreprise id="idIdentificationEntreprise" />
	<!--Debut Bloc Collaboration souhaitee-->
	<com:TPanel id="panelCollaboration" Visible="<%=($this->getCalledFrom() == 'agent') ? false : true %>" cssClass="toggle-panel">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div class="title-toggle" onclick="togglePanel(this,'typeCollaboration');"><%=Prado::localize('DEFINE_COLLABORATION_SOUHAITEE')%></div>
				<div class="panel" style="display:none;" id="typeCollaboration">
					<div class="line">
						<div class="intitule-bloc intitule-150"><%=Prado::localize('DEFINE_COLLABORATION_SOUHAITEE')%> :</div>
						<div class="content-bloc">
							<div class="collaboration">
								<com:TRepeater  ID="repeaterTypeCollaboration" > 
									<prop:ItemTemplate>
											<div  class="statut-collaborateur statut-collaborateur-<%#$this->Data['id']%>sur4" title="<%#($this->Data['libelle'])%>" ></div>
									</prop:ItemTemplate>
								</com:TRepeater>
							</div>
						</div>								
					</div>
                    <div class="spacer-small"></div>
                    <h4><%=Prado::localize('TEXT_PERSONNE_A_CONTACTER_GROUPEMENT')%></h4>
                    <div class="spacer-small"></div>
                    <div class="bloc-355 float-left">
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('NOM')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="nomContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('PRENOM')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="prenomContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('TEXT_ADRESSE')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="adresseContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('TEXT_ADRESSE_SUITE')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="adresseSuiteContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('TEXT_CP')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="cpContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('TEXT_VILLE')%> :</div>
							<div class="content-bloc bloc-190"><com:TLabel ID="VilleContact"/></div>
                        </div>
                        <!-- -->
                    </div>
                    <div class="bloc-400 float-left">
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('DEFINE_FONCTION_MAJ')%> :</div>
							<div class="content-bloc bloc-200"><com:TLabel ID="fonctionContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('DEFINE_E_MAIL')%> :</div>
							<div class="content-bloc bloc-200"><com:TLabel ID="emailContact"/></div>
                        </div>
                        <!-- -->
                        <div class="line">
                            <div class="intitule-150"><%=Prado::localize('TEXT_TELEPHONE')%> :</div>
							<div class="content-bloc bloc-200"><com:TLabel ID="telContact"/></div>
                        </div>
                     </div>
					
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Collaboration souhaitee-->
	<com:DirigeantsEntreprise id="idDirigeantsEntreprise" visible="<%=($this->getCalledFrom() == 'agent')? true: false%>"/>
	<!-- Debut Bloc Adresses Alectroniques -->
    <com:AtexoContactDevis visible="<%=($this->getCalledFrom() == 'agent')? true: false%>"/>
	<!--Fin Bloc Adresses Alectronique -->
	
	<!--Debut Bloc Activite-->
	<div class="toggle-panel">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'activiteEntreprise');"><com:TTranslate>DEFINE_ACTIVITE</com:TTranslate></div>
			<div class="panel" style="display:none;" id="activiteEntreprise">
			<com:TPanel id="labelActiviteEntreprise" Visible = "true">
					<div class="line">
						<div class="intitule-150"  ><strong><com:TTranslate>DEFINE_SITE_INTERNET</com:TTranslate> :</strong></div>
						<div class="content-bloc bloc-600"><com:TLabel  id="labelSiteInternet" /></div>
					</div>
					<div class="spacer-mini"></div>
                    <!-- Début domaines d'activités -->
	                <com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel'))? true:false %>">
		                <div class="intitule-150"><strong><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate></strong> :</div>
						<div class="content-bloc" id="domainesActivites">								
							<com:AtexoReferentiel ID="idAtexoRefDomaineActivite" cas="cas9"/>
						</div>
	                </com:TPanel>
	                <!-- Fin domaines d'activités -->
					<!-- -->
					<div class="line">
						<div class="intitule-150"><strong><%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel'))? Prado::localize('DEFINE_DESCRIPTION_GENERALE_ACTIVITES') : Prado::localize('DEFINE_DESCRIPTION_ACTIVITE') %> :</strong></div>
						<div class="content-bloc bloc-600"><com:TLabel  id="labelDescriptionActivite"/></div>
					</div>
					<div class="spacer-mini"></div>
					<!-- -->
					<com:TPanel ID="panelActiviteDomaineDefense" cssClass="line" Display="Dynamic"  visible="<%=($this->getCalledFrom() == 'agent')? true: false %>">
						<div class="intitule-bloc intitule-150"><strong><com:TTranslate>DEFINE_ACTIVITES_DOMAINE_DEFENSE</com:TTranslate> :</strong></div>
						<div class="content-bloc bloc-600"><com:TLabel  id="labelActiviteDefense"/></div>
					</com:TPanel>
				</com:TPanel>
				<!-- -->
				<com:TPanel ID="panelDocumentsComerciaux" cssClass="line" Display="None">
					<div class="intitule-bloc bloc-150"><strong><com:TTranslate>TEXT_DOCUMENTS_COMMERCIAUX</com:TTranslate> :</strong></div>
					<div class="content-bloc bloc-600">
						<com:TPanel ID="panelLinkDownloadPj" cssClass="picto-link margin-0" display="None"><img Attributes.title="<%=Prado::localize('TEXT_TELECHARGER_FICHIER')%>" Attributes.alt="<%=Prado::localize('TEXT_TELECHARGER_FICHIER')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"/><com:TLinkButton onClick="downloadPiece"><com:TLabel ID="labelleDocCommercial" />(<com:TLabel ID="sizeDocCommercial"/>)</com:TLinkButton></com:TPanel>
					</div>
				</com:TPanel> 
				<div class="spacer-mini"></div>
				<!-- -->
				<com:TPanel ID="panelQualification" Display="Dynamic" cssClass="line"  visible="<%=($this->getCalledFrom() == 'agent')? true : false %>">
					<div class="intitule-150"><strong><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate> :</strong></div>
					<div class="content-bloc bloc-600"><com:TLabel ID="labelQualifications" /></div>
				</com:TPanel>
				<div class="spacer-mini"></div>
				
				<com:AtexoLtReferentiel id="ltrefEntr" mode="0" entreprise="1" cssClass="intitule-250 bold"/>
			
                <!-- Début qualification des entreprises Lt-Ref -->
                <com:TActivePanel ID="panelQualificationLtRef" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel'))? true:false %>" cssClass="line">
					<div class="intitule-bloc intitule-150"><label for="qualification"><strong><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate></strong></label> :</div>
					<com:AtexoReferentiel ID="idAtexoRefQualification" cas="cas9"/>
			   </com:TActivePanel>
                <!-- Fin qualification des entreprises Lt-Ref -->
				<!-- -->
				<com:TPanel ID="panelAgrements" Display="None" cssClass="line"  visible="<%=($this->getCalledFrom() == 'agent')? true : false %>">
					<div class="intitule-150"><strong><com:TTranslate>TEXT_AGREMENT</com:TTranslate> :</strong></div>
					<div class="content-bloc bloc-600"><com:TLabel ID="labelAgrements" /></div>
				</com:TPanel>
				<div class="spacer-mini"></div>
                <!-- Début certification des entreprises Lt-Ref -->
                <com:TActivePanel ID="panelcertification" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel'))? true : false %>" cssClass="line">
					<div class="intitule-bloc intitule-150"><label for="certification"><strong><com:TTranslate>TEXT_AGREMENT_LT_REFERENTIEL</com:TTranslate></strong></label> :</div>
					<com:AtexoReferentiel ID="idAtexoCertificatCompteEtpRef" cas="cas9"/>
			   </com:TActivePanel>
                <!-- Fin certification des entreprises Lt-Ref -->
				<!-- -->
				<com:TPanel ID="panelMoyensHumains" cssClass="line" Display="None">
					<div class="intitule-150"><strong><com:TTranslate>TEXT_MOYENS_HUMAINS</com:TTranslate> :</strong></div>
					<div class="content-bloc bloc-600"><com:TLabel ID="labelMoyensHumains"/><br/></div>
				</com:TPanel>
				<div class="spacer-mini"></div>
				<!-- -->
				<!-- -->
				<com:TPanel ID="panelMoyensTechnique" cssClass="line" Display="None">
					<div class="intitule-150"><strong><com:TTranslate>TEXT_MOYENS_TECHNIQUES</com:TTranslate> :</strong></div>
					<div class="content-bloc bloc-600"><com:TLabel ID="labelMoyensTechniques"/><br/></div>
				</com:TPanel>
				<div class="spacer-mini"></div>
				<!-- -->
			  <com:TActivePanel ID="panelPrestation" Display="None">
					<com:PrestationEntreprise ID="prestationEntreprise"/>
			  </com:TActivePanel>
				<div class="spacer"></div>
				
	<com:ActiviteEntreprise id="idActiviteEntreprise"  visible="<%=($this->getCalledFrom() == 'agent')? true : false %>" />
	<com:InfoDonneesComplementaires id="idDonneesComplementaires" Visible="<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && ($this->getCalledFrom() == 'agent')))?true:false %>" />
	<!--Debut Bloc Document du coffre-fort-->
	
	<com:TPanel cssclass="toggle-panel" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AccesAgentsCfeBdFournisseur', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())? true:false)%>">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'docsEntreprise');"><com:TTranslate>TEXT_DOCUMENTS_ENTREPRISE</com:TTranslate></div>
			<div class="panel" style="display:none;" id="docsEntreprise">
            	<com:AtexoTableauDocCfe ID="tableauDocumentsDuCoffreFort"></com:AtexoTableauDocCfe>
            	<h2><center><com:TLabel ID="aucunDocCoffreFort" visible="false"></com:TLabel><h2><center>
            	<div class="breaker"></div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>
	
	<!--Fin Bloc Document du coffre-fort-->
	<!--Debut Bloc etablissements-->
	<com:TPanel ID="panelEtablissements">
		<com:AtexoEtablissements ID="listeEtablissments" />
	</com:TPanel>
	<!--Fin Bloc etablissements-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TButton cssClass="bouton-moyen-120 float-left" Text="<%=Prado::localize('FERMER_FENETRE')%>" attributes.title="<%=Prado::localize('FERMER_FENETRE')%>" attributes.onclick="self.close();" />
		<com:TButton cssClass="bouton-imprimer-small float-right" Text="<%=Prado::localize('TEXT_IMPRIMER')%>" attributes.title="<%=Prado::localize('TEXT_IMPRIMER')%>" attributes.onclick="window.print();" />
	</div>
	<!--Fin line boutons-->
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
	
</div>
<script type="text/javascript">popupResize();</script>
<com:TPanel visible="false" >
	<com:TLabel id="paysSiren" />
	<com:TLabel id="idRaisonSociale" />
</com:TPanel>