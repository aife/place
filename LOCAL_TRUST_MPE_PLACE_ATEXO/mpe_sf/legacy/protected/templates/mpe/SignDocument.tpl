<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<com:AtexoAppletObject MainClass="MpeChiffrementApplet" ClassIdApplet="mpe.MpeChiffrementApplet" Params="<%=$this->getParamsMpeChiffrementApplet()%>"/>
		<com:AtexoAppletObject MainClass="SelectionFichierApplet" ClassIdApplet="SelectionFichierApplet" Params="<%=$this->getParamsSelectionFichierApplet()%>" />

		<div class="breadcrumbs"><span class="normal"><com:TTranslate>DEFINE_TEXT_OUTILS_DE_SIGNATURE</com:TTranslate></span> &gt; <com:TTranslate>SIGNER_UN_DOCUMENT</com:TTranslate></div>
		<!--Debut Bloc Signature document-->
		<com:AtexoAppletLoading />
		<!--Debut Bloc Message Important-->
		<div class="modal-bloc" id="processing" style="display:none">
			<div class="form-bloc">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<div class="bloc-certificat">
						<div><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto_certificate.png" alt="Certificat" /></div>
						<div><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-bar.gif" alt="" /></div>
						<p><com:TTranslate>TEXT_TRAITEMENT_EN_COURS</com:TTranslate><br /><br /><com:TTranslate>TEXT_FICHIER_A_SIGNER</com:TTranslate> : <com:TLabel id="nomFichierASigner"/><br /><br /><com:TTranslate>REPERTOIRE_DESTINATION</com:TTranslate> : <com:TLabel id="cheminFichierASigner"/></p>
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</div>
		</div>
		<!--Fin Bloc Message Important-->
		<com:PanelMessageErreur ID="panelMessageErreur"/>
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<p><strong><com:TTranslate>TEXT_SIGNATURE_DOC</com:TTranslate></strong></p>
				<div class="spacer-small"></div>
				<div class="line">
					<div class="intitule-bloc bloc-150"><label for="docSignature"><com:TTranslate>DOCUMENT_A_SIGNER</com:TTranslate></label> :</div>
					<div class="content-bloc bloc-600">

					    <com:TTextBox ID="docSignature" Attributes.title="<%=Prado::localize('DOCUMENT_A_SIGNER')%>" CssClass="bloc-500 float-left" attributes.onClick="initialiserSelectionFichierApplet('<%=$this->docSignature->getClientId()%>', new Array(<%=Application\Service\Atexo\Atexo_Util::getExtensionFilesSignatures()%>),'selectionEffectuee');" />
					    <com:TRequiredFieldValidator
							ControlToValidate="docSignature"
							ValidationGroup="validateName"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_AUCUN_FICHIER8A_VERIFIER_SIGNER_CHIFFRER')%>"
							EnableClientScript="true"
							Text=" ">
							<prop:ClientSide.OnValidationError>
			 				document.getElementById('ctl0_CONTENU_PAGE_panelMessageErreur_panelMessage').style.display='block';
			 				document.getElementById('ctl0_CONTENU_PAGE_panelMessageErreur_labelMessage').innerHTML="<%=Prado::localize('TEXT_AUCUN_FICHIER8A_VERIFIER_SIGNER_CHIFFRER')%>";
							</prop:ClientSide.OnValidationError>
							<prop:ClientSide.OnValidationSuccess>
								document.getElementById('ctl0_CONTENU_PAGE_panelMessageErreur_panelMessage').style.display='none';
							</prop:ClientSide.OnValidationSuccess>
			         	</com:TRequiredFieldValidator>
						<input
							type="button"
							class="float-left"
							name="docSignatureBrowse"
							id="docSignatureBrowse"
							title="<%=Prado::localize('REPERTOIRE_DESTINATION')%>"
							Value="<%=Prado::localize('DEFINE_PARCOURIR')%>"
							onClick="initialiserSelectionFichierApplet('<%=$this->docSignature->getClientId()%>', new Array(<%=Application\Service\Atexo\Atexo_Util::getExtensionFilesSignatures()%>),'selectionEffectuee');" />
					</div>
				</div>
				<div class="spacer-small"></div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Signature document-->
		<div class="spacer-small"></div>

		<!--Debut line boutons-->
		<div class="boutons-line">
			<com:TActiveButton CssClass="bouton-validation float-right" Text="<%=Prado::localize('TEXT_VALIDER')%>" ToolTip="<%=Prado::localize('TEXT_VALIDER')%>" Attributes.onclick="if(document.getElementById('<%=$this->docSignature->getClientId()%>').value){initialiserSignDocApplet('<%=$this->docSignature->getClientId()%>', '<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_MODULE_VALIDATION_SIGNATURE')%>','<%=$this->signature->getClientId()%>');}"
			ValidationGroup="validateName" />
		</div>
		<span style="display:none">
			<com:THiddenField ID="signature"/>
			<com:THiddenField ID="cheminDocument"/>
			<com:THiddenField ID="nomFichierSignature"/>
			<com:TButton ID="sendSignature" OnClick="sendSignature"/>
		</span>
		<!--Fin line boutons-->
		<div class="breaker"></div>
	</div>
	<!--Fin colonne droite-->