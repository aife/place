	<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
        <div class="breadcrumbs"><span class="normal"><com:TTranslate>SE_PREPARER_A_REPONDRE</com:TTranslate> &gt; </span><com:TTranslate>TESTER_CONFIGURATION_MON_POSTE</com:TTranslate></div>
        <div class="clearfix hide-agent">
            <ul class="breadcrumb">
                <li>
                    <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                </li>
                <li>
                    <a href="?page=Entreprise.EntrepriseAdvancedSearch&AllCons">
                        <com:TTranslate>SE_PREPARER_A_REPONDRE</com:TTranslate>
                    </a>
                </li>
                <li>
                    <com:THyperLink ID="linkAnnonces"></com:THyperLink>
                    <com:TTranslate>TESTER_CONFIGURATION_MON_POSTE</com:TTranslate>
                </li>
            </ul>
        </div>
        <script src="ressources/java/deployJava.js?k=<%=Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
 <script language="javascript">
	 var gestionTimeOut = 0 ;
/**
 * Permet de renvoyer les informations du test effectué.
 *
 * @param identifiant l'identifiant associé à la réponse attendu
 * @param information l'information complémentaire
 * @param valide <code>true</code> si le test s'est déroulé correctement, sinon <code>false</false>
 */
function renvoyerInformation(identifiant, information, valide) {
    var element = document.getElementById(identifiant);
	if(gestionTimeOut){
		clearTimeout(gestionTimeOut);
	}
    if (element != null) {
        var message = document.getElementById(identifiant+'Message');
        if (message != null) {
        	message.innerHTML = information;
        }
        var imageAttente = document.getElementById(identifiant+'Img');
        if (valide != null && valide != 'true') {
            imageAttente.src = "<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif";
            imageAttente.alt = "<%=Prado::localize('DEFINE_NON')%>";
            imageAttente.title = "<%=Prado::localize('DEFINE_NON')%>";
            divEnSavoirPlus = document.getElementById(identifiant+'NotOK');
            if (divEnSavoirPlus != null) {
            	divEnSavoirPlus.style.display = "";
            }
            document.getElementById("succesApplet").value = "0";
        } else {
            imageAttente.src = "<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif";
            imageAttente.alt = "<%=Prado::localize('COMPATIBLE')%>";
            imageAttente.title = "<%=Prado::localize('COMPATIBLE')%>";
            divEnSavoirPlus = document.getElementById(identifiant+'OK');
            if (divEnSavoirPlus != null) {
            	divEnSavoirPlus.style.display = "";
            }
        }
    }
}
/**
 * Lance l'initialisation de l'applet
 */
function initialiserDiagnosticApplet() {
	gestionTimeOut = setTimeout(timeOutDiagnostic, 30000);
	document.getElementById("succesApplet").value = "1";
	document.DiagnosticApplet.setIdentifiantSystemeExploitation("identifiantSystemeExploitation");
	document.DiagnosticApplet.setIdentifiantJavaVersionNumero("identifiantJavaVersionNumero");
	document.DiagnosticApplet.setIdentifiantJavaVersionBit("identifiantJavaVersionBit");
	document.DiagnosticApplet.setIdentifiantAppletDemarre("identifiantAppletDemarre");
	document.DiagnosticApplet.setIdentifiantTestChiffrement("identifiantTestChiffrement");
	document.DiagnosticApplet.setIdentifiantTestAccesMagasin("identifiantTestAccesMagasin");
	document.DiagnosticApplet.setMethodeJavascriptRenvoiResultat("renvoyerInformation");
	document.DiagnosticApplet.setMethodeJavascriptFinDiagnostic("finDiagnostic");
	document.DiagnosticApplet.setMethodeJavascriptDebutAttente('showLoader');
	document.DiagnosticApplet.setMethodeJavascriptFinAttente('showLoader');
	document.DiagnosticApplet.setMethodeJavascriptRecuperationLog('logApplet');
    document.DiagnosticApplet.executer();
}

function finDiagnostic(){
	 if (document.getElementById("succesApplet").value == "0") {
    	document.getElementById('div_echec').style.display = "";
		document.getElementById('div_succes').style.display = "none";
		document.getElementById('ctl0_CONTENU_PAGE_boutonSubmitEchec').click();
	}else{
		document.getElementById('div_succes').style.display = "";
		document.getElementById('div_echec').style.display = "none";
		document.getElementById('ctl0_CONTENU_PAGE_boutonSubmitSucces').click();
	}
}
function logApplet(log){
	document.getElementById('div_echec').style.display = "";
	document.getElementById('div_succes').style.display = "none";
	document.getElementById('ctl0_CONTENU_PAGE_logApplet').value = log;
	document.getElementById('ctl0_CONTENU_PAGE_boutonSubmitEchec').click();
}
function timeOutDiagnostic(){
	document.getElementById('div_echec').style.display = "";
	document.getElementById('div_succes').style.display = "none";
	document.getElementById('ctl0_CONTENU_PAGE_boutonSubmitEchec').click();

	var srcKO = "<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif";
	var altKO = "<%=Prado::localize('DEFINE_NON')%>";
	var titleKO = "<%=Prado::localize('DEFINE_NON')%>";


	var imageAttente = document.getElementById('identifiantSystemeExploitationImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantSystemeExploitationNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}

	var imageAttente = document.getElementById('identifiantJavaVersionNumeroImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantJavaVersionNumeroNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}

	var imageAttente = document.getElementById('identifiantJavaVersionBitImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantJavaVersionBitNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}
	var imageAttente = document.getElementById('identifiantAppletDemarreImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantAppletDemarreNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}
	var imageAttente = document.getElementById('identifiantTestChiffrementImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantTestChiffrementNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}
	var imageAttente = document.getElementById('identifiantTestAccesMagasinImg');
	if (imageAttente.src.indexOf("picto-check-ok-small.gif") == -1) {
		imageAttente.src = srcKO;
		imageAttente.alt = altKO;
		imageAttente.title = titleKO;
		divEnSavoirPlus = document.getElementById('identifiantTestAccesMagasinNotOK');
		if (divEnSavoirPlus != null) {
			divEnSavoirPlus.style.display = "";
		}
	}


	}
</script>

		<com:AtexoAppletObject MainClass="DiagnosticApplet" ClassIdApplet="DiagnosticApplet" />



		<!--Debut Bloc Message Important-->
		<com:TPanel id="msgCertificatImportant" visible="false">
			<div class="form-bloc bloc-message-certificat">
				<div class="content">
					<div class="msg-important">
						<h3>
							<com:TTranslate>DEFINE_IMPORTANT</com:TTranslate>
						</h3>
						<p>
							<strong>
								<com:TTranslate>CERTAINES_CONSULTATIONS_NECESSITENT_SIGNATURE</com:TTranslate>
								<com:TTranslate>SIGNALEES_PAR_PICTO</com:TTranslate> :
							</strong>
						</p>
						<div class="message-certificat">
							<img alt="<com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE</com:TTranslate>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig.gif" />
							<div class="intitule-legende"> : <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE</com:TTranslate></div>
							<div class="breaker"></div>
							<img alt=" <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE</com:TTranslate> " src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig-avec-signature.gif" />
							<div class="intitule-legende">: <com:TTranslate>TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE</com:TTranslate> </div>
						</div>
						<div class="spacer-small"></div>

						<p><strong><com:TTranslate>POUR_SIGNER_VOS_DOCS_VERIFIER_SI</com:TTranslate> :</strong></p>
						<ul class="default-list">
							<li><com:TTranslate>SIGNATURE_ELECTRONIQUE_REQUISE_POUR_REPONDRE</com:TTranslate></li>
							<li><com:TTranslate>VOUS_DISPOSEZ_DUN_CERTIFICAT_ELECTRONIQUE_CONFORME</com:TTranslate> : <a href="index.php?page=Entreprise.ConditionsUtilisation&calledFrom=entreprise#rubrique_1_paragraphe_3_2" target="_blank"><com:TTranslate>EN_SAVOIR_PLUS</com:TTranslate></a></li>
						</ul>
					</div>
					<div class="breaker"></div>

				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</div>
		</com:TPanel>
		<!--Fin Bloc Message Important-->

		<!--Debut Entete -->
			<div class="form-field">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<com:TPanel CssClass="bloc-message form-bloc-conf msg-info" visible="<%= (Prado::localize('MESSAGE_PERSONNALISE_TEST_CONFIG_POSTE') != 'MESSAGE_PERSONNALISE_TEST_CONFIG_POSTE')%>">
							<com:TTranslate>MESSAGE_PERSONNALISE_TEST_CONFIG_POSTE</com:TTranslate>
						</com:TPanel>
						<h3><com:TTranslate>TEST_CONFIGURATION_POSTE</com:TTranslate></h3>
						<p><com:TActiveLabel ID="testConfiguration" text="<%=(Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])=='agent')?Prado::localize('TEXT_TEST_CONFIGURATION_AGENT'):Prado::localize('TEXT_TEST_CONFIGURATION')%>"/></p>
						<ul class="default-list">
									<li><com:TTranslate>OS_ET_NAVIGATEUR</com:TTranslate></li>
									<li><com:TTranslate>ENVIRONNEMENT_JAVA</com:TTranslate></li>
									<li><com:TTranslate>APPLET_DEMARREE</com:TTranslate></li>
									<li><com:TTranslate>CAPACITE_CRYPTOGRAPHIQUE</com:TTranslate></li>
						</ul>
						<div class="spacer"></div>
						<com:TLabel ID="infoCertif" />
						<a class="bouton float-right" onclick="J('#<%=$this->versionNavigateur->getClientId()%>').hide().fadeIn();J('#panel_resultats').hide().fadeIn();initialiserDiagnosticApplet();" href="#panel_resultats"><span class="left">&nbsp;</span> Lancer le test de configuration de mon poste</a>
						<div class="breaker"></div>
					</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<div id="panel_resultats" class="form-field" style="display:none;">

		<!--Debut Bloc Configuration du poste-->
		<div class="form-field">

			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h3><com:TTranslate>RESULTAT_TEST</com:TTranslate></h3>

				<com:TLabel>
					<div id="div_succes" class="bloc-message form-bloc-conf msg-confirmation" style="display:none">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
						<div class="content">

							<div class="message bloc-700">
								<!--Debut Message -->
								<span><com:TLabel ID="textConfigurationOk"/></span>
								<div class="spacer"></div>
								<div class="float-right"><com:THyperLink ID="imageConsultationTest1" NavigateUrl="index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons&orgTest" CssClass="bouton-arrow-long-120" Attributes.title="<%=Prado::localize('CONSULTATION_DE_TEST')%>"><%=Prado::localize('CONSULTATION_DE_TEST')%></com:THyperLink></div>
								<!--Fin Message-->

							</div>
							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</div>
					<div id="div_echec" style="display:none" class="bloc-message form-bloc-conf msg-erreur">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
						<div class="content">

							<div class="message bloc-700">
								<!--Debut Message -->
								<com:TPanel Visible="<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])!='agent'%>"><%=str_replace("#HtmlAnchorClose", "</a>", str_replace("#HtmlAnchorOpen", "<a href=\"index.php?page=Entreprise.PrerequisTechniques&calledFrom=". Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])."#rubrique_2\">", Prado::localize('CONFIGURATION_NOK')));%></com:TPanel>
								<com:TPanel Visible="<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])=='agent'%>"><%=str_replace("#HtmlAnchorClose", "</a>", str_replace("#HtmlAnchorOpen", "<a href=\"index.php?page=Agent.PrerequisTechniques&calledFrom=". Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])."#rubrique_2\">", Prado::localize('CONFIGURATION_NOK_AGENT')));%></com:TPanel>
								<div class="spacer"></div>
								<com:TPanel CssClass="float-right"  Visible="<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])=='entreprise'%>">
									<com:THyperLink ID="imageConsultationTest2" NavigateUrl="index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons&orgTest" CssClass="bouton-arrow-long-120" Attributes.title="<%=Prado::localize('CONSULTATION_DE_TEST')%>">
										<%=Prado::localize('CONSULTATION_DE_TEST')%>
									</com:THyperLink>
								</com:TPanel>
								<!--Fin Message-->

							</div>
							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</div>
					<input id="succesApplet" type="hidden" />
					<com:TLabel id="initialiserDiagnosticApplet"/>
				</com:TLabel>


				<h2 class="indent-5"><com:TTranslate>OS_ET_NAVIGATEUR</com:TTranslate></h2>
                <div class="table-bloc">
                    <table class="table-results" summary="">
                        <tr class="on" id="identifiantSystemeExploitation">
                            <td class="col-630">
                            	<span class="indent-20">
                            		- <%=Prado::localize('SYSTEME_EXPLOITATION')%> 
                             		<span ID="identifiantSystemeExploitationMessage" />
                             	</span>
                            </td>

                            <td class="actions-small">
                            	<span>
										<img id="identifiantSystemeExploitationImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
                            	</span>
                            </td>
                            <td class="col-100">&nbsp;</td>
                       </tr>
                       <tr>
                            <td class="col-630">
	                            <span class="indent-20">
	                            	- <%=Prado::localize('NAVIGATEUR')%> :
									<com:TLabel id="versionNavigateur" style="display:none"/>
								</span>
							</td>
                            <td class="actions-small"><span><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif" alt="<%=Prado::localize('COMPATIBLE')%>" title="<%=Prado::localize('COMPATIBLE')%>" /></span></td>
                            <td class="col-100">&nbsp;</td>
                       </tr>
                    </table>

				</div>
				<div class="spacer-small"></div>
				<h2 class="indent-5"><com:TTranslate>ENVIRONNEMENT_JAVA</com:TTranslate></h2>
				<div class="table-bloc">
                    <table class="table-results" summary="">
						<tr class="on">
							<td class="col-630"><span class="indent-20">- <com:TTranslate>PRESENCE_ENVIRONNEMENT_JAVA</com:TTranslate> </span></td>
							<td class="actions-small">
								<span><img id='pres_env_java_encours' src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" /></span>
								<script language="javascript">
									if (navigator.javaEnabled()) {
										document.getElementById('pres_env_java_encours').style.display="none";
										document.write("<span><img src=\"<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif\" alt=\"<%=Prado::localize('COMPATIBLE')%>\" title=\"<%=Prado::localize('COMPATIBLE')%>\" /></span>");
									} else {
										document.getElementById('pres_env_java_encours').style.display="none";
										document.write("<span><img src=\"<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif\" alt=\"<%=Prado::localize('DEFINE_NON')%>\" title=\"<%=Prado::localize('DEFINE_NON')%>\" /></span>");
									}

								</script>
							</td>
							<script language="javascript">
								if (navigator.javaEnabled()) {
									document.write("<td class=\"col-100\">&nbsp;</td>");
								} else {
									document.write("<td class=\"col-100\"><span><a href=\"index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_6\" target=\"_blank\" class=\"infos-plus-error\"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a></span></td>");
								}
							</script>
						</tr>
						<tr id="identifiantJavaVersionNumero">
							<td class="col-630">
							<span class="indent-20" id="presEnvPres">- <com:TTranslate>PRESENCE_ENVIRONNEMENT_PRESENT</com:TTranslate> 
								<span id="identifiantJavaVersionNumeroMessage"></span>
							</span>
							</td>
							<td class="actions-small" >
								<img id="identifiantJavaVersionNumeroImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
							</td>
			                <td class="col-100">
								<span  ID="identifiantJavaVersionNumeroOK"  style="display:none">
									&nbsp;
								</span>
								<span  ID="identifiantJavaVersionNumeroNotOK" style="display:none" >
									<a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_6" target="_blank" class="infos-plus-error"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a>
								</span>
							</td>
						</tr>
						<tr class="on" id="identifiantJavaVersionBit" >
							<td>
								<span class="indent-20" id="version32BitsEnvJava">- <com:TTranslate>VERSION_32_BITS_ENV_JAVA</com:TTranslate>
									<span id="identifiantJavaVersionBitMessage"></span>
								</span>
							</td>
							<td class="actions-small">
								<img id="identifiantJavaVersionBitImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
							</td>
							<td class="col-100">
								<span  ID="identifiantJavaVersionBitOK"  style="display:none">
									&nbsp;
								</span>
								<span  ID="identifiantJavaVersionBitNotOK" style="display:none" >
									<a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_6" target="_blank" class="infos-plus-error"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a>
								</span>
							</td>
						</tr>
                    </table>
                </div>
				<div class="spacer-small"></div>

				<h2 class="indent-5"><com:TTranslate>APPLET_DEMARREE</com:TTranslate></h2>
                <div class="table-bloc">
                    <table class="table-results" summary="">
						<tr class="on" id="identifiantAppletDemarre" >
							<td class="col-630">
								<span class="indent-20">- <com:TTranslate>LANCEMENT_APPLET_SIGNATURE</com:TTranslate>
									<span id="identifiantAppletDemarreMessage"></span>
								</span>
							</td>
							<td class="actions-small">
								<img id="identifiantAppletDemarreImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
							</td>

							<td class="col-100">
								<span  ID="identifiantAppletDemarreOK"  style="display:none">
									&nbsp;
								</span>
								<span  ID="identifiantAppletDemarreNotOK" style="display:none">
									<a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_1" target="_blank" class="infos-plus-error"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a>
								</span>
							</td>
						</tr>

                    </table>
                </div>
				<div class="spacer-small"></div>
				<h2 class="indent-5"><com:TTranslate>CAPACITE_CRYPTOGRAPHIQUE</com:TTranslate></h2>
                <div class="table-bloc">
                    <table class="table-results" summary="">
						<tr class="on" id="identifiantTestChiffrement" >
							<td class="col-630" >
								<span class="indent-20">- <com:TTranslate>TEST_CHIFFREMENT</com:TTranslate>
									<span id="identifiantTestChiffrementMessage"></span>
								</span>
							</td>
							<td class="actions-small">
								<img id="identifiantTestChiffrementImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
							</td>

								<td class="col-100">
									<span  ID="identifiantTestChiffrementOK" style="display:none">
										&nbsp;
									</span>
									<span  ID="identifiantTestChiffrementNotOK" style="display:none" >
										<a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_6" target="_blank" class="infos-plus-error"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a>
									</span>
								</td>
						</tr>
						<tr  id="identifiantTestAccesMagasin">
							<td class="col-630">
								<span class="indent-20">- <com:TTranslate>TEST_ACCES_MAGASIN</com:TTranslate>
									<span id="identifiantTestAccesMagasinMessage"></span>
								</span>
							</td>
							<td class="actions-small">
								<img id="identifiantTestAccesMagasinImg" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader-small.gif" alt="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>" title="<%=Prado::localize('EN_COURS_DE_VERIFICATION')%>"/>
							</td>

							<td class="col-100">
								<span  ID="identifiantTestAccesMagasinOK" style="display:none">
									&nbsp;
								</span>
								<span  ID="identifiantTestAccesMagasinNotOK" style="display:none"  >
									<a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ConditionsUtilisation&calledFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['callFrom'])%>#rubrique_2_paragraphe_6" target="_blank" class="infos-plus-error"><com:TTranslate>DEFINE_EN_SAVOIR_PLUS</com:TTranslate></a>
								</span>
							</td>
						</tr>
                    </table>
                </div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>

	    <com:TActivePanel id="panelDiag">
	    </com:TActivePanel>
        <com:TActiveButton ID="boutonSubmitDiag" Attributes.style="display:none" onCallBack="saveTraceDiagInscrit"/>
		</div>


        <com:TActivePanel id="PanelButtons">
	        <com:TActiveButton ID="boutonSubmitSucces"  ActiveControl.CallbackParameter="1" Attributes.style="display:none" onCallBack="Page.saveTraceDiagInscrit"/>
	        <com:TActiveButton ID="boutonSubmitEchec"  ActiveControl.CallbackParameter="0" Attributes.style="display:none" onCallBack="Page.saveTraceDiagInscrit"/>
        </com:TActivePanel>
		<com:THiddenField id="resDiag"/>
		<com:THiddenField id="logApplet"/>
		<div class="link-line">
			<a href="javascript:window.history.back();" class="bouton-retour" title="<%=Prado::localize('TEXT_RETOUR')%>"><com:TTranslate>TEXT_RETOUR</com:TTranslate></a>
		</div>
	    <!--Fin Bloc Resultat de la configuration-->
	</div>