<!--Debut Bloc Liste documents -->
	<div class="table-bloc">
		<com:TRepeater ID="piecesCoffreFort" EnableViewState="true" >
			<prop:HeaderTemplate>
				<table class="table-results" summary="Liste des documents rattachés à l'entreprise">
					<thead>
						<tr>
							<th class="top" colspan="4"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-400" id="docName"><com:TTranslate>TEXT_DOCUMENT</com:TTranslate></th>
							<th class="col-90" id="tailleDoc"><com:TTranslate>TEXT_TAILLE</com:TTranslate></th>

							<th class="col-90" id="dateDocument"><com:TTranslate>DEFINE_TEXT_DATE_FIN_VALIDITE</com:TTranslate></th>
							<th class="actions" id="visualiserDoc"><%#trim(Prado::localize('TEXT_VISUALISER'))%></th>
						</tr>
					</thead>
			</prop:HeaderTemplate>
			<prop:ItemTemplate>
				<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
					<td class="col-400" headers="docName">
					<%#$this->Data->getNom()%>
					<br />
					<%#$this->Data->getIntituleJustificatif()%>
					</td>
					<td class="col-90" headers="tailleDoc"><%#Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1000)%></td>

					<td class="col-90" headers="dateDocument">
						<span class="<%#(strcmp($this->Data->getDateFinValidite(), date('Y-m-d H:i:s')) <= 0 ) ? 'time-red' :'time-green' %>">
							<%#Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data->getDateFinValidite())%>
						</span>
					</td>
					<td class="actions" headers="visualiserDoc">
						<com:TLinkButton onCommand="SourceTemplateControl.downloadPiece" CommandParameter="<%#$this->Data->getIntituleJustificatif()%>" CommandName="<%#$this->Data->getJustificatif()%>"><com:PictoDetail ParentPage="CoffreFort"/></com:TLinkButton>
					</td>
				</tr>
			</prop:ItemTemplate>
			<prop:FooterTemplate>
				</table>
			</prop:FooterTemplate>
		</com:TRepeater>
		<h2><com:TLabel ID="aucunePiece" /></h2>
	</div>
<!--Fin Bloc Liste documents -->