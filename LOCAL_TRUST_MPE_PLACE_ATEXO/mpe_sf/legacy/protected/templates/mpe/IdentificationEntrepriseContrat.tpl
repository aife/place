<com:TPanel visible="false">
    <com:TLabel id="region"/>
    <com:TLabel id="province"/>
    <com:TLabel id="telephone"/>
    <com:TLabel id="telephone2"/>
    <com:TLabel id="telephone3"/>
    <com:TLabel id="panelTelEntreprise2"/>
    <com:TLabel id="panelTelEntreprise3"/>
    <com:TLabel id="fax"/>
    <com:TLabel id="siretSiege"/>
    <com:TLabel id="idNational"/>
    <com:TLabel id="idtaxeprof"/>
    <com:TLabel id="numRc"/>
    <com:TLabel id="villeRc"/>
    <com:TLabel id="idcnss"/>
    <com:TLabel id="idcapital"/>
    <com:TLabel id="ifu"/>
    <com:TLabel id="panelCnss"/>
    <com:TLabel id="panelNumRc"/>
    <com:TLabel id="panelIdTaxProf"/>
    <com:TLabel id="panelRegion"/>
    <com:TLabel id="panelProvince"/>
    <com:TLabel id="panelSiren"/>
    <com:TLabel id="panelIdNational"/>
    <com:TLabel id="panelDomaineActivite"/>
    <com:TLabel id="panelCapital"/>
    <com:TLabel id="panelIfu"/>
    <com:THiddenField id="idsDomaines"/>
    <com:TLabel id="adresse"/>
    <com:TLabel id="adresseSuite"/>
    -
    <com:TLabel id="codePostal"/>
    <com:TLabel id="ville"/>
    -
    <com:TLabel id="pays"/>
    <com:AtexoReferentiel ID="atexoReferentielCodeNace" cas="cas9"/>
</com:TPanel>

<com:TActivePanel cssClass="form-field" ID="panelInfoEntreprise">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2>
            <com:TTranslate>INFORMATIONS_SUR_ATTRIBUTAIRE</com:TTranslate>
            <com:TActiveHyperLink visible="<%=$this->Page->isEditAttributaireAvailable()%>"
                                  Attributes.onclick="openModal('modal-modification-attributaire','popup-moyen1',this);">
                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('TEXT_MODIFICATION_ATTRIBUTAIRES')%>" style="cursor:pointer;"/>
            </com:TActiveHyperLink>
        </h2>

        <div class="panel">
            <div class="line">
                <div class="intitule-150">
                    <com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate>
                    :
                </div>
                <div class="content-bloc content-bloc bloc-570"><strong>
                        <com:TActiveLabel id="idRaisonSociale"/>
                        <com:TActiveLabel id="paysSiren"/>
                    </strong></div>
            </div>
            <div class="line">
                <div class="intitule-150">
                    <com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate>
                    :
                </div>
                <div class="content-bloc content-bloc bloc-570"><span
                            id="ctl0_CONTENU_PAGE_attributaire_infoAttributaire_siegeSocial"><img alt=""
                                                                                                  src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-siege-social.png"><com:TActiveLabel
                                id="siegeSocial"/></span></div>
            </div>
            <com:TPanel ID="panelCodeApe" cssclass="line">
                <div class="intitule-150">
                    <com:TTranslate>TEXT_NAF</com:TTranslate>
                    :
                </div>
                <div class="content-bloc content-bloc bloc-570"><span
                            id="ctl0_CONTENU_PAGE_attributaire_infoAttributaire_codeAPE"><com:TActiveLabel
                                id="codeApeNafNace"/> </span></div>
            </com:TPanel>
            <div class="line">
                <div class="intitule-150">
                    <com:TTranslate>TEXT_FJ</com:TTranslate>
                    :
                </div>
                <div class="content-bloc"><span id="ctl0_CONTENU_PAGE_attributaire_infoAttributaire_formeJuridique"><com:TActiveLabel
                                id="formeJuridique"/></span></div>
            </div>
            <div class="line">
                <div class="intitule-150">
                    <com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate>
                    :
                </div>
                <div class="content-bloc"><span
                            id="ctl0_CONTENU_PAGE_attributaire_infoAttributaire_categorieEntreprise"><com:TActiveLabel
                                id="categorieEntreprise"/></span></div>
            </div>
            <com:TPanel id="panelDescription" cssclass="line">
                <div class="intitule-150">
                    <com:TTranslate>DEFINE_DESCRIPTION</com:TTranslate>
                    :
                </div>
                <div class="content-bloc bloc-570">
                    <com:TActiveLabel id="description"/>
                </div>

            </com:TPanel>
            <com:TPanel id="panelSiteInternet" cssclass="line">
                <div class="intitule-150">
                    <com:TTranslate>TEXT_DEFINE_SITE_INTERNET</com:TTranslate>
                    :
                </div>
                <div class="content-bloc bloc-570">
                    <com:TActiveHyperLink id="siteInternet" attributes.target="_blank" cssclass="lien-ext">
                        <com:TActiveLabel id="siteInternetLabel"/>
                    </com:TActiveHyperLink>
                </div>
            </com:TPanel>
            <com:AtexoSoumissionnaire id="atexoSoumissionnaire" visible="false"/>
            <div class="breaker"></div>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>


<com:TPanel id="panelModifierAttributaire" cssClass="modal-modification-attributaire" style="display:none" visible="<%=$this->Page->isEditAttributaireAvailable()%>">
    <h1>
        <com:TTranslate>MODIFIER_UN_ATTRIBUTAIRE</com:TTranslate>
    </h1>
    <div id="divValidationSiretNotNull" class="bloc-message form-bloc-conf msg-erreur erreur-danger"
         style="display:none;">

        <div role="alert">
            <div class="h4">
                <i class="fa fa-exclamation-triangle m-r-1 hidden"></i>
                <com:TTranslate>DEFINE_ERREUR_SAISIE</com:TTranslate>
            </div>
            <div class="spacer-small"></div>
            <div>
                <com:TTranslate>DEFINE_VERIFIER_CHAMPS_OBLIGATOIRES</com:TTranslate>
                <span id="ctl0_CONTENU_PAGE_listeAttributaires_ctl4_validationSummary"
                      style="color: rgb(217, 83, 79); visibility: visible;">
                    <ul>
                        <li> <com:TTranslate>SIREN_SIRET</com:TTranslate></li>
                    </ul>
                </span>
                <span id="ctl0_CONTENU_PAGE_listeAttributaires_ctl4_serverError"></span>
            </div>
        </div>
    </div>


    <div id="divValidationSiretNotValid" class="bloc-message form-bloc-conf msg-erreur erreur-danger"
         style="display:none;">

        <div class="alert alert-danger" role="alert">
            <div class="h4">
                <i class="fa fa-exclamation-triangle m-r-1 hidden"></i>
                <com:TTranslate>SIRET_NON_FORMATTE</com:TTranslate>
            </div>
        </div>
    </div>

    <div id="divErrorSGMAP" class="bloc-message form-bloc-conf msg-erreur erreur-danger" style="display:none;">

        <div class="alert alert-danger" role="alert">
            <div class="h4">
                <i class="fa fa-exclamation-triangle m-r-1 hidden"></i>
                <com:TTranslate>AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE</com:TTranslate>
            </div>
        </div>
    </div>


    <div class="content">
        <div style="display: block;" class="clear-both" id="etablieFrance">
            <div class="form-bloc">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <div id="blocEntreprise_panelSiren">
                        <div class="intitule-110 indent-40">
                            <div class="float-left">
                                <label for="siren_siret">
                                    <com:TTranslate>SIREN_SIRET</com:TTranslate>
                                </label>
                                <span class="champ-oblig">*</span>:
                            </div>
                        </div>
                        <com:TActiveTextBox id="nepassupprimer" style="display:none"/>
                        <com:TActiveTextBox id="siren" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>"
                                            MaxLength="9"
                                            CssClass="siren"/>
                        <com:TActiveTextBox id="siret" Attributes.title="<%=Prado::localize('TEXT_SIRET')%>"
                                            MaxLength="5"
                                            CssClass="siret"/>
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif"
                             id="imgValidationSIRET"
                             style="display:none;">

                    </div>
                </div>
                <div class="form-bloc bloc-contrat margin-0"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
        </div>
    </div>
    <!--Debut line boutons-->
    <div class="boutons-line">
        <div class="left">
            <com:TActiveButton
                    id="annuler"
                    Text="<%=Prado::localize('DEFINE_ANNULER')%>"
                    CssClass="bouton-moyen"
                    onCommand="Page.annulerAttributaire"
                    CommandParameter=""
            >
            </com:TActiveButton>
        </div>
        <div class="right">
            <com:TActiveButton
                    id="msaveEtab"
                    Text="<%=Prado::localize('TEXT_VALIDER')%>"
                    CssClass="bouton-moyen"
                    onCommand="Page.saveAttributaire"
                    CommandParameter=""
            >
            </com:TActiveButton>
        </div>


    </div>
    <!--Fin line boutons-->
    </div>

</com:TPanel>


