<!--Debut Bloc Dirigeants de l'entreprise-->
	<div class="toggle-panel">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'dirigeantsEntreprise');"><com:TTranslate>DEFINE_DIRIGEANTS_ENTREPRISE</com:TTranslate></div>
			<div class="panel" style="display:none;" id="dirigeantsEntreprise">
				<div class="table-bloc">
				<com:TRepeater id="repeaterDirigeants" 
				       AllowPaging="true"
        			   PageSize="10"
          			   EnableViewState="true"
          			   DataKeyField="Id" 
          			   >
				<prop:HeaderTemplate>
					<table class="table-results" summary="<%=Prado::localize('DEFINE_DIRIGEANTS_ENTREPRISE')%>">
						<caption><com:TTranslate>DEFINE_DIRIGEANTS_ENTREPRISE</com:TTranslate></caption>
						<thead>
							<tr>
								<th class="top" colspan="7"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-250" id="nomDirigeant"><com:TTranslate>TEXT_NOM</com:TTranslate></th>
								<th class="col-250" id="prenomDirigeant"><com:TTranslate>DEFINE_PRENOM</com:TTranslate></th>
								<th class="col-250" id="qualiteDirigeant"><com:TTranslate>QUALITE</com:TTranslate></th>
								<th class="col-250" id="coordonneesDirigeant"><com:TTranslate>DEFINE_COORDONNEES</com:TTranslate></th>
							</tr>
						</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
						<tr class="<%# ($this->ItemIndex%2==0) ? '' : 'on'%>">
							<td class="col-250" headers="nomDirigeant"><%#$this->Data->getNom()%></td>
							<td class="col-250" headers="prenomDirigeant"><%#$this->Data->getPrenom()%></td>
							<td class="col-250" headers="qualiteDirigeant"><%#$this->Data->getQualite()%></td>
							<td class="col-250" headers="coordonneesDirigeant">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" alt="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" /><%#$this->Data->getEmail()%><br />
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" alt="<%=Prado::localize('TEXT_TEL')%>" title="<%=Prado::localize('TEXT_TEL')%>" /> <%#$this->Data->getTelephone()%><br />
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" alt="<%=Prado::localize('TEXT_TELECOPIEUR')%>" title="<%=Prado::localize('TEXT_TELECOPIEUR')%>" /> <%#$this->Data->getFax()%>
							</td>
						</tr>
				</prop:ItemTemplate>
				<prop:FooterTemplate>
					</table>					
				</prop:FooterTemplate>								 
				</com:TRepeater>
				</div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Dirigeants de l'entreprise-->