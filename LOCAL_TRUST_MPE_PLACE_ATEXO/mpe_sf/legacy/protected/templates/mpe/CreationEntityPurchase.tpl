<div class="main-part" id="main-part">
	<div class="breadcrumbs"><a href="#"><com:TTranslate>DEFINE_ADMINISTRATION_PMI</com:TTranslate></a>&gt; <a href="#"><com:TTranslate>DEFINE_GESTION_EA_PMI</com:TTranslate></a> &gt; <com:TTranslate>DEFINE_CREATION_EA_PMI</com:TTranslate></div>
	<com:AtexoValidationSummary  ValidationGroup="validateInfosEA" />
	<com:TActivePanel ID="panelMessageAvertissement"  >
		<com:PanelMessageAvertissement ID="messageAvertissement"  message="<%=Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE')%>" />
	</com:TActivePanel>
	<!--Debut Bloc Infos Entite d'achat-->
	<div class="form-field" style="display:block;">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<h3><com:TTranslate>DEFINE_HEADER_IDENTIFICATION_EA_PMI</com:TTranslate></h3>
			<!--Debut Ligne Libellé-->
			<div class="line">
				<div class="intitule-150">
					<label for="<%=$this->libelle->getClientId()%>"><com:TTranslate>DEFINE_LIBELLE_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :
				</div>
			  <com:TTextBox id="libelle"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_LIBELLE_EA_PMI')%>"/>

			   <com:TRequiredFieldValidator
								ControlToValidate="libelle"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_LIBELLE_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<!--Fin Ligne Libellé-->
			<!--Début Ligne Libellé traduit-->
			<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
				<div class="intitule-150">
						<label for="<%=$this->libelle->getClientId()%>"><com:TTranslate>DEFINE_LIBELLE_EA_PMI_AR</com:TTranslate></label><span class="champ-oblig">*</span> :
				</div>
				<com:TTextBox id="libelle_ar"
								CssClass="input-500"
								Attributes.title="<%=Prado::localize('DEFINE_LIBELLE_EA_PMI_AR')%>"/>
				<com:TRequiredFieldValidator
								ID="validateur_libelle_ar"
								ControlToValidate="libelle_ar"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_LIBELLE_EA_PMI_AR')%>"
								EnableClientScript="true"
								Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</com:TPanel>
			<!--Fin Ligne Libellé traduit-->
			<!--Debut Ligne Sigle-->
			<div class="line">
				<div class="intitule-150">
					<label for="<%=$this->sigle->getClientId()%>"><com:TTranslate>DEFINE_TEXT_SIGLE_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :
				</div>
				<com:TTextBox id="sigle"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_SIGLE_EA_PMI')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="sigle"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_TEXT_SIGLE_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<!--Fin Ligne Sigle-->
			<!--Debut Ligne Entité de rattachement-->
			<div class="line">
				<div class="intitule-150">
					<label for="<%=$this->entiteRattachement->getClientId()%>"><com:TTranslate>DEFINE_ENTITE_RATTACHEMENT_EA_PMI</com:TTranslate></label> :
				</div>
				<com:TPanel id="panellistentite" Visible="true">
					<com:TDropDownList
							id="entiteRattachement"
							CssClass="select-500"
							Attributes.title="<%=Prado::localize('DEFINE_ENTITE_RATTACHEMENT_EA_PMI')%>"
					/>
				</com:TPanel>
				<com:TPanel id="panellibelleentite" Visible="false">
					<com:TLabel
						id="entiteRattachementlabel"
						CssClass="select-500"
						/>
				</com:TPanel>
			</div>
			<!--Fin Ligne Entité de rattachement-->
			<!--Debut Ligne Forme juridique-->
			<div class="line">
				<div class="intitule-150">
					<label for="<%=$this->formeJuridique->getClientId()%>"><com:TTranslate>TEXT_CATEGORIE_JURIDIQUE</com:TTranslate></label><span class="champ-oblig">*</span> :
				</div>
				<com:TActiveTextBox id="formeJuridique"
							  CssClass="input-500"
							  Readonly="true"
									enabled="false"
							  Attributes.title="<%=Prado::localize('TEXT_CATEGORIE_JURIDIQUE')%>"/>
				<com:TRequiredFieldValidator
						ControlToValidate="formeJuridique"
						ValidationGroup="validateInfosEA"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_CATEGORIE_JURIDIQUE')%>"
						EnableClientScript="true"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TRequiredFieldValidator>
				<com:TActiveTextBox id="formeJuridiqueCode"
						CssClass="input-500"
						Display="None"
				/>
			</div>
			<!--Fin Ligne Forme juridique-->
			<div class="spacer"></div>
			<!--Debut Ligne Adresse-->
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->adresse->getClientId()%>"><com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="adresse"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="adresse"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
				<div class="intitule-150"><label for="<%=$this->adresse->getClientId()%>"><com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI_AR</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="adresse_ar"
							CssClass="moyen"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI_AR')%>"/>
				<com:TRequiredFieldValidator
								ID="validateur_adresse_ar"
								ControlToValidate="adresse_ar"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI_AR')%>"
								EnableClientScript="true"
								Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</com:TPanel>
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->adressesuite->getClientId()%>"><com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI</com:TTranslate></label> :</div>
				<com:TTextBox id="adressesuite"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI')%>"/>
			</div>
			<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
				<div class="intitule-150"><label for="<%=$this->adressesuite->getClientId()%>"><com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI_AR</com:TTranslate></label> :</div>
				<com:TTextBox id="adressesuite_ar"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI_AR')%>"/>
			</com:TPanel>

			<div class="line">
				<div class="intitule-150"><label for="<%=$this->cp->getClientId()%>"><com:TTranslate>TEXT_CP</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="cp"
							CssClass="cp"
							Attributes.title="<%=Prado::localize('TEXT_CP')%>"
							Attributes.maxlength="5"
							/>
				<com:TRequiredFieldValidator
								ControlToValidate="cp"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_CP')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			  <com:TDataTypeValidator
								ControlToValidate="cp"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								DataType="Integer"
								ErrorMessage="<%=Prado::localize('TEXT_CP')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			 </com:TDataTypeValidator>
			</div>
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->ville->getClientId()%>"><com:TTranslate>TEXT_VILLE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="ville"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('TEXT_VILLE')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="ville"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_VILLE')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
			<div Class="intitule-150"><label for="<%=$this->ville->getClientId()%>"><com:TTranslate>TEXT_VILLE_AR</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="ville_ar"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('TEXT_VILLE_AR')%>"/>
				<com:TRequiredFieldValidator
								ID="validateur_ville_ar"
								ControlToValidate="ville_ar"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_VILLE_AR')%>"
								EnableClientScript="true"
								Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</com:TPanel>
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->ville->getClientId()%>"><com:TTranslate>TEXT_PAYS</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
			<com:DropDownListCountries id="listPays" TypeAffichage="withoutCode" WithaoutFrance="" Attributes.title="<%=Prado::localize('TEXT_PAYS')%>" CssClass="select-500"/>

				<com:TCustomValidator
					ValidationGroup="validateInfosEA"
					ControlToValidate="listPays"
					ClientValidationFunction="controlValidationListPays"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('TEXT_PAYS')%>"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					EnableClientScript="true" >
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
					</prop:ClientSide.OnValidationError>
				 </com:TCustomValidator>
			</div>

			<!--Fin Ligne Type de procedure-->
			<!--Debut Ligne Contact-->
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->tel->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TEL_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="tel"
							CssClass="tel"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_TEL_EA_PMI')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="tel"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_TEXT_TEL_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->fax->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TELCOPIE_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="fax"
							CssClass="tel"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_TELCOPIE_EA_PMI')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="fax"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_TEXT_TELCOPIE_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			</div>
			<div class="line">
				<div class="intitule-150"><label for="<%=$this->mail->getClientId()%>"><com:TTranslate>DEFINE_EMAIL_EA_PMI</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox id="mail"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_EMAIL_EA_PMI')%>"/>
				<com:TRequiredFieldValidator
								ControlToValidate="mail"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_EMAIL_EA_PMI')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
			  <com:TCustomValidator
				ClientValidationFunction="validatorEmailNonObligatoire"
				ID="validateur1"
				ControlToValidate="mail"
				ValidationGroup="validateInfosEA"
				Display="Dynamic"
				EnableClientScript="true"
				ErrorMessage="<%=Prado::localize('EMAIL_FORMAT_ERROR')%>"
				Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
			>
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
				</prop:ClientSide.OnValidationError>
			  </com:TCustomValidator>

			</div>
				<com:TPanel ID="panelSiret" cssClass="line">
					<div class="intitule-150"><div class="float-left"><label for="siren"><label for="<%=$this->fax->getClientId()%>"><com:TTRanslate>TEXT_SIRET</com:TTRanslate></label><span class="champ-oblig">*</span></div>
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" /> <span class="indent-30">:</span>
					 </div>
					<com:TTextBox ID="siren" MaxLength="9" 	Width="100px"
								Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" CssClass="siren" />
					<com:TTextBox ID="siret" MaxLength="5" Width="60px"
								  Attributes.title="<%=Prado::localize('TEXT_SIRET')%>" CssClass="siret" />
					<com:TRequiredFieldValidator
								ControlToValidate="siren"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_SIREN')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
				  </com:TRequiredFieldValidator>
					<com:TRequiredFieldValidator
								ControlToValidate="siret"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_SIRET')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
				  </com:TRequiredFieldValidator>
					<com:TCustomValidator
						ValidationGroup="validateInfosEA"
						ControlToValidate="siren"
						ClientValidationFunction="validateSirenForService"
						ErrorMessage="<%=Prado::localize('SIRET_INVALIDE')%>"
						Display="Dynamic"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
						 document.getElementById('divValidationSummary').style.display='';
						</prop:ClientSide.OnValidationError>
				   </com:TCustomValidator>
			</com:TPanel>
			<com:TPanel ID="panelCodeAcheteur" cssClass="line">
				<div class="intitule-150">
					<div class="float-left">
						<label for="codeAcheteur"><com:TTranslate>CODE_ACHETEUR</com:TTranslate></label><span class="champ-oblig">*</span> :
					</div>
				</div>
				<com:TTextBox ID="codeAcheteur"  CssClass="siren"  />
				<com:TRequiredFieldValidator
								ControlToValidate="codeAcheteur"
								ValidationGroup="validateInfosEA"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('CODE_ACHETEUR')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
				<com:TCustomValidator
							ControlToValidate="codeAcheteur"
							ValidationGroup="validateInfosEA"
							Display="Dynamic"
							EnableClientScript="true"
							ID="validatorCodeAcheteur"
							onServerValidate="Page.verifyCodeAcheteur"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				</com:TCustomValidator>
			</com:TPanel>
			<com:TPanel ID="affichageService" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GestionMandataire')) ? true:false%>">
			<div class="intitule-150"><label for="affichageService"><com:TTranslate>AFFICHAGE_SERVICE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
			<div class="content-bloc bloc-600">
				<div class="line">
						   <com:TActiveRadioButton
						   GroupName="affichageService"
						   Checked="true"
						   ID="affichageServiceOui"
						   Text="<%=Prado::localize('FCSP_OUI')%>"
						   /></div>
				<div class="line">
						   <com:TActiveRadioButton
							GroupName="affichageService"
							ID="affichageServiceNon"
							Text="<%=Prado::localize('TEXT_NON')%>"
							/>
							</div>
			</div>
			</com:TPanel>
			<!--Fin Ligne Contact-->
			<!-- Début bloc gestion fuseau horaire -->
			<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())) ? true:false%>">
				<div class="line">
					<div class="intitule-bloc bloc-150"><label for="activationFuseauHoraire" class="float-left"><com:TTranslate>DEFINE_UTILISATION_FUSEAU_HORAIRE</com:TTranslate> :</div>
					<div class="intitule-80 float-left">
						<com:TRadioButton
							id="activerFuseauHoraireOui"
							GroupName="activationFuseauHoraire"
							Text="<%=Prado::localize('DEFINE_OUI')%>"
							cssClass="radio"
							AutoPostBack = "true"
						/>
					</div>
					<div class="intitule-80 float-left">
						<com:TRadioButton
							id="activerFuseauHoraireNon"
							GroupName="activationFuseauHoraire"
							Text="<%=Prado::localize('DEFINE_NON')%>"
							cssClass="radio"
							AutoPostBack = "true"
						/>
					</div>
				</div>
				<com:TActivePanel id="panelFuseauHoraire" style="display:<%=($this->activerFuseauHoraireNon->checked)? 'none':'block'%>" >
					<div class="line">
						<div class="intitule-bloc bloc-150"><label for="decalageParRapportGmt"><com:TTranslate>DEFINE_DECALAGE_PAR_RAPPORT_GMT</com:TTranslate></label><span id="etoileDecalageGmt" class="champ-oblig">*</span> :</div>
						<com:TDropDownList
							id="decalageParRapportGmt"
							Attributes.title="<%=Prado::localize('DEFINE_DECALAGE_PAR_RAPPORT_GMT')%>"
							CssClass="select-50"
						/>
						<com:TCustomValidator
							ValidationGroup="validateInfosEA"
							ControlToValidate="decalageParRapportGmt"
							id="validateurDecalageParRapportGmt"
							Display="Dynamic"
							Enabled="<%=($this->activerFuseauHoraireOui->Checked)? true:false%>"
							ClientValidationFunction="validerSelectionDecalageParRapportGmt"
							ErrorMessage="<%=Prado::localize('DEFINE_DECALAGE_PAR_RAPPORT_GMT')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
							EnableClientScript="true" >
							<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
							</prop:ClientSide.OnValidationError>
						 </com:TCustomValidator>
					</div>
					<div class="line">
						<div class="intitule-150"><label for="LieuResidence"><com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate></label><span id="etoileLieuResidence" class="champ-oblig">*</span> :</div>
						<com:TTextBox id="LieuResidence"
							CssClass="input-500"
							Attributes.title="<%=Prado::localize('DEFINE_LIEU_RESIDENCE')%>"/>
						<com:TRequiredFieldValidator
							ControlToValidate="LieuResidence"
							id="validateurLieuResidence"
							ValidationGroup="validateInfosEA"
							Display="Dynamic"
							Enabled="<%=($this->activerFuseauHoraireOui->Checked)? true:false%>"
							ErrorMessage="<%=Prado::localize('DEFINE_LIEU_RESIDENCE')%>"
							EnableClientScript="true"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
							</prop:ClientSide.OnValidationError>
					  </com:TRequiredFieldValidator>
					</div>
				</com:TActivePanel>
			</com:TPanel>
			<!-- Fin bloc gestion fuseau horaire -->
			<!-- Début bloc gestion chorus -->
			<com:TPanel id="panelChorus">
				<div class="line">
					<div class="intitule-bloc bloc-150"><label for="activationChorus" class="float-left"><com:TTranslate>DEFINE_UTILISATION_CHORUS</com:TTranslate> :</div>
					<div class="intitule-50 float-left">
						<com:TRadioButton
								id="activerChorusOui"
								GroupName="activationChorus"
								Text="<%=Prado::localize('DEFINE_OUI')%>"
								cssClass="radio"
								AutoPostBack = "false"
						/>
					</div>
					<div class="intitule-50 float-left">
						<com:TRadioButton
								id="activerChorusNon"
								GroupName="activationChorus"
								Text="<%=Prado::localize('DEFINE_NON')%>"
								cssClass="radio"
								AutoPostBack = "false"
						/>
					</div>
				</div>
			</com:TPanel>
			<!-- Fin bloc gestion chorus -->

			<div class="line" id="bloc-id-entite">
				<div class="intitule-150"><label for="<%=$this->idEntite->getClientId()%>"><com:TTranslate>TEXT_ID_ENTITE</com:TTranslate></label> :</div>
				<com:TTextBox id="idEntite" CssClass="input-500" Attributes.title="<%=Prado::localize('TEXT_ID_ENTITE')%>"></com:TTextBox>
			</div>
			<!--Debut bloc Identification Service archive-->
			<div class="spacer"></div>
			<com:TPanel visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('AfficherCodeService'))%>" >
				<h3><com:TTranslate>IDENTIFICATION_SERVICE_ARCHIVES</com:TTranslate></h3>
				<div class="line">
					<div class="intitule-150">
						<div class="float-left"><label for="nomServiceArchive"><com:TTranslate>DEFINE_NOM</com:TTranslate></label></div>
						<img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infoNomServiceArchive')" onmouseover="afficheBulle('infoNomServiceArchive', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
						<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infoNomServiceArchive"><div><com:TTranslate>DESCRIPTION_NOM_SERVICE_ARCHIVES</com:TTranslate></div></div>
						<span class="indent-30">:</span>

					</div>
					<div class="content-bloc bloc-550">
						<com:TTextBox id="nomServiceArchive" attributes.title="<%=Prado::localize('NOM_SERVICE_ARCHIVES')%>" cssclass="input-500"  />
					</div>
				</div>
				<div class="line">
					<div class="intitule-150">
						<div class="float-left">
							<label for="idServiceArchive"><com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate></label>
						</div>
						<img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infoIdServiceArchive')" onmouseover="afficheBulle('infoIdServiceArchive', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
						<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infoIdServiceArchive"><div><com:TTranslate>DESCRIPTION_IDENTIFIANT_SERVICE_ARCHIVES</com:TTranslate></div></div>
						<span class="indent-30">:</span>

					</div>
					<div class="content-bloc bloc-550">
						<com:TTextBox id="idServiceArchive" attributes.title="<%=Prado::localize('IDENTIFICATION_SERVICE_ARCHIVES')%>" cssclass="input-500"  />
					</div>
				</div>
			</com:TPanel>
			<!--Fin bloc Identification Service archive-->
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Debut Line bouton Modifier-->
	<div class="boutons-line">
		<com:THiddenField id="valeurModuleFuseauHoraire" value="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()))? '1':'0'%>"/>
		<com:TButton
			CssClass="bouton-annulation float-left"
			Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>"
			Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>"
			OnClick="RedirectReturn"
			/>
		<com:TButton
			CssClass="bouton-validation float-right"
			Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>"
			Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
			onClick="createEA"
			ValidationGroup="validateInfosEA" />
	</div>
	<!--Fin Line bouton Modifier-->
	<!--Fin Bloc Infos Entite d'achat-->
	<div class="breaker"></div>
</div>
<com:TLabel id="script"/>
<!--Fin colonne droite-->
<div id="infosSiret" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_SIREN</com:TTranslate></div></div>