<div class="form-field">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h3><%=$this->getTitre()%></h3>
        <div class="table-bloc table-bloc-detail">
            <div class="toggle-details-line float-left">
                <a class="collapse-all" href="#">
                    <com:TTranslate>DEFINE_TOUT_AFFICHER</com:TTranslate>
                </a> /
                <a class="toggle-all" href="#">
                    <com:TTranslate>DEFINE_TOUT_CACHER</com:TTranslate>
                </a>
            </div>
            <com:TPanel ID="exportRapportVerifSignatureHaut" CssClass="file-link float-right" >
                <com:TLinkButton OnCommand="Page.exportRapportSignaturePdf" CommandParameter="<%#$this->getEnveloppe()%>" ><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" alt="Télécharger la version PDF - 12Ko" title="Télécharger la version PDF - 12Ko" /></com:TLinkButton>
                <div class="spacer-small"></div>
            </com:TPanel>

            <com:TRepeater ID="repeaterFichiers"   EnableViewState="true" AllowCustomPaging="true">
                <prop:HeaderTemplate>
                    <table class="table-results tableau-detail tableau-detail-pli" summary="<%=Prado::localize('DEFINE_CANDIDATURE_FICHIERS_CONSTITUANT_DOSSIER')%>">
                        <caption><com:TTranslate>FICHIER_CONSTITUANT_DOSSIER</com:TTranslate></caption>
                        <thead>
                        <tr>
                            <th colspan="3" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
                        </tr>
                        <tr>
                            <th class="toggle-col"></th>
                            <th id="cand_fichierSoummissionnaire" class="col-320">
                                <div class="col-info-bulle">
                                    <com:TTranslate>TEXT_FICHIER_ENVOYE_SOUMISSIONNAIRE</com:TTranslate>
                                    <img alt="Info-bulle" class="picto-info" onmouseout="cacheBulleWithJquery(this)" onmouseover="afficheBulleWithJquery(this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">
                                    <div id="infosFichierEnvoyeSoummissionnaire" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();" ><div><com:TTranslate>DEFINE_INFO_BULLE_FICHIER_ENVOYE_SOUMISSIONNAIRE</com:TTranslate></div></div>
                                </div>
                                <div class="spacer-small"></div>
                            </th>
                            <th id="cand_resultatControleSignature"><com:TTranslate>DEFINE_RESULAT_CONTROLE_SIFNATURE</com:TTranslate></th>
                        </tr>
                        </thead>
                </prop:HeaderTemplate>
                <prop:ItemTemplate >
                    <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
                        <td class="toggle-col">
                            <a style="<%#($this->Data->getTypeFichier() != 'SIG' )? '':'display:none'%>" class="title-toggle" onclick="togglePanelClass(this,'<%#$this->SourceTemplateControl->getClassToggle()%>_<%#$this->ItemIndex%>');" href="javascript:void(0);">Voir le détail du pli</a>
                        </td>
                        <td class="col-150" headers="cand_fichierSoummissionnaire">
                            <com:TLabel Visible="<%#($this->Data->getIdBlob() && $this->Page->_enveloppeOuverte)? true:false%>"	>
                                <com:THyperLink Attributes.title="<%#$this->SourceTemplateControl->getLienTitle($this->Data)%>"
                                                NavigateUrl="index.php?page=Agent.downloadFichierEnveloppe&id=<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&idfichier=<%#$this->Data->getIdFichier()%>&enveloppe=<%#$this->Data->getIdEnveloppe()%>">
                                    <%#utf8_encode($this->Data->getNomFichier())%>
                                </com:THyperLink>
                            </com:TLabel>
                            <com:TLabel Visible="<%#($this->Data->getIdBlob() && $this->Page->_enveloppeOuverte)? false:true%>"	>
                                <%#utf8_encode($this->Data->getNomFichier())%>
                            </com:TLabel>
                        </td>
                        <td headers="cand_resultatControleSignature">
                            <com:TPanel CssClass="<%#$this->SourceTemplateControl->getClassCss($this->Data->getResultatValiditeSignature())%>" Visible="<%#($this->Data->getTypeFichier() != 'SIG' )? true:false%>" >
                                <com:PictoStatutSignature ID="signature" NouveauAffichage="true" Statut="<%#$this->Data->getResultatValiditeSignature()%>"/>
                                <com:TLabel id="statutSignatureFichier" Text="<%#$this->SourceTemplateControl->getStatutSignatureFichier($this->Data->getResultatValiditeSignature())%>"/>
                            </com:TPanel>
                        </td>
                    </tr>
                    <tr class="panel-detail <%#$this->SourceTemplateControl->getClassToggle()%>_<%#$this->ItemIndex%>" style="display:none;">
                        <td colspan="3" class="detail-fichier">
                            <com:BlocDetailCertifFichier Visible="<%#($this->Data->getTypeFichier() != 'SIG' )? true:false%>" id='blocFichier' fichier="<%#$this->Data%>" avecSignature="<%#$this->Page->getViewState('avecSignature')%>" typeSignature="<%#$this->Page->getViewState('typeSignature')%>"/>
                        </td>
                    </tr>
                </prop:ItemTemplate>
            </com:TRepeater>
            </table>
            <div class="toggle-details-line float-left">
                <a class="collapse-all" href="#">
                    <com:TTranslate>DEFINE_TOUT_AFFICHER</com:TTranslate>
                </a> /
                <a class="toggle-all" href="#">
                    <com:TTranslate>DEFINE_TOUT_CACHER</com:TTranslate>
                </a>
            </div>
            <com:TPanel ID="exportRapportVerifSignatureBas" CssClass="file-link float-right" >
                <com:TLinkButton OnCommand="Page.exportRapportSignaturePdf" CommandParameter="<%#$this->getEnveloppe()%>" ><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" alt="Télécharger la version PDF - 12Ko" title="Télécharger la version PDF - 12Ko" /></com:TLinkButton>
                <div class="spacer-small"></div>
            </com:TPanel>
        </div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
