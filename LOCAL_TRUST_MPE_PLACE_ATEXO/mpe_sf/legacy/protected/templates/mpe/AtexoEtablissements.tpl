<com:TActivePanel ID="panelEtablissements" cssClass="form-field">
	<div class="section-heading page-header m-t-2">
		<h3 class="h4 m-0"><com:TTranslate>TEXT_ETABLISSEMENT</com:TTranslate>
		</h3>
	</div>
	<div class="clearfix">
		<div id="toggleEtablissements" style="display: block;" class="panel">
			<!--BEGIN ALERT SELECTIONNER ETABLISSEMENT-->
			<com:TPanel cssClass="form-bloc margin-0" Visible="<%=$this->getMode() != 2 %>">
				<div class="alert alert-info" role="alert">
					<span class="fa fa-info-circle m-r-1" aria-hidden="true"></span>
					<com:TLabel id="labeleMsgEtablissement"
								Text="<%=($this->getMode()!= 1)?Prado::Localize('VEUILLEZ_SELECTIONNER_ETABLISSEMENT'):Prado::Localize('TEXT_VOTRE_ETABLISSEMENT_EST_CELUI_COCHE_SI_DESSOUS')%>"/>
				</div>
			</com:TPanel>
			<!--END ALERT SELECTIONNER ETABLISSEMENT-->

			<!--BEGIN TABLE ETABLISSEMENT-->
			<div class="table-bloc">
				<!--BEGIN PARTITIONNEUR ETABLISSEMENT TOP-->
				<div class="clearfix m-t-2 m-r-3">
					<div class="col-md-3">
						<strong>
							<com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>
							<com:TActiveLabel ID="nombreElement"/>
						</strong>
					</div>
					<div class="form-inline col-md-9">
						<div class=" form-group-sm text-right">
							<div class="form-group">
									<label class="m-r-6">
								<com:TDropDownList ID="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" cssClass="form-control">
									<com:TListItem Text="10" Selected="10"/>
									<com:TListItem Text="20" Value="20"/>
									<com:TListItem Text="50" Value="50"/>
									<com:TListItem Text="100" Value="100"/>
									<com:TListItem Text="500" Value="500"/>
								</com:TDropDownList>
									</label>
							</div>
							<div class="form-group page-result-align">
								<strong>
										<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
								</strong>
							</div>
							<div class="form-group">
								<label class="sr-only">
									<com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
								</label>
								<com:TActivePanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
									<label class="m-l-5 m-r-1" style="display: flex">
									<com:TTextBox ID="pageNumberTop" CssClass="form-control w-50 text-center" Text="1"/>
										<span class="nb-total p-t-1 nb-total-align">/ <com:TActiveLabel ID="nombrePageTop"/></span>
									<com:TActiveButton ID="DefaultButtonTop" OnCallback="goToPage" Attributes.style="display:none"/>
									</label>
								</com:TActivePanel>
							</div>
							<div class="form-group liens">
								<label class="m-l-1">
									<com:TActivePager ID="PagerTop"
												ControlToPaginate="repeaterEtablissements"
												FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
												LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
												Mode="NextPrev"
												NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
												PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
												OnPageIndexChanged="pageChanged"/>
								</label>
							</div>
						</div>
					</div>
				</div>
				<!--END PARTITIONNEUR ETABLISSEMENT TOP+-->

				<!--BEGIN TABLE ETABLISSEMENT AJOUTE-->
				<com:TActiveRepeater ID="repeaterEtablissements" AllowPaging="true" PageSize="10" EnableViewState="true" AllowCustomPaging="true">
					<prop:HeaderTemplate>
						<table class="table-results table-etablissements table-highlight table table-striped">
							<thead>
							<tr>
								<th colspan="9" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<com:TPanel  Visible="<%#$this->TemplateControl->getMode() != 2 %>" >
									<th class="check-col" scope="col"><com:TTranslate>TEXT_MON_ETABLISSEMENT</com:TTranslate></th>
								</com:TPanel>
								<th class="col-90" scope="col" style="<%=(!$this->SourceTemplateControl->getIsEntrepriseLocale() || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique'))?'display:none':''%>">
									<com:TActiveLinkButton
											CssClass="on"
											OnCallback="SourceTemplateControl.sortRepeater"
											Text="<%=Prado::localize('TEXT_NC')%>"
											ActiveControl.CallbackParameter="CodeEtablissement"/>
									<com:TActiveImageButton
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
											OnCallback="SourceTemplateControl.sortRepeater"
											ActiveControl.CallbackParameter="CodeEtablissement"
											AlternateText="<%=Prado::localize('TEXT_NC')%>"
											Attributes.title="<%=Prado::localize('TEXT_NC')%>" />
								</th>
								<com:TPanel   Visible="<%#($this->TemplateControl->getMode() != 4 || ( (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && $this->TemplateControl->getEntrepriseExiste() )
                                    ) )%>">
									<th class="center col-30" scope="col">&nbsp;</th>
								</com:TPanel>
								<com:TPanel  Visible="<%#$this->TemplateControl->getMode() == 4 && (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste() ))%>"  >
									<th class="center col-30" scope="col">

										<a href="#"><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate>
										</a>
									</th>
								</com:TPanel>
								<th class="center col-30" scope="col">&nbsp;</th>
								<th class="col-250" scope="col">
									<com:TActiveLinkButton
											CssClass="on"
											OnCallback="SourceTemplateControl.sortRepeater"
											Text="<%=Prado::localize('TEXT_VOIE')%>"
											ActiveControl.CallbackParameter="Voie"/>
									<com:TActiveImageButton
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
											OnCallback="SourceTemplateControl.sortRepeater"
											ActiveControl.CallbackParameter="Voie"
											AlternateText="<%=Prado::localize('TEXT_VOIE')%>"
											Attributes.title="<%=Prado::localize('DEFINE_TRIER_PAR_VOIE')%>" />
								</th>
								<th class="col-250" scope="col">
									<com:TActiveLinkButton
											CssClass="on"
											OnCallback="SourceTemplateControl.sortRepeater"
											Text="<%=Prado::localize('TEXT_CP_BP')%>"
											ActiveControl.CallbackParameter="Cp"/>
									<com:TActiveImageButton
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
											OnCallback="SourceTemplateControl.sortRepeater"
											ActiveControl.CallbackParameter="Cp"
											AlternateText="<%=Prado::localize('TEXT_CP_BP')%>"
											Attributes.title="<%=Prado::localize('DEFINE_TRIER_CP')%>" />
									<com:TActiveLinkButton
											CssClass="on"
											OnCallback="SourceTemplateControl.sortRepeater"
											Text="<%=Prado::localize('TEXT_VILLE')%>"
											ActiveControl.CallbackParameter="Ville"/>
									<com:TActiveImageButton
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
											OnCallback="SourceTemplateControl.sortRepeater"
											ActiveControl.CallbackParameter="Ville"
											AlternateText="<%=Prado::localize('TEXT_VILLE')%>"
											Attributes.title="<%=Prado::localize('DEFINE_TRIER_VILLE')%>" />
								</th>
								<com:TPanel Visible="<%# (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste()))
                                    						&&($this->TemplateControl->getMode() != 1 )%>" >
									<th class="actions-moyen text-right" scope="col"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
								</com:TPanel>
							</tr>
							</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<com:THiddenField Id="classTr" value="<%#$this->TemplateControl->getClass($this->ItemIndex,$this->Data->getCodeEtablissement())%>"/>
						<tr class="<%=$this->classTr->Value%>">
							<com:TPanel  Visible="<%#$this->TemplateControl->getMode() == 1 %>" >
								<td class="check-col">
									<com:TImage visible="<%#$this->TemplateControl->getPictoMonEtablissement($this->Data->getCodeEtablissement())  ? 'true' : 'false' %>"
												Attributes.alt="<%#$this->TemplateControl->getAltMonEtablissement($this->Data->getCodeEtablissement())%>"
												ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoMonEtablissement($this->Data->getCodeEtablissement())%>" />
								</td>
							</com:TPanel>
							<com:TPanel  Visible="<%#$this->TemplateControl->getMode() == 3 || $this->TemplateControl->getMode() ==4  %>" >
								<td class="check-col">
									<com:TActiveRadioButton
											id="radio_monEtablissement"
											Attributes.title="<%=Prado::localize('TEXT_MON_ETABLISSEMENT')%>"
											UniqueGroupName="monEtablissement"
											value="floue"
											CssClass="radio"
											checked="<%#$this->Data->getCodeEtablissement() == $this->TemplateControl->getSiretMonEtablissement()%>"
											onCallBack="SourceTemplateControl.setSiretEtablissement"
											ActiveControl.CallBackParameter="<%#$this->Data->getCodeEtablissement()%>"

									>
										<prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
										<prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
										<prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>


									</com:TActiveRadioButton>
									<com:THiddenField Id="codeEtab" Value="<%#$this->Data->getCodeEtablissement()%>"/>
								</td>
							</com:TPanel>
							<td class="col-90" data-tnr="codeEtablissementUser" style="<%=(!$this->SourceTemplateControl->getIsEntrepriseLocale() || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique'))?'display:none':''%>"><com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" Text="<%#$this->Data->getCodeEtablissement()%>" /></td>
							<com:TPanel   Visible="<%#( $this->TemplateControl->getMode() != 4  || ( (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && $this->TemplateControl->getEntrepriseExiste() )
                                    ) )%>" >
								<td class="center col-30">
									<com:TActiveImage visible="<%# $this->Data->getEstSiege() ? 'true' : 'false' %>"
													  Attributes.alt="<%# Prado::localize('TEXT_SIEGE_SOCIAL') %>"
													  Attributes.title="<%# Prado::localize('TEXT_SIEGE_SOCIAL') %>"
													  ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-siege-social.png" />
								</td>
							</com:TPanel>
							<com:TPanel  Visible="<%#$this->TemplateControl->getMode() == 4 && (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste() ))%>" >
								<td class="center col-30">
									<com:TActiveRadioButton
											id="radio_siegeSocial"
											Attributes.title="<%=Prado::localize('TEXT_SIEGE_SOCIAL')%>"
											UniqueGroupName="siegeSocial"
											value="floue"
											CssClass="radio"
											checked="<%#$this->Data->getCodeEtablissement() == $this->TemplateControl->getSiretSiegeSocial()%>"
											onCallBack="SourceTemplateControl.setSiretSiegeSocialEse"
											ActiveControl.CallBackParameter="<%#$this->Data->getCodeEtablissement()%>"
									>
										<prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
										<prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
										<prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>


									</com:TActiveRadioButton>
								</td>
							</com:TPanel>
							<td class="center col-30">
								<com:TActiveImage
										visible="<%#$this->TemplateControl->getPictoInscritDefense($this->Data->getInscritAnnuaireDefense())  ? 'true' : 'false' %>"
										Attributes.alt="<%#$this->TemplateControl->getAltinscritDefense($this->Data->getInscritAnnuaireDefense())%>"
										Attributes.title="<%#$this->TemplateControl->getAltinscritDefense($this->Data->getInscritAnnuaireDefense())%>"
										ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoInscritDefense($this->Data->getInscritAnnuaireDefense())%>"
								/>
							</td>
							<td class="col-250"><com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" Text="<%#$this->Data->getAdresse().' ' .$this->Data->getAdresse2()%>" /></td>
							<td class="col-250"><com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>" Text="<%#$this->Data->getCodePostal().' '.$this->Data->getVille()%>" /></td>
							<com:TPanel Visible="<%# (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste() ))
                                    						&&($this->TemplateControl->getMode() != 1 )
                                    						%>" >

								<td colspan="2" class="actions text-right">
											<com:THiddenField id="labelSiret" value="<%#$this->Data->getCodeEtablissement()%>" />
											<com:TActiveLinkButton
													id="pictoModifier"
													cssClass="btn btn-sm btn-primary"
													Attributes.title="<%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>"
													onCommand="SourceTemplateControl.openModalModificationEtablissement"
													CommandParameter="<%#$this->Data->getCodeEtablissement().'#'.$this->pictoModifier->getClientId()%>"
													Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->Action->getClientId()%>').value = 'Modify';
                                                                          document.getElementById('<%#$this->SourceTemplateControl->idToModify->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';
                                                                          document.getElementsByClassName('modal-title')[0].innerHTML = '<%=prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>';
                                                                          document.getElementsByClassName('modal-title')[1].innerHTML = '<%=prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>';
                                                                          document.getElementsByClassName('modal-title')[2].innerHTML = '<%=prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>';
                                                                          document.getElementsByClassName('modal-title')[4].innerHTML = '<%=prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>';
											">
												<i class="fa fa-pencil"></i>
											</com:TActiveLinkButton>
											<com:TActiveLinkButton
													id="pictoModifierInscriptionDefence"
													cssClass="btn btn-sm btn-primary"
													Visible="false"
													Attributes.title="<%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>"
													onCommand="SourceTemplateControl.openModalModifyInscriptionDefence"
													CommandParameter="<%#$this->Data->getCodeEtablissement().'#'.$this->pictoModifierInscriptionDefence->getClientId()%>"
													Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->idToModifyDefence->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';">
												<i class="fa fa-pencil"></i>
											</com:TActiveLinkButton>
											<com:TActiveLinkButton
													Visible="<%#$this->Data->getSaisieManuelle() == '1' %>"
													cssClass="btn btn-sm btn-danger"
													Attributes.title="<%=Prado::localize('TEXT_SUPRIMER_ETABLISSEMENT')%>"
													Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';
                                                                          document.getElementById('<%#$this->SourceTemplateControl->nameToDelete->getClientId()%>').innerHTML =  document.getElementById('<%#$this->labelSiret->getClientId()%>').value;
                                                                          KTJS.pages_entreprise_common.common.openModal('#modalConfirmationSuppression');">
												<i class="fa fa-trash"></i>
											</com:TActiveLinkButton>
								</td>
							</com:TPanel>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
						</table>
					</prop:FooterTemplate>
				</com:TActiveRepeater>
				<!--END TABLE ETABLISSEMENT AJOUTE-->

				<!--BEGIN PARTITIONNEUR ETABLISSEMENT BOTTOM-->
				<div class="line-partitioner clearfix">
					<div class="partitioner" style="display: none;" >
						<div class="intitule">
							<strong>
								<com:TLabel ID="labelAfficherBottom">
									<com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
								</com:TLabel>
							</strong>
						</div>
						<com:TDropDownList ID="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
							<com:TListItem Text="10" Selected="10"/>
							<com:TListItem Text="20" Value="20"/>
							<com:TListItem Text="50" Value="50"/>
							<com:TListItem Text="100" Value="100"/>
							<com:TListItem Text="500" Value="500"/>
						</com:TDropDownList>
						<div class="intitule">
							<com:TLabel id="resultParPageBottom">
								<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
							</com:TLabel>
						</div>
						<label style="display:none;" for="allEtablissement_pageNumberBottom">
							<com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
						</label>
						<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonTop" CssClass="float-left">
							<com:TTextBox ID="pageNumberBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
							<div class="nb-total ">
								<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/>
								<com:TLabel ID="nombrePageBottom"/>
								<com:TButton ID="DefaultButtonBottom" OnClick="goToPage" Attributes.style="display:none"/>
							</div>
						</com:TPanel>
						<div class="liens">
							<com:TActivePager ID="PagerBottom"
										ControlToPaginate="repeaterEtablissements"
										FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
										LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
										Mode="NextPrev"
										NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
										PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
										OnPageIndexChanged="pageChanged"/>
						</div>
					</div>
				</div>
				<!--END PARTITIONNEUR ETABLISSEMENT BOTTOM-->
			</div>
			<!--END TABLE ETABLISSEMENT-->
		</div>
		<div class="clearfix">
			<com:TPanel cssClass="pull-right" Visible="<%= $this->getAjoutManuel() && $this->getOptionAdd()%> ">
				<com:TActiveLinkButton
						id="ajouterEtablissement"
						Visible="<%= $this->getMode() != 1 %>"
						cssClass="ajout-el ajout-etablissement btn btn-sm btn-primary"
						Attributes.title="<%=Prado::localize('TEXT_AJOUTER_ETABLISSEMENT')%>"
						onCommand="SourceTemplateControl.viderInfosEtablissement">
					<i class="fa fa-plus m-r-1"></i>
					<com:TTranslate>TEXT_AJOUTER_ETABLISSEMENT</com:TTranslate>
				</com:TActiveLinkButton>
			</com:TPanel>
			<com:TPanel cssClass="pull-right" Visible="<%= !$this->getAjoutManuel() && $this->getOptionAdd() %>">
				<com:TActiveLinkButton
						id="ajouterEtablissementSGMAP"
						Visible="<%= $this->getMode() != 1 %>"
						cssClass="ajout-el ajout-etablissement btn btn-sm btn-primary"
						Attributes.title="<%=Prado::localize('TEXT_AJOUTER_ETABLISSEMENT')%>"
						onCommand="SourceTemplateControl.viderInfosEtablissementSgmap">
					<i class="fa fa-plus m-r-1"></i>
					<com:TTranslate>TEXT_AJOUTER_ETABLISSEMENT</com:TTranslate>
				</com:TActiveLinkButton>
			</com:TPanel>
		</div>
	</div>
</com:TActivePanel>

<!--BEGIN MODAL AJOUT ETABLISSEMENT ETRANGER-->
<com:TPanel id="panelDegradeEtab" cssClass="modal-etablissement" Visible="<%= $this->getAjoutManuel() && $this->getOptionAdd() %>">
	<div class="modal fade" tabindex="-1" role="dialog" id="modalEtablissement">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="" role="document">
					<div class="modal-header">
						<div class="modal-title"><%=prado::localize('TEXT_AJOUTER_ETABLISSEMENT')%></div>
					</div>
					<div class="modal-body">
						<com:TPanel ID="panelMessageAvertissement" style="display:none">
							<com:PanelMessageAvertissement ID="messageAvertissement" cssClass="" />
						</com:TPanel>

						<com:AtexoValidationSummary id="divValidationSummaryEtablissement" ValidationGroup="validateCreateEtablissement"/>
						<!--Debut Formulaire-->
						<div class="form-bloc">
							<div class="content">
								<com:TPanel cssClass="form-group form-group-sm" Visible="<%= ($this->getIsEntrepriseLocale() && !Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) %>">
									<label class="col-md-3" for="codeEtablissement">
										<com:TTranslate>TEXT_NC</com:TTranslate>
										:
										<span class="text-danger">*</span>
										<span class="m-0 p-0 m-l-1">
                                                        <a data-target="#" data-toggle="tooltip" data-placement="right" title="Cinq derniers chiffres du SIRET">
                                                            <i class="fa fa-question-circle text-info"></i>
                                                        </a>
                                                    </span>
									</label>
									<div class="col-md-9">
										<com:TActiveTextBox ID="codeEtablissement" cssClass="form-control" MaxLength="5"/>
									</div>
									<com:TRequiredFieldValidator
											ControlToValidate="codeEtablissement"
											ValidationGroup="validateCreateEtablissement"
											Display="Dynamic"
											ErrorMessage="<%=Prado::localize('TEXT_NC')%>"
											EnableClientScript="true"
											Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissement').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TRequiredFieldValidator>
									<com:TRegularExpressionValidator
											ID="formatCodeEtablissement"
											Enabled="<%= $this->getIsEntrepriseLocale()%>"
											ValidationGroup="validateCreateEtablissement"
											ControlToValidate="codeEtablissement"
											RegularExpression="[a-zA-Z0-9]{1,5}"
											ErrorMessage="<%= Prado::localize('TEXT_CODE_ETABLISSEMENT_INVALIDE') %>"
											Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissement').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TRegularExpressionValidator>
									<com:TCustomValidator
											id="validateIfCodeExiste"
											ValidationGroup="validateCreateEtablissement"
											ControlToValidate="codeEtablissement"
											Display="Dynamic"
											EnableClientScript="true"
											onServerValidate="validateIfCodeExiste"
											ErrorMessage="Existe Déjà"
											Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissement').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TCustomValidator>
									<com:TCustomValidator
											ID="SirenValidator"
											visible="<%=$this->getIsEntrepriseLocale() && Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseSiren')%>"
											ValidationGroup="validateCreateEtablissement"
											ControlToValidate="codeEtablissement"
											Display="Dynamic"
											EnableClientScript="true"
											ErrorMessage="<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
											ClientValidationFunction="validerCodeEtablissement"
											Text=" "
									>
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissement').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TCustomValidator>
									<script>
                                        function validerCodeEtablissement() {
                                            return controlValidationCodeEtab('<%=$this->getSirenEntreprise()%>', '<%=$this->codeEtablissement->getClientId()%>');
                                        }
									</script>
								</com:TPanel>
								<!--BEGIN ADRESSE-->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_EtablissementAddress1">
										<com:TTranslate>TEXT_ADRESSE</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox ID="EtablissementAddress1" cssClass="form-control"/>
									</div>
									<div class="col-md-1">
										<com:TRequiredFieldValidator
												ControlToValidate="EtablissementAddress1"
												ValidationGroup="validateCreateEtablissement"
												Display="Dynamic"
												ErrorMessage="<%=Prado::localize('TEXT_ADRESSE') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
												EnableClientScript="true"
												Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle text-danger'></i></span>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRequiredFieldValidator>
									</div>
								</div>
								<!--END ADRESSE-->

								<!--BEGIN ADRESSE SUITE-->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_Etablissementaddress2">
										<com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox ID="Etablissementaddress2" cssClass="form-control"/>
									</div>
								</div>
								<!--END ADRESSE SUITE-->

								<!--BEGIN CODE POSTALE-->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_cpEtablissement">
										<com:TTranslate>TEXT_CP</com:TTranslate>
										<com:TLabel visible="<%= $this->getIsEntrepriseLocale()%>" cssclass="text-danger">* </com:TLabel> :
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox ID="cpEtablissement" cssClass="form-control" MaxLength="<%=(!$this->getIsEntrepriseLocale() || !Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte'))?'':'5'%>"/>
									</div>
									<div class="col-md-1">
										<com:TRequiredFieldValidator
												ControlToValidate="cpEtablissement"
												ValidationGroup="validateCreateEtablissement"
												Enabled="<%= $this->getIsEntrepriseLocale()%>"
												Display="Dynamic"
												ErrorMessage="<%=Prado::localize('TEXT_CP') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
												EnableClientScript="true"
												Text="<i class='fa fa-times-circle text-danger'></i>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRequiredFieldValidator>
										<com:TRegularExpressionValidator
												ID="formatCp"
							                    visible="<%=($this->getIsEntrepriseLocale() && Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte'))%>"
												ValidationGroup="validateCreateEtablissement"
												Enabled="<%= $this->getIsEntrepriseLocale()%>"
												ControlToValidate="cpEtablissement"
												ErrorMessage="<%=Prado::localize('CONTROL_CODE_POSTAL') %>"
												Text="<i class='fa fa-times-circle text-danger'></i>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRegularExpressionValidator>
										<com:TRegularExpressionValidator
												ID="formatCpEtrangere"
												Enabled="<%= !$this->getIsEntrepriseLocale()%>"
												ValidationGroup="validateCreateEtablissement"
												ControlToValidate="cpEtablissement"
												ErrorMessage="<%= str_replace('{NBRE_CARCTERE_AUTORISER}', Application\Service\Atexo\Atexo_Config::getParameter('NBRE_CARACTERE_CP_ETABLISSEMENT_ETRANGERE'), Prado::localize('CONTROL_CODE_POSTAL_ETRANGERE')) %>"
												Text="<i class='fa fa-times-circle text-danger'></i>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRegularExpressionValidator>
									</div>
								</div>
								<!--END CODE POSTALE-->

								<!--BEGIN VILLE-->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_villeEtablissement">
										<com:TTranslate>TEXT_VILLE</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox ID="villeEtablissement" cssClass="form-control"/>
									</div>
									<div class="col-md-1">
										<com:TRequiredFieldValidator
												ControlToValidate="villeEtablissement"
												ValidationGroup="validateCreateEtablissement"
												Display="Dynamic"
												ErrorMessage="<%=Prado::localize('TEXT_VILLE') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
												EnableClientScript="true"
												Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle text-danger'></i></span>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRequiredFieldValidator>
									</div>
								</div>
								<!--END VILLE-->

								<!--BEGIN PAYS / TERRITOIRES-->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_paysEtablissement">
										<com:TTranslate>DEFINE_PAYS_TERRITOIRES</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox ID="paysEtablissement" cssClass="form-control"/>
									</div>
									<div class="col-md-1">
										<com:TRequiredFieldValidator
												ControlToValidate="paysEtablissement"
												ValidationGroup="validateCreateEtablissement"
												Display="Dynamic"
												ErrorMessage="<%=Prado::localize('DEFINE_PAYS_TERRITOIRES') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
												EnableClientScript="true"
												Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle text-danger'></i></span>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummaryEtablissement').style.display='';
											</prop:ClientSide.OnValidationError>
										</com:TRequiredFieldValidator>
									</div>
								</div>
								<!--END PAYS / TERRITOIRES-->

								<!-- Début Numéro de TVA intercommunautaire -->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="tvaIntracommunautaire">
										<com:TTranslate>TEXT_TVA_INTRACOMMUNAUTAIRE</com:TTranslate>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox
												ID="tvaIntracommunautaire"
												cssClass="form-control tvaIntracommunautaire"
												Attributes.title="<%=Prado::localize('TEXT_TVA_INTRACOMMUNAUTAIRE')%>"
												MaxLength="18"
										/>
									</div>
								</div>
								<!-- Fin Numéro de TVA intercommunautaire -->
								<com:TPanel CssClass="line" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
									<com:TActiveCheckBox id="inscriptionAnnuaire" Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"/>
									<label for="ctl0_CONTENU_PAGE_etablissements_inscriptionAnnuaire" class="m-l-1">
										<com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
										<com:THyperLink NavigateUrl="#"
														target="_blank"
														CssClass="m-l-1"
														ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
														Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
											<com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
										</com:THyperLink>
									</label>
								</com:TPanel>
								<!-- -->
							</div>
						</div>
						<!--Fin Formulaire-->
					</div>
					<div class="modal-footer">
						<com:THiddenField id="idToModify"/>
						<com:THiddenField id="Action"/>

						<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
						<com:TActiveButton
								id="saveEtab"
								Text="<%=Prado::localize('TEXT_VALIDER')%>"
								CssClass="btn btn-sm btn-primary pull-right"
								ValidationGroup="validateCreateEtablissement"
								onCommand="SourceTemplateControl.saveEtablissement"
								CommandName="<%=$this->Action->value%>"
								onCallBack="SourceTemplateControl.refreshRepeater"
								CommandParameter="<%=$this->idToModify->value%>"
						>
						</com:TActiveButton>
					</div>
				</div>
			</div>
		</div>
	</div>
</com:TPanel>
<!--BEGIN MODAL AJOUT ETABLISSEMENT ETRANGER-->

<!--BEGIN MODAL AJOUT ETABLISSEMENT FRANCAIS-->
<com:TPanel id="panelEtabSgmap" cssClass="modal-etablissementSgmap">
	<div class="modal fade" tabindex="-1" role="dialog" id="modalEtablissementSgmap">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" >
				<div class="modal-header">
					<div class="modal-title"><%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%></div>
				</div>
				<div class="modal-body">
					<com:AtexoValidationSummary id="divValidationSummaryEtablissementSgmap" ValidationGroup="validateCreateEtablissementSgmap"/>
					<!--Debut Formulaire-->
					<div class="form-bloc">
						<div class="content">
							<div class="form-group form-group-sm">
								<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_codeEtablissementSgmap">
									<com:TTranslate>TEXT_NC</com:TTranslate>
									<span class="text-danger">* </span>
									<i class="fa fa-question-circle text-info" data-toggle="tooltip" title="Cinq derniers chiffres du SIRET"></i>
								</label>
								<div class="col-md-3">
									<com:TActiveTextBox ID="codeEtablissementSgmap" cssClass="form-control" MaxLength="5"/>
								</div>
								<div class="col-md-1">
									<com:TRequiredFieldValidator
											ControlToValidate="codeEtablissementSgmap"
											ValidationGroup="validateCreateEtablissementSgmap"
											Display="Dynamic"
											ErrorMessage="<%=Prado::localize('TEXT_NC')%>"
											EnableClientScript="true"
											Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle text-danger'></i></span>">
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TRequiredFieldValidator>
									<com:TRegularExpressionValidator
											ID="formatCodeEtablissementSgmap"
											ValidationGroup="validateCreateEtablissementSgmap"
											ControlToValidate="codeEtablissementSgmap"
											RegularExpression="[0-9]{5}"
											ErrorMessage="<%=Prado::localize('TEXT_CODE_ETABLISSEMENT_INVALIDE') %>"
											Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><i class='fa fa-times-circle text-danger'></i></span>">
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TRegularExpressionValidator>
									<com:TCustomValidator
											ID="SirenValidatorSgmap"
											ValidationGroup="validateCreateEtablissementSgmap"
											ControlToValidate="codeEtablissementSgmap"
											Display="Dynamic"
											EnableClientScript="true"
											ErrorMessage="<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
											ClientValidationFunction="validerCodeEtablissementSgmap"
											Text=" "
									>
										<prop:ClientSide.OnValidationError>
											document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
										</prop:ClientSide.OnValidationError>
									</com:TCustomValidator>
									<script>
                                        function validerCodeEtablissementSgmap() {
                                            return controlValidationCodeEtab('<%=$this->getSirenEntreprise()%>', '<%=$this->codeEtablissementSgmap->getClientId()%>');
                                        }
									</script>
								</div>
							</div>
							<!-- -->
							<!-- -->
							<com:TPanel ID="panelInscrisDefense" CssClass="line" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
								<div class="intitule-auto">
									<com:TActiveCheckBox
											id="inscriptionAnnuaireSgmap"
											Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"
									/>
									<label for="ctl0_CONTENU_PAGE_etablissements_inscriptionAnnuaireSgmap" class="m-l-1">
										<com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
										<com:THyperLink NavigateUrl="#"
														target="_blank"
														CssClass="m-l-1"
														ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
														Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;"
										>
											<com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
										</com:THyperLink>
									</label>
								</div>
							</com:TPanel>
							<!-- -->
						</div>
						<!--Fin Formulaire-->
					</div>
				</div>
				<!--Debut line boutons-->
				<div class="modal-footer">
					<com:THiddenField id="idToModifySgmap"/>
					<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
					<com:TActiveButton
							id="saveEtabSgmap"
							Text="<%=Prado::localize('TEXT_VALIDER')%>"
							Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
							CssClass="btn btn-sm btn-primary pull-right"
							ValidationGroup="validateCreateEtablissementSgmap"
							onCommand="SourceTemplateControl.saveEtablissementSgmap"
							onCallBack="SourceTemplateControl.refreshRepeater"
					>
					</com:TActiveButton>
				</div>
				<!--Fin line boutons-->
			</div>
		</div>
	</div>
</com:TPanel>
<!--END MODAL AJOUT ETABLISSEMENT FRANCAIS-->

<!-- Debut Modal modif inscription defence -->
<com:TPanel id="panelEtabDefence" cssClass="modal-etablissementDefence">
	<div class="modal fade" tabindex="-1" role="dialog" id="modalEtablissementDefence">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="modal-title">Modifier l'&eacute;tablissement</div>
				</div>
				<div class="modal-body">
					<!--Debut Formulaire-->
					<div class="form-group form-group-sm">
						<label class="col-md-3">
							<com:TTranslate>TEXT_NC</com:TTranslate>
						</label>
						<div class="col-md-9">
							<com:TActiveLabel ID="codeEtablissementDefence"/>
						</div>
					</div>
					<com:TPanel ID="panelDefense" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
						<label for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_inscriptionDefence">
							<com:TActiveCheckBox
									id="inscriptionDefence"
									Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"/>
							<com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
						</label>
						<com:THyperLink NavigateUrl="#"
										target="_blank"
										CssClass="infos-plus"
										ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
										Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
							<com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
						</com:THyperLink>
					</com:TPanel>
					<!--Fin Formulaire-->
				</div>
				<div class="modal-footer">
					<!--Debut line boutons-->
					<div class="clearfix">
						<com:THiddenField id="idToModifyDefence"/>
						<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
						<com:TActiveButton
								id="saveEtabDefence"
								Text="<%=Prado::localize('TEXT_VALIDER')%>"
								CssClass="btn btn-sm btn-primary pull-right"
								onCommand="SourceTemplateControl.saveInscriptionDefence"
								CommandParameter="<%=$this->idToModifyDefence->value%>"
								onCallBack="SourceTemplateControl.refreshRepeater"
						>
						</com:TActiveButton>
					</div>
					<!--Fin line boutons-->
				</div>
			</div>
		</div>
	</div>
</com:TPanel>
<!-- Fin Modal modif inscription defence -->

<!--Debut Modal Confirmation-Suppression-->
<div class="modal-confirmation-suppression">
	<div class="modal fade" tabindex="-1" role="dialog" id="modalConfirmationSuppression">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">
						<com:TTranslate>TEXT_SUPRIMER_ETABLISSEMENT</com:TTranslate>
					</div>
				</div>
				<div class="modal-body">
					<com:TPanel ID="panelMessageDelete">
						<com:PanelMessageAvertissement ID="messageAvertissementdelete" message="<%=Prado::localize('ALERTE_SUPPRESSION_ETABLISSEMENT_IMPOSSIBLE')%>"/>
					</com:TPanel>
					<!--Debut Formulaire-->
					<com:TPanel ID="panelConfirmDelete" cssClass="form-bloc" style="display:'';">
						<div class="content">
                           <span class="text-danger">
                               <strong>
                                   <com:TTranslate>TEXT_ALERTE_SUPPRESSION_ETABLISSEMENT</com:TTranslate>
                               </strong>
                           </span>
							<strong>
								<com:TActiveLabel ID="nameToDelete" text=""/>
							</strong>
						</div>
					</com:TPanel>
					<!--Fin Formulaire-->
				</div>
				<div class="modal-footer">
					<!--Debut line boutons-->
					<div class="clearfix">
						<com:THiddenField id="idToDetele"/>
						<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
						<com:TActiveButton
								id="cofirmSupression"
								Text="<%=Prado::localize('TEXT_POURSUIVRE')%>"
								CssClass="btn btn-sm btn-primary pull-right"
								onCommand="SourceTemplateControl.deleteEtablissement"
								onCallBack="SourceTemplateControl.refreshRepeater"
								CommandParameter="<%=$this->idToDetele->value%>"
						>
						</com:TActiveButton>

						<com:TActiveButton
								id="buttonClose"
								Text="<%=Prado::localize('FERMER')%>"
								Attributes.title="<%=Prado::localize('FERMER')%>"
								CssClass="bouton-moyen float-right"
								onCommand="SourceTemplateControl.resetModalDelet"
								style="display:none;"
						>
						</com:TActiveButton>
					</div>
					<!--Fin line boutons-->
				</div>
			</div>
		</div>
	</div>
</div>
<com:TActiveLabel id="scriptJs" style="display:none;"/>
<!--Fin Modal  Confirmation-Suppression-->

<!--BEGIN MODAL MODIFY ETABLISSEMENT SYNCHRO-->
<com:TPanel id="panelDegradeEtab2" cssClass="modal-etablissement">
	<div class="modal fade" tabindex="-1" role="dialog" id="modalEtablissementModify">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="" role="document">
					<div class="modal-header">
						<div class="modal-title">Ajouter un &eacute;tablissement</div>
					</div>
					<div class="modal-body">
						<com:TPanel ID="PanelMessageInformation" Visible="<%=$this->getIsEntrepriseLocale() && Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSgmap')%>">
							<com:PanelMessageInformation ID="messageInformation" cssClass="" message="<%=Prado::localize('INFORMATION_DISPONIBLITE_IDENTIFICATION_ETABLISSEMENT')%>"/>
						</com:TPanel>
						<!--Debut Formulaire-->
						<div class="form-bloc">
							<div class="content">
								<com:TPanel cssClass="form-group form-group-sm" Visible="<%= ($this->getIsEntrepriseLocale() && !Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) %>">
									<label class="col-md-3" for="codeEtablissement">
										<com:TTranslate>TEXT_NC</com:TTranslate>
										:
										<span class="text-danger">*</span>
									</label>
									<com:TActiveLabel ID="mcodeEtablissement"/>
								</com:TPanel>

								<!--BEGIN ADRESSE-->
								<div class="form-group form-group-sm">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_EtablissementAddress1">
										<com:TTranslate>TEXT_ADRESSE</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<com:TActiveLabel ID="mEtablissementAddress1"/>
								</div>
								<!--END ADRESSE-->

								<!--BEGIN ADRESSE SUITE-->
								<div class="form-group form-group-sm">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_Etablissementaddress2">
										<com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate>
									</label>
									<com:TActiveLabel ID="mEtablissementaddress2"/>
								</div>

								<!--END ADRESSE SUITE-->

								<!--BEGIN CODE POSTALE-->
								<div class="form-group form-group-sm">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_cpEtablissement">
										<com:TTranslate>TEXT_CP</com:TTranslate>
										<com:TLabel visible="<%= $this->getIsEntrepriseLocale()%>" cssclass="text-danger">* </com:TLabel> :
									</label>
									<com:TActiveLabel ID="mcpEtablissement"/>
								</div>

								<!--END CODE POSTALE-->

								<!--BEGIN VILLE-->
								<div class="form-group form-group-sm">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_villeEtablissement">
										<com:TTranslate>TEXT_VILLE</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<com:TActiveLabel ID="mvilleEtablissement"/>
								</div>

								<!--END VILLE-->

								<!--BEGIN PAYS / TERRITOIRES-->
								<div class="form-group form-group-sm">
									<label class="col-md-3" for="ctl0_CONTENU_PAGE_etablissements_paysEtablissement">
										<com:TTranslate>DEFINE_PAYS_TERRITOIRES</com:TTranslate>
										<span class="text-danger">* </span>
									</label>
									<com:TActiveLabel ID="mpaysEtablissement"/>
								</div>

								<!--END PAYS / TERRITOIRES-->

								<!-- Début Numéro de TVA intercommunautaire -->
								<div class="form-group form-group-sm clearfix">
									<label class="col-md-3" for="tvaIntracommunautaire">
										<com:TTranslate>TEXT_TVA_INTRACOMMUNAUTAIRE</com:TTranslate>
									</label>
									<div class="col-md-8">
										<com:TActiveTextBox
												ID="mtvaIntracommunautaire"
												cssClass="form-control tvaIntracommunautaire"
												Attributes.title="<%=Prado::localize('TEXT_TVA_INTRACOMMUNAUTAIRE')%>"
												MaxLength="18"
										/>
									</div>
								</div>
								<!-- Fin Numéro de TVA intercommunautaire -->
								<com:TPanel CssClass="line" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
									<com:TActiveCheckBox id="minscriptionAnnuaire" Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"/>
									<label for="ctl0_CONTENU_PAGE_etablissements_inscriptionAnnuaire" class="m-l-1">
										<com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
										<com:THyperLink NavigateUrl="#"
														target="_blank"
														CssClass="m-l-1"
														ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
														Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
											<com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
										</com:THyperLink>
									</label>
								</com:TPanel>
								<!-- -->
							</div>
						</div>
						<!--Fin Formulaire-->
					</div>
					<div class="modal-footer">
						<com:THiddenField id="midToModify"/>
						<com:THiddenField id="ActionModify"/>

						<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
						<com:TActiveButton
								id="msaveEtab"
								Text="<%=Prado::localize('TEXT_VALIDER')%>"
								CssClass="btn btn-sm btn-primary pull-right"
								onCommand="SourceTemplateControl.modifyEtablissement"
								CommandParameter="<%=$this->idToModify->value%>"
								onCallBack="SourceTemplateControl.refreshRepeater"
						>
						</com:TActiveButton>
					</div>
				</div>
			</div>
		</div>
	</div>
</com:TPanel>
<!--END MODAL MODIFY ETABLISSEMENT SYNCHRO-->

<script>
	J('#ctl0_CONTENU_PAGE_etablissements_ajouterEtablissementSGMAP').on('click', function() {
		J('.modal-title').html('<%=Prado::localize("TEXT_AJOUTER_ETABLISSEMENT")%>')
	})
	J('#ctl0_CONTENU_PAGE_etablissements_ajouterEtablissement').on('click', function() {
		J('.modal-title').html('<%=Prado::localize("TEXT_AJOUTER_ETABLISSEMENT")%>')
	})
</script>