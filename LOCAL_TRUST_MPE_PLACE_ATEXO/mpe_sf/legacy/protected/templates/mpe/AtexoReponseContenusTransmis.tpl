<div class="item_consultation">
	<!--BEGIN BLOC INFORMATIONS SUR DEPOT -->
	<div class="table table-striped list-group m-t-2">
		<div class="panel panel-primary small">
			<div class="panel-heading">
				<div>
					<i class="fa fa-info-circle fa-lg m-r-1" aria-hidden="true"></i>
					Informations sur le dep&oacute;t
				</div>
			</div>
			<div class="panel-body">
				<div class="item_depot clearfix panel-body info_panelLots m-b-1" id="info_panelLots">
					<div class="col-md-6">
						<!--BEGIN DATE DEPOT-->
						<div class="clearfix">
							<label class="col-md-3">
								<com:TTranslate>DEFINE_DEPOSE_LE</com:TTranslate>
								:
							</label>
							<div class="col-md-9">
								<%#$this->getDateDepotOffre()%>
							</div>
						</div>
						<!--END DATE DEPOT-->

						<!--BEGIN INFORMATIONS DEPOT-->
						<div class="clearfix">
							<!--BEGIN PAR-->
							<label class="col-md-3">
								<com:TTranslate>PAR</com:TTranslate>
								:
							</label>
							<div class="col-md-9">
								<%#Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit::getNomPrenomEmailInscrit(Application\Service\Atexo\Atexo_CurrentUser::getIdInscrit())%>
							</div>
							<!--END PAR-->
						</div>
						<!--END INFORMATIONS DEPOT-->

						<!-- BEGIN ENTREPRISE -->
						<div class="clearfix">
							<label class="col-md-3">
								<com:TTranslate>TEXT_ENTREPRISE</com:TTranslate>
								:
							</label>
							<div class="col-md-9">
								<%#Application\Service\Atexo\Atexo_Entreprise::getNomEntreprise(Application\Service\Atexo\Atexo_CurrentUser::getIdEntreprise())%>
							</div>
						</div>
						<!-- END ENTREPRISE -->

						<!-- AR -->
						<com:TPanel id="panelAccuse" cssclass="line" visible="<%=$this->hasAccuseReception()%>">
							<div class="clearfix">
								<label class="col-md-3">
									<com:TTranslate>ACCUSE_RECEPTION_ENTREPRISE</com:TTranslate>
									:
								</label>
								<div class="col-md-9">
									<a href="<%=$this->getUrlTelechargementPdfAR()%>" ><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" /></a>
								</div>
							</div>
						</com:TPanel>
						<!-- END AR -->
					</div>
					<!-- BEGIN RECAPITULATIF-->
					<div class="col-md-6 text-right p-0"
						 style="display:<%#($this->SourceTemplateControl->getConsultation() && $this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1) ?'':'none'%>">
						<label class="col-md-4 p-0">
							<com:TTranslate>TEXT_RECAPITULATIF</com:TTranslate>
						</label>
						<div class="col-md-8 small p-0" style="display:<%#($this->SourceTemplateControl->getConsultation() && $this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1) ?'':'none'%>">
							<ul class="list-unstyled list-inline">
								<li class="m-l-1">
									<div class="statut-signature">
										<div class="statut ">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" data-toggle="tooltip" title="<%=Prado::localize('TEXT_CERTIFICAT')%>"/>
											<i class="fa fa-check-circle fa-lg m-l-1 text-success" data-toggle="tooltip" title="<%=Prado::localize('TEXT_VERIFICATION_OK')%>"></i> :
											<strong>
												<com:TLabel Id="totalOk"/>
											</strong>
										</div>
									</div>
								</li>
								<li class="m-l-1">
									<div class="statut-signature">
										<div class="statut ">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" data-toggle="tooltip" title="<%=Prado::localize('TEXT_CERTIFICAT')%>"/>
											<i class="fa fa-lg m-l-1 fa-exclamation-circle text-warning" data-toggle="tooltip" title="<%=Prado::localize('TEXT_VERIFICATION_INCERTAINE')%>"></i> :
											<strong>
												<com:TLabel Id="totalUnknown"/>
											</strong>
										</div>
									</div>
								</li>
								<li class="m-l-1">
									<div class="statut-signature">
										<div class="statut ">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" data-toggle="tooltip" title="<%=Prado::localize('TEXT_CERTIFICAT')%>"/>
											<i class="fa fa-lg m-l-1 fa-times-circle text-danger" data-toggle="tooltip" title="<%=Prado::localize('TEXT_VERIFICATION_KO')%>"></i> :
											<strong>
												<com:TLabel Id="totalKo"/>
											</strong>
										</div>
									</div>
								</li>
								<li class="m-l-1">
									<div class="statut-signature">
										<div class="statut ">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-aucune-signature.gif" data-toggle="tooltip" title="<%=Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE_MANQUANTE')%>"/>
											&nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp;: <strong>
												<com:TLabel Id="totalUnsigned"/>
											</strong>
										</div>
									</div>
								</li>
							</ul>
						</div>

					</div>
					<!-- END RECAPITULATIF-->

				</div>

				<!-- BEGIN TABLE DUME -->
				<com:TActivePanel ID="panelDUME" visible="<%#($this->hasDume())? true:false%>">
					<table summary=""
						   class="table-results tableau-reponse table table-striped panel panel-default">
						<thead class="panel-heading">
						<tr class="row-title">
							<th class="col-piece">
								<com:TTranslate>DUME</com:TTranslate>
							</th>

							<th class="actions text-right">
								<com:TTranslate>TEXT_ACTIONS</com:TTranslate>
							</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<%%
								if($this->getInformationDume() != false) {
								echo "DUME ".$this->getInformationDume()->getNumeroDumeNational()." - ".Prado::localize('TEXT_FORMAT_DUME_XML');
								}
								%>
							<td class="actions text-right">
								<com:TLinkButton ID="linkDownloadDumeDemandeXML"
												 text="<%=Prado::localize('DEFINE_TELECHARGER')%>"
												 onclick="SourceTemplateControl.downloadDumeDemandeXML" />
							</td>
						</tr>
						<tr>
							<td>
								<%%
								if($this->getInformationDume() != false) {
								echo "DUME ".$this->getInformationDume()->getNumeroDumeNational()." - ".Prado::localize('TEXT_FORMAT_DUME_PDF');
								}
								%>

							<td class="actions text-right">
								<com:TLinkButton ID="linkDownloadDumeDemandePDF"
												 text="<%=Prado::localize('DEFINE_TELECHARGER')%>"
												 onclick="SourceTemplateControl.downloadDumeDemandePDF" />
							</td>
						</tr>
						</tbody>
					</table>

				</com:TActivePanel>
				<!-- END BEGIN TABLE DUME -->

				<!--BEGIN TABLE OFFRE LOT -->
				<com:TActivePanel ID="panelLots">
					<com:TRepeater ID="repeaterLots" DataSource="<%#$this->getDataSource()%>">
						<prop:ItemTemplate>
							<com:TRepeater ID="repeaterTypesEnv" DataSource="<%#$this->Data['enveloppe']%>">
								<prop:ItemTemplate>
									<table summary="<%#Prado::localize('DEFINE_CONTENUS_TRANSMIS')%>  - <%#$this->SourceTemplateControl->getNomDossier($this->Parent->Parent->Data['numLot'],$this->Data['typesEnv'])%> Lot<%#$this->Parent->Parent->Data['numLot']%> %> "
										   class="table-results tableau-reponse table table-striped panel panel-default">
										<thead class="panel-heading">
										<tr class="row-title">
											<th class="col-piece">
												<%#$this->SourceTemplateControl->getNomDossier($this->Parent->Parent->Data['numLot'],$this->Data['typesEnv'])%>
											</th>
											<th class="col-50 center" style="display:<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? '':'none'%>">
												<com:TTranslate>DEFINE_STATUS</com:TTranslate>
											</th>
											<th class="col-180" style="display:<%#( $this->SourceTemplateControl->getConsultation()
																					&& $this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)? '' : 'none'%>">
												<com:TTranslate>DEFINE_TEXT_SIGNATURE</com:TTranslate>
											</th>
											<th class="actions text-right">
												<com:TTranslate>TEXT_ACTIONS</com:TTranslate>
											</th>
										</tr>
										</thead>
										<tbody>

										<tr class="on row-title-2" style="display:<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? '':'none'%>">
											<td class="col-piece" colspan="2"><strong><%#$this->SourceTemplateControl->getNomFormulaire($this->Parent->Parent->Data['numLot'],$this->Data['typesEnv'])%></strong>
											</td>
											<td class="col-50 center"><img
														src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getStatutPictoDossier($this->Data['id_enveloppe'],$this->Data['organisme'])%>"
														alt="<%#$this->SourceTemplateControl->getStatutDossier($this->Data['id_enveloppe'],$this->Data['organisme'])%>"
														title="<%#$this->SourceTemplateControl->getStatutDossier($this->Data['id_enveloppe'],$this->Data['organisme'])%>" class="statut"></td>
											<td class="actions text-right">
												<a href="#" style="display:<%#((Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub')))? 'block':'none'%>"
												   title="<%=Prado::localize('TEXT_VISUALISER_FORMULAIRE')%>"
												   onclick="document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/sub';javascript:popUpFormulaireSub('<%#$this->SourceTemplateControl->getUrl($this->Data['id_enveloppe'],$this->Data['organisme'])%>','yes');">
													<img alt="<%=Prado::localize('TEXT_VISUALISER_FORMULAIRE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif">
												</a>
											</td>
										</tr>

										<com:TRepeater ID="repeaterPiecesTypees"
													   DataSource="<%#$this->SourceTemplateControl->getDataSourceFichiers($this->Data['pieces'], Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE'))%>">
											<prop:ItemTemplate>
												<tr class="<%#$this->SourceTemplateControl->getClassPieceTypees($this->ItemIndex)%>">
													<td class="col-piece ellipsis">
														<div class="blue piece-type">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-typee.png" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_DC3')%> - <%=Prado::localize('AE')%>" alt="<%=Prado::localize('DEFINE_DC3')%> - <%=Prado::localize('AE')%>"/>
															<span class="intitule-piece"><com:TTranslate>DEFINE_DC3</com:TTranslate> - <com:TTranslate>AE</com:TTranslate> :</span>
															<abbr data-toggle="tooltip" title="<%#Prado::localize('TEXT_NOM_DU_DOCUMENT')%> : <%#Application\Service\Atexo\Atexo_Util::encodeToHttp($this->Data->getNomFichier())%>">
																<%#Application\Service\Atexo\Atexo_Util::encodeToHttp($this->SourceTemplateControl->getNomFichier($this->Data))%>
															</abbr>
														</div>
													</td>
													<td class="col-180 ellipsis"
														style="display:<%#( $this->SourceTemplateControl->getConsultation() && $this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)? '' : 'none'%>">
														<div class="statut-signature">
															<div class="statut pull-left">
																<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%=$this->SourceTemplateControl->getPictoSigned($this->Data)%>" alt="<%=Prado::localize('TEXT_CERTIFICAT')%>" /> :
																<com:PictoStatutSignature ID="signature" Statut="<%#$this->Data->getResultatValiditeSignature(null,'entreprise')%>"/>
																<com:TLabel visible="<%#$this->Data->getResultatValiditeSignature(null,'entreprise') == Application\Service\Atexo\Atexo_Config::getParameter('SANS')%>" >&nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp;</com:TLabel>
															</div>

															<a href="javascript:void(0);"
															   class="detail-link ellipsis"
															   title="<%=Prado::localize('DEFINE_DETAIL_SIGNATURE')%>"
															   style="display:<%#($this->Data->getSignatureFichier()) ? '' : 'none' %>"
															   onclick="remplir_<%#$this->Data->getIdFichier()%>();openModal('cas-signature','popup-800',this);"
															>
																<%#$this->SourceTemplateControl->getInfosSignature($this)%>
															</a>
															<div style="display:<%#($this->Data->getSignatureFichier()) ? 'none' : '' %>"
																 class="detail-statut"
															>
																<com:TTranslate>TEXT_AUCUNE_SIGNATURE</com:TTranslate>
															</div>
														</div>
													</td>
													<td class="actions test-right">
														<com:TPanel visible="<%#(($this->Data->getSignatureFichier()) && ($this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)) ? true:false%>">
															<a href="<%#$this->SourceTemplateControl->getLienTelechargementSignature($this->Data)%>">
																<img alt="<%#Prado::localize('DEFINE_TELECHARGER_SIGNATURE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif">
															</a>
														</com:TPanel>
													</td>
												</tr>
											</prop:ItemTemplate>
										</com:TRepeater>

										<com:TRepeater
												visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? true:false%>"
												ID="repeaterEditionsSub"
												DataSource="<%#$this->SourceTemplateControl->getDataSourceFichiersEdition($this->Data['pieces'])%>"
										>
											<prop:ItemTemplate>
												<tr class="<%#(($this->ItemIndex%2==0))? 'contenu_01_Offre_lot_01 row-piece-heritee':'on row-piece-heritee'%>">
													<td class="<%#$this->SourceTemplateControl->getCssClassTdRepeaterEditionsSub($this->ItemIndex, $this->Parent->Parent->Data['pieces'])%>">
														<div class="blue piece-type">
															<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-form.png"
																		Attributes.alt="Edition du formulaire"
																		Visible="<%#($this->Data->getTypePiece() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION')) ? 'true' : 'false' %>"
															/>
															<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png"
																		Visible="<%#($this->Data->getTypePiece() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')) ? true : false%>"
																		Attributes.alt="Edition du formulaire"
															/>
															<abbr data-toggle="tooltip"
																  title="<%#Prado::localize('TEXT_NOM_DU_DOCUMENT')%> : <%#Application\Service\Atexo\Atexo_Config::toPfEncoding($this->Data->getNomFichier())%>">
																<%#Application\Service\Atexo\Atexo_Util::cutString($this->SourceTemplateControl->getNomFichier($this->Data),70)%>
															</abbr>
														</div>
													</td>
													<td colspan="2" class="col-180"></td>
													<td class="actions text-right">
														<com:TPanel visible="<%#(($this->Data->getSignatureFichier() || $this->Data->getIdFichierSignature()) && ($this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)) ? true:false%>">
															<a href="<%#$this->SourceTemplateControl->getLienTelechargementSignature($this->Data)%>" class="btn btn-sm btn-primary" data-toggle="tooltip" title="<com:TTranslate>DEFINE_TELECHARGER</com:TTranslate> la signature">
																<i class="fa fa-download"></i>
															</a>
														</com:TPanel>
													</td>
												</tr>
											</prop:ItemTemplate>
										</com:TRepeater>
										<com:TRepeater
												ID="repeaterPiecesLibres"
												DataSource="<%#$this->SourceTemplateControl->getDataSourceFichiers($this->Data['pieces'], Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE'))%>"
										>
											<prop:ItemTemplate>
												<tr class="<%#(($this->ItemIndex%2==0))? '':'on'%>">
													<td class="col-piece ellipsis">
														<div class="blue piece-type">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" data-toggle="tooltip" title="<%=Prado::localize('PIECE_LIBRE')%>" alt="<%=Prado::localize('PIECE_LIBRE')%>"/>
															<span class="intitule-piece"><com:TTranslate>PIECE_LIBRE</com:TTranslate> :</span>
															<abbr data-toggle="tooltip" title="<%#Prado::localize('TEXT_NOM_DU_DOCUMENT')%> : <%#Application\Service\Atexo\Atexo_Util::encodeToHttp($this->Data->getNomFichier())%>">
																<%#Application\Service\Atexo\Atexo_Util::encodeToHttp($this->SourceTemplateControl->getNomFichier($this->Data))%>
															</abbr>
														</div>
													</td>
													<td class="col-180 ellipsis" style="display:<%#( $this->SourceTemplateControl->getConsultation() && $this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)? '' : 'none'%>">
														<div class="statut-signature" style="display:<%#($this->Data->getIdFichierSignature() == $this->Data->getIdFichier())? '' : 'none'%>">
															<div class="statut pull-left m-r-1">
																<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%=$this->SourceTemplateControl->getPictoSigned($this->Data)%>" alt="<%=Prado::localize('TEXT_CERTIFICAT')%>"/> :
																<com:PictoStatutSignature ID="signaturePades" Statut="<%#$this->Data->getResultatValiditeSignature(null,'entreprise')%>"/>
																<com:TLabel visible="<%# ($this->Data->getResultatValiditeSignature(null,'entreprise') == Application\Service\Atexo\Atexo_Config::getParameter('SANS'))? true: false%>" >&nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp;</com:TLabel>
															</div>

															<a href="javascript:void(0);"
															   class="detail-link ellipsis"
															   data-toggle="tooltip"
															   title="<%=Prado::localize('DEFINE_DETAIL_SIGNATURE')%>"
															   style="display:<%#($this->Data->getSignatureFichier()) ? '' : 'none' %>"
															   onclick="remplir_<%#$this->Data->getIdFichier()%>();openModal('cas-signature','popup-800',this);"
															>
																<%#$this->SourceTemplateControl->getInfosSignature($this)%>
															</a>
															<div style="display:<%#($this->Data->getSignatureFichier()) ? 'none' : '' %>"
																 class="detail-statut"
															>
																<com:TTranslate>TEXT_AUCUNE_SIGNATURE</com:TTranslate>
															</div>
														</div>
													</td>
													<td class="actions text-right">
                                                        <com:TPanel visible="<%#(($this->Data->getSignatureFichier()) && ($this->SourceTemplateControl->getConsultation()->getSignatureOffre() == 1)) ? true:false%>">
                                                            <a href="<%#$this->SourceTemplateControl->getLienTelechargementSignature($this->Data)%>">
                                                                <img alt="<%#Prado::localize('DEFINE_TELECHARGER_SIGNATURE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif"/>
                                                            </a>
                                                        </com:TPanel>
                                                    </td>
												</tr>
											</prop:ItemTemplate>
										</com:TRepeater>

										</tbody>
									</table>

									<!-- Debut bouton télécharger l'ensemble des éditions -->
									<div class="link-line margin-top-10">
										<div class="float-left">
											<com:TLinkButton
													cssClass="bouton-telecharger-long230"
													style="display:<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))?'':'none'%>"
													OnCommand="SourceTemplateControl.telechargerIntegraliteEditions"
													CommandParameter="<%#$this->Data['id_enveloppe']%>"
											>
												<com:TTranslate>DEFINE_TELECHARGER_INTEGRALITE_EDITIONS</com:TTranslate>
											</com:TLinkButton>
										</div>
									</div>
									<!-- Fin bouton télécharger l'ensemble des éditions -->

								</prop:ItemTemplate>
							</com:TRepeater>

						</prop:ItemTemplate>
					</com:TRepeater>
				</com:TActivePanel>
				<!--END TABLE OFFRE LOT -->
			</div>
		</div>
	</div>
	<!--END BLOC INFORMATIONS SUR DEPOT -->
</div>
<script>
    function setTotaux<%=$this->ClientId%>(totalOk, totalNok, totalUnknown, totalUnsigned, totalUnverified)
    {
        J("#<%=$this->totalOk->ClientId%>").html(totalOk);
        J("#<%=$this->totalKo->ClientId%>").html(totalNok);
        J("#<%=$this->totalUnknown->ClientId%>").html(totalUnknown);
        J("#<%=$this->totalUnsigned->ClientId%>").html(totalUnsigned);
        J("#<%=$this->totalUnverified->ClientId%>").html(totalUnverified);
    }
</script>
<com:TActiveLabel Id="scriptTotal" Text="<%=$this->getTotal()%>"/>
<com:TLabel Id="totalUnverified" style="display:none"/>