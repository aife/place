<div class="toggle-panel">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <div class="title-toggle" onclick="togglePanel(this,'contactsDevis');"><com:TTranslate>TEXT_CONTACTS_DEMANDE_DEVIS</com:TTranslate></div>
        <div class="panel" style="display:none;" id="contactsDevis">
            <div class="line">
                <div class="intitule-150"><com:TTranslate>TEXT_ADRESSES_ELECTRONIQUE</com:TTranslate></label> :</div>
                <com:TTextBox Enabled="false" id="emailDevis"  TextMode="MultiLine" Attributes.title="<%=Prado::localize('TEXT_ADRESSES_ELECTRONIQUE')%>" CssClass="content-bloc bloc-570 border-bloc" />
            </div>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>

<com:TActivePanel ID="panelEtablissements" cssClass="form-field">
    <div class="clearfix">
        <div id="toggleEtablissements" style="display: block;" class="panel">
            <!--Bloc message-->
            <com:TPanel cssClass="form-bloc margin-0" Visible="<%=$this->getMode() != 2 %>">
                <div class="alert alert-info" role="alert">
                    <span class="fa fa-info-circle" aria-hidden="true"></span>
                    <com:TLabel id="labeleMsgEtablissement"
                                Text="<%=($this->getMode()!= 1)?Prado::Localize('VEUILLEZ_SELECTIONNER_ETABLISSEMENT'):Prado::Localize('TEXT_VOTRE_ETABLISSEMENT_EST_CELUI_COCHE_SI_DESSOUS')%>"/>
                </div>
            </com:TPanel>
            <!--Fin Bloc message-->

            <div class="table-bloc">
                <!--BEGIN PARTITIONNEUR ETABLISSEMENT TOP-->
                <div class="clearfix m-t-2 m-r-2">
                    <div class="col-md-2">
                        <div class="h5">
                            <strong>
                                <com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>
                                <com:TActiveLabel ID="nombreElement"/>
                            </strong>
                        </div>
                    </div>

                    <div class="form-inline col-md-10">
                        <div class=" form-group-sm text-right">
                            <div class="form-group">
                                <label class="m-r-1 sr-only">
                                    <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                                </label>
                                <com:TDropDownList ID="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" cssClass="form-control">
                                    <com:TListItem Text="10" Selected="10"/>
                                    <com:TListItem Text="20" Value="20"/>
                                    <com:TListItem Text="50" Value="50"/>
                                    <com:TListItem Text="100" Value="100"/>
                                    <com:TListItem Text="500" Value="500"/>
                                </com:TDropDownList>
                            </div>
                            <div class="form-group">
                                <label class="sr-only">
                                    <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
                                </label>
                                <com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
                                    <label class="m-l-5 m-r-1">
                                        <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
                                    </label>
                                    <com:TTextBox ID="pageNumberTop" CssClass="form-control w-50 text-center" Text="1"/>
                                    <label class="nb-total ">/
                                        <com:TLabel ID="nombrePageTop"/>
                                    </label>
                                    <com:TButton ID="DefaultButtonTop" OnClick="goToPage" Attributes.style="display:none"/>
                                </com:TPanel>
                            </div>
                            <div class="form-group liens">
                                <label class="m-l-1">
                                    <com:TPager ID="PagerTop"
                                                ControlToPaginate="repeaterEtablissements"
                                                FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
                                                LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
                                                Mode="NextPrev"
                                                NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
                                                PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
                                                OnPageIndexChanged="pageChanged"/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END PARTITIONNEUR ETABLISSEMENT TOP+-->

                <com:TActiveRepeater ID="repeaterEtablissements" AllowPaging="true" PageSize="10" EnableViewState="true" AllowCustomPaging="true">
                    <div class="table table-striped list-group table-results table-etablissements table-highlight">
                        <prop:HeaderTemplate>
                            <div class="list-group-item header kt-vertical-align small m-t-2">
                                <div class="kt-vertical-align">
                                    <!--BEGIN CHEXKBOX-->
                                    <com:TPanel cssClass="col-md-1 middle" Visible="<%#$this->TemplateControl->getMode() != 2 %>">
                                        <div class="check-col"></div>
                                    </com:TPanel>
                                    <!--END CHEXKBOX-->

                                    <!--BEGIN CODE ETABLISSEMENT-->
                                    <div class="col-md-2">
                                        <com:TActiveLinkButton
                                                CssClass="on"
                                                OnCallback="SourceTemplateControl.sortRepeater"
                                                ActiveControl.CallbackParameter="CodeEtablissement">
                                            <span data-toggle="tooltip" title=" ' <%=Prado::localize('DEFINE_TRIER')%>  par  <%=Prado::localize('TEXT_NC')%> ' ">
                                                <%=Prado::localize('TEXT_NC')%>
                                                <i class="fa fa-angle-down"></i>
                                           </span>
                                        </com:TActiveLinkButton>
                                    </div>
                                    <!--END CODE ETABLISSEMENT-->

                                    <!--DEBUT INSCRIT SUR LES ANNUAIRES DES POTAILS DE LA DEFENSE-->
                                    <div class="col-md-1"></div>
                                    <!--FIN INSCRIT SUR LES ANNUAIRES DES POTAILS DE LA DEFENSE-->

                                    <!--BEGIN VOIE -->
                                    <div class="col-md-3">
                                        <com:TActiveLinkButton
                                                CssClass="on"
                                                OnCallback="SourceTemplateControl.sortRepeater"
                                                ActiveControl.CallbackParameter="Voie">
                                            <span data-toggle="tooltip" title=" ' <%=Prado::localize('DEFINE_TRIER')%>  par  <%=Prado::localize('TEXT_VOIE')%> ' ">
                                                <%=Prado::localize('TEXT_VOIE')%>
                                                <i class="fa fa-angle-down"></i>
                                           </span>
                                        </com:TActiveLinkButton>
                                    </div>
                                    <!--END VOIE -->
                                    <!--BEGIN CP-->
                                    <div class="col-md-1">
                                        <com:TActiveLinkButton
                                                CssClass="on"
                                                OnCallback="SourceTemplateControl.sortRepeater"
                                                ActiveControl.CallbackParameter="Cp">
                                           <span data-toggle="tooltip" title=" ' <%=Prado::localize('DEFINE_TRIER')%>  par  <%=Prado::localize('TEXT_CP_BP')%> ' ">
                                                <%=Prado::localize('TEXT_CP_BP')%>
                                                <i class="fa fa-angle-down"></i>
                                           </span>
                                        </com:TActiveLinkButton>
                                    </div>
                                    <!--END CP-->

                                    <!--BEGIN VILLE-->
                                    <div class="col-md-2">
                                        <com:TActiveLinkButton
                                                CssClass="on"
                                                OnCallback="SourceTemplateControl.sortRepeater"
                                                ActiveControl.CallbackParameter="Ville">
                                            <span data-toggle="tooltip" title=" ' <%=Prado::localize('DEFINE_TRIER')%>  par  <%=Prado::localize('TEXT_VILLE')%> ' ">
                                                <%=Prado::localize('TEXT_VILLE')%>
                                                <i class="fa fa-angle-down"></i>
                                           </span>
                                        </com:TActiveLinkButton>
                                    </div>
                                    <!--END VILLE-->

                                    <!--BEGIN ACTION-->
                                    <div class="col-md-2 text-right">
                                        <com:TPanel Visible="<%# (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste()))
                                                                    &&($this->TemplateControl->getMode() != 1 )%>">
                                            <span class="actions-moyen"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></span>
                                        </com:TPanel>
                                    </div>
                                    <!--END ACTION-->
                                </div>
                            </div>
                        </prop:HeaderTemplate>
                        <prop:ItemTemplate>
                            <div class="item_consultation list-group-item kt-callout left kt-vertical-align <%=$this->classTr->Value%> small">
                                <com:TPanel Visible="<%#$this->TemplateControl->getMode() == 1 %>">
                                    <div class="check-col">
                                        <com:TImage visible="<%#$this->TemplateControl->getPictoMonEtablissement($this->Data->getCodeEtablissement())  ? 'true' : 'false' %>"
                                                    Attributes.alt="<%#$this->TemplateControl->getAltMonEtablissement($this->Data->getCodeEtablissement())%>"
                                                    ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoMonEtablissement($this->Data->getCodeEtablissement())%>"/>
                                    </div>
                                </com:TPanel>

                                <!--BEGIN CHEXKBOX-->
                                <com:TPanel cssClass="col-md-1" Visible="<%#$this->TemplateControl->getMode() == 3 || $this->TemplateControl->getMode() ==4  %>">
                                    <div class="check-col">
                                        <com:TActiveRadioButton
                                                id="radio_monEtablissement"
                                                Attributes.title="<%=Prado::localize('TEXT_MON_ETABLISSEMENT')%>"
                                                UniqueGroupName="monEtablissement"
                                                value="floue"
                                                CssClass="radio"
                                                checked="<%#$this->Data->getCodeEtablissement() == $this->TemplateControl->getSiretMonEtablissement()%>"
                                                onCallBack="SourceTemplateControl.setSiretEtablissement"
                                                ActiveControl.CallBackParameter="<%#$this->Data->getCodeEtablissement()%>"

                                        >
                                            <prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
                                            <prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
                                            <prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>


                                        </com:TActiveRadioButton>
                                        <com:THiddenField Id="codeEtab" Value="<%#$this->Data->getCodeEtablissement()%>"/>
                                    </div>
                                </com:TPanel>
                                <!--NED CHEXKBOX-->

                                <!--BEGIN CODE ETABLISSEMENT-->
                                <div class="col-md-2">
                                    <com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>"
                                                Text="<%#$this->Data->getCodeEtablissement()%>"/>
                                </div>
                                <!--END CODE ETABLISSEMENT-->

                                <!--DEBUT INSCRIT SUR LES ANNUAIRES DES POTAILS DE LA DEFENSE-->
                                <div class="col-md-1">
                                    <com:TActiveImage
                                            visible="<%#$this->TemplateControl->getPictoInscritDefense($this->Data->getInscritAnnuaireDefense())  ? 'true' : 'false' %>"
                                            Attributes.alt="<%#$this->TemplateControl->getAltinscritDefense($this->Data->getInscritAnnuaireDefense())%>"
                                            Attributes.title="<%#$this->TemplateControl->getAltinscritDefense($this->Data->getInscritAnnuaireDefense())%>"
                                            ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoInscritDefense($this->Data->getInscritAnnuaireDefense())%>"
                                    />
                                </div>
                                <!--FIN INSCRIT SUR LES ANNUAIRES DES POTAILS DE LA DEFENSE-->

                                <!--BEGIN VOIE -->
                                <com:TPanel cssClass="col-md-3 clearfix"
                                            Visible="<%#( $this->TemplateControl->getMode() != 4  || ( (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && $this->TemplateControl->getEntrepriseExiste() )) )%>">
                                    <div class="pull-left">
                                        <span visible="<%# $this->Data->getEstSiege() ? 'true' : 'false' %>" data-toggle="tooltip" title="<%# Prado::localize('TEXT_SIEGE_SOCIAL') %>">
                                             <i class="fa fa-building-o m-r-1"></i>
                                        </span>
                                    </div>
                                    <div class="pull-left">
                                        <div class="">
                                            <com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>">
                                                <%#$this->Data->getAdresse2()%>
                                            </com:TLabel>
                                        </div>
                                        <div class="">
                                            <com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>">
                                                <%#$this->Data->getAdresse()%>
                                            </com:TLabel>
                                        </div>
                                    </div>
                                    <com:TPanel
                                            Visible="<%#$this->TemplateControl->getMode() == 4 && (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste() ))%>">
                                        <com:TActiveRadioButton
                                                id="radio_siegeSocial"
                                                Attributes.title="<%=Prado::localize('TEXT_SIEGE_SOCIAL')%>"
                                                UniqueGroupName="siegeSocial"
                                                value="floue"
                                                CssClass="radio"
                                                checked="<%#$this->Data->getCodeEtablissement() == $this->TemplateControl->getSiretSiegeSocial()%>"
                                                onCallBack="SourceTemplateControl.setSiretSiegeSocialEse"
                                                ActiveControl.CallBackParameter="<%#$this->Data->getCodeEtablissement()%>">
                                            <prop:ClientSide.OnLoading>showModalLoader();</prop:ClientSide.OnLoading>
                                            <prop:ClientSide.OnFailure>showModalLoader();</prop:ClientSide.OnFailure>
                                            <prop:ClientSide.OnSuccess>showModalLoader();</prop:ClientSide.OnSuccess>


                                        </com:TActiveRadioButton>
                                    </com:TPanel>
                                </com:TPanel>
                                <!--END VOIE -->

                                <!--BEGIN CP-->
                                <div class="col-md-1">
                                    <com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>">
                                        <%#$this->Data->getCodePostal()%>
                                    </com:TLabel>
                                </div>
                                <!--FIN CP-->

                                <!--BEGIN VILLE-->
                                <div class="col-md-2">
                                    <com:TLabel cssClass="<%# (new Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo())->getCssSaisieMnuelle($this->Data,Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>">
                                        <%#$this->Data->getVille()%>
                                    </com:TLabel>
                                </div>
                                <!--END VILLE-->

                                <!--BEGIN ACTION-->
                                <div class="col-md-2">
                                    <com:TPanel cssClass=" text-right"
                                                Visible="<%# (Application\Service\Atexo\Atexo_CurrentUser::isATES() || (!Application\Service\Atexo\Atexo_CurrentUser::isConnected() && !$this->TemplateControl->getEntrepriseExiste() ))&&($this->TemplateControl->getMode() != 1 )%>">
                                        <ul colspan="2" class="actions list-unstyled">
                                            <li>
                                                <com:THiddenField id="labelSiret" value="<%#$this->Data->getCodeEtablissement()%>"/>
                                            </li>
                                            <li class="m-b-1">
                                                <com:TActiveLinkButton
                                                        id="pictoModifier"
                                                        cssClass="btn btn-sm btn-primary"
                                                        Attributes.title="<%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>"
                                                        onCommand="SourceTemplateControl.chargerInfosEtablissement"
                                                        CommandParameter="<%#$this->Data->getCodeEtablissement().'#'.$this->pictoModifier->getClientId()%>"
                                                        Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->Action->getClientId()%>').value = 'Modify';
                                                                          document.getElementById('<%#$this->SourceTemplateControl->idToModify->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';">
                                                    <i class="fa fa-pencil"></i>
                                                </com:TActiveLinkButton>
                                                <com:TActiveLinkButton
                                                        id="pictoModifierInscriptionDefence"
                                                        cssClass="btn btn-sm btn-primary"
                                                        Visible="false"
                                                        Attributes.title="<%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%>"
                                                        onCommand="SourceTemplateControl.openModalModifyInscriptionDefence"
                                                        CommandParameter="<%#$this->Data->getCodeEtablissement().'#'.$this->pictoModifierInscriptionDefence->getClientId()%>"
                                                        Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->idToModifyDefence->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';">
                                                    <i class="fa fa-pencil"></i>
                                                </com:TActiveLinkButton>
                                            </li>
                                            <li>
                                                <com:TActiveLinkButton
                                                        Visible="<%#$this->Data->getSaisieManuelle() == '1' %>"
                                                        cssClass="btn btn-sm btn-danger"
                                                        Attributes.title="<%=Prado::localize('TEXT_SUPRIMER_ETABLISSEMENT')%>"
                                                        Attributes.OnClick="document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getCodeEtablissement()%>';
                                                                          document.getElementById('<%#$this->SourceTemplateControl->nameToDelete->getClientId()%>').innerHTML =  document.getElementById('<%#$this->labelSiret->getClientId()%>').value;
                                                                          KTJS.pages_entreprise_common.common.openModal('#modalConfirmationSuppression');">
                                                    <i class="fa fa-trash"></i>
                                                </com:TActiveLinkButton>
                                            </li>
                                        </ul>
                                    </com:TPanel>
                                </div>
                                <!--END ACTION-->
                            </div>
                            <com:THiddenField Id="classTr" value="<%#$this->TemplateControl->getClass($this->ItemIndex,$this->Data->getCodeEtablissement())%>"/>
                        </prop:ItemTemplate>
                    </div>
                </com:TActiveRepeater>

                <!--BEGIN PARTITIONNEUR ETABLISSEMENT BOTTOM-->
                <div class="line-partitioner" style="display: none;">
                    <div class="partitioner">
                        <div class="intitule">
                            <strong>
                                <com:TLabel ID="labelAfficherBottom">
                                    <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                                </com:TLabel>
                            </strong>
                        </div>
                        <com:TDropDownList ID="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
                            <com:TListItem Text="10" Selected="10"/>
                            <com:TListItem Text="20" Value="20"/>
                            <com:TListItem Text="50" Value="50"/>
                            <com:TListItem Text="100" Value="100"/>
                            <com:TListItem Text="500" Value="500"/>
                        </com:TDropDownList>
                        <div class="intitule">
                            <com:TLabel id="resultParPageBottom">
                                <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
                            </com:TLabel>
                        </div>
                        <label style="display:none;">
                            <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
                        </label>
                        <com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonTop" CssClass="float-left">
                            <com:TTextBox ID="pageNumberBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
                            <div class="nb-total ">
                                <com:TLabel ID="labelSlashBottom" Text="/" visible="true"/>
                                <com:TLabel ID="nombrePageBottom"/>
                                <com:TButton ID="DefaultButtonBottom" OnClick="goToPage" Attributes.style="display:none"/>
                            </div>
                        </com:TPanel>
                        <div class="liens">
                            <com:TPager ID="PagerBottom"
                                        ControlToPaginate="repeaterEtablissements"
                                        FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
                                        LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
                                        Mode="NextPrev"
                                        NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
                                        PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
                                        OnPageIndexChanged="pageChanged"/>
                        </div>
                    </div>
                </div>
                <!--END PARTITIONNEUR ETABLISSEMENT BOTTOM-->
            </div>
        </div>
        <!--BEGIN BTN AJOUTER ETABLISSEMENT -->
        <div class="clearfix">
            <com:TPanel cssClass="pull-right" Visible="<%= $this->getAjoutManuel() && $this->getOptionAdd()%> ">
                <com:TActiveLinkButton
                        id="ajouterEtablissement"
                        Visible="<%= $this->getMode() != 1 %>"
                        cssClass="btn btn-sm btn-primary "
                        onCommand="SourceTemplateControl.viderInfosEtablissement">
                    <i class="fa fa-plus m-r-1"></i>
                    <com:TTranslate>TEXT_AJOUTER_ETABLISSEMENT</com:TTranslate>
                </com:TActiveLinkButton>
            </com:TPanel>
            <com:TPanel cssClass="pull-right" Visible="<%= !$this->getAjoutManuel() && $this->getOptionAdd() %>">
                <com:TActiveLinkButton
                        id="ajouterEtablissementSGMAP"
                        Visible="<%= $this->getMode() != 1 %>"
                        cssClass="btn btn-sm btn-primary"
                        onCommand="SourceTemplateControl.viderInfosEtablissementSgmap">
                    <i class="fa fa-plus m-r-1"></i>
                    <com:TTranslate>TEXT_AJOUTER_ETABLISSEMENT</com:TTranslate>
                </com:TActiveLinkButton>
            </com:TPanel>
        </div>
        <!--END BTN AJOUTER ETABLISSEMENT -->
    </div>
</com:TActivePanel>

<!--Debut Modal modifier Etablissement-->
<com:TPanel id="panelDegradeEtab" cssClass="modal-etablissement" style="display:none;">
    <div class="" tabindex="-1" role="dialog" id="modalEtablissement">
        <div class="modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title"><%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%></div>
                </div>
                <div class="modal-body">
                    <com:TPanel ID="panelMessageAvertissement" Visible="<%=$this->getIsEntrepriseLocale() && Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSgmap')%>">
                        <com:PanelMessageAvertissement ID="messageAvertissement" message="<%=Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE')%>"/>
                    </com:TPanel>
                    <com:AtexoValidationSummary id="divValidationSummaryEtablissement" ValidationGroup="validateCreateEtablissement"/>

                    <!--Debut Formulaire-->
                    <div class="form-bloc">
                        <div class="content">
                            <div class="line">
                                <com:TRequiredFieldValidator
                                        ControlToValidate="codeEtablissement"
                                        ValidationGroup="validateCreateEtablissement"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_NC')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <com:TRegularExpressionValidator
                                        ID="formatCodeEtablissement"
                                        Enabled="<%= $this->getIsEntrepriseLocale()%>"
                                        ValidationGroup="validateCreateEtablissement"
                                        ControlToValidate="codeEtablissement"
                                        RegularExpression="[a-zA-Z0-9]{1,5}"
                                        ErrorMessage="<%= Prado::localize('TEXT_CODE_ETABLISSEMENT_INVALIDE') %>"
                                        Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRegularExpressionValidator>
                                <com:TCustomValidator
                                        id="validateIfCodeExiste"
                                        ValidationGroup="validateCreateEtablissement"
                                        ControlToValidate="codeEtablissement"
                                        Display="Dynamic"
                                        EnableClientScript="true"
                                        onServerValidate="validateIfCodeExiste"
                                        ErrorMessage="Existe D?j?"
                                        Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <com:TCustomValidator
                                        ID="SirenValidator"
                                        visible="<%=$this->getIsEntrepriseLocale() && Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseSiren')%>"
                                        ValidationGroup="validateCreateEtablissement"
                                        ControlToValidate="codeEtablissement"
                                        Display="Dynamic"
                                        EnableClientScript="true"
                                        ErrorMessage="<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
                                        ClientValidationFunction="validerCodeEtablissement"
                                        Text=" "
                                >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <script>
                                    function validerCodeEtablissement() {
                                        return controlValidationCodeEtab('<%=$this->getSirenEntreprise()%>', '<%=$this->codeEtablissement->getClientId()%>');
                                    }
                                </script>
                                <com:TPanel cssClass="form-group form-group-sm clearfix" Visible="<%= $this->getIsEntrepriseLocale() %>">
                                    <label class="col-md-3" for="codeEtablissement">
                                        <com:TTranslate>TEXT_NC</com:TTranslate>
                                        <span class="text-danger">* </span>
                                        <span class="m-0 p-0 m-l-1">
                                                    <a data-target="#" data-toggle="tooltip" data-placement="right"
                                                       title="Cinq derniers chiffres du SIRET"><i
                                                            class="fa fa-question-circle text-info"></i></a>
                                                </span>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="codeEtablissement" cssClass="form-control" MaxLength="5"/>
                                    </div>
                                </com:TPanel>
                                <!-- -->
                                <!-- -->
                                <div class="form-group form-group-sm clearfix">
                                    <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_EtablissementAddress1">
                                        <com:TTranslate>TEXT_ADRESSE</com:TTranslate>
                                        <span class="text-danger">* </span>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="EtablissementAddress1" cssClass="form-control"/>
                                    </div>
                                    <com:TRequiredFieldValidator
                                            ControlToValidate="EtablissementAddress1"
                                            ValidationGroup="validateCreateEtablissement"
                                            Display="Dynamic"
                                            ErrorMessage="<%=Prado::localize('TEXT_ADRESSE') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
                                            EnableClientScript="true"
                                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                        <prop:ClientSide.OnValidationError>
                                            document.getElementById('divValidationSummaryEtablissement').style.display='';
                                        </prop:ClientSide.OnValidationError>
                                    </com:TRequiredFieldValidator>
                                </div>
                                <!-- -->
                                <div class="form-group form-group-sm clearfix">
                                    <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_Etablissementaddress2">
                                        <com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="Etablissementaddress2" cssClass="form-control"/>
                                    </div>
                                </div>
                                <!-- -->
                                <com:TRequiredFieldValidator
                                        ControlToValidate="cpEtablissement"
                                        ValidationGroup="validateCreateEtablissement"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_CP') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <com:TRegularExpressionValidator
                                        ID="formatCp"
                                        ValidationGroup="validateCreateEtablissement"
                                        ControlToValidate="cpEtablissement"
                                        RegularExpression="[0-9]{5}"
                                        ErrorMessage="<%=Prado::localize('CONTROL_CODE_POSTAL') %>"
                                        Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRegularExpressionValidator>
                                <div class="form-group form-group-sm clearfix">
                                    <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_cpEtablissement">
                                        <com:TTranslate>TEXT_CP</com:TTranslate>
                                        <span class="text-danger">* </span>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="cpEtablissement" cssClass="form-control" MaxLength="5"/>
                                    </div>
                                </div>
                                <!-- -->

                                <com:TRequiredFieldValidator
                                        ControlToValidate="villeEtablissement"
                                        ValidationGroup="validateCreateEtablissement"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_VILLE') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <div class="form-group form-group-sm clearfix">
                                    <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_villeEtablissement">
                                        <com:TTranslate>TEXT_VILLE</com:TTranslate>
                                        <span class="text-danger">* </span>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="villeEtablissement" cssClass="form-control"/>
                                    </div>
                                </div>
                                <!-- -->
                                <com:TRequiredFieldValidator
                                        ControlToValidate="paysEtablissement"
                                        ValidationGroup="validateCreateEtablissement"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_PAYS_TERRITOIRES') .' '. Prado::localize('TEXT_DU_ETABLISSEMENT')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummaryEtablissement').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <div class="form-group form-group-sm clearfix">
                                    <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_paysEtablissement">
                                        <com:TTranslate>DEFINE_PAYS_TERRITOIRES</com:TTranslate>
                                        <span class="text-danger">* </span>
                                    </label>
                                    <div class="col-md-9">
                                        <com:TActiveTextBox ID="paysEtablissement" cssClass="form-control"/>
                                    </div>
                                </div>
                                <!-- -->

                                <com:TPanel CssClass="line" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
                                    <span class="intitule-auto">
                                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_inscriptionAnnuaire">
                                            <com:TActiveCheckBox
                                                    id="inscriptionAnnuaire"
                                                    Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"/>
                                            <com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
                                        </label>
                                    </span>
                                    <com:THyperLink NavigateUrl="#"
                                                    target="_blank"
                                                    CssClass="infos-plus"
                                                    ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
                                                    Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
                                        <com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
                                    </com:THyperLink>
                                </com:TPanel>
                                <!-- -->
                            </div>
                        </div>
                    </div>
                    <!--Fin Formulaire-->
                </div>
                <div class="modal-footer">
                    <div class="clearfix">
                        <com:THiddenField id="idToModify"/>
                        <com:THiddenField id="Action"/>
                        <input type="button"
                               class="btn btn-sm btn-default pull-left"
                               value="<%=Prado::localize('DEFINE_ANNULER')%>"
                               title="<%=Prado::localize('DEFINE_ANNULER')%>"
                               onclick="J('.modal-etablissement').dialog('close');return false;"
                        />
                        <com:TActiveButton
                                id="saveEtab"
                                Text = "<%=Prado::localize('TEXT_VALIDER')%>"
                                Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                                CssClass="btn btn-sm btn-primary pull-right"
                                ValidationGroup="validateCreateEtablissement"
                                onCommand="SourceTemplateControl.saveEtablissement"
                                CommandName="<%=$this->Action->value%>"
                                onCallBack="SourceTemplateControl.refreshRepeater"
                                CommandParameter="<%=$this->idToModify->value%>"
                        >
                        </com:TActiveButton >
                    </div>
                </div>
            </div>
        </div>
    </div>
</com:TPanel >
<!--Fin Modal modifier Etablissement-->

<!--DEBUT AJOUT ETABLISSEMENT FRANCAIS-->
<com:TPanel id="panelEtabSgmap" cssClass="modal-etablissementSgmap" style="display:none;">
    <div class="modal-content modal-lg" tabindex="-1" role="dialog" id="modalEtablissementSgmap">
        <div class="modal-header">
            <div class="modal-title"><%=Prado::localize('TEXT_MODIFIER_ETABLISSEMENT')%></div>
        </div>
        <div class="modal-body">
            <com:AtexoValidationSummary id="divValidationSummaryEtablissementSgmap" ValidationGroup="validateCreateEtablissementSgmap"/>
            <div class="form-group form-group-sm">
                <label class="col-md-3" for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_codeEtablissementSgmap">
                    <com:TTranslate>TEXT_NC</com:TTranslate>
                    <span class="text-danger">* </span>
                    <i class="fa fa-question-circle text-info" data-toggle="tooltip" title="Cinq derniers chiffres du SIRET"></i>
                </label>
                <div class="col-md-3">
                    <com:TActiveTextBox ID="codeEtablissementSgmap" cssClass="form-control" MaxLength="5"/>
                </div>
                <com:TRequiredFieldValidator
                        ControlToValidate="codeEtablissementSgmap"
                        ValidationGroup="validateCreateEtablissementSgmap"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('TEXT_NC')%>"
                        EnableClientScript="true"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TRequiredFieldValidator>
                <com:TRegularExpressionValidator
                        ID="formatCodeEtablissementSgmap"
                        ValidationGroup="validateCreateEtablissementSgmap"
                        ControlToValidate="codeEtablissementSgmap"
                        RegularExpression="[0-9]{5}"
                        ErrorMessage="<%=Prado::localize('TEXT_CODE_ETABLISSEMENT_INVALIDE') %>"
                        Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TRegularExpressionValidator>
                <com:TCustomValidator
                        ID="SirenValidatorSgmap"
                        ValidationGroup="validateCreateEtablissementSgmap"
                        ControlToValidate="codeEtablissementSgmap"
                        Display="Dynamic"
                        EnableClientScript="true"
                        ErrorMessage="<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
                        ClientValidationFunction="validerCodeEtablissementSgmap"
                        Text=" "
                >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('divValidationSummaryEtablissementSgmap').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCustomValidator>
                <script>
                    function validerCodeEtablissementSgmap() {
                        return controlValidationCodeEtab('<%=$this->getSirenEntreprise()%>', '<%=$this->codeEtablissementSgmap->getClientId()%>');
                    }
                </script>
            </div>
            <div class="clearfix">
                <com:TPanel ID="panelInscrisDefense" CssClass="clearfix" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
                    <div class="checkbox pull-left">
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_inscriptionAnnuaireSgmap" data-toggle="tooltip" title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>">
                            <com:TActiveCheckBox id="inscriptionAnnuaireSgmap"/>
                            <com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
                        </label>
                    </div>
                    <div class="pull-left p-t-1" data-toggle="tooltip" title="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>">
                        <com:THyperLink NavigateUrl="#"
                                        target="_blank"
                                        CssClass="infos-plus m-l-1"
                                        Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
                            <com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
                        </com:THyperLink>
                    </div>
                </com:TPanel>
            </div>
        </div>
        <div class="modal-footer">
            <div class="clearfix">
                <com:THiddenField id="idToModifySgmap"/>
                <input type="button"
                       class="btn btn-sm btn-default pull-left"
                       value="<%=Prado::localize('DEFINE_ANNULER')%>"
                       title="<%=Prado::localize('DEFINE_ANNULER')%>"
                       onclick="J('.modal-etablissementSgmap').dialog('close');return false;"
                />
                <com:TActiveButton
                        id="saveEtabSgmap"
                        Text = "<%=Prado::localize('TEXT_VALIDER')%>"
                        Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                        CssClass="btn btn-sm btn-primary pull-right"
                        ValidationGroup="validateCreateEtablissementSgmap"
                        onCommand="SourceTemplateControl.saveEtablissementSgmap"
                        onCallBack="SourceTemplateControl.refreshRepeater"
                >
                </com:TActiveButton >
            </div>
        </div>
    </div>
</com:TPanel>
<!--FIN AJOUT ETABLISSEMENT FRANCAIS-->

<!-- Debut Modal modif inscription defence-->
<com:TPanel id="panelEtabDefence" cssClass="modal-etablissementDefence" style="display:none;">
    <div class="" tabindex="-1" role="dialog" id="modalEtablissementDefence">
        <div class="modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">Modifier l'&eacute;tablissement</div>
                </div>
                <div class="modal-body">
                    <!--Debut Formulaire-->
                    <div class="form-group form-group-sm">
                        <label class="col-md-3">
                            <com:TTranslate>TEXT_NC</com:TTranslate>
                        </label>
                        <div class="col-md-9">
                            <com:TActiveLabel ID="codeEtablissementDefence"/>
                        </div>
                    </div>
                    <com:TPanel ID="panelDefense" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PortailDefenseEntreprise')%>">
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_etablissements_inscriptionDefence">
                            <com:TActiveCheckBox
                                    id="inscriptionDefence"
                                    Attributes.title="<%=Prado::localize('JE_M_INSCRIS_DEFENSE')%>"/>
                            <com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
                        </label>
                        <com:THyperLink NavigateUrl="#"
                                        target="_blank"
                                        CssClass="infos-plus"
                                        ToolTip="<%=Prado::localize('EN_SAVOIR_PLUS_DEFENSE')%>"
                                        Attributes.onclick="popUp('index.php?page=Entreprise.EntreprisePopupInfosPortailsDefense','yes');return false;">
                            <com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
                        </com:THyperLink>
                    </com:TPanel>
                    <!--Fin Formulaire-->
                </div>
                <div class="modal-footer">
                    <!--Debut line boutons-->
                    <div class="clearfix">
                        <com:THiddenField id="idToModifyDefence"/>
                        <input type="button"
                               class="btn btn-sm btn-default pull-left"
                               value="<%=Prado::localize('DEFINE_ANNULER')%>"
                               title="<%=Prado::localize('DEFINE_ANNULER')%>"
                               onclick="J('.modal-etablissementDefence').dialog('close');return false;"
                        />
                        <com:TActiveButton
                                id="saveEtabDefence"
                                Text = "<%=Prado::localize('TEXT_VALIDER')%>"
                                Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                                CssClass="btn btn-sm btn-primary pull-right"
                                onCommand="SourceTemplateControl.saveInscriptionDefence"
                                CommandParameter="<%=$this->idToModifyDefence->value%>"
                                onCallBack="SourceTemplateControl.refreshRepeater"
                        >
                        </com:TActiveButton >
                    </div>
                    <!--Fin line boutons-->
                </div>
            </div>
        </div>
    </div>
</com:TPanel>
<!-- Fin Modal modif inscription defence -->

<!--Debut Modal Confirmation-Suppression-->
<div class="modal-confirmation-suppression">
    <div class="modal fade" tabindex="-1" role="dialog" id="modalConfirmationSuppression">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <com:TTranslate>TEXT_SUPRIMER_ETABLISSEMENT</com:TTranslate>
                    </div>
                </div>
                <div class="modal-body">
                    <com:TPanel ID="panelMessageDelete" style="display:none;">
                        <div class="alert alert-danger">
                            <com:PanelMessageAvertissement ID="messageAvertissementdelete" message="<%=Prado::localize('ALERTE_SUPPRESSION_ETABLISSEMENT_IMPOSSIBLE')%>"/>
                        </div>
                    </com:TPanel>
                    <!--Debut Formulaire-->
                    <com:TPanel ID="panelConfirmDelete" cssClass="form-bloc" style="display:'';">
                        <div class="content">
                           <span class="text-danger">
                               <strong>
                                   <com:TTranslate>TEXT_ALERTE_SUPPRESSION_ETABLISSEMENT</com:TTranslate>
                               </strong>
                           </span>
                            <strong>
                                <com:TActiveLabel ID="nameToDelete" text=""/>
                            </strong>
                        </div>
                    </com:TPanel>
                    <!--Fin Formulaire-->
                </div>
                <div class="modal-footer">
                    <!--Debut line boutons-->
                    <div class="clearfix">
                        <com:THiddenField id="idToDetele"/>
                        <button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
                        <com:TActiveButton
                                id="cofirmSupression"
                                Text="<%=Prado::localize('TEXT_POURSUIVRE')%>"
                                CssClass="btn btn-sm btn-primary pull-right"
                                onCommand="SourceTemplateControl.deleteEtablissement"
                                onCallBack="SourceTemplateControl.refreshRepeater"
                                CommandParameter="<%=$this->idToDetele->value%>"
                        >
                        </com:TActiveButton>

                        <com:TActiveButton
                                id="buttonClose"
                                Text="<%=Prado::localize('FERMER')%>"
                                Attributes.title="<%=Prado::localize('FERMER')%>"
                                CssClass="bouton-moyen float-right"
                                onCommand="SourceTemplateControl.resetModalDelet"
                                style="display:none;"
                        >
                        </com:TActiveButton>
                    </div>
                    <!--Fin line boutons-->
                </div>
            </div>
        </div>
    </div>
</div>
<com:TActiveLabel id="scriptJs" style="display:none;"/>
<!--Fin Modal  Confirmation-Suppression-->
