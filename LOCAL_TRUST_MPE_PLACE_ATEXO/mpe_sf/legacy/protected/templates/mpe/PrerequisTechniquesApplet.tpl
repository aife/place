<!--Debut colonne droite-->
<div class="main-part" id="main-part">
    <div class="breadcrumbs">&gt; <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate></div>
    <!--Debut Bloc Conditions utilisation-->
    <div class="form-field">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
        <div class="main-text margin-top-3">
            <span class="message-big" id="rubrique_2"><com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate></span>
            <p><strong><com:TTranslate>UTILISATEURS_DOIVENT_SE_CONFORMER_AUX_PREREQUS</com:TTranslate> :</strong></p>
            <div class="bloc-faq">
                <ul>
                    <li><a id="rubrique_2_1" href="#rubrique_2_paragraphe_1"><com:TTranslate>CONNECTIQUE_CONFIGURATION_RESEAU</com:TTranslate></a></li>
                    <li><a id="rubrique_2_2" href="#rubrique_2_paragraphe_2"><com:TTranslate>CONFIGURATION_POSTE_TRAVAIL</com:TTranslate></a></li>
                    <li><a id="rubrique_2_3" href="#rubrique_2_paragraphe_3"><com:TTranslate>SYSTEM_EXPLOITATION_NAVIGATEUR</com:TTranslate></a></li>
                    <li><a id="rubrique_2_4" href="#rubrique_2_paragraphe_4"><com:TTranslate>FORMAT_CERTIFICAT_NUMERIQUE</com:TTranslate></a></li>
                    <li><a id="rubrique_2_5" href="#rubrique_2_paragraphe_5"><com:TTranslate>VERSION_ENV_JAVA</com:TTranslate></a></li>
                    <li><a id="rubrique_2_6" href="#rubrique_2_paragraphe_6"><com:TTranslate>DEFINE_PROBLEME_INCOMPATIBILITES</com:TTranslate></a></li>
                    <li><a id="rubrique_2_7" href="#rubrique_2_paragraphe_7"><com:TTranslate>DEFINE_RECOMMANDATIONS_AUX_ENTREPRISES</com:TTranslate></a></li>
                </ul>
            </div>
            <!--Debut bloc Test de configuration-->
            <div class="form-bloc margin-0" id="configuration-test">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <h3><com:TTranslate>CONFIG_POSTE</com:TTranslate></h3>
                    <p><com:TTranslate>OUTIL_MIS_A_DISPOSITION_POUR_TESTER_CONFIGURATION</com:TTranslate></p>
                    <div class="spacer-mini"></div>
                    <div class="float-left"><a href="index.php?page=<%= $_GET['calledFrom'] == 'agent' ? 'Commun' : 'Entreprise'%>.DiagnosticPoste&callFrom=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['calledFrom'])%>" class="bouton-test-config-160" title="<%=Prado::localize('TESTER_MA_CONFIGURATION')%>"><com:TTranslate>TESTER_MA_CONFIGURATION</com:TTranslate></a></div>
                    <div class="breaker"></div>
                    <div class="breaker"></div>
                </div>
                 <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
            <div class="spacer-small"></div>
            <!--Fin bloc Test de configuration-->
             <!--Debut bloc Consultation de test-->
            <com:TPanel cssClass="form-bloc margin-0" visible="<%= ! ($this->isAgent())%>">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <h3><com:TTranslate>CONSULTATION_DE_TEST</com:TTranslate></h3>
                    <p><com:TTranslate>CONSULTATION_DE_TEST_MISE_A_DISPOSITION</com:TTranslate><br /><com:TTranslate>TEST_CYCLE_ENTIER_REPONSE</com:TTranslate></p>
                    <div class="spacer-mini"></div>
                    <div class="float-left"><a href="?page=Entreprise.EntrepriseAdvancedSearch&AllCons&orgTest" class="bouton-arrow-long-120 right" title="<%=Prado::localize('CONSULTATION_DE_TEST')%>"><com:TTranslate>CONSULTATION_DE_TEST</com:TTranslate></a> </div>
                    <div class="breaker"></div>
                    <div class="breaker"></div>
                </div>
                 <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </com:TPanel>
            <!--Fin bloc Consultation de test-->

         </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
     </div>
     <!--Fin bloc Conditions utilisation-->
    <!--Debut Bloc Pre-requis techniques-->
    <div class="main-text margin-top-3">
                <h3 id="rubrique_2_paragraphe_1"><com:TTranslate>CONNECTIQUE_CONFIGURATION_RESEAU</com:TTranslate></h3>
                <ul class="liste-actions">
                    <li><com:TTranslate>ACCES_INTERNET_HAUT_DEBIT_REQUIS</com:TTranslate></li>
                    <li><com:TTranslate>ENVIRONNEMENT_RESEAU</com:TTranslate>
                        <ul>
                            <li><com:TTranslate>ENVIRONNEMENT_RESEAU_AUTORISER_HTTPS</com:TTranslate></li>
                            <li><com:TTranslate>ENVIRONNEMENT_RESEAU_AUTORISER_TELECHARGEMENT_FICHIER</com:TTranslate></li>
                        </ul>
                    </li>
                </ul>
                <div class="link-line"> <a href="#rubrique_2" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
                <div class="breaker"></div>
                <h3 id="rubrique_2_paragraphe_2"><com:TTranslate>CONFIGURATION_POSTE_TRAVAIL</com:TTranslate></h3>
                <ul class="liste-actions">
                    <li><com:TTranslate>GESTION_COOKIES_SESSION</com:TTranslate></li>
                    <li><com:TTranslate>ENVIRONNEMENT_JAVA_INSTALLE</com:TTranslate></li>
                    <li><com:TTranslate>GESTION_FONCTION_CRYPTOGRAPHIQUE</com:TTranslate>
                        <ul>
                            <li><com:TTranslate>GESTION_CERTIFICAT_NUMERIQUE_AUTORISEE</com:TTranslate></li>
                            <li><com:TTranslate>UTILISATION_NAVIGATEUR_STANDARD</com:TTranslate></li>
                        </ul>
                    </li>
                </ul>
                <div class="link-line"> <a href="#header" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
                <div class="breaker"></div>
                <h3 id="rubrique_2_paragraphe_3"><com:TTranslate>SYSTEM_EXPLOITATION_NAVIGATEUR</com:TTranslate></h3>
                <p><com:TTranslate>AVOIR_UN_DES_OS_SUIVANTS</com:TTranslate></p>
                <ul class="liste-actions">
                    <li><com:TTranslate>MICROSOFT_WINDOWS_SEVEN</com:TTranslate></li>
                    <li><com:TTranslate>MICROSOFT_WINDOWS_8</com:TTranslate></li>
                    <li><com:TTranslate>MICROSOFT_WINDOWS_10</com:TTranslate></li>
                    <li><com:TTranslate>APPLE_MAC_OSX</com:TTranslate></li>
                    <li><com:TTranslate>LINUX_UBUNTU</com:TTranslate></li>
                </ul>
                <div class="spacer-small"></div>
                <p><com:TTranslate>POSTE_TRAVAIL_UTILISANT_NAVIGATEUR_SUIVANT</com:TTranslate> :</p>
                <ul class="liste-actions" style="display:<%=($this->isAgent())?'none':''%>">
                    <li ><com:TTranslate>NAVIGATEUR_PRE_REQUIS_ENT_APPLET</com:TTranslate></li>
                </ul>
                <ul class="liste-actions" style="display:<%=($this->isAgent())?'':'none'%>">
                    <li ><com:TTranslate>NAVIGATEUR_PRE_REQUIS_AGENT_APPLET</com:TTranslate></li>
                </ul>
                <div class="spacer-small"></div>
                <h3 id="rubrique_2_paragraphe_4"><com:TTranslate>FORMAT_CERTIFICAT_NUMERIQUE</com:TTranslate></h3>
                <p><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES</com:TTranslate></p>
                <ul class="liste-actions">
                    <li><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES_DANS_MAGASIN_CERTIFICAT</com:TTranslate></li>
                    <li><com:TTranslate>CERTIFICAT_UTILISITATEURS_ACCEESIBLES_AU_FORMAT_P12</com:TTranslate></li>
                </ul>
                <div class="link-line"> <a href="#rubrique_2" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
                <div class="breaker"></div>
                <h3 id="rubrique_2_paragraphe_5"><com:TTranslate>VERSION_ENV_JAVA</com:TTranslate></h3>
                <p>
                    <com:TTranslate>APPLET_REQUIERT_JRE</com:TTranslate>
                    <br/>
                    <br />
                    <com:TTranslate>POSTE_TRAVAIL_EQUIPE_JRE</com:TTranslate>
                    <br/>
                    <com:TTranslate>POSTE_TRAVAIL_EQUIPE_JRE_MISE_A_JOUR</com:TTranslate>
                    <br/>
                    <br/>
                    <com:TTranslate>MISE_A_JOUR_JAR</com:TTranslate>
                    <br/>
                </p>
                <ul class="liste-actions">
                    <li><com:TTranslate>NAVIGATEUR_DROIT_ECRITURE_DOSSIER_JRE</com:TTranslate></li>
                    <li><com:TTranslate>NAVIGATEUR_N_A_PAS_DROIT_ECRITURE_DOSSIER_JRE</com:TTranslate></li>
                </ul>
                <p>
                    <br />
                    <com:TTranslate>APRES_INSTALLATION_REDEMARRER_NAVIGATEUR</com:TTranslate>
                </p>
                <com:TLinkButton
                    OnClick="onOKClick"
                    Visible="<%=(Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id']) && Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']))?true:false%>"
                    >
                    <com:TTranslate>TEXT_RETOUR_CONSULTATION</com:TTranslate>
                </com:TLinkButton>
                <div class="link-line"> <a href="#rubrique_2" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>

                <h3 id="rubrique_2_paragraphe_6"><com:TTranslate>DEFINE_PROBLEME_INCOMPATIBILITES</com:TTranslate></h3>
                <p>
                    <com:TTranslate>DEFINE_PROBLEME_JAVA_CACHE</com:TTranslate>
                </p>
                <p>
                    <com:TTranslate>DEFINE_FICHIER_RAR_DECOMPRESSION</com:TTranslate>
                </p>
                <div class="link-line"> <a href="#rubrique_2" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
                <h3 id="rubrique_2_paragraphe_7"><com:TTranslate>DEFINE_RECOMMANDATIONS_AUX_ENTREPRISES</com:TTranslate></h3>
                <ul class="liste-actions">
                    <li><com:TTranslate>RECOMMANDATION_TESTER_CONFIG_POSTE</com:TTranslate></li>
                    <li><com:TTranslate>RECOMMANDATION_VERFIER_CERTIFICAT</com:TTranslate></li>
                    <li><com:TTranslate>RECOMMANDATION_POUR_SIGNATURE</com:TTranslate></li>
                    <li><com:TTranslate>RECOMMANDATION_NOMMAGE_FICHIERS</com:TTranslate></li>
                    <li><com:TTranslate>RECOMMANDATION_POUR_COMPRESSER_FICHIER</com:TTranslate></li>
                </ul>
                <div class="link-line"> <a href="#rubrique_2" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>PREREQUIS_TECHNIQUE</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
                <div class="breaker"></div>
            </div>
     <!--Fin bloc Pre-requis techniques-->
    <div class="breaker"></div>
</div>
<!--Fin colonne droite-->