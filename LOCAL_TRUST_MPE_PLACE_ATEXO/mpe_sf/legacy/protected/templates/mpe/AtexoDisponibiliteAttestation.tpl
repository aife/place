<div class="statut-doc">
	<com:TRepeater ID="repeaterDispoAttestations">
		<prop:ItemTemplate>
			<com:THyperLink
				ID="linkLieuExe1"
				NavigateUrl="<%#$this->SourceTemplateControl->getDlAttestationUrl($this->Data->getIdTypeDocument(),$this->SourceTemplateControl->getIdEntreprise())%>"
			><com:TImage
					CssClass="<%#$this->SourceTemplateControl->getClassPicto($this->Data->getIdTypeDocument())%>"
					ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%=$this->SourceTemplateControl->getPictoByType($this->Data->getIdTypeDocument())%>"
					Attributes.alt="<%#$this->SourceTemplateControl->getAltAttestation($this->Data)%>"
				/></com:THyperLink>
		</prop:ItemTemplate>
	</com:TRepeater>
	+ <a href="<%=$this->getUrlFicheFournisseur()%>" title="<%=$this->getTitreLiensAttestationsSupplementairesEntreprise($this->getIdEntreprise())%>"><%=$this->getNbrDernieresVersionsDocSecondairesParEntreprise($this->getIdEntreprise())%></a>
</div>
