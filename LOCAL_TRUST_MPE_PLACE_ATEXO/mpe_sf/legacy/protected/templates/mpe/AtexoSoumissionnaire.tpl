<div class="separator"></div>
<div class="line">
    <div class="intitule-150"><com:TTranslate>CONTACT_PRINCIPAL_CONTRAT</com:TTranslate> :</div>
    <div class="content-bloc bloc-570">
        <div><img alt="<%=Prado::localize('CONTACT')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-user-agent.png"><strong>
            <com:TActiveLabel id="contact"/>
        </strong></div>
        <div>
            <com:TActiveLabel id="adresse"/>
            <com:TImage id="estSiege" attributes.alt="<%=Prado::localize('TEXT_SIEGE_SOCIAL')%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-siege-social.png"/>
            &nbsp;&nbsp;[<com:TTranslate>TEXT_SIRET</com:TTranslate> : <com:TLabel id="siret"/> ]
        </div>
        <div>
            <com:TImage id="imgEmail" attributes.alt="<%=Prado::localize('EMAIL')%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" />
            <com:TActiveLabel id="email"/>
            <com:TImage id="imgTelephone" attributes.alt="<%=Prado::localize('TEXT_TELEPHONE')%>" cssclass="margin-left-15"
                            ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" />
            <com:TActiveLabel id="telephone"/>
            <com:TActiveImage id="imgFax" attributes.alt="<%=Prado::localize('TEXT_FAX')%>" cssclass="margin-left-15"
                        ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" />
            <com:TActiveLabel id="faxe"/>
        </div>
    </div>
    <div class="intitule-20">
        <com:TActiveImageButton
                ID="updateContact"
                onclick="LoadDataSoumissionaireForUpdate"
                ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif"
                Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER')%>"
                Attributes.alt="<%=Prado::localize('DEFINE_TEXT_EDITER')%>"
                Attributes.data-toggle="modal" Attributes.data-target="#modalUpdateContactPrincipal"/>

        <com:TActiveLabel id="scriptLable" />
        <div class="modal fade" id="modalUpdateContactPrincipal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h1 class="modal-title" id="modalLabel"><com:TTranslate>MODIFIER_CONTACT_PRINCIPAL_CONTRAT</com:TTranslate></h1>
                    </div>
                    <div class="modal-body">
                        <com:AtexoValidationSummary id="divValidationSummaryAttributaire"  ValidationGroup="validateCreateAttributaire" />
                        <com:AtexoValidationSummary id="divValidationSummaryAddAttributaire"  ValidationGroup="validateDonneesAttributaire" />
                        <div class="form-bloc bloc-contrat margin-0">
                            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                            <div class="content">
                                <h2><com:TTranslate>CONTACT</com:TTranslate></h2>
                                <div class="spacer-small"></div>
                                <div class="bloc-50pourcent">
                                    <!-- -->
                                    <div class="line">
                                        <div class="intitule-150">
                                            <label for="nom"><com:TTranslate>NOM</com:TTranslate></label><span class="champ-oblig">* </span> :
                                        </div>
                                        <com:TActiveTextBox ID="nom" Attributes.title="<%=Prado::localize('NOM')%>" CssClass="input-185" />
                                        <com:TRequiredFieldValidator
                                                ControlToValidate="nom"
                                                ValidationGroup="validateCreateAttributaire"
                                                Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('NOM')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TRequiredFieldValidator>
                                    </div>
                                    <!-- -->
                                    <div class="line">
                                        <div class="intitule-150">
                                            <label for="prenom"><com:TTranslate>PRENOM</com:TTranslate></label><span class="champ-oblig">* </span> :
                                        </div>
                                        <com:TActiveTextBox ID="prenom" Attributes.title="<%=Prado::localize('PRENOM')%>" CssClass="input-185" />
                                        <com:TRequiredFieldValidator
                                                ControlToValidate="prenom"
                                                ValidationGroup="validateCreateAttributaire"
                                                Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('PRENOM')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TRequiredFieldValidator>
                                    </div>
                                    <!-- -->
                                    <div class="line">
                                        <div class="intitule-150">
                                            <label for="mail"><com:TTranslate>ADRESSE_ELECTRONIQUE</com:TTranslate></label> <span class="champ-oblig">* </span> :
                                        </div>
                                        <com:TActiveTextBox ID="mail" Attributes.title="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" CssClass="input-185" />

                                        <com:TCustomValidator
                                                ValidationGroup="validateCreateAttributaire"
                                                ControlToValidate="mail"
                                                ErrorMessage="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ClientValidationFunction="validatorEmail"
                                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                    </div>
                                    <!-- -->
                                </div>
                                <div class="bloc-50pourcent">
                                    <div class="line">
                                        <div class="intitule-150">
                                            <label for="tel"><com:TTranslate>DEFINE_TEL</com:TTranslate></label> <span class="champ-oblig">* </span> :
                                        </div>
                                        <com:TActiveTextBox ID="tel" Attributes.title="<%=Prado::localize('DEFINE_TEL')%>" CssClass="input-185"/>
                                        <com:TCustomValidator
                                                ValidationGroup="validateCreateAttributaire"
                                                ControlToValidate="tel"
                                                ErrorMessage="<%=Prado::localize('DEFINE_TEL')%>"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ClientValidationFunction="validerNombreCaracteresNumTelFaxObligatoire"
                                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                    </div>
                                    <!-- -->
                                    <div class="line">
                                        <div class="intitule-150">
                                            <label for="fax"><com:TTranslate>TEXT_FAX</com:TTranslate></label> :
                                        </div>
                                        <com:TActiveTextBox ID="fax" Attributes.title="<%=Prado::localize('TEXT_FAX')%> " CssClass="input-185"/>
                                        <com:TCustomValidator
                                                ValidationGroup="validateCreateAttributaire"
                                                ControlToValidate="fax"
                                                ErrorMessage="<%=Prado::localize('TEXT_FAX')%>"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                    </div>
                                    <!-- -->
                                </div>
                            </div>
                            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                        </div>
                        <!--Fin bloc Attributaire-->
                    </div>
                    <div class="modal-footer">
                        <com:TActiveButton
                                id="annulerContactPrincipal"
                                Text = "<%=Prado::localize('DEFINE_ANNULER')%>"
                                Attributes.title="<%=Prado::localize('DEFINE_ANNULER')%>"
                                CssClass="bouton-moyen float-left"
                                Attributes.data-dismiss="modal"
                        />

                        <com:TActiveButton
                                id="saveContactPrincipal"
                                Text = "<%=Prado::localize('TEXT_VALIDER')%>"
                                Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                                CssClass="bouton-moyen float-right"
                                ValidationGroup="validateCreateAttributaire"
                                onclick="saveContactPrincipal"
                        >
                            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                            <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                            <prop:ClientSide.OnSuccess>showLoader();J('#<%=$this->annulerContactPrincipal->getClientId()%>').click();</prop:ClientSide.OnSuccess>
                        </com:TActiveButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
