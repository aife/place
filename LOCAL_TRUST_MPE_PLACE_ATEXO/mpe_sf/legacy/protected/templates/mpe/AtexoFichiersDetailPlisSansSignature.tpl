<div class="form-field">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h3><%=$this->getTitre()%></h3>
        <div class="table-bloc table-bloc-detail">
            <com:TRepeater ID="repeaterFichiers"   EnableViewState="true" AllowCustomPaging="true">
                <prop:HeaderTemplate>
                    <table class="table-results tableau-detail tableau-detail-pli" summary="<%=Prado::localize('DEFINE_CANDIDATURE_FICHIERS_CONSTITUANT_DOSSIER')%>">
                        <caption><com:TTranslate>FICHIER_CONSTITUANT_DOSSIER</com:TTranslate></caption>
                        <thead>
                        <tr>
                            <th colspan="1" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
                        </tr>
                        <tr>
                            <th id="cand_fichierSoummissionnaire" >
                                <com:TTranslate>TEXT_FICHIER_ENVOYE_SOUMISSIONNAIRE</com:TTranslate>
                            </th>
                        </tr>
                        </thead>
                </prop:HeaderTemplate>
                <prop:ItemTemplate >
                    <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
                        <td class="col-150" headers="cand_fichierSoummissionnaire">
                            <com:TLabel Visible="<%#($this->Data->getIdBlob() && $this->Page->_enveloppeOuverte)? true:false%>"	>
                                <com:THyperLink Attributes.title="<%#$this->SourceTemplateControl->getLienTitle($this->Data)%>"
                                                NavigateUrl="index.php?page=Agent.downloadFichierEnveloppe&id=<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&idfichier=<%#$this->Data->getIdFichier()%>&enveloppe=<%#$this->Data->getIdEnveloppe()%>">
                                    <%#utf8_encode($this->Data->getNomFichier())%>
                                </com:THyperLink>
                            </com:TLabel>
                            <com:TLabel Visible="<%#($this->Data->getIdBlob() && $this->Page->_enveloppeOuverte)? false:true%>"	>
                                <%#utf8_encode($this->Data->getNomFichier())%>
                            </com:TLabel>
                        </td>
                    </tr>
                </prop:ItemTemplate>
            </com:TRepeater>
            </table>
        </div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
