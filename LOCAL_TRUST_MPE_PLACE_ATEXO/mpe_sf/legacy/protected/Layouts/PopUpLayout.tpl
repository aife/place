<!DOCTYPE html>
<html lang="<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>" class="popuplayout">
<com:THead title="<%= (Prado::localize($this->Page->pageTitle)) %>">
    <com:TConditional Condition="$_GET['page']=='Agent.popUpGestionBicle'">
        <prop:TrueTemplate>
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
        </prop:TrueTemplate>
        <prop:FalseTemplate>
            <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        </prop:FalseTemplate>
    </com:TConditional>

<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
<meta http-equiv="Content-Type" content="text/html; charset=<%= $this->getParameter('HTTP_ENCODING')%>" />
<com:TStyleSheet PradoStyles="bootstrap, jquery-ui" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/toggle-switch.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/chosen-gwt.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/vue-multiselect.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/chosen.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/font-awesome.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui-noscript.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/bootstrap-custom.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" media="all" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/loader.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />

    <com:TConditional Condition="$_GET['page']!='Agent.popUpGestionBicle'">
        <prop:TrueTemplate>
<script type="text/javascript">
window.addEventListener("load", function(event) {
    var opt = new OptionTransfer("list1", "list2");
    opt.setAutoSort(true);
    opt.setDelimiter(",");
    opt.saveRemovedLeftOptions("removedLeft");
    opt.saveRemovedRightOptions("removedRight");
    opt.saveAddedLeftOptions("addedLeft");
    opt.saveAddedRightOptions("addedRight");
    opt.saveNewLeftOptions("newLeft");
    opt.saveNewRightOptions("newRight");
});
</script>
        </prop:TrueTemplate>
    </com:TConditional>
<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/print.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" media="print" />
    <com:TConditional Condition="preg_match('/(^agent)/i', $_GET['page']) || preg_match('/(^agent)/i', $_GET['calledFrom']) || preg_match('/(^gestionPub)/i', $_GET['page']) || preg_match('/(^gestionPub)/i', $_GET['calledFrom']) || preg_match('/(^Commun.popUpListePj)/i', $_GET['page']) || preg_match('/(^Entreprise.popUpListePj)/i', $_GET['page'])  || preg_match('/(^Agent.popUpListePj)/i', $_GET['calledFrom']) || preg_match('/(^Agent.popUpListePj)/i', $_GET['calledFrom'])">
        <prop:TrueTemplate>
            <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/styles-mpe.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
            <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/styles.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
            <link rel="stylesheet" href="<%=$this->themesPath()%>/css/mpe-new-client.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
        </prop:TrueTemplate>
        <prop:FalseTemplate>
            <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style-mpe.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
            <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
        </prop:FalseTemplate>
    </com:TConditional>
    <com:VBscripts />
</com:THead>
<body id="popup">



<com:TForm Method="post" Attributes.name="main_form" Attributes.autocomplete="on">
    <com:JavascriptsTopIncludes id="jsincludes"/>

    <com:TConditional Condition="preg_match('/(^agent)/i', $_GET['page']) || preg_match('/(^agent)/i', $_GET['calledFrom']) || preg_match('/(^gestionPub)/i', $_GET['page']) || preg_match('/(^gestionPub)/i', $_GET['calledFrom']) || preg_match('/(^Commun.popUpListePj)/i', $_GET['page']) || preg_match('/(^Entreprise.popUpListePj)/i', $_GET['page'])  || preg_match('/(^Agent.popUpListePj)/i', $_GET['calledFrom']) || preg_match('/(^Agent.popUpListePj)/i', $_GET['calledFrom'])">
        <prop:TrueTemplate>
            <com:TConditional Condition="preg_match('/(^agent.PopupAccesRedaction)/i', $_GET['page'])">
                <prop:FalseTemplate>
                     <com:AtexoUtah id="atexoUtah" calledFrom="agent"/>
                </prop:FalseTemplate>
            </com:TConditional>
        </prop:TrueTemplate>
        <prop:FalseTemplate>
            <com:AtexoUtah id="atexoUtah" />
        </prop:FalseTemplate>
    </com:TConditional>

	<com:TContentPlaceHolder ID="CONTENU_PAGE" />
</com:TForm>
<!-- PopUpLayout -->
<script language="JavaScript" type="text/JavaScript" src="<%=$this->themesPath()%>/js/popup.inc.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"></script>
<script language="JavaScript" type="text/JavaScript" src="<%=$this->themesPath()%>/js/ng-script.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" charset="UTF-8"></script>

<%%
    echo htmlspecialchars_decode($this->getParameter("SCRIPT_JS_CLIENT"));
%>
<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntitiesDecode(Application\Service\Atexo\Atexo_Config::getParameter('PIWIK_SCRIPT'))%>

</body>
</html>

