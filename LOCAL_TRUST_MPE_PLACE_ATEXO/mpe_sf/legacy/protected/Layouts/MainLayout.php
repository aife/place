<?php

namespace Application\Layouts;

use App\Service\RgpdService;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Mobile;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

class MainLayout extends MpeTPage
{
    private array $_roles = [];
    public string $_calledFrom = '';

    public const CALLED_FROM_AGENT = 'agent';
    public const CALLED_FROM_ENTREPRISE = 'entreprise';
    public const CALLED_FROM_ADMIN = 'admin';
    public const CALLED_FROM_SOCLE_INTERNE = 'socleInterne';
    public const CALLED_FROM_AGENT_HELIOS = 'agenthelios';
    public const CALLED_FROM_GESTION_PUB = 'gestionPub';
    public const CALLED_FROM_ANNUAIRE = 'annuaire';
    public const CALLED_FROM_BASE_DCE = 'baseDce';
    public const CALLED_FROM_MULTI_AGENT = 'multiAgent';
    public const CALLED_FROM_COMMISSION = 'commission';

    public const LIST_CALLS_FROM_AGENT_CONTEXT = [
        self::CALLED_FROM_AGENT_HELIOS,
        self::CALLED_FROM_AGENT,
        self::CALLED_FROM_GESTION_PUB,
        self::CALLED_FROM_ANNUAIRE,
        self::CALLED_FROM_BASE_DCE,
        self::CALLED_FROM_MULTI_AGENT,
        self::CALLED_FROM_SOCLE_INTERNE,
        self::CALLED_FROM_COMMISSION,
    ];

    public function isMobile()
    {
        if (!Atexo_Config::getParameter('MPE_MOBILE')) {
            return false;
        }

        if ('out' == $_GET['mobile'] || 'out' == Atexo_CurrentUser::readFromSession('mpe_mobile') ) {
            Atexo_CurrentUser::writeToSession('mpe_mobile', 'out');
            return false;
        }

        if (!in_array('entreprise', explode('.', Atexo_Util::atexoHtmlEntities($_GET['page'])))) {
            return false;
        }
        $mobile = new Atexo_Mobile();

        return $mobile->isMpeMobile();
    }

    public function onLoad($param)
    {
        if ($this->isMobile()) {
            $qs = 'page=Entreprise.EntrepriseHome';
            if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 5) {
                $qs = $_SERVER['QUERY_STRING'];
            }
            $qs = preg_replace('#entreprise\.AccueilEntreprise#', 'Entreprise.EntrepriseHome', $qs);
            $this->response->redirect(Atexo_Config::getParameter('ALIAS_MPE_MOBILE')."/?$qs");
        }
        $this->_roles = $this->User->getRoles();

        $calledFromMandatory = array_merge(
            self::LIST_CALLS_FROM_AGENT_CONTEXT,
            [self::CALLED_FROM_ENTREPRISE, self::CALLED_FROM_ADMIN]
        );
        if (!in_array($this->_calledFrom, $calledFromMandatory)) {
            throw new \Exception('You must precise _calledFrom parameter');
        }
        // Gestion de l'internationalisation
        $globalization = $this->getApplication()->Modules['globalization'];
        $globalization->setTranslationCatalogue('');
        if ('admin' == $this->_calledFrom) {
            $globalization->setTranslationCatalogue('messages.fr');
            if (!Atexo_CurrentUser::readFromSession('lang')) {
                Atexo_CurrentUser::writeToSession('lang', 'fr');
            }
        }
        if (in_array($this->_calledFrom, self::LIST_CALLS_FROM_AGENT_CONTEXT)) {
            if (!isset($_GET['lang'])) {
                if (!(Atexo_CurrentUser::readFromSession('lang'))) {
                    $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                } else {
                    $langueEnSession = Atexo_CurrentUser::readFromSession('lang');
                }
            } else {
                $langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            }
            if (in_array(
                    $langueEnSession,
                    Atexo_Languages::retrieveArrayActiveLangages()
                ) || 'atx' == $langueEnSession) {
                // Enregistrement en session
                Atexo_CurrentUser::writeToSession('lang', $langueEnSession);
                Atexo_CurrentUser::writeToSession('catalogue', $globalization->getTranslationCatalogue());
                $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
            } else {
                unset($_GET['lang']);
                $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
            }
        }
        if ('entreprise' == $this->_calledFrom) {
            if (!isset($_GET['lang']) || !in_array($_GET['lang'], Atexo_Languages::retrieveArrayActiveLangages())) {
                if (!(Atexo_CurrentUser::readFromSession('lang'))) {
                    $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                } else {
                    $langueEnSession = Atexo_CurrentUser::readFromSession('lang');
                }
            } else {
                $langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            }
            if (in_array(
                    $langueEnSession,
                    Atexo_Languages::retrieveArrayActiveLangages()
                ) || 'atx' == $langueEnSession) {
                // Enregistrement en session
                Atexo_CurrentUser::writeToSession('lang', $langueEnSession);

                if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
                    $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
                } else {
                    $globalization->setTranslationCatalogue('messages.fr');
                }
                Atexo_CurrentUser::writeToSession('catalogue', $globalization->getTranslationCatalogue());
            } else {
                unset($_GET['lang']);
                $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
            }
        }
        if (str_contains($_SERVER['HTTP_HOST'], 'design-mpe')) {
            $opt = "<option style=\"background-color:#CCC;\">Themes :</option>\n";
            $handle = opendir('../themes');
            $files = [];
            while (false !== ($file = readdir($handle))) {
                if ('.' != $file && '..' != $file && 'CVS' != $file && 'CMD.sh' != $file && 'demo' != $file) {
                    $files[] = $file;
                }
            }
            closedir($handle);
            sort($files);
            foreach ($files as $file) {
                if (is_file("../themes/$file/images/favicon.ico")) {
                    $fav = "background-image:url('themes/$file/images/favicon.ico')";
                } else {
                    $fav = '';
                }
                if ($_COOKIE['design'] == $file) {
                    $opt .= "<option selected=\"selected\" value=\"$file\" style=\"$fav;font-weight: bold;background-color: #99F;\"> ** $file **</option>\n";
                } else {
                    $opt .= "<option value=\"$file\" style=\"$fav;\">$file</option>\n";
                }
            }
            $opt .= "<option value=\"reset\"> -- RESET -- </option>\n";
            $this->themes->setVisible(true);
            $this->themes->Text = $this->getTheme();
        }

        //Permet d'initialiser le composant "AtexoUtah"
        if (Atexo_Config::getParameter('UTILISER_FAQ') || Atexo_Config::getParameter('UTILISER_UTAH')) {
            $this->atexoUtah->setCalledFrom($this->getCalledFrom());
            $this->atexoUtah->initialiser();
            if (isset($_GET['javaVersion']) && isset($_GET['Utah']) && 'OK' == $_GET['Utah']) {
                $this->atexoUtah->accederFormulaireUtah($_GET['javaVersion']);
            }
        }

        if (Atexo_Util::getSfService(RgpdService::class)->displayPopUpRgpd()) {
            MessageStatusCheckerUtil::initEntryPoints('rgpd');
        }
    }

    public function getTheme($opt = '')
    {
        return "
            <style>
                select.icon-menu option {
                    background-repeat: no-repeat;
                    padding-left: 19px;
                    height: 16px;
                    font-size: 11px;
            }
            select.icon-menu {
            font-size: 11px;
            }
            </style>
            <script>
            function $(v) {return(document.getElementById(v));}
            function agent(v) { return(Math.max(navigator.userAgent.toLowerCase().indexOf(v),0)); }
            function xy(e,v) { return(v?(agent('msie')?event.clientY+document.body.scrollTop:e.pageY):(agent('msie')?event.clientX+document.body.scrollTop:e.pageX)); }
            function dragOBJ(d,e) {
                function drag(e) { if(!stop) { d.style.top=(tX=xy(e,1)+oY-eY+'px'); d.style.left=(tY=xy(e)+oX-eX+'px'); } }
                var oX=parseInt(d.style.left),oY=parseInt(d.style.top),eX=xy(e),eY=xy(e,1),tX,tY,stop;
                document.onmousemove=drag; document.onmouseup=function(){ stop=1; document.onmousemove=''; document.onmouseup=''; };
            }
            function change(elt) {
                if (elt.options[elt.selectedIndex].value == '' || elt.options[elt.selectedIndex].value == 'Themes' || elt.options[elt.selectedIndex].value == 'Themes :') return false;
                qs = document.location.search.replace(new RegExp('(.*)&design=[-_a-z]+(.*)', 'g'), '$1$2');
                window.location.href=qs + '&design='+elt.options[elt.selectedIndex].value
            }
            </script>
            <div style=\"position: absolute;top: 10px;left: 10px;\" onmousedown=\"dragOBJ(this,event); return false;\">
            <form>
            <select class=\"icon-menu\" onChange=\"change(this)\">
            $opt
            </select>
            </form>
            </div>";
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function isCalledFromAgent()
    {
        return 'agent' == $this->_calledFrom;
    }

    public function isCalledFromChoixMultiAgent()
    {
        return 'multiAgent' == $this->_calledFrom;
    }

    public function isCalledFromAdmin()
    {
        return 'admin' == $this->_calledFrom;
    }

    public function isCalledFromCompany()
    {
        return 'entreprise' == $this->_calledFrom;
    }

    public function isCalledFromSocleInterne(): bool
    {
        return self::CALLED_FROM_SOCLE_INTERNE === $this->_calledFrom;
    }

    public function getConnecteAgent()
    {
        $valeur = $this->_roles;

        return 'Agent' == $valeur[1];
    }

    public function getMenugaucheAgent()
    {
        $valeur = $this->_roles;

        return 'Agent' == $valeur[1];
    }

    public function getFooterAgent()
    {
        return true;
    }

    public function getConnecteAdmin()
    {
        $valeur = $this->_roles;

        return 'Admin' == $valeur[1];
    }

    public function getMenugaucheAdmin()
    {
        $valeur = $this->_roles;

        return 'Admin' == $valeur[1];
    }

    public function getConnecteEntreprise()
    {
        $valeur = $this->_roles;

        return 'Entreprise' == $valeur[1];
    }

    public function getMenugaucheEntreprise()
    {
        $valeur = $this->_roles;

        return 'Entreprise' == $valeur[1];
    }

    public function getFooterAdmin()
    {
    }

    public function keepSession()
    {
        if ($this->isCalledFromAgent() && '' != Atexo_CurrentUser::getOrganismAcronym()) {
            return Atexo_Config::getParameter('URL_ABSOLUE_MPE_18').Atexo_CurrentUser::getOrganismAcronym(
                ).'/agent/keepSession.php';
        }

        return '';
    }

    public function includeJsLang($nameFile)
    {
        $lang = '';
        $lang = Atexo_CurrentUser::readFromSession('lang');
        if ('' != $lang) {
            return $nameFile.'.'.$lang.'.js';
        }

        return $nameFile.'.js';
    }

    public function getMenuGaucheHelios()
    {
        $valeur = $this->_roles;

        return 'agenthelios' == $valeur[1];
    }

    public function isCalledFromHelios()
    {
        return 'agenthelios' == $this->_calledFrom;
    }

    public function isCalledFromGestionPub()
    {
        return 'gestionPub' == $this->_calledFrom;
    }

    public function isCalledFromAnnuaire()
    {
        return 'annuaire' == $this->_calledFrom;
    }

    public function isCalledFromBaseDce()
    {
        return 'baseDce' == $this->_calledFrom;
    }

    public function updateDate()
    {
        if ((new MainLayout())->isCalledFromCompany()) {
            $bandeauId = 'bandeauEntreprise';
        } else {
            $bandeauId = 'bandeauAgent';
        }
        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
            $nomDesJour = [
                    'Monday' => Prado::localize('TEXT_DAY1'),
                    'Tuesday' => Prado::localize('TEXT_DAY2'),
                    'Wednesday' => Prado::localize('TEXT_DAY3'),
                    'Thursday' => Prado::localize('TEXT_DAY4'),
                    'Friday' => Prado::localize('TEXT_DAY5'),
                    'Saturday' => Prado::localize('TEXT_DAY6'),
                    'Sunday' => Prado::localize('TEXT_DAY7'),
                ];

            $nomDesMois = [
                    1 => Prado::localize('TEXT_MONTH1'),
                    2 => Prado::localize('TEXT_MONTH2'),
                    3 => Prado::localize('TEXT_MONTH3'),
                    4 => Prado::localize('TEXT_MONTH4'),
                    5 => Prado::localize('TEXT_MONTH5'),
                    6 => Prado::localize('TEXT_MONTH6'),
                    7 => Prado::localize('TEXT_MONTH7'),
                    8 => Prado::localize('TEXT_MONTH8'),
                    9 => Prado::localize('TEXT_MONTH9'),
                    10 => Prado::localize('TEXT_MONTH10'),
                    11 => Prado::localize('TEXT_MONTH11'),
                    12 => Prado::localize('TEXT_MONTH12'),
                ];
            $this->$bandeauId->dateAujourdhui->Text = Atexo_Util::getDateAujourdhui($nomDesJour, $nomDesMois);
        } else {
            $this->$bandeauId->dateAujourdhui->Text = Atexo_Util::getDateAujourdhui();
        }
        //$this->bandeauEntreprise->dateAujourdhui->Text =  date('Y-m-d H:i:s');
    }

    public function isDefaultBackground()
    {
        if (isset($_COOKIE['idIllustration']) && $_COOKIE['idIllustration']) {
            return false;
        }

        return true;
    }

    /**
     * permet de recuperer la css du socle.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCss()
    {
        if (((new MainLayout())->isCalledFromAgent()
        && (('socle' == Atexo_CurrentUser::readFromSession('ServiceMetier'))
         && Atexo_Module::isEnabled('SocleInterne')))) {
            return 'socle';
        }

        return '';
    }

    public function isCalledFromCommission()
    {
        return 'commission' == $this->_calledFrom;
    }
}
