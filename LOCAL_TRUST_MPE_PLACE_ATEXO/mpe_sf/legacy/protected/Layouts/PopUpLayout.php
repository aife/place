<?php

namespace Application\Layouts;

use Application\Controls\MpeTTemplateControl;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class PopUpLayout extends MpeTTemplateControl
{
    private array $_roles = [];
    private $_calledFrom;

    public function onLoad($param)
    {
        $this->_roles = $this->User->getRoles();


        //Permet d'initialiser le composant "AtexoUtah"
        if ((Atexo_Config::getParameter('UTILISER_FAQ') || Atexo_Config::getParameter('UTILISER_UTAH'))
            && !preg_match('/(^agent.PopupAccesRedaction)/i', $_GET['page'])) {
            $this->atexoUtah->initialiser();
            if (isset($_GET['javaVersion']) && isset($_GET['Utah']) && 'OK' == $_GET['Utah']) {
                $this->atexoUtah->accederFormulaireUtah($_GET['javaVersion']);
            }
        }

        $globalization = $this->getApplication()->Modules['globalization'];
        $globalization->setTranslationCatalogue('');
        if (!isset($_GET['lang'])) {
            if (!(Atexo_CurrentUser::readFromSession('lang'))) {
                $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            } else {
                $langueEnSession = Atexo_CurrentUser::readFromSession('lang');
            }
        } else {
            $langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
        }

        if (in_array(
            $langueEnSession,
            Atexo_Languages::retrieveArrayActiveLangages()
        ) || 'atx' == $langueEnSession) {
            // Enregistrement en session
            Atexo_CurrentUser::writeToSession('lang', $langueEnSession);
            Atexo_CurrentUser::writeToSession('catalogue', $globalization->getTranslationCatalogue());
            $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
        } else {
            unset($_GET['lang']);
            $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
        }
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function isCalledFromAgent()
    {
        return 'agent' == $this->_calledFrom;
    }

    public function isCalledFromCompany()
    {
        return 'entreprise' == $this->_calledFrom;
    }

    public function getConnecteAgent()
    {
        $valeur = $this->_roles;
        if ('Agent' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheAgent()
    {
        $valeur = $this->_roles;
        if ('Agent' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getFooterAgent()
    {
        return true;
    }

    public function getConnecteAdmin()
    {
        $valeur = $this->_roles;
        if ('Admin' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheAdmin()
    {
        $valeur = $this->_roles;
        if ('Admin' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getConnecteEntreprise()
    {
        $valeur = $this->_roles;
        if ('Entreprise' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheEntreprise()
    {
        $valeur = $this->_roles;
        if ('Entreprise' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getFooterAdmin()
    {
    }

    public function includeJsLang($nameFile)
    {
        $lang = '';
        $lang = Atexo_CurrentUser::readFromSession('lang');
        if ('' != $lang) {
            return $nameFile.'.'.$lang.'.js';
        } else {
            return $nameFile.'.js';
        }
    }
}
