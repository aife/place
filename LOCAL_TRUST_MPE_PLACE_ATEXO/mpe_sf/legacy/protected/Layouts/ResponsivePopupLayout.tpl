<!DOCTYPE html>
<html lang="<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>">

<com:THead title="<%= (Prado::localize($this->Page->pageTitle)) %>">
    <meta charset="<%= Application\Service\Atexo\Atexo_Config::getParameter('HTTP_ENCODING')%>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <com:TStyleSheet PradoStyles="bootstrap, jquery-ui" />

    <link href="<%=$this->themesPath()%>/css/font-awesome.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" rel="stylesheet" />
    <link href="<%=$this->themesPath()%>/responsive/css/bootstrap-table.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
    <link href="<%=$this->themesPath()%>/responsive/css/bootstrap-datetimepicker.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
    <link href="<%=$this->themesPath()%>/responsive/css/jquery.tagsinput.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
    <link href="<%=$this->themesPath()%>/responsive/css/chosen.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
    <link href="<%=$this->themesPath()%>/responsive/css/main.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
    <link href="<%=$this->themesPath()%>/css/loader.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" rel="stylesheet" />
    <com:TConditional Condition="preg_match('/^Agent.DetailEntreprise/i', $_GET['page'])">
        <prop:TrueTemplate>
        <link href="<%=$this->themesPath()%>/responsive/css/historique-document.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" rel="stylesheet">
        </prop:TrueTemplate>
    </com:TConditional>

</com:THead>

<body>
    <com:TForm  Method="post" Attributes.name="main_form" Attributes.autocomplete="on">
        <com:JavascriptsTopIncludes id="jsincludes"/>
        <script src="<%=$this->themesPath()%>/responsive/js/jquery.treegrid.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"></script>
        <script src="<%=$this->themesPath()%>/responsive/js/scripts.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"></script>
        <div class="container-fluid" id="container">
            <com:AtexoUtah id="atexoUtah"/>
            <com:TContentPlaceHolder ID="CONTENU_PAGE" />
        </div>
    </com:TForm>

    <%%
        echo htmlspecialchars_decode($this->getParameter("SCRIPT_JS_CLIENT"));
    %>
    <%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntitiesDecode(Application\Service\Atexo\Atexo_Config::getParameter('PIWIK_SCRIPT'))%>
</body>
</html>
