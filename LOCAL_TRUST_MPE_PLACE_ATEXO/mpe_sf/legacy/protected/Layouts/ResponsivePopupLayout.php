<?php

namespace Application\Layouts;

use Application\Controls\MpeTTemplateControl;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 21/09/2015
 * Time: 11:15.
 */
class ResponsivePopupLayout extends MpeTTemplateControl
{
    private array $_roles = [];

    public function onLoad($param)
    {
        $this->_roles = $this->User->getRoles();

        //Permet d'initialiser le composant "AtexoUtah"
        if (Atexo_Config::getParameter('UTILISER_FAQ') || Atexo_Config::getParameter('UTILISER_UTAH')) {
            $this->atexoUtah->initialiser();
            if (isset($_GET['javaVersion']) && isset($_GET['Utah']) && 'OK' == $_GET['Utah']) {
                $this->atexoUtah->accederFormulaireUtah($_GET['javaVersion']);
            }
        }
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function isCalledFromAgent()
    {
        return 'agent' == $this->_calledFrom;
    }

    public function isCalledFromCompany()
    {
        return 'entreprise' == $this->_calledFrom;
    }

    public function getConnecteAgent()
    {
        $valeur = $this->_roles;
        if ('Agent' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheAgent()
    {
        $valeur = $this->_roles;
        if ('Agent' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getFooterAgent()
    {
        return true;
    }

    public function getConnecteAdmin()
    {
        $valeur = $this->_roles;
        if ('Admin' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheAdmin()
    {
        $valeur = $this->_roles;
        if ('Admin' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getConnecteEntreprise()
    {
        $valeur = $this->_roles;
        if ('Entreprise' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getMenugaucheEntreprise()
    {
        $valeur = $this->_roles;
        if ('Entreprise' == $valeur[1]) {
            return true;
        } else {
            return false;
        }
    }

    public function getFooterAdmin()
    {
    }

    public function includeJsLang($nameFile)
    {
        $lang = '';
        $lang = Atexo_CurrentUser::readFromSession('lang');
        if ('' != $lang) {
            return $nameFile.'.'.$lang.'.js';
        } else {
            return $nameFile.'.js';
        }
    }
}
