<!DOCTYPE html>
<html lang="<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>">

<com:THead title="<%= (Prado::localize($this->Page->pageTitle)) %>">

	<com:TConditional Condition="preg_match('/(^agent)/i', $_GET['page']) || preg_match('/(^agent)/i', $_GET['calledFrom']) || preg_match('/(^commission)/i', $_GET['page']) || preg_match('/(^administration)/i', $_GET['page']) || preg_match('/(^administration)/i', $_GET['calledFrom']) || preg_match('/(^commun)/i', $_GET['page']) || preg_match('/(^commun)/i', $_GET['calledFrom']) || preg_match('/(^gestionPub)/i', $_GET['page']) || preg_match('/(^gestionPub)/i', $_GET['calledFrom'])">
		<prop:TrueTemplate>	<%% if($_GET['page']!='Agent.RedacClausier' && $_GET['page']!='Agent.RedactionPiecesConsultation'&& $_GET['page']!='Agent.FormulaireConsultation'&& $_GET['page']!='Agent.ChangingConsultation' && $_GET['page']!='Agent.FormulaireContrat'){
			echo '<meta http-equiv="X-UA-Compatible" content="IE=11" />';
			}else{
			echo '<meta http-equiv="X-UA-Compatible" content="IE=edge" />';
			}
			%>

			<!-- <%=preg_replace('/[^0-9]/', '', php_uname('n'))%> -->
			<meta http-equiv="Content-Type" content="text/html; charset=<%= $this->getParameter('HTTP_ENCODING')%>" />
			<title><com:TTranslate>TITRE</com:TTranslate></title><%% if (file_exists('../themes/portal/images/favicon.ico')) echo "\n" . '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />'; %>

			<com:TStyleSheet PradoStyles="bootstrap, jquery-ui" />

			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/vue-multiselect.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/toggle-switch.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/chosen-gwt.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/chosen.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/nav-modules.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/jquery.treeTable.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"  media="all" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/loader.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/print.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" media="print" />

			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/jquery.tagsinput.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/styles-mpe.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"  media="all" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/responsive/css/bootstrap-wysihtml5.css?k=<%= $this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/responsive/css/wysiwyg-color.css?k=<%= $this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/font-marianne.css?k=<%= $this->getParameter('MEDIA_CACHE_KEY')%>"  media="all" />
			<!--bloc added since MPE-2865-->
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/atx-popover-markup-login-usr.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/sign-in-bloc.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all">
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/bootstrap-ng.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" media="all">
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/bootstrap-iso.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" media="all"/>
			<!-- end bloc -->

			<!--Begin CSS SF -->
			<com:CssSymfonyAgent ID="cssSymfonyAgent" visible="<%= ($this->isCalledFromHelios()||$this->isCalledFromAgent()||$this->isCalledFromAdmin() || $this->isCalledFromGestionPub() || $this->isCalledFromAnnuaire() || $this->isCalledFromBaseDce() || $this->isCalledFromChoixMultiAgent() || $this->isCalledFromSocleInterne())%>"/>
			<!--End CSS SF-->

			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui-noscript.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/bootstrap-toggle.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all" />
		</prop:TrueTemplate>
		<prop:FalseTemplate>

			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
			<title>
				<%%
				if (isset($this->Page->pageTitle)) {
					echo Prado::localize($this->Page->pageTitle);
				} else {
					echo Prado::localize('TITRE');
				}
				%>
			</title>

			<com:TConditional Condition="file_exists('../themes/portal/images/favicon.ico')">
				<prop:TrueTemplate>
					<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
				</prop:TrueTemplate>
			</com:TConditional>
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/vue-multiselect.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style-mpe.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
			<link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/chosen.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/leaflet.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/MarkerCluster.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />
			<link rel="stylesheet" href="<%=$this->themesPath()%>/css/leaflet.awesome-markers.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />



			<!-- end bloc -->
			<link rel="stylesheet" type="text/css"
				  href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui-noscript.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"
				  media="all"/>
			<link rel="stylesheet" type="text/css"
				  href="<%=$this->themesPath()%>/css/upload/jquery.fileupload-ui.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>"
				  media="all"/>

		</prop:FalseTemplate>

	</com:TConditional>
</com:THead>
<%%
if ($this->isDefaultBackground()) {
echo "<body class='" . $this->getCss() . " " . $this->getBodyClass() . "' >";
}else {
echo '<body style="background:url(' .$this->getParameter("PF_URL") . 'index.php?page=Agent.IllustrationAgent);" class="persona ' . $this->getBodyClass() . '">';
}
%>

<script>
	var pfUrl = "<%= $this->getParameter("PF_URL") %>";
	var isAgent = <%= $this->isCalledFromAgent() ? 'true' : 'false' %>;
</script>

<!--Debut Bandeau-->
<!--Begin BandeauEntreprise-->
<com:BandeauEntreprise ID="bandeauEntreprise" visible="<%= $this->isCalledFromCompany() %>"/>
<!--End BandeauEntreprise-->
<!--Begin BandeauAgent-->
<com:BandeauAgent ID="bandeauAgent" visible="<%= ($this->isCalledFromHelios()||$this->isCalledFromAgent()||$this->isCalledFromAdmin() || $this->isCalledFromAnnuaire() || $this->isCalledFromBaseDce() || $this->isCalledFromChoixMultiAgent() || $this->isCalledFromCommission())%>"/>
<!--End BandeauAgent-->
<!--Begin BandeauAgentSocleInterne-->
<com:BandeauAgentSocleInterne ID="bandeauAgentSocleInterne" visible="<%= $this->isCalledFromSocleInterne() || $this->isCalledFromGestionPub() %>"/>
<!--End BandeauAgent-->
<!--Fin Bandeau-->

<com:TForm Attributes.name="main_form" Attributes.autocomplete="on">
	<com:JavascriptsTopIncludes id="jsincludes"/>

	<!-- Menu Gauche Gestion Pub-->
	<com:MenuGaucheGestionPub id="menuGaucheGestionPub" visible="<%= $this->isCalledFromGestionPub() %>"/>

	<!--Debut partie centrale-->
	<main id="middle" class="clear-top atx-middle" role="main" style="<%= $_GET['fullWidth'] ? 'width:95%' : '' %>">
	    <com:ErrorAgent ID="errorAgent" />
		<div class="<%= $_GET['fullWidth'] ? '' : 'container container--690-sm' %> m-b-10">
			<div class="clearfix">
				<com:TContentPlaceHolder ID="CONTENU_PAGE"/>
			</div>
		</div>
	</main>
	<!--Fin partie centrale-->

	<com:AtexoUtah id="atexoUtah"/>
	<com:AtexoHelpBar calledFrom="<%=$this->getCalledFrom()%>"/>
	</div>
</com:TForm>

<%=Application\Service\Atexo\MessageStatusCheckerUtil::getEntrypointJs()%>

<com:FooterAgent visible="<%=($this->isCalledFromHelios() || $this->isCalledFromAgent()||$this->isCalledFromAdmin() || $this->isCalledFromAnnuaire() || $this->isCalledFromBaseDce())%>"/>
<com:FooterEntreprise ID="footerEntreprise" visible="<%=$this->isCalledFromCompany()%>"/>
</div>
<script  src="<%=$this->themesPath()%>/js/prototype-noconflict.footer.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" charset="UTF-8"></script>
<com:TLabel id="themes" visible="false"/>
<!-- MainLayout -->
<com:TConditional Condition="preg_match('/^agent/i', $_GET['page'])">
	<prop:FalseTemplate>
		<script  src="<%=$this->themesPath()%>/js/ng-script.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" charset="UTF-8"></script>
	</prop:FalseTemplate>
</com:TConditional>

<%%
    echo htmlspecialchars_decode($this->getParameter("SCRIPT_JS_CLIENT"));
%>
<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntitiesDecode(Application\Service\Atexo\Atexo_Config::getParameter('PIWIK_SCRIPT'))%>

</body>
</html>
