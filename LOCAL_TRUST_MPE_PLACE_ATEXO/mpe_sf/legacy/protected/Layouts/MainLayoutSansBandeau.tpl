<!DOCTYPE html>
<html lang="<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=11"/>
    <!-- <%=preg_replace('/[^0-9]/', '', getenv("HOSTNAME"))%> -->
    <meta http-equiv="Content-Type" content="text/html; charset=<%= Application\Service\Atexo\Atexo_Config::getParameter('HTTP_ENCODING')%>"/>
    <title>
        <%%
        if (isset($this->Page->pageTitle)) {
            echo Prado::localize($this->Page->pageTitle);
        } else {
            echo Prado::localize('TITRE');
        }
        %>
    </title>
    <com:TConditional Condition="file_exists('../themes/portal/images/favicon.ico')">
        <prop:TrueTemplate>
            <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
        </prop:TrueTemplate>
    </com:TConditional>

    <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style-mpe.css?k=<%=Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>

</head>
<body id="body-iframe">
<com:TForm Attributes.name="main_form" Attributes.autocomplete="on">
    <com:JavascriptsTopIncludes id="jsincludes"/>
    <com:AtexoUtah id="atexoUtah"/>
    <com:TContentPlaceHolder ID="CONTENU_PAGE"/>
    <div class="breaker"></div>
</com:TForm>
<com:TLabel id="themes" visible="false"/>
<!-- MainLayoutSansBandeau -->
<%%
    echo htmlspecialchars_decode($this->getParameter("SCRIPT_JS_CLIENT"));
%>
<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntitiesDecode(Application\Service\Atexo\Atexo_Config::getParameter('PIWIK_SCRIPT'))%>
</body>
</html>
