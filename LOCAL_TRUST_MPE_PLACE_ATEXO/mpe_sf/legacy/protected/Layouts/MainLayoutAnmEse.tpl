<!DOCTYPE html>
<html lang="<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=11, edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <%%
        if (isset($this->Page->pageTitle)) {
            echo Prado::localize($this->Page->pageTitle);
        } else {
            echo Prado::localize('TITRE');
        }
        %>
    </title>

    <com:TConditional Condition="file_exists('../themes/portal/images/favicon.ico')">
        <prop:TrueTemplate>
            <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
        </prop:TrueTemplate>
    </com:TConditional>

    <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style-mpe.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<%=$this->themesPath()%>/css/ng-style.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" media="all"/>

</head>
<%%
if ($this->isDefaultBackground()) {
echo '<body>';
}else {
echo '<body style="background:url(' .$this->getParameter(" PF_URL") . 'index.php?page=Agent.IllustrationAgent);" class="persona">';}
%>

<div class="main-wrapper">
    <com:TForm Attributes.name="main_form" Attributes.autocomplete="on">
        <com:JavascriptsTopIncludes id="jsincludes"/>

        <!--Debut partie centrale-->
        <main id="middle" class="atx-middle" role="main">
            <com:AtexoUtah id="atexoUtah"/>
            <com:TContentPlaceHolder ID="CONTENU_PAGE"/>
        </main>

        <!--Fin partie centrale-->
        <com:FooterEntreprise visible="<%=MainLayoutAnmEse::isCalledFromCompany()%>"/>
    </com:TForm>
</div>
<script  src="<%=$this->themesPath()%>/js/prototype-noconflict.footer.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" charset="UTF-8"></script>
<com:TLabel id="themes" visible="false"/>
<script language="JavaScript" type="text/JavaScript" src="<%=$this->themesPath()%>/js/ng-script.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" charset="UTF-8"></script>

<%%
    echo htmlspecialchars_decode($this->getParameter("SCRIPT_JS_CLIENT"));
%>
<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntitiesDecode(Application\Service\Atexo\Atexo_Config::getParameter('PIWIK_SCRIPT'))%>
</body>
</html>
