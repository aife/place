/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

var vueCPV = new Vue({
  components: {
    Multiselect: window.VueMultiselect.default
  },
  data: {
    listeCpv: [],
    listeCodeCpv: [],
    selectedCpv: [],
    isLoading: false,
    baseUrl: '/referentiels/ws/',
    searchquery: null,
    isCategorySelected: false,
    checkBoxHeritage: '',
    selectCategorie: '',
    isSearchPage: false,
    token: ''
  },
  methods: {
    initCPVForSelected: function () {
      var vm = this;
      if (selected) {
        selected.forEach(function (item) {
          if(!vm.selectedCpv.includes(item)) {
            vm.loadCpv(item);
          }
        });

        vm.isCategorySelected = true;
        vm.changeHeritageState();
        vm.clearAll();
        vm.refreshElementDisabled();
      }
    },
    getToken: function () {
      return axios.get('/referentiels/ws/bootstrapConfiguration?locale=fr&configPath=/resources/config/cpv-config.xml')
          .then(function (response) {
            return response.data.token;
          });
    },
    asyncFind: function (query) {
      var vm = this;
      this.searchquery = query;
      if (!this.isCategorySelected) {
        return;
      }

      if (query.length > 2) {
        this.isLoading = true
        var treeType = document.getElementById('typeSelectTreeView').value;
        var url = '';
        if (treeType === 'simple') {
          url = this.baseUrl + 'searchReferentielTree?token=' + this.token + '&userQuery=' + this.searchquery + '&locale=fr';
        } else {
          url = this.baseUrl + 'searchReferentiel?token=' + this.token + '&userQuery=' + this.searchquery + '&locale=fr';
        }
        axios.get(url).then(function (response)  {
          if (treeType === 'simple') {
            vm.listeCpv = vm.formatResponse(response.data.referentielTree.childs);
          } else {
            vm.listeCpv = response.data.referentielList;
          }
          if (!vm.isSearchPage && vm.listeCodeCpv.length == 0 ) {
            vm.refreshElementDisabled();
          }else{
            vm.cleanElementDisabled();
          }
        });
        this.isLoading = false;
      }


    },
    onSelectOption: function (selectedOption) {
      if (selectedOption.$isDisabled) {
        return;
      }

      var currentCpvPrincipal = document.getElementsByClassName('hidenGwtChosenPrincipal')[0];
      var currentCpvSecondaire = document.getElementsByClassName('hidenGwtChosenSecondaires')[0];
      selectedOption.$isDisabled = true;
      this.listeCodeCpv.push(selectedOption.code);
      if (!currentCpvPrincipal.value) {
        currentCpvPrincipal.value = selectedOption.code;
        this.cleanElementDisabled();
        return true;
      }
      if (currentCpvSecondaire.value) {
        var otherCpv = selectedOption.code + '#';
        currentCpvSecondaire.value += otherCpv;
      } else {
        currentCpvSecondaire.value = '#' + selectedOption.code + '#';
      }

      let cpvSecondaire = currentCpvSecondaire.value;
      let arr = cpvSecondaire.split("#");
      let unique = Array.from(new Set(arr));
      currentCpvSecondaire.value = unique.join("#") + '#';

      return true;
    },
    inArray: function (needle, haystack) {
      var length = haystack.length;
      for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
      }
      return false;
    },
    onRemoveOption: function (removedOption) {
      if ( typeof removedOption === 'undefined') {
        return ;
      }
      var currentCpvPrincipal = document.getElementsByClassName('hidenGwtChosenPrincipal')[0];
      var currentCpvSecondaire = document.getElementsByClassName('hidenGwtChosenSecondaires')[0];
      if (currentCpvPrincipal.value == removedOption.code) {
        this.clearAll();
        currentCpvPrincipal.value = "";
        currentCpvSecondaire.value = "";
        return true;
      }

      var newCpvSecondaire = currentCpvSecondaire.value.replace('#' + removedOption.code + '#', '#');
      currentCpvSecondaire.value = newCpvSecondaire;
      removedOption.$isDisabled = false;
      return;
    },
    formatResponse: function (response) {
      var stack = [], array = []
      stack = response;
      while (stack.length !== 0) {
        var node = stack.shift();

        if (node.childs.length == 0 && node.depth !=0) {
          node.childs = null;
          node.style = 'margin-left:' + node.depth + 'em';
          array.push(node);
        } else {
          if (!node.depth) {
            node.depth = 0;
            node.classe = '';
          }
          for (var i = node.childs.length - 1; i >= 0; i--) {
            node.childs[i].depth = node.depth + 1;
            node.style = 'margin-left:' + node.depth + 'em';
            stack.unshift(node.childs[i]);
          }
          node.childs = null;
          array.push(node);
        }
      }

      return array;
    },
    refreshElementDisabled: function () {
      // A FAIRE pour quand on saisi autre chose
      for (var i = 0; i < this.listeCpv.length; i++) {
        if (this.inArray(this.listeCpv[i].code, this.listeCodeCpv)
            || !this.isMatchToCategorySelected(this.listeCpv[i].code)) {
          this.listeCpv[i].$isDisabled = true;
        }
      }
    },
    cleanElementDisabled: function () {
      for (var i = 0; i < this.$children[0].$options.propsData.options.length; i++) {
        this.$children[0].$options.propsData.options[i].$isDisabled = false;
      }
      for (var i = 0; i < this.listeCpv.length; i++) {
        if (this.inArray(this.listeCpv[i].code, this.selectedCpv)) {
          this.listeCpv[i].$isDisabled = true;
        }
      }
    },
    changeCategoryState: function (evt) {
      if (evt.target.value == 0 && !this.isSearchPage) {
        this.isCategorySelected = false;
      } else {
        this.isCategorySelected = true;
      }
      this.changeHeritageState();

      this.clearAll();
    },
    changeHeritageState: function () {
      this.checkBoxHeritage = document.getElementById('ctl0_CONTENU_PAGE_panelCpvHeriteeConsultation');
      if (this.checkBoxHeritage) {
        if (this.isCategorySelected && categorieParent && this.selectCategorie && (categorieParent.toInteger() === this.selectCategorie.value.toInteger())) {
          this.checkBoxHeritage.style.display = 'block';
        } else {
          this.checkBoxHeritage.style.display = 'none';
        }
      }
    },
    isMatchToCategorySelected: function (code) {
      var prefix = code.substring(0, 2).toInteger();
      var categorySelected = document.getElementsByClassName('cpvCategoryCheck')[0].value.toInteger();

      var isTravaux = (categorySelected === 1 && prefix === 45);
      var isFournitures = (categorySelected === 2 && (prefix < 45 || prefix === 48));
      var isServices = (categorySelected === 3 && prefix >= 49);

      if (isTravaux || isFournitures || isServices) {
        return true;
      }

      return false;
    },
    clearAll: function () {
      while (this.selectedCpv.length > 0) {
        this.selectedCpv[this.selectedCpv.length - 1].$isDisabled = false;
        this.selectedCpv.pop();
      }
      this.listeCodeCpv = [];
      this.listeCpv = [];
      document.getElementsByClassName('hidenGwtChosenPrincipal')[0].value = "";
      document.getElementsByClassName('hidenGwtChosenSecondaires')[0].value = "";
      for (var i = 0; i < this.$children[0].$options.propsData.options.length; i++) {
        this.$children[0].$options.propsData.options[i].$isDisabled = false;
      }
    },
    customLabel: function (code, label) {
      return code + ' – ' + label;
    },
    updateCpvFromHeritage: function (evt) {
      var vm = this;
      this.selectedCpv = [];
      this.listeCodeCpv = [];
      this.listeCpv = [];
      if (evt.target.checked) {
        cpvsParent.forEach(function (item) { vm.loadCpv(item);});
      }
    },
    initStateVueJs: function () {

      if (document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_lancerRecherche')) {
        this.isSearchPage = true;
      }

      if (currentLocal == undefined) {
        var currentLocal = 'fr';
      }

      /* Récupération de la locale injecté dans le DOM */
      if (currentLocal) {
        this.locale = currentLocal;
      }

      this.selectCategorie = document.getElementsByClassName('cpvCategoryCheck')[0];
      if (this.selectCategorie) {
        if (this.selectCategorie.value != 0 || this.isSearchPage) {
          this.isCategorySelected = true;
        } else {
          this.isCategorySelected = false;
        }
        this.changeHeritageState()
        return true;
      }

      return false;
    },
    loadCpv: function (codeCpv) {
      var vm = this;
      if(!this.locale){
        this.locale = 'fr';
      }
      var url = this.baseUrl + 'searchReferentiel?token=' + this.token + '&userQuery=' + codeCpv + '&locale=' + this.locale;
      axios.get(url).then(function (response)  {
        if (response.data.referentielList && response.data.referentielList[0]) {
          vm.onSelectOption(response.data.referentielList[0]);
          vm.selectedCpv.push(response.data.referentielList[0]);
        }
      });
    }

  },
  created: function () {
    var vm = this;
    this.getToken().then(function(token) {
      vm.token = token;
      /* on vérifie si nous avons déjà des CPV sélectionné (mode modification) */
      try {
        if (currentLocal) {
          this.locale = currentLocal;
        }
        if (selected) {
          selected.forEach(function (item) { vm.loadCpv(item);});
        }

      } catch (e) {
        /* l'attribut selected ou current local n'est pas présent dans le dom*/
      }
    });
  },
  destroyed: function () {
    if (this.selectCategorie) {
      this.selectCategorie.removeEventListener('change', this.changeCategoryState);
    }
    if (this.checkBoxHeritage) {
      this.checkBoxHeritage.removeEventListener('change', this.updateCpvFromHeritage);
    }
  },
  mounted: function () {
    /* attend le chargement du dom pour la création des évenements. */
    document.addEventListener('DOMContentLoaded', function () {
      this.initStateVueJs();

      if (this.selectCategorie) {
        this.selectCategorie.addEventListener('change', this.changeCategoryState);
      }
      if (this.checkBoxHeritage) {
        this.checkBoxHeritage.addEventListener('change', this.updateCpvFromHeritage);
      }
    }.bind(this));
  }
}).$mount('#cpv');

function toggleTree(container, id) {
  typeSelectTreeView = document.getElementById('typeSelectTreeView');
  if (typeSelectTreeView.value != id) {
    typeSelectTreeView.value = id;
  }
}