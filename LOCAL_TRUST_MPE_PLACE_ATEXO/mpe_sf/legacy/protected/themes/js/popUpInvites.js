/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// fonction qui permet de sélectionner ou déselectionner les invités
function checkAllGuests(myInput, nbr)
{
    if(myInput.checked)
  		{   
  			for(i=1;i<= nbr;i++)
				{
				 var cuestsCheck = document.getElementById('ctl0_CONTENU_PAGE_RepeaterResultats_ctl'+i+'_inviteSelection_1');
				 if(cuestsCheck != null)
				 cuestsCheck.checked = true;
				}		
	  		
	  	}else
	  	{ 
	  	    for(i=1;i<= nbr;i++)
				{
				 var cuestsCheck = document.getElementById('ctl0_CONTENU_PAGE_RepeaterResultats_ctl'+i+'_inviteSelection_1');
				 if(cuestsCheck != null)
				 cuestsCheck.checked = false;
				}		
	  	}  	   	 	 	
}
