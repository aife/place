/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
let url     = '/app.php/entreprise/verification';

/**
 * affichage de la modal si le siret est invalide au chargement de la page
 */
jQuery( document ).ready(function() {
    jQuery("body").append('<div id="verificationEntreprise" ></div>');
    jQuery('#verificationEntreprise').load(url, showModal);
});

/**
 * affiche la modal
 */
showModal = function (){
    jQuery('#verificationSiretModal').modal('show');
}
