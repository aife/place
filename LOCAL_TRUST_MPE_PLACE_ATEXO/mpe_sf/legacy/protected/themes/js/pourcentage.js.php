//----------------------------------Publicite Progress bar----------------------------------//

/*jQuery(document).ready(function ($) {
 initKnob();
 initProgressPub();
 });*/
//Init progress on change
var pbVal = 0;
function initProgressPub() {

    pbVal = calculPourcentage();
    J(".dial").val(pbVal+'%');
    J(".dial").trigger('change');
    J(".updateProgress").text(pbVal);

}
function calculPourcentage(){
    var pbVal = 0;
    var pourcentage = <?php echo Atexo_Config::getParameter('POURCENTAGE_CHAMPS_CONSULTATION') ?>  ;
    pbVal += parseFloat(document.getElementById("ctl0_CONTENU_PAGE_bloc_PubliciteConsultation_pourcentageBoamp").value);
    pbVal = calculValueForInput(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_objet", pourcentage);
    pbVal = calculValueForInput(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_referentielCPV_cpvPrincipale", pourcentage);
    pbVal = calculValueForInput(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis", pourcentage);
    pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_procedureType", pourcentage);
    pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_categorie", pourcentage);
    //pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_natureMarche", pourcentage);
    //pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_produitsAssocies_listMultiSelect", pourcentage);
    //pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_motsCles_listMultiSelect", pourcentage);
    //pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeIdentification_donneeCriteresAttribution_criteresAttribution", pourcentage);
    pbVal = calculValueForSelect(pbVal, "ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeCriteresAttribution_criteresAttribution", pourcentage);
    return pbVal;
}
J(function () {
    J(".champ-pub").on('keyup change', function (){
        pbVal = calculPourcentage();
        if(isNaN(pbVal)){
            pbVal = 0;
        }
        J(".dial").val(pbVal+'%');
        J(".dial").trigger('change');
        animateProgress(pbVal);
        return false;
    });

});

function calculValueForInput(pbVal, idElement, pourcentage){
    if (J( "#" + idElement ).length && J("#" + idElement).val().length > 0) {
        pbVal += pourcentage;
    }
    return pbVal;
}

function calculValueForSelect(pbVal, idElement, pourcentage) {
    select = document.getElementById(idElement);
    var options = select && select.options;
    var opt;
    if(options) {
        for (var i = 0, iLen = options.length; i < iLen; i++) {
            opt = options[i];
            if (opt.selected && opt.index) {
                pbVal += pourcentage;
                break;
            }
        }
    }
    return pbVal;
}

function initPub(){
    jQuery(document).ready(function ($) {
        initKnob();
        initProgressPub();
    });
    //Init progress on change
    var pbVal = 0;
    J(".progress-circle").popover({
        placement: 'top',
        html : true,
        container : "body",
        trigger : 'hover',
        content: function() {
            var content = J(this).attr("data-popover-content");
            return J(content).children(".popover-body").html();
        },
        title: function() {
            var title = J(this).attr("data-popover-content");
            return J(title).children(".popover-heading").html();
        },
        template: '<div class="popover popover-pub bloc-250"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
    });
}
