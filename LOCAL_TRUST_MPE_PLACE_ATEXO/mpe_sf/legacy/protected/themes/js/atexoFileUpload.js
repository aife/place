/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function fileSelected(elementName) {
    var file = document.getElementById(elementName+'_file').files[0];
       if (file) {
          var fileSize = 0;
          if (file.size > 1024 * 1024)
             fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
          else
              fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
	      document.getElementById(elementName+'_prog').style.display = '';
          document.getElementById(elementName+'_fileName').innerHTML = file.name;
          document.getElementById(elementName+'_fileName').style.display = '';
          document.getElementById(elementName+'_fileSize').innerHTML =  fileSize;
          document.getElementById(elementName+'_NomFichier').value =  file.name;
          document.getElementById(elementName+'_hasFile').value =  '1';
          document.getElementById(elementName+'_fileErreur').style.display = 'none';
          document.getElementById(elementName+'_progressNumber').style.display = '';
     }
}
function uploadFile(elementName,fonctionJs,urlServer) {
    var fd = new FormData();
    fd.append("fileToUpload", document.getElementById(elementName+'_file').files[0]);
    if(window.XMLHttpRequest) // Firefox
	   xhr = new XMLHttpRequest();
	else if(window.ActiveXObject) // Internet Explorer
	   xhr = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non supporté par le navigateur
	   return;
	}
    xhr.myvalue = elementName;
    xhr.upload.addEventListener("progress", function(evt){
      if (evt.lengthComputable) {
        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
        document.getElementById(xhr.myvalue+'_progressNumber').innerHTML = percentComplete.toString() + '%';
        document.getElementById(xhr.myvalue+'_prog').value = percentComplete;
        
      }
	  else {
	        document.getElementById(xhr.myvalue+'_progressNumber').innerHTML = 'unable to compute';
	 }
    }, false);
    
    xhr.addEventListener("load", function(evt) {     
    /* This event is raised when the server send back a response */
    
    	if (evt.srcElement) {
    	  elem = evt.srcElement;
    	}else if (evt.target) {
    		elem = evt.target;
    	}
    	if(elem.status == '200'){
   		    document.getElementById(elementName+'_reponseServeur').value = evt.target.responseText;
   		    if(fonctionJs){
  		    	eval(fonctionJs); 
  		    }
    	} else {
     		document.getElementById(elementName+'_fileErreur').innerHTML = "une erreur est survenue lors du chargement du fichier.";
            document.getElementById(elementName+'_fileErreur').style.display = '';
            document.getElementById(elementName+'_prog').style.display = 'none';
            document.getElementById(elementName+'_fileName').style.display = 'none';
            document.getElementById(elementName+'_progressNumber').style.display = 'none';
            }
	}, false);
	 
    xhr.addEventListener("error", uploadFailed, false);
  //xhr.addEventListener("abort", uploadCanceled, false);
    xhr.open("POST", urlServer+'&file',true);
    xhr.send(fd);
}

function uploadFailed(evt) {
    bootbox.alert("une erreur est survenue lors du chargement du fichier.");
}
function addAutresPiecesJointe(){
    document.getElementById('ctl0_CONTENU_PAGE_PieceDce_AddAutresPiece').click();
}		