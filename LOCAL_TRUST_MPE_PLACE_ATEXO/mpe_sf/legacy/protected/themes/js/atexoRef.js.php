//test function retournerReferentiels
function retournerREFERENTIELs(clef, principal, secondaires) {
	//alert('#1'+principal);
	//alert('#2'+secondaires);
	/*var min = 0;
	var maxCPV = 4;
	var langueCPV = 'fr';
	var typeCPV = 'cas2';
	for (indexHidden = 1; indexHidden <=maxCPV; indexHidden++)
	{
	    code_cpv_secondaire = document.getElementById(clef+indexHidden);
	    libeller_code_cpv_secondaire = document.getElementById(clef+ '_libelle'+indexHidden);
	    code_cpv_secondaire.value = '';
	    libeller_code_cpv_secondaire.value = '';
	}
	 */
	var casRef = document.getElementById(clef + '_casRef').value;
	var defineCodePrincipal = document.getElementById(clef + '_defineCodePrincipal').value;
	var p = 0;
	switch (casRef) {
	case 'cas2':
		res = document.getElementById('cas2' + clef);

		chaineTableREF = "<table>";
		code_Ref_pr = document.getElementById(clef + '_codeRefPrinc');
		if (principal[0] != null) {

			chaineTableREF += "<tr>";
			chaineTableREF += "<td>";
			chaineTableREF += creeCodeBulle('id2Bubulle' + clef, principal);
			chaineTableREF += "</td>";
			chaineTableREF += "<td colspan='2'><span class='code'><i>"+ defineCodePrincipal +"</i></span></td>";
			chaineTableREF += "</tr>";
			/*code_cpv_principal = document.getElementById(clef+'1');
			libeller_code_cpv_principal = document.getElementById(clef+ '_libelle'+'1');
			code_cpv_principal.value = principal[0];
			libeller_code_cpv_principal.value = principal[1];*/
			//p = 1;
			code_Ref_pr.value = principal[0];
		}else{
			code_Ref_pr.value = "";
		}
		codes_Ref_sec = document.getElementById(clef + '_codesRefSec');
		var codes_sec = '';
		var sep = '#';
		if (secondaires.length) {
			chaineTableREF += "<tr>";
			for (i = 0; i < secondaires.length; i++) {
				secondairesREF = new Array()
				secondairesREF[0] = secondaires[i][0];
				secondairesREF[1] = secondaires[i][1];
				chaineTableREF += "<td>";
				if (i != 0) {
					secondairesREF[0] = ";" + secondairesREF[0];
				}
				codes_sec = codes_sec + sep + secondaires[i][0];
				/*if (i == (secondaires.length - 1)) {
					codes_sec = codes_sec;
				} else {
					codes_sec = codes_sec + sep;
				}*/
				chaineTableREF += creeCodeBulle('id2Bubulle' + clef + i,
						secondairesREF);
				chaineTableREF += "</td>";

			}
			codes_Ref_sec.value = codes_sec;
			chaineTableREF += "</tr>";
		}else{
			codes_Ref_sec.value = "";
		}

		//}
		chaineTableREF += "<tr>";
		chaineTableREF += "<td colspan='3'><div class='recherche-codes'>";
		urlRef = document.getElementById(clef + '_UrlRef');
		chaineTableREF += CreateLinkRef(urlRef.value, principal, secondaires);
		chaineTableREF += "<span id='erreurcas2"
				+ clef
				+ "'  style='display:none;color:red;'  title='Champ obligatoire' class='check-invalide'><img src='themes/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>";
		chaineTableREF += "</div>";
		chaineTableREF += "</td>";
		chaineTableREF += "</tr>";
		chaineTableREF += "</table>";
		chaineTableREF += "<div class='breaker'></div>";
		res.innerHTML = chaineTableREF;
		break;
	case 'cas4':
		res = document.getElementById('cas4' + clef);
		chaineTableREF = "<table>";
		res.innerHTML = chaineTableREF;
		if (principal[0] != null) {

			chaineTableREF += "<tr>";
			chaineTableREF += "<td>";
			chaineTableREF += creeCodeBulle('id4Bubulle' + clef, principal);
			chaineTableREF += "</td>";
			chaineTableREF += "<td colspan='2'><span class='code'><i>"+ defineCodePrincipal +"</i></span></td>";
			chaineTableREF += "</tr>";
			p = 1;
			if (secondaires.length) {
				chaineTableREF += "<tr>";
				for (i = 0; i < secondaires.length; i++) {

					secondairesREF = secondaires[i];
					chaineTableREF += "<td>";
					if (i != 0) {
						secondairesREF[0] = "<span>;</span>" + secondairesREF[0];
					}
					chaineTableREF += creeCodeBulle('id4Bubulle' + clef + i,
							secondairesREF);
					chaineTableREF += "</td>";

				}
				chaineTableREF += "</tr>";
			}

		}
		chaineTableREF += "</table>";
		chaineTableREF += "<div class='breaker'></div>";
		res.innerHTML = chaineTableREF;
		break;
	case 'cas5':
        res = document.getElementById('cas5' + clef);
        var chaineTableREF = "";

        chaineTableREF += "<table>";
        var codes_sec='';
        if (secondaires.length)
        {
        	var sep = '#';
            for (i = 0 ; i < secondaires.length ; i++) {
                chaineTableREF += "<tr>";
                chaineTableREF += "<td>";
                chaineTableREF +=  creeCodeBulle('id5Bubulle'+ clef + i, secondaires[i]);
                chaineTableREF += "</td>";
                chaineTableREF += "<td>  ";
                if (secondaires[i][1].length >120)
                    chaineTableREF += secondaires[i][1].substring(0,120) + "...";
                else
                    chaineTableREF += secondaires[i][1];
                chaineTableREF += "</td>";
                chaineTableREF += "</tr>";
                codes_sec = codes_sec + sep + secondaires[i][0];

            }
        }
        code_Ref_secondaire = document.getElementById(clef + '_codesRefSec');
        code_Ref_secondaire.value = codes_sec;

        chaineTableREF += "<tr>";
        chaineTableREF += "<td colspan='3'><div class='recherche-codes'>";
        urlRef = document.getElementById(clef + '_UrlRef');
        chaineTableREF += CreateLinkRefSansPrincipal(urlRef.value, principal, secondaires);
        chaineTableREF += "</div>";
        chaineTableREF += "</td>";
        chaineTableREF += "</tr>";
        chaineTableREF += "</table>";
        chaineTableREF += "<div class='breaker'></div>";

        res.innerHTML = chaineTableREF;
        break;

	case 'cas6':
		res = document.getElementById('cas6' + clef);
		codes_Ref_sec = document.getElementById(clef + '_codesRefSec');
		chaineTableREF = "<table>";
		var sep = '#';
		var codes_sec='';
		if (secondaires.length) {
			chaineTableREF += "<tr>";
			for (i = 0; i < secondaires.length; i++) {
				secondairesREF = new Array()
				secondairesREF[0] = secondaires[i][0];
				secondairesREF[1] = secondaires[i][1];
				chaineTableREF += "<td>";
				if (i != 0) {
					secondairesREF[0] = ";" + secondairesREF[0];
				}
				codes_sec = codes_sec + sep + secondaires[i][0];

				chaineTableREF += creeCodeBulle('id6Bubulle' + clef + i,
						secondairesREF);
				chaineTableREF += "</td>";
				//iCPV = i + 1;
				//code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
				//libeller_code_cpv_secondaire = document.getElementById(clef
				//		+ '_libelle' + '' + iCPV);
				//code_cpv_secondaire.value = secondaires[i][0];
				//libeller_code_cpv_secondaire.value = secondaires[i][1];
			}

			chaineTableREF += "</tr>";
		}
		codes_Ref_sec.value = codes_sec;

		chaineTableREF += "<tr>";
		chaineTableREF += "<td colspan='3'><div class='recherche-codes'>";
		urlRef = document.getElementById(clef + '_UrlRef');
		chaineTableREF += CreateLinkRef(urlRef.value, principal, secondaires);
		chaineTableREF += "</div>";
		chaineTableREF += "</td>";
		chaineTableREF += "</tr>";
		chaineTableREF += "</table>";
		chaineTableREF += "<div class='breaker'></div>";
		res.innerHTML = chaineTableREF;
		break;

	case 'cas9':
        res = document.getElementById('cas9' + clef);
        var chaineTableREF = "";

        chaineTableREF += "<table>";
        var codes_sec='';
        if (secondaires.length)
        {
        	var sep = '#';
            for (i = 0 ; i < secondaires.length ; i++) {
                chaineTableREF += "<tr>";
                chaineTableREF += "<td>";
                chaineTableREF +=  creeCodeBulle('id5Bubulle'+ clef + i, secondaires[i]);
                chaineTableREF += "</td>";
                chaineTableREF += "<td>  ";
                if (secondaires[i][1].length >120)
                    chaineTableREF += secondaires[i][1].substring(0,120) + "...";
                else
                    chaineTableREF += secondaires[i][1];
                chaineTableREF += "</td>";
                chaineTableREF += "</tr>";
                codes_sec = codes_sec + sep + secondaires[i][0];

            }
        }
        code_Ref_secondaire = document.getElementById(clef + '_codesRefSec');
        code_Ref_secondaire.value = codes_sec;

        urlRef = document.getElementById(clef + '_UrlRef');
        chaineTableREF += "</table>";
        chaineTableREF += "<div class='breaker'></div>";

        res.innerHTML = chaineTableREF;
        break;

	case 'cas10':
        res = document.getElementById('cas10' + clef);
        var chaineTableREF = "";

        chaineTableREF += "<table>";
        var codes_sec='';
        if (secondaires.length)
        {
        	var sep = '#';
            for (i = 0 ; i < secondaires.length ; i++) {
                chaineTableREF += "<tr>";
                chaineTableREF += "<td>";
                chaineTableREF += "</td>";
                chaineTableREF += "<td>  ";
                if (secondaires[i][1].length >120)
                    chaineTableREF += secondaires[i][1].substring(0,120) + "...";
                else
                    chaineTableREF += secondaires[i][1];
                chaineTableREF += "</td>";
                chaineTableREF += "</tr>";
                codes_sec = codes_sec + sep + secondaires[i][0];

            }
        }
        code_Ref_secondaire = document.getElementById(clef + '_codesRefSec');
        code_Ref_secondaire.value = codes_sec;

        chaineTableREF += "<tr>";
        chaineTableREF += "<td colspan='3'><div class='recherche-codes'>";
        urlRef = document.getElementById(clef + '_UrlRef');
        chaineTableREF += CreateLinkRefSansPrincipal(urlRef.value, principal, secondaires);
        chaineTableREF += "</div>";
        chaineTableREF += "</td>";
        chaineTableREF += "</tr>";
        chaineTableREF += "</table>";
        chaineTableREF += "<div class='breaker'></div>";

        res.innerHTML = chaineTableREF;
        break;

    case 'cas11':
		res = document.getElementById('cas11' + clef);
		codes_Ref_sec = document.getElementById(clef + '_codesRefSec');
		chaineTableREF = "<table>";
		var sep = '#';
		var codes_sec='';
		if (secondaires.length) {
			chaineTableREF += "<tr>";
			for (i = 0; i < secondaires.length; i++) {
				secondairesREF = new Array()
				secondairesREF[0] = secondaires[i][0];
				secondairesREF[1] = secondaires[i][1];
				chaineTableREF += "<td>";
				if (i != 0) {
					secondairesREF[0] = ";" + secondairesREF[0];
				}
				codes_sec = codes_sec + sep + secondaires[i][0];

				chaineTableREF += creeCodeBulle('id6Bubulle' + clef + i,
						secondairesREF);
				chaineTableREF += "</td>";
				//iCPV = i + 1;
				//code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
				//libeller_code_cpv_secondaire = document.getElementById(clef
				//		+ '_libelle' + '' + iCPV);
				//code_cpv_secondaire.value = secondaires[i][0];
				//libeller_code_cpv_secondaire.value = secondaires[i][1];
			}
			chaineTableREF += "</tr>";
		}
		codes_Ref_sec.value = codes_sec;

		chaineTableREF += "<tr>";
		chaineTableREF += "<td colspan='3'><div class='recherche-codes'>";
		urlRef = document.getElementById(clef + '_UrlRef');
		chaineTableREF += "</div>";
		chaineTableREF += "</td>";
		chaineTableREF += "</tr>";
		chaineTableREF += "</table>";
		chaineTableREF += "<div class='breaker'></div>";
		res.innerHTML = chaineTableREF;
		break;

	}

}
//Crée un lien choisir/modifier pour pré-remplir la popup Referentiel
function CreateLinkRef(urlRef, principal, secondaires) {

	var chaineP = "";
	var chaine = "";
	if (principal != null && principal[0] != "") {
		chaineP = principal[0];
		chaine = "&principal=" + chaineP;
	}

	var chaineS = "";
	for (i = 0; i < secondaires.length; i++) {
		if (chaineS != "") {
			chaineS += ",";
		}
		chaineS += secondaires[i][0];
	}
	if (secondaires.length != 0) {
		chaine += "&secondaires=" + chaineS;
	}
	if (principal != null && principal[0] != "" && secondaires != null
			&& secondaires.length < 1) {
		cssClass = "bouton-small";
		textButtonCpv = "<?php Prado::localize('MSG_DEFINIR') ?>";
	} else {
		cssClass = "bouton-details-edit clear-both";
		textButtonCpv = "<?php Prado::localize('MSG_DETAIL_OR_EDIT') ?>";
	}
	return "<a id=\"linkLtReferentiel\" href=\"#\" onclick=\"javascript:new_window('" + urlRef + chaine
			+ "','800px','720px', '1', '1')\"  class=\"" + cssClass + "\">"
			+ textButtonCpv + "</a>";
}

// Crée un span contenant le referentiel et le code pour l'infobulle
function creeCodeBulle(id, cpv) {
	var chaine = '<span class="code" onMouseOver="afficheBulle(\'';
	chaine += id + '\', this)" onMouseOut="cacheBulle(\'';
	chaine += id + '\')">' + cpv[0] + '</span>';

	chaine += '<div class="libelle" id="' + id + '"'
	chaine += 'onMouseOver="mouseOverBulle();"';
	chaine += 'onMouseOut="mouseOutBulle();"><div>' + cpv[1] + '</div></div>';

	return chaine;
}
//////////////////////////////////////Infos-bulles//////////////////////////////////

/*Checks IE 6.0*/
var name = navigator.appName;
var version = navigator.appVersion;

if (name == 'Microsoft Internet Explorer') {
	id = version.indexOf('MSIE');
	version = version.substring(id + 5, id + 9);
}

var idBulleT;
var chrono = null;
var delai = "500";
var brotherIdBullet;

function afficheBulle(idBulle, parent, displayBlock = true) {
	var bulle = document.getElementById(idBulle);
	var offset;
	var exp = new RegExp("^td_", "gi");

	if (chrono != null) {
		clearTimeout(chrono);
		cacheBulleT();
	}

	if (displayBlock) {
		bulle.style.display = "block";
	} else {
		bulle.style.display = "contents";
	}
	var bulleLeft = parent.offsetLeft + 10;
	var bulleTop = parent.offsetTop - bulle.offsetHeight;
    var name = navigator.appName ;

    if (name == 'Microsoft Internet Explorer') {

        bulleTop +=9;

    }

	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

	bulle.style.left = bulleLeft + 'px';
	bulle.style.top = bulleTop + 'px';

	if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
		bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';

	}

	//check Internet Exporer VErsion

	//create iframe to fix ie6 select z-index bug
	if (version == '6.0;') {

		var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="libelle-Iframe" title="Cadre vide"'
				+ ' id="' + idBulle + '-Iframe"' + '></iframe>';
		var myMessage = bulle.innerHTML;
		bulle.innerHTML = myBulleIframe + myMessage;
		var myBulleHeight = bulle.offsetHeight;
		var myBulleWidth = bulle.offsetWidth;
		myNewID = idBulle + '-Iframe';
		var myNewIframe = document.getElementById(myNewID);

		var parentClass = myNewIframe.parentNode.className;
		var iframeNewClass = parentClass + '-Iframe';
		myNewIframe.className = iframeNewClass;
		myNewIframe.style.height = myBulleHeight;
		myNewIframe.style.width = myBulleWidth;
		//alert(myNewIframe.style.height);
		//alert(myNewIframe.className);
	}

}

function cacheBulleT() {

    if(brotherIdBullet) {
        var bulle = J(brotherIdBullet).next();
        J(bulle).css("display", "none");
        brotherIdBullet = null;
    }else {
        document.getElementById(idBulleT).style.display = "none";
    }

	chrono = null;
}

function cacheBulle(idBulle) {
	idBulleT = idBulle;
	chrono = setTimeout("cacheBulleT()", delai);

	//document.getElementById(idBulle).style.display = "none";
}

function mouseOverBulle()

{
	clearTimeout(chrono);
	chrono = null;
}

function mouseOutBulle(parent) {
    let initParent = (parent == undefined) ? null : parent
    if (initParent != undefined) {
        chrono = setTimeout("cacheBulleT(initParent)", delai);
    } else {
        chrono = setTimeout("cacheBulleT()", delai);
    }
}

//////////////////////////////////////End Infos-bulles//////////////////////////////////
// Fonction appelée par la popup CPV
function retournerCPVs2(clef, principal, secondaires) {
	var min = document.getElementById("min").value;
	var maxCPV = document.getElementById("max").value;
	var langueCPV = document.getElementById("langueCPV").value;
	var typeCPV = document.getElementById("typeCPV").value;
	for (indexHidden = 1; indexHidden <= maxCPV; indexHidden++) {
		code_cpv_secondaire = document.getElementById(clef + indexHidden);
		libeller_code_cpv_secondaire = document.getElementById(clef
				+ '_libelle' + indexHidden);
		code_cpv_secondaire.value = '';
		libeller_code_cpv_secondaire.value = '';
	}

	var p = 0;
	switch (typeCPV) {
	case '0':
		res = document.getElementById("resultat");
		res.innerHTML = "Résultats:<br/>";
		if (principal != null) {
			res.innerHTML += "Principal: " + principal[0] + " : "
					+ principal[1];
		}

		if (secondaires != null) {
			chaine = "<br />Secondaires:<br/>";

			for (i = 0; i < secondaires.length; i++) {
				chaine += secondaires[i][0] + ":" + secondaires[i][1]
						+ "<br />";
			}
			res.innerHTML += chaine;
		}
		break;
	case 'cas1':
		res = document.getElementById('cas1');
		res.innerHTML = choisirModifier(clef, "&min=" + min + "&max=" + maxCPV
				+ "&demandeprincipal=true&locale=" + langueCPV + "", principal,
				secondaires);
		code_cpv_principal = document.getElementById('' + clef + '_1');
		libeller_code_cpv_principal = document
				.getElementById(clef + '_libelle' + '_1');
		code_cpv_principal.value = principal[0];
		libeller_code_cpv_principal.value = principal[1];
		break;
	case 'cas2':
		res = document.getElementById('cas2' + clef);
		chaineTableCPV = "<table>";
		if (principal[0] != null) {

			chaineTableCPV += "<tr>";
			chaineTableCPV += "<td>";
			chaineTableCPV += creeCodeBulle('id2Bubulle' + clef, principal);
			chaineTableCPV += "</td>";
			chaineTableCPV += "<td colspan='2'><span class='code'><i>(Code principal)</i></span></td>";
			chaineTableCPV += "</tr>";
			code_cpv_principal = document.getElementById(clef + '1');
			libeller_code_cpv_principal = document
					.getElementById(clef + '_libelle' + '1');
			code_cpv_principal.value = principal[0];
			libeller_code_cpv_principal.value = principal[1];
			p = 1;
			if (secondaires.length) {
				chaineTableCPV += "<tr>";
				for (i = 0; i < secondaires.length; i++) {
					secondairesCPV = new Array()
					secondairesCPV[0] = secondaires[i][0];
					secondairesCPV[1] = secondaires[i][1];
					chaineTableCPV += "<td>";
					if (i != 0) {
						secondairesCPV[0] = ";" + secondairesCPV[0];
					}
					chaineTableCPV += creeCodeBulle('id2Bubulle' + clef + i,
							secondairesCPV);
					chaineTableCPV += "</td>";
					iCPV = i + 1 + p;

					code_cpv_secondaire = document.getElementById(clef + ''
							+ iCPV);
					libeller_code_cpv_secondaire = document.getElementById(clef
							+ '_libelle' + '' + iCPV);
					code_cpv_secondaire.value = secondaires[i][0];
					libeller_code_cpv_secondaire.value = secondaires[i][1];
				}
				chaineTableCPV += "</tr>";
			}

		}
		chaineTableCPV += "<tr>";
		chaineTableCPV += "<td colspan='3'><div class='recherche-codes'>";
		chaineTableCPV += choisirModifier(clef, "&min=" + min + "&max="
				+ maxCPV + "&demandeprincipal=true&locale=" + langueCPV + "",
				principal, secondaires);
		chaineTableCPV += "<span id='erreurcas2"
				+ clef
				+ "'  style='display:none;color:red;'  title='Champ obligatoire' class='check-invalide'><img src='themes/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>";
		chaineTableCPV += "</div>";
		chaineTableCPV += "</td>";
		chaineTableCPV += "</tr>";
		chaineTableCPV += "</table>";
		/*if (principal.length == 0 || principal[0] == null)
		{
		    chaineTableCPV += "</br><span class='txt-attention'><img src='../images/picto-attention.gif' alt='Attention' title='Attention'> (Au moins une valeur à remplir si la case Codes CPV est cochée)</span>";
		}*/
		chaineTableCPV += "<div class='breaker'></div>";
		res.innerHTML = chaineTableCPV;
		break;

	case 'cas3':
		res = document.getElementById('cas3' + clef);
		chaine = "";

		if (principal[0] != "") {
			chaine = principal[0];
			p = 1;
		}

		for (i = 0; i < secondaires.length; i++) {
			if (chaine != "") {
				chaine += "; ";
			}
			chaine += secondaires[i][0];
			iCPV = i + 1 + p;
			code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
			libeller_code_cpv_secondaire = document.getElementById(clef
					+ '_libelle' + '' + iCPV);
			code_cpv_secondaire.value = secondaires[i][0];
			libeller_code_cpv_secondaire.value = secondaires[i][1];
		}

		res.innerHTML = chaine;
		break;
	case 'cas4':
		res = document.getElementById('cas4' + clef);
		chaineTableCPV = "<table>";
		res.innerHTML = chaineTableCPV;
		if (principal[0] != null) {

			chaineTableCPV += "<tr>";
			chaineTableCPV += "<td>";
			chaineTableCPV += creeCodeBulle('id4Bubulle' + clef, principal);
			chaineTableCPV += "</td>";
			chaineTableCPV += "<td colspan='2'><span class='code'><i>(Code principal)</i></span></td>";
			chaineTableCPV += "</tr>";
			code_cpv_principal = document.getElementById(clef + '1');
			libeller_code_cpv_principal = document
					.getElementById(clef + '_libelle' + '1');
			code_cpv_principal.value = principal[0];
			libeller_code_cpv_principal.value = principal[1];
			p = 1;
			if (secondaires.length) {
				chaineTableCPV += "<tr>";
				for (i = 0; i < secondaires.length; i++) {

					secondairesCPV = secondaires[i];
					chaineTableCPV += "<td>";
					if (i != 0) {
						secondairesCPV[0] = ";" + secondairesCPV[0];
					}
					chaineTableCPV += creeCodeBulle('id4Bubulle' + clef + i,
							secondairesCPV);
					chaineTableCPV += "</td>";
					iCPV = i + 1 + p;
					code_cpv_secondaire = document.getElementById(clef + ''
							+ iCPV);
					libeller_code_cpv_secondaire = document.getElementById(clef
							+ '_libelle' + '' + iCPV);
					code_cpv_secondaire.value = secondaires[i][0];
					libeller_code_cpv_secondaire.value = secondaires[i][1];
				}
				chaineTableCPV += "</tr>";
			}

		}
		chaineTableCPV += "</table>";
		chaineTableCPV += "<div class='breaker'></div>";
		res.innerHTML = chaineTableCPV;
		break;
	case 'cas5':
		res = document.getElementById('cas5' + clef);
		var chaine = "";

		chaine += "<table>";
		if (secondaires.length) {
			for (i = 0; i < secondaires.length; i++) {
				chaine += "<tr>";
				chaine += "<td>";
				chaine += creeCodeBulle('id5Bubulle' + clef + i, secondaires[i]);
				chaine += "</td>";
				chaine += "<td> : ";
				if (secondaires[i][1].length > 50)
					chaine += secondaires[i][1].substring(0, 51) + "...";
				else
					chaine += secondaires[i][1];
				chaine += "</td>";
				chaine += "</tr>";
				iCPV = i + 1;
				code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
				libeller_code_cpv_secondaire = document.getElementById(clef
						+ '_libelle' + '' + iCPV);
				code_cpv_secondaire.value = secondaires[i][0];
				libeller_code_cpv_secondaire.value = secondaires[i][1];
			}
		}
		chaine += "<tr>";
		chaine += "<td colspan='3'><div class='recherche-codes'>";
		chaine += choisirModifier(clef, "&min=" + min + "&max=" + maxCPV
				+ "&demandeprincipal=false&locale=" + langueCPV + "",
				principal, secondaires);
		chaine += "</div>";
		chaine += "</td>";
		chaine += "</tr>";
		chaine += "</table>";
		chaine += "<div class='breaker'></div>";
		res.innerHTML = chaine;
		break;
	case 'cas6':
		res = document.getElementById('cas6' + clef);
		chaineTableCPV = "<table>";
		if (secondaires.length) {
			chaineTableCPV += "<tr>";
			for (i = 0; i < secondaires.length; i++) {
				secondairesCPV = new Array()
				secondairesCPV[0] = secondaires[i][0];
				secondairesCPV[1] = secondaires[i][1];
				chaineTableCPV += "<td>";
				if (i != 0) {
					secondairesCPV[0] = ";" + secondairesCPV[0];
				}
				chaineTableCPV += creeCodeBulle('id6Bubulle' + clef + i,
						secondairesCPV);
				chaineTableCPV += "</td>";
				iCPV = i + 1;
				code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
				libeller_code_cpv_secondaire = document.getElementById(clef
						+ '_libelle' + '' + iCPV);
				code_cpv_secondaire.value = secondaires[i][0];
				libeller_code_cpv_secondaire.value = secondaires[i][1];
			}
			chaineTableCPV += "</tr>";
		}
		chaineTableCPV += "<tr>";
		chaineTableCPV += "<td colspan='3'><div class='recherche-codes'>";
		chaineTableCPV += choisirModifier(clef, "&min=" + min + "&max="
				+ maxCPV + "&demandeprincipal=false&locale=" + langueCPV + "",
				principal, secondaires);
		chaineTableCPV += "</div>";
		chaineTableCPV += "</td>";
		chaineTableCPV += "</tr>";
		chaineTableCPV += "</table>";
		chaineTableCPV += "<div class='breaker'></div>";
		res.innerHTML = chaineTableCPV;
		break;
	case 'cas7':
		res = document.getElementById('cas7' + clef);
		if (principal.length) {
			res.innerHTML = principal[0];
			res.innerHTML += " (<i>Code principal</i>)<br/>";
		}
		if (secondaires.length) {
			for (i = 0; i < secondaires.length; i++) {
				if (i != 0) {
					res.innerHTML += ";";
				}
				res.innerHTML += secondaires[i][0];
			}
		}
		break;
	case 'cas8':
		res = document.getElementById('cas8' + clef);
		var chaine = "";

		if (secondaires.length) {
			for (i = 0; i < secondaires.length; i++) {
				chaine += secondaires[i][0] + " : " + secondaires[i][1]
						+ "<br />";
				iCPV = i + 1;
				code_cpv_secondaire = document.getElementById(clef + '' + iCPV);
				libeller_code_cpv_secondaire = document.getElementById(clef
						+ '_libelle' + '' + iCPV);
				code_cpv_secondaire.value = secondaires[i][0];
				libeller_code_cpv_secondaire.value = secondaires[i][1];
			}
		}
		res.innerHTML = chaine;
		res.innerHTML += choisirModifier(clef, "&min=" + min + "&max=" + maxCPV
				+ "&demandeprincipal=false&locale=" + langueCPV + "",
				principal, secondaires);

		break;

	default:
		//alert('positionner une clef');
		bootbox.alert("positionner une clef");

	}

}

// Remplissage dynamique de la page
function remplirCPV(clef, principal, secondaires, themesA) {
	themes = "%2F" + themesA.replace(/\//, "%2F");
	retournerCPVs(clef, principal, secondaires);
}

function isCheckedCPVObligatoire(myInput) {
	if (myInput.checked == true) {
		var inputElems;
		inputElems = document.getElementsByTagName("div");
		for (i = 0; i < inputElems.length; i++) {
			var inputName = inputElems[i].name;
			if (inputName) {
				bootbox.alert(inputName);
				if (inputName.search(/bloccode_cpv/) == 0) {
					inputElems[i].style.display = 'block';
				}
			}
		}

		//document.getElementsByTagName().getElementById(myDiv).style.display = 'block';
	}
	if (myInput.checked == false) {
		var inputElems;
		inputElems = document.getElementsByTagName("div");
		for (i = 0; i < inputElems.length; i++) {
			var inputName = inputElems[i].name;
			if (inputName) {
				bootbox.alert(inputName);
				if (inputName.search(/bloccode_cpv/) == 0) {
					inputElems[i].style.display = 'none';
				}
			}
		}
		//document.getElementById(myDiv).style.display = 'none';
	}
}
//Crée un lien "Définir" ou "choisir/modifier" pour pré-remplir la popup Referentiel qui n'a pas de code principal
function CreateLinkRefSansPrincipal(urlRef, principal, secondaires) {

	var chaineP = "";
	var chaine = "";
	if (principal != null && principal[0] != "") {
		chaineP = principal[0];
		chaine = "&principal=" + chaineP;
	}

	var chaineS = "";
	for (i = 0; i < secondaires.length; i++) {
		if (chaineS != "") {
			chaineS += ",";
		}
		chaineS += secondaires[i][0];
	}
	if (secondaires.length != 0) {
		chaine += "&secondaires=" + chaineS;
	}
	if (secondaires == null || secondaires == "") {
		cssClass = "bouton-small";
		textButtonCpv = "<?php Prado::localize('MSG_DEFINIR') ?>";
	} else {
		cssClass = "bouton-details-edit clear-both";
		textButtonCpv = "<?php Prado::localize('MSG_DETAIL_OR_EDIT') ?>";
	}
	return "<a id=\"linkLtReferentielSansPrincipal\" href=\"#\" onclick=\"javascript:new_window('" + urlRef + chaine
			+ "','800px','720px', '1', '1')\"  class=\"" + cssClass + "\">"
			+ textButtonCpv + "</a>";
}

function afficheBulleWithJquery(parent) {
    var bulle = J(parent).next();
    var offset;
    var exp = new RegExp("^td_", "gi");
    if (chrono != null) {
        brotherIdBullet = parent;
        clearTimeout(chrono);
        cacheBulleT();
    }
    J(bulle).css("display", "block");
    var bulleLeft = parent.offsetLeft + 10;
    var bulleTop = parent.offsetTop - J(bulle).outerHeight();
    var name = navigator.appName ;
    if (name == 'Microsoft Internet Explorer') {
        bulleTop +=9;
    }
    var windowWidth = 0;
    if (typeof (window.innerWidth) == 'number') {
        windowWidth = window.innerWidth;
    } else {
        if (document.documentElement && document.documentElement.clientWidth) {
            windowWidth = document.documentElement.clientWidth;
        } else {
            if (document.body && document.body.clientWidth) {
                windowWidth = document.body.clientWidth;
            }
        }
    }
    J(bulle).css("left", bulleLeft + 'px');
    J(bulle).css("top", bulleTop + 'px');

    if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
        J(bulle).css("left", (bulleLeft - J(bulle).outerWidth()) + 'px');
    }
}

function cacheBulleWithJquery(parent) {
    brotherIdBullet = parent;
    chrono = setTimeout("cacheBulleT()", delai);
}
