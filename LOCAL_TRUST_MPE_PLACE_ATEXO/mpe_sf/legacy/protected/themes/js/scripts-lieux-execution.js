/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// JavaScript Document

//<!--

/*If checked, show or hide element*/
function isChecked(myInput,myDiv) {
if (myInput.checked == true) {
	document.getElementById(myDiv).style.display = 'block';
	}
if (myInput.checked == false) {
	document.getElementById(myDiv).style.display = 'none';
	}
}

/*If checked, check/uncheck collection*/
function isCheckedCheckCol(myInput,myDiv,totalCol) { 
	if (myInput.checked == true) {
		for (i=1;i<totalCol+1;i++) {
		document.getElementById(myDiv+'_dept'+i).checked = true;
		}
	}
	if (myInput.checked == false) {
		for (i=1;i<totalCol+1;i++) {
		document.getElementById(myDiv+'_dept'+i).checked = false;
		}
	}
}

/*Show element*/
function showDiv(myDiv) {
	document.getElementById(myDiv).style.display = 'block';	
}

/*Hide element*/
function hideDiv(myDiv) {

	document.getElementById(myDiv).style.display = 'none';	
}


function checkAll(repeater,repeaterDep,id,item,index){
	item =item+1;
	var i=0;
	var a;
	var bool;
	//alert('ctl0_CONTENU_PAGE_repeaterGeoN0_ctl'+index+'_'+repeater+'_ctl'+item+'_region'+id);return;
	var checkBoxAll = document.getElementById('ctl0_CONTENU_PAGE_repeaterGeoN0_ctl'+index+'_'+repeater+'_ctl'+item+'_region'+id);
	if(checkBoxAll.checked)
	{
	 bool = true;
	}
	else
	{
	 bool = false;
	}
		//alert(document.getElementById('ctl0_CONTENU_PAGE_repeaterGeoN0_ctl0_repeaterGeoN1_ctl'+item+'_repeaterGeoN2_'+id+'_ctl'+i+'_dep'+id));return;
	   while(a = document.getElementById('ctl0_CONTENU_PAGE_repeaterGeoN0_ctl'+index+'_repeaterGeoN1_ctl'+item+'_repeaterGeoN2_'+id+'_ctl'+i+'_dep'+id))
	    {
	    	a.checked=bool;
	    i++; 
	   }
	   
  }
  
function returnSelectedLieuxExec(idSelectedGeo,checked)
{
	window.opener.returnLieuxExecution(idSelectedGeo,checked);
	window.close();
}
//-->

