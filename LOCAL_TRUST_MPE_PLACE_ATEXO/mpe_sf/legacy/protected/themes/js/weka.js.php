function addListnerPage(isLoad) {
	<?php 
	if(!is_dir(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA'))) {
		system("mkdir ".Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA'));
	}
	if(!is_file(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_regles.xml')) {
		echo '';
	} else {
		echo Atexo_Weka_Generateur::generateWekaJs();				
	}
	?>
}

function addListnerElem(elem,event,func) {
	
	if (elem.addEventListener) {  // all browsers except IE before version 9
		elem.addEventListener (event, func, false);
	}
	else {
		if (elem.attachEvent) {   // IE before version 9
			elem.attachEvent ("on"+event, func);
		}
	}
}

function verifAndShow(elem,propriete,value,idWeka,isLoad) {
	
	if(!isLoad) {
		return;
	}
	propValue = null;
	if(propriete!='') {
		switch(propriete) {
			case 'class':propValue=elem.className;break;
			case 'selectedValue':propValue=elem.options[elem.selectedIndex].value;break;
			case 'value':propValue=elem.value;break;
		}
		
		if(propValue==value) {
			showWekaDiv(idWeka);		
		};
	}
	else {
		showWekaDiv(idWeka);
	}
}

function showWekaDiv(id) {
	hideAllWekaDiv();
	document.getElementById('weka_'+id).style.display='';
}

function hideAllWekaDiv() {

	divElems = document.getElementsByTagName("div");

	for( i=0; i < divElems.length; i++)
	{
		var divId = divElems[i].id;
		if( divId.search(/weka_/)==0)
		{
			divElems[i].style.display='none';
		}
	}
}

function getParamValue(param)
{
	var u = document.location.href;
	var reg = new RegExp('(\\?|&|^)'+param+'=(.*?)(&|$)');
	matches = u.match(reg);
	if(matches==null) {
		var reg = new RegExp('(\\?|&|^)'+param+'(.*?)(&|$)');
		matches = u.match(reg);
		if(matches==null) {
			return null;
		}
	}
	return matches[2] != undefined ? decodeURIComponent(matches[2]).replace(/\+/g,' ').replace('#','') : '';
}