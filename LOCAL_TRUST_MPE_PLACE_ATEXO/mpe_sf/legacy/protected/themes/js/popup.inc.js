/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

var sizer;
var oldPage;

/** popup avec animation
 * @ingroup javascript
 * @param page URL de la page à ouvrir dans la popup
 * @param w largeur finale de la popup
 * @param h hauteur finale de la popup
 * @param scrollbar 0 / 1
 * @param resize 0 / 1
 * @return VOID
 */
 
 
 function openPopupCommune(sessionName,sessionId)
{
	
  var element=document.getElementById('main_form');
  new_window('?page=RechercheCommune&param='+element.commune.value+'&'+sessionName+'='+sessionId,600,700,1,1);
}
function openPopupCommuneInstr(sessionName,sessionId)
{
	
  var element=document.getElementById('ctl0_CONTENU_PAGE_Commune');
  new_window('?page=RechercheCommune&action=instr&param='+element.value+'&'+sessionName+'='+sessionId,600,700,1,1);
}

function new_window(page,w,h,scrollbar, resize) 
{
	page=page.replace(/%3F/,"?");
	page=page.replace(/%26/g,"&");
	page=page.replace(/%3D/g,"=");
	page=page.replace(/%23/g,"#");
	popMail(page,w,h, scrollbar, resize);
} // fin


var popup = null; // sert pour popupWin(...)


/** ouvre une page HTML en POPUP
 * @ingroup javascript
 * @param _url URL de la page
 * @param _size dimensions de la fenêtre
 * @return VOID
 */
function popupWin(_url, _size) 
{
	window.open(_url, '_blank', _size);
	//window.name='main';
	if (popup != null) 
	{
		popup.location=popupURL;
		//self.name="main";
	} // end popup != null
}

/** ouvre une page HTML en POPUP
 * @ingroup javascript
 * @ingroup interne_fct
 * @param url URL de la page
 * @param x largeur en pixels
 * @param y hauteur en pixels
 * @param scrollbar 0 / 1
 * @param resize 0 / 1
 */
function popMail(url,x,y,scrollbar, resize) 
{
	popupWin(url,'toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars='+scrollbar+', resizable='+resize+', width='+x+', height='+y);
}

/** fonction muette
 * @ingroup javascript
 * sert pour TODO
 * @return VOID
 */
function x() 
{
	return;
}


function affiche(spanIn)
{
	
	if (spanIn == '')
		return;
		
	var spanToDisplay = document.getElementById(spanIn);
	
	document.getElementById('spanFormation').style.display="none";
	document.getElementById('spanSituationDemandeur').style.display="none";
	document.getElementById('spanSituationFinanciere').style.display="none";
	document.getElementById('spanCharges').style.display="none";
	
	
	document.getElementById('spanSubmitSituationDemandeur').style.display = "none";
	document.getElementById('spanSubmitFormation').style.display = "none";
	document.getElementById('spanSubmitCharges').style.display = "none";
	document.getElementById('spanSubmitSituationFinanciere').style.display = "none";

	document.getElementById('ospanFormation').className="atx1_Onglets1_g_er";
	document.getElementById('ospanSituationDemandeur').className="atx1_Onglets2";
	document.getElementById('ospanSituationFinanciere').className="atx1_Onglets4";
	document.getElementById('ospanCharges').className="atx1_Onglets5";

	
	
	
	var spanAAfficher=document.getElementById('spanAffiche');
	spanAAfficher.value=spanIn;


	spanToDisplay.style.display="";

	if(spanIn == 'spanFormation')
	{	
		document.getElementById('ospanFormation').className="atx1_Onglets1_er on";
		document.getElementById('ospanSituationDemandeur').className="atx1_Onglets2";
		document.getElementById('ospanSituationFinanciere').className="atx1_Onglets4";
		document.getElementById('ospanCharges').className="atx1_Onglets5";
		document.getElementById('onglet_jaune').style.backgroundImage ="url(images/atx1/bg_onglet5_d.gif)";
		document.getElementById('onglet_jaune').style.backgroundPosition = "right top"; 
		
		document.getElementById('spanSubmitFormation').style.display = "";
	}
	else
	{
		if( spanIn == 'spanCharges')
		{
			document.getElementById('ospanFormation').className="atx1_Onglets1_g_er";
			document.getElementById('ospanSituationDemandeur').className="atx1_Onglets2";
			document.getElementById('ospanSituationFinanciere').className="atx1_Onglets4";
			document.getElementById('ospanCharges').className="atx1_Onglets51 on";
			document.getElementById('onglet_jaune').style.backgroundImage ="url(images/atx1/bg_onglet5_d_on.gif)";
			document.getElementById('onglet_jaune').style.backgroundPosition = "right top"; 
					
			document.getElementById('spanSubmitCharges').style.display = "";
			
		}else
			{
				if( spanIn == 'spanSituationDemandeur' )
				{
					document.getElementById('ospanFormation').className="atx1_Onglets1_g_er";
					document.getElementById('ospanSituationDemandeur').className="atx1_Onglets11 on";
					document.getElementById('ospanSituationFinanciere').className="atx1_Onglets4";
					document.getElementById('ospanCharges').className="atx1_Onglets5";
					document.getElementById('spanSubmitSituationDemandeur').style.display = "";
					document.getElementById('onglet_jaune').style.backgroundImage ="url(images/atx1/bg_onglet5_d.gif)";
					document.getElementById('onglet_jaune').style.backgroundPosition = "right top"; 
							
				}else
				{	
					document.getElementById('ospanFormation').className="atx1_Onglets1_g_er";
					document.getElementById('ospanSituationDemandeur').className="atx1_Onglets2";
					document.getElementById('ospanSituationFinanciere').className="atx1_Onglets11 on";
					document.getElementById('ospanCharges').className="atx1_Onglets5";
					document.getElementById('onglet_jaune').style.backgroundImage ="url(images/atx1/bg_onglet5_d.gif)";
					document.getElementById('onglet_jaune').style.backgroundPosition = "right top"; 
					document.getElementById('spanSubmitSituationFinanciere').style.display = "";
				}
			}
	}
		

}
