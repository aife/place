function addFileUploadedMessecV2LocalStorage() {
    let datafileRcJson, datafileInDceJson;
    const data = [];

    setTimeout(function() {
            datafileRcJson = document.getElementById('ctl0_CONTENU_PAGE_fileRc').value;
            datafileInDceJson = document.getElementById('ctl0_CONTENU_PAGE_fileInDce').value;

            if (datafileRcJson != null && datafileRcJson != '') {
                data.push(JSON.parse(datafileRcJson))
            }
            if (datafileInDceJson != null && datafileInDceJson != '') {
                data.push(JSON.parse(datafileInDceJson));
            }

            if(data.length > 0) {
                localStorage.setItem('espace-documentaire-documents-selectionnes', JSON.stringify(data));
            }
        },
        1
    );
}

function getExtension(filename)
{
    return filename.split('.').pop();
}