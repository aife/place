function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant,extensions,methodeJavascriptRenvoiResultat) {
	document.SelectionFichierApplet.setMethodeJavascriptRenvoiResultat(methodeJavascriptRenvoiResultat);
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}
/**
 *
 *
 */
function initialiserSelectionDossierApplet(identifiant) {
   document.SelectionFichierApplet.initialiserRepertoire(identifiant);
   document.SelectionFichierApplet.executer();
}
/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier à signer
 */
function selectionEffectuee(identifiant, cheminFichierSelectionne) {
    document.getElementById(identifiant).value = cheminFichierSelectionne;
}

function selectionEffectueeReponse(identifiant, cheminFichierPdfSelectionne)
{
	document.getElementById(identifiant + '_cheminFichier').value = cheminFichierPdfSelectionne;
	document.getElementById(identifiant + '_ajouterPieceLibre').click();
	//document.getElementById('ctl0_CONTENU_PAGE_candidature_ctl0_atexoReponseEnveloppeCandidature_atexoReponsePiecesLibres_ajouterPieceLibre').click();
}

/**
 * Initialise de l'applet de signature Pades/Xades et de chiffrement.
 *
 * @param chiffrementRequis <code>true</code> si le chiffrement est requis ,sinon <code>false</code>
 * @param signatureRequise  <code>true</code> si la signature Xades est requise ,sinon <code>false</code>
 * @param signaturePadesActeEngagement  <code>true</code> si la signature Pades du fichier d'acte d'engagement est requis ,sinon <code>false</code>
 * @param effectuerVerificationSignature  <code>true</code> si la verification de la signature est requis (uniquement si le fichier de signature Xades est lui aussi passé en paramètre
 * @param afficherBarreProgression <code>true</code> si l'on veut afficher la barre de progression, sinon <code>false</code>
 * de la fonction d'ajout de fichiers),sinon <code>false</code>
 */
function initialiserSignDocApplet(cheminFichier,urlModuleValidation,cheminFichierFinal) {
    var url = null;
    var uid = null;
    document.MpeChiffrementApplet.initialiser(0, url, uid, 1, 0, 0, urlModuleValidation, 0);
    var cheminFichierDoc = document.getElementById(cheminFichier).value;
    if (!isEmpty(cheminFichierDoc)) {
        ajouterFichier(cheminFichierDoc, 1, 1, 0, cheminFichierFinal, "", null, 1);
    }
    document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.MpeChiffrementApplet.executer();
}

function initialiserCoSignHeliosApplet(HashFichier,nomFichier,identifiantFichier,urlModuleValidation) {
    HashFichierContent = document.getElementById(HashFichier).value;
    if (!isEmpty(HashFichierContent)) {
    	document.MpeChiffrementApplet.setMethodeJavascriptRecuperationLog('logAppletSignature');
    	document.MpeChiffrementApplet.initialiser(0, null, null, 1, 0, 0, urlModuleValidation, 1);
        document.MpeChiffrementApplet.ajouterHashFichier('0', '0', 1, identifiantFichier, HashFichierContent, nomFichier, "ACE", null, null);
    	document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    	document.MpeChiffrementApplet.executer();
    }
}

function ajouterFichier(cheminFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, cheminFichierSignatureXML, signatureNecessaire) {
    document.MpeChiffrementApplet.ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, null, cheminFichierSignatureXML, signatureNecessaire);
}
function ajouterHashFichier(hashFichierSelectionne, nomFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, contenuFichierSignatureXML) {
    document.MpeChiffrementApplet.ajouterHashFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, hashFichierSelectionne, nomFichierSelectionne, typeFichier, null, contenuFichierSignatureXML);
}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param reponseJson le contenu de la reponse au format json
 *
 */
function renvoiResultat(reponseJson) {
    try {
    	document.getElementById('ctl0_CONTENU_PAGE_signature').value=reponseJson;
    	var reponse = jQuery.parseJSON(reponseJson);
        for (var i = 0; i < reponse.fichiers.length; i++) {
        	var fichier = reponse.fichiers[i];
            var identifiantFichier = fichier.identifiant;

            var nomFichier = fichier.cheminSignatureXML;
            var chaine = nomFichier.split('*');
			if(chaine.length<=1){
				var index = nomFichier.lastIndexOf("\\");
				if(index<0) {
					index = nomFichier.lastIndexOf("/");
				}
           		hideProcessingBlock();
                bootbox.alert("<?php Prado::localize('SIGNATURE_GENEREE_DANS') ?>" + " : \n\n  -  <?php Prado::localize('TEXT_NOM_DU_JETON') ?> : \n"+nomFichier.substring(index+1,nomFichier.length)+"\n\n  -  <?php Prado::localize('TEXT_JETON_PLACE_DANS_LE_REPERTOIRE') ?> : \n"+ nomFichier.substring(0,index));
			}

        }

    } catch(e) {
        bootbox.alert(e);
    }
}

function renvoiResultatHelios(reponseJson) {
    try {
    	var reponse = jQuery.parseJSON(reponseJson);
        for (var i = 0; i < reponse.hashs.length; i++) {
        	var hash = reponse.hashs[i];
            var identifiantRepeater = hash.identifiant;
            var signature = hash.contenuSignatureXML;
            var idSignature = identifiantRepeater+'_signature';
        	var buttonAddCosignature =identifiantRepeater+'_buttonAddCosignature';
        	var wishOne = identifiantRepeater+'_wishOne';
        	document.getElementById(wishOne).value = '1';
        	document.getElementById(idSignature).value = signature;
        	document.getElementById(buttonAddCosignature).click();
        }

    } catch(e) {
        bootbox.alert(e);
    }
}


/**
 * Renvoie le resultat du déchiffrement effectue.
 *
 */
function renvoiResultatMpeDechiffrement(resultat) {
	jsEnveloppedDecrypted("");
}

/**
 * Ajoute des identifiant des enveloppes à déchiffrer (doit correspondre à celle se trouvant dans le fichier reponse annonce xml)
 * Peut être appeler n fois si l'on veut déchiffrer explicitement plusieurs enveloppe. Si on n'ajoute aucune restriction d'identifiant d'envelope, alors
 * l'ensemble des enveloppes se trouvant le fichier reponse annonce xml seront traités.
 *
 * @param idEnveloppe l'identifiant de l'enveloppe à déchiffrer
 */
function ajouterIdEnveloppe(idEnveloppe) {
    document.MpeDechiffrementApplet.ajouterIdEnveloppe(idEnveloppe);
}

/**
 * Renvoie le resultat du téléchargement effectue.
 *
 */
function renvoiResultatTelechargement(resultat) {
	panelInfos = document.getElementById('panelInfos');
	panelInfos.style.display = 'block';
	document.getElementById("panelInfos").focus();
}



/**
 * Méthode appelé lorsque l'ensemble des hash a été généré.
 */
function finTraitementSignaturePkcs7Applet() {

}

/**
 * Permet d'initialiser l'applet de selection des fichiers lors de la reponse
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserAppletSelectionFichierReponse(identifiant) {
	document.getElementById(identifiant + '_listePiecesSelectionnees').value = "";
	document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceLibre').value = identifiant;
    var extensions = new Array(<?php echo Atexo_Config::getParameter('EXTENSION_FILES_APPLET') ?>);
    document.SelectionFichierApplet.setMethodeJavascriptRecuperationLog('logAppletSelectionFile');
    document.SelectionFichierApplet.initialiser(identifiant, 1, extensions);
    document.SelectionFichierApplet.executer();
}
/**
 * Permet d'initialiser l'applet de selection des fichiers de l'acte d'engagement lors de la reponse
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserAppletSelectionFichierReponseAe(identifiant) {
	document.getElementById(identifiant + '_listePiecesSelectionnees').value = "";
	document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceType').value = identifiant;
    var extensions = new Array('PDF', 'DOC', 'ZIP', 'XLS', 'PPT');
    document.SelectionFichierApplet.setMethodeJavascriptRecuperationLog('logAppletGenerationAE');
    document.SelectionFichierApplet.initialiser(identifiant, 0, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier (appel n fois)
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier)
 * @param cheminFichier chemin du fichier
 */
function selectionMultipleEffectuee(identifiant, cheminFichier) {
    document.getElementById(identifiant).value = cheminFichier;
}

/**
*
* Methode appelée par l'applet après la sélection des pièces
*/
function selectionEffectueeAppletDepotReponse(identifiant, cheminFichierPdfSelectionne,tailleFichier)
{
	try{
		//Sauvegarde du parametre "identifiant" pour le réutiliser dans la fonction "finTraitement()"
		//Recuperer toutes les pièces renvoyées par l'applet de selection multiple pour les mettre dans un hidden: identifiant + '_listePiecesSelectionnees'
		document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value = identifiant;
		listePiece = document.getElementById(identifiant + '_listePiecesSelectionnees');
		var jsonListePiece = "";
		var myarray = new Array();
		if(listePiece.value){
			var jsonDataPiece = JSON.parse(listePiece.value);
			for (var i = 0; i < jsonDataPiece.fichiers.length; i++) {
				var fichier = jsonDataPiece.fichiers[i];
				myarray.push({
					"index": fichier.index,
					"cheminFichier": fichier.cheminFichier,
					"tailleFichier": fichier.tailleFichier
				});

			}
			var index = jsonDataPiece.fichiers.length;
			myarray.push({"index":index,"cheminFichier": cheminFichierPdfSelectionne,"tailleFichier": tailleFichier});
		}else{
			myarray.push({"index":0,"cheminFichier": cheminFichierPdfSelectionne,"tailleFichier": tailleFichier});
		}

		if(!tailleFichier){
			var nomFileVide = getNomFichier(cheminFichierPdfSelectionne);
            bootbox.alert('Attention, le fichier suivant est vide (0 Ko):\n'+nomFileVide);
		}
		jsonListePiece = JSON.stringify({fichiers: myarray});
		listePiece.value = jsonListePiece;
	} catch(e) {

	}

}

/**
 * Méthode appelée par l'applet à la fin du traitement pour la selection des pièces (simple ou multiple)
 */
function finTraitementAppletSelectionFichiers()
{
	initialiserAppletVerifSignature();

}

function initialiserAppletVerifSignature()
{
	typeFichier = "ACE";
	clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
	signatureRequis =  document.getElementById('ctl0_CONTENU_PAGE_isSignatureRequis').value;
	if(clientIdAjoutPiece && signatureRequis) {
		var listePiecesSelectionnees = document.getElementById(clientIdAjoutPiece + '_listePiecesSelectionnees').value;
		var jsonDataPiece = JSON.parse(listePiecesSelectionnees);
		var arrayListePieces = jsonDataPiece.fichiers;
		if(arrayListePieces.length) {
			//Initialisation de l'applet de vérification de la signature
			var urlPkcs11 = getUrlPcks11Libs();
			document.MpeChiffrementApplet.setUrlPkcs11LibRefXml(urlPkcs11);
			var urlModuleValidation = "<?php echo Atexo_Config::getParameter('URL_MODULE_VALIDATION_SIGNATURE') ?>";
			document.MpeChiffrementApplet.setMethodeJavascriptRecuperationLog('logAppletSignature');
			document.MpeChiffrementApplet.setMethodeJavascriptRenvoiResultat('renvoiResultatVerificationSignature');
			document.MpeChiffrementApplet.setOriginePlateforme('<?php echo Atexo_Config::getParameter('ORIGINE_PLATEFORME') ?>');
			document.MpeChiffrementApplet.setOrigineOrganisme('<?php echo Atexo_Config::getParameter('ORIGINE_ORGANISME') ?>');
			document.MpeChiffrementApplet.setOrigineItem('<?php echo Atexo_Config::getParameter('ORIGINE_ITEM') ?>');
			document.MpeChiffrementApplet.setOrigineContexteMetier('MPE_signature');
			document.MpeChiffrementApplet.setTypeHash('<?php echo Atexo_Config::getParameter('TYPE_ALGORITHM_HASH') ?>');
			document.MpeChiffrementApplet.setMethodeJavascriptDebutAttente('showLoader');
			document.MpeChiffrementApplet.setMethodeJavascriptFinAttente('hideLoader');
			document.MpeChiffrementApplet.initialiser(0, null, null, 0, 0, 1, urlModuleValidation, 0);

			//Ajout des fichiers à l'applet
			clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
			if(clientIdAjoutPiece) {
				var idHtmlCheminFichier = clientIdAjoutPiece + '_listePiecesSelectionnees';
				var cheminFichier = document.getElementById(idHtmlCheminFichier).value;
				if (!isEmpty(cheminFichier)) {
					for(var i=0;i < arrayListePieces.length;i++)
					{
						var fichierInfo = arrayListePieces[i];
						var pathFileSelectionne = fichierInfo.cheminFichier;
						var index = fichierInfo.index;
						if(pathFileSelectionne && !isNaN(index)) {
							document.MpeChiffrementApplet.ajouterFichier(1, 1, index, idHtmlCheminFichier, pathFileSelectionne, typeFichier, null, null, 1);
						}
					}
				}
			}

			//Executer l'applet
			document.MpeChiffrementApplet.executer();
		}
		//Masquer le loader "Veuillez patienter..." s'il est affiché sur la page
		hideLoader();
	}else{
		clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
		if(clientIdAjoutPiece) {
			document.getElementById(clientIdAjoutPiece + '_listeJsonRetoursAppletPiecesSelectionnees').value = document.getElementById(clientIdAjoutPiece + '_listePiecesSelectionnees').value;
			document.getElementById(clientIdAjoutPiece+'_ajouterPiecesSelectionnees').click();
		}
		hideLoader();

	}
}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param reponseJson le contenu de la reponse au format json
 *
 */
function renvoiResultatVerificationSignature(reponseSignatureFichierJson)
{
	try {
		reponseSignatureFichierJson = addSizeFileToResponseVerifSignature(reponseSignatureFichierJson);
		hideLoader();
		clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
		if(clientIdAjoutPiece) {
			document.getElementById(clientIdAjoutPiece + '_listeJsonRetoursAppletPiecesSelectionnees').value = reponseSignatureFichierJson;
			document.getElementById(clientIdAjoutPiece+'_ajouterPiecesSelectionnees').click();
		}
	} catch(e) {

	}
}

function addSizeFileToResponseVerifSignature(reponseSignatureFichierJson)
{
	try {
		clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
		if(clientIdAjoutPiece){
			var listePiecesSelectionnees = document.getElementById(clientIdAjoutPiece + '_listePiecesSelectionnees').value;
			var jsonDataPiece = JSON.parse(listePiecesSelectionnees);
			var responseJsonArray = JSON.parse(reponseSignatureFichierJson);
			for (var i = 0; i < jsonDataPiece.fichiers.length; i++) {
				var fichier = jsonDataPiece.fichiers[i];
				var indexFichierS = fichier.index;
				var tailleFichierS = fichier.tailleFichier;
				for (var j = 0; j < responseJsonArray.fichiers.length; j++) {
					var fichierR = responseJsonArray.fichiers[j];
					if(fichierR.index == indexFichierS){
						responseJsonArray.fichiers[j].tailleFichier = tailleFichierS;
					}
				}
			}
			reponseSignatureFichierJson = JSON.stringify(responseJsonArray);
		}
	} catch(e) {

	}
	return reponseSignatureFichierJson;

}

/**
 * Permet d'extraire le nom du fichier de la chaine contenant le chemin complet du fichier
 */
function getNomFichier(cheminFicher)
{
	var nomFichier = '';
	arrayCheminFichier = cheminFicher.split("\\");

	if(Array.isArray(arrayCheminFichier) && arrayCheminFichier.length) {
		nomFichier = arrayCheminFichier[arrayCheminFichier.length - 1];
	}
	return nomFichier;
}

///////////////////////////////////////////////////// Applet génération AE ///////////////////////////////////////////////////
/**
 * Lance l'initialisation de l'applet en mode génération de l'acte d'engagement.
 */
function initialiserGenerationActeEngagementApplet(id_inputAE, id_inputXMl, stringXml, urlDownloadAE, num_lot) {
	var arrayElement = id_inputAE.split('_');
	arrayElement.pop()
	var prefixeId = arrayElement.join('_');
	document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value = prefixeId;
	stringXml = base64_encode(document.getElementById(stringXml).value);
    document.MpeGenerationActeEngagementApplet.initialiserGeneration(id_inputAE, id_inputXMl, stringXml, 0, urlDownloadAE);
    document.MpeGenerationActeEngagementApplet.executer();
}

/**
 * Renvoi le resultat de la génération
 *
 * @param identifiantFichier
 * @param cheminFichier
 * @param identifiantFichierXML
 * @param cheminFichierXML.
 */
function renvoiResultatGeneration(identifiantFichier, cheminFichier, identifiantFichierXML, cheminFichierXML) {
	document.getElementById(identifiantFichier).value = cheminFichier;
    document.getElementById(identifiantFichierXML).value = cheminFichierXML;
	clientIdAjoutPiece = document.getElementById('ctl0_CONTENU_PAGE_valeurClientIdBoutonAjoutPiece').value;
	//alert(clientIdAjoutPiece+'_ajouterPiecesSelectionnees');
	if(clientIdAjoutPiece) {
    	document.getElementById(clientIdAjoutPiece+'_ajouterPiecesSelectionnees').click();
    }
}
/**
* Permet de loger les erreur de l'applet au moment de l'ajout d'un AE
*/
function logAppletGenerationAE(log){
	if(document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceType').value){
		var identiant = document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceType').value;
		document.getElementById(identiant+'_logAppletAjoutAE').value = log;
		document.getElementById(identiant+'_boutonlogAppletAjoutFichier').click();

	}
}
/**
* Permet de loger les erreur de l'applet au moment de l'ajout d'une
* piece libre
*/
function logAppletSelectionFile(log){
	if(document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceLibre').value){
		var identifiant = document.getElementById('ctl0_CONTENU_PAGE_clientIdPieceLibre').value;
		document.getElementById(identifiant+'_logAppletAjoutFichier').value = log;
		document.getElementById(identifiante+'_boutonlogAppletAjoutFichier').click();
	}
}
/**
* Permet de loger les erreurs de l'applet pendant la signature
*/
function logAppletSignature(log){
	document.getElementById('ctl0_CONTENU_PAGE_logAppletSignature').value = log;
}
/**
* Permet de loger les erreurs de l'applet pendant le depot de la reponse
*/
function logAppletDepotReponse(log){
	document.getElementById('ctl0_CONTENU_PAGE_logAppletdepotReponse').value = log;

}
