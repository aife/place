/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function appletSignHash(idHashElement,inOutValue)
{
 
 if(document.getElementById(idHashElement) && document.getElementById(idHashElement).value!="") {
  var os=navigator.platform;
  if(os=="Win32") {
   provider="MSCAPI";
  } else {
   provider="PKCS12";
  }
  document.AppletSignaturePkcs7FromHash.setParam1(provider);
  document.AppletSignaturePkcs7FromHash.setParam_1(document.getElementById(idHashElement).value);
  document.AppletSignaturePkcs7FromHash.setParam3(true);
  document.AppletSignaturePkcs7FromHash.setParam_2(inOutValue);
  
 } else {
  bootbox.alert("Erreur");
 }
}

function getAppletSignatureAndId(StrSignature,inOutValue)
{
	var signature = inOutValue+'_signature';
	var buttonAddCosignature =inOutValue+'_buttonAddCosignature';
	var wishOne = inOutValue+'_wishOne';
	document.getElementById(wishOne).value = '1';
	document.getElementById(signature).value = StrSignature;
	document.getElementById(buttonAddCosignature).click();
}