var	fixedX = -1			// x position (-1 if to appear below control)
var	fixedY = -1			// y position (-1 if to appear below control)
var startAt = 1			// 0 - sunday ; 1 - monday
var showWeekNumber = 1	// 0 - don't show; 1 - show
var showToday = 0	// 0 - don't show; 1 - show
var imgDir = "/themes/images/"			// directory for images ... e.g.

var gotoString = ""
var todayString = ""
var weekString = "<?php Prado::localize('MSG_SEMAINE') ?>"
var scrollLeftMessage = "<?php Prado::localize('MSG_DEFILE_MOIS_PREC') ?>"
var scrollRightMessage = "<?php Prado::localize('MSG_DEFILE_MOIS_SUIV') ?>"
var selectMonthMessage = "<?php Prado::localize('MSG_CHOIX_MOIS') ?>"
var selectYearMessage = "<?php Prado::localize('MSG_CHOIX_ANNEE') ?>"
var selectDateMessage = "<?php Prado::localize('MSG_SELECTIONNER_DATE') ?>"
var closeCalendarWindow = "<?php Prado::localize('MSG_FERMETURE_CALENDRIER') ?>"

var	crossobj, crossMonthObj, crossYearObj, monthSelected, yearSelected, dateSelected, omonthSelected, oyearSelected, odateSelected, monthConstructed, yearConstructed, intervalID1, intervalID2, timeoutID1, timeoutID2, ctlToPlaceValue, ctlNow, dateFormat, nStartingYear, defaultHoursDateFin

var	bPageLoaded=false
var	ie=document.all
var	dom=document.getElementById
var	ns4=document.layers
var	today =	new	Date()
var	dateNow	 = today.getDate()
var	monthNow = today.getMonth()
var	yearNow	 = today.getFullYear()
var	imgsrc = new Array("drop1.gif","drop2.gif","left1.gif","left2.gif","right1.gif","right2.gif")
var	img	= new Array()

var bShow = false;





/* hides <select> and <applet> objects (for IE only) */
function hideElement( elmID, overDiv )
{
if( ie )
{
for( i = 0; i < document.all.tags( elmID ).length; i++ )
{
  obj = document.all.tags( elmID )[i];
  if( !obj || !obj.offsetParent )
  {
	continue;
  }

  // Find the element's offsetTop and offsetLeft relative to the BODY tag.
  objLeft   = obj.offsetLeft;
  objTop    = obj.offsetTop;
  objParent = obj.offsetParent;


  while( objParent != null && objParent.tagName != null && objParent.tagName.toUpperCase() != "BODY" )
  {
	objLeft  += objParent.offsetLeft;
	objTop   += objParent.offsetTop;
	objParent = objParent.offsetParent;
  }

  objHeight = obj.offsetHeight;
  objWidth = obj.offsetWidth;

  if(( overDiv.offsetLeft + overDiv.offsetWidth ) <= objLeft );
  else if(( overDiv.offsetTop + overDiv.offsetHeight ) <= objTop );
  else if( overDiv.offsetTop >= ( objTop + objHeight ));
  else if( overDiv.offsetLeft >= ( objLeft + objWidth ));
  else
  {

	//obj.style.visibility = "hidden";

	/*Creation d'un iframe pour ie6 qui vient se placer sous le calendrier pour regler le pb des SELECT */

	if (version == '6.0;') {

	var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe" title="Cadre vide"'+' id="'+overDiv.id+'-Iframe"'+'></iframe>';
	var myMessage = overDiv.innerHTML;
	//alert(myMessage);
	overDiv.innerHTML = myBulleIframe+ myMessage;

	var myBulleHeight = overDiv.offsetHeight;
	var myBulleWidth = overDiv.offsetWidth;

	myNewID = overDiv.id+'-Iframe';

	var myNewIframe = document.getElementById(myNewID);

	myNewIframe.style.height = myBulleHeight;
	myNewIframe.style.width = myBulleWidth;

	}

  }
}
}
}

/*
* unhides <select> and <applet> objects (for IE only)
*/
function showElement( elmID )
{
if( ie )
{
for( i = 0; i < document.all.tags( elmID ).length; i++ )
{
  obj = document.all.tags( elmID )[i];

  if( !obj || !obj.offsetParent )
  {
	continue;
  }

  obj.style.visibility = "";

}
}
}

function HolidayRec (d, m, y, desc)
{
this.d = d
this.m = m
this.y = y
this.desc = desc
}

var HolidaysCounter = 0
var Holidays = new Array()

function addHoliday (d, m, y, desc)
{
Holidays[HolidaysCounter++] = new HolidayRec ( d, m, y, desc )
}

if (dom)
{
for	(i=0;i<imgsrc.length;i++)
{
	img[i] = new Image
	img[i].src = imgDir + imgsrc[i]
}
document.write ("<div onclick='bShow=true' id='calendar' style='z-index:+999999999;position:absolute;visibility:hidden;'><table class='datePicker'><tr class='tableHeader'><td><table style='width:248px'><tr><td class='tableHeaderLeft'><span id='caption'></span></td><td class='tableHeaderRight'><a href='javascript:hideCalendar()'><img src='"+imgDir+"close.gif' alt='"+closeCalendarWindow+"' /></a></td></tr></table></td></tr><tr class='tableContent'><td id='color_pop_calendar'><span id='content'></span></td></tr>")

if (showToday==1)
{
	document.write ("<tr style='background-color:#f0f0f0;'><td style='padding:5px; text-align:center'><span id='lblToday'></span></td></tr>")
}

document.write ("</table></div><div id='selectMonth' style='z-index:+999999999;position:absolute;visibility:hidden;'></div><div id='selectYear' style='z-index:+999999999;position:absolute;visibility:hidden;'></div>");
}

//var monthName = new Array("Janvier","F&eacute;vrier","Mars","Avril","Mai","Juin","Juillet","Ao&ucirc;t","Septembre","Octobre","Novembre","D&eacute;cembre")
//var	monthName2 = new Array("JAN","FEV","MAR","AVR","MAI","JUN","JUL","AOU","SEP","OCT","NOV","DEC")

var monthName = new Array("<?php Prado::localize('JS_MONTH1') ?>","<?php Prado::localize('JS_MONTH2') ?>","<?php Prado::localize('JS_MONTH3') ?>","<?php Prado::localize('JS_MONTH4') ?>","<?php Prado::localize('JS_MONTH5') ?>","<?php Prado::localize('JS_MONTH6') ?>","<?php Prado::localize('JS_MONTH7') ?>","<?php Prado::localize('JS_MONTH8') ?>","<?php Prado::localize('JS_MONTH9') ?>","<?php Prado::localize('JS_MONTH10') ?>","<?php Prado::localize('JS_MONTH11') ?>","<?php Prado::localize('JS_MONTH12') ?>")
var	monthName2 = new Array("JAN","FEV","MAR","AVR","MAI","JUN","JUL","AOU","SEP","OCT","NOV","DEC")

if (startAt==0)
{
//dayName = new Array ("Dim","Lun","Mar","Mer","Jeu","Ven","Sam")
dayName = new Array ("<?php Prado::localize('JS_DAY7') ?>","<?php Prado::localize('JS_DAY1') ?>","<?php Prado::localize('JS_DAY2') ?>","<?php Prado::localize('JS_DAY3') ?>","<?php Prado::localize('JS_DAY4') ?>","<?php Prado::localize('JS_DAY5') ?>","<?php Prado::localize('JS_DAY6') ?>")
}
else
{
//dayName = new Array ("Lun","Mar","Mer","Jeu","Ven","Sam","Dim")
dayName = new Array ("<?php Prado::localize('JS_DAY1') ?>","<?php Prado::localize('JS_DAY2') ?>","<?php Prado::localize('JS_DAY3') ?>","<?php Prado::localize('JS_DAY4') ?>","<?php Prado::localize('JS_DAY5') ?>","<?php Prado::localize('JS_DAY6') ?>","<?php Prado::localize('JS_DAY7') ?>")
}
var	styleAnchor="text-decoration:none;color:black;"
var	styleLightBorder="border-style:solid;border-width:1px;border-color:#a0a0a0;"

function swapImage(srcImg, destImg){
if (ie)	{ document.getElementById(srcImg).setAttribute("src",imgDir + destImg) }
}

function init()	{
if (!ns4)
{
	/*if (!ie) { yearNow += 1900	}*/

	crossobj=(dom)?document.getElementById("calendar").style : ie? document.all.calendar : document.calendar
	hideCalendar()

	crossMonthObj=(dom)?document.getElementById("selectMonth").style : ie? document.all.selectMonth	: document.selectMonth

	crossYearObj=(dom)?document.getElementById("selectYear").style : ie? document.all.selectYear : document.selectYear

	monthConstructed=false;
	yearConstructed=false;

	if (showToday==1)
	{
		document.getElementById("lblToday").innerHTML =	todayString + " <a onmousemove='window.status=\""+gotoString+"\"' onmouseout='window.status=\"\"' title='"+gotoString+"' style='"+styleAnchor+"' href='javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();'>"+dayName[(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+", " + dateNow + " " + monthName[monthNow].substring(0,3)	+ "	" +	yearNow	+ "</a>"
	}

	sHTML1="<span id='spanLeft'	style='border:1px solid #ddd;cursor:pointer' onmouseover='swapImage(\"changeLeft\",\"left2.gif\");this.style.border=\"1px solid #fff\";window.status=\""+scrollLeftMessage+"\"' onclick='javascript:decMonth()' onmouseout='clearInterval(intervalID1);swapImage(\"changeLeft\",\"left1.gif\");this.style.border=\"1px solid #ddd\";window.status=\"\"' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartDecMonth()\",500)' onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<img id='changeLeft' src='"+imgDir+"left1.gif' alt='"+scrollLeftMessage+"' /></span>&nbsp;"
	sHTML1+="<span id='spanRight' style='border:1px solid #ddd;cursor:pointer'	onmouseover='swapImage(\"changeRight\",\"right2.gif\");this.style.border=\"1px solid #fff\";window.status=\""+scrollRightMessage+"\"' onmouseout='clearInterval(intervalID1);swapImage(\"changeRight\",\"right1.gif\");this.style.border=\"1px solid #ddd\";window.status=\"\"' onclick='incMonth()' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartIncMonth()\",500)'	onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<img id='changeRight' src='"+imgDir+"right1.gif' alt='"+scrollRightMessage+"' /></span>&nbsp"
	sHTML1+="<span id='spanMonth' style='border:1px solid #ddd;cursor:pointer'	onmouseover='swapImage(\"changeMonth\",\"drop2.gif\");this.style.border=\"1px solid #fff\";window.status=\""+selectMonthMessage+"\"' onmouseout='swapImage(\"changeMonth\",\"drop1.gif\");this.style.border=\"1px solid #ddd\";window.status=\"\"' onclick='popUpMonth()'></span>&nbsp;"
	sHTML1+="<span id='spanYear' style='border:1px solid #ddd;cursor:pointer' onmouseover='swapImage(\"changeYear\",\"drop2.gif\");this.style.border=\"1px solid #fff\";window.status=\""+selectYearMessage+"\"'	onmouseout='swapImage(\"changeYear\",\"drop1.gif\");this.style.border=\"1px solid #ddd\";window.status=\"\"'	onclick='popUpYear()'></span>&nbsp;"

	document.getElementById("caption").innerHTML  =	sHTML1

	bPageLoaded=true
}
}

function hideCalendar()	{

if (crossobj) {
	crossobj.visibility="hidden"
	if (crossMonthObj != null){crossMonthObj.visibility="hidden"}
	if (crossYearObj !=	null){crossYearObj.visibility="hidden"}

	showElement( 'SELECT' );
	showElement( 'APPLET' );
	}
	initFunctions();

}

function padZero(num) {
return (num	< 10)? '0' + num : num ;
}

function constructDate(d,m,y)
{
sTmp = dateFormat
sTmp = sTmp.replace	("dd","<e>")
sTmp = sTmp.replace	("d","<d>")
sTmp = sTmp.replace	("<e>",padZero(d))
sTmp = sTmp.replace	("<d>",d)
sTmp = sTmp.replace	("mmmm","<p>")
sTmp = sTmp.replace	("mmm","<o>")
sTmp = sTmp.replace	("mm","<n>")
sTmp = sTmp.replace	("m","<m>")
sTmp = sTmp.replace	("<m>",m+1)
sTmp = sTmp.replace	("<n>",padZero(m+1))
sTmp = sTmp.replace	("<o>",monthName[m])
sTmp = sTmp.replace	("<p>",monthName2[m])
sTmp = sTmp.replace	("yyyy",y)
return sTmp.replace ("yy",padZero(y%100))
}

function closeCalendar()
{
var	sTmp

hideCalendar();

if(ctlToPlaceValue.name=="date_depot_question" || ctlToPlaceValue.name=="datefin_sad" || ctlToPlaceValue.name=="datefin" || ctlToPlaceValue.name=="date_depot" || ctlToPlaceValue.name=="datemiseenligne" ||  ctlToPlaceValue.name=="debut" ||  ctlToPlaceValue.name=="fin" ||  ctlToPlaceValue.name=="datetelechargement")
ctlToPlaceValue.value =	constructDate(dateSelected,monthSelected,yearSelected) + defaultHoursDateFin
else
ctlToPlaceValue.value =	constructDate(dateSelected,monthSelected,yearSelected)

if(ctlToPlaceValue.id=="ctl0_CONTENU_PAGE_FCSPdateEnvoiPub" || ctlToPlaceValue.id=="ctl0_CONTENU_PAGE_FCSPdateEnvoiInvitations" || ctlToPlaceValue.id=="ctl0_CONTENU_PAGE_FCSPdateLimiteRemiseOffres") {
    //  differenceDates('ctl0_CONTENU_PAGE_FCSPdateLimiteRemiseOffres', 'ctl0_CONTENU_PAGE_FCSPdateEnvoiInvitations', 'ctl0_CONTENU_PAGE_FCSPdateEnvoiPub' ,'ctl0_CONTENU_PAGE_FCSPdelaiRemiseOffre');
        document.getElementById('ctl0_CONTENU_PAGE_delaiValiditeOffre').click();
}

if(ctlToPlaceValue.id=="ctl0_CONTENU_PAGE_dateRemisePlis" ) {
	if(document.getElementById('ctl0_CONTENU_PAGE_bouttonRemplissageDateLimiteRemisePlisLocale')){
        document.getElementById('ctl0_CONTENU_PAGE_bouttonRemplissageDateLimiteRemisePlisLocale').click();
	}
}

if(ctlToPlaceValue.id=="ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis" ) {
	if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_bouttonRemplissageDateLimiteRemisePlisLocale')){
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_bouttonRemplissageDateLimiteRemisePlisLocale').click();
	}
	if(document.getElementById("ctl0_CONTENU_PAGE_etapesConsultation_publicite")) {
		//initPub();
	}
}

//gestion des liste de tranche budgetaite
fillListeTrancheBudbetaire(ctlToPlaceValue.id);

if (
    document.getElementById('ctl0_CONTENU_PAGE_attributaire_dateNotification') &&
    document.getElementById('ctl0_CONTENU_PAGE_attributaire_datePrevisionnelleFinMarche') &&
    document.getElementById('ctl0_CONTENU_PAGE_dureeMarche')
) {
    fillDureeInitialeMarche();
}
}

function fillListeTrancheBudbetaire(id){
	if(id == "ctl0_CONTENU_PAGE_dateNotification" || id == "ctl0_CONTENU_PAGE_dateReelleNotification") {
		if(document.getElementById('ctl0_CONTENU_PAGE_btRemplissageListeTranche')){
	        document.getElementById('ctl0_CONTENU_PAGE_btRemplissageListeTranche').click();
	        return;
		}
	} else {
		var valeur = id.split('_dateNotification');
		if(document.getElementById(valeur[0]+'_btRemplissageListeTranche')!= null){
			document.getElementById(valeur[0]+'_btRemplissageListeTranche').click();

		}
	}
}

function fillDureeInitialeMarche()
{
    var dateString1 = J('#ctl0_CONTENU_PAGE_attributaire_dateNotification').val();
    var dateString2 = J('#ctl0_CONTENU_PAGE_attributaire_datePrevFinMaxMarche').val();

    if (dateString1 != "" && dateString2 != "") {
        var dt1   = parseInt(dateString1.substring(0,2));
        var mon1  = parseInt(dateString1.substring(3,5));
        var yr1   = parseInt(dateString1.substring(6,10));
        var date1 = new Date(yr1, mon1-1, dt1);
        var dt2   = parseInt(dateString2.substring(0,2));
        var mon2  = parseInt(dateString2.substring(3,5));
        var yr2   = parseInt(dateString2.substring(6,10));
        var date2 = new Date(yr2, mon2-1, dt2);

        var yearDiff = (date2.getFullYear() - date1.getFullYear()) * 12;
        var monthDiff = Math.ceil((date2.getMonth() + '.' + date2.getDate()) - (date1.getMonth() + '.' + date1.getDate()));

        J('#ctl0_CONTENU_PAGE_dureeMarche').val(yearDiff + monthDiff);
    }
}

/*** Month Pulldown	***/

function StartDecMonth()
{
intervalID1=setInterval("decMonth()",80)
}

function StartIncMonth()
{
intervalID1=setInterval("incMonth()",80)
}

function incMonth () {
monthSelected++
if (monthSelected>11) {
	monthSelected=0
	yearSelected++
}
constructCalendar()
}

function decMonth () {
monthSelected--
if (monthSelected<0) {
	monthSelected=11
	yearSelected--
}
constructCalendar()
}

function constructMonth() {
popDownYear()
if (!monthConstructed) {
	sHTML =	""
	for	(i=0; i<12;	i++) {
		sName =	monthName[i];
		if (i==monthSelected){
			sName =	"<B>" +	sName +	"</B>"
		}
		sHTML += "<tr><td id='m" + i + "' onmouseover='this.style.backgroundColor=\"#dedede\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='monthConstructed=false;monthSelected=" + i + ";constructCalendar();popDownMonth();event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
	}

	document.getElementById("selectMonth").innerHTML = "<table class='selectionMois' onmouseover='clearTimeout(timeoutID1)'	onmouseout='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"popDownMonth()\",100);event.cancelBubble=true'>" +	sHTML +	"</table>"

	monthConstructed=true
}
}

function popUpMonth() {
constructMonth()
crossMonthObj.visibility = (dom||ie)? "visible"	: "show"
crossMonthObj.left = (parseInt(crossobj.left) + 50)+"px"
crossMonthObj.top =	(parseInt(crossobj.top) + 26)+"px"

hideElement( 'SELECT', document.getElementById("selectMonth") );
hideElement( 'APPLET', document.getElementById("selectMonth") );
}

function popDownMonth()	{
crossMonthObj.visibility= "hidden"
}

/*** Year Pulldown ***/

function incYear() {
for	(i=0; i<7; i++){
	newYear	= (i+nStartingYear)+1
	if (newYear==yearSelected)
	{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
	else
	{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
	document.getElementById("y"+i).innerHTML = txtYear
}
nStartingYear ++;
bShow=true
}

function decYear() {
for	(i=0; i<7; i++){
	newYear	= (i+nStartingYear)-1
	if (newYear==yearSelected)
	{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
	else
	{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
	document.getElementById("y"+i).innerHTML = txtYear
}
nStartingYear --;
bShow=true
}

function selectYear(nYear) {
yearSelected=parseInt(nYear+nStartingYear);
yearConstructed=false;
constructCalendar();
popDownYear();
}

function constructYear() {
popDownMonth()
sHTML =	""
if (!yearConstructed) {

	sHTML =	"<tr><td style='text-align:center;' onmouseover='this.style.backgroundColor=\"#dedede\"' onmouseout='clearInterval(intervalID1);this.style.backgroundColor=\"\"' style='cursor:pointer'	onmousedown='clearInterval(intervalID1);intervalID1=setInterval(\"decYear()\",30)' onmouseup='clearInterval(intervalID1)'>-</td></tr>"

	j =	0
	nStartingYear =	yearSelected-3
	for	(i=(yearSelected-3); i<=(yearSelected+3); i++) {
		sName =	i;
		if (i==yearSelected){
			sName =	"<B>" +	sName +	"</B>"
		}

		sHTML += "<tr><td id='y" + j + "' onmouseover='this.style.backgroundColor=\"#dedede\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='selectYear("+j+");event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
		j ++;
	}

	sHTML += "<tr><td style='text-align:center;' onmouseover='this.style.backgroundColor=\"#dedede\"' onmouseout='clearInterval(intervalID2);this.style.backgroundColor=\"\"' style='cursor:pointer' onmousedown='clearInterval(intervalID2);intervalID2=setInterval(\"incYear()\",30)'	onmouseup='clearInterval(intervalID2)'>+</td></tr>"

	document.getElementById("selectYear").innerHTML	= "<table class='selectionAnnee' onmouseover='clearTimeout(timeoutID2)' onmouseout='clearTimeout(timeoutID2);timeoutID2=setTimeout(\"popDownYear()\",100)' cellspacing=0>"	+ sHTML	+ "</table>"

	yearConstructed	= true
}
}

function popDownYear() {
clearInterval(intervalID1)
clearTimeout(timeoutID1)
clearInterval(intervalID2)
clearTimeout(timeoutID2)
crossYearObj.visibility= "hidden"
}

function popUpYear() {
var	leftOffset

constructYear()
crossYearObj.visibility	= (dom||ie)? "visible" : "show"
leftOffset = parseInt(crossobj.left) + document.getElementById("spanYear").offsetLeft
if (ie)
{
	leftOffset += 6
}
crossYearObj.left =	leftOffset+"px"
crossYearObj.top = (parseInt(crossobj.top) + 26)+"px"
}

/*** calendar ***/
function WeekNbr(n) {
// Algorithm used:
// From Klaus Tondering's Calendar document (The Authority/Guru)
// hhtp://www.tondering.dk/claus/calendar.html
// a = (14-month) / 12
// y = year + 4800 - a
// m = month + 12a - 3
// J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
// d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
// L = d4 / 1460
// d1 = ((d4 - L) mod 365) + L
// WeekNumber = d1 / 7 + 1

year = n.getFullYear();
month = n.getMonth() + 1;
if (startAt == 0) {
 day = n.getDate() + 1;
}
else {
 day = n.getDate();
}

a = Math.floor((14-month) / 12);
y = year + 4800 - a;
m = month + 12 * a - 3;
b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
var J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
L = Math.floor(d4 / 1460);
d1 = ((d4 - L) % 365) + L;
week = Math.floor(d1/7) + 1;

return week;
}

function constructCalendar () {
var aNumDays = Array (31,0,31,30,31,30,31,31,30,31,30,31)

var dateMessage
var	startDate =	new	Date (yearSelected,monthSelected,1)
var endDate

if (monthSelected==1)
{
	endDate	= new Date (yearSelected,monthSelected+1,1);
	endDate	= new Date (endDate	- (24*60*60*1000));
	numDaysInMonth = endDate.getDate()
}
else
{
	numDaysInMonth = aNumDays[monthSelected];
}

datePointer	= 0
dayPointer = startDate.getDay() - startAt

if (dayPointer<0)
{
	dayPointer = 6
}

sHTML =	"<table border=0 style='font-family:verdana;font-size:10px;border:0;'><tr>"

if (showWeekNumber==1)
{
	sHTML += "<td class='weekColumn'><strong>" + weekString + "</strong></td><td rowspan=7 class='weekSeparator'>&nbsp;</td>"
}

for	(i=0; i<7; i++)	{
	sHTML += "<td class='weekday'><strong>"+ dayName[i]+"</strong></td>"
}
sHTML +="</tr><tr>"

if (showWeekNumber==1)
{
	sHTML += "<td style='text-align:right;'>" + WeekNbr(startDate) + "&nbsp;</td>"
}

for	( var i=1; i<=dayPointer;i++ )
{
	sHTML += "<td>&nbsp;</td>"
}

for	( datePointer=1; datePointer<=numDaysInMonth; datePointer++ )
{
	dayPointer++;
	sHTML += "<td style='text-align:right;'>"
	sStyle=styleAnchor
	if ((datePointer==odateSelected) &&	(monthSelected==omonthSelected)	&& (yearSelected==oyearSelected))
	{ sStyle+=styleLightBorder }

	sHint = ""
	for (k=0;k<HolidaysCounter;k++)
	{
		if ((parseInt(Holidays[k].d)==datePointer)&&(parseInt(Holidays[k].m)==(monthSelected+1)))
		{
			if ((parseInt(Holidays[k].y)==0)||((parseInt(Holidays[k].y)==yearSelected)&&(parseInt(Holidays[k].y)!=0)))
			{
				sStyle+="background-color:#FFDDDD;"
				sHint+=sHint==""?Holidays[k].desc:"\n"+Holidays[k].desc
			}
		}
	}

	var regexp= /\"/g
	sHint=sHint.replace(regexp,"&quot;")

	dateMessage = "onmousemove='window.status=\""+selectDateMessage.replace("[date]",constructDate(datePointer,monthSelected,yearSelected))+"\"' onmouseout='window.status=\"\"' "

	if ((datePointer==dateNow)&&(monthSelected==monthNow)&&(yearSelected==yearNow))
	{ sHTML += "<strong><a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer+";closeCalendar();'><span class='red'>&nbsp;" + datePointer + "</span>&nbsp;</a></strong>"}
	else if	(dayPointer % 7 == (startAt * -1)+1)
	{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;<span class='weekend'>" + datePointer + "</span>&nbsp;</a>" }
	else
	{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;" + datePointer + "&nbsp;</a>" }

	sHTML += ""
	if ((dayPointer+startAt) % 7 == startAt) {
		sHTML += "</tr><tr>"
		if ((showWeekNumber==1)&&(datePointer<numDaysInMonth))
		{
			sHTML += "<td style='text-align:right;'>" + (WeekNbr(new Date(yearSelected,monthSelected,datePointer+1))) + "&nbsp;</td>"
		}
	}
}

document.getElementById("content").innerHTML   = sHTML
document.getElementById("spanMonth").innerHTML = "&nbsp;" +	monthName[monthSelected] + "&nbsp;<img id='changeMonth' src='"+imgDir+"drop1.gif' alt='"+selectMonthMessage+"' />"
document.getElementById("spanYear").innerHTML =	"&nbsp;" + yearSelected	+ "&nbsp;<img id='changeYear' src='"+imgDir+"drop1.gif' alt='"+selectYearMessage+"' />"
}

function popUpCalendar(ctl,	ctl2, format,defaultHours) {


var	leftpos=0
var	toppos=0

if (bPageLoaded)
{
	if ( crossobj.visibility ==	"hidden" ) {
		ctlToPlaceValue	= ctl2
		defaultHoursDateFin=defaultHours;
		dateFormat=format;

		formatChar = " "
		aFormat	= dateFormat.split(formatChar)
		if (aFormat.length<3)
		{
			formatChar = "/"
			aFormat	= dateFormat.split(formatChar)
			if (aFormat.length<3)
			{
				formatChar = "."
				aFormat	= dateFormat.split(formatChar)
				if (aFormat.length<3)
				{
					formatChar = "-"
					aFormat	= dateFormat.split(formatChar)
					if (aFormat.length<3)
					{
						// invalid date	format
						formatChar=""
					}
				}
			}
		}

		tokensChanged =	0
		if ( formatChar	!= "" )
		{
			// use user's date
			aData =	ctl2.value.split(formatChar)

			for	(i=0;i<3;i++)
			{
				if ((aFormat[i]=="d") || (aFormat[i]=="dd"))
				{
					dateSelected = parseInt(aData[i], 10)
					tokensChanged ++
				}
				else if	((aFormat[i]=="m") || (aFormat[i]=="mm"))
				{
					monthSelected =	parseInt(aData[i], 10) - 1
					tokensChanged ++
				}
				else if	(aFormat[i]=="yyyy")
				{
					yearSelected = parseInt(aData[i], 10)
					tokensChanged ++
				}
				else if	(aFormat[i]=="mmm")
				{
					for	(j=0; j<12;	j++)
					{
						if (aData[i]==monthName[j])
						{
							monthSelected=j
							tokensChanged ++
						}
					}
				}
				else if	(aFormat[i]=="mmmm")
				{
					for	(j=0; j<12;	j++)
					{
						if (aData[i]==monthName2[j])
						{
							monthSelected=j
							tokensChanged ++
						}
					}
				}
			}
		}

		if ((tokensChanged!=3)||isNaN(dateSelected)||isNaN(monthSelected)||isNaN(yearSelected))
		{
			dateSelected = dateNow
			monthSelected =	monthNow
			yearSelected = yearNow
		}

		odateSelected=dateSelected
		omonthSelected=monthSelected

		oyearSelected=yearSelected

		aTag = ctl
        leftpos	= J('#'+ctl2.id).offset().left;
        toppos = J('#'+ctl2.id).offset().top;
		do {
			aTag = aTag.offsetParent;
			crossobj.left =	leftpos+"px"
			crossobj.top = toppos+"px"
			/*Adjust page size if calendar get over border*/
			/*End Adjust page size if calendar get over border*/

		} while(aTag.tagName=="BODY");

		//crossobj.left =	fixedX==-1 ? ctl.offsetLeft	+ leftpos :	fixedX
		crossobj.left =	leftpos+"px"
		crossobj.top = toppos+22+"px"

		constructCalendar (1, monthSelected, yearSelected);
		crossobj.visibility=(dom||ie)? "visible" : "show"

		hideElement( 'SELECT', document.getElementById("calendar") );
		hideElement( 'APPLET', document.getElementById("calendar") );

		bShow = true;
		/*Adjust page size if c,alendar get over border*/
		resetPopupHeight(toppos);
		/*End Adjust page size if calendar get over border*/
	}
	else
	{
		resetPopupHeight(toppos,leftpos);
		hideCalendar()
		if (ctlNow!=ctl) {popUpCalendar(ctl, ctl2, format)}
	}
	ctlNow = ctl
}
}

//document.onkeypress = function hidecal1 () {
//if (event.keyCode==27)
//{
//	hideCalendar()
//}
//}
document.onclick = function hidecal2 () {
if (!bShow)
{
	hideCalendar()
}
bShow = false
}

if(ie)
{
init()
}
else
{
window.onload=init
}
