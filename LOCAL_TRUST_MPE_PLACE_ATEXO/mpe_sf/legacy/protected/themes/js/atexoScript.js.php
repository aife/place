/**
 * Si la consultation est allotie on vérifie qu'on a une catégorie pour chaque lot
 *
 */
function ValidateCategoryLot(sender, parameter)
{
    res = true;

    if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true && parameter == 0)
    {
        res = false;
    }
    return res;
}
function isDureeMarcheValid(sender, parameter){
    var dureeMarche = sender.control.value.toInteger();

    if(dureeMarche && dureeMarche>0 ){
        return true;
    }

    return false;
}

function validateAccessCode(sender, parameter)
{
    res = true;

    if(document.getElementById("ctl0_CONTENU_PAGE_accesRestreint").checked == true && parameter == 0)
    {
        res = false;
    }
    return res;
}

// duplicata validateAccessCode

function validateAccessCodeFormulaireConsultation(sender, parameter)
{
    res = true;
    valeur = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_accessCode").value;
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_accesRestreint").checked == true && (valeur == 0 || valeur == '') )
    {
        document.getElementById('erreurCodeAccess').style.display='';
        res = false;
    }else{
        document.getElementById('erreurCodeAccess').style.display='none';
    }
    return res;
}

function ValidateIntituleLot(sender, parameter)
{
    res = true;

    if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true && parameter == 0)
    {
        res = false;
    }
    return res;
}
function ValidateIntitulesLots()
{
    res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true)
    {
        i = 1;
        while (intituleLot = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_intituleLot"))
        {
            if (!intituleLot.value)
            {
                document.getElementById("errorIntituleLot" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}


function ValidateCategoriesLots()
{
    res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true)
    {
        i = 1;
        while (categorieLot = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_categorieLot"))
        {
            if (categorieLot.options[categorieLot.selectedIndex].value == 0)
            {
                document.getElementById("errorCategorieLot" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}

function ValidateNumLot()
{
    res = true;
    var arrayNumLots = new Array();
    if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true)
    {
        i = 1;
        var regExp = new RegExp("\[1-9][0-9]*");
        while (numLot = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_numLot"))
        {
            //if (numLot.value == '0' || numLot.value='')
            if(!(numLot.value.match(regExp)))
            {
                document.getElementById("errorNumLot" + i).style.display = '';
                res = false;
            }
            if (inArray(arrayNumLots, numLot.value)) {
                document.getElementById("errorNumLot" + i).style.display = '';
                res = false;
            } else {
                arrayNumLots[i] = numLot.value;
            }
            i++;
        }
    }
    return res;
}

function ValidateLieuExecution(sender, parameter)
{
    if(document.getElementById("ctl0_CONTENU_PAGE_idsSelectedGeoN2").value != '')
    {
        return true;
    }
    else
    {
        return false;
    }

}
function ValidateCodeCPV(sender, parameter)
{
    res = true;
    resLot = true;
    if(!document.getElementById("ctl0_CONTENU_PAGE_idAtexoRef_codeRefPrinc").value)
    {
        document.getElementById("erreurcas2ctl0_CONTENU_PAGE_idAtexoRef").style.display = '';
        res = false;
    }
    if(document.getElementById("ctl0_CONTENU_PAGE_marcheUnique").checked == true)
    {
        return res;
    }
    else if(document.getElementById("ctl0_CONTENU_PAGE_alloti").checked == true)
    {
        i = 1;

        while (codeCpvLot = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_idAtexoRef_codeRefPrinc"))
        {

            if (!codeCpvLot.value)
            {
                document.getElementById("erreurcas2ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_idAtexoRef").style.display = '';
                resLot = false;
            }
            i++;
        }
    }
    if (resLot && res)
        return true;
    else
        return false;
}

function ValidateOneCodeCPV(sender, parameter)
{
    res = true;
    resherite = false;
    if(document.getElementById("ctl0_CONTENU_PAGE_cpvHeriteeConsultation")){
        resherite = document.getElementById("ctl0_CONTENU_PAGE_cpvHeriteeConsultation").checked;
    }
    if(!resherite && !document.getElementById("ctl0_CONTENU_PAGE_idAtexoRef_codeRefPrinc").value)
    {
        document.getElementById("erreurcas2ctl0_CONTENU_PAGE_idAtexoRef").style.display = '';
        res = false;
    }
    return res;
}

function replaceAll(str, search, repl)
{
    while (str.indexOf(search) != -1)
        str = str.replace(search, repl);
    return str;
}

function toUpperCaseAndremoveAccent(str)
{
    var norm = new Array("à", "é","À", "Â", "À", "Ä" ,"â", "Ô", "Ò", "Ö", "ô", "ò", "ö", "É", "È", "Ê", "Ë", "é", "è", "ê", "ë", "Ç", "ç", "Î", "Ï", "î", "ï", "Û", "Ü", "Ù", "ù" , "û" , "ü" , "ÿ" , "Ñ" , "ñ"  );
    var spec = new Array("a", "e","A", "A", "A", "A" ,"a", "O", "O", "O", "o", "o", "o", "E", "E", "E", "E", "e", "e", "e", "e", "C", "c", "I", "I", "i", "i", "U", "U", "U", "u" , "u" , "u" , "y" , "N" , "n"  );
    for (var i = 0; i < spec.length; i++)
        str = replaceAll(str, norm[i], spec[i]);
    return str.toUpperCase();
}



/**
 * @name isSirenValide
 *
 *
 * @return   Un booléen qui vaut 'true' si le code SIREN passé en
 *                           paramêtre est valide, false sinon.
 * @param siren
 * @param Boolean isValidationDisabled
 */
function isSirenValide(siren, isValidationDisabled = false) {

    if (isValidationDisabled === true && !isNaN(siren)) {
        return true;
    } else if ((siren.length != 9) || (isNaN(siren)) || (siren == 0)) {
        return false;
    } else if (siren == '356000000') {
        return true;
    }
    else {
        // Donc le SIREN est un numérique à 9 chiffres
        var somme = 0;
        var tmp;
        for (var cpt = 0; cpt < siren.length; cpt++) {
            if ((cpt % 2) == 1) { // Les positions paires : 2ème, 4ème, 6ème et 8ème chiffre
                tmp = siren.charAt(cpt) * 2; // On le multiplie par 2
                if (tmp > 9) tmp -= 9;  // Si le résultat est supérieur à 9, on lui soustrait 9
            }
            else tmp = siren.charAt(cpt);

            somme += parseInt(tmp);
        } // fin for

        if ((somme % 10) == 0) {
            return true;
        }
        else {
            return false;
        }
    } // fin else
} // fin isSirenValide()

function isSiretValide(siret, siren, isValidationDisabled = false) {
    var parameter = siren.concat(siret);
    var estValide;

    if (siret == 0 && siren == 0) {
        estValide = false;
    }

    if (isValidationDisabled === true && !isNaN(parameter) && parameter.length > 0) {
        return true;
    } else if ((parameter.length != 14) || (isNaN(parameter)) || (parameter == 0)) {
        estValide = false;
    } else if (siren == '356000000') {
        estValide = true;
    }
    else {
        // Donc le SIRET est un numérique à 14 chiffres
        // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4suivants
        // correspondent au numéro d'établissement
        // et enfin le dernier chiffre est une clef de LUHN.
        var somme = 0;
        var tmp;
        for (var cpt = 0; cpt < parameter.length; cpt++) {
            if ((cpt % 2) == 0) { // Les positions impaires : 1er, 3è, 5è,etc...
                tmp = parameter.charAt(cpt) * 2; // On le multiplie par 2
                if (tmp > 9)
                    tmp -= 9;    // Si le résultat est supérieur à 9, on lui soustrait 9
            }
            else
                tmp = parameter.charAt(cpt);
            somme += parseInt(tmp);
        }
        if ((somme % 10) == 0)
            estValide = true; // Si la somme est un multiple de 10 alors le SIRET est valide
        else
            estValide = false;
    }
    return estValide;
}


function controlValidationSiret(siren, siret)
{
    if (typeof siret !== "string") {
        var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    }
    if (typeof siren !== "string") {
        var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    }
    if ((siren !=0) && (siret==0)) {
        if(isSirenValide(siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function ValidationSiret()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_codeEtablissement').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    if ((siren !=0) && (siret==0)) {
        if(isSirenValide(siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function validatePassword(sender, parameter)
{
    var CaseLogin =document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_CaseLogin');
    if(CaseLogin.checked==true)
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_password').value =='')
        {
            return false;
        }
    }
    return true;

}
function validateLogin(sender, parameter)
{
    var CaseLogin =document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_CaseLogin');
    if(CaseLogin.checked==true)
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_identifiant').value =='')
        {
            return false;
        }
    }
    return true;

}
function validateConfPassword(sender, parameter)
{
    var CaseLogin =document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_CaseLogin');
    if(CaseLogin.checked==true)
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_confPassword').value =='')
        {
            return false;
        }
    }
    return true;

}
function controlValidationListPays()
{
    var listPays = document.getElementById('ctl0_CONTENU_PAGE_listPays');
    if(listPays.selectedIndex == 0 )
    {
        return false;
    }
    else {
        return true;
    }

}
function isSirenEntrepriseValide()
{
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    if ((siren !=0)) {
        if(isSirenValide(siren)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function totalCA(vente,biens,services,total)
{
    var venteMontant = document.getElementById(vente).value;
    var biensMontant = document.getElementById(biens).value;
    var servicesMontant = document.getElementById(services).value;
    var totalMontant = parseFloat(venteMontant) + parseFloat(biensMontant) + parseFloat(servicesMontant);
    if(totalMontant.toString()!='NaN')
        document.getElementById(total).value = totalMontant.toString();
}

function validatorChamps()
{
    radioObj=document.forms['main_form'].elements['mode_certificate'];
    var radioLength = radioObj.length;
    noError=true;
    ChoiceMade=false;
    browserIe=true; // valeur par défaut, ce paramêtre ne sert plus
    // champs obligatoire le nom du certificat
    if (document.getElementById("ctl0_CONTENU_PAGE_nomBiCle").value == "")  {
        document.getElementById("nameError").style.display="";
        document.getElementById("crossName").style.display="";
        noError=false;
    } else {
        document.getElementById("nameError").style.display="none";
        document.getElementById("crossName").style.display="none";
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_modification').value=='0')
    {
        // Cas creation bi-cles si on a choisi une option
        for(var i = 0; i < radioLength; i++) {
            if(radioObj[i].checked) {
                ChoiceMade=true;
            }
        }
    } else {
        // Cas modification bi-cles
        ChoiceMade=true;
    }

    if(ChoiceMade)
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_modification').value=='0')
        {
            if(document.getElementById('ctl0_CONTENU_PAGE_fileCertificat'))
            {
                if(document.getElementById('ctl0_CONTENU_PAGE_fileCertificat').value=="")
                {
                    document.getElementById("fileError").style.display="";
                    document.getElementById("crossFile").style.display="";
                    noError=false;
                }
            }
        }
    }
    // champ obligatoire CN cas de génération de certificat
    if(document.getElementById("titre"))
    {
        browserIe=true;
        if(document.getElementById('ctl0_CONTENU_PAGE_modification').value=='0')
        {
            if(radioObj[0].checked && document.getElementById("titre").value == "") {
                document.getElementById("cnError").style.display="";
                document.getElementById("crossCn").style.display="";
                noError=false;
            } else {
                document.getElementById("cnError").style.display="none";
                document.getElementById("crossCn").style.display="none";
            }
        }
    }

    if (noError && ChoiceMade) {

        if(browserIe) {

            if(document.getElementById('certificate_create') && document.getElementById('certificate_create').checked) {
                // utilisation de support vierge (activex)
                TriggerAddCertificate();

                if(document.forms['main_form'].elements['pkcs10_req'].value=="") {
                    document.getElementById("noChoiceMadeError").style.display="none";
                    document.getElementById("erreurSaisie").style.display="none";
                    document.getElementById("noPkcs10").style.display="";
                    document.getElementById("divValidationSummary").style.display="";
                    popupResize();
                    return false;
                }
            } else if(document.getElementById('certificate_discard') && document.getElementById('certificate_discard').checked) {
                // cas certificat existant (utilisation du magasin)
                document.ExtractionBitCleApplet.ajouterRestrictionTypeCertificatChiffrement();
                document.ExtractionBitCleApplet.executer();
                return false;
            }
        }

        document.getElementById("divValidationSummary").style.display="none";
    } else {
        document.getElementById("divValidationSummary").style.display="";
        if(noError) {
            document.getElementById("erreurSaisie").style.display="none";
        } else {
            document.getElementById("erreurSaisie").style.display="";
        }
        if(ChoiceMade) {
            document.getElementById("noChoiceMadeError").style.display="none";
        } else {
            document.getElementById("noChoiceMadeError").style.display="";
        }
        popupResize();
    }

    return (noError && ChoiceMade);
}


function ValidateOptionEnvoi()
{
    radio1 = document.getElementById("ctl0_CONTENU_PAGE_courrierElectroniqueSimple");
    radio2 = document.getElementById("ctl0_CONTENU_PAGE_courrierElectroniqueContenuIntegralAR");
    radio3 = document.getElementById("ctl0_CONTENU_PAGE_courrierElectroniqueUniquementLienTelechargementObligatoire");

    if(radio1.checked || radio2.checked || radio3.checked){
        return true;
    }else{
        return false;
    }
}

function getIdsOffreElectroChecked(nbrElementsRepeater)
{
    var arrayIdsOffres = new Array();
    var j = 0
    for(i=1;i<= nbrElementsRepeater;i++)
    {
        var checkElement = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesElectronique_ctl" + i + "_check_list_reponseElec");
        if(checkElement &&  checkElement.checked){
            arrayIdsOffres[j++] = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesElectronique_ctl" + i + "_idOffre").value;
        }
    }
    return arrayIdsOffres.join('#');
}


function validateSirenForLocalCompany()
{
    if(downloadAnonymeDceValidator()) {
        return true;
    }
    var radioLocalCompany = document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_france');
    var siret = document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_siret').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_siren').value;
    if((radioLocalCompany.checked) && (siren!='' || siret!=''))
    {
        if ((siren !=0) && (siret==0)) {
            if(isSirenValide(siren)) {
                return true;
            }
            else {
                return false;
            }
        }
        if((siren !=0) && (siret!=0)) {
            if(isSiretValide(siret,siren)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else
    {
        return true;
    }
}

function DownloadPartialDCE()
{
    var i=1 ;
    var bool= false;
    while (object = document.getElementById("dce_item_"+i))
    {

        if (object.checked)
        {
            bool= true;
        }

        i++;
    }
    return bool;
}

function CheckUnCheckDCEItems(id_element, max_index_element)
{
    var thisElement = document.getElementById(id_element);
    var thisVal = thisElement.value;

    // traitement des cases filles de la case this
    if (thisVal != "")
    {
        for(j=1;j<=max_index_element;j++)
        {
            var curElement = document.getElementById("dce_item_" + j);

            var curVal = curElement.value;

            if (curVal.search(thisVal+"/")>=0)
            {
                if (thisElement.checked)
                    curElement.checked=true;
                else
                    curElement.checked=false;
            }

        }
    }

    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        if (thisVal != "")
        {
            for(j=0;j<=max_index_element;j++)
            {
                var curElement = document.getElementById("dce_item_" + j);
                var curElement2 = document.getElementById("dce_item_x_" + j);

                if(curElement2 != null) {
                    var curVal2 = curElement2.value;
                    if (curVal2)
                    {
                        if (thisVal!=curVal2 && thisVal.search(curVal2)>=0 && curVal2.charAt(curVal2.length -1) != "/") // si c un répértoire
                        {
                            curElement2.checked=false;
                        }
                    }
                }
                if(curElement != null) {
                    var curVal = curElement.value;
                    if (curVal)
                    {
                        if (thisVal!=curVal && thisVal.search(curVal)>=0 && curVal.charAt(curVal.length -1) != "/") // si c un répértoire
                        {
                            curElement.checked=false;
                        }
                    }
                }
            }
        }
    }
}

// fonction qui permet de sélectionner ou déselectionner les invités
function checkAllGuests(myInput, nbr)
{
    if(myInput.checked)
    {
        for(i=1;i<= nbr;i++)
        {
            var cuestsCheck = document.getElementById('ctl0_CONTENU_PAGE_RepeaterResultats_ctl'+i+'_inviteSelection_1');
            if(cuestsCheck != null)
                cuestsCheck.checked = true;
        }

    }else
    {
        for(i=1;i<= nbr;i++)
        {
            var cuestsCheck = document.getElementById('ctl0_CONTENU_PAGE_RepeaterResultats_ctl'+i+'_inviteSelection_1');
            if(cuestsCheck != null)
                cuestsCheck.checked = false;
        }
    }
}

function getFileChange(newFileName,oldFileIndex,oldFileBase,oldFileSize,newFileSize,pathPrefix)
{
    var doc = window.opener.document;
    var selectedFile = document.getElementById('ctl0_CONTENU_PAGE_aRemplacer');

    // Nom de l'ancien Fichier
    var labelOldDoc = doc.getElementById(pathPrefix+'oldDoc');
    if(labelOldDoc.childNodes[0]){
        labelOldDoc.removeChild(labelOldDoc.childNodes[0]);
    }
    var oldDoc =  doc.createTextNode(oldFileBase+selectedFile.options[selectedFile.selectedIndex].text+oldFileSize);
    labelOldDoc.appendChild(oldDoc);

    // Nom du nouveau Fichier
    var labelNewDoc = doc.getElementById(pathPrefix+'newDoc');
    if(labelNewDoc.childNodes[0]){
        labelNewDoc.removeChild(labelNewDoc.childNodes[0]);
    }
    var newDoc = doc.createTextNode(oldFileBase+newFileName+' ('+newFileSize+')');
    labelNewDoc.appendChild(newDoc);

    var divOld = doc.getElementById('divBlocPiecesMoved');
    divOld.style.display = 'block';
    var divNew = doc.getElementById('divLinkChangeFile');
    divNew.style.display = 'none';

    doc.getElementById(pathPrefix+'newFileName').value = newFileName;
    doc.getElementById(pathPrefix+'filePath').value = document.getElementById('ctl0_CONTENU_PAGE_filePath').value;
    doc.getElementById(pathPrefix+'indexOfFile').value = oldFileIndex ;
    doc.getElementById(pathPrefix+'remplacerPieceDce').checked = true ;
    doc.getElementById(pathPrefix+'hiddenOldFileName').value = selectedFile.options[selectedFile.selectedIndex].text ;
    //alert(doc.getElementById('ctl0_CONTENU_PAGE_blocDce_hiddenOldFileName').value);
    if(opener.document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_remplacerPieceDansDce')) {
        opener.document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_remplacerPieceDansDce').value='1';
    }
    window.close();

}
function validateNouvellePiece()
{
    var nouvellePiece = document.getElementById('ctl0_CONTENU_PAGE_nouvellePiece').value;
    if(nouvellePiece=='') {
        return false;
    }
    else {
        return true;
    }
}
function validatePiecesConsultation()
{
    var tr = document.getElementById('ctl0_CONTENU_PAGE_remplacerToutDce').value;
    len = tr.length ;
    rs = 0;
    filename ='';
    for (i = len; i > 0; i--) {
        vb = tr.substring(i,i+1);
        if (vb == "\/" && rs == 0) {
            filename = tr.substring(i+1,len);
            rs = 1 ;
        }
    }

}

function ValidateBaremeEnchere(sender, param)
{
    var ListeBareme = document.getElementById('ctl0_CONTENU_PAGE_formuleCalcul');
    if (ListeBareme.options[ListeBareme.selectedIndex].value == 0)
    {
        res = false;
        return res;
    } else {
        return true;
    }
}

function ValidateBaremeGlobal(sender, param)
{
    var ListeBareme = document.getElementById('ctl0_CONTENU_PAGE_formuleCalcul');
    if (ListeBareme.options[ListeBareme.selectedIndex].value == 0)
    {
        res = false;
        return res;
    } else {
        return true;
    }
}
function refreshRepeater()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();
    window.close();
}

function refreshRepeaterDestinataire()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl1_buttonDisplayDest').click();
    window.close();
}

function validateEnchereReglement(sender, param)
{
    if(document.getElementById("ctl0_CONTENU_PAGE_accepterReglement").checked == true) {
        return true;
    } else {
        return false;
    }
}

function displayErrorEnchere(bool)
{
    if (bool) {
        document.getElementById('divMessagesEnchere').style.display='';
    } else {
        document.getElementById('divMessagesEnchere').style.display='None';
    }

}

function hideValidationSummary()
{
    document.getElementById('divValidationSummary').style.display='None';
}

function checkDateTime()
{
    object = document.getElementById('ctl0_CONTENU_PAGE_dateRemisePlis');
    if (object != null && object.value != "")
    {
        var regExp = new RegExp("\\b[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]\\b\\ [0-2][0-9]\\:[0-6][0-9]");
        if(!(object.value.match(regExp)))
        {

            return false;
        }
        else
        {
            if(!isDate (object.value))
            {
                return false;
            }
        }
    }

    return true;
}

function validateDateTime(sender, parameter)
{
    //object = document.getElementById(parameter);
    if (parameter != null && parameter != "")
    {
        var regExp = new RegExp("\\b[0-3][0-9]\\/[0-1][0-9]\\/[0-9][0-9][0-9][0-9]\\b\\ [0-2][0-9]:[0-5][0-9](:[0-5][0-9])?$");
        if(!(parameter.match(regExp)))
        {
            return false;
        }
        else
        {
            if(!isDate (parameter))
            {
                return false;
            }
        }
    }

    return true;
}

function checkDatemiseEnLigne(sender, parameter)
{
    if (document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne').checked )
    {
        if (parameter != null && parameter != "")
        {
            return validateDateTime(sender, parameter);
        } else {
            return false;
        }

    }else {
        return true;
    }
}

//Fonction  vérifiant le format d'une date récupérée
function isDate(dtStr)
{
    var regExp = new RegExp("\\b([0-3][0-9])\\/([0-1][0-9])\\/([0-9][0-9][0-9][0-9])\\b\\ ([0-2][0-9])\\:([0-5][0-9])\\:?([0-5][0-9])?$");
    var dateArray = [];
    if(! (dateArray = dtStr.match(regExp))) { return false; }
    var strDay=dateArray[1];
    var strMonth=dateArray[2];
    var strYear=dateArray[3];
    var strHour=dateArray[4];
    var strMin=dateArray[5];
    var strSec=dateArray[6];
    var daysInMonth = DaysArray(12);

    strYr=strYear;
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);

    for (var i = 1; i <= 3; i++)
    {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }

    var month=parseInt(strMonth);
    var day=parseInt(strDay);
    var year=parseInt(strYr);
    var hour = parseInt(strHour);
    var minute = parseInt(strMin);
    var seconde = parseInt(strSec);

    if (strMonth.length<1 || month<1 || month>12)
    {
        return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month])
    {
        return false
    }
    if (strYear.length != 4 || year==0 || year<1900 || year>3000)
    {
        return false
    }
    if (strHour.length != 2 ||  hour<0 || hour>23)
    {
        return false
    }
    if (strMin.length != 2 ||  minute<0 || minute>59)
    {
        return false
    }
    if(typeof strSec != 'undefined' && strSec.length > 0 && (strSec.length != 2 ||  seconde<0 || seconde>59))
    {
        return false
    }
    return true
}

//Fonction retournant le nombre de jour pour un mois donné
function DaysArray(n)
{
    for (var i = 1; i <= n; i++)
    {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
        if (i==2) {this[i] = 29}
    }
    return this
}
//Fonction de vérification du nombre de jour pour le mois de février correspondant à l'année récupérée
function daysInFebruary (year)
{
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 ==0))) ? 29 : 28 );
}

//Fonction de vérification d'une sous-chaîne dans une chaîne
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

//Fonction de vérification si caractére entier
function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
//

function recuperationDesAdresses()
{
    var adr = opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adressesLibresHidden');
    var adrPopUp = document.getElementById('ctl0_CONTENU_PAGE_adressesLibres');
    if(adr.value){
        var adrlibre = document.createTextNode(adr.value);
        if(adrPopUp.childNodes[0])
            adrPopUp.removeChild(adrPopUp.childNodes[0]);
        adrPopUp.appendChild(adrlibre);
    }
}
function validateSiretFrance(errorMsgWrongSiret, errorMsgSiretMissing)
{
    var nomEntreprise=document.getElementById('ctl0_CONTENU_PAGE_nomEntreprise').value;
    var horodatage=document.getElementById('ctl0_CONTENU_PAGE_horodatage').value;
    var resultat = true;

    if(horodatage && nomEntreprise) {

        var france=document.getElementById('ctl0_CONTENU_PAGE_france').checked;
        var siret='';
        var siren='';

        if(document.getElementById('ctl0_CONTENU_PAGE_siret')) {
            siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
        }
        if(document.getElementById('ctl0_CONTENU_PAGE_siren')) {
            var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
        }

        if(france)
        {
            if(siren!='') {
                if(controlValidationSiret()) {

                    resultat =true;
                } else {

                    resultat = false;
                }
            } else if(siret!='') {
                resultat = false;
            } else {
                resultat = true;
            }
        }

    }
    if (resultat == true) {
        document.getElementById('divValidationSummary').style.display='none';
        document.getElementById('ctl0_CONTENU_PAGE_ctl0_validationSummary').style.display='none';
        document.getElementById('ctl0_CONTENU_PAGE_ctl0_validationSummary').innerHTML='';
        //document.getElementById('redCross').style.display='' ;
        return true;
    } else {
        document.getElementById('divValidationSummary').style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_ctl0_validationSummary').style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_ctl0_validationSummary').innerHTML='<ul><li>'+errorMsgWrongSiret+'</li></ul>';
        //document.getElementById('redCross').style.display='';
        popupResize('ctl0_CONTENU_PAGE_container');
        return false;
    }

}

function ValidateReference(sender, parameter)
{
    var code = /^[0-9a-zA-Z_.-]+$/;
    if (parameter == '' || code.test(parameter)==false || parameter.length< 5 || parameter.length > 32)
    {
        return false;
    } else {
        return true;
    }
}

function showOrHideInfosAttributaire(decisionShowCandidats,isAccordCadre,  checkBox, divNonAccordCadre, divAccordCadre)
{
    if(decisionShowCandidats=='1') {
        return true;
    }
    else {
        if(isAccordCadre=='1') {
            if(checkBox.checked) {
                document.getElementById(divAccordCadre).style.display='';
            } else {
                document.getElementById(divAccordCadre).style.display='none';
            }
        } else {
            if(checkBox.checked) {
                document.getElementById(divNonAccordCadre).style.display='';
            } else {
                document.getElementById(divNonAccordCadre).style.display='none';
            }
        }
    }
}

function selectValue(dropDownList, valueToSelect)
{
    if(document.getElementById(valueToSelect).value) {
        document.getElementById(dropDownList).selectedValue=document.getElementById(valueToSelect).value;
        bootbox.alert(document.getElementById(dropDownList).selectedValue);
    }
}


function selectedEntiteEligible(selectedEntitys, valueRadioBox)
{

    document.getElementById('errorSelectInvitePermanet').style.display="none";

    if (selectedEntitys == ''  && valueRadioBox == '2') {
        document.getElementById("errorSelectInvitePermanet").style.display="block";
    } else {
        var doc = window.opener.document;
        window.opener.returnEntiteEligible(selectedEntitys, valueRadioBox);
        window.close();
    }
}
function controlSiretCeration()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_codeEtablissement').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    if((siren !='') && (siret!='')) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function closePopUpUpdateUser()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_refreshReaterUser').click();
    window.close();
}

function controlSiretinSerach()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_panelSearch_siret').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_panelSearch_siren').value;
    if ((siren !=0) && (siret==0)) {
        if(isSirenValide(siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    if((siren ==0) && (siret==0)) {
        return true;
    }
}

function refreshRepeaterFormLibre()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_refreshRepeaterFormLibre').click();
    window.close();
}

function updateChosenFile(_fileWithPath, _idInputToUpdate)
{
    textBox = document.getElementById(_idInputToUpdate);
    textBox.value = _fileWithPath;
}

function launchAppletMpe(urlServer, uid, signature, chiffrement, messageErreur,SignFromDir,aeGenere, urlModuleValidation)
{
    if(aeGenere) {
        document.AppletActeDEngagement.verifyIfAEsGeneratedExist();
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation') && document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation').checked) {

        document.AppletMPE.fctTrt1(urlServer, uid, signature, chiffrement, SignFromDir, urlModuleValidation);

        // candidature
        index=0;
        nbrePiecesAjouter = 0;
        element = document.getElementById('ctl0_CONTENU_PAGE_pieceCandidature_nbrePiecesAjouter');
        if(element){
            nbrePiecesAjouter = element.innerHTML;
        }
        for(index=0; index < nbrePiecesAjouter; index++){
            visible = document.getElementById('ctl0_CONTENU_PAGE_pieceCandidature_listePiecesRepeater_ctl'+ index + '_ajouter');
            if(visible && visible.innerHTML == "0"){
                break;
            }
            fileCand = document.getElementById('ctl0_CONTENU_PAGE_pieceCandidature_listePiecesRepeater_ctl' + index + '_fichierPropre');
            if(fileCand.value) {
                document.AppletMPE.fctTrt2(1, 0, index+1, fileCand.value, 'PRI',"");
            }
        }


        fileAutreCand = document.getElementById('ctl0_CONTENU_PAGE_pieceCandidature_autrePieces');
        if(fileAutreCand!=null && fileAutreCand.value) {
            document.AppletMPE.fctTrt2(1, 0, index, fileAutreCand.value, 'SEC',"");
        }

        if(document.getElementById('ctl0_CONTENU_PAGE_alloti')){
            alloti = document.getElementById('ctl0_CONTENU_PAGE_alloti').value;
        }

        if( alloti == 0){
            idComp = 'ctl0_CONTENU_PAGE';
            sousPlis = 0;

            // offre technique
            index=0;
            nbrePiecesAjouter = 0;
            element = document.getElementById(idComp + '_pieceOffreTechnique_nbrePiecesAjouter');
            if(element){
                nbrePiecesAjouter = element.innerHTML;
            }
            for(index=0; index < nbrePiecesAjouter; index++){
                visible = document.getElementById(idComp + '_pieceOffreTechnique_listePiecesRepeater_ctl'+ index + '_ajouter');
                if(visible && visible.innerHTML == "0"){
                    break;
                }
                file = document.getElementById(idComp + '_pieceOffreTechnique_listePiecesRepeater_ctl' + index + '_fichierPropre');
                if(file.value) {
                    document.AppletMPE.fctTrt2(4, sousPlis, index+1, file.value, 'PRI',"");
                }
            }

            fileAutre = document.getElementById(idComp + '_pieceOffreTechnique_autrePieces');
            if(fileAutre!=null && fileAutre.value) {
                document.AppletMPE.fctTrt2(4, sousPlis, index, fileAutre.value, 'SEC',"");
            }

            // offre
            index=0;
            nbrePiecesAjouter = 0;
            element = document.getElementById(idComp + '_pieceOffre_nbrePiecesAjouter');
            if(element){
                nbrePiecesAjouter = element.innerHTML;
            }
            for(index=0; index < nbrePiecesAjouter; index++){
                visible = document.getElementById(idComp + '_pieceOffre_listePiecesRepeater_ctl'+ index + '_ajouter');
                if(visible && visible.innerHTML == "0"){
                    break;
                }

                file = document.getElementById(idComp + '_pieceOffre_listePiecesRepeater_ctl' + index + '_fichierPropre');
                if(file.value) {
                    document.AppletMPE.fctTrt2(2, sousPlis, index+1, file.value, 'PRI',"");
                }
            }

            fileAutre = document.getElementById(idComp + '_pieceOffre_autrePieces');
            if(fileAutre!=null && fileAutre.value) {
                document.AppletMPE.fctTrt2(2, sousPlis, index, fileAutre.value, 'SEC',"");
            }

            fileAe = document.getElementById(idComp + '_fichierAE');
            if(fileAe!=null && fileAe.value) {
                document.AppletMPE.fctTrt2(2, sousPlis, ++index, fileAe.value, 'ACE',"");
            }

            if(aeGenere) {
                document.AppletMPE.fctTrt5();
                fileXmlAe = document.getElementById(idComp + '_xmlAE');
                if(fileXmlAe!=null && fileXmlAe.value) {
                    document.AppletMPE.fctTrt2(2, sousPlis, ++index, fileXmlAe.value, 'ACE',"");
                }
            }

            // offre Anonymat
            index=0;
            nbrePiecesAjouter = 0;
            element = document.getElementById(idComp + '_pieceOffreAnonymat_nbrePiecesAjouter');
            if(element){
                nbrePiecesAjouter = element.innerHTML;
            }
            for(index=0; index < nbrePiecesAjouter; index++){
                visible = document.getElementById(idComp + '_pieceOffreAnonymat_listePiecesRepeater_ctl'+ index + '_ajouter');
                if(visible && visible.innerHTML == "0"){
                    break;
                }
                file = document.getElementById(idComp + '_pieceOffreAnonymat_listePiecesRepeater_ctl' + index + '_fichierPropre');
                if(file.value) {
                    document.AppletMPE.fctTrt2(3, sousPlis, index+1, file.value, 'PRI',"");
                }
            }

            fileAutre = document.getElementById(idComp + '_pieceOffreAnonymat_autrePieces');
            if(fileAutre!=null && fileAutre.value) {
                document.AppletMPE.fctTrt2(3, sousPlis, index, fileAutre.value, 'SEC',"");
            }

        }else {
            // traitement des lot
            i = 0;
            while(document.getElementById('ctl0_CONTENU_PAGE_RepeaterLot_ctl' + i + '_numLot')){
                idComp = 'ctl0_CONTENU_PAGE_RepeaterLot_ctl' + i ;
                sousPlis = document.getElementById(idComp + '_numLot').value;
                // offre technique
                index=0;
                nbrePiecesAjouter = 0;
                element = document.getElementById(idComp + '_pieceOffreTechnique_nbrePiecesAjouter');
                if(element){
                    nbrePiecesAjouter = element.innerHTML;
                }
                for(index=0; index < nbrePiecesAjouter; index++){
                    visible = document.getElementById(idComp + '_pieceOffreTechnique_listePiecesRepeater_ctl'+ index + '_ajouter');
                    if(visible && visible.innerHTML == "0"){
                        break;
                    }
                    file = document.getElementById(idComp + '_pieceOffreTechnique_listePiecesRepeater_ctl' + index + '_fichierPropre');
                    if(file.value) {
                        document.AppletMPE.fctTrt2(4, sousPlis, index+1, file.value, 'PRI',"");
                    }
                }

                fileAutre = document.getElementById(idComp + '_pieceOffreTechnique_autrePieces');
                if(fileAutre!=null && fileAutre.value) {
                    document.AppletMPE.fctTrt2(4, sousPlis, index, fileAutre.value, 'SEC',"");
                }

                // offre
                index=0;
                nbrePiecesAjouter = 0;
                element = document.getElementById(idComp + '_pieceOffre_nbrePiecesAjouter');
                if(element){
                    nbrePiecesAjouter = element.innerHTML;
                }
                for(index=0; index < nbrePiecesAjouter; index++){
                    visible = document.getElementById(idComp + '_pieceOffre_listePiecesRepeater_ctl'+ index + '_ajouter');
                    if(visible && visible.innerHTML == "0"){
                        break;
                    }

                    file = document.getElementById(idComp + '_pieceOffre_listePiecesRepeater_ctl' + index + '_fichierPropre');
                    if(file.value) {
                        document.AppletMPE.fctTrt2(2, sousPlis, index+1, file.value, 'PRI',"");
                    }
                }

                fileAutre = document.getElementById(idComp + '_pieceOffre_autrePieces');
                if(fileAutre!=null && fileAutre.value) {
                    document.AppletMPE.fctTrt2(2, sousPlis, index, fileAutre.value, 'SEC',"");
                }

                fileAe = document.getElementById(idComp + '_fichierAE');
                if(fileAe!=null && fileAe.value) {
                    document.AppletMPE.fctTrt2(2, sousPlis, ++index, fileAe.value, 'ACE',"");
                }
                if(aeGenere) {
                    document.AppletMPE.fctTrt5();
                    fileXmlAe = document.getElementById(idComp + '_xmlAE');
                    if(fileXmlAe!=null && fileXmlAe.value) {
                        document.AppletMPE.fctTrt2(2, sousPlis, ++index, fileXmlAe.value, 'ACE',"");
                    }
                }

                // offre Anonymat
                index=0;
                nbrePiecesAjouter = 0;
                element = document.getElementById(idComp + '_pieceOffreAnonymat_nbrePiecesAjouter');
                if(element){
                    nbrePiecesAjouter = element.innerHTML;
                }
                for(index=0; index < nbrePiecesAjouter; index++){
                    visible = document.getElementById(idComp + '_pieceOffreAnonymat_listePiecesRepeater_ctl'+ index + '_ajouter');
                    if(visible && visible.innerHTML == "0"){
                        break;
                    }
                    file = document.getElementById(idComp + '_pieceOffreAnonymat_listePiecesRepeater_ctl' + index + '_fichierPropre');
                    if(file.value) {
                        document.AppletMPE.fctTrt2(3, sousPlis, index+1, file.value, 'PRI',"");
                    }
                }

                fileAutre = document.getElementById(idComp + '_pieceOffreAnonymat_autrePieces');
                if(fileAutre!=null && fileAutre.value) {
                    document.AppletMPE.fctTrt2(3, sousPlis, index, fileAutre.value, 'SEC',"");
                }

                i++;
            }


        }

        index=0;
        while(certificatChiffrementCandidaure=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatCandidature_ctl'+index+'_certificat')) {
            document.AppletMPE.fctTrt3(1, 0, certificatChiffrementCandidaure.value, index);
            index ++;
        }

        index=0;
        while(certificatChiffrementOffre=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffre_ctl'+index+'_certificat')) {
            sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffre_ctl'+index+'_sousPli').value;
            document.AppletMPE.fctTrt3(2, sousPli, certificatChiffrementOffre.value, index);
            index++;
        }

        index=0;
        while(certificatChiffrementAnonymate=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatAnonymat_ctl'+index+'_certificat')) {
            sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatAnonymat_ctl'+index+'_sousPli').value;
            document.AppletMPE.fctTrt3(3, sousPli, certificatChiffrementAnonymate.value, index);
            index++;
        }

        index=0;
        while(certificatChiffrementOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffreTechnique_ctl'+index+'_certificat')) {
            sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffreTechnique_ctl'+index+'_sousPli').value;
            document.AppletMPE.fctTrt3(4, sousPli, certificatChiffrementOT.value, index);
            index++;
        }
        if(ErreurActeDEngagementNonRenseigne()!='0'){
            if(confirm(ErreurActeDEngagementNonRenseigne())){
                showLoaderWhenSendingResponse();

                document.AppletMPE.fctTrt4();
            }
        }else{
            showLoaderWhenSendingResponse();

            document.AppletMPE.fctTrt4();
        }
    } else {
        bootbox.alert(messageErreur);
    }
}

function launchAppletMpeForSignatureFromDirectory(urlServer, uid, signature, chiffrement, messageErreur,SignFromDir, urlModuleValidation)
{
    if(erreursSignatures()){
        if(document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation') && document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation').checked) {

            document.AppletMPE.fctTrt1(urlServer, uid, signature, chiffrement, SignFromDir, urlModuleValidation);/*,SignFromDir*/

            index=1;
            while(fileCand=document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_intitulePiece')) {
                if(fileCand.value) {
                    pathSign = document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_pathSignature').value;
                    if(pathSign=="")
                    {
                        pathSign="-";
                    }
                    if(index>1) { typeFile = 'SEC'; }
                    else { typeFile = 'PRI'; }
                    document.AppletMPE.fctTrt2(1, 0, index, fileCand.value, typeFile,pathSign);//applet mpe n accepte pas plsrs PRI
                }
                index++;
            }

            //Offre technique
            index=0;
            while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
                indexFile=1;
                while(fileLotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
                    if(fileLotOT.value) {
                        sousPli=lotOT.value;
                        pathSign = document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_pathSignature').value;
                        if(pathSign=="")
                        {
                            pathSign="-";
                        }
                        if(indexFile>1) { typeFile = 'SEC'; }
                        else { typeFile = 'PRI'; }
                        document.AppletMPE.fctTrt2(4, sousPli, indexFile, fileLotOT.value, typeFile,pathSign);
                    }
                    indexFile++;
                }
                index++;
            }

            //Offre
            index=0;
            while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
                sousPli=lotO.value;
                indexFile=1;
                while(fileLotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
                    if(fileLotO.value) {
                        pathSign = document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_pathSignature').value;
                        if(pathSign=="")
                        {
                            pathSign="-";
                        }
                        if(indexFile>1) { typeFile = 'SEC'; }
                        else { typeFile = 'PRI'; }
                        document.AppletMPE.fctTrt2(2, sousPli, indexFile, fileLotO.value, typeFile,pathSign);
                    }
                    indexAe=indexFile+1;
                    indexFile++;
                }
                fileAe = document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichierAE');
                if(fileAe!=null && fileAe.value) {
                    pathSign = document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_pathSignature').value;
                    if(pathSign=="")
                    {
                        pathSign="-";
                    }
                    //alert("ae "+pathSign);
                    document.AppletMPE.fctTrt2(2, sousPli, indexAe, fileAe.value, 'ACE',pathSign);
                }
                index++;
            }

            //anonymat
            index=0;
            while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
                indexFile=1;
                while(fileLotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
                    if(fileLotA.value) {
                        sousPli=lotA.value;
                        pathSign = document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_pathSignature').value;
                        if(pathSign=="")
                        {
                            pathSign="-";
                        }
                        if(indexFile>1) { typeFile = 'SEC'; }
                        else { typeFile = 'PRI'; }
                        document.AppletMPE.fctTrt2(3, sousPli, indexFile, fileLotA.value, typeFile,pathSign);
                    }
                    indexFile++;
                }
                index++;
            }

            index=0;
            while(certificatChiffrementCandidaure=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatCandidature_ctl'+index+'_certificat')) {
                document.AppletMPE.fctTrt3(1, 0, certificatChiffrementCandidaure.value, index);
                index ++;
            }

            index=0;
            while(certificatChiffrementOffre=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffre_ctl'+index+'_certificat')) {
                sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffre_ctl'+index+'_sousPli').value;
                document.AppletMPE.fctTrt3(2, sousPli, certificatChiffrementOffre.value, index);
                index++;
            }

            index=0;
            while(certificatChiffrementAnonymate=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatAnonymat_ctl'+index+'_certificat')) {
                sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatAnonymat_ctl'+index+'_sousPli').value;
                document.AppletMPE.fctTrt3(3, sousPli, certificatChiffrementAnonymate.value, index);
                index++;
            }

            index=0;
            while(certificatChiffrementOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffreTechnique_ctl'+index+'_certificat')) {
                sousPli=document.getElementById('ctl0_CONTENU_PAGE_repeaterCertificatOffreTechnique_ctl'+index+'_sousPli').value;
                document.AppletMPE.fctTrt3(4, sousPli, certificatChiffrementOT.value, index);
                index++;
            }
            if(ErreurActeDEngagementNonRenseigne()!='0'){
                if(confirm(ErreurActeDEngagementNonRenseigne())){
                    showLoaderWhenSendingResponse();
                    document.AppletMPE.fctTrt4();
                }
            }else{
                showLoaderWhenSendingResponse();
                document.AppletMPE.fctTrt4();
            }
        } else {
            alert(messageErreur);
        }
    }
}
function showLoaderWhenSendingResponse(){
    document.getElementById("sendingResponse").style.display="block";
    document.getElementById('ctl0_CONTENU_PAGE_sendResponse').style.display="none";
    document.getElementById("sendingResponse").focus();
}
function getAppletMpeReturn(xmlResult)
{
    document.getElementById('xmlString').value = xmlResult;
    document.getElementById('ctl0_CONTENU_PAGE_boutonSubmit').click();

}

function jsEnveloppedDecrypted(dirDest)
{
    if(document.getElementById('ctl0_CONTENU_PAGE_refreshReponseElectronique')){
        document.getElementById('ctl0_CONTENU_PAGE_refreshReponseElectronique').click();
    }else if(document.getElementById('ctl0_CONTENU_PAGE_refreshCandidatureElectronique')) {
        document.getElementById('ctl0_CONTENU_PAGE_refreshCandidatureElectronique').click();
    } else if(document.getElementById('ctl0_CONTENU_PAGE_refreshOffreTechniqueElectronique')) {
        document.getElementById('ctl0_CONTENU_PAGE_refreshOffreTechniqueElectronique').click();
    }else if(document.getElementById('ctl0_CONTENU_PAGE_refreshOffre')) {
        document.getElementById('ctl0_CONTENU_PAGE_refreshOffre').click();
    } else if (document.getElementById('ctl0_CONTENU_PAGE_refreshAnonymat')) {
        document.getElementById('ctl0_CONTENU_PAGE_refreshAnonymat').click();
    }
    if(dirDest!=""){
        bootbox.alert("<?php Prado::localize('MSG_DEST_PLIS_TELECHARGES') ?>" + dirDest);
    }
}

function launchAppletOuverture(_urlPageDownloadBlob,  _acronymeOrganisme, reponseAnnonceXmlEnBase64)
{
    var _destDir = document.getElementById('ctl0_CONTENU_PAGE_consultation_repDestination').value;
    if (_destDir == '') {
        bootbox.alert("<?php Prado::localize('MSG_CHOIX_DEST') ?>");
        return;
    }
    document.MpeDechiffrementApplet.initialiserMixtePhaseTelecharger(reponseAnnonceXmlEnBase64, _acronymeOrganisme, _urlPageDownloadBlob, _destDir);
    document.MpeDechiffrementApplet.executer();

}

function launchDownloadPlis(reponseAnnonceXmlEnBase64, acronymeOrganisme, urlPageDownloadBlob, repDestId)
{
    var destDir = document.getElementById('ctl0_CONTENU_PAGE_'+repDestId).value;

    document.MpeDechiffrementApplet.initialiserMixtePhaseTelecharger(reponseAnnonceXmlEnBase64, acronymeOrganisme, urlPageDownloadBlob, destDir);


}

function jsPlisDownloaded(resultat)
{
    document.getElementById("ctl0_CONTENU_PAGE_refreshDownloadPlis").click();
    bootbox.alert("<?php Prado::localize('TELECHARGEMENT_TEMINE') ?>");
}

function CheckUnCheckAll(checkBoxAll, repeaterName, elementToCheck)
{
    index=1;
    while(checkBox=document.getElementById('ctl0_CONTENU_PAGE_'+repeaterName+"_ctl"+index+"_"+elementToCheck))  {
        checkBox.checked = checkBoxAll.checked;
        index++;

    }
}

function isOneCheckedAndRepChoosen(repeaterName, element, repertoire, messageErreur, modeApplet)
{

    bool=false;
    index=0;
    while(checkBox=document.getElementById('ctl0_CONTENU_PAGE_'+repeaterName+"_ctl"+(index+1)+"_"+element)) {
        if(checkBox.checked) {
            bool=true;
        }
        index++;
    }

    if(!bool) {
        bootbox.alert(messageErreur);
    }
    if(modeApplet) {
        var destDir = document.getElementById('ctl0_CONTENU_PAGE_' + repertoire).value;
        if (destDir == '') {
            bootbox.alert("<?php Prado::localize('MSG_CHOIX_DEST') ?>");
            return false;
        }
    }

    return bool;
}

function javaTelechargerPlisCallBack(result)
{
    bootbox.alert(result);
}

function javaOuvrirEnveloppeCallBack(result)
{
    bootbox.alert(result);
}

function signerDocument(directory, errorMsg,input, urlModuleValidation)
{
    if(document.getElementById(directory) && document.getElementById(directory).value!="") {
        document.AppletSignatureXades.setParam2(document.getElementById(directory).value);
        document.AppletSignatureXades.setParam3(true);
        document.AppletSignatureXades.setParam5(urlModuleValidation);
        if(input){
            document.AppletSignatureXades.setParam4(directory);
        }
    } else {
        bootbox.alert(errorMsg);
    }

    return false;

}

function getAppletSignature(signature, nomFichierSignature)
{
    document.getElementById('ctl0_CONTENU_PAGE_signature').value=signature;
    document.getElementById('ctl0_CONTENU_PAGE_nomFichierSignature').value=nomFichierSignature;
    document.getElementById('ctl0_CONTENU_PAGE_cheminDocument').value=document.getElementById('ctl0_CONTENU_PAGE_docSignature').value;
    document.getElementById('ctl0_CONTENU_PAGE_sendSignature').click();
}

function getAppletSignatures(signature, nomFichierSignature)
{
    document.getElementById('ctl0_CONTENU_PAGE_signature').value=signature;
    document.getElementById('ctl0_CONTENU_PAGE_nomFichierSignature').value=nomFichierSignature;
    document.getElementById('ctl0_CONTENU_PAGE_cheminDocument').value=document.getElementById('ctl0_CONTENU_PAGE_listePiecesASigner_ctl1_intitulePiece').value;
    document.getElementById('ctl0_CONTENU_PAGE_sendSignature').click();
}

function verifierSignature(fichier, signature, messageErreurFichierEtSignature, messageErreurFichier, messageErreurSignature)
{
    if(document.getElementById(fichier) && document.getElementById(fichier).value!="" && document.getElementById(signature) && document.getElementById(signature).value!="") {

        document.AppletSignaturePkcs7.setParam2(document.getElementById(fichier).value);
        document.AppletSignaturePkcs7.setParam5(document.getElementById(signature).value);
        document.AppletSignaturePkcs7.setParam6(true);

    } else {
        if(document.getElementById(fichier) && document.getElementById(fichier).value=="" && document.getElementById(signature) && document.getElementById(signature).value=="") {
            bootbox.alert(messageErreurFichierEtSignature);
        } else if(document.getElementById(fichier) && document.getElementById(fichier).value=="") {
            bootbox.alert(messageErreurFichier);
        } else if (document.getElementById(signature) && document.getElementById(signature).value=="") {
            bootbox.alert(messageErreurSignature);
        }
    }

    return false;
}

function jsAppletDiagnosticCallBack(param, isValid, id, trace)
{
    if(id=="ctl0_CONTENU_PAGE_resOs") {
        document.getElementById(id).innerHTML=param;
    } else {
        if(id=="ctl0_CONTENU_PAGE_resVersionJava"){
            if (deployJava.versionCheck("1.6+") && isValid == 'true') {
                document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Version de Java OK ";
            }else{
                document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Version de Java NOK ";
            }
        }
        if(document.getElementById(id+"_succes") && document.getElementById(id+"_echec")) {
            document.getElementById(id+"_encours").style.display="none";
            if(isValid == 'true') {
                document.getElementById(id+"_succes").style.display="";
                document.getElementById(id+"_echec").style.display="none";

                document.getElementById(id+"_ok").style.display="";
                document.getElementById(id+"_nok").style.display="none";

                document.getElementById(id+"_hidden").value="1";

                //Trace Inscrit
                if(id=="ctl0_CONTENU_PAGE_resTelechargement"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Telechargement des policy files OK";
                }
                if(id=="ctl0_CONTENU_PAGE_resMagasin"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Test acces au magasin de certifs OK";
                }
                if(id=="ctl0_CONTENU_PAGE_resTestChiffrement"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Test de chiffrement OK.";
                }
                if(id=="ctl0_CONTENU_PAGE_resAppletDemaree"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Applet working ";
                }
            } else {
                document.getElementById(id+"_succes").style.display="none";
                document.getElementById(id+"_echec").style.display="";

                document.getElementById(id+"_ok").style.display="none";
                document.getElementById(id+"_nok").style.display="";

                document.getElementById(id+"_hidden").style.value="0";
                document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = trace;

                //Trace Inscrit
                if(id=="ctl0_CONTENU_PAGE_resTelechargement"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Telechargement des policy files NOK";
                }
                if(id=="ctl0_CONTENU_PAGE_resMagasin"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Test acces au magasin de certifs NOK";
                }
                if(id=="ctl0_CONTENU_PAGE_resTestChiffrement"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Test de chiffrement NOK.";
                }
                if(id=="ctl0_CONTENU_PAGE_resAppletDemaree"){
                    document.getElementById('ctl0_CONTENU_PAGE_resDiag').value = "Applet not working ";
                }

            }
        }
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_resAppletDemaree_hidden').value=="1" &&
        document.getElementById('ctl0_CONTENU_PAGE_resTelechargement_hidden').value=="1" &&
        document.getElementById('ctl0_CONTENU_PAGE_resTestChiffrement_hidden').value=="1" &&
        document.getElementById('ctl0_CONTENU_PAGE_resMagasin_hidden').value=="1" && document.getElementById('presEnvPres').style.color!='red'
    ) {
        if(document.getElementById("div_succes")) {
            document.getElementById("div_succes").style.display="";
            document.getElementById("div_echec").style.display="none";
        }
    }
    document.getElementById('ctl0_CONTENU_PAGE_boutonSubmitDiag').click();
}

function validateQuestion(sender, parameter)
{
    res = true;

    if(parameter == '' || parameter.length > 250)
    {
        res = false;
    }
    return res;
}

function parentWindowClick(idbuttonToClick)
{
    opener.document.getElementById(idbuttonToClick).click();
    return false;
}

function hideLoadingImage()
{
    imageLoading = document.getElementById("loading");
    if (imageLoading != null) {
        imageLoading.style.display='None';
    }
}

function verifierDonneeRechercheRestreinte()
{
    var bool=true;
    if(document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_orgNamesRestreinteSearch').selectedIndex==0) {
        document.getElementById('erreurMinistere').style.display='';
        document.getElementById('erreurMinistereLi').style.display='';
        bool=false;
    } else {
        document.getElementById('erreurMinistere').style.display='none';
        document.getElementById('erreurMinistereLi').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_refRestreinteSearch').value=="") {
        document.getElementById('erreurReference').style.display='';
        document.getElementById('erreurReferenceLi').style.display='';
        bool=false;
    } else {
        document.getElementById('erreurReference').style.display='none';
        document.getElementById('erreurReferenceLi').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_accesRestreinteSearch').value=="") {
        document.getElementById('erreurCode').style.display='';
        document.getElementById('erreurCodeLi').style.display='';
        bool=false;
    } else {
        document.getElementById('erreurCode').style.display='none';
        document.getElementById('erreurCodeLi').style.display='none';
    }

    if(bool) {
        document.getElementById('divValidationSummary').style.display='none';
    } else {
        document.getElementById('divValidationSummary').style.display='';
    }

    return bool;

}

function getAppletCertChiffrement(strCert)
{
    if (strCert == "") {
        return false;
    }
    if (strCert == -1) {
        bootbox.alert("<?php Prado::localize('MSG_BI_CLE_NON_VALIDE') ?>");
        return false;
    }
    document.main_form.user_certificate.value = strCert;


    if(document.forms['main_form'].elements['user_certificate'].value=="") {
        document.getElementById("noChoiceMadeError").style.display="none";
        document.getElementById("erreurSaisie").style.display="none";
        document.getElementById("noPkcs10").style.display="";
        document.getElementById("divValidationSummary").style.display="";
        popupResize();
        return false;
    }
    document.getElementById('ctl0_CONTENU_PAGE_Valider3Masque').click();

}

function maxlength(text,length)
{
    if(text.value.length>length)
        text.value=text.value.substr(0,length);
}

function differenceDates(dateFin, dateEnvoi, datePub, cible)
{
    dateFin=document.getElementById(dateFin).value;
    dateDebut=document.getElementById(dateEnvoi).value;
    if(!dateDebut) {
        dateDebut=document.getElementById(datePub).value;
    }
    if(dateFin && dateDebut) {

        var tableauFin=dateFin.split('/');
        jourFin=tableauFin[0];
        moisFin=tableauFin[1];
        anneeFin=tableauFin[2];

        var tableauDebut=dateDebut.split('/');
        jourDebut=tableauDebut[0];
        moisDebut=tableauDebut[1];
        anneeDebut=tableauDebut[2];

        if(jourFin && moisFin && anneeFin && jourDebut && moisDebut && anneeDebut) {
            var fin =new Date(anneeFin, moisFin, jourFin);
            var debut =new Date(anneeDebut, moisDebut, jourDebut);
            var one_day=1000*60*60*24
            document.getElementById(cible).innerHTML=Math.ceil((fin.getTime()-debut.getTime())/(one_day));
        }
    } else {
        document.getElementById(cible).innerHTML="-";
    }
}

function CheckUnCheckAllDomaineActivite(checkBoxAll, repeaterName, elementToCheck)
{
    index=0;
    checkbox = 'ctl0_CONTENU_PAGE_DomainesActivites_RepeaterNomsDomaines_ctl0_RepeaterNomsDomainesFils_ctl0_el_1_ssElement_1';
    while(checkBox=document.getElementById('ctl0_CONTENU_PAGE_'+repeaterName+"_ctl"+(index+1)+"_"+elementToCheck))  {
        checkBox.checked=checkBoxAll.checked;
        index++;

    }
}

function CheckUnCheckAllRepeaterImbrique(checkBoxAll, repeaterName, elementToCheck)
{
    //repeaterName = nomTemplate_repeater1_repeater2_....
    index=0;
    while(checkBox=document.getElementById(''+repeaterName+"_ctl"+(index)+"_"+elementToCheck))  {
        checkBox.checked=document.getElementById(checkBoxAll).checked;
        index++;

    }
}

function inArray(tab,val)
{
    var tableau= new Array();
    if(parseInt(tab.length)==0)
        return false;
    tableau=tab;
    var cp=0;
    for(var i=0;i < tableau.length;i++)
    {
        if(tableau[i]==val)
        {
            cp++;
        }
    }
    if(cp)
        return true;
    else
        return false;
}
/*
function isNumeric(input)
{
   return (input - 0) == input;
}
*/

/*
*
* Verifie si la variable d'entrée est numerique contenant un point ou une virgule
*/
function isNumeric(input)
{
    if((input - 0) == input) {
        return (input - 0) == input;
    } else {
        if (typeof input !== 'string') {
            input = input.toString();
        }

        var bool=/^\d+$/.test(input);
    }
}

function verifierValiditeDonneesPassation(messageErreur)
{
    flag=true;
    message=messageErreur+"<ul>";

    if(document.getElementById('ctl0_CONTENU_PAGE_nombreVariante').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_nombreVariante').value))  {
            document.getElementById('nombreVariante').style.display='';
            message+="<li> Nombre de variantes</li>";
            flag=false;
        } else {
            document.getElementById('nombreVariante').style.display='none';
        }
    }


    if(document.getElementById('ctl0_CONTENU_PAGE_dureeTotalMarche').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_dureeTotalMarche').value))  {
            document.getElementById('dureeTotalMarche').style.display='';
            message+="<li> Durée totale du marché</li>";
            flag=false;
        } else {
            document.getElementById('dureeTotalMarche').style.display='none';
        }
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_FCSPdelaiValiditeOffres').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_FCSPdelaiValiditeOffres').value)) {
            document.getElementById('delaiValidite').style.display='';
            message+="<li> Délai de validité des offres</li>";
            flag=false;
        } else {
            document.getElementById('delaiValidite').style.display='none';
        }
    }
    message+="</ul>"

    if(document.getElementById('ctl0_CONTENU_PAGE_erreur'))  {
        if(!flag) {
            document.getElementById('ctl0_CONTENU_PAGE_erreur').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_labelMessage').innerHTML=message;
        } else {
            document.getElementById('ctl0_CONTENU_PAGE_erreur').style.display='none';
        }

    }

    return flag;
}


function verifierAjoutAvenant(messageErreur)
{
    flag=true;
    message=messageErreur+"<ul>";

    if(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantHt').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantHt').value))  {
            if(!isDecimal(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantHt').value))  {
                document.getElementById('montantAvenantHt').style.display='';
                message+="<li><?php Prado::localize('TEXT_MONTANT_AVENANT_HT') ?></li>";
                flag=false;
            } else {
                document.getElementById('montantAvenantHt').style.display='none';
            }
        } else {
            document.getElementById('montantAvenantHt').style.display='none';
        }
    } else {
        document.getElementById('montantAvenantHt').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantTtc').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantTtc').value))  {
            if(!isDecimal(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantTtc').value))  {
                document.getElementById('montantAvenantTtc').style.display='';
                message+="<li> <?php Prado::localize('TEXT_MONTANT_AVENANT_TTC') ?></li>";
                flag=false;
            } else {
                document.getElementById('montantAvenantTtc').style.display='none';
            }
        } else {
            document.getElementById('montantAvenantTtc').style.display='none';
        }
    } else {
        document.getElementById('montantAvenantTtc').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationMarcheInitial').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationMarcheInitial').value))  {
            if(!isDecimal(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationMarcheInitial').value))  {
                document.getElementById('pourcentAugmentationMarcheInitial').style.display='';
                message+="<li> <?php Prado::localize('TEXT_POURCENTAGE_AUGMENTATION_MARCHE_INITIAL') ?></li>";
                flag=false;
            } else {
                document.getElementById('pourcentAugmentationMarcheInitial').style.display='none';
            }
        } else {
            document.getElementById('pourcentAugmentationMarcheInitial').style.display='none';
        }
    } else {
        document.getElementById('pourcentAugmentationMarcheInitial').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationCumul').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationCumul').value))  {
            if(!isDecimal(document.getElementById('ctl0_CONTENU_PAGE_pourcentAugmentationCumul').value))  {
                document.getElementById('pourcentAugmentationCumul').style.display='';
                message+="<li> <?php Prado::localize('TEXT_POURCENTAGE_AUGMENTATION_CUMULE') ?></li>";
                flag=false;
            } else {
                document.getElementById('pourcentAugmentationCumul').style.display='none';
            }
        } else {
            document.getElementById('pourcentAugmentationCumul').style.display='none';
        }
    } else {
        document.getElementById('pourcentAugmentationCumul').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantCumulHt').value) {
        if(!isNumeric(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantCumulHt').value))  {
            if(!isDecimal(document.getElementById('ctl0_CONTENU_PAGE_montantAvenantCumulHt').value))  {
                document.getElementById('montantAvenantCumulHt').style.display='';
                message+="<li> <?php Prado::localize('TEXT_MONTANT_TOTAL_MARCHE_TOUS_AVENANTS_CUMULES') ?></li>";
                flag=false;
            } else {
                document.getElementById('montantAvenantCumulHt').style.display='none';
            }
        } else {
            document.getElementById('montantAvenantCumulHt').style.display='none';
        }
    } else {
        document.getElementById('montantAvenantCumulHt').style.display='none';
    }

    //Vérification des dates
    if(document.getElementById('ctl0_CONTENU_PAGE_dateReceptionProjet').value) {
        if(!checkDate('dateReceptionProjet'))  {
            document.getElementById('dateReceptionProjet').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_RECEPTION_PROJET_PRESENTATION_AVENANT_SECRETARIAT_CAO') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateReceptionProjet').style.display='none';
        }
    } else {
        document.getElementById('dateReceptionProjet').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateReceptionRapportAvenantSV').value) {
        if(!checkDate('dateReceptionRapportAvenantSV'))  {
            document.getElementById('dateReceptionRapportAvenantSV').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_RECEPTION_PROJET_PRESENTATION_AVENANT_CHARG_ETUDES_SERVICE_VALIDATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateReceptionRapportAvenantSV').style.display='none';
        }
    } else {
        document.getElementById('dateReceptionRapportAvenantSV').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateObservationsSVProjetRapportAvenant').value) {
        if(!checkDate('dateObservationsSVProjetRapportAvenant'))  {
            document.getElementById('dateObservationsSVProjetRapportAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_FORMULATIONS_PREMIERES_OBSERVATIONS_SERVICE_VALIDATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateObservationsSVProjetRapportAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateObservationsSVProjetRapportAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateRetourProjetRapportAvenant').value) {
        if(!checkDate('dateRetourProjetRapportAvenant'))  {
            document.getElementById('dateRetourProjetRapportAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_RETOUR_PROJET_FINALISE') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateRetourProjetRapportAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateRetourProjetRapportAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_validationProjetRapportAvenant').value) {
        if(!checkDate('validationProjetRapportAvenant'))  {
            document.getElementById('validationProjetRapportAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_VALIDATION_PROJET_RAPPORT_SERVICE_VALIDATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('validationProjetRapportAvenant').style.display='none';
        }
    } else {
        document.getElementById('validationProjetRapportAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateCao').value) {
        if(!checkDate('dateCao'))  {
            document.getElementById('dateCao').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_CAO') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateCao').style.display='none';
        }
    } else {
        document.getElementById('dateCao').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateCp').value) {
        if(!checkDate('dateCp'))  {
            document.getElementById('dateCp').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_CP') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateCp').style.display='none';
        }
    } else {
        document.getElementById('dateCp').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateSignatureAvenantParPA').value) {
        if(!checkDate('dateSignatureAvenantParPA'))  {
            document.getElementById('dateSignatureAvenantParPA').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_SIGNATURE_AVENANT_POUVOIR_ADJUDICATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateSignatureAvenantParPA').style.display='none';
        }
    } else {
        document.getElementById('dateSignatureAvenantParPA').style.display='none';
    }


    if(document.getElementById('ctl0_CONTENU_PAGE_dateReceptionDossierCLParSV').value) {
        if(!checkDate('dateReceptionDossierCLParSV'))  {
            document.getElementById('dateReceptionDossierCLParSV').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_RECEPTION_DOSSIER_CONTROLE_LEGALITE_SERVICE_VALIDATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateReceptionDossierCLParSV').style.display='none';
        }
    } else {
        document.getElementById('dateReceptionDossierCLParSV').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateObservationsSVDossierAvenant').value) {
        if(!checkDate('dateObservationsSVDossierAvenant'))  {
            document.getElementById('dateObservationsSVDossierAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_FORMULATION_SERVICE_VALIDATEUR') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateObservationsSVDossierAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateObservationsSVDossierAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateRetourDossierFinalAvenant').value) {
        if(!checkDate('dateRetourDossierFinalAvenant'))  {
            document.getElementById('dateRetourDossierFinalAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_RETOUR_DOSSIER_FINALISE') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateRetourDossierFinalAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateRetourDossierFinalAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateValidationDossierFinaliseAvenant').value) {
        if(!checkDate('dateValidationDossierFinaliseAvenant'))  {
            document.getElementById('dateValidationDossierFinaliseAvenant').style.display='';
            message+="<li> <?php Prado::localize('DATE_VALIDATION_DOSSIER_FINALISE_SERVICE') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateValidationDossierFinaliseAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateValidationDossierFinaliseAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateTransmissionPrefectureAvenant').value) {
        if(!checkDate('dateTransmissionPrefectureAvenant'))  {
            document.getElementById('dateTransmissionPrefectureAvenant').style.display='';
            message+="<li> <?php Prado::localize('TEXT_DATE_TRANSMISSION_PREFECTURE') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateTransmissionPrefectureAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateTransmissionPrefectureAvenant').style.display='none';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_dateNotificationAvenant').value) {
        if(!checkDate('dateNotificationAvenant'))  {
            document.getElementById('dateNotificationAvenant').style.display='';
            message+="<li> <?php Prado::localize('DATE_NOTIFICATION') ?></li>";
            flag=false;
        } else {
            document.getElementById('dateNotificationAvenant').style.display='none';
        }
    } else {
        document.getElementById('dateNotificationAvenant').style.display='none';
    }

    message+="</ul>"

    if(document.getElementById('ctl0_CONTENU_PAGE_erreur'))  {
        if(!flag) {
            document.getElementById('ctl0_CONTENU_PAGE_erreur').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_labelMessage').innerHTML=message;
        } else {
            document.getElementById('ctl0_CONTENU_PAGE_erreur').style.display='none';
        }

    }

    return flag;
}

function checkDate(input)
{
    dateStr=document.getElementById('ctl0_CONTENU_PAGE_'+input).value;

    var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
    var matchArray = dateStr.match(datePat); // is the format ok?

    if (matchArray == null) {
        return false;
    }
    return true;
}
function checkAllJournaux(itemC){
    var i=0;
    var bool;
    var checkBoxAll = document.getElementById('ctl0_CONTENU_PAGE_listeComptesCentralePub_ctl'+itemC+'_destinataire');
    if(checkBoxAll.checked)
    {
        bool = true;
    }
    else
    {
        bool = false;
    }
    //alert(document.getElementById('ctl0_CONTENU_PAGE_listeComptesCentralePub_ctl'+item+'_listeJournauxCentralePub_ctl'+item+'_journal'));return;
    while(a = document.getElementById('ctl0_CONTENU_PAGE_listeComptesCentralePub_ctl'+itemC+'_listeJournauxCentralePub_ctl'+i+'_journal') )
    {
        a.checked=bool;
        i++;
    }

}

function checkOnlyOne(repeater,champ,element)
{
    var i=0;
    var monRadio ;
    //alert(document.getElementById(element));return;
    while(monRadio=document.getElementById('ctl0_CONTENU_PAGE_'+repeater+'_ctl'+i+'_'+champ))
    {
        if(monRadio.checked) {
            monRadio.checked=false;
        }
        i++;
    }
    document.getElementById(element).checked=true;
}
function hidePictoEnvoyerMsg()
{
    document.getElementById('panelPictoEnvoiMsg').style.display = 'none';

}

function dateBandeau(value, offset)
{
    date=new Date();
    date.setTime( (value/1000 + date.getTimezoneOffset()*60 + new Number(offset)) * 1000);

    var jours=Array("<?php Prado::localize('TEXT_DAY7') ?>", "<?php Prado::localize('TEXT_DAY1') ?>", "<?php Prado::localize('TEXT_DAY2') ?>", "<?php Prado::localize('TEXT_DAY3') ?>", "<?php Prado::localize('TEXT_DAY4') ?>", "<?php Prado::localize('TEXT_DAY5') ?>", "<?php Prado::localize('TEXT_DAY6') ?>");
    var mois=Array("<?php Prado::localize('JS_MONTH1') ?>", "<?php Prado::localize('JS_MONTH2') ?>", "<?php Prado::localize('JS_MONTH3') ?>","<?php Prado::localize('JS_MONTH4') ?>","<?php Prado::localize('JS_MONTH5') ?>","<?php Prado::localize('JS_MONTH6') ?>","<?php Prado::localize('JS_MONTH7') ?>","<?php Prado::localize('JS_MONTH8') ?>","<?php Prado::localize('JS_MONTH9') ?>", "<?php Prado::localize('JS_MONTH10') ?>", "<?php Prado::localize('JS_MONTH11') ?>", "<?php Prado::localize('JS_MONTH12') ?>");

    nomJour=jours[date.getDay()];
    jour=date.getDate();
    nomMois=mois[date.getMonth()];
    annee=date.getFullYear();
    heures=date.getHours();
    minute=date.getMinutes();
    seconde=date.getSeconds();

    if(seconde<=9) {
        seconde="0"+seconde;
    }
    if(minute<=9) {
        minute="0"+minute;
    }
    if(heures<=9) {
        heures="0"+heures;
    }

    if(document.getElementById("ctl0_bandeauAgent_dateAujourdhui")) {
        document.getElementById("ctl0_bandeauAgent_dateAujourdhui").innerHTML=nomJour+" "+jour+" "+nomMois+" "+annee+" "+heures+":"+minute;
    } else if(document.getElementById("ctl0_bandeauEntreprise_dateAujourdhui")){
        document.getElementById("ctl0_bandeauEntreprise_dateAujourdhui").innerHTML=nomJour+" "+jour+" "+nomMois+" "+annee+" "+heures+":"+minute;
    }
}

function showDivIfChecked(myDiv,myCheckBox)
{
    var checkElement = document.getElementById(myCheckBox);
    if(checkElement.checked == true) {
        showDiv(myDiv);
    }
    else {
        hideDiv(myDiv);
    }
}

function chorusValidationSiret()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    if (siren =='' || siret =='') {
        return false;
    }
    if ((siren !='') || (siret !='')) {
        return controlValidationSiret();
    }
    else {
        return true;
    }
}

function showDivIfChecked(myDiv,myCheckBox)
{
    var checkElement = document.getElementById(myCheckBox);
    if(checkElement.checked == true) {
        showDiv(myDiv);
    }
    else {
        hideDiv(myDiv);
    }
}

function ValidateCategorieGA()
{
    var EntitePublic = document.getElementById('ctl0_CONTENU_PAGE_ga');
    if (EntitePublic.options[EntitePublic.selectedIndex].value == 0)
    {
        document.getElementById('spanGA').style.display='';
        return false;
    } else {
        document.getElementById('spanGA').style.display='none';
        return true;
    }
}

function ValidateTypeMarche()
{
    var EntitePublic = document.getElementById('ctl0_CONTENU_PAGE_marcheType');
    if (EntitePublic.options[EntitePublic.selectedIndex].value == 0)
    {
        document.getElementById('spanTypeMarche').style.display='';
        return false;
    } else {
        document.getElementById('spanTypeMarche').style.display='none';
        return true;
    }
}
function validateSirenForService()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    if(siren!='' || siret!='')
    {
        if((siren !=0) && (siret!=0)) {
            if(isSiretValide(siret,siren)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else
    {
        return true;
    }
}

function validateHeureDeRemiseDePlisParDefaut(){
    if(document.getElementById('ctl0_CONTENU_PAGE_heureRemisDesPlisParDefaut').value != '' && !/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(document.getElementById('ctl0_CONTENU_PAGE_heureRemisDesPlisParDefaut').value)){
        return false;
    } else{
        return true;
    }

}

function validateSirenForOrganisme()
{
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    if(siren!='' && siret!='')
    {
        if((siren !=0) && (siret!=0)) {
            if(isSiretValide(siret,siren)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else
    {
        return false;
    }
}
function ValidateCategorieOA()
{
    var EntitePublic = document.getElementById('ctl0_CONTENU_PAGE_oa');
    if (EntitePublic.options[EntitePublic.selectedIndex].value == 0)
    {
        document.getElementById('spanOA').style.display='';
        return false;
    } else {
        document.getElementById('spanOA').style.display='none';
        return true;
    }
}
function zipInvalidetoChorus(type,bool)
{
    if(type=='folder' && bool == '1') {
        bootbox.alert("Fichier non transmissible car le format du fichier n'est pas traité par Chorus. La liste des extensions extensions autorisées est la suivante: PDF,DOC,XLS,TXT,RTF,ODT,XML,P7S,PFX");
        return false;
    }
}

function CheckUnCheckZIPItems(id_element, max_index_element,itemIndex)
{
    var thisElement = document.getElementById(id_element);
    var thisVal = thisElement.value;

    // traitement des cases filles de la case this
    if (thisVal != "")
    {
        for(j=0;j<=max_index_element;j++)
        {
            var curElement = document.getElementById("zip_item_"+itemIndex+"_zip_item_" + j);

            var curVal = curElement.value;

            if (curVal.search(thisVal+"/")>=0)
            {
                if (thisElement.checked)
                    curElement.checked=true;
                else
                    curElement.checked=false;
            }

        }
    }

    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        if (thisVal != "")
        {
            for(j=1;j<=max_index_element;j++)
            {
                var curElement = document.getElementById("zip_item_"+itemIndex+"_zip_item_" + j);
                var curVal = curElement.value;

                if (curVal)
                {
                    if (thisVal!=curVal && thisVal.search(curVal)>=0 && curVal.charAt(curVal.length -1) != "/") // si c un répértoire
                    {
                        curElement.checked=false;
                    }
                }

            }
        }
    }
}

function CheckUnCheckEnveloppeItems(id_element, max_index_element)
{
    var thisElement = document.getElementById(id_element);
    var thisVal = thisElement.value;

    // traitement des cases filles de la case this
    if (thisVal != "")
    {
        for(j=1;j<=max_index_element;j++)
        {
            var curElement = document.getElementById("env_item_" + j);

            var curVal = curElement.value;

            if (curVal.search(thisVal+"/")>=0)
            {
                if (thisElement.checked)
                    curElement.checked=true;
                else
                    curElement.checked=false;
            }

        }
    }

    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        if (thisVal != "")
        {
            for(j=1;j<=max_index_element;j++)
            {
                var curElement = document.getElementById("env_item_" + j);
                var curVal = curElement.value;

                if (curVal)
                {
                    if (thisVal!=curVal && thisVal.search(curVal)>=0 && curVal.charAt(curVal.length -1) != "/") // si c un répértoire
                    {
                        curElement.checked=false;
                    }
                }

            }
        }
    }
}

function navigateurInformations()
{
    navigatorName=navigator.userAgent;
    //document.write(navigatorName);
    if (navigatorName.search("MSIE") != -1) {
        navigatorInfos=navigatorName.split("MSIE");
        infos=navigatorInfos[1].split(";");
        version=infos[0];
        return ("IE "+version);
    } else if(navigatorName.search("Chrome") != -1) {
        navigatorInfos=navigatorName.split("Chrome");
        infos=navigatorInfos[1].split(" ");
        version=infos[0].replace("/","");
        return("Google Chrome "+version);
    } else if(navigatorName.search("Safari") != -1) {
        navigatorInfos=navigatorName.split("Version");
        infos=navigatorInfos[1].split(" ");
        version=infos[0].replace("/","");
        return ("Safari "+version);
    } else if(navigatorName.search("Opera") != -1) {
        navigatorInfos=navigatorName.split("Opera");
        infos=navigatorInfos[1].split(" ");
        version=infos[0].replace("/","");
        return ("Opera "+version);
    }else if(navigatorName.search("Mozilla") != -1) {
        navigatorInfos=navigatorName.split("Firefox");
        infos=navigatorInfos[1].split(";");
        uncleanVersion=infos[0].replace("/","");
        cleanVersion=uncleanVersion.split(" ");
        version=cleanVersion[0];
        return ("Firefox "+version);
    } else {
        return (navigatorName);
    }
}

function fichierAccessibleAgentsObligatoire()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_fichierAccessibleOui").checked == false && document.getElementById("ctl0_CONTENU_PAGE_fichierAccessibleNon").checked == false){
        return false;
    }else{
        return true;
    }
}
function isDocumentAttache()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_docFile").value){
        return false;
    }else{
        return true;
    }
}
function cocherDecocherTousConsultationDocCfe(repeaterName, case_a_cocher)
{
    case_a_cocher_All = document.getElementById('ctl0_CONTENU_PAGE_repeaterDocumentCoffreFort_ctl0_case_a_cocher_All');
    index=0;
    while(checkBox=document.getElementById('ctl0_CONTENU_PAGE_'+repeaterName+"_ctl"+(index+1)+"_"+case_a_cocher))   {
        checkBox.checked=case_a_cocher_All.checked;
        index++;

    }
}
function returnAgrements(idsAgrements,clientId)
{
    var doc = window.opener.document;
    doc.getElementById(clientId+'_idsSelectedAgrements').value = idsAgrements;
    doc.getElementById(clientId+'_selectedAgrements').click();
    // doc.getElementById('ctl0_CONTENU_PAGE_buttonRefresh').click();
    window.close();
}
function returnSelectedQualif(idsQualification,libelleQualif,clientId)
{

    var doc = window.opener.document;
    doc.getElementById(clientId+'_idsQualification').value = idsQualification;
    doc.getElementById(clientId+'_libelleQualif').value = libelleQualif;
    doc.getElementById(clientId+'_displayQualif').click();
    // doc.getElementById('ctl0_CONTENU_PAGE_buttonRefresh').click();
    window.close();
}

function validateRegion()
{
    var liste = document.getElementById('ctl0_CONTENU_PAGE_panelRegionProvince_regionSiegeSocial');
    if (liste.options[liste.selectedIndex].value == '-1')
    {
        document.getElementById('spanRegion').style.display='';
        return false;
    } else {
        document.getElementById('spanRegion').style.display='none';
        return true;
    }
}
function validateProvince()
{
    var liste = document.getElementById('ctl0_CONTENU_PAGE_panelRegionProvince_regionSiegeSocial');
    if (liste.options[liste.selectedIndex].value != '-1') {
        var liste = document.getElementById('ctl0_CONTENU_PAGE_panelRegionProvince_provinceSiegeSocial');
        if (liste.options[liste.selectedIndex].value == '-1' )
        {
            document.getElementById('spanProvince').style.display='';
            return false;
        } else {
            document.getElementById('spanProvince').style.display='none';
            return true;
        }
    }else {
        return true;
    }
}

function onSousCatClicked(sousCatId,sousCatImgId,myPanel)
{
    var layer = document.getElementById(myPanel);
    var sousCatImg = document.getElementById(sousCatImgId);
    var sousCatCheckBox = document.getElementById(sousCatId);
    if (sousCatCheckBox.checked==false) {
        layer.style.display = 'block';
        sousCatImg.src = 'themes/images/picto-moins.gif';
    }
    else {
        layer.style.display = 'none';
        sousCatImg.src = 'themes/images/picto-plus.gif';
    }
    initFunctions();
}

function returnSelectedCate(idsSoutCat,clientId)
{
    var doc = window.opener.document;
    doc.getElementById(clientId+'_idsDomaines').value = idsSoutCat;
    doc.getElementById(clientId+'_displayDomaine').click();
    //doc.getElementById('ctl0_CONTENU_PAGE_buttonRefresh').click();
    window.close();
}

function validateDomainesActivites()
{
    var idsDomaines = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_domaineActivite_idsDomaines');
    if (idsDomaines.value == '')
    {
        document.getElementById('spanDomainesObligatoire').style.display='';
        if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons')) {
            document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
        }
        return false;
    } else {
        //document.getElementById('spanDomainesObligatoire').style.display='none';
        return true;
    }
}

function checkDateAnnulation()
{
    object = document.getElementById('ctl0_CONTENU_PAGE_dateAnnulation');
    if (object != null && object.value != "")
    {
        var regExp = new RegExp("\\b[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]\\b\\ [0-2][0-9]\\:[0-6][0-9]");
        if(!(object.value.match(regExp)))
        {

            return false;
        }
        else
        {
            if(!isDate (object.value))
            {
                return false;
            }
        }
    }

    return true;
}


function launchAppletForAnnulation(messageErreur, stringToSign)
{
    if(document.getElementById('accepterConditionsUtilisation') && document.getElementById('accepterConditionsUtilisation').checked) {
        document.SignaturePkcs7Applet.ajouterHash('contenuSignature1Base64', stringToSign);
        document.SignaturePkcs7Applet.executer();
    } else {
        bootbox.alert(messageErreur);
    }

}

/**
 * Renvoie le resultat de la signature pades effectuée.
 *
 * @param identifiant l'identifiant final du champs html auquel rattaché le résultat
 * @param hashFichierSigneBase64 le chemin vers le fichier pdf signé au format Pades
 */
function renvoiResultatSignaturePkcs7Applet(identifiant, hashFichierSigneBase64) {
    document.getElementById("signature").value=hashFichierSigneBase64;
    document.getElementById("ctl0_CONTENU_PAGE_signatureRecieved").click();
}

function getAppletSignatureAndId(signature, id)
{
    document.getElementById("signature").value=signature;
    document.getElementById("ctl0_CONTENU_PAGE_signatureRecieved").click();
}

function validerSiretHideButton(errorMsgWrongSiret, errorMsgSiretMissing) {
    if(validateSiretFrance(errorMsgWrongSiret, errorMsgSiretMissing) == true) {
        document.getElementById('ctl0_CONTENU_PAGE_validerAjoutRetraitPapier').style.display='None';
        return true;
    } else {
        document.getElementById('ctl0_CONTENU_PAGE_validerAjoutRetraitPapier').style.display='block';
        return false;
    }
}
/***
 *
 * Verifier la selection de la liste des categorie dans la page de gestion des organismes
 */
function controlValidationListCategorieJuridique()
{
    var listCategorieJuridique = document.getElementById('ctl0_CONTENU_PAGE_categorie');
    if(listCategorieJuridique.selectedIndex == 0 )
    {
        return false;
    }
    else {
        return true;
    }

}
/***
 *
 * Verifier la selection de la liste des articles dans la page de gestion des organismes
 */
function controlValidationListArticle()
{
    var listCategorieJuridique = document.getElementById('ctl0_CONTENU_PAGE_article');
    if(listCategorieJuridique.selectedIndex == 0 )
    {
        return false;
    }
    else {
        return true;
    }

}
function refreshRepeaterListeMarches()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_tableauListeMarches_refreshRepeater').click();
    window.close();
}

function controlValidationNumRc(sender,parameter)
{

    var villeRc = document.getElementById(parameter+'Ville');
    var numeroRc = document.getElementById(parameter+'Numero');
    if((numeroRc.value=="" && villeRc.selectedIndex != 0) ||(numeroRc.value!="" && villeRc.selectedIndex == 0))
    {
        return false;
    }
    else {
        return true;
    }

}

function CheckUnCheckHeliosEnvItems(id_element, max_index_element,lot,idBlob)
{
    var thisElement = document.getElementById(id_element);
    var thisVal = thisElement.value;

    // traitement des cases filles de la case this
    if (thisVal != "")
    {
        for(j=1;j<=max_index_element;j++)
        {
            var curElement = document.getElementById("lot_"+lot+"_env_item_"+idBlob+"_" + j);

            var curVal = curElement.value;

            if (curVal.search(thisVal+"/")>=0)
            {
                if (thisElement.checked)
                    curElement.checked=true;
                else
                    curElement.checked=false;
            }

        }
    }

    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        if (thisVal != "")
        {
            for(j=0;j<=max_index_element;j++)
            {
                var curElement = document.getElementById("lot_"+lot+"_env_item_"+idBlob+"_" + j);
                var curElement2 = document.getElementById("lot_"+lot+"_env_item_"+idBlob+"_" + j);

                if(curElement2 != null) {
                    var curVal2 = curElement2.value;
                    if (curVal2)
                    {
                        if (thisVal!=curVal2 && thisVal.search(curVal2)>=0 && curVal2.charAt(curVal2.length -1) != "/") // si c un répértoire
                        {
                            curElement2.checked=false;
                        }
                    }
                }
                if(curElement != null) {
                    var curVal = curElement.value;
                    if (curVal)
                    {
                        if (thisVal!=curVal && thisVal.search(curVal)>=0 && curVal.charAt(curVal.length -1) != "/") // si c un répértoire
                        {
                            curElement.checked=false;
                        }
                    }
                }
            }
        }
    }
}
function jsSignatureDone(signaturePath)
{
    hideProcessingBlock();
    var chaine = signaturePath.split('*');
    if(chaine.length<=1){
        var index = signaturePath.lastIndexOf("\\");
        if(index<0) {
            index = signaturePath.lastIndexOf("/");
        }
        bootbox.alert("<?php Prado::localize('SIGNATURE_GENEREE_DANS') ?>" + " : \n\n  -  <?php Prado::localize('TEXT_NOM_DU_JETON') ?> : \n"+signaturePath.substring(index+1,signaturePath.length)+"\n\n  -  <?php Prado::localize('TEXT_JETON_PLACE_DANS_LE_REPERTOIRE') ?> : \n"+ signaturePath.substring(0,index));
    }else{
        addFilesSignature(chaine[0]);
        document.AppletSignaturesInfos.setListeFoundSignatureForChoosenFiles(document.getElementById('ctl0_CONTENU_PAGE_listeFoundSignatureForChoosenFiles').value);
        getSignatures();
        bootbox.alert("<?php Prado::localize('SIGNATURE_GENEREE_DANS') ?>" + chaine[2]);
    }
}


function downloadAnonymeDceValidator()
{
    var inscriptionAuthentification = document.getElementById("ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_inscriptionAuthentification");
    var choixTelechargement = document.getElementById("ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixTelechargement");
    var choixAnonyme = document.getElementById("ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixAnonyme");
    if(choixTelechargement.checked == true)
    {
        return false;
    }
    else if(choixAnonyme.checked == true || inscriptionAuthentification.checked == true) {
        return true;
    }
}

function accepterConditionsValidator(sender,parameter)
{
    var choixAnonyme = document.getElementById("ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixAnonyme");
    var choixAuthentification = document.getElementById("ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_inscriptionAuthentification");
    if(choixAnonyme.checked == true || choixAuthentification.checked == true) {
        return true;
    }

    var accepterConditions = document.getElementById(parameter+"_accepterConditions");
    if(accepterConditions.checked == true) {
        return true;
    }
    else {
        return false;
    }
}

function nomValidatorDownloadDce(sender,parameter)
{
    if(downloadAnonymeDceValidator()) {
        return true;
    }
    else {
        var nom = document.getElementById(parameter+"_nom");
        if(nom.value !='') {
            return true;
        }
        else {
            return false;
        }
    }
}

function prenomValidatorDownloadDce(sender,parameter)
{
    if(downloadAnonymeDceValidator()) {
        return true;
    }
    else {
        var prenom = document.getElementById(parameter+"_prenom");
        if(prenom.value !='') {
            return true;
        }
        else {
            return false;
        }
    }
}

function emailValidatorDownloadDce(sender,parameter)
{
    if(downloadAnonymeDceValidator()) {
        return true;
    }
    else {
        var email = document.getElementById(parameter+"_email");
        if(email.value !='') {
            return validatorEmailFormulaireDemande(email);
        }
        else {
            return false;
        }
    }
}

function validationInscriptionUser()
{
    cp=document.getElementById('ctl0_CONTENU_PAGE_cpSiegeSocial').value;
    var reg = new RegExp('^[a-zA-Z0-9]+$');
    if(cp!='') {
        if(reg.test(cp)==false) {

        }
    }
}

function ValidateDatesExercice()
{
    dateDebut=document.getElementById('ctl0_CONTENU_PAGE_debutExercice').value;
    dateFin=document.getElementById('ctl0_CONTENU_PAGE_finExercice').value;

    if(dateDebut!="" && dateFin!="") {
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }

    return true;
}

function ValidateDatesExecution()
{
    dateDebut=document.getElementById('ctl0_CONTENU_PAGE_dateDebutExecution').value;
    dateFin=document.getElementById('ctl0_CONTENU_PAGE_dateFinExecution').value;

    if(dateDebut!="" && dateFin!="") {
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }

    return true;
}

function controlValidationVilleRc()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_france')) {
        var france=document.getElementById('ctl0_CONTENU_PAGE_france').checked;
        if(france==true) {
            var villeRc=document.getElementById('ctl0_CONTENU_PAGE_RcVille');
            villeRcChoisi = villeRc.options[villeRc.selectedIndex].value;
            if(villeRcChoisi=="0") {
                return false;
            }
        }
    }

    return true;
}

function controlValidationPays()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etranger')) {
        var etranger=document.getElementById('ctl0_CONTENU_PAGE_etranger').checked;
        if(etranger==true) {
            var pays=document.getElementById('ctl0_CONTENU_PAGE_pays');
            paysChoisi = pays.options[pays.selectedIndex].value;
            if(paysChoisi=="0") {
                return false;
            }
        }
    }

    return true;
}

function controlIdNational()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etranger')) {
        var etranger=document.getElementById('ctl0_CONTENU_PAGE_etranger').checked;
        if(etranger==true) {
            var pays=document.getElementById('ctl0_CONTENU_PAGE_pays');
            paysChoisi = pays.options[pays.selectedIndex].value;
            if(paysChoisi!="0") {
                if(document.getElementById('ctl0_CONTENU_PAGE_idNational')) {
                    if(document.getElementById('ctl0_CONTENU_PAGE_idNational').value=="") {
                        return false;
                    }
                }
            }
        }
    }

    return true;
}
/*
 * Verifie si le nombre passé en parametre est decimal contenant une virgule
 * @return: true si le nombre est decimal, false sinon
 * @param: nombre
 */
function isDecimal(input) {
    if (typeof input !== 'string') {
        input = input.toString();
    }

    var bool=/^\-?\d{1,9}\,\d+$/.test(input);
    if(!bool) {
        window.scrollTo(0,0);
    }
    return bool;

}
function gestionEntrepriseParAgentValidationRc(sender,parameter)
{
    if(document.getElementById('ctl0_CONTENU_PAGE_maroc').checked) {
        var villeRc = document.getElementById(parameter+'Ville');
        var numeroRc = document.getElementById(parameter+'Numero');
        if((numeroRc.value=="" || villeRc.selectedIndex == 0) ) {
            return false;
        }
        else {
            return true;
        }
    }
    return true;
}

function returnLieuxVisites(infoVisite,clientId,addVisite)
{
    var doc = window.opener.document;
    doc.getElementById(clientId+'_addVisite').value = addVisite;
    doc.getElementById(clientId+'_infoVisite').value = infoVisite;
    doc.getElementById(clientId+'_infoVisiteButton').click();
    window.close();
}

// Début fonctions validateurs des données du formulaire de création des sociétés exclues

function validerDocAttacheSocieteExclues()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_document_attacher").value){
        return false;
    }else{
        return true;
    }
}
function validerIdentifiantEntrepriseSocieteExclues()
{
    if(!validerVilleRc() || !document.getElementById("ctl0_CONTENU_PAGE_registreCommerce_numero").value){
        return false;
    }else{
        return true;
    }
}

function validerVilleRc()
{
    var villeRc=document.getElementById('ctl0_CONTENU_PAGE_registreCommerce');
    villeRcChoisi = villeRc.options[villeRc.selectedIndex].value;
    if(villeRcChoisi=="0") {
        return false;
    }
    return true;
}
function validerLibelleFournisseurFrSocieteExclues()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_libelleFournisseurFr").value){
        return false;
    }else{
        return true;
    }
}
function validerLibelleFournisseurArSocieteExclues()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_libelleFournisseurAr").value){
        return false;
    }else{
        return true;
    }
}
function validerMotifFrSocieteExclues()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_motifFr").value){
        return false;
    }else{
        return true;
    }
}
function validerMotifArSocieteExclues()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_motifAr").value){
        return false;
    }else{
        return true;
    }
}
function validerDureeExclusionSocieteExclues()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_exclusion_definitive").checked == false && document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire").checked == false){
        return false;
    }else{
        return true;
    }
}
function validerPorteeExclusionSocieteExclues()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_portee_partielle").checked == false && document.getElementById("ctl0_CONTENU_PAGE_portee_totale").checked == false){
        return false;
    }else{
        return true;
    }
}
function validerExclusionDefinitive()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_exclusion_definitive").checked == true && !document.getElementById("ctl0_CONTENU_PAGE_exclusion_definitive_dateStart").value){
        return false;
    }else{
        return true;
    }
}
function validerExclusionTemporaire()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire").checked == true && (!document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire_dateStart").value
        || !document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire_dateEnd").value)){
        return false;
    }else{
        return true;
    }
}
function validerDateDebutExclusionTemporaire()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire").checked == true
        && !document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire_dateStart").value) {
        return false;
    }else{
        return true;
    }
}

function validerDateFinExclusionTemporaire()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire").checked == true
        && !document.getElementById("ctl0_CONTENU_PAGE_exclusion_temporaire_dateEnd").value) {
        return false;
    }else{
        return true;
    }
}
function ValidateDifferenceDateExclusion()
{
    dateDebut=document.getElementById('ctl0_CONTENU_PAGE_exclusion_temporaire_dateStart').value;
    dateFin=document.getElementById('ctl0_CONTENU_PAGE_exclusion_temporaire_dateEnd').value;

    if(dateDebut!="" && dateFin!="") {
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>=fin){
            return false;
        }
    }
    return true;
}
// Fin fonctions validateurs des données du formulaire de création des sociétés exclues

/*activer selectbox Entites  publiques*/
function activer_entite() {
    document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_entitePublique').className = 'bloc-400';
    document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_entitePublique').disabled = '';
}
/*activer selectbox Entites  publiques*/
function desactiver_entite() {
    document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_entitePublique').className = 'bloc-400 disabled';
    document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_entitePublique').disabled = 'disabled';
}



function isConsultationAlloti()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_marcheUnique") && (document.getElementById("ctl0_CONTENU_PAGE_marcheUnique").checked == true)){

        return false;
    }
    else if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheUnique")
        && document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheUnique").checked == true)
    {
        return false;
    }
    return true;
}

function validateCautionProvisoire()
{
    var cautionProvisoire = document.getElementById("ctl0_CONTENU_PAGE_cautionProvisoire").value;
    if(!isConsultationAlloti() && cautionProvisoire=="")
    {
        return false;
    }
    return true;
}

function checkFormatDateTime(object)
{
    if (object != null && object.value != "")
    {
        var regExp = new RegExp("\\b[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]\\b\\ [0-2][0-9]\\:[0-6][0-9]");
        if(!(object.value.match(regExp)))
        {

            return false;
        }
        else
        {
            if(!isDate (object.value))
            {
                return false;
            }
        }
    }
    return true;
}

function validateEchantillonsDemandes(item,alloti,sender)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(alloti){
        var echantillonOui = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_echantillonOui");
        var echantillonNon = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_echantillonNon");
        var addEchantillion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_addEchantillion");
        var dateLimiteEchantillion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_dateLimiteEchantillion");
    }
    else {
        var echantillonOui = document.getElementById(prefixeId+"_echantillonOui");
        var echantillonNon = document.getElementById(prefixeId+"_echantillonNon");
        var addEchantillion = document.getElementById(prefixeId+"_addEchantillion");
        var dateLimiteEchantillion = document.getElementById(prefixeId+"_dateLimiteEchantillion");
    }
    if(echantillonOui.checked == true)
    {
        if(addEchantillion.value!="" && dateLimiteEchantillion.value!="" && checkFormatDateTime(dateLimiteEchantillion)){
            return true;
        }
        else {
            return false;
        }
    }
    return true;
}


function validateEchantillonsDemandesCons(sender,parameter)
{
    if(validateEchantillonsDemandes("",false,sender)) {
        return true;
        document.getElementById("spanErrorEchantillon").style.display = 'none';
    }
    else {
        //return false;
        document.getElementById("spanErrorEchantillon").style.display = 'block';
    }
}

function validateReunion(item,alloti,sender)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(alloti){
        var reunionNon = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_reunionNon");
        var reunionOui = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_reunionOui");
        var addReunion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_addReunion");
        var dateReunion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_dateReunion");
    }
    else {
        var reunionNon = document.getElementById(prefixeId+"_reunionNon");
        var reunionOui = document.getElementById(prefixeId+"_reunionOui");
        var addReunion = document.getElementById(prefixeId+"_addReunion");
        var dateReunion = document.getElementById(prefixeId+"_dateReunion");
    }
    if(reunionOui.checked == true)
    {
        if(addReunion.value!="" && dateReunion.value!="" && checkFormatDateTime(dateReunion)){
            return true;
        }
        else {
            return false;
        }
    }
    return true;
}

function validateReunionCons(sender,parameter)
{
    if(validateReunion("",false,sender)) {
        return true;
    }
    return false;
}

function hasVisiteLieux(item,alloti,sender)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(alloti) {
        var visiteLotNon = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_visiteLotNon");
        var visiteLotOui = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_visiteLotOui");
        var addVisite = document.getElementById(prefixeId+"_lotsRepeater_ctl" + item + "_addVisite");
    }
    else {
        var visiteLotNon = document.getElementById(prefixeId+"_visiteLotNon");
        var visiteLotOui = document.getElementById(prefixeId+"_visiteLotOui");
        var addVisite =  document.getElementById(prefixeId+"_addVisite");
    }
    if(visiteLotOui.checked == true && addVisite.value!="")
    {
        return true;
    }
    else if(visiteLotNon.checked == true)
    {
        return true;
    }
    return false;
}

function hasVisiteLieuxCons(sender,parameter)
{
    if(hasVisiteLieux("",false,sender)) {
        return true;
    }
    else {
        return false;
    }
}

function isTextNumeric(sText)
{
    var ValidChars = "0123456789.";
    var IsNumber=true;
    var Char;
    for (i = 0; i < sText.length && IsNumber == true; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            IsNumber = false;
        }
    }
    return IsNumber;

}


function validateCautionProvisoireLot()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        while (cautionProvisoire = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_cautionProvisoire"))
        {
            if ((cautionProvisoire.value == '') || !isTextNumeric(cautionProvisoire.value))
            {
                document.getElementById("cautionProvisoire" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}

function validateEchantillonsDemandesLot()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        while (echantillonOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_echantillonOui"))
        {
            if (!validateEchantillonsDemandes(i,true,sender))
            {
                document.getElementById("echantillonError" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}

function validateReunionLot()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        while (reunionOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_reunionOui"))
        {
            if (!validateReunion(i,true,sender))
            {
                document.getElementById("reunionOui" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}

function validateVisitesLieuxLot()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        while (visiteLotOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_visiteLotOui"))
        {
            if (!hasVisiteLieux(i,true,sender))
            {
                document.getElementById("visiteLotOui" + i).style.display = '';
                res = false;
            }
            i++;
        }
    }
    return res;
}

function validateConformiteDossierEntreprise()
{
    res = false;

    if(document.getElementById("ctl0_CONTENU_PAGE_panelInscrit_ouiConforme").checked == true)
    {
        res = true;
    }
    return res;
}
/*
*
* Cette fonction permet de limiter la taille d un textArea
*/
function limiteTailleTextArea(zone,max)
{
    if(zone.value.length>=max)
    {
        zone.value=zone.value.substring(0,max);
    }
}


function validateRetraitDossierChanging()
{
    var retraitDossiers = document.getElementById("ctl0_CONTENU_PAGE_retraitDossiers");
    var retraitDossierCheck = document.getElementById("ctl0_CONTENU_PAGE_retraitDossierCheck");
    if(retraitDossierCheck.checked == true && retraitDossiers.value == "")
    {
        return false;
    }
    return true;
}

function validateAddDepotDossiersChanging()
{
    var addDepotDossiers = document.getElementById("ctl0_CONTENU_PAGE_addDepotDossiers");
    var depotOffresCheck = document.getElementById("ctl0_CONTENU_PAGE_depotOffresCheck");
    if(depotOffresCheck.checked == true && addDepotDossiers.value == "")
    {
        return false;
    }
    return true;
}

function validateOuverturePlisChanging()
{
    var lieuOuverturePlis = document.getElementById("ctl0_CONTENU_PAGE_lieuOuverturePlis");
    var ouverturePlisCheck = document.getElementById("ctl0_CONTENU_PAGE_ouverturePlis");
    if(ouverturePlisCheck.checked == true && lieuOuverturePlis.value == "")
    {
        return false;
    }
    return true;
}

function validatePrixAquisitionChanging()
{
    var prixAquisitaionPlans = document.getElementById("ctl0_CONTENU_PAGE_prixAquisitaionPlans");
    var prixAquisitaionPlansCheck = document.getElementById("ctl0_CONTENU_PAGE_prixAquisitaionPlansCheck");
    //alert(isTextNumeric(prixAquisitaionPlans.value));return ;
    if(!isTextNumeric(prixAquisitaionPlans.value) && prixAquisitaionPlansCheck.checked == true)
    {
        return false;
    }
    return true;
}

function validateCuationProvChanging()
{
    if(!isConsultationAlloti())
    {
        var cautionProvisoire = document.getElementById("ctl0_CONTENU_PAGE_cautionProvisoire");
        var cautionProvisoireCheck = document.getElementById("ctl0_CONTENU_PAGE_cautionProvisoireCheck");
        if(!isTextNumeric(cautionProvisoire.value) && cautionProvisoireCheck.checked == true)
        {
            return false;
        }
        return true;
    }
    return true;
}

function validateEchantillonsChanging()
{
    if(!isConsultationAlloti())
    {
        var addEchantillion = document.getElementById("ctl0_CONTENU_PAGE_addEchantillion");
        var dateLimiteEchantillion = document.getElementById("ctl0_CONTENU_PAGE_dateLimiteEchantillion");
        var echantillionCheck = document.getElementById("ctl0_CONTENU_PAGE_echantillion");
        var echantillionOuiCheck = document.getElementById("ctl0_CONTENU_PAGE_echantillonOui");
        if(echantillionCheck.checked == true && echantillionOuiCheck.checked == true && (addEchantillion.value == "" || checkFormatDateTime(dateLimiteEchantillion)== false || dateLimiteEchantillion.value == "" ))
        {
            return false;
        }
        return true;
    }
    return true;
}

function validateReunionChanging()
{
    if(!isConsultationAlloti())
    {
        var addReunion = document.getElementById("ctl0_CONTENU_PAGE_addReunion");
        var dateReunion = document.getElementById("ctl0_CONTENU_PAGE_dateReunion");
        var echantillionCheck = document.getElementById("ctl0_CONTENU_PAGE_reunion");
        var reunionOuiCheck = document.getElementById("ctl0_CONTENU_PAGE_reunionOui");
        if(echantillionCheck.checked == true && reunionOuiCheck.checked == true && (addReunion.value == "" ||  dateReunion.value== "" || checkFormatDateTime(dateReunion) == false))
        {
            return false;
        }
        return true;
    }
    return true;
}

function validateCautionLotChanging()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 0;
        var cautionProvisoireCheck = document.getElementById("ctl0_CONTENU_PAGE_cautionProvisoireCheck");
        while(cautionProvisoire = document.getElementById("ctl0_CONTENU_PAGE_cautionProvisoireRepeater_ctl" + i + "_cautionProvisoire"))
        {
            if((cautionProvisoire.value=="" || !isNumeric(cautionProvisoire.value))&& cautionProvisoireCheck.checked == true)
            {
                document.getElementById("cautionProvisoire" + i).style.display = '';
                res = false;
            }
            else {
                document.getElementById("cautionProvisoire" + i).style.display = 'none';
            }
            i++;
        }
    }
    return res;
}


function validateEchantillonLotChanging()
{
    var res = true;
    if(isConsultationAlloti())
    {
        i = 0;
        var echantillionCheck = document.getElementById("ctl0_CONTENU_PAGE_echantillion");
        while(addEchantillion = document.getElementById("ctl0_CONTENU_PAGE_echantillonLotRepeater_ctl" + i + "_addEchantillion"))
        {
            var dateLimiteEchantillion = document.getElementById("ctl0_CONTENU_PAGE_echantillonLotRepeater_ctl" + i + "_dateLimiteEchantillion");
            var echantillionOuiChecked = document.getElementById("ctl0_CONTENU_PAGE_echantillonLotRepeater_ctl" + i + "_echantillonOuiLot");
            if((echantillionCheck.checked == true) && (echantillionOuiChecked.checked == true) && (checkFormatDateTime(dateLimiteEchantillion) == false || (dateLimiteEchantillion.value == "") ||(addEchantillion.value == "")))
            {
                document.getElementById("echantillion_" + i).style.display = '';
                res = false;
            }
            else {
                document.getElementById("echantillion_" + i).style.display = 'none';
            }
            i++;
        }
    }
    return res;
}


function validateIntituleTranche(sender, parameter)
{
    var res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true && document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheUnique").checked == true){
        var paramElement = parameter.split('#');
        if(document.getElementById(paramElement[0]).checked == true){
            i = 1;
            while(intituleTranche = document.getElementById(paramElement[1]+"_ctl" + i + "_intituleTranche"))
            {
                if(intituleTranche.value == "")
                {
                    document.getElementById("intituleTrancheText_" + i).style.display = '';
                    res = false;
                }
                else {
                    document.getElementById("intituleTrancheText_" + i).style.display = 'none';
                }
                i++;
            }
        }
    }
    return res;
}

function validateIntituleTrancheLot(sender, parameter)
{
    var res = true;
    var paramElement = parameter.split('#');
    if(document.getElementById(paramElement[0]).checked == true){
        i = 1;
        while(intituleTranche = document.getElementById(paramElement[1]+"_ctl" + i + "_intituleTranche"))
        {
            if(intituleTranche.value == "")
            {
                document.getElementById("intituleTrancheText_" + i).style.display = '';
                res = false;
            }
            else {
                document.getElementById("intituleTrancheText_" + i).style.display = 'none';
            }
            i++;
        }
    }

    return res;
}
function validateFormePrixTranche(sender, parameter)
{
    var res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true && document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheUnique").checked == true){
        var paramElement = parameter.split('#');
        if(document.getElementById(paramElement[0]).checked == true){
            i = 1;
            while(formePrixTranche = document.getElementById(paramElement[1]+"_ctl" + i + "_formePrixTrancheHidden"))
            {
                if(formePrixTranche.value == "")
                {
                    document.getElementById("errorFormePrixTranche_" + i).style.display = '';
                    res = false;
                }
                else {
                    document.getElementById("errorFormePrixTranche_" + i).style.display = 'none';
                }
                i++;
            }
        }
    }
    return res;
}

function validateFormePrixTrancheLot(sender, parameter)
{
    var res = true;
    var paramElement = parameter.split('#');
    if(document.getElementById(paramElement[0]).checked == true){
        i = 1;
        while(formePrixTranche = document.getElementById(paramElement[1]+"_ctl" + i + "_formePrixTrancheHidden"))
        {
            if(formePrixTranche.value == "")
            {
                document.getElementById("errorFormePrixTranche_" + i).style.display = '';
                res = false;
            }
            else {
                document.getElementById("errorFormePrixTranche_" + i).style.display = 'none';
            }
            i++;
        }
    }
    return res;
}

function validateFormePrixSansTranche(sender, parameter)
{
    var res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true && document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheUnique").checked == true){
        var paramElement = parameter.split('#');
        if(document.getElementById(paramElement[0]).checked == false){
            i = 1;
            if(document.getElementById(paramElement[1]).value == "")
            {
                document.getElementById("errorFormePrixSansTranche").style.display = '';
                res = false;

            }else {
                document.getElementById("errorFormePrixSansTranche").style.display = 'none';
                res = true;
            }
        }
    }
    return res;
}

function validateFormePrixSansTrancheLot(sender, parameter)
{
    var res = true;
    var paramElement = parameter.split('#');
    if(document.getElementById(paramElement[0]).checked == false){
        i = 1;
        if(document.getElementById(paramElement[1]).value == "")
        {
            document.getElementById("errorFormePrixSansTranche").style.display = '';
            res = false;

        }else {
            document.getElementById("errorFormePrixSansTranche").style.display = 'none';
            res = true;
        }
    }
    return res;
}


function validateReunionLotChanging()
{
    var res = true;
    if(isConsultationAlloti())
    {
        i = 0;
        var reunionCheck = document.getElementById("ctl0_CONTENU_PAGE_reunion");
        while(addReunion = document.getElementById("ctl0_CONTENU_PAGE_reunionLotRepeater_ctl" + i + "_addReunion"))
        {
            var dateReunion = document.getElementById("ctl0_CONTENU_PAGE_reunionLotRepeater_ctl" + i + "_dateReunion");
            var reunionOuiCheck = document.getElementById("ctl0_CONTENU_PAGE_reunionLotRepeater_ctl" + i + "_reunionOuiLot");
            if((reunionCheck.checked == true) && (reunionOuiCheck.checked == true) && (checkFormatDateTime(dateReunion)==false || (dateReunion.value=="") || (addReunion.value=="")))
            {
                document.getElementById("reunion_" + i).style.display = '';
                res = false;
            }
            else {
                document.getElementById("reunion_" + i).style.display = 'none';
            }
            i++;
        }
    }
    return res;
}

function validateVisitesLotChanging()
{
    res = true;
    if(isConsultationAlloti())
    {
        i = 0;
        var visiteCheck = document.getElementById("ctl0_CONTENU_PAGE_visite");
        if (visiteCheck.checked==true)
        {
            while (visiteLotOui = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl" + i + "_visiteLotOui"))
            {
                deletingAllVisite = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl" + i + "_deletingAllVisite");
                haveVisite = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl" + i + "_haveVisite");
                if (visiteLotOui.checked==true)
                {
                    if(haveVisite.value=="0" || deletingAllVisite.value=="0"){
                        res = false;
                    }
                }

                i++;
            }

        }
    }
    if(res==false) {
        document.getElementById("spanErrorVisite").style.display = '';
    }
    return res;
}


function validateVisitesChanging()
{
    res = true;
    if(!isConsultationAlloti())
    {
        i = 0;
        var visiteCheck = document.getElementById("ctl0_CONTENU_PAGE_visite");
        var deletingAllVisite = document.getElementById("ctl0_CONTENU_PAGE_deletingAllVisite");
        if (visiteCheck.checked==true && (deletingAllVisite.value=="0"))
        {
            res = false;
        }
    }
    return res;
}

function validateDateVisiteLieux()
{
    res = true;
    var dateVal = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
    if ((checkFormatDateTime(dateVal)==false) || (dateVal.value=="")){
        res = false;
    }
    return res;
}

function downloadPartialEnveloppeOffre(lot,idBlob)
{
    var i=1 ;
    var bool= false;
    while (object = document.getElementById("lot_"+lot+"_env_item_"+idBlob+"_"+i))
    {
        if (object.checked)
        {
            bool= true;
        }
        i++;
    }
    return bool;
}


function frnDateToIso(idDateIso)
{
    dateF = idDateIso.split(' ');
    dateFS = dateF[0].split('/');
    if(dateF[1]){
        times = dateF[1].split(':');
    }
    if(dateFS[2] && dateFS[1] && dateFS[0]){
        dateCompar = dateFS[2]+"-"+dateFS[1]+"-"+dateFS[0];
        if(times[0] && times[1]){
            dateCompar += " "+times[0]+":"+times[1];
        }
    }else{
        dateCompar = idDateIso;
    }
    return dateCompar;
}

function validateEchantillonsDateComapare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(!isConsultationAlloti()){
        var echantillonOui = document.getElementById(prefixeId+"_echantillonOui");
        var dateLimiteEchantillion = document.getElementById(prefixeId+"_dateLimiteEchantillion");
        document.getElementById("spanErrorEchantillon").style.display = 'none';
    }
    return true;
}

function validateEchantillonsDateComapareComposant(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    var echantillonOui = document.getElementById(prefixeId+"_echantillonOui");
    var dateLimiteEchantillion = document.getElementById(prefixeId+"_dateLimiteEchantillion");
    var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
    if(dateRemisePlis == null) {
        dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
    }
    if((echantillonOui.checked == true)&& (dateLimiteEchantillion.value!="") && (dateRemisePlis.value!="")) {
        if(frnDateToIso(dateLimiteEchantillion.value)>frnDateToIso(dateRemisePlis.value)) {
            return false;
            document.getElementById("spanErrorEchantillon").style.display = 'block';
        }else {
            document.getElementById("spanErrorEchantillon").style.display = 'none';
        }
    }
    return true;
}

function validateLotEchantillonsDateComapare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
        while(addEchantillion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_addEchantillion"))
        {
            var dateLimiteEchantillion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_dateLimiteEchantillion");
            if((dateLimiteEchantillion.value!="") && (dateRemisePlis.value!="")) {
                dateLimiteEchantillionF = frnDateToIso(dateLimiteEchantillion.value);
                dateRemisePlisF = frnDateToIso(dateRemisePlis.value);
                if(dateLimiteEchantillionF > dateRemisePlisF)
                {
                    res = false;
                }
            }
            i++;
        }
    }
    return res;
}

function checkUnCheckAllArchives()
{
    index=1;
    archiveSelectionAll = document.getElementById('ctl0_CONTENU_PAGE_tableauDeBordRepeater_ctl0_archive_Selection_all');
    while(checkBox=document.getElementById('ctl0_CONTENU_PAGE_tableauDeBordRepeater_ctl'+index+'_archiveSelection'))    {
        if(archiveSelectionAll.checked==true) {
            checkBox.checked=true;
        }
        else {
            checkBox.checked=false;
        }
        index++;

    }
}


function validateReunionDateComapare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(!isConsultationAlloti()){
        var reunionOui = document.getElementById(prefixeId+"_reunionOui");
        var dateReunion = document.getElementById(prefixeId+"_dateReunion");
        var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
        if(dateRemisePlis == null) {
            dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
        }
        if((reunionOui.checked == true)&& (dateReunion.value!="") && (dateRemisePlis.value!="")) {
            if(frnDateToIso(dateReunion.value)>frnDateToIso(dateRemisePlis.value)) {
                return false;
                document.getElementById("spanErrorReunion").style.display = 'block';
            }else {
                document.getElementById("spanErrorReunion").style.display = 'none';
            }
        }
    }
    return true;
}

function validateReunionDateComapareCompsant(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    var reunionOui = document.getElementById(prefixeId+"_reunionOui");
    var dateReunion = document.getElementById(prefixeId+"_dateReunion");
    var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
    if(dateRemisePlis == null) {
        dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
    }
    if((reunionOui.checked == true)&& (dateReunion.value!="") && (dateRemisePlis.value!="")) {
        if(frnDateToIso(dateReunion.value)>frnDateToIso(dateRemisePlis.value)) {
            return false;
            document.getElementById("spanErrorReunion").style.display = 'block';
        }else {
            document.getElementById("spanErrorReunion").style.display = 'none';
        }
    }
    return true;
}
function validateLotReunionDateComapare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
        while(dateReunion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_dateReunion"))
        {
            var dateReunion = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_dateReunion");
            if((dateReunion.value!="") && (dateRemisePlis.value!="")) {
                dateReunionF = frnDateToIso(dateReunion.value);
                dateRemisePlisF = frnDateToIso(dateRemisePlis.value);
                if(dateReunionF > dateRemisePlisF)
                {
                    document.getElementById("reunionOui" + i).style.display = '';
                    res = false;
                }
                else {
                    document.getElementById("reunionOui" + i).style.display = 'none';
                }
            }
            i++;
        }
    }
    return res;
}


function dateVisiteLieuxCompare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    res =  true;

    var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
    if(dateRemisePlis == null) {
        dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
    }
    var visiteLotNon = document.getElementById(prefixeId+"_visiteLotNon");
    var visiteLotOui = document.getElementById(prefixeId+"_visiteLotOui");
    var addVisite =  document.getElementById(prefixeId+"_addVisite");
    if(visiteLotOui.checked == true && addVisite.value!="")
    {
        i=1;
        while(dateValue = document.getElementById(prefixeId+"_visiteRepeater_ctl" + i + "_dateValue"))
        {
            var dateValue = document.getElementById(prefixeId+"_visiteRepeater_ctl" + i + "_dateValue");
            if((dateValue.value!="") && (dateRemisePlis.value!="")) {
                date = dateValue.value;
                dateValueF = date.substring(0,16);
                dateRemisePlisF = frnDateToIso(dateRemisePlis.value);
                if(dateValueF > dateRemisePlisF)
                {
                    res = false;
                    document.getElementById("spanErrorVisite").style.display = 'none';
                }
                else {
                    document.getElementById("spanErrorVisite").style.display = 'none';
                }
            }
            i++;
        }
    }
    else if(visiteLotNon.checked == true)
    {
        res =  true;
    }
    return res;
}

function dateVisiteLieuxLotCompare(sender,parameter)
{
    var echantillonOui = sender.control.id;
    var arrayElement = echantillonOui.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        var dateRemisePlis = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis");
        while(visiteLotOui = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_visiteLotOui"))
        {
            j=1;
            while(dateValue = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_visiteRepeater_ctl" + j + "_dateValue"))
            {
                var dateValue = document.getElementById(prefixeId+"_lotsRepeater_ctl" + i + "_visiteRepeater_ctl" + j + "_dateValue");
                if((dateValue.value!="") && (dateRemisePlis.value!="")) {
                    date = dateValue.value;
                    dateValueF = date.substring(0,16);
                    dateRemisePlisF = frnDateToIso(dateRemisePlis.value);
                    if(dateValueF > dateRemisePlisF)
                    {
                        res = false;
                        document.getElementById("visiteLotOui"+i).style.display = 'none';
                    }
                    else {
                        document.getElementById("visiteLotOui"+i).style.display = 'none';
                    }
                }
                j++;
            }
            i++;
        }
    }
    return res;

}

/*Permet de rendre visible ou masquer des informations sur la consultation: type de procedure, catégorie, intitulé, ...*/
function showAllDiv(langueSelectionne, repeaterItemIndex,languesActives) {
    var listeLanguesActives = languesActives.split('/');
    for (var i = 0; i < listeLanguesActives.length; i++){
        if(listeLanguesActives[i] == langueSelectionne) {
            showDiv('typeProcedure_'+repeaterItemIndex+'_'+langueSelectionne);
            showDiv('categorie_'+repeaterItemIndex+'_'+langueSelectionne);
            showDiv('consultation_'+repeaterItemIndex+'_'+langueSelectionne);
        } else {
            hideDiv('typeProcedure_'+repeaterItemIndex+'_'+listeLanguesActives[i]);
            hideDiv('categorie_'+repeaterItemIndex+'_'+listeLanguesActives[i]);
            hideDiv('consultation_'+repeaterItemIndex+'_'+listeLanguesActives[i]);
        }
    }
}
function displayAvertissementSaveWithPj(countPJ)
{
    if(countPJ!='0')
    {
        document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniqueJAL_buttonSave').click();
    }
    else
    {
        //javascript:popUp('?page=Agent.PopupConfirmationSaveMessage','yes');
        if(confirm("<?php Prado::localize('TEXT_CONFIRMATION_ECHANGE_SANS_PJ') ?>")){
            document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniqueJAL_buttonSave').click();
        }
    }
}

/*
* Fonction qui valide le telechargement des plis dans la page ouverture et analyse
* Elle verifie si un repertoire sélectionné et un dossier sélectionné, alors il masque le bouton "télécharger les plis sélectionnés"
* ou le bouton "télécharger plis par plis" après le click du bouton
*/
function validerTelechargementPlis(repeaterName, element, repertoire, messageErreur, modeApplet) {
    if(isOneCheckedAndRepChoosen(repeaterName, element, repertoire, messageErreur, modeApplet)){

        //Début cas de téléchargement des plis sélectionnés
        if(document.getElementById('ctl0_CONTENU_PAGE_telechargerPlis')) {
            document.getElementById('ctl0_CONTENU_PAGE_telechargerPlis').style.display='none';
        }//Fin cas de téléchargement des plis sélectionnés

        return true;
    } else {
        return false;
    }
}
function servicesAccessiblesCoches()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl0_service').checked == false || document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl0_service').checked == true){
        document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl0_service').checked = true;
    }
}

/*
* Fonction de validation de l'email sur le formulaire demande des entreprises
*/
function validatorEmailFormulaireDemande(email)
{
    if(email.value !='') {
        if(validateEmail(email.value)) return true;
        else return false;
    }
    else {
        return false;
    }
}

/*
* Expression régulière qui valide un email
*/
function validateEmail(elementValue){
    var emailPattern =   /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return emailPattern.test(elementValue);
}

/*
* Fonction de validation de l'email sur le formulaire inscription user
*/

function SisPostInfo()
{
    if(document.all) {
        doc = document.all;//IE
    } else {
        doc = document;
    }
    doc.main_form.action = "<?php echo Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN') ?>";
    doc.main_form.submit();
}

function isAnnonceProgrammeProvisionnel()
{
    if(intituleAvis.options[intituleAvis.selectedIndex].value==7) {
        return true;
    }
    return false;
}


function isAnnonceProgrammeProvisionnel()
{
    var intituleAvis = document.getElementById("ctl0_CONTENU_PAGE_intituleAvis");
    ///
    if(intituleAvis.options[intituleAvis.selectedIndex].value==<?php echo Atexo_Config::getParameter('TYPE_AVIS_PROGRAMME_PREVISIONNEL') ?>) {
        return true;
    }
    return false;
}

function validateReferance()
{
    if(!isAnnonceProgrammeProvisionnel()){
        var reference = document.getElementById("ctl0_CONTENU_PAGE_reference").value;
        if(reference=="")
        {
            return false;
        }
    }
    return true;
}

function validateProcedureType()
{
    if(!isAnnonceProgrammeProvisionnel() && !isAnnonceProjetAchat()){
        var procedureType = document.getElementById("ctl0_CONTENU_PAGE_procedureType");
        if(procedureType.options[procedureType.selectedIndex].value==0){
            return false;
        }
    }
    return true;
}

function validateCategorie()
{
    if(!isAnnonceProgrammeProvisionnel()){
        var categorie = document.getElementById("ctl0_CONTENU_PAGE_categorie");
        if(categorie.options[categorie.selectedIndex].value==0){
            return false;
        }
    }
    return true;
}

function validateObjectConsultation()
{
    if(!isAnnonceProgrammeProvisionnel()){
        var objet = document.getElementById("ctl0_CONTENU_PAGE_objet").value;
        if(objet=="")
        {
            return false;
        }
    }
    return true;
}

/**
 * affiche seul le bloc pour une annonce programme provisionnel
 *
 */
function displayFormulaireAPP()
{
    if(isAnnonceProgrammeProvisionnel()){
        hideDiv('ctl0_CONTENU_PAGE_divprocedureType');
        hideDiv('ctl0_CONTENU_PAGE_divReference');
        hideDiv('ctl0_CONTENU_PAGE_divCategorie');
        hideDiv('ctl0_CONTENU_PAGE_divIntituleConsultation');
        hideDiv('ctl0_CONTENU_PAGE_divObjConsultation');
        hideDiv('ctl0_CONTENU_PAGE_panelCode_cpv');


    }
    else {
        showDiv('ctl0_CONTENU_PAGE_divprocedureType');
        if(isAnnonceProjetAchat()){
            hideSpan('procedureObligatoire');
        }else{
            showSpan('procedureObligatoire');
        }
        showDiv('ctl0_CONTENU_PAGE_divReference');
        showDiv('ctl0_CONTENU_PAGE_divCategorie');

        showDiv('ctl0_CONTENU_PAGE_divIntituleConsultation');
        showDiv('ctl0_CONTENU_PAGE_divObjConsultation');
        showDiv('ctl0_CONTENU_PAGE_panelCode_cpv');

    }
}

function servicesAccessibles()
{
    nbreServices = document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_nbreServices').value;
    var nbreCoche = 0;
    for(var i=0;i< nbreServices;i++)
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl'+i+'_service') && document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl'+i+'_service').checked == true)
        {
            nbreCoche++;
        }
    }
    if(nbreCoche == 0)
    {
        return false;
    }
    else{
        return true;
    }
}

function displayCountries()
{
    var listPays = document.getElementById("ctl0_CONTENU_PAGE_listPays");
    if (listPays.options.length == 1) {
        document.getElementById("ctl0_CONTENU_PAGE_refreshListPays").click();
    }
}
function displayCountries2(listPays,refreshListPays)
{
    if (listPays.options.length == 1) {
        refreshListPays.click();
    }
}
function selectOneRadioButton(myInput,nbr,id)
{
    for(i=0;i < nbr;i++)
    {
        // ctl0_CONTENU_PAGE_repeaterTypePouvoirAdjudicateur_ctl0_pouvoirAdjuActivitePrincipalesType
        var radioCheck = document.getElementById('ctl0_CONTENU_PAGE_repeaterTypePouvoirAdjudicateur_ctl'+i+'_pouvoirAdjuActivitePrincipalesType');
        if(radioCheck != null && id!=i)
            radioCheck.checked = false;
        myInput.checked = true;
    }
}

function marqueItemSelected(hiddenligneSelectionne,itemIndex)
{
    document.getElementById(hiddenligneSelectionne).value=itemIndex;
}

function updateChosenFile2(_fileWithPath, _idInputToUpdate){
    textBox = document.getElementById(_idInputToUpdate);
    textBox.value = _fileWithPath;
    if(document.getElementById('ctl0_CONTENU_PAGE_urlVerifyCertificat') != null) {
        document.AppletSignaturesInfos.setParam3(document.getElementById('ctl0_CONTENU_PAGE_urlVerifyCertificat').value);
    }
    addFilesSignature(_idInputToUpdate);
    document.AppletSignaturesInfos.setListeFoundSignatureForChoosenFiles(document.getElementById('ctl0_CONTENU_PAGE_listeFoundSignatureForChoosenFiles').value);
    getSignatures();

}

function getSignatures()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_urlVerifyCertificat') != null) {
        document.AppletSignaturesInfos.setParam3(document.getElementById('ctl0_CONTENU_PAGE_urlVerifyCertificat').value);
    }
    document.AppletSignaturesInfos.setParam2(true);
}
function recupererSignatures(_signaturesInfos,_idInputToUpdate){
    if(_idInputToUpdate[0][0]!=null && document.getElementById(_idInputToUpdate[0][0])!=null){
        if(_idInputToUpdate[0][1]!=null){
            document.getElementById('ctl0_CONTENU_PAGE_listeFoundSignatureForChoosenFiles').value = _idInputToUpdate[0][1];
        }
        if(document.getElementById(_idInputToUpdate[0][0]).value!=""){
            var concatResults = new Array("","","","","");
            var idLastSign="last";
            var idLot = "";
            var typeEnv = "";
            var ae = "";
            var cand="";
            var indexItem;
            var nbreSigns;
            var messagesErreurSignatures="";
            var elem = _idInputToUpdate[0][0].split('_');
            var path = document.getElementById(_idInputToUpdate[0][0]).value;
            var fichiersAvecCertificatExpire="";
            var fichiersAvecPlsrsSignatures="";
            var fichiersSansSignatures="";
            var dirPiece="";

            if(elem.length==6){
                indexItem =elem[4].substr(3);
                idLot = elem[4].substr(3);
                if(elem[3]=="repeaterOffres"){
                    ae="1";
                }
                suffixe= elem[0]+"_"+elem[1]+"_"+elem[2]+"_"+elem[3]+"_ctl";
            }
            if(elem.length==9){
                indexItem =elem[7].substr(3);
                idLot = elem[4].substr(3);
                if(elem[3]=="repeaterOffres")
                    typeEnv="Offre";
                if(elem[3]=="repeaterAnonymat")
                    typeEnv="Anonymat";
                if(elem[3]=="repeaterOffresTechnique")
                    typeEnv="Technique";
                suffixe= elem[0]+"_"+elem[1]+"_"+elem[2]+"_"+elem[3]+"_"+elem[4]+"_"+elem[5]+"_"+elem[6]+"_ctl";
            }
            if(elem.length==7){
                indexItem =elem[5].substr(3);
                if(elem[3]=="PiecesCand")
                    cand="cand";
                suffixe= elem[0]+"_"+elem[1]+"_"+elem[2]+"_"+elem[3]+"_"+elem[4]+"_ctl";
            }
            if(_signaturesInfos!=null){
                dirPiece += "<img class='picto-info' title='Info-bulle' alt='Info-bulle' onmouseover=\"afficheBulle('" + suffixe+indexItem+ "_dirPiece', this)\" onmouseout=\"cacheBulle('" + suffixe+indexItem+ "_dirPiece')\" src='themes/images/picto-info-utile.gif'/>";
                dirPiece += "<br/><div id='" +suffixe+indexItem+ "_dirPiece' class='info-bulle' onmouseover='mouseOverBulle();' onmouseout='mouseOutBulle();'><div>"+ document.getElementById(_idInputToUpdate[0][0]).value +"</div></div>";

                if(document.getElementById('ctl0_CONTENU_PAGE_lastSignature')){
                    //pour savoir que je suis sur la page d'envoi de réponse.
                    for (var i = 0; i < _signaturesInfos.length; i++){
                        if(_signaturesInfos[i][6]!=null){
                            caCrlDate = _signaturesInfos[i][6].split("-");
                            //signature valide : all valide
                            if(_signaturesInfos[i][0]!=null && _signaturesInfos[i][4]=="0" && caCrlDate[0]=="0" && caCrlDate[1]=="0" && caCrlDate[2]=="0"){
                                idLastSign = i;
                            }
                        }
                        if(idLastSign=="last" && _signaturesInfos[i][0]!=null && (_signaturesInfos[i][4]=="0" || _signaturesInfos[i][4]=="3")){//si le path signature existe et la signature est valide +++VErification signature  || _signaturesInfos[i][4]=="2"
                            idLastSign = i;
                        }
                    }

                    file = path.split('\\');
                    fileName = file[file.length-1];

                    if(idLastSign!="last")
                    {
                        concatResults = construireSignatureLinks(_signaturesInfos,idLastSign,suffixe,indexItem,idLot,typeEnv,ae,cand);
                        // Erreurs de signature
                        if(_signaturesInfos[idLastSign][4]=="3"){
                            //Fichier avec signature par certificat expiré.
                            document.getElementById(suffixe+indexItem+"_fichiersSansSignatures").value="";
                            document.getElementById(suffixe+indexItem+"_fichiersAvecPlsrsSignatures").value="";
                            document.getElementById(suffixe+indexItem+"_fichiersAvecCertificatExpire").value=fileName;
                        }

                        if(idLastSign>=1){
                            //Fichier avec Plusieurs Signatures
                            document.getElementById(suffixe+indexItem+"_fichiersSansSignatures").value="";
                            document.getElementById(suffixe+indexItem+"_fichiersAvecPlsrsSignatures").value=fileName;
                            document.getElementById(suffixe+indexItem+"_fichiersAvecCertificatExpire").value="";
                            document.getElementById(suffixe+indexItem+"_signaturePriseEnCompte").value=_signaturesInfos[idLastSign][0];
                        }
                    }else{
                        //Fichier Sans Signature
                        drawSign = invalideSignatureDraw();
                        concatResults[0] = drawSign[0];
                        concatResults[1] = drawSign[1];
                        document.getElementById(suffixe+indexItem+"_fichiersSansSignatures").value=fileName;
                        document.getElementById(suffixe+indexItem+"_fichiersAvecPlsrsSignatures").value="";
                        document.getElementById(suffixe+indexItem+"_fichiersAvecCertificatExpire").value="";
                    }
                    //Erreurs de signature
                }
                else{
                    for (var i = 0; i < _signaturesInfos.length; i++){
                        if(_signaturesInfos[i][0]!=null)
                        {
                            results = construireSignatureLinks(_signaturesInfos,i,suffixe,indexItem,idLot,typeEnv,ae,cand);
                            concatResults[0]+=results[0];
                            concatResults[1]+=results[1];
                            concatResults[2]+=results[2];
                            concatResults[3]+=results[3];
                            concatResults[4]+=results[4];
                        }
                    }
                }

                if(document.getElementById(suffixe+indexItem+'_cnLabel')){
                    document.getElementById(suffixe+indexItem+'_cnLabel').innerHTML = concatResults[0];
                }
                if(document.getElementById(suffixe+indexItem+'_imgSignElectrnik')){
                    document.getElementById(suffixe+indexItem+'_imgSignElectrnik').innerHTML = concatResults[1];
                }
                if(document.getElementById(suffixe+indexItem+'_cheminFichier')){
                    document.getElementById(suffixe+indexItem+'_cheminFichier').innerHTML = dirPiece;
                }
            }
        }
    }
}

function construireSignatureLinks(_signaturesInfos,i,suffixe,indexItem,idLot,typeEnv,ae,cand){
    var results = new Array;
    var chaineCnSigns="";
    var chaineSignElectr="";
    var chaineSignStatut="";
    var chaineTelechargerSign="";
    var chaineLinkPopupSign="";

    var divParent1 = document.getElementById('divCn1');
    var divParent2 = document.getElementById('divCn2');

    var inputPathSign = document.createElement("INPUT");
    inputPathSign.setAttribute("type", "hidden");
    inputPathSign.setAttribute("id", suffixe+indexItem+"_pathSignature" + i);
    inputPathSign.setAttribute("name", suffixe+indexItem+"_pathSignature" + i);
    inputPathSign.setAttribute("value",_signaturesInfos[i][0]);
    divParent1.appendChild(inputPathSign);

    //debut récuperation path last signature
    if("ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl"+indexItem+"_pathSignature"==suffixe+indexItem+"_pathSignature")
    {
        document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+indexItem+'_pathSignature').value=_signaturesInfos[i][0];
    }
    if("ctl0_CONTENU_PAGE_repeaterOffres_ctl"+idLot+"_pathSignature"==suffixe+idLot+"_pathSignature")
    {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+idLot+'_pathSignature').value=_signaturesInfos[i][0];
    }
    if("ctl0_CONTENU_PAGE_repeaterOffres_ctl"+idLot+"_PiecesOffre_listePiecesRepeater_ctl"+indexItem+"_pathSignature"==suffixe+indexItem+"_pathSignature")
    {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+idLot+'_PiecesOffre_listePiecesRepeater_ctl'+indexItem+'_pathSignature').value=_signaturesInfos[i][0];
    }
    if("ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl"+idLot+"_PiecesTechniques_listePiecesRepeater_ctl"+indexItem+"_pathSignature"==suffixe+indexItem+"_pathSignature")
    {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+idLot+'_PiecesTechniques_listePiecesRepeater_ctl'+indexItem+'_pathSignature').value=_signaturesInfos[i][0];
    }
    if("ctl0_CONTENU_PAGE_repeaterAnonymat_ctl"+idLot+"_PiecesAnonymat_listePiecesRepeater_ctl"+indexItem+"_pathSignature"==suffixe+indexItem+"_pathSignature")
    {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+idLot+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexItem+'_pathSignature').value=_signaturesInfos[i][0];
    }
    //fin récuperation path last signature
    var inputCn = document.createElement("INPUT");
    inputCn.setAttribute("type", "hidden");
    inputCn.setAttribute("id", suffixe+indexItem+"_cnSignature" + i);
    inputCn.setAttribute("name", suffixe+indexItem+"_cnSignature" + i);
    inputCn.setAttribute("value",_signaturesInfos[i][1]);
    divParent1.appendChild(inputCn);

    var inputStatut = document.createElement("INPUT");
    inputStatut.setAttribute("type", "hidden");
    inputStatut.setAttribute("id", suffixe+indexItem+"_statutSignature" + i);
    inputStatut.setAttribute("name", suffixe+indexItem+"_statutSignature" + i);
    inputStatut.setAttribute("value",_signaturesInfos[i][4]);
    divParent2.appendChild(inputStatut);

    SignataireValue=_signaturesInfos[i][3];
    var inputSignataire = document.createElement("INPUT");
    inputSignataire.setAttribute("type", "hidden");
    inputSignataire.setAttribute("id", suffixe+indexItem+"_signataire" + i);
    inputSignataire.setAttribute("name", suffixe+indexItem+"_signataire" + i);
    inputSignataire.setAttribute("value",SignataireValue);
    divParent2.appendChild(inputSignataire);

    EmisParValue=_signaturesInfos[i][2];
    var inputEmisPar = document.createElement("INPUT");
    inputEmisPar.setAttribute("type", "hidden");
    inputEmisPar.setAttribute("id", suffixe+indexItem+"_emisPar" + i);
    inputEmisPar.setAttribute("name", suffixe+indexItem+"_emisPar" + i);
    inputEmisPar.setAttribute("value",EmisParValue);
    divParent2.appendChild(inputEmisPar);

    dateValideFromValue=_signaturesInfos[i][7];
    var inputDateValideFrom = document.createElement("INPUT");
    inputDateValideFrom.setAttribute("type", "hidden");
    inputDateValideFrom.setAttribute("id", suffixe+indexItem+"_dateValideFrom" + i);
    inputDateValideFrom.setAttribute("name", suffixe+indexItem+"_dateValideFrom" + i);
    inputDateValideFrom.setAttribute("value",dateValideFromValue);
    divParent2.appendChild(inputDateValideFrom);

    dateValideToValue=_signaturesInfos[i][8];
    var inputDateValideTo = document.createElement("INPUT");
    inputDateValideTo.setAttribute("type", "hidden");
    inputDateValideTo.setAttribute("id", suffixe+indexItem+"_dateValideTo" + i);
    inputDateValideTo.setAttribute("name", suffixe+indexItem+"_dateValideTo" + i);
    inputDateValideTo.setAttribute("value",dateValideToValue);
    divParent2.appendChild(inputDateValideTo);

    if(_signaturesInfos[i][4]=="0" || _signaturesInfos[i][4]=="3"){//verification signature _signaturesInfos[i][4]=="2" ||
        chaineCnSigns += "<div class='signataire-line'>CN : " + _signaturesInfos[i][1];
        chaineCnSigns += "<img class='picto-info' title='Info-bulle' alt='Info-bulle' onmouseover=\"afficheBulle('" + suffixe+indexItem+ "_cheminSignature_"+i+"', this)\" onmouseout=\"cacheBulle('" + suffixe+indexItem+ "_cheminSignature_"+i+"')\" src='themes/images/picto-info-utile.gif'/>";
        chaineCnSigns += "<br/><div id='" +suffixe+indexItem+ "_cheminSignature_"+i+"' class='info-bulle' onmouseover='mouseOverBulle();' onmouseout='mouseOutBulle();'><div>"+_signaturesInfos[i][0]+"</div></div></div>";


        chaineSignElectr +="<div class='statut-signature-col'><img title='<?php Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE') ?>' alt='<?php Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE') ?>' src='themes/images/picto-signature.gif'/>: ";
        if(_signaturesInfos[i][6]==null) {
            chaineSignElectr +="<img title='<?php Prado::localize('TEXT_VERIFICATION_IMCOMPLETE') ?>' alt='<?php Prado::localize('TEXT_VERIFICATION_IMCOMPLETE') ?>' src='themes/images/picto-verif-incomplete.gif'/></div>";
        } else {
            caCrlDate = _signaturesInfos[i][6].split("-");
            if(_signaturesInfos[i][4]=="0" && caCrlDate[0]=="0" && caCrlDate[1]=="0" && caCrlDate[2]=="0"){
                chaineSignElectr +="<img title='<?php Prado::localize('DEFINE_SIGNATURE_VALIDE') ?>' alt='<?php Prado::localize('DEFINE_SIGNATURE_VALIDE') ?>' src='themes/images/picto-check-ok-small.gif'/></div>";
            }
            if(_signaturesInfos[i][4]=="1" || caCrlDate[0]=="1" || caCrlDate[1]=="1" || caCrlDate[2]=="1"){
                chaineSignElectr +="<img title='<?php Prado::localize('DEFINE_SIGNATURE_INVALIDE_DETAIL_PLI') ?>' alt='<?php Prado::localize('DEFINE_SIGNATURE_INVALIDE_DETAIL_PLI') ?>' src='themes/images/picto-check-not-ok.gif'/></div>";
            }
            /*if(_signaturesInfos[i][4]=="3"){
                    chaineSignElectr +="<img title='<?php Prado::localize('TEXT_VERIFICATION_IMCOMPLETE') ?>' alt='<?php Prado::localize('TEXT_VERIFICATION_IMCOMPLETE') ?>' src='themes/images/picto-verif-incomplete.gif'/></div>";
                }*/
        }
        chaineSignElectr += "<div class='lien-signature-col'><a href=\"javascript:popUp('index.php?page=Entreprise.PopUpDetailSignature";
        if(idLot!="" && document.getElementById('ctl0_CONTENU_PAGE_lastSignature'))
            chaineSignElectr += "&idLot="+idLot;
        chaineSignElectr += "&idPiece="+indexItem+"&idSign="+i+"&statut="+_signaturesInfos[i][4];//+"&chaine=2&crl=2";
        if(_signaturesInfos[i][6]==null) {
            chaineSignElectr += "&chaine=2&crl=2&expired=2";
        }else {
            caCrlDate = _signaturesInfos[i][6].split("-");
            chaineSignElectr += "&chaine="+caCrlDate[0]+"&crl="+caCrlDate[1]+"&expired="+caCrlDate[2];
        }
        //Verification de la signature
        //+"&chaine="+_signaturesInfos[i][5]+"&crl="+_signaturesInfos[i][6]+"&expired="+_signaturesInfos[i][7]

        if(typeEnv!="")
            chaineSignElectr +="&typeEnv="+typeEnv;
        if(ae!="")
            chaineSignElectr +="&ae";
        if(cand!="")
            chaineSignElectr +="&cand";
        chaineSignElectr += "','yes')\"><img src='themes/images/picto-details-link.gif' alt='<?php Prado::localize('DEFINE_DETAIL_SIGNATURE') ?>' title='<?php Prado::localize('DEFINE_DETAIL_SIGNATURE') ?>' /></a> </div><div class='breaker'></div>";
        //<div class='lien-signature-col'><a href='file:///"+_signaturesInfos[i][0]+"'><img src='themes/images/picto-compresse.gif' alt='<?php Prado::localize('DEFINE_TELECHARGER_SIGNATURE') ?>' title='<?php Prado::localize('DEFINE_TELECHARGER_SIGNATURE') ?>'/></a> </div>*/
    }else {
        drawSign = invalideSignatureDraw();
        chaineCnSigns += drawSign[0];
        chaineSignElectr += drawSign[1];
    }

    results[0]= chaineCnSigns;
    results[1]= chaineSignElectr;
    results[2]= chaineSignStatut;
    results[3]= chaineTelechargerSign;
    results[4]= chaineLinkPopupSign;

    return results;
}

function addFilesSignature(_idInputToUpdate){
    if(_idInputToUpdate!="" && document.getElementById(_idInputToUpdate).value!=""){
        document.AppletSignaturesInfos.fctTrt1(_idInputToUpdate,document.getElementById(_idInputToUpdate).value);
    }
}

function sendIdBtnDelete(input){
    var elem = input.split('_');
    var chaine="";

    if(elem.length==9){
        chaine = elem[3]+"_"+elem[4].substr(3);//+"_"+elem[7].substr(3);
    }
    if(elem.length==7){
        chaine = elem[3]+"_"+elem[5].substr(3);
    }
    document.getElementById('ctl0_CONTENU_PAGE_idBtnDelete').value=chaine;
}

function erreursSignatures(){

    var message = "";
    var indexErreur = 0;
    var fichiersReponse="";
    var FSS="";
    var FAPS="";
    var FACE="";
    var fichiersSansSignatures = "";
    var fichiersAvecPlsrsSignatures = "";
    var fichiersAvecCertificatExpire = "";
    var fichierOK="";
    var SPEC="";
    var signaturePriseEnCompte="";

    //Debut Récuperer tous les fichiers de la réponse et les fichiers avec signatues Erronées.
    index=1;
    while(fileCand=document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_intitulePiece')) {
        file = (fileCand.value).split('\\');
        fileName = file[file.length-1];
        fichiersReponse+= fileName+"|";
        if(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersSansSignatures').value!="")
            FSS += document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersSansSignatures').value +"|";
        if(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecPlsrsSignatures').value!="")
            FAPS += document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecPlsrsSignatures').value +"|";
        if(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecCertificatExpire').value!="")
            FACE += document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecCertificatExpire').value +"|";
        if(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_signaturePriseEnCompte').value!="")
            SPEC += document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_signaturePriseEnCompte').value +"|";
        index++;
    }

    //Offre technique
    index=0;
    while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
        indexFile=1;
        while(fileLotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
            file = (fileLotOT.value).split('\\');
            fileName = file[file.length-1];
            fichiersReponse+= fileName+"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value!="")
                FSS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value!="")
                FAPS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value!="")
                FACE += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value!="")
                SPEC += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value +"|";
            indexFile++;
        }
        index++;
    }

    //Offre
    index=0;
    while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
        sousPli=lotO.value;
        indexFile=1;
        while(fileLotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
            file = (fileLotO.value).split('\\');
            fileName = file[file.length-1];
            fichiersReponse+= fileName+"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value!="")
                FSS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value!="")
                FAPS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value!="")
                FACE += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value!="")
                SPEC += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value +"|";
            indexFile++;
        }
        fileAe = document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichierAE');
        if(fileAe!=null && fileAe.value) {
            file = (fileAe.value).split('\\');
            fileName = file[file.length-1];
            fichiersReponse+= fileName+"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersSansSignatures').value!="")
                FSS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersSansSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecPlsrsSignatures').value!="")
                FAPS += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecPlsrsSignatures').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecCertificatExpire').value!="")
                FACE += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecCertificatExpire').value +"|";
            if(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_signaturePriseEnCompte').value!="")
                SPEC += document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_signaturePriseEnCompte').value +"|";
        }
        index++;
    }

    //anonymat
    index=0;
    while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
        indexFile=1;
        while(fileLotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_intitulePiece')){
            if(fileLotA.value) {
                file = (fileLotA.value).split('\\');
                fileName = file[file.length-1];
                fichiersReponse+= fileName+"|";
                if(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value!="")
                    FSS += document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value +"|";
                if(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value!="")
                    FAPS += document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value +"|";
                if(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value!="")
                    FACE += document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value +"|";
                if(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value!="")
                    SPEC += document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value +"|";
            }
            indexFile++;
        }
        index++;
    }
    //Fin Récupérer tous les fichiers  de la réponse et les fichiers avec signatues Erronées.
    tabRep = fichiersReponse.split('|');
    tabFSS= FSS.split('|');
    for(var i = 0; i < tabFSS.length; i++) {
        if(tabFSS[i]!="" && in_array(tabRep, tabFSS[i])){
            fichiersSansSignatures+= tabFSS[i]+"|";
        }
    }
    tabFAPS= FAPS.split('|');
    for(var i = 0; i < tabFAPS.length; i++) {
        if(tabFAPS[i]!="" && in_array(tabRep, tabFAPS[i])){
            fichiersAvecPlsrsSignatures+= tabFAPS[i]+"|";
        }
    }
    tabFACE= FACE.split('|');
    for(var i = 0; i < tabFACE.length; i++) {
        if(tabFACE[i]!="" && in_array(tabRep, tabFACE[i])){
            fichiersAvecCertificatExpire+= tabFACE[i]+"|";
        }
    }
    // Récupérer les fichiers avec des signatures valides sur le disque.
    tabFEronnes = (fichiersAvecCertificatExpire + fichiersAvecPlsrsSignatures + fichiersSansSignatures).split('|');
    for(var i = 0; i < tabRep.length; i++) {
        if(!in_array(tabFEronnes, tabRep[i])){
            fichierOK+= tabRep[i]+"|";
        }
    }
    if(fichierOK!=""){
        indexSeparateur=fichierOK.indexOf("|");
        while (indexSeparateur >= 0) {
            fichierOK=fichierOK.replace("|","\n\t- ");
            indexSeparateur=fichierOK.indexOf("|");
        }
        indexErreur++;
        message += indexErreur + "- <?php Prado::localize('DEFINE_TEXT_SIGNATURES_VALIDES_DES_FICHIERS_TRANSMIS') ?>\n\t- "+fichierOK.substr(0,(fichierOK.length-3))+"\n";
    }
    if(fichiersSansSignatures!=""){
        indexSeparateur=fichiersSansSignatures.indexOf("|");
        while (indexSeparateur >= 0) {
            fichiersSansSignatures=fichiersSansSignatures.replace("|","\n\t- ");
            indexSeparateur=fichiersSansSignatures.indexOf("|");
        }
        indexErreur++;
        message += indexErreur + "- <?php Prado::localize('DEFINE_INVALIDES_SIGNATURES') ?>\n\t- " + fichiersSansSignatures.substr(0,(fichiersSansSignatures.length-3))+"\n";
    }
    if(fichiersAvecPlsrsSignatures!=""){
        indexSeparateur=fichiersAvecPlsrsSignatures.indexOf("|");
        while (indexSeparateur >= 0) {
            fichiersAvecPlsrsSignatures=fichiersAvecPlsrsSignatures.replace("|","\n\t- ");
            indexSeparateur=fichiersAvecPlsrsSignatures.indexOf("|");
        }
        indexSeparateur1=SPEC.indexOf("|");
        while (indexSeparateur1 >= 0) {
            SPEC=SPEC.replace("|","\n\t- ");
            indexSeparateur1=SPEC.indexOf("|");
        }
        indexErreur++;
        message += indexErreur + "- <?php Prado::localize('DEFINE_PLUSIEURS_SIGNATURES_REALISEES') ?>\n\t- "+fichiersAvecPlsrsSignatures.substr(0,(fichiersAvecPlsrsSignatures.length-3)) + "   <?php Prado::localize('DEFINE_CONSERVER_SIGNATURES_A_TRANSMETTRE') ?>\n\t- "+SPEC.substr(0,(SPEC.length-3)) + "\n   <?php Prado::localize('MESSAGE_CONSERVER_SIGNATURE_A_TRANSMETTRE') ?>\n\n";
    }
    if(fichiersAvecCertificatExpire!=""){
        indexSeparateur=fichiersAvecCertificatExpire.indexOf("|");
        while (indexSeparateur >= 0) {
            fichiersAvecCertificatExpire=fichiersAvecCertificatExpire.replace("|","\n\t- ");
            indexSeparateur=fichiersAvecCertificatExpire.indexOf("|");
        }
        indexErreur++;
        message += indexErreur + "- <?php Prado::localize('DEFINE_CERTIFICAT_EXPIRE_UTILISE_AVEC_FICHIERS_TRANSMIS') ?>\n\t- "+fichiersAvecCertificatExpire.substr(0,(fichiersAvecCertificatExpire.length-3))+"\n";
    }

    if(message!=""){
        document.getElementById('erreurSignature').value = message;
        if(!confirm("<?php Prado::localize('ATTENTION') ?>,\n" + message)){
            document.getElementById('ctl0_CONTENU_PAGE_sendResponseRefresh').style.display="";
            document.getElementById('ctl0_CONTENU_PAGE_sendResponse').style.display='none';
            return false;
        }else{
            return true;
        }
    }
    else
        return true;
}
function in_array(array, p_val) {
    for(var i = 0, l = array.length; i < l; i++) {
        if(array[i] == p_val) {
            rowid = i;
            return true;
        }
    }
    return false;
}

function refreshRepeaterListePPs()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_tableauPP_refreshRepeater').click();
    window.close();
}

function invalideSignatureDraw(){
    var result = new Array("","");
    result[0] += "<div class='signataire-line'>-</div>";
    result[1] += "<div class='statut-signature-col'><img title='<?php Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE') ?>' alt='<?php Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE') ?>' src='themes/images/picto-signature.gif'/>: ";
    result[1] += "<img title='<?php Prado::localize('DEFINE_SIGNATURE_INVALIDE_DETAIL_PLI') ?>' alt='<?php Prado::localize('DEFINE_SIGNATURE_INVALIDE_DETAIL_PLI') ?>' src='themes/images/picto-check-not-ok.gif'/></div>";
    result[1] += "<div class='lien-signature-col'></div><div class='lien-signature-col'></div><div class='breaker'></div>";
    return result;
}

function spansAtLeastNCharacterSets( word, N)
{
    // Calcul les différents types de caractères du mot de passe
    // word : mot de passe, N : Nombre minimun de types de caractére différents pour retour à vrai
    if (word == null)
        return false;

    var csets = new Array(false,false,false,false);

    ncs = 0;
    var listeNombre = "0123456789";
    var listeCaractereSpe = "&^'(-_)=*!:;,?./-+<>$%{}@\|[]~]#"+'"';
    for (i = 0; i < word.length; i++)
    {
        c= word.charAt(i);
        if (listeNombre.indexOf(c)>=0)
        {
            // caractére numérique
            if (csets[0] == false)
            {
                csets[0] = true;
                ncs++;
                if (ncs >= N)
                    return true;
            }
        }
        else if (listeCaractereSpe.indexOf(c)>=0)
        {
            // caractére spécial
            if (csets[1] == false)
            {
                csets[1] = true;
                ncs++;
                if (ncs >= N)
                    return true;
            }
        }
        else if (c.toUpperCase() ==c)
        {
            // caractére en Majuscule
            if (!csets[2])
            {
                csets[2] = true;
                ncs++;
                if (ncs >= N)
                    return true;
            }
            continue;
        }
        else if (c.toLowerCase() ==c)
        {
            // caractére en Minuscule
            if (!csets[3])
            {
                csets[3] = true;
                ncs++;
                if (ncs >= N)
                    return true;
            }
        }
    }
    return false;
}

function verifierMdp(sender){
    //optimisation =>utiliser le sender id au lieu de l'id de l element sur chaque page
    //a tester pour les autres et enlever le teste avec l'id en dur
    if(document.getElementById(sender.control.id)){
        var pwd = document.getElementById(sender.control.id).value;
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_pwdAutomatique')!=null) {
        if(document.getElementById("ctl0_CONTENU_PAGE_pwdAutomatique").checked == true){
            return true;
        }
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_passwordAgent')!=null) {
        var pwd = document.getElementById('ctl0_CONTENU_PAGE_passwordAgent').value;
    } else if(document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_password')){
        var pwd = document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_password').value;
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_password')){
        var pwd = document.getElementById('ctl0_CONTENU_PAGE_password').value;
    }
    if(pwd != 'xxxxxxx'){
        if(spansAtLeastNCharacterSets(pwd, 3) && pwd.length >= 8 ){
            return true;
        }
        return false;
    }
    return true;
}

function DisableValidatorPwd(ElementName){
    if(document.getElementById(ElementName)!=null) {
        if(document.getElementById(ElementName).checked == true){
            return false;
        }
    }
    return true;
}
function checkAdresse(){
    if(opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreRetrait').value == '1'){
        document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreRetraits').checked = 'true';
    }

    if(opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreQuestion').value == '1'){
        document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreQuestions').checked = 'true';
    }

    if(opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreDepot').value == '1'){
        document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreDepots').checked = 'true';
    }

    if(opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseLibre').value == '1'){
        document.getElementById('ctl0_CONTENU_PAGE_checkAdressesLibres').checked = 'true';
    }
}

function remplirHiddenCheckedAdresse(){
    if(document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreRetraits').checked){
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreRetrait').value ='1';
    }else{
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreRetrait').value ='0';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreQuestions').checked){
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreQuestion').value ='1';
    }else{
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreQuestion').value ='0';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_checkAdressesRegistreDepots').checked){
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreDepot').value ='1';
    }else{
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseRegistreDepot').value ='0';
    }

    if(document.getElementById('ctl0_CONTENU_PAGE_checkAdressesLibres').checked){
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseLibre').value ='1';
    }else{
        opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adresseLibre').value ='0';
    }

}

function validerValeurDepartCommune(sender, parameter)
{
    var idValeurInitialeCommune = sender.control.id;
    var elem = idValeurInitialeCommune.split('_');
    var idValeurInitialeCommuneValeur = elem[0]+'_'+elem[1]+'_'+elem[2]+'_'+elem[3]+'_'+elem[4]+'_'+'valeurInitialeCommuneValeur';
    if(document.getElementById(idValeurInitialeCommune).checked == true
        && !document.getElementById(idValeurInitialeCommuneValeur).value) {
        return false;
    }else{
        return true;
    }
}
function infoSignataireEmisPar(input1,input2)
{
    var span = document.getElementById(input1);
    var inputText=opener.document.getElementById(input2).value;

    l=inputText.indexOf(',');
    while (l >= 0) {
        inputText=inputText.replace(',','<br />');
        l=inputText.indexOf(',');
    }
    span.innerHTML = inputText;
}
//Vrification Signature
function setUrlVerifyCertificate(UrlVerifyCertificate){
    if(UrlVerifyCertificate)
    {
        document.AppletSignaturesInfos.setParam3(UrlVerifyCertificate);
    }
}
function ErreurActeDEngagementNonRenseigne()
{
    var errorMsg = "";
    var lots="";
    var aeExists=false;
    var j = 0;
    while(document.getElementById("ctl0_CONTENU_PAGE_RepeaterLot_ctl"+j+"_fichierAE")){
        if(document.getElementById("ctl0_CONTENU_PAGE_RepeaterLot_ctl"+j+"_fichierAE").value==""){
            lots += document.getElementById("ctl0_CONTENU_PAGE_RepeaterLot_ctl"+j+"_numLot").value + ", ";
        }
        j++;
        aeExists=true;
    }
    if(aeExists && lots!=""){
        if(i!=1 ){
            errorMsg ="- <?php Prado::localize('MESSAGE_ERREUR_DEPOT_ACTE_DENGAGEMENT_LOTS_OFFRE') ?>" + lots.substr(0,(lots.length-2)) +  " <?php Prado::localize('MESSAGE_DE_CETTE_OFFRE') ?>";
        }else{
            errorMsg = "- <?php Prado::localize('MESSAGE_ERREUR_DEPOT_ACTE_DENGAGEMENT_OFFRE') ?>";
        }
        return errorMsg;
    }else{
        return '0';
    }
}

function verifierAjoutCertificat(){
    panel = document.getElementById('layer_certificat');
    if(panel && panel.style.display != 'block'){
        bool = false;
    }else{
        bool = true;
    }

    file = document.getElementById('ctl0_CONTENU_PAGE_authentificationByCer_certificat').value;
    if(file == "" && bool){
        return false;
    }
    return true;
}

/*
     * Verifie que dateDebut est inferieur à dateFin
     */
function ValidateDifferenceDeuxDates(dateDebut, dateFin)
{
    if(dateDebut!="" && dateFin!="") {
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>=fin){
            return false;
        }
    }
    return true;
}

function confirmerDonneesFicheRecensement(){

    var mapaInf = document.getElementById('ctl0_CONTENU_PAGE_isMapa').value;

    var message = "";
    message += "Attention : certaines données sont absentes ou erronnées: \n ";

    var siret = true;
    var dateNotification = true;
    var dateFinMarche = true;
    var natureActeJuridique = true;
    var typeProcedure = true;
    var FormePrix = true;
    var codeCpv = true;
    var nbrEntrepriseCoTraitant = true;
    var sousTraitanceDeclaree = true;
    var carteAchat = true;
    var clauseSociale = true;
    var clauseEnvironnementale = true;
    var nbrPropositionsRecues = true;
    var nbrPropositionsDematerialisees = true;

    if(!document.getElementById('ctl0_CONTENU_PAGE_siren').value || !document.getElementById('ctl0_CONTENU_PAGE_siret').value ) {
        message += "- N° SIRET de l'acheteur \n ";
        siret = false;
    }
    if(!document.getElementById('ctl0_CONTENU_PAGE_dateNotification').value) {
        message += "- Date de notification \n ";
        dateNotification = false;
    }
    if(!document.getElementById('ctl0_CONTENU_PAGE_finMarche').value
        || !ValidateDifferenceDeuxDates(document.getElementById('ctl0_CONTENU_PAGE_dateNotification').value, document.getElementById('ctl0_CONTENU_PAGE_finMarche').value)) {
        message += "- Date fin du marché \n ";
        dateFinMarche = false;
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_natureActe').value == 0) {
        message += "- Nature de l'acte juridique \n ";
        natureActeJuridique = false;
    }
    if(mapaInf == 0 && !document.getElementById('ctl0_CONTENU_PAGE_atexoRefCpv_codeRefPrinc').value) {
        message += "- Codes CPV \n ";
        codeCpv = false;
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_procedureType').value == 0) {
        message += "- Procédure \n ";
        typeProcedure = false;
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_formeprix').value == 0) {
        message += "- Forme de prix \n ";
        FormePrix = false;
    }
    if(mapaInf == 0 && ((!document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').value ) || (document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').value).length != 2)) {
        message += "- Nombre entreprises co-traitantes \n ";
        nbrEntrepriseCoTraitant = false;
    }
    if(mapaInf == 0 && document.getElementById('ctl0_CONTENU_PAGE_ssTraitanceDeclaree').value == 2) {
        message += "- Sous-traitance declarée à la passation du marché \n ";
        sousTraitanceDeclaree = false;
    }
    if(mapaInf == 0 && document.getElementById('ctl0_CONTENU_PAGE_carteAchat').value == 2) {
        message += "- Le marché permet-il l'utilisation de la carte d'achat ? \n ";
        carteAchat = false;
    }
    if(mapaInf == 0 && document.getElementById('ctl0_CONTENU_PAGE_clauseSociale').value == 2) {
        message += "- Le contrat met-il en oeuvre une clause sociale ? \n ";
        clauseSociale = false;
    }
    if(mapaInf == 0 && document.getElementById('ctl0_CONTENU_PAGE_clauseEnvironnementale').value == 2) {
        message += "- Le contrat met-il en oeuvre une clause environnementale ? \n ";
        clauseEnvironnementale = false;
    }
    if(mapaInf == 0 && !document.getElementById('ctl0_CONTENU_PAGE_nbPropositionsRecues').value) {
        message += "- Nombre proposition reçues \n ";
        nbrPropositionsRecues = false;
    }
    if(mapaInf == 0 && !document.getElementById('ctl0_CONTENU_PAGE_nbPropositionsDemat').value) {
        message += "- Nombre proposition dématerialisées \n ";
        nbrPropositionsDematerialisees = false;
    }

    message += "Confirmez-vous néanmoins la transmission de cet envoi ?";

    if(!dateNotification || !siret || !dateFinMarche || !natureActeJuridique || !typeProcedure || !FormePrix || !nbrEntrepriseCoTraitant || !sousTraitanceDeclaree || !carteAchat
        || !clauseSociale || !clauseEnvironnementale || !nbrPropositionsRecues || !nbrPropositionsDematerialisees || !codeCpv) {
        if(!confirm(message)){
            return false;
        }else{
            //On masque les deux boutons d'enregistrement et de transmission après le clic sur l'un des 2.
            hideButtonsSaveTransmission();
            return true;
        }
    }
}

/*
* Permet de cocher et decocher un ensemble de fichiers ou dossiers dependant du dossier parent
*/
function CheckUnCheckFileItems(id_element, max_index_element, file_item_html)
{
    var thisElement = document.getElementById(id_element);
    var thisVal = thisElement.value;

    // traitement des cases filles de la case this
    if (thisVal != "")
    {
        for(j=1;j<=max_index_element;j++)
        {
            var curElement = document.getElementById(file_item_html+"_" + j);

            var curVal = curElement.value;

            if (curVal.search(thisVal+"/")>=0)
            {
                if (thisElement.checked)
                    curElement.checked=true;
                else
                    curElement.checked=false;
            }

        }
    }

    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        if (thisVal != "")
        {
            for(j=0;j<=max_index_element;j++)
            {
                var curElement = document.getElementById(file_item_html+"_" + j);
                var curElement2 = document.getElementById(file_item_html+"_x_" + j);

                if(curElement2 != null) {
                    var curVal2 = curElement2.value;
                    if (curVal2)
                    {
                        if (thisVal!=curVal2 && thisVal.search(curVal2)>=0 && curVal2.charAt(curVal2.length -1) != "/") // si c un répértoire
                        {
                            curElement2.checked=false;
                        }
                    }
                }
                if(curElement != null) {
                    var curVal = curElement.value;
                    if (curVal)
                    {
                        if (thisVal!=curVal && thisVal.search(curVal)>=0 && curVal.charAt(curVal.length -1) != "/") // si c un répértoire
                        {
                            curElement.checked=false;
                        }
                    }
                }
            }
        }
    }
}
/*
* Affiche un message d'erreur avec la liste des extensions autorisées pour l'envoie des documents à Chorus
*/
function zipInvalideExtensionToChorus(type,bool, listeExtensionsAutorisees)
{
    if(type=='folder' && bool == '1' ) {
        bootbox.alert("<?php Prado::localize('FICHIER_NON_TRANSMISSIBLE_CAR') ?> <?php Prado::localize('FORMAT_FICHIER_N_EST_PAS_TRAITE_PAR_CHORUS') ?> "+listeExtensionsAutorisees+" ou <?php Prado::localize('LE_POIDS_DEPASSE_LE_MAXIMUM') ?>");
        return false;
    }
}
/*
* Masque les deux boutons d'enregistrement et de transmission
*/
function hideButtonsSaveTransmission()
{
    document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='none';
    document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='none';
}

/*
* Masque le bouton d'ajout d'un envoi Chorus
*/
function hideButtonsAjouterEnvoiChorus()
{
    document.getElementById('ctl0_CONTENU_PAGE_buttonAjoutEchangeChorus').style.display='none';
}

function launchAppletActeDEngagement(id_inputAE, id_inputXMl, stringXml, urlDownloadAE, num_lot) {
    document.AppletActeDEngagement.setIdInputAE(id_inputAE);
    document.AppletActeDEngagement.setIdInputXml(id_inputXMl);
    document.AppletActeDEngagement.setXmlString(document.getElementById(stringXml).value);
    document.AppletActeDEngagement.setUrlPageDownloadAE(urlDownloadAE);
    document.AppletActeDEngagement.setNumLot(num_lot);
    document.AppletActeDEngagement.setUrlCondUsr(document.getElementById('urlCondUsr').value);
    document.AppletActeDEngagement.setLaunchAppletAE(true);
}

function updateAeInputs(idInputXml, idInputAE, pathXmlAeUsr, pathPdfAeUsr) {
    var suffix = idInputXml.substring(0,(idInputXml.length)-5);
    document.getElementById(suffix+'divXmlAe').style.display='block'
    document.getElementById(idInputXml).value = pathXmlAeUsr;
    document.getElementById(idInputAE).value = pathPdfAeUsr;
}

function formAeValidation(){
    var doc = window.opener.document;
    var result="";
    var idAEXmlCalledValue = doc.getElementById('idAEXmlCalled').value;

    xmlDoc = loadXMLString(doc.getElementById(idAEXmlCalledValue).value) ;

    if(xmlDoc){

        var ppp=xmlDoc.getElementsByTagName("element");
        for(var i = 0; i < ppp.length; i++) {
            if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ")){
                if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ").value=="" && ppp[i].getAttribute("obligatoire")=='true' && !ppp[i].getAttribute("desactive")){
                    return false;
                }
            }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ")){
                if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ").value==""  && ppp[i].getAttribute("obligatoire")=='true' && !ppp[i].getAttribute("desactive")){
                    return false;
                }
            }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ")){
                if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ").value=="" && ppp[i].getAttribute("obligatoire")=='true' && !ppp[i].getAttribute("desactive")){
                    return false;
                }
            }
        }return true;
    }
}

function readFormAE(){
    if(formAeValidation()== true){
        var doc = window.opener.document;
        var result="";
        var idAEXmlCalledValue = doc.getElementById('idAEXmlCalled').value;

        xmlDoc = loadXMLString(doc.getElementById(idAEXmlCalledValue).value) ;

        if(xmlDoc){

            var ppp=xmlDoc.getElementsByTagName("element");
            for(var i = 0; i < ppp.length; i++) {
                if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ")){
                    ppp[i].setAttribute('valeur',utf8_encode(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ").value));
                }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ")){
                    ppp[i].setAttribute('valeur',arrondirEspace(utf8_encode(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ").value),2,' ',''));
                }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ")){
                    ppp[i].setAttribute('valeur',utf8_encode(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ").value));
                }
                var ssElts = xmlDoc.getElementsByTagName("sousElement");
                var j=0;
                for(var s=0; s < ssElts.length; s++){
                    if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j)){
                        if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j).value == ssElts[s].getAttribute("valeur")){
                            ssElts[s].setAttribute('choisi',document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j).checked);
                        }
                    }j++;
                }
            }
            //var serializer = new XMLSerializer();
            //var xml = serializer.serializeToString(xmlDoc);
            if (typeof window.XMLSerializer != "undefined") {
                var xml = (new window.XMLSerializer()).serializeToString(xmlDoc);
            } else if (typeof xmlDoc.xml != "undefined") {
                var xml = xmlDoc.xml;
            }
            doc.getElementById(idAEXmlCalledValue).value = xml;
            var suffix = idAEXmlCalledValue.substring(0,(idAEXmlCalledValue.length)-8);
            doc.getElementById(suffix+'launchAppletAEButton').click();
            window.close();
        }
    }
}

function preRemplirFormAE(){
    var doc = window.opener.document;
    var idAEXmlCalledValue = doc.getElementById('idAEXmlCalled').value;
    xmlDoc = loadXMLString(doc.getElementById(idAEXmlCalledValue).value) ;

    if(xmlDoc){
        var ppp=xmlDoc.getElementsByTagName("element");
        for(var i = 0; i < ppp.length; i++) {
            if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_label")){
                if(ppp[i].getAttribute("valeur")!=''){
                    document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_label").innerHTML = toPfEncoding((ppp[i].getAttribute("valeur")));
                } else {
                    //document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_label").innerHTML = '-';
                    var parent = (document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_label")).parentNode;
                    parent.style.display='None';
                }
            }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ")){
                document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_champ").value = toPfEncoding((ppp[i].getAttribute("valeur")));
            }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ")){
                document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ").value = toPfEncoding((ppp[i].getAttribute("valeur")));
            }if(ppp[i].getAttribute("id")!='montantOffre' && document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ") && doc.getElementById("variantesAutorisees").value!='1'){
                var parent = (document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Montant_champ")).parentNode;
                parent = parent.parentNode;
                parent = parent.parentNode;
                parent.style.display='None';
            }if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ")){
                document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_Email_champ").value = toPfEncoding((ppp[i].getAttribute("valeur")));
            }
            var ssElts = xmlDoc.getElementsByTagName("sousElement");
            var j=0;
            for(var s=0; s < ssElts.length; s++){
                if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j)){
                    if(document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j).value == ssElts[s].getAttribute("valeur")){
                        if(ssElts[s].getAttribute("choisi")== 'false'){
                            document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j).checked = false;
                        } else {
                            document.getElementById("ctl0_CONTENU_PAGE_" + ppp[i].getAttribute("id") + "_checkboxlist_c"+j).checked = true;
                        }
                    }
                }j++;
            }
        }
    }
}

function loadXMLString(txt)
{
    if (window.DOMParser)
    {
        parser=new DOMParser();
        xmlDoc=parser.parseFromString(txt,"text/xml");
    }
    else // Internet Explorer
    {
        xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async="false";
        xmlDoc.loadXML(txt);
    }
    return xmlDoc;
}

/**
 * Fonction de validation de fichiers joints autres annonces
 */
function isDocumentAttacheAutresAnnonces()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_ajoutFichier").value){
        return false;
    }else{
        return true;
    }
}


function dateAnterieurAujourdhui(sender, parameter)
{
    radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
    dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigne');
    dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');
    radio = true;
    if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
        radio = false;
    }
    if(parameter != ""){
        if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
            if(frnDateToIso(parameter) < frnDateToIso(dateMiseEnligne.value)){
                return false;
            }
        }else if(dateAujourdhui && dateAujourdhui.value != ""){
            if(frnDateToIso(parameter) < frnDateToIso(dateAujourdhui.value)){
                return false;
            }
        }
    }
    return true;
}

function dateInferieureDateLimite(sender, parameter)
{
    dateLimite = document.getElementById('ctl0_CONTENU_PAGE_dateRemisePlis');
    if(dateLimite && dateLimite.value != "" && parameter != ""){
        if(frnDateToIso(parameter) > frnDateToIso(dateLimite.value)){
            return false;
        }
    }
    return true;
}

function validateDateVisitesLot()
{
    if(isConsultationAlloti())
    {
        i = 0;
        var datelimite = document.getElementById("ctl0_CONTENU_PAGE_dateRemisePlis");
        var visiteCheck = document.getElementById("ctl0_CONTENU_PAGE_visite");
        if (visiteCheck.checked==true)
        {
            while (numLot = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl" + i + "_numLot"))
            {
                j=1;
                visiteLotOui = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl" + i + "_visiteLotOui");
                if(visiteLotOui.checked){
                    while (dateVisiteLot = document.getElementById("ctl0_CONTENU_PAGE_visiteLotRepeater_ctl"+i+"_visiteRepeater_ctl"+j+"_dateValuefr"))
                    {
                        if(frnDateToIso(dateVisiteLot.value) > frnDateToIso(datelimite.value)){
                            document.getElementById('spanErrorVisite').style.display='block';
                            return false;
                        }
                        j++;
                    }
                }
                i++;
            }
        }
    }

    return true;
}
function refreshRepeaterListePPs()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_tableauPP_refreshRepeater').click();
    window.close();
}

function validateDateVisites(sender, parameter)
{
    radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
    dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigne');
    dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');
    visiteLieu = document.getElementById(parameter+'_visiteLotOui');
    if(visiteLieu && visiteLieu.checked){
        radio = true;
        if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
            radio = false;
        }
        j=1;
        while (dateVisite = document.getElementById(parameter+"_visiteRepeater_ctl"+j+"_dateValue"))
        {
            if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
                if(dateVisite.value < frnDateToIso(dateMiseEnligne.value)){
                    return false;
                }
            }else if(dateAujourdhui && dateAujourdhui.value != ""){
                if(dateVisite.value < frnDateToIso(dateAujourdhui.value)){
                    return false;
                }
            }
            j++;
        }
    }
    return true;
}
function validateEmailForInscrit(){

    email = document.getElementById('ctl0_CONTENU_PAGE_PanelInscrit_emailPersonnel').value;
    if( email != ''){
        var emailPattern =  /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return emailPattern.test(email);
    }
}

function jsVersionJavaVerify(param){
    document.getElementById('divMessageErrorVersionJava').style.display='block';
    return false;
}


function validateLotReunionDateComapareAujordhui()
{
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
        dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigne');
        dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');

        radio = true;
        if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
            radio = false;
        }
        while(dateReunion = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_dateReunion"))
        {
            reunionOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_reunionOui").checked;
            dateReunion = dateReunion.value;
            if(reunionOui){
                if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
                    if(frnDateToIso(dateReunion) < frnDateToIso(dateMiseEnligne.value)){
                        document.getElementById("reunionOui" + i).style.display = '';
                        res = false;
                    }
                }else if(dateAujourdhui && dateAujourdhui.value != ""){
                    if(frnDateToIso(dateReunion) < frnDateToIso(dateAujourdhui.value)){
                        document.getElementById("reunionOui" + i).style.display = '';
                        res = false;
                    }
                }
            }

            i++;
        }
    }
    return res;
}
function validateLotEchantillonsDateComapareAujourdhui()
{
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
        dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigne');
        dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');

        radio = true;
        if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
            radio = false;
        }
        while(dateLimiteEchantillion = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_dateLimiteEchantillion"))
        {
            dateLimiteEchantillion = dateLimiteEchantillion.value;
            echantillonOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_echantillonOui").checked;
            if(echantillonOui){
                if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
                    if(frnDateToIso(dateLimiteEchantillion) < frnDateToIso(dateMiseEnligne.value)){
                        document.getElementById("echantillonError" + i).style.display = '';
                        res = false;
                    }
                }else if(dateAujourdhui && dateAujourdhui.value != ""){
                    if(frnDateToIso(dateLimiteEchantillion) < frnDateToIso(dateAujourdhui.value)){
                        document.getElementById("echantillonError" + i).style.display = '';
                        res = false;
                    }
                }
            }
            i++;
        }
    }
    return res;
}

function dateVisiteLieuxLotCompareAujourdhui()
{
    var res = true;
    if(isConsultationAlloti())
    {
        i = 1;
        radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
        dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigne');
        dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');

        radio = true;
        if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
            radio = false;
        }
        while(visiteLotOui = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_visiteLotOui"))
        {
            if(visiteLotOui.checked){
                j=1;
                while(dateValue = document.getElementById("ctl0_CONTENU_PAGE_lotsRepeater_ctl" + i + "_visiteRepeater_ctl" + j + "_dateValue"))
                {
                    date = dateValue.value;
                    dateValueF = date.substring(0,16);
                    if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
                        if(dateValueF < frnDateToIso(dateMiseEnligne.value)){
                            document.getElementById("visiteLotOui"+i).style.display = '';
                            res = false;
                        }
                    }else if(dateAujourdhui && dateAujourdhui.value != ""){
                        if(dateValueF < frnDateToIso(dateAujourdhui.value)){
                            document.getElementById("visiteLotOui"+i).style.display = '';
                            res = false;
                        }
                    }

                    j++;
                }
            }
            i++;
        }
    }
    return res;

}
/*
*
* Permet d'afficher l'onglet actif
*/
function displayActiveTab(tabValue) {
    if(tabValue!="") {
        showHideLayer('ongletLayer'+tabValue,document.getElementById('onglet'+tabValue),4);
    }
}

function ValidateDateFluxChorusTransversaux()
{
    dateDebut=document.getElementById('ctl0_CONTENU_PAGE_dateDebut').value;
    dateFin=document.getElementById('ctl0_CONTENU_PAGE_dateFin').value;

    if(dateDebut!="" && dateFin!="") {
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }
    return true;
}

function validateListDestinatairePress() {
    var nbreDest = document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_nbreDest').innerHTML;
    var valide = false;
    for (var i = 0; i < nbreDest; i++) {
        if(document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_RepeaterDestinataire_ctl'+i+'_idCheck').checked == true){
            return true;
        }
    }
    if(valide==false) {
        return false;
    } else {
        return true;
    }

}

function validationRadioOuiNon (sender,params)
{
    var idRadio = sender.control.id;
    var elem = idRadio.split('_');
    var otherRadio = elem[0]+'_'+elem[1]+'_'+elem[2]+'_'+elem[3]+'_'+elem[4]+'_'+'itemRadionon';
    if(document.getElementById(idRadio).checked == true || document.getElementById(otherRadio).checked == true) {
        return true;
    } else {
        return false;
    }
}

function onCheckAppliquerParTout(all) {
    var nbreItems = document.getElementById('ctl0_CONTENU_PAGE_nbreItems').value;
    var valeurTvaParTout = document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl0_tauxTva').value;
    if(document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl0_appliquerPartout').checked == true && valeurTvaParTout=='') {
        bootbox.alert("<?php Prado::localize('TEXT_MSG_SAISIE_TVA_APPLIQUER_PAR_TOUT') ?>");
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl0_appliquerPartout').checked == true && valeurTvaParTout!='') {
        for (var i = 1; i <= nbreItems; i++) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemTauxTva').value = valeurTvaParTout;
        }
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl0_appliquerPartout').checked == false) {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl0_tauxTva').value='';
        for (var i = 1; i <= nbreItems; i++) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemTauxTva').value = '';
        }
    }
    calculerAll();
}

function calculerAll() {
    var nbreItems = document.getElementById('ctl0_CONTENU_PAGE_nbreItems').value;
    var totalHt=0;
    var totalTtc=0;
    for (var i = 1; i <= nbreItems; i++) {
        var valuechampsPrix = parseFloat(document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemPrixUnitaireHt').value);
        var valuetauxTva = parseFloat(document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemTauxTva').value);
        var quantiteVal = document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemQuantite').innerHTML;

        if(valuechampsPrix && valuetauxTva) {
            valueTva = parseFloat(1+(valuetauxTva/100));
            document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemPrixUnitaireHtCalcule').innerHTML = arrondirEspace(valuechampsPrix*valueTva,2,' ','');
        }
        if(valuechampsPrix && quantiteVal) {
            tot1 = parseFloat(valuechampsPrix)*parseFloat(quantiteVal);
            if(tot1.toString()!='NaN') {
                totalHt+=tot1;
                document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemPrixTotalHt').innerHTML = arrondirEspace(tot1,2,' ','');
            }
        }
        if(valuechampsPrix && quantiteVal && valuetauxTva) {
            tot2 = parseFloat(valuechampsPrix)*parseFloat(quantiteVal)*parseFloat(1+(valuetauxTva)/100);
            if(tot2.toString()!='NaN' && tot2.toString()!='NaN,NaN') {
                totalTtc+=tot2;
                document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+i+'_itemPrixTotalTtc').innerHTML = arrondirEspace(tot2,2,' ','');
            }
        }
    }
    indexTot = parseFloat(nbreItems)+1;
    if(totalHt.toString()!='NaN,NaN') {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+indexTot+'_htTotal').innerHTML= arrondirEspace(totalHt,2,' ','');
        document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+indexTot+'_htTotalHidden').value= arrondirEspace(totalHt,2,' ','');
    }
    if(totalTtc.toString()!='NaN,NaN') {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterBordereauPrixItems_ctl'+indexTot+'_ttcTotal').innerHTML= arrondirEspace(totalTtc,2,' ','');
    }
}

// formate un chiffre avec 'decimal' chiffres après la virgule et un separateur
function arrondirEspace(valeur,decimal,separateur,reverse) {
    var nbreArrondit = valeur.toString();
    nbreArrondit = nbreArrondit.replace(',','.');
    nbreArrondit = nbreArrondit.replace(' ','');
    //nbreArrondit = arrondir(nbreArrondit,2); parseFloat(

    var deci=Math.round( Math.pow(10,decimal)*(Math.abs(nbreArrondit)-Math.floor(Math.abs(nbreArrondit)))) ;
    var val=Math.floor(Math.abs(nbreArrondit));

    if ((decimal==0)||(deci==Math.pow(10,decimal))) {
        val=Math.floor(Math.abs(nbreArrondit)); deci=0;
    }
    var val_format=val+"";
    var nb=val_format.length;

    for (var i=1;i<4;i++) {
        if (val>=Math.pow(10,(3*i))) {
            val_format=val_format.substring(0,nb-(3*i))+separateur+val_format.substring(nb-(3*i));
        }
    }
    if (decimal>0) {
        var decim="";
        for (var j=0;j<(decimal-deci.toString().length);j++) {decim+="0";}
        deci=decim+deci.toString();
        val_format=val_format+"."+deci;
    }

    if (parseFloat(nbreArrondit)<0) {
        val_format="-"+val_format;
    }

    if(reverse == 0)
    {
        // val_format = val_format.replace('.',',');
        return val_format;
    }
    else
    {
        return val_format.replace(' ','');
    }
}
function ValidateItemFormulaireBordoreauPrix()
{
    res = true;
    i = 1;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_saveStatut"))
    {
        if (itemStatut.value == 0) {
            prixUnitaire = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_prixUnitaireHt");
            quantite = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_quantite");
            if (!prixUnitaire.checked && !quantite.value) {
                document.getElementById("errorQuantite" + i).style.display = '';
                res = false;
            }

        }
        i++;
    }
    return res;
}

function ValidateItemFormulaireQuestionnaire()
{
    res = true;
    i = 1;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_saveStatut"))
    {
        if (itemStatut.value == 0) {
            typeItem = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_questionType");
            if (typeItem.options[typeItem.selectedIndex].value == 0){
                document.getElementById("errorTypeQuestion" + i).style.display = '';
                res = false;
            }
        }
        i++;
    }
    return res;
}

function ValidateItemFormulaireDescription()
{
    res = true;
    i = 1;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_saveStatut"))
    {
        if (itemStatut.value == 0) {
            description = document.getElementById("ctl0_CONTENU_PAGE_parametrageFormulaire_ItemRepeater_ctl" + i + "_description");
            if (!description.value) {
                document.getElementById("errorDescription" + i).style.display = '';
                res = false;
            }
        }
        i++;
    }
    return res;
}

function calculTotauxCriteresEval() {
    var total=0;
    var totalCP=0;
    var totalCC=0;

    valTotTauxTva = document.getElementById('ctl0_CONTENU_PAGE_totalTauxTva').value;
    if(valTotTauxTva) {
        totalCP+= parseFloat(valTotTauxTva);
    }
    var i = 1;
    while (cpaTauxTva = document.getElementById('ctl0_CONTENU_PAGE_autreCriterePrix_ctl'+i+'_cpaTauxTva')) {
        valCpaTauxTva = cpaTauxTva.value;
        if(valCpaTauxTva) {
            totalCP+= parseFloat(valCpaTauxTva);
        }
        i++;
    }
    var j = 1;
    while (ccaTauxTva = document.getElementById('ctl0_CONTENU_PAGE_autreCritereComplementaire_ctl'+j+'_ccaTauxTva')){
        valCcaTauxTva = ccaTauxTva.value;
        if(valCcaTauxTva) {
            totalCC+= parseFloat(valCcaTauxTva);
        }
        j++;
    }

    document.getElementById('ctl0_CONTENU_PAGE_totalCP').innerHTML = arrondirEspace(totalCP,2,' ','');
    document.getElementById('ctl0_CONTENU_PAGE_totalCC').innerHTML = arrondirEspace(totalCC,2,' ','');
    document.getElementById('ctl0_CONTENU_PAGE_total').innerHTML = arrondirEspace(totalCP + totalCC,2,' ','');
    document.getElementById('ctl0_CONTENU_PAGE_totalhidden').value = arrondirEspace(totalCP + totalCC,2,' ','');
}

function calculTotauxCriteresEvalOffre() {
    var total=0;
    var totalCP=0;
    var totalCC=0;
    var totalPonderationCC=0;
    var totalPonderationCP=0;
    var valnoteCp1Calcule;
    var valcpaNoteCalcule;
    var valccaNoteCalcule;

    if(document.getElementById('ctl0_CONTENU_PAGE_noteCp1')) {
        valnoteCp1 = document.getElementById('ctl0_CONTENU_PAGE_noteCp1').value;
        valponderationCritere = document.getElementById('ctl0_CONTENU_PAGE_ponderationCritere').innerHTML;
        totalPonderationCP+=parseFloat(valponderationCritere);
        if(valnoteCp1 && valponderationCritere) {
            valnoteCp1Calcule = parseFloat(valnoteCp1/100*valponderationCritere);
            document.getElementById('ctl0_CONTENU_PAGE_noteCp1Calcule').innerHTML = arrondirEspace(valnoteCp1Calcule,2,' ','');
        }
        if(valnoteCp1Calcule) {
            totalCP+= valnoteCp1Calcule;
        }
    }
    var nbrElmtautreCP = document.getElementById('ctl0_CONTENU_PAGE_nbreItemsRepeaterAutresCriteres').value;
    var nbrElmtautreCC = document.getElementById('ctl0_CONTENU_PAGE_nbreItemsRepeaterAutresCriteresComplementaires').value;
    var i = 0;
    while (cpaNote = document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteres_ctl'+i+'_cpaNote')){
        valCpaNote = cpaNote.value;
        valponderationCritere = document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteres_ctl'+i+'_ponderationCritere').innerHTML;
        totalPonderationCP+=parseFloat(valponderationCritere);
        if(valponderationCritere && valCpaNote) {
            valcpaNoteCalcule = parseFloat(valCpaNote/100*valponderationCritere);
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteres_ctl'+i+'_cpaNoteCalcule').innerHTML = arrondirEspace(valcpaNoteCalcule,2,' ','');;
            totalCP+= valcpaNoteCalcule;
        }
        i++;
    }
    var i = 0;
    while (ccaNote = document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteresComplementaires_ctl'+i+'_ccaNote')){
        valccaNote = ccaNote.value;
        valponderationCritere = document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteresComplementaires_ctl'+i+'_ponderationCritere').innerHTML;
        totalPonderationCC+=parseFloat(valponderationCritere);
        if(valccaNote && valponderationCritere) {
            valccaNoteCalcule= parseFloat(valccaNote/100*valponderationCritere);
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAutresCriteresComplementaires_ctl'+i+'_ccaNoteCalcule').innerHTML = arrondirEspace(valccaNoteCalcule,2,' ','');
            totalCC+= valccaNoteCalcule;
        }
        i++;
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalCComp')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalCComp').innerHTML = arrondirEspace(totalCC,2,' ','');
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalAC')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalAC').innerHTML = arrondirEspace(totalCP,2,' ','');
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalCriteres')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalCriteres').innerHTML = arrondirEspace(totalCP + totalCC,2,' ','');
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalCriteresHidden')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalCriteresHidden').value = arrondirEspace(totalCP + totalCC,2,' ','');
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalPonderationCC')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalPonderationCC').innerHTML = arrondirEspace(totalPonderationCC,2,' ','');
    }
    if (document.getElementById('ctl0_CONTENU_PAGE_totalPonderationCP')) {
        document.getElementById('ctl0_CONTENU_PAGE_totalPonderationCP').innerHTML = arrondirEspace(totalPonderationCP,2,' ','');
    }

}

function validateDescriptionCPA()
{
    res = true;
    i = 0;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_autreCriterePrix_ctl" + i + "_saveStatutcpa"))
    {
        if (itemStatut.value == 0) {
            libelleItem = document.getElementById("ctl0_CONTENU_PAGE_autreCriterePrix_ctl"+ i + "_cpaLibelle").value;
            if (libelleItem == ""){
                res = false;
                document.getElementById("errorLibelleCpa" + i).style.display = '';
            }else{
                document.getElementById("errorLibelleCpa" + i).style.display = 'none';
            }
        }
        i++;
    }
    return res;
}

function validatePonderationCCA()
{
    res = true;
    i = 0;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_autreCritereComplementaire_ctl" + i + "_saveStatutcca"))
    {
        if (itemStatut.value == 0 ) {
            PonderationCca = document.getElementById("ctl0_CONTENU_PAGE_autreCritereComplementaire_ctl"+ i + "_ccaTauxTva").value;
            if (PonderationCca == "" || PonderationCca == 0){
                res = false;
                document.getElementById("errorCcaTva" + i).style.display = '';
            }else{
                document.getElementById("errorCcaTva" + i).style.display = 'none';
            }
        }
        i++;
    }

    return res;
}

function validatePonderationCPA()
{
    res = true;
    i = 0;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_autreCriterePrix_ctl" + i + "_saveStatutcpa"))
    {
        if (itemStatut.value == 0) {
            ponderationCpa = document.getElementById("ctl0_CONTENU_PAGE_autreCriterePrix_ctl"+ i + "_cpaTauxTva").value;
            if (ponderationCpa == ""  || ponderationCpa == 0){
                res = false;
                document.getElementById("errorCpaTva" + i).style.display = '';
            }else{
                document.getElementById("errorCpaTva" + i).style.display = 'none';
            }
        }
        i++;
    }

    return res;
}

function validateDescriptionCCA()
{
    res = true;
    i = 0;
    while (itemStatut = document.getElementById("ctl0_CONTENU_PAGE_autreCritereComplementaire_ctl" + i + "_saveStatutcca"))
    {
        if (itemStatut.value == 0) {
            libelleItem = document.getElementById("ctl0_CONTENU_PAGE_autreCritereComplementaire_ctl"+ i + "_ccaLibelle").value;
            if (libelleItem == ""){
                res = false;
                document.getElementById("errorLibelleCca" + i).style.display = '';
            }else{
                document.getElementById("errorLibelleCca" + i).style.display = 'none';
            }
        }
        i++;
    }
    return res;
}

function calculerAllForView() {
    var nbreItems = document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_nbreItems').value;
    var totalHt=0;
    var totalTtc=0;
    for (var i = 1; i <= nbreItems; i++) {
        var valuechampsPrix = parseFloat(document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemPrixUnitaireHt').value);
        var valuetauxTva = parseFloat(document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemTauxTva').value);
        var quantiteVal = document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemQuantite').innerHTML;

        if(valuechampsPrix && valuetauxTva) {
            valueTva = parseFloat(1+(valuetauxTva/100));
            document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemPrixUnitaireHtCalcule').innerHTML = arrondirEspace(valuechampsPrix*valueTva,2,' ','');
        }
        if(valuechampsPrix && quantiteVal) {
            tot1 = parseFloat(valuechampsPrix)*parseFloat(quantiteVal);
            if(tot1.toString()!='NaN') {
                totalHt+=tot1;
                document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemPrixTotalHt').innerHTML = arrondirEspace(tot1,2,' ','');
            }
        }
        if(valuechampsPrix && quantiteVal && valuetauxTva) {
            tot2 = parseFloat(valuechampsPrix)*parseFloat(quantiteVal)*parseFloat(1+(valuetauxTva)/100);
            if(tot2.toString()!='NaN' && tot2.toString()!='NaN,NaN') {
                totalTtc+=tot2;
                document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+i+'_itemPrixTotalTtc').innerHTML = arrondirEspace(tot2,2,' ','');
            }
        }
    }
    indexTot = parseFloat(nbreItems)+1;
    if(totalHt.toString()!='NaN,NaN') {
        document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+indexTot+'_htTotal').innerHTML= arrondirEspace(totalHt,2,' ','');
        document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+indexTot+'_htTotalHidden').value= arrondirEspace(totalHt,2,' ','');
    }
    if(totalTtc.toString()!='NaN,NaN') {
        document.getElementById('ctl0_CONTENU_PAGE_atexoBordereauDePrix_repeaterBordereauPrixItems_ctl'+indexTot+'_ttcTotal').innerHTML= arrondirEspace(totalTtc,2,' ','');
    }
}

function validateDescriptionCPSynthese()
{
    res = true;
    i = 0;

    while (enveloppe = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_idEnv"))
    {
        j = 0;
        while (libelleItem = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCPAEnvellope_ctl" + j + "_reponseFornisseurCPA"))
        {
            if (libelleItem.value == ""){
                res = false;
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCPAEnvellope_ctl" + j + "_errorCpa").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCPAEnvellope_ctl" + j + "_errorCpa").style.display = 'none';
            }
            j++;
        }
        i++;
    }
    return res;
}

function validateDescriptionCCSynthese()
{
    res = true;
    i = 0;

    while (enveloppe = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_idEnv"))
    {
        j = 0;
        while (libelleItem = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCCAEnvellope_ctl" + j + "_reponseFornisseurCCA"))
        {
            if (libelleItem.value == ""){
                res = false;
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCCAEnvellope_ctl" + j + "_errorCpa").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCCAEnvellope_ctl" + j + "_errorCpa").style.display = 'none';
            }
            j++;
        }
        i++;
    }
    return res;
}

function validateNotesCPSynthese()
{
    res = true;
    i = 0;

    while (enveloppe = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_idEnv"))
    {
        j = 0;
        while (libelleItem = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCP_ctl" + j + "_noteCP"))
        {
            if (libelleItem.value == ""){
                res = false;
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCP_ctl" + j + "_errorCpa").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCP_ctl" + j + "_errorCpa").style.display = 'none';
            }
            j++;
        }
        i++;
    }
    return res;
}

function validateNotesCCSynthese()
{
    res = true;
    i = 0;

    while (enveloppe = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_idEnv"))
    {
        j = 0;
        while (libelleItem = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCC_ctl" + j + "_noteCC"))
        {
            if (libelleItem.value == ""){
                res = false;
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCC_ctl" + j + "_errorCpa").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCC_ctl" + j + "_errorCpa").style.display = 'none';
            }
            j++;
        }
        i++;
    }
    return res;
}

function sortNumber(a,b)
{
    return a - b;
}

function updateNotePondereeAndRang(){

    i = 0;
    index = 1;
    tabTotal = new Array;
    tabTotal1 = new Array;
    while (enveloppe = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_idEnv"))
    {
        notePCP = 0;
        if(note = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_noteCP1")){
            ponderation = document.getElementById("ctl0_CONTENU_PAGE_ponderationCP1").innerHTML;
            note = note.value;
            noteP = arrondirEspace((note * ponderation) / 100, 2, ' ','');
            notePCP += (note * ponderation) / 100;
            document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_notePCP"). innerHTML = noteP;
        }

        j=0;
        while (noteCPA = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCP_ctl" + j + "_noteCP")){
            ponderation = document.getElementById("ctl0_CONTENU_PAGE_repeaterCP_ctl" + j + "_ponderationCPA").innerHTML;
            noteCPA = noteCPA.value;
            noteP = arrondirEspace((noteCPA * ponderation) / 100, 2, ' ','');
            notePCP += (noteCPA * ponderation) / 100;
            document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCPNotePonderer_ctl" + j + "_notePCP"). innerHTML = noteP;
            j++;
        }


        j=0;
        notePCC = 0;
        while (noteCCA = document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCC_ctl" + j + "_noteCC")){
            ponderation = document.getElementById("ctl0_CONTENU_PAGE_repeaterCC_ctl" + j + "_ponderationCCA").innerHTML;
            noteCCA = noteCCA.value;
            noteP = arrondirEspace((noteCCA * ponderation) / 100, 2, ' ','');
            notePCC += (noteCCA * ponderation) / 100;
            document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_repeaterCCNotePonderer_ctl" + j + "_notePCC"). innerHTML = noteP;
            j++;
        }

        document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_totalNotePCP"). innerHTML = arrondirEspace(notePCP, 2, ' ','');
        document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_totalNotePCC"). innerHTML = arrondirEspace(notePCC, 2, ' ','');
        document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_totalNoteP"). innerHTML = arrondirEspace(notePCP + notePCC, 2, ' ','');
        document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_hiddenTotalNoteP"). value = arrondirEspace(notePCP + notePCC, 2, '','');

        if(tabTotal.indexOf(notePCP + notePCC) == -1) {
            tabTotal [index++] = parseFloat(notePCP) + parseFloat(notePCC);
        }

        tabTotal1 [i] = parseFloat(notePCP) + parseFloat(notePCC);
        i++;
    }

    tabTotal.sort(sortNumber);
    var length = tabTotal.length;
    i = 0;
    while (i < tabTotal1.length)
    {
        rang = length - tabTotal.indexOf(tabTotal1[i])-1;
        document.getElementById("ctl0_CONTENU_PAGE_repeaterEntreprise_ctl" + i + "_rang") . innerHTML = rang;

        i++;
    }

}

function validateAdresseFact() {
    var nbreDest = document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_nbreAdresseFact').innerHTML;
    var valide = false;
    for (var i = 0; i < nbreDest; i++) {
        if(document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_RepeaterAdresseFacturation_ctl'+i+'_idRadioBouton').checked == true){
            valide = true;
        }
    }
    return valide;
}

function validateMotifRejet() {
    if (document.getElementById("ctl0_CONTENU_PAGE_motifRejet").value == "")  {
        return false;
    }
    return true;
}

function VerifierFormatDateValidationEco() {

    element = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigneParEntiteCoordinatrice');
    if(element.checked){
        dateStr = document.getElementById('ctl0_CONTENU_PAGE_dateMiseEnLigneParEntiteCoordinatrice').value;
        var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
        var matchArray = dateStr.match(datePat); // is the format ok?

        if (matchArray == null) {
            return false;
        }
    }
    return true;
}

function VerifierFormatDateValidationEcoAutresAnnonces() {

    dateStr = document.getElementById('ctl0_CONTENU_PAGE_dateMiseEnLigneSouhaitee').value;
    var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
    var matchArray = dateStr.match(datePat);

    if (dateStr && matchArray == null) {
        return false;
    }
    return true;
}

function addPiecesPropre(idComposant){
    var nbrePiece = document.getElementById(idComposant + '_nbrePiecesAjouter').innerHTML;
    ligneRepeater = document.getElementById(idComposant + '_listePiecesRepeater_ctl'+ nbrePiece + '_fichierSignPropre');
    if(ligneRepeater){
        ligneRepeater.style.display = "";
        document.getElementById(idComposant + '_nbrePiecesAjouter').innerHTML = parseInt(nbrePiece) + 1;
        document.getElementById(idComposant + '_listePiecesRepeater_ctl'+ nbrePiece + '_ajouter').innerHTML = 1;
    }

}

function replaceCaracSpecial(element)
{
    var pattern = {
        "<":"<",
        ">":">",
        "-":"-",
        "'":"'",
        "-":"-",
        "_":"_",
        "\u2264":"<=",
        "\u2265":">=",
        "\u00A9":"",
        "\u201D":"\"",
        "\u201C":"\"",
        "\u0152":"OE",
        "\x0133":"..",
        "\x243":"<=",
        "\x242":">=",
        "\x0156":"oe",
        "\x8212":"--",
        "\x0145":"'",
        "\x0146":"'",
        "\x0147":"\"",
        "\x0148":"\"",
        "\x174":"\"",
        "\x175":"\"",
        "\x0192":"A",
        "\x0193":"A",
        "\x0194":"A",
        "\x0195":"A",
        "\x0196":"A",
        "\x0197":"A",
        "\x0200":"E",
        "\x0201":"E",
        "\x0202":"E",
        "\x0203":"E",
        "\x0204":"I",
        "\x0205":"I",
        "\x0206":"I",
        "\x0207":"I",
        "\x0210":"O",
        "\x0211":"O",
        "\x0212":"O",
        "\x0213":"O",
        "\x0214":"O",
        "\x0217":"U",
        "\x0218":"U",
        "\x0219":"U",
        "\x0220":"U",
    }

    for(var key in pattern)
    {
        element.value = element.value.replace(new RegExp(key, 'g'),pattern[key]);
    }
}
function validatorTelephone(sender, parameter)
{
    var result = false;
    if(parameter !='') {
        var telephonePattern =  /^(\+{0,1}\d{1,3}(\d|\.| |-)+$)/;
        result = telephonePattern.test(parameter);
    }
    return result;
}

function validatorEmail(sender, parameter)
{
    if(parameter !='') {
        if(validateEmail(parameter)) return true;
        else return false;
    }
    else {
        return false;
    }
}
function verifyIfGeneratedAEsExist(movedFiles) {
    if(movedFiles!=null && movedFiles!="") {
        bootbox.alert("<?php Prado::localize('MOVED_DELETED_FILES') ?>" + movedFiles.substring(0,(movedFiles.length)-2)+".");
        document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation').checked= false;
        document.getElementById('ctl0_CONTENU_PAGE_sendResponse').style.display="";
        return false;
    }

}

function lastRefresh()
{
    index = 1;
    while(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersSansSignatures')) {
        document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersSansSignatures').value= "";
        index++;
    }
    index = 1;
    while(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecPlsrsSignatures')){
        document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecPlsrsSignatures').value="";
        index++;
    }
    index = 1;
    while(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecCertificatExpire')){
        document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_fichiersAvecCertificatExpire').value="";
        index++;
    }
    index = 1;
    while(document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_signaturePriseEnCompte')){
        document.getElementById('ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'+index+'_signaturePriseEnCompte').value="";
        index++;
    }
    //Offre Technique
    index=0;
    while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures')) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //Offre Technique
    index=0;
    while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures')) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //Offre Technique
    index=0;
    while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire')) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value="";
            indexFile++;
        }
        index++;
    }
    //Offre Technique
    index=0;
    while(lotOT=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_numLotOffreTechnique')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte')) {
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'+index+'_PiecesTechniques_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value="";
            indexFile++;
        }
        index++;
    }
    //Offre
    index=0;
    while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //Offre
    index=0;
    while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //Offre
    index=0;
    while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value="";
            indexFile++;
        }
        index++;
    }
    //Offre
    index=0;
    while(lotO=document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_numLotOffre')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_PiecesOffre_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value="";
            indexFile++;
        }
        index++;
    }
    //Acte dengagement
    index = 0;
    while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersSansSignatures')) {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersSansSignatures').value="";
        index++;
    }
    index = 0;
    while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecPlsrsSignatures')) {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecPlsrsSignatures').value="";
        index++;
    }
    index = 0;
    while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecCertificatExpire')) {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_fichiersAvecCertificatExpire').value="";
        index++;
    }
    index = 0;
    while(document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_signaturePriseEnCompte')) {
        document.getElementById('ctl0_CONTENU_PAGE_repeaterOffres_ctl'+index+'_signaturePriseEnCompte').value="";
        index++;
    }
    //anonymat
    index=0;
    while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersSansSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //anonymat
    index=0;
    while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecPlsrsSignatures').value="";
            indexFile++;
        }
        index++;
    }
    //anonymat
    index=0;
    while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_fichiersAvecCertificatExpire').value="";
            indexFile++;
        }
        index++;
    }
    //anonymat
    index=0;
    while(lotA=document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_numLotAnonymat')) {
        indexFile = 1;
        while(document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte')){
            document.getElementById('ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'+index+'_PiecesAnonymat_listePiecesRepeater_ctl'+indexFile+'_signaturePriseEnCompte').value="";
            indexFile++;
        }
        index++;
    }

    document.AppletSignaturesInfos.setParam2(true);
    document.AppletSignaturesInfos.setLastRefresh(true);
}

function launchSendResponseProcess() {
    document.getElementById('ctl0_CONTENU_PAGE_sendResponse').click();
}
function retourmessageriestatut(newStatut)
{
    if(newStatut == "OK") {
        window.location = window.location.protocol+"//"+window.location.hostname +  "?page=Entreprise.ReponseEntrepriseEnvoye";
        //window.location.assign("?page=Entreprise.ReponseEntrepriseEnvoye");
    }
}

function validatorEmailNonObligatoire(sender, parameter)
{
    if(parameter !='') {
        if(validateEmail(parameter)) return true;
        else return false;
    }
    else {
        return true;
    }
}


function validerSigleCommission()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_sigleCommission").value){
        return false;
    }else{
        return true;
    }
}


function validerIntituleCommission()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_intituleCommisssion").value){
        return false;
    }else{
        return true;
    }
}



function validerStatutSeance()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_statutSeance").value){
        return false;
    }else{
        return true;
    }
}

function validerTypeCommission()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_typeCommission").value == 0){
        return false;
    }else{
        return true;
    }
}

function validerDateHeureSeance()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_dateHeure").value) {
        return false;
    } else {
        return true;
    }
}

function validerSalleSeance()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_salleSeance").value) {
        return false;
    } else {
        return true;
    }
}

function validerLieuSeance()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_lieuSeance").value) {
        return false;
    } else {
        return true;
    }
}

function validerCiviliteIntervenant(){

    if(document.getElementById("ctl0_CONTENU_PAGE_civilite").value == 0){
        return false;
    }else{
        return true;
    }
}

function validerTypeVoixIntervenant(){

    if(document.getElementById("ctl0_CONTENU_PAGE_typeVoix").value == 0){
        return false;
    }else{
        return true;
    }
}


/**
 * Permet de rendre visible les étoites précedant les champs obligatoires si << Activer l'utilisation du fuseau horaire : choix oui >>
 */
function visibiliteEtoileChampObligatoire()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_activerFuseauHoraireOui").checked == true) {
        document.getElementById("etoileDecalageGmt").style.display = '';
        document.getElementById("etoileLieuResidence").style.display = '';
    } else if(document.getElementById("ctl0_CONTENU_PAGE_activerFuseauHoraireOui").checked == false) {
        document.getElementById("etoileDecalageGmt").style.display = 'none';
        document.getElementById("etoileLieuResidence").style.display = 'none';
    }
}

function validerSelectionDecalageParRapportGmt()
{
    if(document.getElementById("ctl0_CONTENU_PAGE_decalageParRapportGmt").value == 'Selectionnez') {
        return false;
    }
    return true;
}

function checkDateTimeDateLimiteRemisePlisLocale()
{
    object = document.getElementById('ctl0_CONTENU_PAGE_dateRemisePlisLocale');
    if (object != null && object.value != "")
    {
        var regExp = new RegExp("\\b[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]\\b\\ [0-2][0-9]\\:[0-6][0-9]");
        if(!(object.value.match(regExp)))
        {

            return false;
        }
        else
        {
            if(!isDate (object.value))
            {
                return false;
            }
        }
    }

    return true;
}

function dateLimiteRemisePlisLocaleAnterieurAujourdhui(sender, parameter)
{
    radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_miseEnLigne');
    dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_datemiseEnLigneLocale');
    dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_dateAujourdhui');
    radio = true;
    if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
        radio = false;
    }
    if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
        if(frnDateToIso(parameter) < frnDateToIso(dateMiseEnligne.value)){
            return false;
        }
    }else if(dateAujourdhui && dateAujourdhui.value != ""){
        if(frnDateToIso(parameter) < frnDateToIso(dateAujourdhui.value)){
            return false;
        }
    }
    return true;
}

function validerLieuResidenceModifConsApresValidation()
{
    if((document.getElementById("ctl0_CONTENU_PAGE_lieuResidence").value == ''
            || !document.getElementById("ctl0_CONTENU_PAGE_lieuResidence").value)
        && document.getElementById("ctl0_CONTENU_PAGE_modifiDateRemisePlis").checked == true
        && document.getElementById("ctl0_CONTENU_PAGE_etatActivationFuseauHoraire").value) {
        return false;
    } else {
        return true;
    }
}

function showProcessingBlock(fichierASigner) {
    if(document.getElementById("processing")!=null) {
        document.getElementById("processing").style.display = '';
        var index = fichierASigner.lastIndexOf("\\");
        if(index<0) {
            index = fichierASigner.lastIndexOf("/");
        }
        document.getElementById("ctl0_CONTENU_PAGE_nomFichierASigner").innerHTML = fichierASigner.substring(index+1,fichierASigner.length);
        document.getElementById("ctl0_CONTENU_PAGE_cheminFichierASigner").innerHTML = fichierASigner.substring(0,index);
    }
}

function hideProcessingBlock() {
    if(document.getElementById("processing")!=null) {
        document.getElementById("processing").style.display = 'none';
    }
}

function checkFournisseurDoc(sender, parameter)
{
    if (document.getElementById('ctl0_CONTENU_PAGE_envoiType').checked )
    {
        if (parameter != null && parameter != 0)
        {
            return true;
        } else {
            return false;
        }
    }else {
        return true;
    }
}

//duplicata checkFournisseurDoc
function checkFournisseurDocFormulaireConsultation(sender, parameter)
{
    if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_envoiType').checked )
    {
        fournisseurDoc = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_fournisseurDoc').value;
        if (fournisseurDoc != null && fournisseurDoc != 0)
        {
            return true;
        } else {
            return false;
        }
    }else {
        return true;
    }
}
function masquerCordonnee()
{
    var avecAuth = document.getElementById('moduleAvecAuth').value;
    var sansIdentification = document.getElementById('moduleSansIden').value;
    if( avecAuth == '0' && sansIdentification == '1')
    {
        if(document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixAnonyme').checked ){
            document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_blocMesCoordonnees').style.display='none';
        } else{
            document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_blocMesCoordonnees').style.display='';
        }
    }
}

function valideControl(sender, parameter)
{
    if (document.getElementById('ctl0_CONTENU_PAGE_RechercheCollaborateur_rechercheCollaborateurOui').checked )
    {
        if (parameter != null && parameter != "")
        {
            return true;
        } else
        {
            return false;
        }

    } else {
        return true;
    }
}

function valideControlTel(sender, parameter){
    if (parameter != null && parameter != "") {
        if(isNumeric(parameter))
        {
            return true;
        } else
        {
            return false;
        }
    } else {
        return true;
    }
}
function valideControlEmail(sender, parameter){
    if (document.getElementById('ctl0_CONTENU_PAGE_RechercheCollaborateur_rechercheCollaborateurOui').checked )
    {

        return validateEmail(parameter);


    } else {
        return true;
    }

}
function dropText(input) {
    input.value = "";
    input.focus();
}

function sendMailPartage(subject,body_message) {
    location.href="mailto:?Subject="+subject+"body="+body_message+"";
}
/**
 * Permet d'écrire dans le cookie
 * @param nom: nom de la cookie
 * @param valeur: valeur de la cookie
 */
function setCook(nom,valeur) {
    document.cookie = nom + "=" + escape(valeur)
}
/**
 * Permet de recuperer la valeur de la cookie
 * @param nom: nom de la cookie
 * @returns
 */
function getCook(nom) {
    deb = document.cookie.indexOf(nom + "=")
    if (deb >= 0) {
        deb += nom.length + 1
        fin = document.cookie.indexOf(";",deb)
        if (fin < 0) fin = document.cookie.length
        return unescape(document.cookie.substring(deb,fin))
    }
    return ""
}
function isCheckedCheckAll(id_element,elementTab)
{
    var thisElement = document.getElementById(id_element);
    // traitement des cases filles de la case this
    if (thisElement.checked)
    {
        j = 1;
        while (curElement = document.getElementById(elementTab+'_tablePiecesMarche_ctl'+j+'_piece_item'))
        {
            curElement.checked=true;
            j++;
        }
    }
    // décochage des cases à cocher parent si this est décoché
    if (!thisElement.checked)
    {
        j=1;
        while (curElement = document.getElementById(elementTab+'_tablePiecesMarche_ctl'+j+'_piece_item'))
        {
            curElement.checked=false;
            j++;
        }
    }
}
function getSelectedTypeDoc(idListeDocTypes,hidenField){
    var listeTypes = document.getElementById(idListeDocTypes);
    if(listeTypes){
        var idTypeDoc = listeTypes.options[listeTypes.selectedIndex].value;
        document.getElementById(hidenField).value=idTypeDoc;
    }
}
function getIdClientErreur(idClientErreur,idClientErrorImage){
    if(idClientErrorImage){
        document.getElementById(idClientErreur).value= idClientErrorImage;
    }
}
function isAllCheckValide(){
    var erreur = true;
    var arrayNumLots = new Array();
    arrayNumLots[0] = "ctl0_CONTENU_PAGE_PiecesDepot";
    arrayNumLots[1] = "ctl0_CONTENU_PAGE_PieceDce";
    arrayNumLots[2] = "ctl0_CONTENU_PAGE_AutresPiece";
    arrayNumLots[3] = "ctl0_CONTENU_PAGE_PiecePublicite";
    arrayNumLots[4] = "ctl0_CONTENU_PAGE_documentsExternes";
    for(var i=0;i < arrayNumLots.length;i++){
        j=1;
        while (curElement = document.getElementById(arrayNumLots[i]+'_tablePiecesMarche_ctl'+j+'_piece_item'))
        {
            if(curElement.checked == true){
                var listeTypes = document.getElementById(arrayNumLots[i]+'_tablePiecesMarche_ctl'+j+'_dce_piece_type_doc');
                if(listeTypes){
                    var idTypeDoc = listeTypes.options[listeTypes.selectedIndex].value;
                    if(idTypeDoc == 0)
                    {
                        document.getElementById(arrayNumLots[i]+'_tablePiecesMarche_ctl'+j+'_error').style.display = '';
                        erreur = false;
                    }else{
                        document.getElementById(arrayNumLots[i]+'_tablePiecesMarche_ctl'+j+'_error').style.display = 'none';
                    }
                }
            }else{
                document.getElementById(arrayNumLots[i]+'_tablePiecesMarche_ctl'+j+'_error').style.display = 'none';
            }
            j++;
        }
    }
    if(erreur){
        document.getElementById('divValidationSummary').style.display='none';
    }
    return erreur;
}

function isTypeDocSelected(sender, parameter)
{
    res = true;

    if(parameter == 0)
    {
        var idTemplate = sender.control.id;
        var elem = idTemplate.split('_');
        i=0;
        var other = "";
        while(i < elem.length-1 ){
            other += elem[i++]+'_';
        }
        other += 'idClientErreur';
        idClientErreur = document.getElementById(other);
        if(idClientErreur){
            error = document.getElementById(idClientErreur.value);
            if(error){
                error.style.display=''
            }
        }
        res = false;
    }else{
        document.getElementById('divValidationSummary').style.display='none';
    }
    return res;
}


/**
 * Pertmet de rendre obligatoire la saisie de la date de notification pour la fiche de recensement chorus
 * Cette date doit etre supérieure à la date du jour
 */
function chorusValidationDateNotification()
{
    if(!document.getElementById('ctl0_CONTENU_PAGE_dateNotification').value) {
        return false;
    }
    return true;
}
/**
 * Permet d'afficher/masquer les blocs co-traitants en fonction du nombre de co-traitants selectionnés
 */
function afficherBlocCoTraitants()
{
    var blocCoTraitant = document.getElementById("ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes");

    for(var i=1;i <= 50;i++)
    {
        //document.getElementById('co_traitant_'+i).style.display='none';
        document.getElementById('blocCodePaysCoTitulaire_'+i).style.display='none';
        document.getElementById('blocSirenSiretTitulaire_'+i).style.display='none';
    }
    for(var i=1;i <= blocCoTraitant.value;i++)
    {
        //document.getElementById('co_traitant_'+i).style.display='';
        document.getElementById('blocCodePaysCoTitulaire_'+i).style.display='';
        if(document.getElementById('ctl0_CONTENU_PAGE_repeatCodePays_ctl'+(i-1)+'_codePaysCoTitulaire').value=='FR') {
            document.getElementById('blocSirenSiretTitulaire_'+i).style.display='';
        }
    }

}
/**
 * Permet de valider la nature de l'acte juridique
 */
function chorusValiderNatureActeJuridique()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_natureActe').value == 0) {
        return false;
    }
    return true;
}
/**
 * Permet de valider les codes cpv
 */
function chorusValiderCodeCpv()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_atexoRefCpv_codeRefPrinc").value)
    {
        document.getElementById("erreurcas2ctl0_CONTENU_PAGE_atexoRefCpv").style.display = '';
        return false;
    }
    return true;
}
/**
 * Permet de valider le type de procedure
 */
function chorusValiderTypeProcedure()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_procedureType').value == 0) {
        return false;
    }
    return true;
}
/**
 * Permet de valider les formes de prix
 */
function chorusValiderFormePrix()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_formeprix').value == 0) {
        return false;
    }
    return true;
}
/**
 * Permet de valider le montant ht
 */
function chorusValiderMontantHt()
{
    if(!document.getElementById('ctl0_CONTENU_PAGE_montantHtAccord').value) {
        return false;
    }
    return true;
}
/**
 * Permet d'afficher/masquer le siret titulaire
 */
function afficherBlocSiretTitulaire()
{
    var codePaysTitulaire = document.getElementById("ctl0_CONTENU_PAGE_codePays");
    if(codePaysTitulaire.value != 'FR') {
        document.getElementById("blocSirenSiretTitulaire").style.display='none';
    } else {
        document.getElementById("blocSirenSiretTitulaire").style.display='';
    }
}
/**
 * Permet de valider le siren/siret du titulaire
 */
function chorusValidationSiretTitulaire()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siretTitulaire').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_sirenTitulaire').value;
    var codePaysTitulaire = document.getElementById("ctl0_CONTENU_PAGE_codePays");
    if(codePaysTitulaire.value != 'FR') {
        return true;
    }
    if ((siren !='') || (siret !='')) {
        return validerSiret(siren, siret);
    }
    else {
        return false;
    }
}
/**
 * Fonction généralisée qui permet de valider le siren/siret
 * @param siren
 * @param siret
 * @param Boolean isValidationDisabled
 * @returns {Boolean}
 */

function validerSiret(siren, siret, isValidationDisabled = false)
{
    if ((siren !=0) && (siret==0)) {
        if(isSirenValide(siren, isValidationDisabled)) {
            return true;
        } else {
            return false;
        }
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret, siren, isValidationDisabled)) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * Permet de valider le siren/siret des co-titulaires
 */
function chorusValidationSiretCoTitulaire(sender,parameter)
{
    var i = parameter;
    var nbrEntreprisesCoTitulaires = document.getElementById("ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes").value;

    if(parseInt(i) >= parseInt(nbrEntreprisesCoTitulaires)) {
        return true;
    }
    var siret = document.getElementById('ctl0_CONTENU_PAGE_repeatCodePays_ctl'+i+'_siretCoTitulaire').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_repeatCodePays_ctl'+i+'_sirenCoTitulaire').value;
    var codePaysCoTitulaire = document.getElementById('ctl0_CONTENU_PAGE_repeatCodePays_ctl'+i+'_codePaysCoTitulaire');
    /*if(codePaysCoTitulaire.value != 'FR') {
            document.getElementById("blocSirenSiretTitulaire_1").style.display='none';
            return true;
        } else {
            document.getElementById("blocSirenSiretTitulaire_1").style.display='';
        }*/
    if (codePaysCoTitulaire.value == 'FR') {
        if((siren !='') || (siret !=''))
            return validerSiret(siren, siret);
        return false;
    }
    else {
        return true;
    }
}
/**
 * Permet d'afficher/masquer le siret titulaire co-Titulaire
 */
function afficherBlocSiretCoTitulaire(element, i)
{
    if(element.value != 'FR') {
        document.getElementById("blocSirenSiretTitulaire_"+i).style.display='none';
    } else {
        document.getElementById("blocSirenSiretTitulaire_"+i).style.display='';
    }
}

/**
 * Permet de valider le nombre total de propositions reçues
 */
function chorusValiderNombreTotalPropositionsRecues()
{
    if(!document.getElementById('ctl0_CONTENU_PAGE_nbPropositionsRecues').value) {
        return false;
    }
    return true;
}
/**
 * Permet de valider le nombre total de propositions dématerialisées
 */
function chorusValiderNombreTotalPropositionsDemat()
{
    if(!document.getElementById('ctl0_CONTENU_PAGE_nbPropositionsDemat').value) {
        return false;
    }
    return true;
}
/**
 * Permet de valider le CCAG de référence
 */
function chorusValiderCcagReference()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_ccgaReference').value == '0') {
        return false;
    }
    return true;
}
/**
 * Permet de valider le type d'avance
 */
function chorusValiderTypeAvance()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_pourcentageAvance').value) {
        if(document.getElementById('ctl0_CONTENU_PAGE_typeAvance').value == 0) {
            return false;
        }
        return true;
    } else {
        return true;
    }
}
/**
 * Permet de pré-remplir l'identifiant de l'accord cadre de la fiche navette par celui rempli dans la fiche de recensement
 */
function remplirIdentifiantAccordCadreFicheNavette()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_idAccordCadre').value) {
        document.getElementById("div_idAccordCadre_navette").style.display='';
        document.getElementById("div_libelleAccordCadre_navette").style.display='';
        document.getElementById("div_dateCreationAccord").style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_idAccordCadre_navette').value = document.getElementById('ctl0_CONTENU_PAGE_idAccordCadre').value;
        document.getElementById('ctl0_CONTENU_PAGE_hiddenIdAccordCadre_navette').value = document.getElementById('ctl0_CONTENU_PAGE_idAccordCadre').value;
    } else {
        document.getElementById("div_idAccordCadre_navette").style.display='none';
        document.getElementById("div_libelleAccordCadre_navette").style.display='none';
        document.getElementById("div_dateCreationAccord").style.display='none';
    }
}
/**
 * Permet de valider le nombre d'entreprises co-traitantes
 */
function chorusValiderNbrEtpCoTraitantes()
{
    var typeGroupement = document.getElementById('ctl0_CONTENU_PAGE_TypeGroupement').value;
    if(typeGroupement == 'GPTC' || typeGroupement == 'GPTS') {
        if(document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').value == '0') {
            return false;
        }
        return true;
    }
    return true;
}

/**
 * Permet de pré-remplir le montant HT de la fiche navette par celui rempli dans la fiche de recensement
 */
function remplirMontantHtFicheNavette()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_montantHtAccord').value) {
        document.getElementById('ctl0_CONTENU_PAGE_montantHtAccordFicheNavette').value = document.getElementById('ctl0_CONTENU_PAGE_montantHtAccord').value;
    }
}
/**
 * Permet de controler la selection de type de groupement
 */
function changerTypeGroupement()
{
    var nbrEtpsCoTraitants = document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').value;
    var typeGroupement = document.getElementById('ctl0_CONTENU_PAGE_TypeGroupement').value;
    if(typeGroupement == 'TUAST') {
        //Empecher la selection de co-traitants
        document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').value = 0;
        document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').disabled = 'disabled';
        hideBlocsCoTraitants();
    } else {
        document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').disabled = '';
    }
    if(nbrEtpsCoTraitants == 0 && (typeGroupement == 'GPTS' || typeGroupement == 'GPTC')) {
        bootbox.alert('<?php Prado::localize('ALERTE_ERREUR_MESSAGE_SELECTION_TYPE_GROUPEMENT_CHORUS2') ?>');
        return false;
    }
    return true;
}
/**
 *
 * Permet de remplacer la virgule par point
 */
function remplacerVirguleParPoint(idElement)
{
    document.getElementById(idElement).value = document.getElementById(idElement).value.replace(',', '.');
}
/**
 * Permet de valider le type de groupement
 */
function ValidateTypeGroupement()
{
    var EntitePublic = document.getElementById('ctl0_CONTENU_PAGE_TypeGroupement');
    if (EntitePublic.options[EntitePublic.selectedIndex].value == 0)
    {
        document.getElementById('spanTypeGroupement').style.display='';
        return false;
    } else {
        document.getElementById('spanTypeGroupement').style.display='none';
        return true;
    }
}

/**
 * Permet de masquer le bloc des cotraitants
 */
function hideBlocsCoTraitants()
{
    for(var i=1;i <= 10;i++)
    {
        document.getElementById('blocCodePaysCoTitulaire_'+i).style.display='none';
        document.getElementById('blocSirenSiretTitulaire_'+i).style.display='none';
    }
}
/**
 *
 * Arrondir un montant et met la virgule comme séparateur
 */
function formatterMontant(id){

    var valeur = document.getElementById(id).value;
    var valeur = valeur.replace(' ','');
    var newValeur = arrondirEspace(valeur,2,' ',0);
    var mt = newValeur.replace('.',',');
    if (newValeur == 'NaN.NaN'){
        mt =  "0,00";
    }
    document.getElementById(id).value = mt;
}

/**
 *
 * Validateur de date de previsionnelle fin du marche
 */
function ValidateDateNotifFinMarche(sender, parameter){
    var parameterElement = parameter.split('#');
    var erreur = validateDatesDansDecision(parameterElement[0],parameterElement[1]);
    return erreur;


}

/**
 *
 * Validateur des date dans décision
 */
function validateDatesDansDecision(nameElement,nameImgError){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre.value == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+nameElement);
            if(valeur.value == ''){
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = '';
                erreur = false;
            }else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de date de notification reelle
 */
function ValidateDateNotificationFinMarche(sender, parameter){
    var parameterElement = parameter.split('#');
    var erreur = validateDateNotificationReelle(parameterElement[0],parameterElement[1],parameterElement[2]);
    return erreur;
}

function validateDateNotificationReelle(dateNotifPrev,dateNotifR,nameImgError){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        if(checkIem.checked == true){
            dateNotifPrevisionnelle = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+dateNotifPrev).value;
            dateNotifReelle = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+dateNotifR).value;
            if(dateNotifPrevisionnelle != '' && dateNotifReelle != ""){
                var deb=dateNotifPrevisionnelle.split('/');
                prev = deb[2]+'-'+deb[1]+'-'+deb[0];

                var fin=dateNotifReelle.split("/");
                reelle=fin[2]+"-"+fin[1]+"-"+fin[0];
                if(prev>reelle){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = '';
                    erreur = false;
                }else{
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
                }

            }else{
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
            }

        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
        }
        i++;
    }
    return erreur;
}

/**
 *
 */
function validateDateNotifFinMarcheReelle(sender, parameter){
    var parameterElement = parameter.split('#');
    var dateNotificationReelle = parameterElement[0];
    var dateFinMarcheReelle = parameterElement[1];
    var nameImgError = parameterElement[2];

    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        if(checkIem.checked == true){
            valeur1 = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+dateNotificationReelle);
            valeur2 = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+dateFinMarcheReelle);
            if(valeur1.value != '' && valeur2.value == ''){
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = '';
                erreur = false;
            }else{
                if(valeur2.value != '' && valeur1.value == ''){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = '';
                    erreur = false;
                }else{
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+nameImgError).style.display = 'none';
        }
        i++;
    }

    return erreur;

}

/**
 *
 * Validateur de ville de tranche budgetaire
 */
function ValidateVille(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_ville");
            if(valeur.value == ''){
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorVille" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorVille" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorVille" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de CP de tranche budgetaire
 */
function ValidateCp(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_cp");
            var paysNational = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_Nationale");
            if(valeur.value == '' && (paysNational.checked == true)){
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorCp" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorCp" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorCp" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de PME de tranche budgetaire
 */
function ValidatePme(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        if(checkIem.checked == true){
            lisePme = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_pmiPme");
            if (lisePme.options[lisePme.selectedIndex].value == 9)
            {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPme" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPme" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPme" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de la tranche budgetaire
 */
function ValidateTranche(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            ListeTranche = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_trancheBudgetaire");
            if(ListeTranche && ListeTranche.options[ListeTranche.selectedIndex]){
                if (ListeTranche.options[ListeTranche.selectedIndex].value)
                {
                    if(ListeTranche.options[ListeTranche.selectedIndex].value == 0){
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = '';
                        erreur = false;
                    } else {
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = 'none';
                    }
                } else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = '';
                    erreur = false;
                }
            }
        } else {
            if(document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" )){
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = 'none';
            }
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de la nature de prestation de tranche budgetaire
 */
function ValidateNaturePrestation(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            Liste = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_naturePrestations");
            if (Liste.options[Liste.selectedIndex] && Liste.options[Liste.selectedIndex].value == 0)
            {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPrest" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPrest" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorPrest" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Validateur de l'objet de tranche budgetaire
 */
function ValidateObjet(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_objetMarche");
            if(valeur.value == '')
            {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorObMarche" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorObMarche" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorObMarche" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}

/**
 *
 * Validateur de Montant de tranche budgetaire
 */
function ValidateMontant(sender, parameter){

    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre.value == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_montant");
            if(valeur.value == '' || !parseFloat(valeur.value))
            {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorMontant" ).style.display = '';
                erreur = false;
            } else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorMontant" ).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrorMontant" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
/**
 *
 * Enleve les espaces d'un montant
 */
function castMontant(id){
    var objet = document.getElementById(id);
    valeur = objet.value
    if(valeur){
        var mt = valeur.replace(' ','');
    }
    objet.value = mt;
}

/**
 *
 * Permet de valider les clauses sociaux
 */
function validateClauseSociale(sender, parameter) {
    var prefix = 'ctl0_CONTENU_PAGE_achatResponsableConsultation';
    if(document.getElementById(prefix+"_clauseSociale_oui").checked == false && document.getElementById(prefix+"_clauseSociale_non").checked == false)
    {
        return false;
    } else if(document.getElementById(prefix+"_clauseSocialeConditionExecution").checked == false
        && document.getElementById(prefix+"_clauseSpecificationTechnique").checked == false
        && document.getElementById(prefix+"_clauseSocialeInsertion").checked == false
        && document.getElementById(prefix+"_marcheReserve").checked == false
        && document.getElementById(prefix+"_clauseSocialeMarcheInsertion").checked == false
        && document.getElementById(prefix+"_clauseSociale_oui").checked == true
    ) {
        return false;
    } else {

        var clauseSocialeConditionExecution = document.getElementById(prefix+"_clauseSocialeConditionExecution").checked
        var clauseSpecificationTechnique = document.getElementById(prefix+"_clauseSpecificationTechnique").checked
        var clauseSocialeInsertion = document.getElementById(prefix+"_clauseSocialeInsertion").checked

        var marcheReserve = document.getElementById(prefix+"_marcheReserve").checked

        var clauseSocialeAteliersProtegesChecked = document.getElementById(prefix+"_clauseSocialeAteliersProteges").checked;
        var clauseSocialeSIAEChecked =  document.getElementById(prefix+"_clauseSocialeSIAE").checked
        var clauseSocialeESSChecked =  document.getElementById(prefix+"_clauseSocialeESS").checked

        var blockClauseSocialeConditionExecution = false;
        var blockClauseSpecificationTechnique = false;
        var blockclauseSocialeInsertion = false;
        var blockMarcheReserve = false;



        var clauseSocialeConditionExecutionChecked = false
        var clauseSpecificationTechniqueChecked = false
        var clauseSocialeInsertionChecked = false


        if (document.getElementById(prefix + "_listConditionExecutionhiddenSelectedValue").value != '') {
            clauseSocialeConditionExecutionChecked = true;
        }

        if (document.getElementById(prefix + "_listSpecificationTechniquehiddenSelectedValue").value != '') {
            clauseSpecificationTechniqueChecked = true;
        }

        if (document.getElementById(prefix + "_listCritereInsertionhiddenSelectedValue").value != '') {
            clauseSocialeInsertionChecked = true;
        }

        if (clauseSocialeConditionExecution == true && clauseSocialeConditionExecutionChecked == true)
        {
            blockClauseSocialeConditionExecution = true;
        }
        else if (clauseSocialeConditionExecution == false && clauseSocialeConditionExecutionChecked == false)
        {
            blockClauseSocialeConditionExecution = true;
        }

        if (clauseSpecificationTechnique == true && clauseSpecificationTechniqueChecked == true)
        {
            blockClauseSpecificationTechnique = true;
        }
        else if (clauseSpecificationTechnique == false && clauseSpecificationTechniqueChecked == false)
        {
            blockClauseSpecificationTechnique = true;
        }

        if (clauseSocialeInsertion == true && clauseSocialeInsertionChecked == true)
        {
            blockclauseSocialeInsertion = true;
        }
        else if (clauseSocialeInsertion == false && clauseSocialeInsertionChecked == false)
        {
            blockclauseSocialeInsertion = true;
        }

        if ( marcheReserve == true
            && (clauseSocialeAteliersProtegesChecked == true
                || clauseSocialeSIAEChecked == true
                || clauseSocialeESSChecked == true))
        {
            blockMarcheReserve = true
        }
        else if (marcheReserve == false
            && clauseSocialeAteliersProtegesChecked == false
            && clauseSocialeSIAEChecked == false
            && clauseSocialeESSChecked == false)
        {
            blockMarcheReserve = true
        }

        if(blockClauseSocialeConditionExecution == true
            && blockClauseSpecificationTechnique == true
            && blockclauseSocialeInsertion == true
            && blockMarcheReserve == true
        ) {

            return true;
        }
    }

    return false;
}

/**
 *
 * Permet de valider les clauses environnementales
 */
function validateClauseEnvironnementale(sender, parameter){

    var prefix = getPrefix(sender);
    if(document.getElementById(prefix+"_oui").checked == false && document.getElementById(prefix+"_non").checked == false){
        //document.getElementsByClassName('TestMessage')[0].innerHTML = '<div id="errorClauseSociale"  class="messageError"></div>'
        return false;
    }else{
        if(document.getElementById(prefix+"_oui").checked == true){
            if(document.getElementById(prefix+"SpecsTechniques").checked == false && document.getElementById(prefix+"CondExecution").checked == false && document.getElementById(prefix+"CriteresSelect").checked == false){
                document.getElementsByClassName('TestMessage')[0].innerHTML = '<div class="messageError"></div>'
                return false;
            }
        }
    }
    return true;
}

/**
 *
 * Permet de valider les clauses sociaux, appelé depuis la page "Identification"
 */
function validateClauseSocialeIdentification(sender, parameter){
    return (document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_alloti").checked == true) ?
        true : validateAchatResponsable(0);
}

/**
 *
 * Permet de valider les clauses environnementales, appelé depuis la page "Identification"
 */
function validateClauseEnvironnementaleIdentification(sender, parameter){
    return (document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_alloti").checked == true) ?
        true : validateAchatResponsable(1);
}

/**
 *
 * Permet de valider les clauses sociaux, appelé depuis la page "Lots"
 */
function validateClauseSocialeLots(sender, parameter){
    return validateAchatResponsable(0);
}

/**
 *
 * Permet de valider les clauses environnementales, appelé depuis la page "Lots"
 */
function validateClauseEnvironnementaleLots(sender, parameter){
    return validateAchatResponsable(1);
}

/**
 *
 * Permet de valider les clauses sociaux, appelé depuis la page "Conrat"
 */
function validateClauseSocialeContrat(sender, parameter){
    return validateAchatResponsable(0);
}

/**
 *
 * Permet de valider les clauses environnementales, appelé depuis la page "Contrat"
 */
function validateClauseEnvironnementaleContrat(sender, parameter){
    return validateAchatResponsable(1);
}

/**
 *
 * Permet de valider les clauses sociaux, appelé depuis la page "PopupDecisionContrat"
 */
function validateClauseSocialeForPagePopupDecisionContrat(sender, parameter){
    return validateAchatResponsable(0);
}

/**
 *
 * Permet de valider les clauses environnementales, appelé depuis la page "PopupDecisionContrat"
 */
function validateClauseEnvironnementaleForPagePopupDecisionContrat(sender, parameter){
    return validateAchatResponsable(1);
}


/**
 * Permet de valider les lieux d'execution pour le formulaire de consultation
 */
function ValidateLieuExecutionFormCons(sender, parameter)
{
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_idsSelectedGeoN2").value != '')
    {
        return true;
    }
    else
    {
        return false;
    }

}
/**
 * Permet de valider les codes CPV pour le formulaire de consultation
 */
function ValidateCodeCPVFormCons(sender, parameter)
{
    res = true;
    if(!document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_idAtexoRef_codeRefPrinc").value)
    {
        document.getElementById("erreurcas2ctl0_CONTENU_PAGE_bloc_etapeIdentification_idAtexoRef").style.display = '';
        res = false;
    }

    if (res)
        return true;
    else
        return false;
}

function checkDatemiseEnLigneFormCons(sender,parameter)
{
    if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_miseEnLigne').checked )
    {
        if (parameter != null && parameter != "")
        {
            return validateDateTime(sender, parameter);
        } else {
            return false;
        }

    }else {
        return true;
    }
}


function dateAnterieurAujourdhuiFormCons(sender, parameter)
{
    radioDateMiseEnLigne = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_miseEnLigne');
    dateMiseEnligne = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_datemiseEnLigne');
    dateAujourdhui = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateAujourdhui');
    radio = true;
    if(radioDateMiseEnLigne && !radioDateMiseEnLigne.checked){
        radio = false;
    }
    if(parameter != ""){
        if(radio && dateMiseEnligne && dateMiseEnligne.value != "" && dateMiseEnligne.value != "00/00/0000 00:00"){
            if(frnDateToIso(parameter) < frnDateToIso(dateMiseEnligne.value)){
                return false;
            }
        }else if(dateAujourdhui && dateAujourdhui.value != ""){
            if(frnDateToIso(parameter) < frnDateToIso(dateAujourdhui.value)){
                return false;
            }
        }
    }
    return true;
}

function VerifierFormatDateValidationEcoFormCons() {

    element = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_miseEnLigneParEntiteCoordinatrice');
    if(element.checked){
        dateStr = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateMiseEnLigneParEntiteCoordinatrice').value;
        var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
        var matchArray = dateStr.match(datePat); // is the format ok?

        if (matchArray == null) {
            return false;
        }
    }
    return true;
}

function getCalendrierJson() {
    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_calednrier_calendrierPrevisionel')){
        if (typeof(getCalendrierGWTJson) == 'function'){
            json = getCalendrierGWTJson();
            element = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_jsFile');
            element.value = window.btoa(json);
        }
    } else if(document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_calendrier_calendrierReel')){
        json = getCalendrierGWTJson();
        element = document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_calendrier_jsonChaine');
        element.value = window.btoa(json);
    }else if(document.getElementById('ctl0_CONTENU_PAGE_parametrage_calendrier_calendrierParametre')){
        json = getCalendrierGWTJson();
        element = document.getElementById('ctl0_CONTENU_PAGE_jsonFile');
        element.value = window.btoa(json);
    }
}

/**
 * Permet d'afficher le message de confirmation à l'enregistrement de la consultation
 */
function confirmSave()
{
    message = "<?php Prado::localize('DEFINE_CONFIRMATION_ENREGISTREMENT_CONSULTATION') ?>";
    return confirm(message);
}

function validationJourMois (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeDureeMarche_dureeDelai").value;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;
    }
    if(checkedDonneeOrme == true){
        if(valueListe=="DUREE_MARCHEE" && (valueChamp)==""){
            return false;
        }
        return true;
    }
    return true;
}

function validationJourMoisLot (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;

    if(valueListe=="DUREE_MARCHEE" && (valueChamp)==""){
        return false;
    }
    return true;
}

function validationDateNotif (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeDureeMarche_dureeDelai").value;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;
    }
    if(checkedDonneeOrme == true){
        if(valueListe=="DELAI_EXECUTION" && (valueChamp)==""){
            return false;
        }
        return true;
    }
    return true;
}

function validationDateNotifLot (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;
    if(valueListe=="DELAI_EXECUTION" && (valueChamp)==""){
        return false;
    }
    return true;
}

function validationDateNotifJusquau (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeDureeMarche_dureeDelai").value;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;
    }
    if(checkedDonneeOrme == true){
        if(valueListe=="DELAI_EXECUTION" && (valueChamp)==""){
            return false;
        }
        return true;
    }
    return true;
}

function validationDateNotifJusquauLot (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;

    if(valueListe=="DELAI_EXECUTION" && (valueChamp)==""){
        return false;
    }
    return true;
}

function validationDelaiLibre (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeDureeMarche_dureeDelai").value;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;
    }
    if(checkedDonneeOrme == true){
        if(valueListe=="DESCRIPTION_LIBRE" && (valueChamp)==""){
            return false;
        }
        return true;
    }
    return true;
}

function validationDelaiLibreLot (sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    var valueListe = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_dureeDelai").value;

    if(valueListe=="DESCRIPTION_LIBRE" && (valueChamp)==""){
        return false;
    }
    return true;
}

function validationDureeValidite (sender,params)
{
    var idChamp = sender.control.id;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }
    if(checkedDonneeOrme == true){
        var valueChamp = document.getElementById(idChamp).value;
        if(valueChamp==""){
            return false;
        }
        return true;
    }

    return true;
}

function validationCriteresAttribution (sender,params)
{
    var idChamp = sender.control.id;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }
    if(checkedDonneeOrme == true){
        var valueChamp = document.getElementById(idChamp).value;
        if(valueChamp=="layerDefinitionVide1"){
            return false;
        }
        return true;
    }
    return true;
}

function validationDureeMarche (sender,params)
{
    var idChamp = sender.control.id;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
    }
    if(checkedDonneeOrme == true){
        var valueChamp = document.getElementById(idChamp).value;
        if(valueChamp=="divSelectionner"){
            return false;
        }
        return true;
    }
    return true;
}
/*
* Permet d executer une liste de fonctions js
* à appeler avant la sauvegarde
*/

function actionJsAvantSauvegarde(consultationExiste,etapeToGoField,etapeToGo){
    document.getElementById(etapeToGoField).value = etapeToGo;
    if(!document.getElementById(consultationExiste).value){
        if(verifyEtape('etapeIdentification')) {
            openModal('demander-enregistrement','popup-small2',document.getElementById(consultationExiste));
            return false;
        }
    }
    getCalendrierJson();
    return saveDumeAcheteurFrom('fromEtape');
}

/**
 * Permet de verifier que je suis dans l'étape en paramêtre
 * et que j'ai choisi une autre étape
 */
function verifyEtape(etapeCourante)
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_valeurEtapeCourante').value == etapeCourante
        && document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_valeurEtapeSuivante').value != etapeCourante) {
        return true;
    }
    return false;
}
function getDate(typeDate) {
    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_calednrier_calendrierPrevisionel')!= null){
        if (typeDate == 7) {
            return document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis")?.value;
        }

    }else if(document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_calendrier_calendrierReel') != null){
        if (typeDate == 7) {
            return document.getElementById("ctl0_CONTENU_PAGE_calendrierReel_calendrier_dateRemisePlis")?.value;
        }
    }
}
function getUrlChargementCalendrier() {

    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_refConsultation')!= null){
        element = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_refConsultation');
        if(element.value){
            elementUrl = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_calednrier_urlPdf')
            valeur = 'index.php?page=Agent.DownloadCalendrierPdf&id='+element.value;
            elementUrl.setAttribute('href',valeur);
            // alert('chargement1'+element.value);
            return "index.php?page=Agent.ChargementCalendrier&id="+element.value;
        }
    } else if(document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_refConsultation') != null){
        element = document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_refConsultation');
        if(element.value){
            elementUrl = document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_calendrier_urlPdf2');
            valeur = 'index.php?page=Agent.DownloadCalendrierPdf&id='+element.value+'&reel';
            elementUrl.setAttribute('href',valeur);
            return "index.php?page=Agent.ChargementCalendrier&id="+element.value;
        }
    } else if(document.getElementById('ctl0_CONTENU_PAGE_parametrage_calendrier_calendrierParametre') != null){
        idTypeProcedure = document.getElementById('ctl0_CONTENU_PAGE_IdTypeProcedure');
        org = document.getElementById('ctl0_CONTENU_PAGE_organisme');
        if(idTypeProcedure.value && org.value){
            return "index.php?page=Agent.ChargementCalendrier&typeProcedure="+idTypeProcedure.value+"&org="+org.value;
        }
    }
}

function afficheCalendrier() {
    if (typeof(afficheCalendrierGWT) == 'function'){
        afficheCalendrierGWT();
    }
}

function displayAppartenanceLot(input,ClientIdTableau, idTh){
    myInput = document.getElementById(input);
    if (myInput.checked == true) {
        document.getElementById(idTh).style.display = 'block';
        i = 0;
        while (document.getElementById(ClientIdTableau + "_ctl" + i + "_trHeadAppartenance")){
            document.getElementById(ClientIdTableau + "_ctl" + i + "_trHeadAppartenance").style.display = 'block';
            i++;
        }
    }
    if (myInput.checked == false) {
        document.getElementById(idTh).style.display = 'none';
        i = 0;
        while (document.getElementById(ClientIdTableau + "_ctl" + i + "_trHeadAppartenance")){
            document.getElementById(ClientIdTableau + "_ctl" + i + "_trHeadAppartenance").style.display = 'none';
            i++;
        }
    }
}
function validationPonderation (sender,params)
{
    var idChamp = sender.control.id;
    idTemplate = idChamp.replace("_ponderationCritere", "");
    if (document.getElementById(idTemplate + "_criteresAttribution").value != "layerDefinitionCriteres_2") {
        return true;
    }

    if (document.getElementById(idChamp).value != "") {
        var valueChamp = parseInt(document.getElementById(idChamp).value);
    } else {
        var valueChamp = 0;
    }
    if (isNaN(valueChamp) == true) {
        return false;
    }
    var Ponderation = parseInt(document.getElementById(idTemplate + "_totalPonderation").value);
    Ponderation += valueChamp;
    if (Ponderation > 100) {
        return false;
    }
    return true;
}


function validationPonderationSave(sender, params) {
    var idChamp = sender.control.id;
    idTemplate = idChamp.replace("_ponderationCritere", "");
    if (document.getElementById(idTemplate + "_criteresAttribution").value != "layerDefinitionCriteres_2") {
        return true;
    }

    if (document.getElementById(idChamp).value != "") {
        var valueChamp = parseInt(document.getElementById(idChamp).value);
    } else {
        var valueChamp = 0;
    }

    if (isNaN(valueChamp) == true) {
        return false;
    }
    var Ponderation = parseInt(document.getElementById(idTemplate + "_totalPonderation").value);
    Ponderation += valueChamp;
    if ((Ponderation > 100) || (Ponderation < 100)) {
        return false;
    }
    return true;
}

function validationValeurPonderationSousCritee(sender, params) {
    var idChamp = sender.control.id;
    idTemplate = idChamp.replace("_ponderationSousCritere", "");
    if (document.getElementById(idTemplate + "_criteresAttribution").value != "layerDefinitionCriteres_2") {
        return true;
    }
    if (document.getElementById(idChamp).value != "") {
        var valueChamp = parseInt(document.getElementById(idChamp).value);
    } else {
        var valueChamp = 0;
    }
    if (isNaN(valueChamp) == true) {
        return false;
    }
    if (valueChamp <= 0 || valueChamp > 100) {
        return false;
    }
    return true;
}

function validationPonderationSousCritee (sender,params)
{
    var idChamp = sender.control.id;
    idTemplate = idChamp.replace("_ponderationSousCritere", "");
    if(document.getElementById(idTemplate + "_criteresAttribution").value != "layerDefinitionCriteres_2"){
        return true;
    }
    if(document.getElementById(idChamp).value != "" ){
        var valueChamp = parseInt(document.getElementById(idChamp).value);
    }else{
        var valueChamp = 0;
    }
    if(isNaN(valueChamp) == false && valueChamp > 0 && valueChamp <= 100){
        var index = document.getElementById(idTemplate + '_IndexItemPere').value;
        var total =  parseInt(document.getElementById(idTemplate + '_repeaterCritereAttribPonderation_ctl'+index+'_totalPonderationSousCritere').value);
        total = total+valueChamp;
        if(<?php echo Atexo_Config::getParameter('SOMME_PONDERATION_SOUS_CRITERE_CENT') ?>){
            var ponderationPere = 100;
        }else{
            var ponderationPere = parseInt(document.getElementById(idTemplate + '_repeaterCritereAttribPonderation_ctl'+index+'_criterePonderationHidden').value);
        }
        if(ponderationPere < total ){
            return false;
        }
    }
    return true;
}

function validationPonderationSousCriteeSave (sender,params)
{
    var idChamp = sender.control.id;
    idTemplate = idChamp.replace("_ponderationSousCritere", "");
    if(document.getElementById(idTemplate + "_criteresAttribution").value != "layerDefinitionCriteres_2"){
        return true;
    }
    i = 0;
    while(criterePonderation = document.getElementById(idTemplate + '_repeaterCritereAttribPonderation_ctl'+i+'_totalPonderationSousCritere')){
        var total = parseInt(criterePonderation.value);

        if(<?php echo Atexo_Config::getParameter('SOMME_PONDERATION_SOUS_CRITERE_CENT') ?>){
            var ponderationPere = 100;
        }else{
            var ponderationPere =  parseInt(document.getElementById(idTemplate + '_repeaterCritereAttribPonderation_ctl'+i+'_criterePonderationHidden').value);
        }
        if(total !=0 && ponderationPere != total ){
            return false;
        }
        i++;
    }
    return true;
}


function hideShowBlocReponse(myInput,myDiv1,myDiv2) {

    if (myInput.checked == true) {
        document.getElementById(myDiv1).style.display = 'none';
        document.getElementById(myDiv2).style.display = 'none';
    } else {
        document.getElementById(myDiv1).style.display = 'block';
        document.getElementById(myDiv2).style.display = 'block';
    }
}

function viderFormePrixConsultation(idComposant)
{
    var ladate = new Date();
    var mois = ladate.getMonth()+1;
    var annee = ladate.getFullYear();
    // Prix forfaitaire
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pf_estimationInterneHT").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pf_estimationInterneTTC").value = "";
    i = 1;
    while(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPF_ctl"+i+"_idPfVariation")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPF_ctl"+i+"_pf_variation").checked = false;
        i++;
    }

    //Prix unitaire
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_bc").checked = true;
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_bdcAvecMinimaxi").checked = true;
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_estimationInterneHT").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_estimationInterneTTC").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_puMax").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_puMin").value = "";

    i = 1;
    while(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterTypePrixPU_ctl"+i+"_idPUTypePrix")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterTypePrixPU_ctl"+i+"_pu_prixCatalogue").checked = false;
        i++;
    }
    i = 1;
    while(variationPrix = document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPU_ctl"+i+"_idPUVariation")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPU_ctl"+i+"_pu_variation").checked = false;
        i++;
    }

    //Prix Mixte
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pf_estimationInterneHT").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pf_estimationInterneTTC").value = "";

    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_bc").checked = true;
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_bdcAvecMinimaxi").checked = true;
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_estimationInterneHT").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_estimationInterneTTC").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pmPuMax").value = "";
    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pmPuMin").value = "";


    document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_formePrix").value = "FVIDE";

    i = 1;
    while(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPFPM_ctl"+i+"_idPMPFVariation")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPFPM_ctl"+i+"_pm_pf_variation").checked = false;
        i++;
    }
    i = 1;
    while(variationPrix = document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPUPM_ctl"+i+"_idPMPUVariation")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterVariationPrixPUPM_ctl"+i+"_pm_pu_variation").checked = false;
        i++;
    }
    i = 1;
    while(variationPrix = document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterTypePrixPUPM_ctl"+i+"_idPUPMTypePrix")){
        document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_repeaterTypePrixPUPM_ctl"+i+"_pm_pu_prixCatalogue").checked = false;
        i++;
    }

    isCheckedShowDiv(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_bdcAvecMinimaxi"),"pm_pu_bdcAvecMinimaxi_selectUnite");
    isCheckedShowDiv(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pm_pu_bdcAvecMinimaxi"),"pm_pu_avecMiniMaxi");
    showDiv("pm_pu_infos-min-max");
    isCheckedShowDiv(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_bdcAvecMinimaxi"),"pu_bdcAvecMinimaxi_selectUnite");
    isCheckedShowDiv(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_pu_bdcAvecMinimaxi"),"pu_avecMiniMaxi");
    showDiv("pu_infos-min-max");
    displayOptionChoice(document.getElementById("ctl0_CONTENU_PAGE"+idComposant+"_donneeFormePrix_formePrix"));
}

/*
* Permet d'afficher masquer les objets de recherche pour une categorie sélectionnée
*/
function updatePanelRecherche(mySelection) {
    if(mySelection) {
        var choixType =  mySelection.options[mySelection.selectedIndex].value;
    } else {
        //var choixType = '1;2;9;22;13;14;16;15;18;24;25';
        var choixType = '0;1;2;9;18;24;25;26';
    }
    var arrayIdsCategoriesSelected=choixType.split(';');
    var arrayIdsAllCategories = new Array('0','1','2','9','22','13','14','16','15','18','24','25','5','17','6','7','8','23','3','4','10','11','12','19','20','21','26','27','28','29','30','31', '32', '33','34','35');
    if(arrayIdsCategoriesSelected) {
        for(var i=0;i< arrayIdsAllCategories.length;i++) {//Masquer toutes les valeurs
            document.getElementById('div_objetRecherche_'+arrayIdsAllCategories[i]).style.display = 'none';
        }
        for(var i=0;i< arrayIdsCategoriesSelected.length;i++) {//Afficher les objets de recherche de la categorie sélectionnée
            document.getElementById('div_objetRecherche_'+arrayIdsCategoriesSelected[i]).style.display = 'block';
        }
        /*arrayUnset(arrayIdsAllCategories ,arrayIdsCategoriesSelected[i]);
        */

    } else {
        return false;
    }
    if(mySelection !== false && (mySelection.options[mySelection.selectedIndex].text == 'Contrats')
        || mySelection.options[mySelection.selectedIndex].text == 'Interface PLACE-CHORUS'){
        document.getElementById('critereFiltre_objet_contrat').style.display = 'block';
        document.getElementById('procedure_mise_en_ligne_aujourdhui').style.display = 'none';
        document.getElementById('filtresComplementairesObjetContrat').style.display = 'block';
        document.getElementById('critereFiltre_objet_autre_que_contrat').style.display = 'none';

    } else {
        document.getElementById('critereFiltre_objet_contrat').style.display = 'none';
        document.getElementById('procedure_mise_en_ligne_aujourdhui').style.display = 'block';
        document.getElementById('filtresComplementairesObjetContrat').style.display = 'none';
        document.getElementById('critereFiltre_objet_autre_que_contrat').style.display = 'block';
    }
    if(mySelection !== false) {
        document.getElementById('hidden-search-object').value = mySelection.options[mySelection.selectedIndex].text;
        setDefaultCheckedSearchObject(mySelection);
    }
}

function setDefaultCheckedSearchObject(widget){
    switch (widget.options[widget.selectedIndex].text) {
        case '<?php Prado::localize('CONSULTATION_STATISTIQUE') ?>':
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_1').checked = true;
            break;
        case '<?php Prado::localize('DCE_STATISTIQUE') ?>':
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_5').checked = true;
            break;
        case '<?php Prado::localize('PUBLICITES_STATISTIQUE') ?>':
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_3').checked = true;
            break;
        case '<?php Prado::localize('REPONSES_ELECTRONIQUE_STATISTIQUE') ?>':
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_10').checked = true;
            break;
        case '<?php Prado::localize('CONTRATS_STATISTIQUE') ?>':
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_27').checked = true;
            break;
        default:
            document.getElementById('ctl0_CONTENU_PAGE_objetRecherche_32').checked = true;

    }
}
/*
*Permet d'afficher le bloc des clauses de la recherche
*/
function updateRechercheParClause(){
    if(document.getElementById('ctl0_CONTENU_PAGE_clauseSocialeConditionExecution').checked == true){
        showPanel('panel_clausesSocialesInsertion');
    }
}

function arrayUnset(array, value){
    array.splice(array.indexOf(value), 1);
}

function enregistrerFormulaireSUB(cleExterneDossier,cleExterneDispositif) {
    document.getElementById('ctl0_CONTENU_PAGE_statutFormulaire').value = '';
    document.getElementById('ctl0_CONTENU_PAGE_cleExterneDossier').value = cleExterneDossier;
    document.getElementById('ctl0_CONTENU_PAGE_cleExterneDispositif').value = cleExterneDispositif;
    document.getElementById('ctl0_CONTENU_PAGE_updateFormulaire').click();
}

function cancelFormSUB() {
    if(document.getElementById('ctl0_CONTENU_PAGE_closeFormulairePub')){
        document.getElementById('ctl0_CONTENU_PAGE_closeFormulairePub').click();
    }else {
        window.close();
    }
}
function validerFormulaireSUB(cleExterneDossier) {
    document.getElementById('ctl0_CONTENU_PAGE_cleExterneDossier').value = cleExterneDossier;
    document.getElementById('ctl0_CONTENU_PAGE_statutFormulaire').value = '1';
    document.getElementById('ctl0_CONTENU_PAGE_updateFormulaire').click();
}
function validateVariationPrix(sender,params){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterVariationPrixPF_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterVariationPrixPF_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pf_variation")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}
function validateVariationPrixU(sender,params){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterVariationPrixPU_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterVariationPrixPU_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pu_variation")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}
function validateTypePrixU(sender,params){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterTypePrixPU_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterTypePrixPU_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pu_prixCatalogue")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}
function validateTypePrixM(sender,params){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterTypePrixPUPM_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterTypePrixPUPM_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pm_pu_prixCatalogue")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}

function validationVariationPrixMF(sender, parameter){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterVariationPrixPFPM_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterVariationPrixPFPM_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pm_pf_variation")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}
function validateVariationPrixMU(sender, parameter){
    var chaine = sender.control.id;
    if(chaine.indexOf('bloc_etapeDonneesComplementaires')== -1){
        chaineF = 'ctl0_CONTENU_PAGE_donneeFormePrix_repeaterVariationPrixPUPM_ctl';
    } else {
        chaineF = 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_repeaterVariationPrixPUPM_ctl';
    }
    i = 1;
    while (variation = document.getElementById(chaineF + i + "_pm_pu_variation")){
        if(variation.checked == true){
            return true;
        }
        i++;
    }
    return false;
}

function validerFormPrix(sender, parameter){
    result = true;
    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_errorFormPrixST') != null){
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_errorFormPrixST').style.display='none';
    }
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true && document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_alloti").checked == false){
        if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_marcheTranche").checked == true){
            i = 1;
            while (formePrix = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_listeTranchesMarchePrix_ctl" + i + "_formPrixTC"))
            {
                if(formePrix.value == '0'){
                    document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_listeTranchesMarchePrix_ctl"+i+"_errorFormPrixTC").style.display='';
                    result = false;
                }
                i++;
            }
        } else if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_formPrix").value == '0'){
            document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeFormePrix_errorFormPrixST').style.display='';
            result = false;
        }
    }
    if(result == false){
        return false;
    }
    return true;
}
function validerFormPrixPopupLot(sender, parameter){
    result = true;
    if(document.getElementById('ctl0_CONTENU_PAGE_donneeFormePrix_errorFormPrixST') != null){
        document.getElementById('ctl0_CONTENU_PAGE_donneeFormePrix_errorFormPrixST').style.display='none';
    }
    if(window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true){
        if(document.getElementById("ctl0_CONTENU_PAGE_donneeFormePrix_marcheTranche").checked == true){
            i = 1;
            while (formePrix = document.getElementById("ctl0_CONTENU_PAGE_donneeFormePrix_listeTranchesMarchePrix_ctl" + i + "_formPrixTC"))
            {
                if(formePrix.value == '0'){
                    document.getElementById("ctl0_CONTENU_PAGE_donneeFormePrix_listeTranchesMarchePrix_ctl"+i+"_errorFormPrixTC").style.display='';
                    result = false;
                }
                i++;
            }
        } else if(document.getElementById("ctl0_CONTENU_PAGE_donneeFormePrix_formPrix").value == '0'){
            document.getElementById('ctl0_CONTENU_PAGE_donneeFormePrix_errorFormPrixST').style.display='';
            result = false;
        }
    }
    if(result == false){
        return false;
    }
    return true;
}
function verifyConditionUtilisation()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation') && document.getElementById('ctl0_CONTENU_PAGE_accepterConditionsUtilisation').checked) {
        return true;
    } else {
        bootbox.alert("<?php Prado::localize('ERREUR_CONDITION_UTILISATION') ?>");
        return false;
    }
}
function selectAllLocalisationsItems(idListLocalisation) {
    selectBox = document.getElementById(idListLocalisation);
    for (var i = 0; i < selectBox.options.length; i++) {
        selectBox.options[i].selected = "selected";
    }
}

/*
* Permet de selectionner/deselectionner toutes les demandes de complèment
*/
function checkAllDemandesComplements(myInput, nbrElementsRepeater)
{
    if(myInput.checked)
    {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            //var cuestsCheck = document.getElementById('ctl0_CONTENU_PAGE_RepeaterResultats_ctl'+i+'_inviteSelection_1');
            var complementCheck = document.getElementById('ctl0_CONTENU_PAGE_tableauDeBordRepeater_ctl'+i+'_idTcheckboxComplement');
            if(complementCheck != null)
                complementCheck.checked = true;
        }

    }else
    {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var complementCheck = document.getElementById('ctl0_CONTENU_PAGE_tableauDeBordRepeater_ctl'+i+'_idTcheckboxComplement');
            if(complementCheck != null)
                complementCheck.checked = false;
        }
    }
}

function isCheckedCheckAllReponse(myInput,nbrElementsRepeater) {
    if (myInput.checked == true) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesElectronique_ctl" + i + "_check_list_reponseElec");
            if(checkElement != null)
                checkElement.checked = true;
        }

    }
    if (myInput.checked == false) {

        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesElectronique_ctl" + i + "_check_list_reponseElec");
            if(checkElement != null)
                checkElement.checked = false;
        }

    }
}

function isCheckedCheckAllReponsePapier(myInput,nbrElementsRepeater) {
    if (myInput.checked == true) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesPapiers_ctl" + i + "_check_list_reponsePapier");
            if(checkElement != null)
                checkElement.checked = true;
        }

    }

    if (myInput.checked == false) {

        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_enveloppeReponsesPapiers_ctl" + i + "_check_list_reponsePapier");
            if(checkElement != null)
                checkElement.checked = false;
        }

    }

}

function isAllElementChecked(myInput,repeaterName,checkName) {
    var nbrElementsRepeater = document.getElementById("ctl0_CONTENU_PAGE_Nombre_"+repeaterName).value;
    nbrElementsRepeater ++;
    if (myInput.checked == true) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+repeaterName+"_ctl" + i + checkName);
            if(checkElement != null)
                checkElement.checked = true;
        }

    }
    if (myInput.checked == false) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+repeaterName+"_ctl" + i + checkName);
            if(checkElement != null)
                checkElement.checked = false;
        }

    }
}
function utf8_encode( string ) {
    return unescape( encodeURIComponent( string ) );
}
function utf8_decode( string ) {
    return decodeURIComponent( escape( string ) );
}

/**
 * Permet d'encoder en base 64
 */
function base64_encode (data) {
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
        ac = 0,
        enc = "",
        tmp_arr = [];

    if (!data) {
        return data;
    }

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    var r = data.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}
/**
 *
 * Validateur de date de notification de tranche budgetaire
 */
function validateBlocIdentificationEse(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_etranger");
            var paysNational = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_Nationale");
            if(paysNational.checked == true){
                var siret = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_siret").value;
                var siren = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_siren").value;
                if((siren !='') && (siret!='')) {
                    if(isSiretValide(siret,siren)) {
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgError" ).style.display = 'none';
                    }
                    else {
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgError" ).style.display = '';
                        erreur = false;
                    }
                }
                else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgError" ).style.display = '';
                    erreur = false;
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgError" ).style.display = 'none';
        }

        i++;
    }

    return erreur;
}
function validateBlocIdentificationEsePays(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_etranger");
            if(paysEtrange.checked == true){
                var lisePays = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_listPays");
                if(lisePays != null){
                    if (lisePays.options[lisePays.selectedIndex].value == 0){
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorPays" ).style.display = '';
                        erreur = false;
                    } else {
                        document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorPays" ).style.display = 'none';
                    }
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorPays" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}

function validateBlocIdentification(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_etranger");
            if(paysEtrange.checked == true){
                var identifiant  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_identifiant_national").value;
                if (identifiant != ''){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorIdentifiant" ).style.display = 'none';
                } else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorIdentifiant" ).style.display = '';
                    erreur = false;
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorIdentifiant" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}

function showHideBlocEntreprise(identifiantRadio,identifiantNational,identifiantEtranger,identifiantCp,disabled) {
    var cp = identifiantCp.replace('blocEntreprise','')+'cp';
    isCheckedShowDiv(identifiantRadio,identifiantNational);
    isCheckedHideDiv(identifiantRadio,identifiantEtranger);
    if(cp != null){
        elementCp = document.getElementById(cp);
        if(disabled == '1'){
            elementCp.disabled = 'disabled';
            J('#'+cp).attr('disabled','disabled').addClass('disabled');
        } else {
            J('#'+cp).removeAttr('disabled','disabled').removeClass('disabled');
            elementCp.disabled = '';
        }
    }
}
function validateBlocIdentificationVilleRc(){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_etranger");
            var paysNational = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_Nationale");
            if(paysNational.checked == true){
                var rcVille = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_rcVille");
                if (rcVille.options[rcVille.selectedIndex].value == 0){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorRc" ).style.display = '';
                    erreur = false;
                } else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorRc" ).style.display = 'none';
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorRc" ).style.display = 'none';
        }

        i++;
    }
    return erreur;
}
function validatePaysAtributaire(sender, parameter){
    var erreur = true;
    var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_etranger");
    if(paysEtrange.checked == true){
        var lisePays = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_listPays");
        if (lisePays.options[lisePays.selectedIndex].value == 0){
            erreur = false;
        }
    }
    return erreur;
}
function validateSirenAttributaire(sender, parameter){
    var erreur = true;
    var paysNational = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_Nationale");
    if(paysNational.checked == true){
        var siret = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_siret").value;
        var siren = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_siren").value;
        if((siren !='') && (siret!='')) {
            if(!isSiretValide(siret,siren)) {
                erreur = false;
            }
        }
        else {
            erreur = false;
        }
    }
    return erreur;
}
function validateIdentifiantnationalAttributaire(sender, parameter){
    var erreur = true;
    var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_etranger");
    if(paysEtrange.checked == true){
        var identifiant = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_identifiant_national").value;
        if(identifiant==''){
            erreur = false;
        }
    }
    return erreur;
}

function validateVilleRcAttributaire(sender, parameter){
    var erreur = true;
    var paysNational = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_Nationale");
    if(paysNational.checked == true){
        var rcVille = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_rcVille");
        if (rcVille.options[rcVille.selectedIndex].value == 0){
            erreur = false;
        }
    }
    return erreur;
}
function validateNumRcAttributaire(sender, parameter){
    var erreur = true;
    var paysNational = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_Nationale");
    if(paysNational.checked == true){
        var numRc = document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_numero_Rc").value;
        if (numRc == ''){
            erreur = false;
        }
    }
    return erreur;
}
function validateBlocIdentificationNumRc(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysnationale  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_Nationale");
            if(paysnationale.checked == true){
                var numRc  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_numero_Rc").value;
                if (numRc != ''){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorNumRc" ).style.display = 'none';
                } else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorNumRc" ).style.display = '';
                    erreur = false;
                }
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorNumRc" ).style.display = 'none';
        }
        i++;
    }
    return erreur;
}
function controleSelectedImportedFile(){
    error = true;
    i=1;
    j=1;
    while (intituleFile = document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl" + i + "_enveloppeImportee"))
    {
        var chaine = intituleFile.value;
        var FileName = "";
        var fileText = document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl" + i + "_pieceText");
        if(intituleFile.value){
            var FileName =  chaine.replace("C:\\fakepath\\","");
            if (FileName == fileText.firstChild.nodeValue)
            {
                document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl"+i+"_ImgError" ).style.display = 'none';
            } else {
                error = false;
                document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl"+i+"_ImgError" ).style.display = '';
            }
        } else {
            j++
            error = false;
            document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl"+i+"_ImgError" ).style.display = '';
        }
        i++;
    }
    if( j== i){
        return true;
    }
    return error;
}
function controleSelectedImportedFileNull(){
    erreur = false;
    i=1;
    while (intituleFile = document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl" + i + "_enveloppeImportee"))
    {
        if (intituleFile.value != "" && intituleFile.value != null)
        {
            document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl"+i+"_ImgError" ).style.display = 'none';
            erreur = true;
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_listePiecesRepeater_ctl"+i+"_ImgError" ).style.display = '';
        }
        i++;
    }
    return erreur;
}
function validateMontantWithBorns(){
    var borneSuper =document.getElementById("ctl0_CONTENU_PAGE_bornSuperieur").value;
    var borneInfer = document.getElementById("ctl0_CONTENU_PAGE_bornInferieur").value;
    var montant = document.getElementById("ctl0_CONTENU_PAGE_montant").value;
    return compareTrancheBorn("ctl0_CONTENU_PAGE_montant","ctl0_CONTENU_PAGE_bornInferieur","ctl0_CONTENU_PAGE_bornSuperieur");
}
function compareTrancheBorn(montant,borneInfer,borneSuper){
    var borneSuper =document.getElementById(borneSuper).value;
    var borneInfer = document.getElementById(borneInfer).value;
    var montant = document.getElementById(montant).value;

    var borneSuper = borneSuper.replace(' ','');
    var borneSuper = parseFloat(borneSuper);

    var borneInfer = borneInfer.replace(' ','');
    var borneInfer = parseFloat(borneInfer);

    var montant = montant.replace(' ','');
    var montant = montant.replace(' ','');
    var montant = montant.replace(',','.');
    var montant = parseFloat(montant);

    if((borneSuper == 0) || (borneSuper == null)){
        if(montant >= borneInfer){
            return true;

        }
    }
    if((montant >= borneInfer) && (montant <= borneSuper) ){
        return true;
    }
    return false;
}

function isEntrepriseNationale(){
    if((document.getElementById("ctl0_CONTENU_PAGE_blocEntreprise_attributaire_Nationale").checked == true )
        && (document.getElementById("ctl0_CONTENU_PAGE_cp").value == '')
    )
    {
        return false;
    }
    return true;
}
/**
 *
 * Validateur de la tranche budgetaire
 */
function validateBorneTrancheButgetaire(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            ListeTranche = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_trancheBudgetaire");
            montant = "ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_montant";
            bornInf = "ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_bornInferieur";
            bonrSup = "ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_bornSuperieur";
            if(ListeTranche){
                if (ListeTranche.options[ListeTranche.selectedIndex] && ListeTranche.options[ListeTranche.selectedIndex].value)
                {
                    if(ListeTranche.options[ListeTranche.selectedIndex].value != 0){
                        if(!compareTrancheBorn(montant,bornInf,bonrSup)){
                            erreur = false;
                            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = '';
                        }
                    }
                }
            }
            if(erreur == true){
                if(document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" )){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_ImgErrortranche" ).style.display = 'none';
                }
            }
        }
        i++;
    }
    return erreur;
}

/*
* permet de changer disabled du code postal dans le cas d'ajout/modification depot papier
*/
function enabledCodePostal(enabled){
    document.getElementById("ctl0_CONTENU_PAGE_cp").disabled = enabled;

}


function validateOptionSelected(sender, parameter)
{
    var paramElement = parameter.split('#');
    if(document.getElementById(paramElement[0]).checked == false && document.getElementById(paramElement[1]).checked == false){
        return false
    }
    return true;
}

function refreshRepeaterListeSRA()
{
    var doc = window.opener.document;
    doc.getElementById('ctl0_CONTENU_PAGE_tableauSRA_refreshRepeater').click();
    window.close();
}

function supprimerEspace(element)
{
    element.value = element.value.trim();
}
function uncheckOther(element)
{
    if(document.getElementById(element)){
        document.getElementById(element).checked = false;
    }
}

/**
 * Permet de supprimer le validateur sur un bloc ou un champ
 *@param idForm: le client ID du formulaire <form></form>
 *@param partName: L'id du bloc
 */
function removeValidators(idForm, partName) {
    var i = 0;
    var length = Prado.Validation.managers[idForm].validators.length;
    while(i<length)
    {
        if(Prado.Validation.managers[idForm].validators[i].control.name.indexOf(partName)>0)
        {
            Prado.Validation.managers[idForm].removeValidator(Prado.Validation.managers[idForm].validators[i]);
        }
        else {
            i++;
        }
    }
}

function afficherLang(idRepeterLang,idImageLang) {
    var i = 0;
    while (document.getElementById(idRepeterLang + i + "_idBoutonChangerLangueConsultation") != null){
        document.getElementById(idRepeterLang + i + "_idBoutonChangerLangueConsultation").className = '';
        i++;
    }
    document.getElementById(idImageLang).className = 'on';
}

function isCheckedCheckAllInRepater(myInput,idRepeaterElement) {

    if (myInput.checked == true) {
        i=1;
        while (checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+idRepeaterElement+"_ctl" + i + "_check_list"))
        {
            checkElement.checked = true;
            i++;
        }
    }

    if (myInput.checked == false) {
        i=1;
        while (checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+idRepeaterElement+"_ctl" + i + "_check_list"))
        {
            checkElement.checked = false;
            i++;
        }
    }

}

function validateExerciceSignificatif(sender, parameter){
    resultat = true;
    i=1;
    while (significatif = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_exercice_significatif"))
    {
        var checkList = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_check_list");
        if(checkList && checkList.checked == true){
            if(significatif.value && !isNumeric(significatif.value)){
                resultat = false;
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgError").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgError").style.display = 'none';
            }
        }else{
            document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgError").style.display = 'none';
        }
        i++;
    }
    return resultat;
}

function validateDateDebut(sender, parameter){
    resultat = true;
    i=1;
    while (exerciceDateDebut = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_exercice_date_debut"))
    {
        var checkList = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_check_list");
        if(checkList && checkList.checked == true){
            if(exerciceDateDebut.value && !checkDateFr(exerciceDateDebut.value)){
                resultat = false;
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateDebut").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateDebut").style.display = 'none';
            }
        }else{
            document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateDebut").style.display = 'none';
        }
        i++;
    }
    return resultat;
}

function validateDateFin(sender, parameter){
    resultat = true;
    i=1;
    while (exerciceDateFin = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_exercice_date_fin"))
    {
        var checkList = document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl" + i + "_check_list");
        if(checkList && checkList.checked == true){
            if(exerciceDateFin.value && !checkDateFr(exerciceDateFin.value)){
                resultat = false;
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateFin").style.display = '';
            }else{
                document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateFin").style.display = 'none';
            }
        }else{
            document.getElementById("ctl0_CONTENU_PAGE_"+parameter+"_ctl"+i+"_ImgErrorDateFin").style.display = 'none';
        }
        i++;
    }
    return resultat;
}

function checkDateFr(date)
{
    var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
    var matchArray = date.match(datePat); // is the format ok?

    if (matchArray == null) {
        return false;
    }
    return true;
}

/**
 *
 * Permet de parcourir le tableau de bord des archives pour afficher une alerte pour les consultations sélectionnées pour le telechargement groupe
 * Et asynchrone
 */
function verifierCochesArchivesBoutonTelechargementGroupeAsynchrone()
{
    nbrElement = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_nombreResultatsRecherche').value;
    consStatutDecisionTrouve = false;
    consStatutAArchiverTrouve = false;
    for(i=1;i<= nbrElement;i++)
    {
        cocheListeArchives = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_toUpdate');
        if(cocheListeArchives != null && cocheListeArchives.value == "on" && cocheListeArchives.checked == true) {
            statutCons = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_statutConsultation') ;
            if(statutCons.value == <?php echo Atexo_Config::getParameter('STATUS_DECISION') ?>) {
                consStatutDecisionTrouve = true;
            } else if(statutCons.value == <?php echo Atexo_Config::getParameter('STATUS_A_ARCHIVER') ?>) {
                consStatutAArchiverTrouve = true;
            }
        }
    }
    if(consStatutDecisionTrouve == true) {
        openModal('modal-msg-archives-a-telecharger','modal-form popup-small2',document.getElementById('modal_demande_telechargement_statut_decision_found'));
    } else if(consStatutDecisionTrouve == false && consStatutAArchiverTrouve == true) {
        openModal('modal-confirmation-telechargement','modal-form popup-small2',document.getElementById('modal_confirmation_telechargement_archives'));
    }
}

/**
 *
 * Permet de parcourir le tableau de bord des archives pour afficher une alerte pour les consultations sélectionnées pour le passage au statut "A Archiver"
 */
function verifierCochesArchivesBoutonPasserStatutAArchiver()
{
    nbrElement = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_nombreResultatsRecherche').value;
    consStatutDecisionTrouve = false;
    consStatutAArchiverTrouve = false;
    compteurNbrConsStatutDecision = 0;
    for(i=1;i<= nbrElement;i++)
    {
        cocheListeArchives = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_toUpdate');
        if(cocheListeArchives != null && cocheListeArchives.value == "on" && cocheListeArchives.checked == true) {
            statutCons = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_statutConsultation') ;
            if(statutCons.value == <?php echo Atexo_Config::getParameter('STATUS_DECISION') ?>) {
                consStatutDecisionTrouve = true;
                compteurNbrConsStatutDecision ++;
            } else if(statutCons.value == <?php echo Atexo_Config::getParameter('STATUS_A_ARCHIVER') ?>) {
                consStatutAArchiverTrouve = true;
            }
        }
    }
    if(consStatutAArchiverTrouve == true) {
        openModal('modal-msg-a-archiver','modal-form popup-small2',document.getElementById('modal_message_statut_a_archiver'));
    } else if(consStatutAArchiverTrouve == false && consStatutDecisionTrouve == true) {
        if(compteurNbrConsStatutDecision == 1) {
            openModal('modal-msg-confirmation-passage-a-archiver','modal-form popup-small2',document.getElementById('modal_message_passage_a_archiver'));
        } else if(compteurNbrConsStatutDecision > 1) {
            openModal('modal-msg-confirmation-passage-a-archiver-plusieurs-cons','modal-form popup-small2',document.getElementById('modal_message_passage_a_archiver_plusieurs_cons'));
        }
    }
}

/**
 * Permet de gerer l'affichage du message de confirmation en fonction des cas (une consultation ou plusieurs consultations)
 */
function verifierCochesArchivesAfficherConfirmationPassageAArchiver()
{
    nbrElement = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_nombreResultatsRecherche').value;
    consStatutDecisionTrouve = 0;
    for(i=1;i<= nbrElement;i++)
    {
        cocheListeArchives = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_toUpdate');
        if(cocheListeArchives != null && cocheListeArchives.value == "on" && cocheListeArchives.checked == true) {
            statutCons = document.getElementById('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl'+i+'_statutConsultation') ;
            if(statutCons.value == <?php echo Atexo_Config::getParameter('STATUS_DECISION') ?>) {
                consStatutDecisionTrouve ++;
            }
        }
    }
    if(consStatutDecisionTrouve == 1) {
        openModal('modal-msg-confirmation-passage-a-archiver','modal-form popup-small2',document.getElementById('modal_message_passage_a_archiver'));
    } else if(consStatutDecisionTrouve > 1) {
        openModal('modal-msg-confirmation-passage-a-archiver-plusieurs-cons','modal-form popup-small2',document.getElementById('modal_message_passage_a_archiver_plusieurs_cons'));
    }
}

/**
 * Fonction permettant de valider le nombre de caracteres maximum de la saisie des numeros de telephone et fax
 * @param numeroTel: de tel ou fax
 * @param nbrCaracteresAutorises: nombre de caracteres autorises
 * @returns {Boolean}: true si tout se passe bien, false sinon
 */
function validerNombreCaracteresNumeroTelephoneFax(numeroTel, nbrCaracteresAutorises)
{
    if(numeroTel) {
        if(numeroTel.length <= nbrCaracteresAutorises) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

/**
 * Permet de valider le nombre de caracteres maximum de la saisie du champ "Numero de telephone" lors de l'ajout d'un dirigeant dans le compte entreprise
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validerNombreCaracteresNumTelFax(sender, parameter)
{
    if(validerNombreCaracteresNumeroTelephoneFax(parameter, <?php echo Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX') ?>)) {
        return true;
    }
    return false;
}

/**
 * Permet de valider les champs lt referentiel obligatoire, verifie l'existence du champs avant la validation (probleme lie au validaeur que prado ne supprime pas
 * @returns {Boolean}: true si validation OK, false sinon
 */

function ValidateLtReferentielZoneText(sender, parameter)
{
    res = true;
    if (document.getElementById(sender.control.id)) {
        if (parameter == null || parameter == "") {
            res = false;
        }
    }
    return res;
}
function validateFormatNumber(sender, parameter)
{
    res = true;
    if (document.getElementById(sender.control.id)) {
        var regExp = new RegExp("\[0-9]+[0-9]*((\.?|\,?)[0-9]{1,})?");
        if(parameter && !(parameter.match(regExp))){
            res = false;
        }
    }
    return res;
}

/**
 * Permet de gerer l'affichage du bloc "Mes coordonnees"
 */
function gererBlocCoordonnees()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixTelechargement').checked) {
        document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_blocMesCoordonnees').style.display='';
        document.getElementById('boutonAcceptationCGU').style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_libelleConditionsFormulaireDemandePart2').style.display='';
    } else{
        document.getElementById('boutonAcceptationCGU').style.display='None';
        document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_libelleConditionsFormulaireDemandePart2').style.display='None';
        document.getElementById('ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_blocMesCoordonnees').style.display='none';
    }
}


/**
 * Permet de valider si le champ est vide ou bien un nombre decimal
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validatorDecimalPositive(sender,parameter){
    valeur = parseFloat(parameter);
    if(parameter == "" || valeur >= 0 ){
        return true;
    }
    return false;
}

/**
 * Permet de valider la date de debut de l'exercice
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validatorDateDebutExercice(sender,parameter)
{
    numElement = parameter;
    dateDebut  = document.getElementById('ctl0_CONTENU_PAGE_debutExercice'+numElement).value;

    return validatorDateExercice(dateDebut,numElement)
}
/**
 * Permet de valider la date de fin de l'exercice
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validatorDateFinExercice(sender,parameter)
{
    numElement = parameter;
    dateFin  = document.getElementById('ctl0_CONTENU_PAGE_finExercice'+numElement).value;

    return validatorDateExercice(dateFin,numElement)
}

/**
 * Permet de valider la date de l'exercice
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validatorDateExercice(dateExercice,numElement)
{

    if( (document.getElementById('ctl0_CONTENU_PAGE_vente'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_vente'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_bien'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_bien'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_service'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_service'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_total'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_total'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_caA'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_caA'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_bA'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_bA'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_cfA'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_cfA'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_ceA'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_ceA'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_effectifMoyen'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_effectifMoyen'+numElement).value)>0 )
        || (document.getElementById('ctl0_CONTENU_PAGE_effectifEncadrement'+numElement).value != "" && parseFloat(document.getElementById('ctl0_CONTENU_PAGE_effectifEncadrement'+numElement).value)>0 )

    ){
        if(!dateExercice){
            return false;
        }

    }
    return true;
}

/**
 * Permet de valider la date de l'exercice
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validatorDateDebutFinExercice(sender,parameter)
{
    numElement = parameter;
    dateDebut  = document.getElementById('ctl0_CONTENU_PAGE_debutExercice'+numElement).value;
    dateFin  = document.getElementById('ctl0_CONTENU_PAGE_finExercice'+numElement).value;
    if(dateDebut && dateFin){
        var deb=dateDebut.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=dateFin.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];
        if(deb>fin){
            return false;
        }
    }
    return true;

}

/**
 * Permet d'initialiser les valeur de sauvegarde de la recherche
 */
function initialiserSauvegardeRecherche(nomRecherche,sauvrecherche,sauvAlerte,infoCompAlerte,idRecherche)
{
    if(!idRecherche) {
        if(nomRecherche){
            nomRecherche.value = '';
        }
        if(sauvrecherche){
            sauvrecherche.checked = false;
        }
        if(sauvAlerte){
            sauvAlerte.checked = false;
            isCheckedShowDiv(sauvAlerte,infoCompAlerte);
        }
    }
}

/**
 * Permet d'encoder la variable passee en parametre dans l'encodage de la plateforme contenue dans Atexo_Config::getParameter('HTTP_ENCODING')
 */
function toPfEncoding(chaine)
{
    charset_pf = "<?php echo Atexo_Config::getParameter('HTTP_ENCODING');?>";
    if(charset_pf != "utf8") {
        return utf8_decode(chaine);
    }
    return chaine;
}

function controllerValeursSaisieNumLot()
{
    if(parseInt(document.getElementById('ctl0_CONTENU_PAGE_numLot').value) == '0')
    {
        return false;
    }
    return true;
}

function enabledSiCreation (sender,parameter)
{
    var idProfile = sender.control.id;
    var idLigne = idProfile.replace('profils','');
    var idService = document.getElementById(idLigne+'service');
    if(idService.checked == true && parameter == 0){
        return false;
    }
    return true;
}

/**
 * Permet de valider les codes CPV pour le formulaire de consultation
 */
function ValidateCPVConsultation(sender, parameter)
{
    if(!document.getElementById(sender.control.id).value)
    {
        return false;
    }
    return true;

}

function addEntreprise(myInput,nameButton,idEse)
{

    if(myInput.checked == true && document.getElementById(idEse) && document.getElementById(idEse).value == ""){
        if(document.getElementById(nameButton)){
            document.getElementById(nameButton).click();
        }
    }else{
        return ;
    }
}

function isAllElementCheckedComposant(myInput,repeaterName,checkName,nbrElementId) {
    var nbrElementsRepeater = document.getElementById(nbrElementId).value;
    nbrElementsRepeater ++;
    if (myInput.checked == true) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+repeaterName+"_ctl" + i + checkName);
            if(checkElement != null){
                checkElement.checked = true;
            }
        }

    }
    if (myInput.checked == false) {
        for(i=1;i<= nbrElementsRepeater;i++)
        {
            var checkElement = document.getElementById("ctl0_CONTENU_PAGE_"+repeaterName+"_ctl" + i + checkName);
            if(checkElement != null){
                checkElement.checked = false;
            }
        }

    }
}


/**
 * Permet de valider les codes CPV pour le formulaire de consultation
 */
function ValidateCPVConsultation(sender, parameter)
{
    if(!document.getElementById(sender.control.id).value)
    {
        return false;
    }
    return true;
}

function showHideActiveInfoMsg($checkBox, panelId)
{
    if($checkBox.checked) {
        document.getElementById(panelId).style.display = '';
    }else{
        document.getElementById(panelId).style.display  = 'none';
    }
}

function controlValidationCodeEtab(siren,codeEtablissement)
{
    var siret = document.getElementById(codeEtablissement).value;
    if ((siren !=0) && (siret=='')) {
        if(isSirenValide(siren)) {
            return true;
        }
        return false;
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        return false;
    }
}

function getPrefix(sender)
{
    var senderControlId = sender.control.id;
    var arrayElement = senderControlId.split('_');
    arrayElement.pop();
    return arrayElement.join('_');
}

function validateTypeGroupement(sender, parameter){
    var prefix = getPrefix(sender);
    if(document.getElementById(sender.control.id).checked == false
        && document.getElementById(prefix+"_collaborateurCotraitantSolidaire").checked == false
        && document.getElementById(prefix+"_collaborateurCotraitantConjoint").checked == false
        && document.getElementById(prefix+"_collaborateurSoustraitant").checked == false){
        return false;
    }
    return true;
}

function validateBlocIdentificationunique(sender, parameter){
    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre == 'true'){
            var paysEtrange  = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_etranger");
            var paysNational = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_attributaire_Nationale");
            if(paysNational.checked == true){
                var identifiantUnique = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_blocEntreprise_identifiantUnique").value;
                if((identifiantUnique =='') || (!identifiantUnique)) {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorIdentifiantUnique" ).style.display = '';
                    erreur = false;
                }else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_blocEntreprise_ImgErrorIdentifiantUnique" ).style.display = 'none';
                }
            }
        }

        i++;
    }

    return erreur;
}


function ValidateMontantContrat(sender, parameter)
{
    var x = parameter;
    var x = x.replace(/ /g,"").replace(',','.');
    var y =  parseFloat(x);
    return ((x == y) && y > 0);

}

function VerifierFormatDateDecisionPhaseAttribution()
{
    dateStr = document.getElementById('ctl0_CONTENU_PAGE_dateDecision').value;
    var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
    var matchArray = dateStr.match(datePat);

    if (dateStr && matchArray == null) {
        return false;
    }
    return true;
}
function getPrefix(sender)
{
    var senderControlId = sender.control.id;
    var arrayElement = senderControlId.split('_');
    arrayElement.pop();
    return arrayElement.join('_');
}

function ValidateDatePrevNotificationFinMarche(sender, parameter)
{
    var prefix = getPrefix(sender);
    datePrevNotification = document.getElementById(prefix+'_dateNotification').value;
    datePrevFinMarche = document.getElementById(prefix+'_datePrevisionnelleFinMarche').value;
    if(datePrevNotification!="" && datePrevFinMarche!="") {
        var deb=datePrevNotification.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=datePrevFinMarche.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }
    return true;
}

function ValidateDatePrevFinMarcheMax(sender, parameter)
{
    var prefix = getPrefix(sender);
    datePrevFinMarche = document.getElementById(prefix+'_datePrevisionnelleFinMarche').value;
    datePrevFinMarcheMax = document.getElementById(prefix+'_datePrevFinMaxMarche').value;
    if(datePrevFinMarche!="" && datePrevFinMarcheMax!="") {
        var deb=datePrevFinMarche.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=datePrevFinMarcheMax.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }
    return true;
}

function ValidateTypeContrat()
{
    var typeDeContrat = document.getElementById('ctl0_CONTENU_PAGE_typeContrat');
    if (typeDeContrat.options[typeDeContrat.selectedIndex].value == 0)
    {
        document.getElementById('spanTypeContrat').style.display='';
        return false;
    } else {
        document.getElementById('spanTypeContrat').style.display='none';
        return true;
    }
}

function validateNumeroAC(sender, parameter){

    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_marcheSubsequent').value == 'true'){
        if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_numeroAC').value
            && document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_numeroACValue').value){
            return true;
        }else{
            return false;
        }
    }
    return true;
}

function ValidateDateReelleNotificationFinMarche(sender, parameter)
{
    var prefix = getPrefix(sender);
    datePrevNotification = document.getElementById(prefix+'_dateDefinitiveNotification').value;
    datePrevFinMarche = document.getElementById(prefix+'_dateDefinitiveFinMarche').value;
    if(datePrevNotification!="" && datePrevFinMarche!="") {
        var deb=datePrevNotification.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=datePrevFinMarche.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }
    return true;
}

function ValidateDateReelleFinMarcheFinMarcheMax(sender, parameter)
{
    var prefix = getPrefix(sender);
    datePrevNotification = document.getElementById(prefix+'_dateDefinitiveFinMarche').value;
    datePrevFinMarche = document.getElementById(prefix+'_dateDefinitiveFinMaxMarche').value;
    if(datePrevNotification!="" && datePrevFinMarche!="") {
        var deb=datePrevNotification.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        var fin=datePrevFinMarche.split("/");
        fin=fin[2]+"-"+fin[1]+"-"+fin[0];

        if(deb>fin){
            return false;
        }
    }
    return true;
}

function ValidateDateReelleNotificationDateJour(sender, parameter)
{
    var prefix = getPrefix(sender);
    datePrevNotification = document.getElementById(prefix+'_dateDefinitiveNotification').value;
    var now = new Date();
    var annee   = now.getFullYear();
    var mois    = now.getMonth() + 1;
    var jour    = now.getDate();
    dateJour = jour+"/"+mois+"/"+annee
    if(datePrevNotification!="" && dateJour!="") {
        var deb=datePrevNotification.split('/');
        deb=deb[2]+'-'+deb[1]+'-'+deb[0];

        if(jour<10) {
            jour = '0'+jour;
        }

        var fin=annee+"-"+mois+"-"+jour;

        if(deb<fin){
            return false;
        }
    }
    return true;
}

function validationDescriptionVariantesTechniques(sender, parameter)
{
    if (parameter == '' || !parameter)
    {
        return false;
    } else {
        return true;
    }
}

/*
* Permet de valider le siren/siret du titulaire
*/
function validationSiretTitulaire()
{
    var siren = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_siren').value;
    var siret = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_siret').value;
    if ((siren !='') || (siret !='')) {
        var isValidationDisabled = false;
        if (document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_compteEntrepriseIdUnique').value == '1') {
            isValidationDisabled = true;
        }
        return validerSiret(siren, siret, isValidationDisabled);
    }
    else {
        return false;
    }
}

/*
* Permet de valider le siren/siret du titulaire
*/
function validationSiretAttributaireNationale()
{
    idEntreprise = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idEntreprise').value
    if((!idEntreprise) && document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_attributaireNationale').checked){
        return validationSiretTitulaire();
    }else{
        return true;
    }
}

/*
* Permet de valider le siren/siret du titulaire
*/
function validationPaysAttributaireEtranger()
{

    idEntreprise = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idEntreprise').value
    if((!idEntreprise) && document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_attributaireEtranger').checked && document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_pays').selectedIndex == 0){
        return false;
    }else{
        return true;
    }
}

/*
* Permet de valider le siren/siret du titulaire
*/
function validationIdNationalAttributaireEtranger()
{
    idEntreprise = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idEntreprise').value
    if((!idEntreprise) && document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_attributaireEtranger').checked){
        if(document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idNational').value != ''){
            return true;
        }else{
            return false;
        }
    }
    else{
        return true;
    }
}

/**
 * Permet de valider le nombre de caracteres maximum de la saisie du champ "Numero de telephone" lors de l'ajout d'un dirigeant dans le compte entreprise
 * @returns {Boolean}: true si validation OK, false sinon
 */
function validerNombreCaracteresNumTelFaxObligatoire(sender, parameter)
{
    if(parameter != ''){
        if(validerNombreCaracteresNumeroTelephoneFax(parameter, <?php echo Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX') ?>)) {
            return true;
        }
        return false;
    }else{
        return false;
    }
}
function validationChampsAttributaire(sender, parameter)
{
    idEntreprise = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idEntreprise').value
    if(idEntreprise != '' || parameter != ''){
        return true;
    }else{
        return false;
    }
}


function validationCPAttributaire(sender, parameter)
{
    idEntreprise = document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_idEntreprise').value
    if(idEntreprise != '' || document.getElementById('ctl0_CONTENU_PAGE_listeAttributaires_attributaireEtranger').checked || (parameter != '' && !isNaN(parameter))){
        return true;
    }else{
        return false;
    }
}

function enabledCodePostalAttributaire(param)
{
    if(param != '' ){
        document.getElementById("ctl0_CONTENU_PAGE_listeAttributaires_cp").disabled = 'disabled';
    }else{
        document.getElementById("ctl0_CONTENU_PAGE_listeAttributaires_cp").disabled = '';
    }
}

/*Clear input default value onclick */
function clearOnFocusChamps(o){
    if(o.value=="Numéro long ou objet de l'accord cadre")
        o.value='';
}

function clearOnBlurChamps(o){
    if(o.value=='') o.value="Numéro long ou objet de l'accord cadre";
}

function afficherBlocNouveauxFournisseurs()
{
    var blocFournisseur = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_nbrNouveauxFournisseurs");
    for(var i=0;i < 50;i++){
        if(document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl'+i+'_fournisseur')) {
            document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl'+i+'_fournisseur').style.display='none';
        }
    }
    for(var i=0;i < blocFournisseur.value;i++){
        if(document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl'+i+'_fournisseur')) {
            document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl'+i+'_fournisseur').style.display='';
        }
    }
}


function refreshRepeaterPjFicheModificative()
{
    showLoader();
    window.opener.J(".cssAso").click();
    window.close();
}

function ValiderRadioBoutonsDateFinModifiee()
{
    var radioOui = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_dateFinMarcheModifOui");
    var radioNon = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_dateFinMarcheModifNon");

    if(!radioOui.checked && !radioNon.checked) {
        return false;
    }
    return true;
}

function validerSirenNouveauxFournisseursFicheModificative(sender,parameter)
{
    var i = parseInt(parameter);
    var radioNon = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_entrepriseEtablieFranceNon');
    if(radioNon.checked) {
        return true;
    }
    var nbrNouveauxFournisseurs = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_nbrNouveauxFournisseurs");
    if (nbrNouveauxFournisseurs.options[nbrNouveauxFournisseurs.selectedIndex].value != 0) {
        var siret = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_codeEtablissement').value;
        var siren = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_sirenEntreprise').value;
        if (i >= parseInt(nbrNouveauxFournisseurs.value)) {
            return true;
        }
        if ((siren != '') || (siret != '')) {
            return validerSiret(siren, siret);
        }
        return false;
    }
    return true;
}

function validerNomsNouveauxFournisseursFicheModificative(sender,parameter)
{
    var i = parseInt(parameter);
    var nbrNouveauxFournisseurs = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_nbrNouveauxFournisseurs");
    if (nbrNouveauxFournisseurs.options[nbrNouveauxFournisseurs.selectedIndex].value != 0) {
        var nomEtp = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_nomEntreprise').value;
        if (i >= parseInt(nbrNouveauxFournisseurs.value)) {
            return true;
        }
        if (nomEtp != '') {
            return true;
        }
        return false;
    }
    return true;
}

function validerTypesNouveauxFournisseursFicheModificative(sender,parameter)
{
    var i = parseInt(parameter);
    var nbrNouveauxFournisseurs = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_nbrNouveauxFournisseurs");
    if (nbrNouveauxFournisseurs.options[nbrNouveauxFournisseurs.selectedIndex].value != 0) {
        if (i >= parseInt(nbrNouveauxFournisseurs.value)) {
            return true;
        }
        var typeFournisseur = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_typeFournisseur');
        if (typeFournisseur.options[typeFournisseur.selectedIndex].value != 0) {
            return true;
        }
        return false;
    }
    return true;
}

function validerEtpFrancaiseNouveauxFournisseursFicheModificative(sender,parameter) {
    var i = parseInt(parameter);
    var nbrNouveauxFournisseurs = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_nbrNouveauxFournisseurs");
    if (nbrNouveauxFournisseurs.options[nbrNouveauxFournisseurs.selectedIndex].value != 0) {
        if (i >= parseInt(nbrNouveauxFournisseurs.value)) {
            return true;
        }
        var radioOui = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_entrepriseEtablieFranceOui');
        var radioNon = document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_repeatNouveauxFournisseurs_ctl' + i + '_entrepriseEtablieFranceNon');
        if (radioOui.checked || radioNon.checked) {
            return true;
        }
        return false;
    }
    return true;
}


function validerDateFinMarcheModifie()
{
    var dateFinMarcheModifieOui = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_dateFinMarcheModifOui");
    var dateFinMarcheModifie = document.getElementById("ctl0_CONTENU_PAGE_FicheModificative_dateFinMarcheModifiee");
    if(dateFinMarcheModifieOui.checked && !dateFinMarcheModifie.value) {
        return false;
    }
    return true;
}

function validerMontantActeFicheModificativeChorus(sender,parameter)
{
    if(!parameter) {
        return false;
    }
    return true;
}

/**
 *
 * Validateur de format des dates dans décision
 */
function ValidateFormatDate(sender, parameter){
    var parameterElement = parameter.split('#');

    i = 1;
    var erreur = true;
    while (checkIem = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_attributaire"))
    {
        nonAccordCadre = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_nonAccordCadreHidden");
        if(checkIem.checked == true && nonAccordCadre && nonAccordCadre.value == 'true'){
            valeur = document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl" + i + "_"+parameterElement[0]);

            if(valeur.value != ''){
                var newValue = valeur.value + ' 00:00';
                if(validateDateTime(valeur,newValue) == false){
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+parameterElement[1]).style.display = '';
                    erreur = false;
                } else {
                    document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+parameterElement[1]).style.display = 'none';
                }
            }
            else {
                document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+parameterElement[1]).style.display = 'none';
            }
        } else {
            document.getElementById("ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl"+i+"_"+parameterElement[1]).style.display = 'none';
        }
        i++;
    }
    return erreur;
}

function chorusValidationSiretDonneesContrat()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siretDonneesCandidat').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_sirenDonneesCandidat').value;
    if (siren =='' || siret =='') {
        return false;
    }
    if ((siren !='') || (siret !='')) {
        return controlValidationSiret(siren, siret);
    }
    else {
        return true;
    }
}

function chorusValiderMontantHtDonneesContrat()
{
    if(!document.getElementById('ctl0_CONTENU_PAGE_montantHtAccordDonneesCandidat').value) {
        return false;
    }
    return true;
}
function traitementConsAlloti(input) {
    isChecked2(input, 'ctl0_CONTENU_PAGE_etapesConsultation_etapeDetailsLots', 'ctl0_CONTENU_PAGE_etapesConsultation_etapeDetailsLotsInactif');
    isCheckedHideDiv(input, 'panel_achatResponsable');
    isCheckedHideDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelVariantesAutorisees');
    isCheckedHideDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_panelVariantesAutorisees');
    isCheckedHideDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelJustificationNonAllotie');
}



function traitementConsNonAlloti(input) {
    isChecked2(input, 'ctl0_CONTENU_PAGE_etapesConsultation_etapeDetailsLotsInactif', 'ctl0_CONTENU_PAGE_etapesConsultation_etapeDetailsLots');
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDetailsLotsInactif')&& document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDetailsLots')) {
        isChecked2(input, 'ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDetailsLotsInactif', 'ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDetailsLots');
    }
    isCheckedShowDiv(input, 'panel_achatResponsable');
    isCheckedShowDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelVariantesAutorisees');
    isCheckedShowDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_panelVariantesAutorisees');
    isCheckedShowDiv(input, 'ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelJustificationNonAllotie');
}

function compteurNbrCaracteresSaisisRestants(element, idCompteur, nbrCharMax) {
    document.getElementById(idCompteur).innerHTML = nbrCaracteresSaisisRestants(element.value, nbrCharMax);
}

function nbrCaracteresSaisisRestants(texte, nbrCharMax) {
    var nbrCaracteresRestants = nbrCharMax - texte.replace(/\[\/?\w+\]/g,'').length;
    return nbrCaracteresRestants + ((nbrCaracteresRestants < 2) ? ' caractére restant' : ' caractères restants');
}

function validerChoixTypeEnvoiChorus()
{
    if(!document.getElementById("ctl0_CONTENU_PAGE_typeEnvoiPj").checked && !document.getElementById("ctl0_CONTENU_PAGE_typeEnvoiActeModif").checked)
    {
        return false;
    }
    return true;
}

function gererAffichageOngletsTypeEnvoiChorusFicheModificatif()
{
    //Desactiver des onglets
    desactiverOngletPiecesJointesChorus();
    desactiverOngletFicheNavetteChorus();
    desactiverOngletDonneesContratChorus();
    desactiverOngletFicheRecensementChorus();
    //Masquer les onglet pieces jointes et fiches navettes
    document.getElementById('ctl0_CONTENU_PAGE_onglet1').style.display="none";
    document.getElementById('ongletLayer1').style.display="none";
    document.getElementById('ctl0_CONTENU_PAGE_onglet3').style.display="none";
    document.getElementById('ongletLayer2').style.display="none";
    //Activer l'onglet "Fiche Modificative"
    J( "#linkOnglet5" ).click();
}

function gererAffichageOngletsTypeEnvoiChorusPiecesJointes()
{
    //Desactiver des onglets
    desactiverOngletFicheRecensementChorus();
    desactiverOngletDonneesContratChorus();
}

function desactiverOngletPiecesJointesChorus()
{
    J('#ongletLayer1').find("input,button,textarea,select").attr("disabled", "disabled");
    J('#ongletLayer1'+' a').click(function(event){
        event.preventDefault();
    });
    document.getElementById('ctl0_CONTENU_PAGE_panelPiecesExternes').style.display="none";
}

function desactiverOngletFicheRecensementChorus()
{
    J('#ongletLayer2').find("input,button,textarea,select").attr("disabled", "disabled");
    J('#ongletLayer2'+' a').click(function(event){
        event.preventDefault();
    });
}

function desactiverOngletFicheNavetteChorus()
{
    J('#ongletLayer3').find("input,button,textarea,select").attr("disabled", "disabled");
    J('#ongletLayer3'+' a').click(function(event){
        event.preventDefault();
    });
}

function desactiverOngletDonneesContratChorus()
{
    J('#ongletLayer4').find("input,button,textarea,select").attr("disabled", "disabled");
    J('#ongletLayer4'+' a').click(function(event){
        event.preventDefault();
    });
}

function recentrerPopIn(css)
{
    J(window).scroll(function() {J('.'+css).dialog('option','position','center');});
}

function returnEntiteEligible(idsEntiteEligible, valueRadioBox)
{
    if (idsEntiteEligible!='_##_' && valueRadioBox == 1) {
        document.getElementById('spanEntityPurchese').style.display='';
        document.getElementById('spanMyEntity').style.display='none';
        document.getElementById('spanInvitePermanent').style.display='none';
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_idEntite').value = idsEntiteEligible;
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_isMyEntity').value = '1';
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_entiteEligible').click();
    } else if (idsEntiteEligible!='_##_' && valueRadioBox == 2) {

        document.getElementById('spanInvitePermanent').style.display='';
        document.getElementById('spanMyEntity').style.display='none';
        document.getElementById('spanEntityPurchese').style.display='none';
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_idInvite').value = idsEntiteEligible;
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_listInvite').value = idsEntiteEligible;
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_isMyEntity').value = '2';
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_inviteEligible').click();
    } else {
        document.getElementById('spanEntityPurchese').style.display='none';
        document.getElementById('spanInvitePermanent').style.display='none';
        document.getElementById('spanMyEntity').style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_isMyEntity').value = '0';

    }
}
function clickAssitance(){
    document.getElementById("ctl0_atexoUtah_boutonAccesUtah").click();
}

function getUid()
{
    return Math.random().toString(36).substr(2, 9);
}

/*
 * Permet de valider le siren/siret du titulaire
 */
function validationSiretCoTraitantNationale(sender,parameter)
{
    var paramElement = parameter.split('#');
    var siren = document.getElementById(paramElement[0]).value;
    var siret = document.getElementById(paramElement[1]).value;
    if ((siren !='') || (siret !='')) {
        return validerSiret(siren, siret);
    }
    else {
        return false;
    }
}

/*
 * Permet de valider le siren/siret du titulaire
 */
function validationSiretSousTraitantNationale(sirenId,siretId,imgError,labelSiretInvalide)
{
    var siren = document.getElementById(sirenId).value;
    var siret = document.getElementById(siretId).value;
    var message = '';
    if ((siren !='') || (siret !='')) {
        if(validerSiret(siren, siret)){
            document.getElementById(imgError).style.display='none';
            document.getElementById(labelSiretInvalide).innerHTML = '';
            return true;
        }else{
            message = '<?php Prado::localize('SIRET_INVALIDE') ?>';
        }
    }else{
        message = '<?php Prado::localize('CHAMPS_OBLIGATOIRE') ?>';
    }
    document.getElementById(labelSiretInvalide).innerHTML = message;
    document.getElementById(imgError).style.display='';
    return false;

}


function validationModalitesReconduction(sender,params)
{
    var idChamp = sender.control.id;
    var valueChamp = document.getElementById(idChamp).value;
    if(idChamp.indexOf("etapeDonneesComplementaires")!=-1){
        var checkedDonneeOrme = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var checkedMarcheReconductible = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeDureeMarche_marcheReconductible").checked;
    }else{
        var checkedDonneeOrme = window.opener.document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked;
        var checkedMarcheReconductible = document.getElementById("ctl0_CONTENU_PAGE_donneeDureeMarche_marcheReconductible").checked;
    }
    if(checkedDonneeOrme == true && checkedMarcheReconductible == true){
        if(valueChamp==""){
            return false;
        }
    }
    return true;
}


//////////////////////////////////////////////// NOTIFICATIONS AGENT
function refreshNotifications(){

    var url = "<?php echo Atexo_Config::getParameter('URL_PAGE_CALCULE_NOMBRE_NOTIFICATIONS')?>";

    J.get(url, function( data ) {
        J( ".notifications-count" ).html( data );
        if(data>0){
            J( ".notifications-count").show();
        }else{
            J( ".notifications-count").hide();
        }
        //alert( "Load was performed." );
    });
}


function refrechSelectTypeSignature(select, typeSignature){
    if(select.options[select.selectedIndex].value == typeSignature){
        document.getElementById('ctl0_CONTENU_PAGE_panelDocSignature').style="display:none";
    }else{
        document.getElementById('ctl0_CONTENU_PAGE_panelDocSignature').style="display:''";
    }
}

function updateChosenSelect(select, idHiddenOffre, valueSelectAll) {
    var result = [];
    var options = select && select.options;
    var opt;
    var selectAll = false;
    var allValue = [];
    for (var i = 0, iLen = options.length; i < iLen; i++) {
        opt = options[i];
        allValue.push(opt.value);
        if (opt.selected) {
            result.push(opt.value);
            if(valueSelectAll == opt.value){
                selectAll = true;
            }
        }
    }
    allValue.pop(valueSelectAll);
    if(selectAll){
        result = allValue;
    }
    document.getElementById(idHiddenOffre).value = result;
}

function resizeIframe(el) {
    var f = document.getElementById(el);
    jQuery('#iframeRedac').attr('style', 'height:'+(f.contentWindow.document.body.offsetHeight + 20)+ 'px !important');
}

function validerSelectionChoixSupportsPublicite()
{
    nbrSupports = document.getElementById('ctl0_CONTENU_PAGE_formulaireAjoutPublicite_nbrSupports').value;
    var nbreCoche = 0;
    for(var i=0;i< nbrSupports;i++)
    {
        element = document.getElementById('ctl0_CONTENU_PAGE_formulaireAjoutPublicite_listeSupportsPub_ctl'+i+'_support');
        if(element && element.checked == true)
        {
            nbreCoche++;
        }
    }
    if(nbreCoche == 0)
    {
        return false;
    }
    else{
        return true;
    }
}

function arrondirValeurElement(element){

    var valeur = document.getElementById(element).value;
    var valeur = valeur.replace(' ','');
    var newValeur = arrondirEspace(valeur,2,'',0);
    var mt = newValeur.replace(',','.');
    document.getElementById(element).value = mt;
}

function validerMontantEstimeMarche(sender,parameter)
{
    var idElement = sender.control.id;
    var elementValue = document.getElementById(idElement).value;
    if(elementValue == '0,00') {
        return false;
    }
    elementValue = elementValue.replace(/ /g,"");
    elementValue = elementValue.replace(',', '.');
    if(isNumeric(elementValue)) {
        return true;
    }
    return false;
}


function updateSelection(select,idHiddenOffre) {
    var result = [];
    var options = select && select.options;
    var opt;
    for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value || opt.text);
        }
    }
    document.getElementById(idHiddenOffre).value = result;
}

function updateMessageSupport(input, idHiddenMessageSupport) {
    var value = input.value;

    document.getElementById(idHiddenMessageSupport).value = value;
}

function validateListMotsDescriptifs(sender, param){
    res = true;
    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked == true){
        if (param == null || param == "") {
            res = false;
        }
    }
    return res;
}

function validatorSiteBoampComptePublicite(sender,parameter)
{
    var idElement = sender.control.id;
    var arrayElement = idElement.split('_');
    arrayElement.pop()
    var prefixeId = arrayElement.join('_');
    if(!document.getElementById(prefixeId + '_siteBoampDemo').checked && !document.getElementById(prefixeId + '_siteBoampProd').checked) {
        return false;
    }
    return true;
}

function actionChoixPubliciteRedac(idInput)
{
    var choixPubCocheOui = false;
    var choixPubCocheNon = true;
    var choixRedacCocheOui = false;
    var choixRedacCocheNon = true;

    var arrayElement = idInput.split('_');
    arrayElement.pop();
    var prefixeId = arrayElement.join('_');

    if(document.getElementById(prefixeId+'_intitulePubliciteOui')) {
        choixPubCocheOui = document.getElementById(prefixeId + '_intitulePubliciteOui').checked;
    }
    if(document.getElementById(prefixeId+'_intitulePubliciteNon')) {
        choixPubCocheNon = document.getElementById(prefixeId + '_intitulePubliciteNon').checked;
    }
    if(document.getElementById(prefixeId+'_intituleRedactionOui')) {
        choixRedacCocheOui = document.getElementById(prefixeId + '_intituleRedactionOui').checked;
    }
    if(document.getElementById(prefixeId+'_intituleRedactionNon')) {
        choixRedacCocheNon = document.getElementById(prefixeId + '_intituleRedactionNon').checked;
    } else {
        var choixRedacCocheNon = false;
    }

    if (choixPubCocheOui || choixRedacCocheOui) {
        activerOngletDonneesComplementaires();
        activerBlocsDonneesComplementaires();
        activerBlocsDonneesComptePublicite();
        activerOngletPublicite();
    } else if(choixPubCocheNon && choixRedacCocheNon) {
        desactiverOngletDonneesComplementaires();
        desactiverBlocsDonneesComplementaires();
        desactiverBlocsDonneesComptePublicite();
        desactiverOngletPublicite();
    } else if(choixRedacCocheOui && choixPubCocheNon) {
        activerOngletDonneesComplementaires();
        activerBlocsDonneesComplementaires();
        desactiverBlocsDonneesComptePublicite();
        desactiverOngletPublicite();
    }
}

function activerOngletDonneesComplementaires()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementairesInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementairesInactif').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementaires')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementaires').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementairesInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementairesInactif').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementaires')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementaires').style.display = '';
    }
}

function desactiverOngletDonneesComplementaires()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementairesInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementairesInactif').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementaires')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDonneesComplementaires').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementairesInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementairesInactif').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementaires')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDonneesComplementaires').style.display = 'none';
    }
}

function activerBlocsDonneesComplementaires()
{
    J('.panel-donnees-complementaires').css('display','block');
    J('.panel-donnees-redac .title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
    J('.panel-donnees-redac .title-toggle-open').removeClass('detail-toggle-inactive');
    J('.panel-donnees-redac .donnees-redac').removeClass('panel-off');
}

function desactiverBlocsDonneesComplementaires()
{
    J('.panel-donnees-complementaires').css('display','none');
    J('.panel-donnees-redac .title-toggle').addClass('detail-toggle-inactive');
    J('.panel-donnees-redac .donnees-redac').addClass('panel-off');
    J('.panel-donnees-redac .title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
}

function activerOngletPublicite()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultationInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultationInactif').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultation')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultation').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultationInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultationInactif').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultation')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultation').style.display = '';
    }
}

function desactiverOngletPublicite()
{
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultationInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultationInactif').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultation')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_PubliciteConsultation').style.display = 'none';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultationInactif')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultationInactif').style.display = '';
    }
    if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultation')) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_PubliciteConsultation').style.display = 'none';
    }
}

function activerBlocsDonneesComptePublicite()
{
    J('.panel-donnees-complementaires-compte-publicite').css('display','block');
    J('.donnees-compte-publicite .title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
    J('.donnees-compte-publicite .donnees-redac').removeClass('panel-off');
    J('.donnees-compte-publicite').removeClass('panel-off');
}

function desactiverBlocsDonneesComptePublicite()
{
    J('.panel-donnees-complementaires-compte-publicite').css('display','none');
    J('.donnees-compte-publicite').addClass('panel-off');
    J('.donnees-compte-publicite .title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
}

function cocherDecocherBoutonDonneesOrmeOui(idInput)
{
    var choixPubCocheNon = true;
    var choixRedacCocheNon = true;

    var arrayElement = idInput.split('_');
    arrayElement.pop();
    var prefixeId = arrayElement.join('_');

    if(document.getElementById(prefixeId+'_intitulePubliciteNon')) {
        choixPubCocheNon = document.getElementById(prefixeId + '_intitulePubliciteNon').checked;
    }
    if(document.getElementById(prefixeId+'_intituleRedactionNon')) {
        choixRedacCocheNon = document.getElementById(prefixeId + '_intituleRedactionNon').checked;
    }
    if(choixPubCocheNon && choixRedacCocheNon) {
        if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui")) {
            document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked = false;
        }
        if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeNon")) {
            document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeNon").checked = true;
        }
    } else {
        if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui")) {
            document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeOui").checked = true;
        }
        if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeNon")) {
            document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_donneeOrmeNon").checked = false;
        }
    }
}

function showOffreSupport(check, idDiv){
    var style = "display:none";
    if(check.checked){
        style = "display:''";
    }
    document.getElementById(idDiv).style= style;
}

function verifyPreferenceSupport(){
    var i =0;
    while(support = document.getElementById('ctl0_CONTENU_PAGE_listeSupports_ctl' + i + '_actif')){
        if(support.checked){
            offresSelected = false;
            var j = 0;
            while(offre = document.getElementById('ctl0_CONTENU_PAGE_listeSupports_ctl' + i + '_listeOffresSupport_ctl' + j + '_actifOffre')){
                if(offre.checked){
                    offresSelected = true;
                }
                j++;
            }
            if(!offresSelected){
                return false;
            }
        }
        i++
    }
    return true;
}

function checkSupportMemeGroupe(check, groupe){
    var i =0;
    while(support = document.getElementById('ctl0_CONTENU_PAGE_listeSupports_ctl' + i + '_actif')){
        groupeSupport = document.getElementById('ctl0_CONTENU_PAGE_listeSupports_ctl' + i + '_groupe').value;
        if(groupeSupport == groupe){
            support.checked = check.checked;
            showOffreSupport(support, 'ctl0_CONTENU_PAGE_listeSupports_ctl' + i + '_panelListeOffre');
            checkOffreSupport(support, i);
        }
        i++
    }
}
function checkOffreSupport(check, indexSupport){
    var i =0;
    while(offreSupport = document.getElementById('ctl0_CONTENU_PAGE_listeSupports_ctl'+ indexSupport + '_listeOffresSupport_ctl' + i + '_actifOffre')){
        offreSupport.checked = check.checked;
        i++
    }
}

function isAnnonceProjetAchat()
{
    var intituleAvis = document.getElementById("ctl0_CONTENU_PAGE_intituleAvis");
    ///
    if(intituleAvis.options[intituleAvis.selectedIndex].value==<?php echo Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT') ?>) {
        return true;
    }
    return false;
}

function traitementDumeDemande(input) {
    if (input.checked == true) {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDumeAcheteurInactif').style.display = 'none';
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDumeAcheteur').style.display = null;
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_activeInfoMsgDume_panelMessage').style.display = null;
        if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteurInactif')){
            document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteurInactif').style.display = 'none';
        }
        if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteur')) {
            document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteur').style.display = null;
        }

        paramTypeProcedureDume = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_paramTypeProcedureDume').value;
        style = "none";
        if(paramTypeProcedureDume == "1" || paramTypeProcedureDume == "+1" || paramTypeProcedureDume == "0" || paramTypeProcedureDume == "+0" ){
            style = "block";
        }
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelTypeProcedureDume').style.display = style;
        if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelDumeTypeFormulaire')){
            document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelDumeTypeFormulaire').style.display = null;
        }
    }else{
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_activeInfoMsgDume_panelMessage').style.display = 'none';
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDumeAcheteurInactif').style.display = null;
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_etapeDumeAcheteur').style.display = 'none';
        if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteurInactif')) {
            document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteurInactif').style.display = null;
        }
        if(document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteur')) {
            document.getElementById('ctl0_CONTENU_PAGE_etapesConsultationBas_etapeDumeAcheteur').style.display = 'none';
        }
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelTypeProcedureDume').style.display = 'none';
        if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelDumeTypeFormulaire')){
            document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelDumeTypeFormulaire').style.display = "none";
        }
    }
}

var TOKEN_ACCESS;
var MPE_ID_CONTEXTE;

function chargerLtDume(token, idContexteDume, urlDume){

    TOKEN_ACCESS = token;
    MPE_ID_CONTEXTE = idContexteDume;
    var scriptsList = [
        'inline.bundle.js',
        'polyfills.bundle.js',
        'scripts.bundle.js',
        'vendor.bundle.js',
        'main.bundle.js'
    ];
    var cssList = [
        'styles.bundle.css'
    ];

    addScriptElmt(scriptsList, urlDume, true);
    addCssElmt(cssList, urlDume);
}

function chargerChosenDepartementParution() {
    J(".chosenDepartementParution").chosen({
        disable_search: false,
        no_results_text: "Aucun résultat pour : ",
        placeholder_text_multiple: "Selectionnez"
    });

    J('.chosenDepartementParution').chosen().change(function(event, data) {
        var select = event.currentTarget.id;
        var selectSplit = select.split('ctl');
        var index = selectSplit[2].split('_')[0];

        var hiddenField = document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultation_listeSupport_ctl'+index+'_hiddenDepartementsParution');

        if (data.selected) {
            hiddenField.value += data.selected + ',';
        }

        if (data.deselected) {
            hiddenField.value = hiddenField.value.replace(data.deselected + ',', '');
        }
    });
}

function addScriptElmt(scriptsList, baseUrl, forceReload) {
    var forceReload = (typeof forceReload !== 'undefined') ? forceReload : true;
    if (scriptsList.length > 0) {
        var currentScript = scriptsList.shift();
        var script = document.createElement('script');

        script.src = baseUrl + currentScript;

        script.onload = function() {
            addScriptElmt(scriptsList, baseUrl, forceReload);
        };

        // On n'ajoute les fichiers que s'ils ne sont pas déjà présents ou s'il faut forcer.
        if (forceReload
            || J('script[src="' + script.src + '"]').length < 1
            && J('script[src="' + script.src.substr(window.location.origin.length) + '"]').length < 1) {
            document.body.appendChild(script);
        }
    }
}
function addCssElmt(cssList, urlDume) {
    cssList.forEach(function(currentCss) {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = urlDume + currentCss;
        link.media = 'all';
        head.appendChild(link);
    });
}

function saveDumeAcheteurFrom(from) {
    J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();

    if (
        verifyEtape('etapeDumeAcheteur') &&
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').value == 0
    ) {
        J("#UpdateDumeAcheteur").trigger('click');
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value = from;
        showLoader();
        return false;
    }

    return true;
}

function enregistrementDumeAcheteurSucces() {
    document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').value = 1;

    if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value == 'fromEtape') {
        document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_enregistrementEtapeDume').click();
    } else if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value == 'save') {
        document.getElementById('ctl0_CONTENU_PAGE_save').click();
    } else if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value == 'saveBack') {
        document.getElementById('ctl0_CONTENU_PAGE_saveBack').click();
    } else if (document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value == 'validateBack') {
        document.getElementById('ctl0_CONTENU_PAGE_validerElaboration').click();
    }

    J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();
    hideLoader();
    remonter();
}

function enregistrementDumeAcheteurErreur(message){
    J('#ctl0_CONTENU_PAGE_messageErreurDume_labelMessage').html(message);
    J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').show();
    J('#ctl0_CONTENU_PAGE_blocBoutons').show();
    showLoader();
    remonter();
}

function ValidateTypeProcedureDume(sender, param){
    if (document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_panelTypeProcedureDume").style.display != "none") {
        if (document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_dumeDemande").checked) {
            if (param == 0) {
                return false;
            }
        }
    }

    return true;
}

function saveDumeAcheteurWithoutEtapeFrom(){
    J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();

    if (
        J('#ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_bloc_etapeDumeAcheteur').is(':visible') &&
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').value == 0
    ) {
        J("#UpdateDumeAcheteur").trigger('click');
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saveDumefrom').value = '';
        showLoader();
        return false;
    }

    return true;
}

function getPrefixFromId(id)
{
    var senderControlId = id;
    var arrayElement = senderControlId.split('_');
    arrayElement.pop();
    return arrayElement.join('_');
}
function gererAffichaDetailgeSupport(element) {
    var prefix = getPrefixFromId(element.id);
    if(element.checked ) {
        J('#'+prefix+'_panelSuppport').show();
    }else {
        J('#'+prefix+'_panelSuppport').hide();
    }
}

/**
 *
 * Permet de valider la forme du prix
 */
function validateFormePrix(sender, parameter) {
    var prefix = getPrefix(sender);

    if (
        document.getElementById(prefix+"_ferme").checked == false &&
        document.getElementById(prefix+"_ferme_actualisable").checked == false &&
        document.getElementById(prefix+"_revisable").checked == false
    ) {
        return false;
    }

    return true;
}

/*
* Permet de valider le siren/siret du titulaire
*/
function validationSiretTitulaireModificationContrat()
{
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    if ((siren !='') && (siret !='')) {
        return validerSiret(siren, siret);
    }
    else {
        return false;
    }
}


function validateMarcheDefense(sender, parameter) {
    var prefix = getPrefix(sender);
    if (document.getElementById(prefix+"_Oui").checked == false
        && document.getElementById(prefix+"_Non").checked == false) {
        return false;
    }
    return true;
}

function resizeFrameGeneral(el) {
    var f = document.getElementById(el);
    jQuery('#'+el).attr('style', 'height:'+(f.contentWindow.document.body.offsetHeight + 20)+ 'px !important');
}

function validationVariantesTechniques(sender, $parameter)
{
    var prefix = getPrefix(sender);
    if (document.getElementById(prefix+"_Oui").checked == false
        && document.getElementById(prefix+"_Non").checked == false) {
        return false;
    }
    return true;
}

function validerMontant(sender,parameter)
{
    var idElement = sender.control.id;
    var elementValue = document.getElementById(idElement).value;
    if(!elementValue || elementValue == '0,00') {
        return false;
    }
    elementValue = elementValue.replace(/ /g,"");
    elementValue = elementValue.replace(',', '.');
    if(isNumeric(elementValue)) {
        return true;
    }
    return false;
}

function ValidateTypeFormulaireDume(sender,parameter)
{
    if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_dumeDemande')
        && document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_dumeDemande').checked
        && document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard')
        && document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireSimplifie')
    ){
        if(document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard').checked
            || document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireSimplifie').checked){
            return true;
        }else{
            return false;
        }
    }
    return true;

}

function confirmChangeDumeType(dumeA, label)
{
    if(document.getElementById(dumeA).value)
    {
        openModal('changement-dume-type','popup-small2', document.getElementById(label));
        return false;
    }
    return true;
}

function changeDumeType()
{
    if(J('#ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard').attr("checked") == "checked")
    {
        J('#ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireSimplifie').attr("checked",true);
        J('#ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard').removeAttr("checked");
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireSimplifie').checked = true;
    } else {
        J('#ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard').attr("checked",true);
        J('#ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireSimplifie').removeAttr("checked");
        document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_typeFormulaireStandard').checked = true;
    }
    J('.changement-dume-type').dialog('destroy');
    return true;
}

function getRGFileChange(newFileName,pathPrefix)
{
    var doc = window.opener.document;

    // Nom du nouveau Fichier
    var labelNewDoc = doc.getElementById(pathPrefix+'_newDocRg');
    if(labelNewDoc.childNodes[0]){
        labelNewDoc.removeChild(labelNewDoc.childNodes[0]);
    }
    var newDoc = doc.createTextNode(newFileName);
    labelNewDoc.appendChild(newDoc);

    var divOld = doc.getElementById('divBlocPiecesRgMoved');
    divOld.style.display = 'block';

}

function getDCEFileChange(msgChenged,pathPrefix)
{
    if(msgChenged){
        var doc = window.opener.document;

        // Nom du nouveau Fichier
        var labelNewDoc = doc.getElementById(pathPrefix+'_DCEChangedLabel');
        if(labelNewDoc.childNodes[0]){
            labelNewDoc.removeChild(labelNewDoc.childNodes[0]);
        }
        var newDoc = doc.createTextNode(msgChenged);
        labelNewDoc.appendChild(newDoc);

        var divOld = doc.getElementById('divBlocPiecesDCEChanged');
        divOld.style.display = 'block';
    }

}

function formatMontant(id){

    var valeur = document.getElementById(id).value;
    if(valeur == ''){
        return valeur;
    }
    return  formatterMontant(id);
}

/**
 * Permet de valider les codes CPV pour le formulaire de consultation
 */
function ValidateCPVIdentificationConsultation(sender, parameter)
{
    var paramElement = parameter.split('#');
    if(document.getElementById(paramElement[0]) && document.getElementById(paramElement[1])){

        var enabledElement = document.getElementById(paramElement[0]).value;
        var cpvValue = document.getElementById(paramElement[1]).value;
        if((enabledElement === '1') && !cpvValue)
        {
            return false;
        }
    }
    return true;
}

/**
 *
 * @param myPanel
 * @param myElement
 * @param clauseSocialeAteliersProteges
 * @param clauseSocialeSIAE
 */
function showPanelMarcheReserve(myPanel, myElement, clauseSocialeAteliersProteges, clauseSocialeSIAE, clauseSocialeESS){
    J('#'+myPanel).toggle();
    if(J('#'+myElement).is(":not(:checked)")){
        J('#' + clauseSocialeAteliersProteges).removeAttr('checked');
        J('#' + clauseSocialeAteliersProteges).removeAttr('disabled');

        J('#' + clauseSocialeSIAE).removeAttr('checked');
        J('#' + clauseSocialeSIAE).removeAttr('disabled');

        J('#' + clauseSocialeESS).removeAttr('checked');
        J('#' + clauseSocialeESS).removeAttr('disabled');
    }
}

/**
 *
 * @param myPanel
 * @param myElement
 * @param elementChosen
 */
function showPanelChosen(myPanel, myElement, elementChosen, elementHidden){
    J('#'+myPanel).toggle();
    if(J('#'+myElement).is(":not(:checked)")){
        J('#'+elementChosen+' option').each(function(){
            J(this).removeAttr('selected');
        });
        J('#' + elementHidden).val('').trigger("chosen:updated");
        J('#' + elementChosen).val('').trigger("chosen:updated");

    }
}

function resetError(){
    document.getElementById('ctl0_CONTENU_PAGE_ctl1_serverError').innerHTML='';
}

/* configuration par défaut de la lib filesizejs */
function fileSize(octets){
    return filesize(octets,{locale:"fr",symbols: {B: "o",KB: "Ko",MB: "Mo",TB: "To"}});
}

function chargerAnnoncesConcentrateur(urlPf, divElement, params, scriptsList) {
    var divContent = '<' + divElement;
    Object.keys(params).forEach(function (key) {
        var value = params[key];
        divContent = divContent + ' ' + key + "='" + value + "'";
    });
    divContent = divContent + ' />';
    var element = document.getElementById(divElement);
    if(element){
        element.innerHTML = divContent;
    }
    addScriptElmt(scriptsList, urlPf);
}

function chargerAnnoncesConcentrateurConsultationFormulaire(urlPf, scriptsList) {
    addScriptElmt(scriptsList, urlPf);
}


function checkRgpd(agentCommunicationPlaceRgpd, agentEnqueteRgpd) {

    if (agentCommunicationPlaceRgpd == '1') {
        J('#communicationPlaceRgpd').prop("checked", true);
        J('#communicationPlaceRgpd').parent().removeClass('btn-default');
        J('#communicationPlaceRgpd').parent().addClass('btn-success');
        J('#communicationPlaceRgpd').parent().addClass('on');
    }

    if (agentEnqueteRgpd == '1') {
        J('#enqueteRgpd').prop("checked", true);
        J('#enqueteRgpd').parent().removeClass('btn-default');
        J('#enqueteRgpd').parent().addClass('btn-success');
        J('#enqueteRgpd').parent().addClass('on');
    }
}

function checkRgpdEntreprise(communicationPlaceRgpd, enqueteRgpd, communicationRgpd)
{
    checkRgpd(communicationPlaceRgpd, enqueteRgpd);

    if (communicationRgpd == '1') {
        J('#communicationRgpd').prop("checked", true);
        J('#communicationRgpd').parent().removeClass('btn-default');
        J('#communicationRgpd').parent().addClass('btn-success');
        J('#communicationRgpd').parent().addClass('on');
    }
}

function language(lang) {
    J('.select_wrap label').html(J('#lang_' + lang).html());

    J('.select_wrap').click(function() {
        J('.select_wrap ul').slideToggle(200);
        J("html, body").animate({ scrollTop: J(document).height() }, 1000);
    });
    J('.select_wrap ul li').click(function() {
        var affichage = J(this).html();
        var valeur = J(this).attr('data-value');
        J('.select_wrap label').html(affichage);
        J('.select_wrap input').val(valeur);
    });
}
function hideBlocButton() {
    document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display = 'none';
}

function isBtnAnalysisEnabled(habilitation) {
    if(habilitation == 0) {
        var linkAnalysisRanking = document.getElementById('ctl0_CONTENU_PAGE_linkAnalysisRanking');
        if (linkAnalysisRanking) {
            linkAnalysisRanking.setAttribute("disabled", "disabled");
            linkAnalysisRanking.classList.add("disabled");
        }
        var linkAnalysisRankingCandidature = document.getElementById('ctl0_CONTENU_PAGE_linkAnalysisRankingCandidature');
        if (linkAnalysisRankingCandidature) {
            linkAnalysisRankingCandidature.setAttribute("disabled", "disabled");
            linkAnalysisRankingCandidature.classList.add("disabled");
        }
        var linkAnalysisRankingOffre = document.getElementById('ctl0_CONTENU_PAGE_linkAnalysisRankingOffre');
        if (linkAnalysisRankingOffre) {
            linkAnalysisRankingOffre.setAttribute("disabled", "disabled");
            linkAnalysisRankingOffre.classList.add("disabled");
            document.getElementById('btnAnalysisEnabling').style.cursor = 'not-allowed';
        }
    }
}

// validateDCERestreint
function validateDCERestreintFormulaireConsultation(sender, parameter)
{
    res = true;
    valeur = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_accessCodeDCE").value;

    if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_accesRestreintDCE").checked == true && (valeur == 0 || valeur == '') )
    {
        res = false;
    }

    return res;
}

function validerConditionsParticipation(sender, parameter)
{
    activiteProfessionel = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_activiteProfessionel").value;
    economiqueFinanciere = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_economiqueFinanciere").value;
    techniquesProfessionels = document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_techniquesProfessionels").value;

    if (
        ('' !== activiteProfessionel && 5 > activiteProfessionel.length)
        || ('' !== economiqueFinanciere && 5 > economiqueFinanciere.length)
        || ('' !== techniquesProfessionels && 5 > techniquesProfessionels.length)
    ) {
        return false;
    }
    if ('' !== activiteProfessionel && 5 <= activiteProfessionel.length) {

        return true;
    }
    if ('' !== economiqueFinanciere && 5 <= economiqueFinanciere.length) {
        return true;

    }
    if ('' !== techniquesProfessionels && 5 <= techniquesProfessionels.length) {
        return true;
    }

    return false;
}

function disableCheckBoxSelection(idComposant)
{
    document.getElementById(idComposant).disabled = 'disabled';
}

function validateAchatResponsable(index)
{
    const achatResponsable = window.achatResponsable;
    achatResponsable.validate();
    const clauses = achatResponsable.formValueToJson;
    J('input[id$="achatResponsableFormValue"]').val(clauses);

    if(typeof achatResponsable.clauses[index] != 'undefined') {
        return achatResponsable.clauses[index].isValid;
    }

    return true;
}

function controlValidationSiretForEntreprise()
{
    var siret = document.getElementById('ctl0_CONTENU_PAGE_siret').value;
    var siren = document.getElementById('ctl0_CONTENU_PAGE_siren').value;

    if ((siren !=0) && (siret==0)) {
        if(isSirenValide(siren)) {
            return true;
        }
        else {
            return false;
        }
    }
    if((siren !=0) && (siret!=0)) {
        if(isSiretValide(siret,siren)) {
            return true;
        }
        else {
            return false;
        }
    }
}


function checkModalitesReponse()
{
    var envCand = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_envelopCandidature');
    var envOffre = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_envelopOffre');
    var envAnonymat = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_anonymat');

    var  res = false;
    if(
        (envCand && envCand.checked == true)
        ||  (envOffre && envOffre.checked == true)
        ||  (envAnonymat && envAnonymat.checked == true)
    ){
        res = true;
    }
    return res;
}