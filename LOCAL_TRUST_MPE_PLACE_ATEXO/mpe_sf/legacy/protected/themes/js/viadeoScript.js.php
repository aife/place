
VD.init({
    apiKey: '<?php echo Atexo_Config::getParameter('VIADEO_API_KEY'); ?>', //Param Client 
    status: true,  
    cookie: true  
});  

function partagerCons() {
	showLoaderPartagerConsultation();
	param=new Object();
	param.message=document.getElementById("ctl0_CONTENU_PAGE_resultSearch_messageViadeo").value+" "+document.getElementById("ctl0_CONTENU_PAGE_resultSearch_urlViadeo").value;
	
	VD.api('/status', 'post',param, function(r) {  
		hideDiv('message');
		if(r!=null && (r.created==true || r.error.type == "CONFLICT")){
			showDiv('confirmationViadeo');
			
		}
		else {
			showDiv('erreurViadeo');
		}
		showLoaderPartagerConsultation();
    }); 
    
}


function calls() {  
	
   /* méthode appelé après authentification */  
   VD.api('/me', function(r) { 
	showLoader();
	    showDiv('message');
		hideDiv('confirmationViadeo'); 
		hideDiv('erreurViadeo');
		
	    J("#panelViadeo").dialog({
			modal: true,
			resizable: false,
			closeText:'Fermer',
			title:'<div class="pull-left" id="user">'+
				'<a class="user" target="_blank" href="'+r.link+'">'+
				'<img src="'+r.picture_large+'" alt="" class="img" />'+r.name+'</a></div>',
			dialogClass: 'share-message-panel',
			open: function(){
				jQuery('.ui-widget-overlay').bind('click',function(){
					jQuery('#panelViadeo').dialog('close');
				})
			}
		});	
    });  
} 

function setValue(ref,intitule,url,urlViadeo,idMsg,idUrl,idUrlViadeo) {
	
	document.getElementById(idMsg).value = intitule;
	document.getElementById(idUrl).value = url;
	document.getElementById(idUrlViadeo).value = urlViadeo;

	document.getElementById(idMsg).setAttribute('maxlength',<?php echo Atexo_Config::getParameter('CARACTERES_MAX_ENVOI_VIADEO_SANS_URL');?>);

	titreViadeo = document.getElementById('titreViadeo');
	if(titreViadeo.childNodes[1])
		titreViadeo.removeChild(titreViadeo.childNodes[1]);
	if(titreViadeo.childNodes[0])
		titreViadeo.removeChild(titreViadeo.childNodes[0]);
	var strong=document.createElement("strong")
	var t=document.createTextNode(ref);
	strong.appendChild(t);
	var newTitreViadeo = document.createTextNode(' - '+intitule);
	titreViadeo.appendChild(strong);
	titreViadeo.appendChild(newTitreViadeo);
}

function connect() {

	VD.getLoginStatus(function(r) {  
	    if (!r.session) {  
	        VD.login(function(r) {  
	          if(r.session){  
	             calls();  
	          }  
	        });
	    } else {  
	       calls();  
	    }  
	}); 
}

function clickViadeo(ref,intitule,urlMpe) {
	
	var xhr_object = null;
 	showLoader();
	if(window.XMLHttpRequest) // Firefox
	   xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject) // Internet Explorer
	   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non supporté par le navigateur
	   return;
	}

	url = "?page=Entreprise.UrlViadeo&url="+encodeURIComponent(urlMpe);
	
	xhr_object.open("GET", url, true);
	
	xhr_object.onreadystatechange = function() {
	   if(xhr_object.readyState == 4) {
		   setValue(ref,intitule,urlMpe,xhr_object.responseText,"ctl0_CONTENU_PAGE_resultSearch_messageViadeo","ctl0_CONTENU_PAGE_resultSearch_urlMPE","ctl0_CONTENU_PAGE_resultSearch_urlViadeo");
		   connect();
		  
	   }
	}
	
	xhr_object.send(null);
}

function showLoaderPartagerConsultation() {
		el = document.getElementById("ctl0_CONTENU_PAGE_resultSearch_pageLoader");
		if(el.style.display == "none") {
			el.style.display = "block";
		}
		else {
			el.style.display = "none";
		}
}