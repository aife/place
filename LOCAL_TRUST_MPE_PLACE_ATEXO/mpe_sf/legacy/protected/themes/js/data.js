/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/*Data autocomplete recherche Entités*/
var completeResults = [
{ "permalink": "#", "title": "CG77 - Conseil général de Seine-et-Marne (77000 - Melun)" },
{ "permalink": "#", "title": "CG78 - Conseil général des Yvelines (78000 - Versailles)" },
{ "permalink": "#", "title": "CG91 - Conseil général de l'Essonne (91000 - Evry)" },
{ "permalink": "#", "title": "CG92 - Conseil général des Hauts-de-Seine (92000 - Nanterre)" },
{ "permalink": "#", "title": "CG93 - Conseil général de la Seine-Saint-Denis (93000 - Bobigny)" },
{ "permalink": "#", "title": "CG94 - Conseil général du Val-de-Marne (94000 - Créteil)" },
{ "permalink": "#", "title": "CG95 - Conseil général du Val-d'Oise" }
];
