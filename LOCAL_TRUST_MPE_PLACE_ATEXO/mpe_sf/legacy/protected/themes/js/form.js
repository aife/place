/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function wipeForm(nameForm) {
	var formSize = nameForm.length;
	for (var i=0 ; i<formSize ; i++) {
		var elem = document.getElementById(nameForm).elements[i];
		var type = elem.type;
		if (type=='text') {
			elem.value='';
		} else {
			if (type != 'hidden') {
				//				alert('NAME:'+elem.name+'| TYPE:'+type+'| VALUE:'+elem.value+'|');
			} // end if
		}
		if (type=='checkbox') {
			elem.checked = false;
		}
	} //end for i<formSize
	return;
} // end function (_nameForm)

function DeleteCons (ConsId,SESSION)
{
	confirmSupp = confirm("Etes-vous certain de vouloir supprimer cette consultation ?");
	if (! confirmSupp)
	{
		return;
	}
	document.location.href = '?page=Agent.RefuserUneConsultation&id='+ConsId+'&session='+SESSION;
}

function PSR_imprimer (nom_cible)
{
	var PSR_f1 = null;
	var PSR_content=document.getElementById(nom_cible).innerHTML;
	var PSR_title=document.getElementsByTagName('title')[0].innerText;

	PSR_f1 = window.open ('',"PSR_f1","height=600,width=740,menubar=yes,scrollbars=yes,resizable=yes,left=10,top=10");
	PSR_f1.document.open();
	PSR_f1.document.write("<html><head><link rel='stylesheet'type='text/css' href='../print.css'><link rel='stylesheet'type='text/css' href='../mpe-1.8/organismes/joli.css'><title>" + PSR_title +"</title></head> <body style='background: white'>"+PSR_content+"</body></html>");

	PSR_f1.document.close();
	PSR_f1.focus();
	PSR_f1.window.print();
	PSR_f1.window.close();

} // fin PSR_imprimer()

function choixParDefaut(num) {
	
	//if(document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems_ctl1_parDefaut"+num).checked)
		//document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems_ctl1_parDefaut"+num).checked=false;
	for (var i=1 ; i<=4 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefaut"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefaut"+num).checked=true;
}
function choixParDefautAllot(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefautAllot"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefautAllot"+num).checked=true;
}
function choixParDefautaucunFicherAnnonce(num) {
for (var i=1 ; i<=3 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefautaucunFicherAnnonce"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_parDefautaucunFicherAnnonce"+num).checked=true;
}
function choixParDefautReponseElectro(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_parDefautReponseElectro"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_parDefautReponseElectro"+num).checked=true;
}
function choixParDefautprocedurePhase(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_parDefautprocedurePhase"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_parDefautprocedurePhase"+num).checked=true;
}

function choixParDefautsignature(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautsignature"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautsignature"+num).checked=true;
}
function choixParDefautchiffrement(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautchiffrement"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautchiffrement"+num).checked=true;
}
function choixParDefautenvoie(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautenvoie"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautenvoie"+num).checked=true;
}
function choixParDefautenvCandidature(num) {
for (var i=1 ; i<=3 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautenvCandidature"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautenvCandidature"+num).checked=true;
}
function choixParDefautoffre(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautoffre"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_parDefautoffre"+num).checked=true;
}
function choixParDefautgestionEnvoies(num) {
for (var i=1 ; i<=2 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_parDefautgestionEnvoies"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_parDefautgestionEnvoies"+num).checked=true;
}
function choixParDefautenvoies(num) {
for (var i=1 ; i<=4 ; i++) {
		if (i != num)
			document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_parDefautenvoies"+i).checked=false;
	}
	document.getElementById("ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_parDefautenvoies"+num).checked=true;
}




function verifierBiClesOptionnels()
{

//alert('test');
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl1_availableCertificates'))
 	var certificat1=document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl1_availableCertificates');
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl2_availableCertificates'))
 	var certificat2=document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl2_availableCertificates');
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl3_availableCertificates'))
 	var certificat3=document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl3_availableCertificates');
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl4_availableCertificates'))
	 var certificat4=document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl4_availableCertificates');
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl5_availableCertificates'))
	 var certificat5=document.getElementById('ctl0_CONTENU_PAGE_RepeaterBiCle_ctl5_availableCertificates');	 
 
if(certificat1)
	 var certificatval1=certificat1.value;	 
if(certificat2)
	 var certificatval2=certificat2.value;
if(certificat3)
 	var certificatval3=certificat3.value;
if(certificat4)
 	var certificatval4=certificat4.value;
if(certificat5)
 	var certificatval5=certificat5.value;

if( (certificatval1 ==certificatval2 && (certificatval2 !=0 && certificatval1 !=0 ))
	|| (certificatval1 ==certificatval3 && (certificatval3 !=0 && certificatval1 !=0 ))
	|| (certificatval1 ==certificatval4 && (certificatval4 !=0 && certificatval1 !=0 ))
	|| (certificatval1 ==certificatval5 && (certificatval5 !=0 && certificatval1 !=0 ))
	)
 	{
		 return false;
 	}
if(    (certificatval3 ==certificatval2 && (certificatval2 !=0 && certificatval3 !=0 ))
	|| (certificatval2 ==certificatval4 && (certificatval2 !=0 && certificatval4 !=0 ))
	|| (certificatval3 ==certificatval4 && (certificatval3 !=0 && certificatval4 !=0 ))
	|| (certificatval5 ==certificatval4 && (certificatval5 !=0 && certificatval4 !=0 ))
	|| (certificatval5 ==certificatval3 && (certificatval5 !=0 && certificatval3 !=0 ))
	|| (certificatval5 ==certificatval2 && (certificatval5 !=0 && certificatval2 !=0 ))
   )
 {
 return false;
 }

 else {
 return true;
 }
}
/*function checkAll1(){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserconsDossier1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvCandidature1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvAnonymat1').checked = true;
}
}*/
function notAfficher(){


if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_affichergestionEnvoies2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies1').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies2').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies3').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies4').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_affichergestionEnvoies2').checked)
{
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').disabled == true)
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_affichergestionEnvoies1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_affichergestionEnvoies1').checked)
{
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies1').disabled == true)
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies1').disabled = false;
}

}
function grisageenv()
{
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvAnonymat1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvAnonymat1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvAnonymat1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvAnonymat1').checked)
{
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvAnonymat1').disabled == true)
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvAnonymat1').disabled = false;
}

}

function notAfficherDce(){

if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherdossier1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherpartiel1').checked = false;

}
}
function readOnlynotCheckedDce(nomCheck)
{
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherdossier1').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_'+nomCheck).checked = false;
}



}
function notAfficherOffre(){


if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffre1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr1').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr2').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').checked = false;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffre1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvCandidature1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvCandidature1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvCandidature1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvCandidature1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvCandidature1').disabled = false;
}

}

function griserenvois(){


if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvoie2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvoie2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvoie1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherenvoie1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserenvoie1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_affichersignature1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_affichersignature1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_affichersignature2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_affichersignature2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_grisersignature2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherchiffrement1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherchiffrement1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherchiffrement2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficherchiffrement2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griserchiffrement2').disabled = false;
}



}

function grisercarac(){


if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherReponseElectro1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherReponseElectro1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherReponseElectro2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherReponseElectro2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserReponseElectro2').disabled = false;
}

if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocOuverte1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocOuverte1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocOuverte1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocOuverte1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocOuverte1').disabled = false;
}

if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocedurePhase1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocedurePhase1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocedurePhase2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase2').checked = false;

}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherprocedurePhase2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserprocedurePhase2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherpole1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserpole1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserpole1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_afficherpole1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems2_ctl1_griserpole1').disabled = false;
}


if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher3').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser3').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser3').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher3').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser3').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher4').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher4').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher4').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficher4').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griser4').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherAllot1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherAllot1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherAllot2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherAllot2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserAllot2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce1').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce2').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce2').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce3').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce3').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce3').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficheraucunFicherAnnonce3').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griseraucunFicherAnnonce3').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherpartiel1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserpartiel1').disabled = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserpartiel1').checked = false;
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_afficherpartiel1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserpartiel1').disabled = false;
}



}






function readOnlynotCheckedOffre(nomCheck)
{
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffre1').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_'+nomCheck).checked = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr2').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').disabled)
	{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').checked = false;
		}
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr2').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').disabled = false;
}
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr1').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').disabled)
		{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').checked = false;
		}
}
else if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_afficheroffr1').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').disabled = false;
}
}

function readOnlynotChecked(nomCheck)
{
if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_affichergestionEnvoies2').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_'+nomCheck).checked = false;
}
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies1').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').disabled = false;
}
else if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies1').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').disabled)
		{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').checked = false;
		}
}
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies2').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').disabled = false;
}
else if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies2').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').disabled)
	{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').checked = false;
	}
}
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies3').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').disabled = false;
}
else if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies3').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').disabled)
	{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').checked = false;
	}
}

if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies4').checked)
{
	if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').disabled)
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').disabled = false;
}
else if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_afficherenvoies4').checked)
{
	if(!document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').disabled)
	{
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').disabled = true;
		document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').checked = false;
		}
}

}
function checkAll1(){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserdossier1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserpartiel1').checked = true;
}
}
function checkAll11(){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffr2').checked = true;
}
}
function readOnly1(nomCheck){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_griserdossier1').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems1_ctl1_'+nomCheck).checked = true;
}

}
function readOnly11(nomCheck){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_griseroffre1').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems3_ctl1_'+nomCheck).checked = true;
}

}
/*function checkAll2(){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoie1').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').checked = true;

}
}*/
function checkAll22(){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').checked)
{
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies1').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies2').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies3').checked = true;
document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_griserenvoies4').checked = true;

}
}
function readOnly2(nomCheck){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoie1').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_'+nomCheck).checked = true;
}
if(nomCheck == 'grisergestionEnvoies2')
	checkAll22();
}
function readOnly22(nomCheck){
if(document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_grisergestionEnvoies2').checked)
{
	document.getElementById('ctl0_CONTENU_PAGE_RepeaterItems4_ctl1_'+nomCheck).checked = true;
}


}
 function addRow(itemId)
   {
    
     var current=document.getElementById(itemId);
     
     var tr = current.insertRow(current.rows.length);
     var nbreRow = current.rows.length-1;
     
	var hid = document.getElementById("nbreManagers");
	hid.value = nbreRow;
	
     var td = tr.insertCell(0);
     var inputNode = document.createElement("INPUT");
     inputNode.setAttribute("type", "text");
     inputNode.setAttribute("size", "20");
     inputNode.setAttribute("maxlength", "32");
     inputNode.setAttribute("id", "_nommanager"+nbreRow);
     inputNode.setAttribute("name", "-nommanager"+nbreRow);
     td.appendChild(inputNode);

     td = tr.insertCell(1);
     inputNode = document.createElement("INPUT");
     inputNode.setAttribute("type", "text");
     inputNode.setAttribute("size", "20");
     inputNode.setAttribute("maxlength", "32");
     inputNode.setAttribute("id", "_prenommanager"+nbreRow);
     inputNode.setAttribute("name", "-prenommanager"+nbreRow);
     td.appendChild(inputNode);

     td = tr.insertCell(2);
     inputNode = document.createElement("INPUT");
     inputNode.setAttribute("type", "text");
     inputNode.setAttribute("size", "20");
     inputNode.setAttribute("maxlength", "32");
     inputNode.setAttribute("id", "_qualitemanager"+nbreRow);
     inputNode.setAttribute("name", "-qualitemanager"+nbreRow);
     td.appendChild(inputNode);
     //     alert("L'index est " + index);
   }
   function GoToPotail()
{
	if (document.all)
	doc = document.all.main_form;
	else
	doc = document.main_form;
	doc.action = "../index.php";
	ActivateMenu('get_main_page');
}

function send_msg(session_name,session_id,offre_id,liste_offres,source,num_env,cons_ref,num_marche,cmd,invisible,organisme){
	var adresse = '../'+organisme +'/messagerie/index.php';
	if(session_name.charAt(0) == "i" || session_name.charAt(0) == "a")
		adresse = '../mpe-1.8/messagerie/index.php';

	var page = adresse + "?sess_name="+session_name+"&sess_id="+session_id+"&id_offre="+offre_id+"&cmd="+cmd+"&source=notification&num_env="+num_env+"&id="+cons_ref+"&num_marche="+num_marche+"&source="+source+"&msgCreationForm="+invisible+"&liste_offres="+liste_offres;
	new_window(page,'800','600','1','1');
}


// debut module code cpv partie 2

		// Crée un lien choisir/modifier pour pré-remplir la popup CPV 
		function choisirModifier(clef, autre, principal, secondaires) {
			var chaineP = "";
			var chaine = "";
			if (principal != null && principal[0] != "") {
				chaineP = principal[0];
				chaine = "&principal=" + chaineP;
			}

			var chaineS = "";			
			for (i = 0; i < secondaires.length ; i++) {
				if (chaineS != "") {
					chaineS += ",";
				}
				chaineS += secondaires[i][0];
			}
			if (secondaires.length != 0) {
				chaine += "&secondaires=" + chaineS;
			}

			return "<a href=\"../atexo.cpv/cpv.jsp?clef=" + clef + ""
				+ chaine + autre + "\" target=\"_blank\">Choisir/Modifier</a>";
		}
		
		// Crée un span contenant le CPV et le code pour l'infobulle 
		function creeCodeBulle(id, cpv) {
			var chaine = '<span class="code" onMouseOver="afficheBulle(\'';
			chaine += id + '\', this)" onMouseOut="cacheBulle(\'';
			chaine += id + '\')">' + cpv[0] + '</span>';

			chaine += '<div class="libelle" id="' + id	+ '"'
			chaine += 'onMouseOver="mouseOverBulle();"';
			chaine += 'onMouseOut="mouseOutBulle();"><div>'	+ cpv[1] + '</div></div>';

			return chaine;
		}
	
		// Fonction appelée par la popup CPV 
		function retournerCPVs(clef, principal, secondaires) {
		
		
		var min = document.getElementById("min").value;
		var maxCPV = document.getElementById("max").value;
		var langueCPV = document.getElementById("langueCPV").value;
		var typeCPV = document.getElementById("typeCPV").value;
		
		for (indexHidden = 1; indexHidden <=maxCPV; indexHidden++)
		{
			code_cpv_secondaire = document.getElementById(clef+indexHidden);
			libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+indexHidden);
			code_cpv_secondaire.value = '';
			libeller_code_cpv_secondaire.value = '';
		}
		
		
		var p =0;
		switch (typeCPV) {
			case '0':
				res = document.getElementById("resultat");
				res.innerHTML = "Résultats:<br/>";
				if (principal != null) {
					res.innerHTML += "Principal: "
						+ principal[0]
						+ " : " + principal[1];
				}
	
				if (secondaires != null) {
					chaine = "<br />Secondaires:<br/>";
	
					for (i = 0; i < secondaires.length; i++) {
						chaine += secondaires[i][0] + ":" + secondaires[i][1] + "<br />";
					}
					res.innerHTML += chaine; 
				}
				break;
			case 'cas1':
				res = document.getElementById('cas1');
				res.innerHTML = choisirModifier(clef, "&min=" + min +"&max=" + maxCPV +"&demandeprincipal=true&locale="+ langueCPV +"", principal, secondaires);
				code_cpv_principal = document.getElementById(''+clef+'_1');
				libeller_code_cpv_principal = document.getElementById('libelle_'+clef+'_1');
				code_cpv_principal.value = principal[0];
				libeller_code_cpv_principal.value = principal[1];
				break;
			case 'cas2':
				res = document.getElementById('cas2'+ clef);
				chaineTableCPV = "<table>";
				if (principal[0] != null) 
				{
					
                	chaineTableCPV += "<tr>";
                	chaineTableCPV += "<td>";
                	chaineTableCPV += creeCodeBulle('id2Bubulle'+ clef, principal);
                    chaineTableCPV += "</td>";              
                    chaineTableCPV +="<td colspan='2'><span class='code'><i>((en) Code principal)</i></span></td>";
                    chaineTableCPV += "</tr>";
                    code_cpv_principal = document.getElementById(clef+'1');
					libeller_code_cpv_principal = document.getElementById('libelle_'+clef+'1');
					code_cpv_principal.value = principal[0];
					libeller_code_cpv_principal.value = principal[1];
					p = 1;
					if (secondaires.length)
					{
						chaineTableCPV += "<tr>";
						for (i = 0 ; i < secondaires.length ; i++) {
							secondairesCPV = new Array()
							secondairesCPV[0] = secondaires[i][0];
							secondairesCPV[1] = secondaires[i][1];
							chaineTableCPV += "<td>";
							if (i != 0) {
								secondairesCPV[0] = ";"+secondairesCPV[0]; 
							}
                			chaineTableCPV += creeCodeBulle('id2Bubulle'+ clef + i, secondairesCPV);
                    		chaineTableCPV += "</td>";
							iCPV = i + 1 + p;
							code_cpv_secondaire = document.getElementById(clef+''+iCPV);
							libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
							code_cpv_secondaire.value = secondaires[i][0];
							libeller_code_cpv_secondaire.value = secondaires[i][1];
						}
						chaineTableCPV += "</tr>";
					}
                    
				}
				chaineTableCPV += "<tr>";
                chaineTableCPV += "<td colspan='3'><div class='recherche-codes'>";
				chaineTableCPV += choisirModifier(clef, "&min=" + min +"&max=" + maxCPV +"&demandeprincipal=true&locale="+ langueCPV +"", principal, secondaires);
                chaineTableCPV += "</div>";
                chaineTableCPV += "</td>";            
                chaineTableCPV += "</tr>";          
                chaineTableCPV += "</table>";
                if (principal.length == 0 || principal[0] == null)
                {
                	chaineTableCPV += "</br><span class='txt-attention'><img src='../images/picto-attention.gif' alt='Attention' title='Attention'>(en) (Au moins une valeur à remplir si la case Codes CPV est cochée)</span>";
                }
				chaineTableCPV += "<div class='breaker'></div>";
				res.innerHTML = chaineTableCPV;
				break; 
                    
				
				
			case 'cas3':
				res = document.getElementById('cas3' + clef);
				chaine = "";
 
				if (principal[0] != "") {
					chaine = principal[0];
					p = 1;
				}

				for (i = 0 ; i < secondaires.length ; i++) {
					if (chaine != "") {
						chaine += "; ";
					}
					chaine += secondaires[i][0];
					iCPV = i + 1 + p;
					code_cpv_secondaire = document.getElementById(clef+''+iCPV);
					libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
					code_cpv_secondaire.value = secondaires[i][0];
					libeller_code_cpv_secondaire.value = secondaires[i][1];
				}

				res.innerHTML = chaine;
				break;
			case 'cas4':
				res = document.getElementById('cas4'+ clef);
				chaineTableCPV = "<table>";
                res.innerHTML = chaineTableCPV;	
				if (principal[0] != null) 
				{
					
                	chaineTableCPV += "<tr>";
                	chaineTableCPV += "<td>";
                	chaineTableCPV += creeCodeBulle('id4Bubulle'+ clef, principal);
                    chaineTableCPV += "</td>";              
                    chaineTableCPV +="<td colspan='2'><span class='code'><i>((en) Code principal)</i></span></td>";
                    chaineTableCPV += "</tr>";
                    code_cpv_principal = document.getElementById(clef+'1');
					libeller_code_cpv_principal = document.getElementById('libelle_'+clef+'1');
					code_cpv_principal.value = principal[0];
					libeller_code_cpv_principal.value = principal[1];
					p = 1;
					if (secondaires.length)
					{
						chaineTableCPV += "<tr>";
						for (i = 0 ; i < secondaires.length ; i++) {
							
							secondairesCPV = secondaires[i];
							chaineTableCPV += "<td>";
							if (i != 0) {
								secondairesCPV[0] = ";"+secondairesCPV[0]; 
							}
                			chaineTableCPV += creeCodeBulle('id4Bubulle'+ clef + i, secondairesCPV);
                    		chaineTableCPV += "</td>";
							iCPV = i + 1 + p;
							code_cpv_secondaire = document.getElementById(clef+''+iCPV);
							libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
							code_cpv_secondaire.value = secondaires[i][0];
							libeller_code_cpv_secondaire.value = secondaires[i][1];
						}
						chaineTableCPV += "</tr>";
					}
                    
				}
                chaineTableCPV += "</table>";
				chaineTableCPV += "<div class='breaker'></div>";
				res.innerHTML = chaineTableCPV;
				break; 
			case 'cas5':
				res = document.getElementById('cas5' + clef);
				var chaine = "";
				
				chaine += "<table>";
				if (secondaires.length)
				{
					for (i = 0 ; i < secondaires.length ; i++) {
						chaine += "<tr>";
                		chaine += "<td>";
						chaine +=  creeCodeBulle('id5Bubulle'+ clef + i, secondaires[i]);
						chaine += "</td>";
						chaine += "<td> : ";
						if (secondaires[i][1].length >50)
							chaine += secondaires[i][1].substring(0,51) + "...";
						else
							chaine += secondaires[i][1];
						chaine += "</td>";
						chaine += "</tr>";
						iCPV = i + 1;
						code_cpv_secondaire = document.getElementById(clef+''+iCPV);
						libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
						code_cpv_secondaire.value = secondaires[i][0];
						libeller_code_cpv_secondaire.value = secondaires[i][1];
					}
				}
				chaine += "<tr>";
                chaine += "<td colspan='3'><div class='recherche-codes'>";
				chaine += choisirModifier(clef, "&min=" + min +"&max=" + maxCPV +"&demandeprincipal=false&locale="+ langueCPV +"", principal, secondaires);
                chaine += "</div>";
                chaine += "</td>";            
                chaine += "</tr>";          
                chaine += "</table>";
				chaine += "<div class='breaker'></div>";
				res.innerHTML = chaine;
				break;
			case 'cas6':
				res = document.getElementById('cas6'+ clef);
				chaineTableCPV = "<table>";
					if (secondaires.length)
					{
						chaineTableCPV += "<tr>";
						for (i = 0 ; i < secondaires.length ; i++) {
							secondairesCPV = new Array()
							secondairesCPV[0] = secondaires[i][0];
							secondairesCPV[1] = secondaires[i][1];
							chaineTableCPV += "<td>";
							if (i != 0) {
								secondairesCPV[0] = ";"+secondairesCPV[0]; 
							}
                			chaineTableCPV += creeCodeBulle('id6Bubulle'+ clef + i, secondairesCPV);
                    		chaineTableCPV += "</td>";
							iCPV = i + 1;
							code_cpv_secondaire = document.getElementById(clef+''+iCPV);
							libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
							code_cpv_secondaire.value = secondaires[i][0];
							libeller_code_cpv_secondaire.value = secondaires[i][1];
						}
						chaineTableCPV += "</tr>";
					}
				chaineTableCPV += "<tr>";
                chaineTableCPV += "<td colspan='3'><div class='recherche-codes'>";
				chaineTableCPV += choisirModifier(clef, "&min=" + min +"&max=" + maxCPV +"&demandeprincipal=false&locale="+ langueCPV +"", principal, secondaires);
                chaineTableCPV += "</div>";
                chaineTableCPV += "</td>";            
                chaineTableCPV += "</tr>";          
                chaineTableCPV += "</table>";
				chaineTableCPV += "<div class='breaker'></div>";
				res.innerHTML = chaineTableCPV;
				break; 
			case 'cas7':
				res = document.getElementById('cas7' + clef);
				if (principal.length)
				{
					res.innerHTML = principal[0];
					res.innerHTML += " (<i>(en) Code principal</i>)<br/>";
				}
				if (secondaires.length)
				{
					for (i = 0 ; i < secondaires.length ; i++) {
						if (i != 0) {
							res.innerHTML += ";";
						}
						res.innerHTML += secondaires[i][0];
					}
				}
				break;
			case 'cas8':
				res = document.getElementById('cas8' + clef);
				var chaine = "";
				
				
				if (secondaires.length)
				{
					for (i = 0 ; i < secondaires.length ; i++) {
						chaine += secondaires[i][0] + " : " + secondaires[i][1] + "<br />";
						iCPV = i + 1;
						code_cpv_secondaire = document.getElementById(clef+''+iCPV);
						libeller_code_cpv_secondaire = document.getElementById('libelle_'+clef+''+iCPV);
						code_cpv_secondaire.value = secondaires[i][0];
						libeller_code_cpv_secondaire.value = secondaires[i][1];
					}
				}
				res.innerHTML = chaine;
				res.innerHTML += choisirModifier(clef, "&min=" + min +"&max=" + maxCPV +"&demandeprincipal=false&locale="+ langueCPV +"", principal, secondaires);

				break;
				
			default:
                bootbox.alert('positionner une clef');
			}
		
		}

		// Remplissage dynamique de la page 		
		function remplirCPV(clef, principal, secondaires) 
		{
			retournerCPVs(clef, principal, secondaires);
		}
		//////////////////////////////////////Infos-bulles//////////////////////////////////


/*Checks IE 6.0*/
var name = navigator.appName ;
var version = navigator.appVersion;

if (name == 'Microsoft Internet Explorer') {
	id = version.indexOf('MSIE');
	version = version.substring(id+5,id+9);
}


var idBulleT;
var chrono = null;
var delai = "500";



function afficheBulle(idBulle,parent)
{
  var bulle = document.getElementById(idBulle);
  var offset;
  var exp = new RegExp("^td_","gi");
 
  if (chrono!=null)
  {
     clearTimeout(chrono);
     cacheBulleT();
  }
 
  bulle.style.display = "block";
  
  var bulleLeft = parent.offsetLeft+10;
  var bulleTop = parent.offsetTop-bulle.offsetHeight; 

	var windowWidth = 0;
	if (typeof(window.innerWidth) == 'number') {
		windowWidth= window.innerWidth;
	}
	else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth= document.documentElement.clientWidth;
		}
		else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

  bulle.style.left = bulleLeft+'px';
  bulle.style.top = bulleTop+'px'; 
  
  
	  if((bulleLeft+bulle.offsetWidth+20) > windowWidth) {
	  bulle.style.left = (bulleLeft-bulle.offsetWidth)+'px';


}

    //check Internet Exporer VErsion

   //create iframe to fix ie6 select z-index bug 
	if (version == '6.0;') {
		
	  var myBulleIframe = '<iframe scrolling="no" frameborder="0" class="libelle-Iframe" title="Cadre vide"'+' id="'+idBulle+'-Iframe"'+'></iframe>';
	  var myMessage = bulle.innerHTML;
	  bulle.innerHTML = myBulleIframe + myMessage;
	  var myBulleHeight = bulle.offsetHeight;
	  var myBulleWidth = bulle.offsetWidth;
	  myNewID = idBulle+'-Iframe';
	  var myNewIframe = document.getElementById(myNewID);
	  
	  
	  var parentClass = myNewIframe.parentNode.className;
	  var iframeNewClass = parentClass+'-Iframe';
	  myNewIframe.className = iframeNewClass;
	  myNewIframe.style.height = myBulleHeight;
	  myNewIframe.style.width = myBulleWidth;
	  //alert(myNewIframe.style.height);
	  //alert(myNewIframe.className);
	}


}




function cacheBulleT()
{
  document.getElementById(idBulleT).style.display = "none";
  chrono = null;
}
 
function cacheBulle(idBulle)
{
  idBulleT = idBulle;
  chrono = setTimeout("cacheBulleT()",delai);
 
  //document.getElementById(idBulle).style.display = "none";
}
 
function mouseOverBulle()

{
  clearTimeout(chrono);
  chrono = null;
}
 
function mouseOutBulle()
{
   chrono = setTimeout("cacheBulleT()",delai);
}
 

//////////////////////////////////////End Infos-bulles//////////////////////////////////

function isCheckedCPVObligatoire(myInput) {
if (myInput.checked == true) 
{
	var inputElems;
	inputElems = document.getElementsByTagName("div");
	for( i=0; i < inputElems.length; i++)
	{
		var inputName = inputElems[i].name;
		if (inputName)
		{
            bootbox.alert(inputName);
			if(inputName.search(/bloccode_cpv/)==0)
		   	{
		    	inputElems[i].style.display = 'block';
		   	}
	   	}
	}
	
	//document.getElementsByTagName().getElementById(myDiv).style.display = 'block';
}
if (myInput.checked == false) {
	var inputElems;
	inputElems = document.getElementsByTagName("div");
	for( i=0; i < inputElems.length; i++)
	{
		var inputName = inputElems[i].name;
		if (inputName)
		{
            bootbox.alert(inputName);
			if(inputName.search(/bloccode_cpv/)==0)
		   	{
		    	inputElems[i].style.display = 'none';
		   	}
	   	}
	}
	//document.getElementById(myDiv).style.display = 'none';
	}
}		
		
// fin module code cpv partie 2
   							





