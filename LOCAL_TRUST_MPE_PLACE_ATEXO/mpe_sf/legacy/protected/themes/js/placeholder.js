/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

jQuery(function($) {
    //  Au focus
    $('.inputWithPlaceholder .rechercher').focus(function(){
        $(this).parent().addClass('is-focused has-label');
    });

    // à la perte du focus
    $('.inputWithPlaceholder .rechercher').blur(function(){
        $parent = $(this).parent();
        if($(this).val() != ''){
            $parent.removeClass('has-label');
        }
        $parent.removeClass('is-focused');
    });

    // si un champs est rempli on rajoute directement la class has-label
    $('.inputWithPlaceholder .rechercher').each(function(){
        if($(this).val() != ''){
            $(this).parent().addClass('has-label');
        }
    });
})
