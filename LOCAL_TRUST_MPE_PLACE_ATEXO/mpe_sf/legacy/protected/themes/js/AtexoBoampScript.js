/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

var itemIndex ='';
var butonClic='';

function setBoamp() {
	butonClic = "boamp";
	 return true;
}

function setMoniteur() {
	butonClic = "moniteur";
	 return true;
}

function gotoMoniteur(idAnnonce)
{
	window.opener.document.getElementById('ctl0_CONTENU_PAGE_IdAnnonceMoniteur').value = idAnnonce;
	if(document.all) { 
		doc = document.all;//IE
	} else {
		doc = document;
	}
	doc.main_form.action = "/atexo.boamp/boamp";
	doc.main_form.submit();
}

function BoampPostInfo()
{
	if(document.all) { 
		doc = document.all;//IE
	} else {
		doc = document;
	}
	url = window.location.protocol+"%2F%2F"+window.location.hostname+"%2Fthemes%2Fboamp%2Fcss%2F";
	style = "?style1=" + url + "common-boamp.css&style2=" + url +"styles-boamp.css";
	doc.main_form.action = "/atexo.boamp/boamp";
	doc.main_form.submit();
	
}

function getXmlResponseClient(xmlString,typeRetour)
{
	itemIndex = typeRetour;
	document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+typeRetour+'_xmlString').value = xmlString;
}
function enregistrer()
{
	if(butonClic=="boamp") {
		// Cas où l'utilisateur a cliqué sur le bouton enregistrer
		document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+itemIndex+'_validateXml').value = '0';
		document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+itemIndex+'_actionXml').value = '1';
		document.getElementById('ctl0_CONTENU_PAGE_buttonaddXml').click();
	}
	else if(butonClic=="moniteur") {
		document.getElementById('ctl0_CONTENU_PAGE_statutXml').value = '0';
		document.getElementById('ctl0_CONTENU_PAGE_addXmlMoniteur').click();
	}
}

function valider(){

	// Cas où l'utilisateur a cliqué sur le bouton valider.
	if(butonClic=="boamp") {
		document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+itemIndex+'_validateXml').value = '1';
		document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+itemIndex+'_actionXml').value = '1';
		document.getElementById('ctl0_CONTENU_PAGE_buttonaddXml').click();
	}
	else if(butonClic=="moniteur") {
		document.getElementById('ctl0_CONTENU_PAGE_statutXml').value = '1';
		document.getElementById('ctl0_CONTENU_PAGE_addXmlMoniteur').click();
	}
}

function updateStatutDestFormXml(statutEnvoi)
{
	var doc = window.opener.document;
	document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+itemIndex+'_statutEnvoi').value = statutEnvoi;
	doc.getElementById('ctl0_CONTENU_PAGE_buttonRefreshDestFormXml').click();
	window.close();

}

function refreshRepeaterFormLibre()
{
	var doc = window.opener.document;
	doc.getElementById('ctl0_CONTENU_PAGE_refreshRepeaterFormLibre').click();
	window.close();
}

function annuler()
{

}

function hidePictoEnvoyer(parentIndex,index)
{
  var parentIndex = (parentIndex+1);
  var inex = (index+1);
  panelPictoSend = 'ctl0_CONTENU_PAGE_repeaterFormPublicite_ctl'+parentIndex+'_repeaterDestinataire_ctl'+inex+'_panelPictoSend';
 document.getElementById(panelPictoSend).style.display = 'none';
}

function getXmlMoniteurResponseClient(xmlResponse)
{
	document.getElementById('ctl0_CONTENU_PAGE_xmlMoniteur').value = xmlResponse;
}

function gotoMoniteur(idAnnonce)
{
	window.opener.document.getElementById('ctl0_CONTENU_PAGE_IdAnnonceMoniteur').value = idAnnonce;
	if(document.all) { 
		doc = document.all;//IE
	} else {
		doc = document;
	}
	doc.main_form.action = "/atexo.boamp/boamp";
	doc.main_form.submit();
}


function ConcentrateurPostInfo()
{
	if(document.all) { 
		doc = document.all;//IE
	} else {
		doc = document;
	}
	doc.main_form.action = "/publication/interapp/entermolpublication.do";
	doc.main_form.submit();
}

function OpocePostInfo()
{
	if(document.all) { 
		doc = document.all;//IE
	} else {
		doc = document;
	}
//	doc.main_form.action = "https://ovh12.local-trust.com/eSender/esender/esender.jsp";
	doc.main_form.action = "/eSender/esender/initialisationServlet.srv";
	doc.main_form.submit();
	
}

function enregistrementEsender(fluxXmlBase64, jetonSession, noDocExt) {
	document.getElementById('ctl0_CONTENU_PAGE_xmlAvis').value = fluxXmlBase64;
	document.getElementById('ctl0_CONTENU_PAGE_noDocExt').value = noDocExt;
	document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPubliciteAvis_ctl'+jetonSession+'_repeaterDestinataire_ctl1_enregistrementEsender').click();
}
function enregistrementValidationEsender(fluxXmlBase64, jetonSession, noDocExt) {
	document.getElementById('ctl0_CONTENU_PAGE_xmlAvis').value = fluxXmlBase64;
	document.getElementById('ctl0_CONTENU_PAGE_noDocExt').value = noDocExt;
	document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPubliciteAvis_ctl'+jetonSession+'_repeaterDestinataire_ctl1_enregistrementValidationEsender').click();
}
function enregistrementValidationEnvoiEsender(fluxXmlBase64, jetonSession, noDocExt) {
	document.getElementById('ctl0_CONTENU_PAGE_xmlAvis').value = fluxXmlBase64;
	document.getElementById('ctl0_CONTENU_PAGE_noDocExt').value = noDocExt;
	document.getElementById('ctl0_CONTENU_PAGE_repeaterFormPubliciteAvis_ctl'+jetonSession+'_repeaterDestinataire_ctl1_enregistrementValidationEnvoiEsender').click();
}
