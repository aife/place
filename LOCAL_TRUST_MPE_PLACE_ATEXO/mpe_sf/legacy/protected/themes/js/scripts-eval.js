/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// JavaScript Document

  
J(document).ready(function() {  
	J('<style type="text/css"> table.tableau-prix .hidden { display:none; } </style>').appendTo("head");
	//Cache les Critères sur les Prix unitaires et Critères issus du Questionnaire au chargement de la page						   
	J(".readOnlyRow").hide();
	//Cache/affiche les Critères sur les Prix unitaires et Critères issus du Questionnaire au chargement de la page						   
	J(".toggle-rows").click(function() {  
		J(this).parents('table').find(".readOnlyRow").toggle(J(this).parents('table').find(".readOnlyRow").css('display') == 'none'); 
		J(this).toggleClass('toggle-on');
	});  
  
	//Active/Desactive les champs du critère			   
	J(".activer-layer-criteres").click(function(){ 
		J(this).children().is(':checked') ? J(this).parents('tr').find('.layer-criteres').removeClass('hidden') : J(this).parents('tr').find('.layer-criteres').addClass('hidden');
	});
	
	//Active/Desactive les prix unitaire 		   
	J("input.prix-unitaire").click(function(){ 
		J(this).is(':checked') ? J(this).parents('tr').find('.quantite').addClass('hidden') : J(this).parents('tr').find('.quantite').removeClass('hidden');
	});

	//Appliquer le taux au tableau entier
	J("#appliquerPartout").click(function(){ 
		var myValue = J(".taux-tva-copy").val();	
		J(this).is(':checked') ? J(".taux-tva").val(myValue) : J(".taux-tva").val('');
	});
	
	J(".input-rejet").click(function(){ 
		J(this).children().is(':checked') ? J("#totalEval").addClass('total-erreur') : J("#totalEval").removeClass('total-erreur');
	});
	
	J(".pourcentage").change(function(){ 
		if (isNaN(parseFloat(J(this).val())) || (J(this).val() > 100)) {
			J(this).val(0);
		}
		calculTotalCriteresEval();
	});
	
	calculTotalCriteresEval();
 
});



//Edition modèles Qestionnaires et Bordereau de prix//

//Ajout d'un ligne et mise à jour des fleches et numero d'item
function ajoutLigne() {
	ajoutLigneGenerique('tableau-prix');
}

function ajoutLigneGenerique(classTableau) {

        var numberRows = J('.' + classTableau + ' tbody tr').length;
        var incrementedItemIndex = parseInt(J('.' + classTableau + ' tr:visible:last .num-item').val());
        if (isNaN(incrementedItemIndex)) {
                incrementedItemIndex = 0;
        }
        incrementedItemIndex++;
        var nextElement = J('.' + classTableau + '  tbody tr:visible:last').next('tr');
        if (nextElement.length > 0) {
                nextElement.removeClass('hidden');
        }else {
                J('.' + classTableau + '  tbody tr:first').removeClass('hidden');
        }
        if (incrementedItemIndex<numberRows) {
                J('.' + classTableau + ' tr:visible:last .saveStatut').val('0');
                J('.' + classTableau + ' tr:visible:last .num-item').val(incrementedItemIndex);
                updateArrowStatut(classTableau);
                tableAlternateColor(classTableau);
                initItemNumber(classTableau);

        }
        updateArrowStatut(classTableau);
        tableAlternateColor(classTableau);
        initItemNumber(classTableau);
}

function supprimer(element,classTableau, minRow)
{
	 var row = J(element).parents("tr:");
        var lastRow = row.siblings(":last");
		var tableRowLength = J('.' + classTableau + ' tbody tr:visible').length;
		if (tableRowLength>minRow) {
			row.find('.saveStatut').val('1');
			row.addClass('hidden');
			row.insertAfter(lastRow);
			tableAlternateColor(classTableau);		
			initItemNumber(classTableau);
			updateArrowStatut(classTableau);
		}
		else {
		}
}

function deplacer(element, classTableau)
{
	updateArrowStatut(classTableau);
	var row = J(element).parents("tr:first");
	if (J(element).is(".remonter")) {
		row.insertBefore(row.prev());
		tableAlternateColor(classTableau);
		updateArrowStatut(classTableau);
		initItemNumber(classTableau);
	}
	if (J(element).is(".descendre")) {
		row.insertAfter(row.next());
		tableAlternateColor(classTableau);
		updateArrowStatut(classTableau);
		initItemNumber(classTableau);
	} 
   else {
   }
}

//*****************************Gestion de l'alternance des couleur de ligne*****************************//
function tableAlternateColor(classTableau) {
	J('.' + classTableau + ' tbody tr:visible:odd').addClass('on');
	J('.' + classTableau + ' tbody tr:visible:even').removeClass('on');
}

//Gestion de l'affichage des fleche monter/descendre selon position de l'item
function updateArrowStatut(classTableau) {
	
	J('.' + classTableau + ' tbody tr:visible .remonter-off').removeClass().addClass('remonter').attr('title','Remonter');
	J('.' + classTableau + ' tbody tr:visible .descendre-off').removeClass().addClass('descendre').attr('title','Descendre');
	
	J('.' + classTableau + ' tbody tr:visible:first .remonter').removeClass().removeClass('remonter-off').addClass('remonter-off').attr('title','Remonter (inactif)');
	J('.' + classTableau + ' tbody tr:visible:first .descendre').removeClass().removeClass('descendre-off').addClass('descendre').attr('title','Descendre');
	
	J('.' + classTableau + ' tbody tr:visible:last .descendre-off').removeClass().removeClass('descendre').addClass('descendre').attr('title','Descendre');
	J('.' + classTableau + ' tbody tr:visible:last .remonter-off').removeClass().removeClass('remonter').addClass('remonter').attr('title','Remonter');
	
	J('.' + classTableau + ' tbody tr:visible:last .descendre').removeClass().addClass('descendre-off').attr('title','Descendre (inactif)');
	J('.' + classTableau + ' tbody tr:visible:last .remonter').removeClass().addClass('remonter').attr('title','Remonter');
  
  
  	//Desacactive les fleche si derniere ligne du tableau
	var tableRowLength = J('.' + classTableau + ' tbody tr:visible').length;
	if (tableRowLength<2) {
	  J('.' + classTableau + ' tbody tr .descendre').addClass('descendre-off').attr('title','Descendre (inactif)');
	  J('.' + classTableau + ' tbody tr .remonter').addClass('remonter-off').attr('title','Remonter (inactif)');
	}
	else {
	}
  
}

//Reinitialisation du numero d'item
function initItemNumber(classTableau) {
	J('.' + classTableau + ' tbody tr:visible').each(function(index) {
		J(this).find('.num-item').val(index+1);
	});
}

function calculTotalCriteresEval()
{
	var totalPrix = 0;
	var totalCompl = 0;
	var total = 0;
	elementTBP = J('.activer-layer-criteres');
	if (elementTBP.children().is(':checked')) {
		var prixBdp = 0;
		prixBdp = parseFloat(J(elementTBP).parents("tr:").find('.pourcentage').val());
		if (!isNaN(prixBdp)){
			totalPrix += prixBdp;
		}
	}
	J('.saisie-criteres-prix tbody tr:visible').each(function(index) {
		if (J(this).find('.pourcentage').is(':input')) {
			var prix = 0;
			prix = parseFloat(J(this).find('.pourcentage').val());
			if (!isNaN(prix)){
				totalPrix += prix;
			}
		}
	}
	);
	J('.saisie-criteres-complementaires tbody tr:visible').each(function(index) {
		if (J(this).find('.pourcentage').is(':input')) {
			prixCompl = parseFloat(J(this).find('.pourcentage').val());
			if (!isNaN(prixCompl)){
				totalCompl += prixCompl;
			}
		}
	}
	);
	total = totalCompl + totalPrix;
	J("#ctl0_CONTENU_PAGE_totalCP").text(totalPrix);
	J("#ctl0_CONTENU_PAGE_totalCC").text(totalCompl);
	J("#ctl0_CONTENU_PAGE_total").text(total);
	J("#ctl0_CONTENU_PAGE_totalhidden").val(total);
	if (total == 100) {
		J("#totalCriteres").removeClass('total-erreur');
	}else {
		J("#totalCriteres").addClass('total-erreur');
	}
}

//*****************************Fin Edition Qestionnaires et Bordereau de prix*****************************//

