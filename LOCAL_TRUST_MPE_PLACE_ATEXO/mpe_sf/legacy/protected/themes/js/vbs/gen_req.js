/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function CreateCertEnrollPKCS10Request(cn)
{
    //alert('fonction js CreateCertEnrollPKCS10Request');
    var objDN = classFactory.CreateObject("X509Enrollment.CX500DistinguishedName");
    nCSPIndex = document.main_form.lbCSP.selectedIndex;
    var selectedCsp = classFactory.CreateObject("X509Enrollment.CCspInformation");
    selectedCsp.InitializeFromName(document.main_form.lbCSP.options(nCSPIndex).text);

    objPrivateKey.ProviderName = document.main_form.lbCSP.options(nCSPIndex).text;
    objPrivateKey.KeySpec = "1"; //AT_KEYEXCHANGE
    objPrivateKey.KeyProtection = "1";
    objPrivateKey.ExportPolicy = 1


    objRequest.InitializeFromPrivateKey(1, objPrivateKey, "");
    var strDN = "CN=" + cn;
    if(document.main_form.country.value != "") {
        strDN += ";C="+document.main_form.country.value;
    }
    if(document.main_form.city.value != "") {
        strDN += ";L="+document.main_form.city.value;
    }
    if(document.main_form.company_name.value != "") {
        strDN += ";O="+document.main_form.company_name.value;
    }
    if(document.main_form.company_id.value != "") {
        strDN += ";OU="+document.main_form.company_id.value;
    }
    if(document.main_form.email.value != "") {
        strDN += ";E="+document.main_form.email.value;
    }
    objDN.Encode(strDN, 0);
    objRequest.Subject = objDN;
    objEnroll.InitializeFromRequest(objRequest);
    szDN = objEnroll.CreateRequest(1);
    return szDN
}