Option Explicit

Dim cur_xenroll_id
Dim xenroll_ids
Dim num_xenroll_ids
Dim cert_enroll
Dim objEnroll, objPrivateKey, objRequest

xenroll_ids = Array("clsid:127698e4-e730-4e5c-a2b1-21490a70c8a1", _
                    "clsid:43F8F289-7A20-11D0-8F06-00C04FC295E1")

num_xenroll_ids = UBound(xenroll_ids) + 1
cur_xenroll_id = 0

Dim xenroll_verify_done_cb
xenroll_verify_done_cb = ""

Function TestXEnroll()
  On Error Resume Next
  Dim test
  test = XEnroll.HashAlgID
  If Err.Number <> 0 Then
	TestXEnroll = False
  Else
	TestXEnroll = True
  End If
End Function

Function CallResultCB(result)
  On Error Resume Next
  GetRef(xenroll_verify_done_cb)(result)
End Function

Function VerifyXEnrollID(id)
  On Error Resume Next
  switch_object(id)
  SetTimeout "VerifyXEnrollID2()", 1, "VBScript"
End Function

Function VerifyXEnrollID2()
  On Error Resume Next
  Dim test
  test = XEnroll.HashAlgID
  If Err.Number <> 0 Then
    VerifyNextXEnrollID()
    Exit Function
  End If
  CallResultCB(1)
End Function

Function VerifyNextXEnrollID()
  On Error Resume Next
  If cur_xenroll_id < num_xenroll_ids Then
    VerifyXEnrollID(xenroll_ids(cur_xenroll_id))
    cur_xenroll_id = cur_xenroll_id + 1
  Else
    CallResultCB(0)
  End If
End Function

Function VerifyXEnroll(cb)
  xenroll_verify_done_cb = cb
  VerifyNextXEnrollID()
End Function

