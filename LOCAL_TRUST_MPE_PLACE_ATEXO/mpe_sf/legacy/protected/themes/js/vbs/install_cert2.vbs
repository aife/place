Option Explicit

'Function postLoad()
'  On Error Resume Next   
 ' VerifyXEnroll("postLoad2")
'End Function

'Function postLoad2(status)
'  On Error Resume Next   
'  If status = 0 Then 
 '   MsgBox "XEnroll version mismatch", 64, "Error"
 '   Exit Function
 ' End If
'End Function

sub CertAcceptSub()
  On error resume next
  Dim installOK 
  installOK = True
  initCertEnroll()
  If (Not IsObject(objEnroll)) Then
  	XEnroll.MyStoreFlags=&H10000
  	XEnroll.RequestStoreFlags=&H10000
  	'MsgBox document.getElementById("ctl0_CONTENU_PAGE_certificate").value  
 	XEnroll.acceptPKCS7(document.getElementById("ctl0_CONTENU_PAGE_certificate").value)
  Else
 	x509installCertEnroll()
  End If
  dim errNum 
  errnum = Err.number

  If err.Number = -2147023673 then 
  	' Rediriger vers page=agent.ValiderConsultations
	window.location = "./?page=agent.ValiderConsultations"
  ElseIf err.Number <> 0 then      
    	MsgBox "Une erreur est survenue lors de l'insertion du certificat"
  	' Rediriger vers page=agent.ValiderConsultations
	window.location = "./?page=agent.ValiderConsultations"
  End If 
  
  If (document.getElementById("ctl0_CONTENU_PAGE_numEnvEnCours").value > _
  		document.getElementById("ctl0_CONTENU_PAGE_nbrEnveloppes").value And installOK=True) Then
 	'MsgBox "dernier certificat installé"
 	document.main_form.submit()
 End If
end sub

Sub DiscardCertAcceptSub()
  document.main_form.cmd.value = "do_validate_cons"
  document.main_form.submit()
End Sub

sub CertAcceptSub2()
  On error resume next
  initCertEnroll()
  If cert_enroll Then
  	x509installCertEnroll()
  Else
	  XEnroll.MyStoreFlags=&H10000
	  XEnroll.RequestStoreFlags=&H10000
	  XEnroll.acceptPKCS7(document.getElementById("ctl0_CONTENU_PAGE_certificate").value)
 End if

  dim errNum 
  errnum = Err.number
  If err.Number = -2147023673 then 
  	document.main_form.install_result.value="Annuler"
  ElseIf err.Number <> 0 then
  	document.main_form.install_result.value="KO"
  Else
  	document.main_form.install_result.value="OK"
  End If 	
	document.main_form.cmd.value="install_certificate"
    document.main_form.submit()
end sub

sub x509installCertEnroll()
    On Error Resume Next
 	Dim response
	response = document.getElementById("ctl0_CONTENU_PAGE_certificate").value
    If IsObject(objEnroll) Then
      objEnroll.Initialize(1)
      objEnroll.InstallResponse 4,response,0,""
      'MsgBox err.number
      'if err.number<>0 then
       'msgbox err.Description
      'else
       'msgbox "Certificate installed successfully. Please don't forget to backup now"
      'end if
    End if
end sub


