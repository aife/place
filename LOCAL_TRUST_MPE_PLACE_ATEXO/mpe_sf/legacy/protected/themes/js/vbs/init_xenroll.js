/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function initCertEnroll()
{
    if (! TestXEnroll())
    {
        objEnroll = classFactory.CreateObject("X509Enrollment.CX509Enrollment");
        objPrivateKey = classFactory.CreateObject("X509Enrollment.CX509PrivateKey");
        objRequest = classFactory.CreateObject("X509Enrollment.CX509CertificateRequestPkcs10");
        cert_enroll = true;
    }
}
function switch_object(id) {
    xenrollspan.innerHTML =
        "<OBJECT " +
        "  classid=\"" + id + "\" " +
        "  id=XEnroll> " +
        "</OBJECT>";
}