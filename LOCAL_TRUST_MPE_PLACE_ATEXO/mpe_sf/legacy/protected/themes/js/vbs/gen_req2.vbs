Option Explicit

Function MakeKeyFlags(keysize)
  Dim flags
  ' If CRYPT_EXPORTABLE is set, the generated private key can be exported 
  ' from IE in passphrase protected pkcs12 envelope.
  ' If CRYPT_USER_PROTECTED is set, the user is allowed to select security
  ' level for the generated private key. If not set the lowest security
  ' level is set as default. This means that private key is not protected
  ' on local disk and user is not prompted is the key is used to sign data.
  Const CRYPT_EXPORTABLE = 1
  Const CRYPT_USER_PROTECTED = 2


'  If document.main_form.protectkey.checked = true Then
'    flags = CRYPT_EXPORTABLE Or CRYPT_USER_PROTECTED
'  Else
'    flags = CRYPT_EXPORTABLE 
'  End If

' On met par d�faut ces flags-l� 
    flags = CRYPT_EXPORTABLE Or CRYPT_USER_PROTECTED

  MakeKeyFlags = flags Or ( keysize * 65536)
End Function

Function CreatePKCS10Request(keysize)
  On Error Resume Next
  Dim DNName
  Const AT_KEYEXCHANGE = 1
  Const AT_SIGNATURE = 2
'MsgBox ("cn" & document.getElementById("ctl0_CONTENU_PAGE_numEnvEnCours").value)
DNName = "CN=" & document.getElementById("ctl0_CONTENU_PAGE_cons_ref").value & document.getElementById("ctl0_CONTENU_PAGE_numEnvEnCours").value


'  If document.main_form.storetype.value = "local-machine" Then
'    XEnroll.RequestStoreFlags = &H20000
'    document.main_form.storeflags.value = "&H20000"
'  Else
'    XEnroll.RequestStoreFlags = &H10000
'    document.main_form.storeflags.value = "&H10000"
'  End If

'On met par d�faut local-machine
    XEnroll.RequestStoreFlags = &H10000

  XEnroll.GenKeyFlags = MakeKeyFlags(keysize)
  XEnroll.KeySpec = AT_KEYEXCHANGE
  'XEnroll.KeySpec = AT_SIGNATURE
  CreatePKCS10Request = XEnroll.createPKCS10(DNName, "")

  dim errNum 
  errNum = Err.number
  If errNum=-2147023673 Or errNum=-2146893792 then
  	document.main_form.cmd.value="list_new_cons"
	CreatePKCS10Request = "error"
  ElseIf errNum <> 0 Then
    MsgBox ("Impossible de g�n�rer la cl�: " & Err.Description & _
            " (" & CStr(Err.Number) & ")")
    CreatePKCS10Request = "error"
  End If
End Function

Function CreatePKCS10Request2(keysize)
  On Error Resume Next
  Dim DNName
  Const AT_KEYEXCHANGE = 1
  Const AT_SIGNATURE = 2
  DNName = "CN=" & ucase(document.main_form.titre.value)
	If document.main_form.country.value <> "" Then
		DNName = DNName & ";C=" & document.main_form.country.value
	End If
	if document.main_form.city.value <> "" Then
		DNName = DNName & ";L=" & document.main_form.city.value
	End If
	if document.main_form.company_name.value <> "" Then
		DNName = DNName & ";O=" & document.main_form.company_name.value
	End If
	if document.main_form.company_id.value <> "" Then
		DNName = DNName & ";OU=" & document.main_form.company_id.value
	End If
	if document.main_form.email.value <> "" Then
		DNName = DNName & ";E=" & document.main_form.email.value
	End If
  
  
'  If document.main_form.storetype.value = "local-machine" Then
'    XEnroll.RequestStoreFlags = &H20000
'    document.main_form.storeflags.value = "&H20000"
'  Else
'    XEnroll.RequestStoreFlags = &H10000
'    document.main_form.storeflags.value = "&H10000"
'  End If

'On met par d�faut local-machine
    XEnroll.RequestStoreFlags = &H10000

  XEnroll.GenKeyFlags = MakeKeyFlags(keysize)
  XEnroll.KeySpec = AT_KEYEXCHANGE
  'XEnroll.KeySpec = AT_SIGNATURE
  CreatePKCS10Request2 = XEnroll.createPKCS10(DNName, "")

  dim errNum 
  errNum = Err.number
  If errNum=-2147023673 Or errNum=-2146893792 then
  	document.main_form.cmd.value="manage_certificates"
	CreatePKCS10Request2 = "error"
  ElseIf errNum <> 0 Then
    MsgBox ("Impossible de g�n�rer la cl�: " & Err.Description & _
            " (" & CStr(Err.Number) & ")")
    CreatePKCS10Request2 = "error"
  End If
End Function

Function CheckKeys(keyNb)
  CheckKeys = true
End Function

Function CertRequestSub
	
	CertRequestSub = False

End Function

Sub CertRequestEdit
  Dim keysize, req
  keysize = document.main_form.keysize.value
  req = CreatePKCS10Request(keysize)
  If req = "error" Then
    Exit Sub
  End If
'  document.requestData.data.value = req
'  document.requestData.action.value = "edit"
'  document.requestData.reqkeysize.value = keysize
'  document.requestData.submit()
End Sub


'-----------------------------------------------------------------
' IE SPECIFIC:
' Get the list of CSPs from Enroll
' returns error number
' assumes Enroll is named 'Enroll' and the list box is 'document.main_form.lbCSP'
Function GetCSPList()
  On Error Resume Next
  Dim nProvType, nOrigProvType, nTotCSPs, nDefaultCSP, bNoDssBase, bNoDssDh, sUserAgent, nCSPIndexSelected
  ' should be >= the number of providers defined in wincrypt.h (~line 431)
  Const nMaxProvType=25 
  nTotCSPs=0
  nDefaultCSP=-1
  sUserAgent=navigator.userAgent
  If CInt(Mid(sUserAgent, InStr(sUserAgent, "MSIE")+5, 1))<=4 Then
    bNoDssDh=True
    bNoDssBase=True
  Else
    bNoDssDh=False
    If 0<>InStr(sUserAgent, "95") Then
      bNoDssBase=True
    ' NT 4 does not include version num in string.
    ElseIf 0<>InStr(sUserAgent, "NT)") Then 
      bNoDssBase=True
    Else
      bNoDssBase=False
    End If
  End If

  ' save the original provider type
  If (Not cert_enroll) Then
  	nOrigProvType=XEnroll.ProviderType
  	If 0 <> Err.Number Then
    	' something wrong with XEnroll
    	GetCSPList=Err.Number 
    	MsgBox "ProviderType error: " + hex(err.number), 64, "Error"
    	' Exit Function
  	End If

  	' enumerate through each of the provider types
 	Dim DefaultCSPFound
  	DefaultCSPFound = False
  	For nProvType=0 To nMaxProvType 
    		Dim nCSPIndex
    		nCSPIndex=0
    		XEnroll.ProviderType=nProvType
			
    		' enumerate through each of the providers for this type
    		Do 
      			Dim sProviderName
      			'get the name
      			sProviderName=XEnroll.enumProviders(nCSPIndex, 0)
				
      			If &H80070103=Err.Number Then 
        			' no more providers
        			Err.Clear
        			Exit Do
      			ElseIf 0<>Err.Number Then
        			' something wrong with XEnroll
        			'  - ex, Win16 IE4 XEnroll doesn't support this call.
        			GetCSPList=Err.Number 
       	 			Exit Function
      			End If
    
			'      If ("Microsoft Base DSS Cryptographic Provider"=sProviderName And True=bNoDssBase) _
			'        Or ("Microsoft Base DSS and Diffie-Hellman Cryptographic Provider"=sProviderName And True=bNoDssDh) Then
			'        ' skip this provider
      		If InStr("Microsoft Enhanced Cryptographic Provider v1.0, Gemplus GemSAFE Card CSP, Rainbow iKey 1000 RSA Cryptographic Service Provider, SafeNet RSA CSP, Gemalto Classic Card CSP", sProviderName) = 0 Then
        		' skip this provider
      			Else 
      
        			' For each provider, add an element to the list box.
        			Dim oOption
        			Set oOption=document.createElement("Option")
        			'If InStr(sProviderName, "Microsoft Enhanced Cryptographic Provider v1.0") <> 0 Then
         			' oOption.text = "Cl� logicielle - " & sProviderName
        			'ElseIf InStr(sProviderName, "Gemplus GemSAFE Card CSP") <> 0 Then
        			'  oOption.text = "Carte � puce - " & sProviderName
        			'ElseIf InStr(sProviderName, "Rainbow iKey 1000 RSA Cryptographic Service Provider") <> 0 Then
         			' oOption.text = "Cl� USB - " & sProviderName
        			'Else
        
          			oOption.text=sProviderName
				'End If
		
        			oOption.Value=nProvType
        			'If document.main_form.lbCSP.value = "" Then
        			document.main_form.lbCSP.add(oOption)
       				'End If
        
        			If InStr(sProviderName, "Microsoft Enhanced Cryptographic Provider v1.0, Gemplus GemSAFE Card CSP, Rainbow iKey 1000 RSA Cryptographic Service Provider, SafeNet RSA CSP, Gemalto Classic Card CSP") <> 0 Then
    	  				oOption.selected=True
	  				DefaultCSPFound = True
	  				nDefaultCSP=nTotCSPs
        			End If
				nTotCSPs=nTotCSPs+1
      			End If
				
      			' get the next provider
      			nCSPIndex=nCSPIndex+1
		Loop
  		Next

		'  If (DefaultCSPFound = False) Then
    		'document.main_form.cmd.value="list_new_cons"
 		'   CreatePKCS10Request = "error"
    		'document.main_form.submit()
    		'Exit Function
		'  End If

  		' if there are no CSPs, we're kinda stuck
  		If 0=nTotCSPs Then
    			Set oElement=document.createElement("Option")
    			oElement.text="-- No CSP's found --"
    			document.main_form.lbCSP.Options.Add(oElement)
  		End If

  		' remove the 'loading' text
  		document.main_form.lbCSP.remove(0)

  		' select the default provider
  		If -1 <> nDefaultCSP Then
    			document.main_form.lbCSP.selectedIndex=nDefaultCSP
  		End If

  		' restore the original provider type
  		XEnroll.ProviderType=nOrigProvType
	Else ' cas vista
		FindProvidersOnVista
  	End If
  ' set the return value and exit
  If 0 <> Err.Number Then
    GetCSPList=Err.Number
    ElseIf 0 = nTotCSPs Then
    ' signal no elements with -1
    GetCSPList=-1
  Else
    GetCSPList=0
  End If
  nCSPIndexSelected = document.main_form.lbCSP.selectedIndex
  document.main_form.namelbCSP.value = document.main_form.lbCSP.options(nCSPIndexSelected).text  
  End Function

Function postLoad()
  On Error Resume Next
  VerifyXEnroll("postLoad2")
End Function

Function postLoad2(status)
  On Error Resume Next   
  Dim nResult  
  If status = 0 Then
    'MsgBox "XEnroll version mismatch", 64, "Error"
    'Exit Function
    initCertEnroll()
    cert_enroll = True
  End If

  nResult = 0
  ' get the CSP list
  nResult = GetCSPList()
  If 0 <> nResult Then
    Exit Function
  End If
  handleCSPChange()
End Function

Function handleCSPChange()

    Dim g_bOkToSubmit, nCSPIndex, nProvType, nSupportedKeyUsages
    Dim keymax, keymin

    ' IE is not ready until XEnroll has been loaded
    g_bOkToSubmit = false

    ' some constants defined in wincrypt.h:
    Const CRYPT_EXPORTABLE = 1
    Const CRYPT_USER_PROTECTED = 2
    Const CRYPT_MACHINE_KEYSET = &H20
    Const AT_KEYEXCHANGE = 1
    Const AT_SIGNATURE = 2
    Const CERT_SYSTEM_STORE_LOCAL_MACHINE = &H20000
    Const ALG_CLASS_ANY = 0
    Const ALG_CLASS_SIGNATURE = &H2000
    Const ALG_CLASS_HASH = &H4000
    Const PROV_DSS=3
    Const PROV_DSS_DH=13

    ' convenience constants, for readability
    Const KEY_LEN_MIN=true
    Const KEY_LEN_MAX=false
    Const KEY_USAGE_EXCH=0
    Const KEY_USAGE_SIG=1
    Const KEY_USAGE_BOTH=2

    ' defaults
    Const KEY_LEN_MIN_DEFAULT=384
    Const KEY_LEN_MAX_DEFAULT=16384
    nCSPIndex = document.main_form.lbCSP.selectedIndex
    
    On Error Resume Next
	    If Err.Number <> 0 Then
	    	XEnroll.ProviderName = document.main_form.lbCSP.options(nCSPIndex).text
		    nProvType = document.main_form.lbCSP.options(nCSPIndex).value
		   	XEnroll.ProviderType = nProvType
	 	 End if
	   	
    document.main_form.namelbCSP.value = document.main_form.lbCSP.options(nCSPIndex).text


    nSupportedKeyUsages = AT_KEYEXCHANGE



    if (PROV_DSS = nProvType OR PROV_DSS_DH = nProvType) then
     nSupportedKeyUsages = AT_SIGNATURE
    End If

    document.getElementById("ctl0_CONTENU_PAGE_keyusage").value="both"

End Function

Sub TriggerAddCertificate
    If document.main_form.certificate_create.checked Then
      Dim keysize, req
      keysize = document.main_form.keysize.value
      If cert_enroll Then
      	  req = CreateCertEnrollPKCS10Request(document.main_form.titre.value)
      ELSE
	      req = CreatePKCS10Request2(keysize)
	  End If
      If req = "error" Then
        Exit Sub
      End If
      document.main_form.pkcs10_req.value = req
    ElseIf document.main_form.certificate_sequestre.checked Then
      Dim req2
      req2 =CreatePkcs10ForGeneratedCard2()
      If req2 = "error" Then
        Exit Sub
      End If
      document.main_form.pkcs10_req.value = req2
    ElseIf  document.main_form.certificate_discard.checked Then
      Dim TheCertificate
      TheCertificate = mpe.ProbeCertificate()
      If TheCertificate = "NO_CERTIFICATE" Then
        Exit Sub
      ElseIf TheCertificate = "INVALID_CERTIFICATE" Then
        MsgBox "Le bi-cl� s�lectionn� ne peut �tre utilis� pour les op�rations de chiffrement / d�chiffrement." & vbCrLf & vbCrLf & "Adressez-vous � votre autorit� de certification pour obtenir un bi-cl� de chiffrement / d�chiffrement.", ,"Bi-cl� invalide"
        Exit Sub
      Else
        document.main_form.user_certificate.value = TheCertificate
      End If
    End If
  document.main_form.cmd.value = "validate_certificate"
  'document.main_form.submit()
End Sub

Sub TriggerModifyCertificate
If document.main_form.certificate_discard.checked = false Then
    If document.main_form.certificate_create.checked Then
      Dim keysize, req
      keysize = document.main_form.keysize.value
      req = CreatePKCS10Request2(keysize)
      If req = "error" Then
        Exit Sub
      End If
      document.main_form.pkcs10_req.value = req
    ElseIf document.main_form.certificate_sequestre.checked Then
      Dim req2
      req2 =CreatePkcs10ForGeneratedCard2()
      If req2 = "error" Then
        Exit Sub
      End If
      document.main_form.pkcs10_req.value = req2
    Else
      Dim TheCertificate
      TheCertificate = mpe.ProbeCertificate()
      If TheCertificate = "NO_CERTIFICATE" Then
        Exit Sub
      ElseIf TheCertificate = "INVALID_CERTIFICATE" Then
        MsgBox "Le bi-cl� s�lectionn� ne peut �tre utilis� pour les op�rations de chiffrement / d�chiffrement." & vbCrLf & vbCrLf & "Adressez-vous � votre autorit� de ceon pour obtenir un bi-cl� de chiffrement / d�chiffrement.", ,"Bi-cl� invalide"
        Exit Sub
      Else
        document.main_form.user_certificate.value = TheCertificate
      End If
    End If
End If

  document.main_form.cmd.value = "validate_certificate"
  document.main_form.submit()
End Sub


Function CreatePkcs10ForGeneratedCard()
	On Error Resume Next
End Function

Function CreatePkcs10ForGeneratedCard2()
	On Error Resume Next
  Dim DNName
  DNName = "CN=" & UCase(document.main_form.titre.value)
  Const AT_KEYEXCHANGE = 1
  Const AT_SIGNATURE = 2
  Const PROV_RSA_FULL = 1
  Const CRYPT_MACHINE_KEYSET = &H20
   
  XEnroll.KeySpec = AT_KEYEXCHANGE
  XEnroll.UseExistingKeySet = True
  XEnroll.ContainerName = "ATEXO - Local Trust MPE - Sequestre"

  XEnroll.ProviderType = PROV_RSA_FULL
  XEnroll.ProviderName = "Gemplus GemSAFE Card CSP"

  XEnroll.ProviderFlags = CRYPT_MACHINE_KEYSET
  XEnroll.resetExtensions()
  XEnroll.EnableSMIMECapabilities = false
  XEnroll.resetAttributes()
  CreatePkcs10ForGeneratedCard2 = XEnroll.createPKCS10(DNName, "")
  If Err.Number <> 0 Then
    MsgBox ("Impossible de g�n�rer la PKCS10: " & Err.Description & _
            " (" & CStr(Err.Number) & ")")
  CreatePkcs10ForGeneratedCard2 = "error"
  End if
End Function


 Sub FindProvidersOnVista()
    'MsgBox "Called FindProvidersOnVista"
    Dim cspInfos, cspInfo
    Dim cspCount
    Dim oOptionCertEnroll
    On Error Resume Next

    Set cspInfos = CreateObject("X509Enrollment.CCspInformations")

    'MsgBox "X509Enrollment.CCspInformations Object Created"

    If ( err.number <> 0 ) then
       MsgBox Err.Number & " " & Err.Description
       Exit Sub
    End If

    On Error Resume Next
    'MsgBox "Adding CSPs"
    cspInfos.AddAvailableCsps()
    'MsgBox "Added CSPs"

    If ( err.number <> 0 ) then
       MsgBox Err.Number & " " & Err.Description
       Exit Sub
    End If

    cspCount = cspInfos.Count
    'MsgBox "Number of CSPs: " & cspCount

    Dim i
    Dim j
    Dim el
    Dim first

    i = 1
    j = 12
    first = 0

    Do While True
       If i = cspCount Then
          Exit Do
       End If

       set cspInfo = cspInfos.ItemByIndex(i)
       'MsgBox "CSP Name[" & i & "]: " & cspInfo.Name & ", Type: " & cspInfo.Type

       If InStr("Microsoft Enhanced RSA and AES Cryptographic Provider, Gemplus GemSAFE Card CSP, Rainbow iKey 1000 RSA Cryptographic Service Provider, SafeNet RSA CSP, Gemalto Classic Card CSP", cspInfo.Name) = 0 Then
        		' skip this provider
       Else
          	Set oOptionCertEnroll=document.createElement("Option")
			oOptionCertEnroll.Value=i
        	oOptionCertEnroll.text=cspInfo.Name
            document.main_form.lbCSP.add(oOptionCertEnroll)
       End If
       i = i + 1
    Loop
    document.main_form.lbCSP.remove(0)
    nCSPIndexSelected = document.main_form.lbCSP.selectedIndex
    document.main_form.namelbCSP.value = document.main_form.lbCSP.options(nCSPIndexSelected).text  
 End Sub
