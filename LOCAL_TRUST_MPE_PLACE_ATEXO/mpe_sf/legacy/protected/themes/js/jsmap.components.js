
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/*
 *  Composants JS carte interactive
 *
 *  Version 1.0
 *
 *
 *  Composants jsMap, jsMapSubItem, jsMapSubItemsTab, jsMapItem, jsMapItemsTab
 *
 */


//Composant jsMapSubItem
jsMapSubItem = function (obj, map, type, pos, subpos){

	this.component     = 'jsMapSubItem';

	this.name          = obj.name; //ex : "Somme"
	this.code          = obj.code; //ex : "80"
	this.nuts          = obj.nuts; //ex : "FR223"
	this.path          = obj.path; //Chemin vectoriel de l'element
	this.color         = obj.color; //ex : "#edc667"
	this.map           = map; //Carte JS

	this.id            = 'sub_it_'+this.nuts;
	this.label         = this.code+' - '+this.name; //ex : "80 - Somme"

	this.local         = +obj.local; //Sous-element a placer dans la carte locale ou non
	this.local_color   = obj.local_color; //Couleur du sous-element en mode "local"
	this.local_selcolor= obj.local_selcolor; //Couleur du sous-element selectionne en mode "local"
	this.local_path    = obj.local_path; //Chemin vectoriel du sous-element en mode "local"

	this.pos           = pos; //Position de l'element parent dans le tableau listant les elements de la carte
	this.subpos        = subpos; //Position du sous-element courant dans le tableau listant les sous elements de l'element parent

	this.type          = type; //ex : departement

	this.selected      = false; // Sous-element selectionne ou non

	/**
     * Met a jour la valeur de l'etat de selection du sous-element
     *
     */
	this.applySelection = function(selection){

		//Modification de l'etat de selection du sous-element
		this.selected = selection;
		
		//Actualisation de la couleur du sous-element
		if(this.map.mode == this.type){
		
			if(this.map.browser == 'MSIE' && (this.map.browser_version == '7' || this.map.browser_version == '8' )){
				
				this.draw();
				
			} else {
				
				this.updateColor();
				
			}		
		}
	}

	/**
    * Modifie la couleur du sous-element
    *
    */
	this.setColor = function(color){

		//Recuperation de l'ID HTML du sous-element
		var map_id   = '#'+this.id;
		
		//Recuperation du composant HTML du sous-element
		var map_item = J(map_id);
		
		//Actualisation de la couleur du composant HTML
        map_item.attr('fill',color);
	}
	
		/**
     * Met a jour la couleur du sous-element en fonction de son statut "selectionne"/"non selectionne"
     *
     */
	this.updateColor = function (){

		var show_local = this.map.show_local;

		if(this.selected){

			if(show_local && this.local){

				this.setColor(this.local_selcolor);

			} else {

				this.setColor(this.map.features.selcolor);

			}

		} else {

			if(show_local && this.local){

				this.setColor(this.local_color);

			} else {

				this.setColor(this.color);

			}
		}
	}
	
	/**
     * Met a jour la couleur du sous-element en fonction de son statut "selectionne"/"non selectionne"
     *
     */
	this.drawColor = function (){
		
		var map_id = this.map.name + '.' + this.id;
		
		if(this.selected){

			if(this.map.show_local && this.local){

				this.map.container.map_id = this.map.container.path(this.local_path).attr(this.map.features).attr('fill',this.local_selcolor);

			} else {

				this.map.container.map_id = this.map.container.path(this.path).attr(this.map.features).attr('fill',this.map.features.selcolor);

			}

		} else {

			if(this.map.show_local && this.local){

				this.map.container.map_id = this.map.container.path(this.local_path).attr(this.map.features).attr('fill',this.local_color);

			} else {
				
				this.map.container.map_id = this.map.container.path(this.path).attr(this.map.features).attr('fill',this.color);

			}
		}
	}

	/**
     * Dessine le chemin vectoriel du sous-element et les evenements lies
     *
     */
	this.draw = function(){

		//Attributs de l'objet Raphael a creer

		//Ajouter cet objet a la carte
		var map_id     = this.map.name + '.' + this.id;
		
		this.drawColor();
		
		//Mettre a jour ses proprietes
		this.map.container.map_id[0].style.cursor = 'pointer';
		this.map.container.map_id[0].setAttribute("id",this.id);
				
				
		var obj    = this.map.container.map_id;
		var obj_id = '#'+this.map.container.map_id[0].id;
		var label  = this.label;
		
		//Ajout evenement "onMouseOver"
		this.map.container.map_id[0].onmouseover= function () {

			tipText = label;

			obj.attr('opacity',0.5);
			map.container.safari();
			// no longer usable
			//J(tip).stop(true, true).show();
			over = true;

		};

		//Ajout evenement "onMouseOut"
		this.map.container.map_id[0].onmouseout= function () {

			obj.attr('opacity',1);
			map.container.safari();
			// no longer usable
			//J(tip).stop(true, true).hide();
			over = false;

		};

		//Ajout evenement "onClick"
		var js_obj       = this;
		var onmouseclick = this.map.onmouseclick;
		this.map.container.map_id[0].onclick = function () {
			onmouseclick(js_obj);
		};

	}

	/**
     * Deselectionne le sous-element
     *
     */
	this.unselect = function (){

		var selection = false;

		this.applySelection(selection);

	}

}


//Composant jsMapSubItemsTab
jsMapSubItemsTab = function (map, pos, json_subitems){

	this.map           = map;
	this.type          = json_subitems.type;
	this.json_subitems = json_subitems.tab;

	this.tab           = new Array();

	for(var i = 0, c = this.json_subitems.length; i < c; i++){

		//Position du sous element dans le tableau
		var subpos  = this.tab.length;

		//Creation du sous-element
		var subitem = null;
		subitem     = new jsMapSubItem(this.json_subitems[i], this.map, this.type, pos, subpos);

		//Ajout du sous-element au tableau des sous-elements
		this.tab[subpos] = subitem;

	}

	/**
     * Modifie la valeur du statut de selection pour l'ensemble des sous-elements references dans le tableau des sous-elements
     *
     */
	this.applySelection = function(selection){

		for (var i = 0, c = this.tab.length; i < c; i++) {

			this.tab[i].applySelection(selection);

		}

	}

	/**
     * Dessine les sous-elements references dans le tableau des sous-elements
     *
     */
	this.draw = function(subitems){

		if(this.map.mode == this.type){

			for (var i = 0, c = this.tab.length; i < c; i++) {

				var selection = false;
				if(subitems.subitems.tab[i].id){

					selection = subitems.subitems.tab[i].selection;

				}

				this.tab[i].draw(selection);

			}
		}

	}

	/**
     * Chargement des sous-elements deja selectionnes
     *
     */
	this.loadSelectedSubitems = function(selSubitemsTab){

		//Initialisation
		var selection = true;
		var d         = selSubitemsTab.length;
		
		if(d > 0){
			//Parcours de l'ensemble des sous-elements composant le tableau des sous-elements
			for (var i = 0, c = this.tab.length; i < c; i++) {
				//Si navigateur IE7 ou IE8 (version JS ne prenant pas en compte la mathode indexOf)
				if(this.map.browser == 'MSIE' && (this.map.browser_version == '7' || this.map.browser_version == '8' )){	//|| this.map.browser_version == '9')){
					//Verification presence du sous-element dans le tableau des sous-elements precedemment selectionnes				
					for(var j = 0; j < d; j++){				
					
						if(selSubitemsTab[j] == this.tab[i].code){ // cette ligne pose pb sous IE7
							this.tab[i].applySelection(selection); // cette ligne pose pb sous IE7
							break;
						}				
					}
				} else {
					//Si navigateurs plus evolues
					if(selSubitemsTab.indexOf(this.tab[i].code)> -1){
						//Mise a jour du statut du sous-element
						this.tab[i].applySelection(selection);
					}
				}
			}
		}
	}

}


//Composant jsMapItem
jsMapItem = function (obj, map, type, pos, subitems){

	this.component     = 'jsMapItem';

	this.name          = obj.name;
	this.code          = obj.code;
	this.nuts          = obj.nuts;
	this.path          = obj.path;
	this.color         = obj.color;
	this.map           = map;

	this.local         = +obj.local;

	this.id            = 'it_'+this.nuts;
	this.label         = this.name;

	this.pos           = pos;
	this.type          = type;

	this.subitems      = subitems;

	this.selected          = false;
	this.selected_subitems = false;

	/**
     * Met a jour la valeur de l'etat de selection de l'element
     *
     */
	this.applySelection = function(selection){

		//Modification de l'attribut pour les sous-composants
		this.subitems.applySelection(selection);

		//Mise a jour des proprietes du composant
		this.update();

	}
	
	/**
     * Modifie la couleur de l'element
     *
     */
	this.setColor = function (color){

		var map_id   = '#'+this.id;
		var map_item = J(map_id);
		map_item.attr('fill', color);

	}

	/**
     * Met a jour la couleur de l'element en fonction de son statut "selectionne"/"non selectionne"
     *
     */
	this.updateColor = function (){

		if(this.selected){

			this.setColor(this.map.features.selcolor);

		} else {

			this.setColor(this.color);

		}
	}


	/**
     * Met a jour la couleur de l'element en fonction de son statut "selectionne"/"non selectionne"
     *
     */
	this.drawColor = function (){

		var map_id  = this.map.name + '.' + this.id;

		if(this.selected){

			this.map.container.map_id = this.map.container.path(this.path).attr(this.map.features).attr('fill',this.map.features.selcolor);

		} else {

			this.map.container.map_id = this.map.container.path(this.path).attr(this.map.features).attr('fill',this.color);

		}		
	}

	/**
     * Met a jour le statut "selectionne"/"non selectionne" de l'element en fonction du statut de ses sous-elements
     *
     */
	this.update = function(){

		var selected     = 0;
		var not_selected = 0;

		//Verification du statut de selection des elements imbriques
		for (var i = 0, c = this.subitems.tab.length; i < c; i++) {

			if(this.subitems.tab[i].selected){

				selected += 1;

			} else {

				not_selected += 1;

			}
		}

		//Test pour la mise a jour du statut de l'element imbriquant
		if(selected == this.subitems.tab.length){

			//Tous les sous elements selectionnes

			//Mise a jour de l'attribut selected_subitems
			this.selected_subitems = true;

			//Mise a jour de l'attribut selected
			this.selected = true;


		} else if(not_selected == this.subitems.tab.length){

			//Tous les sous elements deselectionnes

			//Mise a jour de l'attribut selected_subitems
			this.selected_subitems = false;

			//Mise a jour du statut
			this.selected = false;


		} else {

			//Il y a a la fois des sous elements selectionnes et deselectionnes
			//Si au moins un subitem selectionne, l'item est selectionne

			//Mise a jour de l'attribut selected_subitems
			this.selected_subitems = true;

			//Mise a jour de l'attribut selected
			this.selected = true;


		}		
		
		//Si IE7 et IE8
		if(this.map.browser == 'MSIE' && (this.map.browser_version == '7' || this.map.browser_version == '8')){
		
			if(this.map.mode == this.type){
			
				this.draw(this.selected);
		
			}
		} else {
			
			//Si navigateurs plus evolues
			this.updateColor();
			
		}
	}

	/**
     * Dessine le chemin vectoriel de l'element et les evenements lies aux interactions avec la souris
     *
     */
	this.draw = function(){

		//Ajouter cet objet a la carte
		var map_id = this.map.name + '.' + this.id;		
					
		this.drawColor();
		
		//Mettre a jour ses proprietes
		this.map.container.map_id[0].style.cursor = 'pointer';
		this.map.container.map_id[0].setAttribute("id",this.id);

		var obj    = this.map.container.map_id;
		var obj_id = '#'+this.map.container.map_id[0].id;
		var label  = this.label;
		
		//Ajout evenement "onMouseOver"
		this.map.container.map_id[0].onmouseover= function () {

			tipText = label;

			obj.attr('opacity',0.5);
			map.container.safari();
            // no longer usable
			//J(tip).stop(true, true).show();
			over = true;

		};

		//Ajout evenement "onMouseOut"
		this.map.container.map_id[0].onmouseout= function () {

			obj.attr('opacity',1);
			map.container.safari();
            // no longer usable
			//J(tip).stop(true, true).hide();
			over = false;

		};
		
		//Ajout evenement "onClick"
		var js_obj       = this;
		var onmouseclick = this.map.onmouseclick;
		this.map.container.map_id[0].onclick = function () {
			onmouseclick(js_obj);
		};

	}

	/**
     * Dessine les sous-elements de l'element
     *
     */
	this.drawSubitems = function(subitems){

		var show_local = this.map.show_local;

		//Tracage des elements si la vue n'est pas locale ou si ils font partie de la vue locale
		if((show_local && this.local) || !show_local){

			this.subitems.draw(subitems);

		}
	}


	/**
     * Deselectionne l'element
     *
     */
	this.unselect = function(){

		var selection = false;

		//Mise a jour de l'attribut de selection du composant
		this.applySelection(selection);

	}

	/**
     * Chargement des sous-elements deja selectionnes
     *
     */
	this.loadSelectedSubitems = function(selSubitemsTab){
		
		if(selSubitemsTab.length > 0){
			//Chargement des sous-elements deja selectionnes
			this.subitems.loadSelectedSubitems(selSubitemsTab);
			//Mise a jour de l'element courant en fonction du statut de ses sous-elements
			this.update();
		}
	}

}


//Composant jsMapItemsTab
jsMapItemsTab = function (map, json_items){

	this.map          = map;
	this.type         = json_items.type;
	this.json_items   = json_items.tab;

	this.tab          = new Array();

	for(var i = 0, c = this.json_items.length; i < c; i++){

		//Position de l'element dans le tableau
		var pos = this.tab.length;

		//Recuperation des donnnes parsees a partir du format JSON
		var json_subitems = this.json_items[i].subitems;

		//Creation des sous-elements
		var subitems_tab = new jsMapSubItemsTab(this.map, pos, json_subitems);

		//Creation de l'element
		var item = null;
		item = new jsMapItem(this.json_items[i], this.map, this.type, pos, subitems_tab);

		//Ajout de l'element au tableau des elements
		this.tab[pos] = item;

	}

	/**
     * Charge les sous-elements devant etre pre-selectionnes
     *
     */
	this.update = function(){

		for (var i = 0, c = this.tab.length; i < c; i++) {

			this.tab[i].update();

		}

	}

	/**
     * Dessine les elements references dans le tableau des elements
     *
     */
	this.draw = function (map){

		if(map.mode == this.type){

			for (var i = 0, c = this.tab.length; i < c; i++) {

				var item_selection = '';

				if(map.items.tab[i].id){

					item_selection = map.items.tab[i].selection;

				}

				this.tab[i].draw(item_selection);

			}

		} else {

			for (var i = 0, c = this.tab.length; i < c; i++) {

				this.tab[i].drawSubitems(map.items.tab[i]);

			}
		}
	}

	/**
     * Deselectionne l'ensemble des elements contenus dans le tableau des elements
     *
     */
	this.unselect = function (){

		for (var i = 0, c = this.tab.length; i < c; i++) {

			this.tab[i].unselect();

		}
	}


	/**
     * Charge les sous-elements devant etre pre-selectionnes
     *
     */
	this.loadSelectedSubitems = function(selSubitemsTab){

		if(selSubitemsTab.length > 0){

			for (var i = 0, c = this.tab.length; i < c; i++) {

				//Chargement des sous-elements deja selectionnes
				this.tab[i].loadSelectedSubitems(selSubitemsTab);

			}
		}
	}

}



//Composant jsMap

jsMap = function (map_json, onmouseclick, browser_name, browser_version){

	//Initialisation des valeurs des attributs
	this.component             = 'jsMap';
	this.mode                  = ''; //Mode de la carte pour savoir quels elements tracer
	this.name                  = map_json.name; //Nom de la carte
	this.canvas_items          = map_json.canvas_items; //Parametres du canvas des elements de la carte
	this.canvas_subitems       = map_json.canvas_subitems; //Parametres du canvas des sous elements de la carte
	this.canvas_local_subitems = map_json.canvas_local_subitems; //Parametres du canvas des elements de la carte en mode affichage local
	this.features              = map_json.features; //Caracteristiques generaux de la carte
	this.json_items            = map_json.items; //Elements de la carte au format JSON

	this.show_local            = true; //Afficher les elements de la carte locale

	//Recuperation des methodes pour la gestion evenementielles
	this.onmouseclick          = onmouseclick;

	//Creation des elements de la carte
	var items                  = new jsMapItemsTab (this, this.json_items);
	this.items                 = items;
	
	//Recuperation des informations sur le navigateur
	this.browser               = browser_name;
	this.browser_version       = browser_version;


	/**
     * Dessine le canvas des sous-elements a dessiner dans la vue locale
     *
     */
	this.drawCanvasLocalSubitems = function (){

		//Suppresion du contenu du canvas
		J('#paper').empty();

		//Initialisation de l'objet Raphael
		this.container = Raphael("paper", +this.canvas_local_subitems.paper_width, +this.canvas_local_subitems.paper_height);

		//Initialisation du canvas de l'objet Raphael
		this.container.setViewBox(+this.canvas_local_subitems.x, +this.canvas_local_subitems.y, +this.canvas_local_subitems.width, +this.canvas_local_subitems.height, +this.canvas_local_subitems.fit);

	}


	/**
     * Dessine le canvas des elements a dessiner dans la vue regionale
     *
     */
	this.drawCanvasItems = function (){

		//Suppresion du contenu du canvas
		J('#paper').empty();

		//Initialisation de l'objet Raphael
		this.container = Raphael("paper", +this.canvas_items.paper_width, +this.canvas_items.paper_height);

		//Initialisation du canvas de l'objet Raphael
		this.container.setViewBox(+this.canvas_items.x, +this.canvas_items.y, +this.canvas_items.width, +this.canvas_items.height, +this.canvas_items.fit);

	}

	/**
     * Dessine le canvas des sous-elements a dessiner dans la vue departementale
     *
     */
	this.drawCanvasSubitems = function (){

		//Suppresion du contenu du canvas
		J('#paper').empty();

		//Initialisation de l'objet Raphael
		this.container = Raphael("paper", +this.canvas_subitems.paper_width, +this.canvas_subitems.paper_height);

		//Initialisation du canvas de l'objet Raphael
		this.container.setViewBox(+this.canvas_subitems.x, +this.canvas_subitems.y, +this.canvas_subitems.width, +this.canvas_subitems.height, +this.canvas_subitems.fit);

	}

	/**
     * Dessine la vue "regionale"
     *
     */
	this.drawItems = function (obj,mode){

		//Mise a jour du mode de la carte
		this.mode = mode;		
		this.show_local = false;

		//Tracage du canvas des elements
		this.drawCanvasItems();

		//Tracage des elements
		this.items.draw(obj);

		//Recuperation de l'objet cree
		var obj = this;

		return obj;

	}

	/**
     * Dessine la vue "departementale"
     *
     */
	this.drawSubitems = function (obj,mode){

		//Mise a jour du mode de la carte
		this.mode = mode;		
		this.show_local = false;

		//Tracage du canvas des sous elements
		this.drawCanvasSubitems();

		//Tracage des sous elements
		this.items.draw(obj);

		//Recuperation de l'objet cree
		var obj = this;

		return obj;

	}

	/**
     * Dessine la vue "locale"
     *
     */
	this.drawLocalSubitems = function (obj,mode){

		//Mise a jour du mode de la carte
		this.mode = mode;		
		this.show_local = true;

		//Tracage du canvas des sous elements
		this.drawCanvasLocalSubitems();

		//Tracage des sous elements
		this.items.draw(obj);

		//Recuperation de l'objet cree
		var obj = this;

		return obj;

	}

	/**
     * Met a jour l'ensemble des elements et sous-elements composant la carte
     *
     */
	this.update = function (){
		//Mise a jour des elements et sous elements de la carte (statut "selectionne")
		this.items.update();

	}

	/**
     * Deselectionne l'ensemble des elements et sous-elements composant la carte
     *
     */
	this.unselect = function(){
		//Mise a jour des elements et sous elements de la carte (statut "selectionne")
		this.items.unselect();

	}

	/**
     * Charge l'ensemble des sous-elements de la carte devant etre pre-selectionnes
     *
     */
	this.loadSelectedSubitems = function(selSubitemsTab){

		if(selSubitemsTab.length > 0){
			//Chargement des sous-elements preselectionnes
			this.items.loadSelectedSubitems(selSubitemsTab);
		}
	}


}


