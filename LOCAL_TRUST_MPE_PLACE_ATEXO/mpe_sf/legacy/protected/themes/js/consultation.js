/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function refreshOpener(invite)
{
document.getElementById('ctl0_CONTENU_PAGE_invites').value=invite; 
var refreshButton = document.getElementById('ctl0_CONTENU_PAGE_refreshButton'); 
refreshButton.click();
}