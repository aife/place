/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

var over = false;
var js_map;
var map_json = new Array();


/**
 * Recuperer le nom du navigateur
 * @returns browser_name
 */
function get_browser() {

    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return 'Opera ' + tem[1];
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[0];
}

/**
 * Recuperer le numero de version du navigateur
 * @returns browser_version
 */
function get_browser_version() {

    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return 'Opera ' + tem[1];
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[1];
}


function onCreateMap() {

    var browser_name = get_browser();
    var browser_version = get_browser_version();

    js_map = new jsMap(map_json, onMouseClick, browser_name, browser_version);

}

function onInitMap() {

    //On retire l'affichage local si aucune donnee locale n'est presente dans la carte
    if (typeof(js_map.canvas_local_subitems) == 'undefined') {

        if (typeof(J('#localSubitemRadioGroup')[0]) != 'undefined') {

            node = J('#localSubitemRadioGroup');
            node.hide();

        }

        if (typeof(J('#subitemRadioGroup')[0]) != 'undefined') {

            J('#subitemRadioGroup input')[0].checked = 'checked';

        }

        js_map = js_map.drawSubitems(js_map, "departement");

    } else {

        if (typeof(J('#localSubitemRadioGroup')[0]) == 'undefined' && typeof(J('#subitemRadioGroup')[0]) != 'undefined') {

            J('#subitemRadioGroup input')[0].checked = 'checked';
            js_map = js_map.drawSubitems(js_map, "departement");

        } else {

            js_map = js_map.drawLocalSubitems(js_map, "departement");

        }
    }

    onInitMapCallback();

}


function onInitMapCallback() {

    if (typeof(selected_items) != 'undefined' && selected_items != '') {

        var selected_items_tab = selected_items.split('_');

        //Chargement des sous-elements pre-selectionnes
        js_map.loadSelectedSubitems(selected_items_tab);

        //Parcours des elements de la carte
        for (var i in js_map.items.tab) {

            if (js_map.items.tab[i].selected_subitems) {

                //Recuperation des attributs de l'element
                var optgroup_code = js_map.items.tab[i].code;
                var optgroup_name = js_map.items.tab[i].name;
                var optgroup_nuts = js_map.items.tab[i].nuts;

                //Creation d'un nouvel element HTML
                var optgroup = J('<optgroup>');
                optgroup.attr('label', optgroup_name);
                optgroup.attr('groupId', optgroup_nuts);
                optgroup.attr('iid', i);


                //Parcours des sous elements de la carte
                for (var j in js_map.items.tab[i].subitems.tab) {

                    if (js_map.items.tab[i].subitems.tab[j].selected) {

                        //Recuperation des attributs de l'element
                        var subitem_label = js_map.items.tab[i].subitems.tab[j].label;
                        var subitem_nuts = js_map.items.tab[i].subitems.tab[j].nuts;
                        var subitem_code = js_map.items.tab[i].subitems.tab[j].code;

                        var option = '<option sid="' + j + '" iid="' + i + '" region="' + optgroup_nuts + '" value="' + subitem_code + '">' + subitem_label + '</option>';

                        optgroup.append(option);

                    }
                }

                //Ajout de l'element HTML
                J(".liste-region-selectionnes").append(optgroup);

            }

        }
    }

}


function displayLocalSubitemMap() {

    js_map = js_map.drawLocalSubitems(js_map, "departement");

}

function displaySubitemMap() {

    js_map = js_map.drawSubitems(js_map, "departement");

}


function displayItemMap() {

    js_map = js_map.drawItems(js_map, "region");

}


function onMouseClick(map_item) {

    //Changement de statut "selection"
    selection = !map_item.selected;

    //Application du changement de statut "selection"
    map_item.applySelection(selection);

    //Rafraichissement de la carte
    js_map.update();

    //Retracage de la carte
    map_item.draw(selection);

    //Rafraichissement boite de selection
    onMouseClickCallback(map_item);

}


function onMouseClickCallback(map_item) {

    //Recuperation du groupe du select HTML correspondant
    var optgroup_node = J(".liste-region-selectionnes").find('optgroup[iid=' + map_item.pos + ']');
    var optgroup_exists = optgroup_node.length;

    //Si le composant est un item
    if (map_item.component == 'jsMapItem') {

        //Suppression du groupe du select HTML si il existe
        if (optgroup_exists) {

            optgroup_node.remove();

        }

        //Si le composant est selectionne, ajout au select HTML
        if (map_item.selected) {

            //Recuperation des attributs du groupe
            var optgroup_code = map_item.code;
            var optgroup_name = map_item.name;
            var optgroup_nuts = map_item.nuts;

            //Creation d'un nouveau groupe dans le select HTML
            var optgroup = J('<optgroup>');
            optgroup.attr('iid', map_item.pos);
            optgroup.attr('label', optgroup_name);
            optgroup.attr('groupId', optgroup_nuts);

            //Ajout des options au groupe dans le select HTML
            for (j in map_item.subitems.tab) {

                if (map_item.subitems.tab[j].selected) {

                    //Recuperation des attributs de l'option
                    var subitem_label = map_item.subitems.tab[j].label;
                    var subitem_nuts = map_item.subitems.tab[j].nuts;
                    var subitem_code = map_item.subitems.tab[j].code;

                    var option = '<option sid="' + j + '" iid="' + map_item.pos + '" region="' + optgroup_nuts + '" value="' + subitem_code + '">' + subitem_label + '</option>';

                    optgroup.append(option);

                }
            }

            //Ajout du nouveau groupe au select HTML
            J(".liste-region-selectionnes").append(optgroup);

        }

    } else if (map_item.component == 'jsMapSubItem') {

        //Si le composant est un subitem

        //Si le groupe existe dans le select HTML
        if (optgroup_exists) {

            if (map_item.selected) {

                //Recuperation des attributs du groupe
                var optgroup_nuts = map_item.map.items.tab[map_item.pos].nuts;

                //Ajout de la nouvelle option a la fin

                //Recuperation des attributs de l'option
                var subitem_label = map_item.label;
                var subitem_nuts = map_item.nuts;
                var subitem_code = map_item.code;

                var option = '<option sid="' + map_item.subpos + '" iid="' + map_item.pos + '" region="' + optgroup_nuts + '" value="' + subitem_code + '">' + subitem_label + '</option>';

                optgroup_node.append(option);

                //Suppression du groupe du select HTML
                optgroup_node.remove();

                //Ajout du groupe a la fin du select HTML
                J(".liste-region-selectionnes").append(optgroup_node);

            } else {

                //Suppression de l'option correspondante dans le select HTML si elle existe
                var option_node = optgroup_node.find('option[sid=' + map_item.subpos + ']');
                var option_exists = option_node.length;

                if (option_exists) {

                    option_node.remove();

                }

                //Suppression du groupe parent du select HTML
                optgroup_node.remove();

                //Retablissement (a la fin) du groupe parent si il est non vide
                if (optgroup_node.children().length) {

                    //Ajout du groupe a la fin du select HTML
                    J(".liste-region-selectionnes").append(optgroup_node);

                }

            }

        } else {

            //Creation du groupe dans le select HTML

            //Recuperation des attributs du groupe
            var optgroup_code = map_item.map.items.tab[map_item.pos].code;
            var optgroup_name = map_item.map.items.tab[map_item.pos].name;
            var optgroup_nuts = map_item.map.items.tab[map_item.pos].nuts;

            //Creation d'un groupe dans le select HTML
            var optgroup = J('<optgroup>');
            optgroup.attr('iid', map_item.pos);
            optgroup.attr('label', optgroup_name);
            optgroup.attr('groupId', optgroup_nuts);

            //Ajout de la nouvelle option au groupe HTML
            var subitem_label = map_item.label;
            var subitem_nuts = map_item.nuts;
            var subitem_code = map_item.code;

            var option = '<option sid="' + map_item.subpos + '" iid="' + map_item.pos + '" region="' + optgroup_nuts + '" value="' + subitem_code + '">' + subitem_label + '</option>';

            optgroup.append(option);

            //Ajout du nouveau groupe au select HTML
            J(".liste-region-selectionnes").append(optgroup);

        }
    }
}

function colorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
    }
    lum = lum || 0;
    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00"+c).substr(c.length);
    }
    return rgb;
}

//Afficher la legende lorsque la souris se place sur un element de la carte
J(document).mousemove(function (e) {


    if (over) {

        var $map_tooltip = J('#id-map-tooltip'),
            _color = J(e.target).attr('fill');


        $map_tooltip.find('.tooltip-inner').html(tipText);
        $map_tooltip.find('.tooltip-arrow').css("borderTopColor", colorLuminance(_color, -0.5));
        $map_tooltip.find('.tooltip-inner').css("backgroundColor", colorLuminance(_color, -0.3));
        $map_tooltip.find('.tooltip-inner').css("borderColor", colorLuminance(_color, -0.5));
        if ($map_tooltip.not(":visible")) {
            $map_tooltip.show();
        }
        $map_tooltip.css({
            "top": (e.pageY - $map_tooltip.innerHeight() - 5 + 'px'),
            "left": (e.pageX - ($map_tooltip.innerWidth() / 2) + 'px')
        });
    } else {
        J('#id-map-tooltip').hide();
    }
})


//Ajout/Modification du champ "Ma selection actuelle"
J(document).ready(function () {

    //Vider la liste
    J(".vider-liste ").click(function () {

        //Deselection de tous les composants de la carte
        js_map.unselect();

        //Mise a jour du select HTML
        J(".liste-region-selectionnes").empty();
        J('.clearDomTom').html('');

        //On force le rafraichissement de la carte
        js_map.update();

        //Retracage de la carte
        if (typeof(J('#localSubitemRadioGroup input')[0]) != 'undefined'
            && J('#localSubitemRadioGroup input')[0].checked) {

            displayLocalSubitemMap();

        } else if (typeof(J('#subitemRadioGroup input')[0]) != 'undefined'
            && J('#subitemRadioGroup input')[0].checked) {

            displaySubitemMap();

        } else if (typeof(J('#itemRadioGroup input')) != 'undefined'
            && J('#itemRadioGroup input')[0].checked) {

            displayItemMap();

        }

        return false;

    });

    //Effacer la selection
    J(".effacer-selection ").click(function () {

        J('.liste-region-selectionnes option:selected').each(function () {

            //Recuperation des positions du sous item concerne et de son item
            var pos_subitem = J(this).attr('sid');
            var pos_item = J(this).attr('iid');

            //Mise a jour du composant concerne
            js_map.items.tab[pos_item].subitems.tab[pos_subitem].unselect();
            js_map.items.tab[pos_item].update();

            //Recuperation du parent HTML
            var parent_node = J(this).parent();

            //Supression de l'option HTML
            J(this).remove();

            //Suppression du groupe parent si il est maintenant vide
            if (!parent_node.children().length) {

                parent_node.remove();

            }

            //On force le rafraichissement de la carte
            js_map.update();

            //Retracage de la carte
            if (typeof(J('#localSubitemRadioGroup input')[0]) != 'undefined'
                && J('#localSubitemRadioGroup input')[0].checked) {

                displayLocalSubitemMap();

            } else if (typeof(J('#subitemRadioGroup input')[0]) != 'undefined'
                && J('#subitemRadioGroup input')[0].checked) {

                displaySubitemMap();

            } else if (typeof(J('#itemRadioGroup input')) != 'undefined'
                && J('#itemRadioGroup input')[0].checked) {

                displayItemMap();

            }


        });
        return false;

    });


});


