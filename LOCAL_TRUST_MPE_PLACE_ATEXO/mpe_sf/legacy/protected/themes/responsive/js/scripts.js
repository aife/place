/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function popupResize (containerId) {
	if (containerId == null) {
		containerId = "container";
	}
	var pageHeight = document.getElementById(containerId).offsetHeight;
	var pageWidth = screen.availWidth -50;
	var name = navigator.appName ;
	var version = navigator.appVersion;
	var autoLeft = (screen.availWidth-(pageWidth))/2;
	var autoTop = (screen.availHeight-pageHeight)/2;
//nous avons enlevé 15 du screen.availHeight pour prendre en concideration la barre des taches
	var screenAvailHeight = screen.availHeight-15;
	/*if popup content higher than screen height*/
	if (pageHeight > screenAvailHeight) {
		/*if IE*/
		if (name == 'Microsoft Internet Explorer') {
			id = version.indexOf('MSIE');
			version = version.substring(id+5,id+9);
			/*if IE 7*/
			if ( version == '7.0;') {
				//alert(version);
				window.resizeTo(pageWidth+50,screenAvailHeight-100);
				window.moveTo(autoLeft-20,50);
			}
			/*if other IE version*/
			else {
				window.resizeTo(pageWidth+50,screenAvailHeight-100);
				window.moveTo(autoLeft,50);
			}
		}
		/*if other navitor*/
		else {
			window.resizeTo(pageWidth+50,screenAvailHeight-100);
			window.moveTo(autoLeft,50);
		}
	}
	/*else if content smaller than screen height */
	else {
		/*if IE*/
		if (name == 'Microsoft Internet Explorer') {
			id = version.indexOf('MSIE');
			version = version.substring(id+5,id+9);
			/*if IE 7*/
			if ( version == '7.0;') {
				//alert(version);
				window.resizeTo(pageWidth+50,pageHeight+100);
				window.moveTo(autoLeft-20,autoTop);
			}
			/*if other IE version*/
			else {
				window.resizeTo(pageWidth+50,pageHeight+80);
				window.moveTo(autoLeft,autoTop);
			}
		}
		/*if other navitor*/
		else {
			window.resizeTo(pageWidth+50,pageHeight+100);
			window.moveTo(autoLeft,autoTop);
		}
	}
}

function openModal(myModal,myClass,myEl) {
	J("."+myModal).dialog({
		modal: true,
		resizable: false,
		closeText:'Fermer',
		width:'auto',
		open: function(){
			/*jQuery('.ui-widget-overlay').bind('click',function(){
			 J("."+myModal).dialog('close');
			 });*/
			//Dialog freezes window scrollbar - Chrome fix
			J(".ui-dialog-titlebar").hide();
			window.setTimeout( function () {
				jQuery(document).unbind( 'mousedown.dialog-overlay' ).unbind( 'mouseup.dialog-overlay' );
			}, 100 );
		}
	});
}

function deplierBlocsDocumentsEtablissement()
{
	J(document).ready(function() {
		J(".docs-etablissement").removeClass("treegrid-collapsed").addClass("treegrid-expanded");
		J(".etablissement-type-attestation").show();
	});
}
function plierBlocsDocumentsEtablissement()
{
	J(document).ready(function() {
		J(".docs-etablissement").removeClass("treegrid-expanded").addClass("treegrid-collapsed");
		J(".etablissement-type-attestation").hide();
	});
}
function deplierBlocDocumentsEtablissementId(idEtablissement)
{
	J(document).ready(function() {
		J("#docs-etablissement-"+idEtablissement).removeClass("treegrid-collapsed").addClass("treegrid-expanded");
		J(".etablissement-type-attestation-id-"+idEtablissement).show();
	});
}

function initialiserAffichageTableauDocumentsEntreprise()
{
	J('.table-attestations').treegrid({ initialState: 'collapsed' });
	var cas = J('#initialisationAttestationTreeGrid').val();
	var idEtablissement = J('#idEtablissementAyantDeposeReponse').val();
	if(cas == 'cas1') {
		deplierBlocsDocumentsEtablissement();
	} else if(cas == 'cas2') {
		plierBlocsDocumentsEtablissement();
		deplierBlocDocumentsEtablissementId(idEtablissement);
	}
}

