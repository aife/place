/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// JavaScript Document

//Treegrid
J(".treeview .toggle").on('click',function(e){
	e.preventDefault();
	if(J(this).attr('data-click-state') == 1) {
		J(this).attr('data-click-state', 0)
		J(this).parents(".treeview").treegrid('getRootNodes').treegrid('collapse');
	} else {
		J(this).attr('data-click-state', 1)
		J(this).parents(".treeview").treegrid('getRootNodes').treegrid('expand');
	}
});

J( document ).ready(function() {
	//Init contact treegrid state
	var cas = J('#initialisationAttestationTreeGrid').val();
	var idEtablissement = J('#idEtablissementAyantDeposeReponse').val();
	if(cas == 'cas1') {
		deplierBlocsDocumentsEtablissement();
	} else if(cas == 'cas2') {
		plierBlocsDocumentsEtablissement();
		deplierBlocDocumentsEtablissementId(idEtablissement);
	}
});

J( document ).ready(function() {
	//Init contact treegrid state
	J('.table-contacts').treegrid({ initialState: 'collapsed' });
	J('.table-attestations').treegrid({ initialState: 'collapsed' });
	//g1.refresh(g1.originalValue, 100);
});
