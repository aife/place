/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

J(document).ready(function(){
	
	J('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
	
	J(".btn-navbar").click(function () {
		J('.row-fluid').toggleClass("fixed");
    });
		
	J(".liste-resultats > li").click(function () {
		J(".liste-resultats > li").removeClass("active");
		J(this).addClass("active");
    });
	J(".liste-resultats .lien-detail").click(function () {
		//return false;
    });
	J(".btn-retour-liste").click(function () {
		J(".bloc-resultats").css('display','block');
		J(".btn-retour").attr('href','javascript:history.go(-1);');
		J(".detail-selected-item").removeClass('detail-only');
    });
	
	
	J(".lien-detail").click(function () {
		var linkId = J(this).attr('href');
		J(".detail-bloc").removeClass("active");
		J(linkId).addClass("active");
		
		var windowWidth= J(window).width();
		if(windowWidth< 600){
			J(".bloc-resultats").hide();
			J(".detail-selected-item").show().addClass('detail-only');
			J(".detail-selected-item").css('margin-left','0');
			J(".detail-selected-item").css('border-left','0');
			J(".detail-selected-item").css('width','100%');
			J(".detail-selected-item").css('position','absolute');
			J("#resultats-recherche").css('background','#fff');
			if(J(linkId).attr('class') == "detail-bloc active"){
				document.getElementById('ctl0_menuGauche_btnRetourFromDetail').style.display = '' ;
				document.getElementById('ctl0_menuGauche_btnRetour').style.display = 'none' ;
			}
			J(".btn-retour").attr('href','');
			J(".btn-retour").addClass('btn-retour-liste');
		}else{
			document.getElementById('ctl0_menuGauche_btnRetourFromDetail').style.display = 'none' ;
			document.getElementById('ctl0_menuGauche_btnRetour').style.display = '' ;
		}
		
    });
	
	J(".btn-retour-detail").click(function () {
		var linkId = J(this).attr('href');
		J(".bloc-resultats").show();
		J(linkId).addClass("active");
		var windowWidth= J(window).width();
		document.getElementById('ctl0_menuGauche_btnRetourFromDetail').style.display = 'none' ;
		document.getElementById('ctl0_menuGauche_btnRetour').style.display = '' ;
		if(windowWidth< 600){
			J(".detail-selected-item").hide();
			J(".btn-retour-detail").attr('href','');
			J(".btn-retour-detail").addClass('btn-retour-liste');
		}else{
		}
		
    });
	
	J(".pager .btn-left").click(function () {
		if (J(this).parents(".detail-bloc").prev(".detail-bloc").length != 0) {
			J(".detail-bloc").removeClass("active");
			J(this).parents(".detail-bloc").prev(".detail-bloc").addClass("active");
			
			var itemId = J(this).parents(".detail-bloc").prev(".detail-bloc").attr('id');
			J(".liste-resultats li").removeClass("active");	
			var itemHref = '#'+itemId;
			J(".liste-resultats a[href="+itemHref+"]").parent('li').addClass("active");				
			J(".detail-selected-item").scrollTop(0);
		}
		else {}
    });
	
	J(".pager .btn-right").click(function () {
		if (J(this).parents(".detail-bloc").next(".detail-bloc").length != 0) {
			J(".detail-bloc").removeClass("active");
			J(this).parents(".detail-bloc").next(".detail-bloc").addClass("active");
			
			var itemId = J(this).parents(".detail-bloc").next(".detail-bloc").attr('id');
			J(".liste-resultats li").removeClass("active");	
			var itemHref = '#'+itemId;
			J(".liste-resultats a[href="+itemHref+"]").parent('li').addClass("active");				
			J(".detail-selected-item").scrollTop(0);
		}
		else {}
    });
	
    J("a:not(.btn-navbar,.lien-externe,.pager a)").click(function (event) {
        if (  (navigator.standalone)
              &&
              ((navigator.userAgent.indexOf("iPhone") > -1) || (navigator.userAgent.indexOf("iPad") > -1))
            ) {
            // On bloque les liens quand on est en mode web-app sur iPhone et iPad
            event.preventDefault();
            // On recâble le lien à la main grâce à window.location
            window.location = J(this).attr("href");
        }
		//event.preventDefault();
		// On recâble le lien à la main grâce à window.location
		//window.location = J(this).attr("href");
   });
   
   
	//Always display barre top onscroll
	J(function() {
		J(window).scroll(sticky_relocate);
		sticky_relocate();
	});
	
});



//Fonction display barre top
function sticky_relocate() {
	var window_top = J(window).scrollTop();
	var div_top = J('.bloc-resultats').offset().top;
	if (window_top > div_top)
	J('.barre-top').addClass('stick')
	else
	J('.barre-top').removeClass('stick');
}
		  

