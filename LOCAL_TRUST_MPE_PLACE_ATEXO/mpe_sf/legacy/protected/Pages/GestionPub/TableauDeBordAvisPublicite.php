<?php

namespace Application\Pages\GestionPub;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

class TableauDeBordAvisPublicite extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('gestionPub');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        if ($_GET['acces']) {
            Atexo_CurrentUser::writeToSession('acces_gestion_publicite', Atexo_Util::atexoHtmlEntities($_GET['acces']));
        }
    }

    public function onLoad($param)
    {
        self::remplirTableauAvisnationaux();
        self::remplirTableauAvisEuropeens();
    }

    public function remplirTableauAvisNationaux()
    {
        $dataSourceAvisNationaux = (new Atexo_Publicite_AvisPub())->getAvisJoinTypesAvisByRegion(Atexo_Config::getParameter('NATIONAL'));
        $this->tableauAvisNationaux->DataSource = $dataSourceAvisNationaux;
        $this->tableauAvisNationaux->DataBind();
    }

    public function remplirTableauAvisEuropeens()
    {
        $dataSourceAvisEuropeens = (new Atexo_Publicite_AvisPub())->getAvisJoinTypesAvisByRegion(Atexo_Config::getParameter('EUROPEEN'));
        $this->tableauAvisEuropeens->DataSource = $dataSourceAvisEuropeens;
        $this->tableauAvisEuropeens->DataBind();
    }
}
