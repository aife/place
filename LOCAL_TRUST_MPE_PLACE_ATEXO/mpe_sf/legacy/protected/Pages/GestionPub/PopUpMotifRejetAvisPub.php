<?php

namespace Application\Pages\GestionPub;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;

/*
 * Created on 8 août 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class PopUpMotifRejetAvisPub extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('gestionPub');
    }

    public function onLoad($param)
    {
        $idAvis = Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
        $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        $avis = (new Atexo_Publicite_AvisPub())->retreiveAvis($idAvis, $organisme);

        if ($avis instanceof CommonAvisPub) {
            if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_AVIS_DEJA_ENVOYE'));
                $this->panelRejet->setVisible(false);
            } else {
                $this->panelMessageErreur->setVisible(false);
            }
        }
    }

    public function rejeterAvisPub($sender, $param)
    {
        $idAvis = Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
        $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        (new Atexo_Publicite_AvisPub())->rejeterAvis($idAvis, $organisme, $this->motifRejet->Text);
        echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshActionPub').click();window.close();</script>";
    }
}
