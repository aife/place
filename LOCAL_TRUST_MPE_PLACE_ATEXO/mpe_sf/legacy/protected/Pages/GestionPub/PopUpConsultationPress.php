<?php

namespace Application\Pages\GestionPub;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * PopUpConsultationPress.
 *
 * @author Hajar HANNAD <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @since MPE-4.0
 */
class PopUpConsultationPress extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $organisme = ($_GET['organisme']) ? Atexo_Util::atexoHtmlEntities($_GET['organisme']) : Atexo_CurrentUser::getCurrentOrganism();
        $this->TemplateEnvoiCourrierElectroniquePress->setOrg($organisme);
        $this->TemplateEnvoiCourrierElectroniquePress->annuler->setVisible(false);
    }
}
