<?php

namespace Application\Pages\GestionPub;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonHistoriqueAvisPub;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTypeAvisPub;
use Application\Propel\Mpe\CommonTypeAvisPubPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CriteriaVo;
use Prado\Prado;

/*
 * Created on 8 août 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class FormulaireRechercheAvisPub extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('gestionPub');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            self::displayDestinataire();
            self::displayStatut();
            self::displayTypeAvis();
            self::remplirListeOrganismeService();
            self::remplirListeProcedureType();
        }

        self::afficherTableauRecherche();

        //$dataSource = Atexo_Publicite_AvisPub::getInfosTableauSuiviAvis();
        //$this->tableauDesAvisPub->DataSource = $dataSource;
        //$this->tableauDesAvisPub->DataBind();
        $criteriaVo = new Atexo_Publicite_CriteriaVo();
        if (isset($_GET['attentePublication'])) {
            $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_ENVOYE'));
        }
        /*if(isset($_GET['publie'])) {
            $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_VALIDE_ECO'));
        }*/
        if (isset($_GET['rejete'])) {
            if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
                $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP'));
            } else {
                $criteriaVo->setStatut([0 => Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO'), 1 => Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')]);
            }
        }
        if (isset($_GET['envoye'])) {
            $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_ENVOYE'));
        }
        if (isset($_GET['attenteValidationSip'])) {
            if (!(new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
                $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP'));
            }
        }
        if (isset($_GET['envoiPlanifie'])) {
            $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'));
        }
        if (isset($_GET['region']) && 'national' == $_GET['region']) {
            $criteriaVo->setRegionAvis(Atexo_Config::getParameter('NATIONAL'));
        } elseif (isset($_GET['region']) && 'europeen' == $_GET['region']) {
            $criteriaVo->setRegionAvis(Atexo_Config::getParameter('EUROPEEN'));
        }
        if (isset($_GET['typeAvis'])) {
            $criteriaVo->setIdTypeAvis(Atexo_Util::atexoHtmlEntities($_GET['typeAvis']));
        }
        if (isset($_GET['aTraiter'])) {
            if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
                $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP'));
            } else {
                $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO'));
            }
        }
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
            $criteriaVo->setValidationSip(true);
        } else {
            $criteriaVo->setValidationSip(false);
        }
        if (isset($_GET['attentePublication']) || isset($_GET['publie']) || isset($_GET['rejete'])
           || isset($_GET['region']) || isset($_GET['typeAvis']) || isset($_GET['attenteValidationSip'])
               || isset($_GET['envoiPlanifie'])) {
            $this->remplirTableauListeAvisPub($criteriaVo);
        }
        if (!$this->isPostBack) {
            //Initialisation des dates sur le formulaire de recherche
            $this->dateReceptionStart->text = Atexo_Util::iso2frnDate(Atexo_Util::getDateMonthsAgo(3));
            $this->dateReceptionEnd->text = date('d/m/Y');
            $this->dateRemisePliStart->text = date('d/m/Y');
            $this->dateRemisePliEnd->text = Atexo_Util::iso2frnDate(substr(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), 0, 0, 3), 0, 10));
        }
    }

    public function displayDestinataire()
    {
        $dataSource = [];
        $dataSource['0'] = '--- '.Prado::localize('TEXT_INDIFFERENT').' ---';
        $dataSource[Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')] = Prado::localize('TEXT_PORTAIL');
        $dataSource[Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')] = Prado::localize('TEXT_JOUE');
        $dataSource[Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')] = Prado::localize('TEXT_PRESSE');
        $this->destinataire->DataSource = $dataSource;
        $this->destinataire->DataBind();
    }

    public function displayStatut()
    {
        $dataSource = [];
        $dataSource['0'] = '--- '.Prado::localize('TEXT_TOUT_STATUT').' ---';
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
            $dataSource[Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')] = Prado::localize('TEXT_A_TRAITER');
        } else {
            $dataSource[Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')] = Prado::localize('TEXT_A_TRAITER');
        }
        if (!(new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
            $dataSource[Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')] = Prado::localize('TEXT_ATTENTE_VALIDATION_SIP');
        }
        $dataSource[Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')] = Prado::localize('TEXT_ENVOI_PLANIFIE');
        $dataSource[Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')] = Prado::localize('TEXT_ENVOYE');

        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
            $dataSource[Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')] = Prado::localize('TEXT_REJETES');
        } else {
            $dataSource[Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')] = Prado::localize('TEXT_REJETES');
        }

        $this->statut->DataSource = $dataSource;
        $this->statut->DataBind();
    }

    public function displayTypeAvis()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $typesAvis = [];
        $typesAvis['0'] = '--- '.Prado::localize('TEXT_TOUS_LES_AVIS').' ---';
        $listeTypesAvis = CommonTypeAvisPubPeer::doSelect($criteria, $connexion);
        if (is_array($listeTypesAvis) && count($listeTypesAvis)) {
            foreach ($listeTypesAvis as $typeAvis) {
                if ($typeAvis->getRegion() == Atexo_Config::getParameter('NATIONAL')) {
                    $typeAvis->setRegion(Prado::localize('TEXT_AVIS_NATIONAUX'));
                }
                if ($typeAvis->getRegion() == Atexo_Config::getParameter('EUROPEEN')) {
                    $typeAvis->setRegion(Prado::localize('TEXT_AVIS_EUROPEENS'));
                }
                $typesAvis[] = $typeAvis;
            }
        }
        $this->avisType->DataSource = $typesAvis;
        $this->avisType->DataBind();
    }

    public function getFlagValidation($etatValidation)
    {
        if ($etatValidation) {
            return $this->themesPath().'/images/picto-check-ok-small.gif';
        }

        return $this->themesPath().'/images/picto-check-not-ok.gif';
    }

    public function remplirListeOrganismeService()
    {
        $arrayServices = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $organismes = CommonOrganismePeer::doSelect($criteria, $connexion);
        if (is_array($organismes) && count($organismes)) {
            $arrayServices = [];
            $arrayServices[0] = '--- '.Prado::localize('TEXT_INDIFFERENT').' ---';
            foreach ($organismes as $organisme) {
                $arrayServices[$organisme->getAcronyme().'-0'] = $organisme->getSigle().' - '.$organisme->getDenominationOrg();
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $criteria = new Criteria();
                $criteria->add(CommonServicePeer::ORGANISME, $organisme->getAcronyme());
                $listeServices = CommonServicePeer::doSelect($criteria, $connexion);
                if (is_array($listeServices) && count($listeServices)) {
                    foreach ($listeServices as $service) {
                        $pathOrgService = (new Atexo_EntityPurchase())->getEntityPathById($service->getId(), $organisme->getAcronyme(), true, true);
                        $arrayServices[$organisme->getAcronyme().'-'.$service->getId()] = $pathOrgService;
                    }
                }
            }
        }
        asort($arrayServices);
        $this->organismeService->dataSource = $arrayServices;
        $this->organismeService->dataBind();
    }

    public function remplirListeTypeAvis()
    {
        $arrayTypeAvis = [];
        $arrayTypeAvis[] = '--- Tous les avis ---';
    }

    public function lancerRecherche()
    {
        $criteriaVo = new Atexo_Publicite_CriteriaVo();

        if (trim($this->referenceAvis->Text)) {
            $criteriaVo->setReferenceAvis(trim($this->referenceAvis->Text));
        }
        if (trim($this->agentValidateurPublicite->Text)) {
            $criteriaVo->setCritereNomPrenomAgentValidateur($this->agentValidateurPublicite->Text);
            $arrayIdsAgent = (new Atexo_Agent())->getIdsAgentByName(trim($this->agentValidateurPublicite->Text));
            if (is_array($arrayIdsAgent) && count($arrayIdsAgent)) {
                $criteriaVo->setIdAgentValidateurAvisPub('('.implode(',', $arrayIdsAgent).')');
            }
        }
        if ('' != $this->organismeService->getSelectedValue() && '0' != $this->organismeService->getSelectedValue()) {
            $orgService = explode('-', $this->organismeService->getSelectedValue());
            $criteriaVo->setIdService($orgService[1]);
            $criteriaVo->setOrganisme($orgService[0]);
        }
        if ('' != $this->avisType->getSelectedValue() && '0' != $this->avisType->getSelectedValue()) {
            $criteriaVo->setIdTypeAvis($this->avisType->getSelectedValue());
        }
        if ('' != $this->destinataire->getSelectedValue()) {
            $criteriaVo->setIdSupport($this->destinataire->getSelectedValue());
        }
        if ('' != $this->procedureType->getSelectedValue()) {
            $criteriaVo->setIdTypeProcedure($this->procedureType->getSelectedValue());
        }
        if ('' != $this->statut->getSelectedValue()) {
            $criteriaVo->setStatut($this->statut->getSelectedValue());
        }

        if (trim($this->dateReceptionStart->Text)) {
            $criteriaVo->setDateReceptionStart(trim($this->dateReceptionStart->Text));
        }
        if (trim($this->dateReceptionEnd->Text)) {
            $criteriaVo->setDateReceptionEnd(trim($this->dateReceptionEnd->Text));
        }
        if (trim($this->dateRemisePliStart->Text)) {
            $criteriaVo->setDateRemisePlisStart(trim(substr($this->dateRemisePliStart->Text, 0, 10)));
        }
        if (trim($this->dateRemisePliEnd->Text)) {
            $criteriaVo->setDateRemisePlisEnd(trim(substr($this->dateRemisePliEnd->Text, 0, 10)));
        }
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {
            $criteriaVo->setValidationSip(true);
        } else {
            $criteriaVo->setValidationSip(false);
        }
        $this->remplirTableauListeAvisPub($criteriaVo);
    }

    public function remplirTableauListeAvisPub($criteriaVo)
    {
        self::afficherTableauResultat();
        //Critères de tri du tableau
        $criteriaVo->setCommandeName($this->getViewState('commandeName'));
        $criteriaVo->setSensTri($this->getViewState('sensTri'));
        //Critères de pagination
        $offset = $this->tableauDesAvisPub->CurrentPageIndex * $this->tableauDesAvisPub->PageSize;
        $limit = $this->tableauDesAvisPub->PageSize;
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);

        $this->setViewState('CriteriaVo', $criteriaVo);
        $nombreElementTotal = (new Atexo_Publicite_AvisPub())->getInfosTableauSuiviAvis($criteriaVo, true);
        $listeAvisPub = [];
        if ($nombreElementTotal >= 1) {
            $listeAvisPub = (new Atexo_Publicite_AvisPub())->getInfosTableauSuiviAvis($criteriaVo, false);

            $this->panelMoreThanOneElementFound->setStyle('display:block');
            $this->panelNoElementFound->setStyle('display:none');
            $this->nombreElement->Text = $nombreElementTotal;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->setViewState('listeAvis', $listeAvisPub);
            $this->nombrePageTop->Text = ceil($nombreElementTotal / $this->tableauDesAvisPub->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElementTotal / $this->tableauDesAvisPub->PageSize);
            $this->tableauDesAvisPub->setVirtualItemCount($nombreElementTotal);
            $this->tableauDesAvisPub->dataSource = $listeAvisPub;
            $this->tableauDesAvisPub->dataBind();
        } else {
            $this->panelMoreThanOneElementFound->setStyle('display:none');
            $this->panelNoElementFound->setStyle('display:block');
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->tableauDesAvisPub->dataSource = [];
            $this->tableauDesAvisPub->dataBind();
        }
        $this->setViewState('criteres', $criteriaVo);
    }

    public function afficherTableauResultat()
    {
        $this->panelSearch->visible = false;
        $this->panelResult->visible = true;
    }

    public function afficherTableauRecherche()
    {
        $this->panelSearch->visible = true;
        $this->panelResult->visible = false;
    }

    public function populateData()
    {
        $offset = $this->tableauDesAvisPub->CurrentPageIndex * $this->tableauDesAvisPub->PageSize;
        $limit = $this->tableauDesAvisPub->PageSize;
        if ($offset + $limit > $this->tableauDesAvisPub->getVirtualItemCount()) {
            $limit = $this->tableauDesAvisPub->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $this->remplirTableauListeAvisPub($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauDesAvisPub->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->populateData();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauDesAvisPub->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->tableauDesAvisPub->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauDesAvisPub->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTop':
                $pageSize = $this->listePageSizeTop->getSelectedValue();
                $this->listePageSizeBottom->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottom':
                $pageSize = $this->listePageSizeBottom->getSelectedValue();
                $this->listePageSizeTop->setSelectedValue($pageSize);
                break;
        }
        // echo $pageSize;exit;
        $this->tableauDesAvisPub->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDesAvisPub->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDesAvisPub->PageSize);
        $this->tableauDesAvisPub->setCurrentPageIndex(0);
        $this->populateData();
    }

    public function raffraichirBlocActions($sender, $param)
    {
        $this->populateData();
        $this->panelMoreThanOneElementFound->render($param->getNewWriter());
    }

    public function validerAvisPub($sender, $param)
    {
        $parametres = explode('-', $param->CommandParameter);
        (new Atexo_Publicite_AvisPub())->validerAvis($parametres[0], $parametres[1]);
    }

    public function rejeterAvisPub($sender, $param)
    {
        $parametres = explode('-', $param->CommandParameter);
        $idAvis = $parametres[0];
        $organisme = $parametres[1];
        (new Atexo_Publicite_AvisPub())->rejeterAvis($idAvis, $organisme);
    }

    public function remplirListeProcedureType()
    {
        $typesProcedures = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, false);
        $typesProcedures[0] = '--- '.Prado::localize('TEXT_TOUTES_PROCEDURES').' ---';
        ksort($typesProcedures);

        $this->procedureType->DataSource = $typesProcedures;
        $this->procedureType->DataBind();
    }

    public function displayCriteria($sender, $param)
    {
        $this->Page->panelSearch->setVisible(true);
        $this->Page->panelResult->setVisible(false);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        if ('' != $criteriaVo->getReferenceAvis()) {
            $this->referenceAvis->Text = $criteriaVo->getReferenceAvis();
        }

        if ('' != $criteriaVo->getIdService()) {
            $this->organismeService->setSelectedValue($criteriaVo->getIdService());
        }
        if ('' != $criteriaVo->getIdTypeAvis()) {
            $this->avisType->setSelectedValue($criteriaVo->getIdTypeAvis());
        }
        if ('' != $criteriaVo->getIdSupport()) {
            $this->destinataire->setSelectedValue($criteriaVo->getIdSupport());
        }
        if ('' != $criteriaVo->getStatut()) {
            if (is_array($criteriaVo->getStatut())) {//Cas des avis rejeté: statut rejet ECO = 2, statut rejet sip = 6
                $arrayStatut = $criteriaVo->getStatut();
                $this->statut->setSelectedValue($arrayStatut[0]);
            } else {
                $this->statut->setSelectedValue($criteriaVo->getStatut());
            }
        }
        /* if($criteriaVo->getIdTypeProcedure()!='') {
             $this->procedureType->setSelectedValue($criteriaVo->getIdTypeProcedure());
         }
        */
        if ('' != $criteriaVo->getDateReceptionStart()) {
            $this->dateReceptionStart->Text = $criteriaVo->getDateReceptionStart();
        }
        if ('' != $criteriaVo->getDateReceptionEnd()) {
            $this->dateReceptionEnd->Text = $criteriaVo->getDateReceptionEnd();
        }
        if ('' != $criteriaVo->getDateRemisePlisStart()) {
            $this->dateRemisePliStart->Text = $criteriaVo->getDateRemisePlisStart();
        }
        if ('' != $criteriaVo->getDateRemisePlisEnd()) {
            $this->dateRemisePliEnd->Text = $criteriaVo->getDateRemisePlisEnd();
        }
    }

    public function critereTri($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTri');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);
    }

    public function Trier($sender, $param)
    {
        $this->populateData();
        $this->panelTableauAvisPub->render($param->NewWriter);
    }

    public function actionPublicite($sender, $param)
    {
        self::actionDestinataire($sender, $param);
    }

    public function actionDestinataire($sender, $param)
    {
        $parametters = explode('-', $param->CommandParameter);
        $idDestinataire = $parametters[0];
        $idTypeDest = $parametters[1];
        $itemIndex = $parametters[2];
        $idTypeAvis = $parametters[3];
        $idAvis = $parametters[4];
        $organisme = $parametters[5];
        $consultationId = $parametters[6];
        $typeDoc = Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS');
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        $typeProc = '';
        if ($consultation instanceof CommonConsultation) {
            $typeProc = $consultation->getIdTypeProcedure();
            if ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
                $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, $organisme);
                if ($destinataire instanceof CommonDestinatairePub) {
                    if ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_ESENDER')) {
                        //Call Esender
                        $resourceForm = (new CommonTypeAvisPub())->getResourceForm($idTypeAvis);
                        $this->scriptPub->Text = "<script>popUpSetSize('?page=gestionPub.PopupEsender&idTypeSupport=".$idTypeDest.'&idDest='.$idDestinataire.'&resourceFormulaire='.$resourceForm.'&itemIndex='.$itemIndex.'&id='.$consultationId.'&isConsultation='.$idAvis.'&org='.$organisme."','900px','1000px','yes');</script>";
                    }
                }
            } elseif ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {//consulter email de la press
                if (self::isDetailSupport($idAvis, $consultationId, $organisme)) {
                    $this->scriptPub->Text = "<script>popUpSetSize('?page=gestionPub.PopUpConsultationPress&idDest=".$idDestinataire.'&id='.$consultationId."&consult&organisme=$organisme','840px','700px','yes');</script>";
                } else {
                    $this->scriptPub->Text = "<script>popUpSetSize('?page=gestionPub.PopUpConsultationPress&idDest=".$idDestinataire.'&id='.$consultationId."&organisme=$organisme','840px','700px','yes');</script>";
                }
            } elseif ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {//telecharger avis générer
                $document = (new Atexo_Publicite_Avis())->retreiveListFormulaireLibreByTypeDocGenere($consultationId, $organisme, $typeDoc);
                if ($document[0] instanceof CommonAVIS) {
                    if (self::isDetailSupport($idAvis, $consultationId, $organisme)
                       && $document[0]->getStatut() != Atexo_Config::getParameter('DESTINATAIRE_SUSPENDU')) {
                        //Recuperation de la piece jointe de l'annonce envoyée au portail
                        $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvis, $organisme, $consultationId);
                        $idAvisPortail = '';
                        if ($avisPub instanceof CommonAvisPub) {
                            $idAvisPortail = $avisPub->getIdAvisPortail();
                        }
                        $this->scriptPub->Text = "<script>popUpSetSize('?page=gestionPub.DownloadDocument&id=".$consultationId.'&org='.$organisme.'&typeDoc='.$typeDoc.'&idAvis='.$idAvisPortail."');</script>";
                    }
                }
            }
        }
    }

    public function isDetailSupport($idAvis, $consultationId, $organisme)
    {
        return (new Atexo_Publicite_AvisPub())->isDetailSupport($idAvis, $organisme, $consultationId);
    }

    /**
     * Gère l'affichage des picto "valider" et "rejeter" actifs.
     *
     * @param $statut : statut de l'avis de publicité
     *
     * @return : true si visible, false sinon
     */
    public function avisActionPictoValiderActif($statut)
    {
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {//Coté SIP
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')) {
                return true;
            }
        } else {//Coté ECO
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gère l'affichage des picto "valider" et "rejeter" inactifs.
     *
     * @param $statut : statut de l'avis de publicité
     *
     * @return : true si visible, false sinon
     */
    public function avisActionPictoValiderInactif($statut)
    {
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {//Coté SIP
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')
               || $statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')
                   || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                return true;
            }
        } else {//Coté ECO
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')
               || $statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')
                   || $statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')
                       || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                           || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gère l'affichage du picto "rejeter" actif.
     *
     * @param $statut : statut de l'avis de publicité
     *
     * @return : true si visible, false sinon
     */
    public function avisActionPictoRejeterActif($statut)
    {
        if ($statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')) {
            return true;
        }
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {//Coté SIP
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')) {
                return true;
            }
        } else {//Coté ECO
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO') || $statut == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gère l'affichage du picto "rejeter" inactif.
     *
     * @param $statut : statut de l'avis de publicité
     *
     * @return : true si visible, false sinon
     */
    public function avisActionPictoRejeterInactif($statut)
    {
        if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) {//Coté SIP
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')
                   || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                return true;
            }
        } else {//Coté ECO
            if ($statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')
                       || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                           || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de générer l'excel tableau de bord ECO et SIP.
     */
    public function genererTableauBordAvis($sender, $param)
    {
        $listeAvis = $this->getViewState('listeAvis');
        $criteres = $this->getViewState('criteres');
        (new Atexo_GenerationExcel())->genererTableauBordAvis($listeAvis, $criteres);
    }

    /**
     * Permet de préciser pour les avis europeens, si reussite envoi OPOCE.
     *
     * @param $idAvis: identifiant de l'avis de publicité
     * @param $organisme: l'organisme
     *
     * @return true si reussite envoi OPOCE, false sinon
     */
    public function avisOpoceEchecPublicationVisible($idAvis, $organisme)
    {
        $historique = (new Atexo_Publicite_AvisPub())->getLastHistoriqueByStatut($idAvis, Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'), $organisme);
        if ($historique instanceof CommonHistoriqueAvisPub && null != $historique->getMotifRejet() && '' != $historique->getMotifRejet()) {
            return true;
        }

        return false;
    }

    /**
     * Récupère le message d'erreur en cas d'échec de publication de l'avis.
     *
     * @param $idAvis: identifiant de l'avis de publicité
     * @param $organisme: l'organisme
     *
     * @return le message d'erreur
     */
    public function messageEchecPublicationAvisOPOCE($idAvis, $organisme)
    {
        $historique = (new Atexo_Publicite_AvisPub())->getLastHistoriqueByStatut($idAvis, Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'), $organisme);
        if ($historique instanceof CommonHistoriqueAvisPub && null != $historique->getMotifRejet() && '' != $historique->getMotifRejet()) {
            return $historique->getMotifRejet();
        }

        return '';
    }

    /**
     * Permet de telecharger un avis de publicite de la consultation.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <mal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2015
     */
    public function DownloadAvis($sender, $param)
    {
        $parms = explode('#', $param->CommandParameter);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $avis = (new Atexo_Publicite_Avis())->retreiveAvisById($parms[0], $connexion, $parms[1]);
        if ($avis instanceof CommonAVIS) {
            //Nom du fichier joint
            $nomFichier = $avis->getNomFichier();
            (new DownloadFile())->downloadFiles($avis->getAvis(), $nomFichier, $avis->getOrganisme());
        }
    }

    /**
     * Permet de telecharger pdf SUB.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <mal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2015
     */
    public function DownloadPdfSub($sender, $param)
    {
        $destinataire = $param->CommandParameter;
        if ($destinataire instanceof CommonDestinatairePub && $destinataire->getIdDossier() && $destinataire->getIdDispositif()) {
            Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($destinataire->getIdDossier());
        }
    }
}
