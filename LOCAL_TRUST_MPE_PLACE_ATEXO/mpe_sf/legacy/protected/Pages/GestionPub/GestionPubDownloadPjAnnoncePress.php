<?php

namespace Application\Pages\GestionPub;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\AnnoncePress\Press;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

/**
 * Permet de télécharger le Pj de l'annonce presse: accès ECO/SIP.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE-4.0
 */
class GestionPubDownloadPjAnnoncePress extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Util::atexoHtmlEntities($_GET['idPj'])) {
            $idPj = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
            $org = Atexo_CurrentUser::getCurrentOrganism();
            $pj = (new Press())->retreiveAnnoncePressPJByIdPiece($idPj, $org);
            if ($pj) {//Cas du téléchargement de la pièce jointe manuelle
                $this->_idFichier = $idPj;
                $this->_nomFichier = $pj->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
            } else {
                exit;
            }
        } else {//Cas du téléchargement de la piece jointe dynamique
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($consultation instanceof CommonConsultation) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_Util::atexoHtmlEntities($_GET['organisme']), Atexo_Util::atexoHtmlEntities($_GET['id']));
                if ($avisPub instanceof CommonAvisPub) {
                    $pieceJointe = (new Atexo_Publicite_AvisPub())->CommonAnnoncePressPieceJointeByIdAndOrganisme($avisPub->getIdAvisPresse(), $avisPub->getOrganisme(), $connexion);
                    if ($pieceJointe instanceof CommonAnnoncePressPieceJointe) {
                        if (isset($_GET['fileStored'])) {//Cas du fichier stocké en base
                            if ($pieceJointe->getPiece()) {
                                $this->downloadFiles($pieceJointe->getPiece(), $pieceJointe->getNomFichier(), Atexo_Util::atexoHtmlEntities($_GET['organisme']));
                            }
                        } else {//Cas du nouveau fichier
                            $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avisPub->getTypeAvis());
                            if ($modele) {
                                $fileRtf = (new Atexo_Publicite_AvisPub())->getFileContent($avisPub, $consultation, true);
                                static::downloadFileContent($pieceJointe->getNomFichier(), file_get_contents($fileRtf));
                            }
                        }
                    }
                }
            }
        }
    }
}
