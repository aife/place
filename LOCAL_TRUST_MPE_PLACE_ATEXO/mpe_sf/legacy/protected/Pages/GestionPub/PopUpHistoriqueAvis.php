<?php

namespace Application\Pages\GestionPub;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;

/**
 * Classe qui gere l historique des avis.
 *
 * @author Hajar HANNAD <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @since MPE-4.0
 */
class PopUpHistoriqueAvis extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        self::remplirDataSourceHistorique();
    }

    public function remplirDataSourceHistorique($commandeName = null, $sensTri = null)
    {
        if (isset($_GET['idAvis']) && isset($_GET['org'])) {
            $historique = (new Atexo_Publicite_AvisPub())->getHistoriqueAvis(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_Util::atexoHtmlEntities($_GET['org']), $commandeName, $sensTri);
            $this->repeaterFormHistoriqueAvis->dataSource = $historique;
            $this->repeaterFormHistoriqueAvis->dataBind();
        }
    }

    public function sortRepeater($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'ASC');
        $sensTri = $this->getViewState('sensTri');
        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }
        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);
        self::remplirDataSourceHistorique($this->getViewState('commandeName'), $sensTri);
    }

    public function getLibelleStatut($objetHistorique)
    {
        if ('0' == $objetHistorique->getTypeHistorique()) {
            return Prado::localize('AVIS_DE_PUBLICITE').' : '.(new CommonAvisPub())->getLibelleStatut($objetHistorique);
        } elseif ($objetHistorique->getTypeHistorique() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
            return Prado::localize('TEXT_SUPPORT').' '.Prado::localize('TEXT_OPOCE').' : '.(new CommonAvisPub())->getLibelleStatut($objetHistorique);
        } elseif ($objetHistorique->getTypeHistorique() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
            return Prado::localize('TEXT_SUPPORT').' '.Prado::localize('TEXT_PRESSE').' : '.(new CommonAvisPub())->getLibelleStatut($objetHistorique);
        } elseif ($objetHistorique->getTypeHistorique() == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {
            return Prado::localize('TEXT_SUPPORT').' '.Prado::localize('TEXT_SUPPORT_SIMAP2').' : '.(new CommonAvisPub())->getLibelleStatut($objetHistorique);
        }
    }
}
