<?php

namespace Application\Pages\Frame;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

class ConsultationsOrganisme extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('ListeConsOrg')) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteriaVo->setcalledFromPortail(true);
            $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['org'])); //ajouter l'organisme
            $this->setViewState('CriteriaVo', $criteriaVo);
            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
            Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
            $this->dataForSearchResult($criteriaVo);
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
        }
    }

    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->panelResult->setVisible(true);
        $this->resultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /*
     * fonction qui retourne le picto du type de la reponse du consultation
     * */
    public function returnPictoReponseConsultation($consultation)
    {
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return 'reponse-elec-inconnu.gif';
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig-avec-signature.gif';
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-avec-signature.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec.gif';
            }
        } else {
            return '';
        }
    }

    /*
     * fonction qui retourne le titre du type de la reponse du consultation
     * */
    public function returnTitlePictoReponseConsultation($consultation)
    {
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return Prado::localize('TEXT_STATUT_INCONNUE_REPONSE_CONSULTATION');
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return Prado::localize('TEXT_PAS_REPONSE_POUR_CONSULTATION');
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            }
        } else {
            return '';
        }
    }
}
