<?php

namespace Application\Pages\Frame;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * Permet de télécharger le DCE.
 *
 * @author ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 */
class DownloadDceForBaseDce extends DownloadFile
{
    public $_reference;
    public $_organisme;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dce = '';
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronym']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_reference, $this->_organisme);
        Atexo_Db::closeCommon();
        if ($consultation) {
            $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $this->_organisme);
            if (0 != $dce->getDce()) {
                $this->_idFichier = $dce->getDce();
                $this->_nomFichier = $dce->getNomDce();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
            }
        }
    }
}
