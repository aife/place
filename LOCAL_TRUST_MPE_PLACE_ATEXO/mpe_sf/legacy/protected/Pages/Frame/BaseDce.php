<?php

namespace Application\Pages\Frame;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TiersAuthentification;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

class BaseDce extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['ticket']) && isset($_GET['fonctionnalite'])) {
            $valide = (new Atexo_TiersAuthentification())->VerifyToken(Atexo_Util::atexoHtmlEntities($_GET['ticket']));
            if ($valide) {
                if (!$this->IsPostBack) {
                    $this->rechercheAvancee->setVisible(true);
                    $this->panelResult->setVisible(false);
                }
                $criteriaVo = new Atexo_Consultation_CriteriaVo();
                if (isset($_GET['keyWord'])) {
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);

                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $keyWord = Atexo_Util::atexoHtmlEntities($_GET['keyWord']);
                    $criteriaVo->setKeyWordAdvancedSearch($keyWord);
                    $criteriaVo->setcalledFromPortail(true);
                    $criteriaVo->setWithDce(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                }
            } else {
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TICKET_INVALIDE'));
            }
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TICKET_OU_FONCTIONNALITE_INTROUVABLE'));
        }
    }

    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->panelResult->setVisible(true);
        $this->resultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /*
     * fonction du tri des resultat
     * */
    public function Trier($sender, $param)
    {
        $this->resultSearch->Trier($param->CommandName);
    }

    /*
     * fonction qui retourne le picto du type de la reponse du consultation
     * */
    public function returnPictoReponseConsultation($consultation)
    {
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return 'reponse-elec-inconnu.gif';
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig-avec-signature.gif';
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-avec-signature.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec.gif';
            }
        } else {
            return '';
        }
    }

    /*
     * fonction qui retourne le titre du type de la reponse du consultation
     * */
    public function returnTitlePictoReponseConsultation($consultation)
    {
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return Prado::localize('TEXT_STATUT_INCONNUE_REPONSE_CONSULTATION');
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return Prado::localize('TEXT_PAS_REPONSE_POUR_CONSULTATION');
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            }
        } else {
            return '';
        }
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    public function getUrlConsultationExterne($url)
    {
        return $url;
    }
}
