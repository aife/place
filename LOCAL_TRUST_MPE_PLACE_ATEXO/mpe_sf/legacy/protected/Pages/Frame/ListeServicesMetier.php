<?php

namespace Application\Pages\Frame;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonServiceMertier;
use Application\Propel\Mpe\CommonServiceMertierPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Prado\Prado;

class ListeServicesMetier extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $ssoServiceCourant = null;
        $div = '';
        $login = Atexo_Util::atexoHtmlEntities($_GET['login']);
        $idServiceMetier = Atexo_Util::atexoHtmlEntities($_GET['idServiceMetier']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $agent = (new Atexo_Agent())->retrieveAgentByLogin($login);
        if ($agent instanceof CommonAgent) {
            $services = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesOrganisme($agent->getId(), $agent->getOrganisme(), true);
            if (is_array($services)) {
                $div .= '<div id="barre-modules">';
                $div .= '<ul id="list-modules">';
                $serviceSocle = CommonServiceMertierPeer::retrieveByPK(Atexo_Config::getParameter('SERVICE_METIER_SOCLE'), $connexionCom);
                $logoSocle = $serviceSocle->getLogo();
                $logoSocleSmall = str_replace('.png', '-small.png', $logoSocle);
                if ($serviceSocle instanceof CommonServiceMertier) {
                    $urlSocle = Atexo_Config::getParameter('PF_URL_AGENT').'index.php?page=Agent.AccueilAgentAuthentifieSocleinterne';
                    $div .= '<li><a href="'.$urlSocle.'" title="'.Prado::localize($serviceSocle->getDenomination()).'" class="off" ><img src="'.$this->themesPath().'/images/modules/'.$logoSocleSmall.'" alt="'.$this->themesPath().'/images/modules/'.$logoSocle.'" /></a></li>';
                }
                foreach ($services as $oneService) {
                    $url = '';
                    $sso = '';
                    if ($oneService['activate']) {
                        $sso = (new Atexo_Agent())->getSsoValide($agent->getId(), $agent->getOrganisme(), $oneService['idServiceMetier']);
                        $classCss = 'off';
                        if ($idServiceMetier == $oneService['idServiceMetier']) {
                            $classCss = 'on';
                            $ssoServiceCourant = $sso;
                        }
                        if (strstr($oneService['url_acces'], '?')) {
                            $separateur = '&';
                        } else {
                            $separateur = '?';
                        }
                        $logo = $oneService['logo'];
                        $logoSmall = str_replace('.png', '-small.png', $logo);
                        $url = $oneService['url_acces'].$separateur.'sso='.$sso;
                        $profilIdExterne = (new Atexo_Socle_AgentServicesMetiers())->retrieveServiceMetierProfil($oneService['idServiceMetier'], $oneService['idProfilService']);
                        $hiddenInfoProfil = '';
                        if ($profilIdExterne) {
                            $hiddenInfoProfil = '<input type="hidden" id="id_externe_service_metier_'.$profilIdExterne->getIdServiceMetier().'" name="id_externe_service_metier_'.
                                $profilIdExterne->getIdServiceMetier().'" value="'.$profilIdExterne->getIdExterne().'" /> ';
                            $hiddenInfoProfil .= '<input type="hidden" id="libelle_service_metier_'.$profilIdExterne->getIdServiceMetier().'" name="libelle_service_metier_'.
                                $profilIdExterne->getIdServiceMetier().'" value="'.$profilIdExterne->getLibelle().'"/> ';
                        }
                        $li = '<li><a href="'.$url.'" title="'.Prado::localize($oneService['denomination']).'" class="'.$classCss.'" ><img src="'.$this->themesPath().
                            '/images/modules/'.$logoSmall.'" alt="'.$this->themesPath().'/images/modules/'.$logo.'" /></a>'.$hiddenInfoProfil.'</li>';
                        $div .= $li;
                    }
                }
                $div .= '</ul>';
                $div .= '</div>';
            }

            $nomPrenom = $agent->getPrenom().' '.$agent->getNom();
            $entite = Atexo_EntityPurchase::getPathEntityById($agent->getServiceId(), $agent->getOrganisme());
            $divInfoUser = '<a id="triggerUserInfos" class="trigger" href="#" title="Afficher/Cacher les infos Utilisateur"><span class="user">'.$nomPrenom.'</span></a>';
            $divInfoUser .= '<div class="panel-user-infos"><div class="panel-user-infos-arrow"></div>';
            $divInfoUser .= '<div class="panel-user-infos-login">';
            $divInfoUser .= '<p class="user-name">'.$nomPrenom.'</p>';
            $divInfoUser .= '<p class="entite">'.$entite.'</p>';
            $divInfoUser .= '<a href="'.Atexo_Config::getParameter('PF_URL_AGENT').'index.php?page=Agent.AccueilAgentAuthentifieSocleinterne" class="compte">'.Prado::localize('DEFINE_TEXT_MON_COMPTE').'</a><a href="'.Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.Disconnect&sso='.$ssoServiceCourant.'" class="deconnexion">'.Prado::localize('DEFINE_TEXT_SE_DECONNECTER').'</a>';
            $divInfoUser .= '</div></div>';

            $div .= $divInfoUser;
        }
        echo $div;
        exit;
    }
}
