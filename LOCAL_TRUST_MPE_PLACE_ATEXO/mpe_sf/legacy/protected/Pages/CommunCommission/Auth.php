<?php

namespace Application\Pages\CommunCommission;

use Application\Controls\MpeTPage;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Propel\Mpe\CommonSsoAgent;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;

class Auth extends MpeTPage {

	public function onInit($param) {
		$this->Master->setCalledFrom("agent");
	}

	public function onLoad($param) {

		if ($_GET['action'] == 'Deconnexion') {
			$this->getApplication()->getModule("auth")->logout();
			$this->response->redirect(Atexo_Config::getParameter("PF_URL_AGENT")."index.php?page=agent.AgentHome");
		}

		if (Atexo_Module::isEnabled("SocleInterne")) {
			$agentSso=Atexo_InterfaceWs_Authentication::getSsoAgentByIdSso($_GET['sso']);
			if ($agentSso instanceof CommonSsoAgent) {
				$agent=Atexo_Agent::retrieveAgent($agentSso->getIdAgent());
				if ($agent instanceof CommonAgent) {
					$userVo = new Atexo_UserVo();
					$userVo->setLogin($agent->getLogin());
					$userVo->setPassword($agent->getPassword());
					$userVo->setType("agent");
					if ($this->getApplication()->getModule("auth")->login($userVo, null)) {
						$this->response->redirect("index.php?page=Commission.SuiviSeance");
					}
				}
			} else {
				$this->response->redirect(Atexo_Config::getParameter("PF_URL_AGENT")."index.php?page=agent.AccueilAgentAuthentifieSocleinterne");
			}
		} else if (Atexo_Module::isEnabled("SocleExternePpp")) {
			if (isset($_SERVER['HTTP_EXTERNALID']) && isset($_SERVER['HTTP_USERTYPE'])) {
				Atexo_CurrentUser::writeToSession('externalId',$_SERVER['HTTP_EXTERNALID']);
				Atexo_CurrentUser::writeToSession('userType', $_SERVER['HTTP_USERTYPE']);
				if ($_SERVER['HTTP_USERTYPE'] == "AGENT") {
					self::authenticateViaPPPSocleAgent($_SERVER['HTTP_EXTERNALID']);
				}
			} else {
				if ($_SERVER['REQUEST_URI'] == '/') {
					$this->response->redirect(Atexo_Config::getParameter('PF_URL')."?page=entreprise.EntrepriseHome");
				} else {
					$this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
				}
			}
		}
	}
	
	public function authenticateViaPPPSocleAgent($idExterne) {
		if ($idExterne) {
			$agent = Atexo_Agent::retrieveAgentByIdExterne($idExterne);
			$idApplication = Atexo_Config::getParameter('SERVICE_METIER_CAO');

			if ($agent instanceof CommonAgent){
				$agentServiceMetiers = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService($agent->getId(), $idApplication);
				if ($agentServiceMetiers && is_array($agentServiceMetiers) && count($agentServiceMetiers) > 0){
					$userVo = new Atexo_UserVo();
					$userVo->setLogin($agent->getLogin());
					$userVo->setPassword("PPP");
					$userVo->setAuthenticatePpp(true);
					$userVo->setType('agentcao');
					if ($this->getApplication()->getModule("auth")->login($userVo, null)) {
							$this->response->redirect('?page=Commission.SuiviSeance');
						}
					}
				}
			}
		$this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
	}
	
}

