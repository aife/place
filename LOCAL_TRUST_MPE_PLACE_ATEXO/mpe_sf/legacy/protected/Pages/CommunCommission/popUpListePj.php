<?php

namespace Application\Pages\CommunCommission;

use Application\Controls\MpeTPage;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Config;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnoncePressPeer;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Service\Atexo\Atexo_Message;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonNewsletterPieceJointePeer;
use Application\Propel\Mpe\CommonNewsletterPeer;

/**
 * Classe popUpListePj
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package agent
 * @subpackage Page
 */
 
 class popUpListePj extends MpeTPage
 {
     
     
 	public function onInit($param)
    {
    	Atexo_Languages::setLanguageCatalogue("entreprise");
    }
 	public function onLoad($param)
	{	
		if(isset($_GET['Idmsg'])){
			$this->IdUploadPj->_ID = $_GET['Idmsg'];
		}
		
		if ($_GET['org']){
       	    $this->IdUploadPj->_org = $_GET['org'];
       	}
       	else {
	    	$this->IdUploadPj->_org = Atexo_CurrentUser::getCurrentOrganism();
       	}
       	
		if(!$this->IsPostBack)
		{
			$connexionCom = Propel :: getConnection(Atexo_Config :: getParameter('COMMON_DB') . Atexo_Config :: getParameter('CONST_READ_ONLY'));
			$c = new Criteria();
			$PJ = array();
			// Si le message est une press
			if (isset ($_GET['type']) && ($_GET['type']==Atexo_Config :: getParameter('TYPE_PRESS'))){
				$this->IdUploadPj->_type = $_GET['type'];
				$ObjetMessage = CommonAnnoncePressPeer :: retrieveByPK($_GET['Idmsg'],$_GET['org'],$connexionCom);
				if($ObjetMessage){
			    	$c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $_GET['org']);
					$PJs = $ObjetMessage->getCommonAnnoncePressPieceJointes($c,$connexionCom);
				}
			}	
			// Si le message est une newsletter
			else if (isset ($_GET['type']) && ($_GET['type']==Atexo_Config :: getParameter('TYPE_NEWSLETTER'))){
				$this->IdUploadPj->_type = $_GET['type'];
				$ObjetMessage = CommonNewsletterPeer :: retrieveByPK($_GET['Idmsg'],$_GET['org'],$connexionCom);
				if($ObjetMessage){
			    	$c->add(CommonNewsletterPieceJointePeer::ORGANISME, $_GET['org']);
					$PJs = $ObjetMessage->getCommonNewsletterPieceJointes($c,$connexionCom);
				}	
			}
			else {
				$ObjetMessage = Atexo_Message::retrieveMessageById($_GET['Idmsg'],$_GET['org']);
				if($ObjetMessage){
			    	$c->add(CommonEchangePieceJointePeer::ORGANISME, $_GET['org']);
					$PJs = $ObjetMessage->getCommonEchangePieceJointes($c,$connexionCom);
				}
			}
			$this->IdUploadPj->remplirTableauPJ($PJs);
		}
	}
	
	public function OnValidClick()
	{
		$this->IdUploadPj->onAjoutClick(null,null);
		if (isset ($_GET['type']) && ($_GET['type']==Atexo_Config :: getParameter('TYPE_PRESS'))){
			echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_validatePJ').click();window.close();</script>";	
		}
		else if (isset ($_GET['type']) && ($_GET['type']==Atexo_Config :: getParameter('TYPE_NEWSLETTER'))){
			echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validatePJ').click();window.close();</script>";	
		}
		else echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_validatePJ').click();window.close();</script>";
	}
	
 }