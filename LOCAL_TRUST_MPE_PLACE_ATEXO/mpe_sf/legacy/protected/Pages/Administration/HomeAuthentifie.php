<?php

namespace Application\Pages\Administration;

use Application\Controls\BandeauAgent;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class HomeAuthentifie extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('admin');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->nomPrenom->Text = (new BandeauAgent())->getNomPrenomUser();
        $this->alertArchivage->Visible = $this->isFirstOpenDayOfTheMonth();

        if (isset($_GET['deleted'])) {
            $this->panelMessage->setVisible(true);
            $this->panelMessage->setMessage(Prado::localize('ACTION_SUPPRESSION_REALISE'));
        }
    }

    public function isFirstOpenDayOfTheMonth()
    {
        $tsNow = time();
        $day = strftime('%d', $tsNow);
        $month = strftime('%m', $tsNow);
        $year = strftime('%Y', $tsNow);
        $tsFirstOpenDay = Atexo_Util::getNextOpenDay(strtotime($year.'-'.$month.'-01'), 7);
        $dayFisrtOpenDay = strftime('%d', $tsFirstOpenDay);
        if ($day == $dayFisrtOpenDay) {
            return true;
        } else {
            return false;
        }
    }
}
