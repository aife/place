<?php

namespace Application\Pages\Administration;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupEditionLogo extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
    }

    public function saveImage()
    {
        //commencer le scan Antivirus
        if ($this->ajoutFichier->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                return;
            }
        }
        //Fin du code scan Antivirus
        $orga = Atexo_CurrentUser::getOrganismAcronym();
        $message = '';

        if (!is_dir(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE'))) {
            system('mkdir '.Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE'), $res);
            system('chmod 777 '.Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE'), $res);
        }

        if (isset($_GET['small'])) {
            $filePath = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg';
        } else {
            $filePath = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg';
        }

        if ($this->ajoutFichier->HasFile) {
            if ('JPG' == strtoupper(Atexo_Util::getExtension($this->ajoutFichier->FileName))) {
                if (!move_uploaded_file($this->ajoutFichier->LocalName, $filePath)) {
                    $message .= ($message ? '<br>' : '')."Problème lors de la copie de l'image de l'organisme";
                } else {
                    if (isset($_GET['small'])) {
                        @unlink("themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg');
                        @copy($filePath, "themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg');
                    } else {
                        @unlink("themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg');
                        @copy($filePath, "themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg');
                    }
                }
            } else {
                $message .= ($message ? '<br>' : '')."Image de l'organisme doit être de type .jpg";
            }
        }

        if ($message) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
        } else {
            $javascript = '<script>'
                      ."opener.document.getElementById('ctl0_CONTENU_PAGE_refresh').click(); "
                      .'window.close();'
                      .'</script>';

            echo $javascript;
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
