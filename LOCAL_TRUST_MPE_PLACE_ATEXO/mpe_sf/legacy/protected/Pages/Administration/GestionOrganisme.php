<?php

namespace Application\Pages\Administration;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdministrateurPeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * permet de créer ou modifier un organisme.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 3.0
 *
 * @copyright Atexo 2008
 */
class GestionOrganisme extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('admin');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->EnableForms();
        }

        self::customizeForm();
        if (!$this->isPostBack) {
            $this->servicesMetiersAccessibles->setOrg($this->getAcronymeOrganisme());
            $this->servicesMetiersAccessibles->displayOrganismeCompenenant();
            $id = Atexo_CurrentUser::getId();
            $orga = $this->getAcronymeOrganisme();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $this->acronyme->Text = $orga;
            $my_array = [];
            $my_array[] = ' -- Choisissez une catégorie -- ';
            $c = new Criteria();

            $arrayTypeAvis = CommonCategorieINSEEPeer::doSelect($c, $connexionCom);
            foreach ($arrayTypeAvis as $elt) {
                $tab = substr_count($elt->getId(), '.');
                if (0 == $tab) {
                    $tab = '';
                } elseif (1 == $tab) {
                    $tab = '&nbsp;&nbsp;';
                } else {
                    $tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
                }
                $my_array[$elt->getId()] = $tab.$elt->getId().' - '.$elt->getLibelle();
            }

            $this->categorie->DataSource = $my_array;
            $this->categorie->DataBind();

            $article = ['&nbsp;', 'Le', 'La', 'L\''];
            $this->article->DataSource = $article;
            $this->article->DataBind();

            $this->displayBlocLogos($orga);

            if (false != $id && '' != $id && $id > 0) {
                $admin = CommonAdministrateurPeer::retrieveByPK(intval($id), $connexionCom);
                if ($admin->getOrganisme() === $orga) {
                    $c = new Criteria();
                    $c->add(CommonOrganismePeer::ACRONYME, $orga, Criteria::EQUAL);
                    $organisme = CommonOrganismePeer::doSelectOne($c, $connexionCom);
                    $this->sigle->Text = $organisme->getSigle();
                    $this->categorie->selectedValue = $organisme->getCategorieInsee();
                    $this->article->selectedValue = $organisme->getTypeArticleOrg();
                    $this->denomination->Text = $organisme->getDenominationOrg();
                    $this->description->Text = $organisme->getDescriptionOrg();
                    $this->email->Text = $organisme->getEmail();
                    $this->url->Text = $organisme->getUrl();
                    $this->adresse->Text = $organisme->getAdresse();
                    $this->adresse2->Text = $organisme->getAdresse2();
                    $this->cp->Text = $organisme->getCp();
                    $this->ville->Text = $organisme->getVille();
                    $this->pays->Text = $organisme->getPays();
                    $this->tel->Text = $organisme->getTel();
                    $this->telecopie->Text = $organisme->getTelecopie();
                    $this->pays_ar->Text = $organisme->getPaysAr();
                    $this->ville_ar->Text = $organisme->getVilleAr();
                    $this->adresse2_ar->Text = $organisme->getAdresse2Ar();
                    $this->adresse_ar->Text = $organisme->getAdresseAr();
                    $this->denomination_ar->Text = $organisme->getDenominationOrgAr();
                    $this->description_ar->Text = $organisme->getDescriptionOrgAr();
                    $this->siren->Text = $organisme->getSiren();
                    $this->siret->Text = $organisme->getComplement();
                    $this->updateForm->setVisible(true);
                    $this->idEntite->Text = $organisme->getIdEntite();
                    if (1 == $organisme->getSousTypeOrganisme()) {
                        $this->sousTypeOrgEtat->checked = true;
                        $this->sousTypeOrgColectivite->checked = false;
                    } elseif (2 == $organisme->getSousTypeOrganisme()) {
                        $this->sousTypeOrgEtat->checked = false;
                        $this->sousTypeOrgColectivite->checked = true;
                    }
                } else {
                    $this->saveForm->setVisible(true);
                    $this->sigle->Text = strtoupper($orga);
                    $this->pays->Text = 'France';
                }
            } else {
                $this->sigle->Text = strtoupper($orga);
                $this->pays->Text = 'France';
                $this->saveForm->setVisible(true);
            }
        }

        // On n'affiche pas les boutons save/create si le socle_externe_ppp est actif
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->updateForm->setVisible(false);
            $this->saveForm->setVisible(false);
        }

        $this->panelMessageErreur->setVisible(false);
    }

    /**
     * Permet de creer l'organisme.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0
     *
     * @copyright Atexo 2008
     */
    public function save()
    {
        $infos = [];
        $orga = $this->getAcronymeOrganisme();
        $idAdmin = Atexo_CurrentUser::getId();
        if (!Atexo_Module::isEnabled('SocleExternePpp')) {
            if ('' != $this->description->Text) {
                $infos['description'] = $this->description->Text;
            }
            if ('' != $this->adresse->Text) {
                $infos['adresse'] = $this->adresse->Text;
            }
            if ('' != $this->adresse2->Text) {
                $infos['adresse2'] = $this->adresse2->Text;
            }
            if ('' != $this->cp->Text) {
                $infos['cp'] = $this->cp->Text;
            }
            if ('' != $this->ville->Text) {
                $infos['ville'] = $this->ville->Text;
            }
            if ('' != $this->tel->Text) {
                $infos['tel'] = $this->tel->Text;
            }
            if ('' != $this->telecopie->Text) {
                $infos['telecopie'] = $this->telecopie->Text;
            }
            if ('' != $this->email->Text) {
                $infos['email'] = $this->email->Text;
            }
            if ('' != $this->url->Text) {
                $infos['url'] = $this->url->Text;
            }
            if ('' != $this->pays->Text) {
                $infos['pays'] = $this->pays->Text;
            }
            if ('' != $this->denomination_ar->Text) {
                $infos['denomination_ar'] = $this->denomination_ar->Text;
            }
            if ('' != $this->description_ar->Text) {
                $infos['description_ar'] = $this->description_ar->Text;
            }
            if ('' != $this->adresse_ar->Text) {
                $infos['adresse_ar'] = $this->adresse_ar->Text;
            }
            if ('' != $this->adresse2_ar->Text) {
                $infos['adresse2_ar'] = $this->adresse2_ar->Text;
            }
            if ('' != $this->ville_ar->Text) {
                $infos['ville_ar'] = $this->ville_ar->Text;
            }
            if ('' != $this->pays_ar->Text) {
                $infos['pays_ar'] = $this->pays_ar->Text;
            }
            if ('' != $this->siren->Text) {
                $infos['siren'] = $this->siren->Text;
            }
            if ('' != $this->siret->Text) {
                $infos['siret'] = $this->siret->Text;
            }
        }
        if ('' != $this->idEntite->Text) {
            $infos['idEntite'] = $this->idEntite->Text;
        }

        $infos['sousTypeOrg'] = 2;
        if ($this->sousTypeOrgEtat->checked) {
            $infos['sousTypeOrg'] = 1;
        }
        $listeIdServices = [];
        $i = 0;
        foreach ($this->servicesMetiersAccessibles->serviceMetier->getItems() as $item) {
            if ($item->service->Checked) {
                $listeIdServices[$i++] = $item->idService->Value;
            }
        }
        $organismeByAcro = Atexo_Organismes::retrieveOrganismeByAcronyme($orga);
        if ($organismeByAcro instanceof CommonOrganisme) {
            $message = Prado::Localize('ACRONYME_DEJA_EXISTE');
        } else {
            $agentAdmin = (new Atexo_Organismes())->create($orga, $this->sigle->Text, $this->denomination->Text, $this->article->getSelectedValue(), $this->categorie->getSelectedValue(), $infos, false, $idAdmin, true, $listeIdServices);
            $message = $this->gestionImages($orga);
            if ($agentAdmin instanceof CommonAgent) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $admin = CommonAdministrateurPeer::retrieveByPK(intval($idAdmin), $connexionCom);
                $this->panelMessage->setMessage("L'organisme <strong>$orga</strong> a bien été créé.<br />
		         Un Agent avec toutes les habilitations a également été créé sur cet organisme.<br />
		         Ses identifiants sont admin_$orga /  Le mot de passe est identique à celui que vous avez utilisé pour vous connecter sur ce compte. (Il est recommandé de modifier ce mot de passe)");
                $this->confirmationPart->setVisible(true);
                $this->updateForm->setVisible(true);
                $this->saveForm->setVisible(false);
            }
            $this->displayBlocLogos($orga);
        }
        if ($message) {
            $this->afficherErreur($message);
        }
    }

    public function update()
    {
        $infos = [];
        $orga = $this->getAcronymeOrganisme();
        if (!Atexo_Module::isEnabled('SocleExternePpp')) {
            if ('' != $this->description->Text) {
                $infos['description'] = $this->description->Text;
            }
            if ('' != $this->adresse->Text) {
                $infos['adresse'] = $this->adresse->Text;
            }
            if ('' != $this->adresse2->Text) {
                $infos['adresse2'] = $this->adresse2->Text;
            }
            if ('' != $this->cp->Text) {
                $infos['cp'] = $this->cp->Text;
            }
            if ('' != $this->ville->Text) {
                $infos['ville'] = $this->ville->Text;
            }
            if ('' != $this->tel->Text) {
                $infos['tel'] = $this->tel->Text;
            }
            if ('' != $this->telecopie->Text) {
                $infos['telecopie'] = $this->telecopie->Text;
            }
            if ('' != $this->email->Text) {
                $infos['email'] = $this->email->Text;
            }
            if ('' != $this->url->Text) {
                $infos['url'] = $this->url->Text;
            }
            if ('' != $this->pays->Text) {
                $infos['pays'] = $this->pays->Text;
            }
            if ('' != $this->pays_ar->Text) {
                $infos['pays_ar'] = $this->pays_ar->Text;
            }
            if ('' != $this->ville_ar->Text) {
                $infos['ville_ar'] = $this->ville_ar->Text;
            }
            if ('' != $this->adresse2_ar->Text) {
                $infos['adresse2_ar'] = $this->adresse2_ar->Text;
            }
            if ('' != $this->adresse_ar->Text) {
                $infos['adresse_ar'] = $this->adresse_ar->Text;
            }
            if ('' != $this->description_ar->Text) {
                $infos['description_ar'] = $this->description_ar->Text;
            }
            if ('' != $this->denomination_ar->Text) {
                $infos['denomination_ar'] = $this->denomination_ar->Text;
            }
            if ('' != $this->siren->Text) {
                $infos['siren'] = $this->siren->Text;
            }
            if ('' != $this->siret->Text) {
                $infos['siret'] = $this->siret->Text;
            }
        }
        if ('' != $this->idEntite->Text) {
            $infos['idEntite'] = $this->idEntite->Text;
        }

        $infos['sousTypeOrg'] = 2;
        if ($this->sousTypeOrgEtat->checked) {
            $infos['sousTypeOrg'] = 1;
        }
        $listesServices = [];
        foreach ($this->servicesMetiersAccessibles->serviceMetier->getItems() as $item) {
            $listesServices[$item->idService->Value] = $item->service->Checked;
        }

        (new Atexo_Organismes())->update($orga, $this->sigle->Text, $this->denomination->Text, $this->article->getSelectedValue(), $this->categorie->getSelectedValue(), $infos, $listesServices);

        $message = $this->gestionImages($orga);
        if ($message) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
        }

        $this->panelMessage->setMessage("L'organisme <strong>$orga</strong> a bien été modifié<br />");
        $this->confirmationPart->setVisible(true);
        $this->updateForm->setVisible(true);
        $this->saveForm->setVisible(false);

        $this->displayBlocLogos($orga);
    }

    public function annuler()
    {
        $this->response->redirect('?page=Administration.GestionOrganisme');
    }

    public function gestionImages($orga)
    {
        //commencer le scan Antivirus
        if ($this->petitLogo->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->petitLogo->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->petitLogo->FileName.'"');

                return;
            }
        }

        if ($this->grandLogo->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->grandLogo->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->grandLogo->FileName.'"');

                return;
            }
        }
        //Fin du code scan Antivirus

        $message = '';
        $orga = $this->getAcronymeOrganisme();
        $baseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR');
        $pathOrganismeImage = Atexo_Config::getParameter('PATH_ORGANISME_IMAGE');

        if (Atexo_Module::isEnabled('AfficherImageOrganisme')) {
            if (!is_dir($baseRootDir.'/'.$orga.$pathOrganismeImage)) {
                system('mkdir '.escapeshellarg($baseRootDir.'/'.$orga.$pathOrganismeImage), $res);
                system('chmod 777 '.escapeshellarg($baseRootDir.'/'.$orga.$pathOrganismeImage), $res);
            }

            if ($this->petitLogo->HasFile) {
                if ('JPG' == strtoupper(Atexo_Util::getExtension($this->petitLogo->FileName))) {
                    if (!move_uploaded_file($this->petitLogo->LocalName, $baseRootDir.'/'.$orga.$pathOrganismeImage.'logo-organisme-petit.jpg')) {
                        $message .= ($message ? '<br>' : '')."Problème lors de la copie de l'image de l'organisme (petit format)";
                    } else {
                        @unlink("themes/$orga/".$pathOrganismeImage.'logo-organisme-petit.jpg');
                        @copy($baseRootDir.'/'.$orga.$pathOrganismeImage.'logo-organisme-petit.jpg', "themes/$orga/".$pathOrganismeImage.'logo-organisme-petit.jpg');
                    }
                } else {
                    $message .= ($message ? '<br>' : '')."Image de l'organisme (petit format) doit être de type .jpg";
                }
            }

            if ($this->grandLogo->HasFile) {
                if ('JPG' == strtoupper(Atexo_Util::getExtension($this->grandLogo->FileName))) {
                    if (!move_uploaded_file($this->grandLogo->LocalName, $baseRootDir.'/'.$orga.$pathOrganismeImage.'logo-organisme-grand.jpg')) {
                        $message .= ($message ? '<br>' : '')."Problème lors de la copie de l'image de l'organisme (grand format)";
                    } else {
                        @unlink("themes/$orga/".$pathOrganismeImage.'logo-organisme-grand.jpg');
                        @copy($baseRootDir.'/'.$orga.$pathOrganismeImage.'logo-organisme-grand.jpg', "themes/$orga/".$pathOrganismeImage.'logo-organisme-grand.jpg');
                    }
                } else {
                    $message .= ($message ? '<br>' : '')."Image de l'organisme (grand format) doit être de type .jpg";
                }
            }
        }

        return $message;
    }

    public function displayBlocLogos($orga)
    {
        $this->panelModifierPetiteImage->setVisible(false);
        $this->panelAjoutPetiteImage->setVisible(false);
        $this->panelModifierGrandeImage->setVisible(false);
        $this->panelAjoutGrandeImage->setVisible(false);

        if (Atexo_Module::isEnabled('AfficherImageOrganisme')) {
            if (is_file(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg')) {
                $this->panelModifierPetiteImage->setVisible(true);
            } else {
                $this->panelAjoutPetiteImage->setVisible(true);
            }
            if (is_file(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg')) {
                $this->panelModifierGrandeImage->setVisible(true);
            } else {
                $this->panelAjoutGrandeImage->setVisible(true);
            }
        }
    }

    public function reloadLogos($sender, $param)
    {
        $this->panelModifierPetiteImage->render($param->NewWriter);
        $this->panelModifierGrandeImage->render($param->NewWriter);
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('TraduireOrganismeArabe')) {
            $this->validateur_denomination_ar->setEnabled(true);
            $this->validateur_description_ar->setEnabled(true);
        } else {
            $this->validateur_denomination_ar->setEnabled(false);
            $this->validateur_description_ar->setEnabled(false);
        }
        $this->servicesMetiersAccessibles->setVisible('true');
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
        $this->confirmationPart->setVisible(true);
        $this->panelMessage->setVisible(false);
    }

    public function EnableForms()
    {
        $this->sigle->enabled = 'false';
        $this->categorie->enabled = 'false';
        $this->article->enabled = 'false';
        $this->denomination->enabled = 'false';
        $this->description->enabled = 'false';
        $this->email->enabled = 'false';
        $this->url->enabled = 'false';
        $this->adresse->enabled = 'false';
        $this->adresse2->enabled = 'false';
        $this->cp->enabled = 'false';
        $this->ville->enabled = 'false';
        $this->pays->enabled = 'false';
        $this->tel->enabled = 'false';
        $this->telecopie->enabled = 'false';
        $this->pays_ar->enabled = 'false';
        $this->ville_ar->enabled = 'false';
        $this->adresse2_ar->enabled = 'false';
        $this->adresse_ar->enabled = 'false';
        $this->denomination_ar->enabled = 'false';
        $this->description_ar->enabled = 'false';
        $this->siren->enabled = 'false';
        $this->siret->enabled = 'false';
        $this->idEntite->enabled = 'true';
        $this->sousTypeOrgEtat->enabled = 'false';
        $this->sousTypeOrgEtat->enabled = 'false';
    }

    private function getAcronymeOrganisme(): string
    {
        return Atexo_CurrentUser::getOrganismAcronym() ?: (new Atexo_CurrentUser())->getLoginUser();
    }
}
