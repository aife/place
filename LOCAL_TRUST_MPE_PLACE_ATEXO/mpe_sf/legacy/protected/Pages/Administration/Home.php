<?php

namespace Application\Pages\Administration;

use Application\Controls\MpeTPage;

/**
 * Ancienne page d'accueil non authentifié des admin.
 */
class Home extends MpeTPage
{
    public function onInit($param)
    {
        $this->response->redirect('/admin/login');
    }
}
