<?php

namespace Application\Pages\WebService;

use Application\Service\Atexo\Atexo_Db;
use PDO;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
include __DIR__.'/../../library/Atexo/Exception.php';
include __DIR__.'/../../library/Atexo/Config.php';
include __DIR__.'/../../library/Atexo/Db.php';
include __DIR__.'/../../library/Atexo/Controller/Front.php';
include __DIR__.'/../../library/Atexo/Config/Exception.php';

$server = new \SoapServer(null, ['uri' => '']);
$server->addFunction('retrieveAgentGeneralProfil');
$server->addFunction('retrieveAgentSocleBySSO');
$server->addFunction('retrieveInfoOrganisme');
$server->addFunction('agentHaveService');
$server->addFunction('retreiveEmailAdmin');
$server->handle();

function retrieveAgentGeneralProfil($ssoAgent)
{
    $query_general_profil = "SELECT id_sso,id_agent FROM sso_agent WHERE id_sso ='".$ssoAgent."'";
    $statement = Atexo_Db::getLinkCommon(true)->query($query_general_profil);
    $array = [];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        $array['id_sso'] = $row['id_sso'];
        $array['db_name'] = $row['db_name'];
        $array['id_service_ebrgn'] = $row['id_service_ebrgn'];
    }

    return $array;
}

function retrieveAgentSocleBySSO($ssoAgent)
{
    $sql = "Select sso_agent.id_sso,Agent.email,Agent.nom,Agent.prenom,Agent.organisme,Agent.login,Agent.organisme,Agent.service_id,Agent.elu,Agent.id from Agent, sso_agent where Agent.id = sso_agent.id_agent and sso_agent.id_sso = '".$ssoAgent."'";
    $statement = Atexo_Db::getLinkCommon(true)->query($sql);
    $array = [];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        $array['loginAgent'] = $row['login'];
        $array['organisme'] = $row['organisme'];
        $array['id'] = $row['id'];
        $array['id_sso'] = $row['id_sso'];
        $array['email'] = $row['email'];
        $array['nom'] = $row['nom'];
        $array['prenom'] = $row['prenom'];
        $array['db_name'] = $row['organisme'];
        $array['service_id'] = $row['service_id'];
    }

    return $array;
}

function retrieveInfoOrganisme($orgAcronyme)
{
    $sql = 'Select id,acronyme'
    ." from Organisme where acronyme ='".$orgAcronyme."'";
    $statement = Atexo_Db::getLinkCommon(true)->query($sql);
    $array = [];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        $array['id'] = $row['id'];
        $array['acronyme'] = $row['acronyme'];
        $array['organisme'] = $row['acronyme'];
//        $array['categorie_insee'] =  $row['categorie_insee'];
//        $array['adresse'] =  $row['adresse'];
//        $array['cp'] =  $row['cp'];
//        $array['canton'] =  $row['canton'];
//        $array['email'] =  $row['email'];
//        $array['url'] =  $row['url'];
//        $array['date_creation'] =  $row['date_creation'];
//        $array['type_article_org'] =  $row['type_article_org'];
//        $array['latitude_degre'] =  $row['latitude_degre'];
//        $array['latitude_minute'] =  $row['latitude_minute'];
//        $array['latitude_seconde'] =  $row['latitude_seconde'];
//        $array['longitude_degre'] =  $row['longitude_degre'];
//        $array['longitude_minute'] =  $row['longitude_minute'];
//        $array['longitude_seconde'] =  $row['longitude_seconde'];
//        $array['signataire_cao'] =  $row['signataire_cao'];
    }

    return $array;
}

/**
 * Fonction qui retourne si l'agent à déjà le service metier.
 */
function agentHaveService($email, $organisme, $idServiceMetier)
{
    $sql = 'SELECT Agent_Service_Metier.id_agent, Agent_Service_Metier.id_service_metier, Agent.id, Agent.organisme, Agent.login ';
    $sql .= 'FROM Agent_Service_Metier, Agent ';
    $sql .= "WHERE Agent.id = Agent_Service_Metier.id_agent AND Agent_Service_Metier.id_service_metier = '".$idServiceMetier."' AND Agent.organisme = '".$organisme."' AND Agent.email = '".$email."'";
    $statement = Atexo_Db::getLinkCommon(true)->query($sql);
    if ($statement->fetch(PDO::FETCH_ASSOC)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Fonction qui retourne l'email de l'administrateur du organisme'.
 */
function retreiveEmailAdmin($organisme)
{
    $sql = 'SELECT Agent_Service_Metier.id_agent, Agent_Service_Metier.id_service_metier, Agent.id,Agent.organisme, Agent.service_id, Agent.email ';
    $sql .= 'FROM Agent_Service_Metier, Agent ';
    $sql .= "WHERE Agent.id = Agent_Service_Metier.id_agent AND Agent.organisme = '".$organisme."' AND Agent_Service_Metier.id_service_metier = '2' AND Agent.service_id is null";

    return $sql;
}
