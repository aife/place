<?php

namespace Application\Pages\WebService;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Prado\Prado;
use Prado\Util\TLogger;

class ImportConsultation extends MpeTPage
{
    public function onInit($param)
    {
    }

    public function onLoad($param)
    {
        self::insertTenders($_POST['xmlTender']);
    }

    public function insertTenders($xmlTender)
    {
        $organismeName = null;
        //echo "Début Appel WS \n";
        Prado::log('Début Appel WS de la plate forme ', TLogger::INFO, 'Atexo.Interfaces.ImportConsultation');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        try {
            if ($xmlTender) {
                $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($xmlTender);
                $nodeTender = $domXml->getElementsByTagName('Tender');
                $nombreTender = is_countable($nodeTender) ? count($nodeTender) : 0;
                echo "Nombre de consultation $nombreTender \n";

                for ($i = 0; $i < $nombreTender; ++$i) {
                    $organisme = utf8_decode($nodeTender[$i]->getAttribute('organism'));
                    $acronymeOrg = $organisme;
                    $organismeName = utf8_decode($nodeTender[$i]->getAttribute('organismeName'));
                    // Calcul de la nouvelle reference
                    $newRef = utf8_decode($nodeTender[$i]->getAttribute('referenceConsultation'));
                    $refUtilisateur = utf8_decode($nodeTender[$i]->getAttribute('reference'));
                    //$newRef = sha1($organisme . $reference);
                    $updateConsultation = true;
                    //Pour ferivier si la consultation éxiste déjà

                    $c = new Criteria();
                    $c->add(CommonConsultationPeer::REF_ORG_PARTENAIRE, $newRef);
                    $c->add(CommonConsultationPeer::ORGANISME, $acronymeOrg);
                    $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);

                    if (!($commonConsultation instanceof CommonConsultation)) {
                        $commonConsultation = new CommonConsultation();
                        $commonConsultation->setOrganisme($acronymeOrg);
                        $commonConsultation->setRefOrgPartenaire($newRef);
                        $updateConsultation = false;
                    }

                    // informations modalités de réponse
                    $electronicResponse = $nodeTender[$i]->getAttribute('electronicResponse');
                    $digitalSignature = $nodeTender[$i]->getAttribute('digitalSignature');

                    $dateMiseOnLigneCalcule = $nodeTender[$i]->getAttribute('dateMiseOnLigneCalcule');
                    $type = (string) $nodeTender[$i]->getAttribute('type');
                    $procedureType = $nodeTender[$i]->getAttribute('procedureType');
                    $category = utf8_decode($nodeTender[$i]->getAttribute('category'));
                    $cpv = $nodeTender[$i]->getAttribute('cpv');
                    $closureDate = $nodeTender[$i]->getAttribute('closureDate');
                    $nodeTitle = $nodeTender[$i]->getElementsByTagName('Title');
                    $title = utf8_decode($nodeTitle[0]->get_content());
                    $nodeSubject = $nodeTender[$i]->getElementsByTagName('Subject');
                    $description = $nodeSubject[0]->get_content();
                    //$nodeUrl = $nodeTender[$i]->getElementsByTagName('Url');
                    /* KBE */
                    $echantillon = utf8_decode($nodeTender[$i]->getAttribute('echantillon'));
                    $modePassation = utf8_decode($nodeTender[$i]->getAttribute('modePassation'));
                    $variante = utf8_decode($nodeTender[$i]->getAttribute('variante'));
                    $visite = utf8_decode($nodeTender[$i]->getAttribute('visite'));
                    $prixAquisitionPlans = utf8_decode($nodeTender[$i]->getAttribute('prixAquisitionPlans'));
                    $cautionProvisoire = utf8_decode($nodeTender[$i]->getAttribute('cautionConsultation'));

                    $nodeadresseRetraisDossiers = $nodeTender[$i]->getElementsByTagName('adresseRetraisDossiers');
                    $adresseRetraisDossiers = $nodeadresseRetraisDossiers[0]->get_content();

                    $nodeadresseDepotOffres = $nodeTender[$i]->getElementsByTagName('adresseDepotOffres');
                    $adresseDepotOffres = $nodeadresseDepotOffres[0]->get_content();

                    $nodelieuOuverturePlis = $nodeTender[$i]->getElementsByTagName('lieuOuverturePlis');
                    $lieuOuverturePlis = $nodelieuOuverturePlis[0]->get_content();

                    $nodeadresseRetraisDossiersAr = $nodeTender[$i]->getElementsByTagName('adresseRetraisDossiersAr');
                    $adresseRetraisDossiersAr = $nodeadresseRetraisDossiersAr[0]->get_content();

                    $nodeadresseDepotOffresAr = $nodeTender[$i]->getElementsByTagName('adresseDepotOffresAr');
                    $adresseDepotOffresAr = $nodeadresseDepotOffresAr[0]->get_content();

                    $nodelieuOuverturePlisAr = $nodeTender[$i]->getElementsByTagName('lieuOuverturePlisAr');
                    $lieuOuverturePlisAr = $nodelieuOuverturePlisAr[0]->get_content();

                    /* KBE */
                    // Inserssion en base de l'objet consultation
                    //$commonConsultation->setOrgDenomination($organismeName);
                    $commonConsultation->setReferenceUtilisateur($refUtilisateur);
                    $commonConsultation->setIdTypeProcedure((new Atexo_Consultation_ProcedureType())->getIdTypeProcedureByAbbreviation($procedureType));
                    $commonConsultation->setIdTypeProcedureOrg((new Atexo_Consultation_ProcedureType())->getIdTypeProcedureOrgByAbbreviation($procedureType, $acronymeOrg));
                    $arrayTypeAvisObject = (new Atexo_Consultation_TypeAvis())->retreiveTypeAvisByAbreviation($type);
                    $typeAvisObject = $arrayTypeAvisObject[0];
                    $commonConsultation->setIdTypeAvis($typeAvisObject->getId());
                    $commonConsultation->setDateMiseEnLigneCalcule($dateMiseOnLigneCalcule);
                    $commonConsultation->setDatedebut(date('Y-m-d H:i:s'));
                    $commonConsultation->setDatemiseenligne($dateMiseOnLigneCalcule);
                    $commonConsultation->setDatevalidation($dateMiseOnLigneCalcule);
                    $commonConsultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $commonConsultation->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2'));
                    $commonConsultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE'));
                    $commonConsultation->setServiceId('0');
                    $commonConsultation->setServiceAssocieId('0');
                    $commonConsultation->setEtatEnAttenteValidation('1');
                    //$commonConsultation->setConsultationMigration('1');
                    //$commonConsultation->setUrlConsultationMigration($url);

                    $categoryObject = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($category);
                    $idCategorie = 0;
                    if ($categoryObject) {
                        $idCategorie = $categoryObject->getId();
                    }
                    $commonConsultation->setCategorie($idCategorie);
                    //$arCpv = explode(",", $cpv, 2);
                    //$commonConsultation->setCodeCpv1($arCpv[0]);
                    //$commonConsultation->setCodeCpv2(str_replace(',','#', $arCpv[1]));
                    $commonConsultation->setDatefin($closureDate);
                    /*$scriptGeo = ",47,48,49,45,46,1,2,3,4,5,6,62,63,64,65,66,67,68,26,27,28,29,30,22,23,24,25,38,39,40,41,42,43,44,7,9,10,11";
                     * $scriptGeo .= ",12,6486,16,17,18,19,20,21,50,51,52,53,54,31,32,33,34,35,36,37,55,56,57,58,59,60,61,13,14,15,";
                    //$lieuxExec = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeoN2ByArrayDenomination2(explode(",", $deliveryPlace));
                    if ($lieuxExec) {
                        // tout les ids doivent être entouré par une virgule
                        $scriptGeo = ",";

                        foreach ($lieuxExec as $oneGeo) {
                            $scriptGeo .= $oneGeo->getId() . ",";
                        }
                    }*/
                    $commonConsultation->setLieuExecution('');
                    $commonConsultation->setIntitule($title);
                    $commonConsultation->setObjet($description);
                    $commonConsultation->setAutoriserReponseElectronique($electronicResponse);
                    $commonConsultation->setSignatureOffre($digitalSignature);

                    /* KBE */
                    $commonConsultation->setEchantillon($echantillon);
                    $commonConsultation->setModePassation($modePassation);
                    $commonConsultation->setVariantes($variante);
                    $commonConsultation->setVisitesLieux($visite);

                    $commonConsultation->setAdresseRetraisDossiers($adresseRetraisDossiers);
                    $commonConsultation->setAdresseDepotOffres($adresseDepotOffres);
                    $commonConsultation->setLieuOuverturePlis($lieuOuverturePlis);

                    $commonConsultation->setAdresseRetraisDossiersAr($adresseRetraisDossiersAr);
                    $commonConsultation->setAdresseDepotOffresAr($adresseDepotOffresAr);
                    $commonConsultation->setLieuOuverturePlisAr($lieuOuverturePlisAr);
                    $commonConsultation->setCautionProvisoire($cautionProvisoire);
                    /* KBE */
                    $commonConsultation->setAlloti('0');
                    $commonConsultation->setPrixAquisitionPlans($prixAquisitionPlans);

                    /*if ($dateFin != '' && strcmp($dateFinOld,$dateFin) != 0) {
                        $dateTmpFile = Atexo_Config::getParameter('COMMON_TMP') . "df" . session_id() . time() . "date.tmp";
                        Atexo_Util::write_file($dateTmpFile, "Creation de la consultation -" . $this->reference->Text . "-, date de cleture : " . $dateFin);
                        if (is_file($dateTmpFile)) {
                            $arrayTimeStampDateFin = $atexoCrypto->timeStampFile($dateTmpFile);
                            if ($arrayTimeStampDateFin) {
                                $dateFinO->setHorodatage($arrayTimeStampDateFin['horodatage']);
                                $dateFinO->setUntrusteddate($arrayTimeStampDateFin['untrustedDate']);
                                $dateFinO->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                                $dateFinO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                                $dateFinO->setDatefin($dateFin);
                                if ($modificationAllowed) {
                                    $dateFinO->setStatut(Atexo_Config::getParameter('REMPLACEMENT_FILE'));
                                } else {
                                    $dateFinO->setStatut(Atexo_Config::getParameter('CREATION_FILE'));
                                }
                                $commonConsultationO->addCommonDATEFIN($dateFinO);

                            }

                        }
                    }*/
                    $commonConsultation->save($connexionCom);
                    echo 'cons save '.$refUtilisateur."\n";
                    /*$logo = utf8_decode($nodeTender[$i]->getAttribute('urlLogo'));
                    if($logo){
                        $commonConsultation->setUrl($logo);
                    }*/

                    // Récuperation des Lots
                    $allotisement = $nodeTender[$i]->getElementsByTagName('Lot');
                    if ((is_countable($allotisement) ? count($allotisement) : 0) > 0) {
                        $commonConsultation->setAlloti('1');
                    } else {
                        $commonConsultation->setAlloti('0');
                    }
                    $commonConsultation->save($connexionCom);

                    if (Atexo_Module::isEnabled('AlerteMetier')) {
                        Atexo_AlerteMetier::lancerAlerte('ImportConsultation', $commonConsultation);
                    }
                    $nodeLots = $nodeTender[$i]->getElementsByTagName('Lot');
                    $nombreLot = is_countable($nodeLots) ? count($nodeLots) : 0;
                    for ($j = 0; $j < $nombreLot; ++$j) {
                        $number = $nodeLots[$j]->getAttribute('number');
                        $category = utf8_decode($nodeLots[$j]->getAttribute('category'));
                        $cpv = $nodeLots[$j]->getAttribute('cpv');
                        $nodeTitle = $nodeLots[$j]->getElementsByTagName('Title');
                        $title = $nodeTitle[0]->get_content();
                        $nodeSubject = $nodeLots[$j]->getElementsByTagName('Subject');
                        $description = $nodeSubject[0]->get_content();
                        $caution = $nodeLots[$j]->getAttribute('caution');
                        if ($updateConsultation) {
                            $categoryLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($acronymeOrg, $commonConsultation->getId(), $number);
                            //$categoryLot = CommonCategorieLotPeer::retrieveByPK($acronymeOrg, $commonConsultation->getId(), $number, $connexionCom);
                            if (!($categoryLot instanceof CommonCategorieLot)) {
                                $categoryLot = new CommonCategorieLot();
                            }
                        } else {
                            $categoryLot = new CommonCategorieLot();
                        }
                        $categoryLot->setOrganisme($acronymeOrg);
                        $categoryLot->setConsultationId($commonConsultation->getId());
                        $categoryLot->setLot($number);
                        $categoryLot->setDescription($description);
                        $categoryObject = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($category);
                        $idCategorie = 0;
                        if ($categoryObject) {
                            $idCategorie = $categoryObject->getId();
                        }
                        $categoryLot->setCategorie($idCategorie);
                        $categoryLot->setDescription($title);
                        $categoryLot->setDescriptionDetail($description);
                        $arCpv = explode(',', $cpv);
                        $categoryLot->setCodeCpv1($arCpv[0]);
                        $categoryLot->setCodeCpv2(str_replace(',', '#', $arCpv[1]));
                        $categoryLot->setCautionProvisoire($caution);
                        $categoryLot->save($connexionCom);
                        unset($categoryLot);
                        ///////
                    }
                    unset($commonConsultation);
                }
                Prado::log('Cool ! Migration de :'.$nombreTender.' consultations réussite pour '.$organismeName.'! !', TLogger::INFO, 'Atexo.Interfaces.ImportConsultation');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            Prado::log('Erreur lors de la recuperation OU insersion  des consultations : '.$e, TLogger::ERROR, 'Atexo.Interfaces.ImportConsultation');
        }

        return;
    }
}
