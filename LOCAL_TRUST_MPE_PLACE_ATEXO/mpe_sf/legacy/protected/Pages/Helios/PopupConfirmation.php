<?php

namespace Application\Pages\Helios;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupConfirmation extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (('' != isset($_GET['isTrans'])) && ('' != isset($_GET['lot']))) {
            $this->labelClose->Text = '<script>window.opener.document.main_form.submit();window.close();</script>';
        }
    }

    public function deleteTransmission($sender, $param)
    {
        if (isset($_GET['idTrans'])) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            CommonHeliosTeletransmissionPeer::doDelete([Atexo_Util::atexoHtmlEntities($_GET['idTrans']), Atexo_CurrentUser::getCurrentOrganism()], $connexion);
            $this->labelClose->Text = '<script>window.opener.document.main_form.submit();window.close();</script>';
        }
    }
}
