<?php

namespace Application\Pages\Helios;

use Application\Controls\BandeauAgent;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/**
 * Page d'acceuil autentifie des agents au service teletransmission.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class HeliosAccueilAuthentifie extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agenthelios');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->nomPrenom->Text = (new BandeauAgent())->getNomPrenomUser();
        $this->idMonEntitePublic->Text = Atexo_EntityPurchase::getPathEntityById('0', Atexo_CurrentUser::getOrganismAcronym());
        $this->idMonEntiteAchat->Text = Atexo_EntityPurchase::getPathEntityById(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getOrganismAcronym());
    }

    public function gotoUpdateProfil()
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_PPP_MON_COMPTE_AGENT'));
        }
        $this->response->redirect('index.php?page=Agent.AgentModifierCompte');
    }
}
