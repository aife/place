<?php

namespace Application\Pages\Helios;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonHeliosPiecePublicite;
use Application\Propel\Mpe\CommonHeliosPvConsultation;
use Application\Propel\Mpe\CommonHeliosRapportPrefet;
use Application\Propel\Mpe\CommonHeliosTableauAr;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Page de ajout de fichier a une teletransmission.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupAddFile extends MpeTPage
{
    private string $_type = '';
    private string $_reference = '';

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->description->setVisible(false);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->_type = Atexo_Util::atexoHtmlEntities($_GET['type']);
        switch ($this->_type) {
            case 'piece_publicite':
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_FICHIER_ANNONCE');
                break;
            case 'pv':
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_PV');
                break;
            case 'rapport_prefet':
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_RAPPORT');
                break;
            case 'deliberation_mapa':
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_DELIBERATION_MAPA');
                break;
            case 'tableau_ar':
                $this->description->setVisible(true);
                $this->labelDescription->Text = Prado::localize('DEFINE_ATTESTATION_PERSONNE_PUBLIC');
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_TABLEAU_RECEPTION_AR');
                break;
            case 'piece_optionnelle':
                $this->description->setVisible(true);
                $this->mainLabel->Text = Prado::localize('DEFINE_AJOUT_PIECES_OPTIONNELLE');
                break;
            default:
                return;
        }
    }

    /**
     * Verifier la taille maximum du document à ajouter
     * ( 2Mo maximum).
     */
    public function verifyDocTailleCreation($sender, $param)
    {
        $sizedocAttache = $this->ajoutFichier->FileSize;
        if (0 == $sizedocAttache) {
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_FICHIER_VIDE').'<br/>';
        } elseif ($sizedocAttache >= 2 * 1024 * 1024) {
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_TAILLE_DOC', ['ajoutFichier' => $this->ajoutFichier->FileName]).'<br/>';
        }
    }

    /**
     *ajouter un document dans la base.
     */
    public function addPieceJointe($sender, $param)
    {
        //if ($this->IsValid) {
        if ($this->ajoutFichier->HasFile) {
            $infile = Atexo_Config::getParameter('COMMON_TMP').'pieceJointeHelios'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                $atexoBlob = new Atexo_Blob();
                $atexoCrypto = new Atexo_Crypto();
                $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                if (is_array($arrayTimeStampAvis)) {
                    $idBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, Atexo_CurrentUser::getCurrentOrganism());
                    switch ($this->_type) {
                        case 'piece_publicite':
                            $this->doAddPiecePub(Atexo_CurrentUser::getCurrentOrganism(), $arrayTimeStampAvis, $idBlob);
                            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshPiecePub').click();window.close();</script>";
                            break;
                        case 'pv':
                            $this->doAddPiecePv(Atexo_CurrentUser::getCurrentOrganism(), $arrayTimeStampAvis, $idBlob);
                            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshPiecePv').click();window.close();</script>";
                            break;
                        case 'rapport_prefet':
                            $this->doAddPieceRapport(Atexo_CurrentUser::getCurrentOrganism(), $arrayTimeStampAvis, $idBlob);
                            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshPiecesRapport').click();window.close();</script>";
                            break;
                        case 'tableau_ar':
                            $this->doAddPieceAr(Atexo_CurrentUser::getCurrentOrganism(), $arrayTimeStampAvis, $idBlob);
                            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshPiecesAr').click();window.close();</script>";
                            break;
                        case 'piece_optionnelle':
                            $this->doAddPieceOptionnelle(Atexo_CurrentUser::getCurrentOrganism(), $arrayTimeStampAvis, $idBlob);
                            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshPiecesOptionnelles').click();window.close();</script>";
                            break;
                    }
                }
            }
            @unlink($infile);
        }
        //$this->labelClose->Text = "<script>window.opener.document.main_form.submit();window.close();</script>";
        // }
    }

    /**
     * ajout d'une pièce de publicité.
     */
    public function doAddPiecePub($org, $arrayTimeStampAvis, $idBlob)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piecePub = new CommonHeliosPiecePublicite();
        $piecePub->setOrganisme($org);
        $piecePub->setConsultationId($this->_reference);
        $piecePub->setFichier($idBlob);
        $piecePub->setNomFichier($this->ajoutFichier->FileName);
        $piecePub->setTaille($this->ajoutFichier->FileSize);
        $piecePub->setHorodatage($arrayTimeStampAvis['horodatage']);
        $piecePub->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
        $piecePub->save($connexion);
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $org, $connexion);
        $trans->setPubliciteCons($trans->getPubliciteCons().';'.$piecePub->getId());
        $trans->save($connexion);
    }

    /**
     * ajout d'une pièce de publicité.
     */
    public function doAddPiecePv($org, $arrayTimeStampAvis, $idBlob)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piecePv = new CommonHeliosPvConsultation();
        $piecePv->setOrganisme($org);
        $piecePv->setConsultationId($this->_reference);
        $piecePv->setFichier($idBlob);
        $piecePv->setNomFichier($this->ajoutFichier->FileName);
        $piecePv->setTaille($this->ajoutFichier->FileSize);
        $piecePv->setHorodatage($arrayTimeStampAvis['horodatage']);
        $piecePv->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
        $piecePv->save($connexion);
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $org, $connexion);
        $trans->setIdsPvsCao($trans->getIdsPvsCao().';'.$piecePv->getId());
        $trans->save($connexion);
    }

    /**
     * ajout d'une pièce de publicité.
     */
    public function doAddPieceRapport($org, $arrayTimeStampAvis, $idBlob)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = new CommonHeliosRapportPrefet();
        $piece->setOrganisme($org);
        $piece->setConsultationId($this->_reference);
        $piece->setFichier($idBlob);
        $piece->setNomFichier($this->ajoutFichier->FileName);
        $piece->setTaille($this->ajoutFichier->FileSize);
        $piece->setHorodatage($arrayTimeStampAvis['horodatage']);
        $piece->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
        $piece->save($connexion);
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $org, $connexion);
        $trans->setIdsRapportsPrefet($trans->getIdsRapportsPrefet().';'.$piece->getId());
        $trans->save($connexion);
    }

    /**
     * ajout d'une pièce de publicité.
     */
    public function doAddPieceAr($org, $arrayTimeStampAvis, $idBlob)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = new CommonHeliosTableauAr();
        $piece->setOrganisme($org);
        $piece->setDescription($this->labelDescription->Text);
        $piece->setConsultationId($this->_reference);
        $piece->setFichier($idBlob);
        $piece->setNomFichier($this->ajoutFichier->FileName);
        $piece->setTaille($this->ajoutFichier->FileSize);
        $piece->setHorodatage($arrayTimeStampAvis['horodatage']);
        $piece->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
        $piece->save($connexion);
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $org, $connexion);
        $trans->setTableauAr($piece->getId());
        $trans->save($connexion);
    }

    /**
     * ajout d'une pièce optionnelle.
     */
    public function doAddPieceOptionnelle($org, $arrayTimeStampAvis, $idBlob)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $org, $connexion);
        $fileName = $this->ajoutFichier->FileName;
        $fileSize = $this->ajoutFichier->FileSize;
        if ('' == $trans->getNomPiecej1()) {
            $trans->setPiecej1($idBlob);
            $trans->setNomPiecej1($fileName);
            $trans->setTaillePiecej1($fileSize);
            $trans->setHorodatagePiecej1($arrayTimeStampAvis['horodatage']);
            $trans->setUntrusteddatePiecej1($arrayTimeStampAvis['untrustedDate']);
            $trans->setPiecej1Desc($this->labelDescription->Text);
        } else {
            $trans->setPiecej2($idBlob);
            $trans->setNomPiecej2($fileName);
            $trans->setTaillePiecej2($fileSize);
            $trans->setHorodatagePiecej2($arrayTimeStampAvis['horodatage']);
            $trans->setUntrusteddatePiecej2($arrayTimeStampAvis['untrustedDate']);
            $trans->setPiecej2Desc($this->labelDescription->Text);
        }
        $trans->save($connexion);
    }
}
