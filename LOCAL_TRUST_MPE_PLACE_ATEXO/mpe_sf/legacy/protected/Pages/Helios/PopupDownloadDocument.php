<?php

namespace Application\Pages\Helios;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonHeliosPiecePublicitePeer;
use Application\Propel\Mpe\CommonHeliosPvConsultationPeer;
use Application\Propel\Mpe\CommonHeliosRapportPrefetPeer;
use Application\Propel\Mpe\CommonHeliosTableauArPeer;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de telecharger le document du coffre fort.
 *
 * @author Mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupDownloadDocument extends DownloadFile
{
    private string $_type = '';
    private string $_identifiant = '';
    private string|bool $_organisme = '';

    public function onLoad($param)
    {
        if (isset($_GET['type']) && isset($_GET['id'])) {
            $this->_identifiant = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->_type = Atexo_Util::atexoHtmlEntities($_GET['type']);
            $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
            switch ($this->_type) {
                case 'piece_publicite':
                    $this->downloadPiecePublicite();
                    break;
                case 'pv':
                    $this->downloadPv();
                    break;
                case 'rapport_prefet':
                    $this->downloadRapportPrefet();
                    break;
                case 'deliberation_mapa':
                    break;
                case 'tableau_ar':
                    $this->downloadTableauAr();
                    break;
                case 'piece_optionnelle':
                    $this->downloadPieceOptionnelle();
                    break;
                case 'acte_engagement':
                    $this->downloadPieceActe();
                    break;
                default:
                    return;
            }
        }
    }

    public function downloadPiecePublicite()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonHeliosPiecePublicitePeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }

    public function downloadPv()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonHeliosPvConsultationPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }

    public function downloadTableauAr()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonHeliosTableauArPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }

    public function downloadRapportPrefet()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonHeliosRapportPrefetPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }

    public function downloadPieceOptionnelle()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonHeliosTeletransmissionPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            if ('0' == $_GET['piece']) {
                $this->_idFichier = $piece->getPiecej1();
                $this->_nomFichier = $piece->getNomPiecej1();
            } elseif ('1' == $_GET['piece']) {
                $this->_idFichier = $piece->getPiecej2();
                $this->_nomFichier = $piece->getNomPiecej2();
            }
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }

    public function downloadPieceActe()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonFichierEnveloppePeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexionCom);
        if ($piece) {
            $this->_idFichier = $piece->getIdBlob();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }
}
