<?php

namespace Application\Pages\Helios;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonHeliosTeletransmissionLotPeer;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Helios\Atexo_Helios_CriteriaVo;
use Application\Service\Atexo\Helios\Atexo_Helios_Teletransmission;

/**
 * Page d'accueil Recherche Teletransmission.
 *
 * @author ABISOUROUR Othmane <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class RechercheTeletransmission extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agenthelios');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['keyWord'])) {
            $keyWord = Atexo_Util::atexoHtmlEntities($_GET['keyWord']);
            $criteriaVo = new Atexo_Helios_CriteriaVo();
            $criteriaVo->setKeyWordAdvancedSearch($keyWord);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->dataForSearchResult($criteriaVo);
            $this->advancedSearch->setVisible(false);
            $this->resultSearch->setVisible(true);
        } elseif (isset($_GET['result'])) {
            $criteriaVo = Atexo_CurrentUser::readFromSession('criteriaTrans');
            if ($criteriaVo instanceof Atexo_Helios_CriteriaVo) {
                $this->searchTeletransmition($criteriaVo);
            } else {
                $this->advancedSearch->setVisible(true);
                $this->resultSearch->setVisible(false);
            }
        } else {
            $this->advancedSearch->setVisible(true);
            $this->resultSearch->setVisible(false);
        }
    }

    public function doSearchTeletransmition($sender, $param)
    {
        $criteriaVo = new Atexo_Helios_CriteriaVo();
        self::setData($criteriaVo);
        $this->searchTeletransmition($criteriaVo);
    }

    /**
     * Fonction qui affiche le tableau de bord des resultats de la recherche.
     */
    public function searchTeletransmition($criteriaVo)
    {
        Atexo_CurrentUser::writeToSession('criteriaTrans', $criteriaVo);
        $nombreElement = (new Atexo_Helios_Teletransmission())->searchTeletransmission($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->advancedSearch->setVisible(false);
            $this->resultSearch->setVisible(true);
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->repeaterTeleTransmission->setVirtualItemCount($nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterTeleTransmission->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterTeleTransmission->PageSize);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->populateData($criteriaVo);
        } else {
            $this->advancedSearch->setVisible(false);
            $this->resultSearch->setVisible(true);
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            //echo "Test";exit;
        }
    }

    /**
     * Fonction qui remplit les criteres necessaire pour l'affichage du resultat'.
     */
    public function setData($criteriaVo)
    {
        $criteriaVo->setNumTeletransmission($this->numTeletransmission->Text);
        $criteriaVo->setRefConsultation($this->referenceConsultation->Text);
        $criteriaVo->setNumMarche($this->numMarche->Text);
        if ('0' != $this->statutEnvoi->getSelectedValue()) {
            $criteriaVo->setStatutEnvoi($this->statutEnvoi->getSelectedValue());
        }
        $criteriaVo->setDateEnvoiStart($this->dateEnvoiStart->Text);
        $criteriaVo->setDateEnvoiEnd($this->dateEnvoiEnd->Text);
    }

    public function pageChanged($sender, $param)
    {
        $this->advancedSearch->setVisible(false);
        $this->resultSearch->setVisible(true);
        $this->repeaterTeleTransmission->CurrentPageIndex = $param->NewPageIndex;
        $this->repeaterTeleTransmission->CurrentPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function populateData(Atexo_Helios_CriteriaVo $criteriaVo)
    {
        $infos = [];
        $offset = $this->repeaterTeleTransmission->CurrentPageIndex * $this->repeaterTeleTransmission->PageSize;
        $limit = $this->repeaterTeleTransmission->PageSize;
        if ($offset + $limit > $this->repeaterTeleTransmission->getVirtualItemCount()) {
            $limit = $this->repeaterTeleTransmission->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $results = (new Atexo_Helios_Teletransmission())->searchTeletransmission($criteriaVo, false);
        foreach ($results as $result) {
            $teletransmission['id'] = $result['id'];
            $teletransmission['num_teletransmission'] = $result['shown_id'];
            $teletransmission['reference_utilisateur'] = $result['reference_utilisateur'];
            $teletransmission['ref_cons'] = $result['reference'];
            $agent = (new Atexo_Agent())->retrieveAgent($result['id_agent_envoi']);
            if ($agent) {
                $teletransmission['agent'] = $agent->getPrenom().' '.$agent->getNom();
                $teletransmission['date_envoi'] = str_replace(' ', '<br />', Atexo_Util::iso2frnDateTime($result['date_envoi']));
            } else {
                $teletransmission['agent'] = '-';
                $teletransmission['date_envoi'] = '-';
            }
            $teletransmission['TransLot'] = (new Atexo_Helios_Teletransmission())->retreiveTransLotByIdtrans($result['id']);
            $statut = [];
            if ($result['id']) {
                foreach ($teletransmission['TransLot'] as $oneTransLot) {
                    $statut[] = $oneTransLot->getStatutEnvoi();
                }
                if (in_array(Atexo_Config::getParameter('TRANSMISSION_SAVE'), $statut) && in_array(Atexo_Config::getParameter('TRANSMISSION_SEND'), $statut)) {
                    $teletransmission['statut_envoi'] = '12';
                } elseif (in_array(Atexo_Config::getParameter('TRANSMISSION_SAVE'), $statut)) {
                    $teletransmission['statut_envoi'] = Atexo_Config::getParameter('TRANSMISSION_SAVE');
                } elseif (in_array(Atexo_Config::getParameter('TRANSMISSION_SEND'), $statut)) {
                    $teletransmission['statut_envoi'] = Atexo_Config::getParameter('TRANSMISSION_SEND');
                }
                //print_r($teletransmission);exit;
            }
            $infos[] = $teletransmission;
        }
        //print_r($infos);exit;
        $this->repeaterTeleTransmission->DataSource = $infos;
        $this->repeaterTeleTransmission->DataBind();
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Helios_CriteriaVo $criteriaVo)
    {
        $this->searchTeletransmition($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $nouveauStatutTeletransmission = null;
        $criteriaVo = $this->getViewState('CriteriaVo', new Atexo_Helios_CriteriaVo());
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                $this->firstDiv->setCssClass('tab-on');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
            //$nouveauStatutTeletransmission='0';//Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE');
                break;
            case 'secondTab':
                $this->firstDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab-on');
                $this->thirdDiv->setCssClass('tab');
                $nouveauStatutTeletransmission = '1'; //"En cours de traitement";
                break;
            case 'thirdTab':
                $this->firstDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab-on');
                $nouveauStatutTeletransmission = '2'; //"Acheve";
                break;

            default:
                $nouveauStatutTeletransmission = '';
                break;
        }
        $this->advancedSearch->setVisible(false);
        $this->resultSearch->setVisible(true);
        //$criteriaVo->setSecondConditionOnEtatTeletransmission($nouveauStatutTeletransmission);
        $criteriaVo->setStatutEnvoi($nouveauStatutTeletransmission);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $this->advancedSearch->setVisible(false);
        $this->resultSearch->setVisible(true);
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->repeaterTeleTransmission->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        $this->advancedSearch->setVisible(false);
        $this->resultSearch->setVisible(true);
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterTeleTransmission->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->repeaterTeleTransmission->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->repeaterTeleTransmission->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
        }

        $this->advancedSearch->setVisible(false);
        $this->resultSearch->setVisible(true);
        $this->repeaterTeleTransmission->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterTeleTransmission->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterTeleTransmission->PageSize);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->repeaterTeleTransmission->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function retreiveTeletransmissionLot($idTeletransmission)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionLotPeer::ID_TELETRANSMISSION, $idTeletransmission);
        $c->add(CommonHeliosTeletransmissionLotPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $teletransmissionLot = CommonHeliosTeletransmissionLotPeer::doSelect($c, $connexionOrg);
        if ($teletransmissionLot) {
            return $teletransmissionLot;
        } else {
            return [];
        }
    }

    public function deleteTeletransmission($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idTrans = $param->CommandName;
        if ($idTrans) {
            CommonHeliosTeletransmissionPeer::doDelete([$idTrans, Atexo_CurrentUser::getCurrentOrganism()], $connexion);
        }
        $criteriaVo = $this->getViewState('CriteriaVo');
        self::setData($criteriaVo);
        $this->searchTeletransmition($criteriaVo);
    }

    public function truncate($texte)
    {
        $arrayText = explode(';', $texte);
        if (strlen($texte) > 4) {
            return $arrayText[0];
        } else {
            return $texte;
        }
    }

    public function isTruncated($texte)
    {
        return (strlen($texte) > 4) ? '' : 'display:none';
    }

    public function editTeletransmission($sender, $param)
    {
        $id = $param->CommandName;
        $this->response->redirect('index.php?page=helios.HeliosCreateTeletransmission&idTrans='.$id);
    }
}
