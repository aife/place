<?php

namespace Application\Pages\Helios;

use Application\Controls\EntrepriseDownloadDce;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonHeliosCosignature;
use Application\Propel\Mpe\CommonHeliosCosignaturePeer;
use Application\Propel\Mpe\CommonHeliosTeletransmission;
use Application\Propel\Mpe\CommonHeliosTeletransmissionLot;
use Application\Propel\Mpe\CommonHeliosTeletransmissionLotPeer;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Helios\Atexo_Helios_PiecesAttributaires;
use Application\Service\Atexo\Helios\Atexo_Helios_Signature;
use Application\Service\Atexo\Helios\Atexo_Helios_Teletransmission;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use Prado\Prado;

/**
 * Page de creation des teletransmission.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class HeliosCreateTeletransmission extends MpeTPage
{
    protected $_consultation = '';
    protected $_lastTrans = '';
    protected $_organisme = '';
    protected $_transLot = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agenthelios');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
        if (isset($_GET['id']) && (0 == strcmp($_GET['id'], $_GET['id']))) {
            //  Creer unr nouvelle transmission pour la consultation
            $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            if (!$this->IsPostBack) {
                $this->createTransmission();
            }
        } elseif (isset($_GET['idTrans']) && ('' != $_GET['idTrans'])) {
            // recherche de la transmission de l'id passer en GET
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->_lastTrans = CommonHeliosTeletransmissionPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idTrans']), $this->_organisme, $connexion);
            $this->_consultation = $this->retrieveConsultation($this->_lastTrans->getConsultationId());
            $this->nuremoTrans->Text = $this->_lastTrans->getShownId();
        }
        if ($this->_consultation instanceof CommonConsultation) {
            $this->IdConsultationSummary->setConsultation($this->_consultation);
        }
        if (!$this->IsPostBack) {
            $this->displayTransmission();
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme($this->_organisme);
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $critere->setCalledFromHelios(true);
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function createTransmission()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $newTrans = new CommonHeliosTeletransmission();
        $newTrans->setConsultationId($this->_consultation->getId());
        $newTrans->setOrganisme($this->_organisme);
        $newTrans->setRefUtilisateur($this->_consultation->getReferenceUtilisateur());
        $newTrans->setDateCreation(date('Y-m-d H:i'));
        $newTrans->setIdAgentCreation(Atexo_CurrentUser::getIdAgentConnected());

        //Ajout des pieces deja lier a la consultation
        $piecesPub = (new Atexo_Helios_Teletransmission())->reteivePiecesPubliciteConsByrefCons($this->_consultation->getId());
        if ('' != $piecesPub) {
            $idsPiecesPub = '';
            foreach ($piecesPub as $onePiece) {
                $idsPiecesPub .= $onePiece->getId().';';
            }
            $newTrans->setPubliciteCons($idsPiecesPub);
        }

        //Ajout des pieces de PV de CAO / PV de Commission
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesPvConsultationByrefCons($this->_consultation->getId());
        if ('' != $pieces) {
            $idsPieces = '';
            foreach ($pieces as $onePiece) {
                $idsPieces .= $onePiece->getId().';';
            }
            $newTrans->setIdsPvsCao($idsPieces);
        }

        //Ajout des pieces de Rapport de presentation au prefet (ART. 75)
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesRapportByRefCons($this->_consultation->getId());
        if ('' != $pieces) {
            $idsPieces = '';
            foreach ($pieces as $onePiece) {
                $idsPieces .= $onePiece->getId().';';
            }
            $newTrans->setIdsRapportsPrefet($idsPieces);
        }

        //Ajout des pieces de Tableau de reception des AR
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesArByrefCons($this->_consultation->getId());
        if ('' != $pieces) {
            $idsPieces = '';
            foreach ($pieces as $onePiece) {
                $idsPieces .= $onePiece->getId().';';
            }
            $newTrans->setTableauAr($idsPieces);
        }
        $newTrans->save($connexion);
        $this->_lastTrans = $newTrans;
        (new Atexo_Helios_Teletransmission())->buildFormatedTransmissionId($newTrans->getId());
        $this->response->redirect('?page=helios.HeliosCreateTeletransmission&fromResultCons&idTrans='.$newTrans->getId());
    }

    /**
     * afficher la teletransmission s'il existe deja une.
     */
    public function displayTransmission()
    {
        $this->objet->Text = $this->_lastTrans->getObjetLibre();
        $this->commentaire->Text = $this->_lastTrans->getCommentaireLibre();
        if ('' != $this->_lastTrans->getPubliciteCons()) {
            $this->piecePublicite->Checked = true;
        } else {
            $this->piecePublicite->Checked = false;
        }
        $dateMiseEnLigne = Atexo_Util::iso2frnDateTime(Atexo_Consultation::getDateMiseEnLigne($this->_consultation));
        //Publicite consultation
        $this->dateMiseEnLigne->Text = ('' != $dateMiseEnLigne) ? $dateMiseEnLigne : '-';
        $this->displayPublicationBoamp();
        $this->displayPublicationJal();
        $this->displayFichierAnnonces();
        $this->displayPiecesDce();
        $this->displayPiecesPv();
        $this->displayPiecesRapport();
        $this->displayPiecesAr();
        $this->displayPiecesOptionnelles();
        $this->displayPieceEntreprise();
    }

    public function displayLayer2()
    {
        $this->displayPiecesPv();
        $this->displayPiecesRapport();
        $this->displayPiecesAr();
        $this->displayPiecesOptionnelles();
        $this->script->Text = '<script language="javascript">showHideLayer(\'ongletLayer2\',this,3);</script>';
    }

    /**
     * Publication au boamp.
     */
    public function displayPublicationBoamp()
    {
        $annancesBoamp = (new Atexo_Publicite_Destinataire())->retreiveAnnoncePublieWithTypeForm($this->_consultation->getId(), $this->_organisme);
        if (!$annancesBoamp) {
            $this->annonceBoamp->Text = Prado::localize('TEXT_AUCUNE_ANNONCE_BOAMP');
        } else {
            $this->repeaterAnnonceBoamp->DataSource = $annancesBoamp;
            $this->repeaterAnnonceBoamp->dataBind();
        }
    }

    public function displayPublicationJal()
    {
        $annancesJal = (new Atexo_Publicite_DestinataireJAL())->retreiveSendAnnonceJal($this->_consultation->getId());
        if (!$annancesJal) {
            $this->annonceJal->Text = Prado::localize('TEXT_AUCUNE_TRANSMISSION');
        } else {
            $this->repeaterAnnonceJal->DataSource = $annancesJal;
            $this->dataBind();
        }
    }

    /**
     *  Fichier d'annonce/avis mis sur la plate-forme.
     */
    public function displayFichierAnnonces()
    {
        $piecesPub = (new Atexo_Helios_Teletransmission())->reteivePiecesPubliciteConsByIds($this->_lastTrans->getPubliciteCons());
        //print_r($piecesPub);
        if ((is_countable($piecesPub) ? count($piecesPub) : 0) > 0) {
            $this->labelPiecePub->setVisible(false);
        } else {
            $this->labelPiecePub->setVisible(true);
        }
        $this->repeaterPiecesPub->DataSource = $piecesPub;
        $this->repeaterPiecesPub->dataBind();
    }

    public function deletePiecePublicite($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idPiecePub = $param->CommandName;
        if ($idPiecePub) {
            $arrayIds = explode(';', $this->_lastTrans->getPubliciteCons());
            if (is_array($arrayIds) && (in_array($idPiecePub, $arrayIds))) {
                $newIdsPiecesPub = '';
                foreach ($arrayIds as $oneId) {
                    if ($idPiecePub != $oneId && '' != $oneId) {
                        $newIdsPiecesPub .= $oneId.';';
                    }
                }
                $this->_lastTrans->setPubliciteCons($newIdsPiecesPub);
                $this->_lastTrans->save($connexion);
            }
        }
    }

    /**
     * affiche l'arboressance du DCE.
     */
    public function displayPiecesDce()
    {
        $downloadDce = new EntrepriseDownloadDce();
        $downloadDce->setReference($this->_consultation->getId());
        $downloadDce->setOrganisme($this->_organisme);
        $dceItems = explode(';', $this->_lastTrans->getDceitems());
        $scriptDce = $downloadDce->getArborescence($dceItems);
        if ('' != $scriptDce) {
            $this->labelDce->Text = Prado::localize('TEXT_SELECTIONNER_LES_PIECES_DCE').' : ';
            $this->scriptDce->Text = $scriptDce;
        } else {
            $this->linkdownloadDce->setVisible(false);
            $this->labelDce->Text = Prado::localize('TEXT_AUCUNE_PIECES_DCE_TRANSMETTRE');
        }
    }

    public function selectedFilesForDce()
    {
        $downloadDce = new EntrepriseDownloadDce();
        $downloadDce->setReference($this->_consultation->getId());
        $downloadDce->setOrganisme($this->_organisme);
        $dce = $downloadDce->getDce($this->_organisme);
        if ($dce) {
            $arrayIndexSelectedFiles = Atexo_Files::getArrayIndexSelectedFiles($dce);
            if ($arrayIndexSelectedFiles) {
                return $arrayIndexSelectedFiles;
            }
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des index des fichiers selectionnes du DCE.
     */
    public function downloadSelectedFilesForDce($sender, $param)
    {
        $arrayIndexSelectedFiles = $this->selectedFilesForDce();
        if ($arrayIndexSelectedFiles) {
            (new Atexo_Files())->downloadPartialConsFile($this->_consultation->getId(), $this->_organisme);
        }
    }

    /**
     * Affichier tableau des pieces de PV.
     */
    public function displayPiecesPv()
    {
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesPvConsultationByIds($this->_lastTrans->getIdsPvsCao());
        if ((is_countable($pieces) ? count($pieces) : 0) > 0) {
            $this->labelPiecesPv->setVisible(false);
        } else {
            $this->labelPiecesPv->setVisible(true);
        }
        $this->repeaterPiecesPv->DataSource = $pieces;
        $this->repeaterPiecesPv->dataBind();
    }

    public function deletePiecePvConsultation($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idPiece = $param->CommandName;
        if ($idPiece) {
            $arrayIds = explode(';', $this->_lastTrans->getIdsPvsCao());
            if (is_array($arrayIds) && (in_array($idPiece, $arrayIds))) {
                $newIds = '';
                foreach ($arrayIds as $oneId) {
                    if ($idPiece != $oneId && '' != $oneId) {
                        $newIds .= $oneId.';';
                    }
                }
                $this->_lastTrans->setIdsPvsCao($newIds);
                $this->_lastTrans->save($connexion);
            }
        }
    }

    /**
     * Affichier tableau des pieces du rapport.
     */
    public function displayPiecesRapport()
    {
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesRapportByIds($this->_lastTrans->getIdsRapportsPrefet());
        if ((is_countable($pieces) ? count($pieces) : 0) > 0) {
            $this->labelPiecesRapport->setVisible(false);
        } else {
            $this->labelPiecesRapport->setVisible(true);
        }
        $this->repeaterPiecesRapport->DataSource = $pieces;
        $this->repeaterPiecesRapport->dataBind();
    }

    public function deletePieceRapport($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idPiece = $param->CommandName;
        if ($idPiece) {
            $arrayIds = explode(';', $this->_lastTrans->getIdsRapportsPrefet());
            if (is_array($arrayIds) && (in_array($idPiece, $arrayIds))) {
                $newIds = '';
                foreach ($arrayIds as $oneId) {
                    if ($idPiece != $oneId && '' != $oneId) {
                        $newIds .= $oneId.';';
                    }
                }
                $this->_lastTrans->setIdsRapportsPrefet($newIds);
                $this->_lastTrans->save($connexion);
            }
        }
    }

    /**
     * Affichier tableau des pieces du rapport.
     */
    public function displayPiecesAr()
    {
        $pieces = (new Atexo_Helios_Teletransmission())->reteivePiecesArByIds($this->_lastTrans->getTableauAr());
        if (1 == (is_countable($pieces) ? count($pieces) : 0)) {
            $this->linkTableauAr->setStyle('display:none');
        } else {
            $this->linkTableauAr->setStyle('display:block');
        }
        $this->repeaterPiecesAr->DataSource = $pieces;
        $this->repeaterPiecesAr->dataBind();
    }

    public function deletePieceAr($sender, $param)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idPiece = $param->CommandName;
        if ($idPiece) {
            $arrayIds = explode(';', $this->_lastTrans->getTableauAr());
            if (is_array($arrayIds) && (in_array($idPiece, $arrayIds))) {
                $newIds = '';
                foreach ($arrayIds as $oneId) {
                    if ($idPiece != $oneId && '' != $oneId) {
                        $newIds .= $oneId.';';
                    }
                }
                $this->_lastTrans->setTableauAr($newIds);
                $this->_lastTrans->save($connexionOrg);
            }
        }
    }

    /**
     * Affichier tableau des pieces optionnelles.
     */
    public function displayPiecesOptionnelles()
    {
        $pieces = [];
        if ($this->_lastTrans instanceof CommonHeliosTeletransmission) {
            $pieces = [];
            if ('' != $this->_lastTrans->getNomPiecej1()) {
                $pieces[0]['id'] = $this->_lastTrans->getId();
                $pieces[0]['nom_piecej'] = $this->_lastTrans->getNomPiecej1();
                $pieces[0]['taille_piecej'] = $this->_lastTrans->getTaillePiecej1();
                $pieces[0]['desc_piecej'] = $this->_lastTrans->getPiecej1Desc();
                $pieces[0]['piece'] = '1';
            }
            if ('' != $this->_lastTrans->getNomPiecej2()) {
                $pieces[1]['id'] = $this->_lastTrans->getId();
                $pieces[1]['nom_piecej'] = $this->_lastTrans->getNomPiecej2();
                $pieces[1]['taille_piecej'] = $this->_lastTrans->getTaillePiecej2();
                $pieces[1]['desc_piecej'] = $this->_lastTrans->getPiecej2Desc();
                $pieces[1]['piece'] = '2';
            }
        }
        if ((is_countable($pieces) ? count($pieces) : 0) >= 1) {
            $this->labelPiecesOptionnelles->setVisible(false);
        } else {
            $this->labelPiecesOptionnelles->setVisible(true);
            //$this->labelPiecesOptionnelles->setText(Prado::localize('TEXT_AUCUN_DOCUMENT_AJOUTER'));
        }
        $this->repeaterPiecesOptionnelles->DataSource = $pieces;
        $this->repeaterPiecesOptionnelles->dataBind();
    }

    public function deletePieceOptionnelle($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = $param->CommandName;
        if ('' != $piece) {
            if ('1' == $piece) {
                $this->_lastTrans->setPiecej1('');
                $this->_lastTrans->setNomPiecej1('');
                $this->_lastTrans->setTaillePiecej1('0');
                $this->_lastTrans->setPiecej1Desc('');
            } elseif ('2' == $piece) {
                $this->_lastTrans->setPiecej2('');
                $this->_lastTrans->setNomPiecej2('');
                $this->_lastTrans->setTaillePiecej2('0');
                $this->_lastTrans->setPiecej2Desc('');
            }
            $this->_lastTrans->save($connexion);
        }
        $this->displayTransmission();
    }

    public function displayPieceEntreprise()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $lots = $this->_consultation->getAllLots();
        if (0 == (is_countable($lots) ? count($lots) : 0)) {
            $lots = [];
            $lot = new CommonCategorieLot();
            $lot->setConsultationId($this->_consultation->getId());
            $lot->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $lot->setLot('0');
            $lot->setDescription($this->_consultation->getIntitule());
            $status = ($this->_consultation->getIdEtatConsultation() < Atexo_Config::getParameter('STATUS_DECISION')) ? '0' : '1';
            $lot->setDecision($status);
            $lots[] = $lot;
        }
        foreach ($lots as $lot) {
            $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($lot->getConsultationId(), $lot->getLot(), $this->_organisme);
            if ($decisionLot instanceof CommonDecisionLot) {
                $lot->setDateDecision($decisionLot->getDateDecision());
            }
            $attibutaires = (new Atexo_Consultation_Decision())->retrieveAttributaireInformation($this->_organisme, $this->_consultation->getId(), $lot->getLot());
            $lot->setAttributaires($attibutaires);
            $transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $this->_organisme, $lot->getLot(), $connexion);
            if (!($transLot instanceof CommonHeliosTeletransmissionLot)) {
                $transLot = new CommonHeliosTeletransmissionLot();
                $transLot->setIdTeletransmission($this->_lastTrans->getId());
                $transLot->setOrganisme($this->_organisme);
                $transLot->setLot($lot->getLot());
                $transLot->save($connexion);
            }
        }
        $this->PiecesEntreprises->DataSource = $lots;
        $this->PiecesEntreprises->DataBind();
        $this->displayPiecesEle();
    }

    public function getDateAttributionLot($lot)
    {
        if ('0' == $lot->getDecision()) {
            return Prado::localize('TEXT_CE_LOT_A_DES_REPONSE_NON_TRAITE');
        } else {
            if ($lot->getDateDecision()) {
                return Atexo_Util::iso2frnDate($lot->getDateDecision());
            } else {
                if ($lot->hasAttributaires()) {
                    return 'N/A';
                } else {
                    return Prado::localize('TEXT_CE_LOT_NA_PAS_ATTRIBUTAURES');
                }
            }
        }
    }

    /**
     * La fonction qui affiche l'arbo du zip de l'envoloppe d'offre.
     */
    public function getArborescenceEnveloppe($lot, $idBlob, $idFile)
    {
        $ths = null;
        $profondeurCourante = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $ths->_transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $organisme, $lot, $connexion);
        $arEnvItem = [];
        if ($ths->_transLot instanceof CommonHeliosTeletransmissionLot) {
            $IdsEnveloppesItems = $ths->_transLot->getIdsEnveloppesItems();
            if ($IdsEnveloppesItems) {
                $IdsEnveloppesItems = explode(';', $IdsEnveloppesItems);
                $arEnvItem = [];
                foreach ($IdsEnveloppesItems as $oneEnv) {
                    $arIdEnveloppe = explode('_', $oneEnv);
                    if ($arIdEnveloppe['0'] == $idFile) {
                        $arEnvItem[] = str_replace('_', '', strstr($oneEnv, '_'));
                    }
                }
            }
        }
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $organisme);
        $blob_file = $blobResource['blob']['pointer'];
        //echo $blob_file;exit;
        $result_affiche = ' ';
        if ($blob_file) {
            $ordered_res = Atexo_Zip::getOrderedFilesForZip($blob_file, false);
            $maxIndex = (new Atexo_Zip())->getMaxIndexZip($blob_file, false);
            $arrayProfondeur = [0];
            if (is_array($ordered_res)) {
                $result_affiche .= '<ul>';
                $ordre = '';
                $key = '';
                foreach ($ordered_res as $ordre => $directory_files) {
                    foreach ($directory_files as $key => $directory_one_file) {
                        $profondeurCourante = $directory_one_file['profondeur'];

                        if ($profondeurCourante) {
                            if ($profondeurCourante > $arrayProfondeur[0]) {
                                $result_affiche .= '<ul id="sous-cat_0" style="display:block">';
                                array_unshift($arrayProfondeur, $profondeurCourante);
                            } elseif ($profondeurCourante == $arrayProfondeur[0]) {
                                $result_affiche .= '</li>';
                            } else {
                                foreach ($arrayProfondeur as $index => $value) {
                                    if ($value && $index) {
                                        $result_affiche .= '</li></ul></li>';
                                    }
                                    if ($profondeurCourante == $value) {
                                        break;
                                    }
                                }
                                $arrayProfondeur = [0];
                                array_unshift($arrayProfondeur, $profondeurCourante);
                                $result_affiche .= '<li id="sous-cat_0" style="display:block">';
                            }
                        } elseif ($arrayProfondeur[0] > 0) {
                            for ($i = 0; $i < $arrayProfondeur[0]; ++$i) {
                                $result_affiche .= '</ul>';
                            }
                            $arrayProfondeur = [0];
                        }

                        $val_check_box = $directory_one_file['base'];
                        $checked = '';
                        // cocher les fichiers deja choisi dans l'enveloppe
                        if (in_array($directory_one_file['index'], $arEnvItem)) {
                            $checked = 'checked=\"checked\"';
                        }
                        $result_affiche .= '<li>'.(('folder' == $directory_one_file['type']) ? '<strong>' : '').'<span class="check-bloc"><input type="checkbox" name="lot_'.$lot.'_env_item_'.$idBlob.'_'.$directory_one_file['index'].'"  id="lot_'.$lot.'_env_item_'.$idBlob.'_'.$directory_one_file['index']
                            .'" '.$checked.' value="'.$val_check_box."\" onClick=\"javascript:CheckUnCheckHeliosEnvItems(this.id, '".$maxIndex."','$lot','$idBlob')\"   />\n";

                        $result_affiche .= '</span><label class="content-bloc bloc-580" for="lot_'.$lot.'_env_item_'.$idBlob.'_'.$directory_one_file['index'].'">';

                        if ($directory_one_file['taille']) {
                            $result_affiche .= $directory_one_file['name'].'&nbsp;'.'( '.$directory_one_file['taille'].' )';
                        } else {
                            $result_affiche .= $directory_one_file['name'].'&nbsp;'.$directory_one_file['taille'];
                        }
                        $result_affiche .= '</label>'.(('folder' == $directory_one_file['type']) ? '</strong>' : '');
                    }
                }
                array_unshift($arrayProfondeur, $profondeurCourante);
                foreach ($arrayProfondeur as $value) {
                    if ($value) {
                        $result_affiche .= '</li></ul>';
                    }
                }
                $result_affiche .= '</li></ul>';
            }
        }

        return $result_affiche;
    }

    /**
     * retourn true si l'enveloppe est un fichier zip.
     */
    public function isZipEnveloppe($lot, $idBlob)
    {
        $ths = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $ths->_transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), Atexo_CurrentUser::getCurrentOrganism(), $lot, $connexion);
        $arEnvItem = [];
        if ($ths->_transLot instanceof CommonHeliosTeletransmissionLot) {
            $arEnvItem = explode(';', $ths->_transLot->getIdsEnveloppesItems());
        }
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, Atexo_CurrentUser::getCurrentOrganism());
        $blob_file = $blobResource['blob']['pointer'];
        if ((new Atexo_Zip())->verifyZipIntegrity($blob_file)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retroune un tableau des index des fichiers enregistres dans le ZIP.
     *
     * @param string chemin du dosser ZIP
     */
    public static function getArrayIndexSelectedFilesEnv($lot, $env, $idBlob)
    {
        $arrayIndexFiles = [];
        $zip = zip_open($env);
        if ($zip) {
            $index = 0;
            $zip_entry = '';
            while ($zip_entry = zip_read($zip)) {
                ++$index;
                if ($_POST['lot_'.$lot.'_env_item_'.$idBlob.'_'.$index]) {
                    $arrayIndexFiles[] = $index;
                }
            }
        }

        return $arrayIndexFiles;
    }

    public function selectedFilesForEnveloppe($lot, $idBlob)
    {
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, Atexo_CurrentUser::getCurrentOrganism());
        $blob_file = $blobResource['blob']['pointer'];
        if ($blob_file) {
            return self::getArrayIndexSelectedFilesEnv($lot, $blob_file, $idBlob);
        }
    }

    public function doAddCoSignature($sender, $param)
    {
        $params = explode('#_#', $param->CommandName);
        $nomActe = $params[0];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->PiecesEntreprises->getItems() as $oneItem) {
            foreach ($oneItem->PieceAttibutaires->getItems() as $oneItemPieceAttributaire) {
                foreach ($oneItemPieceAttributaire->repeaterPiecesEle->getItems() as $oneItemPiecesEle) {
                    $wishOne = $oneItemPiecesEle->wishOne->Text;
                    $idFichier = $oneItemPiecesEle->idFichier->Value;
                    if ('1' == $wishOne) {
                        $signature = $oneItemPiecesEle->signature->Text;
                        $tmpDir = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'/';
                        @mkdir($tmpDir, 0700);
                        $nameZipFile = Atexo_Config::getParameter('NAME_ZIP_COSIGNATURE_ACTE');
                        $pathZipFile = $tmpDir.$nameZipFile;
                        // recuperation des autres signature de l'acte d'engagament (autre que celle de l'entreprise)
                        $consign = CommonHeliosCosignaturePeer::retrieveByPK($idFichier, $this->_organisme, $connexion);
                        if ($consign instanceof CommonHeliosCosignature && '' != $consign->getIdBlob()) {// Il y a deja des cosignatures
                            $blobResource = Atexo_Blob::acquire_lock_on_blob($consign->getIdBlob(), $this->_organisme);
                            Atexo_Util::write_file($pathZipFile, file_get_contents($blobResource['blob']['pointer']));
                            $lastSigIndex = (new Atexo_Helios_Signature())->getLastSignatureIndex($pathZipFile, $nomActe);
                            $nomFichierSig = $nomActe.' - Signature '.$lastSigIndex.'.p7s';
                            Atexo_Util::write_file($tmpDir.$nomFichierSig, $signature);
                            //echo $lastSigIndex.'----'.$pathZipFile;exit;
                            $systemZipCmd = "cd $tmpDir; zip  ".escapeshellarg($nameZipFile).' '.escapeshellarg($nomFichierSig).' &> '.TMP_DIR.'zip_error';
                            system($systemZipCmd, $sysAnswer);
                            if (0 != $sysAnswer) {
                                return false;
                            }
                        } else { // aucune co-signature n'existe
                            $consign = new CommonHeliosCosignature();
                            $consign->setIdEnveloppe($idFichier);
                            $consign->setOrganisme($this->_organisme);
                            $nomFichierSig = $nomActe.' - Signature 2.p7s'; // le 1 est reserve a la signature de l'entreprise
                            Atexo_Util::write_file($tmpDir.$nomFichierSig, $signature);
                            $pathZipFile = (new Atexo_Files())->zip_file($tmpDir.$nomFichierSig, $pathZipFile);
                        }
                        $atexoBlob = new Atexo_Blob();
                        $atexoCrypto = new Atexo_Crypto();
                        //  echo '----'.$pathZipFile;exit;
                        $arrayTimeStampAvis = $atexoCrypto->timeStampFile($pathZipFile);
                        if (is_array($arrayTimeStampAvis)) {
                            $idBlob = $atexoBlob->insert_blob($nameZipFile, $pathZipFile, $this->_organisme);
                            $consign->setIdFichier($idFichier);
                            $consign->setIdBlob($idBlob);
                            $consign->setSignatureActe($nameZipFile);
                            //$transLot->setTailleActe(filesize($pathZipFile));
                            $consign->setHorodatageActe($arrayTimeStampAvis['horodatage']);
                            $consign->setUntrusteddateActe($arrayTimeStampAvis['untrustedDate']);
                        }
                        $consign->save($connexion);
                        @unlink($pathZipFile);
                        Atexo_Util::removeDir($tmpDir);
                    }
                }
            }
        }
    }

    /**
     * refrech du repeater des pieces entreprise.
     */
    public function onCallBackRefreshPieceEntreprise($sender, $param)
    {
        $this->displayPieceEntreprise();
        $this->displayPiecesEle();
        // Re-affichage du TActivePanel
        // $this->panelPiecesEntreprise->render($param->getNewWriter());
        foreach ($this->PiecesEntreprises->getItems() as $oneItem) {
            foreach ($oneItem->PieceAttibutaires->getItems() as $oneItemPieceAttributaire) {
                foreach ($oneItemPieceAttributaire->repeaterPiecesEle->getItems() as $oneItemPiecesEle) {
                    // echo "test";exit;
                    $oneItemPiecesEle->panelPeiceEle->render($param->getNewWriter());
                }
            }
        }
        $this->refreshComposant($sender, $param);
        //$this->panelPiecesEntreprise->render($param->getNewWriter());
    }

    /*
     * retourne la liste des cosignatures d'un acte d'engagement
     *
     */
    public function displayCoSignatureAr($idFicher)
    {
        $cosig = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $cosign = CommonHeliosCosignaturePeer::retrieveByPK($idFicher, $this->_organisme, $connexion);
        if (($cosign instanceof CommonHeliosCosignature) && ('' != $cosign->getIdBlob())) {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($cosign->getIdBlob(), $this->_organisme);
            $cosignaturesTmpFile = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'_cosignatures.zip';
            Atexo_Util::write_file($cosignaturesTmpFile, file_get_contents($blobResource['blob']['pointer']));
            $zip = zip_open($cosignaturesTmpFile);
            if ($zip) {
                while ($zip_entry = zip_read($zip)) {
                    if (zip_entry_open($zip, $zip_entry, 'r')) {
                        $signature = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        $cosig[] = (new Atexo_Crypto_Certificat())->getCnSubject($signature);
                        zip_entry_close($zip_entry);
                    }
                }
            }
            zip_close($zip);
            @unlink($cosignaturesTmpFile);

            return $cosig;
        }
    }

    /**
     * retourne true si le lot de la transmission a un acte d'engagement.
     */
    public function checkedActeEngagement($idFichier, $lot)
    {
        if ('' == $lot) {
            return false;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $this->_organisme, $lot, $connexion);
        if (($transLot instanceof CommonHeliosTeletransmissionLot) && in_array($idFichier, explode(';', $transLot->getIdsEnvActesEngagements()))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retourne true si le lot de la transmission a un enveloppe d'offre.
     */
    public function checkedFileEnveloppe($idFichier, $lot)
    {
        if ('' == $lot) {
            return false;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $this->_organisme, $lot, $connexion);
        if (($transLot instanceof CommonHeliosTeletransmissionLot) && (in_array($idFichier, explode(';', $transLot->getIdsEnveloppesItems())))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retourne true si le lot de la transmission a un acte d'engagement.
     */
    public function checkedLot($lot)
    {
        if ('' == $lot) {
            return false;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $this->_organisme, $lot, $connexion);
        if (($transLot instanceof CommonHeliosTeletransmissionLot) && ('1' == $transLot->getLotInTransmission())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * enregistrer la teletransmission.
     */
    public function actionTransmission($transmettre = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($transmettre) {
            $this->_lastTrans->setIdAgentEnvoi(Atexo_CurrentUser::getIdAgentConnected());
            $this->_lastTrans->setDateEnvoi(date('Y-m-d H:i'));
        }
        $this->_lastTrans->setShownId($this->nuremoTrans->Text);
        $this->_lastTrans->setObjetLibre($this->objet->Text);
        $this->_lastTrans->setCommentaireLibre($this->commentaire->Text);
        $arrayIndexSelectedFiles = $this->selectedFilesForDce();
        if ($arrayIndexSelectedFiles) {
            $indexSelectedFiles = implode(';', $arrayIndexSelectedFiles);
            $this->_lastTrans->setDceItems($indexSelectedFiles);
        }
        $this->_lastTrans->save($connexion);
        $this->actionTransmissionLot($transmettre, $this->_organisme);
    }

    /**
     * Enregistement dans la table helios_teletransmission_lot.
     */
    public function actionTransmissionLot($transmettre, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->PiecesEntreprises->getItems() as $oneItem) {
            $lot = $oneItem->lot->Value;
            $this->_transLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), $org, $lot, $connexion);
            if (true == $oneItem->choixLot->checked) {
                // Debut Bloc Attributaire
                $selectedEnv = '';
                $selectedAec = '';
                $numeroMarche = '';
                foreach ($oneItem->PieceAttibutaires->getItems() as $oneItemPieceAttributaire) {
                    if ($oneItemPieceAttributaire->checkNumMarche->checked) {
                        $numeroMarche .= $oneItemPieceAttributaire->numeroMarche->Value.'; ';
                    }
                    //Pieces electroniques des attributaires
                    foreach ($oneItemPieceAttributaire->repeaterPiecesEle->getItems() as $oneItemPiecesEle) {
                        $idFichier = $oneItemPiecesEle->idFichier->Value;
                        $typeFichier = $oneItemPiecesEle->typeFichier->Value;
                        $nameFichier = $oneItemPiecesEle->nameFichier->Value;
                        $idBlob = $oneItemPiecesEle->idBlob->Value;
                        $idFichier = $oneItemPiecesEle->idFichier->Value;
                        if ('ACE' == $typeFichier) {
                            if ($oneItemPiecesEle->checkPieceEnv->checked) {
                                $selectedAec .= $idFichier.';';
                            //$ths->_transLot->setIdsEnvActesEngagements($idFichier);
                            } else {
                                $selectedAec = str_replace($idFichier, '', $selectedAec);
                                //$ths->_transLot->setIdsEnvActesEngagements('');
                            }
                        } elseif ('PRI' == $typeFichier) {
                            if ((new Atexo_Zip())->verifyZipExtension($nameFichier)) { // Si fichier zip
                                $selectedIndex = $this->selectedFilesForEnveloppe($lot, $idBlob);
                                if ($selectedIndex) {
                                    $envListIndex = '';
                                    foreach ($selectedIndex as $oneIndex) {
                                        $envListIndex .= $idFichier.'_'.$oneIndex.';';
                                    }
                                    $selectedEnv .= $envListIndex;
                                    // $ths->_transLot->setIdsEnveloppesItems(implode(';',$selectedIndex));
                                }
                            } else {
                                if ($oneItemPiecesEle->checkFileEnv->checked) {
                                    $selectedEnv .= $idFichier.';';
                                } else {
                                    $selectedEnv = str_replace($idFichier, '', $selectedEnv);
                                }
                            }
                        }
                    }
                }
                if ($transmettre) {
                    $this->_transLot->setStatutEnvoi(Atexo_Config::getParameter('TRANSMISSION_SEND'));
                }//echo $selectedEnv.'---------'.$selectedAec;exit;
                $this->_transLot->setNumeroMarche($numeroMarche);
                $this->_transLot->setIdsEnveloppesItems($selectedEnv);
                $this->_transLot->setIdsEnvActesEngagements($selectedAec);
                $this->_transLot->setLotInTransmission('1');
                $selectedEnv = '';
                $selectedAec = '';
            } else {
                if (($this->_transLot instanceof CommonHeliosTeletransmissionLot) && $transmettre && '0' == $lot) {
                    $this->_transLot->setStatutEnvoi(Atexo_Config::getParameter('TRANSMISSION_SEND'));
                // $this->_transLot->save($connexionOrg);
                  //  print_r($this->_transLot);exit;
                } else {
                    $this->_transLot->setStatutEnvoi(Atexo_Config::getParameter('TRANSMISSION_SAVE'));
                }
                $this->_transLot->setLotInTransmission('0');
            }
            $this->_transLot->save($connexion);
        }//exit;
    }

    /**
     * fonction de teletransmission de  la transmission.
     */
    public function saveTransmission($sender, $param)
    {
        $this->actionTransmission(false);
        $this->response->redirect('?page=helios.RechercheTeletransmission&keyWord='.$this->_lastTrans->getShownId());
    }

    /**
     * fonction de teletransmission de  la transmission.
     */
    public function validateTransmission($sender, $param)
    {
        $this->actionTransmission(true);
        $this->response->redirect('?page=helios.RechercheTeletransmission&keyWord='.$this->_lastTrans->getShownId());
    }

    public function onCallBackRefreshPiecePub($sender, $param)
    {
        $this->displayFichierAnnonces();
        $this->panelPiecePub->render($param->getNewWriter());
        $this->refreshComposant($sender, $param);
    }

    public function onCallBackRefreshPiecePv($sender, $param)
    {
        $this->displayPiecesPv();
        $this->panelPiecePv->render($param->getNewWriter());
        $this->refreshComposant($sender, $param);
    }

    public function onCallBackRefreshPiecesRapport($sender, $param)
    {
        $this->displayPiecesRapport();
        $this->panelPiecesRapport->render($param->getNewWriter());
        $this->refreshComposant($sender, $param);
    }

    public function onCallBackRefreshPieceAr($sender, $param)
    {
        $this->displayPiecesAr();
        $this->panelPiecesAr->render($param->getNewWriter());
        $this->refreshComposant($sender, $param);
    }

    public function onCallBackRefreshPiecesOptionnelles($sender, $param)
    {
        $this->displayPiecesOptionnelles();
        $this->panelPiecesOptionnelles->render($param->getNewWriter());
        $this->refreshComposant($sender, $param);
    }

    /**
     * action boutton Annuler
     *  retoure vers la page de detail de la consultation.
     */
    public function actionBouttonAnnuler($sender, $param)
    {
        if (isset($_GET['fromResultCons'])) {
            $this->response->redirect('index.php?page=Agent.TableauDeBord&AS=1&searchAnnCons&helios');
        } else {
            $this->response->redirect('index.php?page=helios.RechercheTeletransmission&result');
        }
    }

    public function checkednumeroMarche($lot, $numMarche)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $trnasLot = CommonHeliosTeletransmissionLotPeer::retrieveByPK($this->_lastTrans->getId(), Atexo_CurrentUser::getCurrentOrganism(), $lot, $connexion);
        $arrayNumMarche = explode('; ', $trnasLot->getNumeroMarche());
        if (('' != $numMarche) && in_array($numMarche, $arrayNumMarche)) {
            return true;
        } else {
            return false;
        }
    }

    public function refreshComposant($sender, $param)
    {
        $this->displayFichierAnnonces();
        $this->displayPiecesDce();
        $this->displayPiecesPv();
        $this->displayPiecesRapport();
        $this->displayPiecesAr();
        $this->displayPiecesOptionnelles();
        $this->panelPiecePub->render($param->getNewWriter());
        $this->panelPiecesRapport->render($param->getNewWriter());
        $this->panelPiecePv->render($param->getNewWriter());
        $this->panelPiecesAr->render($param->getNewWriter());
        $this->panelPiecesOptionnelles->render($param->getNewWriter());
    }

    /**
     * Pour le telechargement partiel des pieces d'offre.
     */
    public function downloadEnveloppeOffre($sender, $param)
    {
        $u = new Atexo_Util();
        $params = explode('#', $param->CommandName);
        //print_r($params);exit;
        $idBlob = $params[1];
        $nameFile = str_replace(' ', '_', $params[2]);
        $lot = $params[3];
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, Atexo_CurrentUser::getCurrentOrganism());
        $blob_file = $blobResource['blob']['pointer'];

        $env_tmp_file = Atexo_Config::getParameter('COMMON_TMP').session_id().date('Ymd').'_enveloppe.zip';
        $dirFiles = Atexo_Config::getParameter('COMMON_TMP').session_id().date('Ymd').'_enveloppe';
        @unlink($env_tmp_file);
        $u->write_file($env_tmp_file, file_get_contents($blob_file));
        if (is_dir($dirFiles)) {
            $u->deleteDir($dirFiles);
        }
        $u->createDirectory($dirFiles);

        if (strchr(strtoupper($nameFile), '.ZIP')) {
            $zip = zip_open($env_tmp_file);
            $listeIndex = static::getArrayIndexSelectedFilesEnv($lot, $blob_file, $idBlob);
            if (!$listeIndex) {
                return [];
            }
            if ($zip) {
                $index = 1;
                while ($zip_entry = zip_read($zip)) {
                    if (in_array($index, $listeIndex) && (zip_entry_open($zip, $zip_entry, 'r'))) {
                        $content_file = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        $file_name = str_replace('/', '_', zip_entry_name($zip_entry));
                        $file_name = str_replace(' ', '_', zip_entry_name($zip_entry));
                        $u->write_file($dirFiles.'/'.$file_name, $content_file);
                        //return;
                    }
                    ++$index;
                }
                $cmd = 'cd '.$dirFiles.' ;  zip -r '.escapeshellarg($nameFile).' *';
                system($cmd.' 2>&1 > '.Atexo_Config::getParameter('COMMON_TMP').'zip.log', $sys_answer);
                if ('0' != $sys_answer) {
                    return;
                }
                //  echo $cmd;exit;
                ///////////
                header('Pragma: public');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private', false);
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$nameFile.'";');
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: '.filesize($dirFiles.'/'.$nameFile));

                $fp = fopen($dirFiles.'/'.$nameFile, 'rb');
                while (!feof($fp) && 0 == connection_status()) {
                    set_time_limit(0);
                    echo fread($fp, 1024 * 8);
                    flush();
                    ob_flush();
                }
                fclose($fp);
                zip_entry_close($zip_entry);
                zip_close($zip);
                @unlink($dirFiles.'/'.$nameFile);
                $u->deleteDir($dirFiles);
                @unlink($env_tmp_file);
            }
            exit;
        } else {
            $downloadFile = new DownloadFile();
            $downloadFile->downloadFiles($idBlob, $nameFile, Atexo_CurrentUser::getCurrentOrganism());
        }
    }

    public function displayPiecesEle()
    {
        foreach ($this->PiecesEntreprises->getItems() as $oneItemEntreprise) {
            foreach ($oneItemEntreprise->PieceAttibutaires->getItems() as $oneItemPieceAttributaire) {
                $dataSource = [];
                if ($oneItemPieceAttributaire->checkNumMarche->Checked) {
                    $idEnveloppe = $oneItemPieceAttributaire->idEnveloppe->Value;
                    $typeEnveloppe = $oneItemPieceAttributaire->typeEnveloppe->Value;
                    if ($typeEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                        $lot = $oneItemPieceAttributaire->lot->Value;
                        $dataSource = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributairesWithLot($idEnveloppe, $typeEnveloppe, $lot, $this->Page->_consultation->getSignatureOffre());
                        $oneItemPieceAttributaire->repeaterPiecesEle->DataSource = $dataSource;
                        $oneItemPieceAttributaire->repeaterPiecesEle->DataBind();
                    }
                }
            }
        }
    }

    public function onCallBackdisplayPiecesEle($sender, $param)
    {
        foreach ($this->PiecesEntreprises->getItems() as $oneItemEntreprise) {
            foreach ($oneItemEntreprise->PieceAttibutaires->getItems() as $oneItemPieceAttributaire) {
                $dataSource = [];
                if ($oneItemPieceAttributaire->checkNumMarche->Checked) {
                    $idEnveloppe = $oneItemPieceAttributaire->idEnveloppe->Value;
                    $typeEnveloppe = $oneItemPieceAttributaire->typeEnveloppe->Value;
                    if ($typeEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                        $lot = $oneItemPieceAttributaire->lot->Value;
                        $dataSource = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributairesWithLot($idEnveloppe, $typeEnveloppe, $lot, $this->Page->_consultation->getSignatureOffre());
                        $oneItemPieceAttributaire->repeaterPiecesEle->DataSource = $dataSource;
                        $oneItemPieceAttributaire->repeaterPiecesEle->DataBind();
                        $oneItemPieceAttributaire->panelAttributaire->render($param->getNewWriter());
                        foreach ($oneItemPieceAttributaire->repeaterPiecesEle->getItems() as $item) {
                            $item->cosignaturePanel->render($param->getNewWriter());
                        }
                    }
                }
            }
        }
        $this->refreshComposant($sender, $param);
    }

    /*
        * Permet d'avoir les parametre pour MpeChiffrementApplet
        */
    public function getParamsMpeChiffrementApplet()
    {
        $modeRestriction = 'false';
        if (Atexo_Module::isEnabled('ModeRestrictionRGS')) {
            $modeRestriction = 'true';
        }

        return ['origineContexteMetier' => 'MPE_Helios_teletransmission',
        'originePlateforme' => Atexo_Config::getParameter('ORIGINE_PLATEFORME'),
        'origineOrganisme' => Atexo_Config::getParameter('ORIGINE_ORGANISME'),
        'origineItem' => Atexo_Config::getParameter('ORIGINE_ITEM'),
        'TypeHash' => Atexo_Config::getParameter('TYPE_ALGORITHM_HASH'),
        'UrlPkcs11LibRefXml' => Atexo_Config::getParameter('URL_LIBRARY_PKCS11'),
        'MethodeJavascriptDebutAttente' => 'afficherVeuillezPatienter', 'MethodeJavascriptFinAttente' => 'masquerVeuillezPatienter',
        'MethodeJavascriptRenvoiResultat' => 'renvoiResultatHelios', 'modeRestrictionRGS' => $modeRestriction, ];
    }
}
