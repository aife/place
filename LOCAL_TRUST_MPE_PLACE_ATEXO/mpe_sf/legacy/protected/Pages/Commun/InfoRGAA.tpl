<com:TContent ID="CONTENU_PAGE">
<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<div class="breadcrumbs">&gt; <com:TTranslate>INFO_RGAA</com:TTranslate></div>
		<!--Debut Texte InfoRGAA-->
		<div class="main-text margin-top-3">
			<h2 class="message-big">
				<%= utf8_encode(Application\Service\Atexo\Atexo_Config::getParameter('PF_LONG_NAME')) %>
			</h2>
			<%=$this->getInfoRGAA()%>
			<div class="breaker"></div>
		</div>
		<!--Fin Texte InfoRGAA-->		
		<div class="breaker"></div>
	</div>
	<!--Fin colonne droite-->
</com:TContent>