<?php

namespace Application\Pages\Commun;

use App\Service\Crypto\WebServicesCrypto;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\Signature\Atexo_Signature_Gestion;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SignDocument extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['callFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setDisplay('None');
    }

    public function sendSignature($sender, $param)
    {
$file = null;
        //      $json = $this->signature->value;
//      $dataSourceNew = Atexo_Signature_Gestion::transformeJson($json);
//      print_r($dataSourceNew);exit;
        $signature = $this->signature->Value;
        $error = false;
        if ($signature) {
            $file = Atexo_Config::getParameter('COMMON_TMP').'/signature_document'.session_name().session_id().time();
            if (!Atexo_Util::write_file($file, $signature)) {
                $error = true;
            }
        } else {
            $error = true;
        }

        if ($error) {
            $this->panelMessageErreur->setDisplay('Dynamic');
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_LORS_DE_LA_SIGNATURE'));
        } else {
            $this->panelMessageErreur->setDisplay('None');
            $cheminFichierSigner = $this->cheminDocument->Value;
            $cheminEtNomFichier = explode('\\', $cheminFichierSigner);
            if ('' != $this->nomFichierSignature->Value) {
                $nomFichierSignature = $this->nomFichierSignature->Value;
            } else {
                $nomFichierSignature = $cheminEtNomFichier[count($cheminEtNomFichier) - 1].'.p7s';
            }
            $fp = fopen($file, 'r');
            $data = fread($fp, filesize($file));
            fclose($fp);
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private', false);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$nomFichierSignature.'";');
            header('Content-Transfer-Encoding: binary');
            echo $data;
            exit;
        }
    }

    /*
     * Permet d'avoir les parametre pour MpeChiffrementApplet
     */
    public function getParamsMpeChiffrementApplet()
    {
        $modeRestriction = 'false';
        if (Atexo_Module::isEnabled('ModeRestrictionRGS')) {
            $modeRestriction = 'true';
        }

        return ['origineContexteMetier' => 'MPE_chiffrement',
        'originePlateforme' => Atexo_Config::getParameter('ORIGINE_PLATEFORME'),
        'origineOrganisme' => Atexo_Config::getParameter('ORIGINE_ORGANISME'),
        'origineItem' => Atexo_Config::getParameter('ORIGINE_ITEM'),
        'UrlPkcs11LibRefXml' => Atexo_Config::getParameter('URL_LIBRARY_PKCS11'),
        'TypeHash' => Atexo_Config::getParameter('TYPE_ALGORITHM_HASH'),
        'MethodeJavascriptDebutAttente' => 'afficherVeuillezPatienter',
        'MethodeJavascriptFinAttente' => 'masquerVeuillezPatienter',
        'MethodeJavascriptRenvoiResultat' => 'renvoiResultat',
        'modeRestrictionRGS' => $modeRestriction, ];
    }

    /*
     * Permet d'avoir les parametre pour SelectionFichierApplet
     */
    public function getParamsSelectionFichierApplet()
    {
        return ['MethodeJavascriptDebutAttente' => 'showLoader', 'MethodeJavascriptFinAttente' => 'showLoader'];
    }

    public function lancerJWS()
    {
        $resultat = self::getJnlpToSignDocument();
        if ($resultat) {
            $this->panelMessageErreur->setDisplay('None');
            header('Content-Length: '.strlen($resultat));
            header('Content-disposition: attachment; filename="'.Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_SIGNATURE').'"');
            header('Content-type: application/x-java-jnlp-file');
            echo $resultat;
            exit;
        } else {
            $this->panelMessageErreur->setDisplay('Dynamic');
            $this->panelMessageErreur->setMessage(Prado::localize('SERVICE_CRYPTO_ACTUELLEMENT_INDISPONIBLE'));
        }
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param array $offres
     * @param int   $idAgent
     * @param array $params
     *
     * @return Dehiffrement
     */
    public function getJnlpToSignDocument()
    {
        $logger = Atexo_LoggerManager::getLogger('crypto');

        $service = new Atexo_Crypto_Service(
            Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')),
            $logger,
            Atexo_Config::getParameter('ENABLE_SIGNATURE_SERVER_V2'),
            null,
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );

        try {
            return $service->jnlpSignature(Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')));
        } catch (AtexoCrypto\Exception\ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAgent()
    {
        if ('agent' == strtolower(Atexo_Util::atexoHtmlEntities($_GET['callFrom']))) {
            return true;
        }

        return false;
    }

    public function getModeServeurCrypto()
    {
        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            || ($this->isAgent() && Atexo_CurrentUser::getId() && Atexo_Module::isEnabled('ModeApplet'))
        ) {
            return 0;
        } else {
            return 1;
        }
    }
}
