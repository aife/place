<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class ConditionsUtilisation extends MpeTPage
{
    public function onInit($param)
    {

        $dateDebAction = date('Y-m-d H:i:s');
        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $details = "Conditions d'utilisation";
        $idInscrit = Atexo_CurrentUser::getIdInscrit();
        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d');
        $action = substr($_SERVER['QUERY_STRING'], 0);
        $dateFinAction = date('Y-m-d H:i:s');
        Atexo_InscritOperationsTracker::trackingOperations($idInscrit, $idEntreprise, $ip, $date, $dateDebAction, $action, $details, $dateFinAction);
    }

    public function onOKClick()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
    }

    /**
     * Permet de gerer la visibilite des CGU specifiques.
     *
     * @return bool : true si CGU specifiques visibles, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function visibiliteBlocCguSpecifique()
    {
        if ('entreprise' == $_GET['calledFrom'] && 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE' != Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE')) {
            return true;
        } elseif ('agent' == $_GET['calledFrom'] && 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' != Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT')) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer le contenu specifique a afficher selon les cas.
     *
     * @return string : le contenu specifique a afficher
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getLibelleBlocCguSpecifique()
    {
        if ('entreprise' == $_GET['calledFrom'] && 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE' != Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE')) {
            return Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE');
        } elseif ('agent' == $_GET['calledFrom'] && 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' != Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT')) {
            return Prado::localize('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT');
        }

        return '';
    }
}
