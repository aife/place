<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Guides;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class PopupGuidesUser extends MpeTPage
{
    public $langue;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
    }

    public function onLoad($param)
    {
        if (isset($_GET['file'])) {
            $this->langue = Atexo_CurrentUser::readFromSession('lang');
            $fileName = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['file'])).$this->langue.'.swf';
            $guide = (new Atexo_Guides())->retrieveGuideByFileName($fileName);
            if ($guide) {
                $this->response->redirect('index.php?page='.Atexo_Util::atexoHtmlEntities($_GET['calledFrom']).'.GuideUser&calledFrom='.Atexo_Util::atexoHtmlEntities($_GET['calledFrom']).'&file='.base64_encode($fileName));
            } else {
                $fileName = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['file'])).Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE').'.swf';
                $guide = (new Atexo_Guides())->retrieveGuideByFileName($fileName);
                if ($guide) {
                    $this->response->redirect('index.php?page='.Atexo_Util::atexoHtmlEntities($_GET['calledFrom']).'.GuideUser&calledFrom='.Atexo_Util::atexoHtmlEntities($_GET['calledFrom']).'&file='.base64_encode($fileName));
                } else {
                    if (isset($_GET['calledFrom']) && 'agent' == $_GET['calledFrom']) {
                        $this->response->redirect('index.php?page=Agent.AgentHome');
                    }
                    if (isset($_GET['calledFrom']) && 'entreprise' == $_GET['calledFrom']) {
                        $this->response->redirect('index.php?page=Entreprise.EntrepriseHome');
                    }
                }
            }
        }
    }
}
