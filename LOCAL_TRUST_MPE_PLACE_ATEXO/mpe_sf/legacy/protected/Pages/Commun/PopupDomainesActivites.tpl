<%@ MasterClass="Application.layouts.PopUpLayout" %>
<com:TContent ID="CONTENU_PAGE">
	<div class="popup-moyen" id="container">
<h1><com:TTranslate>TEXT_DOMAINES_ACTIVITES</com:TTranslate></h1>
<!--Debut Bloc Secteur d'activite-->
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_CHOIX_DOMAINES_ACTIVITES</com:TTranslate></span></div>
	<div class="content">
		<div class="line"><com:TTranslate>TEXT_SELECTIONNEZ_DOMAINES_ACTIVITES</com:TTranslate></div>
		<div class="nav-entite-achat">
			<ul class="check-arbo">
			<com:TRepeater  ID="repeaterCategorie">
			<prop:ItemTemplate>
				<li> 
    				<span class="check-bloc">
        				<img id="img_plus_moins_<%#$this->ItemIndex%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-moins.gif" alt="<%=Prado::localize('TEXT_OUVRIR_FERMER')%>" title="<%=Prado::localize('TEXT_OUVRIR_FERMER')%>" onclick="openSousCat(this,'all-cats_<%#$this->ItemIndex%>');" />
        				<com:TCheckBox
        					visible="<%=(isset($_GET['AllLevel'])) ? false:true%>" 
        				    Checked="<%#$this->Page->exestingDomaines($this->Data->getId())%>"
        					Value = "<%#$this->Data->getId()%>"
        					Attributes.onclick="onSousCatClicked('<%#$this->idCategorie->getClientId()%>','img_plus_moins_<%#$this->ItemIndex%>','all-cats_<%#$this->ItemIndex%>')"
        					id="idCategorie" 
        					Attributes.title="<%#$this->Data->getLibelleTraduit()%>" />
    				</span>
    					<label class="content-bloc bloc-580">
    						<strong><%#$this->Data->getLibelleTraduit();%></strong>
    					</label>
    					<ul style="display:block" id="all-cats_<%#$this->ItemIndex%>">
    						<com:TRepeater  ID="repeaterSousCategorie" DataSource="<%#Application\Service\Atexo\Consultation\Atexo_Consultation_Category::retreiveGategorieNiveau2($this->Data->getId(),false,false,true)%>">
    							<prop:ItemTemplate>
    								<!--  debut niveau 2 -->
    								<com:TPanel Visible="<%=$this->Data['active']%>">
	        							<li> 
	        								<span class="check-bloc">
	        									<img id="sous-cats_img_plus_moins_<%#$this->Parent->Parent->ItemIndex%>_<%#$this->ItemIndex%>" onclick="openSousCat(this,'sous-cat_<%#$this->Parent->Parent->ItemIndex%>_<%#$this->ItemIndex%>')"  
	            									src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-plus.gif"
	            									alt="<%=Prado::localize('TEXT_OUVRIR_FERMER')%>"
	            									title="<%=Prado::localize('TEXT_OUVRIR_FERMER')%>" />
	        									<com:TCheckBox 
	        										visible="<%=(isset($_GET['AllLevel'])) ? false:true%>" 
	        										Checked="<%#($this->Page->exestingDomaines($this->Data['id']) ? true:false)%>"
	        										id="idSection" 
	        									    Value = "<%#$this->Data['id']%>"
	        										Attributes.onclick="onSousCatClicked('<%#$this->idSection->getClientId()%>','sous-cats_img_plus_moins_<%#$this->Parent->Parent->ItemIndex%>_<%#$this->ItemIndex%>','sous-cat_<%#$this->Parent->Parent->ItemIndex%>_<%#$this->ItemIndex%>')" />
	        								</span>
	        								<label class="content-bloc bloc-580">
	        									<%#$this->Data[$this->Page->getLibelleTraduit()]%>
	        								</label>
	        								
	        								<ul id="sous-cat_<%#$this->Parent->Parent->ItemIndex%>_<%#$this->ItemIndex%>" style="display:none;" class="indent-ss-cat">
	        								<!-- fin niveau 2-->
	            								<com:TRepeater ID="repeaterCatNiv3" DataSource="<%#$this->Data['SousNiveau']%>">
	                								<prop:ItemTemplate>
		                								<com:TPanel Visible="<%=$this->Data['active']%>">
		                									<li>
		                										<span class="check-bloc">
		                										<com:TCheckBox 
		                											id="idBranche" 
		                											Checked="<%#$this->Page->exestingDomaines($this->Data['id'])%>"
		                											Value = "<%#$this->Data['id']%>"/>
		            											</span>
		                										<label for="option_01" class="content-bloc bloc-580"><%#$this->Data[$this->Page->getLibelleTraduit()]%></label>
		                									</li>
		                								</com:TPanel>
	                								</prop:ItemTemplate>
	            								</com:TRepeater>
	        									<!-- -->
	        								</ul>
	        							</li>
        							</com:TPanel>
    							</prop:ItemTemplate>
							</com:TRepeater>
						</ul>
					</li>
                </prop:ItemTemplate>
                </com:TRepeater>
			</ul>
		</div>
		<!--Debut line Autres pays-->
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Secteur d'activite-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:PictoAnnuler />
		<com:PictoValidateButton ID="validateButton"  onClick="selectedCategories"/>
    	<com:TLabel ID="script"/>	
    </div>
	<!--Fin line boutons-->
	</div>
<script type="text/javascript">popupResize();</script>

</com:TContent>
