<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger un fichier.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadFile extends MpeTPage
{
    protected $_idFichier;
    protected $_nomFichier;

    public function onLoad($param)
    {
    }

    /**
     * Permet de télécharger un fichier.
     *
     * @param string $file_name  le nom du fichier
     * @param object $connection la connection
     */
    public function downloadFiles($idFichier = null, $nomFichier = null, $dest = null)
    {
        if (!Atexo_Util::isSafeFileName($nomFichier)) {
            return;
        }

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);

        if ('.zip' == strrchr($nomFichier, '.')) {
            header('Content-Type: application/zip');
        } else {
            header('Content-Type: application/octet-stream');
        }

        $nomFichier = str_replace('\\', '', $nomFichier);

        header('Content-Disposition: attachment; filename="'.$nomFichier.'";');
        header('Content-Transfer-Encoding: binary');

        $atexoBlob = new Atexo_Blob();
        $atexoBlob->send_blob_to_client($idFichier, $dest);

        exit;
    }

    /**
     * Donne le fichier en téléchargement.
     *
     * @param string $nameFile Nom du fichier reçu par le client
     * @param string $content  Contenu du fichier à envoyer au client
     */
    public static function downloadFileContent($nameFile, $content)
    {
        if (!Atexo_Util::isSafeFileName($nameFile)) {
            return;
        }

        header('Pragma: ');
        header('Expires: ');
        header('Cache-control: ');
        header('Content-Disposition: attachment; filename="'.$nameFile.'";');

        if ('.zip' == strrchr($nameFile, '.')) {
            header('Content-Type: application/zip');
        } else {
            header('Content-Type: application/octet-stream');
        }

        header('Content-Tranfert-Encoding: binary');
        header('Content-Length: '.strlen($content));

        if ('.csv' == strrchr($nameFile, '.')) {
            echo "\xEF\xBB\xBF";
        }

        echo $content;

        exit;
    }
}
