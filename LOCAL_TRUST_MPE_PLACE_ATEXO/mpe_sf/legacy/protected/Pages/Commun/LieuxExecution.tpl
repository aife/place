<%@ MasterClass="Application.layouts.PopUpLayout" %>
<com:TContent ID="CONTENU_PAGE">
<div class="popup-moyen" id="container">
	<com:THiddenField ID="selectedType1GeoN2" Value=""/>
	<h1><com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate></h1>
	<!--Debut Bloc Lieu d'execution-->
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_CHOIX_LIEU_EXECUTION</com:TTranslate></span></div>
		<div class="content">
			<com:TRepeater  ID="repeaterGeoN0"  onItemDataBound="itemDataBindGeoN0" >
			<prop:ItemTemplate>
					<com:THiddenField ID="typeN0" Value="<%# ($this->Data->getType()) %>"/>
					<!--Debut line Geolocalisation N0-->
                			<div class="line">
                				<div class="intitule-auto">
            						<label for="<%#$this->getClientId()%>denominationN0">
            							<com:TLabel ID="nameChek0" Text="<%#$this->Page->writeValue($this->Data, 'Denomination')%>" />
            						</label>
            					</div>
                				<div class="breaker"></div>
                				<div id="blockGeo<%#$this->ItemIndex%>" style="display:block;">
                				<!--Debut bloc Geolocalisation N0 Type 1-->
                				<com:TPanel Attributes.class="content-bloc-auto indent-20" id="blocFrance">
                				
                    				<div class="intitule-auto  clear-both">
                    						<com:TRadioButton  ID="selectiongeoN0Aucun" Checked="true" GroupName="RadioGroup<%#$this->ItemIndex %>" 
                    							Attributes.title="<%#$this->Page->writeValue($this->Data, 'LibelleAucun')%>" cssClass="radio"
                    							Attributes.onclick="hideDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId()%>');"
                    						/>
                    						<label for="<%# $this->getClientId()%>selectiongeoN0Aucun">
                    							<com:TLabel  Text="<%#$this->Page->writeValue($this->Data, 'LibelleAucun')%>" />
                    						</label>
                    					</div>
                				
                					<div class="intitule-auto  clear-both">
                						<com:TRadioButton  ID="selectiongeoN0Tout" GroupName="RadioGroup<%#$this->ItemIndex %>" 
                							Attributes.title="<%#$this->Page->writeValue($this->Data, 'LibelleTous')%>" cssClass="radio"
                							Attributes.onclick="hideDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId()%>');"
                						/>
                						<label for="<%# $this->getClientId()%>selectiongeoN0">
                							<com:TLabel  Text="<%#$this->Page->writeValue($this->Data, 'LibelleTous')%>" />
                						</label>
                					</div>
                					
                					<div class="intitule-auto  clear-both">
                						<com:TRadioButton ID="selectiongeoN0Select" GroupName="RadioGroup<%#$this->ItemIndex %>" Attributes.name="selectionFrance" 
                    							 Attributes.title="<%#$this->Page->writeValue($this->Data, 'LibelleSelectionner')%>" cssClass="radio"
                    						Attributes.onclick="showDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId() %>');"
                    					/>
                						<label for="<%#$this->getClientId() %>selectiongeoN0Select">
                							<com:TLabel Text="<%#$this->Page->writeValue($this->Data, 'LibelleSelectionner')%>" />
                						</label>
                					</div>
                					
                					<!--Debut bloc Selection Geolocalisation N1 et N2-->
                					<com:TPanel id="choixDept" Attributes.style="display:none;" Attributes.class="liste-depts">
                						<table summary="<%=Prado::localize('DEFINE_LISTE_GEOLOCALISATION_N2')%>">
                							<com:TRepeater ID="repeaterGeoN1"  visible = "<%# ($this->Data->getType()==0) ? true:false%>">
                								<caption><com:TTranslate>DEFINE_LISTE_GEOLOCALISATION_N2</com:TTranslate></caption>
                    							<prop:HeaderTemplate>
                    								<tr>
                    									<th colspan="3" style="display:none;" abbr="Départements"><com:TTranslate>DEFINE_LISTE_GEOLOCALISATION_N2</com:TTranslate></th>
                    								</tr>
                    							</prop:HeaderTemplate>
                    							<prop:ItemTemplate>
                    							<tr>
                    								<td>
                    								<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[1])=='') ? 'none':'block'%>" >
                    									<div class="title-toggle" onclick="togglePanel(this,'blocRegion1_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
                    										<%#$this->Page->getGeo1Denomination1($this->Data[1])%>
                    									</div>
                    									<ul id="blocRegion1_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
                    												<li>
																	<com:TCheckBox id="region1" Attributes.title="Tous" cssClass="check" 
																		Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',1,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																	<label for="region1"><%# Prado::localize('DEFINE_TOUS') %></label>
																	</li>
                    										<com:TRepeater  ID="repeaterGeoN2_1" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[1]) %>>
                            									<prop:ItemTemplate>
                            										<li>
                            										<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                            											<com:TCheckBox id="dep1" Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
                            											<label for="<%# $this->getClientId()%>dep1">
                            												<com:TLabel ID="geoN2Name_1" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
                            												<com:THiddenField ID="geoN2Id_1" Value="<%# $this->Data->getId()%>"/>
                            											</label>
                            											<!-- info bulle -->
                                											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                											style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? 'none':'block'%>"
                                												onmouseover="afficheBulle('infosDep1', this)" 
                                												onmouseout="cacheBulle('infosDep1')" 
                                												class="picto-info-intitule" 
                                												alt="Info-bulle" 
                                												title="Info-bulle" /> 
                            											<div id="infosDep1" class="info-bulle" 
                            												onmouseover="mouseOverBulle();" 
                            												onmouseout="mouseOutBulle();">
                            												<div>
                            													<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                            												</div>
                            											</div>
                            											<!-- fin  -->
                            										</com:TPanel>
                            										</li>
                            									</prop:ItemTemplate>
                            								</com:TRepeater>
                    									</ul>
                								</span>
                    						</td>
            								<td class="<%# ($this->Page->getGeo1Denomination1($this->Data[2])=='') ? 'td-vide':''%>">
            								<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[2])=='') ? 'none':'block'%>" >
                									<div class="title-toggle" onclick="togglePanel(this,'blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
                										<%#$this->Page->getGeo1Denomination1($this->Data[2])%>
                									</div>
                									<ul id="blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
                												<li>
																	<com:TCheckBox id="region2" Attributes.title="Tous" cssClass="check" 
																		Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',2,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																	<label for="region2"><%# Prado::localize('DEFINE_TOUS') %></label>
																</li>
                										<com:TRepeater ID="repeaterGeoN2_2" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[2]) %>>
                        									<prop:ItemTemplate>
                        										<li>
                        											<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                            											<com:TCheckBox 	id="dep2"  Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
                            											<label for="<%# $this->getClientId()%>dep2">
                            												<com:TLabel ID="geoN2Name_2" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
                            												<com:THiddenField ID="geoN2Id_2" Value="<%# $this->Data->getId()%>"/>
                            											</label>
                            											<!-- info bulle -->
                                											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                											style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? none:''%>"
                                												onmouseover="afficheBulle('infosDep2', this)" 
                                												onmouseout="cacheBulle('infosDep2')" 
                                												class="picto-info-intitule" 
                                												alt="Info-bulle" 
                                												title="Info-bulle" /> 
                            											<div id="infosDep2" class="info-bulle" 
                            												onmouseover="mouseOverBulle();" 
                            												onmouseout="mouseOutBulle();">
                            												<div>
                            													<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                            												</div>
                            											</div>
                            											<!-- fin  -->
                            										</com:TPanel>
                        										</li>
                        									</prop:ItemTemplate>
                        								</com:TRepeater>
                									</ul>
                									</span>
                								</td>
                								<td class="<%# ($this->Page->getGeo1Denomination1($this->Data[3])=='') ? 'td-vide':''%>">
                									<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[3])=='') ? 'none':'block'%>" >
                									<div class="title-toggle" onclick="togglePanel(this,'blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
                									<%#$this->Page->getGeo1Denomination1($this->Data[3])%>
                									</div>
                									<ul id="blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
                												<li>
																	<com:TCheckBox id="region3" Attributes.title="Tous" cssClass="check" 
																		Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',3,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																	<label for="region3"><%# Prado::localize('DEFINE_TOUS') %></label>
																</li>
                										<com:TRepeater   ID="repeaterGeoN2_3" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[3]) %>>
                        									<prop:ItemTemplate>
                        										<li>
                        											<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                            											<com:TCheckBox 	id="dep3"  Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
                            											<label for="<%# $this->getClientId()%>dep3">
                            												<com:TLabel ID="geoN2Name_3" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
                            												<com:THiddenField ID="geoN2Id_3" Value="<%# $this->Data->getId()%>"/>
                            											</label>
                            											<!-- info bulle -->
                                											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                											style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? 'none':'block'%>"
                                												onmouseover="afficheBulle('infosDep3', this)" 
                                												onmouseout="cacheBulle('infosDep3')" 
                                												class="picto-info-intitule" 
                                												alt="Info-bulle" 
                                												title="Info-bulle" /> 
                            											<div id="infosDep3" class="info-bulle" 
                            												onmouseover="mouseOverBulle();" 
                            												onmouseout="mouseOutBulle();">
                            												<div>
                            													<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                            												</div>
                            											</div>
                            											<!-- fin  -->
                        											</com:TPanel>
                        										</li>
                        									</prop:ItemTemplate>
                        								</com:TRepeater>
                									</ul>
                								</span>
                							</td>
                    						</tr>
                    					</prop:ItemTemplate>
                            		</com:TRepeater>
                				</table>
                			</com:TPanel>
                		<!--Fin bloc Selection Geolocalisation N1 et N2-->
                		</com:TPanel>
                	</div>
                	<!--Fin bloc Geolocalisation N0 Type 0-->
                	<!--Debut bloc Geolocalisation N0 Type 1-->
        			<div class="line">
        				<com:TPanel visible = "<%# ($this->Data->getType()==1) ? true:false%>">
        				<!--Debut Bloc Selection par Geolocalisation N0 Type 1-->
        				 <com:TPanel id="choixPays" Attributes.style="display:none;" Attributes.class="autres-pays">   
        					<div class="column-auto">
        						<div class="intitule-auto"><label for="list1"><com:TTranslate>DEFINE_LISTE_PAYS</com:TTranslate></label></div>
        						<div class="breaker"></div>
        						<com:TListBox 
            						ID="list1" 
            						DataSource=<%# $this->Page->retrieveListGeolocalisationType1InArray($this->Data) %>
            						cssClass="liste-pays" Rows="6"  
            						Attributes.multiple="multiple" 
            						Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" 
            						Attributes.title="<%# Prado::localize('DEFINE_LISTE_PAYS')%>"/>
        					</div>
        					<div class="transfer-input">
            						<com:TButton Attributes.name="right" Attributes.type="button" Attributes.value="&nbsp;" CssClass="move-right"  Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferRight();return false;" />
            						<com:TButton Attributes.name="right" Attributes.type="button" Attributes.value="<%# Prado::localize('DEFINE_TOUS') %>" CssClass="move-right-all"  Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllRight();return false;" />
            						<com:TButton Attributes.name="left" Attributes.type="button" Attributes.value="&nbsp;" CssClass="move-left" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" />
            						<com:TButton Attributes.name="left" Attributes.type="button" Attributes.value="<%# Prado::localize('DEFINE_TOUS') %>"  CssClass="move-left-all" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllLeft();return false;" />
            				</div>			
        					<div class="column-auto">
        					   <div class="intitule-auto"><label for="list2"><com:TTranslate>DEFINE_MA_SELECTION</com:TTranslate></label></div>
        					   <div class="breaker"></div>
        					   <com:TListBox ID="list2"  cssClass="liste-pays" Rows="6" Attributes.multiple="multiple" Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" Attributes.title="<%# Prado::localize('DEFINE_MA_SELECTION')%>"/>
        					</div>
        					<script type="text/javascript">
                				var list1Name = document.getElementById('<%# $this->list1->getClientId()%>').name;
                				var list2Name = document.getElementById('<%# $this->list2->getClientId()%>').name;
                                var opt<%# $this->ItemIndex%> = new OptionTransfer(list1Name, list2Name);
                                opt<%# $this->ItemIndex%>.init(document.forms[0]);
                                opt<%# $this->ItemIndex%>.setAutoSort(true);
                                opt<%# $this->ItemIndex%>.setDelimiter(",");
                                opt<%# $this->ItemIndex%>.saveRemovedLeftOptions("removedLeft");
                                opt<%# $this->ItemIndex%>.saveRemovedRightOptions("removedRight");
                                opt<%# $this->ItemIndex%>.saveAddedLeftOptions("addedLeft");
                                opt<%# $this->ItemIndex%>.saveAddedRightOptions("addedRight");
                                opt<%# $this->ItemIndex%>.saveNewLeftOptions("newLeft");
                                opt<%# $this->ItemIndex%>.saveNewRightOptions("newRight");
                            </script>
        				
        				</com:TPanel>
        				<!--Fin Bloc Selection par Geolocalisation N0 Type 1-->
        			</com:TPanel>
        			</div> 
        			</div>
        			<!--Fin bloc Geolocalisation N0 Type 1-->
   		</prop:ItemTemplate>
	</com:TRepeater>		
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
    <!--Fin Bloc Lieu d'execution-->
    <!--Debut line boutons-->
    <div class="boutons-line">
    	<com:PictoAnnuler />
    	<com:PictoValidateButton ID="validateButton"  onClick="retrieveAllSelectedGeo"/>
    	<com:TLabel ID="script"/>	
    </div>
    <!--Fin line boutons-->
    </div>
    <com:TLabel ID="script2"/>						
</com:TContent>
