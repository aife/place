<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonSsoAgent;
use Application\Propel\Mpe\CommonSsoEntreprise;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Service;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;
use Application\Service\Atexo\Soap\Atexo_Soap_SocleSynchro;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Application\Service\Atexo\TokenManager\Atexo_TokenManager_TokenManager;

class Auth extends MpeTPage
{
    public function onInit($param)
    {
        $returnUrl = $this->getApplication()->getModule('auth')->ReturnUrl;

        if (mb_strpos($returnUrl, 'page=Agent.') !== false) {
            $redirect = '/agent/login';
        } elseif (mb_strpos($returnUrl, 'page=Administration.') !== false) {
            $redirect = '/admin/login';
        } else {
            $redirect = '/';
        }

        $this->response->redirect($redirect);
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $headersArray = array_change_key_case(getallheaders(), CASE_LOWER);
            if ((isset($headersArray['externalid']) && isset($headersArray['usertype']))) {
                $externalId = $headersArray['externalid'];
                $userType = $headersArray['usertype'];
                Atexo_CurrentUser::writeToSession('externalId', $externalId);
                Atexo_CurrentUser::writeToSession('userType', $userType);
                if ('AGENT' == $userType) {
                    if (isset($_GET['access']) && 'HELIOS' == $_GET['access']) {
                        $accessHelios = true;
                    } else {
                        $accessHelios = false;
                    }
                    self::authenticateViaPPPSocleAgent($externalId, $accessHelios);
                } elseif ('EMPLOYEE' == $userType) {
                    self::authenticateViaPPPSocleEntreprise($externalId);
                }
            } else {
                if ('/' == $_SERVER['REQUEST_URI']) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
                }
            }
        }
        if ('disconnect_agent' == $_GET['action']) { // deconnexion
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect(
                Atexo_Config::getParameter(
                    'URL_INDEX_SSO'
                ) . '?action=disconnect_agent&sso=' . Atexo_Util::atexoHtmlEntities($_GET['sso'])
            );
        }
        if ('disconnect' == $_GET['action']) { // deconnexion
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect(
                Atexo_Config::getParameter(
                    'URL_INDEX_SSO_ENTREPRISE'
                ) . '?action=disconnect&sso=' . Atexo_Util::atexoHtmlEntities($_GET['sso'])
            );
        }
        if ($_GET['fromSoclegoto']) {
            $returnUrl = '?page=' . Atexo_Util::atexoHtmlEntities($_GET['fromSoclegoto']);
            $encodeUrl = false;
        } else {
            $returnUrl = $this->getApplication()->getModule('auth')->ReturnUrl;
            $encodeUrl = true;
        }
        $accessTo = $_GET['access']; // authentification depuis socle externe
        if (Atexo_Module::isEnabled('PortailDefenseEntreprise') && isset($_GET['sso'])) {
            if ($accessTo) {
                $inscritSso = (new Atexo_InterfaceWs_Authentication())->getSsoInscritByIdSso(Atexo_Util::atexoHtmlEntities($_GET['sso']));
                if ($inscritSso instanceof CommonSsoEntreprise) {
                    $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($inscritSso->getIdInscrit());
                    if ($inscrit instanceof CommonInscrit) {
                        $userVo = new Atexo_UserVo();
                        if ($inscrit->getLogin() && $inscrit->getMdp()) {
                            $userVo->setLogin($inscrit->getLogin());
                            $userVo->setPassword($inscrit->getMdp());
                        } else {
                            $userVo->setCertificat('__CERT__');
                            $userVo->setPassword('__CERT__');
                        }
                        $userVo->setType('entreprise');
                        $userVo->setSso(Atexo_Util::atexoHtmlEntities($_GET['sso']));
                        if ($this->getApplication()->getModule("auth")->login($userVo, null)) {
                            if ($accessTo == "profil") {
                                $this->response->redirect(
                                    Atexo_Config::getParameter(
                                        'PF_URL'
                                    ) . "?page=Entreprise.EntrepriseAccueilAuthentifie"
                                );
                            } else {
                                if ($accessTo == "tenders") {
                                    $this->response->redirect(
                                        Atexo_Config::getParameter('PF_URL')
                                        . "?page=Entreprise.EntrepriseAdvancedSearch&AllCons"
                                    );
                                }
                            }
                        } else {
                            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome&goto=' . urlencode($returnUrl));
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome&goto=' . urlencode(
                            $returnUrl
                        ));
                    }
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome&goto=' . urlencode(
                        $returnUrl
                    ));
                }
            }
            else {
                $agentSso = (new Atexo_InterfaceWs_Authentication())->getSsoAgentByIdSso(Atexo_Util::atexoHtmlEntities($_GET['sso']));
                if ($agentSso instanceof CommonSsoAgent) {
                    $agent = (new Atexo_Agent())->retrieveAgent($agentSso->getIdAgent());
                    if ($agent instanceof \CommonAgent) {
                        $userVo = new Atexo_UserVo();
                        $userVo->setLogin($agent->getLogin());
                        $userVo->setPassword($agent->getPassword());
                        $userVo->setType("agent");
                        if ($this->getApplication()->getModule("auth")->login($userVo, null)) {
                            $this->response->redirect(
                                Atexo_Config::getParameter('PF_URL_AGENT') . "agent"
                            );
                        } else {
                            $this->response->redirect(
                                Atexo_Config::getParameter('PF_URL_AGENT') . "?page=Agent.AgentHome&goto=" . urlencode(
                                    $returnUrl
                                )
                            );
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&goto=' . urlencode(
                            $returnUrl
                        ));
                    }
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&goto=' . urlencode(
                        $returnUrl
                    ));
                }
            }
        }
        else {
            if ((!$returnUrl || strstr($returnUrl, "page=Entreprise.")) && (!$accessTo)) {
                if (Atexo_Module::isEnabled("SocleExterneEntreprise")) { // autentification distante via web service
                    self::authentificationSocleExtereneEntreprise($returnUrl, $encodeUrl);
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome&goto=' . urlencode(
                        $returnUrl
                    ));
                }
            } else {
                if (strstr($returnUrl, 'page=Commission.')) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Agent.AgentHome');
                } elseif (strstr($returnUrl, 'page=Agent.') || 'agent' == $accessTo) {
                    if (Atexo_Module::isEnabled('SocleExterneAgent')) { // autentification distante via web service
                        if (Atexo_Config::getParameter('SERVEUR_NAME_AGENT')) {
                            self::authentificationSocleExtereneAgent($returnUrl, $encodeUrl);
                        } else {
                            self::authentificationSocleShibboleth();
                        }
                    } elseif (Atexo_Module::isEnabled('AuthentificationBasic')) {
                        if (!isset($_SERVER['PHP_AUTH_USER'])) {
                            if (!str_contains($_SERVER['QUERY_STRING'], 'page=Agent') || !str_contains($_SERVER['QUERY_STRING'], 'page=administration')) {
                                $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
                            }
                        } else {
                            self::authenticateBasic($_SERVER['PHP_AUTH_USER']);
                        }
                    } else { // redirection vers la page d'autentification locale
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&goto=' . urlencode(
                            $returnUrl
                        ));
                    }
                } elseif (strstr($returnUrl, 'page=Administration.')) {
                    $this->response->redirect(
                        Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Administration.Home&goto=' . urlencode(
                            $returnUrl
                        )
                    );
                }
            }
        }
    }

    /**
     * Redirige vers la page Home authentifie, ou vers le socle en cas d'echec.
     *
     * @param Atexo_UserVo $entrepriseUserVo
     */
    public function authenticateViaSocleEntreprise($entrepriseUserVo, $returnUrl = null, $encodeUrl = true)
    {
        $rurl = '';
        if ($returnUrl) {
            if ($encodeUrl) {
                $urlPf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
                $rurl = '?rurl=' . urlencode($urlPf . '?page='.Atexo_Util::getTypeUserCalledForPradoPages().'.Auth&goto=' . $returnUrl);
            } else {
                $rurl = $returnUrl;
            }
        } elseif (!$entrepriseUserVo) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
        }
        if ($entrepriseUserVo) {
            if ($this->getApplication()->getModule('auth')->login($entrepriseUserVo, null)) {
                if ($returnUrl) {
                    $this->response->redirect($returnUrl);

                    return;
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseAccueilAuthentifie');
                }
            } else {
                if (isset($_GET['sso'])) {
                    $this->response->redirect(Atexo_Config::getParameter('URL_INDEX_SSO_ENTREPRISE') . $rurl);
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
                }
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('URL_INDEX_SSO_ENTREPRISE') . $rurl);
        }
    }

    /**
     * Redirige vers la page Home authentifie, ou vers le socle en cas d'echec.
     *
     * @param Atexo_UserVo $agentUserVo
     */
    public function authenticateViaSocleAgent($agentUserVo)
    {
        if ($agentUserVo) {
            if ($this->getApplication()->getModule('auth')->login($agentUserVo, null)) {
                if ('agenthelios' == $agentUserVo->getType()) {
                    $this->response->redirect('index.php?page=helios.HeliosAccueilAuthentifie');
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AccueilAgentAuthentifieSocleinterne');
                }
            } else {
                $this->response->redirect(urldecode(Atexo_Config::getParameter('URL_INDEX_SSO')));
            }
        } else {
            $this->response->redirect(urldecode(Atexo_Config::getParameter('URL_INDEX_SSO')));
        }
    }

    /**
     * Authentification pour les appels SF.
     *
     * @param Atexo_UserVo $agentUserVo
     */
    public function authenticateViaSfWs($agentUserVo)
    {
        return $this->getApplication()->getModule('auth')->login($agentUserVo, null);
    }

    /**
     * Redirige vers la page Home authentifie, ou vers le socle en cas d'echec.
     *
     * @param Atexo_UserVo $agentUserVo
     */
    public function authenticateViaPPPSocleAgent($idExterne, $accessHelios)
    {
        if ($idExterne) {
            $agent = (new Atexo_Agent())->retrieveAgentByIdExterne($idExterne);
            if ($accessHelios) {
                $idApplication = Atexo_Config::getParameter('IDENTIFIANT_SERVICE_HELIOS');
            } else {
                $idApplication = Atexo_Config::getParameter('ID_SERVICE_METIER_MPE');
            }
            if ($agent instanceof CommonAgent) {
                $agentServiceMetiers = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService(
                    $agent->getId(),
                    $idApplication
                );
                if ($agentServiceMetiers && is_array($agentServiceMetiers) && count($agentServiceMetiers) > 0) {
                    $userVo = new Atexo_UserVo();
                    $userVo->setLogin($agent->getLogin());
                    $userVo->setPassword('PPP');
                    if ($accessHelios) {
                        $userVo->setType('agenthelios');
                    } else {
                        $userVo->setType('agent');
                    }

                    $userVo->setAuthenticatePpp(true);
                    if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                        if ('agenthelios' == $userVo->getType()) {
                            $this->response->redirect('index.php?page=helios.HeliosAccueilAuthentifie');
                        } else {
                            if (strstr($this->Application->getModule('auth')->ReturnUrl, 'page=Agent.SearchDCE')) {
                                $this->response->redirect($this->Application->getModule('auth')->ReturnUrl);
                            } else {
                                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . 'agent');
                            }
                        }
                    }
                }
            }
        }
        $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
    }

    public function authenticateViaPPPSocleEntreprise($idExterne)
    {
        if ($idExterne) {
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdExterne($idExterne);
            if ($inscrit instanceof CommonInscrit) {
                $userVo = new Atexo_UserVo();
                $userVo->setAuthenticatePpp(true);
                if ($inscrit->getLogin()) {
                    $userVo->setLogin($inscrit->getLogin());
                    $userVo->setPassword('PPP');
                } else {
                    $userVo->setCertificat('__CERT__');
                    $userVo->setPassword('__CERT__');
                }
                $userVo->setType('entreprise');
                if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                    if (isset($_SERVER['HTTP_REFERER'])) {
                        $qs = explode('&', parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY));
                        $url = [];
                        foreach ($qs as $kv) {
                            [$key, $value] = explode('=', $kv);
                            $url[$key] = urldecode($value);
                        }
                        if (isset($url['goto']) && Atexo_Util::urlIsAllow($_GET['goto'])) {
                            $this->response->redirect($url['goto']);
                        } elseif (Atexo_CurrentUser::readFromSession('url_response')) {
                            $referer = Atexo_CurrentUser::readFromSession('url_response');
                            Atexo_CurrentUser::deleteFromSession('url_response');
                            $this->response->redirect($referer);
                        } else {
                            $this->response->redirect(Atexo_Config::getParameter('PF_URL')
                                              . "?page=Entreprise.EntrepriseAccueilAuthentifie");
                        }
                    } else {
                        $this->response->redirect(
                            Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseAccueilAuthentifie'
                        );
                    }
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL')
                                              . '?page=Entreprise.EntrepriseAccueilAuthentifie');
                }
            }
        }
        $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
    }

    /**
     * Redirige vers la page Home authentifie.
     *
     * @param login de l'agent
     */
    public function authenticateBasic($login)
    {
        if ($login) {
            $userVo = new Atexo_UserVo();
            $userVo->setLogin($login);
            $userVo->setPassword('AuthBasic');
            $userVo->setType('agent');
            $userVo->setAuthenticatePpp(true);
            if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . 'agent');
            }
        }
    }

    public function authentificationSocleExtereneEntreprise($returnUrl, $encodeUrl)
    {
        $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
        if (
            $headersArray && Atexo_Config::getParameter(
                'SERVEUR_NAME_ENTREPRISE'
            ) && isset($headersArray['IV-SERVER-NAME']) && $headersArray['IV-SERVER-NAME']
        ) {
            $serverNameEntreprise = explode('#', Atexo_Config::getParameter('SERVEUR_NAME_ENTREPRISE'));
            if (in_array($headersArray['IV-SERVER-NAME'], $serverNameEntreprise)) {
                if ('disconnect' == $_GET['action']) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseDisconnect');
                }

                if (isset($headersArray['IV-USER']) && $headersArray['IV-USER'] && 'Unauthenticated' != $headersArray['IV-USER'] && 'unknown_cert' != $headersArray['IV-USER']) {
                    $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($headersArray['IV-USER']);
                    if ($inscrit) {
                        $userVo = new Atexo_UserVo();
                        $userVo->setLogin($inscrit->getLogin());
                        $userVo->setPassword($inscrit->getMdp());
                        $userVo->setType('entreprise');
                        self::authenticateViaSocleEntreprise($userVo, $returnUrl, $encodeUrl);
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.FormulaireCompteEntreprise&action=creation&goto=' . urlencode(Atexo_Util::atexoHtmlEntities($_GET['goto'])));
                    }
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
                }
            } else { // redirection vers la page d'autentification locale
                $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
        }
    }

    public function authentificationSocleExtereneAgent($returnUrl, $encodeUrl)
    {
        $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
        if (
            Atexo_Config::getParameter(
                'SERVEUR_NAME_AGENT'
            ) && isset($headersArray['IV-SERVER-NAME']) && $headersArray['IV-SERVER-NAME']
        ) {
            $serverNameAgent = explode('#', Atexo_Config::getParameter('SERVEUR_NAME_AGENT'));
            if (in_array($headersArray['IV-SERVER-NAME'], $serverNameAgent)) {
                if ('disconnect' == $_GET['action']) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.Disconnect');
                }

                if (isset($headersArray['IV-USER']) && $headersArray['IV-USER']) {
                    $agent = (new Atexo_Agent())->retrieveAgentByLogin($headersArray['IV-USER']);
                    if ($agent) {
                        if (true == $agent->getActif()) {
                            $userVo = new Atexo_UserVo();
                            $userVo->setLogin($agent->getLogin());
                            $userVo->setPassword($agent->getPassword());
                            $userVo->setType('agent');
                            self::authenticateViaSocleAgent($userVo);
                        } else {
                            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&erreurAgent=2');
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&erreurAgent=1');
                    }
                } else {
                    $this->response->redirect(urldecode(Atexo_Config::getParameter('URL_INDEX_SSO')));
                }
            } else { // redirection vers la page d'autentification locale
                $this->response->redirect(urldecode(Atexo_Config::getParameter('URL_INDEX_SSO')));
            }
        } else {
            $this->response->redirect(urldecode(Atexo_Config::getParameter('URL_INDEX_SSO')));
        }
    }

    public function authentificationSocleShibboleth()
    {
        $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
        $urlRedirect = Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome';
        if (isset($headersArray['EXTERNALID']) && $headersArray['EXTERNALID'] && isset($headersArray['SERVICEID']) && $headersArray['SERVICEID']) {
            if ('disconnect' == $_GET['action']) {
                $urlRedirect = Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.Disconnect';
            } else {
                $service = Atexo_Service::retrieveServiceByIdExterne($headersArray['SERVICEID']);
                $urlRedirect = Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&erreurAgent=1';
                if ($service instanceof CommonService) {
                    $agent = (new Atexo_Agent())->retrieveAgentByExternalIdAndService($headersArray['EXTERNALID'], $service->getId());
                    if ($agent) {
                        if (true == $agent->getActif()) {
                            $userVo = new Atexo_UserVo();
                            $userVo->setLogin($agent->getLogin());
                            $userVo->setPassword($agent->getPassword());
                            $userVo->setType('agent');
                            if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                                if (Atexo_Module::isEnabled('SocleInterne')) {
                                    $urlRedirect = 'index.php?page=Agent.AccueilAgentAuthentifieSocleinterne';
                                } else {
                                    $urlRedirect = 'agent';
                                }
                            }
                        } else {
                            $urlRedirect = Atexo_Config::getParameter('PF_URL_AGENT') . '?page=Agent.AgentHome&erreurAgent=2';
                        }
                    }
                }
            }
        }
        $this->response->redirect($urlRedirect);
    }
}
