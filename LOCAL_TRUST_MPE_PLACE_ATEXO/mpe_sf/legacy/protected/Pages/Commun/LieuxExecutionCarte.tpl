<%@ MasterClass="Application.layouts.PopUpLayout" %>
<com:TContent ID="CONTENU_PAGE">
    <link rel="stylesheet" href="<%=$this->themesPath()%>/css/ng-style.css" type="text/css" />
    <div class="" id="ModalLieuxExecutionCarte" tabindex="-1" role="dialog" aria-labelledby="ModalLieuxExecutionCarteLabel"> <!--*********************ADD CLASS "MODAL FADE"*********************-->
        <com:THiddenField ID="selectedType1GeoN2" Value=""/>
        <div class="" role="document"> <!--*********************ADD CLASS MODAL-DIALOG*********************-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="fermer">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="h4 modal-title" id="myModalLabel">
                        <com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate>
                    </h2>
                </div>
                <div class="modal-body">
                    <!--BEGIN LIEUX D'EXECUTION-->
                    <com:PanelMessageInformation ID="panelMessageInformation" />
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panel_1_tab" aria-controls="panel_1_tab" role="tab" data-toggle="tab">
								<com:TTranslate>DEFINE_FRANCE_METROPOLITAINE</com:TTranslate>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#panel_3_tab" aria-controls="panel_3_tab" role="tab" data-toggle="tab">
								<com:TTranslate>DEFINE_AUTRE_PAYS</com:TTranslate>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <com:TRepeater  ID="repeaterGeoN0"  onItemDataBound="itemDataBindGeoN0" >
                            <prop:ItemTemplate>
                                <!--BEGIN FRANCE METROPOLITAINE-->
                                <div id="panel_<%#$this->Data->getId()%>_tab" class="panel panel-default tab-pane fade in" role="tabpanel">
                                    <div class="panel-body">
                                        <!--BEGIN CARTE-->
										<com:TPanel cssClass="clearfix" visible="<%# ($this->Data->getType()==2) ? true : false %>">
                                            <div class="clearfix">
                                                <div class="col-md-6 col-xs-6 p-l-0">
                                                    <div id="blocMap">
                                                        <div id="canvas" >
                                                            <div id="paper"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xs-6 form-group form-group-sm m-t-8">
                                                    <!--BEGIN RADIO BTN-->
                                                    <div class="form-group form-group-sm clearfix">
                                                        <div id="localSubitemRadioGroup">
                                                            <label class="radio-inline p-l-0" for="localSubitemSelectionRadio">
                                                                <com:TRadioButton  ID="localSubitemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                   Checked="true"
                                                                                   Attributes.id="selectionByLocalSubItem"
                                                                                   Attributes.name="selectionFrance"
																				   Attributes.title="<%=Prado::localize('DEFINE_SELECTIONNER_CARTE_LOCALE')%>"
                                                                                   Attributes.onclick="displayLocalSubitemMap();"/>
                                                                <com:TTranslate>DEFINE_SELECTIONNER_CARTE_LOCALE</com:TTranslate>
                                                            </label>
                                                        </div>
                                                        <div class="p-l-3">
                                                            <div id="subitemRadioGroup">
                                                                <label class="radio-inline p-l-0" for="ctl0_CONTENU_PAGE_repeaterGeoN0_ctl0_subitemSelectionRadio">
                                                                    <com:TActiveRadioButton  ID="subitemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                             Attributes.id="selectionBySubItem"
                                                                                             Attributes.name="selectionFrance"
                                                                                             Attributes.onclick="displaySubitemMap();"/>
                                                                    <com:TTranslate>DEFINE_SELECTIONNER_CARTE_DEPARTEMENT</com:TTranslate>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="p-l-3">
                                                            <div id="itemRadioGroup">
                                                                <label class="radio-inline p-l-0" for="ctl0_CONTENU_PAGE_repeaterGeoN0_ctl0_itemSelectionRadio">
                                                                    <com:TRadioButton  ID="itemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                       Attributes.id="selectionByItem"
                                                                                       Attributes.name="selectionFrance"
                                                                                       Attributes.onclick="displayItemMap();"/>
                                                                    <com:TTranslate>DEFINE_SELECTIONNER_CARTE_REGION</com:TTranslate>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--END RADIO BTN-->
                                                    <div class="form-group form-group-sm">
                                                        <label for="ctl0_CONTENU_PAGE_repeaterGeoN0_ctl0_listeLocalisationsSelect">
                                                            <com:TTranslate>MA_SELECTION_ACTUELLE</com:TTranslate>
                                                        </label>
                                                        <com:TListBox cssClass="liste-region-selectionnes form-control" id="listeLocalisationsSelect" SelectionMode="Multiple" Attributes.name="localisations[]"></com:TListBox>
                                                        <!--select class="liste-region-selectionnes" multiple="multiple" id="listeLocalisationsSelect" name="localisations"></select-->
                                                        <div class="clearfix m-t-1">
                                                            <div class="pull-left vider-liste">
                                                                <a href="javascript:void(0);" class="btn btn-sm btn-default">
                                                                    <com:TTranslate>VIDER_MA_SELECTION_ACTUELLE</com:TTranslate>
                                                                </a>
                                                            </div>
                                                            <div class="pull-right effacer-selection">
                                                                <a href="javascript:void(0);" class="btn btn-sm btn-default">
                                                                    <com:TTranslate>EFFACER_MA_SELECTION_ACTUELLE</com:TTranslate>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </com:TPanel>
                                        <!--BEGIN CARTE-->
                                        <!--Debut line France-->
                                        <com:THiddenField ID="typeN0" Value="<%#($this->Data->getType()) %>"/>
                                        <!--Debut line Geolocalisation N0-->
                                        <div id="blockGeo<%#$this->ItemIndex%>" visible="<%# ($this->Data->getType()==2) ? false : true %>')">
                                            <!--Debut bloc Geolocalisation N0 Type 1-->
                                            <com:TPanel id="blocFrance">
                                                <com:TPanel visible = "<%# ($this->Data->getType()!=2) ? true:false%>" >
                                                    <div class="col-md-6 col-xs-6">
                                                        <label class="radio-inline p-l-0" for="ctl0_CONTENU_PAGE_repeaterGeoN0_ctl1_selectiongeoN0Aucun">
                                                            <com:TRadioButton  ID="selectiongeoN0Aucun" Checked="true" GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                               Attributes.title="<%#$this->Page->writeValue($this->Data, 'LibelleAucun')%>"
                                                                               Attributes.onclick="hideDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId()%>');javaScript:opt<%# $this->ItemIndex%>.transferAllLeft();"
                                                            />
                                                            <com:TLabel  Text="<%#$this->Page->writeValue($this->Data, 'LibelleAucun')%>" />
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <label class="radio-inline p-l-0" for="ctl0_CONTENU_PAGE_repeaterGeoN0_ctl1_selectiongeoN0Select">
                                                            <com:TRadioButton ID="selectiongeoN0Select" value="<%# $this->Data->getType() %>" GroupName="RadioGroup<%#$this->ItemIndex %>" Attributes.name="selectionFrance"
                                                                              Attributes.title="<%#$this->Page->writeValue($this->Data, 'LibelleSelectionner')%>"
                                                                              Attributes.onclick="showDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId() %>');"
                                                            />
                                                            <com:TLabel Text="<%#$this->Page->writeValue($this->Data, 'LibelleSelectionner')%>" />
                                                        </label>
                                                    </div>
                                                </com:TPanel>
                                                <!--Debut bloc Selection Geolocalisation N1 et N2-->
                                                <com:TPanel id="choixDept" Attributes.style="display:none;" Attributes.class="liste-depts">
													<table summary="<%=Prado::localize('DEFINE_LISTE_GEOLOCALISATION_N2')%>">
                                                        <com:TRepeater ID="repeaterGeoN1"  visible = "<%# ($this->Data->getType()==0) ? true:false%>">
                                                            <caption><com:TTranslate>DEFINE_LISTE_GEOLOCALISATION_N2</com:TTranslate></caption>
                                                            <prop:HeaderTemplate>
                                                                <tr>
                                                                    <th colspan="2" style="display:none;" abbr="Collectivités">DEFINE_LISTE_GEOLOCALISATION_N2</th>
                                                                </tr>
                                                            </prop:HeaderTemplate>
                                                            <prop:ItemTemplate>
                                                                <tr>
                                                                    <td>
																	<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[1])=='') ? 'none':'block'%>" >
																		<div class="title-toggle" onclick="togglePanel(this,'blocCollectivite1_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
																			<%#$this->Page->getGeo1Denomination1($this->Data[1])%>
																		</div>
																		<div id="blocCollectivite1_<%# $this->Data[1]->getId().$this->ItemIndex%>" class="clearDomTom" style="display:none;" class="clear-both">
																			<ul>
																				<li>
																					<com:TCheckBox id="region1" Attributes.title="Tous" cssClass="check"
                                                                                                   Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',1,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																					<label for="region1"><%# Prado::localize('DEFINE_TOUS') %></label>
																				</li>
																				<com:TRepeater  ID="repeaterGeoN2_1" DataSource=<%# (new Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2())->retrieveListGeolocalisation($this->Data[1]) %>>
																					<prop:ItemTemplate>
																						<li>
																							<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
																								<com:TCheckBox id="dep1" Value="<%# $this->Data->getId()%>" Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
																								<label for="<%# $this->getClientId()%>dep1">
																									<com:TLabel ID="geoN2Name_1" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
																									<com:THiddenField ID="geoN2Id_1" Value="<%# $this->Data->getId()%>"/>
																								</label>
																								<!-- info bulle -->
																								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
																									 style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? 'none':'block'%>"
																									 onmouseover="afficheBulle('infosDep1', this)"
																									 onmouseout="cacheBulle('infosDep1')"
																									 class="picto-info-intitule"
																									 alt="Info-bulle"
																									 title="Info-bulle" />
																								<div id="infosDep1" class="info-bulle"
																									 onmouseover="mouseOverBulle();"
																									 onmouseout="mouseOutBulle();">
																									<div>
																										<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
																									</div>
																								</div>
																								<!-- fin  -->
																							</com:TPanel>
																						</li>
																					</prop:ItemTemplate>
																				</com:TRepeater>
																			</ul>
																		</div>
																	</span>
																	</td>
																	<td class="<%# ($this->Page->getGeo1Denomination1($this->Data[2])=='') ? 'td-vide':''%>">
																	<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[2])=='') ? 'none':'block'%>" >
																		<div class="title-toggle" onclick="togglePanel(this,'blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
																			<%#$this->Page->getGeo1Denomination1($this->Data[2])%>
																		</div>
																		<ul id="blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
																			<li>
																				<com:TCheckBox id="region2" Attributes.title="Tous" cssClass="check"
																							   Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',2,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																				<label for="region2"><%# Prado::localize('DEFINE_TOUS') %></label>
																			</li>
																			<com:TRepeater ID="repeaterGeoN2_2" DataSource=<%# (new Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2())->retrieveListGeolocalisation($this->Data[2]) %>>
																				<prop:ItemTemplate>
																					<li>
																						<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
																							<com:TCheckBox 	id="dep2" Value="<%# $this->Data->getId()%>" Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
																							<label for="<%# $this->getClientId()%>dep2">
																								<com:TLabel ID="geoN2Name_2" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
																								<com:THiddenField ID="geoN2Id_2" Value="<%# $this->Data->getId()%>"/>
																							</label>
																							<!-- info bulle -->
																							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
																								 style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? 'none':''%>"
																								 onmouseover="afficheBulle('infosDep2', this)"
																								 onmouseout="cacheBulle('infosDep2')"
																								 class="picto-info-intitule"
																								 alt="Info-bulle"
																								 title="Info-bulle" />
																							<div id="infosDep2" class="info-bulle"
																								 onmouseover="mouseOverBulle();"
																								 onmouseout="mouseOutBulle();">
																								<div>
																									<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
																								</div>
																							</div>
																							<!-- fin  -->
																						</com:TPanel>
																					</li>
																				</prop:ItemTemplate>
																			</com:TRepeater>
																		</ul>
																	</span>
																	</td>
																	<td class="<%# ($this->Page->getGeo1Denomination1($this->Data[3])=='') ? 'td-vide':''%>">
																	<span style="display:<%# ($this->Page->getGeo1Denomination1($this->Data[3])=='') ? 'none':'block'%>" >
																		<div class="title-toggle" onclick="togglePanel(this,'blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>');">
																			<%#$this->Page->getGeo1Denomination1($this->Data[3])%>
																		</div>
																		<ul id="blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
																			<li>
																				<com:TCheckBox id="region3" Attributes.title="Tous" cssClass="check"
                                                                                               Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',3,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
																				<label for="region3"><%# Prado::localize('DEFINE_TOUS') %></label>
																			</li>
																			<com:TRepeater   ID="repeaterGeoN2_3" DataSource=<%# (new Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2())->retrieveListGeolocalisation($this->Data[3]) %>>
																				<prop:ItemTemplate>
																					<li>
																						<com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
																							<com:TCheckBox 	id="dep3" Value="<%# $this->Data->getId()%>" Attributes.title="<%# $this->Page->getGeo2Denomination1($this->Data)%>" cssClass="check" />
																							<label for="<%# $this->getClientId()%>dep3">
																								<com:TLabel ID="geoN2Name_3" Text="<%# $this->Page->getGeo2Denomination1($this->Data)%>"/>
																								<com:THiddenField ID="geoN2Id_3" Value="<%# $this->Data->getId()%>"/>
																							</label>
																							<!-- info bulle -->
																							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
																								 style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? 'none':'block'%>"
																								 onmouseover="afficheBulle('infosDep3', this)"
																								 onmouseout="cacheBulle('infosDep3')"
																								 class="picto-info-intitule"
																								 alt="Info-bulle"
																								 title="Info-bulle" />
																							<div id="infosDep3" class="info-bulle"
																								 onmouseover="mouseOverBulle();"
																								 onmouseout="mouseOutBulle();">
																								<div>
																									<com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
																								</div>
																							</div>
																							<!-- fin  -->
																						</com:TPanel>
																					</li>
																				</prop:ItemTemplate>
																			</com:TRepeater>
																		</ul>
																	</span>
																	</td>
																</tr>
															</prop:ItemTemplate>
														</com:TRepeater>
													</table>
												</com:TPanel>
												<!--Fin bloc Selection Geolocalisation N1 et N2-->
											</com:TPanel>
										</div>
										<!--Fin bloc Geolocalisation N0 Type 0-->
										<!--Debut bloc Geolocalisation N0 Type 1-->
										<com:TPanel cssClass="m-t-4">
											<!--Debut Bloc Selection par Geolocalisation N0 Type 1-->
											<com:TPanel id="choixPays" Attributes.style="display:none;" Attributes.class="clearfix">
												<div class="col-xs-5 col-md-5">
													<div class="intitule-auto">
														<label for="list1">
															<com:TTranslate>DEFINE_LISTE_PAYS</com:TTranslate>
														</label>
													</div>
													<com:TListBox
															ID="list1"
															DataSource=<%# $this->Page->retrieveListGeolocalisationType1InArray($this->Data) %>
														cssClass="liste-pays list1 form-control"
														Rows="6"
														Attributes.multiple="multiple"
														Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;"
														Attributes.title="<%# Prado::localize('DEFINE_LISTE_PAYS')%>"/>
												</div>
												<div class=" col-xs-2 col-md-2">
													<ul class="list-unstyled m-t-5 text-center">
														<li data-toggle="tooltip" title="D&eacute;placer un &eacute;l&eacute;ment &agrave; droite" >
															<com:TActiveLinkButton Attributes.name="right" Attributes.type="button"
																				   Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferRight();return false;">
																<i class="fa fa-step-forward"></i>
															</com:TActiveLinkButton>
														</li>
														<li data-toggle="tooltip" title="Tous d&eacute;placer &agrave; droite">
															<com:TActiveLinkButton Attributes.name="right" Attributes.type="button" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllRight();return false;">
																<i class="fa fa-fast-forward"></i>
															</com:TActiveLinkButton>
														</li>
														<li data-toggle="tooltip" title="D&eacute;placer un &eacute;l&eacute;ment &agrave; gauche">
															<com:TActiveLinkButton Attributes.name="left" Attributes.type="button" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;">
																<i class="fa fa-backward"></i>
															</com:TActiveLinkButton>
														</li>
														<li data-toggle="tooltip" title="Tous d&eacute;placer &agrave; gauche">
															<com:TActiveLinkButton Attributes.name="left" Attributes.type="button"  Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllLeft();return false;">
																<i class="fa fa-fast-backward"></i>
															</com:TActiveLinkButton>
														</li>
													</ul>
												</div>
												<div class="col-xs-5 col-md-5">
													<div class="">
														<label for="list2">
															<com:TTranslate>DEFINE_MA_SELECTION</com:TTranslate>
														</label>
													</div>
													<com:TListBox ID="list2"  cssClass="liste-pays form-control" Rows="6" Attributes.multiple="multiple" Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" Attributes.title="<%# Prado::localize('DEFINE_MA_SELECTION')%>"/>
												</div>

												<script type="text/javascript">
                                                    if(document.getElementById('<%# $this->list1->getClientId()%>')){
                                                        var list1Name = document.getElementById('<%# $this->list1->getClientId()%>').name;
                                                        var list2Name = document.getElementById('<%# $this->list2->getClientId()%>').name;
                                                        var opt<%# $this->ItemIndex%> = new OptionTransfer(list1Name, list2Name);
                                                        opt<%# $this->ItemIndex%>.init(document.forms[0]);
                                                        opt<%# $this->ItemIndex%>.setAutoSort(true);
                                                        opt<%# $this->ItemIndex%>.setDelimiter(",");
                                                        opt<%# $this->ItemIndex%>.saveRemovedLeftOptions("removedLeft");
                                                        opt<%# $this->ItemIndex%>.saveRemovedRightOptions("removedRight");
                                                        opt<%# $this->ItemIndex%>.saveAddedLeftOptions("addedLeft");
                                                        opt<%# $this->ItemIndex%>.saveAddedRightOptions("addedRight");
                                                        opt<%# $this->ItemIndex%>.saveNewLeftOptions("newLeft");
                                                        opt<%# $this->ItemIndex%>.saveNewRightOptions("newRight");

                                                    }
												</script>
											</com:TPanel>
											<!--Fin Bloc Selection par Geolocalisation N0 Type 1-->
										</com:TPanel>
										<!--Fin bloc Geolocalisation N0 Type 1-->
										<!--Fin line France-->
									</div>
								</div>
								<!--END FRANCE METROPOLITAINE-->
							</prop:ItemTemplate>
						</com:TRepeater>
					</div>
					<com:TPanel cssClass="clearfix m-t-2 m-l-3" visible="<%=(Application\Service\Atexo\Atexo_CurrentUser::getIdAgentConnected()!= '') ? 'true' : 'false' %>">
						<div class="pull-left">
							<label class="checkbox-inline p-l-0" for="ctl0_CONTENU_PAGE_addFavori">
								<com:TCheckBox id="addFavori" />
								<%=Prado::localize('DEFINE_ENREGISTRER_SELECTION_PREFERENCES') %>
							</label>
							<span data-toggle="tooltip" title="<com:TTranslate>DEFINE_REMPLACER_PREFERENCES_LIEUX_EXEC</com:TTranslate>" class="m-l-1"> <i class="fa fa-question-circle text-info"></i></span>
						</div>
					</com:TPanel>
					<com:THiddenField ID="idListeLocalisation" />
					<!--END LIEUX D'EXECUTION-->
				</div>
				<div class="modal-footer">
					<div class="clearfix">

						<div class="pull-left">
							<com:TButton Text="Annuler" Attributes.onClick="window.close();" Cssclass="btn btn-sm btn-default btn-reset" />
						</div>

						<div class="pull-right">
							<com:TButton ID="validateButton" Text="Valider" OnClick="retrieveAllSelectedGeo" Cssclass="btn btn-primary btn-sm" />
						</div>

						<com:TLabel ID="script"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<com:TLabel ID="script2"/>
	<com:TLabel ID="script3"/>
	<script type="text/javascript" >
        var dept = false;
        var pays = false;
        if(typeof(checked_loc) != 'undefined' && checked_loc != ''){
            var checked_loc_tab = checked_loc.split(',');
            J(".check").each(function() {
                if(checked_loc_tab.indexOf(J( this ).prop('value')) != -1){
                    J( this ).prop('checked', true);
                    dept = true;
                }
            });
            J(".list1 > option").each(function() {
                if(checked_loc_tab.indexOf(this.value) != -1){
                    J(".list1 option[value='" +this.value+ "']").prop('selected', true);
                    pays = true;
                }
            });
            opt2.transferRight();
        }
        if(dept == true ){
            J('input[id$="_selectiongeoN0Select"]').filter( function(){
                if(J( this ).prop('value') == 0){
                    J( this ).prop('checked', true);
                    J('div[id$="_choixDept"]').filter( function(){
                        showDiv(this.id);
                    });
                }
            });
        }
        if(pays == true){
            J('input[id$="_selectiongeoN0Select"]').filter( function(){
                if(J( this ).prop('value') == 1){
                    J( this ).prop('checked', true);
                    J ('div[id$="_choixPays"]').filter( function(){
                        showDiv(this.id);
                    });
                }
            });
        }
	</script>
	<script>
        J('#panel_1_tab').addClass('active');
	</script>
</com:TContent>
