<com:TContent ID="CONTENU_PAGE">
    <!--Debut colonne droite-->
    <div class="outils-informatiques">
        <div class="main-part" id="main-part">
            <!--Debut Bloc logiciels Chiffrement et Signature-->
            <div class="clearfix">
                <ul class="breadcrumb">
                    <li>
                        <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                    </li>
                    <li>
                        <com:TTranslate>DEFINE_TEXT_AIDE</com:TTranslate>
                    </li>
                    <li>
                        <com:TTranslate>TEXT_OUTILS_INFORMATIQUES</com:TTranslate>
                    </li>
                </ul>
            </div>
            <!-- Debut Tableau Bootstrap -->
            <table class="table table-striped outils-table">
                <thead>
                <tr>
                    <th>
                        <com:TTranslate>TEXT_CHIFFREMENT_SIGNATURE</com:TTranslate>
                    </th>

                    <th class="text-center">
                        <com:TTranslate>TELECHARGEMENT</com:TTranslate>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-9"><a href="http://www.java.com/fr/" target="_blank">
                        <com:TTranslate>MACHINE_VIRTUELLE_JAVA</com:TTranslate>
                    </a>
                    </td>
                    <td class="col-md-3 text-center"><a href="http://www.java.com/fr/" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/java.gif" alt="logo Java"
                                                                                                            title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a></td>
                </tr>
                <tr>
                    <td class="col-md-9"><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_DOCS_OUTILS');%>/ATEXO-Sign.zip" target="_blank">
                        <com:TTranslate>UTILITAIRE_ATEXO_SIGN</com:TTranslate>
                        (6.56 Mo)</a></td>
                    <td class="col-md-3 text-center"><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_DOCS_OUTILS');%>/ATEXO-Sign.zip" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-signature-big.gif"
                                                                                                                                                       alt="logo ATEXO-Sign"
                                                                                                                                                       title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- Fin Tableau Bootstrap -->
            <!-- Debut Tableau Bootstrap -->
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        <com:TTranslate>TEXT_OUTILS_BUREAUTIQUE</com:TTranslate>
                    </th>
                    <th class="text-center">
                        <com:TTranslate>TELECHARGEMENT</com:TTranslate>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-9"><a href="http://www.7-zip.org/download.html" target="_blank">
                        <com:TTranslate>FILE_COMPRESSION_DECOMPRESSION_ZIP</com:TTranslate>
                    </a></td>
                    <td class="col-md-3 text-center"><a href="http://www.7-zip.org/download.html" target="_blank">
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-7zip.png" alt="Logo 7-zip" title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/>
                    </a></td>
                </tr>
                <tr>
                    <td class="col-md-9"><a href="http://www.adobe.com/fr/" target="_blank">
                        <com:TTranslate>FILE_LECTURE_DOCUMENT_PDF</com:TTranslate>
                    </a></td>
                    <td class="col-md-3 text-center"><a href="http://www.pdfforge.org/products/pdfcreator/download" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/doc-pdf.gif" alt="Logo PDF"
                                                                                                                                         title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-9"><a href="http://www.pdfforge.org/products/pdfcreator/download" target="_blank">
                        <com:TTranslate>FILE_GENERATION_DOCUMENT_PDF</com:TTranslate>
                    </a></td>
                    <td class="col-md-3 text-center"><a href="http://www.pdfforge.org/products/pdfcreator/download" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/doc-pdf.gif" alt="Logo PDF"
                                                                                                                                         title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-9"><a href="http://fr.libreoffice.org/" target="_blank">
                        <com:TTranslate>SUITE_OPEN_OFFICE</com:TTranslate>
                    </a></td>
                    <td class="col-md-3 text-center"><a href="http://fr.libreoffice.org/" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-libre-office.png" alt="Logo LibreOffice"
                                                                                                               title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a></td>
                </tr>
                <tr>
                    <td class="col-md-9"><a href="http://www.autodesk.com/designreview-download" target="_blank">
                        <com:TTranslate>TEXT_LECTURE_DOC_DWF</com:TTranslate>
                    </a></td>
                    <td class="col-md-3 text-center"><a href="http://www.autodesk.com/designreview-download" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dwf.gif" alt="Logo DWF"
                                                                                                                                  title="<com:TTranslate>TELECHARGEMENT</com:TTranslate>"/></a></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</com:TContent>