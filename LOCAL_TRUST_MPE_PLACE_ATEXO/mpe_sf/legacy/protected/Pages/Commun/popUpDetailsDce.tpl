<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent id="CONTENU_PAGE">
<div class="popup-moyen" id="container">
<com:TPanel CssClass="main-part" ID="mainPart">
	<h1><com:TTranslate>DETAIL_PIECE</com:TTranslate> </h1>
		<com:TPanel ID="errorPart" visible="false">
			<com:PanelMessageErreur ID="panelMessageErreur" visible="false"/>
		</com:TPanel>
		<com:AtexoValidationSummary ValidationGroup="ValidateInfoUser" />
		<com:EntrepriseConsultationSummary />
	
		<com:TPanel   id="panelEntrepriseDownloadDce" visible="true">
			<com:EntrepriseDownloadDce   id="EntrepriseDownloadDce" BaseDce = "true" />
		</com:TPanel>
		<div class="boutons-line">
			<com:TActiveButton CssClass="bouton-moyen-120 float-left" Text="<%=Prado::localize('FERMER')%>" Attributes.title="<%=Prado::localize('FERMER')%>" Attributes.onclick="window.close();" />
		</div>
</com:TPanel>
<!--Fin colonne droite-->
</div>	
<script type="text/javascript">popupResize();</script>
</com:TContent>
