<com:TContent ID="CONTENU_PAGE">
<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<div class="breadcrumbs"><a href="#"><com:TTranslate>DEFINE_TEXT_CONSULTATIONS</com:TTranslate></a>&gt; <com:TTranslate>DEFINE_TEXT_ACCUSE_RECEPTION</com:TTranslate></div>
			<com:TPanel id="contenuEchange">
				<com:PanelMessage ID="messageconfirm" />
				<com:DetailEchangeMail ID="accusereception" CalledFrom="entreprise"/>
			</com:TPanel>
			<com:PanelMessageErreur id="errorMsg" Visible="false"/>
		<div class="breaker"></div>
        <div class="link-line">
            <!--
		    <com:THyperLink 
				ID="idretourConsultation"  
				visible = "false";
				CssClass="bouton-retour"
				Attributes.title="<%=Prado::localize('TEXT_RETOUR_CONSULTATION')%>" 
				Text="<%=Prado::localize('TEXT_RETOUR_CONSULTATION')%>"
			/>
			  -->
			<com:THyperLink 
				ID="idretour"  
				CssClass = "bouton-suivant"
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_RETOUR_ACCUEIL')%>" 
				Text="<%=Prado::localize('DEFINE_TEXT_RETOUR_ACCUEIL')%>"
			/>
		</div>
        <div class="link-line">
		   <com:TButton  
				CssClass="bouton-imprimer float-left" 
				Attributes.value="<%=Prado::localize('DEFINE_TEXT_IMPRESSION_MESSAGE')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_IMPRESSION_MESSAGE')%>"
				Attributes.onclick="javascript:window.print();" 
			/>
		</div>
	</div>
	<!--Fin colonne droite-->
</com:TContent>