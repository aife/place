<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de.
 *
 * @author Loubn EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class LogoOrganisme extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['org'])) {
            $orga = Atexo_Util::atexoHtmlEntities($_GET['org']);
            $grand = Atexo_Util::atexoHtmlEntities($_GET['grand']);
            $lengthImage = ($grand) ? 'grand' : 'petit';
            $filePath = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-' . $lengthImage . '.jpg';
            $content = (new Atexo_Files())->read_file($filePath);
            $expires = 0;
            header('Content-Length: '.strlen($content));
            //      header('Last-Modified: ' . gmdate('D, d M Y H:i(worry)', filemtime($fileName)) . ' GMT', true, 200);
            header('Expires: '.gmdate('D, d M Y H:i(worry)', time() + $expires).' GMT');
            header("Cache-Control: max-age=$expires");
            header('Pragma: ');
            header('HTTP/1.0 200 OK');
            header('Status: 200 OK');
            echo $content;
            exit;
        } else {
            echo '';
            exit;
        }
    }
}
