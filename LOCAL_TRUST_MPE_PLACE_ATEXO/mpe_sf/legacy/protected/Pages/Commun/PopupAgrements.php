<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class PopupAgrements extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue(Atexo_Util::atexoHtmlEntities($_GET['callFrom']));
    }

    public function onload($param)
    {
        if (!$this->isPostBack) {
            $this->fillRepeaterAgrement();
            if ($_GET['ids']) {
                $this->displayAgrementChecked(Atexo_Util::atexoHtmlEntities($_GET['ids']));
            }
        }
    }

    /***
     * Remplir le repeater des Agrements à afficher
     */
    public function fillRepeaterAgrement()
    {
        $listAgrements = (new Atexo_Agrement())->getAllAgrement();
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if ($listAgrements) {
            foreach ($listAgrements as $agrement) {
                $getLibelleTraduit = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$agrement->$getLibelleTraduit()) {
                    $agrement->setLibelle($agrement->getLibelle());
                } else {
                    $agrement->setLibelle($agrement->$getLibelleTraduit());
                }
            }
        }
        $this->repeaterAgrement->DataSource = $listAgrements;
        $this->repeaterAgrement->dataBind();
    }

    public function getAgrementChecked()
    {
        $idsAgrements = '';
        $atLeastOneChecked = false;
        foreach ($this->repeaterAgrement->items as $item) {
            if ($item->checkBoxAgrement->checked) {
                $atLeastOneChecked = true;
                $idsAgrements .= ','.$this->repeaterAgrement->DataKeys[$item->ItemIndex];
            }
        }
        if ($atLeastOneChecked) {
            $idsAgrements .= ',';
        }
        $this->script->Text = "<script> 
								returnAgrements('".$idsAgrements."','".$_GET['clientId']."');
							 </script>";
    }

    /****
    * cocher les agrements déja cochées lors de creation
    * @param les ids des agrements $ids
    */

    public function displayAgrementChecked($ids)
    {
        $idAgrements = base64_decode($ids);
        $arIds = explode(',', $idAgrements);
        foreach ($this->repeaterAgrement->items as $item) {
            if (in_array($this->repeaterAgrement->DataKeys[$item->ItemIndex], $arIds)) {
                $item->checkBoxAgrement->checked = true;
            }
        }
    }
}
