<com:TContent ID="CONTENU_PAGE">
	<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<div class="breadcrumbs">&gt; <com:TTranslate>TEXT_ACCUEIL</com:TTranslate></div>
		
		<!-- Debut messages validation -->
		<com:AtexoValidationSummary id="divValidationSummary" ValidationGroup="validateUpdatePsw" />
		<!-- Fin messages validation -->
		<!-- Debut messages de confirmation -->
		<com:TPanel id="panelMessage"  visible="false">
			<com:PanelMessageConfirmation id="panelConfigmation" />
			<div class="spacer"></div>
		</com:TPanel>
		<!-- Fin messages de confirmation -->
		<!--Debut Bloc Modification du mot de passe-->
		<com:TPanel id="updateMdp" cssClass="form-field" >
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div class="top-title">
					<h2 class="big float-left"><com:TTranslate>TEXT_MOT_PASSE_OUBLIE</com:TTranslate></h2>
					<div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
					<div class="breaker"></div>
					<p><com:TTranslate>VEUILLEZ_RENSEIGNER_LES_CHAMPS</com:TTranslate> :</p>
				</div>
				<!--Debut Bloc Login Identifiant Mot de passe-->
				<div class="form-bloc">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<div class="line">
							<div class="intitule-150"><label for="password"><com:TTranslate>TEXT_NOUVEAU_MOT_PASSE</com:TTranslate></label><span class="champ-oblig">* </span> :</div>
							<com:TTextBox
									id="password"
									PersistPassword="true"
									TextMode="Password"
									Attributes.title="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
									cssClass="input-185"
							/>
							<a class="picto-info-intitule" onblur="cacheBulle('infosPassword')" onfocus="afficheBulle('infosPassword', this)" onmouseout="cacheBulle('infosPassword')" onmouseover="afficheBulle('infosPassword', this)" href="javascript:void(0);" style="display: <%=$this->validerFormatPassWord()? '' : 'none'%>;">
								<img alt="Info-bulle" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
							</a>
							<com:TRequiredFieldValidator
									ControlToValidate="password"
									ValidationGroup="validateUpdatePsw"
									Display="Dynamic"
									ErrorMessage="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
									EnableClientScript="true"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TRequiredFieldValidator>
							<com:TCustomValidator
									ValidationGroup="validateUpdatePsw"
									ControlToValidate="password"
									ClientValidationFunction="verifierMdp"
									ErrorMessage="<%=Prado::localize('REGLE_VALIDATION_MDP')%>"
									visible="<%=$this->validerFormatPassWord()%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
							<div id="infosPassword" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
								<div><com:TTranslate>REGLE_VALIDATION_MDP_INFO_BULLE</com:TTranslate></div>
							</div>
						</div>
						<!--  -->
						<div class="line">
							<div class="intitule-150"><label for="confPassword"><com:TTranslate>TEXT_CONFIRMATION_MOT_PASSE</com:TTranslate></label><span class="champ-oblig">* </span> :</div>
							<com:TTextBox
									id="confPassword"
									TextMode="Password"
									PersistPassword="true"
									Attributes.title="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
									cssClass="input-185"
							/>
							<com:TRequiredFieldValidator
									ControlToValidate="confPassword"
									ValidationGroup="validateUpdatePsw"
									Display="Dynamic"
									ErrorMessage="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
									EnableClientScript="true"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TRequiredFieldValidator>
							<com:TCompareValidator
									ValidationGroup="validateUpdatePsw"
									ID="confPasswordCompareValidator"
									ControlToValidate="password"
									ControlToCompare="confPassword"
									DataType="String"
									Display="Dynamic"
									Operator="Equal"
									ErrorMessage="<%=Prado::localize('TEXT_DEUX_PASSWORD_DIFF')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCompareValidator>
						</div>
						<div class="line">
							<div class="intitule-150"><label for="emailPersonnel"><com:TTranslate>DEFINE_ADRESSE_ELECTRONIQUE</com:TTranslate></label> <span class="champ-oblig">* </span> :</div>
							<com:TTextBox id="emailPersonnel" Attributes.title="<%=Prado::localize('DEFINE_ADRESSE_ELECTRONIQUE')%>"  cssClass="input-185" />
							<com:TCustomValidator
									ControlToValidate="emailPersonnel"
									ValidationGroup="validateUpdatePsw"
									Display="Dynamic"
									EnableClientScript="true"
									ID="emailValidator"
									ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
									ClientValidationFunction="validatorEmail"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span> ">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
							<com:TCustomValidator
									ControlToValidate="emailPersonnel"
									ValidationGroup="validateUpdatePsw"
									Display="Dynamic"
									ID="emailPersonnelValidator"
									EnableClientScript="false"
									onServerValidate="verifyEmail"
									Text="<span  title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
							</com:TCustomValidator>

							<com:TLinkButton CssClass="ok" onClick="savePassword" id="save" ValidationGroup="validateUpdatePsw">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif" id="btn_valider"  alt="<%=Prado::localize('OK')%>"/>
							</com:TLinkButton>
						</div>
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>
				<!--Fin Bloc Login Identifiant Mot de passe-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Modification du mot de passe-->
	</div>
	<!--Fin colonne droite-->
	<com:TLabel id="script" />
</com:TContent>