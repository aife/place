<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
<com:TLabel ID="error" Visible="false">
	<div class="popup-moyen" id="container">
		<com:PanelMessageErreur ID="panelMessageErreurModule" Visible="false"/>
	</div>
</com:TLabel>
<com:TLabel ID="noError">
	<div class="popup-moyen" id="container">
		<h1><com:TTranslate>TEXT_AJOUTER_MODIFIER_PIECES_JOINTES</com:TTranslate></h1>
		<com:UploadPieceJointe ID="IdUploadPj"/>
	</div>
</com:TLabel>
<script type="text/javascript">popupResize();</script>
</com:TContent>