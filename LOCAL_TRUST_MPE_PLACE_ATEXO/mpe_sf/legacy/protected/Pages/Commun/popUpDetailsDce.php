<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TiersAuthentification;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class popUpDetailsDce extends MpeTPage
{
    public $langueEnSession;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['ticket']) && isset($_GET['fonctionnalite'])) {
            $valide = (new Atexo_TiersAuthentification())->VerifyToken(Atexo_Util::atexoHtmlEntities($_GET['ticket']));
            if ($valide) {
                $globalization = $this->getApplication()->Modules['globalization'];
                if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
                    $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();
                    $sessionLang = Atexo_CurrentUser::readFromSession('lang');
                    if (isset($_GET['lang']) && in_array($_GET['lang'], $arrayActiveLangages)) {
                        $this->langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
                    } elseif (isset($sessionLang) && in_array($sessionLang, $arrayActiveLangages)) {
                        $this->langueEnSession = $sessionLang;
                    } else {
                        $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                    }
                } else {
                    $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                }
                // Enregistrement en session
                Atexo_CurrentUser::writeToSession('lang', $this->langueEnSession);

                if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
                    $globalization->setTranslationCatalogue('messages.'.$this->langueEnSession);
                } else {
                    $globalization->setTranslationCatalogue('messages.fr');
                }
                $this->dataBind();
            } else {
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TICKET_INVALIDE'));
            }
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TICKET_OU_FONCTIONNALITE_INTROUVABLE'));
        }
    }
}
