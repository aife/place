<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
<div class="popup-moyen" id="container">
	<h1><com:TTranslate>TEXT_AGREMENTS_MAJUSCULE</com:TTranslate></h1>	
	<form  id="infosUtilisateur">
	<!--Debut Bloc Agréments-->
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
			<com:TRepeater ID="repeaterAgrement"  DataKeyField="Id">
			  <prop:ItemTemplate>
				<!-- -->
				<div class="content-bloc bloc-350 indent-20" >
					<span class="check-bloc"><com:TCheckBox id="checkBoxAgrement" Attributes.title="<%#$this->Data->getLibelle()%>"  /></span>
					<div class="content-bloc bloc-320"><label for="<%#$this->checkBoxAgrement->getClientId()%>"><%#$this->Data->getLibelle()%></label></div>
				</div>
				<!-- -->
			 </prop:ItemTemplate>	
		  </com:TRepeater>	
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
	<!--Fin Bloc Agréments-->
	
	<!--Debut line boutons-->
	<div class="boutons-line"> 
		<input type="button" class="bouton-annulation float-left" value="<%=Prado::localize('TEXT_ANNULER')%>" title="<%=Prado::localize('TEXT_ANNULER')%>" onclick="window.close();" />
		<com:TButton cssClass="bouton-validation float-right" Text="<%=Prado::localize('TEXT_VALIDER')%>" Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>" onclick="getAgrementChecked" />
	</div>
	<!--Fin line boutons-->
	</form>
</div>
<script type="text/javascript">popupResize();</script>
<com:TLabel ID="script"/>
</com:TContent>
