<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CalendrierPdf;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class DownloadCalendrierPdf extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['id']) {
            $lang = Atexo_CurrentUser::readFromSession('lang');
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
            if (isset($_GET['reel'])) {
                (new Atexo_CalendrierPdf())->generateCalendrierPrevisionel($consultation, Atexo_Config::getParameter('CHEMIN_MODELE_CALENDRIER_REEL'), $lang, Atexo_Config::getParameter('NOM_FICHIER_CALENDRIER_REEL'));
            } else {
                (new Atexo_CalendrierPdf())->generateCalendrierPrevisionel($consultation, Atexo_Config::getParameter('CHEMIN_MODELE_CALENDRIER'), $lang, Atexo_Config::getParameter('NOM_FICHIER_CALENDRIER_PREVISIONNEL'));
            }
        }
    }
}
