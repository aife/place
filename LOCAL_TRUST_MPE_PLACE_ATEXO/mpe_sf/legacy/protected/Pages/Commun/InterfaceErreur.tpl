<com:TContent ID="CONTENU_PAGE">
	<com:TPanel CssClass="main-part" ID="errorPart">
		<div class="breadcrumbs"><a href="#"><com:TTranslate>TEXT_CONNECTEURS</com:TTranslate></a>&gt; <com:TTranslate>TEXT_MESSAGES_ERREURS</com:TTranslate></div>
		<com:PanelMessageErreur ID="panelMessageErreur"/>
	</com:TPanel>
</com:TContent>