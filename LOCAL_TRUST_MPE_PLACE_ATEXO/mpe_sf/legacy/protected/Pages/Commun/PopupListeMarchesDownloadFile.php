<?php

namespace Application\Pages\Commun;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonFichiersListeMarchesPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger le document.
 *
 * @author Mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupListeMarchesDownloadFile extends DownloadFile
{
    private string $_identifiant = '';
    private $_organisme = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['id'])) {
            $this->_identifiant = Atexo_Util::atexoHtmlEntities($_GET['id']);
            if (isset($_GET['org']) && '' != $_GET['org']) {
                $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
            } else {
                $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
            }
            $this->downloadPiece();
        }
    }

    public function downloadPiece()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $piece = CommonFichiersListeMarchesPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexionCom);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }
}
