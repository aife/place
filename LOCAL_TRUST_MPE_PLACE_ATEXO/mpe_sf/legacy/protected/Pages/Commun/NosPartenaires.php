<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonPartenairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 8 juin 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class NosPartenaires extends MpeTPage
{
    public $langue;

    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
    }

    public function onLoad($param)
    {
        self::displayPartenaire();
    }

    public function displayPartenaire()
    {
        $Partenaires = self::retreiveInfoPartenaire();
        $this->repeaterPartenaires->DataSource = $Partenaires;
        $this->repeaterPartenaires->DataBind();
    }

    public function retreiveInfoPartenaire()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $listePartenaires = BaseCommonPartenairePeer::doSelect($c, $connexionCom);
        if ($listePartenaires) {
            return $listePartenaires;
        } else {
            return [];
        }
    }
}
