<?php

namespace Application\Pages\Commun;

use App\Traits\ConfigProxyTrait;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use phpseclib\File\X509;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Affichage de la liste des certificats valides par le gouvernement
 * en ce basant sur le fichier de reference gouvernemental :
 * https://references.modernisation.gouv.fr/sites/default/files/TSL-FR.xml.
 *
 * @since 4.6.0
 *
 * @author Gregory CHEVRET<gregory.chevret@atexo.com>
 * @author Loubna  EZZIANI<loubna.ezziani@atexo.com>
 * @copyright ATEXO 2014
 */
class ListeAcRGS extends MpeTPage
{
    use ConfigProxyTrait;

    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('ListeACRgs')) {
            if (!$this->isPostBack) {
                $this->displayListeCertificat();
            }
        } else {
            $this->response->redirect('agent');
        }
    }

    /**
     * Permet de telecharger le certificat.
     *
     * @return void
     *
     * @since 4.6.0
     *
     * @author Loubna  EZZIANI<loubna.ezziani@atexo.com>
     * @copyright ATEXO 2014
     */
    public function downloadCertificat($sender, $param)
    {
        $params = explode('#', $sender->CommandParameter);
        if (is_array($params)) {
            $dataSource = $this->getViewState('certificatDataSource');
            $indexPere = $params[0];
            $index = $params[1];
            $certificat = $dataSource[$indexPere]['ACFilles'][$index]['Certificate'];
            $nameCertif = $dataSource[$indexPere]['ACFilles'][$index]['CN'];
            DownloadFile::downloadFileContent($nameCertif.'.cer', $certificat);
        }
    }

    /**
     * Permet d'afficher la liste des certificats RGS
     * a partir du fichier de reference :
     * https://references.modernisation.gouv.fr/sites/default/files/TSL-FR.xml.
     *
     * @return void
     *
     * @since 4.6.0
     *
     * @author Gregory CHEVRET<gregory.chevret@atexo.com>
     * @author Loubna  EZZIANI<loubna.ezziani@atexo.com>
     * @copyright ATEXO 2014
     */
    public function displayListeCertificat()
    {
        $xml_str = null;
        $cacheXmlFile = Atexo_Config::getParameter('DOCUMENT_ROOT_DIR').'/protected/var/cache/tsl_fr_cache.xml';
        $cacheDataFile = Atexo_Config::getParameter('DOCUMENT_ROOT_DIR').'/protected/var/cache/tsl_fr_atx_data.cache';
        if (!file_exists($cacheXmlFile) || strtotime('now') >= strtotime('+'.Atexo_Config::getParameter('NBR_JOURS_AFFICHAGE_RGS').' DAY', filemtime($cacheXmlFile))) {
            $this->unlinkCacheFile($cacheXmlFile, $cacheDataFile);
        }
        $dataSource = unserialize(file_get_contents($cacheDataFile));
        if (!file_exists($cacheDataFile) || (file_exists($cacheDataFile) && empty($dataSource))) {
            if (!file_exists($cacheXmlFile) || (file_exists($cacheXmlFile) && ($xml_str = file_get_contents($cacheXmlFile)) && empty($xml_str))) {
                $client = new Client();
                $configProxy = self::getConfigProxyForLaminasHttpClient(Atexo_Util::getSfContainer());
                if ($configProxy) {
                    $client->setOptions($configProxy);
                }
                $client->setUri(Atexo_Config::getParameter('LISTE_AC_RGS_TSL_FR'));
                $client->setMethod(Request::METHOD_GET);
                $xml_str = $client->send();
            }
            if (simplexml_load_string($xml_str) instanceof \SimpleXMLElement) {
                $xml = new \SimpleXMLElement($xml_str);
                $dataSource = [];
                include_once '../../../../vendor/phpseclib/phpseclib/phpseclib/File/X509.php';
                $cptACRac = 0;
                $blackListCertificate = [
                    //BANQUE POPULAIRE - AUTORITE DE CERTIFICATION - V2 | BANQUE POPULAIRE - CLICK AND TRUST - TVA - V2
                    //'1492356859923958345537647725834901842215489',
                    //ChamberSign France | ChamberSign France - AC 1 etoile
                    //'1491938635963497667980446097248444678330265',
                    //CERTIFICATION AUTHORITY-CLICK AND TRUST | SIGN-SOFTWARE-CLICK AND TRUST
                    //'36182215007811347660694623157364479652543',
                    //CERTIFICATION AUTHORITY-CLICK AND TRUST | AUTH-SOFTWARE-CLICK AND TRUST
                    //'1491945331460888690230637893086288295305982',
                    //Cryptolog International - Autorit? de certification pour l'horodatage Universign
                    //Cryptolog International - Horodatage Universign
                    '31158342327722195646521991179711490984',
                    //Dhimyotis - Certigna Cachet Serveur
                    '30',
                    //Dhimyotis - Certigna ID PRIS ** PRO
                    //'29',
                    //Dhimyotis - Certigna ID PRIS *** PRO
                    //'24',
                    //Dhimyotis - Certigna ID PRIS ***
                    //'23',
                    //Dhimyotis - Certigna ID PRIS **
                    //'28',
                    //Dhimyotis - Certigna ID PRIS Pro
                    //'22',
                    //Dhimyotis - Certigna ID PRIS
                    //'21',
                ];

                foreach ($xml->TrustServiceProviderList->TrustServiceProvider as $k => $trustServiceProvider) {
                    //Information AC Racine
                    $ACRacine = [];
                    foreach ($trustServiceProvider->TSPInformation->TSPName->Name as $name) {
                        if ('fr' == $name->attributes('xml', true)->lang) {
                            $ACRacine['TSPName'] = (string) $name;
                        }
                    }
                    if (empty($ACRacine['TSPName'])) {
                        $ACRacine['TSPName'] = (string) $trustServiceProvider->TSPInformation->TSPName->Name[0];
                    }

                    foreach ($trustServiceProvider->TSPInformation->TSPTradeName->Name as $name) {
                        if ('fr' == $name->attributes('xml', true)->lang) {
                            $ACRacine['TSPTradeName'] = (string) $name;
                        }
                    }
                    if (empty($ACRacine['TSPTradeName'])) {
                        $ACRacine['TSPTradeName'] = (string) $trustServiceProvider->TSPInformation->TSPTradeName->Name[0];
                    }

                    foreach ($trustServiceProvider->TSPInformation->TSPAddress->ElectronicAddress->URI as $uri) {
                        if ('fr' == $uri->attributes('xml', true)->lang) {
                            if (preg_match('#mailto#', $uri)) {
                                $ACRacine['ElectronicAddress'] = (string) $uri;
                            } else {
                                $ACRacine['InformationURI'] = (string) $uri;
                            }
                        }
                    }

                    $ACRacine['Index'] = "ACR_$cptACRac";
                    //$ACFilles = array();
                    $cptACFille = 0;
                    foreach ($trustServiceProvider->TSPServices->TSPService as $key => $service) {
                        $ACFille = [];
                        $ACFille['typeIdentifier'] = (string) $service->ServiceInformation->ServiceTypeIdentifier;

                        foreach ($service->ServiceInformation->ServiceName->Name as $serviceName) {
                            if ('fr' == $serviceName->attributes('xml', true)->lang) {
                                $ACFille['ServiceName'] = (string) $serviceName;
                            }
                        }
                        if (empty($ACFille['ServiceName'])) {
                            $ACFille['ServiceName'] = (string) $service->ServiceInformation->ServiceName->Name[0];
                        }

                        $x509 = new X509();
                        $X509Certificate = '-----BEGIN CERTIFICATE-----'.PHP_EOL.((string) $service->ServiceInformation->ServiceDigitalIdentity->DigitalId->X509Certificate).PHP_EOL.'-----END CERTIFICATE-----'.PHP_EOL;
                        if ($x509->loadX509($X509Certificate)) {
                            if (!in_array((string) $x509->currentCert['tbsCertificate']['serialNumber'], $blackListCertificate)) {
                                $ACFille['SerialNumber'] = (string) $x509->currentCert['tbsCertificate']['serialNumber'];
                                $cn = $x509->getDNProp('CN');
                                $ACFille['Certificate'] = $X509Certificate;
                                $ACFille['CN'] = (string) $cn[0];
                                $CommonName = $x509->getIssuerDNProp('id-at-commonName');
                                $ACFille['CommonName'] = (string) $CommonName[0];
                                $ACFille['ValidityNotBefore'] = date('d/m/Y', strtotime($x509->currentCert['tbsCertificate']['validity']['notBefore']['utcTime']));
                                $ACFille['ValidityNotAfter'] = date('d/m/Y', strtotime($x509->currentCert['tbsCertificate']['validity']['notAfter']['utcTime']));
                                $CrlDistributionPoints = $x509->_getExtension('id-ce-cRLDistributionPoints');
                                $ACFille['CrlDistributionPoints'] = [];
                                if (!empty($CrlDistributionPoints) && is_array($CrlDistributionPoints)) {
                                    foreach ($CrlDistributionPoints as $CrlDistributionPoint) {
                                        $ACFille['CrlDistributionPoints'][] = $CrlDistributionPoint['distributionPoint']['fullName'][0]['uniformResourceIdentifier'];
                                    }
                                } else {
                                    $ACFille['CrlDistributionPoints'][] = ''; //A verifier
                                }

                                foreach ($service->ServiceInformation->TSPServiceDefinitionURI->URI as $CertificatePolicyURI) {
                                    if ('fr' == $CertificatePolicyURI->attributes('xml', true)->lang) {
                                        $CertificatPoliciesURIDefault = (string) $CertificatePolicyURI;
                                    }
                                }
                                if (empty($ACFille['ServiceName'])) {
                                    $CertificatPoliciesURIDefault = (string) $service->ServiceInformation->TSPServiceDefinitionURI->URI[0];
                                }

                                $CertificatPolicies = $x509->_getExtension('id-ce-certificatePolicies');
                                $ACFille['CertificatPolicies'] = [];
                                if (!empty($CertificatPolicies) && is_array($CertificatPolicies)) {
                                    foreach ($CertificatPolicies as $k => $CertificatPolicy) {
                                        $ACFille['CertificatPolicies'][$k]['URI'] = (!empty($CertificatPolicy['policyQualifiers'][0]['qualifier']['ia5String'])) ? $CertificatPolicy['policyQualifiers'][0]['qualifier']['ia5String'] : $CertificatPoliciesURIDefault;
                                        $ACFille['CertificatPolicies'][$k]['PolicyIdentifier'] = $CertificatPolicy['policyIdentifier'];
                                        if (array_key_exists($ACFille['CertificatPolicies'][$k]['policyIdentifier'], $x509->oids)) {
                                            $ACFille['CertificatPolicies'][$k]['PolicyIdentifier'] .= '('.$x509->oids[$ACFille['CertificatPolicies'][$k]['PolicyIdentifier']].')';
                                        } elseif ($value = array_search($ACFille['CertificatPolicies'][$k]['PolicyIdentifer'], $x509->oids)) {
                                            $ACFille['CertificatPolicies'][$k]['PolicyIdentifier'] .= '('.$value.')';
                                        }
                                    }
                                } else {
                                    $ACFille['CertificatPolicies'][]['URI'] = $CertificatPoliciesURIDefault; //A verifier
                                    $ACFille['CertificatPolicies'][]['PolicyIdentifier'] = '';
                                }
                                $ACFille['IndexPere'] = $ACRacine['Index'];
                                $ACFille['Index'] = $ACRacine['Index']."_ACFille_$cptACFille";
                                $ACRacine['ACFilles'][$ACFille['Index']] = $ACFille;
                            }
                            ++$cptACFille;
                        }
                    }

                    $dataSource[$ACRacine['Index']] = $ACRacine;
                    ++$cptACRac;
                }
                file_put_contents($cacheDataFile, serialize($dataSource));
            } else {
                $dataSource = [];
                $this->unlinkCacheFile($cacheXmlFile, $cacheDataFile);
            }
        }

        $this->setViewState('certificatDataSource', $dataSource);
        $this->repeaterRacineCertifRGS->DataSource = $dataSource;
        $this->repeaterRacineCertifRGS->DataBind();
    }

    public function unlinkCacheFile($cacheDataFile, $cacheXmlFile)
    {
        if (file_exists($cacheDataFile)) {
            unlink($cacheDataFile);
        }
        if (file_exists($cacheXmlFile)) {
            unlink($cacheXmlFile);
        }
    }
}
