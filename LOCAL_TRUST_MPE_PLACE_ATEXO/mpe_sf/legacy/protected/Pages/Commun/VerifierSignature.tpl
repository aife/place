<com:TContent ID="CONTENU_PAGE">

    <div class="verifier-signature">
        <div class="main-part" id="main-part">

            <div class="clearfix">
                <ul class="breadcrumb">
                    <li>
                        <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                    </li>
                    <li>
                        <com:TTranslate>DEFINE_TEXT_OUTILS_DE_SIGNATURE</com:TTranslate>
                    </li>
                    <li>
                        <com:TTranslate>VERIFIER_SIGNATURE</com:TTranslate>
                    </li>
                </ul>
            </div>

            <com:TActivePanel id="activePanelErreur">
                <com:PanelMessageErreur ID="panelMessageErreur"/>
            </com:TActivePanel>
            <com:AtexoValidationSummary id="validerAjoutDoc" ValidationGroup="validerAjoutDoc"/>

            <!--Debut Bloc Verification signature-->
            <!--img ID="loading" src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/loader.gif' alt="<%=Prado::localize('CHARGEMENT_APPLET')%>" title="<%=Prado::localize('CHARGEMENT_APPLET')%>" /-->
            <h1 class="h2 m-t-0 m-b-2 page-header">
                <com:TTranslate>VERIFIER_SIGNATURE</com:TTranslate>
            </h1>
            <div class="form-field">
                <div class="content">
                    <div class="m-b-4">
                        <com:TTranslate>VERIFIER_VALIDITE_SIGNATURE</com:TTranslate>
                    </div>


                    <com:TPanel visible="false" cssclass="line">
                        <div class="intitule-bloc bloc-150"><label for="typeSignature">
                            <com:TTranslate>TYPE_SIGNATURE</com:TTranslate>
                        </label> :
                        </div>
                    </com:TPanel>

                    <div class="file-upload-v m-t-1">
                        <div class="container-fluid form-horizontal">

                            <div class="col-md-6">
                                <div class="form-group m-b-0">
                                    <label class="col-md-5 control-label">
                                        <com:TTranslate>DOCUMENT_A_VERIFIER</com:TTranslate>
                                        :</label>
                                    <div class="col-md-7">
                                        <com:AtexoQueryFileUpload ID="docVerif" CssClass="bloc-500 float-left" titre="<%=Prado::localize('DEFINE_PARCOURIR_3_POINTS')%>"
                                                                  fonctionJs="showFileUploadSignature()"/>
                                    </div>
                                </div>
                            </div>

                            <com:TActivePanel cssClass="col-md-6 border-left" id="panelDocSignature" style="display:none">
                                <label class="col-md-5 control-label">
                                    <com:TTranslate>FICHIER_ASSOCIE</com:TTranslate>
                                    :</label>
                                <div class="col-md-7">
                                    <com:AtexoQueryFileUpload ID="fichierSignatureAssocie" CssClass="bloc-500 float-left" titre="<%=Prado::localize('DEFINE_PARCOURIR_3_POINTS')%>"/>
                                    <com:TCustomValidator
                                            ControlToValidate="typeSignature"
                                            ValidationGroup="validerAjoutDoc"
                                            Display="Dynamic"
                                            ID="docValidator"
                                            EnableClientScript="false"
                                            onServerValidate="verifyDocAttache"
                                            Text=" ">
                                    </com:TCustomValidator>
                                </div>
                            </com:TActivePanel>

                        </div>

                    </div>

                    <!--Debut line boutons-->
                    <div class="clearfix">
                        <div class="m-t-3 pull-right">
                            <com:TActiveButton id="btnVerif" CssClass="btn btn-sm btn-primary" Text="<%=Prado::localize('TEXT_VERIFIER')%>" ToolTip="<%=Prado::localize('TEXT_VERIFIER')%>"
                                               OnCallBack="verifierSignatureFiles" ValidationGroup="validerAjoutDoc">
                                <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                                <prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
                                <prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
                            </com:TActiveButton>
                        </div>
                    </div>
                    <!--Fin line boutons-->


                </div>
            </div>
            <!--Fin Bloc Verification signature-->
            <!--  Debut integration nouveau affichage -->
            <com:TActivePanel ID="resultat" CssClass="form-field clearfix" style="display:none;">
                <div class="p-t-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2 class="h4 panel-title">
                                <com:TTranslate>RESULTAT_VERIFICATION</com:TTranslate>
                            </h2>
                        </div>

                        <div class="panel-body">

                            <table class="table table-results panel panel-default tableau-detail tableau-detail-pli tableau-verif-signature">
                                <thead class="panel-heading">
                                <tr>
                                    <th class="toggle-col"></th>
                                    <th id="fichierSoummissionnaire">
                                        <com:TTranslate>TEXT_NOM_FICHIER_PRINCIPAL</com:TTranslate>
                                    </th>
                                    <th id="resultatControleSignature">
                                        <com:TTranslate>DEFINE_RESULAT_CONTROLE_SIFNATURE</com:TTranslate>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <!--Debut Ligne fichier-->
                                <tr>
                                    <td class="toggle-col"></td>
                                    <td headers="fichierSoummissionnaire">
                                        <com:TLabel id="fichierverifie" Text=""></com:TLabel>
                                    </td>
                                    <td headers="resultatControleSignature">
                                        <com:TPanel id="resultatControleSignature">
                                            <com:TLabel ID="messageInfoSignature"/><br />
                                            <com:PictoStatutSignature ID="signature" NouveauAffichage="true"/>
                                            <com:TLabel id="statutSignatureFichier"/>
                                        </com:TPanel>
                                    </td>
                                </tr>
                                <!--Fin Ligne fichier-->
                                </tbody>
                            </table>
                            <!--BEGIN RESUME SIGNATURE-->
                            <div class="panel-detail panel_detailFichier_01">
                                <div class="detail-fichier">
                                    <!--Debut bloc detail-->
                                    <com:AtexoDetailsSignature ID="blocDetailsSignature" type="fichierVo" titre=""/>
                                    <!--Fin bloc detail-->
                                </div>
                            </div>
                            <!--END RESUME SIGNATURE-->
                        </div>
                        <div class="panel-footer">
                            <div class="clearfix">

                                <div class="pull-right">
                                    <com:TPanel ID="exportRapportSignatureDocumentBas" CssClass="file-link float-right">
                                        <com:TButton
                                                cssClass="btn btn-sm btn-primary"
                                                Text="Export au format PDF du rapport de v&eacute;rification de signature"
                                                OnClick="Page.printRapportSignatureDocument"
                                        />
                                    </com:TPanel>
                                </div>
                            </div>
                            <!--<div id="infosSiret" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div>Indiquez ici votre SIREN. Vous pouvez le compléter par les 5 chiffres de votre code établissement pour former votre SIRET.</div></div>-->
                        </div>
                    </div>
                </div>
            </com:TActivePanel>
            <!--  Fin integration nouveau affichage -->
        </div>
    </div>
    <com:TActiveLabel id="scriptJs"/>
    <!--Fin colonne droite-->
    <com:TActiveHiddenField id="typeSignature"/>
    <script>
        function showFileUploadSignature() {
            var fileName = J("#<%=$this->docVerif->NomFichier->getClientId()%>").val();
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if (fileNameExt.toLowerCase() != "pdf") {
                J("#ctl0_CONTENU_PAGE_panelDocSignature").show();
                J("#ctl0_CONTENU_PAGE_typeSignature").val("INCONNUE");
            } else {
                J("#ctl0_CONTENU_PAGE_typeSignature").val("<%=  Application\Service\Atexo\Atexo_Config::getParameter('PADES_SIGNATURE')%>");
                J("#ctl0_CONTENU_PAGE_btnVerif").click();
            }
        }
    </script>
</com:TContent>
