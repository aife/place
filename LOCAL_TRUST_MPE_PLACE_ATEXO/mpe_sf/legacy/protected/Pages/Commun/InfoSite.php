<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class InfoSite extends MpeTPage
{
    protected $_calledFrom = '';
    protected string $version = '';

    /**
     * Set the value of [_calledFrom] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2016
     */
    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    /**
     * Get the value of [_calledFrom] column.
     *
     * @return string [_calledFrom]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2016
     */
    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onLoad($param)
    {
        $this->response->redirect('/entreprise/footer/info-site');
    }
}
