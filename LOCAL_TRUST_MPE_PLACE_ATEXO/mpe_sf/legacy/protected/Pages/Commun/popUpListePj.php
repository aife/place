<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnoncePressPeer;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonNewsletterPeer;
use Application\Propel\Mpe\CommonNewsletterPieceJointePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe popUpListePj.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpListePj extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (isset($_GET['Idmsg'])) {
            $this->IdUploadPj->_ID = Atexo_Util::atexoHtmlEntities($_GET['Idmsg']);
        }
        if (isset($_GET['newLetter'])) {
            $this->IdUploadPj->newsLetter = true;
        }
        if ($_GET['org']) {
            $this->IdUploadPj->_org = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->IdUploadPj->_org = Atexo_CurrentUser::getCurrentOrganism();
        }

        if (!$this->IsPostBack) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $PJs = [];
            // Si le message est une press
            if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_PRESS'))) {
                $this->IdUploadPj->_type = Atexo_Util::atexoHtmlEntities($_GET['type']);
                $ObjetMessage = CommonAnnoncePressPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
                if ($ObjetMessage) {
                    $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['org']));
                    $PJs = $ObjetMessage->getCommonAnnoncePressPieceJointes($c, $connexionCom);
                }
            }
            // Si le message est une newsletter
            elseif (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
                $this->IdUploadPj->_type = Atexo_Util::atexoHtmlEntities($_GET['type']);
                $ObjetMessage = CommonNewsletterPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
                if ($ObjetMessage) {
                    $c->add(CommonNewsletterPieceJointePeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['org']));
                    $PJs = $ObjetMessage->getCommonNewsletterPieceJointes($c, $connexionCom);
                }
            } else {
                $ObjetMessage = Atexo_Message::retrieveMessageById(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']));
                if ($ObjetMessage) {
                    $c->add(CommonEchangePieceJointePeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['org']));
                    $PJs = $ObjetMessage->getCommonEchangePieceJointes($c, $connexionCom);
                }
            }
            $this->IdUploadPj->remplirTableauPJ($PJs);
        }
    }

    public function OnValidClick()
    {
        $this->IdUploadPj->onAjoutClick(null, null);
        if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_PRESS'))) {
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_validatePJ').click();window.close();</script>";
        } elseif (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validatePJ').click();window.close();</script>";
        } else {
            echo "<script>opener.document.getElementById('".$this->getIdBoutonValidationPj()."').click();window.close();</script>";
        }
    }

    /**
     * Permet de recuperer le client id du bouton de validation des pj.
     */
    public function getIdBoutonValidationPj()
    {
        if (isset($_GET['idBtnValidatePj']) && $_GET['idBtnValidatePj']) {
            return $_GET['idBtnValidatePj'];
        } else {
            return 'ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_validatePJ';
        }
    }
}
