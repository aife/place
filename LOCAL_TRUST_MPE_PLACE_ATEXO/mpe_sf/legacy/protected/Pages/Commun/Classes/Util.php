<?php

namespace Application\Pages\Commun\Classes;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

class Util extends MpeTPage
{
    public function getMailRefusValidation($reference)
    {
        return Prado::localize('MADAME_MONSIEUR').' '.$reference.Prado::localize('SUITE_MADAME_MONSIEUR').' ';
    }

    public function isEntier($valeur)
    {
        return preg_match('/^([0-9]+)$/', $valeur);
    }

    /** convertit une date ISO en date française.
     * @param $_isoDate date au format yyyy-mm-dd
     *
     * @return date au format dd/mm/yyyy
     */
    public function iso2frnDate($_isoDate)
    {
        if (!$_isoDate || '0000-00-00' == $_isoDate) {
            return;
        }
        if (strstr($_isoDate, '-')) {
            [$y, $m, $d] = explode('-', $_isoDate);

            return $d.'/'.$m.'/'.$y;
        } else {
            return $_isoDate;
        }
    }

    /** convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy
     *
     * @return date au format yyyy-mm-dd
     */
    public function frnDate2iso($_frnDate)
    {
        if (!$_frnDate) {
            return;
        }
        if (strstr($_frnDate, '/')) {
            [$d, $m, $y] = explode('/', $_frnDate);

            return $d.'-'.$m.'-'.$y;
        } else {
            return $_frnDate;
        }
    }

    /** convertit une datetime ISO en date française.
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format dd/mm/yyyy
     */
    public function iso2frnDateTime($_isoDate, $withTime = true, $withTimeSeconds = false)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min] = explode(':', $heure);

        return $j.'/'.$m.'/'.$a.' '.$h.':'.$min;
    }

    //retourne 1 ; -1 ou 0
    public function diffDate($dateDebut, $dateFin)
    {
        return strcmp($this->frnDate2iso($dateFin), $this->frnDate2iso($dateDebut));
    }

    //retourne la différence des jours en nombre positif entre les deux dates
    public function diffJours($date1, $date2)
    {
        if (!$date1 || !$date2) {
            return;
        }
        $chiffreDate1 = explode('/', $date1); // On tri les infos
        $chiffreDate2 = explode('/', $date2); // On tri les infos

        $time_date1 = mktime(
            0,
            0,
            0,
            $chiffreDate1[1],
            $chiffreDate1[0],
            $chiffreDate1[2]
        ); // On recupere sa date de naissance en timestamp
        $time_date2 = mktime(
            0,
            0,
            0,
            $chiffreDate2[1],
            $chiffreDate2[0],
            $chiffreDate2[2]
        ); // On recupere sa date de reference en timestamp
        $seconde = abs($time_date1 - $time_date2);
        $seconde_par_jours = (24 * 60 * 60); // On calcule le nombre de secondes

        return $seconde / $seconde_par_jours;
    }

    //retourne la différence des jours en nombre positif ou négatif entre les deux dates
    public function differenceJours($date1, $date2)
    {
        if (!$date1 || !$date2) {
            return;
        }
        $chiffreDate1 = explode('/', $date1); // On tri les infos
        $chiffreDate2 = explode('/', $date2); // On tri les infos

        $time_date1 = mktime(
            0,
            0,
            0,
            $chiffreDate1[1],
            $chiffreDate1[0],
            $chiffreDate1[2]
        ); // On recupere sa date de naissance en timestamp
        $time_date2 = mktime(
            0,
            0,
            0,
            $chiffreDate2[1],
            $chiffreDate2[0],
            $chiffreDate2[2]
        ); // On recupere sa date de reference en timestamp
        $seconde = $time_date1 - $time_date2;
        $seconde_par_jours = (24 * 60 * 60); // On calcule le nombre de secondes

        return $seconde / $seconde_par_jours;
    }

    /** convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy
     *
     * @return date au format yyyy-mm-dd
     */
    public function frnDateTime2iso($_frnDate)
    {
        if (!$_frnDate) {
            return;
        }
        [$j, $m, $annee_et_heure] = explode('/', $_frnDate);
        [$annee, $heure] = explode(' ', $annee_et_heure);
        [$h, $min, $sec] = explode(':', $heure);
        if (!$sec) {
            $sec = '00';
        }

        return $annee.'-'.$m.'-'.$j.' '.$h.':'.$min.':'.$sec;
    }

    public function isDate($chaine)
    {
        [$d, $m, $y] = explode('/', $chaine);

        if (4 != strlen($y) || strlen($d) > 2 || strlen($m) > 2) {
            return false;
        }
        if ((!$this->isEntier($d)) || (!$this->isEntier($m)) || (!$this->isEntier($y))) {
            return false;
        }
        if ($m > 12 || $d > 31) {
            return false;
        }

        return true;
    }

    public function getDateRespectantDelai($delai, $reference)
    {
        $chiffreReference = explode('/', $reference); // On tri les infos
        $chiffreReference[2] -= $delai;

        return $chiffreReference[0].'/'.$chiffreReference[1].'/'.$chiffreReference[2];
    }

    public function getDateFromDateTime($dateTime)
    {
        if (!$dateTime) {
            return;
        }
        [$date] = explode(' ', $dateTime);

        return $date;
    }

    public function getMinOrganisme()
    {
        $bd = $this->User->getOrganisme();
        if (Atexo_Config::getParameter('DB_NAME_IN_UPPERCASE' == 1)) {
            $organisme = strtolower($bd);
        } else {
            $organisme = $bd;
        }

        return $organisme;
    }

    public function getMajOrganisme($org)
    {
        if (Atexo_Config::getParameter('DB_NAME_IN_UPPERCASE') == 1) {
            $organisme = strtoupper($org);
        } else {
            $organisme = $org;
        }

        return $organisme;
    }

    // returns decoded string as arrays of variables
    //  or false on error (when session_decode returns false)
    public function DecodeSession($sess_string)
    {
        // save current session data
        //  and flush $_SESSION array
        $old = $_SESSION;
        $_SESSION = array();

        // try to decode passed string
        session_start();
        $ret = session_decode($sess_string);
        if (!$ret) {

            // if passed string is not session data,
            //  retrieve saved (old) session data
            //  and return false
            $_SESSION = array();
            $_SESSION = $old;

            return false;
        }

        // save decoded session data to sess_array
        //  and flush $_SESSION array
        $sess_array = $_SESSION;
        $_SESSION = array();

        // restore old session data
        $_SESSION = $old;

        // return decoded session data
        return $sess_array;
    }
}
