<?php

namespace Application\Pages\Commun\Classes;

use Prado\Security\TUser;

class MPEUser extends TUser
{
    private string $_connectedWith = '';
    private string $_prenom = '';
    private string $_nom = '';
    private string $_email = '';
    private string $_id = '';
    private string $_organisme = '';
    private string $_adminType = '';
    private string $_admin = '';
    private ?string $_telephone = null;
    private ?string $_serviceId = '';
    private array $_module = [];
    private string $_login = '';
    private string $_working_org = '';
    private string $_admin_general = '';
    private string $_sess_super_administrateur_logged = '';
    //données entreprise
    private string $_admin_id = '';
    private string $_paysadresse = '';
    private string $_villeadresse = '';
    private string $_codepostal = '';
    private string $_adresse = '';
    private string $_repmetiers = '';
    private ?string $_siren = '';
    private string $_formejuridique = '';
    private string $_villeenregistrement = '';
    private string $_motifNonIndNum = '';
    private string $_ordreProfOuAgrement = '';
    private string $_dateConstSociete = '';
    private string $_nomOrgInscription = '';
    private string $_adrOrgInscription = '';
    private string $_dateConstAssoc = '';
    private string $_dateConstAssocEtrangere = '';
    private string $_nomPersonnePublique = '';
    private string $_nationalite = '';
    private string $_redressement = '';
    private string $_paysenregistrement = '';
    private string $_numAssoEtrangere = '';
    private string $_debutExerciceGlob1 = '';
    private string $_finExerciceGlob1 = '';
    private string $_debutExerciceGlob2 = '';
    private string $_finExerciceGlob2 = '';
    private string $_debutExerciceGlob3 = '';
    private string $_finExerciceGlob3 = '';
    private string $_ventesGlob1 = '';
    private string $_ventesGlob2 = '';
    private string $_ventesGlob3 = '';
    private string $_biensGlob1 = '';
    private string $_biensGlob2 = '';
    private string $_biensGlob3 = '';
    private string $_servicesGlob1 = '';
    private string $_servicesGlob2 = '';
    private string $_servicesGlob3 = '';
    private string $_totalGlob1 = '';
    private string $_totalGlob2 = '';
    private string $_totalGlob3 = '';
    private string $_codeape = '';
    private ?string $_sirenEtranger = null;
    private string $_owner_name = '';
    private string|int $_idEntreprise = '';
    private string|int $_idEtablissement = '';
    private $_profileEntreprisePrenom;
    private $_profileEntrepriseIdInscrit;
    private $_profileEntrepriseModeAuthInscrit;
    private string $_authMethod = '';
    private string $_bloque = '';
    private string $_sso = '';
    private string $_entrepriseCompteActif = '';
    private int|bool $authenticatedWithFc = 0;
    private bool $_siretValide = false;
    private bool $_saisieManuelle = false;
    private string $_etatAdministratifEtablissement = '';

    public function getEtatAdministratifEtablissement()
    {
        return $this->_etatAdministratifEtablissement;
    }

    public function setEtatAdministratifEtablissement($etatAdministratifEtablissement)
    {
        $this->_etatAdministratifEtablissement = $etatAdministratifEtablissement;
    }

    public function isSaisieManuelle()
    {
        return $this->_saisieManuelle;
    }

    public function setSaisieManuelle($saisieManuelle)
    {
        $this->_saisieManuelle = $saisieManuelle;
    }

    public function isSiretValide()
    {
        return $this->_siretValide;
    }

    public function setSiretValide(bool $siret)
    {
        $this->_siretValide = $siret;
    }

    public function getEntrepriseCompteActif()
    {
        return $this->_entrepriseCompteActif;
    }

    public function setEntrepriseCompteActif($entrepriseCompteActif)
    {
        $this->_entrepriseCompteActif = $entrepriseCompteActif;
    }

    public function getSso()
    {
        return $this->_sso;
    }

    public function setSso($sso)
    {
        $this->_sso = $sso;
    }

    public function getBloque()
    {
        return $this->_bloque;
    }

    public function setBloque($authMethod)
    {
        $this->_bloque = $authMethod;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getTelephone()
    {
        return $this->_telephone;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function setTelephone($telephone)
    {
        $this->_telephone = $telephone;
    }

    public function getPrenom()
    {
        return $this->_prenom;
    }

    public function setPrenom($prenom)
    {
        $this->_prenom = $prenom;
    }

    public function getModule()
    {
        return $this->_module;
    }

    public function setModule($module)
    {
        $this->_module = $module;
    }

    public function getNom()
    {
        return $this->_nom;
    }

    public function setNom($nom)
    {
        $this->_nom = $nom;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    public function getAdminType()
    {
        return $this->_adminType;
    }

    public function setAdminType($adminType)
    {
        $this->_adminType = $adminType;
    }

    public function getAdmin()
    {
        return $this->_admin;
    }

    public function setAdmin($admin)
    {
        $this->_admin = $admin;
    }

    public function getServiceId()
    {
        return $this->_serviceId;
    }

    public function setServiceId($serviceId)
    {
        $this->_serviceId = $serviceId;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function setLogin($login)
    {
        $this->_login = $login;
    }

    public function getWorkingOrg()
    {
        return $this->_working_org;
    }

    public function setWorkingOrg($working_org)
    {
        $this->_working_org = $working_org;
    }

    public function getAdminGeneral()
    {
        return $this->_admin_general;
    }

    public function setAdminGeneral($admin_general)
    {
        $this->_admin_general = $admin_general;
    }

    public function getSessSuperAdministrateurLogged()
    {
        return $this->_sess_super_administrateur_logged;
    }

    public function setSessSuperAdministrateurLogged($sess_super_administrateur_logged)
    {
        $this->_sess_super_administrateur_logged = $sess_super_administrateur_logged;
    }

    public function getOwnerName()
    {
        return $this->_owner_name;
    }

    public function setOwnerName($ownerName)
    {
        $this->_owner_name = $ownerName;
    }

    //les setters et getters des données entreprise

    public function getAdminId()
    {
        return $this->_admin_id;
    }

    public function setAdminId($admin_id)
    {
        $this->_admin_id = $admin_id;
    }

    public function getPaysadresse()
    {
        return $this->_paysadresse;
    }

    public function setPaysadresse($paysadresse)
    {
        $this->_paysadresse = $paysadresse;
    }

    public function getVilleadresse()
    {
        return $this->_villeadresse;
    }

    public function setVilleadresse($villeadresse)
    {
        $this->_villeadresse = $villeadresse;
    }

    public function getCodepostal()
    {
        return $this->_codepostal;
    }

    public function setCodepostal($codepostal)
    {
        $this->_codepostal = $codepostal;
    }

    public function getAdresse()
    {
        return $this->_adresse;
    }

    public function setAdresse($adresse)
    {
        $this->_adresse = $adresse;
    }

    public function getRepmetiers()
    {
        return $this->_repmetiers;
    }

    public function setRepmetiers($repmetiers)
    {
        $this->_repmetiers = $repmetiers;
    }

    public function getSiren(): ?string
    {
        return $this->_siren;
    }

    public function setSiren($siren = null)
    {
        $this->_siren = $siren;
    }

    public function getFormejuridique()
    {
        return $this->_formejuridique;
    }

    public function setFormejuridique($formejuridique)
    {
        $this->_formejuridique = $formejuridique;
    }

    public function getVilleenregistrement()
    {
        return $this->_villeenregistrement;
    }

    public function setVilleenregistrement($villeenregistrement)
    {
        $this->_villeenregistrement = $villeenregistrement;
    }

    public function getMotifNonIndNum()
    {
        return $this->_motifNonIndNum;
    }

    public function setMotifNonIndNum($motifNonIndNum)
    {
        $this->_motifNonIndNum = $motifNonIndNum;
    }

    public function getOrdreProfOuAgrement()
    {
        return $this->_ordreProfOuAgrement;
    }

    public function setOrdreProfOuAgrement($ordreProfOuAgrement)
    {
        $this->_ordreProfOuAgrement = $ordreProfOuAgrement;
    }

    public function getDateConstSociete()
    {
        return $this->_dateConstSociete;
    }

    public function setDateConstSociete($dateConstSociete)
    {
        $this->_dateConstSociete = $dateConstSociete;
    }

    public function getNomOrgInscription()
    {
        return $this->_nomOrgInscription;
    }

    public function setNomOrgInscription($nomOrgInscription)
    {
        $this->_nomOrgInscription = $nomOrgInscription;
    }

    public function getAdrOrgInscription()
    {
        return $this->_adrOrgInscription;
    }

    public function setAdrOrgInscription($adrOrgInscription)
    {
        $this->_adrOrgInscription = $adrOrgInscription;
    }

    public function getDateConstAssoc()
    {
        return $this->_dateConstAssoc;
    }

    public function setDateConstAssoc($dateConstAssoc)
    {
        $this->_dateConstAssoc = $dateConstAssoc;
    }

    public function getDateConstAssocEtrangere()
    {
        return $this->_dateConstAssocEtrangere;
    }

    public function setDateConstAssocEtrangere($dateConstAssocEtrangere)
    {
        $this->_dateConstAssocEtrangere = $dateConstAssocEtrangere;
    }

    public function getNomPersonnePublique()
    {
        return $this->_nomPersonnePublique;
    }

    public function setNomPersonnePublique($nomPersonnePublique)
    {
        $this->_nomPersonnePublique = $nomPersonnePublique;
    }

    public function getNationalite()
    {
        return $this->_nationalite;
    }

    public function setNationalite($nationalite)
    {
        $this->_nationalite = $nationalite;
    }

    public function getRedressement()
    {
        return $this->_redressement;
    }

    public function setRedressement($redressement)
    {
        $this->_redressement = $redressement;
    }

    public function getPaysenregistrement()
    {
        return $this->_paysenregistrement;
    }

    public function setPaysenregistrement($paysenregistrement)
    {
        $this->_paysenregistrement = $paysenregistrement;
    }

    public function getNumAssoEtrangere()
    {
        return $this->_numAssoEtrangere;
    }

    public function setNumAssoEtrangere($numAssoEtrangere)
    {
        $this->_numAssoEtrangere = $numAssoEtrangere;
    }

    public function getDebutExerciceGlob1()
    {
        return $this->_debutExerciceGlob1;
    }

    public function setDebutExerciceGlob1($debutExerciceGlob1)
    {
        $this->_debutExerciceGlob1 = $debutExerciceGlob1;
    }

    public function getDebutExerciceGlob2()
    {
        return $this->_debutExerciceGlob2;
    }

    public function setDebutExerciceGlob2($debutExerciceGlob2)
    {
        $this->_debutExerciceGlob2 = $debutExerciceGlob2;
    }

    public function getDebutExerciceGlob3()
    {
        return $this->_debutExerciceGlob3;
    }

    public function setDebutExerciceGlob3($debutExerciceGlob3)
    {
        $this->_debutExerciceGlob3 = $debutExerciceGlob3;
    }

    public function getFinExerciceGlob1()
    {
        return $this->_finExerciceGlob1;
    }

    public function setFinExerciceGlob1($finExerciceGlob1)
    {
        $this->_finExerciceGlob1 = $finExerciceGlob1;
    }

    public function getFinExerciceGlob2()
    {
        return $this->_finExerciceGlob2;
    }

    public function setFinExerciceGlob2($finExerciceGlob2)
    {
        $this->_finExerciceGlob2 = $finExerciceGlob2;
    }

    public function getFinExerciceGlob3()
    {
        return $this->_finExerciceGlob3;
    }

    public function setFinExerciceGlob3($finExerciceGlob3)
    {
        $this->_finExerciceGlob3 = $finExerciceGlob3;
    }

    public function getVentesGlob1()
    {
        return $this->_ventesGlob1;
    }

    public function setVentesGlob1($ventesGlob1)
    {
        $this->_ventesGlob1 = $ventesGlob1;
    }

    public function getVentesGlob2()
    {
        return $this->_ventesGlob2;
    }

    public function setVentesGlob2($ventesGlob2)
    {
        $this->_ventesGlob2 = $ventesGlob2;
    }

    public function getVentesGlob3()
    {
        return $this->_ventesGlob3;
    }

    public function setVentesGlob3($ventesGlob3)
    {
        $this->_ventesGlob3 = $ventesGlob3;
    }

    public function getBiensGlob1()
    {
        return $this->_biensGlob1;
    }

    public function setBiensGlob1($biensGlob1)
    {
        $this->_biensGlob1 = $biensGlob1;
    }

    public function getBiensGlob2()
    {
        return $this->_biensGlob2;
    }

    public function setBiensGlob2($biensGlob2)
    {
        $this->_biensGlob2 = $biensGlob2;
    }

    public function getBiensGlob3()
    {
        return $this->_biensGlob3;
    }

    public function setBiensGlob3($biensGlob3)
    {
        $this->_biensGlob3 = $biensGlob3;
    }

    public function getServicesGlob1()
    {
        return $this->_servicesGlob1;
    }

    public function setServicesGlob1($servicesGlob1)
    {
        $this->_servicesGlob1 = $servicesGlob1;
    }

    public function getServicesGlob2()
    {
        return $this->_servicesGlob2;
    }

    public function setServicesGlob2($servicesGlob2)
    {
        $this->_servicesGlob2 = $servicesGlob2;
    }

    public function getServicesGlob3()
    {
        return $this->_servicesGlob3;
    }

    public function setServicesGlob3($servicesGlob3)
    {
        $this->_servicesGlob3 = $servicesGlob3;
    }

    public function getTotalGlob1()
    {
        return $this->_totalGlob1;
    }

    public function setTotalGlob1($totalGlob1)
    {
        $this->_totalGlob1 = $totalGlob1;
    }

    public function getTotalGlob2()
    {
        return $this->_totalGlob2;
    }

    public function setTotalGlob2($totalGlob2)
    {
        $this->_totalGlob2 = $totalGlob2;
    }

    public function getTotalGlob3()
    {
        return $this->_totalGlob3;
    }

    public function setTotalGlob3($totalGlob3)
    {
        $this->_totalGlob3 = $totalGlob3;
    }

    public function getCodeape()
    {
        return $this->_codeape;
    }

    public function setCodeape($codeape)
    {
        $this->_codeape = $codeape;
    }

    public function getSirenEtranger()
    {
        return $this->_sirenEtranger;
    }

    public function setSirenEtranger($sirenEtranger)
    {
        $this->_sirenEtranger = $sirenEtranger;
    }

    public function getProfileEntreprisePrenom()
    {
        return $this->_profileEntreprisePrenom;
    }

    public function setProfileEntreprisePrenom($profileEntreprisePrenom)
    {
        $this->_profileEntreprisePrenom = $profileEntreprisePrenom;
    }

    public function getProfileEntrepriseIdInscrit()
    {
        return $this->_profileEntrepriseIdInscrit;
    }

    public function setProfileEntrepriseIdInscrit($profileEntrepriseIdInscrit)
    {
        $this->_profileEntrepriseIdInscrit = $profileEntrepriseIdInscrit;
    }

    public function getEntrepriseModeAuthInscrit()
    {
        return $this->_profileEntrepriseModeAuthInscrit;
    }

    public function setProfileEntrepriseModeAuthInscrit($profileEntrepriseModeAuthInscrit)
    {
        $this->_profileEntrepriseModeAuthInscrit = $profileEntrepriseModeAuthInscrit;
    }

    public function getAuthMethod()
    {
        return $this->_authMethod;
    }

    public function setAuthMethod($authMethod)
    {
        $this->_authMethod = $authMethod;
    }

    //fin setters et getters des données entreprise

    /**
     * @return string
     * @desc permet d'avoir le compte par lequel est connecté le gestionnaire.
     */
    public function getConnectedWith()
    {
        return $this->_connectedWith;
    }

    /**
     * @param int $value
     * @desc permet de positionner l'id de l'entreprise
     * @return void
     */
    public function setIdEntreprise($value)
    {
        $this->_idEntreprise = $value;
    }

    /**
     * @return string
     * @desc permet d'avoir l'id de l'entreprise.
     */
    public function getIdEntreprise()
    {
        return $this->_idEntreprise;
    }

    /**
     * @param int $value
     * @desc permet de positionner l'id de l'etablissement
     * @return void
     */
    public function setIdEtablissement($value)
    {
        $this->_idEtablissement = $value;
    }

    /**
     * @return string
     * @desc permet d'avoir l'id de l'etablissement.
     */
    public function getIdEtablissement()
    {
        return $this->_idEtablissement;
    }

    /**
     * @param String $loginOfOF
     * @desc permet de positionner le gestionnaire sur un Organisme de formation
     * @return void
     */
    public function setConnectedWith($loginOfOF)
    {
        $this->_connectedWith = $loginOfOF;
    }

    /**
     * @return bool
     */
    public function isAuthenticatedWithFc()
    {
        return $this->authenticatedWithFc;
    }

    /**
     * @param bool $authenticatedWithFc
     */
    public function setAuthenticatedWithFc($authenticatedWithFc)
    {
        $this->authenticatedWithFc = $authenticatedWithFc;
    }
    }
