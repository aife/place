<?php

namespace Application\Pages\Commun\Classes;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Inscrit;
use App\Entity\SocleHabilitationAgent;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Doctrine\ORM\EntityNotFoundException;
use Prado\Security\TUser;
use Prado\Security\TUserManager;

class MPEUserManager extends TUserManager
{
    private array $_habilitations = array();
    private array $_userData =  array();

    public function getHabilitations()
    {
        return $this->_habilitations;
    }

    //
    public function validateUser($userVo, $password)
    {
        $certif = Atexo_CurrentUser::readFromSession('userCert');
        $serial = Atexo_CurrentUser::readFromSession('userSerial');
        Atexo_CurrentUser::writeToSession("servicesMetiersAgent", false);
        if ($userVo instanceof Atexo_UserVo) {
            if ($userVo->getType() == "entreprise") {
                if ($userVo->getCertificat() === "__CERT__" && $userVo->getPassword() === "__CERT__" && $certif !== null && $serial !== null) { // Entreprise authentifiee par certificat
                    $inscr = Atexo_Entreprise_Inscrit::retrieveCertAuthUE($certif, $serial);
                    if ($inscr) {
                        $this->_userData['sess_subs_profile'] = $inscr;
                        if ($this->_userData['sess_subs_profile']) {
                            $this->_userData['sess_subs_company'] = Atexo_Entreprise::retrieveCompanyBdeById($this->_userData['sess_subs_profile']->getEntrepriseId());
                        }
                        $this->_userData['modules'] = Atexo_Module::retrieveModules();
                        $this->_userData['sso'] = $userVo->getSso();
                    }
                } elseif ($userVo->getLogin() !== null && $userVo->getPassword() !== null) { // Entreprise authentifiee par login mot de passe
                    if ($userVo->getAuthenticatePpp()) {
                        $inscr = Atexo_Entreprise_Inscrit::retrieveInscritByLogin($userVo->getLogin());
                    } else {
                        $inscr = Atexo_Entreprise_Inscrit::retrieveInscritByLoginMdp($userVo->getLogin(), $userVo->getPassword());
                    }

                    //if ($inscr) {
                    if ($inscr instanceof CommonInscrit) {
                        // Si la synchronisation lors de l'authentification est active et l'entreprise est française et le module de synchronisation avec Api Gouv Entreprise est activé on fait la
                        // synchronisation de l'etablissement et l'entreprise ratachés à l'inscrit
                        if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_SGMAP_LORS_D_AUTHENTIFICATION') && Atexo_Entreprise::isEntrepriseFrancaise($inscr->getEntrepriseId())
                            && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                            $sirenInscrit = $inscr->getSirenEntrepriseInscrit();
                            Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($sirenInscrit, '1', $modeSynchro);
                            Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($sirenInscrit . $inscr->getSiretEtablissementInscrit(), '1', $modeSynchro);
                        }
                        $this->_userData['cfAuthentication'] = $userVo->isAuthenticatedFC();
                        //Si inscrit nbre Tentattives est inferieur a 3 et il n'est pas bloque.
                        if ($inscr->getTentativesMdp() < Atexo_Config::getParameter('MAX_TENTATIVES_MDP') && $inscr->getBloque() == 0) {
                            $this->_userData['sess_subs_profile'] = $inscr;
                            if ($this->_userData['sess_subs_profile']) {
                                $this->_userData['sess_subs_company'] = Atexo_Entreprise::retrieveCompanyBdeById($inscr->getEntrepriseId());
                            }
                            $this->_userData['modules'] = Atexo_Module::retrieveModules();
                            $this->_userData['sso'] = $userVo->getSso();
                            //Mettre a 0 les tentatives mot de passe
                            if ($inscr->getTentativesMdp() != 0) {
                                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                                    . Atexo_Config::getParameter('CONST_READ_WRITE'));
                                $inscr->setTentativesMdp('0');
                                $inscr->save($connexionCom);
                            }
                        }
                    } else {
                    //Si lot de passe incorrect,recuperer Inscrit by Login.
                        $inscrO = Atexo_Entreprise_Inscrit::retrieveInscritByLogin($userVo->getLogin());
                        if ($inscrO instanceof CommonInscrit) {
                            $tentative = $inscrO->getTentativesMdp();
                            if ($tentative < Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                                $tentative++;
                                //mettre la nouvelle valeur de tentative
                                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                                $inscrO->setTentativesMdp($tentative);
                                $inscrO->save($connexionCom);
                                if ($tentative == Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                                    //Envoi du mail
                                    Atexo_Message_UserCompte::sendAlerteDesactivationCompte($userVo, $inscrO);
                                }
                            }
                        }
                    }
                }

                if ($this->_userData) {
                    return true;
                }

                return false;
            } elseif ($userVo->getType() == "admin") {
                $admin = Atexo_Admin::retrieveAdminByLoginAndMdp($userVo->getLogin(), $userVo->getPassword());
                //Si Admin et nbre tentatives inferieur a 3.
                if (isset($admin) && $admin instanceof CommonAdministrateur) {
                    if ($admin->getTentativesMdp() < Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                        $this->_userData['sess_admin_profile']["admin"] = "true";
                        $this->_userData['sess_admin_profile']['acronyme'] = $admin->getLogin();
                        $this->_userData['sess_admin_profile']['id'] = $admin->getId();
                        $this->_userData['sess_admin_profile']['organisme'] = $admin->getLogin();
                        //Mettre bre tentatives a zero
                        if ($admin->getTentativesMdp() != 0) {
                            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $admin->setTentativesMdp('0');
                            $admin->save($connexionCom);
                        }
                        return true;
                    }
                } else {
                    //Si mot de passe incorrect, recuperer Admin by Login.
                    $adminO = Atexo_Admin::retrieveAdminByLogin($userVo->getLogin());
                    if ($adminO instanceof CommonAdministrateur) {
                        $tentative = $adminO->getTentativesMdp();
                        if ($tentative < Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                            $tentative++;
                            //mettre la nouvelle valeur de tentative
                             $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $adminO->setTentativesMdp($tentative);
                            $adminO->save($connexionCom);
                            if ($tentative == Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                                //Envoi du mail
                                Atexo_Message_UserCompte::sendAlerteDesactivationCompte($userVo, $adminO);
                            }
                        }
                    }
                }
            } else {
                if ($userVo->getCertificat() === "__CERT__" && $userVo->getPassword() === "__CERT__" && $certif !== null && $serial !== null) {
                    $agent = Atexo_Agent::retrieveAgentByCertificatAndSerial($certif, $serial);
                } elseif ($userVo->getAuthenticatePpp()) {
                    $agent = Atexo_Agent::retrieveAgentByLogin($userVo->getLogin());
                } else {
                    $agent = Atexo_Agent::retrieveAgentByLoginAndMdp($userVo->getLogin(), $userVo->getPassword());
                }
                if ($agent instanceof CommonAgent) {
                    //Si nbre tentatives < 3 et le compte est actif.
                    if ($agent && $agent->getTentativesMdp() < Atexo_Config::getParameter('MAX_TENTATIVES_MDP') && $agent->getActif() == 1) {
                        Atexo_CurrentUser::deleteFromSession("cachedServices");
                        $this->_userData['sess_agent_profile']['organisme'] = $agent->getOrganisme();
                        $this->_userData['sess_agent_profile']['nom'] = $agent->getNom();
                        $this->_userData['sess_agent_profile']['prenom'] = $agent->getPrenom();
                        $this->_userData['sess_agent_profile']['id'] = $agent->getId();
                        $this->_userData['sess_agent_profile']['email'] = $agent->getEmail();
                        $this->_userData['sess_agent_profile']['login'] = $agent->getLogin();
                        $this->_userData['sess_agent_profile']['service_id'] = $agent->getServiceId();
                        $this->_userData['sess_agent_profile']['service_metier'] = $userVo->getType();
                        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        //Mettre nbre tentatives a 0.
                        if ($agent->getTentativesMdp() != 0) {
                            $agent->setTentativesMdp('0');
                        }
                        $agent->setDateConnexion(date('Y-m-d H:i:s'));
                        $agent->save($connexionCom);
                        $accesAgent = new CommonSuiviAcces();
                        $accesAgent->setDateAcces(date('Y-m-d H:i'));
                        $accesAgent->setEmail($agent->getEmail());
                        $accesAgent->setIdAgent($agent->getId());
                        $accesAgent->setIdService($agent->getServiceId());
                        $accesAgent->setNom($agent->getNom());
                        $accesAgent->setOrganisme($agent->getOrganisme());
                        $accesAgent->setPrenom($agent->getPrenom());
                        $accesAgent->save($connexionCom);

                        return true;
                    }
                } elseif (!$userVo->getAuthenticatePpp()) {
                    //Si mot passe incorrect, recuprer Agent by Login.
                    $agentO = Atexo_Agent::retrieveAgentByLogin($userVo->getLogin());
                    if ($agentO instanceof CommonAgent) {
                        $tentative = $agentO->getTentativesMdp();
                        if ($tentative < Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                            $tentative++;
                            //mettre la nouvelle valeur de tentative
                            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $agentO->setTentativesMdp($tentative);
                            $agentO->save($connexionCom);
                            if ($tentative == Atexo_Config::getParameter('MAX_TENTATIVES_MDP')) {
                                //Envoi du mail
                                Atexo_Message_UserCompte::sendAlerteDesactivationCompte($userVo, $agentO);
                            }
                        }
                    }
                }
            }
        }
        return false;
    }



    public function getUser($username = null)
    {
        $user = new MPEUser($this);
        $user->setIsGuest(true);

        return $user;
    }

    public function mapSfUserToPradoUser(\App\Entity\Agent|\App\Entity\Inscrit|\App\Entity\Administrateur $sfUser): TUser
    {
        // Le premier element doit etre toujours vide
        $roles = [''];
        $modules = [];
        $user = new MPEUser($this);
        $user->setIsGuest(false);
        $user->setNom($sfUser->getNom());
        $user->setPrenom($sfUser->getPrenom());
        $user->setId($sfUser->getId());
        $user->setEmail($sfUser->getEmail());
        $user->setLogin($sfUser->getLogin());

        if ($sfUser instanceof Agent) {
            /** @var Agent $sfUser */

            $roles[] = "Agent";

            $organisme = $sfUser->getOrganisme();
            $user->setOrganisme($organisme->getDenominationOrg());

            $user->setTelephone($sfUser->getTelephone());
            $user->setServiceId($sfUser->getServiceId());

            $habilitations = $this->getAgentHabilitations($sfUser, $organisme->getAcronyme());

            $roles = array_merge($roles, $habilitations);
        } elseif ($sfUser instanceof Inscrit) {
            /** @var Inscrit $sfUser */

            $roles[] = "Entreprise";

            if ($sfUser->getProfil() == 2) {
                $roles[] = "ATES"; // Inscrit Administrateur entreprise
            } else {
                $roles[] = "UES"; // Inscrit simple
            }

            $modules = [];
            $inscritModules = CommonConfigurationPlateformePeer::getConfigurationPlateforme();

            $modules['AuthenticateInscritByCert'] = $inscritModules->getAuthenticateInscritByCert();
            $modules['AuthenticateInscritByLogin'] = $inscritModules->getAuthenticateInscritByLogin();
            $modules['BaseQualifieeEntrepriseInsee'] = $inscritModules->getBaseQualifieeEntrepriseInsee();
            $modules['CodeCpv'] = $inscritModules->getCodeCpv();
            $modules['GestionFournisseursDocsMesSousServices'] =
                $inscritModules->getGestionFournisseursDocsMesSousServices();
            $modules['MultiLinguismeEntreprise'] = $inscritModules->getMultiLinguismeEntreprise();

            $entreprise = $sfUser->getEntreprise();

            $user->setBloque($sfUser->getBloque());
            $user->setSiren($entreprise->getSiren());
            $user->setSirenEtranger($entreprise->getSirenEtranger());
            $user->setSaisieManuelle($entreprise->getSaisieManuelle());
            $user->setEntrepriseCompteActif($entreprise->getCompteActif());
            $user->setIdEtablissement($sfUser->getIdEtablissement());

            $etablissement = $sfUser->getEtablissement();
            $siret = $entreprise->getSiren() . $etablissement->getCodeEtablissement();
            $user->setSiretValide(Atexo_Util::isSiretValide($siret));
        } elseif ($sfUser instanceof Administrateur) {
            /** @var Administrateur $sfUser */

            $roles[] = "Admin";

            $organisme = $sfUser->getOrganisme();
            if ($organisme) {
                $user->setOrganisme($organisme->getDenominationOrg());
            }

            $modules['admin'] = 1;
        }

        $user->setRoles($roles);
        $user->setModule($modules);

        return $user;
    }

    protected function getAgentHabilitations(Agent $sfUser, string $organismeAcronyme): array
    {
        $agentHabilitations = [];

        // HabilitationAgent
        $userHabilitations = $sfUser->getHabilitation();
        $reflect = new \ReflectionClass(HabilitationAgent::class);
        $habilitations = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

        foreach ($habilitations as $habilitation) {
            $habilitationName = ucfirst($habilitation->getName());

            $getterMethod = 'get' . $habilitationName;
            if (method_exists($userHabilitations, $getterMethod) && $userHabilitations->$getterMethod()) {
                $agentHabilitations[] = $habilitationName;
            }

            $getterMethod = 'is' . $habilitationName;
            if (method_exists($userHabilitations, $getterMethod) && $userHabilitations->$getterMethod()) {
                $agentHabilitations[] = $habilitationName;
            }
        }

        if ($userHabilitations->getModuleAutoformation()) {
            $roles[] = 'ModuleAutoformation';
        }

        // SocleHabilitationAgent
        $userSocleHabilitations = $sfUser->getSocleHabilitations();
        if ($userSocleHabilitations instanceof SocleHabilitationAgent) {
            $reflect = new \ReflectionClass(SocleHabilitationAgent::class);
            $habilitations = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

            foreach ($habilitations as $habilitation) {
                $habilitationName = ucfirst($habilitation->getName());
                if ($habilitationName == 'Agent') {
                    continue;
                }

                try {
                    $getterMethod = 'get' . $habilitationName;
                    if (method_exists($userSocleHabilitations, $getterMethod)
                        && $userSocleHabilitations->$getterMethod()
                    ) {
                        $agentHabilitations[] = $habilitationName;
                    }

                    $getterMethod = 'is' . $habilitationName;
                    if (method_exists($userSocleHabilitations, $getterMethod)
                        && $userSocleHabilitations->$getterMethod()
                    ) {
                        $agentHabilitations[] = $habilitationName;
                    }
                } catch (EntityNotFoundException) {
                    // Si l'habilitation socle n'existe pas pour le user, pas de rôle à associer.
                }
            }
        }

        if (Atexo_Module::isEnabled('SocleInterne')) {
            $agentHabilitations[] = "AgentSocle";
            $this->removeHabilitations(['GestionAgents', 'GestionAgentPole'], $agentHabilitations);

            //récuperation des habilitations pour le module de gestion Entité coordinatrice
            $serviceMetier = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService(
                $sfUser->getId(),
                Atexo_Config::getParameter('ID_SERVCE_METIER_ENTITE_COORDINATRICE')
            );
            if ($serviceMetier[0] instanceof CommonAgentServiceMetier) {
                $agentHabilitations[] = 'GestionPubEntiteCoordinatrice';
            }

            //récuperation des habilitations pour le module de SIP (service information presse)
            $serviceMetierSip = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService(
                $sfUser->getId(),
                Atexo_Config::getParameter('ID_SERVCE_METIER_SIP')
            );
            if ($serviceMetierSip[0] instanceof CommonAgentServiceMetier) {
                $agentHabilitations[] = 'GestionPubSip';
            }

            //récuperation des habilitations pour le module de CAO
            $serviceMetierCAO = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService(
                $sfUser->getId(),
                Atexo_Config::getParameter('SERVICE_METIER_CAO')
            );
            if ($serviceMetierCAO[0] instanceof CommonAgentServiceMetier) {
                $agentHabilitations[] = 'AgentCommission';
            }
            //récuperation des habilitations pour le module ANNUAIRE
            $serviceMetierAnnuaire = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService(
                $sfUser->getId(),
                Atexo_Config::getParameter('SERVICE_METIER_ANNAUIRE')
            );
            if ($serviceMetierAnnuaire[0] instanceof CommonAgentServiceMetier) {
                $agentHabilitations[] = 'AnnuaireAcheteur';
            }
        }
        if (!Atexo_Module::isEnabled('SocleInterne', $organismeAcronyme)) {
            $this->removeHabilitations(
                [
                    'GestionAgentPoleSocle',
                    'GestionAgentsSocle',
                    'DroitGestionServicesSocle',
                ],
                $agentHabilitations
            );
        }
        if (!Atexo_Module::isEnabled('espaceDocumentaire', $organismeAcronyme)) {
            $this->removeHabilitations(['EspaceDocumentaireConsultation'], $agentHabilitations);
        }
        if (!Atexo_Module::isEnabled('EchangesDocuments', $organismeAcronyme)) {
            $this->removeHabilitations(['AccesEchangeDocumentaire'], $agentHabilitations);
        }
        if (!Atexo_Module::isEnabled('moduleExec', $organismeAcronyme)) {
            $this->removeHabilitations(
                [
                    'ExecVoirContratsEa',
                    'ExecVoirContratsEaDependantes',
                    'ExecVoirContratsOrganisme',
                ],
                $agentHabilitations
            );
        }

        return $agentHabilitations;
    }

    /**
     * @param $habilitationsToRemove
     * @return array
     */
    protected function removeHabilitations($habilitationsToRemove, array $habilitationsList)
    {
        if (!is_array($habilitationsToRemove)) {
            $habilitationsToRemove = [$habilitationsToRemove];
        }

        foreach ($habilitationsToRemove as $habilitationToRemove) {
            unset($habilitationsList[array_search($habilitationToRemove, $habilitationsList)]);
        }
        return $habilitationsList;
    }
}
