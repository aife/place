<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
<div class="popup-moyen" id="container">
	<div class="form-horizontal" id="container">
		<div class="" tabindex="-1" role="dialog">
			<div class="" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.close();"><span aria-hidden="true" onclick="window.close();">&times;</span></button>
						<h4 class="modal-title">
							<com:TTranslate>TEXT_AJOUTER_MODIFIER_PIECES_JOINTES</com:TTranslate>
						</h4>
					</div>
					<div class="modal-body clearfix">
						<com:UploadEntreprisePJ ID="IdUploadPj"/>
					</div>
					<div class="panel-footer clearfix">
						<div class="boutons-line">
							<com:TButton
									CssClass="bouton-moyen float-left btn btn-default btn-sm pull-left m-t-1 m-l-2"
									Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>"
									Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>"
									Attributes.onclick = "window.close();"/>
							<com:TButton
									CssClass="bouton-moyen float-right btn btn-primary btn-sm pull-right m-t-1 m-r-2"
									Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>"
									Attributes.title="<%=Prado::localize('DEFINE_VALIDER_SELECTION')%>"
									onClick="OnValidClick"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">popupResize();</script>
</com:TContent>