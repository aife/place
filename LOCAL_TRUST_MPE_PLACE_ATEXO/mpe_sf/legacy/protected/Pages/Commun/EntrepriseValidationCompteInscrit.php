<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

class EntrepriseValidationCompteInscrit extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $message = null;
        $uid = Atexo_Util::atexoHtmlEntities($_GET['uid']);
        if ($uid) {
            $inscritCreate = (new Atexo_Entreprise_Inscrit())->retrieveInscritByUid($uid);
        } else {
            exit;
        }
        if ($inscritCreate instanceof CommonInscrit) {
            if ($inscritCreate->getUid() == $uid) {
                $inscritCreate->setBloque(Atexo_Config::getParameter('ETAT_COMPTE_INSCRIT_VALIDE'));
                $inscritCreate->setUid('valide_'.$uid);
                $inscritCreate = $inscritCreate->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')));
                $message = Prado::localize('MESSAGE_CONFIRMATION_VALIDATION_COMPTE_INSCRIT');
            } elseif ($inscritCreate->getUid() == 'valide_'.$uid) {
                $message = Prado::localize('DEFINE_COMPTE_DEJA_VALIDE');
            }
            $this->messageConfirmation->setVisible(true);
            $this->messageConfirmation->setMessage($message);
        } else {
            exit;
        }
    }
}
