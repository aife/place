<%@ MasterClass="Application.layouts.PopUpLayout" %>
<com:TContent ID="CONTENU_PAGE">
	<div class="popup-moyen" id="container">
	<h1><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate></h1>
	<!--Debut Bloc Qualifications-->
	<com:TActivePanel ID="panelQualification" cssClass="form-field">
		<com:TLabel ID="script"/>	
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="form-bloc">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<h3><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate></h3>
				<div class="line">
					<div class="intitule-100"><label for="<%=Prado::localize('TEXT_TYPE')%>"><com:TTranslate>TEXT_TYPE</com:TTranslate> :</label></div>
					<com:TActiveDropDownList 
    					ID="idType" 
    					cssClass="bloc-600" 
    					AutoPostBack="true" 
    					onCallBack="displaySecteur"  
    					Attributes.title="<%=Prado::localize('TEXT_TYPE')%>"/>
				</div>
				<div id="divSecteur" style="display:none;" class="line">
					<div class="intitule-100"><label for="<%=Prado::localize('TEXT_SECTEUR')%>"><com:TTranslate>TEXT_SECTEUR</com:TTranslate> :</label></div>
					<com:TActiveDropDownList 
    					ID="idSecteur" 
    					cssClass="bloc-600" 
    					AutoPostBack="true" 
    					onCallBack="displayQualification"  
    					Attributes.title="<%=Prado::localize('TEXT_SECTEUR')%>"/>
				</div>
				<div id="divQualification" style="display:none;" class="line">
					<div class="intitule-100"><label for="<%=Prado::localize('TEXT_QUALIFICATION')%>"><com:TTranslate>TEXT_QUALIFICATION</com:TTranslate> :</label></div>
					<com:TActiveDropDownList 
    					ID="idQualification" 
    					cssClass="bloc-600" 
    					AutoPostBack="true" 
    					onCallBack="displayClasse"  
    					Attributes.title="<%=Prado::localize('TEXT_QUALIFICATION')%>"/>
				</div>
				<div id="divClasse" style="display:none;" class="line">
					<div class="intitule-100"><label for="<%=Prado::localize('TEXT_CLASSE')%>"><com:TTranslate>TEXT_CLASSE</com:TTranslate> :</label></div>
					<com:TActiveDropDownList 
    					ID="idClasse" 
    					cssClass="bloc-600" 
    					AutoPostBack="true" 
    					onCallBack="callBackDisplayLinkAdd" 
    					Attributes.title="<%=Prado::localize('TEXT_CLASSE')%>"/>
				</div>
				<div id="divAddSelection" style="display:none;" class="line">
					<div class="intitule-100">&nbsp;</div>
					<com:TActivePanel ID="panelLinkSelection" cssClass="content-bloc bloc-600">
						<com:TActiveLinkButton  
    						OnCallBack="displaySelection" 
    						ID="linkAddSelection" 
    						Text="<%=Prado::localize('DEFINE_AJOUTER_SELECTION')%>"
    						cssClass="ajout-el float-left"/>
					</com:TActivePanel>
				</div>
				<div class="breaker"></div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="breaker"></div>
			</div>
			<com:TActivePanel ID="panelMaSelection" cssClass="table-bloc">
				<div id="divMaSelection" style="display:none;">
					<h2><com:TTranslate>TEXT_MA_SELECTION</com:TTranslate></h2>
				</div>
				<table class="table-results" summary="Qualifications">
				<com:TRepeater ID="repeaterSelection" >
					<prop:HeaderTemplate>
						<caption><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate></caption>
    						<thead>
    						<tr>
    							<th class="top" colspan="5"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
    						</tr>
    						<tr>
    							<th class="col-100" id="choixType"><com:TTranslate>TEXT_TYPE</com:TTranslate> </th>
    							<th class="col-250" id="choixSecteur"><com:TTranslate>TEXT_SECTEUR</com:TTranslate></th>
    							<th class="col-250" id="choixQualification"><com:TTranslate>TEXT_QUALIFICATION</com:TTranslate></th>
    							<th class="col-50" id="choixClasse"><com:TTranslate>TEXT_CLASSE</com:TTranslate></th>
    							<th class="actions" id="supprimer"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
    						</tr>
    						</thead>
						</prop:HeaderTemplate>
						<prop:ItemTemplate>
        					<tr class="<%=(($this->ItemIndex%2==0)? 'on':'')%>">
        						<td class="col-100" headers="choixType"><com:TLabel ID="type" Text="<%#$this->Data['type']%>"/></td>
        						<td class="col-250" headers="choixSecteur">
        							<div><com:TActiveLabel ID="secteur" Text="<%# Application\Service\Atexo\Atexo_Util::truncateTexte(wordwrap($this->Data['secteur'],220, '<br/>', true),40)%>"/>
                            			<span
                            				onmouseover="afficheBulle('infosSecteur', this)" 
                            				onmouseout="cacheBulle('infosSecteur')" 
                            				class="info-suite"> <com:TActiveLabel Display="<%#(strlen($this->Data['secteur'])>39) ? 'Dynamic':'None'%>" Text ="..."/>
                            			</span>
                            			</div>
                            			<div ID="infosSecteur" class="info-bulle" >
											<div><com:TActiveLabel ID="libellesSecteur" Text="<%#wordwrap($this->Data['secteur'],220, '<br/>', true)%>"/></div>
										</div>
        						
        						</td>
        						<td class="col-250" headers="choixQualification">
        							<div><com:TActiveLabel ID="qualification" Text="<%# Application\Service\Atexo\Atexo_Util::truncateTexte(wordwrap($this->Data['qualification'],220, '<br/>', true),60)%>"/>
                            			<span
                            				onmouseover="afficheBulle('infosQualif', this)" 
                            				onmouseout="cacheBulle('infosQualif')" 
                            				class="info-suite"> <com:TActiveLabel Display="<%#(strlen($this->Data['qualification'])>39) ? 'Dynamic':'None'%>" Text ="..."/>
                            			</span>
                            			</div>
                            			<div ID="infosQualif" class="info-bulle" >
											<div><com:TActiveLabel ID="libellesQualif" Text="<%#wordwrap($this->Data['qualification'],220, '<br/>', true)%>"/></div>
										</div>
        						</td>
        						<td class="col-50" headers="choixClasse"><com:TLabel ID="classe" Text="<%#$this->Data['classe']%>"/></td>
        						<td class="actions" headers="supprimer">
                            		<com:TTextBox Display="None" ID="idInterne" Text="<%#$this->Data['idInterne']%>"/>
            						<com:PictoDeleteActive
        									onCommand="Page.deleteClasse" 
                							onCallBack="Page.onCallBackRefreshRepeater" 
                							CommandName = "<%#$this->Data['id']%>""
                							AlternateText="<%=Prado::localize('TEXT_SUPPRIMER')%>"
                							Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('CONFIRMATION_SUPPRESSION_CLASSE_QUALIFICATION')%>"))return false;'/>
        						</td>
        					</tr>
						</prop:ItemTemplate>
					</com:TRepeater>
				</table>
			</com:TActivePanel>
		</div>
		<div class="breaker"></div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TActivePanel>
	<!--Fin Bloc Qualifications-->
<!--Debut line boutons-->
	<div class="boutons-line">
		<com:PictoAnnuler />
		<com:PictoValidateButton ID="validateButton"  onClick="onValidateClick"/>
    </div>
	<!--Fin line boutons-->
</div>
		<com:TLabel ID="onLoadScript"/>
</com:TContent>
