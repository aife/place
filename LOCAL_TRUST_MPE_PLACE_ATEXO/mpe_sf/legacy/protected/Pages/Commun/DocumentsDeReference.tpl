<com:TContent ID="CONTENU_PAGE">
    <div class="main-part" id="main-part">
        <div class="clearfix">
            <div class="breadcrumbs">
                <span class="normal">
                    <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                </span>
                &gt;
                <span class="normal">
                    <com:TTranslate>DEFINE_TEXT_AIDE</com:TTranslate>
                </span>
                &gt;
                    <com:TTranslate>DEFINE_DOCUMENTS_REFERENCE</com:TTranslate>
            </div>
        </div>
        <!--Debut bloc documents de reference-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="h3 panel-title">
                    <com:TTranslate>DEFINE_DOCUMENTS_REFERENCE</com:TTranslate>
                </h1>
            </div>
            <div class="panel-body">
                <p><%=$this->getMessageDocReference()%></p>
                <!--Debut Bloc documents de reference agent -->
                <com:TPanel id="documentsRefAgent">
                    <a href="http://www.economie.gouv.fr" target="_blank" title="<%=Prado::localize('TEXT_ACCEDER')%>">
                        http://www.economie.gouv.fr
                    </a>
                </com:TPanel>
                <!--Fin Bloc documents de reference agent -->
                <!--Debut Bloc documents de reference entreprise -->
                <com:TPanel cssClass="content-bloc bloc-730" id="documentsRefEntreprise">
                    <a href="http://www.economie.gouv.fr/daj/formulaires-marches-publics" target="_blank" title="<%=Prado::localize('TEXT_ACCEDER')%>">
                        http://www.economie.gouv.fr/daj/formulaires-marches-publics
                    </a>
                </com:TPanel>
                <!--Fin Bloc documents de reference entreprise -->
            </div>
        </div>
        <!--Fin bloc documents de reference-->
    </div>
</com:TContent>
