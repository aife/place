<?php

namespace Application\Pages\Commun;

use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Soap\Atexo_Soap_InterfaceWsTED;
use Exception;
use Prado\Prado;

/**
 * Permet de télécharger l'AVIS TED en utilisation le web service.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4
 */
class DownloadAvisTED extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $this->panelErreurMessage->Visible = false;
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $idDestinataire = Atexo_Util::atexoHtmlEntities($_GET['idDestinataire']);
            $idavis = Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
            $formXmlDestOpoce = (new Atexo_Publicite_AvisPub())->getObjetAvisOpoceByIdDest($idDestinataire, $organisme);
            $avis = (new Atexo_Publicite_AvisPub())->getAvisById($idavis, $organisme, $consultationId);
            if ($avis instanceof CommonAvisPub) {
                $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, $organisme);
                if ($destinataire instanceof CommonDestinatairePub) {
                    if ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_ESENDER')) {
                        if ($formXmlDestOpoce instanceof CommonFormXmlDestinataireOpoce) {
                            $lang = Atexo_Languages::readLanguageFromSession();
                            $arrayPdfContent = (new Atexo_Soap_InterfaceWsTED())->generatePDFTED(base64_decode($formXmlDestOpoce->getXml()), $formXmlDestOpoce->getCodeRetour(), $lang);
                            if (!is_array($arrayPdfContent) || !$arrayPdfContent['erreur']) {
                                $this->log(' Retour du WS : '.print_r($arrayPdfContent, true));
                                $this->afficherMessageErreurTelechargement();
                            }
                            if ('false' == $arrayPdfContent['erreur']) {
                                $pdfContent = $arrayPdfContent['resultat'];
                                if ($pdfContent && '' != $pdfContent && 'null' != $pdfContent) {
                                    $nomFichier = 'Avis_TED_'.(new Atexo_Publicite_AvisPub())->getNomFichierAnnoncePresse($consultationId, $idavis).'.pdf';
                                    DownloadFile::downloadFileContent($nomFichier, $pdfContent);
                                } else {
                                    $this->log(" Contenu de l'avis PDF est vide ");
                                    $this->afficherMessageErreurTelechargement();
                                }
                            } else {
                                $this->log(' Retour du WS : '.print_r($arrayPdfContent, true));
                                $this->afficherMessageErreurTelechargement();
                            }
                        }
                    } elseif ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB')) {
                        if (isset($_GET['pdf']) && 'sub' == $_GET['pdf'] && $destinataire->getIdDossier()) {
                            Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($destinataire->getIdDossier());
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            $this->log(" Message d'exception généré: ".$ex->getMessage());
            $this->afficherMessageErreurTelechargement();
        }
    }

    /**
     * Permet de construire l'entete du log.
     */
    public function getEnteteLogGenerationAvisTed()
    {
        $log = "\n";
        $log .= "\n\t_______________________".date('Y-m-d H:i:s')."_______________________\n";
        $log .= "\n\n Generation avis ted depuis la page de gestion agent \n ";
        $log .= "\n\t___________________________________________________________\n";

        return $log;
    }

    /**
     * Permet de logger le message d'erreur.
     *
     * @param string $erreur
     */
    private function log($erreur)
    {
        $pathFile = Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_PUB').'telechargementPdf.log';
        $log = $this->getEnteteLogGenerationAvisTed();
        $log .= $erreur;
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_PUB'), $pathFile, $log, 'a');
    }

    /**
     * Permet d'afficher le message d'erreur de téléchargement de l'avis TED.
     */
    public function afficherMessageErreurTelechargement()
    {
        $this->panelErreurMessage->Visible = true;
        $this->erreurTelechargement->Message = Prado::localize('MESSAGE_ERREUR_TELECHARGEMENT_AVIS_TED_PDF');
    }
}
