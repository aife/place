<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use PDO;

class Guides extends MpeTPage
{
    private $accesFrom;

    public function getAccesFrom()
    {
        return $this->accesFrom;
    }

    public function setAccesFrom($accesFrom)
    {
        $this->accesFrom = $accesFrom;
    }

    public function onLoad($param)
    {
        if (!isset($_GET['calledFrom']) || (!Atexo_Module::isEnabled('PublierGuides'))) {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        } else {
            if (!Atexo_CurrentUser::isConnected()) {
                if (isset($_GET['calledFrom']) && 'agent' == $_GET['calledFrom']) {
                    $this->response->redirect('index.php?page=Agent.AgentHome');
                }
                if (isset($_GET['calledFrom']) && 'entreprise' == $_GET['calledFrom']) {
                    $this->response->redirect('index.php?page=Entreprise.EntrepriseHome');
                }
            }
        }
        $this->accesFrom = Atexo_Util::atexoHtmlEntities($_GET['calledFrom']);
        self::fillRepeaterAtexoGuides();
    }

    /*
     * cree l'ensemble des blocs guides a remplir
     * depend des types presents ds la BD
     */
    public function fillRepeaterAtexoGuides()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$langue) {
            $langue = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }
        $sql = " SELECT DISTINCT(type) as type FROM Guides where acces_from='".$this->accesFrom."'".
             " AND langue ='".$langue."'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        foreach ($statement as $row) {
            $arrayGuides[]['type'] = $row[0];
        }

        $this->RepeaterAtexoGuides->DataSource = $arrayGuides;
        $this->RepeaterAtexoGuides->DataBind();
        foreach ($this->RepeaterAtexoGuides->items as $item) {
            $item->guideAtexo->accesFrom = Atexo_Util::atexoHtmlEntities($_GET['calledFrom']);
        }
    }

    public function onInit($param)
    {

        if ($_GET['calledFrom']) {
            $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        } else {
            $this->Master->setCalledFrom('entreprise');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
}
