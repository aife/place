<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_UploadHandler;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Classe SaveJqueryUploadFile.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.2
 *
 * @since MPE-4.0
 */
class SaveJqueryUploadFile extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $upload_handler = new Atexo_UploadHandler();
            exit;
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS'), date('Y-m-d H:i:s').
            ' -- > Jquery Upload File: '.$e->getMessage()." \n", 'a+');
        }
    }
}
