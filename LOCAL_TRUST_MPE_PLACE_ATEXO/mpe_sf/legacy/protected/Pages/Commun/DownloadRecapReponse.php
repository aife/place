<?php

namespace Application\Pages\Commun;

use App\Utils\Encryption;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 21/12/2015
 * Time: 17:51.
 */
class DownloadRecapReponse extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom($_GET['callFrom']);
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $inscritId = Atexo_CurrentUser::getIdInscrit();
            if (Atexo_Util::atexoHtmlEntities($_GET['idFile']) && $inscritId) {
                $encryption = Atexo_Util::getSfService(Encryption::class);
                $this->_idFichier =  $encryption->decryptId(Atexo_Util::atexoHtmlEntities($_GET['idFile']));
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $blobFile = CommonBlobOrganismeFileQuery::create()
                    ->filterById($this->_idFichier)
                    ->filterByOrganisme($organisme)->findOne();
                $offre = CommonOffresQuery::create()
                    ->filterByEntrepriseId(Atexo_CurrentUser::getIdEntreprise())
                    ->filterByInscritId($inscritId)
                    ->filterByIdPdfEchangeAccuse($this->_idFichier)
                    ->findOne();
                if (! $offre instanceof CommonOffres) {
                    throw new AccessDeniedHttpException('Unauthorized to download this file.');
                }
                if ($blobFile instanceof CommonBlobOrganismeFile) {
                    $this->_nomFichier = $blobFile->getName();
                    $this->downloadFiles($this->_idFichier, $this->_nomFichier, $organisme);
                }
            }
            throw new AccessDeniedHttpException('Unauthorized to download this file.');
        } catch (Exception $e) {
            Prado::log(
                'Erreur lors du telechargenemt du fichier Recap Mail Reponse entreprise' . $e->getTraceAsString(),
                TLogger::ERROR,
                'Atexo.pages.entreprise.EntrepriseDownloadRecapReponse.php'
            );

            throw $e;
        }
    }
}
