<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonGeolocalisationN0;
use Application\Propel\Mpe\CommonGeolocalisationN1;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN0;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/*
 * Created on 10 sept. 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class LieuxExecutionCarte extends MpeTPage
{
    private ?array $_arJsFunctions = null;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            //Recuperation des lieux d'execution pre-selectionnes (CARTE)
            $lieux = Atexo_Util::atexoHtmlEntities($_GET['lieux']);

            $this->script->Text = "<script>var selected_items = '".$lieux."';togglePanel(this,'panel_2');togglePanel(this,'panel_3');</script>";
            $this->displayGeolocalisationN0();

            $this->generateOnClickJsFunc();
            //Recuperation des lieux d'execution pre-selectionnes (checkbox)
            $selected = Atexo_Util::atexoHtmlEntities($_GET['selected']);
            $this->script3->Text = "<script>var checked_loc = '".$selected."';</script>";
        }
        $this->panelMessageInformation->setMessage(Prado::localize('DEFINE_MSG_LIEUX_EXECUTION'));
    }

    /**
     * affichage des geolocalisation de niveau N1 (trois block par ligne).
     */
    public function groupObjects($listObjects, $maxPerLine)
    {
        $data = [];
        if ($listObjects) {
            $i = 0;
            $j = 1;
            foreach ($listObjects as $oneObject) {
                if ($j == $maxPerLine + 1) {
                    ++$i;
                    $j = 1;
                }
                $data[$i][$j] = $oneObject;
                ++$j;
            }

            if ($j < $maxPerLine + 1) {
                for ($k = $j; $k < $maxPerLine + 1; ++$k) {
                    $data[$i][$k] = new CommonGeolocalisationN1();
                }
            }
        }

        return $data;
    }

    /**
     * affichage des geolocalisation niveau N0.
     */
    public function displayGeolocalisationN0()
    {
        $listGeoN0 = (new Atexo_Geolocalisation_GeolocalisationN0())->retrieveActifGeoN0();
        $this->repeaterGeoN0->DataSource = $listGeoN0;
        $this->repeaterGeoN0->dataBind();

        // Generation des fcts Js
        $i = 0;
        foreach ($this->repeaterGeoN0->getItems() as $itemN0) {
            if (1 == $itemN0->typeN0->getValue()) {
                $jsFunction = self::generateType1JsFunc($itemN0->getItemIndex(), $itemN0->list2->getClientId());
                $this->_arJsFunctions[$jsFunction[0]] = $jsFunction[1];
            }
            //recupere le classement de l item du type 2 (carte) pour generer l'id de la liste à utiliser en js
            elseif (2 == $itemN0->typeN0->getValue()) {
                $this->idListeLocalisation->Value = 'ctl0_CONTENU_PAGE_repeaterGeoN0_ctl'.$i.'_listeLocalisationsSelect';
            }
            ++$i;
        }
    }

    /**
     * affiche la liste des geolocalisations N1 et geolocalisations N2 de type 0.
     */
    public function itemDataBindGeoN0($sender, $param)
    {
        $itemN0 = $param->Item;
        $geoN0 = $param->Item->Data;
        $dataSource = [];
        $dataSource = self::groupObjects((new Atexo_Geolocalisation_GeolocalisationN1())->retrieveGeoN1($geoN0), 3);
        $countDataSource = is_countable($dataSource) ? count($dataSource) : 0;
        $this->setViewState('countDataSource', $countDataSource);
        $itemN0->repeaterGeoN1->DataSource = $dataSource;
        $itemN0->repeaterGeoN1->DataBind();
    }

    /**
     * affiche la liste des geolocalisations N2 de type 1.
     */
    public function retrieveListGeolocalisationType1InArray($geoN0)
    {
        $listGeoType1 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListGeolocalisationType1($geoN0);
        $data = [];
        foreach ($listGeoType1 as $oneGeoType) {
            $data[$oneGeoType->getId()] = $oneGeoType->getDenomination1();
        }

        return $data;
    }

    /**
     *retourne les identifiants des geolocalisations
     *  N2 séléctionnés de type 0.
     */
    public function retrieveCheckedGeoN2()
    {
        $result = '';
        foreach ($this->repeaterGeoN0->getItems() as $itemN0) {
               // test sur le type de geo
            if (0 == $itemN0->typeN0->Value) {
                if ($itemN0->selectiongeoN0Select->checked) {
                    foreach ($itemN0->repeaterGeoN1->getItems() as $itemN1) {
                        foreach ($itemN1->repeaterGeoN2_1->getItems() as $itemN2_1) {
                            if ($itemN2_1->dep1->checked) {
                                $geoN2Name = $itemN2_1->geoN2Id_1->getValue();
                                $result = $result.$geoN2Name.',';
                            }
                        }
                        foreach ($itemN1->repeaterGeoN2_2->getItems() as $itemN2_2) {
                            if ($itemN2_2->dep2->checked) {
                                $geoN2Name = $itemN2_2->geoN2Id_2->getValue();
                                $result = $result.$geoN2Name.',';
                            }
                        }
                        foreach ($itemN1->repeaterGeoN2_3->getItems() as $itemN2_3) {
                            if ($itemN2_3->dep3->checked) {
                                $geoN2Name = $itemN2_3->geoN2Id_3->getValue();
                                $result = $result.$geoN2Name.',';
                            }
                        }
                    }
                }
            }
        }

        return substr($result, 0, -1);
    }

    /**
     * Génére une fonction javascript pour chaque geolocalisation de type 1
     * qui retourne les identifiants des geolocalisations N2 séléctionnés.
     */
    public function generateType1JsFunc($index, $list2Id)
    {
        $nameFunction = 'retrieveType1SelectedGeoN2'.$index;
        $jsFunction = '
                   function '.$nameFunction."() {
                       var listValueType1 ='';
                           var listType1 = document.getElementById('".$list2Id."');
        					var totalList = listType1.options.length;
        					for (i=0;i<totalList;i++) {
        						listValueType1 = listValueType1+','+(listType1.options[i].value);
    							}
    				return listValueType1;
    				}";
        $this->script->Text .= '<script>'.$jsFunction.'</script>';

        return [$nameFunction, $jsFunction];
    }

    /**
     * génére une fonction javascript lier au boutton valider
     * retourne les identifiants séléctionnés.
     */
    public function generateOnClickJsFunc()
    {
        $jsCalls = '';
        foreach (array_keys($this->_arJsFunctions) as $jsFuncName) {
            $jsCalls .= 'arSelectedN2 += '.$jsFuncName.'();';
        }
        $onClickJsFunc = "
        	function onClickJsFunc()
        	{
        		var arSelectedN2 = '';
        		".
        $jsCalls
        ."
        		document.getElementById('".$this->selectedType1GeoN2->getClientId()."').value += arSelectedN2;

        	}

        ";
        $this->script->Text .= '<script>'.$onClickJsFunc.'</script>';
        $this->validateButton->Attributes->OnClick = "selectAllLocalisationsItems('".$this->idListeLocalisation->Value."');onClickJsFunc();";
    }

    /**
     * passer tout les geolocalisations N2 séléctionnés
     * de type 0 et 1 en parametre de la fonction js.
     */
    public function retrieveAllSelectedGeo()
    {
        $checkedOnMap = self::retrieveCheckedGeoCarte();
        $checkedType0 = self::retrieveCheckedGeoN2();
        $checkedType1 = $this->selectedType1GeoN2->Value;
        if ('' != $checkedType0.$checkedType1.$checkedOnMap) {
            $lieux = ','.$checkedType0.$checkedType1.','.$checkedOnMap.',';
            if ($this->addFavori->checked) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
                if ($agent instanceof CommonAgent) {
                    $agent->setLieuExecution($lieux);
                    $agent->save($connexionCom);
                }
            }
        } else {
            $lieux = '';
        }

        $checkedOnMap_buffer = self::getCheckedOnMapCodeList();
        if (isset($_GET['sf'])) {
            Atexo_CurrentUser::writeToSession('lieux', $lieux);
        }

        $this->script2->Text = "<script>returnSelectedLieuxExec('".$lieux."','".$checkedOnMap_buffer."');</script>";
        $this->script->Text = '';
    }

    public function getCheckedOnMapCodeList()
    {
        $tab = $_POST['localisations'];
        $buffer = '';

        if (!empty($tab)) {
            $buffer = implode('_', $tab);
        }

        return $buffer;
    }

    public static function writeValue(CommonGeolocalisationN0 $valueName, $methodeName)
    {
        if ($_GET['lang']) {
            $langue = Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['lang']));
        } else {
            $langue = Atexo_CurrentUser::readFromSession('lang');
        }
        $methodeName = 'get'.$methodeName;
        if ($langue) {
            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                                                                           || !$valueName->$methodeName()) {
                $valueLangue = $methodeName;
                $value = $valueName->$valueLangue();
            } else {
                $valueLangue = $methodeName.Atexo_Languages::getLanguageAbbreviation($langue);
                $value = $valueName->$valueLangue();
            }
        } else {
            $valueLangue = $methodeName;
            $value = $valueName->$valueLangue();
        }

        return $value;
    }

    /**
     * Retourne les denominations des géolocalisations N2.
     *
     * @param géolocalisation N2
     */
    public function getGeo2Denomination1($geolocalisationN2)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getDenomination1 = 'getDenomination1'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                                                                           || !$geolocalisationN2->$getDenomination1()) {
            return $geolocalisationN2->getDenomination1();
        } else {
            return $geolocalisationN2->$getDenomination1();
        }
    }

    /**
     * Retourne les denominations des géolocalisations N1.
     *
     * @param géolocalisation N1
     */
    public function getGeo1Denomination1($geolocalisationN1)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getDenomination1 = 'getDenomination1'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                                                                           || !$geolocalisationN1->$getDenomination1()) {
            return $geolocalisationN1->getDenomination1();
        } else {
            return $geolocalisationN1->$getDenomination1();
        }
    }

    public function retrieveCheckedGeoCarte()
    {
        $liste = $_POST['localisations']; //print_r($liste);exit;
        $arrayIdGeolocalisation = [];

        if (is_array($liste)) {
            foreach ($liste as $denominationN2) {
                $denominationN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByDenomination2($denominationN2);
                if ($denominationN2 instanceof CommonGeolocalisationN2) {
                    $arrayIdGeolocalisation[] = $denominationN2->getId();
                }
            }
        }
        if (count($arrayIdGeolocalisation)) {
            return implode(',', $arrayIdGeolocalisation);
        } else {
            return '';
        }
    }
}
