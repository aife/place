<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;

/**
 * Page de création compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupDomainesActivites extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->displayCategorie();
        }
    }

    public function displayCategorie()
    {
        $listeCat = Atexo_Consultation_Category::retrieveCategories(true, true);
        $this->repeaterCategorie->DataSource = $listeCat;
        $this->repeaterCategorie->DataBind();
    }

    public function selectedCategories($sender, $param)
    {
        $listeIds = '';
        foreach ($this->repeaterCategorie->getItems() as $oneItem) {
            if ($oneItem->idCategorie->Checked) {
                $listeIds .= $oneItem->idCategorie->Value.'#';
            }
            foreach ($oneItem->repeaterSousCategorie->getItems() as $oneItemSousCat) {
                if ($oneItemSousCat->idSection->Checked) {
                    $listeIds .= $oneItemSousCat->idSection->Value.'#';
                }
                foreach ($oneItemSousCat->repeaterCatNiv3->getItems() as $oneItemSousCatN3) {
                    if ($oneItemSousCatN3->idBranche->Checked) {
                        $listeIds .= $oneItemSousCatN3->idBranche->Value.'#';
                    }
                }
            }
        }
        $this->script->Text = "<script>returnSelectedCate('".$listeIds."','".Atexo_Util::atexoHtmlEntities($_GET['clientId'])."');</script>";
    }

    /**
     * Selecionner la categorie si elle est deja choisi.
     */
    public function exestingDomaines($idCat)
    {
        $idsDomaines = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['ids']));
        if (!$idsDomaines) {
            return false;
        }
        $arIds = explode('#', $idsDomaines);
        if (in_array($idCat, $arIds)) {
            return true;
        }

        return false;
    }

    public function getLibelleTraduit()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $libelle = 'libelle_'.$langue;
        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$libelle) {
            return 'libelle';
        } else {
            return $libelle;
        }
    }
}
