<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTIllustrationFond;
use Application\Propel\Mpe\CommonTIllustrationFondPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_IllustrationFond;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Classe de.
 *
 * @author Loubn EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class IllustrationAgent extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $illustration = null;
        if (isset($_COOKIE['idIllustration']) && $_COOKIE['idIllustration']) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $illustration = CommonTIllustrationFondPeer::retrieveByPK($_COOKIE['idIllustration'], $connexion);
        } else {
            $idAgent = Atexo_CurrentUser::getIdAgentConnected();
            if ($idAgent) {
                $illustration = (new Atexo_IllustrationFond())->getIllustrationFondByAgent($idAgent, Atexo_Config::getParameter('ILLUSTRATION_DE_FOND_ACTIVER'), true);
                if ($illustration instanceof CommonTIllustrationFond) {
                    setcookie('idIllustration', $illustration->getIdIllustrationFond());
                    $_COOKIE['idIllustration'] = $illustration->getIdIllustrationFond();
                }
            }
        }
        if ($illustration instanceof CommonTIllustrationFond) {
            $idBlob = $illustration->getIdBlobImage();
            $atexoBlob = new Atexo_Blob();
            $content = $atexoBlob->return_blob_to_client($idBlob, Atexo_Config::getParameter('COMMON_BLOB'));
            $tableau = explode('.', $illustration->getNomImage());
            // retourne les caractere apres le dernier .
            $extension = $tableau[count($tableau) - 1];
            switch (strtolower($extension)) {
                case 'jpeg':
                case 'jpg':
                    header('Content-type: image/jpeg');
                    break;
                case 'png':
                    header('Content-type: image/png');
                    break;
                default:
                    header('Content-type: image/gif');
            }

            $expires = 0;
            header('Content-Length: '.strlen($content));
            header('Expires: '.gmdate('D, d M Y H:i(worry)', time() + $expires).' GMT');
            header("Cache-Control: max-age=$expires");
            header('Pragma: ');
            header('HTTP/1.0 200 OK');
            header('Status: 200 OK');
            echo $content;
            exit;
        } else {
            echo '';
            exit;
        }
    }
}
