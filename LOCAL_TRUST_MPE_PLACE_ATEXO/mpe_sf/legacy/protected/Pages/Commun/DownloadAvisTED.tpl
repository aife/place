<com:TContent ID="CONTENU_PAGE">
	<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<div class="breadcrumbs"><a href="#"><com:TTranslate>DEFINE_TEXT_MARCHES</com:TTranslate></a>&gt; <com:TTranslate>DEFINE_TEXT_AVIS</com:TTranslate></div>
		<!--Debut message de confirmation-->
		<com:TActivePanel id="panelErreurMessage">
			<com:PanelMessageErreur id="erreurTelechargement"/>
		</com:TActivePanel>
		 <!--Fin message de confirmation-->
		 <form action="" id="infos-Entreprise">
		 	<!--Debut line boutons-->
			<div class="boutons-line">
				<com:THyperLink 
					id="linkRetourBas" 
					NavigateUrl="index.php?page=Agent.PubliciteAvis&id=<%=base64_encode( Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id']))%>"
					CssClass="bouton-retour" 
					Attributes.title="<%=Prado::localize('DEFINE_TEXT_RETOUR_ACCUEIL')%>"
			    >
					<com:TTranslate>TEXT_RETOUR</com:TTranslate>
				</com:THyperLink>
			</div>
			<!--Fin line boutons-->
		 </form>
	</div>
	<!--Fin colonne droite-->
</com:TContent>