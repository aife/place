<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonDestinataireAnnonceJAL;
use Application\Propel\Mpe\CommonDestinataireCentralePub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use Prado\Prado;

/**
 * Classe CommissionDetailEchangeMail.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE4
 */
class CommissionDetailEchangeMail extends MpeTPage
{
    protected $orgAcronyme;
    protected $idMsg;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $objetMessage = null;
								$org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->orgAcronyme = $org;
        $this->accusereception->_acronymeOrg = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->accusereception->_numAccuseReception = Atexo_Util::atexoHtmlEntities($_GET['num_ar']);

        $echangeDest = Atexo_Message::getechangeDestinataireByUid(Atexo_Util::atexoHtmlEntities($_GET['num_ar']), $org);
        if ($echangeDest) {
            if ($echangeDest->getIdEchange()) {
                $objetMessage = Atexo_Message::retrieveMessageById($echangeDest->getIdEchange(), $org);
            }
            $this->accusereception->_refConsultation = $objetMessage->getConsultationId();
            if (!$this->IsPostBack) {
                $this->messageconfirm->setMessage(Prado::localize('DEFINE_TEXT_MESSAGE_CONFIRMATION'));
                if ($echangeDest) {
                    $this->idMsg = $echangeDest->getIdEchange();
					   if($echangeDest->getAr() != Atexo_Config::getParameter('ACCUSE_RECEPTION_VALIDE')){
                        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $echangeDest->setAr(Atexo_Config::getParameter('ACCUSE_RECEPTION_VALIDE'));
                        $echangeDest->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE'));
                        $echangeDest->setDateAr(date('Y-m-d H:i:s'));
                        $echangeDest->save($connexionCom);
                    }
                    $this->contenuEchange->setVisible(true);
                    $this->accusereception->remplirInfoEchange($echangeDest, $org);
                    $this->updateAR($this->getTableToUpByEchangeDestinataire($echangeDest, $org), $org);
                }
                $this->idretour->NavigateUrl = 'index.php?page=Entreprise.EntrepriseHome';
                if (Atexo_Module::isEnabled('PanierEntreprise')) {
                }
            }
        } else {
            $this->errorMsg->setVisible(true);
            $this->errorMsg->setMessage(Prado::localize('TEXT_ERREUR_URL'));
            $this->contenuEchange->setVisible(false);
        }
    }

    public function getTableToUpByEchangeDestinataire($echangeDest, $org)
    {
        $desObject = null;
								if ($echangeDest) {
            $desCentralePub = (new Atexo_Publicite_CentralePublication())->retrieveDestinataireCentralPubByEchangeDestinataire($echangeDest, $org);
            $desAnnonceJal = (new Atexo_Publicite_DestinataireJAL())->retrieveDestinataireAnnonceJalByEchangeDestinataire($echangeDest, $org);

            if ($desCentralePub) {
                $desObject = $desCentralePub;
            } elseif ($desAnnonceJal) {
                $desObject = $desAnnonceJal;
            }
            if ($desObject) {
                return $desObject;
            } else {
                return false;
            }
        }
    }

    public function updateAR($obj, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($obj instanceof CommonDestinataireCentralePub || $obj instanceof CommonDestinataireAnnonceJAL) {
            $obj->setAccuse(1);
            $obj->setdateAr(date('Y-m-d H:i:s'));
            $obj->save($connexion);
        }
    }
}
