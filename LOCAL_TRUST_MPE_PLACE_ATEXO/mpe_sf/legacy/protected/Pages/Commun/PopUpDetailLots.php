<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Prado\Prado;

/**
 * Formulaire de recherche avancée.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpDetailLots extends MpeTPage
{
    protected $reference;
    protected $organisme;
    protected $langueEnSession;
    protected ?string $cautionProvisoire = null;
    protected ?string $qualification = null;
    protected ?string $agrement = null;
    protected ?string $echantillonsDemandes = null;
    protected ?string $reunion = null;
    protected ?string $visiteDesLieux = null;
    protected ?string $affichageCodeCPV = null;
    protected ?string $variante = null;

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->customizeForm();
        // Gestion de l'internationalisation
        $globalization = $this->getApplication()->Modules['globalization'];

        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
            $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();
            $sessionLang = Atexo_CurrentUser::readFromSession('lang');

            if (isset($_GET['lang']) && in_array($_GET['lang'], $arrayActiveLangages)) {
                $this->langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            } elseif (isset($sessionLang) && in_array($sessionLang, $arrayActiveLangages)) {
                $this->langueEnSession = $sessionLang;
            } else {
                $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            }
        } else {
            $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        // Enregistrement en session
        Atexo_CurrentUser::writeToSession('lang', $this->langueEnSession);

        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
            $globalization->setTranslationCatalogue('messages.'.$this->langueEnSession);
        } else {
            $globalization->setTranslationCatalogue('messages.fr');
        }

        $this->dataBind();
        $this->reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);

        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s'),
            $this->reference
        );
    }

    /**
     * retourne les lots d'une consultation.
     */
    public function getAllLots()
    {
        $lots = [];
        $this->reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->reference, $connexionCom);
        $lots = $consultation->getAllLots($connexionCom, $consultation->getOrganisme(), $consultation->getId());

        return $lots;
    }

    /**
     * rempli le repeater des lots.
     */
    public function dataBind()
    {
        $lots = $this->getAllLots();
        $this->repeaterLots->DataSource = $lots;
        $this->repeaterLots->DataBind();
        $indexLot = 0;
        foreach ($this->repeaterLots->getItems() as $item) {
            //<!-- Module referentiel CPV
            $atexoRef = new Atexo_Ref();
            $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
            $atexoRef->setPrincipal($lots[$indexLot]->getCodeCpv1());
            $atexoRef->setSecondaires($lots[$indexLot]->getCodeCpv2());
            $item->idAtexoRef->afficherReferentiel($atexoRef);

            //-->
            $caution = 0;
            $donnesComplementaitre = $lots[$indexLot]->getDonneComplementaire();

            if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                $caution = $donnesComplementaitre->getCautionProvisoire();
            }
            $item->cautionProvisoire->text = Atexo_Util::getMontantArronditEspace($caution, 2).' '.Prado::localize('TEXT_CURRENCY');
            if ($libellesQualification = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($lots[$indexLot]->getQualification())) {
                $textQualification = '';
                $arrayLibellesQualif = explode(' , ', $libellesQualification);
                foreach ($arrayLibellesQualif as $oneLibelleQualif) {
                    if ($oneLibelleQualif) {
                        $textQualification .= '<li>'.$oneLibelleQualif.'</li>';
                    }
                }
                $item->qualification->Text = "<ul class='default-list'>".$textQualification.'</ul>';
            } else {
                $item->qualification->Text = '-';
            }
            if ($libellesAgrements = (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($lots[$indexLot]->getAgrements())) {
                $textAgrements = '';
                $agLibelles = explode(' , ', $libellesAgrements);
                foreach ($agLibelles as $oneAgrementLibelle) {
                    if ($oneAgrementLibelle) {
                        $textAgrements .= '<li>'.$oneAgrementLibelle.'</li>';
                    }
                }
                $item->agrements->Text = "<ul class='default-list'>".$textAgrements.'</ul>';
            } else {
                $item->agrements->Text = '-';
            }

            if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                if ('' != $donnesComplementaitre->getAddEchantillonTraduit() && '' != $donnesComplementaitre->getDateLimiteEchantillon()) {
                    $item->dateEchantillons->text = Atexo_Util::iso2frnDateTime($donnesComplementaitre->getDateLimiteEchantillon());
                    $item->adresseEchantillons->text = $donnesComplementaitre->getAddEchantillonTraduit();
                } else {
                    $item->labelDateHeureLimiteEchantillon->setVisible(false);
                    $item->dateEchantillons->Text = '-';
                    $item->adresseEchantillons->setVisible(false);
                }
                if ('' != $donnesComplementaitre->getAddReunionTraduit() && '' != $donnesComplementaitre->getDateReunion()) {
                    $item->dateReunion->text = Atexo_Util::iso2frnDateTime($donnesComplementaitre->getDateReunion());
                    $item->adresseReunion->text = $donnesComplementaitre->getAddReunionTraduit();
                } else {
                    $item->labelDateHeureReunion->setVisible(false);
                    $item->dateReunion->Text = '-'; //Prado::localize('TEXT_PAS_REUNION');
                    $item->adresseReunion->setVisible(false);
                }

                $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($lots[$indexLot]->getConsultationId(), $lots[$indexLot]->getLot(), $this->organisme);
                $item->repeaterVisitesLieux->dataSource = $visitesLieux;
                $item->repeaterVisitesLieux->dataBind();
                if (!$visitesLieux) {
                    $item->panelRepeaterVisitesLieux->setVisible(false);
                    $item->panelPasVisitelieu->setVisible(true);
                }
            }

            $variante = '';
            $donnesComplementaitre = $lots[$indexLot]->getDonneComplementaire();
            if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                $variante = $donnesComplementaitre->getVariantes();
            }
            if ('1' == $variante) {
                $variante = Prado::Localize('TEXT_OUI');
            } else {
                $variante = Prado::Localize('TEXT_NON');
            }
            $item->varianteValeur->Text = $variante;
            $item->ltrefLot->afficherReferentielConsultation($this->reference, $lots[$indexLot]->getLot(), $this->organisme);
            $item->idReferentielZoneTextLot->afficherTextConsultation($this->reference, $lots[$indexLot]->getLot(), $this->organisme);
            $item->idRefRadio->afficherReferentielRadio($this->reference, $lots[$indexLot]->getLot(), $this->organisme);
            ++$indexLot;
        }
    }

    /**
     * retourne la categorie du lot.
     */
    public function getCategoryLot($categorie)
    {
        $categorieConsultationObj = Atexo_Consultation_Category::retrieveCategorie($categorie, true);

        $abbreviationLangue = '';

        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $abbreviationLangue = strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }
        $libelleTraduit = 'getLibelle'.$abbreviationLangue;

        if ($categorieConsultationObj) {
            if ($categorieConsultationObj->$libelleTraduit()) {
                return $categorieConsultationObj->$libelleTraduit();
            } else {
                return $categorieConsultationObj->getLibelle();
            }
        }
    }

    public function getDescriptionLot($categorieLot)
    {
        $nomFonction = 'getDescription';
        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $nomFonction .= strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }

        if ($categorieLot) {
            return $categorieLot->$nomFonction() ?: $categorieLot->getDescription();
        }
    }

    public function getDescriptionDetail($categorieLot)
    {
        $nomFonction = 'getDescriptionDetail';
        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $nomFonction .= strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }
        if ($categorieLot) {
            return $categorieLot->$nomFonction() ?: $categorieLot->getDescriptionDetail();
        }
    }

    /***
     * afficher les champs qui sont propre à mpe Maroc
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            $this->affichageCodeCPV = 'Dynamic';
        } else {
            $this->affichageCodeCPV = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires', Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']))) {
            $this->cautionProvisoire = 'Dynamic';
        } else {
            $this->cautionProvisoire = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->qualification = 'Dynamic';
        } else {
            $this->qualification = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->agrement = 'Dynamic';
        } else {
            $this->agrement = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->echantillonsDemandes = 'Dynamic';
        } else {
            $this->echantillonsDemandes = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->reunion = 'Dynamic';
        } else {
            $this->reunion = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->visiteDesLieux = 'Dynamic';
        } else {
            $this->visiteDesLieux = 'None';
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires', Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']))) {
            $this->variante = 'Dynamic';
        } else {
            $this->variante = 'None';
        }
    }

    public function getCodeCpvSecondaire($codeSecondaire)
    {
        return trim(str_replace('#', ';', $codeSecondaire), ';');
    }
}
