<com:TContent id="CONTENU_PAGE">
	<com:TConditional condition="$this->getModeServeurCrypto() == 1" >
		<prop:trueTemplate>
			<%include Application.templates.crypto.DiagnosticPoste %>
		</prop:trueTemplate>
		<prop:falseTemplate>
			<%include Application.templates.mpe.DiagnosticPoste %>
		</prop:falseTemplate>
	</com:TConditional>
</com:TContent>
