<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
    <div class="guideUtilitaireDV detailConsultaion container m-t-3">
        <div class=" panel panel-default">
            <div class="panel-heading">
                <h1 class="h4 panel-title">
                    <com:TTranslate>TEXT_GUIDE_INSTALATION_ET_UTILISATION</com:TTranslate>
                </h1>
            </div>
            <div class="panel-body">
                <div class="description-text m-b-4">

                </div><com:TTranslate>DESCRIPTION_GUIDE_INSTALATION_ET_UTILISATION</com:TTranslate>

                <!--DEBUT NAVIGATION-->

                <!--ONGLETS-->
                <ul class="nav nav-tabs hide-agent" id="targetRechercheAvance--m" role="tablist">
                    <li class="active" role="presentation">
                        <a href="#windows" aria-controls="windows" role="tab" data-toggle="tab">
                            <h2 class="h3 panel-title">
                                <i class="fa fa-windows m-r-1"></i>
                                <com:TTranslate>TEXT_WINDOWS</com:TTranslate>
                            </h2>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#macOS" aria-controls="macOS" role="tab" data-toggle="tab">
                            <h2 class="h3 panel-title">
                                <i class="fa fa-apple m-r-1"></i>
                                <com:TTranslate>TEXT_MACOS</com:TTranslate>
                            </h2>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#linux" aria-controls="linux" role="tab" data-toggle="tab">
                            <h2 class="h3 panel-title">
                                <i class="fa fa-linux m-r-1"></i>
                                <com:TTranslate>TEXT_LINUX</com:TTranslate>
                            </h2>
                        </a>
                    </li>
                </ul>

                <!--CONTENU-->
                <div class="tab-content form-content">

                    <!--DEBUT WINDOWS-->
                    <div class="panel panel-default panel-content tab-pane fade in active" id="windows">
                        <div class="panel-body ">
                            <div class="bloc-rmc">

                                <!--ETAPE 1-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_1_RECUPERATION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>LIEN_TELECHARGEMENT_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        <a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_UTILITAIRE_DOWNLOAD_DV_WINDOWS')%>">
                                            <com:TTranslate>ICI</com:TTranslate>
                                        </a>.
                                    </div>
                                </div>

                                <!--ETAPE 2-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_2_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 3-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_3_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 4-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_4_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--FIN WINDOWS-->

                    <!--DEBUT macOS-->
                    <div class="panel panel-default panel-content tab-pane fade" id="macOS">
                        <div class="panel-body ">
                            <div class="bloc-rmc">

                                <!--ETAPE 1-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_1_RECUPERATION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>LIEN_TELECHARGEMENT_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        <a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_UTILITAIRE_DOWNLOAD_DV_MAC')%>">
                                            <com:TTranslate>ICI</com:TTranslate>
                                        </a>.
                                    </div>
                                </div>

                                <!--ETAPE 2-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_2_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 3-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_3_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 4-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_4_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--FIN macOS-->

                    <!--DEBUT LINUX-->
                    <div class="panel panel-default panel-content tab-pane fade" id="linux">
                        <div class="panel-body ">
                            <div class="bloc-rmc">

                                <!--ETAPE 1-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_1_RECUPERATION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>LIEN_TELECHARGEMENT_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        <a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_UTILITAIRE_DOWNLOAD_DV_LINUX')%>">
                                            <com:TTranslate>ICI</com:TTranslate>
                                        </a>.
                                    </div>
                                </div>

                                <!--ETAPE 2-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_2_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_EXTRACTION_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 3-->
                                <div class="section clearfix m-b-3">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_3_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_OUVERTURE_UTILITAIRE_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>

                                <!--ETAPE 4-->
                                <div class="section clearfix">
                                    <div class="section-heading page-header m-t-1 p-b-0">
                                        <h3 class="h4">
                                            <com:TTranslate>TEXT_ETAPE_4_TELECHARGEMENT_DV</com:TTranslate>
                                        </h3>
                                    </div>
                                    <div class="section-body clearfix">
                                        <com:TTranslate>DESCRIPTION_TELECHARGEMENT_DV_WINDOWS</com:TTranslate>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--FIN LINUX-->
                </div>
                <!--FIN NAVIGATION-->
            </div>
        </div>
    </div>
    <com:TActiveLabel ID="scriptJs"/>
</com:TContent>