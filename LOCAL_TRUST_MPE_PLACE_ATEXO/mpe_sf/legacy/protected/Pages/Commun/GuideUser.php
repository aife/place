<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

class GuideUser extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
    }

    public function onLoad($param)
    {
        if (!isset($_GET['calledFrom']) || (!Atexo_Module::isEnabled('PublierGuides'))) {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        } else {
            if (!Atexo_CurrentUser::isConnected()) {
                if (isset($_GET['calledFrom']) && 'agent' == $_GET['calledFrom']) {
                    $this->response->redirect('index.php?page=Agent.AgentHome');
                }
                if (isset($_GET['calledFrom']) && 'entreprise' == $_GET['calledFrom']) {
                    $this->response->redirect('index.php?page=Entreprise.EntrepriseHome');
                }
            }
        }
    }
}
