<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContactContratQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_BourseCotraitance;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CoffreFort;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Prestation;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Composant Detail de l'Entreprise.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailEntrepriseMere extends MpeTPage
{
    public string $_calledFrom = '';

    public function onInit($param)
    {

        //Atexo_Languages::setLanguageCatalogue("agent");
        $this->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['callFrom']));
        Atexo_Languages::setLanguageCatalogue($this->getCalledFrom());
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onLoad($param)
    {
        $offre = null;
        try {
            $idEntreprise = Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']);
            $this->customizeForm();
            $refCons = $_GET['id'];
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            if (('entreprise' == $this->getCalledFrom() && $this->entrepriseAutoriseAcces() && Atexo_Module::isEnabled('BourseALaSousTraitance'))
            || ('entreprise' == $this->getCalledFrom() && Atexo_Module::isEnabled('BourseCotraitance') && self::entrepriseInscrite($idEntreprise, $refCons))
            || (('agent' == $this->getCalledFrom()) && ($this->hasAccesFornisseurDB()))) {
                $idEntreprise = Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']);
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexion, false);
                $this->idIdentificationEntreprise->setIdEntreprise($idEntreprise);
                $this->idDirigeantsEntreprise->setIdEntreprise($idEntreprise);
                $this->idActiviteEntreprise->setIdEnreprise($idEntreprise);
                $this->idActiviteEntreprise->setCalledFrom('agent');
                $this->idDonneesComplementaires->setIdEntreprise($idEntreprise);
                $this->idDonneesComplementaires->displayDonneeComplementaire();
                if ($entreprise) {
                    if ('E' == $_GET['type']) {
                        $offre = CommonOffresQuery::create()->filterById($_GET['idO'])->filterByEntrepriseId($idEntreprise)->findOne($connexion);
                    } elseif ('P' == $_GET['type']) {
                        $offre = CommonOffrePapierQuery::create()->filterById($_GET['idO'])->filterByEntrepriseId($idEntreprise)->findOne($connexion);
                    }
                    if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
                        $contact = new CommonTContactContrat();
                        $contact->setIdEtablissement($offre->getIdEtablissement());
                        $contact->setNom($offre->getNom());
                        $contact->setPrenom($offre->getPrenom());
                        $contact->setEmail($offre->getEmail());
                        $contact->setTelephone($offre->getTelephone());
                        $this->idIdentificationEntreprise->setSoumissionnaire($contact);
                    }

                    $contactContrat = CommonTContactContratQuery::create()->filterByIdContactContrat($_GET['idC'])->filterByIdEntreprise($idEntreprise)->findOne($connexion);
                    if ($contactContrat instanceof CommonTContactContrat) {
                        $this->idIdentificationEntreprise->setSoumissionnaire($contactContrat);
                    }

                    if ('agent' == $this->getCalledFrom()) {
                        $paysSiren = $entreprise->getPaysenregistrement().($entreprise->getSiren() ? ($entreprise->getPaysenregistrement() ? ' - ' : '').$entreprise->getSiren() : '');
                        if ($paysSiren) {
                            $this->paysSiren->text = '('.$paysSiren.')';
                        }
                        $this->idRaisonSociale->Text = $entreprise->getNom();
                    }
                    $this->labelSiteInternet->Text = ($entreprise->getSiteInternet());
                    $this->labelDescriptionActivite->Text = ($entreprise->getDescriptionActivite());
                    $this->labelActiviteDefense->Text = ($entreprise->getActiviteDomaineDefense());
                    $this->labelQualifications->Text = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($entreprise->getQualification());
                    $this->labelAgrements->Text = (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($entreprise->getAgrement());
                    $this->labelleDocCommercial->Text = $entreprise->getIntituleDocumentsCommerciaux();
                    $sizePjDocumentsCommerciaux = Atexo_Util::arrondirSizeFile($entreprise->getTailleDocumentsCommerciaux() / 1024);
                    if ($sizePjDocumentsCommerciaux > 0) {
                        $this->panelLinkDownloadPj->setDisplay('Dynamic');
                        $this->sizeDocCommercial->Text = $sizePjDocumentsCommerciaux;
                    }
                    $this->labelMoyensHumains->Text = $entreprise->getMoyensHumains();
                    $this->labelMoyensTechniques->Text = $entreprise->getMoyensTechnique();
                    $prestations = Atexo_Entreprise_Prestation::retrievePrestations($idEntreprise);
                    if (is_array($prestations)) {
                        $this->prestationEntreprise->repeaterPrestations->dataSource = $prestations;
                        $this->prestationEntreprise->repeaterPrestations->dataBind();
                    }
                    if ('entreprise' == $this->getCalledFrom()) {
                        if ((Atexo_Module::isEnabled('BourseALaSousTraitance'))) {
                            $contact = $entreprise->getContactByIdEntreprise($entreprise->getId());
                            if ($contact) {
                                $this->nomContact->Text = $contact->getNom();
                                $this->prenomContact->Text = $contact->getPrenom();
                                $this->adresseContact->Text = $contact->getAdresse();
                                $this->adresseSuiteContact->Text = $contact->getAdresseSuite();
                                $this->cpContact->Text = $contact->getCodePostal();
                                $this->VilleContact->Text = $contact->getVille();
                                $this->fonctionContact->Text = $contact->getFonction();
                                $this->emailContact->Text = $contact->getEmail();
                                $this->telContact->Text = $contact->gettelephone();
                            }
                            $this->repeaterTypeCollaboration->DataSource = $this->getArrayTypeColloboration($entreprise->getTypeCollaboration());
                            $this->repeaterTypeCollaboration->DataBind();
                        } elseif (Atexo_Module::isEnabled('BourseCotraitance')) {
                            $tBourseCotraitance = CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($refCons)->filterByIdEntreprise($idEntreprise)->findOne($connexion);
                            $this->nomContact->Text = $tBourseCotraitance->getNomInscrit();
                            $this->prenomContact->Text = $tBourseCotraitance->getPrenomInscrit();
                            $this->adresseContact->Text = $tBourseCotraitance->getAdresseInscrit();
                            $this->adresseSuiteContact->Text = $tBourseCotraitance->getAdresse2Incsrit();
                            $this->cpContact->Text = $tBourseCotraitance->getCpInscrit();
                            $this->VilleContact->Text = $tBourseCotraitance->getVilleInscrit();
                            $this->fonctionContact->Text = $tBourseCotraitance->getFonctionInscrit();
                            $this->emailContact->Text = $tBourseCotraitance->getEmailInscrit();
                            $this->telContact->Text = $tBourseCotraitance->getTelMobileInscrit();
                            $dataSource = [];
                            if ($tBourseCotraitance->getMandataireGroupement()) {
                                $dataSource['1']['id'] = '1';
                                $dataSource['1']['libelle'] = Prado::localize('MANDATAIRE_DE_GROUPEMENT');
                            }
                            if ($tBourseCotraitance->getCotraitantSolidaire()) {
                                $dataSource['2']['id'] = '2';
                                $dataSource['2']['libelle'] = Prado::localize('TEXT_CO_TRAITANT_SOLIDAIRE');
                            }
                            if ($tBourseCotraitance->getCotraitantConjoint()) {
                                $dataSource['3']['id'] = '3';
                                $dataSource['3']['libelle'] = Prado::localize('TEXT_CO_TRAITANT_CONJOINT');
                            }
                            $this->repeaterTypeCollaboration->DataSource = $dataSource;
                            $this->repeaterTypeCollaboration->DataBind();
                        }
                    }

                    if ((('agent' == $this->getCalledFrom()) && ($this->hasAccesFornisseurDB()))) {
                        $this->panelEtablissements->setVisible(true);
                        self::chargerInfoEtablissements($entreprise);
                    }
                }
                if (!$this->IsPostBack) {
                    $dataDocumentsCfeVisibleAgent = (new Atexo_Entreprise_CoffreFort())->retrievePiecesCoffreFortByIdEntreprise($idEntreprise);
                    if (!is_array($dataDocumentsCfeVisibleAgent)) {
                        $dataDocumentsCfeVisibleAgent = [];
                    }
                    if (count($dataDocumentsCfeVisibleAgent) > 0) {
                        $this->tableauDocumentsDuCoffreFort->setDataSource($dataDocumentsCfeVisibleAgent);
                    } else {
                        $this->aucunDocCoffreFort->setVisible(true);
                        $this->aucunDocCoffreFort->Text = Prado::localize('TEXT_AUCUN_DOC_DISPONIBLE');
                    }//Domaines d'activités Lt-Ref
                    if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
                        $atexoDomainesActivitesRef = new Atexo_Ref();
                        $atexoDomainesActivitesRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
                        if ($entreprise->getDomainesActivites()) {
                            $atexoDomainesActivitesRef->setSecondaires($entreprise->getDomainesActivites());
                        }
                        $this->idAtexoRefDomaineActivite->afficherReferentiel($atexoDomainesActivitesRef);
                    }// affichage des certificats des comptes entreprises Lt-Ref
                    if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
                        $atexoCertificatCompteEtpRef = new Atexo_Ref();
                        $atexoCertificatCompteEtpRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CERTIFICAT_COMPTE_ENTREPRISE_CONFIG'));
                        if ($entreprise->getAgrement()) {
                            $atexoCertificatCompteEtpRef->setSecondaires($entreprise->getAgrement());
                        }
                        $this->idAtexoCertificatCompteEtpRef->afficherReferentiel($atexoCertificatCompteEtpRef);
                    }// affichage des qualifications des comptes entreprises Lt-Ref
                    if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
                        $atexoRefQualification = new Atexo_Ref();
                        $atexoRefQualification->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_QUALIFICATION_CONFIG'));
                        if ($entreprise->getQualification()) {
                            $atexoRefQualification->setSecondaires($entreprise->getQualification());
                        }
                        $this->idAtexoRefQualification->afficherReferentiel($atexoRefQualification);
                    }
                    //echo $idEntreprise;exit;
                    $this->ltrefEntr->afficherReferentielEntreprise($idEntreprise);
                }
            } else {
                exit; //TODO
            }
        } catch (Exception $e) {
            //echo $e->getMessage();exit;
            Prado::log('Erreur DetailEntrepriseMere.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'DetailEntrepriseMere.php');
        }
    }

    public function fillRepeaterTypeCollaboration()
    {
        $arrayType = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_COLLABORATION'));
        foreach ($arrayType as $key => $value) {
            $dataSource[$key]['id'] = $key;
            $dataSource[$key]['libelle'] = $value;
        }
        $this->repeaterTypeCollaboration->DataSource = $dataSource;
        $this->repeaterTypeCollaboration->DataBind();
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) {
            $this->panelActiviteDomaineDefense->setDisplay('Dynamic');
        } else {
            $this->panelActiviteDomaineDefense->setDisplay('None');
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseDocumentsCommerciaux')) {
            $this->panelDocumentsComerciaux->setDisplay('Dynamic');
        } else {
            $this->panelDocumentsComerciaux->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualification') && ('agent' == $this->getCalledFrom())) {
            $this->panelQualification->setDisplay('Dynamic');
        } else {
            $this->panelQualification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrement') && ('agent' == $this->getCalledFrom())) {
            $this->panelAgrements->setDisplay('Dynamic');
        } else {
            $this->panelAgrements->setDisplay('None');
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseMoyensTechniques') && ('agent' == $this->getCalledFrom())) {
            $this->panelMoyensTechnique->setDisplay('Dynamic');
        } else {
            $this->panelMoyensTechnique->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseMoyensHumains') && ('agent' == $this->getCalledFrom())) {
            $this->panelMoyensHumains->setDisplay('Dynamic');
        } else {
            $this->panelMoyensHumains->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntreprisePrestationsRealisees') && ('agent' == $this->getCalledFrom())) {
            $this->panelPrestation->setDisplay('Dynamic');
        } else {
            $this->panelPrestation->setDisplay('None');
        }
    }

    public function downloadPiece($sender, $param)
    {
        if ($_GET['idEntreprise']) {
            $entreprise = $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']));
            $nomFichier = $entreprise->getIntituleDocumentsCommerciaux();
            $idFichier = $entreprise->getdocumentsCommerciaux();

            $atexoBlob = new Atexo_Blob();
            $tailleFichier = $atexoBlob->getTailFile($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private', false);
            if ('.zip' == strrchr($nomFichier, '.')) {
                header('Content-Type: application/zip');
            } else {
                header('Content-Type: application/octet-stream');
            }
            $nomFichier = str_replace('\\', '', $nomFichier);
            header('Content-Disposition: attachment; filename="'.$nomFichier.'";');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.$tailleFichier);

            $atexoBlob->send_blob_to_client($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
            exit;
        }
    }

    public function getTypeCollaboration()
    {
        $typeCollaboration = null;
        if ($_GET['idEntreprise']) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']));
            $typeCollaborationEse = $entreprise->getTypeCollaboration();
            $res = (new Atexo_Entreprise())->isTypeCollaborationExiste($typeCollaborationEse, $typeCollaboration);
            if ($res) {
                return '';
            }

            return 'none';
        }

        return 'none';
    }

    public function entrepriseAutoriseAcces()
    {
        if (Atexo_CurrentUser::isConnected()) {
            $autorisation = (new Atexo_Entreprise())->autoriseAccesEntreprises(Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']));
            if ($autorisation) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function hasAccesFornisseurDB()
    {
        if (Atexo_CurrentUser::isConnected()) {
            if (Atexo_CurrentUser::hasHabilitation('SuiviEntreprise')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getArrayTypeColloboration($typeCollaborationEse)
    {
        $dataTypeCollaboration = $this->getViewState('arrayTypeCollaboration');
        $dataSource = [];
        if (!is_array($dataTypeCollaboration)) {
            $dataTypeCollaboration = $this->remplirValTypeCollaboration();
        }
        if ($typeCollaborationEse) {
            $arrayIds = explode('#', $typeCollaborationEse);
            foreach ($arrayIds as $id) {
                if ($id) {
                    $dataSource[$id] = $dataTypeCollaboration[$id];
                }
            }
        }

        return $dataSource;
    }

    public function remplirValTypeCollaboration()
    {
        $dataSource = [];
        $arrayType = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_COLLABORATION'));
        foreach ($arrayType as $key => $value) {
            $dataSource[$key]['id'] = $key;
            $dataSource[$key]['libelle'] = $value;
        }
        $this->setViewState('arrayTypeCollaboration', $dataSource);

        return $dataSource;
    }

    /**
     * Permet de charger les informations d'etablissements.
     *
     * @param int $idEntreprise l'id de l'entreprise
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerInfoEtablissements($entreprise)
    {
        if ($entreprise instanceof Entreprise) {
            $etablissements = $entreprise->getCommonTEtablissements();
            $etablissementsVo = (new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($etablissements);
            $this->listeEtablissments->setMode('2');
            $this->listeEtablissments->setOptionAdd(false);
            $this->listeEtablissments->setIsEntrepriseLocale($entreprise->isEntrepriseLocale());
            $this->listeEtablissments->setSiretSiegeSocial($entreprise->getSiretSiegeSocial());
            $this->listeEtablissments->setEtablissementsList($etablissementsVo);
            $this->listeEtablissments->initComposant();
        }
    }

    public function entrepriseInscrite($idEntreprise, $refCons)
    {
        $groupement = Atexo_BourseCotraitance::getCasGroupement($refCons, $idEntreprise);
        if ('cas2' == $groupement['cas'] || 'cas4' == $groupement['cas']) {
            return true;
        }

        return false;
    }

    /**
     * Permet de refresh les attestations.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna Ezziani <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function refreshAttestations($sender, $param)
    {
        $this->dispoAttestationsReponse->setIdEntreprise(Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']));
        $this->dispoAttestationsReponse->getDataSource();
    }

    /**
     * Permet d'afficher ou pas le bloc des attestations.
     *
     * @return true or false
     *
     * @author Khalid BENAMAR <khalid.benamar@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function gestionAffichageBlocAttestations()
    {
        $idEntreprise = Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']);
        $ats = sha1($idEntreprise.'1');
        $isEntrepriseLocale = false;
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, null, false);
        if ($entreprise instanceof Entreprise) {
            $isEntrepriseLocale = empty($entreprise->getSiren()) ? false : true;
        }

        return Atexo_Module::isEnabled('DonneesCandidat')
        && $isEntrepriseLocale
        && (
            !Atexo_Config::getParameter('AFFICHAGE_BLOC_ATTESTATIONS_FICHE_ENTREPRISE_JUSTE_OUVERTURE_ANALYSE')
            || (Atexo_Config::getParameter('AFFICHAGE_BLOC_ATTESTATIONS_FICHE_ENTREPRISE_JUSTE_OUVERTURE_ANALYSE') && isset($_GET['ats']) && $_GET['ats'] == $ats)
        );
    }
}
