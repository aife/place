<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Page de création compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupQualification extends MpeTPage
{
    private string $_listeQualifications = '';
    private bool $_alwaysShowLinkSelection = false;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (isset($_GET['clientId']) && '' != strstr($_GET['clientId'], 'panelSearch')) {
            $this->_alwaysShowLinkSelection = true;
        } else {
            $this->_alwaysShowLinkSelection = false;
        }
        $this->displayLinkAddSelection();
        $this->_listeQualifications = (new Atexo_Qualifications())->getCachedQualifications(true);
        if (!$this->IsPostBack) {
            $this->displaySelectedQualification();
            $this->displayAllQualification();
        }
    }

    /**
     * pour afficher la liste des qualifications.
     */
    public function displayAllQualification()
    {
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if ($this->_listeQualifications) {
            $libelleTraduit = 'libelle_'.$langue;
            foreach ($this->_listeQualifications as $oneQualif) {
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$oneQualif[$libelleTraduit]) {
                    $data[$oneQualif['id']] = $oneQualif['libelle'];
                } else {
                    $data[$oneQualif['id']] = $oneQualif[$libelleTraduit];
                }
            }
        }
        $this->idType->DataSource = $data;
        $this->idType->DataBind();
    }

    /**
     * pour afficher la liste des secteurs lié au type séléctionné dans le menu déroulant.
     */
    public function displaySecteur($sender, $param)
    {
        $this->script->Text = "<script>document.getElementById('divSecteur').style.display = '';
        						document.getElementById('divAddSelection').style.display = '';</script>";
        $idType = $this->idType->SelectedValue;
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        if ('0' != $idType) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            if ($this->_listeQualifications) {
                $libelleTraduit = 'libelle_'.$langue;
                foreach ($this->_listeQualifications[$idType]['SousNiveau'] as $oneSecteur) {
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$oneSecteur[$libelleTraduit]) {
                        $data[$oneSecteur['id']] = $oneSecteur['libelle'];
                    } else {
                        $data[$oneSecteur['id']] = $oneSecteur[$libelleTraduit];
                    }
                }
            }
        } else {
            $this->_alwaysShowLinkSelection = false;
        }
        $this->displayLinkAddSelection();
        $this->idSecteur->DataSource = $data;
        $this->idSecteur->DataBind();
        $this->panelQualification->render($param->NewWriter);
    }

    /**
     * pour afficher la liste des qualifications lié au secteur séléctionné dans le menu déroulant.
     */
    public function displayQualification($sender, $param)
    {
        $this->script->Text = "<script>document.getElementById('divSecteur').style.display = '';
        						document.getElementById('divQualification').style.display = '';
                               document.getElementById('divAddSelection').style.display = '';</script>";
        $idType = $this->idType->SelectedValue;
        $idSecteur = $this->idSecteur->SelectedValue;
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $listeQualif = $this->_listeQualifications[$idType]['SousNiveau'][$idSecteur];
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if ($listeQualif) {
            $libelleTraduit = 'libelle_'.$langue;
            foreach ($listeQualif['SousNiveau'] as $oneQualif) {
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$oneQualif[$libelleTraduit]) {
                    $data[$oneQualif['id']] = $oneQualif['libelle'];
                } else {
                    $data[$oneQualif['id']] = $oneQualif[$libelleTraduit];
                }
            }
        }
        $this->idQualification->DataSource = $data;
        $this->idQualification->DataBind();
        $this->panelQualification->render($param->NewWriter);
    }

    /**
     * pour afficher la liste des qualifications lié au secteur séléctionné dans le menu déroulant.
     */
    public function displayClasse($sender, $param)
    {
        $this->script->Text = "<script>document.getElementById('divSecteur').style.display = '';
                    			document.getElementById('divQualification').style.display = '';
       					 		document.getElementById('divClasse').style.display = '';
       					 		document.getElementById('divAddSelection').style.display = '';</script>";
        $idType = $this->idType->SelectedValue;
        $idSecteur = $this->idSecteur->SelectedValue;
        $idQualification = $this->idQualification->SelectedValue;
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $listeClasse = $this->_listeQualifications[$idType]['SousNiveau'][$idSecteur]['SousNiveau'][$idQualification];
        if ($listeClasse && is_array($listeClasse['SousNiveau'])) {
            foreach ($listeClasse['SousNiveau'] as $oneClasse) {
                $data[$oneClasse['id']] = $oneClasse['libelle'];
            }
        }
        $this->idClasse->DataSource = $data;
        $this->idClasse->DataBind();
        $this->panelQualification->render($param->NewWriter);
    }

    public function displayLinkAdd()
    {
        $this->onLoadScript->Text = "<script>document.getElementById('divSecteur').style.display = '';
                    			document.getElementById('divQualification').style.display = '';
       					 		document.getElementById('divClasse').style.display = '';
                    			document.getElementById('divAddSelection').style.display = '';</script>";
    }

    public function callBackDisplayLinkAdd($sender, $param)
    {
        if ('0' != $this->idClasse->selectedValue) {
            $this->panelLinkSelection->SetDisplay('Dynamic');
        } else {
            $this->panelLinkSelection->SetDisplay('None');
        }
        $this->panelQualification->render($param->NewWriter);
    }

    /**
     * pour afficher la liste des qualifications Séléctionnée.
     */
    public function displaySelection($sender, $param)
    {
        $this->displayListeSelection();
        $this->panelLinkSelection->SetDisplay('Dynamic');
        $this->panelLinkSelection->render($param->NewWriter);
        $this->panelMaSelection->render($param->NewWriter);
    }

    /**
     * supprimer sur le tableau la classe sélectionnée.
     */
    public function deleteClasse($sender, $param)
    {
        //if(true) {
        $dataSourceSelection = $this->getViewState('dataSourceSelection');
        unset($dataSourceSelection[$param->CommandName]);
        $this->setViewState('dataSourceSelection', $dataSourceSelection);
        $this->repeaterSelection->DataSource = $dataSourceSelection;
        $this->repeaterSelection->dataBind();
        // }
    }

    /**
     * Rafrechir le tableau des classes sélectionnées.
     */
    public function onCallBackRefreshRepeater($sender, $param)
    {
        $this->panelMaSelection->render($param->NewWriter);
        $this->panelLinkSelection->render($param->NewWriter);
    }

    public function onValidateClick($sender, $param)
    {
        $listeLibelles = '';
        $listeIds = '';
        foreach ($this->repeaterSelection->getItems() as $oneItem) {
            $listeIds .= $oneItem->idInterne->getText().'#';
            if ('-' != $oneItem->type->getText()) {
                $listeLibelles .= $oneItem->type->getText();
            }
            if ('-' != $oneItem->libellesSecteur->getText()) {
                $listeLibelles .= ' / '.$oneItem->libellesSecteur->getText();
            }
            if ('-' != $oneItem->libellesQualif->getText()) {
                $listeLibelles .= ' / '.$oneItem->libellesQualif->getText();
            }
            if ('-' != $oneItem->classe->getText()) {
                $listeLibelles .= ' / Classe '.$oneItem->classe->getText();
            }
            $listeLibelles .= '<br/> ';
        }
        $this->script->Text = "<script>returnSelectedQualif('".base64_encode($listeIds)."','".base64_encode($listeLibelles)."','".Atexo_Util::atexoHtmlEntities($_GET['clientId'])."');</script>";
    }

    public function displayListeSelection()
    {
        $inSelectedQualif = false;
        $this->script->Text = "<script>document.getElementById('divSecteur').style.display = '';
                        			document.getElementById('divQualification').style.display = '';
           					 		document.getElementById('divClasse').style.display = '';
           					 		document.getElementById('divAddSelection').style.display = '';
                        			document.getElementById('divMaSelection').style.display = '';</script>";

        // Récuperer l'identifiant  interne de la classe de qualification
        $idType = $this->idType->SelectedValue;
        $idSecteur = $this->idSecteur->SelectedValue;
        $idQualification = $this->idQualification->SelectedValue;
        $idClasse = $this->idClasse->SelectedValue;
        $idInterne = '';
        if ($idType) {
            $idInterne = $idType;
        }
        if ($idSecteur) {
            $idInterne = $idSecteur;
        }
        if ($idQualification) {
            $idInterne = $idQualification;
        }
        if ($idClasse) {
            $idInterne = $idClasse;
        }
        $data = $this->getViewState('dataSourceSelection');
        if ($data) {
            $newDataSource = $data;
        } else {
            $newDataSource = [];
        }
        // Ajout de la nouvelle classe sélectionnée dans le dataSource

        if (is_array($data)) {
            $i = 0;
            $inSelectedQualif = false;
            if (isset($data[$idInterne])) {
                $inSelectedQualif = true;
            }
        }
        if (!$inSelectedQualif && '0' != $idInterne) {
            $newDataSource[$idInterne]['idInterne'] = $idInterne;
            $newDataSource[$idInterne]['id'] = $idInterne;
            if ('0' != $idType && '' != $idType) {
                $type = $this->idType->getSelectedItem()->getText();
            } else {
                $type = '-';
            }
            if ('0' != $idSecteur && '' != $idSecteur) {
                $secteur = $this->idSecteur->getSelectedItem()->getText();
            } else {
                $secteur = '-';
            }
            if ('0' != $idQualification && '' != $idQualification) {
                $qualification = $this->idQualification->getSelectedItem()->getText();
            } else {
                $qualification = '-';
            }
            if ('0' != $idClasse && '' != $idClasse) {
                $classe = $this->idClasse->getSelectedItem()->getText();
            } else {
                $classe = '-';
            }
            if ('-' != $type || '-' != $secteur || '-' != $qualification || '-' != $classe) {
                $newDataSource[$idInterne]['type'] = $type;
                $newDataSource[$idInterne]['secteur'] = $secteur;
                $newDataSource[$idInterne]['qualification'] = $qualification;
                $newDataSource[$idInterne]['classe'] = $classe;
            }
        }
        $this->setViewState('dataSourceSelection', $newDataSource);

        $this->repeaterSelection->DataSource = $newDataSource;
        $this->repeaterSelection->dataBind();
    }

    /**
     * Afficher les qualifications déjà choisi.
     */
    public function displaySelectedQualification()
    {
        if (isset($_GET['ids'])) {
            $arrayIds = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['ids'])));
            if (!is_array($arrayIds)) {
                return;
            }
            $listeQualif = (new Atexo_Qualifications())->reteiveQualificationsByArrayIds($arrayIds);
            $newDataSource = [];
            $dataSource = $this->_listeQualifications;
            $i = 0;
            foreach ($listeQualif as $oneQualif) {
                $idInterne = $oneQualif->getIdInterne();
                $coefIdSousCat = explode('.', $idInterne);
                $count = count($coefIdSousCat);
                $type = '-';
                $secteur = '-';
                $qualification = '-';
                $classe = '-';
                $newDataSource[$i]['idInterne'] = $idInterne;
                $newDataSource[$i]['id'] = $i;
                //type

                if (1 == $count) {
                    $idParent = $coefIdSousCat[0];
                    $type = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($idParent, $dataSource);
                }
                //secteur
                elseif (2 == $count) {
                    $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
                    $type = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                    $secteur = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($idParent, $dataSource);
                }
                //qualification
                elseif (3 == $count) {
                    $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2];
                    $type = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                    $secteur = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1], $dataSource);
                    $qualification = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($idParent, $dataSource);
                }
                //classe
                elseif (4 == $count) {
                    $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2].'.'.$coefIdSousCat[3];
                    $type = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                    $secteur = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1], $dataSource);
                    $qualification = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2], $dataSource);
                    $classe = (new Atexo_Qualifications())->getLibelleQualificationByIdInterne($idParent, $dataSource);
                }
                $newDataSource[$i]['type'] = $type;
                $newDataSource[$i]['secteur'] = $secteur;
                $newDataSource[$i]['qualification'] = $qualification;
                $newDataSource[$i]['classe'] = $classe;
                ++$i;
            }
            $this->setViewState('dataSourceSelection', $newDataSource);

            $this->repeaterSelection->DataSource = $newDataSource;
            $this->repeaterSelection->dataBind();
        }
    }

    /**
     * pour afficher le lien ajouter a ma selection selon le param $bool.
     */
    public function displayLinkAddSelection()
    {
        if ($this->_alwaysShowLinkSelection) {
            $this->panelLinkSelection->SetDisplay('Dynamic');
        } else {
            $this->panelLinkSelection->SetDisplay('None');
        }
    }
}
