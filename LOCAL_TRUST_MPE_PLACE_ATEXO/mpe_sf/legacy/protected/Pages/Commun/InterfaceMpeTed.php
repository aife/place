<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 30 août 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class InterfaceMpeTed extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $result = null;
        switch ($_POST['service']) {
            case 'parseNotice':
                $result = $this->parseNotice();
                break;
            case 'submitNotice':
                $result = $this->submitNotice();
                break;
            case 'getNoticeStatus':
                $result = $this->getNoticeStatus();
                break;
            case 'getNoticeReport':
                $result = $this->getNoticeReport();
                break;
            case 'generatePDFRegulation'://jamais utiliser paramettre "notice" => $notice, "submissionId" => $submissionId
                break;
            case 'generatePDFTED'://"notice" => $notice, "submissionId" => $submissionId, "coverPage" => "no", "language"=>$lang
                $result = $this->generatePDFTED();
                break;
            case 'getLastResponseNoticeReport'://submissionId
                $result = $this->getLastResponseNoticeReport();
                break;
            default:
                $result = ['erreur' => 'true', 'code_erreur' => 'null', 'libelle_erreur' => 'Pas de service '.$_POST['service'], 'resultat' => 'null'];
                break;
        }
        $result = serialize($result);
        print_r($result);
        exit;
    }

    public function validateXmlForThisClass(&$messageErreur)
    {
        $isValid = false;
        if (isset($_POST['notice']) && $_POST['notice']) {
            $xml = $_POST['notice'];
            $valideXml = simplexml_load_string($xml);
            if ($valideXml instanceof \SimpleXMLElement) {
                $form = $valideXml['FORM'];
                $form = 'Form_'.((intval($form) < 10) ? '0'.$form : $form);
                $xsd = Atexo_Config::getParameter('PATH_XSD_TED').$form.'.xsd';
                if (is_file($xsd)) {
                    $logger = Atexo_LoggerManager::getLogger('app');
                    $isValid = Atexo_Util::validateXmlBySchema($xml, $xsd, $logger, $messageErreur);
                } else {
                    $messageErreur = "xsd du formulaire choisi n'existe pas : ".$xsd;
                }
            }
        }

        return $isValid;
    }

    public function parseNotice()
    {
        $result = [];
        $messageErreur = true;
        $validateXml = $this->validateXmlForThisClass($messageErreur);
        if (boolval($validateXml)) {
            $result['erreur'] = 'false';
            $result['resultat'] = 'VERIFICATION XML OK';
        } else {
            $result['erreur'] = 'true';
            $result['libelle_erreur'] = $messageErreur;
        }

        return $result;
    }

    public function submitNotice()
    {
        $result = [];
        $messageErreur = '';
        $validateXml = $this->validateXmlForThisClass($messageErreur);
        if (boolval($validateXml)) {
            $result['erreur'] = 'false';
            $result['resultat'] = 'TED72-'.date('Ymd-His').'-BCH';
        } else {
            $result['erreur'] = 'true';
            $result['libelle_erreur'] = $messageErreur;
        }

        return $result;
    }

    public function getNoticeStatus()
    {
        $result = [];
        if (isset($_POST['submissionId']) && strstr($_POST['submissionId'], 'TED72-')) {
            $result['erreur'] = 'false';
            $result['resultat'] = 'PUBLISHED';
        } else {
            $result['erreur'] = 'true';
            $result['libelle_erreur'] = ' SubmissionId incorrect '.$_POST['submissionId'];
        }

        return $result;
    }

    public function getNoticeReport()
    {
        $result = [];
        if (isset($_POST['submissionId']) && strstr($_POST['submissionId'], 'TED72-')) {
            $result['erreur'] = 'false';
            $result['resultat'] = $_POST['submissionId'];
        } else {
            $result['erreur'] = 'true';
            $result['libelle_erreur'] = ' SubmissionId est incorrect '.$_POST['submissionId'];
        }

        return $result;
    }

    public function generatePDFTED()
    {
        $result = [];
        if (isset($_POST['submissionId']) && strstr($_POST['submissionId'], 'TED72-')) {
            $result['erreur'] = 'false';
            $result['resultat'] = file_get_contents(Atexo_Config::getParameter('PATH_PDF_TED'));
        } else {
            $result['erreur'] = 'true';
            $result['libelle_erreur'] = ' SubmissionId est incorrect '.$_POST['submissionId'];
        }

        return $result;
    }

    public function getLastResponseNoticeReport()
    {
        $lien = Atexo_Config::getParameter('URL_PUB_TED');
        $idJoue = date('Y').'/S '.date('dm-His');
        $dateJoue = date('Y-m-d');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
<getNoticeReportResponse>
    <noticeReport>
        <publicationInfo>
            <tedLinks>
                <tedLink language="FR">'.$lien.'</tedLink>
            </tedLinks>
            <noDocOjs>'.$idJoue.'</noDocOjs>
            <publicationDate>'.$dateJoue.'</publicationDate>
        </publicationInfo>
    </noticeReport>
</getNoticeReportResponse>';

        return $xml;
    }
}
