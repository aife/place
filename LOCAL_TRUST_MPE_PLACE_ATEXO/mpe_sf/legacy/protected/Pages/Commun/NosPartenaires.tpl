<com:TContent ID="CONTENU_PAGE">
		<!--Debut colonne droite-->
		<div id="main-part" class="main-part" style="height: auto;">
			<div class="breadcrumbs">&gt; <com:TTranslate>TEXT_NOS_PARTENAIRES</com:TTranslate></div>
			<!--Debut Bloc Partenaires-->
			<div class="form-field bloc-partenaires">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<h3>Partenaires</h3>
					<div class="spacer-small"></div>
					<!--Debut partenaire-->
					<div class="line">
						<div class="content-bloc bloc-150 col-logo">
							<img alt="" height="73px" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/UE.png">
						</div>
						<div class="content-bloc bloc-500 col-liens">
							<i>Cofinancé par le mécanisme pour l'interconnexion en Europe</i>
						</div>
					</div>
					<div class="spacer"></div>
					<com:TRepeater ID="repeaterPartenaires">
						<prop:ItemTemplate>						
							<div class="line">
								<div class="content-bloc bloc-150 col-logo"><a target="_blank" href="<%#$this->Data->getLienExterne()%>" title="<%#$this->Data->getTitle()%>"><img alt="" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->Data->getLienImg()%>" /></a></div>
								<div class="content-bloc bloc-500 col-liens">
									<a class="lien-ext" target="_blank" href="<%#$this->Data->getLienExterne()%>" title="<%#$this->Data->getTitle()%>"><%#$this->Data->getInitials()%></a> - <i><%#$this->Data->getDescPartenaire() %></i>
								</div>	
							</div>
			                <div class="spacer"></div>
						</prop:ItemTemplate>
					</com:TRepeater>	                
					<!--Fin partenaire-->
	                <div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</div>
			<!--Fin Bloc Partenaires-->
			<div class="breaker"></div>
		</div>
		<!--Fin colonne droite-->
		<div class="breaker"></div>
</com:TContent> 
