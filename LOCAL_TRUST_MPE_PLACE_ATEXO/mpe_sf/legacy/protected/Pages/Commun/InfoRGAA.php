<?php

namespace Application\Pages\Commun;

use App\Service\Composant\Bloc;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Util;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Languages;

class InfoRGAA extends MpeTPage
{

    public const TWIG = 'composant/info_rgaa.html.twig';
    public function onInit($param)
    {

        if (isset($_GET['AG']) || (isset($_GET['calledFrom']) && 'agent' == $_GET['calledFrom'] )) {
            $this->Master->setCalledFrom('agent');
        } else {
            $this->Master->setCalledFrom('entreprise');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }

    public function getInfoRGAA()
    {
        $footerService = Atexo_Util::getSfService(Bloc::class);

        return $footerService->getTemplate(self::TWIG);
    }
}
