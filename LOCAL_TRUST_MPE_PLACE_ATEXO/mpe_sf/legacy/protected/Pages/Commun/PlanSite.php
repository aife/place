<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class PlanSite extends MpeTPage
{
    public $langue;
    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $this->langue = Atexo_CurrentUser::readFromSession("lang");
    }

    public function onLoad($param)
    {
        if (!Atexo_CurrentUser::isAgent() && !Atexo_CurrentUser::isConnected()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT')."?page=Agent.AccueilAgentAuthentifie");
        }
    }
}