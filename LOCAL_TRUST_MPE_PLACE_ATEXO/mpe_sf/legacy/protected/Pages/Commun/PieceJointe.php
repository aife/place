<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/**
 * Popup des pièces jointes*/
class PieceJointe extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!(Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && (new Atexo_Entreprise_Inscrit())->isAdmin(Atexo_CurrentUser::getIdInscrit()))) {
            $this->panelMessageErreurModule->setVisible(true);
            $this->panelMessageErreurModule->setMessage("Erreur. Vous n'êtes pas habilité à accéder à cette page");
            $this->noError->setVisible(false);
            $this->error->setVisible(true);
        }
        if (!$this->IsPostBack) {
            if (isset($_GET['ids']) && $_GET['ids']) {
                $this->IdUploadPj->displayPiecesJointes(Atexo_Util::atexoHtmlEntities($_GET['ids']));
            }
        }
    }
}
