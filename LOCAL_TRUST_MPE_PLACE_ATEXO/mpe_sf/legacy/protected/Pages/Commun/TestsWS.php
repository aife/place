<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Exception;
use Laminas\Http\Client\Adapter\Proxy;
use Zend_Http_Client;

class TestsWS extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $ticket = $this->getTicket();
        echo 'Test Ws reception bloc chiffré ...';
        $res = $this->testWsChiffrementBloc($ticket);
        echo $res ? ' OK' : ' KO';

        echo '<br/><br/>Test Ws reception statut Offre ...';
        $res = $this->testWsChiffrementOffre($ticket);
        echo $res ? 'OK' : 'KO';

        echo '<br/><br/>Test Ws reception fichier dechiffre ...';
        $res = $this->testWsDechiffrementFichier($ticket);
        echo $res ? 'OK' : 'KO';

        echo '<br/><br/>Test Ws reception statut Enveloppe ...';
        $res = $this->testWsDechiffrementEnveloppe($ticket);
        echo $res ? 'OK' : 'KO';

        exit;
    }

    public function testWsChiffrementBloc($ticket)
    {
        try {
            $return = false;
            $client = $this->getZendClient();
            $uri = Atexo_Config::getParameter('PF_URL_REFERENCE').'api.php/ws/registre/pli/chiffre/938?ticket='.$ticket;
            $client->setUri($uri);
            $client->setParameterPost('numeroOrdreBloc', '55');
            $client->setParameterPost('organisme', 'a1a');
            $client->setFileUpload('/tmp/Bloc_test', 'bloc', 'APRGOEPZSOKERGFPELGOERJHEPRLGFMEORGPEAKRGKQELMJGOQERKGPKEOPRJKTGO');
            $res = $client->request('POST')->getBody();
            $domDoc = simplexml_load_string($res);
            if (isset($domDoc->reponse->blocPli->idBloc) && !empty($domDoc->reponse->blocPli->idBloc)) {
                $return = true;
            } elseif (isset($domDoc->reponse->messageErreur)) {
                echo $domDoc->reponse->messageErreur;
            }
        } catch (\Exception $e) {
            echo 'Erreur : '.$e->getMessage();
        }

        return $return;
    }

    public function testWsChiffrementOffre($ticket)
    {
        try {
            $return = false;
            $client = $this->getZendClient();
            $xml = '<?xml version="1.0" encoding="UTF-8"?> <mpe xmlns="http://www.atexo.com/epm/xml"> <envoi>    <offre> 	<idOffre>328</idOffre > 	<organisme>a1a</organisme>      	<reponseAnnonceXML>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSIgc3RhbmRhbG9uZT0ieWVzIj8+DQo8UmVwb25zZUFubm9uY2UgaWRSZXBvbnNlQW5ub25jZT0iMzI3IiBpbmRleFJlcG9uc2U9IjE0IiByZWZlcmVuY2U9IjUwIiBvcmdhbmlzbWU9ImExYSIgaG9yb2RhdGFnZURlcG90PSIyMDE2LTA5LTMwIDE2OjUyOjQ0IiB0eXBlU2lnbmF0dXJlPSJ4bWwiPjxPcGVyYXRldXJFY29ub21pcXVlPjxOb20+Q0xBVURJTkUgTUVOQUdFPC9Ob20+PC9PcGVyYXRldXJFY29ub21pcXVlPjxBbm5vbmNlIGNoaWZmcmVtZW50PSJ0cnVlIiB0eXBlUHJvY2VkdXJlPSIiIGFsbG90aXNzZW1lbnQ9InRydWUiIG9yZ2FuaXNtZT0iYTFhIiBub21icmVMb3RzPSIxIiBlbnRpdGVBY2hhdD0iT1BSIC0gT3JnYW5pc21lIGRlIHBhcmFt6XRyYWdlIGRlIHLpZulyZW5jZSAiIHNlcGFyYXRpb25TaWduYXR1cmVBZT0iZmFsc2UiIHNpZ25hdHVyZT0idHJ1ZSIgcmVmZXJlbmNlPSI1MCIgcmVmZXJlbmNlVXRpbGlzYXRldXI9IkxFWl8xMTIiIGRhdGVMaW1pdGU9IjIwMTctMDMtMjcgMTc6MzA6MDAiPjxJbnRpdHVsZS8+PE9iamV0PnRyYXZhdXg8L09iamV0PjwvQW5ub25jZT48RW52ZWxvcHBlcz48RW52ZWxvcHBlIHR5cGU9IjEiIG51bUxvdD0iMCIgc3RhdHV0T3V2ZXJ0dXJlPSJPVVYiIHN0YXR1dEFkbWlzc2liaWxpdGU9IkFUUiIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgaWRFbnZlbG9wcGU9Ijc0NCI+PEZpY2hpZXIgdHlwZUZpY2hpZXI9IlBSSSIgb3JpZ2luZUZpY2hpZXI9IjMiIG51bU9yZHJlPSIxIiB0YWlsbGVFbkNsYWlyPSI3MTQ5ODEyNSIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbmJyQmxvY3NDaGlmZnJlbWVudD0iMTgiIGhhc2g9ImZhbHNlIiBpZEZpY2hpZXI9Ijk0MCI+PE5vbT5yZWNldHRlLnppcDwvTm9tPjxFbXByZWludGU+MEU1MTE1OEM1MTc3NEM1NTEwRDA3MDQxN0U2NzY0MEQ3NEQ2QkNCOTwvRW1wcmVpbnRlPjxCbG9jc0NoaWZmcmVtZW50IG5ickJsb2NzPSIxOCI+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMSIgZW1wcmVpbnRlPSIzRjg5QUYzNUFCNThCN0QxMzlEMTc1RUREQkM0RTU1MzZBMTQxNTZBIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA0Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMiIgZW1wcmVpbnRlPSIyRUJDRUY3Q0E1MkFBNEEzMjVDMjNBOEQ2NUQ0MkJDRkU0RTFGRDkxIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA1Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMyIgZW1wcmVpbnRlPSI0QzQ2RUU1QkUzMUVDRDBDNTFEOUE5NDE0Qzk2QjFENDAyNTdDQUUwIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA2Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iNCIgZW1wcmVpbnRlPSJFNTk1MTIzMEU0QUQzQzRCRjcwM0U3RUNENUE3Q0FGRjc0QjVFMTMwIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA3Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iNSIgZW1wcmVpbnRlPSI0QTQzQ0RGMEFEODAzMDg2QkM1OTc5RjY0NDg5RDgyREVBQkU3Rjg4IiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA4Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iNiIgZW1wcmVpbnRlPSI3NUZDQkVBM0MzMEQ5NDE5NUQ4NjE1QzZBMUU0ODcxRjgxODczMDg0IiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTA5Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iNyIgZW1wcmVpbnRlPSJDODkzMTkwQjdCQTEwMTMzMzkzMTEzMTlEODg3MUVDNjcyNUMyNTMzIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTEwIi8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iOCIgZW1wcmVpbnRlPSJGMDg4QUM3RDM4ODg1QzVCQkU2NDVEQ0RGMzFDNTc3NUM4N0UyQTUyIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTExIi8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iOSIgZW1wcmVpbnRlPSJGQjQ0QTVFMjdDNEU4RDZCNjIyQkFBMDVFNjJBMEZDNUEzMjUxRjJEIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTEyIi8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMTAiIGVtcHJlaW50ZT0iNzMwRjgyMkYzOUMzMEFCMUIzNTc5MjgxNDA4ODI1NjEwMkExMTVBQyIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUxMyIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjExIiBlbXByZWludGU9IkFDRkY2RjlCQzNFNDRGREMzNUEyRjVFODMwRkYxQzZFMDI4N0NDRTUiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MTQiLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjQxOTQzMDQiIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9Ijc0NjAyMzYiIG51bU9yZHJlPSIxMiIgZW1wcmVpbnRlPSJCM0U0QUZDREZDOERENTVGMUNGN0Q3MzIwQ0QwNkNEMkJCRUNEQjc0IiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTE1Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMTMiIGVtcHJlaW50ZT0iMkZEMUNDMjAxOUEwRkQzRDdFODFCMkQyQUM2ODdFN0FFM0Q0Mzk3NiIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUxNiIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjE0IiBlbXByZWludGU9IkJDRjM1MjM1NUE3OUEyRjg2QzE0QUVBMDMyMzcyREM3M0FGMTFGM0QiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MTciLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjQxOTQzMDQiIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9Ijc0NjAyMzYiIG51bU9yZHJlPSIxNSIgZW1wcmVpbnRlPSJBMTE0RkVCNURBOTUxRDBDOUMyODdFMjhGQ0Q2Q0YwRjM5RkZERTg0IiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTE4Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMTYiIGVtcHJlaW50ZT0iRDQxQUMwQTgwRjg0QTdBNTBBRkI5NEFGRjNGMERDQjEyNzAzMTM5MyIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUxOSIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjE3IiBlbXByZWludGU9IjlBNUQzNjFFMDRDMEIzRjE2NjQxRDlEMDNDQUE1OUJCQjAwOTYwNDEiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MjAiLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjE5NDk1NyIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iMzUwMjY0IiBudW1PcmRyZT0iMTgiIGVtcHJlaW50ZT0iRTlGN0I2ODVDQzhFRDBDMDQyRERGOUY4NEZBNTU4QzMzNDRFRThBNSIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyMSIvPjwvQmxvY3NDaGlmZnJlbWVudD48L0ZpY2hpZXI+PC9FbnZlbG9wcGU+PEVudmVsb3BwZSB0eXBlPSIyIiBudW1Mb3Q9IjAiIHN0YXR1dE91dmVydHVyZT0iT1VWIiBzdGF0dXRBZG1pc3NpYmlsaXRlPSJBVFIiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIGlkRW52ZWxvcHBlPSI3NDUiPjxGaWNoaWVyIHR5cGVGaWNoaWVyPSJQUkkiIG9yaWdpbmVGaWNoaWVyPSIzIiBudW1PcmRyZT0iMiIgdGFpbGxlRW5DbGFpcj0iNzE0OTgxMjUiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5ickJsb2NzQ2hpZmZyZW1lbnQ9IjE4IiBoYXNoPSJmYWxzZSIgaWRGaWNoaWVyPSI5NDEiPjxOb20+cmVjZXR0ZS56aXA8L05vbT48RW1wcmVpbnRlPjBFNTExNThDNTE3NzRDNTUxMEQwNzA0MTdFNjc2NDBENzRENkJDQjk8L0VtcHJlaW50ZT48QmxvY3NDaGlmZnJlbWVudCBuYnJCbG9jcz0iMTgiPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjEiIGVtcHJlaW50ZT0iM0Y4OUFGMzVBQjU4QjdEMTM5RDE3NUVEREJDNEU1NTM2QTE0MTU2QSIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyMiIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjIiIGVtcHJlaW50ZT0iMkVCQ0VGN0NBNTJBQTRBMzI1QzIzQThENjVENDJCQ0ZFNEUxRkQ5MSIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyMyIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjMiIGVtcHJlaW50ZT0iNEM0NkVFNUJFMzFFQ0QwQzUxRDlBOTQxNEM5NkIxRDQwMjU3Q0FFMCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyNCIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjQiIGVtcHJlaW50ZT0iRTU5NTEyMzBFNEFEM0M0QkY3MDNFN0VDRDVBN0NBRkY3NEI1RTEzMCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyNSIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjUiIGVtcHJlaW50ZT0iNEE0M0NERjBBRDgwMzA4NkJDNTk3OUY2NDQ4OUQ4MkRFQUJFN0Y4OCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyNiIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjYiIGVtcHJlaW50ZT0iNzVGQ0JFQTNDMzBEOTQxOTVEODYxNUM2QTFFNDg3MUY4MTg3MzA4NCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyNyIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjciIGVtcHJlaW50ZT0iQzg5MzE5MEI3QkExMDEzMzM5MzExMzE5RDg4NzFFQzY3MjVDMjUzMyIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyOCIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjgiIGVtcHJlaW50ZT0iRjA4OEFDN0QzODg4NUM1QkJFNjQ1RENERjMxQzU3NzVDODdFMkE1MiIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUyOSIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjkiIGVtcHJlaW50ZT0iRkI0NEE1RTI3QzRFOEQ2QjYyMkJBQTA1RTYyQTBGQzVBMzI1MUYyRCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUzMCIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjEwIiBlbXByZWludGU9IjczMEY4MjJGMzlDMzBBQjFCMzU3OTI4MTQwODgyNTYxMDJBMTE1QUMiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MzEiLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjQxOTQzMDQiIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9Ijc0NjAyMzYiIG51bU9yZHJlPSIxMSIgZW1wcmVpbnRlPSJBQ0ZGNkY5QkMzRTQ0RkRDMzVBMkY1RTgzMEZGMUM2RTAyODdDQ0U1IiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTMyIi8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMTIiIGVtcHJlaW50ZT0iQjNFNEFGQ0RGQzhERDU1RjFDRjdENzMyMENEMDZDRDJCQkVDREI3NCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUzMyIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjEzIiBlbXByZWludGU9IjJGRDFDQzIwMTlBMEZEM0Q3RTgxQjJEMkFDNjg3RTdBRTNENDM5NzYiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MzQiLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjQxOTQzMDQiIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9Ijc0NjAyMzYiIG51bU9yZHJlPSIxNCIgZW1wcmVpbnRlPSJCQ0YzNTIzNTVBNzlBMkY4NkMxNEFFQTAzMjM3MkRDNzNBRjExRjNEIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTM1Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSI0MTk0MzA0IiB0YWlsbGVBcHJlc0NoaWZmcmVtZW50PSI3NDYwMjM2IiBudW1PcmRyZT0iMTUiIGVtcHJlaW50ZT0iQTExNEZFQjVEQTk1MUQwQzlDMjg3RTI4RkNENkNGMEYzOUZGREU4NCIgc3RhdHV0Q2hpZmZyZW1lbnQ9IkNISSIgbm9tQmxvYz0iMjUzNiIvPjxCbG9jQ2hpZmZyZW1lbnQgdGFpbGxlRW5DbGFpcj0iNDE5NDMwNCIgdGFpbGxlQXByZXNDaGlmZnJlbWVudD0iNzQ2MDIzNiIgbnVtT3JkcmU9IjE2IiBlbXByZWludGU9IkQ0MUFDMEE4MEY4NEE3QTUwQUZCOTRBRkYzRjBEQ0IxMjcwMzEzOTMiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MzciLz48QmxvY0NoaWZmcmVtZW50IHRhaWxsZUVuQ2xhaXI9IjQxOTQzMDQiIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9Ijc0NjAyMzYiIG51bU9yZHJlPSIxNyIgZW1wcmVpbnRlPSI5QTVEMzYxRTA0QzBCM0YxNjY0MUQ5RDAzQ0FBNTlCQkIwMDk2MDQxIiBzdGF0dXRDaGlmZnJlbWVudD0iQ0hJIiBub21CbG9jPSIyNTM4Ii8+PEJsb2NDaGlmZnJlbWVudCB0YWlsbGVFbkNsYWlyPSIxOTQ5NTciIHRhaWxsZUFwcmVzQ2hpZmZyZW1lbnQ9IjM1MDI2NCIgbnVtT3JkcmU9IjE4IiBlbXByZWludGU9IkU5RjdCNjg1Q0M4RUQwQzA0MkRERjlGODRGQTU1OEMzMzQ0RUU4QTUiIHN0YXR1dENoaWZmcmVtZW50PSJDSEkiIG5vbUJsb2M9IjI1MzkiLz48L0Jsb2NzQ2hpZmZyZW1lbnQ+PC9GaWNoaWVyPjwvRW52ZWxvcHBlPjwvRW52ZWxvcHBlcz48L1JlcG9uc2VBbm5vbmNlPg0K</reponseAnnonceXML>      <statut>1</statut>      <heureFinChiffrement>2016-10-07T10:55:55</heureFinChiffrement>    </offre> </envoi> </mpe>';
            $uri = Atexo_Config::getParameter('PF_URL_REFERENCE').'api.php/ws/registre/depot/update?ticket='.$ticket;
            $client->setUri($uri);
            $client->setParameterPost('xml', $xml);
            $res = $client->request('POST')->getBody();
            $domDoc = simplexml_load_string($res);
            $var = (array) $domDoc->reponse;
            if (isset($var['@attributes']['statutReponse']) && 'OK' == $var['@attributes']['statutReponse']) {
                $return = true;
            } elseif (isset($domDoc->reponse->messageErreur)) {
                echo $domDoc->reponse->messageErreur;
            }
        } catch (Exception $e) {
            echo 'Erreur : '.$e->getMessage();
        }

        return $return;
    }

    public function testWsDechiffrementFichier($ticket)
    {
        try {
            $return = false;
            $client = $this->getZendClient();
            $uri = Atexo_Config::getParameter('PF_URL_REFERENCE').'api.php/ws/registre/pli/dechiffre/938?ticket='.$ticket;
            $client->setUri($uri);
            $client->setFileUpload('/tmp/Bloc_test', 'file', 'APRGOEPZSOKERGFPELGOERJHEPRLGFMEORGPEAKRGKQELMJGOQERKGPKEOPRJKTGO');
            $res = $client->request('POST')->getBody();
            $domDoc = simplexml_load_string($res);
            $var = (array) $domDoc->reponse;
            if (isset($var['@attributes']['statutReponse']) && 'OK' == $var['@attributes']['statutReponse']) {
                $return = true;
            } elseif (isset($domDoc->reponse->messageErreur)) {
                echo $domDoc->reponse->messageErreur;
            }
        } catch (Exception $e) {
            echo 'Erreur : '.$e->getMessage();
        }

        return $return;
    }

    public function testWsDechiffrementEnveloppe($ticket)
    {
        try {
            $return = false;
            $client = $this->getZendClient();
            $xml = '<?xml version="1.0" encoding="UTF-8"?> <mpe xmlns="http://www.atexo.com/epm/xml"> <envoi>    <enveloppe> 		<idOffre>328</idOffre> 		<idAgent>1</idAgent> 		<organisme>a1a</organisme> 		<idEnveloppe>744</idEnveloppe> 		<statut>2</statut> 		<integriteFichier>true</integriteFichier> 		<heureFinDechiffrement>2016-10-07T12:55:55</heureFinDechiffrement>    </enveloppe> </envoi> </mpe>';
            $uri = Atexo_Config::getParameter('PF_URL_REFERENCE').'api.php/ws/registre/depot/update?ticket='.$ticket;
            $client->setUri($uri);
            $client->setParameterPost('xml', $xml);
            $res = $client->request('POST')->getBody();
            $domDoc = simplexml_load_string($res);
            $var = (array) $domDoc->reponse;
            if (isset($var['@attributes']['statutReponse']) && 'OK' == $var['@attributes']['statutReponse']) {
                $return = true;
            } elseif (isset($domDoc->reponse->messageErreur)) {
                echo $domDoc->reponse->messageErreur;
            }
        } catch (Exception $e) {
            echo 'Erreur : '.$e->getMessage();
        }

        return $return;
    }

    /**
     * Return a Zend client with proxy or without proxy setup.
     *
     * @return Zend_Http_Client
     */
    private function getZendClient()
    {
        $config = [
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => [
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => 30,
            ],
        ];

        if ('' != Atexo_Config::getParameter('URL_PROXY')) {
            $config = array(
                'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
            );
        }

        return new Zend_Http_Client(null, $config);
    }

    public function getTicket()
    {
        $config = [
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = Atexo_Config::getParameter('PF_URL_REFERENCE').'api.php/ws/authentification/connexion/'.Atexo_Config::getParameter('LOGIN_WS_REST_AUTENTIFICATION').'/'.Atexo_Config::getParameter('PASSWORD_WS_REST_AUTENTIFICATION');
        //echo $uri;exit;
        $client = new Zend_Http_Client($uri, $config);
        $ticketXml = $client->request('GET')->getBody();
        $domDoc = (array) simplexml_load_string($ticketXml);

        return $domDoc[0];
    }
}
