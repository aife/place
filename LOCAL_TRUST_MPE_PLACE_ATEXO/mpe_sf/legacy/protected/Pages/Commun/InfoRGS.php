<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;

class InfoRGS extends MpeTPage
{
    public function onInit($param)
    {

        if (isset($_GET['AG']) || (isset($_GET['calledFrom']) && 'agent' == $_GET['calledFrom'])) {
            $this->Master->setCalledFrom('agent');
        } else {
            $this->Master->setCalledFrom('entreprise');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }
}
