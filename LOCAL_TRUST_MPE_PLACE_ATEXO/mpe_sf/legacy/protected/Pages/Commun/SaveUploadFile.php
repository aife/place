<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Exception;

/**
 * Classe SaveUploadFile.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.2
 *
 * @since MPE-4.0
 */
class SaveUploadFile extends MpeTPage
{
    public function onLoad($param)
    {
        try {
            if (isset($_GET['file'])) {
                $infilepiece = Atexo_Config::getParameter('COMMON_TMP').'autrePiece'.session_id().time();
                $exe = 'cp -f '.$_FILES['fileToUpload']['tmp_name'].' '.escapeshellarg($infilepiece).' ;';
                $message = 'autrePiece'.session_id().time();
                system($exe, $sys_answer);
                if (0 != $sys_answer) {
                    header('HTTP/1.1 500 Internal Server Error');
                    exit;
                }
                echo $message;
                exit;
            }
        } catch (Exception) {
            header('HTTP/1.1 500 Internal Server Error');
            exit;
        }
    }
}
