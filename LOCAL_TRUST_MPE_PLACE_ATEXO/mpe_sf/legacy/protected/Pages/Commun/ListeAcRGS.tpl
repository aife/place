<com:TContent ID="CONTENU_PAGE">
    <!--Debut colonne droite-->
    <div class="main-part" id="main-part">
        <div class="breadcrumbs"><span class="normal"><com:TTranslate>TEXT_MESSAGERIE_ACCUEIL</com:TTranslate> </span> &gt; <com:TTranslate>LISTE_DES_CERTIFICATS_EUROPEEN</com:TTranslate></div>
        <div class="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <h2><com:TTranslate>TITRE_LISTE_DES_CERTIFICATS_EUROPEEN</com:TTranslate></h2>
                <p><com:TTranslate>LIEN_LISTE_CERTIFICATS_CONFORMES_EUROPEEN</com:TTranslate></p>
                <div class="table-bloc table-bloc-detail">
                    <div class="toggle-ac"><a class="collapse-all" href="#"><com:TTranslate>DEFINE_TOUT_AFFICHER</com:TTranslate></a> / <a class="toggle-all" href="#"><com:TTranslate>DEFINE_TOUT_CACHER</com:TTranslate></a></div>
                        <table summary="<%=Prado::localize('LISTE_AUTORITE_DE_CERTIFICATION')%>" class="table-results tableau-detail tableau-detail-pli liste-ac">
                            <caption><%=Prado::localize('LISTE_AUTORITE_DE_CERTIFICATION')%> (<com:TTranslate>DEFINE_AC</com:TTranslate>)</caption>
                            <com:TActiveRepeater ID="repeaterRacineCertifRGS" >
                                <prop:ItemTemplate>
                                    <!--Debut Ligne Racine-->
                                    <tr class="line-racine">
                                        <td colspan="3" class="title col-320">
                                            <com:THyperLink ID="togleRacine"  Attributes.onclick="togglePanelAc(this,'panel_<%#$this->Data['Index']%>');" cssClass="title-toggle" ><com:TTranslate>AC_RACINE</com:TTranslate> :</com:THyperLink>
                                            <a href="<%#$this->Data['InformationURI']%>" class="lien-ext" target="_blank"><%#($this->Data['TSPName'])%></a>
                                        </td>
                                        <td class="col-30 center">&nbsp;</td>
                                    </tr>
                                    <!--Fin Ligne Racine-->
                                    <!--Debut Ligne Filles-->
                                    <com:TActiveRepeater ID="repeaterFilleCertifRGS" DataSource="<%#$this->Data['ACFilles']%>" >
                                        <prop:ItemTemplate>
                                            <!--Debut Ligne fille-->
                                            <tr class="panel-detail panel_<%#$this->Data['IndexPere']%>" style="display:none;" data-serial-number="<%#$this->Data['SerialNumber']%>">
                                                <td class="toggle-col">
                                                    <a class="title-toggle" onclick="togglePanelClass(this,'panel_detail_<%#$this->Data['Index']%>');" href="javascript:void(0);"><com:TTranslate>TEXT_VOIR_DETAIL</com:TTranslate></a>
                                                </td>
                                                <td class="col-320"><com:TTranslate>AC_FILLE</com:TTranslate> : <strong><%#($this->Data['CN'])%></strong></td>
                                                <td>
                                                    <div class="intitule-auto">
                                                        <div class="float-left"><com:TTranslate>NOM_COMMERCIAL</com:TTranslate> : </div>
                                                    </div>
                                                    <div class="intitule-auto indent-15"><strong><%#$this->Data['ServiceName']%></strong></div>
                                                </td>
                                                <td class="col-30 center">
                                                    <com:TImageButton
                                                        onCommand="Page.downloadCertificat"
                                                        CommandParameter="<%#$this->Data['IndexPere'].'#'.$this->Data['Index']%>"
                                                        ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif"
                                                        AlternateText="<%=Prado::localize('TELECHARGER_CLE_PIBLIQUE_AC_FILLE')%>"
                                                        Attributes.title="<%=Prado::localize('TELECHARGER_CLE_PIBLIQUE_AC_FILLE')%>"
                                                    >
                                                    </com:TImageButton>
                                                </td>
                                            </tr>
                                            <tr class="panel_detailAC_01 panel_detail_<%#$this->Data['Index']%>" style="display:none;">
                                                <td colspan="4" class="detail-fichier">
                                                    <!--Debut bloc detail-->
                                                    <div class="bloc-detail">
                                                        <div class="title"><com:TTranslate>DETAIL_AC_FILLE</com:TTranslate></div>
                                                        <div class="line">
                                                            <span class="intitule-auto"><com:TTranslate>VALIDE_A_PARTIR</com:TTranslate> :</span><%#$this->Data['ValidityNotBefore']%>
                                                        </div>
                                                        <div class="line">
                                                            <span class="intitule-auto"><com:TTranslate>VALABLE_JUSQU_AU</com:TTranslate> :</span><%#$this->Data['ValidityNotAfter']%>
                                                        </div>
                                                        <div class="line">
                                                            <span class="intitule-auto"><com:TTranslate>LISTE_CERTIFICATS_REVOQUE</com:TTranslate> :</span><a href="<%#($this->Data['CrlDistributionPoints'][0])?$this->Data['CrlDistributionPoints'][0]:"#"%>" class="<%#($this->Data['CrlDistributionPoints'][0])?"lien-ext":""%>" target="_blank"><%#($this->Data['CrlDistributionPoints'][0])?$this->Data['CrlDistributionPoints'][0]:" - "%></a>
                                                        </div>
                                                        <div class="line">
                                                            <span class="intitule-auto"><com:TTranslate>POLITIQUE_DE_CERTIFICATION</com:TTranslate> :</span><a href="<%#($this->Data['CertificatPolicies'][0]['URI'])?$this->Data['CertificatPolicies'][0]['URI']:"#"%>" class="<%#($this->Data['CertificatPolicies'][0]['URI'])?"lien-ext":""%>" target="_blank"><%#($this->Data['CertificatPolicies'][0]['URI'])?$this->Data['CertificatPolicies'][0]['URI']:" - "%></a>
                                                        </div>
                                                        <div class="line">
                                                            <span class="intitule-auto"><com:TTranslate>IDENTIFIANT_POLITIQUE_DE_CERTIFICATION</com:TTranslate> :</span><%#$this->Data['CertificatPolicies'][0]['PolicyIdentifier']%>
                                                        </div>
                                                        <div class="breaker"></div>
                                                    </div>
                                                    <!--Fin bloc detail-->
                                                </td>
                                            </tr>
                                        </prop:ItemTemplate>
                                    </com:TActiveRepeater>
                                    <!--Fin Ligne Filles-->
                                </prop:ItemTemplate>
                            </com:TActiveRepeater>

                        </table>
                    <div class="toggle-ac"><a class="collapse-all" href="#"><com:TTranslate>DEFINE_TOUT_AFFICHER</com:TTranslate></a> / <a class="toggle-all" href="#"><com:TTranslate>DEFINE_TOUT_CACHER</com:TTranslate></a></div>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>

        <div class="breaker"></div>
    </div>
    <!--Fin colonne droite-->
    <div class="breaker"></div>
</com:TContent>