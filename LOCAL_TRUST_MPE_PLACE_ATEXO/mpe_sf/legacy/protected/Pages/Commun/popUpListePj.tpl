<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
<div class="popup-moyen" id="container">
	<h1 class="hide"><com:TTranslate>TEXT_AJOUTER_MODIFIER_PIECES_JOINTES</com:TTranslate></h1>
	<com:UploadPj ID="IdUploadPj"/>
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TButton  
				CssClass="bouton-moyen float-left btn btn-default btn-sm pull-left m-t-1 m-l-2"
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.onclick = "window.close();"/>
		<com:TButton  
				CssClass="bouton-moyen float-right btn btn-primary btn-sm pull-right m-t-1 m-r-2"
				Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_VALIDER_SELECTION')%>" 
				onClick="OnValidClick"/>	
	</div>
	<!--Fin line boutons-->
</div>
<script type="text/javascript">popupResize();</script>
</com:TContent>