<com:TContent ID="CONTENU_PAGE">
    <com:TConditional condition="$this->getModeServeurCrypto() == 1">
        <prop:trueTemplate>
            <%include Application.templates.crypto.PrerequisTechniquesJWS %>
        </prop:trueTemplate>
        <prop:falseTemplate>
            <%include Application.templates.mpe.PrerequisTechniquesApplet %>
        </prop:falseTemplate>
    </com:TConditional>
</com:TContent>