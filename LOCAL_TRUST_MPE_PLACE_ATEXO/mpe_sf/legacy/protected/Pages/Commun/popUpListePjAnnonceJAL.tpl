<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent ID="CONTENU_PAGE">
<div class="popup-moyen" id="container">
	<h1><com:TTranslate>TEXT_AJOUTER_MODIFIER_PIECES_JOINTES</com:TTranslate></h1>
	<com:PanelMessageErreur id="panelMessageErreur"/>
	<!--Debut Bloc Echange-->
<div class="form-field">
<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<div class="content">		

	<!--Debut Bloc Infos Annonce JAL-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	  <div class="content">
			<div class="line">
				<div class="intitule-100 bold">
					<com:TTranslate>DEFINE_TEXT_PJ</com:TTranslate> :
				</div>
							
	             <com:TActivePanel CssClass="content-bloc bloc-660" ID="PanelPJ">
				  <com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
					<prop:ItemTemplate>
						<div class="picto-link inline">
								<a href="?page=Agent.AgentDownloadPjAnnonce&idPj=<%#$this->Data->getPiece()%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
								<com:TActiveLinkButton 
									CssClass="indent-5"
									OnCallBack="Page.refreshRepeaterPJ" 
									OnCommand="Page.onDeleteClick" 
									CommandParameter="<%#$this->Data->getId()%>"
									>
									<com:TImage 
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
										AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
										Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
										/>
								</com:TActiveLinkButton>
						</div>
						<br/><br/>
					</prop:ItemTemplate>
				</com:TRepeater>	  			
			  </com:TActivePanel>
							
			</div>
			
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Infos Annonce JAL-->
	
	<!--Debut Bloc Ajout piece jointe-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	  <div class="content">
			<h2><com:TTranslate>DEFINE_TEXT_AJOUT_PJ</com:TTranslate></h2>
			<div class="spacer-small"></div>
			<div class="line">
				<div class="intitule-100 bold"><label for="<%=$this->ajoutFichier->getClientId()%>"><com:TTranslate>DEFINE_TEXT_FICHIER</com:TTranslate></label> :</div>
				<com:AtexoFileUpload 
					ID="ajoutFichier" 
					Attributes.title="<%=Prado::localize('OBJET')%>"
					Attributes.CssClass="file-580 float-left" 
					Attributes.size="98" 
					/>
				<com:TImageButton
					OnClick="onAjoutClick"
					ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif"
					AlternateText="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>" 
					Attributes.title="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>" 
					/>
			</div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Ajout piece jointe-->
	
	<div class="breaker"></div>
	</div>
	<div class="breaker"></div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Annonce JAL-->
	
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TButton  
				CssClass="bouton-moyen float-left" 
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.onclick = "window.close();"/>
		<com:TButton  
				CssClass="bouton-moyen float-right" 
				Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_VALIDER_SELECTION')%>" 
				onClick="OnValidClick"/>	
	</div>
	<!--Fin line boutons-->
</div>
<script type="text/javascript">popupResize();</script>
</com:TContent>