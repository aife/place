<com:TContent ID="CONTENU_PAGE">
<!--Debut colonne droite-->
	<com:TConditional condition="$this->getModeServeurCrypto() == 1">
		<prop:trueTemplate>
			<%include Application.templates.crypto.SignDocument %>
		</prop:trueTemplate>
		<prop:falseTemplate>
			<%include Application.templates.mpe.SignDocument %>
		</prop:falseTemplate>
	</com:TConditional>
<!--Fin colonne droite-->
</com:TContent>