<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;

/**
 * Page d'accueil non authentifié des agents.
 *
 * @author Adil El Kanabi
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PortailDefense extends MpeTPage
{
    public function onInit($param)
    {

        $type = Atexo_Util::atexoHtmlEntities($_GET['type']);
        if ($type == Atexo_Config::getParameter('TYPE_COMPTE_DEFENSE_AGENT')) {
            $this->Master->setCalledFrom('agent');
        } elseif ($type == Atexo_Config::getParameter('TYPE_COMPTE_DEFENSE_INSCRIT')) {
            $this->Master->setCalledFrom('entreprise');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::isConnected()) {
            $type = Atexo_Util::atexoHtmlEntities($_GET['type']);
            $urlDefense = Atexo_Config::getParameter('URL_DEFENSE');

            if ($type == Atexo_Config::getParameter('TYPE_COMPTE_DEFENSE_AGENT')) {
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
                $ssoAgent = (new Atexo_InterfaceWs_Authentication())->generateSsoForAgent($agent);
                $ident = $agent->getLogin();
                $sso = $ssoAgent->getIdSso();
            } elseif ($type == Atexo_Config::getParameter('TYPE_COMPTE_DEFENSE_INSCRIT')) {
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
                $ssoInscrit = (new Atexo_InterfaceWs_Authentication())->generateSsoForInscrit($inscrit);
                $ident = $inscrit->getLogin();
                $sso = $ssoInscrit->getIdSso();
            } else {
                return;
            }

            $url = 'https://'.$urlDefense.'/?ident='.$ident.'&type='.Atexo_Util::atexoHtmlEntities($type).'&sso='.$sso.'';
            $this->response->redirect($url);
        }
    }
}
