<com:TContent ID="CONTENU_PAGE">
<!--Debut colonne droite-->
	<div style="height: auto;" class="main-part" id="main-part">
		<div class="breadcrumbs"><a target="_blank" href="#">Aide</a>&gt; Guides Multimedia</div>
		<com:TRepeater ID="RepeaterAtexoGuides">
			<prop:ItemTemplate>
				<com:AtexoGuides id="guideAtexo" typeGuides="<%#$this->Data['type']%>" idUnicite="<%#$this->itemIndex%>"  />
			</prop:ItemTemplate>
		</com:TRepeater>
		<div class="breaker"></div>
	</div>
	<!--Fin colonne droite-->
</com:TContent>