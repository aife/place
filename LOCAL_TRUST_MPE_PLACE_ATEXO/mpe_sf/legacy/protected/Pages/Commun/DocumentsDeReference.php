<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Pages\Commun;

use App\Service\Routing\RouteBuilderInterface;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use AtexoDume\Service\EntrepriseService;
use Prado\Prado;

/**
 * Classe de gestion des documents de reference (pour les agents et pour les inscrits des entreprises).
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4.8
 */
class DocumentsDeReference extends MpeTPage
{
    public const HOME_PAGE_LINK = '?page=Entreprise.EntrepriseHome';

    /**
     * Initialisation de la classe.
     *
     * (non-PHPdoc)
     *
     * @see TControl::onInit()
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['callFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     *
     * (non-PHPdoc)
     *
     * @see TControl::onLoad()
     */
    public function onLoad($param)
    {
        $this->hasAccessToDocumentsReference();

        if ('agent' == $_GET['callFrom']) {
            $this->documentsRefEntreprise->visible = false;
            $this->documentsRefAgent->visible = true;
        } else {
            $this->documentsRefEntreprise->visible = true;
            $this->documentsRefAgent->visible = false;
        }
    }

    /**
     * Permet de recuperer le message des documents de reference.
     *
     * @return: le document de reference
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getMessageDocReference()
    {
        return Prado::localize('TEXT_MSG_DOCUMENTS_REFERENCE');
    }

    public function hasAccessToDocumentsReference(): void
    {
        if (!Atexo_Module::isEnabled('DocumentsReference')) {
            if ('agent' == $_GET['callFrom']) {
                $redirectLink = Atexo_Util::getSfService(RouteBuilderInterface::class)->getRoute(
                    'atexo_agent_accueil_index'
                );
            } else {
                $redirectLink = self::HOME_PAGE_LINK;
            }

            $this->response->redirect($redirectLink);
        }
    }
}
