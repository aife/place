<%@MasterClass="Application.layouts.PopUpLayout"%>
<com:TContent id="CONTENU_PAGE">
    <div class="form-horizontal" id="container">
        <div class="" tabindex="-1" role="dialog">
            <div class="" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.close();"><span aria-hidden="true" onclick="window.close();">&times;</span></button>
                        <h4 class="modal-title">
                            <com:TTranslate>TEXT_DESCRIPTIF_LOTS_CONSULTATION</com:TTranslate>
                        </h4>
                    </div>
                    <div class="modal-body clearfix">
                        <com:TRepeater id="repeaterLots">
                            <prop:ItemTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading collapsed clearfix" id="headingOne" role="button" data-toggle="collapse"
                                         data-parent="#accordion" href="#collapse_<%#$this->Data->getLot()%>" aria-expanded="false" aria-controls="collapseOne">
                                        <strong class="text-primary">
                                            <com:TLabel Text="<%=Prado::localize('TEXT_LOT')%>"/>
                                            <%#$this->Data->getLot()%> :
                                        </strong>
                                        <span>
                                                <%#$this->Page->getDescriptionLot($this->Data)%>
                                            </span>
                                    </div>
                                    <div id="collapse_<%#$this->Data->getLot()%>" class="panel-collapse collapse" role="tabpane_<%#$this->Data->getLot()%>"
                                         aria-labelledby="heading_<%#$this->Data->getLot()%>">
                                        <div class="panel-body">
                                            <!--Debut Lot 1-->
                                            <div class="form-group form-group-sm">
                                                <label class="col-md-2 col-xs-4">
                                                    <com:TLabel Text="<%=Prado::localize('TEXT_CATEGORIE')%>"/>
                                                    :
                                                </label>
                                                <div class="col-md-10 col-xs-8">
                                                    <%#$this->Page->getCategoryLot($this->Data->getCategorie())%>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label class="col-md-2 col-xs-4">
                                                    <com:TLabel Text="<%=Prado::localize('DEFINE_DESCRIPTION')%>"/>
                                                    :
                                                </label>
                                                <div class="col-md-10 col-xs-8">
                                                    <%#$this->Page->getDescriptionDetail($this->Data)%>
                                                </div>
                                            </div>

                                            <!--<com:TPanel ID="panelCodeCPV" display="<%=$this->page->affichageCodeCPV%>">
                                                 <div class="intitule-bloc intitule-150">
                                                     <com:TLabel Text="<%=Prado::localize('TEXT_CPV')%>" /> :
                                                 </div>
                                                 <div class="content-bloc bloc-600">
                                                 <com:AtexoCPV id="idAtexoCPV" />
                                                 </div>
                                             </com:TPanel> -->

                                            <!-- module referentiel CPV -->
                                            <com:TPanel ID="panelCodeRef" cssClass="form-group form-group-sm" display="<%=$this->page->affichageCodeCPV%>">
                                                <label class="col-md-2 col-xs-4">
                                                    <com:TLabel Text="<%=Prado::localize('TEXT_CPV')%>"/>:
                                                </label>
                                                <div class="col-md-10 col-xs-8">
                                                    <!--<com:AtexoReferentiel id="idAtexoRef" cas="cas4"/>-->
                                                    <span style="display: <%#$this->Data->getCodeCpv1()?'block':'none'%>">
                                                        <%#$this->Data->getCodeCpv1()%>
                                                        <span class="small"><com:TTranslate>DEFINE_CODE_PRINCIPAL</com:TTranslate></span>
                                                    </span>
                                                    <span style="display: <%#$this->Data->getCodeCpv2()?'block':'none'%>">
                                                        <%=$this->Page->getCodeCpvSecondaire($this->Data->getCodeCpv2())%>
                                                    </span>
                                                </div>
                                            </com:TPanel>
                                            <com:AtexoLtReferentiel id="ltrefLot" lot="1" mode="0" cssClass="intitule-150"/>
                                            <com:AtexoReferentielZoneText id="idReferentielZoneTextLot" lot="1" mode="0" cssClass="intitule-150" label="true" type="1" line="0"/>
                                            <!--  -->
                                            <com:TPanel ID="panelCautionProvisoire" display="<%=$this->page->cautionProvisoire%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>CAUTION_PROVISOIRE</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <com:TLabel ID="cautionProvisoire"/>
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelQualification" display="<%=$this->page->qualification%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <!-- ul class="default-list" !-->
                                                    <com:TLabel ID="qualification"/>
                                                    <!-- /ul -->
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelAgrements" display="<%=$this->page->agrement%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>TEXT_AGREMENT</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <!-- ul class="default-list" -->
                                                    <com:TLabel ID="agrements"/>
                                                    <!-- /ul -->
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelEchantillons" display="<%=$this->page->echantillonsDemandes%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>ECHANTILLONS_DEMANDES</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <com:TLabel ID="labelDateHeureLimiteEchantillon" Text="<%=Prado::localize('DEFINE_DATE_HEURE_LIMITE')%> : "/>
                                                    <com:TLabel ID="dateEchantillons"/>
                                                    <br/>
                                                    <com:TLabel ID="adresseEchantillons"/>
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelReunion" display="<%=$this->page->reunion%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>REUNION</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <com:TLabel ID="labelDateHeureReunion" Text="<%=Prado::localize('DEFINE_DATE_HEURE')%> : "/>
                                                    <com:TLabel ID="dateReunion"/>
                                                    <br/>
                                                    <com:TLabel ID="adresseReunion"/>
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelVisitesLieux" display="<%=$this->page->visiteDesLieux%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>VISITE_DES_LIEUX</com:TTranslate>
                                                    :
                                                </div>
                                                <com:TPanel ID="panelRepeaterVisitesLieux" cssClass="content-bloc bloc-600">
                                                    <com:TRepeater id="repeaterVisitesLieux">
                                                        <prop:HeaderTemplate>
                                                            <ul class="default-list">
                                                        </prop:HeaderTemplate>
                                                        <prop:ItemTemplate>
                                                            <li>
                                                                <com:TLabel ID="dateVisites" Text="<%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDate())%>"/>
                                                                ,
                                                                <com:TLabel ID="adresseVisites" Text="<%#$this->Data->getAdresseTraduit()%>"/>
                                                            </li>
                                                        </prop:ItemTemplate>
                                                        <prop:FooterTemplate>
                                                            </ul>
                                                        </prop:FooterTemplate>
                                                    </com:TRepeater>
                                                </com:TPanel>
                                                <com:TPanel ID="panelPasVisitelieu" cssClass="content-bloc bloc-500" visible="false">
                                                    -
                                                </com:TPanel>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:TPanel ID="panelVariante" display="<%=$this->page->variante%>">
                                                <div class="intitule-bloc intitule-150">
                                                    <com:TTranslate>TEXT_VARIANTE</com:TTranslate>
                                                    :
                                                </div>
                                                <div class="content-bloc bloc-600">
                                                    <com:TLabel ID="varianteValeur"/>
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <!-- Clauses Clauses soc./env -->
                                            <com:TPanel ID="panelClauseConsultation" Visible="<%# Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause') && (Application\Service\Atexo\Atexo_Consultation::hasClauseSociale($this->Data) || Application\Service\Atexo\Atexo_Consultation::hasClauseEnvironnementales($this->data))%>">
                                                <div style="display:block;">
                                                    <div class="form-group form-group-sm">
                                                        <label class="col-md-2 col-xs-4">
                                                            <com:TTranslate>DEFINE_CLAUSES_SOC_ENV</com:TTranslate> :
                                                        </label>
                                                        <div class="col-md-10 col-xs-8">
                                                            <div class="dispositions">
                                                                <com:IconeAchatResponsableConsultation objet="<%#$this->Data%>" Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause')%>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </com:TPanel>
                                            <!-- -->
                                            <com:AtexoLtRefRadio id="idRefRadio" consultation="1" cssClass="intitule-bloc intitule-150" mode="0" type="2" label="true" lot="1" line="0"/>

                                            <!-- Fin Clauses Clauses soc./env -->
                                            <!--Fin Lot 1-->
                                        </div>
                                    </div>
                                </div>
                            </prop:ItemTemplate>
                        </com:TRepeater>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-sm btn-default" value="<%=Prado::localize('FERMER_FENETRE')%>" title="<%=Prado::localize('FERMER_FENETRE')%>" onclick="window.close();"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">popupResize('container');</script>
</com:TContent>