<com:TContent ID="CONTENU_PAGE">
<!--Debut colonne droite-->
	<div class="main-part" id="main-part">
		<div class="breadcrumbs">&gt;  <com:TTranslate>CONDITION_UTILISATION</com:TTranslate></div>

		<div class="clearfix hide-agent">
			<ul class="breadcrumb">
				<li>
					<a href="/entreprise">
						<com:TTranslate>TEXT_MESSAGERIE_ACCUEIL</com:TTranslate>
					</a>
				</li>
				<li>
					<com:TTranslate>CONDITION_UTILISATION</com:TTranslate>
				</li>
			</ul>
		</div>

		<!--Debut Bloc Conditions utilisation-->
		<com:TPanel visible="<%=($this->visibiliteBlocCguSpecifique())? true:false%>">
			<%=$this->getLibelleBlocCguSpecifique()%>
		</com:TPanel>
		<com:TPanel visible="<%=($this->visibiliteBlocCguSpecifique())? false:true%>">
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
            	<div class="main-text margin-top-3">
					<h2 class="message-big" id="rubrique_1"><com:TTranslate>CONDITION_UTILISATION</com:TTranslate></h2>
                	<p><com:TTranslate>TEXT_CONDITION_UTILISATION</com:TTranslate></p>
					<div class="spacer-mini"></div>

					<div class="bloc-faq">
						<ul>
							<li><a id="rubrique_1_1" href="#rubrique_1_paragraphe_1"><com:TTranslate>APPROBATION_APPLICATION</com:TTranslate></a></li>
							<li><a id="rubrique_1_2" href="#rubrique_1_paragraphe_2"><com:TTranslate>AVERTISSEMENT_RECOMMANDATION</com:TTranslate></a></li>
                            <li><a id="rubrique_1_4" href="#rubrique_1_paragraphe_4"><com:TTranslate>UTILISATION_MESSAGERIE_SECURISEE</com:TTranslate></a></li>
							<div style="display:<%=( Application\Service\Atexo\Atexo_Config::getParameter('AFFICHER_CONDITION_CERTIFICAT'))? '':'none'%>" >
							<li><a id="rubrique_1_3" href="#rubrique_1_paragraphe_3"><com:TTranslate>CERTIFICAT_ELECTRONIQUE</com:TTranslate></a>
								<ul>
									<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ListeACRgs'))? '':'none'%>" ><a id="rubrique_1_3_0" href="#rubrique_1_paragraphe_3_0"><com:TTranslate>LISTE_DES_CERTIFICATS_RGS</com:TTranslate></a></li>
									<li><a id="rubrique_1_3_1" href="#rubrique_1_paragraphe_3_1"><com:TTranslate>PK_CERTIFICAT_ELECTRONIQUE</com:TTranslate></a></li>

									<li><a id="rubrique_1_3_2" href="#rubrique_1_paragraphe_3_2"><com:TTranslate>OBTENIR_CERTIFICAT_ELECTRONIQUE</com:TTranslate></a></li>
									<li><a id="rubrique_1_3_3" href="#rubrique_1_paragraphe_3_3"><com:TTranslate>DIFFICULTE_CERTIFICAT_ELECTRONIQUE</com:TTranslate></a></li>

								</ul>
							</li>
							<li><a id="rubrique_1_5" href="#rubrique_1_paragraphe_5"><com:TTranslate>PROTECTION_DONNES_PERSONNELLES</com:TTranslate></a></li>
							<li><a id="rubrique_1_6" href="#rubrique_1_paragraphe_6"><com:TTranslate>TEMOINS_CONNEXION_COOKIES</com:TTranslate></a></li>
							</div>
						</ul>
					</div>
				</div>
            	<div class="breaker"></div>
            </div>
 			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin bloc Conditions utilisation-->
		<!--Debut Bloc Conditions utilisation-->
		<div class="main-text margin-top-3">
			<div class="spacer"></div>
			<!--Fin bloc Consultation de test-->
			<h2 class="message-big"><com:TTranslate>CONDITION_UTILISATION</com:TTranslate></h2>
			<p><com:TTranslate>TEXT_CONDITION_UTILISATION</com:TTranslate></p>
			<h3 id="rubrique_1_paragraphe_1"><com:TTranslate>APPROBATION_APPLICATION</com:TTranslate></h3>
			<p><com:TTranslate>TEXT_APPROBATION_APPLICATION</com:TTranslate></p>
			<div class="link-line"> <a href="#rubrique_1" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>CONDITION_UTILISATION</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
			<h3 id="rubrique_1_paragraphe_2"><com:TTranslate>AVERTISSEMENT_RECOMMANDATION</com:TTranslate></h3>
			<p><com:TTranslate>TEXT_AVERTISSEMENT_RECOMMANDATION</com:TTranslate></p>
            <div class="link-line"> <a href="#rubrique_1" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>CONDITION_UTILISATION</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
            <h3 id="rubrique_1_paragraphe_4"><com:TTranslate>UTILISATION_MESSAGERIE_SECURISEE</com:TTranslate></h3>
            <p><com:TTranslate>TEXT_UTILISATION_MESSAGERIE_SECURISEE</com:TTranslate></p>
			<div class="link-line"> <a href="#rubrique_1" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>CONDITION_UTILISATION</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a> </div>
            <div style="display:<%=( Application\Service\Atexo\Atexo_Config::getParameter('AFFICHER_CONDITION_CERTIFICAT'))? '':'none'%>">
			<div class="breaker"></div>
			<h3 id="rubrique_1_paragraphe_3"><com:TTranslate>CERTIFICAT_ELECTRONIQUE</com:TTranslate></h3>
			<!--Debut Ajout Rubrique Liste certificats RGS-->
			<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ListeACRgs'))? '':'none'%>">
				<p id="rubrique_1_paragraphe_3_0"><strong><com:TTranslate>LISTE_DES_CERTIFICATS_RGS</com:TTranslate></strong><br />
					<com:TTranslate>TEXT_LISTE_AUTORITES_CERTIFICATIONS</com:TTranslate> ( <a href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.ListeAcRGS&calledFrom=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['calledFrom'])%>" > <com:TTranslate>DEFINE_CLIQUEZ_ICI</com:TTranslate></a> <com:TTranslate>POUR_Y_ACCEDER_DIRECTEMENT</com:TTranslate> )
				</p>
			</div>
			<!--Fin Ajout Rubrique Liste certificats RGS-->
			<p id="rubrique_1_paragraphe_3_1"><strong><com:TTranslate>PK_CERTIFICAT_ELECTRONIQUE</com:TTranslate></strong><br />
				<com:TTranslate>CERTIFICAT_ELECTRONIQUE_PERMET</com:TTranslate></p>

			<ul class="liste-actions">
				<li><com:TTranslate>CERTIFICAT_ELECTRONIQUE_PERMET_DE_PROUVER_IDENTITE</com:TTranslate></li>
				<li><com:TTranslate>CERTIFICAT_ELECTRONIQUE_PERMET_DE_SIGNER_OFFRES</com:TTranslate></li>
			</ul>
			<p id="rubrique_1_paragraphe_3_2"><br /><strong><com:TTranslate>OBTENIR_CERTIFICAT_ELECTRONIQUE</com:TTranslate></strong><br />
				<com:TTranslate>TEXT_OBTENIR_CERTIFICAT_ELECTRONIQUE</com:TTranslate>
			 </p>

			<p id="rubrique_1_paragraphe_3_3"><strong><com:TTranslate>DIFFICULTE_CERTIFICAT_ELECTRONIQUE</com:TTranslate></strong><br />
				<com:TTranslate>TEXT_DIFFICULTE_CERTIFICAT_ELECTRONIQUE</com:TTranslate></p>
			<div class="link-line">
				<a href="#rubrique_1" class="bouton-retour-top" title="<%=Prado::localize('RETOUR_HAUT_RUBRIQUE')%> - <com:TTranslate>CONDITION_UTILISATION</com:TTranslate>"><com:TTranslate>HAUT_PAGE</com:TTranslate></a>
			</div>
			</div>
			<div>
				<h3 id="rubrique_1_paragraphe_5">Protection des données à caractère personnel</h3>
				<div>
					<p>Lexique :</p>
					<p><strong>Un traitement de données à caractère personnel</strong> est une opération ou un ensemble organisé d'opérations effectué sur des données à caractère personnel (collecte, structuration, conservation, modification, communication...).</p>
					<p><strong>Une donnée à caractère personnel </strong>est une information qui permet d'identifier un être humain (personne physique), directement (par exemple son nom/prénom), ou indirectement (par exemple son numéro de téléphone, son numéro de contrat, son pseudonyme). La personne concernée est celle qui peut être identifiée par les données utilisées dans le cadre du traitement de données à caractère personnel.</p>
					<p><strong>Le responsable de traitement </strong>est celui qui décide de la manière dont sera mis en œuvre le traitement des données à caractère personnel, notamment en déterminant les fins et les moyens du traitement.</p>
					<p><strong>Le sous-traitant</strong> est, le cas échéant, celui qui effectue des opérations sur les données pour le compte du responsable de traitement. Il dispose notamment des garanties techniques et organisationnelles, lui permettant de traiter les données à caractère personnel qui lui sont confiées conformément à la réglementation.</p>
					<p>Le responsable de traitement est le propriétaire du site désigné dans la rubrique "Mentions légales". Il traite et conserve les données suivantes à caractère personnel des utilisateurs de l'application :</p>
					<ul style="list-style: none;">
						<li>- L'identité ;</li>
						<li>- Les données de connexion ;</li>
						<li>- Autres : enregistrements des appels des utilisateurs dans le cadre du support, traces applicatives</li>
					</ul>
					<br/>
					<p>Ces données sont conservées pendant la durée d'utilisation du compte utilisateur. Elles sont obligatoires pour utiliser les fonctionnalités de l'application et toute déclaration fausse ou irrégulière engage la responsabilité des utilisateurs concernés. à défaut d'avoir renseigné les champs obligatoires de ces informations, la plateforme ne peut pas être utilisée.</p>
					<p>Seules les données à caractère personnel qui sont strictement utiles sont collectées et traitées et ne sont pas conservées au-delà de la durée nécessaire pour les opérations pour lesquelles elles ont été collectées.</p>
					<p>Dans le cadre de l'utilisation de l'application, le responsable de traitement met en œuvre un traitement de données à caractère personnel sur le fondement de l'article 6.1 c) du règlement n&deg; 2016/679 du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE dit règlement général sur la protection des données (RGPD).</p>
					<p>Les données à caractère personnel peuvent être communiquées par le responsable de traitement à des sous-traitants, au sens du RGPD. à ce titre, les sous-traitants concernés se sont engagés dans le cadre de leur relation avec le responsable de traitement à respecter le RGPD. Le responsable de traitement s'est attaché à sélectionner des sous-traitants présentant les garanties nécessaires.</p>
					<p>Ces données sont traitées de manière confidentielle et ne sont communiquées qu'aux agents publics (acheteurs), gestionnaire de l'application (exploitation, mainteneur), auditeurs et contr&ocirc;leurs externes (juridictions financières et corps de contr&ocirc;le, AQ-SSI) et au délégué à la protection des données à caractère personnel (se reporter à la rubrique &ldquo;Mentions légales&rdquo;).</p>
					<p>Ces données personnelles servent au responsable de traitement afin de permettre la dématérialisation des procédures de marchés publics.</p>
					<p>Le responsable de traitement prend, au regard de la nature des données à caractère personnel et des risques que présentent les traitements, les mesures techniques, physiques et organisationnelles appropriées pour assurer la protection des données à caractère personnel et empêcher qu'elles ne soient perdues, altérées, détruites ou que des tiers non autorisés y aient accès de manière accidentelle ou illicite.</p>
					<p>Conformément aux dispositions du RGPD, les utilisateurs concernés disposent de droits dédiés sur leurs données à caractère personnel, notamment : un droit d'information, un droit d'accès, un droit de rectification, un droit d'effacement, un droit de limitation. Ces droits peuvent être exercés auprès du délégué à la protection des données (DPD) (se reporter à la rubrique &ldquo;Mentions légales&rdquo;), ainsi que directement auprès du responsable de traitement.</p>
					<p>Pour faciliter les démarches, chaque utilisateur concerné, lors de l'envoi d'une demande d'exercice des droits, est invité à :</p>
					<p>
						<ul class="liste-actions">
							<li>Indiquer quel(s) droit(s) il souhaite exercer,</li>
							<li>Mentionner ses noms / prénoms / coordonnées auxquels il souhaite recevoir les réponses,</li>
							<li>Joindre une copie de sa pièce d'identité</li>
						</ul>
					</p>
					<p>Pour plus d'informations, la CNIL a créé une rubrique dédiée à la compréhension des droits :
						<a href="https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles">https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles</a>.
					</p>
				</div>
			</div>
			<br><br>
			<div>
				<h3 id="rubrique_1_paragraphe_6">Témoins de connexion (« cookies »)</h3>
				<div>
					<p>Un témoin de connexion, aussi appelé cookie, est une suite d'informations, généralement de petite taille et identifié par un nom, qui peut être transmis à votre navigateur par un site web sur lequel vous vous connectez. Votre navigateur web le conservera pendant une certaine durée, et le renverra au serveur web chaque fois que vous vous y reconnecterez.</p>
					<p>Les témoins de connexions internes utilisées par la plateforme permettent au site de fonctionner de manière nominale pour un utilisateur authentifié. Vous pouvez vous y opposer et les supprimer en utilisant les paramètres de votre navigateur, cependant votre expérience utilisateur risque d’être très dégradée. En effet, la connexion à votre compte personnel et aux fonctionnalités réservées aux utilisateurs connectés vous seront impossibles.</p>

					<table class="table table-bordered">
						<thead style="background: #000; color: #fff">
						<tr>
							<th style="text-align: center">Nom du témoin</th>
							<th style="text-align: center">Finalité</th>
							<th style="text-align: center">Durée de conservation</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>PHPSESSID</td>
							<td>Récupère ou définit l’identifiant de session pour la session courante de l’utilisateur</td>
							<td>session</td>
						</tr>
						<tr>
							<td>SERVERID</td>
							<td>Détermine le serveur de l’application sur lequel l’utilisateur est connecté</td>
							<td>session</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>
		</com:TPanel>
        <!--Fin bloc Conditions utilisation-->
		<div class="breaker"></div>
	</div>
	<!--Fin colonne droite-->
</com:TContent>