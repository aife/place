<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Prado\Web\UI\TPage;
use Prado\Prado;
use Application\Service\Atexo\Atexo_Languages;

class ProtectionDonnees extends MpeTPage
{

    protected $_calledFrom = "";

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom("agent");
        $this->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {

    }
}
