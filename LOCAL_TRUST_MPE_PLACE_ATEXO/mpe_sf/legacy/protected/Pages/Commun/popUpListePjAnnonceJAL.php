<?php

namespace Application\Pages\Commun;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonceJALPieceJointe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;

/**
 * Classe popUpListePjAnnonceJAL.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpListePjAnnonceJAL extends MpeTPage
{
    protected int $_idAnnonceJAL;
    protected $_org = false;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);

        $this->_idAnnonceJAL = Atexo_Util::atexoHtmlEntities($_GET['IdAnnonceJAL']);
        if ($_GET['org']) {
            $this->_org = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->_org = Atexo_CurrentUser::getCurrentOrganism();
        }

        if (!$this->IsPostBack) {
            $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $PJs = [];

            if ($annonceObject) {
                $PJs = $annonceObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);
            }
            $this->setViewState('listePj', $PJs);
            $this->remplirTableauPJ($PJs);
        }
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function onAjoutClick($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($this->ajoutFichier->HasFile) {
            //Début scan antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $PJs = $this->getViewState('listePj');
                $this->remplirTableauPJ($PJs);
                $this->afficherErreur($msg.' "'.$this->ajoutFichier->FileName.'"');

                return;
            }
            //Fin scan Antivirus

            $infilepj = Atexo_Config::getParameter('COMMON_TMP').'pj'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infilepj)) {
                $atexoBlob = new Atexo_Blob();
                $pjIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infilepj, $this->_org);

                //ajout piece jointe
                $c = new Criteria();
                $PJs = [];

                $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);
                if ($annonceObject) {
                    $PJs = $annonceObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);
                }
                $Pj = new CommonAnnonceJALPieceJointe();
                $Pj->setOrganisme($this->_org);

                $Pj->setNomFichier($this->ajoutFichier->FileName);
                $Pj->setPiece($pjIdBlob);

                $Pj->setIdAnnonceJal($this->_idAnnonceJAL);
                $Pj->setTaille($atexoBlob->getTailFile($pjIdBlob, $this->_org));

                if ($annonceObject) {
                    $annonceObject->addCommonAnnonceJALPieceJointe($Pj);
                    if ($annonceObject->save($connexionCom)) {
                        $PJs[] = $Pj;
                    }
                }
                $this->setViewState('listePj', $PJs);
                $this->remplirTableauPJ($PJs);
            }
        } else {
            $pjs = $this->getViewState('listePj');
            $this->remplirTableauPJ($pjs);
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Publicite_AnnonceJAL::DeletePjAnnonceJAL($idpj, $this->_org);
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        $PJs = null;
        //Raffraichissement du tableau PJ:
        $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($annonceObject) {
            $PJs = $annonceObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);
            if (is_array($PJs) && 0 != count($PJs)) {
                foreach ($PJs as $PJ) {
                    $PJ->setTaille((new Atexo_Blob())->getTailFile($PJ->getPiece(), $this->_org));
                }
            }
        }
        $this->setViewState('listePj', $PJs);
        $this->remplirTableauPJ($PJs);
        $this->PanelPJ->render($param->NewWriter);
    }

    public function OnValidClick()
    {
        self::onAjoutClick(null, null);
        echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniqueJAL_validatePJ').click();window.close();</script>";
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
