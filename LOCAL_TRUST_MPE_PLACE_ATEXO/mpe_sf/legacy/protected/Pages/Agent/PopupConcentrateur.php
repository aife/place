<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Annonce;

/**
 * Popup.
 *
 * @author  Ayoub SOUID AHMED<ayoub.souidahmed@atexo.com>
 * @copyright Atexo
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupConcentrateur extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        header('Pragma:nocache');
        $uidPFDouble = false;
        if ($_GET['double']) {
            $uidPFDouble = Atexo_Config::getParameter('UID_PF_MPE_DOUBLE');
            if (empty($uidPFDouble)
                ||
                (
                    (
                        'ORME' == Atexo_Config::getParameter('PF_SHORT_NAME')
                        ||
                        'PLACE' == Atexo_Config::getParameter('PF_SHORT_NAME')
                    )
                    &&
                    'g7h' != Atexo_CurrentUser::getOrganismAcronym()
                )
            ) {
                return false;
            }
        }
        $consRef = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $consultation = $this->retrieveConsultation($consRef);
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getCompteBoampAssocie()) {
                $this->gotoConcentrateur($consultation, $uidPFDouble);
            }
        }
    }

    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function gotoConcentrateur($consultation, $uidPFDouble = false)
    {
        $xmlBoamp = (new Atexo_Publicite_Annonce())->generateXml($consultation);
        $xmlBoamp = (new Atexo_Publicite_Annonce())->addCompteBoampInfoInXml($consultation->getCompteBoampAssocie(), $xmlBoamp, $consultation);
        $this->xmlboamp->Text = base64_encode($xmlBoamp);
        $xmlContext = (new Atexo_Publicite_Annonce())->generateXmlContexte($consultation, $uidPFDouble);
        $this->xmltoken->Text = base64_encode($xmlContext);
        $xmlSignature = self::getSignature($xmlContext);
        $this->xmlsignature->Text = $xmlSignature;
        if (!isset($_GET['debug']) || 1 != $_GET['debug']) {
            $this->script->Text = '<script language="javascript">document.getElementById(\'ctl0_CONTENU_PAGE_buttonSend\').click();</script>';
        }
    }

    public function getSignature($content)
    {
        $sign = base64_encode($content).Atexo_Config::getParameter('KEY');

        return base64_encode(sha1($sign, true));
    }
}
