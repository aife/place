<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Exceptions\THttpException;

/**
 * Class inviteTransverse.
 */
class inviteTransverse extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
            Atexo_Config::getParameter('CONST_READ_ONLY'));

        if (Atexo_CurrentUser::isAgent() && Atexo_CurrentUser::isConnected()) {
            if (!$this->isPostBack) {
                if (isset($_POST['invite'])) {
                    if ('true' == $_POST['list']) {
                        $datas = explode('checked=', $_POST['invite']);
                        foreach ($datas as $data) {
                            if (!empty($data)) {
                                $this->setData($data, $connexion);
                            }
                        }
                    } elseif ('false' == $_POST['list']) {
                        $this->setData($_POST['invite'], $connexion);
                    }
                }

                $list = [
                    'statut' => true,
                ];

                header('Content-type: application/json');
                echo json_encode($list, JSON_THROW_ON_ERROR);
                exit;
            }
        } else {
            throw new THttpException(403, 'Vous n avez pas les droits');
        }
    }

    private function setData($post, $connexion)
    {
        $data = explode('_', base64_decode($post));
        if (3 === count($data)) {
            $commonInvitePermanentTransverseQuery = new CommonInvitePermanentTransverseQuery();
            $commonOrganismeQuery = new CommonOrganismeQuery();
            //$Org = $commonOrganismeQuery->findOneBy('acronyme', $data[0], $connexion);
            $serviceId = null;
            if ('0' != $data[1]) {
                $serviceId = $data[1];
            }
            $tInvitePermanentTransverse = $commonInvitePermanentTransverseQuery->findByArray([
                'serviceId' => $serviceId,
                'agentId' => $data[2],
                'acronyme' => $data[0],
            ]);

            if ((is_countable($tInvitePermanentTransverse) ? count($tInvitePermanentTransverse) : 0) > 0) {
                $tInvitePermanentTransverse->delete();
            } else {
                $commonInvitePermanentTransverse = new CommonInvitePermanentTransverse();
                $commonInvitePermanentTransverse->setAgentId((int) $data[2]);
                $commonInvitePermanentTransverse->setAcronyme($data[0]);
                $commonInvitePermanentTransverse->setServiceId($serviceId);
                $commonInvitePermanentTransverse->save();
            }
        } else {
            throw new THttpException(403, 'Donnees incompletes');
        }
    }
}
