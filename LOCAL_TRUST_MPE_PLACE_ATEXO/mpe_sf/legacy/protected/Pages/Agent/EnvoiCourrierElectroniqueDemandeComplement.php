<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EnvoiCourrierElectroniqueDemandeComplement.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiCourrierElectroniqueDemandeComplement extends MpeTPage
{
    protected $_typeMsg;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectroniqueAgent->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_DEMANDE_COMPLEMENT');
        $this->TemplateEnvoiCourrierElectroniqueAgent->messageType->setEnabled(false);
        if (!$_GET['IdEchange'] && !$this->IsPostBack) {
            $this->TemplateEnvoiCourrierElectroniqueAgent->courrierElectroniqueDeDemandeDeComplement->checked = true;
        }

        $this->TemplateEnvoiCourrierElectroniqueAgent->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.ChangingConsultation&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    public function remplirListeDestinataires()
    {
        $listeDestinataires = '';
        $mailsDestinataire = [];
        $tabMails = [];

        if (Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            $listeDestinataires = Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        }
        if (Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }
        if (Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }

        $mailsDestinataire = explode(' , ', $listeDestinataires);
        if (is_array($mailsDestinataire) && 0 != count($mailsDestinataire)) {
            foreach ($mailsDestinataire as $mail) {
                if (!in_array($mail, $tabMails)) {
                    $tabMails[] = $mail;
                }
            }
        }
        $listeDestinataires = implode(' , ', $tabMails);

        return $listeDestinataires;
    }
}
