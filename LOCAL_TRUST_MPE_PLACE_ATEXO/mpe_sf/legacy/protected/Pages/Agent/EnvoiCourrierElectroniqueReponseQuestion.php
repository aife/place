<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EnvoiCourrierElectroniqueReponseQuestion.
 *
 * @author Khalid Atlassi <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 */
class EnvoiCourrierElectroniqueReponseQuestion extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if (!$this->IsPostBack) {
            if (!$_GET['IdEchange'] && !$this->IsPostBack) {
                $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueUniquementLienTelechargementObligatoire->checked = true;
            }
            $this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_REPONSE_A_QUESTION');
            $this->TemplateEnvoiCourrierElectronique->messageType->setEnabled(false);
            if ($_GET['idQuestion']) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $questionObject = CommonQuestionsDcePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idQuestion']), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if ($questionObject) {
                    $this->TemplateEnvoiCourrierElectronique->destinataires->Text = $questionObject->getEmail();
                }
            }
        }
        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }
}
