<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use AtexoPdf\PdfGeneratorClient;
use DOMDocument;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupDetailChorusFSO extends MpeTPage
{
    /**
     * @return array
     */
    public function getFsoInfos()
    {
        return $this->getViewState('fsoInfos');
    }

    /**
     * @param array $fsoInfos
     */
    public function setFsoInfos($fsoInfos)
    {
        $this->setViewState('fsoInfos', $fsoInfos);
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        ///print_r();exit;
        if (isset($_GET['fso'])) {
            $this->fillDataFso();
        }
    }

    /*
     * Permet de remplir les labels FSO
     */
    public function fillDataFso()
    {
        $nameFso = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['fso']));
        $numEj = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['numMarche']));
        $organisme = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['org']));

        if (is_file(Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE').$nameFso)) {
            $xml = file_get_contents(Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE').$nameFso);
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $compteRendu = $domDocument->getElementsByTagName('FSO0028A')->item(0);
            $engagementsJuridiques = $compteRendu->getElementsByTagName('ENGAGEMENT_JURIDIQUE');
            foreach ($engagementsJuridiques as $ej) {
                $entete = $ej->getElementsByTagName('ENTETE')->item(0);
                $numeroMarche = (string) $entete->getElementsByTagName('ID_EJ')->item(0)->nodeValue;
                if ($numeroMarche == $numEj) {
                    $entete = $ej->getElementsByTagName('ENTETE')->item(0);
                    self::displayFirstPartLabels($entete, $organisme);
                    self::displaySecondPartLabels($entete, $organisme);
                }
            }
        }

        //print_r($this->getFsoInfos());exit;
    }

    /*
     * Permet d'avoir le libelle et les massage de la premiere colonne
     */
    public function displayFirstPartLabels($entete, $organisme)
    {
        $fsoInfos = $this->getFsoInfos();
        if (!is_array($fsoInfos)) {
            $fsoInfos = [];
        }

        $identifiantMarche = self::getValeurIfExiste((string) $entete->getElementsByTagName('ID_EJ')->item(0)->nodeValue);
        $this->msgIdEj->Text = Prado::localize('IDENTIFIANT_EJ_CHORUS');
        $this->ID_EJ->Text = $identifiantMarche;
        $fsoInfos['IDENTIFIANT_EJ_CHORUS'] = $identifiantMarche;

        $numeroLongMarche = self::getValeurIfExiste((string) $entete->getElementsByTagName('ID_LONG')->item(0)->nodeValue);
        $this->msgIdLong->Text = Prado::localize('NUMERO_LONG_EJ_CHORUS');
        $this->ID_LONG->Text = $numeroLongMarche;
        $fsoInfos['NUMERO_LONG_EJ_CHORUS'] = $numeroLongMarche;

        $typeEjValue = (string) $entete->getElementsByTagName('PROCESS_TYPE')->item(0)->nodeValue;
        $this->msgProcessType->Text = Prado::localize('TYPE_DE_L_EJ');
        if ($typeEjValue) {
            $typeEj = (new Atexo_Chorus_Echange())->getLibelleTypeMarcheByCode($typeEjValue);
        } else {
            $typeEj = Prado::localize('NON_RENSEIGNE');
        }
        $this->PROCESS_TYPE->Text = $typeEj;
        $fsoInfos['TYPE_DE_L_EJ'] = $typeEj;

        $dateCreationEj = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('CREATED_AT')->item(0)->nodeValue));
        $this->msgCreatedAt->Text = Prado::localize('DATE_CREATION_EJ');
        $this->CREATED_AT->Text = $dateCreationEj;
        $fsoInfos['DATE_CREATION_EJ'] = $dateCreationEj;

        $approbateur = self::getValeurIfExiste((string) $entete->getElementsByTagName('APPROBATEUR')->item(0)->nodeValue);
        $this->msgApprobateur->Text = Prado::localize('APPROBATEUR_EJ');
        $this->APPROBATEUR->Text = $approbateur;
        $fsoInfos['APPROBATEUR_EJ'] = $approbateur;

        $changedBy = self::getValeurIfExiste((string) $entete->getElementsByTagName('CHANGED_BY')->item(0)->nodeValue);
        $this->msgChangedBy->Text = Prado::localize('DERNIER_EDITEUR_EJ');
        $this->CHANGED_BY->Text = $changedBy;
        $fsoInfos['DERNIER_EDITEUR_EJ'] = $changedBy;

        $changedAt = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('CHANGED_AT')->item(0)->nodeValue));
        $this->msgChangedAt->Text = Prado::localize('DERNIER_DATE_MODIFICATION_EJ');
        $this->CHANGED_AT->Text = $changedAt;
        $fsoInfos['DERNIER_DATE_MODIFICATION_EJ'] = $changedAt;

        $statutEj = self::getValeurIfExiste((string) $entete->getElementsByTagName('STATUT')->item(0)->nodeValue);
        $this->msgStatut->Text = Prado::localize('STATUT_DE_L_EJ');
        $this->STATUT->Text = $statutEj;
        $fsoInfos['STATUT_DE_L_EJ'] = $statutEj;

        $societe = self::getValeurIfExiste((string) $entete->getElementsByTagName('CO_CODE')->item(0)->nodeValue);
        $this->msgCoCode->Text = Prado::localize('SOCIETE_LIE_ORGANISATION_ACHAT');
        $this->CO_CODE->Text = $societe;
        $this->InfoBulle_CO_CODE->Text = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleRegroupementComptableByCode($societe, $organisme));
        //TODO LEZ AJOUT LIBELLE
        $fsoInfos['SOCIETE_LIE_ORGANISATION_ACHAT'] = $societe;

        $devise = self::getValeurIfExiste((string) $entete->getElementsByTagName('CURRENCY')->item(0)->nodeValue);
        $this->msgCurrency->Text = Prado::localize('DEVISE_EJ');
        $this->CURRENCY->Text = $devise;
        $fsoInfos['DEVISE_EJ'] = $devise;

        $montantTotal = Atexo_Util::formaterStringToMontant((string) $entete->getElementsByTagName('TOTAL_VALUE')->item(0)->nodeValue);
        $this->msgTotalValue->Text = Prado::localize('MONTANT_TOTAL_EJ');
        $this->TOTAL_VALUE->Text = $montantTotal;
        $fsoInfos['MONTANT_TOTAL_EJ'] = $montantTotal;

        $conditionPaiement = self::getValeurIfExiste((string) $entete->getElementsByTagName('PMNTTRMS')->item(0)->nodeValue);
        $this->msgPmnttrms->Text = Prado::localize('CONDITION_DE_PAIEMENT');
        $this->PMNTTRMS->Text = $conditionPaiement;
        $this->InfoBulle_PMNTTRMS->Text = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleConditionPaiementByCode($conditionPaiement));
        //TODO LEZ AJOUT LIBELLE
        $fsoInfos['CONDITION_DE_PAIEMENT'] = $conditionPaiement;

        $dateComptable = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('PS_POSTING_DATE')->item(0)->nodeValue));
        $this->msgPsPostingDate->Text = Prado::localize('DATE_COMPTABLE_EJ');
        $this->PS_POSTING_DATE->Text = $dateComptable;
        $fsoInfos['DATE_COMPTABLE_EJ'] = $dateComptable;

        $exerciceComptable = self::getValeurIfExiste((string) $entete->getElementsByTagName('FISC_YEAR')->item(0)->nodeValue);
        $this->msgFiscYear->Text = Prado::localize('EXERCICE_COMPTABLE_EJ');
        $this->FISC_YEAR->Text = $exerciceComptable;
        $fsoInfos['EXERCICE_COMPTABLE_EJ'] = $exerciceComptable;

        $organisationAchats = self::getValeurIfExiste((string) $entete->getElementsByTagName('BE_PUR_ORG')->item(0)->nodeValue);
        $this->msgBePurOrg->Text = Prado::localize('ORGANISATION_ACHATS');
        $this->BE_PUR_ORG->Text = $organisationAchats;
        $this->InfoBulle_BE_PUR_ORG->Text = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleOrganisationAchatByCode($organisationAchats, $organisme));
        //TODO LEZ AJOUT LIBELLE
        $fsoInfos['ORGANISATION_ACHATS'] = $organisationAchats;

        $groupAchteur = self::getValeurIfExiste((string) $entete->getElementsByTagName('BE_PUR_GROUP')->item(0)->nodeValue);
        $this->msgBePurGroup->Text = Prado::localize('GROUPE_ACHTEURS');
        $this->BE_PUR_GROUP->Text = $groupAchteur;
        $this->InfoBulle_BE_PUR_GROUP->Text = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleGroupementAchatByCode($groupAchteur, $organisme));
        //TODO LEZ AJOUT LIBELLE
        $fsoInfos['GROUPE_ACHTEURS'] = $groupAchteur;

        $groupementFr = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleTypeGroupementByCode((string) $entete->getElementsByTagName('PS_PGS_ID')->item(0)->nodeValue, $organisme));
        $this->msgPsPgsId->Text = Prado::localize('STRUCTURE_GROUPEMENT_FOURNISSEUR');
        $this->PS_PGS_ID->Text = $groupementFr;
        $fsoInfos['STRUCTURE_GROUPEMENT_FOURNISSEUR'] = $groupementFr;

        $dateNotif = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('DEBUT_MARCHE')->item(0)->nodeValue));
        $this->msgDebutMarche->Text = Prado::localize('DATE_NOTIFICATION');
        $this->DEBUT_MARCHE->Text = $dateNotif;
        $fsoInfos['DATE_NOTIFICATION'] = $dateNotif;

        $dureeMarche = self::getValeurIfExiste((string) $entete->getElementsByTagName('DUREE_MARCHE')->item(0)->nodeValue);
        $this->msgDureeMarche->Text = Prado::localize('DUREE_MARCHE_EN_MOIS');
        $this->DUREE_MARCHE->Text = $dureeMarche;
        $fsoInfos['DUREE_MARCHE_EN_MOIS'] = $dureeMarche;

        $dateFin = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('FIN_MARCHE')->item(0)->nodeValue));
        $this->msgFinMarche->Text = Prado::localize('TEXT_DATE_FIN_MARCHE');
        $this->FIN_MARCHE->Text = $dateFin;
        $fsoInfos['TEXT_DATE_FIN_MARCHE'] = $dateFin;

        $reconductionAnnuelle = self::isValeurNotNull((string) $entete->getElementsByTagName('RECOND_ANN')->item(0)->nodeValue);
        $this->msgRecondAnn->Text = Prado::localize('RECONDUCTION_ANNUELLE');
        $this->RECOND_ANN->Text = $reconductionAnnuelle;
        $fsoInfos['RECONDUCTION_ANNUELLE'] = $reconductionAnnuelle;

        $dateReconduction = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('RECOND_DATE')->item(0)->nodeValue));
        $this->msgRecondDate->Text = Prado::localize('DATE_DE_RECONDUCTION');
        $this->RECOND_DATE->Text = $dateReconduction;
        $fsoInfos['DATE_DE_RECONDUCTION'] = $dateReconduction;

        $penaliteR = self::isValeurNotNull((string) $entete->getElementsByTagName('PENAL_RETARD')->item(0)->nodeValue);
        $this->msgPenalRetard->Text = Prado::localize('PENALITES_DE_RETARD');
        $this->PENAL_RETARD->Text = $penaliteR;
        $fsoInfos['PENALITES_DE_RETARD'] = $penaliteR;

        $this->setFsoInfos($fsoInfos);
    }

    /*
     * Permet d'avoir le libelle et les massage de la deuxième colonne
     */
    public function displaySecondPartLabels($entete, $organisme)
    {
        $fsoInfos = $this->getFsoInfos();
        if (!is_array($fsoInfos)) {
            $fsoInfos = [];
        }

        $debutExec = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('DEBUT_EXEC')->item(0)->nodeValue));
        $this->msgDebutExec->Text = Prado::localize('DEBUT_PRESTATION_TRANCHE');
        $this->DEBUT_EXEC->Text = $debutExec;
        $fsoInfos['DEBUT_PRESTATION_TRANCHE'] = $debutExec;

        $delaiPrest = self::getValeurIfExiste((string) $entete->getElementsByTagName('DELAI_EXEC')->item(0)->nodeValue);
        $this->msgDelaiExec->Text = Prado::localize('DELAI_PRESTATION_EN_MOIS');
        $this->DELAI_EXEC->Text = $delaiPrest;
        $fsoInfos['DELAI_PRESTATION_EN_MOIS'] = $delaiPrest;

        $FinExec = self::getValeurIfExiste(Atexo_Util::formaterStringToDate((string) $entete->getElementsByTagName('FIN_EXEC')->item(0)->nodeValue));
        $this->msgFinExec->Text = Prado::localize('FIN_PRESTATION_TRANCHE');
        $this->FIN_EXEC->Text = $FinExec;
        $fsoInfos['FIN_PRESTATION_TRANCHE'] = $FinExec;

        $siretAchat = self::getValeurIfExiste((string) $entete->getElementsByTagName('SIRET_ACHAT')->item(0)->nodeValue);
        $this->msgSiretAchat->Text = Prado::localize('SIRET_ACHETEUR');
        $this->SIRET_ACHAT->Text = $siretAchat;
        $this->InfoBulle_SIRET_ACHAT->Text = self::getValeurIfExiste((new Atexo_EntityPurchase())->getLibelleServiceBySiret($siretAchat, $organisme));
        // TODO LEZ INFO BULLE
        $fsoInfos['SIRET_ACHETEUR'] = $siretAchat;

        $typeMarche = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleActeJuridiqueByCode((string) $entete->getElementsByTagName('TYPE_MARCHE')->item(0)->nodeValue));
        $this->msgTypeMarche->Text = Prado::localize('DEFINE_TYPE_MARCHE');
        $this->TYPE_MARCHE->Text = $typeMarche;
        $fsoInfos['DEFINE_TYPE_MARCHE'] = $typeMarche;

        $procedure = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleTypeProcedureByCode((string) $entete->getElementsByTagName('PROCEDURE')->item(0)->nodeValue));
        $this->msgProcedure->Text = Prado::localize('DEFINE_PROCEDURE');
        $this->PROCEDURE->Text = $procedure;
        $fsoInfos['DEFINE_PROCEDURE'] = $procedure;

        $ccag = self::getMessageCCAGByCode((string) $entete->getElementsByTagName('CCAG')->item(0)->nodeValue);
        $this->msgCcag->Text = Prado::localize('DEFINE_CCAG');
        $this->CCAG->Text = $ccag;
        $fsoInfos['DEFINE_CCAG'] = $ccag;

        $refAccCadre = self::getValeurIfExiste((string) $entete->getElementsByTagName('REF_ACC_CADRE')->item(0)->nodeValue);
        $this->msgRefAccCadre->Text = Prado::localize('REF_ACC_CADRE');
        $this->REF_ACC_CADRE->Text = $refAccCadre;
        $fsoInfos['REF_ACC_CADRE'] = $refAccCadre;

        $nbEntCo = self::getValeurIfExiste((string) $entete->getElementsByTagName('NB_ENTR_CO')->item(0)->nodeValue);
        $this->msgNbEntrCo->Text = Prado::localize('NOMBRE_ENTREPRISES_COTRAITANTES');
        $this->NB_ENTR_CO->Text = $nbEntCo;
        $fsoInfos['NOMBRE_ENTREPRISES_COTRAITANTES'] = $nbEntCo;

        $carteAchat = self::isValeurNotNull((string) $entete->getElementsByTagName('CARTE_ACHAT')->item(0)->nodeValue);
        $this->msgCarteAchat->Text = Prado::localize('UTILISATION_CARTE_ACHAT');
        $this->CARTE_ACHAT->Text = $carteAchat;
        $fsoInfos['UTILISATION_CARTE_ACHAT'] = $carteAchat;

        $clauseSoc = self::isValeurNotNull((string) $entete->getElementsByTagName('CLAUSE_SOCIALE')->item(0)->nodeValue);
        $this->msgClauseSociale->Text = Prado::localize('TEXT_CLAUSE_SOCIALE');
        $this->CLAUSE_SOCIALE->Text = $clauseSoc;
        $fsoInfos['TEXT_CLAUSE_SOCIALE'] = $clauseSoc;

        $nbPropR = self::getValeurIfExiste((string) $entete->getElementsByTagName('NB_PROP_RECUES')->item(0)->nodeValue);
        $this->msgNbPropRecues->Text = Prado::localize('NOMBRE_PROPOSITIONS_RECUES');
        $this->NB_PROP_RECUES->Text = $nbPropR;
        $fsoInfos['NOMBRE_PROPOSITIONS_RECUES'] = $nbPropR;

        $nbPropDem = self::getValeurIfExiste((string) $entete->getElementsByTagName('NB_PROP_DEMAT')->item(0)->nodeValue);
        $this->msgNBPropDemat->Text = Prado::localize('NOMBRE_PROPOSITIONS_DEMATERIALISEES');
        $this->NB_PROP_DEMAT->Text = $nbPropDem;
        $fsoInfos['NOMBRE_PROPOSITIONS_DEMATERIALISEES'] = $nbPropDem;

        $clauseEnv = self::isValeurNotNull((string) $entete->getElementsByTagName('CLAUSE_ENV')->item(0)->nodeValue);
        $this->msgClauseEnv->Text = Prado::localize('TEXT_CLAUSE_ENVIRONNEMENTALE');
        $this->CLAUSE_ENV->Text = $clauseEnv;
        $fsoInfos['TEXT_CLAUSE_ENVIRONNEMENTALE'] = $clauseEnv;

        $formePrix = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleFormePrixByCode((string) $entete->getElementsByTagName('FORME_PRIX')->item(0)->nodeValue));
        $this->msgFormePrix->Text = Prado::localize('TEXT_FORME_DE_PRIX');
        $this->FORME_PRIX->Text = $formePrix;
        $fsoInfos['TEXT_FORME_DE_PRIX'] = $formePrix;

        $vzskz = self::getValeurIfExiste((new Atexo_Chorus_Echange())->getLibelleCodeInteretByCode((string) $entete->getElementsByTagName('VZSKZ')->item(0)->nodeValue));
        $this->msgVzskz->Text = Prado::localize('CODE_CALCUL_INTERETS');
        $this->VZSKZ->Text = $vzskz;
        $fsoInfos['CODE_CALCUL_INTERETS'] = $vzskz;

        $this->setFsoInfos($fsoInfos);

        self::displayCodCpv($entete);
    }

    /*
     * Permet d'avoir le message correspond à le code CCAG
     */
    public function getMessageCCAGByCode($code)
    {
        $message = match ($code) {
            '000' => Prado::localize('DEFINE_AUCUN_CCAG_VISE'),
            '001' => Prado::localize('DEFINE_CCAG_FOURNITURES_COURANTES_PRESTATIONS_SERVICES'),
            '002' => Prado::localize('DEFINE_CCAG_PRESTATIONS_INTELLECTUELLES'),
            '003' => Prado::localize('DEFINE_CCAG_TRAVAUX_MARCHES_INDUSTRIELS'),
            '004' => Prado::localize('DEFINE_CCAG_TECHNIQUES_INFORMATION_COMMUNICATION'),
            '005' => Prado::localize('DEFINE_CCAG_MARCHES_PUBLICS_INDUSTRIELS'),
            '006' => Prado::localize('DEFINE_CCAG_TRAVAUX'),
            default => Prado::localize('NON_RENSEIGNE'),
        };
        return $message;
    }

    /*
     * permet de display les code cpv
     */
    public function displayCodCpv($entete)
    {
        $fsoInfos = $this->getFsoInfos();
        if (!is_array($fsoInfos)) {
            $fsoInfos = [];
        }
        //TODO LEZ INFO BULLE

        // la langue
        $lang = Atexo_CurrentUser::readFromSession('lang');
        $dataRefFile = (new Atexo_Ref())->getDataFile(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'), $lang);
        $codeCpvP = (string) $entete->getElementsByTagName('CPV_PRINC')->item(0)->nodeValue;
        $this->msgCpvPrinc->Text = Prado::localize('CODE_CPV_PRINCIPAL');
        if ($codeCpvP) {
            $code = explode('-', $codeCpvP);
            $this->InfoBulle_CPV_PRINC->Text = (new Atexo_Ref())->getLibelleRef($code[0], $dataRefFile);
            $this->CPV_PRINC->Text = $codeCpvP;
            $fsoInfos['CODE_CPV_PRINCIPAL'] = $codeCpvP;
        } else {
            $this->CPV_PRINC->Text = Prado::localize('NON_RENSEIGNE');
            $this->InfoBulle_CPV_PRINC->Text = Prado::localize('NON_RENSEIGNE');
            $fsoInfos['CODE_CPV_PRINCIPAL'] = Prado::localize('NON_RENSEIGNE');
        }
        $codeCpvS1 = (string) $entete->getElementsByTagName('CPV_SECOND1')->item(0)->nodeValue;
        $this->msgCpvSecond1->Text = Prado::localize('CODE_CPV_SECONDAIRE_1');
        if ($codeCpvS1) {
            $code = explode('-', $codeCpvS1);
            $this->InfoBulle_CPV_SECOND1->Text = (new Atexo_Ref())->getLibelleRef($code[0], $dataRefFile);
            $this->CPV_SECOND1->Text = $codeCpvS1;
            $fsoInfos['CODE_CPV_SECONDAIRE_1'] = $codeCpvS1;
        } else {
            $this->CPV_SECOND1->Text = Prado::localize('NON_RENSEIGNE');
            $this->InfoBulle_CPV_SECOND1->Text = Prado::localize('NON_RENSEIGNE');
            $fsoInfos['CODE_CPV_SECONDAIRE_1'] = Prado::localize('NON_RENSEIGNE');
        }
        $codeCpvS2 = (string) $entete->getElementsByTagName('CPV_SECOND2')->item(0)->nodeValue;
        $this->msgCpvSecond2->Text = Prado::localize('CODE_CPV_SECONDAIRE_2');
        if ($codeCpvS2) {
            $code = explode('-', $codeCpvS2);
            $this->InfoBulle_CPV_SECOND2->Text = (new Atexo_Ref())->getLibelleRef($code[0], $dataRefFile);
            $this->CPV_SECOND2->Text = $codeCpvS2;
            $fsoInfos['CODE_CPV_SECONDAIRE_2'] = $codeCpvS2;
        } else {
            $this->CPV_SECOND2->Text = Prado::localize('NON_RENSEIGNE');
            $this->InfoBulle_CPV_SECOND2->Text = Prado::localize('NON_RENSEIGNE');
            $fsoInfos['CODE_CPV_SECONDAIRE_2'] = Prado::localize('NON_RENSEIGNE');
        }
        $codeCpvS3 = (string) $entete->getElementsByTagName('CPV_SECOND3')->item(0)->nodeValue;
        $this->msgCpvSecond3->Text = Prado::localize('CODE_CPV_SECONDAIRE_3');
        if ($codeCpvS3) {
            $code = explode('-', $codeCpvS3);
            $this->InfoBulle_CPV_SECOND3->Text = (new Atexo_Ref())->getLibelleRef($code[0], $dataRefFile);
            $this->CPV_SECOND3->Text = $codeCpvS3;
            $fsoInfos['CODE_CPV_SECONDAIRE_3'] = $codeCpvS3;
        } else {
            $this->CPV_SECOND3->Text = Prado::localize('NON_RENSEIGNE');
            $this->InfoBulle_CPV_SECOND3->Text = Prado::localize('NON_RENSEIGNE');
            $fsoInfos['CODE_CPV_SECONDAIRE_3'] = Prado::localize('NON_RENSEIGNE');
        }
        $this->setFsoInfos($fsoInfos);
    }

    /*
     * permet de retourner la valeur si non null si oui il retourn 'non renseigné'
     */
    public function getValeurIfExiste($valeur)
    {
        if ($valeur) {
            return $valeur;
        } else {
            return Prado::localize('NON_RENSEIGNE');
        }
    }

    /*
     * permet de retourner Oui si la valeur si non null si oui il retourn Non
     */
    public function isValeurNotNull($valeur)
    {
        if ($valeur) {
            return Prado::localize('DEFINE_OUI');
        } else {
            return Prado::localize('DEFINE_NON');
        }
    }

    public function printFSOPdf()
    {
        $fsoInfos = $this->getFsoInfos();
        if (is_array($fsoInfos) && count($fsoInfos)) {
            $fsoInfos['DATE_EDITION'] = date('d/m/Y');
            $odtFilePath = $this->generateFsoOdt($fsoInfos);
            //echo $odtFilePath;exit;
            if ($odtFilePath) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
                @unlink($odtFilePath);
                DownloadFile::downloadFileContent('EJ Chorus n°'.$fsoInfos['IDENTIFIANT_EJ_CHORUS'].' - '.date('Ymd His').'.pdf', $pdfContent);
            }
        }
    }

    public function generateFsoOdt($fsoInfos)
    {
        $fileModel = Atexo_Config::getParameter('CHEMIN_MODELE_EJ_CHORUS_FSO');
        if (is_file($fileModel)) {
            if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
            }
            $config = [
                    'ZIP_PROXY' => \PhpZipProxy::class,
                    'DELIMITER_LEFT' => '{',
                    'DELIMITER_RIGHT' => '}',
                    'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
            ];
            $odf = new Atexo_Odf($fileModel, $config);
            foreach ($fsoInfos as $key => $value) {
                $odf->setVars($key, Atexo_Util::atexoHtmlEntitiesDecode($value), true, Atexo_Config::getParameter('HTTP_ENCODING'));
            }

            $pathFile = Atexo_Config::getParameter('COMMON_TMP').'EJ_Chorus_N_'.session_id().time().'_fichier.odt';
            $odf->saveToDisk($pathFile);

            return $pathFile;
        }

        return false;
    }
}
