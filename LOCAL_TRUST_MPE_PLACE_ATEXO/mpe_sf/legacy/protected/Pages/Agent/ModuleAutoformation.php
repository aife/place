<?php

namespace Application\Pages\Agent;

use App\Service\Autoformation\ModuleAutoformation as ModuleAutoformationService;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

class ModuleAutoformation extends MpeTPage
{
    /**
     * @var string
     */
    private $access;

    public function onInit($param)
    {

        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        MessageStatusCheckerUtil::initEntryPoints('module-autoformation');
    }

    public function onLoad($param){
        if(Atexo_CurrentUser::hasHabilitation('ModuleAutoformation') && $_GET['access']) {
            $this->access = $_GET['access'];
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT')."?page=Agent.AgentHome");
        }
    }

    /**
     * @return string
     */
    public function getAccess(): string
    {
        return $this->access;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        /**
         * @var ModuleAutoformationService $moduleAutoformation
         */
        $moduleAutoformation = Atexo_Util::getSfService(ModuleAutoformationService::class);

        return $moduleAutoformation->getBody($this->access);
    }
}