<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

/**
 * permet de gerer les recherches favorites sauvegardées par un agent.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class AgentGestionRecherches extends MpeTPage
{
    /**
     * permet d'initialiser la page.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * permet de charger la page.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $this->rechercheFavoritesEntreprise->IdCreateur = Atexo_CurrentUser::getIdAgentConnected();
        $this->rechercheFavoritesEntreprise->TypeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_AGENT');
        $this->rechercheFavoritesEntreprise->setTypeAvisConsultation('all');

        $this->rechercheFavoritesEntreprise->setCallFrom('agent');
        $this->rechercheFavoritesEntreprise->initConposant();
    }
}
