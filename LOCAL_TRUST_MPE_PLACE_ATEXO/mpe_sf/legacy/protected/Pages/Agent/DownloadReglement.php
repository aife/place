<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;

/**
 * Permet de télécharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadReglement extends DownloadFile
{
    private $_reference;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $rg = '';
        $idSeviceAgent = Atexo_CurrentUser::getCurrentServiceId();
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_reference, Atexo_CurrentUser::getCurrentOrganism());

        /**
         * Seul les agents ayant le droit de voir la consultation qui peuvent télécharger le Reglement.
         */
        $consultation = new Atexo_Consultation();
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($this->_reference);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService($idSeviceAgent);
        $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationFound = $consultation->search($consultationCriteria);

        if (count($consultationFound) > 0) {
            if ($rg) {
                $this->_idFichier = $rg->getRg();
                $this->_nomFichier = $rg->getNomFichier();
            }
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getCurrentOrganism());
        } else {
            $this->response->redirect('agent');
        }
    }
}
