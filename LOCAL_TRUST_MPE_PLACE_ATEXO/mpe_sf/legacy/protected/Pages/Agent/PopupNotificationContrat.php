<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_Gestion;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupNotificationContrat extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {

        $this->accessContract();

        $this->warningSiret->setVisible(false);
        if (!Atexo_Util::hasSiretCheckeSiretCentralise()) {
            $this->warningSiret->setVisible(true);
            $this->buttonSave->setVisible(false);
            $this->buttonSaveWithDE->setVisible(false);
        }

        //DONNEES_ESSENTIELLES_SUIVI_SN
        if (1 == Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn')) {
            $this->buttonSave->Visible = false;
        } else {
            $this->buttonSaveWithDE->Visible = false;
        }
        if (!$this->getIsPostBack()) {
            $messageErreur = null;
            $this->panelMessageErreur->setVisible(false);
            $logger = Atexo_LoggerManager::getLogger('contrat');

            if (isset($_GET['idContrat'])) {
                $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                $lots = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots']));
                $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
                $this->recapConsultation->initialiserComposant($consultationId, $lots, $idContrat);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);

                if ($contrat instanceof CommonTContratTitulaire) {
                    if ($contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
                        $this->idCartoucheAttributaire->setContrat($contrat);
                        $this->datePrevisionnelleNotification->Text = Atexo_Util::iso2frnDate($contrat->getDatePrevueNotification());
                        $this->datePrevisionnelleFinMarche->Text = Atexo_Util::iso2frnDate($contrat->getDatePrevueFinContrat());
                        $this->datePrevisionnelleFinMaxMarche->Text = Atexo_Util::iso2frnDate($contrat->getDatePrevueMaxFinContrat());
                        if ($contrat->isTypeContratConcession()) {
                            $this->panelDateExecution->setVisible(true);
                            $this->dateExecution->Text = Atexo_Util::iso2frnDate($contrat->getDateDebutExecution());
                        } else {
                            $this->panelDateExecution->setVisible(false);
                        }

                        $this->initaliserDatesDefinitives($contrat);
                        $this->dureeMarche->Text = $contrat->getDureeInitialeContrat();
                    } else {
                        $logger->error("Le contrat dont l'id = ".$idContrat." n'as pas le statut notification contrat");
                        $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                    }
                } else {
                    $logger->error("Le contrat dont l'id = ".$idContrat." n'existe pas");
                    $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                }
            } else {
                $logger->error("Erreur d'accés à la PopupNotificationContrat sans le parametre idContrat");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }

            if ($messageErreur) {
                $this->afficherMessageErreur($messageErreur);
            }
        }
    }

    public function setIdContrat($value)
    {
        $this->setViewState('idContrat', $value);
    }

    public function getIdContrat()
    {
        $this->getViewState('idContrat');
    }

    /**
     * Permet de sauvegarde l'attribution.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function save($sender, $param)
    {
        try {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);

            if ($contrat instanceof CommonTContratTitulaire) {
                $contrat->setDateNotification(Atexo_Util::frnDate2iso($this->dateDefinitiveNotification->Text));
                $contrat->setDatePublicationInitialeDe(Atexo_Util::frnDate2iso($this->dateDefinitiveNotification->Text));
                $gestionContrat = new Atexo_Contrat_Gestion();
                $gestionContrat->changeTrancheBudgetaire($contrat);
                $contrat->setDateFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMarche->Text));
                $contrat->setDateMaxFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMaxMarche->Text));
                $contrat->setDureeInitialeContrat($this->dureeMarche->Text);
                $contrat->setStatutContrat(Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
                $contrat->setDateModification(date('Y-m-d H:i:s'));
                if ($contrat->isTypeContratConcession()) {
                    $contrat->setDateDebutExecution(Atexo_Util::frnDate2iso($this->dateExecution->getText()));
                }
                $contratMulti = $contrat->getCommonTContratMulti();

                if ($contratMulti instanceof CommonTContratMulti) {
                    $contratMulti->setDateModification(date('Y-m-d H:i:s'));
                    $contratMulti->save($connexion);
                }

                $millesime = $contrat->getDateNotification('Y');
                $numeroContrat = $contrat->getNumeroContrat();

                //dans le cas ou le numéro de contrat est NULL cf MPE-5991
                if (null == $numeroContrat || '' == $numeroContrat) {
                    $numeroContrat = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contrat, $connexion);
                    $contrat->setNumeroContrat($numeroContrat);
                }
                //dans le cas ou le numéro long oeap est NULL cf MPE-5991
                $numeroLongOeap = $contrat->getNumLongOeap();
                if (null == $numeroLongOeap || '' == $numeroLongOeap) {
                    $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($contrat, $connexion);
                    $contrat->setNumLongOeap($numeroLong.'01');
                }

                $numIdUnique = $millesime.$numeroContrat.'00';
                if (Atexo_Module::isEnabled('NumDonneesEssentiellesManuel')) {
                    $contrat->setNumIdUniqueMarchePublic($contrat->getReferenceLibre());
                } else {
                    $contrat->setNumIdUniqueMarchePublic($numIdUnique);
                }

                $contrat->save($connexion);
                $this->labelScript->Text = "<script type='text/javascript'>window.opener.document.getElementById('ctl0_CONTENU_PAGE_refresh').click();window.close();</script>";
            }
        } catch (\Exception $e) {
            Prado::log('Erreur de notification du contrat : '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PopupNotificationContrat.php');
        }
    }

    /**
     * Permet d'initialiser les dates definitives.
     *
     * @param $contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initaliserDatesDefinitives($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->dateDefinitiveNotification->Text = $contrat->getDatePrevueNotification() ? Atexo_Util::iso2frnDate($contrat->getDatePrevueNotification()) : '';
            $this->dateDefinitiveFinMarche->Text = $contrat->getDatePrevueFinContrat() ? Atexo_Util::iso2frnDate($contrat->getDatePrevueFinContrat()) : '';
            $this->dateDefinitiveFinMaxMarche->Text = $contrat->getDatePrevueMaxFinContrat() ? Atexo_Util::iso2frnDate($contrat->getDatePrevueMaxFinContrat()) : '';
        }
    }

    /*
    * Permet d'afficher le message d'erreur et cacher les autres composants
    *
    * @param string $messageErreur le message d'erreur
    * @return void
    * @author loubna EZZIANI <loubna.ezziani@atexo.com>
    * @version 1.0
    * @since 4.11.0
    * @copyright Atexo 2015
    */
    public function afficherMessageErreur($messageErreur)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageErreur);
        //$this->messageConfirmationEnregistrement->setVisible(false);
        $this->panelPopupNotificationContrat->setVisible(false);
    }

    public function validationNotificationPopin($sender, $param)
    {
        $this->script->Text = "<script>openModal('demander-validation','popup-small2',document.getElementById('panelValidationNt'), 'Demande de validation');</script>";
    }


    private function accessContract() {
        $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setFindOne(true);
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setIdAgentConnecte(Atexo_CurrentUser::getId());
        $criteriaVo->setIdContratTitulaire($idContrat);
        $propelConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contract = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo, $propelConnexion);

        if (!$contract instanceof CommonTContratTitulaire) {
            if ($this->accessContratForDecision()) {
                return $this->getResponse()->redirect('/agent/');
            }
        }
    }

    private function accessContratForDecision()
    {
        $noAccess = true;
        $consultationId = base64_decode($_GET['id']);
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService($idService);
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if ('decision' === $_GET['action']
            && count($consultation) > 0
            && $consultation[0] instanceof CommonConsultation) {
            $noAccess = false;
        }

        return $noAccess;
    }
}
