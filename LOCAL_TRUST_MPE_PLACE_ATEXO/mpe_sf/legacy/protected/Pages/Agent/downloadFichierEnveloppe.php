<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class downloadFichierEnveloppe extends MpeTPage
{
    public ?string $_typeSignature = null;

    public function onLoad($param)
    {
        if (isset($_GET['id']) && isset($_GET['idfichier']) && isset($_GET['enveloppe'])) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById(Atexo_Util::atexoHtmlEntities($_GET['enveloppe']), Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
                if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $organisme = Atexo_CurrentUser::getCurrentOrganism();
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                    if ($enveloppe instanceof CommonEnveloppe) {
                        $offre = CommonOffresPeer::retrieveByPK($enveloppe->getOffreId(), $organisme, $connexionCom);
                        if ($offre instanceof CommonOffres) {
                            $xmlStr = $offre->getXmlString();
                            $pos = strpos($xmlStr, 'typeSignature');
                            if ($pos > 0) {
                                $this->_typeSignature = 'XML';
                            }
                        }
                    }

                    $c = new Criteria();
                    $c->add(CommonFichierEnveloppePeer::ID_FICHIER, Atexo_Util::atexoHtmlEntities($_GET['idfichier']));
                    $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                    $fichier = CommonFichierEnveloppePeer::doSelectOne($c, $connexionCom);
                    if ($fichier instanceof CommonFichierEnveloppe && $fichier->getIdBlob()) {
                        $fileName = $fichier->getNomFichier();
                        //$signature = $fichier->getSignatureFichier();
                        //$infosFile = pathinfo($fileName);
                        (new DownloadFile())->downloadFiles($fichier->getIdBlob(), $fileName, $organisme);
                    }
                }
            }
        }
    }
}
