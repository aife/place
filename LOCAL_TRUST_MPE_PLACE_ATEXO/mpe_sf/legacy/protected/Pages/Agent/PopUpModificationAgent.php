<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpModificationAgent extends MpeTPage
{
    private $idAgent;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
        if (!$this->isPostBack) {
            $agentConnectedO = new CommonAgent();
            $agentConnectedO->setId(Atexo_CurrentUser::getIdAgentConnected());
            $agentConnectedO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $agentConnectedO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
            $agentToUpO = (new CommonAgentQuery())->findOneById($this->idAgent);
            if (!(new Atexo_Agent())->verifyCapacityToModify($agentConnectedO, $agentToUpO) && !Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $this->panelMessage->setVisible('true');
                $this->panelMessage->setMessage(Prado::localize('TEXT_PAS_AUTORISE_MODIFIER_INFORMATIONS_AGENT'));
                $this->panelCompteAgent->visible = false;
            } else {
                $this->AtexoReferentielAgent->afficherReferentielAgent($this->idAgent, $agentToUpO->getOrganisme());
                $this->panelMessage->setVisible('false');
                if (0 == $_GET['all']) {
                    $this->listeEntitesAchat->visible = true;
                    $this->entiteAchat->visible = false;
                    $this->getListeEntitesAchat();
                } else {
                    $this->listeEntitesAchat->visible = false;
                    $this->entiteAchat->visible = true;
                }
                $this->getDataAgent();
            }
        }
    }

    /**
     * retourne les informations relatives à l'agent.
     */
    public function getDataAgent()
    {
        $agent = (new CommonAgentQuery())->findOneById($this->idAgent);
        if ($agent) {
            $this->nomAgent->Text = $agent->getNom();
            $this->prenomAgent->Text = $agent->getPrenom();
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $this->entiteAchat->Text = (new Atexo_EntityPurchase())->getEntityPathById($agent->getServiceId(), $agent->getOrganisme(), true, true);
            } else {
                $this->entiteAchat->Text = (new Atexo_EntityPurchase())->getEntityPathById($agent->getServiceId(), $agent->getOrganisme(), true);
            }
            if (0 == $_GET['all']) {
                $this->listeEntitesAchat->SelectedValue = $agent->getServiceId();
            }
            $this->telOrga->Text = $agent->getNumTel();
            $this->faxOrga->Text = $agent->getNumFax();
            $this->email->Text = $agent->getEmail();
            $this->setViewState('email', $agent->getEmail());
            if ('1' == $agent->getElu()) {
                $this->agent->checked = false;
                $this->elu->checked = true;
            } else {
                $this->agent->checked = true;
                $this->elu->checked = false;
            }
        }
    }

    /**
     * met à jour les informations de l'agent.
     */
    public function updateAgentBasicInformation($sender, $param)
    {
        $idService = ($this->listeEntitesAchat->SelectedValue == 0) ? null : $this->listeEntitesAchat->SelectedValue;
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $agent = CommonAgentPeer::retrieveByPK($_GET['id'], $connexionCom);
            $agent->setNom($this->nomAgent->Text);
            $agent->setPrenom($this->prenomAgent->Text);
            $agent->setNumTel($this->telOrga->Text);
            $agent->setNumFax($this->faxOrga->Text);
            $agent->setEmail(trim($this->email->Text));
            $agent->setDateModification(date('Y-m-d H:i:s'));
            if (0 == $_GET['all']) {
                $agent->setServiceId($idService);
            }
            if ($this->agent->checked) {
                $agent->setElu('0');
            } else {
                $agent->setElu('1');
            }
            $agent->save($connexionCom);
            $this->AtexoReferentielAgent->saveReferentielAgent($agent->getId(), $agent->getOrganisme());
            if ($agent->getAlerteCreationModificationAgent()) {
                $message = Atexo_Message::getMessageModificationCompte($agent);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $this->email->Text, Prado::localize('DEFINE_TEXT_OBJET_CREATION_MODIFICATION_MON_COMPTE'), $message, '', '', false, true);
            }

            $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }

    /**
     * Verifier que l'adresse mail est unique.
     */
    public function verifyMailModification($sender, $param): void
    {
        $scriptText = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";

        $email = $this->email->getText();
        if ($email) {
            if (false == Atexo_Util::checkEmailFormat($email)) {
                $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('MSG_ERROR_FORMAT_EMAIL').' : '.$email;
                $this->script->Text = $scriptText;
                $param->IsValid = false;

                return;
            }
            if ($this->getViewState('email') != $this->email->getText()
                && Atexo_Module::isEnabled('UniciteMailAgent')
                && (new Atexo_Agent())->retrieveAgentByMail($email)
            ) {
                $this->script->Text = $scriptText;
                $param->IsValid = false;
                $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('TEXT_EMAIL_EXISTE_DEJA', ['adresse mail' => $email]);

                return;
            }
        }
    }

    public function getListeEntitesAchat()
    {
        // Si l'agent est affecté à un service onb affiche tout les services en doussous de son pôle
        if (Atexo_CurrentUser::getIdServiceAgentConnected()) {
            $myEntity = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
            $listeChilds = Atexo_Util::arrayUnshiftWithKey(Atexo_EntityPurchase::getSubServices(
                Atexo_CurrentUser::getIdServiceAgentConnected(),
                Atexo_CurrentUser::getCurrentOrganism()
            ), $myEntity->getId(), $myEntity->getSigle());
        } else {
            // Si l'agent n'est pas affecté à un service on affiche tout les services
            $listeChilds = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        }
        if (is_array($listeChilds)) {
            $this->listeEntitesAchat->DataSource = $listeChilds;
            $this->listeEntitesAchat->DataBind();
        }
    }
}
