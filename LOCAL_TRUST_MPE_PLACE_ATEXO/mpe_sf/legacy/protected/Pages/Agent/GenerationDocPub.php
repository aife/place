<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

/**
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 4
 */
class GenerationDocPub extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if ($consultation instanceof CommonConsultation) {
            $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_Util::atexoHtmlEntities($_GET['organisme']), Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($avisPub instanceof CommonAvisPub) {
                if (!isset($_GET['fileRtf'])) {
                    $contentFile = (new Atexo_Publicite_AvisPub())->getFileContent($avisPub, $consultation);
                    Atexo_Util::telechargerFichierNonStocke($contentFile, 'Avis.pdf');
                }
            }
        }
    }
}
