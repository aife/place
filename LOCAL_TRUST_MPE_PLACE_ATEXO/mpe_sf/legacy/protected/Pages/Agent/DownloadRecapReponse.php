<?php

namespace Application\Pages\Agent;

use App\Service\Authorization\AuthorizationAgent;
use App\Utils\Encryption;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;


use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 21/12/2015
 * Time: 17:51.
 */
class DownloadRecapReponse extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $agentAuth = Atexo_Util::getSfService(AuthorizationAgent::class);
            $encryption = Atexo_Util::getSfService(Encryption::class);
            if (Atexo_Util::atexoHtmlEntities($_GET['idFile'])) {
                $this->_idFichier = $encryption->decryptId(Atexo_Util::atexoHtmlEntities($_GET['idFile']));
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $blobFile = CommonBlobOrganismeFileQuery::create()->filterById($this->_idFichier)
                    ->filterByOrganisme($organisme)->findOne();
                $offre = CommonOffresQuery::create()
                    ->filterByIdPdfEchangeAccuse($this->_idFichier)
                    ->findOne();
                if (!$agentAuth->isInTheScope($offre->getConsultationId())) {
                    throw new AccessDeniedHttpException('Unauthorized to download this file.');
                }
                if ($blobFile instanceof CommonBlobOrganismeFile) {
                    $this->_nomFichier = $blobFile->getName();
                    $this->downloadFiles($this->_idFichier, $this->_nomFichier, $organisme);
                }
            }

            if (Atexo_Util::atexoHtmlEntities($_GET['idOffre'])) {
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $idOffre =  $encryption->decryptId(Atexo_Util::atexoHtmlEntities($_GET['idOffre']));
                $offre = CommonOffresQuery::create()->filterById($idOffre)->filterByOrganisme($organisme)->findOne();
                if ($offre instanceof CommonOffres) {
                    $this->_idFichier = $offre->getIdPdfEchangeAccuse();
                    $blobFile = CommonBlobOrganismeFileQuery::create()->filterById($this->_idFichier)
                        ->filterByOrganisme($organisme)->findOne();
                    if (!$agentAuth->isInTheScope($offre->getConsultationId())) {
                        throw new AccessDeniedHttpException('Unauthorized to download this file.');
                    }
                    if (
                        $blobFile instanceof CommonBlobOrganismeFile) {
                        $this->_nomFichier = $blobFile->getName();
                        $this->downloadFiles($this->_idFichier, $this->_nomFichier, $organisme);
                    }
                }
            }

            return false;
        } catch (Exception $e) {
            Prado::log(
                'Erreur lors du telechargenemt du fichier Recap Mail Reponse entreprise' . $e->getTraceAsString(),
                TLogger::ERROR,
                'Atexo.pages.entreprise.EntrepriseDownloadRecapReponse.php'
            );
        }
    }


}
