<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;
use Prado\Prado;

/**
 * permet de choisir la liste des archives a telecharger, archive de la consultation et les differentes archives des lots
 * cette fonctionnalité est geré par le module "ArchiveParLot".
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class PopUpChoixArchive extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            if (Atexo_Module::isEnabled('ArchiveParLot')) {
                self::fillRepeaterListeDocLot(base64_decode($_GET['id']));
            }
        }
    }

    /**
     * permet de remplir la liste des archives.
     *
     * @param string $consultationId la reference de la Consultation
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillRepeaterListeDocLot($consultationId)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId);
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getAlloti()) {
                $listeLots = $consultation->getAllLots();
                $this->repeaterListeDocLot->DataSource = $listeLots;
                $this->repeaterListeDocLot->dataBind();
            }
        }
    }

    /**
     * permet de recuperer la liste des archives a telecharger et redirige vers la page de telechargement.
     *
     * @param void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function downloadeSelectedArchives()
    {
        $dac = null;
        $listeSelectedFiles = [];
        if (true == $this->dicCons->checked) {
            $listeSelectedFiles[] = 0;
        }
        foreach ($this->repeaterListeDocLot->getItems() as $item) {
            if (true == $item->dilCheck->checked) {
                $listeSelectedFiles[] = $item->numLot->Value;
            }
        }
        if (count($listeSelectedFiles)) {
            $this->panelMessageErreur->setVisible(false);
            $listLots = base64_encode(implode('#', $listeSelectedFiles));
            if ($_GET['DAC']) {
                $dac = '&DAC=1';
            }
            $lien = 'index.php?page=Agent.DownloadArchive&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])).'&lots='.$listLots.$dac;
            $script = "<script>lienDownloadarchives = opener.document.getElementById('lienDownloadarchives');";
            $script .= " lienDownloadarchives.href = '".$lien."';";
            $script .= ' lienDownloadarchives.click();';
            $script .= ' window.close();';
            $script .= ' </script> ';
            $this->scriptDownloadArchive->Text = $script;
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_MESSAGE_CHOISIR_MIN_ELEMENT'));
        }
    }
}
