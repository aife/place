<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Prado\Prado;

/**
 * Page de modification d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupUpdateDce extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->displayListFileDce();
        }
    }

    /**
     * Afficher la liste des fichiers dans le zip DCE.
     */
    public function displayListFileDce()
    {
        $refConsultation = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));

        $DataSource = [];
        $DataSource['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $DataSource = (new Atexo_Consultation_Dce())->getDataSourecFileDCE($refConsultation, Atexo_CurrentUser::getCurrentOrganism());
        $this->aRemplacer->DataSource = $DataSource;
        $this->aRemplacer->dataBind();
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($msg);
    }

    public function getFileSizeMax(){
        return Atexo_Config::getParameter("MAX_UPLOAD_FILE_SIZE");
    }
    /**
     *
     *
     */
    public function moveFileInTmpDir()
    {
        if ($this->nouvellePiece->getErrorCode()) {
            $msg = Prado::localize('TEXT_TAILLE_AUTORISEE_DOC',
                array('size' => Atexo_Util::arrondirSizeFile(($this->nouvellePiece->MaxFileSize / 1024)))
            );
            $this->afficherErreur($msg);
        } else {
            if ($this->nouvellePiece->HasFile) {
                $this->panelMessageErreur->setVisible(false);
                $msg = Atexo_ScanAntivirus::startScan($this->nouvellePiece->LocalName);
                if ($msg) {
                    $this->afficherErreur($msg . ' "' . $this->nouvellePiece->FileName . '"');
                    return;
                }
            }

            if (Atexo_Config::getParameter('IS_COMMON_TMP_SHARED') == '1') {
                $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP_SHARED') . 'fileUploaded/' . session_id() . '_' . time() . '_' . md5(uniqid(mt_rand(), true)) . '/';
            } else {
                $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP') . 'fileUploaded/' . session_id() . '_' . time() . '_' . md5(uniqid(mt_rand(), true)) . '/';
            }
            Atexo_Files::createDir($pathDirectoryTmpDceFile);
            if ($this->nouvellePiece->HasFile) {
                $infileDce = $pathDirectoryTmpDceFile . 'tmpDceFile';
                $this->filePath->Value = $pathDirectoryTmpDceFile;
                if (!move_uploaded_file($this->nouvellePiece->LocalName, $infileDce)) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize(TEXT_ERREUR_MODIF_DCE));// Erreur  modification du DCE
                    return;
                }
                $newFileSize = Atexo_Util::arrondirSizeFile(($this->nouvellePiece->FileSize) / 1024);
                $infoFile = explode('-', $this->aRemplacer->SelectedValue);
                $oldFileIndex = $infoFile[0];
                if ($infoFile[1] == '__R__') {
                    $oldFileSize = '';
                } else {
                    $oldFileSize = $infoFile[1];
                }
                $oldFileSize = $infoFile[2];
                $oldFileBase = "";
                $this->labelClose->Text = "<script>getFileChange('" . $this->nouvellePiece->FileName . "'," . $oldFileIndex . ",'" . $oldFileBase . "','(" . $oldFileSize . ")','" . $newFileSize . "','" . base64_decode($_GET["prefix"]) . "');</script>";
            }
        }
    }
}
