<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Page de modification du compte utilisateur.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupUpdateCompany extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->displayVilleRc();
            $this->displayEntrepriseEtrangere();
            $this->preRemplirInfosEntreprise();
        }
        if ($this->maroc->Checked) {
            $script = "<script type='text/javascript'>isCheckedShowDiv(document.getElementById('ctl0_CONTENU_PAGE_maroc'),'etablieMaroc');isCheckedShowDiv(document.getElementById('ctl0_CONTENU_PAGE_etranger'),'nonEtablieMaroc');</script>";
        } else {
            $script = "<script> isCheckedShowDiv(document.getElementById('ctl0_CONTENU_PAGE_etranger'),'nonEtablieMaroc');isCheckedShowDiv(document.getElementById('ctl0_CONTENU_PAGE_maroc'),'etablieMaroc');</script>";
        }
        $this->javascript->Text = $script;
        $this->messageErreur->visible = false;
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    public function displayEntrepriseEtrangere()
    {
        $datasource = [];
        $datasource[0] = Prado::localize('TEXT_SELECTIONNER');
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveArrayCountries();
        $datasource = array_merge($datasource, is_array($listCountries) ? $listCountries : []);
        $this->setViewState('listePays', $datasource);
        $this->pays->DataSource = $datasource;
        $this->pays->DataBind();
    }

    public function onSearchClick($sender, $param)
    {
        $rc = null;
        if ($this->maroc->Checked) {
            $villeRc = $this->RcVille->getSelectedValue();
            $numeroRc = $this->RcNumero->Text;
            $rc = $villeRc.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$numeroRc;
            $company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($rc);
            $this->javascript->Text = "<script>var doc = window.opener.document;doc.getElementById('ctl0_CONTENU_PAGE_identifiantCompany').value = '$rc';</script>";
        } else {
            $paysEtranger = $this->pays->getSelectedItem()->getText();
            $idNational = $this->idNational->Text;
            $company = (new Atexo_Entreprise())->retrieveCompanyByIdNational($idNational, $paysEtranger);
            $identifiant = $idNational.'-'.$paysEtranger;
            $this->javascript->Text = "<script>var doc = window.opener.document;doc.getElementById('ctl0_CONTENU_PAGE_identifiantCompany').value = '".$identifiant."';</script>";
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($this->maroc->Checked) {
            $c = new Criteria();
            $c->add(EntreprisePeer::SIREN, $rc);
            $c->add(EntreprisePeer::ID, Atexo_Util::atexoHtmlEntities($_GET['idCompany']), Criteria::NOT_EQUAL);
            $companySearch = EntreprisePeer::doSelectOne($c, $connexionCom);
        } else {
            $c = new Criteria();
            $c->add(EntreprisePeer::SIRENETRANGER, $this->idNational->Text);
            $c->add(EntreprisePeer::PAYSADRESSE, $this->pays->getSelectedItem()->getText());
            $c->add(EntreprisePeer::ID, Atexo_Util::atexoHtmlEntities($_GET['idCompany']), Criteria::NOT_EQUAL);
            $companySearch = EntreprisePeer::doSelectOne($c, $connexionCom);
        }
        if ($companySearch instanceof Entreprise) {
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(Prado::localize('DEFINE_ENTREPRISE_EXISTE_DEJA'));
        } else {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $entreprise = EntreprisePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompany']), $connexionCom);

            if ($this->maroc->Checked) {
                if ($this->RcVille->getSelectedValue() && $this->RcNumero->Text) {
                    $entreprise->setSiren($this->RcVille->getSelectedValue().'-'.$this->RcNumero->Text);
                }
            } else {
                if ($this->pays->getSelectedItem()->getText()) {
                    $entreprise->setPaysadresse($this->pays->getSelectedItem()->getText());
                }
                if ($this->idNational->Text) {
                    $entreprise->setSirenEtranger($this->idNational->Text);
                }
            }
            if ($this->raison_sociale->Text) {
                $entreprise->setNom($this->raison_sociale->Text);
            }
            $entreprise->save($connexionCom);
            $this->javascript->Text .= "<script>var doc = window.opener.document;doc.getElementById('ctl0_CONTENU_PAGE_refreshReaterCompany').click();window.close();</script>";
        }
    }

    public function preRemplirInfosEntreprise()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $entreprise = EntreprisePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompany']), $connexionCom);
        if ($entreprise instanceof Entreprise) {
            $this->raison_sociale->Text = $entreprise->getNom();
            if ($entreprise->getSiren() && !$entreprise->getSirenEtranger()) {
                $arrayRc = explode('-', $entreprise->getSiren());
                $this->maroc->Checked = true;
                $this->etranger->Checked = false;
                $this->RcVille->SelectedValue = $arrayRc[0];
                $this->RcNumero->Text = $arrayRc[1];
            } elseif ($entreprise->getSirenEtranger() && !$entreprise->getSiren()) {
                $this->maroc->Checked = false;
                $this->etranger->Checked = true;
                $listePays = $this->getViewState('listePays');
                $selectedValue = '';
                if ($listePays && (is_countable($listePays) ? count($listePays) : 0)) {
                    foreach ($listePays as $index => $pays) {
                        if ($pays == $entreprise->getPaysadresse()) {
                            $selectedValue = $index;
                        }
                    }
                }
                $this->pays->SelectedValue = $selectedValue;
                $this->idNational->Text = $entreprise->getSirenEtranger();
            }
        }
    }
}
