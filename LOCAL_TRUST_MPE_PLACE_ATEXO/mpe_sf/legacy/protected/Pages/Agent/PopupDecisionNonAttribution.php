<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Prado\Prado;

/**
 * permet de lister les differents telechargement des archives effectues.
 *
 * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 * @
 */
class PopupDecisionNonAttribution extends MpeTPage
{
    protected $typeDecision;
    protected $consultation;
    protected $reference;
    protected $lots;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $logger = Atexo_LoggerManager::getLogger('contrat');
        if (isset($_GET['id']) && isset($_GET['lots']) && $_GET['typeDecision']) {
            $this->reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            $this->lots = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots']));
            $this->typeDecision = $_GET['typeDecision'];
            $this->consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($this->reference);
            if ($this->consultation instanceof CommonConsultation) {
                $this->recapConsultation->initialiserComposant($this->reference, $this->lots);
                $this->getTypeDecision($this->typeDecision);
                $this->dateDecision->Text = Atexo_Util::iso2frnDate(date('Y-m-d'));
            } else {
                $logger->error(' PopupDecisionNonAttribution : probleme de recuperertion de la consultation : reference = '.$_GET['id'].' lot = '.$_GET['lots'].'idAgent = '.Atexo_CurrentUser::getId());
                self::showPanelError(Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
            }
        } else {
            $logger->error(" PopupDecisionNonAttribution Erreur d'accés ,parametre manquant : reference = ".$_GET['id'].' lot = '.$_GET['lots'].' decision '.$_GET['typeDecision']);
            self::showPanelError(Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
        }
    }

    public function getTypeDecision($typeDecision)
    {
        switch ($typeDecision) {
            case Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE'):
                $this->labelTypeDecision->Text = Prado::localize('DECISION_DECLARATION_SANS_SUITE');
                break;

            case Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX'):
                $this->labelTypeDecision->Text = Prado::localize('DECISION_DECLARATION_INFRUCTUEUX');
                break;

            case Atexo_Config::getParameter('DECISION_AUTRE'):
                $this->labelTypeDecision->Text = Prado::localize('DECISION_AUTRE');
                break;
        }
    }

    protected function saveDecision()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $listeLots = explode('#', $this->lots);
        if (is_array($listeLots)) {
            foreach ($listeLots as $lot) {
                $decision = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($this->reference, $lot, $this->consultation->getOrganisme());
                if (!$decision instanceof CommonDecisionLot) {
                    $decision = new CommonDecisionLot();
                }
                $decision->setOrganisme($this->consultation->getOrganisme());
                $decision->setConsultationId($this->reference);
                $decision->setLot($lot);
                $decision->setIdTypeDecision($this->typeDecision);
                $decision->setDateDecision(Atexo_Util::frnDate2iso($this->dateDecision->Text));
                $decision->setCommentaire($this->commentaire->Text);
                $decision->setDateMaj(date('Y-m-d H:i:s'));
                $decision->save($connexion);
            }
        }
        $this->scriptJS->Text = "<script type='text/javascript'>window.opener.location.replace(window.opener.location.href);window.close();</script>";
    }

    /**
     * Permet de gerer les composant en cas d'erreur.
     *
     * @param string $msg le message d'erreur
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED < ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function showPanelError($msg)
    {
        $this->PanelPopupOk->setVisible(false);
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($msg);
    }
}
