<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Operations\Atexo_Operations_CriteriaVo;
use Application\Service\Atexo\Operations\Atexo_Operations_Operations;
use Prado\Prado;

/**
 * Formulaire de gestion des operations.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 4.6
 */
class FormulaireOperation extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('GestionOperations') && Atexo_CurrentUser::hasHabilitation('GererOperations')) {
            if (!$this->IsPostBack) {
                $this->chargerCategories();
                $this->chargerType();
                if (isset($_GET['idOperation']) && $_GET['idOperation']) {
                    $idOperation = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOperation']));
                    $this->chargerOperation($idOperation);
                    $this->setViewState('idOperation', $idOperation);
                }
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
        }
    }

    /**
     * Permet de charger les categories.
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories, Atexo_CurrentUser::readFromSession('lang'));
        $this->categorie->Datasource = $dataCategories;
        $this->categorie->dataBind();
    }

    /**
     * Permet de charger les types.
     */
    public function chargerType()
    {
        $defautlValue = '---'.Prado::localize('DEFINE_TOUS_LES_TYPES').'---';
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $typeArray = (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_ORG_TYPE_OPERATION'), $organisme, null, $defautlValue);
        $this->type->Datasource = $typeArray;
        $this->type->dataBind();
    }

    /*
     * Permet de creer une operation
     */
    public function createOperation($sender, $param)
    {
        if ($this->isValid) {
            $criteriaVo = new Atexo_Operations_CriteriaVo();
            $operation = false;
            if (isset($_GET['idOperation']) && $_GET['idOperation']) {
                $criteriaVo->setDateModification(date('Y-m-d H:i:s'));
                $operation = $this->getViewState('Operation');
            } else {
                $criteriaVo->setAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteriaVo->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                $criteriaVo->setDateCreation(date('Y-m-d H:i:s'));
            }
            $criteriaVo->setType($this->type->getSelectedValue());
            $criteriaVo->setCategorie($this->categorie->getSelectedValue());
            $criteriaVo->setCode($this->codeOperation->Text);
            $criteriaVo->setDescription($this->descriptionObjet->Text);
            $criteriaVo->setAnneeDebut($this->anneeDebut->Text);
            $criteriaVo->setAnneeFin($this->anneeFin->Text);
            $criteriaVo->setBudget($this->budgetGlobal->Text);
            $id = (new Atexo_Operations_Operations())->createUpdateOperarion($criteriaVo, $operation);
            if ($id) {
                $this->retour(base64_encode($id));
            }
        }
    }

    /*
     * Permet de charger une operation
     */
    public function chargerOperation($idOperation)
    {
        if ($operation = (new Atexo_Operations_Operations())->retrieveByRefOperation($idOperation)) {
            $this->setViewState('Operation', $operation);
            $this->type->SelectedValue = $operation->getType();
            $this->categorie->SelectedValue = $operation->getCategorie();
            $this->codeOperation->Text = $operation->getCode();
            $this->descriptionObjet->Text = $operation->getDescription();
            $this->anneeDebut->Text = $operation->getAnneeDebut();
            $this->anneeFin->Text = $operation->getAnneeFin();
            $this->budgetGlobal->Text = Atexo_Util::getMontantArronditEspace($operation->getBudget());
        }
    }

    /*
     * Permet faire la redirect
     */
    public function retour($idOperation)
    {
        $urlRedirect = 'index.php?page=Agent.SearchOperations&idOperation='.$idOperation;
        $this->response->redirect($urlRedirect);
    }

    /*
    * Permet de tester si on est on mode modification ou creation d'une operation
    */
    public function isModification()
    {
        if (isset($_GET['idOperation'])) {
            return true;
        }

        return false;
    }
}
