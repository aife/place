<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;

/**
 * commentaires.
 *
 * @author Mouslim Mitali <Mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupInfosCreationCompteBOAMP extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
        $this->labelCodeLogiciel->Text = $organismeObj->getCodeAccesLogiciel();
    }
}
