<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EnvoiCourrierElectroniqueModifConsultation.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiCourrierElectroniqueAnnulationConsultation extends MpeTPage
{
    protected ?string $_typeMsg = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_ANNULATION_CONSULTATION');
        $this->TemplateEnvoiCourrierElectronique->messageType->setEnabled(false);
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->_typeMsg = 'modifConsult';
        if (!$this->IsPostBack && !$_GET['IdEchange']) {
            $this->TemplateEnvoiCourrierElectronique->destinataires->Text = $this->remplirListeDestinataires();
            $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueUniquementLienTelechargementObligatoire->checked = true;
        }

        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.ChangingConsultation&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    public function remplirListeDestinataires()
    {
        $listeDestinataires = '';
        $mailsDestinataire = [];
        $tabMails = [];

        if (Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            $listeDestinataires = Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        }
        if (Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }
        if (Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }

        $mailsDestinataire = explode(' , ', $listeDestinataires);
        if (is_array($mailsDestinataire) && 0 != count($mailsDestinataire)) {
            foreach ($mailsDestinataire as $mail) {
                if (!in_array($mail, $tabMails)) {
                    $tabMails[] = $mail;
                }
            }
        }
        $listeDestinataires = implode(' , ', $tabMails);

        return $listeDestinataires;
    }
}
