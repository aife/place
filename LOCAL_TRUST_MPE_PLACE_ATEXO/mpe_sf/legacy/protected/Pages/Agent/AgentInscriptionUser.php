<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentInscriptionUser extends MpeTPage
{
    public string|\Application\Propel\Mpe\Entreprise $_company = '';
    public string $_acronymePays = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['rc']) && '' != $_GET['rc']) {
            $this->rcCompany->Text = Atexo_Util::atexoHtmlEntities($_GET['rc']);
            $this->intituleIdEntreprise->Text = Prado::localize('REGISTRE_DU_COMMERCE');
        }
        if (isset($_GET['idCompany']) && isset($_GET['pays'])) {
            $this->_acronymePays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryByDenomination(Atexo_Util::atexoHtmlEntities($_GET['pays']));
            $this->rcCompany->Text = $this->_acronymePays.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').Atexo_Util::atexoHtmlEntities($_GET['idCompany']);
            $this->intituleIdEntreprise->Text = Prado::localize('TEXT_IDENTIFIANT_NATIONNAL');
        }
        $this->panelInscrit->loginModificationValidator->setEnabled(false);
        $this->panelInscrit->emailModificationValidator->setEnabled(false);
        $this->panelInscrit->InfoProfil->setVisible(false);
    }

    /**
     * ajouter Nouveau compte inscrit et entreprise si n'existe pas.
     */
    public function onSaveClick($sender, $param)
    {
        if ($this->IsValid) {
            if ($this->addCompteCompany()) {
                if (isset($_GET['rc']) && '' != $_GET['rc']) {
                    $this->response->redirect('index.php?page=Agent.AgentGestionEntreprises&rc='.$this->_company->getSiren());
                } elseif (isset($_GET['idCompany']) && isset($_GET['pays'])) {
                    $this->response->redirect('index.php?page=Agent.AgentGestionEntreprises&idCompany='.Atexo_Util::atexoHtmlEntities($_GET['idCompany']).'&pays='.Atexo_Util::atexoHtmlEntities($_GET['pays']));
                }
            }
        }
    }

    public function EnvoiMailConfirmationInscrit($inscrit, $entreprise, $pwd = false, $update = false)
    {
        $objet = null;
        if (Atexo_Module::isEnabled('MailActivationCompteInscritEntreprise')) {
            $msg = Atexo_Message::getMessageConfirmationInscritWithCompanyInfo($inscrit, $entreprise, $pwd, $update);
        } else {
            $msg = Atexo_Message::getMessageConfirmationInscrit($inscrit, $entreprise, $pwd, $update);
        }
        if ($update) {
            $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
            (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($inscrit, $objet, $msg);
        } else {
            $objet = Prado::Localize('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR');
            $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
            (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($inscrit, $objet, $msg);
        }
    }

    public function addCompteCompany()
    {
        if ($this->IsValid) {
            $newCompany = new Entreprise();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (isset($_GET['rc']) && '' != $_GET['rc']) {
                $newCompany->setSiren($this->rcCompany->Text);
                $newCompany->setPaysenregistrement(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                $newCompany->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                $newCompany->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            } else {
                $newCompany->setSirenetranger(Atexo_Util::atexoHtmlEntities($_GET['idCompany']));
                $newCompany->setPaysenregistrement($this->_acronymePays);
                $newCompany->setPaysadresse(Atexo_Util::atexoHtmlEntities($_GET['pays']));
                $newCompany->setAcronymePays($this->_acronymePays);
            }
            $newCompany->setNom($this->raisonSociale->Text);
            $newCompany->setDateCreation(date('Y-m-d H:i:s'));
            $newCompany->setDateModification(date('Y-m-d H:i:s'));
            $newCompany->setIdAgentCreateur(Atexo_CurrentUser::getIdAgentConnected());
            $newCompany->setNomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
            $newCompany->setPrenomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
            $newInscrit = new CommonInscrit();
            $newInscrit = $this->panelInscrit->setNewInscrit($newInscrit);
            $newInscrit->setPays($newCompany->getPaysadresse());
            if ($newInscrit) {
                $newCompany->addCommonInscrit($newInscrit);
            }
            $res = $newCompany->Save($connexion);

            // Ajoute l'entreprise dans l'index Entreprise de Lucene
            if (Atexo_Module::isEnabled('UtiliserLucene')) {
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                $searchLucene->addEntreprise($newCompany);
            }
            $this->_company = $newCompany;
            if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit') && true == $this->panelInscrit->pwdAutomatique->checked) {
                $pwd = $this->Page->getViewState('PwdInscrit');
                self::EnvoiMailConfirmationInscrit($newInscrit, $this->_company, $pwd);
            } else {
                self::EnvoiMailConfirmationInscrit($newInscrit, $this->_company);
            }
            if ($res > 0) {
                return $newInscrit;
            } else {
                return false;
            }
        }
    }
}
