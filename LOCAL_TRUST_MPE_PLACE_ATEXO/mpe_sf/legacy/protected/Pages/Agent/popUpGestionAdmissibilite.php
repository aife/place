<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLot;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Atexo_Consultation_StatutEnveloppe;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpGestionAdmissibilite extends MpeTPage
{
    private $_consultationAlloti;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $enveloppe = null;
        $messageError = '';
        $this->panelMessageErreur->setVisible(false);
        if (Atexo_CurrentUser::hasHabilitation('GererAdmissibilite')) {
            if (!$this->IsPostBack) {
                $idOffre = Atexo_Util::atexoHtmlEntities($_GET['offre']);
                $formeEnv = Atexo_Util::atexoHtmlEntities($_GET['formeEnveloppe']);
                $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $typeEnv = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);
                $sousPli = Atexo_Util::atexoHtmlEntities($_GET['sousPli']);
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $connectionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($organisme);
                $criteria->setIdReference($idReference);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);
                if (is_array($consultation) && 1 == count($consultation)) {
                    $consultation = array_shift($consultation);
                }
                if ($consultation instanceof CommonConsultation) {
                    $admissible = true;
                    if (Atexo_Module::isEnabled('InterfaceModuleSub') || $consultation->getModeOuvertureReponse()) {
                        $admissible = false;
                    }
                    $admissibiliteFor = (new Atexo_Consultation_Responses())->admissibiliteSuivante($typeEnv, $consultation);
                    $this->setViewState('admissibiliteFor', $admissibiliteFor);
                    $constitutionDossierReponse = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($idReference);
                    $criteriaResponse->setIdOffre($idOffre);
                    $criteriaResponse->setTypesEnveloppes($constitutionDossierReponse);
                    $criteriaResponse->setOrganisme($organisme);
                    $criteriaResponse->setSearchByIdOffre(true);
                    $criteriaResponse->setAdmissible($admissible);
                    if ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme, $typeEnv, $sousPli);
                            (new Atexo_Consultation_Responses())->addAdmissibility($enveloppe, $idReference, $organisme, $typeEnv, $sousPli, $admissible);
                        }
                    } elseif ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier($idReference, $idOffre, null, '', '', $organisme, true, false, null, null, false, $admissible);
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idOffre, $organisme, $typeEnv, $sousPli);
                            (new Atexo_Consultation_Responses())->addAdmissibility($enveloppe, $idReference, $organisme, $typeEnv, $sousPli, $admissible);
                        }
                    } else {
                        $messageError .= Prado::localize('TYPE_DEPOT_INCONNU');
                    }
                    if (!$enveloppe) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $messageError = Prado::localize('AUCUNE_CANDIDATURE_TROUVE');
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $messageError = Prado::localize('AUCUNE_OFFRE_TECHNIQUE_TROUVE');
                        }
                    }
                    if (1 == (is_countable($enveloppe) ? count($enveloppe) : 0)) {
                        $enveloppe = array_shift($enveloppe);
                    } else {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $messageError = Prado::localize('PLUSIEURS_CANDIDATURE_TROUVE');
                        }
                    }
                    if ((!$enveloppe  instanceof CommonEnveloppe) && !($enveloppe instanceof CommonEnveloppePapier)) {
                        $messageError = Prado::localize('AUCUNE_CANDIDATURE_TROUVE');
                    }
                    if ('0' != $consultation->getEnvOffre()) {
                        if ($enveloppe instanceof CommonEnveloppe) {
                            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                                $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreByIdOffre($idOffre, $organisme);
                            } else {
                                $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreByIdOffre($idOffre, $organisme);
                            }
                            if (!$enveloppeOffre) {
                                $messageError = Prado::localize('ERREUR_AUCUNE_OFFRE');
                            }
                        }
                    }
                    if (!$messageError) {
                        $this->stautEnveloppe->Text = (new Atexo_Consultation_StatutEnveloppe())->getStatutEnveloppeById($enveloppe->getStatutEnveloppe());
                        if ($enveloppe  instanceof CommonEnveloppe) {
                            $this->nonEntreprise->Text = $enveloppe->getOffre($connectionCom)->getNomEntrepriseInscrit();
                            $this->_consultationAlloti = $enveloppe->getOffre($connectionCom)->getMoreThanOneLot();
                        } else {
                            $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($enveloppe->getOffrePapierId(), $organisme);
                            $this->nonEntreprise->Text = $offrePapier->getNomEntreprise();
                            $this->_consultationAlloti = $offrePapier->getMoreThanOneLot();
                        }
                        $listeOffres = [];
                        $listeOffres = $enveloppe->getCollAdmissibiliteLot();
                        $this->repeaerAdmissibilite->DataSource = $listeOffres;
                        $this->repeaerAdmissibilite->DataBind();
                    }
                } else {
                    $messageError = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            }
        } else {
            $messageError = Prado::localize('HABILITATION_GERE_ADMISSIBILITE');
        }
        if ($messageError) {
            $this->panelErreur->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageError);
            $this->panelSuccess->setVisible(false);
            $this->panelBouton->setVisible(false);
        }
    }

    public function moreThanOnLOt($admissibiliteLot)
    {
        if ($this->_consultationAlloti) {
            return Prado::localize('TEXT_LOT').' '.$admissibiliteLot->getSousPli().'<br/>';
        } else {
            return '';
        }
    }

    public function isLotAdmis($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_ADMISSIBLE');
    }

    public function isLotNonAdmis($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_NON_ADMISSIBLE');
    }

    public function isLotATraiter($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_A_TRAITER');
    }

    public function saveAdmissibilite($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $formeEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['formeEnveloppe']);
        foreach ($this->repeaerAdmissibilite->getItems() as $item) {
            $admissionLot = (new Atexo_Consultation_Responses())->retrieveAdmissibiliteByPk(Atexo_Util::atexoHtmlEntities($_GET['offre']), $item->sousPli->Value, $formeEnveloppe, $org, $this->getViewState('admissibiliteFor'));

            if (!$admissionLot) {
                if ($formeEnveloppe == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                    $admissionLot = new CommonAdmissibiliteEnveloppeLot();
                    $admissionLot->setIdOffre(Atexo_Util::atexoHtmlEntities($_GET['offre']));
                    $admissionLot->setSousPli($item->sousPli->Value);
                    $admissionLot->setOrganisme($org);
                } else {
                    $admissionLot = new CommonAdmissibiliteEnveloppePapierLot();
                    $admissionLot->setIdOffrePapier(Atexo_Util::atexoHtmlEntities($_GET['offre']));
                    $admissionLot->setSousPli($item->sousPli->Value);
                    $admissionLot->setOrganisme($org);
                }
            }
            if ($item->lotAdmis->getChecked()) {
                $admissionLot->setAdmissibilite(Atexo_Config::getParameter('LOT_ADMISSIBLE'));
            } elseif ($item->lotNonAdmis->getChecked()) {
                $admissionLot->setAdmissibilite(Atexo_Config::getParameter('LOT_NON_ADMISSIBLE'));
            } elseif ($item->lotATraiter->getChecked()) {
                $admissionLot->setAdmissibilite(Atexo_Config::getParameter('LOT_A_TRAITER'));
            }
            $admissionLot->setCommentaire($item->commentaireLot->Text);

            $admissionLot->setTypeEnveloppe($this->getViewState('admissibiliteFor'));

            $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $admissionLot->save($connection);
        }

        $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callbackButton'])."').click();window.close();</script>";
    }

    public function isConsultationAlloti()
    {
        if ($this->_consultationAlloti) {
            return Prado::localize('TEXT_LOT');
        } else {
            return '';
        }
    }

    public function getIdEnveloppe($admissibilite)
    {
        if ($admissibilite instanceof CommonAdmissibiliteEnveloppeLot) {
            return $admissibilite->getIdEnveloppe();
        } elseif ($admissibilite instanceof CommonAdmissibiliteEnveloppePapierLot) {
            return $admissibilite->getIdEnveloppePapier();
        }
    }
}
