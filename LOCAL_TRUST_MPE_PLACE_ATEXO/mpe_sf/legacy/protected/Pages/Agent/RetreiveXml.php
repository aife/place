<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;

class RetreiveXml extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $arrayIdCons = explode('-', Atexo_Util::atexoHtmlEntities($_GET['id']));
        echo (new Atexo_Entreprise_Reponses())->getReponsesAnnoncesXml($arrayIdCons, Atexo_Util::atexoHtmlEntities($_GET['organisme']));
        exit;
    }
}
