<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCentralePublicationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Popup.
 *
 * @author Khalid ATLASSI <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupCentralePublication extends MpeTPage
{
    private string $_connexionCom = '';
    private string|bool $_org = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->_org = Atexo_CurrentUser::getCurrentOrganism();
        if (!$this->IsPostBack) {
            if (isset($_GET['idCompte']) && $_GET['idCompte']) {
                $this->displayInfoCompte();
            }
        }
    }

    public function displayInfoCompte()
    {
        $compteCentrale = CommonCentralePublicationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompte']), $this->_org, $this->_connexionCom);
        $this->nomCompte->Text = $compteCentrale->getNom();
        $this->email->Text = $compteCentrale->getMail();
        $this->telecopieur->Text = $compteCentrale->getFax();
        $this->information->Text = $compteCentrale->getInformation();
    }

    public function onValider($sender, $param)
    {
        if (isset($_GET['idCompte']) && $_GET['idCompte']) {
            $c = CommonCentralePublicationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompte']), $this->_org, $this->_connexionCom);
            $c->setNom($this->nomCompte->Text);
            $c->setMail(trim($this->email->Text));
            $c->setFax($this->telecopieur->Text);
            $c->setInformation($this->information->Text);
            $c->save($this->_connexionCom);
        }
        $this->scriptClose->Text = '<script>refreshRepeater();</script>';
    }
}
