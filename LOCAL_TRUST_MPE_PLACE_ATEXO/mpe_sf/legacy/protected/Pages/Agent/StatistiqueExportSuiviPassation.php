<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GenerationCsv;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiqueExportSuiviPassation extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $dirName = Atexo_Config::getParameter('STATISTIQUES_PASSATIONS_PATH').Atexo_CurrentUser::getCurrentOrganism().'/';
            if (is_file($dirName.'Export_Passation.csv')) {
                $fp = fopen($dirName.'Export_Passation.csv', 'r');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private', false);
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=Export_Passation.csv;');
                header('Content-Transfer-Encoding: binary');
                echo fread($fp, filesize($dirName.'Export_Passation.csv'));
                fclose($fp);
                exit;
            } else {
                $csv = (new Atexo_GenerationCsv())->genererCvsSuiviPassation(Atexo_CurrentUser::getCurrentOrganism());
                if ($csv) {
                    $dirName = Atexo_Config::getParameter('STATISTIQUES_PASSATIONS_PATH').Atexo_CurrentUser::getCurrentOrganism().'/';

                    if (!is_dir($dirName)) {
                        mkdir($dirName, 0777, true);
                    }
                    Atexo_Util::write_file($dirName.'Export_Passation.csv', $csv);

                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Cache-Control: private', false);
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=Export_Passation.csv;');
                    header('Content-Transfer-Encoding: binary');
                    echo $csv;
                    exit;
                } else {
                    $this->errorPart->setVisible(true);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage('Aucune donnée à remonter.');
                }
            }
        } catch (Exception) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage("Une erreur est survenu lors de l'export.");
        }
    }
}
