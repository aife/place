<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe TableauVersions.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 4
 *
 * @since MPE4
 */
class TableauVersions extends MpeTPage
{
    public array $xmlArray = [];

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->erreurMessage->visible = false;

        $lang = Atexo_CurrentUser::readFromSession('lang');

        $this->xmlArray = Atexo_Util::xmlFileToArray(Atexo_Config::getParameter('URL_XML_VERSION').'version_'.(Atexo_Config::getParameter('CONFIG_REDAC') ? 'redac_' : '').$lang.'.xml');

        $this->repeaterProduits->dataSource = $this->getProductData();
        $this->repeaterProduits->dataBind();
    }

    public function dataBindRepeaterVersion($sender, $param)
    {
        $item = $param->Item;
        if ('Item' === $item->ItemType || 'AlternatingItem' === $item->ItemType) {
            $item->repeaterVersion->DataSource = $this->getVersionByProductData($item->Data['name']);
            $item->repeaterVersion->dataBind();
        }
    }

    public function repeaterItemCreated($sender, $param)
    {
        static $itemIndex = 0;
        $item = $param->Item;
        if ('Item' === $item->ItemType || 'AlternatingItem' === $item->ItemType) {
            $item->Cell->BackColor = $itemIndex % 2 ? '#6078BF' : '#809FFF';
            $item->Cell->ForeColor = 'white';
            ++$itemIndex;
        }
    }

    protected function getProductData()
    {
        $productList = [];
        $arrayProduit = $this->xmlArray['produit'];
        if (is_array($arrayProduit)) {
            if ($arrayProduit[0]) {
                foreach ($arrayProduit as $product) {
                    $productList[] = [
                    'name' => $product['@attributes']['name'],
                    'title' => !empty($product['@attributes']['title']) ? $product['@attributes']['title'] : Prado::localize('LISTE_VERSIONS_PLATEFORME'),
                    ];
                }
            } else {
                $productList[] = [
                'name' => $arrayProduit['@attributes']['name'],
                'title' => !empty($arrayProduit['@attributes']['title']) ? $arrayProduit['@attributes']['title'] : Prado::localize('LISTE_VERSIONS_PLATEFORME'),
                ];
            }
        }

        return $productList;
    }

    protected function getVersionByProductData($productName)
    {
        $versionsList = [];
        static $productVersionsList = [];
        $versionsListTmp = [];

        $arrayProduit = $this->xmlArray['produit'];
        if (is_array($arrayProduit)) {
            if ($arrayProduit[0]) {
                foreach ($arrayProduit as $product) {
                    $versionsListTmp[$product['@attributes']['name']] = $product['versions']['version'];
                }
            } else {
                $versionsListTmp[$arrayProduit['@attributes']['name']] = $arrayProduit['versions']['version'];
            }
        }
        foreach ($versionsListTmp[$productName] as $version) {
            $add_product_directory = ('MPE' != $productName) ? '/'.strtolower($productName) : '';
            $url_access = Atexo_Config::getParameter('URL_DOCS_OUTILS').$add_product_directory.'/'.Atexo_Config::getParameter('NOM_FICHIER_VERSION').$version['numeroVersion'].'.zip';
            $version['url_access'] = $url_access;
            $version['product_name'] = $productName;
            $versionsList[] = $version;
        }

        return $versionsList;
    }
}
