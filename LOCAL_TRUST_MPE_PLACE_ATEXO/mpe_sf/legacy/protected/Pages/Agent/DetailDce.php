<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * Contient le détail d'une DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailDce extends MpeTPage
{
    public $_reference;
    public $referenceUtilisateur;
    public $_organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($this->_reference);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);
        } else {
            return false;
        }
        $this->IdConsultationSummary->setConsultation($consultation);
        $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
        $this->retourDetailConsultation->NavigateUrl = 'index.php?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->getArborescence();
    }

    /**
     * Fabrique l'arboresconce des pièces du zip.
     */
    public function getArborescence()
    {
        $blob_file = $this->getDce();
        $result_affiche = ' ';
        if ($blob_file) {
            $ordered_res = Atexo_Zip::getOrderedFilesForZip($blob_file, false);
            $this->setViewState('arrayDceFiles', $ordered_res);

            if (is_array($ordered_res)) {
                foreach ($ordered_res as $directory_files) {
                    foreach ($directory_files as $key => $directory_one_file) {
                        $profondeur = $directory_one_file['profondeur'];
                        for ($nb_nbsp = 0; $nb_nbsp < $profondeur; ++$nb_nbsp) {
                            $result_affiche .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }

                        if (0 == $key) {
                            $result_affiche .= '&nbsp;&nbsp;<strong>-'.$directory_one_file['name'].'&nbsp;'.$directory_one_file['taille'].'</strong>';
                        } else {
                            if ($directory_one_file['taille']) {
                                $result_affiche .= '&nbsp;&nbsp;-'.$directory_one_file['name'].'&nbsp;'.'( '.$directory_one_file['taille'].' )';
                            } else {
                                $result_affiche .= '&nbsp;&nbsp;-'.$directory_one_file['name'].'&nbsp;'.$directory_one_file['taille'];
                            }
                        }
                        $result_affiche .= '<br/>';
                    }
                }
            }
        }

        return $result_affiche;
    }

    /**
     * retourne le fichier zip.
     */
    public function getDce()
    {
        $dce = '';
        $consultation = $this->getConsultation();
        if ($consultation) {
            $this->referenceUtilisateur = $consultation->getReferenceUtilisateur();
        }

        $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
        if (0 != $dce->getDce()) {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $this->_organisme);
        } else {
            return false;
        }
        $dceTmpFile = $blobResource['blob']['pointer'];
        //$dceTmpFile = Atexo_Files::rebuild_zip($dceTmpFile, $this->referenceUtilisateur);

        return $dceTmpFile;
    }

    /**
     * récupère la consultation.
     */
    public function getConsultation()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->_reference, $connexionCom);

        return $consultation;
    }

    /**
     * Téléchargement complet du DCE.
     */
    public function downloadCompleteDce($sender, $param)
    {
        (new Atexo_Files())->downloadPartialConsFile($this->_reference, $this->_organisme);
    }

    /*
     * Pour l'affichage de l'arborescence du DCE
     */
    public function displayPiecesDce()
    {
        $scriptDce = (new Atexo_Consultation_Dce())->getArborescence($this->_reference, $this->_organisme);
        if ('' != $scriptDce) {
            return $scriptDce;
        }
    }
}
