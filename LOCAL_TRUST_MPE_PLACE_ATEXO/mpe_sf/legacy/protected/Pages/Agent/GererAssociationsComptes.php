<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * commentaires
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Pages
 */
class GererAssociationsComptes extends MpeTPage
{
    public $nbrElementRepeater;
    public ?string $titre = null;

    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!isset($_GET['id'])) {
            $idAgent = Atexo_CurrentUser::getIdAgentConnected();
        } else {
            $idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
        }
        if (!$this->isCallBack) {
            $this->setViewState('idAgent', $idAgent);
            $this->blocInformations->chargerAgent($idAgent, Prado::localize('TEXT_GESTION_ASSOCIATIONS'));
        }
        if (!$this->isPostBack) {
            if (isset($_GET['ges'])) {
                $this->titre = Prado::localize('DEFINE_ADMIN_PMI').' &gt; '.Prado::localize('GESTION_AGENTS').'
							   &gt; '.Prado::localize('TEXT_GERER_LES_ASSOCIATIONS');
            } else {
                $this->titre = Prado::localize('DEFINE_TEXT_MON_COMPTE').' &gt; '.Prado::localize('TEXT_MES_ASSOCIATIONS');
            }
            $this->repeaterPrincipal->repeaterDataBind($idAgent);
            $this->repeaterSecondaire->repeaterDataBind($idAgent);
        }
    }

    /**
     * Annuler et retour a la page d'accueil agent.
     */
    public function retour($sender, $param)
    {
        if (isset($_GET['id'])) {
            $this->response->redirect('index.php?page=Agent.GestionAgents&all=0');
        } else {
            $this->response->redirect('agent');
        }
    }
}
