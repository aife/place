<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Exception;
use Prado\Prado;

/**
 * Page acces au formulaire de la publicite.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class FormulaireAnnoncePublicite extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->panelMessageAvertissement->setMessage(Prado::localize('MESSAGES_MODIFICATION_ANNONCE_SUB'));
        if (!$this->getIsPostBack()) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if (Atexo_Module::isEnabled('publicite')) {
                $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
                $org = Atexo_CurrentUser::getOrganismAcronym();
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                if ($consultation instanceof CommonConsultation) {
                    $logger->info('Accés au formulaire publicité pour consultation '.$consultationId);
                    if (isset($_GET['idAnnonce'])) {
                        $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
                        $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $consultationId, $org, $connexion);
                        if (!$annonce instanceof CommonTAnnonceConsultation) {
                            $this->panelBlocErreur->setVisible(true);
                            $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                            $this->frameAnnonce->setVisible(false);
                            $logger->error('Accés au formulaire publicité pour une annonce '.$idAnnonce." de la consultation qu'on a pas droit dessus ".$consultationId.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                                .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                        }
                    }
                } else {
                    $this->panelBlocErreur->setVisible(true);
                    $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                    $this->frameAnnonce->setVisible(false);
                    $logger->error("Accés au formulaire publicité pour consultation qu'on a pas droit dessus ".$consultationId.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                        .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                }
            } else {
                $logger->error("Accés au formulaire publicité, module plateforme 'Publicite' non active");
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        }
    }

    public function updateAndClose()
    {
        $connexion = null;
        $idAnnonce = null;
        $consultationId = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $consultationId, $org, $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $connexion->beginTransaction();
                $this->updateConultationAndCompteBomapFromSub($consultationId, $annonce, $connexion, $logger);
                $connexion->commit();
            }
            $this->scriptSaveClose->text = '<script>window.close();</script>';
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error("une erreur s'est produite lors de la mise à jour du statut de l'annonce ".$idAnnonce.' consultation '.$consultationId.' '.$e->getMessage().' '.$e->getTraceAsString());
            $this->panelBlocErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('MESSAGES_ERREUR_VALIDATION_ANNONCE_SUB'));
            $this->frameAnnonce->setVisible(false);
        }
    }

    public function updateStatutAnnonce()
    {
        $connexion = null;
        $idAnnonce = null;
        $consultationId = null;
        try {
            $statut = Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER');
            $scriptferme = '';
            $updateAnnonceFromSub = false;
            if ('1' == $this->statutFormulaire->value) {
                $statut = Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET');
                $scriptferme = 'window.close();';
                $updateAnnonceFromSub = true;
            }
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $consultationId, $org, $connexion);
            $connexion->beginTransaction();
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $oldStatut = $annonce->getStatut();
                $annonce->setDateStatut(date('Y-m-d H:i:s'));
                $annonce->setStatut($statut);
                $annonce->save($connexion);
                if ($updateAnnonceFromSub) {
                    $this->updateConultationAndCompteBomapFromSub($consultationId, $annonce, $connexion, $logger);
                }
                $connexion->commit();
                if ($oldStatut != $statut) {
                    //historique apres le commite car il ya des info qui doivent s'enregistrer en base avant de creer l'historique
                    Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                }
                $logger->info("L'annonce ".$idAnnonce.' a été mise à jour statut : '.$statut);
            }
            $this->scriptSaveClose->text = "<script>if(opener.document.getElementById('ctl0_CONTENU_PAGE_refreshAnnonceEncours'))opener.document.getElementById('ctl0_CONTENU_PAGE_refreshAnnonceEncours').click();".$scriptferme.'</script>';
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error("une erreur s'est produite lors de la mise à jour du statut de l'annonce ".$idAnnonce.' consultation '.$consultationId.' '.$e->getMessage().' '.$e->getTraceAsString());
            $this->panelBlocErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('MESSAGES_ERREUR_VALIDATION_ANNONCE_SUB'));
            $this->frameAnnonce->setVisible(false);
        }
    }

    public function updateConultationAndCompteBomapFromSub($consultationId, $annonce, $connexion, $logger)
    {
        try {
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $compte = $annonce->getCompteBoamp($connexion);
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
                if ($consultation instanceof CommonConsultation) {
                    if (($consultation->getStatusConsultation() != Atexo_Config::getParameter('STATUS_PREPARATION')
                        && $consultation->getStatusConsultation() != Atexo_Config::getParameter('STATUS_ELABORATION'))) {
                        $consultation = null;
                    }
                }
                $xml = Atexo_FormulaireSub_AnnonceEchange::getDossier($annonce->getIdDossierSub(), $logger);
                Atexo_FormulaireSub_AnnonceEchange::updateFromXmlSUB($consultation, $compte, $xml, $connexion, $logger);
                if ($compte instanceof CommonAcheteurPublic) {
                    $compte->save($connexion);
                }
                if ($consultation instanceof CommonConsultation) {
                    $consultation->save($connexion);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getUrlFrame()
    {
        $idAnnonce = null;
        try {
            if (!$this->getIsPostBack()) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                if (isset($_GET['id']) && isset($_GET['idAnnonce'])) {
                    $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getOrganismAcronym();
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
                    $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $consultationId, $org, $connexion);
                    if ($annonce instanceof CommonTAnnonceConsultation) {
                        $logger->info("Consutruction de l'annonce".$idAnnonce.' pour consultation '.$consultationId);
                        $jeton = Atexo_FormulaireSub_AnnonceEchange::authentification($logger);
                        $urlFrame = Atexo_Config::getParameter('URL_SUB');
                        $urlFrame .= Atexo_Config::getParameter('URL_SUB_ANNONCE_FORMULAIRE').'?jeton='.$jeton;
                        if ($jeton) {
                            $urlFrame .= '&idDispositif='.$annonce->getIdDispositif();
                            $urlFrame .= '&idDossier='.$annonce->getIdDossierSub();
                            $logger->info("URL iframe construit formulaire publicité de l'annonce ".$idAnnonce.' de la consultation '.$consultationId.' : '.$urlFrame);

                            return $urlFrame;
                        } else {
                            $this->panelBlocErreur->setVisible(true);
                            $this->messageErreur->setMessage(Prado::localize('MESSAGES_ERREUR_JETON_VIDE_ANNONCE_SUB'));
                            $this->frameAnnonce->setVisible(false);
                            $logger->error("Pas de jeton pour l'accés au formulaire publicité pour une annonce ".$idAnnonce." de la consultation qu'on a pas droit dessus ".$consultationId.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                                .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                        }
                    } else {
                        $this->panelBlocErreur->setVisible(true);
                        $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                        $this->frameAnnonce->setVisible(false);
                        $logger->error('Accés au formulaire publicité pour une annonce '.$idAnnonce." de la consultation qu'on a pas droit dessus ".$consultationId.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                            .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                    }
                }
            }
        } catch (Exception) {
            $logger->error("Erreur est survenu lors de la construction de l'url du iframe de l'annonce ".$idAnnonce);
        }
    }
}
