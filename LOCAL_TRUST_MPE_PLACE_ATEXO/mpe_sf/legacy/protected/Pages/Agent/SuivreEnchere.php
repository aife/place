<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonEnchereOffre;
use Application\Propel\Mpe\CommonEnchereOffrePeer;
use Application\Propel\Mpe\CommonEnchereOffreReference;
use Application\Propel\Mpe\CommonEncherePmi;
use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Class pour la suivi temps réelle d'une enchere.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SuivreEnchere extends MpeTPage
{
    private $_enchere;
    private $_references;
    private $_connexionCom;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            if (0 == strcmp($_GET['idEnchere'], (int) $_GET['idEnchere'])) {
                $this->pageNonAccessible->setVisible(false);
                $this->historique->setVisible(false);

                $this->_enchere = $this->getEnchereFromId(Atexo_Util::atexoHtmlEntities($_GET['idEnchere']));
                if (!$this->_enchere) {
                    $this->etapeSuivre->setVisible(false);
                    $this->historique->setVisible(false);
                    $this->pageNonAccessible->setVisible(true);
                    $this->panelErreur->setMessage(Prado::localize('ERREUR_CHARGEMENT_ENCHERE'));

                    return;
                }

                $c = new Criteria();
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $this->_references = $this->_enchere->getCommonEncherereferences($c, $connexionCom);

                $this->setViewState('references', $this->_references);
                $this->setViewState('enchere', $this->_enchere);
                $this->etapeSuivre();
            }
        } else {
//            $this->_enchere = $this->getViewState("enchere");
//            $this->_references = $this->getViewState("references");
//            $this->etapeSuivre();
        }
    }

    public function getConnection()
    {
        if (null == $this->_connexionCom) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }

        return $this->_connexionCom;
    }

    private function getEnchereFromId($idEnchere)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEncherePmiPeer::ID, $idEnchere, Criteria::EQUAL);
        $c->add(CommonEncherePmiPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism(), Criteria::EQUAL);
        $enchere = CommonEncherePmiPeer::doSelect($c, $connexion);
        if (null != $enchere) {
            return $enchere[0];
        }

        return null;
    }

    public function getEnchere()
    {
        if (null == $this->_enchere) {
            $this->_enchere = $this->getViewState('enchere');
        }

        return $this->_enchere;
    }

    public function isEnchereSuspendue()
    {
        if (null == $this->_enchere) {
            $this->_enchere = $this->getViewState('enchere');
        }
        $dateSuspention = $this->_enchere->getDatesuspension();
        if (0 == strcmp($dateSuspention, '0000-00-00 00:00:00')) {
            return false;
        } else {
            return true;
        }
    }

    private function updateStatus($enchere)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $enchereUpdated = CommonEncherePmiPeer::retrieveByPK($enchere->getId(), $connexion);

        // Affichage des informations de l'enchère
        $this->dateDebutSuivi->Text = date('d/m/Y H:i:s', strtotime($enchereUpdated->getDatedebut()));
        $this->dateFinPrevSuivi->Text = date('d/m/Y H:i:s', strtotime($enchereUpdated->getDatefin()));

        $status = $enchereUpdated->getStatusEnchere();
        $this->statusLabelSuivi->Text = $status['text'];
        $this->statusLabelSuivi->CssClass = $status['css'];

        $tempsRestant = $enchereUpdated->getTsTempsRestant();
        $this->timeLeftSuivi->Text = $tempsRestant['text'];
        $this->timeLeftSuivi->CssClass = $tempsRestant['css'];

        $this->messageSoumissionnaires->Text = $enchereUpdated->getCommentaire();
    }

    private function updateTableauSuivi(CommonEncherePmi $enchere, $references)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ('1' == $enchere->getMeilleurnotehaute()) {
            $tri = 'DESC';
        } else {
            $tri = 'ASC';
        }
        // Récupérations des dernières offres faites par les entreprises
        $lastOffre = CommonEnchereOffre::getLastOffreOrdered($enchere->getId(), $tri, $enchere->getOrganisme());

        // On ajoute également les entreprises qui n'ont pas encore fait d'offre
        $allEntreprises = $enchere->getCommonEnchereEntreprisePmis(null, $connexion);
        $otherOffres = [];
        foreach ($allEntreprises as $entreprise) {
            $alreadyIn = false;
            foreach ($lastOffre as $offre) {
                if ($offre->getIdenchereentreprise() == $entreprise->getId()) {
                    $alreadyIn = true;
                }
            }
            if (!$alreadyIn) {
                $newOffre = new CommonEnchereOffre();
                $newOffre->setIdenchereentreprise($entreprise->getId());
                $newOffre->setIdenchere($enchere->getId());
                $newOffre->setOrganisme($enchere->getOrganisme());

                // On crer temporairement des offre sur enchere (pour que le tableau s'affiche bien...)
                foreach ($references as $reference) {
                    $enchereOffreReferenceTmp = new CommonEnchereOffreReference();
                    $enchereOffreReferenceTmp->setValeur(null);
                    $enchereOffreReferenceTmp->setCommonEnchereReference($reference);
                    $newOffre->addCommonEnchereOffreReference($enchereOffreReferenceTmp);
                }

                $otherOffres[] = $newOffre;
            }
        }

        $lastOffre = array_merge($lastOffre, $otherOffres);
        $this->repeaterOffres->setDataSource($lastOffre);
        $this->repeaterOffres->dataBind();

        // Libelles du tableau
        $this->repeaterReferenceOffre->setDataSource($references);
        $this->repeaterReferenceOffre->dataBind();

        // Valeurs des offres
        foreach ($this->repeaterOffres->Items as $offre) {
            $offresReferences = $offre->Data->getCommonEnchereOffreReferences(null, $connexion);
            if (!isset($offresReferences[0])) {
                $offresReferencesObj = new CommonEnchereOffreReference();
                $offresReferencesObj->setValeur('-');
                $offresReferences = [];
                $offresReferences[] = $offresReferencesObj;
            }
            $offre->repeaterValeursOffreReference->setDataSource($offresReferences);
            $offre->repeaterValeursOffreReference->dataBind();
        }
    }

    public function etapeSuivre()
    {
        $this->etapeSuivre->setVisible(true);
        $this->historique->setVisible(false);
        $this->pageNonAccessible->setVisible(false);

        $this->_enchere = $this->getViewState('enchere');
        if (null == $this->_references) {
            $this->_references = $this->getViewState('references');
        }

        $this->updateStatus($this->_enchere);
        $this->updateTableauSuivi($this->_enchere, $this->_references);
    }

    public function updateEnchere($sender, $param)
    {
        $references = $this->getViewState('references');
        $enchere = $this->getViewState('enchere');

        $this->updateTableauSuivi($enchere, $references);
        $this->updateStatus($enchere);

        $this->infosEnchere->render($param->getNewWriter());
    }

    public function suspendreEnchere($sender, $param)
    {
        if (null == $this->_enchere) {
            $this->_enchere = $this->getViewState('enchere');
        }

        if (null == $this->_references) {
            $this->_references = $this->getViewState('references');
        }

        if ($this->isEnchereSuspendue()) {
            $this->_enchere->reprendre($this->getConnection());
        } else {
            $this->_enchere->suspendre($this->getConnection());
        }

        $this->updateStatus($this->_enchere);
        $this->updateTableauSuivi($this->_enchere, $this->_references);
        $this->infosEnchere->render($param->getNewWriter());
    }

    public function sendMessage($sender)
    {
        $this->_enchere = $this->getViewState('enchere');
        $this->_enchere->setCommentaire($this->messageSoumissionnaires->Text);
        $this->_enchere->save($this->getConnection());
    }

    public function etapeHistorique()
    {
        $this->etapeSuivre->setVisible(false);
        $this->historique->setVisible(true);
        $this->pageNonAccessible->setVisible(false);

        $this->_enchere = $this->getViewState('enchere');
        $references = $this->_enchere->getCommonEnchereReferences(null, $this->getConnection());

        $c = new Criteria();
        $c->addDescendingOrderByColumn(CommonEnchereOffrePeer::DATE);
        $allOffres = $this->_enchere->getCommonEnchereOffres($c, $this->getConnection());

        $this->repeaterOffresHistorique->setDataSource($allOffres);
        $this->repeaterOffresHistorique->dataBind();

        // Libelles du tableau
        $this->repeaterReferences->setDataSource($references);
        $this->repeaterReferences->dataBind();

        // Valeurs des offres
        foreach ($this->repeaterOffresHistorique->Items as $offre) {
            $offresReferences = $offre->Data->getCommonEnchereOffreReferences(null, $this->getConnection());
            if (!isset($offresReferences[0])) {
                $offresReferencesObj = new CommonEnchereOffreReference();
                $offresReferencesObj->setValeur('-');
                $offresReferences = [];
                $offresReferences[] = $offresReferencesObj;
            }
            $offre->repeaterValeursOffresReferences->setDataSource($offresReferences);
            $offre->repeaterValeursOffresReferences->dataBind();
        }
    }

    public function telechargerHistorique($sender, $param)
    {
        $ligne = [];
        $this->etapeSuivre->setVisible(false);
        $this->historique->setVisible(true);
        $this->pageNonAccessible->setVisible(false);

        $this->_enchere = $this->getViewState('enchere');
        $references = $this->_enchere->getCommonEncherereferences(null, $this->getConnection());

        $c = new Criteria();
        $c->addDescendingOrderByColumn(CommonEnchereOffrePeer::DATE);
        $allOffres = $this->_enchere->getCommonEnchereOffres($c, $this->getConnection());

        $i = 0;
        $ligne[$i][] = Prado::localize('DEFINE_NOM');
        $ligne[$i][] = Prado::localize('DATE_ENCHERE');
        $ligne[$i][] = Prado::localize('NOTE');
        //$ligne[$i][] = Prado::localize('DEFINE_RANG');
        foreach ($references as $reference) {
            $ligne[$i][] = $reference->getLibelle().'('.$reference->getUnite().')';
        }
        $ligne[$i][] = Prado::localize('ECART_RELATIF_MONTANT_RESERVE');
        $ligne[$i][] = Prado::localize('ECART_ABSOLU_MONTANT_RESERVE');

        foreach ($allOffres as $offre) {
            ++$i;
            $offresReferences = $offre->getCommonEnchereOffreReferences(null, $this->getConnection());
            if (!isset($offresReferences[0])) {
                $offresReferencesObj = new CommonEnchereOffreReference();
                $offresReferencesObj->setValeur('-');
                $offresReferencesObj->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                $offresReferences = [];
                $offresReferences[] = $offresReferencesObj;
            }
            $ligne[$i][] = $offre->getCommonEnchereEntreprisePmi($this->getConnection())->getNom();
            $ligne[$i][] = $offre->getDate();
            $ligne[$i][] = $offre->getValeurngc();
            //$ligne[$i][] = $offre->getRang();
            foreach ($offresReferences as $ref) {
                $ligne[$i][] = $ref->getValeur();
            }
            $ligne[$i][] = $offre->getEcartRelatif($this->getConnection());
            $ligne[$i][] = $offre->getEcartAbsolu($this->getConnection());
        }
        $content = '';
        foreach ($ligne as $val) {
            $content .= implode(',', $val)."\n";
        }
        DownloadFile::downloadFileContent('Historique.csv', $content);
    }

    public function getLibelleEntiteAssocie()
    {
        $service = $this->getEnchere()->getService($this->Page->getConnection());
        if ($service instanceof CommonService) {
            return $service->getLibelle();
        } else {
            $AcronymeOrganisme = Atexo_CurrentUser::getOrganismAcronym();
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($AcronymeOrganisme);
            if ($organisme instanceof CommonOrganisme) {
                return $organisme->getDenominationOrg();
            }
        }
    }
}
