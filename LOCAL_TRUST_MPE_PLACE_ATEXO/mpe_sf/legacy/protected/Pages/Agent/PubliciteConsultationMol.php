<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Prado\Prado;

/*
 * Created on 13 juin 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class PubliciteConsultationMol extends MpeTPage
{
    private $_consultation = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        $this->idConsultationSummary->setConsultation($this->_consultation);
        if ($this->_consultation instanceof CommonConsultation) {
            $this->mainPanel->visible = true;
            $this->errorPanel->visible = false;
            if ($this->_consultation->getCompteBoampAssocie()) {
                $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($this->_consultation->getCompteBoampAssocie(), Atexo_CurrentUser::getOrganismAcronym());
                if ($acheteurPublic instanceof CommonAcheteurPublic) {
                    $this->choixCompteBoamp->setVisible(false);
                    $this->buttonsChoixCompteBoamp->setVisible(false);
                    $this->retourFrameMol->setVisible(true);
                    $this->scriptJs->Text = '<script>showLoader();</script>';
                    $this->retourFrameMolH->setVisible(true);
                    $this->frameMol->setVisible(true);
                    if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                    || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                        $urlRetour = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                        $urlRetour = 'index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    } else {
                        $urlRetour = '#';
                    }
                    $this->boutonRetour->setNavigateUrl($urlRetour);
                    $this->boutonRetourH->setNavigateUrl($urlRetour);
                } else {
                    $this->errorPanel->setVisible(true);
                    $this->errorPanel->setMessage(Prado::localize('DEFINE_COMPTE_BOAMP_ASSOCIE_SUPPRIME'));
                    $this->choixCompteBoamp->setVisible(true);
                    $this->buttonsChoixCompteBoamp->setVisible(true);
                    $this->retourFrameMol->setVisible(false);
                    $this->retourFrameMolH->setVisible(false);
                    $this->frameMol->setVisible(false);
                    $this->scriptJs->Text = '<script>hideLoader();</script>';
                    if (!$this->isPostBack) {
                        $this->displayAcheteurPublic();
                    }
                }
            } else {
                $this->choixCompteBoamp->setVisible(true);
                $this->buttonsChoixCompteBoamp->setVisible(true);
                $this->retourFrameMol->setVisible(false);
                $this->retourFrameMolH->setVisible(false);
                $this->frameMol->setVisible(false);
                $this->scriptJs->Text = '<script>hideLoader();</script>';
                if (!$this->isPostBack) {
                    $this->displayAcheteurPublic();
                }
            }
        } else {
            $this->mainPanel->visible = false;
            $this->errorPanel->visible = true;
            $this->errorPanel->setMessage('Error Consultation');
            $this->scriptJs->Text = '<script>hideLoader();</script>';

            return;
        }
    }

    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /**
     * afficher les acheteurs publics.
     */
    public function displayAcheteurPublic()
    {
        $idServiceRattach = Atexo_CurrentUser::getIdServiceAgentConnected();
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getOrganismAcronym());
        $idServiceAssocie = $consultation->getServiceAssocieId();
        $listAcheteur = (new Atexo_PublicPurchaser())->retrievePublicPurchasersCons($idServiceRattach, $idServiceAssocie);
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        foreach ($listAcheteur as $oneAcheteur) {
            // $siteBoamp = '';
            $mail = '';
            /*if ($oneAcheteur->getBoampTarget()=='0') {
                $siteBoamp = ' - '.Prado::localize('TEXT_DEMONSTRATION');
            }
            else {
                $siteBoamp = ' - '.Prado::localize('TEXT_PRODUCTION');
            }*/
            if ('' != $oneAcheteur->getBoampMail()) {
                $mail = ' - '.$oneAcheteur->getBoampMail();
            }
            $dataSource[$oneAcheteur->getId()] = $oneAcheteur->getBoampLogin().$mail; //$oneAcheteur->getDenomination() . $mail . $siteBoamp;
        }
        $this->acheteurPublic->DataSource = $dataSource;
        $this->acheteurPublic->DataBind();
    }

    public function actionBouttonAnnuler()
    {
        if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
        || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
            $link = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->response->redirect($link);
        } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $link = 'index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->response->redirect($link);
        }
    }

    public function onValider()
    {
        $value = $this->acheteurPublic->getSelectedValue();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultationComm = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), null);
        $consultationComm->setCompteBoampAssocie($value);
        $consultationComm->save($connexionCom);
        $this->_consultation = $consultationComm;
        if ($this->_consultation->getCompteBoampAssocie()) {
            $this->choixCompteBoamp->setVisible(false);
            $this->buttonsChoixCompteBoamp->setVisible(false);
            $this->retourFrameMol->setVisible(true);
            $this->frameMol->setVisible(true);
            $this->errorPanel->visible = false;
            if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
            || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } else {
                $this->boutonRetour->setNavigateUrl('#');
            }
        }
    }
}
