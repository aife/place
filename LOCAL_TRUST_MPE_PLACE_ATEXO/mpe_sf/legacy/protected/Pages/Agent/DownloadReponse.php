<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;
use ZipArchive;

/**
 * Permet de télécharger la réponse.
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadReponse extends DownloadFile
{
    public ?string $_typeSignature = null;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
            if (isset($_GET['id'])) {
                /* Recherche de la consultation via la méthode Atexo_Consultation::search pour voir si l'utilisateur a accés
                 à la consultation */
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($organisme);
                $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);

                if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                    /* Si une consultation trouvé il faut vérifier si un des état de la consultation est égale à
                     ouverture net analyse ou décision */
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') ||
                    $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                    || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                    || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                        if (isset($_GET['idOffre']) && isset($_GET['typeDownload']) && 'offre' == $_GET['typeDownload']) {
                            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                            $idOffre = Atexo_Util::atexoHtmlEntities(Atexo_Util::atexoHtmlEntities($_GET['idOffre']));
                            //TODO
                            $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                            if ($offre instanceof CommonOffres) {
                                $zipFileName = 'EL'.utf8_encode($offre->getNumeroReponse()).'_Reponse.zip';
                                $xmlStr = $offre->getXmlString();
                                $pos = strpos($xmlStr, 'typeSignature');
                                if ($pos > 0) {
                                    $this->_typeSignature = 'XML';
                                }
                                $directory = Atexo_Config::getParameter('COMMON_TMP').session_name().session_id().time().md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
                                mkdir($directory);
                                $zip = new ZipArchive();
                                $res = $zip->open($directory.$zipFileName, ZipArchive::CREATE);
                                if (true === $res) {
                                    $enveloppes = $offre->getCommonEnveloppes();
                                    foreach ($enveloppes as $oneEnveloppe) {
                                        $fichiers = (new Atexo_Consultation_Responses())->retrieveEnveloppeFiles($oneEnveloppe->getIdEnveloppeElectro(), $organisme);
                                        if (is_array($fichiers) && count($fichiers)) {
                                            $nameDirEnveloppe = self::getNomDossierEnveloppe($oneEnveloppe, $offre->getNumeroReponse());
                                            $zip->addEmptyDir($nameDirEnveloppe);
                                            $this->addFichierEnveloppeToZip($zip, $fichiers, $oneEnveloppe->getTypeEnv(), $offre->getUntrusteddate(), $nameDirEnveloppe);
                                        }
                                    }

                                    $zip->close();
                                    header('Pragma: public');
                                    header('Expires: 0');
                                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                                    header('Cache-Control: private', false);
                                    header('Content-Type: application/octet-stream');
                                    header('Content-Disposition: attachment; filename="'.$zipFileName.'";');
                                    header('Content-Transfer-Encoding: binary');
                                    header('Content-Length: '.filesize($directory.$zipFileName));
                                    $fp = fopen($directory.$zipFileName, 'rb');
                                    while (!feof($fp) && 0 == connection_status()) {
                                        set_time_limit(0);
                                        echo fread($fp, 1024 * 8);
                                        flush();
                                        ob_flush();
                                    }
                                    fclose($fp);
                                    Atexo_Util::removeDirectoryRecursively($directory);
                                    exit;
                                }
                            }
                        }
                    } else {
                        echo 'Droit Habillitation';
                    }
                } else {
                    echo 'Acces à la consultation';
                }
            } else {
                echo 'réf non fourni';
            }
        } else {
            echo 'Habillitation AccesReponses';
        }
    }

    /*
     * Retourne le nom du dossier d'un enveloppe
     */
    public function getNomDossierEnveloppe($enveloppe, $numeroRep)
    {
        $fileNameEnv = 'EL_'.$numeroRep.'_';
        if ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $fileNameEnv .= Prado::localize('ENVELOPPE_CANDIDATURE');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $fileNameEnv .= Prado::localize('ENVELOPPE_OFFRE');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $fileNameEnv .= Prado::localize('ENVELOPPE_ANONYMAT');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $fileNameEnv .= Prado::localize('ENVELOPPE_OFFRE_TECHNIQUE');
        }
        if ($enveloppe->getSousPli()) {
            $fileNameEnv .= '_Lot_'.$enveloppe->getSousPli();
        }

        return $fileNameEnv;
    }

    /*
     * Ajouter les fichiers d'un enveloppe au zip passer en paramettre
     */
    public function addFichierEnveloppeToZip($zip, $fichiers, $typeEnv, $untrusteddate, $nameDirEnveloppe)
    {
        $atexoBlob = new Atexo_Blob();
        $filesAddedToZip = [];
        foreach ($fichiers as $fichier) {
            $organisme = $fichier->getOrganisme();
            $numeroFichierAutre = 1;
            /*Tableau contenant la liste des fichiers mis dans le zip princiapl, permet de savoir si deux fichiers portant le même nom
             ont été mis dans le zip , si oui un numéro ($numeroFichierMemeNom) est ajouté avant l'extension du fichier pour faire la différence */
            $numeroFichierMemeNom = 1;
            //Si le nom de l'enreprise est visible ou le paramètre NomEntrepriseToujoursVisible est à 1 (toujours afficher le nom d'entreprise)
            $infosFile = pathinfo($fichier->getNomFichier());
            //TODO
            if (true || '' != $entrepriseName || '-' != $entrepriseName || Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
                //On garde le vrai nom du fichier
                $fileName = $fichier->getNomFichier();
                $originalFileName = true;
            } else {
                $originalFileName = false;
                // Si nom entreprise nom visible les fichiers déposés sont renomés selon leur type et leur catégorie
                if ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_FICHIER_PRINCIPAL')) {
                    if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                        $fileName = Prado::localize('ENVELOPPE_CANDIDATURE');
                    } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                        $fileName = Prado::localize('ENVELOPPE_OFFRE');
                    } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                        $fileName = Prado::localize('ENVELOPPE_ANONYMAT');
                    }
                } elseif ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_FICHIER_ACTE_ENGAGEMENT')) {
                    $fileName = Prado::localize('ACTE_ENGAGEMENT');
                } else {
                    $fileName = Prado::localize('AUTRE_FICHIER').$numeroFichierAutre;
                    ++$numeroFichierAutre;
                }

                //Si le nom du fichier a été modifié il faut garder la même extension que le vrai fichier
                $fileName = $fileName.'.'.$infosFile['extension'];
            }

            //Si un même fichier a déjà été inséré ajouter un numéro avant l'extension
            if (isset($filesAddedToZip[$fileName])) {
                $fileName = $infosFile['filename'].'_'.$numeroFichierMemeNom.'.'.$infosFile['extension'];
                ++$numeroFichierMemeNom;
            }

            $fileName = Atexo_Util::replaceCharactersMsWordWithRegularCharacters($fileName);
            //Création d'un fichier contenant la signature du fichier en cours
            $signature = $fichier->getSignatureFichier();
            if ((!$fichier->getIdFichierSignature()) && '' != $signature) {
                if ('XML' == $this->_typeSignature) {
                    $zip->addFromString($nameDirEnveloppe.'/'.$fichier->getJetonFileName(true, $fileName), base64_decode($signature));
                } else {
                    $zip->addFromString($nameDirEnveloppe.'/'.$fileName.' - Signature 1.p7s', $signature);
                }
            }
            $blobresource = $atexoBlob->acquire_lock_on_blob($fichier->getIdBlob(), $organisme);
            $zip->addFile($blobresource['blob']['pointer'], $nameDirEnveloppe.'/'.$fileName);
            $filesAddedToZip[$fileName] = $fileName;
        }
    }
}
