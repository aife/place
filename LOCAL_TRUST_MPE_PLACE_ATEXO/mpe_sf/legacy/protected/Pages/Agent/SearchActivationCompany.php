<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;

/**
 * page de recherche des entreprises.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SearchActivationCompany extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->panelSearch->setVisible(true);
            $this->panelResultSerch->setVisible(false);
            $this->panelSearch->dateStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y H:i:s'), 2, 0, 0);
            $this->panelSearch->dateEnd->Text = Atexo_Util::dateWithparameter(+1, date('d/m/Y H:i:s'), 0, 0, 0);
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les critères de recherches
     */
    public function dataForSearchResult(Atexo_Entreprise_CriteriaVo $criteriaVo)
    {
        $this->panelSearch->setVisible(false);
        $this->panelResultSerch->setVisible(true);
        $this->panelResultSerch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $this->panelResultSerch->Trier($param->CommandName);
    }
}
