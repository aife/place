<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use AtexoPdf\PdfGeneratorClient;
use Prado\Prado;

/**
 * Classe SuiviEchanges.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SuiviEchanges extends MpeTPage
{
    private $critereTri;
    private $triAscDesc;
    protected array $arrayTypeAR = [];
    protected array $arrayFormat = [];
    protected $idRef;
    protected array $criterTri = [];

    private $arrayObjet;
    private $arrayDestinataire;
    private $arrayHeure;
    private $_consultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->idRef = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->idretour->NavigateUrl = '?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']);

        $this->arrayTypeAR = Atexo_Message::getAllTypesAR(Atexo_CurrentUser::getCurrentOrganism());
        $this->arrayFormat = Atexo_Message::getAllFormatsEchange(Atexo_CurrentUser::getCurrentOrganism(), true);

        if ($_GET['id']) {
            //Recherche de la consultation
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                $this->_consultation = $consultation;
            }
        }

        if (!$this->IsPostBack) {
            if ($this->_consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
                $msgAvertissement = '<span>'.Prado::localize('MSG_AVERTISSEMENT_MODIFS_NON_INTEGRES_CAR_CONS_ARCHIVEE').'</span><br/>';
                $this->panelMessageWarning->setVisible(true);
                $this->panelMessageWarning->setMessage($msgAvertissement);
            }
            if ($_GET['id']) {
                $this->displayListeObjet(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->displayListeDatesMessage(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->displayListeMailDestinataires(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->displayListeFormat();
            }
            $this->setViewState('OBJET', $this->idObjet->SelectedValue);
            $this->setViewState('MAIL_DESTINATAIRE', $this->IdDestinataireFiltre->SelectedValue);
            $this->setViewState('FORMAT', $this->idFormat->SelectedValue);
            $this->setViewState('DATE_MESSAGE', $this->idDateHeureFiltre->SelectedValue);

            $this->criterTri['offSet'] = '0';
            $this->setViewState('offSet', 0);
            $this->criterTri['limit'] = $this->RepeaterListEchanges->PageSize;
            $this->setViewState('limit', $this->RepeaterListEchanges->PageSize);

            $this->remplirTableauListeEchanges(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        }
    }

    public function remplirTableauListeEchanges($consultationId, $org, $tri = false, $resultLimeted = true)
    {
        $arrayDests = [];
        $acrOrg = Atexo_CurrentUser::getOrganismAcronym();
        if (0 != $this->getViewState('OBJET')) {
            $arrayObjet = $this->getViewState('arrayObjet');
            $this->criterTri['OBJET'] = $arrayObjet[$this->getViewState('OBJET')]; //$this->idObjet->SelectedValue
        } else {
            $this->criterTri['OBJET'] = '';
        }
        if (0 != $this->getViewState('MAIL_DESTINATAIRE')) {
            $arrayDestinataire = $this->getViewState('arrayDestinataire');
            $this->criterTri['MAIL_DESTINATAIRE'] = $arrayDestinataire[$this->getViewState('MAIL_DESTINATAIRE')]; //$this->IdDestinataireFiltre->SelectedValue
        } else {
            $this->criterTri['MAIL_DESTINATAIRE'] = '';
        }
        if (0 != $this->getViewState('FORMAT')) {
            $this->criterTri['FORMAT'] = $this->getViewState('FORMAT'); //$this->idFormat->SelectedValue
        } else {
            $this->criterTri['FORMAT'] = '';
        }
        if (0 != $this->getViewState('DATE_MESSAGE')) {
            $arrayHeure = $this->getViewState('arrayHeure');
            $this->criterTri['DATE_MESSAGE'] = Atexo_Util::frnDate2iso($arrayHeure[$this->getViewState('DATE_MESSAGE')]); //$this->idDateHeureFiltre->SelectedValue
        } else {
            $this->criterTri['DATE_MESSAGE'] = '';
        }
        if ($resultLimeted) {
            $this->criterTri['limit'] = $this->getViewState('limit');
            $this->criterTri['offSet'] = $this->getViewState('offSet');
        }
        $nombreElementTotal = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $org, $this->criterTri, false);
        $destinataires = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $org, $this->criterTri, true, $resultLimeted);
        if ($nombreElementTotal >= 1) {
            foreach ($destinataires as $destinataire) {
                $PJ = '';
                $Result = [];
                if ($destinataire['FORMAT'] != Atexo_Config::getParameter('ECHANGE_PLATE_FORME')) {
                    $Pjs = Atexo_Message::getPjByIdEchange($destinataire['ID_ECHANGE'], $org);
                    if (is_array($Pjs)) {
                        foreach ($Pjs as $pj) {
                            $Result[] = '<a href="?page=Agent.DownloadPjEchange&idPj='.$pj->getPiece().'">'.$pj->getNomFichier().'</a>';
                            $nomPjs[] = $pj->getNomFichier();
                        }
                    }
                    if (is_array($Result) && 0 != count($Result)) {
                        $PJ = '<br>'.implode('<br>', $Result);
                    }
                    if (is_array($nomPjs) && 0 != count($nomPjs)) {
                        $nomPJs = implode('\n', $nomPjs);
                    }
                }
                if ($destinataire['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE')) {
                    $destinataire['TypeAccuse'] = Atexo_Util::iso2frnDateTime($destinataire['DATE_AR']);
                } elseif ($destinataire['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')) {
                    $destinataire['TypeAccuse'] = $this->arrayTypeAR[Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')];
                } else {
                    $destinataire['TypeAccuse'] = '-';
                }
                if ($destinataire['FORMAT'] != Atexo_Config::getParameter('ECHANGE_PLATE_FORME')) {
                    $destinataire['PJ'] = $PJ;
                    $destinataire['nomPjs'] = $nomPJs;
                } else {
                    $destinataire['PJ'] = '<br><a href="javascript:popUp(\'?page=Agent.PopUpDetailMail&idMsgDest='.$destinataire['ID'].'&orgAcronyme='.$acrOrg."'".',\'yes\');">'.Prado::localize('DEFINE_TEXT_VISUALISATION_MESSAGE').'</a>';
                }
                $destinataire['FormatText'] = $this->arrayFormat[$destinataire['FORMAT']];
                $arrayDests[] = $destinataire;
            }
            if (!$resultLimeted) {
                return $arrayDests;
            }
            //$this->numPageTop->Text=1;
            //$this->numPageBottom->Text=1;
            //$this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelMoreThanOneElementFound->setStyle('display:block');
            $this->panelNoElementFound->setStyle('display:none');
            $this->addLinkIfNoElementFound->setStyle('display:none');
            $this->nombreElement->Text = $nombreElementTotal;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->nombrePageTop->Text = ceil($nombreElementTotal / $this->RepeaterListEchanges->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElementTotal / $this->RepeaterListEchanges->PageSize);
            $this->RepeaterListEchanges->setVirtualItemCount($nombreElementTotal);
            $this->RepeaterListEchanges->dataSource = $arrayDests;
            $this->RepeaterListEchanges->dataBind();
        } else {
            // $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelMoreThanOneElementFound->setStyle('display:none');
            $this->panelNoElementFound->setStyle('display:block');
            $this->addLinkIfNoElementFound->setStyle('display:block');
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $iddest = $param->CommandParameter;
        Atexo_Message::deleteObjetDestinataire($iddest);
    }

    public function refreshRepeaterEchange($sender, $param)
    {
        $this->remplirDataSource(true);
    }

    /**
     * Trier un tableau des échanges.
     *
     * @param sender
     * @param param
     */
    public function sortEchange($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');

        if ('De' == $sender->getActiveControl()->CallbackParameter) {
            $this->setCritereAndTypeTri('De');
            $this->criterTri['De'] = $this->triAscDesc;
        } elseif ('A' == $sender->getActiveControl()->CallbackParameter) {
            $this->setCritereAndTypeTri('A');
            $this->criterTri['A'] = $this->triAscDesc;
        } elseif ('DATE' == $sender->getActiveControl()->CallbackParameter) {
            $this->setCritereAndTypeTri('DATE');
            $this->criterTri['DATE'] = $this->triAscDesc;
        } else {
            return;
        }
        $dataSourceTriee = $this->remplirTableauListeEchanges($this->idRef, Atexo_CurrentUser::getCurrentOrganism(), $this->criterTri);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->RepeaterListEchanges->DataSource = $dataSourceTriee;
        $this->RepeaterListEchanges->DataBind();
        // Re-affichage du TActivePanel
        $this->PanelListEchanges->render($param->getNewWriter());
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterListEchanges->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->populateData();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterListEchanges->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTop':
                $pageSize = $this->listePageSizeTop->getSelectedValue();
                $this->listePageSizeBottom->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottom':
                $pageSize = $this->listePageSizeBottom->getSelectedValue();
                $this->listePageSizeTop->setSelectedValue($pageSize);
                break;
        }
        // echo $pageSize;exit;
        $this->RepeaterListEchanges->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterListEchanges->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterListEchanges->PageSize);
        $this->RepeaterListEchanges->setCurrentPageIndex(0);
        $this->populateData();
    }

    public function displayListeObjet($idRef, $org)
    {
        $objets = Atexo_Message::getAllObjetsEchanges($idRef);

        array_unshift($objets, Prado::localize('TEXT_SELECTIONNEZ'));
        $this->setViewState('arrayObjet', $objets);
        $this->arrayObjet = $objets;
        $this->idObjet->DataSource = $objets;
        $this->idObjet->DataBind();
        if ($this->getViewState('OBJET')) {
            $this->idObjet->SelectedValue = $this->getViewState('OBJET');
        }
    }

    public function displayListeFormat()
    {
        if ($this->arrayFormat) {
            $formats = $this->arrayFormat;
            array_unshift($formats, Prado::localize('TEXT_SELECTIONNEZ'));
            $this->setViewState('arrayFormat', $formats);
            $this->idFormat->DataSource = $formats;
            $this->idFormat->DataBind();
            if ($this->getViewState('FORMAT')) {
                $this->idFormat->SelectedValue = $this->getViewState('FORMAT');
            }
        }
    }

    //recuperer la liste des heures et dates
    public function displayListeDatesMessage($idRef, $org)
    {
        $criterTri = [];
        $criterTri['De'] = 'ASC';
        $datesMessage = Atexo_Message::getAllDatesMessage($idRef, $criterTri);
        array_unshift($datesMessage, Prado::localize('TEXT_SELECTIONNEZ'));
        $this->setViewState('arrayHeure', $datesMessage);
        $this->arrayHeure = $datesMessage;
        $this->idDateHeureFiltre->DataSource = $datesMessage;
        $this->idDateHeureFiltre->DataBind();
        if ($this->getViewState('DATE_MESSAGE')) {
            $this->idDateHeureFiltre->SelectedValue = $this->getViewState('DATE_MESSAGE');
        }
    }

    //recuperer la liste des Expediteurs
    public function displayListeMailDestinataires($idRef, $org)
    {
        $criterTri = [];
        $criterTri['De'] = 'ASC';
        $expediteurs = Atexo_Message::getAllMailDestinataire($idRef, $criterTri);
        array_unshift($expediteurs, Prado::localize('TEXT_SELECTIONNEZ'));
        $this->setViewState('arrayDestinataire', $expediteurs);
        $this->arrayDestinataire = $expediteurs;
        $this->IdDestinataireFiltre->DataSource = $expediteurs;
        $this->IdDestinataireFiltre->DataBind();

        if ($this->getViewState('MAIL_DESTINATAIRE')) {
            $this->IdDestinataireFiltre->SelectedValue = $this->getViewState('MAIL_DESTINATAIRE');
        }
    }

    public function populateData($appelRecherche = false)
    {
        if (!$appelRecherche) {
            $offset = $this->RepeaterListEchanges->CurrentPageIndex * $this->RepeaterListEchanges->PageSize;
            $limit = $this->RepeaterListEchanges->PageSize;
            if ($offset + $limit > $this->RepeaterListEchanges->getVirtualItemCount()) {
                $limit = $this->RepeaterListEchanges->getVirtualItemCount() - $offset;
            }
            $this->setViewState('offSet', $offset);
            $this->setViewState('limit', $limit);
        }
        $this->remplirTableauListeEchanges(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), true);
    }

    public function changeObjet($sender, $param)
    {
        $this->setViewState('OBJET', $this->idObjet->SelectedValue);
    }

    public function changeDestinataireFiltre()
    {
        $this->setViewState('MAIL_DESTINATAIRE', $this->IdDestinataireFiltre->SelectedValue);
    }

    public function changeFormat()
    {
        $this->setViewState('FORMAT', $this->idFormat->SelectedValue);
    }

    public function changeDateHeureFiltre()
    {
        $this->setViewState('DATE_MESSAGE', $this->idDateHeureFiltre->SelectedValue);
    }

    public function onCallBackRefreshEchange($sender, $param)
    {
        $this->remplirDataSource();
        $this->panelMoreThanOneElementFound->render($param->getNewWriter());
        $this->panelNoElementFound->render($param->getNewWriter());
        $this->addLinkIfNoElementFound->render($param->getNewWriter());
    }

    public function remplirDataSource($appelRecherche = false)
    {
        $this->setViewState('OBJET', $this->idObjet->SelectedValue);
        $this->setViewState('MAIL_DESTINATAIRE', $this->IdDestinataireFiltre->SelectedValue);
        $this->setViewState('FORMAT', $this->idFormat->SelectedValue);
        $this->setViewState('DATE_MESSAGE', $this->idDateHeureFiltre->SelectedValue);

        $this->criterTri['offSet'] = '0';
        $this->criterTri['limit'] = $this->RepeaterListEchanges->PageSize;
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->RepeaterListEchanges->PageSize);
        $this->populateData($appelRecherche);
        $this->numPageTop->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
        $this->numPageBottom->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
    }

    /*
     * Verifie si l'agent connecté a l'habilitation modifier une consultation
     * @return true s'il a l'habilitation, non sinon
     */
    public function hasHabilitationModifierConsultation()
    {
        if ($this->_consultation->getCurrentUserReadOnly()) {
            return false;
        }
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE')) {
            return Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation');
        } else {
            return $this->_consultation->hasHabilitationModifierApresValidation();
        }
    }

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function getDocResultSearch($sender, $param)
    {
        $result = $this->remplirTableauListeEchanges(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), true, false);
        if ('excel' == $param->CommandName) {
            //avoir le doc sous format Excel
            //print_r($result);exit;
            (new Atexo_GenerationExcel())->generateExcelRegistreEchange(Atexo_Util::atexoHtmlEntities($_GET['id']), $result);
        } elseif ('pdf' == $param->CommandName) {
            //avoir le doc sous format Pdf
            $filePath = (new Atexo_GenerationExcel())->generateExcelRegistreEchange(Atexo_Util::atexoHtmlEntities($_GET['id']), $result, true);
            if ($filePath) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
                $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $nomFichier = Prado::localize('DEFINE_SUIVI_MESSAGES').'_'.$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf';
                @unlink($filePath);
                DownloadFile::downloadFileContent($nomFichier, $pdfContent);
            }
        }
    }
}
