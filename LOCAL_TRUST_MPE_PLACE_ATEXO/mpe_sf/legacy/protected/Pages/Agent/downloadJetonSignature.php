<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-4.0
 */
class downloadJetonSignature extends MpeTPage
{
    public ?string $_typeSignature = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['id']) && isset($_GET['idfichier']) && isset($_GET['enveloppe'])) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById(Atexo_Util::atexoHtmlEntities($_GET['enveloppe']), Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
                if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $organisme = Atexo_CurrentUser::getCurrentOrganism();
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                    if ($enveloppe instanceof CommonEnveloppe) {
                        $offre = CommonOffresPeer::retrieveByPK($enveloppe->getOffreId(), $organisme, $connexionCom);
                        if ($offre instanceof CommonOffres) {
                            $xmlStr = $offre->getXmlString();
                            $pos = strpos($xmlStr, 'typeSignature');
                            if ($pos > 0) {
                                $this->_typeSignature = 'XML';
                            }
                        }
                    }

                    $c = new Criteria();
                    $c->add(CommonFichierEnveloppePeer::ID_FICHIER, Atexo_Util::atexoHtmlEntities($_GET['idfichier']));
                    $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                    $fichier = CommonFichierEnveloppePeer::doSelectOne($c, $connexionCom);
                    if ($fichier instanceof CommonFichierEnveloppe) {
                        $signature = $fichier->getSignatureFichier();
                        $signtaureFileName = $fichier->getNomFichierSignature();
                        $directory = Atexo_Config::getParameter('COMMON_TMP').session_name().session_id().time().'/';
                        mkdir($directory);
                        Atexo_Util::writeFile($directory.$signtaureFileName, base64_decode($signature));

                        header('Pragma: public');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                        header('Cache-Control: private', false);
                        //                      header("Content-Type: application/zip");
                        header('Content-Disposition: attachment; filename="'.$signtaureFileName.'";');
                        header('Content-Transfer-Encoding: binary');
                        header('Content-Length: '.filesize($directory.$signtaureFileName));
                        echo file_get_contents($directory.$signtaureFileName);
                        @unlink($directory.$signtaureFileName);
                        @rmdir($directory);
                        exit();
                    } else {
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_SIGNATURE_NON_DISPONIBLE'));
                        Prado::log('le fichier '.Atexo_Util::atexoHtmlEntities($_GET['idfichier'])."de l'enveloppe ".Atexo_Util::atexoHtmlEntities($_GET['enveloppe'])."n'existe pas", TLogger::ERROR, 'ERREUR');
                    }
                }
            }
        }
    }
}
