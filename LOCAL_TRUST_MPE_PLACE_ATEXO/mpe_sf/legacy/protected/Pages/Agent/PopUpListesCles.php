<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpListesCles extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $typeEnv = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);
        $refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($refConsultation);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (!(is_array($consultationArray) && 1 == count($consultationArray))) {
            exit;
        }
        $consultation = array_shift($consultationArray);
        $this->reference->Text = $consultation->getReferenceUtilisateur();
        $cles = (new Atexo_Certificat())->retrieveCertificatChiffrement($refConsultation, $typeEnv, Atexo_CurrentUser::getCurrentOrganism());
        if (!is_array($cles) || 0 == count($cles)) {
            $cles = [];
            $this->entete->Text = Prado::localize('TEXT_AUCUNE_CLE_ENVELOPPE');
        } else {
            $this->entete->Text = Prado::localize('TEXT_CLE_AFFECTEES_ENVELOPPE');
        }

        switch ($typeEnv) {
            case 1:
                $texteTypeEnv = Prado::localize('DEFINE_ENVELOPPE_CONDIDATURE');
                break;

            case 2:
                $texteTypeEnv = Prado::localize('DEFINE_ENVELOPPE_OFFRE');
                break;

            case 3:
                $texteTypeEnv = Prado::localize('DEFINE_TROISIEME_ENVELOPPE');
                break;

            case 4:
                $texteTypeEnv = Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE');
                break;

            default:
                exit;
        }

        $this->cles->DataSource = $cles;
        $this->cles->DataBind();
        $this->typeEnv->Text = $texteTypeEnv;
    }
}
