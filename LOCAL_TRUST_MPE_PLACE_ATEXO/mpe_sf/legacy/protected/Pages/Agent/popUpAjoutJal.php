<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonJAL;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe popUpAjoutJal.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpAjoutJal extends MpeTPage
{
    protected $_idService;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idService'])) {
            $this->_idService = Atexo_Util::atexoHtmlEntities($_GET['idService']);
        }
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                self::displayJal(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
        }

        $this->customizeForm();
        if (Atexo_Module::isEnabled('GestionAdressesFacturationJAL')) {
            $this->panelInfosComplementaires->setVisible(false);
        } else {
            $this->panelInfosComplementaires->setVisible(true);
        }
    }

    public function saveJal($sender, $param)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $jalBean = new CommonJAL();
            $jalBean->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            if (!isset($_GET['id'])) {
                $jalBean->setServiceId(empty($this->_idService) ? null : $this->_idService);
            } else {
                $jalBean = Atexo_Jal::retrieveJalById(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }

            $jalBean->setNom($this->nomFournisseur->Text);
            $jalBean->setEmail(trim($this->mailFournisseur->Text));
            $jalBean->setEmailAr(trim($this->emailAr->Text));
            $jalBean->setTelecopie($this->telecopieur->Text);
            $jalBean->setInformationFacturation($this->infosFacturation->Text);

            $jalBean->save($connexionCom);

            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateJal').click();window.close();</script>";
        } catch (Exception $e) {
            Prado::log("Impossible de creer un Jal'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function displayJal($IdJal)
    {
        $JalBean = Atexo_Jal::retrieveJalById($IdJal);

        if ($JalBean) {
            $this->nomFournisseur->Text = $JalBean->getNom();
            $this->mailFournisseur->Text = $JalBean->getEmail();
            $this->emailAr->Text = $JalBean->getEmailAr();
            $this->telecopieur->Text = $JalBean->getTelecopie();
            $this->infosFacturation->Text = $JalBean->getInformationFacturation();
        }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
            $this->validateur2->Enabled = false;
        }
    }
}
