<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Enchere;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Enchere\Atexo_Enchere_CriteriaVo;
use Prado\Prado;

/**
 * Class pour la recherche d'une enchêre.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class RechercherEncheres extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->formulaireRecherche->setVisible(true);
            $this->tableauDeBord->setVisible(false);

            $this->loadStatus();
        }
    }

    public function loadStatus()
    {
        $statuts = [];
        $statuts[0] = Prado::localize('TOUS_LES_STATUS');
        $statuts[Atexo_Config::getParameter('ENCHERE_EN_ATTENTE')] = Prado::localize('EN_ATTENTE');
        $statuts[Atexo_Config::getParameter('ENCHERE_EN_COURS')] = Prado::localize('EN_COURS');
        $statuts[Atexo_Config::getParameter('ENCHERE_SUSPENDUE')] = Prado::localize('SUSPENDUE');
        $statuts[Atexo_Config::getParameter('ENCHERE_CLOTUREE')] = Prado::localize('TEXT_CLOTUREE');

        $this->statuts->setDataSource($statuts);
        $this->statuts->dataBind();
    }

    public function getNbrResultats()
    {
    }

    public function pageChanged($sender, $param)
    {
        $this->repeaterEncheres->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->populateData($criteriaVo);
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->repeaterEncheres->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterEncheres->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterEncheres->PageSize);
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->repeaterEncheres->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function populateData(Atexo_Enchere_CriteriaVo $criteriaVo)
    {
        $offset = $this->repeaterEncheres->CurrentPageIndex * $this->repeaterEncheres->PageSize;
        $limit = $this->repeaterEncheres->PageSize;
        if ($offset + $limit > $this->repeaterEncheres->getVirtualItemCount()) {
            $limit = $this->repeaterEncheres->getVirtualItemCount() - $offset;
        }
        $enchere = new Atexo_Enchere();

        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);

        $arrayEncheres = $enchere->search($criteriaVo);
        $this->repeaterEncheres->DataSource = $arrayEncheres;
        $this->repeaterEncheres->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterEncheres->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('criteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->repeaterEncheres->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->repeaterEncheres->CurrentPageIndex + 1;
        }
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->getCommandName();
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('criteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('criteriaVo', $criteriaVo);
        $this->repeaterEncheres->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function afficherResultats()
    {
        $this->formulaireRecherche->setVisible(false);
        $this->tableauDeBord->setVisible(true);

        $criteriaVo = new Atexo_Enchere_CriteriaVo();

        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());

        if (trim($this->referenceConsultation->Text)) {
            $criteriaVo->setRefConsultation($this->referenceConsultation->Text);
        }

        if (trim($this->keywordSearch->Text)) {
            $criteriaVo->setMotclef(trim($this->keywordSearch->Text));
        }

        if (trim($this->referenceEnchere->Text)) {
            $criteriaVo->setReferenceUtilisateur(trim($this->referenceEnchere->Text));
        }

        if ('0' != $this->statuts->getSelectedValue()) {
            if ($this->statuts->getSelectedValue() == Atexo_Config::getParameter('ENCHERE_EN_ATTENTE')) {
                $criteriaVo->setStatuts(Atexo_Config::getParameter('ENCHERE_EN_ATTENTE'));
            } elseif ($this->statuts->getSelectedValue() == Atexo_Config::getParameter('ENCHERE_EN_COURS')) {
                $criteriaVo->setStatuts(Atexo_Config::getParameter('ENCHERE_EN_COURS'));
            } elseif ($this->statuts->getSelectedValue() == Atexo_Config::getParameter('ENCHERE_SUSPENDUE')) {
                $criteriaVo->setStatuts(Atexo_Config::getParameter('ENCHERE_SUSPENDUE'));
            } elseif ($this->statuts->getSelectedValue() == Atexo_Config::getParameter('ENCHERE_CLOTUREE')) {
                $criteriaVo->setStatuts(Atexo_Config::getParameter('ENCHERE_CLOTUREE'));
            }
        }

        if ($this->dateDebutStart->Text) {
            $criteriaVo->setDateDebut1(Atexo_Util::frnDateTime2iso($this->dateDebutStart->Text));
        }
        if ($this->dateDebutEnd->Text) {
            $criteriaVo->setDateDebut2(Atexo_Util::frnDateTime2iso($this->dateDebutEnd->Text));
        }
        if ($this->dateFinStart->Text) {
            $criteriaVo->setDateFin1(Atexo_Util::frnDateTime2iso($this->dateFinStart->Text));
        }
        if ($this->dateFinEnd->Text) {
            $criteriaVo->setDateFin2(Atexo_Util::frnDateTime2iso($this->dateFinEnd->Text));
        }

        // Cas de l'organisation centralisée
        if (!Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
            //Service de l'agent connecté
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            //Recuperation des descendances du service de l'agent connecté
            $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme());
            $criteriaVo->setIdsServices($listOfServicesAllowed);
        } else {
            //Service de l'agent connecté
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        }

        $this->setViewState('criteriaVo', $criteriaVo);

        $arrayEncheres = (new Atexo_Enchere())->search($criteriaVo);
        $nombreElement = is_countable($arrayEncheres) ? count($arrayEncheres) : 0;

        if ($nombreElement >= 1) {
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterEncheres->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterEncheres->PageSize);
            $this->repeaterEncheres->setVirtualItemCount($nombreElement);
            $this->repeaterEncheres->setCurrentPageIndex(0);

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nbrResultats->Text = $nombreElement;
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }

        $this->populateData($criteriaVo);
    }

    public function editEnchere($sender, $param)
    {
        $idEnchere = $sender->getParent()->idEnchere->Value;
        $this->response->redirect('index.php?page=Agent.GererEnchere&refEnchere='.base64_encode($idEnchere));
    }

    public function suivreEnchere($sender, $param)
    {
        $idEnchere = $sender->getParent()->idEnchere->Value;
        $this->response->redirect('index.php?page=Agent.SuivreEnchere&idEnchere='.$idEnchere);
    }
}
