<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonMesureAvancementPeer;
use Application\Propel\Mpe\CommonValeurReferentielOrgPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_MesuresAvancement;
use DOMDocument;
use Prado\Prado;

/**
 * Classe StatistiquesMesureAvancementFormulaire.
 *
 * @author nabil bellahcen <nabil.bellahcen@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesMesureAvancementFormulaire extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                $MesureAvancement = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementById(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                //$domXml->loadXML(file_get_contents('./protected/pages/agent/aaaa.xml'));
                if ($MesureAvancement->getXml()) {
                    $domXml = new DOMDocument();
                    //$domXml->loadXML(file_get_contents('./protected/pages/agent/aaaa.xml'));
                    $domXml->loadXML(utf8_encode($MesureAvancement->getXml()));
                    $this->setViewState('domXml', $domXml->saveXML());
                }
                $this->displayDonnees('TRepeaterAAPC', 'AAPC');
                $this->displayDonnees('TRepeaterDCE', 'DCE');
                $this->displayDonnees('TRepeaterCand', 'Cand');
                $this->displayDonnees('TRepeaterOffre', 'Offre');
                $this->displayDonnees('TRepeaterNotifie', 'Notifie');
            }
        }
        $this->panelMessageErreur->setVisible(false);
        $this->panelMessage->setVisible(false);
    }

    public function retreiveDataXml($parm, $parmOnglet)
    {
        if (isset($_GET['id'])) {
            $xml = $this->getViewState('domXml');
            $domXml = new DOMDocument();
            $domXml->loadXML($xml);
            $arrayData = null;
            $domActes = $domXml->getElementsByTagName('Acte');
            $j = 0;
            foreach ($domActes as $domActe) {
                $domTypeMarche = $domActe->getElementsByTagName('MesureAchatPublicTypeMarche');
                if (0 == strcmp($domTypeMarche->item(0)->nodeValue, $parm)) {
                    $arrayData[$parmOnglet][$j]['MesureAchatPublicTypeMarche'] = $parm;
                    $domTypeProcedure = $domActe->getElementsByTagName('MesureAchatPublicTypeProcedure');
                    $arrayData[$parmOnglet][$j]['MesureAchatPublicTypeProcedure'] = $domTypeProcedure->item(0)->nodeValue;

                    $domAvis = $domActe->getElementsByTagName('Avis');
                    foreach ($domAvis as $oneAvis) {
                        $tagNameAvis = $oneAvis->getElementsByTagName('MesureAchatPublicNombreProcedure');
                        $arrayData[$parmOnglet][$j]['Avis']['MesureAchatPublicNombreProcedure'] = $tagNameAvis->item(0)->nodeValue;

                        $tagNameAvis = $oneAvis->getElementsByTagName('MesureAchatPublicNombreLot');
                        $arrayData[$parmOnglet][$j]['Avis']['MesureAchatPublicNombreLot'] = $tagNameAvis->item(0)->nodeValue;

                        $tagNameAvis = $oneAvis->getElementsByTagName('MesureAchatPublicNombreAvisPapier');
                        $arrayData[$parmOnglet][$j]['Avis']['MesureAchatPublicNombreAvisPapier'] = $tagNameAvis->item(0)->nodeValue;

                        $tagNameAvis = $oneAvis->getElementsByTagName('MesureAchatPublicNombreAvisDemat');
                        $arrayData[$parmOnglet][$j]['Avis']['MesureAchatPublicNombreAvisDemat'] = $tagNameAvis->item(0)->nodeValue;
                    }

                    $domDossierConsultationEntreprise = $domActe->getElementsByTagName('DossierConsultationEntreprise');
                    foreach ($domDossierConsultationEntreprise as $oneDossierConsultationEntreprise) {
                        $tagNameDossierConsultationEntreprise = $oneDossierConsultationEntreprise->getElementsByTagName('MesureAchatPublicNombreProcedure');
                        $arrayData[$parmOnglet][$j]['DossierConsultationEntreprise']['MesureAchatPublicNombreProcedure'] = $tagNameDossierConsultationEntreprise->item(0)->nodeValue;

                        $tagNameDossierConsultationEntreprise = $oneDossierConsultationEntreprise->getElementsByTagName('MesureAchatPublicNombreLot');
                        $arrayData[$parmOnglet][$j]['DossierConsultationEntreprise']['MesureAchatPublicNombreLot'] = $tagNameDossierConsultationEntreprise->item(0)->nodeValue;

                        $tagNameDossierConsultationEntreprise = $oneDossierConsultationEntreprise->getElementsByTagName('MesureAchatPublicNombreDossierPapier');
                        $arrayData[$parmOnglet][$j]['DossierConsultationEntreprise']['MesureAchatPublicNombreDossierPapier'] = $tagNameDossierConsultationEntreprise->item(0)->nodeValue;

                        $tagNameDossierConsultationEntreprise = $oneDossierConsultationEntreprise->getElementsByTagName('MesureAchatPublicNombreDossierMixte');
                        $arrayData[$parmOnglet][$j]['DossierConsultationEntreprise']['MesureAchatPublicNombreDossierMixte'] = $tagNameDossierConsultationEntreprise->item(0)->nodeValue;

                        $tagNameDossierConsultationEntreprise = $oneDossierConsultationEntreprise->getElementsByTagName('MesureAchatPublicNombreDossierDemat');
                        $arrayData[$parmOnglet][$j]['DossierConsultationEntreprise']['MesureAchatPublicNombreDossierDemat'] = $tagNameDossierConsultationEntreprise->item(0)->nodeValue;
                    }

                    $domCandidature = $domActe->getElementsByTagName('Candidature');
                    foreach ($domCandidature as $oneCandidature) {
                        $tagNameCandidature = $oneCandidature->getElementsByTagName('MesureAchatPublicNombreProcedure');
                        $arrayData[$parmOnglet][$j]['Candidature']['MesureAchatPublicNombreProcedure'] = $tagNameCandidature->item(0)->nodeValue;

                        $tagNameCandidature = $oneCandidature->getElementsByTagName('MesureAchatPublicNombreLot');
                        $arrayData[$parmOnglet][$j]['Candidature']['MesureAchatPublicNombreLot'] = $tagNameCandidature->item(0)->nodeValue;

                        $tagNameCandidature = $oneCandidature->getElementsByTagName('MesureAchatPublicNombreCandidaturePapier');
                        $arrayData[$parmOnglet][$j]['Candidature']['MesureAchatPublicNombreCandidaturePapier'] = $tagNameCandidature->item(0)->nodeValue;

                        $tagNameCandidature = $oneCandidature->getElementsByTagName('MesureAchatPublicNombreCandidatureDemat');
                        $arrayData[$parmOnglet][$j]['Candidature']['MesureAchatPublicNombreCandidatureDemat'] = $tagNameCandidature->item(0)->nodeValue;
                    }

                    $domOffre = $domActe->getElementsByTagName('Offre');
                    foreach ($domOffre as $oneOffre) {
                        $tagNameOffre = $oneOffre->getElementsByTagName('MesureAchatPublicNombreProcedure');
                        $arrayData[$parmOnglet][$j]['Offre']['MesureAchatPublicNombreProcedure'] = $tagNameOffre->item(0)->nodeValue;

                        $tagNameOffre = $oneOffre->getElementsByTagName('MesureAchatPublicNombreLot');
                        $arrayData[$parmOnglet][$j]['Offre']['MesureAchatPublicNombreLot'] = $tagNameOffre->item(0)->nodeValue;

                        $tagNameOffre = $oneOffre->getElementsByTagName('MesureAchatPublicNombreOffrePapier');
                        $arrayData[$parmOnglet][$j]['Offre']['MesureAchatPublicNombreOffrePapier'] = $tagNameOffre->item(0)->nodeValue;

                        $tagNameOffre = $oneOffre->getElementsByTagName('MesureAchatPublicNombreOffreDemat');
                        $arrayData[$parmOnglet][$j]['Offre']['MesureAchatPublicNombreOffreDemat'] = $tagNameOffre->item(0)->nodeValue;
                    }

                    $domNotifie = $domActe->getElementsByTagName('MarcheNotifie');
                    foreach ($domNotifie as $oneNotifie) {
                        $tagNameNotifie = $oneNotifie->getElementsByTagName('MesureAchatPublicNombreProcedure');
                        $arrayData[$parmOnglet][$j]['MarcheNotifie']['MesureAchatPublicNombreProcedure'] = $tagNameNotifie->item(0)->nodeValue;

                        $tagNameNotifie = $oneNotifie->getElementsByTagName('MesureAchatPublicNombreLot');
                        $arrayData[$parmOnglet][$j]['MarcheNotifie']['MesureAchatPublicNombreLot'] = $tagNameNotifie->item(0)->nodeValue;

                        $tagNameNotifie = $oneNotifie->getElementsByTagName('MesureAchatPublicNombreMarcheNotifiePapier');
                        $arrayData[$parmOnglet][$j]['MarcheNotifie']['MesureAchatPublicNombreMarcheNotifiePapier'] = $tagNameNotifie->item(0)->nodeValue;

                        $tagNameNotifie = $oneNotifie->getElementsByTagName('MesureAchatPublicNombreMarcheNotifieDemat');
                        $arrayData[$parmOnglet][$j]['MarcheNotifie']['MesureAchatPublicNombreMarcheNotifieDemat'] = $tagNameNotifie->item(0)->nodeValue;
                    }
                    ++$j;
                }
            }

            return $arrayData[$parmOnglet];
        }
    }

    public function CreateXml()
    {
        $blockTypeProcedure = null;
        if (isset($_GET['id'])) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            for ($i = 0; $i < (is_countable($this->TRepeaterAAPC->Items) ? count($this->TRepeaterAAPC->Items) : 0); ++$i) {
                $blockTypeProcedure .= '
<ram:Acte>';
                $blockTypeProcedure .= '
<ram:MesureAchatPublicTypeMarche>'.$this->TRepeaterAAPC->Items[$i]->AAPCTypeMarche->Text.'</ram:MesureAchatPublicTypeMarche>';

                $blockTypeProcedure .= '
<ram:MesureAchatPublicTypeProcedure>'.utf8_encode(($this->TRepeaterAAPC->Items[$i]->AAPCTypeProcedure->Text)).'</ram:MesureAchatPublicTypeProcedure>';

                $blockTypeProcedure .= '
								<ram:Avis>
<ram:MesureAchatPublicNombreProcedure>'.($this->TRepeaterAAPC->Items[$i]->AAPCNombreProcedure->Text).'</ram:MesureAchatPublicNombreProcedure>
<ram:MesureAchatPublicNombreLot>'.($this->TRepeaterAAPC->Items[$i]->AAPCNombreLot->Text).'</ram:MesureAchatPublicNombreLot>
<ram:MesureAchatPublicNombreAvisPapier>'.($this->TRepeaterAAPC->Items[$i]->AAPCNombreAvisPapier->Text).'</ram:MesureAchatPublicNombreAvisPapier>
<ram:MesureAchatPublicNombreAvisDemat>'.($this->TRepeaterAAPC->Items[$i]->AAPCNombreAvisDemat->Text).'</ram:MesureAchatPublicNombreAvisDemat>
</ram:Avis>';
                $blockTypeProcedure .= '
								<ram:DossierConsultationEntreprise>
<ram:MesureAchatPublicNombreProcedure>'.($this->TRepeaterDCE->Items[$i]->DCENombreProcedure->Text).'</ram:MesureAchatPublicNombreProcedure>
<ram:MesureAchatPublicNombreLot>'.($this->TRepeaterDCE->Items[$i]->DCENombreLot->Text).'</ram:MesureAchatPublicNombreLot>
<ram:MesureAchatPublicNombreDossierPapier>'.($this->TRepeaterDCE->Items[$i]->DCENombreDossierPapier->Text).'</ram:MesureAchatPublicNombreDossierPapier>
<ram:MesureAchatPublicNombreDossierMixte>'.($this->TRepeaterDCE->Items[$i]->DCENombreDossierMixte->Text).'</ram:MesureAchatPublicNombreDossierMixte>
<ram:MesureAchatPublicNombreDossierDemat>'.($this->TRepeaterDCE->Items[$i]->DCENombreDossierDemat->Text).'</ram:MesureAchatPublicNombreDossierDemat>
</ram:DossierConsultationEntreprise>';
                $blockTypeProcedure .= '
								<ram:Candidature>
<ram:MesureAchatPublicNombreProcedure>'.($this->TRepeaterCand->Items[$i]->CandNombreProcedure->Text).'</ram:MesureAchatPublicNombreProcedure>
<ram:MesureAchatPublicNombreLot>'.($this->TRepeaterCand->Items[$i]->CandNombreLot->Text).'</ram:MesureAchatPublicNombreLot>
<ram:MesureAchatPublicNombreCandidaturePapier>'.($this->TRepeaterCand->Items[$i]->CandNombreAvisPapier->Text).'</ram:MesureAchatPublicNombreCandidaturePapier>
<ram:MesureAchatPublicNombreCandidatureDemat>'.($this->TRepeaterCand->Items[$i]->CandNombreAvisDemat->Text).'</ram:MesureAchatPublicNombreCandidatureDemat>
</ram:Candidature>';
                $blockTypeProcedure .= '
								<ram:Offre>
<ram:MesureAchatPublicNombreProcedure>'.($this->TRepeaterOffre->Items[$i]->OffreNombreProcedure->Text).'</ram:MesureAchatPublicNombreProcedure>
<ram:MesureAchatPublicNombreLot>'.($this->TRepeaterOffre->Items[$i]->OffreNombreLot->Text).'</ram:MesureAchatPublicNombreLot>
<ram:MesureAchatPublicNombreOffrePapier>'.($this->TRepeaterOffre->Items[$i]->OffreNombreAvisPapier->Text).'</ram:MesureAchatPublicNombreOffrePapier>
<ram:MesureAchatPublicNombreOffreDemat>'.($this->TRepeaterOffre->Items[$i]->OffreNombreAvisDemat->Text).'</ram:MesureAchatPublicNombreOffreDemat>
</ram:Offre>';
                $blockTypeProcedure .= '
								<ram:MarcheNotifie>
<ram:MesureAchatPublicNombreProcedure>'.($this->TRepeaterNotifie->Items[$i]->OffreNombreProcedure->Text).'</ram:MesureAchatPublicNombreProcedure>
<ram:MesureAchatPublicNombreLot>'.($this->TRepeaterNotifie->Items[$i]->OffreNombreLot->Text).'</ram:MesureAchatPublicNombreLot>
<ram:MesureAchatPublicNombreMarcheNotifiePapier>'.($this->TRepeaterNotifie->Items[$i]->OffreNombreAvisPapier->Text).'</ram:MesureAchatPublicNombreMarcheNotifiePapier>
<ram:MesureAchatPublicNombreMarcheNotifieDemat>'.($this->TRepeaterNotifie->Items[$i]->OffreNombreAvisDemat->Text).'</ram:MesureAchatPublicNombreMarcheNotifieDemat>
</ram:MarcheNotifie>';
                $blockTypeProcedure .= '</ram:Acte> ';
            }

            $MesureAvancement = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementById(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            $keyTab = [];
            $keyTab['<!--_SIRET_-->'] = $MesureAvancement->getSiren().$MesureAvancement->getNic();
            $keyTab['<!--_TypeService_-->'] = utf8_encode($MesureAvancement->getIdentifiantService());

            if ($MesureAvancement->getDepartement()) {
                $Depart = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($MesureAvancement->getDepartement());
                $arrayDepartExplod1 = explode('(', $Depart);
                $keyTab['<!--_MesureAchatPublicDepartement_-->'] = utf8_encode($Depart);
                if ($arrayDepartExplod1) {
                    $arrayDepartExplod2 = explode(')', $arrayDepartExplod1[1]);

                    if ($arrayDepartExplod2[1]) {
                        $keyTab['<!--_MesureAchatPublicDepartement_-->'] = utf8_encode($arrayDepartExplod2[0]);
                    }
                }
            } else {
                $keyTab['<!--_MesureAchatPublicDepartement_-->'] = '';
            }

            if ($MesureAvancement->getMail()) {
                $keyTab['<!--_AdresseMelDemandeStat_-->'] = $MesureAvancement->getMail();
                $keyTab['<!--_AdresseMelContactTechnique_-->'] = $MesureAvancement->getMail();
            }

            $etat = CommonValeurReferentielOrgPeer::retrieveByPK($MesureAvancement->getTypePouvoirAdjudicateur(), $organisme, $connexion);
            if ($etat) {
                $keyTab['<!--_TypePouvoirAdjudicateur_-->'] = utf8_encode($etat->getLibelleValeurReferentiel());
            }

            $keyTab['<!--_Trimestre_-->'] = $MesureAvancement->getAnnee().$MesureAvancement->getTrimestre();
            $keyTab['<!--_BLOC_TYPE_PROCEDURE_-->'] = $blockTypeProcedure;

            $contents = '';
            $contents = file_get_contents(Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_XML_MESURE_DEMAT'));

            foreach ($keyTab as $key => $val) {
                $contents = str_replace($key, $val, $contents);
            }

            $mesure = CommonMesureAvancementPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), $organisme, $connexion);
            $mesure->setXml(utf8_decode($contents));

            if (true === Atexo_Util::IsXmlValide($mesure->getXml(), Atexo_Config::getParameter('FILE_XSD_MESURE_DEMAT'))) {
                $mesure->save($connexion);
                $this->panelMessage->setMessage('<div class="line"><div class="intitule-auto">'.Prado::localize('CONFIRMATION_MODIFICATION_FOURMULAIRE_AVANCEMENT').'</div> </div>');
                $this->panelMessage->setVisible(true);
            } else {
                $this->panelMessageErreur->setMessage('<div class="line"><div class="intitule-auto">'.Prado::localize('ERREUR_MODIFICATION_FOURMULAIRE_AVANCEMENT').'</div> </div>');
                $this->panelMessageErreur->setVisible(true);
            }
        }
    }

    public function displayDonnees($treapter, $paramOnglet)
    {
        $dataService = [];
        $dataService = $this->retreiveDataXml(Prado::localize('DEFINE_SERVICES'), $paramOnglet);
        $dataFour = [];
        $dataFour = $this->retreiveDataXml(Prado::localize('DEFINE_FOURNITURES'), $paramOnglet);
        $dataTravaux = [];
        $dataTravaux = $this->retreiveDataXml(Prado::localize('DEFINE_TRAVAUX'), $paramOnglet);

        if ($dataFour && $dataTravaux && $dataService) {
            $data = array_merge($dataFour, $dataTravaux);
            $data = array_merge($data, $dataService);
        } elseif ($dataFour && $dataTravaux && !$dataService) {
            $data = array_merge($dataFour, $dataTravaux);
        } elseif ($dataFour && !$dataTravaux && $dataService) {
            $data = array_merge($dataFour, $dataService);
        } elseif (!$dataFour && $dataTravaux && $dataService) {
            $data = array_merge($dataTravaux, $dataService);
        } elseif ($dataFour && !$dataTravaux && !$dataService) {
            $data = $dataFour;
        } elseif (!$dataFour && $dataTravaux && !$dataService) {
            $data = $dataTravaux;
        } elseif (!$dataFour && !$dataTravaux && $dataService) {
            $data = $dataService;
        }

        $this->$treapter->Datasource = $data;
        $this->$treapter->dataBind();
    }

    public function onAnnulerClick()
    {
        $this->response->redirect('index.php?page=Agent.StatistiquesMesureAvancement&idService='.$this->getIdService().
        '&annee='.Atexo_Util::atexoHtmlEntities($_GET['annee']).'&trim='.Atexo_Util::atexoHtmlEntities($_GET['trim']).'');
    }

    public function onEnregistrerBrouillonClick($sender, $param)
    {
        $this->CreateXml();
        $this->response->redirect('index.php?page=Agent.StatistiquesMesureAvancement&idService='.$this->getIdService().
        '&annee='.Atexo_Util::atexoHtmlEntities($_GET['annee']).'&trim='.Atexo_Util::atexoHtmlEntities($_GET['trim']).'');
    }

    public function onEnregistrerEnvoyerClick($sender, $param)
    {
        $this->CreateXml();
        $this->scriptEnregister->Text = "<script>javascript:popUp('?page=Agent.PopupBoampMesureAvancement&idService=".$this->getIdService().
        '&idMesure='.$this->getIdMesure().'&annee='.Atexo_Util::atexoHtmlEntities($_GET['annee']).'&trim='.Atexo_Util::atexoHtmlEntities($_GET['trim'])."','yes');</script>";
    }

    public function getIdService()
    {
        if (isset($_GET['idService'])) {
            return Atexo_Util::atexoHtmlEntities($_GET['idService']);
        }
    }

    public function getIdMesure()
    {
        if (isset($_GET['id'])) {
            return Atexo_Util::atexoHtmlEntities($_GET['id']);
        }
    }
}
