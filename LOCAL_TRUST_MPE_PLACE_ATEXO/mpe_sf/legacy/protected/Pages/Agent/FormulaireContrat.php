<?php

namespace Application\Pages\Agent;

use App\Service\Clauses\ClauseUsePrado;
use App\Service\Routing\RouteBuilderInterface;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeContratMpePivotQuery;
use Application\Propel\Mpe\CommonTypeContratPivotQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Contrat\Atexo_Contrat_AttributaireVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de creation du contrat.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class FormulaireContrat extends MpeTPage
{
    /**
     * Permet d'initialiser la page.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Permet de charger le formulaire.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function onLoad($param)
    {
        // Redirect to exec component page
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $urlRedirection = Atexo_Util::getSfService(RouteBuilderInterface::class)->getRoute(
                'agent_web_component_contrat_create'
            );
            $this->response->redirect($urlRedirection);
        }

        (Atexo_Util::hasSiretCheckeSiretCentralise(
        )) ? $this->warningSiret->displayStyle = 'None' : $this->warningSiret->displayStyle = 'Dynamic';

        // TODO lorsque on a une habillitaion ou un module pour permet de créer un contrat
        if (true) {
            if (!$this->isPostBack && !$this->isCallBack) {
                self::chargerTypeContrat();
                self::chargerNatureContrat();
                $this->infoContrat->initialiserComposant();
                $idTypeContrat = $this->contratType->getSelectedValue();
                $this->listeAttributaires->setTypeContrat($idTypeContrat);
                $this->listeAttributaires->initialiserComposant();
            }
        } else {
            // Message d'erreur
        }

        $this->labelScript->Text = '';
    }

    /**
     * Permet de charger les types de contrats.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerTypeContrat()
    {
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat();
        $this->contratType->Datasource = $listeContrats;
        $this->contratType->dataBind();

        //TODO
        $this->contratType->setSelectedValue('1');
        self::typeContratChanged();
    }

    /**
     * Permet de charger les comoposant selon le type de contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function typeContratChanged()
    {
        $libelle = null;
        $holder = null;
        $idTypeContrat = $this->contratType->getSelectedValue();
        $this->infoContrat->setTypeContrat($idTypeContrat);
        $this->infoContrat->initialiserComposant($idTypeContrat);
        $this->listeAttributaires->setTypeContrat($idTypeContrat);
        $this->listeAttributaires->initialiserComposant();
        $this->entitesEligibles($idTypeContrat);
        $this->numeroACValue->Value = '';
        $this->idContratSelected->Value = '';
        $this->numeroAC->Text = '';
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($idTypeContrat)
            || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($idTypeContrat)) {
            $this->panelNumAC->setDisplay('Dynamic');
            if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($idTypeContrat)) {
                $libelle = Prado::Localize('NUMERO_ACCORD_CADRE_CORRESPONDANT');
                $holder = Prado::localize('NUM_LONG_OR_OBJET_AC');
                //$this->activeInfoMsgMarcheSubsequent->setDisplayStyle("Dynamic");
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($idTypeContrat)) {
                $libelle = Prado::Localize('NUMERO_SAD_CORRESPONDANT');
                $holder = Prado::localize('NUM_LONG_OR_OBJET_SAD');
                //$this->activeInfoMsgMarcheSpecifique->setDisplayStyle("Dynamic");
            }
            $this->labelObjetMarche->setText($libelle);
            $this->labelScript->Text = "<script>J('#".$this->numeroAC->ClientId."').prop('title', '".addslashes(
                    $libelle
                )."');
												J('#".$this->numeroAC->ClientId."').prop('placeholder', '".addslashes(
                    $holder
                )."');
										</script>";
        } else {
            $this->panelNumAC->setDisplay('None');
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratConcession($idTypeContrat)) {
            $this->panelNatureConcession->setDisplay('Dynamic');
        } else {
            $this->panelNatureConcession->setDisplay('None');
        }
        $this->infoContrat->loadFieldsContratConcession($idTypeContrat);
    }

    /**
     * Permet de valider les donnees  de contrat.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerDonneesContrat($sender, $param)
    {
        $isValid = true;
        $script = '';
        $arrayMsgErreur = [];
        if ($this->contratType->getSelectedValue()) {
            $idContratType = $this->contratType->getSelectedValue();
            $script .= "document.getElementById('".$this->contratTypeImgError->getClientId(
                )."').style.display = 'none';";
            $this->infoContrat->validerInfoContrat($isValid, $script, $arrayMsgErreur);
            $this->listeAttributaires->validerInfoAttributaire($isValid, $script, $arrayMsgErreur);
            if (((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($idContratType)
                    || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique(
                        $idContratType
                    )) && (!$this->numeroACValue->Value)) {
                $script .= "document.getElementById('".$this->numAcImgError->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('MSG_VALIDATION_MARCHE_SPECIFIQUE_SUBSEQUENT');
                $isValid = false;
            } else {
                $script .= "document.getElementById('".$this->numAcImgError->getClientId()."').style.display = 'none';";
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratConcession($idContratType)) {
                if (!$this->natureContratConcession->getSelectedValue()) {
                    $script .= "document.getElementById('".$this->natureContratConcessionImgError->getClientId(
                        )."').style.display = '';";
                    $arrayMsgErreur[] = Prado::localize('NATURE_CONTRAT_CONCESSION');
                    $isValid = false;
                } else {
                    $script .= "document.getElementById('".$this->natureContratConcessionImgError->getClientId(
                        )."').style.display = 'none';";
                }
            } else {
                $script .= "document.getElementById('".$this->natureContratConcessionImgError->getClientId(
                    )."').style.display = 'none';";
            }
        } else {
            $script .= "document.getElementById('".$this->contratTypeImgError->getClientId()."').style.display = '';";
            $script .= "document.getElementById('".$this->numAcImgError->getClientId()."').style.display = 'none';";
            $arrayMsgErreur[] = Prado::localize('DEFINE_TYPE_CONTRAT');
            $isValid = false;
        }
        if (!$this->reference->Text) {
            $script .= "document.getElementById('".$this->ImgErrorReference->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('REFERENCE_INTERNE_CONTRAT');
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorReference->getClientId()."').style.display = 'none';";
        }

        $this->afficherMessagesValidationServeur($param, $isValid, $script, $arrayMsgErreur);
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param bool $isValid
     * @param string $script
     * @param array $arrayMsgErreur
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function afficherMessagesValidationServeur($param, $isValid, $script, $arrayMsgErreur)
    {
        if ($isValid) {
            $param->IsValid = true;
            $script .= "document.getElementById('divValidationSummary').style.display='none'";
            $this->labelServerSide->Text = '<script>'.$script.'</script>';
        } else {
            $param->IsValid = false;
            $script .= "document.getElementById('divValidationSummary').style.display='';";
            $this->labelServerSide->Text = "<script>$script</script>";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }

            return;
        }
    }

    /**
     * Permet de valider le contrat.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerContrat($sender, $param)
    {
        $connexion = null;
        if ($this->isValid) {
            try {
                $connexion = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                );
                $connexion->beginTransaction();
                $idContrat = '';
                $idTypeContrat = $this->contratType->getSelectedValue();
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
                $serviceId = Atexo_CurrentUser::getCurrentServiceId();
                $referencInterne = $this->reference->getSafeText();
                $referencConsultation = $this->referenceConsultation->getSafeText();
                //Le chapeau
                if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idTypeContrat)) {
                    $contratMulti = new CommonTContratMulti();
                    $contratMulti->setIdTypeContrat($idTypeContrat);

                    $typeContratMpeQuery = new CommonTypeContratMpePivotQuery();
                    $typeContratMpePivot = $typeContratMpeQuery->findOneById($idTypeContrat);
                    if ($typeContratMpePivot) {
                        $idTypeContratPivot = $typeContratMpePivot->getIdTypeContratPivot();
                        $typeContratPivotQuery = new CommonTypeContratPivotQuery();
                        $typeContratPivot = $typeContratPivotQuery->findOneById($idTypeContratPivot);
                        if ($typeContratPivot) {
                            $contratMulti->setLibelleTypeContratPivot($typeContratPivot->getLibelle());
                        }
                    }

                    $contratMulti->setOrganisme($organisme);
                    $contratMulti->setServiceId($serviceId);
                    $contratMulti->setReferenceLibre($referencInterne);
                    $contratMulti->setReferenceConsultation($referencConsultation);
                    $this->infoContrat->saveContratMulti($contratMulti);
                    $contratMulti->setHorsPassation(
                        Atexo_Config::getParameter('CONTRAT_DEPUIS_FORMULAIRE_CREER_CONTRAT')
                    );
                    $contratMulti->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $contratMulti->setNomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
                    $contratMulti->setPrenomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
                    $contratMulti->setDateCreation(date('Y-m-d H:i:s'));
                    $contratMulti->setDateModification(date('Y-m-d H:i:s'));
                    $contratMulti->setDateAttribution(date('Y-m-d'));
                    $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat(
                        $contratMulti,
                        $connexion
                    );
                    $numeroContratMulti = $numeroLong.Atexo_Config::getParameter(
                            'SEPARATEUR_NUMEROTATION_CONTRAT'
                        ).'00';
                    $contratMulti->setNumeroContrat($numeroContratMulti);
                    $contratMulti->setUuid((new Atexo_MpeSf())->uuid());
                    $contratMulti->save($connexion);
                    $this->saveInvitationConsultationTransverse($contratMulti, $connexion);
                }
                //Les Attributaires
                $attributaires = $this->listeAttributaires->getDataSourceAttributaire();
                if (is_array($attributaires) && count($attributaires)) {
                    $j = 1;
                    foreach ($attributaires as $attributaire) {
                        $idEtablissement = null;
                        if ($attributaire instanceof Atexo_Contrat_AttributaireVo) {
                            $contrat = new CommonTContratTitulaire();
                            $contrat->setIdTypeContrat($idTypeContrat);

                            $typeContratMpeQuery = new CommonTypeContratMpePivotQuery();
                            $typeContratMpePivot = $typeContratMpeQuery->findOneById($idTypeContrat);
                            $idTypeContratPivot = $typeContratMpePivot->getIdTypeContratPivot();

                            $typeContratPivotQuery = new CommonTypeContratPivotQuery();
                            $typeContratPivot = $typeContratPivotQuery->findOneById($idTypeContratPivot);
                            $contrat->setLibelleTypeContratPivot($typeContratPivot->getLibelle());

                            $contrat->setOrganisme($organisme);
                            $contrat->setServiceId($serviceId);
                            $contrat->setReferenceLibre($referencInterne);
                            $contrat->setReferenceConsultation($referencConsultation);
                            $this->infoContrat->saveContrat($contrat);
                            $contrat->setStatutContrat(Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR'));
                            $contrat->setHorsPassation(
                                Atexo_Config::getParameter('CONTRAT_DEPUIS_FORMULAIRE_CREER_CONTRAT')
                            );
                            $contrat->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                            $contrat->setNomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
                            $contrat->setPrenomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
                            $contrat->setDateCreation(date('Y-m-d H:i:s'));
                            $contrat->setDateModification(date('Y-m-d H:i:s'));
                            $contrat->setDateAttribution(date('Y-m-d'));
                            if ((new Atexo_Consultation_Contrat())->isTypeContratConcession($idTypeContrat)) {
                                $contrat->setIdTypeContratConcessionPivot(
                                    $this->natureContratConcession->getSelectedValue()
                                );
                            }

                            $organismeQuery = new CommonOrganismeQuery();
                            $organismeObj = $organismeQuery->findOneByAcronyme($organisme);
                            $contrat->setSiretPaAccordCadre($organismeObj->getSiren().$organismeObj->getComplement());

                            $typeContratQuery = new CommonTTypeContratQuery();
                            $typeContrat = $typeContratQuery->findOneByIdTypeContrat($idTypeContrat);
                            $contrat->setAcMarcheSubsequent(1 == $typeContrat->getAccordCadreSad() ? true : false);

                            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById(
                                    $idTypeContrat
                                ) && $contratMulti instanceof CommonTContratMulti) {
                                $contrat->setIdContratMulti($contratMulti->getIdContratTitulaire());
                                if ((new Atexo_Consultation_Contrat())->getCasTypesMarchesPourNumerotation($contrat)) {
                                    $numeroCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat(
                                        $contrat,
                                        $connexion
                                    );
                                    $contrat->setNumeroContrat($numeroCourt);
                                    $contrat->setNumLongOeap($numeroLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                }
                            } else {
                                if ((new Atexo_Consultation_Contrat())->getCasTypesMarchesPourNumerotation($contrat)) {
                                    $numeroCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat(
                                        $contrat,
                                        $connexion
                                    );
                                    $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat(
                                        $contrat,
                                        $connexion
                                    );
                                    $contrat->setNumeroContrat($numeroCourt);
                                    $contrat->setNumLongOeap($numeroLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                }
                            }

                            $entreprise = $attributaire->getEntrepriseVo();
                            if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
                                $idEntreprise = $entreprise->getId();
                                if (!$idEntreprise) {
                                    $entreprise->setSaisieManuelle('1');
                                    $entreprise->setDateModification(date('Y-m-d H:i:s'));
                                    $entreprise->setDateCreation(date('Y-m-d H:i:s'));
                                    $entreprise->setCreatedFromDecision('1');
                                    $entreprise = (new Atexo_Entreprise())->save($entreprise, $connexion);
                                    $idEntreprise = $entreprise->getId();
                                } else {
                                    $etablissement = $attributaire->getEtablissementVo();
                                    if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
                                        $idEtablissement = $etablissement->getIdEtablissement();
                                        if (!$idEtablissement) {
                                            $etablissement->setSaisieManuelle('1');
                                            $etablissement->setDateModification(date('Y-m-d H:i:s'));
                                            $etablissement->setDateCreation(date('Y-m-d H:i:s'));
                                            $etablissement = $etablissement->getCommonTEtablissement();
                                            $etablissement->setIdEntreprise($idEntreprise);
                                            $etablissement->save($connexion);
                                        }
                                    }
                                }
                                $connexion->commit();
                                if (!$idEtablissement && $entreprise instanceof Entreprise) {
                                    $etablissements = $entreprise->getCommonTEtablissements();
                                    if ($etablissements instanceof PropelObjectCollection) {//echo
                                        foreach ($etablissements as $etablissement) {
                                            if ($etablissement instanceof CommonTEtablissement) {
                                                $idEtablissement = $etablissement->getIdEtablissement();
                                            }
                                        }
                                    }
                                }
                                $contrat->setIdTitulaire($idEntreprise);
                                $contrat->setIdTitulaireEtab($idEtablissement);
                                if ($this->idContratSelected->Value) {
                                    $lienACSad = (new Atexo_Consultation_Contrat())->getLienACByIdContratAndIdTitulaire(
                                        $this->idContratSelected->Value,
                                        $idEntreprise
                                    );
                                    if (is_null($lienACSad)) {
                                        $lienACSad = $this->idContratSelected->Value;
                                    }
                                    $contrat->setLienAcSad($lienACSad);
                                }
                            }

                            $contact = new CommonTContactContrat();
                            $contact->setIdEntreprise($idEntreprise);
                            $contact->setIdEtablissement($idEtablissement);
                            $contact->setNom($attributaire->getNomContact());
                            $contact->setPrenom($attributaire->getPrenomContact());
                            $contact->setEmail($attributaire->getEmailContact());
                            $contact->setTelephone($attributaire->getTelephoneContact());
                            $contact->setFax($attributaire->getFaxContact());
                            $contrat->setCommonTContactContrat($contact);

                            $contrat->setSiret((new Atexo_Consultation())->getSiretByService($organisme, $serviceId));
                            $nomEntiteAcheteur = (new Atexo_Consultation())->getOrganismeDenominationByService(
                                $organisme,
                                $serviceId
                            );
                            $nomEntiteAcheteurToClean = explode('(', $nomEntiteAcheteur);

                            // Remove last part of the sentence which contain something like (CP - VILLE)
                            $lastKeyArrayNomEntiteAcheteurToClean = array_key_last($nomEntiteAcheteurToClean);
                            $nomEntiteAcheteurClean = '';
                            foreach ($nomEntiteAcheteurToClean as $key => $value) {
                                if ($key < $lastKeyArrayNomEntiteAcheteurToClean) {
                                    $nomEntiteAcheteurClean .= (($key == 0) ? '' : '(').$value;
                                }
                            }

                            $contrat->setNomEntiteAcheteur($nomEntiteAcheteurClean);
                            $contrat->setUuid((new Atexo_MpeSf())->uuid());
                            $contrat->save($connexion);
                            $idContrat = $contrat->getIdContratTitulaire();

                            $this->saveAchatResponsable(
                                $idContrat,
                                $this->infoContrat->achatResponsableConsultation->achatResponsableFormValue->Data
                            );
                            $this->saveInvitationConsultationTransverse($contrat, $connexion);
                        }
                    }
                }

                if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById(
                        $idTypeContrat
                    ) && $contratMulti instanceof CommonTContratMulti) {
                    $urlRedirection = '?page=Agent.ResultatRechercheContrat&contratMulti='.$contratMulti->getIdContratTitulaire(
                        );
                } else {
                    $urlRedirection = '?page=Agent.ResultatRechercheContrat&contrat='.$idContrat;
                }
                $connexion->commit();
                $this->response->redirect($urlRedirection);
            } catch (Exception $e) {
                $connexion->rollBack();
                Prado::log(
                    "Erreur de creation d'un contrat : ".$e->getMessage().$e->getTraceAsString(),
                    TLogger::ERROR,
                    'FormulaireContrat.php'
                );
            }
        }
    }

    /**
     * Permet de charger les comoposant selon le type de contrat.
     *
     * @param int $idTypeContrat
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function entitesEligibles($idTypeContrat)
    {
        if (Atexo_Module::isEnabled('AcSadTransversaux')
            && ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre(
                    $idTypeContrat
                ) || (new Atexo_Consultation_Contrat())->isTypeContratSad($idTypeContrat))
        ) {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:block;');
        } else {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:none;');
        }
    }

    /**
     * Permet d'enregistrer les entités éligible.
     *
     * @param CommonTContratTitulaire $contrat
     * @param $connexion
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function saveInvitationConsultationTransverse($contrat, $connexion)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            if (Atexo_Module::isEnabled('AcSadTransversaux')
                && ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre(
                        $contrat->getIdTypeContrat()
                    ) || (new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat()))
            ) {
                if ('1' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                    $this->decisionEntitesEligibles->addInvitationConsultationTransverse(
                        0,
                        null,
                        0,
                        $contrat->getIdContratTitulaire(),
                        $connexion
                    );
                }
                if ('2' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                    $this->decisionEntitesEligibles->addInvitePermanentContrat(
                        0,
                        null,
                        0,
                        $contrat->getIdContratTitulaire(),
                        $connexion
                    );
                }
            }
        }
    }

    /**
     * Permet d'afficher les numero AC suggerer.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function suggestNumbersAC($sender, $param)
    {
        $token = Atexo_Util::httpEncodingToUtf8($param->getToken());
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $typeACSad = '';
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($this->contratType->getSelectedValue())) {
            $typeACSad = [
                Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE'),
                Atexo_Config::getParameter('TYPE_CONTRAT_SAD')
            ];
        } elseif ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique(
            $this->contratType->getSelectedValue()
        )) {
            $typeACSad = Atexo_Config::getParameter('TYPE_CONTRAT_SAD');
        }
        $dataSource = (new Atexo_Consultation_Contrat())->getDataContratAcByNumero(
            $token,
            $idService,
            $organisme,
            $typeACSad
        );

        $sender->DataSource = $dataSource;
        $sender->dataBind();
        $this->labelScript->Text = '<script>hideLoader();</script>';
    }

    /*
     * Fonction appelé a la selection de la liste de l'auto completion
     *
     * @param $sender
     * @param $param
     * @return void
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-roadmap
     * @copyright Atexo 2015
     */
    protected function suggestionNumbersACSelected($sender, $param)
    {
        $id = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById($id);
        $commentaire = '';
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->numeroACValue->Value = $contrat->getNumeroContrat();
            $this->idContratSelected->Value = $id;
            //$this->referenceConsultation->Text  = $contrat->getReferenceConsultation();
        }
        $this->labelScript->Text = '<script>hideLoader();</script>';
    }

    /**
     * Permet de charger les types de contrats.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerNatureContrat()
    {
        $listeNatureContrat = Atexo_Contrat::getListeNatureContratConcession();
        $this->natureContratConcession->Datasource = $listeNatureContrat;
        $this->natureContratConcession->dataBind();
    }

    /**
     * ne pas afficher le bouton de modification d'attributaire.
     */
    public function isEditAttributaireAvailable()
    {
        return false;
    }

    protected function saveAchatResponsable(int $idContrat, string $values): void
    {
        /** @var ClauseUsePrado $clause */
        $clause = Atexo_Util::getSfService(ClauseUsePrado::class);
        $data = [
            'values' => json_decode($values, true),
            'id_contrat' => $idContrat
        ];
        $clause->getContent('sendContrat', $data);
    }
}
