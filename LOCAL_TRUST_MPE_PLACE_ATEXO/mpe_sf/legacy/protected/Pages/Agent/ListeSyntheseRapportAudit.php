<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

/*
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class ListeSyntheseRapportAudit extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->tableauSRA->setPostBack($this->IsPostBack);
        $this->tableauSRA->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $this->tableauSRA->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
    }
}
