<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;

/**
 * Created on 14 déc. 2011.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 */
class ChoixDestinataireBdFournisseur extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelSearch->setPostBack($this->IsPostBack);
        $this->panelSearch->setCallBack($this->IsCallBack);
        if (!$this->IsPostBack) {
            $this->panelSearch->setVisible(true);
            $this->panelSearch->displayDomaines();
            $this->panelResultSerch->setVisible(false);
        }
        $this->panelSearch->customizeForm();
    }

    public function dataForSearchResult(Atexo_Entreprise_CriteriaVo $criteriaVo)
    {
        $this->panelSearch->setVisible(false);
        $this->panelResultSerch->setVisible(true);
        $this->panelResultSerch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $this->panelResultSerch->Trier($param->CommandName);
    }
}
