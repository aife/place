<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpRenseignerStatut extends MpeTPage
{
    private $_consultationAlloti;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function getConsultationAlloti()
    {
        return $this->_consultationAlloti;
    }

    public function setConsultationAlloti($consultationAlloti)
    {
        $this->_consultationAlloti = $consultationAlloti;
    }

    public function onLoad($param)
    {
        $showEnveloppesPostulees = null;
        $lots = [];
        $this->erreur->setVisible(false);
        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
            if (!$this->IsPostBack) {
                $idEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']);
                $idOffre = Atexo_Util::atexoHtmlEntities($_GET['idOffre']);
                $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $typePli = Atexo_Util::atexoHtmlEntities($_GET['typePli']);
                $messageErreur = '';
                $afficherRepeater = false;
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById($idEnveloppe, $typePli, Atexo_CurrentUser::getCurrentOrganism());

                if ($enveloppe instanceof CommonEnveloppePapier || $enveloppe instanceof CommonEnveloppe) {
                    if (($enveloppe instanceof CommonEnveloppePapier || $enveloppe instanceof CommonEnveloppe) && $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                        $this->statutFerme->Checked = true;

                        if ($enveloppe instanceof CommonEnveloppePapier) {
                            $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($enveloppe->getOffrePapierId(), Atexo_CurrentUser::getCurrentOrganism());
                            $consultation = $offrePapier->getCommonConsultation($connexion);

                            $consultation = (new Atexo_Consultation())->retrieveConsultation($consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());

                            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());

                            $showEnveloppesPostulees = false;
                            if ($typeEnveloppe == Atexo_Config::getParameter('OFFRE_SEUL') || $typeEnveloppe == Atexo_Config::getParameter('OFFRE_ET_ANONYMAT')) {
                                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idOffre, Atexo_CurrentUser::getCurrentOrganism(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                $enveloppeTraitees = false;
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $enveloppeOffre) {
                                        if ($enveloppeOffre->getStatutEnveloppe() != Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                                            $enveloppeTraitees = true;
                                        }
                                    }
                                }
                                if (!$enveloppeTraitees) {
                                    $showEnveloppesPostulees = true;
                                }
                            }
                        }
                        if ($showEnveloppesPostulees || ($enveloppe instanceof CommonEnveloppePapier && $enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'))) {
                            $this->repeater->Visible = true;
                            $this->javascript->Text = "<script>hideDiv('ctl0_CONTENU_PAGE_repeater');popupResize('ctl0_CONTENU_PAGE_container');</script>";
                            $afficherRepeater = true;
                        } else {
                            $this->repeater->Visible = false;
                            $afficherRepeater = false;
                        }
                    } elseif ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE') ||
                          ($enveloppe->getStatutEnveloppe() != Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') && $enveloppe->getStatutEnveloppe() != Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'))) {
                        $messageErreur = Prado::localize('IMPOSSIBLE_RENSEIGNER_STATUT');
                    }
                } else {
                    $messageErreur = Prado::localize('AUCUN_PLI');
                }

                if ($messageErreur) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->erreur->setVisible(true);
                    $this->container->setVisible(false);
                    $this->panelMessageErreur->setMessage($messageErreur);
                } else {
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($idReference, Atexo_CurrentUser::getCurrentOrganism());
                    if (!($consultation instanceof CommonConsultation)) {
                        //Todo Consultation introuvable
                    }
                    $nombreLots = is_countable($consultation->getAllLots()) ? count($consultation->getAllLots()) : 0;

                    if ($nombreLots) {
                        $this->_consultationAlloti = true;
                    } else {
                        $this->_consultationAlloti = false;
                    }

                    if ($afficherRepeater) {
                        //$enveloppeSuivante=Atexo_Consultation_Responses::
                        if ($this->_consultationAlloti) {
                            $lots = $consultation->getAllLots();
                        } else {
                            $lot = new CommonCategorieLot();
                            $lot->setLot('0');
                            $lot->setDescription($consultation->getIntituleTraduit());
                            $lots[] = $lot;
                        }
                        //$enveloppesOffre=Atexo_Consultation_Responses::retrieveAllEnveloppeOffrePapier($idOffre , $consultation, Atexo_CurrentUser::getCurrentOrganism());
                        if (is_array($lots)) {
                            $this->listeLots->DataSource = $lots;
                            $this->listeLots->DataBind();
                        }
                    }

                    if ($enveloppe instanceof CommonEnveloppe) {
                        $offre = $enveloppe->getOffre($connexion);
                        if ($offre instanceof CommonOffres) {
                            $this->entreprise->Text = Atexo_Entreprise::getNomEntreprise($offre->getEntrepriseId());
                        }
                    } elseif ($enveloppe instanceof CommonEnveloppePapier) {
                        $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($enveloppe->getOffrePapierId(), Atexo_CurrentUser::getCurrentOrganism());
                        if ($offrePapier instanceof CommonOffrePapier) {
                            $this->entreprise->Text = $offrePapier->getNomEntreprise();
                        }
                    }
                }
            }
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->erreur->setVisible(true);
            $this->container->setVisible(false);
            $this->panelMessageErreur->setMessage(Prado::localize('EREUR_RENSEIGNER_STATUT'));
        }
    }

    public function onEnregistrerClick()
    {
        $connexionCom = null;
        if ($this->statutFerme->Checked) {
            $this->javascript->Text = '<script>window.close();</script>';
        } elseif ($this->statutOuverte->Checked) {
            try {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexionCom->beginTransaction();

                if ($_GET['typePli'] == Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE')) {
                    $enveloppe = CommonEnveloppePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']), $organisme, $connexionCom);
                } else {
                    $enveloppe = CommonEnveloppePapierPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']), $organisme, $connexionCom);
                }

                if ($enveloppe instanceof CommonEnveloppePapier || $enveloppe instanceof CommonEnveloppe) {
                    if ($_GET['typePli'] == Atexo_Config::getParameter('TYPE_ENVELOPPE_PAPIER')) {
                        if (trim($this->entreprise->Text)) {
                            $offre = CommonOffrePapierPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idOffre']), $organisme, $connexionCom);
                            if ($offre instanceof CommonOffrePapier) {
                                $offre->setNomEntreprise(trim($this->entreprise->Text));
                                $offre->save($connexionCom);
                            } else {
                                throw new Exception();
                            }
                        }
                    }

                    $enveloppe->setDateheureOuverture(Atexo_Util::frnDateTime2iso($this->dateOuvertue->Text));
                    if ($enveloppe instanceof CommonEnveloppe) {
                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'));
                    } else {
                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE'));
                        $enveloppe->setCryptage('0');
                    }
                    $enveloppe->setNomAgentOuverture($this->par->Text);
                    $enveloppe->save($connexionCom);

                    if ($enveloppe instanceof CommonEnveloppePapier) {
                        foreach ($this->listeLots->getItems() as $item) {
                            $c = new Criteria();
                            $c->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, Atexo_Util::atexoHtmlEntities($_GET['idOffre']));
                            $c->add(CommonEnveloppePapierPeer::SOUS_PLI, $item->sousPli->Value);
                            $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
                            $enveloppes = CommonEnveloppePapierPeer::doSelect($c, $connexionCom);

                            if ($item->lot->Checked) {
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $enveloppe) {
                                        $enveloppe->setEnveloppePostule('1');
                                        $enveloppe->save($connexionCom);
                                    }
                                }
                            } else {
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $enveloppe) {
                                        $enveloppe->setEnveloppePostule('0');
                                        $enveloppe->save($connexionCom);
                                    }
                                }
                            }
                        }
                    }
                }
                $connexionCom->commit();
                $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callbackButton'])."').click();window.close();</script>";
            } catch (Exception $e) {
                $connexionCom->rollback();
                print_r($e->getMessage());
                exit;
            }
        }
    }
}
