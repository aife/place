<?php

namespace Application\Pages\Agent;

use App\Service\Clauses\ClauseUsePrado;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Prado\Prado;

/**
 * Contient le detail des lot d'une consultation.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.0
 *
 * @since MPE-4.0
 */
class PopupAjoutLot extends MpeTPage
{
    protected $_reference;
    protected $_organisme;
    protected $_consultation;
    protected $_lot;
    protected $_langueEnSession;
    public ?string $_langue = null;

    /**
     * recupère la langue.
     */
    public function getLangue()
    {
        return $this->_langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->_langue !== $langue) {
            $this->_langue = $langue;
        }
    }

    // setLangue()

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    // getConsultation()

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $categorieLot = null;
        $numLot = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->_consultation = $this->retrieveConsultation($this->_reference);
        $donneesComplementaireCons = $this->_consultation->getDonneComplementaire();

        if (
            !Atexo_Module::isEnabled('Publicite')
            || !$this->getConsultation()->getDonneePubliciteObligatoire()
        ) {
            $this->showHideDonnesRedac($this->_consultation->getDonneeComplementaireObligatoire());
        } else {
            if (($this->getConsultation()->getDonneeComplementaireObligatoire() && $this->getConsultation()->getDonneePubliciteObligatoire()) ||
                (!$this->getConsultation()->getDonneeComplementaireObligatoire() && $this->getConsultation()->getDonneePubliciteObligatoire())
            ) {
                $this->script->Text .= '<script>activerBlocsDonneesComplementaires();</script>';
            } elseif (!$this->getConsultation()->getDonneeComplementaireObligatoire() && !$this->getConsultation()->getDonneePubliciteObligatoire()) {
                $this->script->Text .= '<script>desactiverBlocsDonneesComplementaires();</script>';
            } elseif ($this->getConsultation()->getDonneeComplementaireObligatoire() && !$this->getConsultation()->getDonneePubliciteObligatoire()) {
                $this->script->Text .= '<script>activerBlocsDonneesComplementaires();</script>';
            }
        }

        if (isset($_GET['idLot'])) {
            // Modification d'un lot
            $numLot = Atexo_Util::atexoHtmlEntities($_GET['idLot']);
            $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->_reference, $numLot, $this->_organisme);
            $this->_lot = $categorieLot;
        }
        $this->donneeVariante->setAfficher(true);
        $this->donneeDureeMarche->setAfficherMarcheReconductible(true);
        if (!$this->IsPostBack) {
            $categories = Atexo_Consultation_Category::retrieveCategories();
            $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories);
            $this->categorieLot->DataSource = $dataCategories;
            $this->categorieLot->dataBind();
            if (isset($_GET['idLot'])) {
                // Modification d'un lot
                if ($categorieLot instanceof CommonCategorieLot) {
                    $donneeComplementaire = $categorieLot->getDonneComplementaire();
                    $this->numLot->Text = $numLot;
                    $this->intituleLot->Text = $categorieLot->getDescriptionTraduite();
                    $this->DescriptionLot->Text = $categorieLot->getDescriptionDetail();
                    $this->categorieLot->SelectedValue = $categorieLot->getCategorie();
                    $atexo_ref = new Atexo_Ref();
                    $atexo_ref->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'));
                    $atexo_ref->setPrincipal($categorieLot->getCodeCpv1());
                    $atexo_ref->setSecondaires($categorieLot->getCodeCpv2());
                    $this->setViewState('atexoRefLot', $atexo_ref);

                    $this->idReferentielZoneTextLot->afficherTextConsultation($this->_reference, $numLot, $this->_organisme, $this->idReferentielZoneTextLot->getValueReferentielTextVo(), false);
                    $this->ltRefLot->afficherReferentielConsultation($this->_reference, $numLot, $this->_organisme);

                    //charger referentiel CPV
                    $this->referentielCPV->onInitComposant();
                    $this->referentielCPV->chargerReferentielCpv($categorieLot->getCodeCpv1(), $categorieLot->getCodeCpv2());

                    //ref radio
                    $this->idReferentielRadioLot->afficherReferentielRadio($this->_reference, $numLot, $this->_organisme, $this->idReferentielRadioLot->getValueReferentielRadioVo(), false);
                    if (Atexo_Module::isEnabled('ConsultationClause')) {
                        $this->achatResponsableConsultation->afficherClausesConsultation($categorieLot);
                    }

                    if (Atexo_Module::isEnabled('DonneesComplementaires')) {// Données complementaire
                        if (Atexo_Module::isEnabled('DonneesRedac')) { //Données Redac
                            $this->donneeFormePrix->afficherDataFormeMarcheFormePrix($categorieLot->getDonneComplementaire());
                            $this->donneeDureeMarche->setDonneeComplementaire($categorieLot->getDonneComplementaire());
                            $this->donneeVariante->setDonneeComplementaire($categorieLot->getDonneComplementaire());
                            if (Atexo_Module::isEnabled('Publicite') && Atexo_Module::isEnabled('AfficherValeurEstimee')) {
                                $this->donneeMontantMarche->setDonneeComplementaire($categorieLot->getDonneComplementaire());
                            }
                            $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setDonneeComplementaireCons($donneesComplementaireCons);
                        }
                        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                            //Caution provisoire
                            if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
                                $this->cautionProvisoire->Text = $donneeComplementaire->getCautionProvisoire();
                            }
                            //variante
                            if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                                if ('1' == $donneeComplementaire->getVariantes()) {
                                    $this->variantesOui->Checked = true;
                                } else {
                                    $this->variantesNon->Checked = true;
                                }
                            }
                        }
                    }
                }
            } else {
                // Création d'un lot
                $this->referentielCPV->onInitComposant();
                $this->ltRefLot->afficherReferentielConsultation();
                $this->idReferentielZoneTextLot->afficherTextConsultation();
                //ref radio
                $this->idReferentielRadioLot->afficherReferentielRadio();
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $this->achatResponsableConsultation->afficherClausesConsultation();
                }
                if (Atexo_Module::isEnabled('DonneesComplementaires')) {// Données complementaire
                    if (Atexo_Module::isEnabled('DonneesRedac')) { //Données Redac
                        $this->donneeFormePrix->afficherDataFormeMarcheFormePrix();
                    }
                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                        $this->variantesNon->Checked = true;
                    }
                }
            }
            if ($this->_consultation instanceof CommonConsultation) {
                $atexoRef = new Atexo_Ref();
                $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'));
                $atexoRef->setPrincipal($this->_consultation->getCodeCpv1());
                $atexoRef->setSecondaires($this->_consultation->getCodeCpv2());
                $this->setViewState('atexoRef', $atexoRef);

                $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->getConsultation()->getIdTypeProcedureOrg());
                if ($procEquivalence instanceof CommonProcedureEquivalence) {
                    $this->gererAffichageCodeCpv($procEquivalence);
                }
            }
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {// Données complementaire
                if ($this->_lot instanceof CommonCategorieLot && $this->_lot->getDonneComplementaire() instanceof CommonTDonneeComplementaire) {
                    $donneesComplementaire = $this->_lot->getDonneComplementaire();
                } else {
                    $donneesComplementaire = new CommonTDonneeComplementaire();
                }
                //Echantillons demandés
                if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                    $this->echantillonsDemandes->setDonneesComplementaires($donneesComplementaire);
                    $this->echantillonsDemandes->setAfficher(true);
                    $this->echantillonsDemandes->setLangue($this->getLangue());
                    $this->echantillonsDemandes->setValidationGroup('validateInfosConsultation');
                    $this->echantillonsDemandes->chargerComposant();
                }
                //Reunions
                if (Atexo_Module::isEnabled('ConsultationReunion')) {
                    $this->reunions->setDonneesComplementaires($donneesComplementaire);
                    $this->reunions->setAfficher(true);
                    $this->reunions->setLangue($this->getLangue());
                    $this->reunions->setValidationGroup('validateInfosConsultation');
                    $this->reunions->chargerComposant();
                }
                //Visites des lieux
                if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                    $this->visitesLieux->setDonneesComplementaires($donneesComplementaire);
                    $this->visitesLieux->setConsultation($this->_consultation);
                    if ($this->_lot) {
                        $numLot = $this->_lot->getLot();
                    }
                    $this->visitesLieux->setLot($numLot);
                    $this->visitesLieux->setAfficher(true);
                    $this->visitesLieux->setLangue($this->getLangue());
                    $this->visitesLieux->setValidationGroup('validateInfosConsultation');
                    $this->visitesLieux->chargerComposant();
                }
                if (Atexo_Module::isEnabled('DonneesRedac')) { //Données Redac
                    if ($donneesComplementaire instanceof CommonTDonneeComplementaire) {
                        $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setDonneeComplementaire($donneesComplementaire);
                    }
                    $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setDonneeComplementaireCons($donneesComplementaireCons);
                    $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setObjet($this->_lot);
                    $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->chargerComposant();
                    if (Atexo_Module::isEnabled('Publicite') && Atexo_Module::isEnabled('AfficherValeurEstimee')) {
                        $this->donneeMontantMarche->chargerComposant();
                    }
                }
            }

            $this->referentielCPV->setCpvParent($this->getConsultation()->getCodeCpv1(), $this->getConsultation()->getCodeCpv2(), $this->getConsultation()->getCategorie());
        } else {
            $donneesComplementaireCons = $this->_consultation->getDonneComplementaire();
            $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setDonneeComplementaireCons($donneesComplementaireCons);
            if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac') && $categorieLot instanceof CommonCategorieLot) {// Données complementaire
                $donneeComplementaire = $categorieLot->getDonneComplementaire();
                $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->setDonneeComplementaireCons($donneeComplementaire);
            }
        }
        //Qualifications
        if (Atexo_Module::isEnabled('ConsultationQualification') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->qualification->setAfficher(true);
            if ($this->_lot) {
                $this->qualification->setObjet($this->_lot);
                $this->qualification->setConnexion($connexion);
                $this->qualification->chargerComposant();
            }
        } else {
            $this->qualification->setAfficher(false);
            $this->panelQualification->Visible = false;
        }
        //Agrements
        if (Atexo_Module::isEnabled('ConsultationAgrement') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->agrements->setAfficher(true);
            if ($this->_lot) {
                $this->agrements->setObjet($this->_lot);
            }
            $this->agrements->chargerComposant();
            $this->agrements->setConnexion($connexion);
        } else {
            $this->agrements->setAfficher(false);
            $this->panelAgrement->Visible = false;
        }

        //DUME
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $this->buttonSave->Visible = false;
        } else {
            $this->buttonSaveWithDume->Visible = false;
        }

        $this->customize();
    }

    /*
     * Permet d'enregistrer les données d'un lot
     */
    public function saveLot($sender, $param)
    {
        $donneeComplementaire = null;
        $connexion = null;
        if ($this->isValid) {
            $consultationId = $this->_reference;
            $organisme = $this->_organisme;
            $consultation = $this->_consultation;

            if (isset($_GET['idLot'])) {
                $modification = true;
                // ModificationCommonCategorieLotPeer
                $numLot = Atexo_Util::atexoHtmlEntities($_GET['idLot']);
                $newNumLot = Atexo_Util::replaceSpecialCharacters($this->numLot->Text);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

                if ($numLot != $newNumLot) {
                    // update le numèro de lot par une requète sql
                    // c'est pas possible de le faire par propel puisque on aurra dans le where la nouvelle valeur du lot qui réellement n'existe pas encore
                    $queryUpdateLot = 'UPDATE CategorieLot SET LOT = '.$newNumLot.' ';
                    $queryUpdateLot .= " WHERE ORGANISME='".$organisme."' AND consultation_id=".$consultationId.' AND LOT='.$numLot.'';
                    $statement = $connexion->prepare($queryUpdateLot);
                    $statement->execute();

                    $queryUpdateRef = 'UPDATE Referentiel_Consultation SET LOT='.$newNumLot.' ';
                    $queryUpdateRef .= " WHERE ORGANISME='".$organisme."' AND REFERENCE=".$consultationId.' AND LOT='.$numLot.'';
                    $statement = $connexion->prepare($queryUpdateRef);
                    $statement->execute();
                    $numLot = $newNumLot;
                }

                $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $numLot, $organisme);

                if ($lot instanceof CommonCategorieLot) {
                    $donneeComplementaire = $lot->getDonneComplementaire();

                    if (!($donneeComplementaire instanceof CommonTDonneeComplementaire)) {
                        $donneeComplementaire = new CommonTDonneeComplementaire();

                        if (Atexo_Module::isEnabled('DonneesComplementaires')) {// Données complementaire
                            $donneeComplementaire->setIdFormePrix('');
                        }
                    }
                }
            } else {
                $modification = false;
                //Création
                $lot = new CommonCategorieLot();
                $donneeComplementaire = new CommonTDonneeComplementaire();

                if (Atexo_Module::isEnabled('DonneesComplementaires')) {// Données complementaire
                    $donneeComplementaire->setIdFormePrix('');
                }

                $numLot = Atexo_Util::replaceSpecialCharacters($this->numLot->Text);
                $lot->setlot($numLot);
            }

            $intituleLot = Atexo_Util::replaceSpecialCharacters((new Atexo_Config())->toPfEncoding($this->intituleLot->Text));
            $descriptionLot = Atexo_Util::replaceSpecialCharacters((new Atexo_Config())->toPfEncoding($this->DescriptionLot->Text));
            $categorie = Atexo_Util::replaceSpecialCharacters($this->categorieLot->getSelectedValue());

            $lot->setDescriptionDetailTraduite(Atexo_Languages::readLanguageFromSession(), $descriptionLot);
            $lot->setDescriptionTraduite(Atexo_Languages::readLanguageFromSession(), $intituleLot);

            $lot->setCategorie($categorie);
            $lot->setConsultationId($consultationId);
            $lot->setOrganisme($organisme);

            $codePrincipalLot = $this->referentielCPV->getCpvPrincipale();
            $codesSecLot = $this->referentielCPV->getCpvSecondaires();
            $lot->setCodeCpv1($codePrincipalLot);
            $lot->setCodeCpv2($codesSecLot);

            // Données complementaires
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                //donnée complementaire
                if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    //caution provisoire
                    if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
                        $cautionProvisoireOld = $donneeComplementaire->getCautionProvisoire();
                        $donneeComplementaire->setCautionProvisoire($this->cautionProvisoire->Text);
                        $this->saveHistoriqueCaution($cautionProvisoireOld, $consultation, $donneeComplementaire, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $numLot, $modification);
                    }

                    // variante et variante calcule
                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                        $varianteOld = $donneeComplementaire->getVariantes();
                        $variantes = '0';

                        if ($this->variantesOui->Checked) {
                            $variantes = '1';
                        }

                        $donneeComplementaire->setVariantes($variantes);
                        $this->saveHistoriqueVariantes($varianteOld, $consultation, $donneeComplementaire, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $numLot, $modification);
                    }

                    //Echantillons
                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                        $this->echantillonsDemandes->save($donneeComplementaire, $consultation, $lot->getLot(), $connexion, $modification);
                    }

                    //Reunions
                    if (Atexo_Module::isEnabled('ConsultationReunion')) {
                        $this->reunions->save($donneeComplementaire, $consultation, $lot->getLot(), $connexion, $modification);
                    }

                    //Visites des lieux
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                        $this->visitesLieux->save($consultation, $lot->getLot(), $donneeComplementaire, $connexion, $modification);
                    }

                    $donneeComplementaire->save($connexion);

                    //Qualification
                    if (Atexo_Module::isEnabled('ConsultationQualification')) {
                        $this->qualification->save($lot, false, $modification);
                    }

                    //Qualification
                    if (Atexo_Module::isEnabled('ConsultationAgrement')) {
                        $this->agrements->save($lot, false, $modification);
                    }

                    if (Atexo_Module::isEnabled('DonneesRedac')) { //Données Redac
                        $this->donneeFormePrix->save($donneeComplementaire);
                        $this->donneeDureeMarche->saveDureeMarche($donneeComplementaire);

                        if (Atexo_Module::isEnabled('Publicite') && Atexo_Module::isEnabled('AfficherValeurEstimee')) {
                            $this->donneeMontantMarche->saveMontantMarche($donneeComplementaire);
                        }

                        $this->donneeVariante->saveVariante($donneeComplementaire);
                        $this->bloc_etapeDonneesComplementaires_donneeCriteresAttribution->saveCritereAttribution($donneeComplementaire, $param);
                    }

                    $donneeComplementaire->save($connexion);
                }
            }

            $donneeComplementaire->save($connexion);

            $lot->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());
            $lot->save($connexion);

            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $this->saveAchatResponsable($lot->getId(), $this->achatResponsableConsultation->achatResponsableFormValue->Data);
            }
            //DUME
            if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $consultation->getDumeDemande()) {
                $c = new Criteria();
                $c->add(CommonTDumeContextePeer::ORGANISME, $consultation->getOrganisme());
                $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $consultation->getId());
                $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
                $tDumeContexte = CommonTDumeContextePeer::doSelectOne($c, $connexion);

                if (null != $tDumeContexte) {
                    Atexo_Dume_AtexoDume::checkPurge($consultation, $tDumeContexte->getContexteLtDumeId());

                    Atexo_Dume_AtexoDume::createOrUpdateDumeContexte($consultation);
                }
            }

            // refrentiel zone de text
            $this->idReferentielZoneTextLot->saveConsultationAndLot($consultationId, $numLot);


            // referentiel
            $this->ltRefLot->saveReferentielConsultationAndLot($consultationId, $numLot);

            //referebtiel radio
            $this->idReferentielRadioLot->saveConsultationAndLot($consultationId, $numLot);
            $refresh = Atexo_Util::atexoHtmlEntities($_GET['idButtonRefresh']);
            $this->script->Text = "<script>opener.document.getElementById('".$refresh."').click();window.close();</script>";
        }
    }

    /**
     * Verifier le numèro du lot.
     */
    public function verifyNumLot($sender, $param)
    {
        $numLot = trim($this->numLot->Text);

        if ($numLot) {
            try {
                $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->_reference, $numLot, $this->_organisme);
            } catch (\Exception) {
                $param->IsValid = false;
                $this->numLotValidator->ErrorMessage .= Prado::localize('TEXT_LOT_ALPHANUMERIQUE');
                $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

                return false;
            }

            if ($categorieLot instanceof CommonCategorieLot) {
                if (isset($_GET['idLot'])) {
                    if ($categorieLot->getLot() != Atexo_Util::atexoHtmlEntities($_GET['idLot'])) {
                        $param->IsValid = false;
                        $this->numLotValidator->ErrorMessage = Prado::localize('DEFINE_LOT_DEJA_EXISTE');
                        $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

                        return false;
                    }
                } else {
                    $param->IsValid = false;
                    $this->numLotValidator->ErrorMessage = Prado::localize('DEFINE_LOT_DEJA_EXISTE');
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /* Permet de sauvegarder l'historique Variante
     * @param $consultation: Objet consultation
     * @param $consultation: Objet donnees complementaire
     * @param $consultation: num lot
     * @param $consultation: l'ancienne donnee
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueVariantes($oldVariante, $consultation, $donneComplementaire, $connexion, $org, $lot, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && ($oldVariante != $donneComplementaire->getVariantes() || !$modification)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES');
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($donneComplementaire->getVariantes()) {
                $valeur = '1';
                $detail1 = $donneComplementaire->getVariantes();
            }
            if ($this->isModificationAllowed($consultation)) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Caution.
     *
     * @param $consultation: Objet consultation
     * @param $consultation: Objet donnees complementaire
     * @param $consultation: num lot
     * @param $consultation: l'ancienne donnee
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueCaution($cautionProvisoireOld, $consultation, $donneComplementaire, $connexion, $org, $lot, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && (trim($cautionProvisoireOld) != trim($donneComplementaire->getCautionProvisoire()) || !$modification)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CAUTION_PROVISOIRE');
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($donneComplementaire->getCautionProvisoire()) {
                $valeur = '1';
                $detail1 = $donneComplementaire->getCautionProvisoire();
            }
            if ($this->isModificationAllowed($consultation)) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, '', isset($_GET['id']));
    }

    public function customize()
    {
        //Echantillons demandés
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->echantillonsDemandes->setValidationGroup('validateInfosConsultation');
        }
        //Reunions
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->reunions->setValidationGroup('validateInfosConsultation');
        }
        //Visites des lieux
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->visitesLieux->setValidationGroup('validateInfosConsultation');
        }
    }

    public function displayreferentiel($sende, $param)
    {
    }

    public function showHideDonnesRedac($choix)
    {
        if ('1' == $choix) {
            $this->script->Text = "<script>
				J('.panel-donnees-redac .panel').css('display','block');
				J('.title-toggle').removeClass('title-toggle').addClass('title-toggle-open')
				J('.title-toggle-open').removeClass('detail-toggle-inactive');
				J('.donnees-redac').removeClass('panel-off');
				</script>";
        } else {
            $this->script->Text = "<script>
                J('.panel-donnees-redac').css('display','none');
				J('.panel-donnees-redac .panel').css('display','none');
				J('.title-toggle').addClass('detail-toggle-inactive');
				J('.donnees-redac').addClass('panel-off');
				J('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
			   </script>";
        }
    }

    /**
     * permet de mettre à jour les valeurs selectionnees.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getInheritedCpv($sender, $param)
    {
        if (true == $this->cpvHeriteeConsultation->checked) {
            $this->referentielCPV->updateSelection($this->getConsultation()->getCodeCpv1(), $this->getConsultation()->getCodeCpv2());
        } else {
            $this->referentielCPV->updateSelection();
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function checkPurge($sender, $param)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
        );

        //DUME
        $doSaveLot = true;
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $c = new Criteria();
            $c->add(CommonTDumeContextePeer::ORGANISME, $this->_consultation->getOrganisme());
            $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $this->_consultation->getId());
            $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
            $tDumeContexte = CommonTDumeContextePeer::doSelectOne($c, $connexion);

            if (null != $tDumeContexte) {
                if (isset($_GET['idLot'])) {
                    $modification = true;
                    $idLot = $_GET['idLot'];
                } else {
                    $modification = false;
                    $idLot = null;
                }

                $lot = [
                    'idLot' => $idLot, //idLot présent uniquement dans le cas d'une modification
                    'numLot' => Atexo_Util::replaceSpecialCharacters($this->numLot->Text),
                    'intituleLot' => Atexo_Util::replaceSpecialCharacters($this->intituleLot->Text),
                ];

                $checkPurge = Atexo_Dume_AtexoDume::checkPurge(
                    $this->_consultation,
                    $tDumeContexte->getContexteLtDumeId(),
                    null,
                    $lot,
                    $modification
                );

                if ($checkPurge['statut'] == Atexo_Config::getParameter('CHECK_PURGE_STATUS_PURGE')) {
                    $doSaveLot = false;
                    $this->script->Text = "<script>openModal('demander-validation','popup-small2',document.getElementById('panelValidationCheckPurge'), 'Demande de validation');</script>";
                }
            }
        }
        if ($doSaveLot) {
            $this->saveLot($sender, $param);
        }
    }

    public function gererAffichageCodeCpv($procEquivalence)
    {
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if (Atexo_Module::isEnabled('AffichageCodeCpv')
                    && $procEquivalence->getAfficherCodeCpv()
            ) {
                $this->referentielCPV->setAfficherCodeCpv(true);
                $this->panelCpvHeriteeConsultation->setDisplay('Dynamic');
            } else {
                $this->referentielCPV->setAfficherCodeCpv(false);
                $this->panelCpvHeriteeConsultation->setDisplay('None');
            }

            if (Atexo_Module::isEnabled('AffichageCodeCpv')
                    && Atexo_Module::isEnabled('CodeCpv')
                    && $procEquivalence->getAfficherCodeCpv()
                    && $procEquivalence->getCodeCpvObligatoire()
            ) {
                $this->referentielCPV->setCodeCpvObligatoire(true);
                $this->enabledCodeCPV->setValue('1');
            } else {
                $this->referentielCPV->setCodeCpvObligatoire(false);
                $this->enabledCodeCPV->setValue('0');
            }
        }
    }

    protected function saveAchatResponsable(int $idLot, string $values): void
    {
        /** @var ClauseUsePrado $clause */
        $clause = Atexo_Util::getSfService(ClauseUsePrado::class);
        $data = [
            'values' => json_decode($values, true),
            'id_lot' => $idLot
        ];

        $clause->getContent('sendLot', $data);
    }

}
