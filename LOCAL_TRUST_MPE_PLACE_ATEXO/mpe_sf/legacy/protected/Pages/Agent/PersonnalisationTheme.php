<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ThemeGraphique;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PersonnalisationTheme extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('PersonnaliserAffichageThemeEtIllustration')) {
            if (!$this->isPostBack) {
                self::displayThemes();
            } else {
                $this->script->Text = '';
            }
        } else {
            $this->response->redirect('agent');
        }
    }

    /*
     * permet de remplir le drop down liste et le repeater par les themes qui existent
     */
    public function displayThemes()
    {
        $themes = (new Atexo_ThemeGraphique())->getAllThemeGraphique(Atexo_Config::getParameter('THEME_GRAPHIQUE_ACTIVER'));
        $dataListe = [];
        $dataApercu = [];
        $i = 0;
        $idAgent = Atexo_CurrentUser::getIdAgentConnected();
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            if ($agent->getCodeTheme()) {
                $themeActive = $agent->getCodeTheme();
                $dataApercu[$i]['actif'] = false;
            } else {
                $themeActive = Atexo_Config::getParameter('CODE_DEFAULT_THEME');
                $dataApercu[$i]['actif'] = true;
            }
            $dataApercu[$i]['code'] = Atexo_Config::getParameter('CODE_DEFAULT_THEME');
            $dataApercu[$i++]['libelle'] = Prado::localize('DEFINE_DEFAULT_THEME');
            if (is_array($themes)) {
                foreach ($themes as $theme) {
                    $dataListe[$theme->getCode()] = Prado::localize($theme->getLibelle());
                    $dataApercu[$i]['code'] = $theme->getCode();
                    if ($themeActive == $theme->getCode()) {
                        $dataApercu[$i]['actif'] = $theme->getActif();
                    } else {
                        $dataApercu[$i]['actif'] = false;
                    }
                    $dataApercu[$i]['libelle'] = Prado::localize($theme->getLibelle());
                    ++$i;
                }
            }
        }
        array_unshift($dataListe, Prado::localize('DEFINE_DEFAULT_THEME'));
        $dataDropDownListe = [];
        foreach ($dataListe as $k => $value) {
            if ('0' == $k) {
                $dataDropDownListe[Atexo_Config::getParameter('CODE_DEFAULT_THEME')] = $value;
            } else {
                $dataDropDownListe[$k] = $value;
            }
        }
        $this->themeGraphique->dataSource = $dataDropDownListe;
        $this->themeGraphique->DataBind();
        $this->themeGraphique->SelectedValue = $themeActive;
        $this->repeaterThemeApercu->dataSource = $dataApercu;
        $this->repeaterThemeApercu->DataBind();
    }

    /*
     * Permet d'enregistrer le theme choisi
     *
     */
    public function saveTheme($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idAgent = Atexo_CurrentUser::getIdAgentConnected();
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        $selctedValue = $this->themeGraphique->SelectedValue;
        if ($agent instanceof CommonAgent) {
            if ($selctedValue == Atexo_Config::getParameter('CODE_DEFAULT_THEME')) {
                $agent->setCodeTheme(0);
            } else {
                $agent->setCodeTheme($selctedValue);
            }
            $agent->save($connexion);
        }

        $this->script->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();</script>";
    }

    public function refreshRepeater($sender, $param)
    {
        self::displayThemes();
    }
}
