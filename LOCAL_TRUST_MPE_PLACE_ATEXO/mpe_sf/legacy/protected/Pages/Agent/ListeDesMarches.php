<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

class ListeDesMarches extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->tableauListeMarches->setPostBack($this->IsPostBack);
        $this->tableauListeMarches->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
    }
}
