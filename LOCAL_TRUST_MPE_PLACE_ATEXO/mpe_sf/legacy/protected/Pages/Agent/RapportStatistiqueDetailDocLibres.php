<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

class RapportStatistiqueDetailDocLibres extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $uri = '/reports/ORME_STAT/Export_Documents_Libres';
        $this->criteresStatistiques->setUri($uri);
        $this->criteresStatistiques->setNomStat(Prado::localize('TEXT_CONSULTATIONS_DETAIL'));
    }
}
