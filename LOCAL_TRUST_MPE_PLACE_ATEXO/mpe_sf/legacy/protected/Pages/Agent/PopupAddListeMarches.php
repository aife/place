<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonFichiersListeMarches;
use Application\Propel\Mpe\CommonFichiersListeMarchesPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupAddListeMarches extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
    }

    /**
     * Verifier la taille maximum du document à ajouter
     * ( 2Mo maximum).
     */
    public function verifyDocTailleCreation($sender, $param)
    {
        $sizedocAttache = $this->ajoutFichier->FileSize;
        if (0 == $sizedocAttache) {
            $this->labelClose->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_FICHIER_VIDE');
        } elseif ($sizedocAttache >= 2 * 1024 * 1024) {
            $this->labelClose->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_TAILLE_DOC');
        }
    }

    /**
     * Verifier les éxtensions acceptées.
     */
    public function verifyExtensionDoc($sender, $param)
    {
        $extensionFile = strtoupper(Atexo_Util::getExtension($this->ajoutFichier->FileName));

        $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
        if (!in_array($extensionFile, $data)) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = str_replace('[__EXTENSION_FILES__]', implode(',', $data), Prado::localize('TEXT_MSG_EXTESION_NON_VALIDE'));

            return;
        }
    }

    /* ajouter un document dans la base
     *
       *
       */
    public function addPieceJointe($sender, $param)
    {
        if ($this->ajoutFichier->HasFile) {
            $infile = Atexo_Config::getParameter('COMMON_TMP').'listeMarches'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $atexoBlob = new Atexo_Blob();
                $atexoCrypto = new Atexo_Crypto();
                $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                if (isset($_GET['id'])) {
                    $pj = CommonFichiersListeMarchesPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                } else {
                    $pj = new CommonFichiersListeMarches();
                    $pj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                }
                if (is_array($arrayTimeStampAvis)) {
                    $pj->setHorodatage($arrayTimeStampAvis['horodatage']);
                    $pj->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                }
                $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, Atexo_CurrentUser::getCurrentOrganism());
                $pj->setNomFichier($this->ajoutFichier->FileName);
                $pj->setTaille($this->ajoutFichier->FileSize);
                $pj->setFichier($avisIdBlob);
                $pj->save($connexionCom);
            }
            @unlink($infile);
        }
        $this->labelClose->Text = '<script>refreshRepeaterListeMarches();</script>';
    }
}
