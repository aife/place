<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonChorusPjPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger le document.
 *
 * @author Mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChorusDownloadFile extends DownloadFile
{
    private string $_identifiant = '';
    private string|bool $_organisme = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['id'])) {
            $this->_identifiant = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
            $this->downloadPiece();
        }
    }

    public function downloadPiece()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $piece = CommonChorusPjPeer::retrieveByPK($this->_identifiant, $this->_organisme, $connexion);
        if ($piece) {
            $this->_idFichier = $piece->getFichier();
            $this->_nomFichier = $piece->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
        }
    }
}
