<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCompteCentralePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;

/**
 * Page de creation de compte cen trale de publication d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionCentralePub extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->ChoixEntiteAchat->setVisible(true);
            $this->setViewState('idService', Atexo_CurrentUser::getIdServiceAgentConnected());
            $this->lienAjouter->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopupCentralePub&idService=".$this->getViewState('idService')."','yes');";
            $this->displayComptesCentrales();
        }
    }

    public function displayComptesCentrales()
    {
        $this->repeaterComptesCentrale->DataSource = (new Atexo_Publicite_CentralePublication())->retreiveListeComptesCentraleByService($this->getViewState('idService'));
        $this->repeaterComptesCentrale->DataBind();
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouter->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopupCentralePub&idService=".$this->_selectedEntity."','yes');";
        $this->repeaterComptesCentrale->DataSource = (new Atexo_Publicite_CentralePublication())->retreiveListeComptesCentraleByService($this->getViewState('idService'));
        $this->repeaterComptesCentrale->DataBind();
        // Rafraichissement du ActivePanel
        $this->mainPanel->render($param->NewWriter);
    }

    public function onCallBackRefreshRepeater($sender, $param)
    {
        $this->displayComptesCentrales();
        $this->mainPanel->render($param->getNewWriter());
    }

    /** Pour insérer un retour à la ligne (<br>), tous les 22 caractères, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine après y avoir insérer un retour à la lligne tous les $everyHowManyCaracter caractères
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    public function deleteCompteCentrale($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idCompte = $param->CommandName;
        if ($idCompte) {
            $c = new Criteria();
            $c->add(CommonCompteCentralePeer::ID_COMPTE, $idCompte);
            $c->add(CommonCompteCentralePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            CommonCompteCentralePeer::doDelete($c, $connexionCom);
        }
    }
}
