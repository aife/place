<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEnchereOffrePeer;
use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Class pour la suivi temps réelle d'une enchere.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GraphSuivi extends MpeTPage
{
    private $_idEnchere;
    private string $_type = 'note';
    private $graph;

    public function setIdEnchere($idEnchere)
    {
        $this->_idEnchere = $idEnchere;
    }

    public function getIdEnchere()
    {
        return $this->_idEnchere;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }

    private function drawGraph()
    {
        // TODO :
        // Sinon ca renvoie une erreur E_STRICT ...
        date_default_timezone_set('Europe/Paris');
        require_once 'Artichow/ScatterPlot.class.php';

        $graph = new Graph(900, 600);
        $graph->setAntiAliasing(true);
        $group = new PlotGroup();

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $Org = Atexo_CurrentUser::getCurrentOrganism();
        $enchere = CommonEncherePmiPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEnchere']), $connexion);
        $tsDebutEnchere = strtotime($enchere->getDateDebut());
        $tsDebutEnchere = 0;

        $entreprises = $enchere->getCommonEnchereEntreprisePmis(null, $connexion);
        foreach ($entreprises as $entreprise) {
            $colorIndex = $entreprise->getNumeroAnonyme();

            $c = new Criteria();
            $c->addAscendingOrderByColumn(CommonEnchereOffrePeer::DATE);
            $enchereOffres = $entreprise->getCommonEnchereOffres($c, $connexion);

            $valeursNgcOffres = [];
            $datesRelativeOffres = [];

            foreach ($enchereOffres as $offre) {
                $tsOffre = strtotime($offre->getDate());
                $datesRelativeOffres[] = floor(($tsOffre - $tsDebutEnchere));
                $valeursNgcOffres[] = ('note' == $this->_type) ? floor($offre->getValeurngc()) : floor($offre->getValeurtc());
            }

            if (count($valeursNgcOffres) >= 1) {
                $plot = new ScatterPlot($valeursNgcOffres, $datesRelativeOffres);
                $color = new Color((73 * $colorIndex) % 255, (143 * $colorIndex) % 255, (27 * $colorIndex) % 255);
                $plot->setColor($color);
                $plot->mark->setSize(5);
                $plot->mark->setFill($color);
                $plot->link(true);
                $plot->linkNull(true);

                $group->add($plot);
                $group->legend->add($plot, $entreprise->getNom(), Legend::MARK);
                $group->legend->setAlign(Legend::RIGHT, Legend::MIDDLE);
            }
        }

        $group->axis->left->setLabelNumber(10);
        $group->axis->bottom->setLabelNumber(0);
        $group->axis->bottom->hideTicks(true);
        $group->axis->bottom->title->set(Prado::localize('TEMPS'));
        $group->grid->setInterval(1, 60);

        $group->setPadding(80, 240, 60, 50);
        $group->setBackgroundColor(new Color(240, 240, 240));

        if ('note' == $this->_type) {
            $graph->title->set(Prado::localize('SUIVI_NOTE'));
        } else {
            $graph->title->set(Prado::localize('SUIVI_PRIX'));
        }
        $graph->add($group);
        $graph->shadow->setSize(2);

        return $graph;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $Org = Atexo_CurrentUser::getOrganismAcronym();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $enchere = CommonEncherePmiPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEnchere']), $connexion);

        if (isset($_GET['type'])) {
            $this->setType(Atexo_Util::atexoHtmlEntities($_GET['type']));
        }

        if (isset($_GET['idEnchere'])) {
            $this->setIdEnchere(Atexo_Util::atexoHtmlEntities($_GET['idEnchere']));
        }

        $nbOffres = $enchere->countCommonEnchereOffres(null, false, $connexion);

        if (0 == $nbOffres) {
            return;
        }

        $this->graph = $this->drawGraph();
        $this->graph->draw();
    }
}
