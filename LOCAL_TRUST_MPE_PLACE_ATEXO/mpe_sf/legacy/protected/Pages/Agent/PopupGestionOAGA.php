<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusGroupementAchat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Prado\Prado;

/**
 * Admministration des fournisseurs de documents.
 */
class PopupGestionOAGA extends MpeTPage
{
    private string $organisme = '';
    private string $idService = '';

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getIdService()
    {
        return $this->idService;
    }

    /**
     * @param string $idService
     */
    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $messageErreur = null;
        $this->panelMessageErreur->setVisible(false);
        $organisme = Atexo_CurrentUser::hasHabilitation('HyperAdmin') ? base64_decode(Atexo_Util::atexoHtmlEntities($_GET['organisme'])) : Atexo_CurrentUser::getCurrentOrganism();
        $idService = isset($_GET['idService']) ? base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idService'])) : Atexo_CurrentUser::getIdServiceAgentConnected();
        $this->setIdService($idService);
        $this->setOrganisme($organisme);
        if ($organisme && $idService && Atexo_Module::isEnabled('InterfaceChorusPmi') && Atexo_CurrentUser::hasHabilitation('GererOaGa')) {
            if ($idService) {
                $EntitePurchaseBean = Atexo_EntityPurchase::retrieveEntityById($idService, $organisme);
                if ($EntitePurchaseBean) {
                    $this->libelle->Text = $EntitePurchaseBean->getSigle().' - '.$EntitePurchaseBean->getLibelle();
                }
            } else {
                $EntitePurchaseBean = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
                if ($EntitePurchaseBean) {
                    $this->libelle->Text = $EntitePurchaseBean->getSigle().' - '.$EntitePurchaseBean->getDenominationOrg();
                }
            }
            $this->panelGererOaGa->setVisible(true);
            if (!$this->getIsPostBack()) {
                self::displayOa();
            }
        } else {
            $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            $this->panelGererOaGa->setVisible(false);
        }
        if ($messageErreur) {
            $this->afficherMessageErreur($messageErreur);
        }
    }

    public function displayOa()
    {
        $data = [];
        $listeOA = (new Atexo_Chorus_Echange())->retreiveOrganisationAchatByOrganisme($this->getOrganisme(), 1);
        if (is_array($listeOA) && count($listeOA)) {
            $data = $listeOA;
        }
        $this->repeaterOA->DataSource = $data;
        $this->repeaterOA->DataBind();
    }

    public function isGaActif($codeGa, $idOA)
    {
        $groupementAchat = (new Atexo_Chorus_Echange())->retrieveGroupementAchatByCode($codeGa, $this->getOrganisme(), $idOA, $this->getIdService());
        if ($groupementAchat instanceof CommonChorusGroupementAchat) {
            return $groupementAchat->getActif();
        }

        return false;
    }

    public function saveGroupementAchat()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->repeaterOA->Items as $itemOA) {
            $idOa = $itemOA->idOa->value;
            foreach ($itemOA->repeaterGA->Items as $itemGA) {
                $codeGa = $itemGA->codeGa->value;
                $groupementAchat = (new Atexo_Chorus_Echange())->retrieveGroupementAchatByCode($codeGa, $this->getOrganisme(), $idOa, $this->getIdService());
                if ($this->getIdService() && !$groupementAchat && $itemGA->groupementAchatActif->checked) {
                    $groupementAchatOrganisme = (new Atexo_Chorus_Echange())->retrieveGroupementAchatByCode($codeGa, $this->getOrganisme(), $idOa, null);
                    if ($groupementAchatOrganisme instanceof CommonChorusGroupementAchat) {
                        $groupementAchat = $groupementAchatOrganisme->copy();
                        $groupementAchat->setServiceId($this->getIdService());
                    }
                }
                if ($groupementAchat instanceof CommonChorusGroupementAchat) {
                    if ($itemGA->groupementAchatActif->checked) {
                        $groupementAchat->setActif(1);
                    } else {
                        $groupementAchat->setActif(0);
                    }

                    $groupementAchat->save($connexion);
                }
            }
        }
        $this->labelClose->Text = '<script>window.close();</script>';
    }

    /*
     * Permet d'afficher le message d'erreur et cacher les autres composants
     *
     * @param string $messageErreur le message d'erreur
     * @return void
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function afficherMessageErreur($messageErreur)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageErreur);
    }
}
