<?php

namespace Application\Pages\Agent;
use App\Service\Autoformation\ModuleAutoformation;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Classe de.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentAutoformation extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
	        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
            MessageStatusCheckerUtil::initEntryPoints('module-autoformation');
	    }

	 public function onLoad($param)
 	{

 	}

    /**
     * @return string
     */
    public function getBody(): string
    {
        $moduleAutoformation = Atexo_Util::getSfService(ModuleAutoformation::class);

        return $moduleAutoformation->getBody('agent', false);
    }
}
