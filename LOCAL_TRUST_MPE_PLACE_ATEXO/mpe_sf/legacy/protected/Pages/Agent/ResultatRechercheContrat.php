<?php

namespace Application\Pages\Agent;

use App\Service\Bandeau\Menu\Action\Spaser;
use App\Service\Routing\RouteBuilderInterface;
use App\Service\Spaser\IndicateurProgress;
use App\Service\Spaser\SpaserUtils;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;

use App\Utils\Encryption;

use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonTConsLotContratQuery;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireFavori;
use Application\Propel\Mpe\CommonTContratTitulaireFavoriQuery;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_Gestion;
use Application\Service\Atexo\Contrat\Atexo_Contrat_IndicateurEnum;
use Prado\Prado;

use Application\Service\Atexo\Atexo_MultiDomaine;

/**
 * Résultat de recherche des contrats.
 *
 * @author Fatima Ezzahra <fatima.izgua@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 4.11.0
 */
class ResultatRechercheContrat extends MpeTPage
{
    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        // Redirect to exec component page
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $routeBuilderService = Atexo_Util::getSfService(RouteBuilderInterface::class);
            if ($keyword = $this->getFormatedValues('keyWord')) {
                $urlRedirection = $routeBuilderService->getRoute(
                    'agent_web_component_contrat_list_keyword',
                    ["keyword" => $keyword]
                );
            } else {
                $urlRedirection = $routeBuilderService->getRoute('agent_web_component_contrat_list');
            }

            $this->response->redirect($urlRedirection);
        }

        $this->scriptJs->setText('');
        if (!$this->getIsPostBack()) {
            $this->fillTypeContrat();
            $this->fillListeJustif();
            $this->fillListeJustifMarcheNonPublie();
            if (isset($_GET['advanceSearch'])) {
                $this->rechercheAvanceeContrat->setVisible(true);
                $this->resultSearch->setVisible(false);
                $this->advancedSearchContrat->initialiserComposant();
            } else {
                self::displayFilters();
                $criteriaVo = $this->chargerCriteresParDefaut(true);

                if (isset($_GET['contrat'])) {
                    $criteriaVo->setIdContratTitulaire($_GET['contrat']);
                }
                if (isset($_GET['contratMulti'])) {
                    $criteriaVo->setIdContratMulti($_GET['contratMulti']);
                }
                if (isset($_GET['contratAcSad'])) {
                    $criteriaVo->setLienAcSad($_GET['contratAcSad']);
                }
                if (isset($_GET['keyWord'])) {
                    $criteriaVo->setMotsCles(Atexo_Util::atexoHtmlEntities($_GET['keyWord']));
                }
                if (isset($_GET['searchAcSad']) || isset($_GET['keyWord'])) {
                    $criteriaVo->setContratOrganismeInvite(true);
                }
                if (isset($_GET['searchAcSad'])) {
                    $typeQuery = new CommonTTypeContratQuery();
                    $types = $typeQuery->getTypeContratACSAD('0', Criteria::NOT_EQUAL);
                    $criteriaVo->setTypes($types);
                }

                if (isset($_GET['retour'])) {
                    $this->getCriteraVo();
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    $criteriaVo = $this->chargerCriteresParDefaut();
                }

                $this->fillRepeaterWithDataForSearchResult($criteriaVo);
                $this->populateData($criteriaVo);
            }
        }
    }

    public function isFromAcSad()
    {
        if (isset($_GET['searchAcSad'])) {
            return true;
        }

        return false;
    }

    public function hasDroitModification($data)
    {
        return $data->isCurrectUserPermanentAgent() && $data->getOrganisme() == Atexo_CurrentUser::getOrganismAcronym() && Atexo_CurrentUser::hasHabilitation('ModifierContrat') && !$data->isContratChapeau();
    }

    public function hasDroitConsultation($data)
    {
        return Atexo_CurrentUser::hasHabilitation('ConsulterContrat') && !$data->isContratChapeau() && $data->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
    }

    public function hasDroitSuppression($contrat)
    {
        $hasDroit = false;
        if ($contrat->isCurrectUserPermanentAgent() && $contrat->getOrganisme() == Atexo_CurrentUser::getOrganismAcronym() && Atexo_CurrentUser::hasHabilitation('SupprimerContrat') && !$contrat->isContratChapeau()) {
            $hasDroit = true;
        }
        if (Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn') && ($contrat->isCurrectUserPermanentAgent() && $contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))) {
            $hasDroit = false;
        }

        return $hasDroit;
    }

    public function hasDroitNotification($contrat)
    {
        $hasDroit = false;
        if ($contrat->isCurrectUserPermanentAgent() && Atexo_CurrentUser::hasHabilitation('ModifierContrat') && !$contrat->isContratChapeau() && $contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
            $hasDroit = true;
            if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected()) && $contrat->getModeEchangeChorus() && !(new Atexo_Consultation_Contrat())->isRetourChorus($contrat)) {
                $hasDroit = false;
            }
        }

        return $hasDroit;
    }

    public function hasDroitLancerConsultation($data)
    {
        $droit = false;
        if ($data instanceof CommonTContratTitulaire && $data->isAcSad()) {
            $droit = true;
            if ((2 == $data->getContratClassKey() && !$data->hasContratNotifie())
                ||
                (1 == $data->getContratClassKey() && $data->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))
            ) {
                $droit = false;
            }
        }

        return $droit;
    }

    public function search()
    {
        $criteriaVo = $this->chargerCriteresParDefaut(false);
        if (isset($_GET['searchAcSad'])) {
            $typeQuery = new CommonTTypeContratQuery();
            $types = $typeQuery->getTypeContratACSAD('0', Criteria::NOT_EQUAL);
            $criteriaVo->setTypes($types);
            $this->getCriteriaEntite($criteriaVo);
        }
        $criteriaVo->setMotsCles($this->filtreMotsCles->getSafeText());
        $this->getCriteriaTypeEtStatus($criteriaVo);

        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->populateData($criteriaVo);
    }

    /**
     * modifie le critere d'entite.
     *
     * @param Atexo_Contrat_CriteriaVo $criteriaVo
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getCriteriaEntite(&$criteriaVo)
    {
        if ($this->filtreContratEntite_1->checked && $this->filtreContratEntite_2->checked) {
            $param = Atexo_Config::getParameter('TOUTES_ENTITES');
        } elseif ($this->filtreContratEntite_2->checked) {
            $param = Atexo_Config::getParameter('TRANSVERSE');
        } elseif ($this->filtreContratEntite_1->checked) {
            $param = Atexo_Config::getParameter('MON_ENTITE_SEULEMENT');
        } else {
            $param = Atexo_Config::getParameter('TOUTES_ENTITES');
        }
        $criteriaVo->setContratOrganismeInvite($param);
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->chargerCriteresParDefaut();
        if ($criteriaVo instanceof Atexo_Contrat_CriteriaVo) {
            $criteriaVo->setChampOrderBy($champsOrderBy);
            $arraySensTri = $this->getViewState('sensTriArray', []);
            $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
            $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
            $this->setViewState('sensTriArray', $arraySensTri);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->setCriteraVo($criteriaVo);
            $this->listeContrat->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->listeContrat->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->chargerCriteresParDefaut();
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->listeContrat->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->listeContrat->CurrentPageIndex + 1;
        }
    }

    public function populateData(Atexo_Contrat_CriteriaVo $criteriaVo)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->setCriteraVo($criteriaVo);
        $offset = $this->listeContrat->CurrentPageIndex * $this->listeContrat->PageSize;
        $limit = $this->listeContrat->PageSize;
        if ($offset + $limit > $this->listeContrat->getVirtualItemCount()) {
            $limit = $this->listeContrat->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);

        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $listeContrats = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo, $connexion);

        // Ajout de la progression de la saisie des indicateurs Spaser pour chaque contrat
        if ($this->isSpaserButtonAccessGranted()) {
            $progressionIndicateurs = $this->getSaisieIndicateurProgressStatus($listeContrats);
        }
        foreach ($listeContrats as $listeContrat) {
            $listeContrat->indicateurProgress = $progressionIndicateurs[$listeContrat->getIdContratTitulaire()] ?? null;
        }

        $this->listeContrat->DataSource = $listeContrats;
        $this->listeContrat->DataBind();
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $criteriaVo->setCount(true);
        $criteriaVo->setLimit(null);
        $criteriaVo->setOffset(null);
        $nombreElement = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo, $connexion);
        $criteriaVo->setCount(false);
        if ($nombreElement >= 1) {
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->listeContrat->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeContrat->PageSize);
            $this->listeContrat->setVirtualItemCount($nombreElement);
            $this->listeContrat->setCurrentPageIndex(0);
            $this->panelAucunResult->setVisible(false);
            $this->panelResult->setVisible(true);
            $this->populateData($criteriaVo);
        } else {
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->panelResult->setVisible(false);
            $this->panelAucunResult->setVisible(true);
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->listeContrat->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->chargerCriteresParDefaut();
        $this->populateData($criteriaVo);
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->numPageTop->Text);
        $criteriaVo = $this->chargerCriteresParDefaut();
        $this->populateData($criteriaVo);
    }

    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nombreElement');
        $this->listeContrat->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->listeContrat->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeContrat->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->listeContrat->setCurrentPageIndex($numPage - 1);
        $this->numPageBottom->Text = $numPage;
        $this->numPageTop->Text = $numPage;
    }

    /**
     * Permet de retourner l'etape de decision.
     *
     * @param string $statut le statut du contrat
     *
     * @return string l'etape
     *
     * @author Fatima Ezzahra <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getEtapeDecision(string $statut)
    {
        return match ($statut) {
            Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR')=> 'contrat-etape  contrat-etape-bis contrat-etape2sur3',
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')=> 'contrat-etape  contrat-etape-bis contrat-etape3asur3',
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')=> 'contrat-etape  contrat-etape-bis contrat-etape3bsur3',
            default=> '',
        };
    }

    /**
     * Permet de retourner le titre de l'etape.
     *
     * @param string $statut le statut du contrat
     *
     * @return string le titre de l'etape
     *
     * @author Fatima Ezzahra <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getTitreEtape(string $statut)
    {
        return match ($statut) {
            Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR')=> Prado::Localize('DONNEES_DU_CONTRAT_A_SAISIR'),
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')=> Prado::Localize('ETAPE_NOTIFICATION'),
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')=> Prado::Localize('ETAPE_NOTIFICATION_EFFECTUEE'),
            default=> '',
        };
    }

    /**
     * Permet de retourner l'url de modification du contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string le titre de l'etape
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlMofification($contrat)
    {
        $url = '#';
        if ($contrat instanceof CommonTContratTitulaire) {
            $url = "javascript:popUp('?page=Agent.PopupDecisionContrat&idContrat=".base64_encode($contrat->getIdContratTitulaire())."','yes');";
        }

        return $url;
    }

    public function modifierFavori($sender, $param)
    {
        $currentItem = $sender->NamingContainer;
        $repeater = $currentItem->Parent;
        $id = $repeater->DataKeys[$currentItem->ItemIndex];

        $commonTContratTitulaireFavoriQuery = new CommonTContratTitulaireFavoriQuery();
        $contratFavori = $commonTContratTitulaireFavoriQuery->getContratFavoriByIdContratTitulaire($id);
        $contratFavori = $contratFavori[0];
        if ($repeater->items[$currentItem->ItemIndex]->favori->Checked) {
            if (!$contratFavori instanceof CommonTContratTitulaireFavori) {
                $contratFavori = new CommonTContratTitulaireFavori();
            }

            $contratFavori->setIdContratTitulaire($id);
            $contratFavori->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $contratFavori->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
            $contratFavori->save();
            $repeater->items[$currentItem->ItemIndex]->hiddenIsFav->value = 1;
        } else {
            $contratFavori->delete();
            $repeater->items[$currentItem->ItemIndex]->hiddenIsFav->value = 0;
        }

        $repeater->items[$currentItem->ItemIndex]->hiddenIsFav->render($param->NewWriter);
        $repeater->items[$currentItem->ItemIndex]->panelCheckBox->render($param->NewWriter);
    }

    public function chargerCriteresParDefaut($forcerInitialisation = false)
    {
        $arrayStatus = [];
        $criteriaVo = $this->getViewState('CriteriaVo');
        if ($forcerInitialisation || !$criteriaVo instanceof Atexo_Contrat_CriteriaVo) {
            $criteriaVo = new Atexo_Contrat_CriteriaVo();
            $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setIdAgentConnecte(Atexo_CurrentUser::getId());
            if (isset($_GET['wsState'])) {
                switch ($_GET['wsState']) {
                    case Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR'):
                        $arrayStatus[] = Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR');
                        $this->statutASaisir->checked = true;
                        break;
                    case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'):
                        $arrayStatus[] = Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT');
                        $this->statutANotifier->checked = true;
                        break;
                    case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'):
                        $arrayStatus[] = Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
                        $this->statutNotifier->checked = true;
                        break;
                    default:
                        break;
                }
                $criteriaVo->setStatus($arrayStatus);
            }
            if (isset($_GET['uuid'])) {
                $criteriaVo->setUuid($_GET['uuid']);
            }

            if (isset($_GET['contrat'])) {
                $criteriaVo->setIdContratTitulaire($_GET['contrat']);
            }
        }

        return $criteriaVo;
    }

    public function getCriteriaTypeEtStatus(&$criteriaVo)
    {
        if ($criteriaVo instanceof Atexo_Contrat_CriteriaVo) {
            if (self::isFromAcSad()) {
                if ($this->typeAttributionMono->checked && !$this->typeAttributionMulti->checked) {
                    $criteriaVo->setTypeAttribution(1);
                } elseif (!$this->typeAttributionMono->checked && $this->typeAttributionMulti->checked) {
                    $criteriaVo->setTypeAttribution(2);
                } else {
                    $criteriaVo->setTypeAttribution(null);
                }
            }

            if (!self::isFromAcSad()) {
                if ($this->typeContrat->getSelectedValue()) {
                    $criteriaVo->setTypes([$this->typeContrat->getSelectedValue()]);
                } else {
                    $criteriaVo->setTypes(null);
                }
            }
            $arrayStatus = [];
            if ($this->statutASaisir->checked) {
                $arrayStatus[] = Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR');
            }
            if ($this->statutANotifier->checked) {
                $arrayStatus[] = Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT');
            }
            if ($this->statutNotifier->checked) {
                $arrayStatus[] = Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
            }
            $criteriaVo->setStatus($arrayStatus);

            if ($this->statutFavori->checked) {
                $criteriaVo->setFavorite(true);
            } else {
                $criteriaVo->setFavorite(false);
            }
        }
    }

    public function onCheckFilter($sender, $param)
    {
        $criteriaVo = $this->chargerCriteresParDefaut();
        $this->getCriteriaTypeEtStatus($criteriaVo);
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->populateData($criteriaVo);
    }

    /**
     * Permet de retourner le montant d'un contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string le montant du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getMontantContrat($contrat)
    {
        $montantC = '';
        if ($contrat instanceof CommonTContratMulti || $contrat instanceof CommonTContratTitulaire) {
            $idTypeContrat = $contrat->getIdTypeContrat();
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat)) {
                if ($contrat instanceof CommonTContratMulti || !$contrat->hasChapeau()) {
                    $montantC = Prado::Localize('MAX').' '.Atexo_Util::getMontantArronditEspace($contrat->getMontantMaxEstime());
                } elseif ($contrat instanceof CommonTContratTitulaire && $contrat->hasChapeau()) {
                    $montantC = Prado::Localize('VOIR_CONTRAT_CHAPEAU');
                }
            } else {
                if ($contrat instanceof CommonTContratMulti) {
                    $montantC = Prado::Localize('VOIR_CHAQUE_CONTRAT');
                } elseif ($contrat instanceof CommonTContratTitulaire && (new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($idTypeContrat)) {
                    $montantC = Atexo_Util::getMontantArronditEspace($contrat->getMontantContrat());
                }
            }
        }

        return $montantC;
    }

    /**
     * Permet de retourner si le contrat est publié avec montant.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return bool
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getCssArticle133($contrat)
    {
        $css = '';
        if ($contrat instanceof CommonTContratTitulaire && empty($contrat->getDateNotification())) {
            $css = ' incomplet';
        }

        return $css;
    }

    /**
     * Permet de retourner si le contrat est avec publication article 133.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return bool
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function isPublicationArticle133($contrat)
    {
        $affiche = false;
        if (
            $contrat instanceof CommonTContratTitulaire &&
            ($contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') || $contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))
        ) {
            if (Atexo_Config::getParameter('AFFICHER_ARTICLE_133_CONTRAT_A_NOTIFIER') || $contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) {
                $idTypeContrat = $contrat->getIdTypeContrat();
                if ((new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($idTypeContrat)) {
                    $affiche = true;
                }
            }
        }

        return $affiche;
    }

    /**
     * Permet de retourner msg de publication du contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string message de Publication Avec Montant
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getMsgPublicationAvecMontant($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire && !empty($contrat->getDateNotification())) {
            $msg = Prado::localize('PUBLICATION_AVEC_MONTANT_RECHERCHE_CONTRAT');
        } else {
            $msg = Prado::localize('PUBLICATION_A_VALIDER');
        }

        return $msg;
    }

    /**
     * Permet de retourner msg de publication du contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string message de Publication sans Montant
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getMsgPublicationSansMontant($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire && !empty($contrat->getDateNotification())) {
            $msg = Prado::localize('PUBLICATION_SANS_MONTANT_RECHERCHE_CONTRAT');
        } else {
            $msg = Prado::localize('PUBLICATION_A_VALIDER');
        }

        return $msg;
    }

    /**
     * Permet de retourner msg de publication du contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string message de pas de Publication
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getMsgPasDePublication($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire && !empty($contrat->getDateNotification())) {
            $msg = Prado::localize('PAS_PUBLICATION_RECHERCHE_CONTRAT');
        } else {
            $msg = Prado::localize('PUBLICATION_A_VALIDER');
        }

        return $msg;
    }

    /**
     * Permet de retourner msg de publication du contrat.
     *
     * @param CommonTContratTitulaire $contrat l'objet contrat
     *
     * @return string message de pas de Publication
     *
     * @author Amal EL Bekkaoui <amal.elebakkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function updatePublicationContrat($sender, $param)
    {
        $currentItem = $sender->NamingContainer;
        $param = explode('#', $param->CallbackParameter);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $contratQuery->getTContratTitulaireById($param[0], $connexion);
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->hiddenIdContrat->setValue($contrat->getIdContratTitulaire());
            $this->hiddenIdItem->setValue($currentItem->btnArt133_contrat->getClientId());
            if (empty($contrat->getDateNotification()) || $contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
                $this->checkPublicationArticle133($contrat, $currentItem);
                $this->chargerDonneesContrat($contrat);
                $this->scriptJs->setText("<script>J('#".$currentItem->btnArt133_contrat->getClientId()."').buttonset( 'refresh');openModal('modal-art133','popup-small2',document.getElementById('".$this->titrePopin->getClientID()."'));</script>");
            } else {
                switch ($param[1]) {
                    case 'PubAvecMontant':
                        $contrat->setPublicationMontant('0');
                        $contrat->setPublicationContrat('0');
                        break;
                    case 'PubSansMontant':
                        $contrat->setPublicationMontant('1');
                        $contrat->setPublicationContrat('0');
                        break;
                    case 'PasDePub':
                        $contrat->setPublicationMontant('1');
                        $contrat->setPublicationContrat('1');
                        break;
                }
                $contrat->save($connexion);
            }
        }
    }

    public function checkPublicationArticle133($contrat, $element)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $statut1 = false;
            $statut2 = false;
            $statut3 = false;
            if ('0' == $contrat->getPublicationContrat() && '0' == $contrat->getPublicationMontant()) {
                $statut1 = true;
            } elseif ('0' == $contrat->getPublicationContrat() && '1' == $contrat->getPublicationMontant()) {
                $statut2 = true;
            } elseif ('1' == $contrat->getPublicationContrat()) {
                $statut3 = true;
            }
            $element->btnArt133_contrat_statut_1->setChecked($statut1);
            $element->btnArt133_contrat_statut_2->setChecked($statut2);
            $element->btnArt133_contrat_statut_3->setChecked($statut3);
        }
    }

    /**
     * Permet de retourner l'url de la popup de modification du numero du contrat.
     *
     * @param $idContrat l'id Contrat
     *
     * @return string l'url
     *
     * @author Fatima Ezzahra <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlPageNumerotation($idContrat)
    {
        $idContrat = base64_encode($idContrat);

        return "index.php?page=Agent.PopupNumerotationContrat&idContrat=$idContrat";
    }

    /**
     * Permet de remplir la liste de type de contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillTypeContrat()
    {
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat('TOUS_LES_TYPES_DE_CONTRAT');
        $this->typeContrat->DataSource = $listeContrats;
        $this->typeContrat->DataBind();
    }

    /**
     * Permet de faire la recherche et par la suite faire un render.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function searchActive($sender, $param)
    {
        self::search();
        $this->panelFullResult->render($param->getNewWriter());
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function dataForSearchResult($criteriaVo)
    {
        self::displayFilters($criteriaVo);
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Permet D'afficher les champs des filteres selon les cas.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function displayFilters($criteriaVo = null)
    {
        $this->advancedSearchContrat->setVisible(false);
        $this->resultSearch->setVisible(true);
        if ($criteriaVo instanceof Atexo_Contrat_CriteriaVo) {
            $typeContrat = $criteriaVo->getTypes();
            if (is_array($typeContrat) && count($typeContrat)) {
                $this->typeContrat->setSelectedValue($typeContrat[0]);
            }
            $this->filtreMotsCles->setText($criteriaVo->getMotsCles());
        }
        if (isset($_GET['searchAcSad'])) {
            $this->panelTypeContrat->setVisible(false);
            $this->panelTypeAttribution->setVisible(true);
            if (Atexo_Module::isEnabled('FiltreContratAcSad')) {
                $this->panelFiltreEntite->setVisible(true);
            } else {
                $this->panelFiltreEntite->setVisible(false);
            }
        } else {
            $this->panelTypeAttribution->setVisible(false);
            $this->panelTypeContrat->setVisible(true);
            $this->panelFiltreEntite->setVisible(false);
        }
    }

    /**
     * Peremt de retourner l'url d'accés au chapeau.
     *
     * @param $idContratMulti
     *
     * @return string l'url
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getUrlToContratMulti($idContratMulti)
    {
        return '?page=Agent.ResultatRechercheContrat&contratMulti='.$idContratMulti.(isset($_GET['searchAcSad']) ? '&searchAcSad' : '');
    }

    public function getUrlNotifierContrat($contrat)
    {
        $url = '#';
        if ($contrat instanceof CommonTContratTitulaire) {
            $url = "javascript:popUp('?page=Agent.PopupNotificationContrat&idContrat=".base64_encode($contrat->getIdContratTitulaire())."','yes');";
        }

        return $url;
    }

    /**
     * Remplit la liste des Justification.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillListeJustif()
    {
        $data = (new Atexo_CommonReferentiel())->fillListeReferentielKeyLibelle2(Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION'), Prado::localize('TEXT_SELECTIONNER').' ...');
        if (is_array($data)) {
            $this->justification->DataSource = $data;
            $this->justification->DataBind();
        }
    }

    /*
       * Remplit la liste des Justification marche non publie
       * @return void
       * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
       * @version 1.0
       * @since 2015-roadmap
       * @copyright Atexo 2015
       */
    public function fillListeJustifMarcheNonPublie()
    {
        $data = (new Atexo_CommonReferentiel())->fillListeReferentielKeyLibelle2(Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION_MARCHE_NON_PUBLIE'), Prado::localize('TEXT_SELECTIONNER').' ...');
        if (is_array($data)) {
            $this->justificationNonPublication->DataSource = $data;
            $this->justificationNonPublication->DataBind();
        }
    }

    /*
     * Remplit les informations du contrat dans la popin
     *
     * @param CommonTContratTitulaire $contrat
     * @return void
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     * @version 1.0
     * @since 2015-roadmap
     * @copyright Atexo 2015
     */
    public function chargerDonneesContrat($contrat)
    {
        $this->dateDefinitiveNotification->text = '';
        $this->justification->setSelectedValue(0);
        $this->justificationNonPublication->setSelectedValue(0);
        if ($contrat instanceof CommonTContratTitulaire) {
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($contrat->getIdTypeContrat())) {
                $this->MontantMarche->Text = Atexo_Util::getMontantArronditEspace($contrat->getMontantContrat());
                if (Atexo_Module::isEnabled('DecisionTrancheBudgetaire') && $contrat->getIdTrancheBudgetaire()) {
                    $this->trancheBudgetaire->Text = (new Atexo_TrancheBudgetaire())->getLibelleTrancheById($contrat->getIdTrancheBudgetaire(), $contrat->getOrganisme());
                }
                $this->panelMontantContrat->setDisplay('Dynamic');
            } else {
                $this->panelMontantContrat->setDisplay('None');
            }

            if ($contrat->getPublicationMontant() && $contrat->getPublicationContrat()) {
                $this->art133Publie->setChecked(false);
                $this->art133PublieSansMontant->setChecked(false);
                $this->art133NonPublie->setChecked(true);
                $this->panel_NonPublie->setStyle('display:block;');
                if ($contrat->getIdMotifNonPublicationMontant()) {
                    $valeurRef = CommonValeurReferentielPeer::retrieveByPK($contrat->getIdMotifNonPublicationMontant(), Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION_MARCHE_NON_PUBLIE'));
                    if ($valeurRef instanceof CommonValeurReferentiel) {
                        $this->justificationNonPublication->setSelectedValue($valeurRef->getLibelle2());
                    }
                }
                $this->panel_PublieSansMontant->setStyle('display:none;');
            } elseif ($contrat->getPublicationMontant() && 0 === $contrat->getPublicationContrat()) {
                $this->art133Publie->setChecked(false);
                $this->art133PublieSansMontant->setChecked(true);
                $this->art133NonPublie->setChecked(false);
                $this->panel_PublieSansMontant->setStyle('display:block;');
                $this->panel_NonPublie->setStyle('display:none;');
                if ($contrat->getIdMotifNonPublicationMontant()) {
                    $valeurRef = CommonValeurReferentielPeer::retrieveByPK($contrat->getIdMotifNonPublicationMontant(), Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION'));
                    if ($valeurRef instanceof CommonValeurReferentiel) {
                        $this->justification->setSelectedValue($valeurRef->getLibelle2());
                        if ('AUTRE' == $valeurRef->getLibelle2()) {
                            $this->panel_precision_montantNonPublie->setStyle('display:block;');
                        }
                    }
                }
                $this->precisionMontantNonPublie->Text = $contrat->getDescMotifNonPublicationMontant();
            } elseif (0 === $contrat->getPublicationMontant() && 0 === $contrat->getPublicationContrat()) {
                $this->art133Publie->setChecked(true);
                $this->art133PublieSansMontant->setChecked(false);
                $this->art133NonPublie->setChecked(false);
                $this->panel_NonPublie->setStyle('display:none;');
                $this->panel_PublieSansMontant->setStyle('display:none;');
            }
        }
    }

    public function saveContrat($sender, $param)
    {
        $idContrat = $this->hiddenIdContrat->getValue();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat, $connexion);
        if ($contrat instanceof CommonTContratTitulaire) {
            $contrat->setDateNotification(Atexo_Util::frnDate2iso($this->dateDefinitiveNotification->Text));
            $gestionContrat = new Atexo_Contrat_Gestion();
            $gestionContrat->changeTrancheBudgetaire($contrat);
            if ((new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($contrat->getIdTypeContrat())) {
                $contrat->setIdMotifNonPublicationMontant(null);
                $contrat->setDescMotifNonPublicationMontant(null);
                if ($this->art133NonPublie->getChecked()) {
                    $contrat->setPublicationMontant('1');
                    $contrat->setPublicationContrat('1');
                    if ($this->justificationNonPublication->getSelectedValue()) {
                        $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION_MARCHE_NON_PUBLIE'), $this->justificationNonPublication->getSelectedValue());
                        if ($valeurRef instanceof CommonValeurReferentiel) {
                            $contrat->setIdMotifNonPublicationMontant($valeurRef->getId());
                        }
                    }
                } elseif ($this->art133PublieSansMontant->getChecked()) {
                    $contrat->setPublicationMontant('1');
                    $contrat->setPublicationContrat('0');
                    if ($this->justification->selectedValue) {
                        $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_JUSTIFICATION'), $this->justification->getSelectedValue());
                        if ($valeurRef instanceof CommonValeurReferentiel) {
                            $contrat->setIdMotifNonPublicationMontant($valeurRef->getId());
                        }
                        $contrat->setDescMotifNonPublicationMontant($this->precisionMontantNonPublie->getText());
                    }
                } elseif ($this->art133Publie->getChecked()) {
                    $contrat->setPublicationMontant('0');
                    $contrat->setPublicationContrat('0');
                }
            }
            $contrat->setStatutContrat(Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
            $contrat->save($connexion);
            $criteriaVo = $this->chargerCriteresParDefaut();
            if ($criteriaVo instanceof Atexo_Contrat_CriteriaVo) {
                $this->populateData($criteriaVo);
            }
            $this->scriptJs->setText("<script>J('.modal-art133').dialog('close');</script>");
        }
    }

    /**
     * Permet d'executer une action sur le contrat choisi.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function actionSupprimerContrat($sender, $param)
    {
        $listeParams = explode('#', $param->CallbackParameter);
        $idContrat = $listeParams[0];
        $organisme = $listeParams[1];
        if ($idContrat) {
            $this->idContratToDelete->value = $idContrat;
            $this->organismeContrat->value = $organisme;
            $cas = Atexo_Consultation_Contrat::getCasSuppressionContrat($idContrat, $organisme);
            $this->messageConfirmationSuppressionContrat->Text = Atexo_Consultation_Contrat::getMessageConfirmationSuppressionContrat($cas, $idContrat, $organisme);
            $this->gererAffichageBoutonValidationSuppressionContrat($cas);
            $script = "<script>openModal('demander-suppression','popup-small2',document.getElementById('".$this->titrePopin2->getClientId()."'));</script>";
            $this->scriptJs->Text = $script;
        }
    }

    /**
     * Permet de supprimer un contrat.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function supprimerAttributaire($sender, $param)
    {
        (new Atexo_Consultation_Contrat())->deleteContrat($this->idContratToDelete->value);
        $this->scriptJs->Text = "<script>J('.demander-suppression').dialog('destroy');</script>";

        //TODO : reprendre les criteres selectionnes
        $criteriaVo = $this->chargerCriteresParDefaut();
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->populateData($criteriaVo);

        $this->scriptJs->Text = '<script>window.location.replace(window.location.href);</script>';
    }

    /**
     * Permet d'afficher ou non le bouton d'accés aux échanges CHORUS.
     *
     * @param CommonTcontratTitulaire $data
     *
     * @return bool
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since palce-2015
     *
     * @copyright Atexo 2016
     */
    public function hasDroitAccesEchangeChorus($data)
    {
        return !$data->isContratChapeau()
            && ($data->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') || $data->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))
            && (new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getCurrentServiceId()) && $data->getModeEchangeChorus()
            && ($data->getServiceId() == Atexo_CurrentUser::getCurrentServiceId()
                || is_null(Atexo_CurrentUser::getCurrentServiceId()))
            && $data->getOrganisme() == Atexo_CurrentUser::getOrganismAcronym()
        ;
    }

    /**
     * retourne l'url d'accés aux échanges CHORUS.
     *
     * @param CommonTcontratTitulaire $data
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since palce-2015
     *
     * @copyright Atexo 2016
     */
    public function getUrlAccesEchange($data)
    {
        return '?page=Agent.ChorusEchanges&idContrat='.base64_encode($data->getIdContratTitulaire());
    }

    /*
     * Recharger le tableau
     *
     * @return void
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function refreshSearch()
    {
        $criteriaVo = $this->chargerCriteresParDefaut();
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->populateData($criteriaVo);
    }

    /**
     * Permet de gerer l'affichage des boutons de suppression du contrat et d'envoi de mail de suppression de l'EJ.
     *
     * @param $cas
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function gererAffichageBoutonValidationSuppressionContrat($cas)
    {
        switch ($cas) {
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE:
                $this->supprimerAttributaire->visible = true;
                $this->envoiMailAgent->visible = false;
                break;
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL:
                $this->supprimerAttributaire->visible = false;
                $this->envoiMailAgent->visible = true;
                break;
            default:
                $this->supprimerAttributaire->visible = false;
                $this->envoiMailAgent->visible = false;
                break;
        }
    }

    /**
     * Permet d'envoyer le mail de suppression de l'EJ (suite a la demande de suppression du contrat) a l'agent.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function envoiMailSuppressionEj($sender, $param)
    {
        Atexo_Consultation_Contrat::envoiMailSuppressionEj($this->idContratToDelete->value, $this->organismeContrat->value);
        $this->scriptJs->Text = "<script>J('.demander-suppression').dialog('destroy');</script>";
    }

    /**
     * Permet de generer et telecharger excel de liste des contrats.
     *
     * @param $sender
     * @param $param
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function downloadExcel($sender, $param)
    {
        if ($criteriaVo = $this->getCriteraVo()) {
            $criteriaVo->setOffset(null);
            $criteriaVo->setLimit(null);
        }
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $listeContrats = (array) $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo);
        Atexo_GenerationExcel::genererExcelContrats($listeContrats);
    }

    /**
     * @param $contrat
     *
     * @return bool
     */
    public function getStatutDonneesAPublier($contrat)
    {
        return 0 === $contrat->getPublicationContrat();
    }

    /**
     * @param $contrat
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     */
    public function getIconeStatutDonneesEssentiellesPubliees($contrat)
    {
        $enAttenteSn = Atexo_Config::getParameter('STATUT_EN_ATTENTE_SN');
        $publieSn = Atexo_Config::getParameter('STATUT_PUBLIE_SN');

        $retourOui = 'oui';
        $retourNon = 'non';
        $retourEnAttente = 'en_attente';

        if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
            if ($contrat->getStatutPublicationSn() == $enAttenteSn) {
                $statut = $retourEnAttente;
            } elseif ($contrat->getstatutPublicationSn() == $publieSn) {
                $statut = $retourOui;
            } else {
                $statut = $retourNon;
            }
        } else {
            $statut = $retourOui;
        }

        return $statut;
    }

    /**
     * @param $data
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     */
    public function hasDroitModificationWithDE($data)
    {
        return (is_null($data)) ? false : ($data->isCurrectUserPermanentAgent() && $data->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
    }

    /**
     *  Retourne le label de l'organisme ou du service.
     *
     * @param $organisme
     * @param $idService
     *
     * @return string
     */
    public function getLabelOrganismeService($organisme, $idService)
    {
        return (new Atexo_Consultation())->getOrganismeDenominationByService($organisme, $idService, true);
    }

    /**
     * @param $contrat
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     */
    public function displayDonneeEssentielle($contrat)
    {
        return (0 === $contrat->getPublicationContrat())
            && ($contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))
            && ($contrat->getStatutPublicationSn() != Atexo_Config::getParameter('STATUT_JAMAIS_PUBLIE_SN'));
    }

    /**
     * @param CommonTContratTitulaire $contrat
     * @return bool
     */
    public function hasDroitSuiviMessage(CommonTContratTitulaire $contrat): bool
    {
        $consLotContratQuery = new CommonTConsLotContratQuery();
        $consLotContrat = $consLotContratQuery->findOneByArray(
            [
                'idContratTitulaire' => $contrat->getIdContratTitulaire(),
            ]
        );
        if ($consLotContrat) {
            $commonConsultationQuery = new CommonConsultationQuery();
            $consultation = $commonConsultationQuery->findOneByArray(
                [
                    'id' => $consLotContrat->getConsultationId(),
                ]
            );
            if ($consultation) {
                $messecVersion = $consultation->getVersionMessagerie();
                if ($messecVersion === 2) {
                    return true;
                }
            }
        }
        return false;
    }

    public function isButtonNotifieMessageDisplayed(CommonTContratTitulaire $contrat): bool
    {
        if ($contrat->getStatutContrat() === 4) {
            return false;
        }
        return true;
    }

    /**
     * @param CommonTContratTitulaire $contrat
     * @return string
     */
    public function setSuiviEchangesUrl(CommonTContratTitulaire $contrat): string
    {
        $consLotContratQuery = new CommonTConsLotContratQuery();
        $consLotContrat = $consLotContratQuery->findOneByArray(
            [
                'idContratTitulaire' => $contrat->getIdContratTitulaire(),
            ]
        );
        if ($consLotContrat) {
            $commonConsultationQuery = new CommonConsultationQuery();
            $consultation = $commonConsultationQuery->findOneByArray(
                [
                    'id' => $consLotContrat->getConsultationId(),
                ]
            );

            if ($consultation) {
                $id = $consultation->getId();
                $messecVersion = $consultation->getVersionMessagerie();
                if ($messecVersion === 2) {
                    $encrypte = Atexo_Util::getSfService(Encryption::class);
                    $idCrypte = $encrypte->cryptId($id);
                    $pfUrl = Atexo_MultiDomaine::replaceDomain(
                        Atexo_Config::getParameter('PF_URL_MESSAGERIE_SUIVI_MESSAGE')
                    );
                    $url = $pfUrl
                        . '/'
                        . $idCrypte;
                    return $url;
                }
            }
        }

        return '';
    }

    /**
     * @param CommonTContratTitulaire $contrat
     * @return string
     */
    public function setNotificationsUrl(CommonTContratTitulaire $contrat): string
    {
        $consLotContratQuery = new CommonTConsLotContratQuery();
        $consLotContrat = $consLotContratQuery->findOneByArray(
            [
                'idContratTitulaire' => $contrat->getIdContratTitulaire(),
            ]
        );
        if ($consLotContrat) {
            $commonConsultationQuery = new CommonConsultationQuery();
            $consultation = $commonConsultationQuery->findOneByArray(
                [
                    'id' => $consLotContrat->getConsultationId(),
                ]
            );

            if ($consultation) {
                $id = $consultation->getId();
                $messecVersion = $consultation->getVersionMessagerie();
                if ($messecVersion === 2) {
                    $encrypte = Atexo_Util::getSfService(Encryption::class);
                    $idCrypte = $encrypte->cryptId($id);
                    $idContratCrypte = $encrypte->cryptId($contrat->getIdContratTitulaire());
                    $pfUrl = Atexo_MultiDomaine::replaceDomain(
                        Atexo_Config::getParameter('PF_URL_MESSAGERIE_COURRIER_NOTIFICATION_CONTRAT')
                    );

                    $url = $pfUrl
                        . '/'
                        . $idCrypte
                        . '/'
                        . $idContratCrypte;

                    return $url;
                }
            }
        }

        return '';
    }

    public function getUrlAccesExec($data)
    {
        $sso = (new Atexo_Agent())->getSsoForSocle(
            Atexo_CurrentUser::getId(),
            Atexo_CurrentUser::getOrganismAcronym(),
            Atexo_Config::getParameter("SERVICE_METIER_EXEC")
        );

        return Atexo_Config::getParameter("URL_EXEC")
            . '/#/contrats/visualiser/'
            . $data->getIdContratTitulaire()
            . '?sso='
            . $sso;
    }

    /**
     * @param Atexo_Contrat_CriteriaVo $criteriaVo
     */
    public function setCriteraVo(Atexo_Contrat_CriteriaVo $criteriaVo): void
    {
        Atexo_CurrentUser::writeToSession($this->getKeyCache(), serialize($criteriaVo));
    }

    /**
     * @return Atexo_Contrat_CriteriaVo|false|mixed
     */
    public function getCriteraVo()
    {
        return unserialize(Atexo_CurrentUser::readFromSessionSf($this->getKeyCache()));
    }

    public function getKeyCache(): string
    {
        return  base64_encode(
            Atexo_CurrentUser::getId()
            . '-'
            . Atexo_CurrentUser::getOrganismAcronym()
            . '-CriteriaVoContrat'
        );
    }

    public function getUrlAccesSpaserContrat(CommonTContratTitulaire $data): string
    {
        return Atexo_Util::getSfService(RouteBuilderInterface::class)->getRoute(
            'agent_spaser_objet_metier_contrat',
            [
                "id" => $data->getIdContratTitulaire(),
            ]
        );
    }

    public function isSpaserButtonAccessGranted(): bool
    {
        $spaserService = Atexo_Util::getSfService(Spaser::class);

        return $spaserService->hasConfigurationOrganisme("acces_module_spaser")
            && $spaserService->hasHabilitationAgent('gestion_spaser_consultations', 'is');
    }

    /**
     * @throws \Exception
     */
    public function getSaisieIndicateurProgressStatus(array|PropelCollection $listeContrats): array
    {
        $contratsIds = [];

        /** @var CommonTContratTitulaire $contrat */
        foreach ($listeContrats as $contrat) {
            $contratsIds[] = [
                "externalId" => $contrat->getIdContratTitulaire(),
                "dateFin" => $contrat->getDateAttributionYear()
            ];
        }

        /** @var IndicateurProgress $indicateurProgress */
        $indicateurProgress = Atexo_Util::getSfService(IndicateurProgress::class);

        return $indicateurProgress->getIndicateurProgression(
            SpaserUtils::OBJET_METIER_TYPE_CONTRAT,
            $contratsIds,
        );
    }

    private function getFormatedValues(string $key): ?string
    {
        return isset($_GET[$key]) ? Atexo_Util::atexoHtmlEntities($_GET[$key]) : null;
    }
}
