<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonRG;
use Application\Service\Atexo\Atexo_ConsultationRma;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;

/**
 * permet de telecharger le RG a partir du tableau du bord RMA.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since place-2015
 *
 * @copyright Atexo 2016
 */
class DownloadReglementRma extends DownloadFile
{
    private $_reference;
    private $_organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $this->_organisme = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['org']));
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($this->_reference);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService(false);
        $atexoConsultation = new Atexo_ConsultationRma();
        $consultationFound = $atexoConsultation->search($consultationCriteria);
        $rg = '';
        if (is_array($consultationFound) && count($consultationFound) > 0) {
            $rg = (new Atexo_Consultation_Rg())->getReglement($this->_reference, $this->_organisme);
            if ($rg instanceof CommonRG) {
                $this->_idFichier = $rg->getRg();
                $this->_nomFichier = $rg->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
            }
        } else {
            $this->response->redirect('agent');
        }
    }
}
