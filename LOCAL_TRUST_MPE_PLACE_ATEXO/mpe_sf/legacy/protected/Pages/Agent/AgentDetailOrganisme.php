<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe AgentDetailOrganisme.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentDetailOrganisme extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->DetailOrganisme->setCodeAchteur(Atexo_Module::isEnabled('AfficherCodeService'));
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $this->DetailOrganisme->_org = $organisme;
        $this->IDTRepeaterRPA->_organisme = $organisme;
        $this->IDTRepeaterRPA->_ID = 0;
        $this->IDTRepeaterRPA->detailRPA(0, $organisme);
        $this->idretour->NavigateUrl = '?page=Agent.GestionEntitesAchat&org='.$organisme;
    }
}
