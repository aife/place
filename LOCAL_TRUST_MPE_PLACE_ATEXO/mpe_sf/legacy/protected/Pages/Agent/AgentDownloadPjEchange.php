<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Permet de télécharger le Pj.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentDownloadPjEchange extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultationId = false;
        $idPj = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $pj = Atexo_Message::getPjByIdPiece($idPj, $org);

        if ($pj) {
            $echange = Atexo_Message::retrieveMessageById($pj->getIdMessage(), $org);
            if ($echange) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($echange->getConsultationId());
                if ($consultation) {
                    $consultationId = $consultation->getId();
                }
            }
        }

        if ($consultationId) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdReference($consultationId);
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setAcronymeOrganisme($org);
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());

            $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);

            if (1 == count($arrayConsultation)) {
                $this->_idFichier = $idPj;
                $this->_nomFichier = $pj->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
            } else {
                exit;
            }
        }
    }
}
