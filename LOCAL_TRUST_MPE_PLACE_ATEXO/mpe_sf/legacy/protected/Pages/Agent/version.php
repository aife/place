<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class version extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $lang = Atexo_Languages::readLanguageFromSession();
        $xml = simplexml_load_file(Atexo_Config::getParameter('URL_XML_VERSION').'version_'.$lang.'.xml');
        $dataSource = [];

        $pathFile = 'protected/pages/agent/CVS/Entries';
        $this->versionCourante->text = Atexo_Util::getVersionMPE($pathFile);
        $this->dateMAJ->text = Atexo_Util::getMajMPE($pathFile);

        foreach ($xml->version as $version) {
            $arrayFonctionalite = (array) $version->nouvelleFonctionalite;
            $arrayCorrection = (array) $version->principalesCorrection;

            if (is_array($arrayFonctionalite['fonctionnalite'])) {
                $arrayFonctionalite['fonctionnalite'] = array_map('utf8_decode', $arrayFonctionalite['fonctionnalite']);
            } else {
                $arrayFonctionalite['fonctionnalite'] = array_map('utf8_decode', [0 => $arrayFonctionalite['fonctionnalite']]);
            }

            if (is_array($arrayCorrection['correction'])) {
                $arrayCorrection['correction'] = array_map('utf8_decode', $arrayCorrection['correction']);
            } else {
                $arrayCorrection['correction'] = array_map('utf8_decode', [0 => $arrayCorrection['correction']]);
            }
            $dataSource[] = ['numVersion' => (string) $version->numeroVersion, 'fonctionnalite' => $arrayFonctionalite['fonctionnalite'], 'correction' => $arrayCorrection['correction']];
        }
        $this->versionsGlobale->datasource = $dataSource;
        $this->versionsGlobale->dataBind();
    }
}
