<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonSousCategorie;
use Application\Propel\Mpe\CommonSousCategoriePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Exception;

/**
 * Page de gestion des nomenclatures.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionNomenclature extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin') || !(Atexo_Module::isEnabled('ConsultationDomainesActivites'))) {
            $this->response->redirect('agent');
        }
        if (!$this->IsPostBack) {
            $this->panelMessageErreur->setVisible(false);
            $this->displayCategorie();
        } else {
            $this->categorieRechercher->setVisible(true);
            $this->allCategorie->setVisible(true);
            $this->repeaterResultSearchCategorie->DataSource = $this->getViewState('dataSourceResultSearchCategorie');
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = $this->getViewState('dataSourceAllCategorie');
            $this->repeaterCategorie->DataBind();
        }
    }

    public function suggestNames($sender, $param)
    {
        // Get the token
        $token = $param->getToken();
        $data = [];
        if ($this->affichageValeurDesactivee_non->Checked) {
            $active = 1;
        } else {
            $active = 3;
        }
        $data = (new Atexo_Consultation_Category())->retrieveDomaineActiviteByLibelleOrCode($token, $active);
        $sender->DataSource = $data;
        $sender->dataBind();
        $this->pageLoader->Style = 'display:none';
        $this->pageLoader->render($param->getNewWriter());
    }

    public function suggestionSelected($sender, $param)
    {
        $this->pageLoader->Style = 'display:none';
        $this->pageLoader->render($param->getNewWriter());
        $id = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->panelMessageErreur->setVisible(false);
        if ($id) {
            $this->categorieRechercher->setVisible(true);
            $this->allCategorie->setVisible(false);
            $data = (new Atexo_Consultation_Category())->retrieveListeSousCategorie([$id]);

            $this->repeaterResultSearchCategorie->DataSource = $data;
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = [];
            $this->repeaterCategorie->DataBind();
            $this->setViewState('dataSourceAllCategorie', []);
            $this->setViewState('dataSourceResultSearchCategorie', $data);
        }
        $this->panelErreur->render($param->getNewWriter());
        $this->panelPrincipal->render($param->getNewWriter());
    }

    public function displayCategorie()
    {
        $listeCat = Atexo_Consultation_Category::retrieveCategories(true, true);
        $this->repeaterCategorie->DataSource = $listeCat;
        $this->repeaterCategorie->DataBind();
        $this->setViewState('dataSourceAllCategorie', $listeCat);
        $this->setViewState('dataSourceResultSearchCategorie', []);
    }

    public function getLibelle($value)
    {
        $libelle = ' - ';
        if ($value) {
            $libelle = $value;
        }

        return $libelle;
    }

    public function deleteSousCategorie($idSousCategorie)
    {
        if (self::verifyDeleteCategorie($idSousCategorie)) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $ObjetCat = CommonSousCategoriePeer::retrieveByPK($idSousCategorie, $connexionCom);
            if ($ObjetCat instanceof CommonSousCategorie) {
                $objetsFils = (new Atexo_Consultation_Category())->getCategorieFils($idSousCategorie, false, 0);
                if (!$objetsFils) {
                    $consultationsByCat = (new Atexo_Consultation())->getConsultationByIdDomaineActivite($idSousCategorie);
                    if (!$consultationsByCat) {
                        $entreprisesByCat = (new Atexo_Entreprise())->getEntrepriseByIdDomaineActivite($idSousCategorie);
                        if (!$entreprisesByCat) {
                            //supprimer l'objet
                            CommonSousCategoriePeer::doDelete($idSousCategorie, $connexionCom);
                        } else {
                            $ObjetCat->setActive(0);
                            $ObjetCat->save($connexionCom);
                        }
                    } else {
                        //d?sactiver la sous cat?gorie
                        $ObjetCat->setActive(0);
                        $ObjetCat->save($connexionCom);
                    }
                } else {
                    //d?sactiver la sous cat?gorie
                    $ObjetCat->setActive(0);
                    $ObjetCat->save($connexionCom);
                }
            }
            Atexo_CurrentUser::deleteFromSession('cachedSousCategories');

            return true;
        }

        return false;
    }

    public function deleteSousCategorieSearch($sender, $param)
    {
        $idSousCategorie = $sender->TemplateControl->idSousCategorie->value;
        if (self::deleteSousCategorie($idSousCategorie)) {
            $this->categorieRechercher->setVisible(true);
            $this->allCategorie->setVisible(false);
            $data = (new Atexo_Consultation_Category())->retrieveListeSousCategorie([$idSousCategorie]);

            $this->repeaterResultSearchCategorie->DataSource = $data;
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = [];
            $this->repeaterCategorie->DataBind();
            $this->setViewState('dataSourceAllCategorie', []);
            $this->setViewState('dataSourceResultSearchCategorie', $data);

            $this->repeaterResultSearchCategorie->DataSource = $this->getViewState('dataSourceResultSearchCategorie');
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = $this->getViewState('dataSourceAllCategorie');
            $this->repeaterCategorie->DataBind();
            $this->panelPrincipal->render($param->getNewWriter());
        }
        $this->panelErreur->render($param->getNewWriter());
    }

    public function deleteSousCategorieFromAll($sender, $param)
    {
        $idSousCategorie = $sender->TemplateControl->idSousCategorie->value;

        if (self::deleteSousCategorie($idSousCategorie)) {
            $this->repeaterResultSearchCategorie->DataSource = [];
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = $this->getViewState('dataSourceAllCategorie');
            $this->repeaterCategorie->DataBind();
            $this->panelPrincipal->render($param->getNewWriter());
        }
        $this->panelErreur->render($param->getNewWriter());
    }

    public function restaurerSousCategorieSearch($sender, $param)
    {
        $idSousCategorie = $sender->TemplateControl->idSousCategorie->value;
        if (self::restaurerSousCategorie($idSousCategorie)) {
            $this->categorieRechercher->setVisible(true);
            $this->allCategorie->setVisible(false);
            $data = (new Atexo_Consultation_Category())->retrieveListeSousCategorie([$idSousCategorie]);

            $this->repeaterResultSearchCategorie->DataSource = $data;
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = [];
            $this->repeaterCategorie->DataBind();
            $this->setViewState('dataSourceAllCategorie', []);
            $this->setViewState('dataSourceResultSearchCategorie', $data);

            $this->repeaterResultSearchCategorie->DataSource = $this->getViewState('dataSourceResultSearchCategorie');
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = $this->getViewState('dataSourceAllCategorie');
            $this->repeaterCategorie->DataBind();
            $this->panelPrincipal->render($param->getNewWriter());
        }
        $this->panelErreur->render($param->getNewWriter());
    }

    public function restaurerSousCategorieFromAll($sender, $param)
    {
        $idSousCategorie = $sender->TemplateControl->idSousCategorie->value;
        if (self::restaurerSousCategorie($idSousCategorie)) {
            $this->repeaterResultSearchCategorie->DataSource = [];
            $this->repeaterResultSearchCategorie->DataBind();
            $this->repeaterCategorie->DataSource = $this->getViewState('dataSourceAllCategorie');
            $this->repeaterCategorie->DataBind();
            $this->panelPrincipal->render($param->getNewWriter());
        }
        $this->panelErreur->render($param->getNewWriter());
    }

    public function restaurerSousCategorie($idSousCategorie)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $ObjetCat = CommonSousCategoriePeer::retrieveByPK($idSousCategorie, $connexionCom);

            $elementsId = explode('.', $idSousCategorie);
            if (3 == count($elementsId)) {
                $idParent = $elementsId[0].'.'.$elementsId[1];
                $parentCat = CommonSousCategoriePeer::retrieveByPK($idParent, $connexionCom);
                if (($parentCat instanceof CommonSousCategorie) && $parentCat->getActive()) {
                    $ObjetCat->setActive(1);
                    $ObjetCat->save($connexionCom);
                } else {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(prado::localize('ALERTE_RESTAURATION_NOMENCLATURE_IMPOSSIBLE'));

                    return false;
                }
            } elseif (2 == count($elementsId)) {
                $ObjetCat->setActive(1);
                $ObjetCat->save($connexionCom);
            }
        } catch (Exception) {
            return false;
        }

        Atexo_CurrentUser::deleteFromSession('cachedSousCategories');

        return true;
    }

    /**
     * Permet de verifier si la cat?gorie a des fils active.
     */
    public function verifyDeleteCategorie($idCat)
    {
        $active = 1;
        $objetsFils = (new Atexo_Consultation_Category())->getCategorieFils($idCat, false, $active);
        if ($objetsFils) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(prado::localize('ALERTE_SUPPRESSION_NOMENCLATURE_IMPOSSIBLE'));

            return false;
        }
        $this->panelMessageErreur->setVisible(false);

        return true;
    }

    public function isNiveau2($id)
    {
        $nb = Atexo_Consultation_Category::countSeparatorInCategorie($id, '.');
        if (1 == $nb) {
            return true;
        }

        return false;
    }

    public function displayThisNomenclature($active)
    {
        if (($active && $this->affichageValeurDesactivee_non->Checked) || $this->affichageValeurDesactivee_oui->Checked) {
            return true;
        }

        return false;
    }
}
