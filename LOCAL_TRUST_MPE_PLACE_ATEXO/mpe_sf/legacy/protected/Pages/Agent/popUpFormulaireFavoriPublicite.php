<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Khalid BENAMAR <kbe@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since 2016-les-echos
 */
class popUpFormulaireFavoriPublicite extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getOrganismAcronym(), true);
            if (in_array(Atexo_Util::atexoHtmlEntities($_GET['idService']), $listOfServicesAllowed)) {
                if (isset($_GET['id'])) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $favorisPub = CommonTPubliciteFavorisPeer::retrieveByPk(Atexo_Util::atexoHtmlEntities($_GET['id']), $connexion);
                    if (($favorisPub instanceof CommonTPubliciteFavoris) && ($favorisPub->getServiceId() == Atexo_Util::atexoHtmlEntities($_GET['idService']))) {
                        self::displayFavori($favorisPub);
                    } else {
                        $this->blocFavori->Visible = false;
                        $this->messageErreur->setMessage(Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
                        $this->messageErreur->Visible = true;
                    }
                }
            } else {
                $this->blocFavori->Visible = false;
                $this->messageErreur->setMessage(Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
                $this->messageErreur->Visible = true;
            }
        }
    }

    public function saveFavori()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (isset($_GET['id'])) {
            $favorisPub = CommonTPubliciteFavorisPeer::retrieveByPk(Atexo_Util::atexoHtmlEntities($_GET['id']), $connexion);
        } else {
            $favorisPub = new CommonTPubliciteFavoris();
            $favorisPub->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $serviceId = Atexo_Util::atexoHtmlEntities($_GET['idService']);
            $favorisPub->setServiceId(empty($serviceId) ? null : $serviceId);
        }
        $favorisPub->setLibelle($this->nomFavori->SafeText);
        $favorisPub->setAcheteurCorrespondant($this->correspondant->SafeText);
        $favorisPub->setAcheteurNomOrganisme($this->nomOrganisme1->SafeText);
        $favorisPub->setAcheteurAdresse($this->adresseVoie1->SafeText);
        $favorisPub->setAcheteurCp($this->adresseCP1->SafeText);
        $favorisPub->setAcheteurVille($this->adresseVille1->SafeText);
        $favorisPub->setAcheteurTelephone($this->telephone1->SafeText);
        $favorisPub->setAcheteurEmail($this->email1->SafeText);

        $favorisPub->setAcheteurUrl($this->adresseURL1->SafeText);

        $favorisPub->setFactureDenomination($this->nomOrganisme3->SafeText);
        $favorisPub->setFactureAdresse($this->adresseVoie3->SafeText);
        $favorisPub->setFactureCp($this->adresseCP3->SafeText);
        $favorisPub->setFactureVille($this->adresseVille3->SafeText);

        $favorisPub->setInstanceRecoursOrganisme($this->nomOrganisme2->SafeText);
        $favorisPub->setInstanceRecoursAdresse($this->adresseVoie2->SafeText);
        $favorisPub->setInstanceRecoursCp($this->adresseCP2->SafeText);
        $favorisPub->setInstanceRecoursVille($this->adresseVille2->SafeText);
        $favorisPub->setInstanceRecoursUrl($this->adresseURL2->SafeText);
        $favorisPub->setRegimeFinancierCautionnement($this->cautionnementGaranties->SafeText);
        $favorisPub->setRegimeFinancierModalitesFinancement($this->modalitesFinancement->SafeText);
        //print_r($favorisPub); exit;

        $favorisPub->save($connexion);
        $connexion->beginTransaction();
        try {
            $favorisPub->save($connexion);
            $connexion->commit();
        } catch (\Exception) {
            $connexion->rollback();
        }

        $script = "<script>if(opener.document.getElementById('ctl0_CONTENU_PAGE_validateFavori')) opener.document.getElementById('ctl0_CONTENU_PAGE_validateFavori').click();";
        $script .= 'window.close();</script>';

        $this->scriptAddAndClose->Text = $script;
    }


    public function displayFavori(CommonTPubliciteFavoris $favorisPub) : void
    {
        $this->nomFavori->Text = $favorisPub->getLibelle();
        $this->correspondant->Text = $favorisPub->getAcheteurCorrespondant();
        $this->nomOrganisme1->Text = $favorisPub->getAcheteurNomOrganisme();
        $this->adresseVoie1->Text = $favorisPub->getAcheteurAdresse();
        $this->adresseCP1->Text = $favorisPub->getAcheteurCp();
        $this->adresseVille1->Text = $favorisPub->getAcheteurVille();
        $this->telephone1->Text = $favorisPub->getAcheteurTelephone();
        $this->email1->Text = $favorisPub->getAcheteurEmail();
        $this->adresseURL1->Text = $favorisPub->getAcheteurUrl();

        $this->nomOrganisme3->Text = $favorisPub->getFactureDenomination();
        $this->adresseVoie3->Text = $favorisPub->getFactureAdresse();
        $this->adresseCP3->Text = $favorisPub->getFactureCp();
        $this->adresseVille3->Text = $favorisPub->getFactureVille();

        $this->nomOrganisme2->Text = $favorisPub->getInstanceRecoursOrganisme();
        $this->adresseVoie2->Text = $favorisPub->getInstanceRecoursAdresse();
        $this->adresseCP2->Text = $favorisPub->getInstanceRecoursCp();
        $this->adresseVille2->Text = $favorisPub->getInstanceRecoursVille();
        $this->adresseURL2->Text = $favorisPub->getInstanceRecoursUrl();
        $this->cautionnementGaranties->Text = $favorisPub->getRegimeFinancierCautionnement();
        $this->modalitesFinancement->Text = $favorisPub->getRegimeFinancierModalitesFinancement();
    }
}
