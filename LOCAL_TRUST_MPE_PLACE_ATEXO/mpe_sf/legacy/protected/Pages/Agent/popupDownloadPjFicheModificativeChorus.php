<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class popupDownloadPjFicheModificativeChorus extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['id'])) {
            self::downloadPjFicheModificative(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    /**
     * permet de telecharger une piece jointe de la fiche modificative.
     *
     * @param int $idPjFicheModificative
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function downloadPjFicheModificative($idPjFicheModificative)
    {
        $ficheModificative = CommonTChorusFicheModificativePjQuery::create()->filterByIdFicheModificativePj($idPjFicheModificative)->findOne();
        if ($ficheModificative instanceof CommonTChorusFicheModificativePj) {
            $this->downloadFiles($ficheModificative->getFichier(), $ficheModificative->getNomFichier(), Atexo_CurrentUser::getOrganismAcronym());
        }
    }
}
