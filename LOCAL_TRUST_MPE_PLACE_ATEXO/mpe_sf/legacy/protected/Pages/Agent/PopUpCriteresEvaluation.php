<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonCriteresEvaluation;
use Application\Propel\Mpe\CommonCriteresEvaluationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Permet d'éditer les critères d'évaluation d'une consultation.
 *
 * @author Houriya MAATALLA <Houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class PopUpCriteresEvaluation extends MpeTPage
{
    private $_critereEvaluation;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $critereEvaluation = (new Atexo_CriteresEvaluation())->retrieveCriteresEvaluationById(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->_critereEvaluation = $critereEvaluation;
        if ($critereEvaluation) {
            $consultation = $this->retrieveConsultation($critereEvaluation->getConsultationId());
            if ($consultation instanceof CommonConsultation) {
                $this->IdConsultationSummary->setConsultation($consultation);
                if (!$this->isPostBack) {
                    $this->enveloppeFormulaire->Text = $critereEvaluation->getLibelleTypeEnveloppe();
                    $this->enveloppeFormulaireLot->Text = $critereEvaluation->getLibelleTypeEnveloppe();
                    if ($critereEvaluation->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                        $this->panelAvecLot->setVisible(false);
                    } else {
                        if ('0' == $critereEvaluation->getLot()) {
                            $this->panelAvecLot->setVisible(false);
                        } else {
                            $this->numLot->Text = $critereEvaluation->getLot();
                            $this->panelSansLot->setVisible(false);
                        }
                    }
                }
            }
        }
    }

    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function Allotie()
    {
        if ('0' != $this->_critereEvaluation->getLot()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retourne le type de l'enveloppe encodé en base64.
     */
    public function getTypeEnveloppe()
    {
        return base64_encode($this->_critereEvaluation->getTypeEnveloppe());
    }

    /**
     * Permet de mettre à jour le critère d'évaluation.
     *
     * @param $sender
     * @param $param
     */
    public function updateCriteresEvaluation($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $critereEval = CommonCriteresEvaluationPeer::retrieveByPK(Atexo_Util::builIdFromUidPlateforme($this->uidCritereEvaluation->value), $connexion);
        if ($critereEval instanceof CommonCriteresEvaluation) {
            $critereEval->setStatut($this->statutCritereEvaluation->value);
            $critereEval->save($connexion);
        }
        //Mise à jour de la page
        $script = '<script>';
        $script .= "window.opener.document.getElementById('ctl0_CONTENU_PAGE_AtexoCriteres_refreshPanel').click();window.close();";
        $script .= '</script>';
        $this->script->Text = $script;
    }
}
