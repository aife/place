<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonMarchePublie;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ListeMarches;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Marche\Atexo_Marche_Annee;
use Exception;

/**
 * Page de publication de la liste des marchés - Article 133.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PublicationListeMarches extends MpeTPage
{
    public $_idServiceAgent;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->getIsCallback()) {
            $this->fillRepeater((new Atexo_Marche_Annee())->getYearToDisplay());
        }
    }

    public function getIdServiceAgent()
    {
        if (null === $this->_idServiceAgent) {
            $this->_idServiceAgent = Atexo_CurrentUser::getCurrentServiceId();
        }

        return $this->_idServiceAgent;
    }

    /**
     * Remplir le repeater de publication.
     *
     * @param array $years liste des année
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function fillRepeater($years)
    {
        $dataSource = [];
        if (is_array($years) && count($years)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $idService = !$this->isOrganismeCentralisee() ? $this->getIdServiceAgent() : Atexo_ListeMarches::SERVICE_ORGANISME_CENTRALISEE;
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $i = 0;
            foreach ($years as $year) {
                $dataSource[$i]['annee'] = $year;
                $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($organisme, $idService, $year, $connexion);
                if ($marchePublie instanceof CommonMarchePublie) {
                    $dataSource[$i]['publie'] = $marchePublie->getIspubliee();
                    $dataSource[$i]['sourceDonnees'] = $marchePublie->getDatasource();
                } else {
                    $dataSource[$i]['publie'] = 0;
                }
                ++$i;
            }
        }
        $this->repraterAnneePublication->dataSource = $dataSource;
        $this->repraterAnneePublication->dataBind();
    }

    /**
     * Permet de changer le statut de publication d'une annee donnee.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function changePublication($sender, $param)
    {
        $connexion = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('app');
            $year = $param->CallbackParameter;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $idService = Atexo_CurrentUser::getCurrentServiceId();
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $connexion->beginTransaction();
            foreach ($this->repraterAnneePublication->Items as $oneItem) {
                if ($oneItem->labelYear->Text == $year) {
                    if ($this->isOrganismeCentralisee()) {
                        (new Atexo_ListeMarches())->setMarchePublieOrganismeCentralisee($organisme, $year, $connexion, $oneItem->btnPublie->checked);
                    } else {
                        $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($organisme, $idService, $year, $connexion);

                        if ($oneItem->btnPublie->checked) {
                            if (!($marchePublie instanceof CommonMarchePublie)) {
                                $marchePublie = new CommonMarchePublie();
                                $marchePublie->setOrganisme($organisme);
                                $marchePublie->setServiceId($idService);
                                $marchePublie->setNumeromarcheannee($year);
                            }
                            $marchePublie->setIspubliee('1');
                        } else {
                            if ($marchePublie instanceof CommonMarchePublie) {
                                $marchePublie->setIspubliee('0');
                            }
                        }
                        if ($marchePublie instanceof CommonMarchePublie) {
                            $marchePublie->save($connexion);
                        }
                    }
                }
            }
            $connexion->commit();
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error(' ERREUR ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }
    }

    /**
     * Permet de changer la source de donnée d'une publication d'une annee donnee.
     *
     * @param $sender
     * @param $param
     */
    public function updateDataSource($sender, $param): void
    {
        $connexion = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('app');
            $year = $param->CallbackParameter;
            $idService = $this->getIdServiceAgent();
            $organisme = Atexo_CurrentUser::getOrganismAcronym();

            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
            $connexion->beginTransaction();

            foreach ($this->repraterAnneePublication->Items as $oneItem) {
                if ($oneItem->labelYear->Text == $year) {
                    if ($this->isOrganismeCentralisee()) {
                        (new Atexo_ListeMarches())->setDataSourceOrganismeCentralisee($organisme, $year, $oneItem->sourceDonnees->Text, $oneItem->btnPublie->checked, $connexion);
                    } else {
                        $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($organisme, $idService, $year, $connexion);

                        if (!($marchePublie instanceof CommonMarchePublie)) {
                            $marchePublie = new CommonMarchePublie();
                            $marchePublie->setOrganisme($organisme);
                            $marchePublie->setServiceId($idService);
                            $marchePublie->setNumeromarcheannee($year);
                        }

                        $marchePublie->setDatasource($oneItem->sourceDonnees->Text);

                        if ($marchePublie instanceof CommonMarchePublie) {
                            $marchePublie->save($connexion);
                        }
                    }
                }
            }

            $connexion->commit();
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error(' ERREUR ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }
    }

    public function isOrganismeCentralisee(): bool
    {
        return Atexo_Module::isEnabled('OrganisationCentralisee', Atexo_CurrentUser::getOrganismAcronym());
    }
}
