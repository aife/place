<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Connecteur;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_ConsultationVo;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_GenerateXml;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfaceAuthentificationVo;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfacesUtilities;

class InterfaceSIS extends MpeTPage
{
    private bool $_xmlEncoded = true;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $interfacesVo = [];
        // Parcours de l'xml d'authentification
        $interfaceAuthVo = new Atexo_Interfaces_InterfaceAuthentificationVo();
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            if ($_POST['demande_xml_sis']) {
                if ($this->_xmlEncoded) {
                    $xml_demande = base64_decode($_POST['demande_xml_sis']);
                } else {
                    $xml_demande = $_POST['demande_xml_sis'];
                }
            } else {
                $xml_demande = Atexo_CurrentUser::readFromSession('xml');
            }
        } else {
            if ($this->_xmlEncoded) {
                $xml_demande = base64_decode($_POST['demande_xml_sis']);
            //echo $xml_demande;exit;
            } else {
                $xml_demande = $_POST['demande_xml_sis'];
            }
        }
        $xml_demande_content = simplexml_load_string($xml_demande);
        //récupération des attributs
        $interfaceAuthVo->setLogin(trim($xml_demande_content->DONNEES_LOCAL_TRUST_MPE['LOGIN_SIS']));
        $passwordSis = trim($xml_demande_content->DONNEES_LOCAL_TRUST_MPE['PASSWORD_SIS']);
        if (Atexo_Config::getParameter('LOGIN_SIS_PAR_MAIL')) {
            $interfaceAuthVo->setEmail($passwordSis);
        } else {
            $interfaceAuthVo->setPassWord($passwordSis);
        }
        $refConsDemande = trim((string) $xml_demande_content->DONNEES_LOCAL_TRUST_MPE['REF_CONS']);
        $refConsConnecteur = explode('-', $refConsDemande);
        //patch car dans le cas d'une demande import SIS transmet 11S0118-00-O alors que dans le cas creation cons : 11S0118-00
        //nous stockons toujours 11S0118-00 et donc pour retrouver la consultation il est nécessaire d'appliquer ce patch
        $interfaceAuthVo->setReference($refConsConnecteur[0].'-'.$refConsConnecteur[1]);

        $action = (new Atexo_Interfaces_InterfacesUtilities())->getActionFromCommande($xml_demande_content->DONNEES_LOCAL_TRUST_MPE['CMD_LT_MPE'], Atexo_Config::getParameter('CONNECTEUR_SIS'));
        $interfaceAuthVo->setAction($action);
        //fin
        // Parcours de l'xml de données
        $interfaceVo = new Atexo_Interfaces_ConsultationVo();
        if ($this->_xmlEncoded) {
            $xml_donnee = base64_decode($_POST['fichier_xml_donnee']);
        } else {
            $xml_donnee = $_POST['fichier_xml_donnee'];
        }
        $xml_donnee_content = simplexml_load_string($xml_donnee);
        //récupération des attributs
        $refConsDonnee = trim((string) $xml_donnee_content->DONNEES['REF_CONS']);
        $refUtilisateur = explode('-', $refConsDonnee);
        $interfaceVo->setReferenceUtilisateur($refUtilisateur[0]);
        //patch car dans le cas d'une demande import SIS transmet 11S0118-00-O alors que dans le cas creation cons : 11S0118-00
        //nous stockons toujours 11S0118-00 et donc pour retrouver la consultation il est nécessaire d'appliquer ce patch
        $interfaceVo->setReferenceConnecteur($refUtilisateur[0].'-'.$refUtilisateur[1]);
        $interfaceVo->setIntitule(utf8_encode(base64_decode(trim((string) $xml_donnee_content->DONNEES['TITRE_CONS']))));
        $interfaceVo->setObjet(utf8_encode(base64_decode(trim((string) $xml_donnee_content->DONNEES['RESUME_CONS']))));

        if ($xml_donnee_content->DONNEES['DATE_CLOT']) {
            $interfaceVo->setDateFin(str_replace(' ', 'T', Atexo_Util::frnDateTime2iso(trim((string) $xml_donnee_content->DONNEES['DATE_CLOT']))));
        }
        $categorieConsultation = (new Atexo_Interfaces_InterfacesUtilities())->getCategorieFromCode((string) $xml_donnee_content->DONNEES['CAT_CONS'], Atexo_Config::getParameter('CONNECTEUR_SIS'));
        $interfaceVo->setCategorie($categorieConsultation);
        //TODO
        $typePub = (string) $xml_donnee_content->DONNEES['TYPE_PUB'];
        $idTypeProcedure = (new Atexo_Interfaces_InterfacesUtilities())->getInterfaceTypeProcedureById($typePub, Atexo_Config::getParameter('CONNECTEUR_SIS'));
        $interfaceVo->setTypeAcces($idTypeProcedure);
        //TODO
        $typeProcedure = 'PUBLIC';
        $interfaceVo->setPhaseAcces($typeProcedure);
        // Ajout des Lots
        $nbrLot = trim((string) $xml_donnee_content->DONNEES['NB_LOT']);
        if ($nbrLot > 0) {
            $lots = (array) $xml_donnee_content->LOTS;
            foreach ((array) $lots['LOT'] as $oneLot) {
                $numero = trim((string) $oneLot['NUM_LOT']);
                $categorie = trim((new Atexo_Interfaces_InterfacesUtilities())->getCategorieFromCode((string) $oneLot['CAT_LOT'], Atexo_Config::getParameter('CONNECTEUR_SIS')));
                $libelle = utf8_encode(base64_decode(trim((string) $oneLot['INTITULE_LOT'])));
                $description = utf8_encode(base64_decode(trim((string) $oneLot['DESCRIPTION_LOT'])));
                $interfaceVo->addLot($numero, $categorie, $libelle, $description);
            }
        }
        $interfacesVo[] = $interfaceVo;
        //fin
        $xmlGenerator = new Atexo_Interfaces_GenerateXml();
        $xmlString = $xmlGenerator->generateInterfaceXml($interfaceAuthVo, $interfacesVo, Atexo_Config::getParameter('GENERATE_XML_CONSULTATION'));

        (new Atexo_Interfaces_InterfacesUtilities())->saveLog($_POST['fichier_xml_donnee'], 'fichier_xml', Atexo_Config::getParameter('CONNECTEUR_SIS'), '');
        (new Atexo_Interfaces_InterfacesUtilities())->saveLog($_POST['demande_xml_sis'], 'demande_xml', Atexo_Config::getParameter('CONNECTEUR_SIS'), '');

        $connecteur = new Atexo_Connecteur();
        $connecteur->setTypeInterface(Atexo_Config::getParameter('CONNECTEUR_SIS'));
        $connecteur->getXmlResponse(base64_encode($xmlString), '');
        //$this->js->Text="<script>document.main_form.xml.value='".base64_encode(htmlentities($xmlString)).
                //"';document.main_form.action='index.php?page=Commun.Connecteur';document.main_form.send.click();</script>";
    }
}
