<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Spreadsheet_Excel_Writer;

/*
 * Created on 3 nov. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class GenererExcel extends MpeTPage
{
    /*public function onLoad($param)
    {
        $this->genererFichierExcel();
    }
    */

    /**
     * La fonction génére un fichier excel.
     */
    public function genererFichierExcel($donnees)
    {
        try {
            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //$file = $this->getApplication()->Parameters['FICHIER_EXPORT_COMMISSION'];
            $file = Atexo_Config::getParameter('FICHIER_EXCEL_GENERE');

            //Initialisation du fichier excel
            $workbook = new Spreadsheet_Excel_Writer($file);

            //Style des titre 1
            $hdr0_format = &$workbook->addFormat();
            $hdr0_format->setBold();
            $hdr0_format->setColor('black');
            $hdr0_format->setPattern(1);
            $hdr0_format->setFgColor(1);
            $hdr0_format->setAlign('center');
            $hdr0_format->setFontFamily('Times New Roman');
            $hdr0_format->setSize(14);

            //Style des cellules d'entete
            $hdr1_format = &$workbook->addFormat();
            $hdr1_format->setBold();
            $hdr1_format->setColor('black');
            $hdr1_format->setPattern(1);
            $hdr1_format->setFgColor('white');
            $hdr1_format->setBorder(1);
            $hdr1_format->setAlign('center');
            $hdr1_format->setItalic();
            $hdr1_format->setFontFamily('Times New Roman');
            $hdr1_format->setAlign('center');
            $hdr1_format->setSize(12);
            $hdr1_format->setAlign('vcenter');
            $hdr1_format->setSize(10);

            //Style des cellules d'entete
            $hdr_format = &$workbook->addFormat();
            $hdr_format->setColor('black');
            $hdr_format->setBold();
            $hdr_format->setPattern(1);
            $hdr_format->setFgColor(1);
            $hdr_format->setAlign('center');
            $hdr_format->setFontFamily('Times New Roman');
            $hdr_format->setSize(12);

            //Style du footer
            $footer_format = &$workbook->addFormat();
            $footer_format->setBold();
            $footer_format->setColor('black');
            $footer_format->setPattern(1);
            $footer_format->setFgColor(1);
            $footer_format->setSize(14);
            $footer_format->setBorder(1);

            //Style des cellules
            $corps_format = &$workbook->addFormat();
            $corps_format->setColor('black');
            $corps_format->setPattern(1);
            $corps_format->setFgColor(1);
            $corps_format->setSize(10);
            $corps_format->setBorder(1);
            $corps_format->setAlign('center');
            $corps_format->setAlign('vcenter');

            $sing_format = &$workbook->addFormat();
            $sing_format->setColor('black');
            $sing_format->setPattern(1);
            $sing_format->setFgColor(1);
            $sing_format->setSize(10);
            $sing_format->setAlign('center');
            $sing_format->setAlign('vcenter');

            // la feuille
            $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso('CR_AUVRG'));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            //Creation de l'entete du fichier excel
            $worksheet->setColumn(0, 0, 25);
            $worksheet->setColumn(0, 1, 25);
            $worksheet->setColumn(0, 2, 25);
            $worksheet->setColumn(0, 3, 25);
            $worksheet->setColumn(0, 4, 20);
            $worksheet->setColumn(0, 5, 20);
            $worksheet->setColumn(0, 6, 25);
            $worksheet->setColumn(0, 7, 25);

            //Style des cellules modification des couleurs ( col : Rattachement )

            $corps_format_color = &$workbook->addFormat();
            $corps_format_color->setColor('black');
            $corps_format_color->setPattern(1);
            $corps_format_color->setFgColor(1);
            $corps_format_color->setSize(10);
            $corps_format_color->setBorder(1);
            $corps_format_color->setAlign('left');
            $corps_format_color->setAlign('vcenter');
            $corps_format_color->setFontFamily('Times New Roman');

            $corps_format_color1 = &$workbook->addFormat();
            $corps_format_color1->setColor('black');
            $corps_format_color1->setPattern(1);
            $corps_format_color1->setFgColor(23);
            $corps_format_color1->setSize(10);
            $corps_format_color1->setBorder(1);
            $corps_format_color1->setAlign('left');
            $corps_format_color1->setAlign('vcenter');
            $corps_format_color1->setFontFamily('Times New Roman');

            $corps_format_color2 = &$workbook->addFormat();
            $corps_format_color2->setColor('black');
            $corps_format_color2->setPattern(1);
            $corps_format_color2->setFgColor(22);
            $corps_format_color2->setSize(10);
            $corps_format_color2->setBorder(1);
            $corps_format_color2->setAlign('left');
            $corps_format_color2->setAlign('vcenter');
            $corps_format_color2->setFontFamily('Times New Roman');

            $corps_format_color3 = &$workbook->addFormat();
            $corps_format_color3->setColor('black');
            $corps_format_color3->setPattern(1);
            $corps_format_color3->setFgColor(51);
            $corps_format_color3->setSize(10);
            $corps_format_color3->setBorder(1);
            $corps_format_color3->setAlign('left');
            $corps_format_color3->setAlign('vcenter');
            $corps_format_color3->setFontFamily('Times New Roman');

            $corps_format_color4 = &$workbook->addFormat();
            $corps_format_color4->setColor('black');
            $corps_format_color4->setPattern(1);
            $corps_format_color4->setFgColor(41);
            $corps_format_color4->setSize(10);
            $corps_format_color4->setBorder(1);
            $corps_format_color4->setAlign('left');
            $corps_format_color4->setAlign('vcenter');
            $corps_format_color4->setFontFamily('Times New Roman');

            //Style des cellules en gras
            $corps_format_bold = &$workbook->addFormat();
            $corps_format_bold->setColor('black');
            $corps_format_bold->setPattern(1);
            $corps_format_bold->setFgColor(1);
            $corps_format_bold->setSize(10);
            $corps_format_bold->setBorder(1);
            $corps_format_bold->setAlign('center');
            $corps_format_bold->setAlign('vcenter');
            $corps_format_bold->setFontFamily('Times New Roman');
            $corps_format_bold->setBold();

            // Récuperation de la date de commission
            $worksheet->setRow(8, 25);
            $worksheet->write(2, 2, 'Liste des marchés', $hdr0_format);
            $worksheet->write(9, 0, "Type d'annonce", $hdr1_format);
            $worksheet->write(9, 1, 'Type de procédure', $hdr1_format);
            $worksheet->write(9, 2, 'Reference', $hdr1_format);
            $worksheet->write(9, 3, 'Catégorie principale', $hdr1_format);
            $worksheet->write(9, 4, 'Titre', $hdr1_format);
            $worksheet->write(9, 5, 'Objet du marché', $hdr1_format);
            $worksheet->write(9, 6, 'Date de mise enligne', $hdr1_format);
            $worksheet->write(9, 7, 'Date de clôture', $hdr1_format);
            $worksheet->write(9, 8, 'Marché alloti', $hdr1_format);
            $worksheet->write(9, 9, 'Libellé du lot', $hdr1_format);
            $worksheet->write(9, 10, 'Catégorie du lot', $hdr1_format);
            $worksheet->write(9, 11, 'Date de début du marché', $hdr1_format);
            $worksheet->write(9, 12, 'Date de fin du marché', $hdr1_format);
            $worksheet->write(9, 13, 'Montant du marché', $hdr1_format);
            $worksheet->write(9, 14, "Nom de l'attributaire", $hdr1_format);
            $worksheet->write(9, 15, 'Code postal des attributaire', $hdr1_format);

            $row = 10;

            //$valRefManager = new ValeurReferentielManager();

            $i = 1;
            if (is_array($donnees)) {
                $liste = new ListeDesMarches();
                foreach ($donnees as $ligne) {
                    $col = 0;
                    $worksheet->setRow($row, 20);

                    $worksheet->write($row, $col++, $liste->getLibellesTypeProcedure($ligne->getIdTypeProcedure()), $corps_format);
                    $worksheet->write($row, $col++, $liste->getAvisAbbreviation($ligne->getIdTypeAvis()), $corps_format);
                    $worksheet->write($row, $col++, $ligne->getReference(), $corps_format);
                    $worksheet->write($row, $col++, $liste->getCategories($ligne->getReference()), $corps_format);
                    $worksheet->write($row, $col++, $liste->getTitres($ligne->getReference()), $corps_format);
                    $worksheet->write($row, $col++, $liste->getResumes($ligne->getObjet()), $corps_format);
                    $worksheet->write($row, $col++, Atexo_Util::iso2frnDate($liste->dateTime($ligne->getDatemiseenligne())), $corps_format);
                    $worksheet->write($row, $col++, Atexo_Util::iso2frnDate($liste->dateTime($ligne->getDateFin())), $corps_format);
                    if ('' == $ligne->getLot()) {
                        $worksheet->write($row, $col++, 'non', $corps_format);
                    } else {
                        $worksheet->write($row, $col++, 'oui', $corps_format);
                    }
                    if ('' == $ligne->getLot()) {
                        $worksheet->write($row, $col++, 'N/A', $corps_format);
                    } else {
                        $worksheet->write($row, $col++, $ligne->getDescription(), $corps_format);
                    }
                    if ('' == $ligne->getLot()) {
                        $worksheet->write($row, $col++, 'N/A', $corps_format);
                    } else {
                        $worksheet->write($row, $col++, $ligne->getCategorieLot(), $corps_format);
                    }
                    ++$row;
                    ++$i;
                }
            }

            $workbook->close();
        } catch (Exception $e) {
            throw new Exception("Erreur d'export excel.  ".$e->getMessage());
        }
    }

    public function genererFichierExcel1()
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        //Initialisation du fichier excel
        $workbook = new Spreadsheet_Excel_Writer();

        //Style des cellules d'entete
        $hdr0_format = &$workbook->addFormat();
        $hdr0_format->setBold();
        $hdr0_format->setColor('black');
        $hdr0_format->setPattern(1);
        $hdr0_format->setFgColor(1);
        $hdr0_format->setAlign('center');
        $hdr0_format->setSize(14);

        //Style des cellules d'entete
        $hdr_format = &$workbook->addFormat();
        $hdr_format->setColor('black');
        $hdr_format->setBold();
        $hdr_format->setPattern(1);
        $hdr_format->setFgColor(24);
        $hdr_format->setAlign('center');
        $hdr_format->setSize(12);
        $hdr_format->setBorder(1);
        $hdr_format->setOutLine();

        //Style du footer
        $footer_format = &$workbook->addFormat();
        $footer_format->setBold();
        $footer_format->setColor('black');
        $footer_format->setPattern(1);
        $footer_format->setFgColor(1);
        $footer_format->setSize(14);
        $footer_format->setBorder(1);

        //Style des cellules
        $corps_format = &$workbook->addFormat();
        $corps_format->setColor('black');
        $corps_format->setPattern(1);
        $corps_format->setFgColor(1);
        $corps_format->setSize(10);
        $corps_format->setBorder(1);
        $corps_format->setAlign('left');
        $corps_format->setAlign('vcenter');

        $nomDuFichier = 'VSF';
        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomDuFichier));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

        for ($i = 0; $i < 36; ++$i) {
            $worksheet->setColumn($i, $i, 20);
        }

        $worksheet->write(0, 0, 'Programme 2000-2006', $corps_format);
        $worksheet->write(0, 1, 'Mesure', $corps_format);
        $worksheet->write(0, 2, 'Dispositif-Campagne', $corps_format);
        $worksheet->write(0, 3, 'N° de marché', $corps_format);
        $worksheet->write(0, 4, "Maître d'ouvrage", $corps_format);
        $worksheet->write(0, 5, 'Libellé opération', $corps_format);
        $worksheet->write(0, 6, 'coût total éligible '.'\n'.' programmé du marché', $corps_format);
        $worksheet->write(0, 7, 'Montant payé '.'\n'.' au titre de '.'\n'." l'avance forfaitaire", $corps_format);
        $worksheet->write(0, 8, 'N°mandat avance', $corps_format);
        $worksheet->write(0, 9, 'date mandat avance', $corps_format);
        $worksheet->write(0, 10, 'Montant payé '.'\n'.' au titre de '.'\n'." l'acompte facultatif de 20%", $corps_format);
        $worksheet->write(0, 11, 'N°mandat 20%', $corps_format);
        $worksheet->write(0, 12, 'date mandat 20%', $corps_format);
        $worksheet->write(0, 13 + $i * 3, 'Solde', $corps_format);
        $worksheet->write(0, 14 + $i * 3, 'N°mandat solde', $corps_format);
        $worksheet->write(0, 15 + $i * 3, 'date mandat solde', $corps_format);
        $worksheet->write(0, 16 + $i * 3, 'Montant du marché HT', $corps_format);
        $worksheet->write(0, 17 + $i * 3, 'Montant marché TTC', $corps_format);
        $worksheet->write(0, 18 + $i * 3, 'Date réelle '.'\n'.' de démarage '.'\n'." de l'action", $corps_format);
        $worksheet->write(0, 19 + $i * 3, "Nombre d'heures ".'\n'.' éligibles réalisées', $corps_format);
        $worksheet->write(0, 20 + $i * 3, "Nombre d'heures ".'\n'.' éligibles réalisées '.'\n'.' en centre', $corps_format);
        $worksheet->write(0, 21 + $i * 3, "Nombre d'heures ".'\n'.' éligibles réalisées '.'\n'.' en etp.', $corps_format);
        $worksheet->write(0, 22 + $i * 3, "Nombre de stagiaires\néligibles intégrés", $corps_format);
        $worksheet->write(0, 23 + $i * 3, 'Total réalisation '.'\n'.' intermédiaire', $corps_format);
        $worksheet->write(0, 24 + $i * 3, '% réalisation intermédiaire '.'\n'." /\ncoût total éligible programmé", $corps_format);
        $worksheet->setRow(0, 50);

        try {
            $workbook->send('FichierExcel.xls');
            $workbook->close();

            return true;
        } catch (Exception $e) {
            throw new Exception("Erreur d'export excel.  ".$e->getMessage());
        }
    }
}
