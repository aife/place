<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_AutresPiecesMiseDisposition;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger Autres Pieces Mise disposition.
 *
 * @author Khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class DownloadAutrePieceMiseDisposition extends DownloadFile
{
    private $_piece;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['idPiece'])) {
            $this->_piece = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idPiece']));
            $autrePiece = (new Atexo_AutresPiecesMiseDisposition())->getAutrePieceById($this->_piece);
            if ($autrePiece) {
                $this->_idFichier = $autrePiece->getIdBlob();
                $this->_nomFichier = $autrePiece->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getCurrentOrganism());
            }
        }
    }
}
