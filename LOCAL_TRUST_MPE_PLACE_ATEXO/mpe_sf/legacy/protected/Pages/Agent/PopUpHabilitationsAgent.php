<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\Om\BaseCommonConfigurationOrganisme;
use Application\Service\Atexo\Agent\Atexo_Agent_VisionRma;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Exceptions\THttpException;
use Prado\Prado;

/**
 * Classe de configuration des habilitations.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpHabilitationsAgent extends MpeTPage
{
    protected $idAgent;
    protected $connexion;
    protected bool $readOnly = false;
    protected $previousStyle = 'on';
    protected bool $previousVisibility = false;
    protected ?bool $profilRmaVisible = null;
    protected $codeOrganisme = null;
    protected $disabled = null;
    protected ?string $base64 = null;
    protected $codeRandom;
    protected ?string $selecte = null;
    protected $commonTInviteTransverse;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($_GET['id']) {
            $idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $agentToUpO = (new CommonAgentQuery())->findOneById($idAgent);

            $agentConnectedO = new CommonAgent();
            $agentConnectedO->setId(Atexo_CurrentUser::getIdAgentConnected());
            $agentConnectedO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $agentConnectedO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());

            // mode visualisation
            if (2 == $_GET['mode'] && $agentToUpO->getOrganisme() == Atexo_CurrentUser::getOrganismAcronym()) {
                $this->readOnly = true;
                $this->buttonAnnuler->visible = false;
                $this->buttonValider->visible = false;
                $this->titreHabilitationAgent->Text = Prado::localize('TEXT_VISUALISER_HABILITATIONS_AGENT');
                $this->buttonFermer->visible = true;
            }
            // mode modification
            elseif (
                Atexo_CurrentUser::hasHabilitation('HyperAdmin')
                    || (Atexo_CurrentUser::hasHabilitation('GestionHabilitations') && (new Atexo_Agent())->verifyCapacityToModify($agentConnectedO, $agentToUpO))
            ) {
                $this->titreHabilitationAgent->Text = Prado::localize('TEXT_DETAIL_HABILITATIONS_AGENT');
                $this->buttonFermer->visible = false;
            }
            // Erreur de droits
            else {
                $this->panelMessage->setVisible('true');
                $this->panelMessage->setMessage(Prado::localize('TEXT_PAS_AUTORISE_MODIFIER_INFORMATIONS_AGENT'));
                $this->panelHabilitations->visible = false;

                return;
            }
            $this->idAgent = $idAgent;
            // habilitations autorisees, affichage des habilitations de l'agent
            if (!$this->isPostBack) {
                $this->panelMessage->setVisible('false');
                $this->getAgentHabilitations();
                $agentObject = $this->getInfosAgent();
                if ($agentObject) {
                    $this->nomAgent->Text = $agentObject->getNom();
                    $this->prenomAgent->Text = $agentObject->getPrenom();
                }
            }
        }

        $organismeUser = Atexo_CurrentUser::getCurrentOrganism();
        $string = $organismeUser . '_0_' . $_GET['id'];
        $this->base64 = base64_encode($string);
        $this->codeRandom = $this->generateRandomString();

        $commonInviteTransverse = (new CommonInvitePermanentTransverseQuery())->getSelectedService($_GET['id']);

        $this->selecte = '';
        foreach ($commonInviteTransverse as $invite) {
            if (
                $organismeUser == $invite['acronyme'] &&
                null == $invite['serviceId'] &&
                $_GET['id'] == $invite['agentId']
            ) {
                $this->selecte = "checked='checked'";
            }
        }

        if (!$this->IsPostBack) {
            if (
                Atexo_CurrentUser::hasHabilitation('HyperAdmin') &&
                Atexo_Module::isEnabled('GestionOrganismeParAgent') &&
                isset($_GET['org']) && $_GET['org']
            ) {
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
            } else {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
            }
            $this->displayInfosOrganisme($organisme);
            $this->TreeServices->Text = self::displayEntitiesTree($organisme, $organismeUser);
        }
    }

    /**
     * affiche les habilitations des agents.
     */
    public function getAgentHabilitations()
    {
        $c = new Criteria();
        $c->add(CommonHabilitationAgentPeer::ID_AGENT, $this->idAgent, Criteria::EQUAL);
        $habilitationsAgent = CommonHabilitationAgentPeer::doSelectOne($c, $this->connexion);
        if ($habilitationsAgent) {
            $this->gestionModulEnvol->setVisible('false');

            $configurationOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());

            if ($configurationOrganisme instanceof BaseCommonConfigurationOrganisme && $configurationOrganisme->getModuleEnvol()) {
                $this->gestionModulEnvol->setVisible('true');
            }

            $this->espaceDoc->setVisible('false');
            if (Atexo_Module::isEnabled('EspaceDocumentaire')) {
                $this->espaceDoc->setVisible('true');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getEspaceDocumentaireConsultation(), 'espace_documentaire_consultation');
            }

            if (Atexo_Module::isEnabled('EchangesDocuments', Atexo_CurrentUser::getOrganismAcronym())) {
                $this->echangesDocumentaires->setVisible('true');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getAccesEchangeDocumentaire(), 'acces_echange_documentaire');
            }

            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererMapaInferieurMontant(), 'gerer_mapa_inferieur_9000');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererMapaSuperieurMontant(), 'gerer_mapa_superieur_9000');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAdministrerProceduresFormalisees(), 'administrer_procedure');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionEnvol(), 'gestion_envol');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerConsultationTransverse(), 'creer_consultation_transverse');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerSuiteConsultation(), 'creer_suite_consultation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getDuplicationConsultations(), 'duplication_consultation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierConsultationAvantValidation(), 'modifier_consultation_avant_validation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getValidationSimple(), 'validation_simple');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getValidationIntermediaire(), 'validation_intermediaire');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getValidationFinale(), 'validation_finale');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierConsultationApresValidation(), 'modifier_consultation_apres_validation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAccesRegistreRetraitsPapier(), 'acces_registre_retraits_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAccesRegistreQuestionsPapier(), 'acces_registre_questions_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAccesRegistreDepotsPapier(), 'acces_registre_depots_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAccesReponses(), 'acces_reponses');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getTelechargementGroupeAnticipePlisChiffres(), 'telechargement_groupe_anticipe_plis_chiffres');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getTelechargementUnitairePlisChiffres(), 'telechargement_unitaire_plis_chiffres');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirCandidatureEnLigne(), 'ouvrir_candidature_en_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirCandidatureADistance(), 'ouvrir_candidature_a_distance');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirCandidatureHorsLigne(), 'ouvrir_candidature_hors_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreEnLigne(), 'ouvrir_offre_en_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreADistance(), 'ouvrir_offre_a_distance');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreHorsLigne(), 'ouvrir_offre_hors_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirAnonymatEnLigne(), 'ouvrir_anonymat_en_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirAnonymatHorsLigne(), 'ouvrir_anonymat_hors_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererEncheres(), 'gerer_encheres');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuivreEncheres(), 'suivre_encheres');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAttributionMarche(), 'reseigner_decision');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getDecisionSuiviSeul(), 'reseigner_decision_suivi_seul');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceInformation(), 'creer_annonce_information');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceAttribution(), 'creer_annonce_attribution');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSupprimerEnveloppe(), 'supprimer_enveloppe');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getPublicationMarches(), 'publication_marches');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererStatistiquesMetier(), 'gerer_statistiques_metier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererArchives(), 'gerer_archives');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAccederTousTelechargements(), 'accederTousTelechargement');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getDownloadArchives(), 'telechargement_archives');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionAgentPole(), 'gestion_agent_pole');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererAdmissibilite(), 'gerer_admissibilite');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getImporterEnveloppe(), 'importer_enveloppe');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreTechniqueEnLigne(), 'ouvrir_offre_technique_en_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreTechniqueADistance(), 'ouvrir_offre_technique_a_distance');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirOffreTechniqueHorsLigne(), 'ouvrir_offre_technique_hors_ligne');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererMonService(), 'gerer_mon_service');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceExtraitPv(), 'creer_annonce_extrait_pv');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceRapportAchevement(), 'creer_annonce_rapport_achevement');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceDecisionResiliation(), 'creer_annonce_decision_resiliation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAvisProgrammePrevisionnel(), 'creer_avis_programme_previsionnel');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerAnnonceSyntheseRapportAudit(), 'creer_avis_synthese_rapport_audit');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererNewsletter(), 'gerer_newsletter');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererNewsletterRedac(), 'gerer_newsletter_redac');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getEspaceCollaboratifGestionnaire(), 'espaceCollaboratifGestionnaire');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getEspaceCollaboratifContributeur(), 'espaceCollaboratifContributeur');
            if ($this->isProfilRmaVisible()) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getProfilRma(), 'profilRMA');
            }
            $this->checkOrUncheckHabilitation($habilitationsAgent->getHistoriqueNavigationInscrits(), 'historiqueEntreprises');
            if (true == $this->readOnly) {
                $this->gestion_agent_pole->Enabled = false;
            } else {
                if (Atexo_CurrentUser::hasHabilitation('DroitGestionServices')) {
                    $this->gestion_agent_pole->Enabled = true;
                } else {
                    $this->gestion_agent_pole->Enabled = false;
                }
            }
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionAgents(), 'gestion_agents');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionHabilitations(), 'gestion_habilitations');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionCompteBoamp(), 'gestion_compte_boamp');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionBiCles(), 'gestion_bi_cles');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getPublierConsultation(), 'publier_consultation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionFournisseursEnvoisPostaux(), 'gestion_fournisseurs_envois_postaux');
            //$this->checkOrUncheckHabilitation($habilitationsAgent->getStatistiquesSite(),'statistiques_site');
            //$this->checkOrUncheckHabilitation($habilitationsAgent->getStatistiquesQos(),'statistiques_QoS');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getInvitePermanentEntiteDependante(), 'invite_permanent_entite_dependante');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getInvitePermanentMonEntite(), 'invite_permanent_mon_entite');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getInvitePermanentTransverse(), 'invite_permanent_transverse');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getRefuserEnveloppe(), 'refuser_enveloppe');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionCompteJal(), 'gestion_compte_jal');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionAdressesFacturationJal(), 'gestion_facturation_jal');
            if (Atexo_CurrentUser::hasHabilitation('GestionAdressesFacturationJal')) {
                $this->gestion_facturation_jal->Enabled = true;
            } else {
                $this->gestion_facturation_jal->Enabled = false;
            }
            $this->checkOrUncheckHabilitation($habilitationsAgent->getOuvrirAnonymatADistance(), 'ouvrir_anonymat_a_distance');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getRestaurerEnveloppe(), 'restaurer_enveloppe');
            if (Atexo_Module::isEnabled('CentralePublication')) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionCentralePub(), 'gestion_centrale_pub');
            }
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionCompteGroupeMoniteur(), 'gestion_groupe_moniteur');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreRetraitsPapier(), 'suivi_seul_registre_retraits_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreQuestionsPapier(), 'suivi_seul_registre_questions_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreDepotsPapier(), 'suivi_seul_registre_depots_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreRetraitsElectronique(), 'suivi_seul_registre_retraits_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreQuestionsElectronique(), 'suivi_seul_registre_questions_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviSeulRegistreDepotsElectronique(), 'suivi_seul_registre_depots_papier');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierConsultationMapaInferieurMontantApresValidation(), 'modifier_consultation_mapa_inferieur_montant_apres_validation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierConsultationMapaSuperieurMontantApresValidation(), 'modifier_consultation_mapa_superieur_montant_apres_validation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierConsultationProceduresFormaliseesApresValidation(), 'modifier_consultation_procedures_formalisees_apres_validation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererLesEntreprises(), 'gerer_les_entreprises');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAnnuaireAcheteur(), 'annuaire_acheteur');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviEntreprise(), 'suivi_entreprise_bdf');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGererAdressesService(), 'gerer_adresses_service');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getResultatAnalyse(), 'resultat_analyse');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getPorteeSocietesExclues(), 'portee_societes_exclues');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getPorteeSocietesExcluesTousOrganismes(), 'portee_societes_exclues_tous_organismes');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierSocietesExclues(), 'modifier_societes_exclues');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSupprimerSocietesExclues(), 'supprimer_societes_exclues');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionCertificatsAgent(), 'gestion_certificats_agent');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionMandataire(), 'gestion_mandataire');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getAnnulerConsultation(), 'annuler_consultation');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getEnvoyerPublicite(), 'envoyer_publicite');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getListeMarchesNotifies(), 'liste_marches_notifies');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getenvoyerMessage(), 'envoyer_message');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuivreMessage(), 'suivre_message');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSuiviFluxChorusTransversal(), 'suivi_flux_chorus_transversal');
            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getRedactionDocumentsRedac(), 'redaction_documents_redac');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getValidationDocumentsRedac(), 'validation_documents_redac');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getReprendreIntegralementArticle(), 'reprendre_Integralement_Article');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getAdministrerClausesEntiteAchats(), 'administer_clauses_entite_achat');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGenererPiecesFormatOdt(), 'generer_pieces_format');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGererGabaritEntiteAchats(), 'gerer_gabarit_entite_achats');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGererGabaritAgent(), 'gerer_gabarit_agent');
            }
            if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGererReouverturesModification(), 'reouverture_modification');
            }
            if (Atexo_Module::isEnabled('MiseDispositionPiecesMarche')) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionMiseDispositionPiecesMarche(), 'mise_a_disposition');
            }
            if (Atexo_Module::isEnabled('gestionOperations', Atexo_CurrentUser::getOrganismAcronym())) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGererOperations(), 'GererOperations');
            }
            $this->checkOrUncheckHabilitation($habilitationsAgent->getCreerContrat(), 'check_creer_contrat');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getModifierContrat(), 'check_gestion_contrat');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getSupprimerContrat(), 'check_supprimer_contrat');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getConsulterContrat(), 'check_consulter_contrat');

            $this->checkOrUncheckHabilitation($habilitationsAgent->getExecVoirContratsEa(), 'check_exec_voir_contrats_ea');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getExecVoirContratsEaDependantes(), 'check_exec_voir_contrats_ea_dependantes');
            $this->checkOrUncheckHabilitation($habilitationsAgent->getExecVoirContratsOrganisme(), 'check_exec_voir_contrats_organisme');

            if (Atexo_Module::isEnabled('ModuleEcoSip')) {
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionValidationSip(), 'gestion_validation_sip');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionValidationEco(), 'gestion_validation_eco');
            } else {
                $this->gestionModulEcoSip->setVisible('false');
            }

            if (Atexo_Module::isEnabled('AfficherRattachementService')) {
                $this->gestionRattachementService->setVisible('true');
                $this->checkOrUncheckHabilitation($habilitationsAgent->getRattachementService(), 'rattachementService');
            } else {
                $this->gestionRattachementService->setVisible('false');
            }

            $this->checkOrUncheckHabilitation($habilitationsAgent->getGestionSpaserConsultations(), 'spaserConsultation');
            $this->getHabilitationModuleRecensementProgrammation($habilitationsAgent);
        }

        return $habilitationsAgent;
    }

    private function getHabilitationModuleRecensementProgrammation(CommonHabilitationAgent $habilitationsAgent): void
    {
        if (
            Atexo_Module::isEnabled(
                'moduleRecensementProgrammation',
                Atexo_CurrentUser::getOrganismAcronym()
            )
        ) {
            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getBesoinUnitaireConsultation(),
                'besoin_unitaire_consultation'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getBesoinUnitaireCreationModification(),
                'besoin_unitaire_creation_modification'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getDemandeAchatConsultation(),
                'demande_achat_consultation'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getDemandeAchatCreationModification(),
                'demande_achat_creation_modification'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getProjetAchatConsultation(),
                'projet_achat_consultation'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getProjetAchatCreationModification(),
                'projet_achat_creation_modification'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getValidationOpportunite(),
                'validation_opportunite'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getValidationAchat(),
                'validation_achat'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getValidationBudget(),
                'validation_budget'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getValiderProjetAchat(),
                'valider_projet_achat'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getStrategieAchatGestion(),
                'strategie_achat_gestion'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getRecensementProgrammationAdministration(),
                'recensement_programmation_administration'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getProjetAchatLancementSourcing(),
                'projet_achat_lancement_sourcing'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getProjetAchatInvalidation(),
                'projet_achat_invalidation'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getProjetAchatAnnulation(),
                'projet_achat_annulation'
            );

            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getLancementProcedure(),
                'lancement_procedure'
            );


        }
        if (
            Atexo_Module::isEnabled(
                'ModuleAdministrationDocument',
                Atexo_CurrentUser::getOrganismAcronym()
            )
        ) {
            $this->checkOrUncheckHabilitation(
                $habilitationsAgent->getAdministrationDocumentsModeles(),
                'administration_documents_modeles'
            );
        }

        return;
    }

    private function setHabilitationModuleRecensementProgrammation(CommonHabilitationAgent $habilitationsAgent): CommonHabilitationAgent
    {
        if (
            Atexo_Module::isEnabled(
                'moduleRecensementProgrammation',
                Atexo_CurrentUser::getOrganismAcronym()
            )
        ) {
            $habilitationsAgent->setBesoinUnitaireConsultation(
                $this->getEnabledHabilitation('besoin_unitaire_consultation')
            );

            $habilitationsAgent->setBesoinUnitaireCreationModification(
                $this->getEnabledHabilitation('besoin_unitaire_creation_modification')
            );

            $habilitationsAgent->setDemandeAchatConsultation(
                $this->getEnabledHabilitation('demande_achat_consultation')
            );

            $habilitationsAgent->setDemandeAchatCreationModification(
                $this->getEnabledHabilitation('demande_achat_creation_modification')
            );

            $habilitationsAgent->setProjetAchatConsultation(
                $this->getEnabledHabilitation('projet_achat_consultation')
            );

            $habilitationsAgent->setProjetAchatCreationModification(
                $this->getEnabledHabilitation('projet_achat_creation_modification')
            );

            $habilitationsAgent->setValidationOpportunite(
                $this->getEnabledHabilitation('validation_opportunite')
            );

            $habilitationsAgent->setValidationAchat(
                $this->getEnabledHabilitation('validation_achat')
            );

            $habilitationsAgent->setValidationBudget(
                $this->getEnabledHabilitation('validation_budget')
            );

            $habilitationsAgent->setValiderProjetAchat(
                $this->getEnabledHabilitation('valider_projet_achat')
            );

            $habilitationsAgent->setStrategieAchatGestion(
                $this->getEnabledHabilitation('strategie_achat_gestion')
            );

            $habilitationsAgent->setRecensementProgrammationAdministration(
                $this->getEnabledHabilitation('recensement_programmation_administration')
            );

            $habilitationsAgent->setProjetAchatLancementSourcing(
                $this->getEnabledHabilitation('projet_achat_lancement_sourcing')
            );

            $habilitationsAgent->setProjetAchatInvalidation(
                $this->getEnabledHabilitation('projet_achat_invalidation')
            );

            $habilitationsAgent->setProjetAchatAnnulation(
                $this->getEnabledHabilitation('projet_achat_annulation')
            );

            $habilitationsAgent->setLancementProcedure(
                $this->getEnabledHabilitation('lancement_procedure')
            );


        }


        if (
            Atexo_Module::isEnabled(
                'ModuleAdministrationDocument',
                Atexo_CurrentUser::getOrganismAcronym()
            )
        ) {
            $habilitationsAgent->setAdministrationDocumentsModeles(
                $this->getEnabledHabilitation('administration_documents_modeles')
            );
        }

        return $habilitationsAgent;
    }

    /**
     * enregistre les habilitations de l'agent.
     */
    public function saveHabilitationsForAgent($sender, $param)
    {
        $habilitationsAgent = null;
        $this->panelMessage->setVisible('false');
        if ('1' == $this->getEnabledHabilitation('invite_permanent_transverse') && is_null($_POST['service'])) {
            $this->panelMessage->setVisible('true');
            $this->panelMessage->setMessage(Prado::localize('INVITE_TRANSVERSE_SELECT_SERVICE'));

            return;
        }

        if ($this->idAgent) {
            $c = new Criteria();
            $c->add(CommonHabilitationAgentPeer::ID_AGENT, $this->idAgent, Criteria::EQUAL);
            $habilitationsAgent = CommonHabilitationAgentPeer::doSelectOne($c, $this->connexion);
        }
        if ($habilitationsAgent instanceof CommonHabilitationAgent) {
            $habilitationsAgent->setGestionValidationSip($this->getEnabledHabilitation('gestion_validation_sip'));
            $habilitationsAgent->setGestionValidationEco($this->getEnabledHabilitation('gestion_validation_eco'));
            $habilitationsAgent->setGererMapaInferieurMontant($this->getEnabledHabilitation('gerer_mapa_inferieur_9000'));
            $habilitationsAgent->setGererMapaSuperieurMontant($this->getEnabledHabilitation('gerer_mapa_superieur_9000'));
            $habilitationsAgent->setAdministrerProceduresFormalisees($this->getEnabledHabilitation('administrer_procedure'));
            $habilitationsAgent->setGestionEnvol($this->getEnabledHabilitation('gestion_envol'));
            $habilitationsAgent->setCreerConsultationTransverse($this->getEnabledHabilitation('creer_consultation_transverse'));
            $habilitationsAgent->setCreerSuiteConsultation($this->getEnabledHabilitation('creer_suite_consultation'));
            $habilitationsAgent->setDuplicationConsultations($this->getEnabledHabilitation('duplication_consultation'));
            $habilitationsAgent->setModifierConsultationAvantValidation($this->getEnabledHabilitation('modifier_consultation_avant_validation'));
            $habilitationsAgent->setValidationSimple($this->getEnabledHabilitation('validation_simple'));
            $habilitationsAgent->setValidationIntermediaire($this->getEnabledHabilitation('validation_intermediaire'));
            $habilitationsAgent->setValidationFinale($this->getEnabledHabilitation('validation_finale'));
            $habilitationsAgent->setModifierConsultationApresValidation($this->getEnabledHabilitation('modifier_consultation_apres_validation'));
            $habilitationsAgent->setAccesRegistreRetraitsPapier($this->getEnabledHabilitation('acces_registre_retraits_papier'));
            $habilitationsAgent->setAccesRegistreQuestionsPapier($this->getEnabledHabilitation('acces_registre_questions_papier'));
            $habilitationsAgent->setAccesRegistreDepotsPapier($this->getEnabledHabilitation('acces_registre_depots_papier'));

            $habilitationsAgent->setAccesRegistreRetraitsElectronique($this->getEnabledHabilitation('acces_registre_retraits_papier'));
            $habilitationsAgent->setAccesRegistreQuestionsElectronique($this->getEnabledHabilitation('acces_registre_questions_papier'));
            $habilitationsAgent->setAccesRegistreDepotsElectronique($this->getEnabledHabilitation('acces_registre_depots_papier'));

            $habilitationsAgent->setAccesReponses($this->getEnabledHabilitation('acces_reponses'));
            $habilitationsAgent->setTelechargementGroupeAnticipePlisChiffres($this->getEnabledHabilitation('telechargement_groupe_anticipe_plis_chiffres'));
            $habilitationsAgent->setTelechargementUnitairePlisChiffres($this->getEnabledHabilitation('telechargement_unitaire_plis_chiffres'));
            $habilitationsAgent->setOuvrirCandidatureEnLigne($this->getEnabledHabilitation('ouvrir_candidature_en_ligne'));
            $habilitationsAgent->setOuvrirCandidatureADistance($this->getEnabledHabilitation('ouvrir_candidature_a_distance'));
            $habilitationsAgent->setOuvrirCandidatureHorsLigne($this->getEnabledHabilitation('ouvrir_candidature_hors_ligne'));
            $habilitationsAgent->setOuvrirOffreEnLigne($this->getEnabledHabilitation('ouvrir_offre_en_ligne'));
            $habilitationsAgent->setOuvrirOffreADistance($this->getEnabledHabilitation('ouvrir_offre_a_distance'));
            $habilitationsAgent->setOuvrirOffreHorsLigne($this->getEnabledHabilitation('ouvrir_offre_hors_ligne'));
            $habilitationsAgent->setOuvrirAnonymatEnLigne($this->getEnabledHabilitation('ouvrir_anonymat_en_ligne'));
            $habilitationsAgent->setOuvrirAnonymatHorsLigne($this->getEnabledHabilitation('ouvrir_anonymat_hors_ligne'));
            $habilitationsAgent->setGererEncheres($this->getEnabledHabilitation('gerer_encheres'));
            $habilitationsAgent->setSuivreEncheres($this->getEnabledHabilitation('suivre_encheres'));
            $habilitationsAgent->setAttributionMarche($this->getEnabledHabilitation('reseigner_decision'));
            $habilitationsAgent->setDecisionSuiviSeul($this->getEnabledHabilitation('reseigner_decision_suivi_seul'));
            $habilitationsAgent->setCreerAnnonceInformation($this->getEnabledHabilitation('creer_annonce_information'));
            $habilitationsAgent->setCreerAnnonceAttribution($this->getEnabledHabilitation('creer_annonce_attribution'));
            $habilitationsAgent->setSupprimerEnveloppe($this->getEnabledHabilitation('supprimer_enveloppe'));
            $habilitationsAgent->setPublicationMarches($this->getEnabledHabilitation('publication_marches'));
            $habilitationsAgent->setGererStatistiquesMetier($this->getEnabledHabilitation('gerer_statistiques_metier'));
            $habilitationsAgent->setGererArchives($this->getEnabledHabilitation('gerer_archives'));
            $habilitationsAgent->setAccederTousTelechargements($this->getEnabledHabilitation('accederTousTelechargement'));
            $habilitationsAgent->setDownloadArchives($this->getEnabledHabilitation('telechargement_archives'));
            $habilitationsAgent->setGestionAgentPole($this->getEnabledHabilitation('gestion_agent_pole'));
            $habilitationsAgent->setGestionAgents($this->getEnabledHabilitation('gestion_agents'));
            $habilitationsAgent->setGestionHabilitations($this->getEnabledHabilitation('gestion_habilitations'));
            $habilitationsAgent->setGestionCompteBoamp($this->getEnabledHabilitation('gestion_compte_boamp'));
            $habilitationsAgent->setGestionBiCles($this->getEnabledHabilitation('gestion_bi_cles'));
            $habilitationsAgent->setPublierConsultation($this->getEnabledHabilitation('publier_consultation'));
            $habilitationsAgent->setGestionFournisseursEnvoisPostaux($this->getEnabledHabilitation('gestion_fournisseurs_envois_postaux'));


            $invitePermanentEntiteDependante = $this->getEnabledHabilitation('invite_permanent_entite_dependante');
            $invitePermanentMonEntite = $this->getEnabledHabilitation('invite_permanent_mon_entite');
            $invitePermanentTransverse = $this->getEnabledHabilitation('invite_permanent_transverse');
            if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')
                && $invitePermanentMonEntite === '1'
                && Atexo_Module::isEnabled('OrganisationCentralisee')) {
                $invitePermanentEntiteDependante = '1';
                $invitePermanentTransverse = '0';
            } elseif (Atexo_Config::getParameter('SPECIFIQUE_PLACE')
                && $invitePermanentMonEntite === '0'
                && !Atexo_Module::isEnabled('OrganisationCentralisee')) {
                $invitePermanentEntiteDependante = '0';
                $invitePermanentTransverse = '0';
            }

            $habilitationsAgent->setInvitePermanentEntiteDependante($invitePermanentEntiteDependante);
            $habilitationsAgent->setInvitePermanentMonEntite($invitePermanentMonEntite);
            $habilitationsAgent->setInvitePermanentTransverse($invitePermanentTransverse);

            $habilitationsAgent->setRefuserEnveloppe($this->getEnabledHabilitation('refuser_enveloppe'));
            $habilitationsAgent->setGestionCompteJal($this->getEnabledHabilitation('gestion_compte_jal'));
            $habilitationsAgent->setGestionAdressesFacturationJal($this->getEnabledHabilitation('gestion_facturation_jal'));
            $habilitationsAgent->setGererAdmissibilite($this->getEnabledHabilitation('gerer_admissibilite'));
            $habilitationsAgent->setOuvrirAnonymatADistance($this->getEnabledHabilitation('ouvrir_anonymat_a_distance'));
            $habilitationsAgent->setRestaurerEnveloppe($this->getEnabledHabilitation('restaurer_enveloppe'));
            if (Atexo_Module::isEnabled('CentralePublication')) {
                $habilitationsAgent->setGestionCentralePub($this->getEnabledHabilitation('gestion_centrale_pub'));
            }
            $habilitationsAgent->setGestionCompteGroupeMoniteur($this->getEnabledHabilitation('gestion_groupe_moniteur'));
            $habilitationsAgent->setImporterEnveloppe($this->getEnabledHabilitation('importer_enveloppe'));
            $habilitationsAgent->setOuvrirOffreTechniqueEnLigne($this->getEnabledHabilitation('ouvrir_offre_technique_en_ligne'));
            $habilitationsAgent->setOuvrirOffreTechniqueADistance($this->getEnabledHabilitation('ouvrir_offre_technique_a_distance'));
            $habilitationsAgent->setOuvrirOffreTechniqueHorsLigne($this->getEnabledHabilitation('ouvrir_offre_technique_hors_ligne'));
            $habilitationsAgent->setSuiviSeulRegistreRetraitsPapier($this->getEnabledHabilitation('suivi_seul_registre_retraits_papier'));
            $habilitationsAgent->setSuiviSeulRegistreQuestionsPapier($this->getEnabledHabilitation('suivi_seul_registre_questions_papier'));
            $habilitationsAgent->setSuiviSeulRegistreDepotsPapier($this->getEnabledHabilitation('suivi_seul_registre_depots_papier'));
            $habilitationsAgent->setSuiviSeulRegistreRetraitsElectronique($this->getEnabledHabilitation('suivi_seul_registre_retraits_papier'));
            $habilitationsAgent->setSuiviSeulRegistreQuestionsElectronique($this->getEnabledHabilitation('suivi_seul_registre_questions_papier'));
            $habilitationsAgent->setSuiviSeulRegistreDepotsElectronique($this->getEnabledHabilitation('suivi_seul_registre_depots_papier'));
            $habilitationsAgent->setResultatAnalyse($this->getEnabledHabilitation('resultat_analyse'));
            $habilitationsAgent->setModifierConsultationMapaInferieurMontantApresValidation($this->getEnabledHabilitation('modifier_consultation_mapa_inferieur_montant_apres_validation'));
            $habilitationsAgent->setModifierConsultationMapaSuperieurMontantApresValidation($this->getEnabledHabilitation('modifier_consultation_mapa_superieur_montant_apres_validation'));
            $habilitationsAgent->setModifierConsultationProceduresFormaliseesApresValidation($this->getEnabledHabilitation('modifier_consultation_procedures_formalisees_apres_validation'));
            $habilitationsAgent->setSuiviEntreprise($this->getEnabledHabilitation('suivi_entreprise_bdf'));
            $habilitationsAgent->setGererAdressesService($this->getEnabledHabilitation('gerer_adresses_service'));
            $habilitationsAgent->setGererLesEntreprises($this->getEnabledHabilitation('gerer_les_entreprises'));
            $habilitationsAgent->setAnnuaireAcheteur($this->getEnabledHabilitation('annuaire_acheteur'));
            $habilitationsAgent->setModifierConsultationProceduresFormaliseesApresValidation($this->getEnabledHabilitation('modifier_consultation_procedures_formalisees_apres_validation'));
            $habilitationsAgent->setPorteeSocietesExclues($this->getEnabledHabilitation('portee_societes_exclues'));
            $habilitationsAgent->setPorteeSocietesExcluesTousOrganismes($this->getEnabledHabilitation('portee_societes_exclues_tous_organismes'));
            $habilitationsAgent->setModifierSocietesExclues($this->getEnabledHabilitation('modifier_societes_exclues'));
            $habilitationsAgent->setSupprimerSocietesExclues($this->getEnabledHabilitation('supprimer_societes_exclues'));
            $habilitationsAgent->setGererMonService($this->getEnabledHabilitation('gerer_mon_service'));
            $habilitationsAgent->setCreerAnnonceExtraitPv($this->getEnabledHabilitation('creer_annonce_extrait_pv'));
            $habilitationsAgent->setCreerAnnonceRapportAchevement($this->getEnabledHabilitation('creer_annonce_rapport_achevement'));
            $habilitationsAgent->setCreerAnnonceDecisionResiliation($this->getEnabledHabilitation('creer_annonce_decision_resiliation'));
            $habilitationsAgent->setGestionCertificatsAgent($this->getEnabledHabilitation('gestion_certificats_agent'));
            $habilitationsAgent->setGestionMandataire($this->getEnabledHabilitation('gestion_mandataire'));
            $habilitationsAgent->setCreerAvisProgrammePrevisionnel($this->getEnabledHabilitation('creer_avis_programme_previsionnel'));
            $habilitationsAgent->setCreerAnnonceSyntheseRapportAudit($this->getEnabledHabilitation('creer_avis_synthese_rapport_audit'));
            $habilitationsAgent->setAnnulerConsultation($this->getEnabledHabilitation('annuler_consultation'));
            $habilitationsAgent->setEnvoyerPublicite($this->getEnabledHabilitation('envoyer_publicite'));
            $habilitationsAgent->setListeMarchesNotifies($this->getEnabledHabilitation('liste_marches_notifies'));
            $habilitationsAgent->setEnvoyerMessage($this->getEnabledHabilitation('envoyer_message'));
            $habilitationsAgent->setSuivreMessage($this->getEnabledHabilitation('suivre_message'));
            $habilitationsAgent->setSuiviFluxChorusTransversal($this->getEnabledHabilitation('suivi_flux_chorus_transversal'));
            $habilitationsAgent->setGererNewsletter($this->getEnabledHabilitation('gerer_newsletter'));
            $habilitationsAgent->setGererNewsletterRedac($this->getEnabledHabilitation('gerer_newsletter_redac'));
            $habilitationsAgent->setGestionMiseDispositionPiecesMarche($this->getEnabledHabilitation('mise_a_disposition'));
            $habilitationsAgent->setEspaceCollaboratifGestionnaire($this->getEnabledHabilitation('espaceCollaboratifGestionnaire'));
            $habilitationsAgent->setEspaceCollaboratifContributeur($this->getEnabledHabilitation('espaceCollaboratifContributeur'));
            if ($this->isProfilRmaVisible()) {
                $habilitationsAgent->setProfilRma($this->getEnabledHabilitation('profilRMA'));
            }
            $habilitationsAgent->setHistoriqueNavigationInscrits($this->getEnabledHabilitation('historiqueEntreprises'));
            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                $habilitationsAgent->setRedactionDocumentsRedac($this->getEnabledHabilitation('redaction_documents_redac'));
                $habilitationsAgent->setValidationDocumentsRedac($this->getEnabledHabilitation('validation_documents_redac'));
                $habilitationsAgent->setReprendreIntegralementArticle($this->getEnabledHabilitation('reprendre_Integralement_Article'));
                $habilitationsAgent->setAdministrerClausesEntiteAchats($this->getEnabledHabilitation('administer_clauses_entite_achat'));
                $habilitationsAgent->setGenererPiecesFormatOdt($this->getEnabledHabilitation('generer_pieces_format'));
                $habilitationsAgent->setGererGabaritEntiteAchats($this->getEnabledHabilitation('gerer_gabarit_entite_achats'));
                $habilitationsAgent->setGererGabaritAgent($this->getEnabledHabilitation('gerer_gabarit_agent'));
            }
            if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                $habilitationsAgent->setGererReouverturesModification($this->getEnabledHabilitation('reouverture_modification'));
            }
            if (Atexo_Module::isEnabled('gestionOperations', Atexo_CurrentUser::getOrganismAcronym())) {
                $habilitationsAgent->setGererOperations($this->getEnabledHabilitation('GererOperations'));
            }
            $habilitationsAgent->setCreerContrat($this->getEnabledHabilitation('check_creer_contrat'));
            $habilitationsAgent->setModifierContrat($this->getEnabledHabilitation('check_gestion_contrat'));
            $habilitationsAgent->setSupprimerContrat($this->getEnabledHabilitation('check_supprimer_contrat'));
            $habilitationsAgent->setConsulterContrat($this->getEnabledHabilitation('check_consulter_contrat'));
            $habilitationsAgent->setExecVoirContratsEa($this->getEnabledHabilitation('check_exec_voir_contrats_ea'));
            $habilitationsAgent->setExecVoirContratsEaDependantes($this->getEnabledHabilitation('check_exec_voir_contrats_ea_dependantes'));
            $habilitationsAgent->setExecVoirContratsOrganisme($this->getEnabledHabilitation('check_exec_voir_contrats_organisme'));

            if (Atexo_Module::isEnabled('EspaceDocumentaire')) {
                $habilitationsAgent->setEspaceDocumentaireConsultation($this->getEnabledHabilitation('espace_documentaire_consultation'));
            }

            if (Atexo_Module::isEnabled('EchangesDocuments', Atexo_CurrentUser::getOrganismAcronym())) {
                $habilitationsAgent->setAccesEchangeDocumentaire($this->getEnabledHabilitation('acces_echange_documentaire'));
            }

            $habilitationsAgent->setRattachementService($this->getEnabledHabilitation('rattachementService'));

            $habilitationsAgent = $this->setHabilitationModuleRecensementProgrammation($habilitationsAgent);
            $habilitationsAgent->setGestionSpaserConsultations($this->getEnabledHabilitation('spaserConsultation'));

            $habilitationsAgent->save($this->connexion);

            if ('0' == $this->getEnabledHabilitation('invite_permanent_transverse')) {
                $this->clearInvitePermanentTransverse();
            } elseif ('1' == $this->getEnabledHabilitation('invite_permanent_transverse')) {
                $this->setData();
            }

            $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }

    /**
     * coche ou decoche une habilitation.
     */
    public function checkOrUncheckHabilitation($habilitation, $element)
    {
        if ('1' == $habilitation) {
            $this->$element->checked = true;
        } else {
            $this->$element->checked = false;
        }
        $this->$element->Enabled = true;

        if (true == $this->readOnly) {
            $this->$element->Enabled = false;
        }
    }

    /**
     * retourne etat de l'habilitation activee / desactivee.
     */
    public function getEnabledHabilitation($element)
    {
        if ($this->$element->checked) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * retourne les informations relatives à l'agent.
     */
    public function getInfosAgent()
    {
        return (new CommonAgentQuery())->findOneById($this->idAgent);
    }

    /*
     * $stylePrevTr: accepte les valeurs groupeLine ou ''
     * $visibilityCurrentTr : pour definir la visibilite de tr courant
     * $isGroupLine: si c'est un titre de groupe (true) ou une simple ligne (false)
     */
    public function getCurrentTrStyle($stylePrevTr, $visibilityCurrentTr, $isGroupLine = false)
    {
        if ($visibilityCurrentTr) {//si le tr courant est visible
            $this->previousVisibility = $visibilityCurrentTr;

            if ($stylePrevTr) {
                $this->previousStyle = $stylePrevTr;

                return $stylePrevTr;
            }
            if ('on' == $this->previousStyle || 'groupLine' == $this->previousStyle) {
                $this->previousStyle = '';

                return '';
            } else {
                if ($isGroupLine) {
                    $this->previousStyle = 'groupLine';

                    return 'groupLine';
                } else {
                    $this->previousStyle = 'on';

                    return 'on';
                }
            }
        }
    }

    public function isProfilRmaVisible()
    {
        if (null === $this->profilRmaVisible) {
            $this->profilRmaVisible = Atexo_Agent_VisionRma::isProfilRmaVisible();
        }

        return $this->profilRmaVisible;
    }

    /**
     * @param $organisme
     */
    public function displayInfosOrganisme($organisme)
    {
        (new Atexo_EntityPurchase())->reloadCachedEntities($organisme);
        $atexoOrganisme = new Atexo_Organismes();
        $organismeO = $atexoOrganisme->retrieveOrganismeByAcronyme($organisme);
        $this->sigleorganisme->Text = $organismeO->getSigle();
        $this->libelleorganisme->Text = $organismeO->getDenominationOrg();
    }

    /**
     * @param $organisme
     * @param null $organismeUser
     *
     * @return string
     */
    public function displayEntitiesTree($organisme, $organismeUser = null)
    {
        $onClick = null;
        $id = null;
        $style = null;
        $commonInviteTransverse = (new CommonInvitePermanentTransverseQuery())->getSelectedService($_GET['id']);

        $allEntities = [];
        Atexo_EntityPurchase::getAllChilds(0, $allEntities, $organisme);

        $htmlStr = '<ul style="display:block" id=\'all-cats\'>';
        $nbAllEntities = is_countable($allEntities) ? count($allEntities) : 0;
        for ($i = 0; $i < $nbAllEntities; ++$i) {
            $curService = $allEntities[$i];
            $curDepth = $curService->getDepth();

            $selecte = $this->selectedInviteTransverse($commonInviteTransverse, $organisme, $curService);

            $idRandom = $this->generateRandomString();

            if ($i < $nbAllEntities - 1) { // Ce n'est pas le dernier élément
                $nextService = $allEntities[$i + 1];
                $nextDepth = $nextService->getDepth();

                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $picto = 'picto-plus.gif';
                    $id = 'id=sous-cat_' . $i;
                    $style = 'style="display:none"';
                    $onClick = ' onclick="openSousCat(this,\'sous-cat_' . $i . '\')" ';
                } else {
                    $picto = 'picto-tiret.gif';
                }

                $string = $organisme . '_' . $curService->getId() . '_' . $_GET['id'];
                $base64 = base64_encode($string);

                $htmlStr .= '<li >
                    <img ' . $onClick . ' src="' . $this->themesPath() . '/images/' . $picto . '" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" />'
                    /* Code a ne pas supprimer ! A remettre dans le cas du select ALL
                     onclick="selectOrg(\''.$idRandom.'\', \''.$base64.'\')"
                     */
                    . '<input type="checkbox"  
                    data-selected="' . $base64 . '"  
                    data-children="sous-cat_' . $i . '" 
                    id="' . $idRandom . '" 
                    name="service[]"
                    value="' . $base64 . '"
                    class="checkOrg" ' . $selecte . '>'

                    . '<strong>'
                    . $curService->getSigle() . '</strong>' . ' - ' . $curService->getLibelle() . '';

                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $htmlStr .= '<ul ' . $id . ' ' . $style . '>';
                }

                if ($curDepth > $nextDepth) { // le prochain n'est ni un fils ni un parent (fin d'une branche)
                    $differenceDepth = $curDepth - $nextDepth;
                    for ($j = 0; $j < $differenceDepth; ++$j) {
                        $htmlStr .= '</li></ul>';
                    }
                }
                if ($curDepth == $nextDepth) { //le prochain est un frère, on ferme le li
                    $htmlStr .= '</li>';
                }
            } else { // C'est le dernier élément
                $string = $organisme . '_' . $curService->getId() . '_' . $_GET['id'];
                $base64 = base64_encode($string);
                $htmlStr .= '<li><img src="' . $this->themesPath() . '/images/picto-tiret.gif" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" />'
                /* Code a ne pas supprimer ! A remettre dans le cas du select ALL
                 onclick="selectOrg(\''.$idRandom.'\', \''.$base64.'\')"
                 */
                . ' <input type="checkbox" 
                    data-selected="' . $base64 . '" data-children="sous-cat_' . $i . '" 
                    id="' . $idRandom . '" name="service[]" 
                    value="' . $base64 . '" ' . $selecte . '>'
                . '<strong>'
                . $curService->getSigle()
                . '</strong>'
                . ' - '
                . $curService->getLibelle()
                ;
                $htmlStr .= '</li>';
                for ($j = 0; $j < ($curDepth); ++$j) {
                    $htmlStr .= "</ul>\n";
                }
            }
        }

        $htmlStr .= '';

        return $htmlStr;
    }

    public function onCallBackOrganismeSelected($sender, $param, $selectedOrganisme)
    {
        $organismeUser = Atexo_CurrentUser::getCurrentOrganism();
        $this->displayInfosOrganisme($selectedOrganisme);
        $this->TreeServices->Text = $this->displayEntitiesTree($selectedOrganisme, $organismeUser);
        $this->panelRefreshTreeServices->render($param->NewWriter);
    }

    /**
     * @param $commonTInviteTransverse
     * @param $commonOrganisme
     * @param $curService
     *
     * @return string
     */
    protected function selectedInviteTransverse($commonInviteTransverse, $organisme, $curService)
    {
        $selected = '';
        foreach ($commonInviteTransverse as $invite) {
            if (
                $organisme == $invite['acronyme'] &&
                $curService->getId() == $invite['serviceId'] &&
                $_GET['id'] == $invite['agentId']
            ) {
                $selected = "checked='checked'";
            }
        }

        return $selected;
    }

    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    private function setData()
    {
        $this->clearInvitePermanentTransverse();
        foreach ($_POST['service'] as $post) {
            $data = explode('_', base64_decode($post));
            if (3 === count($data)) {
                $serviceId = null;
                if ('0' != $data[1]) {
                    $serviceId = $data[1];
                }
                $commonInvitePermanentTransverse = new CommonInvitePermanentTransverse();
                $commonInvitePermanentTransverse->setAgentId((int) $data[2]);
                $commonInvitePermanentTransverse->setAcronyme($data[0]);
                $commonInvitePermanentTransverse->setServiceId($serviceId);
                $commonInvitePermanentTransverse->save();
            } else {
                throw new THttpException(403, 'Donnees incompletes');
            }
        }
    }

    private function clearInvitePermanentTransverse()
    {
        $commonInvitePermanentTransverseQuery = new CommonInvitePermanentTransverseQuery();
        $invitePermanentTransverse = $commonInvitePermanentTransverseQuery->findByArray([
            'agentId' => $_GET['id'],
            'acronyme' => Atexo_CurrentUser::getCurrentOrganism(),
        ]);

        $invitePermanentTransverse->delete();
    }
}
