<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;

/**
 * Classe StatistiquesMapa.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesMapa extends MpeTPage
{
    public bool $visible = true;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['allOrgs'])) {
            $this->visible = false;
        }

        if (!$this->IsPostBack) {
            // remplissage de la liste des entités
            $this->remplirListEntiteAssociee();
            $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
            $this->onSearchClick();
        }
    }

    public function remplirListEntiteAssociee()
    {
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        $this->entiteeAssociee->Datasource = $entities;
        $this->entiteeAssociee->dataBind();
        $this->entiteeAssociee->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    public function onSearchClick()
    {
        $resultsTravaux1 = 0;
        $resultsTravaux2 = 0;
        $resultsTravaux3 = 0;
        $resultsFournitures1 = 0;
        $resultsFournitures2 = 0;
        $resultsFournitures3 = 0;
        $resultsServices1 = 0;
        $resultsServices2 = 0;
        $resultsServices3 = 0;
        $resultsRetraitsTravaux1 = 0;
        $resultsRetraitsTravaux2 = 0;
        $resultsRetraitsTravaux3 = 0;
        $resultsRetraitsFournitures1 = 0;
        $resultsRetraitsFournitures2 = 0;
        $resultsRetraitsFournitures3 = 0;
        $resultsRetraitsServices1 = 0;
        $resultsRetraitsServices2 = 0;
        $resultsRetraitsServices3 = 0;
        $resultsOffresTravaux1 = 0;
        $resultsOffresTravaux2 = 0;
        $resultsOffresTravaux3 = 0;
        $resultsOffresFournitures1 = 0;
        $resultsOffresFournitures2 = 0;
        $resultsOffresFournitures3 = 0;
        $resultsOffresServices1 = 0;
        $resultsOffresServices2 = 0;
        $resultsOffresServices3 = 0;

        if (!isset($_GET['allOrgs'])) {
            /*
         * Critere de la recherche
         */
            $atexoOrg = Atexo_CurrentUser::getCurrentOrganism();
            $criteria = new Atexo_Statistiques_CriteriaVo();

            $service = 'IN';
            if ($this->cumulValeurs->Checked) {
                $criteria->setValeurCumule(true);
                if (0 != $this->entiteeAssociee->SelectedValue) {
                    $service .= Atexo_EntityPurchase::retrieveAllChildrenServices($this->entiteeAssociee->SelectedValue, $atexoOrg);
                } else {
                    $i = 1;
                    $service .= '(';
                    $entities = Atexo_EntityPurchase::getEntityPurchase($atexoOrg, true);
                    $nbr = count($entities);
                    $value = '';
                    foreach ($entities as $key => $value) {
                        if ($i != $nbr) {
                            $service .= $key.',';
                        } else {
                            $service .= $key;
                        }

                        ++$i;
                    }

                    $service .= ')';
                }
            } else {
                $service .= '('.$this->entiteeAssociee->SelectedValue.')';
            }

            $criteria->setSelectedEntity($this->entiteeAssociee->SelectedValue);

            $criteria->setIdService($service);
            $criteria->setOrganisme($atexoOrg);
            if ('' != $this->critereFiltre_1_dateStart->Text) {
                $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
            }
            if ('' != $this->critereFiltre_1_dateEnd->Text) {
                $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
            }

            $this->setViewState('criteria', $criteria);

            /*
             * Resultat de la recherche pour procédure mise en ligne
            */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                        Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);
                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

            /*
             * Resultat de la recherche pour les retraits
             */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                    Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsRetraitsTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsRetraitsTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsRetraitsTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsRetraitsFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsRetraitsFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsRetraitsFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsRetraitsServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsRetraitsServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsRetraitsServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

            /*
             * Resultat de la recherche pour les retraits
             */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                        Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsOffresTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsOffresTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsOffresTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsOffresFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsOffresFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsOffresFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsOffresServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsOffresServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsOffresServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
        } else {
            $atexoOrgs = Atexo_Organismes::retrieveOrganismes();
            /*
             * Critere de la recherche
             */
            $criteria = new Atexo_Statistiques_CriteriaVo();
            if ('' != $this->critereFiltre_1_dateStart->Text) {
                $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
            }
            if ('' != $this->critereFiltre_1_dateEnd->Text) {
                $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
            }

            $this->setViewState('criteria', $criteria);

            if (is_array($atexoOrgs) && 0 != count($atexoOrgs)) {
                foreach ($atexoOrgs as $atexoOrg) {
                    $criteria->setOrganisme($atexoOrg->getAcronyme());

                    /*
                     * Resultat de la recherche pour procédure mise en ligne
                     */
                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                                Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

                    /*
                     * Resultat de la recherche pour les retraits
                     */

                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'), Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'),
                                Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsRetraitsTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsRetraitsTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsRetraitsTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsRetraitsFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsRetraitsFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsRetraitsFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsRetraitsServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsRetraitsServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsRetraitsServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

                    /*
                     * Resultat de la recherche pour les retraits
                     */

                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                                Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsOffresTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsOffresTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsOffresTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsOffresFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsOffresFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsOffresFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsOffresServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsOffresServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsOffresServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

                    Atexo_Db::closeOrganism($atexoOrg->getAcronyme());
                }
            }
        }

        /*
         * remplissage des champs
         */

        /*-- Procédure mises en ligne --*/
        //travaux
        $this->proceduretravauxsansreponse->Text = number_format($resultsTravaux1, '0', ',', ' ');
        $this->proceduretravauxavecreponsesanssignature->Text = number_format($resultsTravaux2, '0', ',', ' ');
        $this->proceduretravauxavecreponseavecsignature->Text = number_format($resultsTravaux3, '0', ',', ' ');
        $this->totalproceduretravaux->Text = number_format($resultsTravaux1 + $resultsTravaux2 + $resultsTravaux3, '0', ',', ' ');
        //fournitures
        $this->procedurefournituressansreponse->Text = number_format($resultsFournitures1, '0', ',', ' ');
        $this->procedurefournituresavecreponsesanssignature->Text = number_format($resultsFournitures2, '0', ',', ' ');
        $this->procedurefournituresavecreponseavecsignature->Text = number_format($resultsFournitures3, '0', ',', ' ');
        $this->totalprocedurefournitures->Text = number_format($resultsFournitures1 + $resultsFournitures2 + $resultsFournitures3, '0', ',', ' ');
        //services
        $this->procedureservicessansreponse->Text = number_format($resultsServices1, '0', ',', ' ');
        $this->procedureservicesavecreponsesanssignature->Text = number_format($resultsServices2, '0', ',', ' ');
        $this->procedureservicesavecreponseavecsignature->Text = number_format($resultsServices3, '0', ',', ' ');
        $this->totalprocedureservices->Text = number_format($resultsServices1 + $resultsServices2 + $resultsServices3, '0', ',', ' ');

        //sous total
        $this->soustotalConsSanReponse->Text = number_format(($resultsTravaux1 + $resultsFournitures1 + $resultsServices1), '0', ',', ' ');
        $this->soustotalConsSanSignature->Text = number_format(($resultsTravaux2 + $resultsFournitures2 + $resultsServices2), '0', ',', ' ');
        $this->soustotalConsAvecSignature->Text = number_format(($resultsTravaux3 + $resultsFournitures3 + $resultsServices3), '0', ',', ' ');
        $this->sousTotalConsAdaptee->Text = number_format(($resultsTravaux1 + $resultsFournitures1 + $resultsServices1) +
                                    ($resultsTravaux2 + $resultsFournitures2 + $resultsServices2) + ($resultsTravaux3 + $resultsFournitures3 + $resultsServices3), '0', ',', ' ');

        /*-- Retraits --*/
        //travaux
        $this->retraittravauxsansreponse->Text = number_format($resultsRetraitsTravaux1, '0', ',', ' ');
        $this->retraittravauxavecreponsesanssignature->Text = number_format($resultsRetraitsTravaux2, '0', ',', ' ');
        $this->retraittravauxavecreponseavecsignature->Text = number_format($resultsRetraitsTravaux3, '0', ',', ' ');
        $this->totalretraittravaux->Text = number_format($resultsRetraitsTravaux1 + $resultsRetraitsTravaux2 + $resultsRetraitsTravaux3, '0', ',', ' ');
        //fournitures
        $this->retraitfournituressansreponse->Text = number_format($resultsRetraitsFournitures1, '0', ',', ' ');
        $this->retraitfournituresavecreponsesanssignature->Text = number_format($resultsRetraitsFournitures2, '0', ',', ' ');
        $this->retraitfournituresavecreponseavecsignature->Text = number_format($resultsRetraitsFournitures3, '0', ',', ' ');
        $this->totalretraitfournitures->Text = number_format($resultsRetraitsFournitures1 + $resultsRetraitsFournitures2 + $resultsRetraitsFournitures3, '0', ',', ' ');
        //services
        $this->retraitservicessansreponse->Text = number_format($resultsRetraitsServices1, '0', ',', ' ');
        $this->retraitservicesavecreponsesanssignature->Text = number_format($resultsRetraitsServices2, '0', ',', ' ');
        $this->retraitservicesavecreponseavecsignature->Text = number_format($resultsRetraitsServices3, '0', ',', ' ');
        $this->totalretraitservices->Text = number_format($resultsRetraitsServices1 + $resultsRetraitsServices2 + $resultsRetraitsServices3, '0', ',', ' ');
        //sous total
        $this->soustotalRetraitsSanReponse->Text = number_format(($resultsRetraitsTravaux1 + $resultsRetraitsFournitures1 + $resultsRetraitsServices1), '0', ',', ' ');
        $this->soustotalRetraitsSanSignature->Text = number_format(($resultsRetraitsTravaux2 + $resultsRetraitsFournitures2 + $resultsRetraitsServices2), '0', ',', ' ');
        $this->soustotalRetraitsAvecSignature->Text = number_format(($resultsRetraitsTravaux3 + $resultsRetraitsFournitures3 + $resultsRetraitsServices3), '0', ',', ' ');
        $this->sousTotalRetraitsAdaptee->Text = number_format(($resultsRetraitsTravaux1 + $resultsRetraitsFournitures1 + $resultsRetraitsServices1) +
                ($resultsRetraitsTravaux2 + $resultsRetraitsFournitures2 + $resultsRetraitsServices2) + ($resultsRetraitsTravaux3 + $resultsRetraitsFournitures3 + $resultsRetraitsServices3), '0', ',', ' ');

        /*-- Offres --*/
        //travaux
        $this->offretravauxsansreponse->Text = number_format($resultsOffresTravaux1, '0', ',', ' ');
        $this->offretravauxavecreponsesanssignature->Text = number_format($resultsOffresTravaux2, '0', ',', ' ');
        $this->offretravauxavecreponseavecsignature->Text = number_format($resultsOffresTravaux3, '0', ',', ' ');
        $this->totaloffretravaux->Text = number_format($resultsOffresTravaux1 + $resultsOffresTravaux2 + $resultsOffresTravaux3, '0', ',', ' ');
        //fournitures
        $this->offrefournituressansreponse->Text = number_format($resultsOffresFournitures1, '0', ',', ' ');
        $this->offrefournituresavecreponsesanssignature->Text = number_format($resultsOffresFournitures2, '0', ',', ' ');
        $this->offrefournituresavecreponseavecsignature->Text = number_format($resultsOffresFournitures3, '0', ',', ' ');
        $this->totaloffrefournitures->Text = number_format($resultsOffresFournitures1 + $resultsOffresFournitures2 + $resultsOffresFournitures3, '0', ',', ' ');
        //services
        $this->offreservicessansreponse->Text = number_format($resultsOffresServices1, '0', ',', ' ');
        $this->offreservicesavecreponsesanssignature->Text = number_format($resultsOffresServices2, '0', ',', ' ');
        $this->offreservicesavecreponseavecsignature->Text = number_format($resultsOffresServices3, '0', ',', ' ');
        $this->totaloffreservices->Text = number_format($resultsOffresServices1 + $resultsOffresServices2 + $resultsOffresServices3, '0', ',', ' ');
        //sous total
        $this->soustotalDepotSanReponse->Text = number_format(($resultsOffresTravaux1 + $resultsOffresFournitures1 + $resultsOffresServices1), '0', ',', ' ');
        $this->soustotalDepotSanSignature->Text = number_format(($resultsOffresTravaux2 + $resultsOffresFournitures2 + $resultsOffresServices2), '0', ',', ' ');
        $this->soustotalDepotAvecSignature->Text = number_format(($resultsOffresTravaux3 + $resultsOffresFournitures3 + $resultsOffresServices3), '0', ',', ' ');
        $this->sousTotalDepotAdaptee->Text = number_format(($resultsOffresTravaux1 + $resultsOffresFournitures1 + $resultsOffresServices1) +
            ($resultsOffresTravaux2 + $resultsOffresFournitures2 + $resultsOffresServices2) + ($resultsOffresTravaux3 + $resultsOffresFournitures3 + $resultsOffresServices3), '0', ',', ' ');
    }

    public function onEffaceClick()
    {
        $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
        $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
        $this->cumulValeurs->Checked = false;
    }

    public function genererExcel()
    {
        $dataStatistique = [];
        $resultsTravaux1 = null;
        $resultsFournitures1 = null;
        $resultsServices1 = null;
        $resultsTravaux2 = null;
        $resultsFournitures2 = null;
        $resultsServices2 = null;
        $resultsTravaux3 = null;
        $resultsFournitures3 = null;
        $resultsServices3 = null;
        $resultsRetraitsTravaux1 = null;
        $resultsRetraitsFournitures1 = null;
        $resultsRetraitsServices1 = null;
        $resultsRetraitsTravaux2 = null;
        $resultsRetraitsFournitures2 = null;
        $resultsRetraitsServices2 = null;
        $resultsRetraitsTravaux3 = null;
        $resultsRetraitsFournitures3 = null;
        $resultsRetraitsServices3 = null;
        $resultsOffresTravaux1 = null;
        $resultsOffresFournitures1 = null;
        $resultsOffresServices1 = null;
        $resultsOffresTravaux2 = null;
        $resultsOffresFournitures2 = null;
        $resultsOffresServices2 = null;
        $resultsOffresTravaux3 = null;
        $resultsOffresFournitures3 = null;
        $resultsOffresServices3 = null;
        $valeurCumule = null;
        $entiteName = null;
        $atexoOrg = Atexo_CurrentUser::getCurrentOrganism();

        $criteria = $this->getViewState('criteria');

        if (!isset($_GET['allOrgs'])) {
            /*
         * Resultat de la recherche pour procédure mise en ligne
            */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                    Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);

                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

            /*
             * Resultat de la recherche pour les retraits
             */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                        Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsRetraitsTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsRetraitsTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsRetraitsTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsRetraitsFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsRetraitsFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsRetraitsFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsRetraitsServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsRetraitsServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsRetraitsServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

            /*
             * Resultat de la recherche pour les retraits
             */

            foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                    Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(false);
                $criteria->setReponsesAvecSignature(null);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(false);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                $criteria->setCategorieConsultation($categorie);
                $criteria->setReponsesAutotisee(true);
                $criteria->setReponsesAvecSignature(true);
                $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false);
                $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
            }

            $resultsOffresTravaux1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsOffresTravaux2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
            $resultsOffresTravaux3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

            $resultsOffresFournitures1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsOffresFournitures2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
            $resultsOffresFournitures3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

            $resultsOffresServices1 = $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsOffresServices2 = $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
            $resultsOffresServices3 = $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
        } else {
            $atexoOrgs = Atexo_Organismes::retrieveOrganismes();
            /*
             * Critere de la recherche
             */

            if (is_array($atexoOrgs) && 0 != count($atexoOrgs)) {
                foreach ($atexoOrgs as $atexoOrg) {
                    $criteria->setOrganisme($atexoOrg->getAcronyme());

                    /*
                     * Resultat de la recherche pour procédure mise en ligne
                     */
                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                            Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

                    /*
                     * Resultat de la recherche pour les retraits
                     */

                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                                    Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsRetraitsTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsRetraitsTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsRetraitsTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsRetraitsFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsRetraitsFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsRetraitsFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsRetraitsServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsRetraitsServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsRetraitsServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];

                    /*
                     * Resultat de la recherche pour les retraits
                     */

                    foreach ([Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
                            Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'), Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'), ] as $categorie) {
                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(false);
                        $criteria->setReponsesAvecSignature(null);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results1[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(false);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results2[$categorie] = $proceduresEnLigneTravaux['totalMapa'];

                        $criteria->setCategorieConsultation($categorie);
                        $criteria->setReponsesAutotisee(true);
                        $criteria->setReponsesAvecSignature(true);
                        $proceduresEnLigneTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, true);
                        $results3[$categorie] = $proceduresEnLigneTravaux['totalMapa'];
                    }

                    $resultsOffresTravaux1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsOffresTravaux2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
                    $resultsOffresTravaux3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];

                    $resultsOffresFournitures1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsOffresFournitures2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
                    $resultsOffresFournitures3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];

                    $resultsOffresServices1 += $results1[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsOffresServices2 += $results2[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                    $resultsOffresServices3 += $results3[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
                }
            }
        }

        $dataStatistique['procedureMiseEnLigne']['travaux']['sans_reponse'] = $resultsTravaux1;
        $dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_sans_signature'] = $resultsTravaux2;
        $dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_avec_signature'] = $resultsTravaux3;

        $dataStatistique['procedureMiseEnLigne']['fournitures']['sans_reponse'] = $resultsFournitures1;
        $dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_sans_signature'] = $resultsFournitures2;
        $dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_avec_signature'] = $resultsFournitures3;

        $dataStatistique['procedureMiseEnLigne']['services']['sans_reponse'] = $resultsServices1;
        $dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_sans_signature'] = $resultsServices2;
        $dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_avec_signature'] = $resultsServices3;

        $dataStatistique['retraits']['travaux']['sans_reponse'] = $resultsRetraitsTravaux1;
        $dataStatistique['retraits']['travaux']['avec_reponses_sans_signature'] = $resultsRetraitsTravaux2;
        $dataStatistique['retraits']['travaux']['avec_reponses_avec_signature'] = $resultsRetraitsTravaux3;

        $dataStatistique['retraits']['fournitures']['sans_reponse'] = $resultsRetraitsFournitures1;
        $dataStatistique['retraits']['fournitures']['avec_reponses_sans_signature'] = $resultsRetraitsFournitures2;
        $dataStatistique['retraits']['fournitures']['avec_reponses_avec_signature'] = $resultsRetraitsFournitures3;

        $dataStatistique['retraits']['services']['sans_reponse'] = $resultsRetraitsServices1;
        $dataStatistique['retraits']['services']['avec_reponses_sans_signature'] = $resultsRetraitsServices2;
        $dataStatistique['retraits']['services']['avec_reponses_avec_signature'] = $resultsRetraitsServices3;

        $dataStatistique['offres']['travaux']['sans_reponse'] = $resultsOffresTravaux1;
        $dataStatistique['offres']['travaux']['avec_reponses_sans_signature'] = $resultsOffresTravaux2;
        $dataStatistique['offres']['travaux']['avec_reponses_avec_signature'] = $resultsOffresTravaux3;

        $dataStatistique['offres']['fournitures']['sans_reponse'] = $resultsOffresFournitures1;
        $dataStatistique['offres']['fournitures']['avec_reponses_sans_signature'] = $resultsOffresFournitures2;
        $dataStatistique['offres']['fournitures']['avec_reponses_avec_signature'] = $resultsOffresFournitures3;

        $dataStatistique['offres']['services']['sans_reponse'] = $resultsOffresServices1;
        $dataStatistique['offres']['services']['avec_reponses_sans_signature'] = $resultsOffresServices2;
        $dataStatistique['offres']['services']['avec_reponses_avec_signature'] = $resultsOffresServices3;

        //sous total
        $dataStatistique['sousTotal']['procedureMiseEnLigne']['sans_reponse'] = $resultsTravaux1 + $resultsFournitures1 + $resultsServices1;
        $dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_sans_signature'] = $resultsTravaux2 + $resultsFournitures2 + $resultsServices2;
        $dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_avec_signature'] = $resultsTravaux3 + $resultsFournitures3 + $resultsServices3;

        $dataStatistique['sousTotal']['retraits']['sans_reponse'] = $resultsRetraitsTravaux1 + $resultsRetraitsFournitures1 + $resultsRetraitsServices1;
        $dataStatistique['sousTotal']['retraits']['avec_reponses_sans_signature'] = $resultsRetraitsTravaux2 + $resultsRetraitsFournitures2 + $resultsRetraitsServices2;
        $dataStatistique['sousTotal']['retraits']['avec_reponses_avec_signature'] = $resultsRetraitsTravaux3 + $resultsRetraitsFournitures3 + $resultsRetraitsServices3;

        $dataStatistique['sousTotal']['offres']['sans_reponse'] = $resultsOffresTravaux1 + $resultsOffresFournitures1 + $resultsOffresServices1;
        $dataStatistique['sousTotal']['offres']['avec_reponses_sans_signature'] = $resultsOffresTravaux2 + $resultsOffresFournitures2 + $resultsOffresServices2;
        $dataStatistique['sousTotal']['offres']['avec_reponses_avec_signature'] = $resultsOffresTravaux3 + $resultsOffresFournitures3 + $resultsOffresServices3;

        $acronymeOrganisme = Atexo_CurrentUser::getCurrentOrganism();

        $objetRecherche = 'Statistiques concernant les procédures mise en lignes ';
        if ($criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= 'entre le '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart()).' et le '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
        } elseif ($criteria->getDateMiseEnLigneStart() && !$criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= 'à patrir du '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart());
        } elseif (!$criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= "jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
        }

        $service = $criteria->getSelectedEntity();
        if ('0' === $service || 0 === $service) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
            if ($organisme instanceof CommonOrganisme) {
                $entiteName = $organisme->getDenominationOrg();
            }
        } else {
            $entite = Atexo_EntityPurchase::retrieveEntityById($service, $acronymeOrganisme);
            if ($entite instanceof CommonService) {
                $entiteName = $entite->getLibelle();
            }
        }

        if ($criteria->getValeurCumule()) {
            $valeurCumule = ' / Valeurs cumulées';
        }

        (new Atexo_GenerationExcel())->genererExcelProceduresAdaptes($acronymeOrganisme, $dataStatistique, $objetRecherche, $valeurCumule, $entiteName);
    }
}
