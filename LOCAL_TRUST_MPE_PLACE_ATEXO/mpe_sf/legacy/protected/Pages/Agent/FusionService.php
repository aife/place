<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Admministration des fournisseurs de documents.
 */
class FusionService extends MpeTPage
{
    private string $organisme = '';
    private string $idService = '';

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->getViewState('organisme');
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->setViewState('organisme', $organisme);
    }

    /**
     * @return string
     */
    public function getIdService()
    {
        return $this->getViewState('idService');
    }

    /**
     * @param string $idService
     */
    public function setIdService($idService)
    {
        $this->setViewState('idService', $idService);
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->getIsPostBack()) {
            $this->panelBlocMessages->setDisplay('None');
            $organisme = Atexo_CurrentUser::hasHabilitation('HyperAdmin') ? base64_decode(Atexo_Util::atexoHtmlEntities($_GET['organisme'])) : Atexo_CurrentUser::getCurrentOrganism();
            if ($organisme && isset($_GET['idService']) && Atexo_CurrentUser::hasHabilitation('DeplacerService')) {
                $idService = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idService']));
                $this->setIdService($idService);
                $this->setOrganisme($organisme);
                self::displayEntiteCible();
                $this->serviceToFusion->Text = Atexo_EntityPurchase::getPathEntityById($idService, $organisme);
                $this->idretour->NavigateUrl = '?page=Agent.AgentDetailEntiteAchat&idEntite='.$this->getIdService().'&org='.$this->getOrganisme();
            } else {
                //// redirect
            }
        }
    }

    public function displayEntiteCible()
    {
        $entities = Atexo_EntityPurchase::getAllEntityPurchase($this->getOrganisme(), true, [$this->getIdService()]);
        $this->listeSerivces->DataSource = $entities;
        $this->listeSerivces->DataBind();
    }

    /*
     * Permet d'afficher le message d'erreur et cacher les autres composants
     *
     * @param string $messageErreur le message d'erreur
     * @return void
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function RedirectReturn()
    {
        $this->response->redirect('index.php?page=Agent.AgentDetailEntiteAchat&idEntite='.$this->getIdService().'&org='.$this->getOrganisme());
    }

    public function fusionnerService()
    {
        $idServiceSource = $this->getIdService();
        $organisme = $this->getOrganisme();
        $idServiceCible = null;
        foreach ($this->listeSerivces->Items as $service) {
            if ($service->serviceChecked->checked) {
                $idServiceCible = $service->idService->value;
            }
        }
        if (null !== $idServiceCible) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $fusionService = (new Atexo_EntityPurchase())->retrievefusionServiceNotMergedByIdService($idServiceSource, $idServiceCible, $organisme, $connexion);
            if (!$fusionService) {
                $fusionService = new CommonTFusionnerServices();
                $fusionService->setIdServiceSource($idServiceSource);
                $fusionService->setIdServiceCible($idServiceCible);
                $fusionService->setOrganisme($organisme);
                $fusionService->setIdAgent(Atexo_CurrentUser::getId());
                $fusionService->setDateCreation(date('Y-m-d H:i:s'));
                $fusionService->save($connexion);
            }
            $this->panelBlocMessages->setDisplay('Dynamic');
            $this->panelMessageConfirmation->setMessage(Prado::localize('DEPLACEMENT_ENTITE_ACHAT_EN_COURS'));
            $this->infosFusionnement->setDisplay('None');
        }
        $this->labelJavascript->Text .= "<script>J('.demander-fusion').dialog('destroy');resetElementHeight();</script>";
    }

    public function demandeFusionnement($sender, $param)
    {
        if ($this->isValid) {
            $this->labelJavascript->Text .= "<script>openModal('demander-fusion','popup-small2',document.getElementById('".$sender->getClientId()."'));</script>";
        }
    }

    public function validerFusionnement($sender, $param)
    {
        $idServiceCible = null;
        foreach ($this->listeSerivces->Items as $service) {
            if ($service->serviceChecked->checked) {
                $idServiceCible = $service->idService->value;
            }
        }
        if (null === $idServiceCible) {
            $param->IsValid = false;
            $scriptJs = "document.getElementById('divValidationSummary').style.display='';";
            $scriptJs .= "goToTopPage('divValidationSummary');";

            $this->validateDonneesFusionnement->ErrorMessage = '';
            $errorMsg = Prado::localize('ENTITE_ACHAT_CIBLE');
            $this->divValidationSummary->addServerError($errorMsg, false);
        } else {
            $scriptJs = "document.getElementById('divValidationSummary').style.display='none';";
        }
        $this->labelJavascript->Text = "<script>$scriptJs</script>";
    }
}
