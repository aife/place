<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;

/**
 * Formulaire de Liste des Accords-cadres
 * et Systèmes d'acquisition dynamique transversaux.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ListeConsultationsAcSad extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->displayConsultations();
    }

    public function displayConsultations()
    {
        $countElement = (new Atexo_Consultation_InvitationConsTransverse())->reterieveConsultationTransverse(Atexo_CurrentUser::getCurrentOrganism(), true);
        if ($countElement > 0) {
            $this->mesgAucunResultat->setVisible(false);
            $ListeConsTrans = (new Atexo_Consultation_InvitationConsTransverse())->reterieveConsultationTransverse(Atexo_CurrentUser::getCurrentOrganism());
            //print_r($ListeConsTrans); exit;
            $this->repeaterConsTrans->DataSource = $ListeConsTrans;
            $this->repeaterConsTrans->DataBind();
        } else {
            $this->mesgAucunResultat->setVisible(true);
        }
    }

    public function insertNewLineIfTextTooLong($texte, $everyHowMuchCaracter)
    {
        return wordwrap($texte, $everyHowMuchCaracter, '<br>', true);
    }

    public function truncate($texte)
    {
        $truncatedText = null;
        $maximumNumberOfCaracterToReturn = 185;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 180) ? '' : 'display:none';
    }

    public function redirectTo($sender, $param)
    {
        $url = $param->CommandName;
        $this->response->redirect($url);
    }

    public function displayDateDecision($invConsTrans)
    {
        if ($invConsTrans) {
            return Atexo_Util::iso2frnDateTime($invConsTrans->getDateDecision());
        } else {
            return '-';
        }
    }

    public function displayDescriptionLot($ref, $numLot, $org)
    {
        if ($numLot) {
            return (new Atexo_Consultation_Lots())->retrieveIntituleLot($ref, $numLot, $org, '-');
        }
    }
}
