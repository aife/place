<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTypeProcedureOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Prado\Prado;

/**
 * Page de choix de regle de validation d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionReglesValidation extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->panelMessage->setVisible(false);
            $this->panelMessageErreur->setVisible(false);
            $this->panelChoixTypeProc->setVisible(true);
            $this->panelReglesProc->setVisible(false);
            $this->displayProceduresType();
            $this->displayServices();
            $this->displayServices2();
        }
    }

    /**
     * Afficher la liste des types de procedure.
     */
    public function displayProceduresType()
    {
        $listTypeProc = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false);
        if (is_array($listTypeProc) && count($listTypeProc) > 0) {
            $arrayTypeProc = [];
            $arrayTypeProc = Atexo_Util::arrayUnshift($listTypeProc, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES').' ---');
            $this->procedureType->DataSource = $arrayTypeProc;
            $this->procedureType->DataBind();
        }
    }

    /**
     * charger la liste des regles et leurs service.
     */
    public function gotoRegles($sender, $param)
    {
        $this->panelChoixTypeProc->setVisible(false);
        $this->panelReglesProc->setVisible(true);
        $this->selectedProcType->Text = $this->procedureType->getSelectedItem()->getText();
        $this->regle1Text->Text = Prado::localize('REGLE_VALIDATION_SIMPLE');
        $this->regle2Text->Text = Prado::localize('REGLE_VALIDATION_FINALE');
        $this->regle3Text->Text = Prado::localize('REGLE_VALIDATION_FINALE_SDM');
        $this->regle4Text->Text = Prado::localize('REGLE_VALIDATION_INTERMEDIAIRE');
        $this->regle5Text->Text = Prado::localize('REGLE_VALIDATION_INTERMEDIAIRE_FINALE');
        $this->checkedExistingRule();
    }

    /**
     * afficher la liste des services avec path.
     */
    public function displayServices()
    {
        $services = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        if (is_array($services)) {
            $this->listeAdmin1->DataSource = $services;
            $this->listeAdmin1->DataBind();
        }
    }

    /**
     * afficher la liste des services avec path.
     */
    public function displayServices2()
    {
        $services = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        if (is_array($services)) {
            $this->listeAdmin2->DataSource = $services;
            $this->listeAdmin2->DataBind();
        }
    }

    /**
     * enregister le resultat dans la base.
     *
     * @return un entier positif si ok sinon -1
     */
    public function saveValidationRules()
    {
        $validation = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $procedureType = $this->procedureType->SelectedValue;
        $pole = $this->listeAdmin1->getSelectedValue();
        $pole2 = $this->listeAdmin2->getSelectedValue();
        $proc = CommonTypeProcedureOrganismePeer::retrieveByPK($procedureType, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);

        if ($this->regle1->Checked) {
            $validation = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1');
            $proc->setServiceValidation(null);
        } elseif ($this->regle2->Checked) {
            $validation = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2');
            $proc->setServiceValidation(null);
        } elseif ($this->regle3->Checked) {
            $validation = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3');
            $proc->setServiceValidation(empty($pole) ? null : $pole);
        } elseif ($this->regle4->Checked) {
            $validation = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4');
            $proc->setServiceValidation(empty($pole2) ? null : $pole2);
        } elseif ($this->regle5->Checked) {
            $validation = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5');
            $proc->setServiceValidation(null);
        }
        if ($validation) {
            $proc->setIdTypeValidation($validation);
            if ($proc->validate()) {
                //print_r($proc);exit;
                $proc->save($connexionCom);
            //$this->response->redirect("?page=Agent.GestionTypeValidation&init=true");
            } else {
                return -1;
            }
        }
    }

    /**
     * action boutton valider selon le resultat de la sauvgarde.
     */
    public function doSave($sender, $param)
    {
        $resultSave = self::saveValidationRules();
        if ($resultSave >= 0) {
            $this->panelMessage->setVisible(true);
            $this->panelMessage->setMessage(Prado::localize('TEXT_SACCESS_ENREGISTREMENT_REGLE_VALIDATION'));
            $this->panelMessageErreur->setVisible(false);
            $this->panelChoixTypeProc->setVisible(true);
            $this->panelReglesProc->setVisible(false);
        } else {
            $this->panelMessage->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_ENREGISTREMENT_REGLE_VALIDATION'));
            $this->panelChoixTypeProc->setVisible(true);
            $this->panelReglesProc->setVisible(false);
        }
    }

    /**
     * cocher la regle suavrager dans la base.
     */
    public function checkedExistingRule()
    {
        $idTypeProcedureType = $this->procedureType->SelectedValue;
        $procedureType = (new Atexo_Consultation_ProcedureType())->retreiveNewProcedureType($idTypeProcedureType);
        if ($procedureType) {
            if ($procedureType->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')) {
                $this->regle1->Checked = true;
            } elseif ($procedureType->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2')) {
                $this->regle2->Checked = true;
            } elseif ($procedureType->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')) {
                $this->regle3->Checked = true;
                $this->listeAdmin1->SelectedValue = $procedureType->getServiceValidation();
            } elseif ($procedureType->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4')) {
                $this->regle4->Checked = true;
                $this->listeAdmin2->SelectedValue = $procedureType->getServiceValidation();
            } elseif ($procedureType->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5')) {
                $this->regle5->Checked = true;
            }
        } else {
            return;
        }
    }
}
