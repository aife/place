<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Agent\Atexo_Agent_Notifications;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupNombreNotifications extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        echo (new Atexo_Agent_Notifications())->getNombreNotificationActif(Atexo_CurrentUser::getId(), '0');
        exit;
    }
}
