<?php

namespace Application\Pages\Agent;

use App\Service\WebServices\WebServicesExec;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Helios\Atexo_Helios_PiecesAttributaires;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChorusSelectedFiles extends MpeTPage
{
    private $_consultation = '';
    private $_organisme = '';
    private $_echange = '';
    private $_decision = '';
    private $_contrat = '';
    private $_contratExec = null;
    private $_lots = null;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idEchange'])) {
            $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
            $idEchange = Atexo_Util::atexoHtmlEntities($_GET['idEchange']);
            $this->_echange = (new Atexo_Chorus_Echange())->retreiveEchangeByIdJoinDecisionenveloppe($idEchange, $this->_organisme);
            // @TODO KBE $this->_decision = $this->_echange->getDecisionenveloppe();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->_contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($this->_echange->getIdDecision(), $connexion);

            if (($this->_echange) && Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                $uuid = $this->_echange->getUuidExterneExec();
                if (!empty($uuid)) {
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    $contratExec = $execWS->getContent('getContratByUuid', $uuid);
                    $this->_contratExec = $contratExec;

                    $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_contratExec['idConsultation'], $this->_organisme);
                    if ($consultation instanceof CommonConsultation) {
                        $this->_consultation = $consultation;
                        $this->displayPiecesDce();
                    } else {
                        $msgErreur = "Impossible d'accéder . Vous n'avez pas accès à cette consultation.";
                    }
                }
            } elseif (($this->_echange) && ($this->_contrat instanceof CommonTContratTitulaire)) {
                // @TODO OK KBE $refCons = $this->_decision->getConsultationRef();
                $refCons = $this->_contrat->getRefConsultation();
                $lotConsContrat = (new Atexo_Consultation_Contrat())->getListeConsLotsContratByIdContrat($this->_contrat->getIdContratTitulaire(), $this->_organisme);

                if (is_array($lotConsContrat)) {
                    foreach ($lotConsContrat as $oneEl) {
                        if ($oneEl instanceof CommonTConsLotContrat) {
                            $refCons = $oneEl->getConsultationId();
                            $lots[] = $oneEl->getLot();
                        }
                    }
                }
                $this->_lots = $lots;

                $consultation = (new Atexo_Consultation())->retrieveConsultation($refCons, $this->_organisme);
                if ($consultation instanceof CommonConsultation) {
                    $this->_consultation = $consultation;
                    $this->displayPiecesDce();
                } else {
                    $msgErreur = "Impossible d'accéder . Vous n'avez pas accès à cette consultation.";
                }
            } else {
                $msgErreur = "Aucune décision n'a été faites";
            }

            if (!$this->IsPostBack) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $this->_echange = CommonChorusEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEchange']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                // Pièces Externe
                $this->displayPiecesExternes();
                //Afficher les pièces de la reponse
                if (($this->_contrat instanceof CommonTContratTitulaire && !empty($this->_contrat->getRefConsultation())) ||
                    ($this->usingExecContrat() && !empty($this->_contratExec['idConsultation']))
                ) {
                    $this->displayPiecesReponse();
                }
                // les Document extèrne de la consultation
                $this->displayDocumentExterne();

                //Pièce DUME
                $this->displayPiecesDume(
                    Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'),
                    $this->getViewState('arrayIndexSelectedFilesDumeAcheteur')
                );

                $this->displayPiecesDume(
                    Atexo_Config::getParameter('TYPE_DUME_OE'),
                    $this->getViewState('arrayIndexSelectedFilesDumeOE')
                );

                // Les pièces de notification
                // @TODO KBE $this->displayPiecesNotification();
                //Afficher les pièces d'ouverture et analyse
                $this->displayPiecesOuvertureAnalyse();
                self::displayPiecesFicheModificative();
            }
        } else {
            $msgErreur = "Impossible d'accéder à la fiche du marché. Paramètre manquant."; //TODO msg err
        }
    }

    /*
     * retourne la liste des rapports de verfication de signature
     */
    public function displayPiecesOuvertureAnalyse()
    {
        $pieces = [];
        if ($this->_echange instanceof CommonChorusEchange) {
            $pieces = (new Atexo_Chorus_Echange())->retriveListeRapportSignatureEchange($this->_echange, $this->_organisme, true);
        }
        $this->repeaterPiecesOuvertureAnalyse->DataSource = $pieces;
        $this->repeaterPiecesOuvertureAnalyse->dataBind();
    }

    /**
     *  Autres pièces externes :.
     */
    public function displayPiecesExternes()
    {
        $piecesexternes = (new Atexo_Chorus_Echange())->retreivePiecesExternes(Atexo_Util::atexoHtmlEntities($_GET['idEchange']), $this->_organisme);
        if ($piecesexternes) {
            $this->repeaterPiecesExternes->DataSource = $piecesexternes;
            $this->repeaterPiecesExternes->dataBind();
        } else {
            $this->labelPiecesExternes->setVisible(false);
        }
    }

    public function displayPiecesReponse()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        // @TODO KBE cas plusieurs lots

        if ($this->_contrat instanceof CommonTContratTitulaire) {
            $refCons = $this->_contrat->getRefConsultation();
        }

        if ($this->usingExecContrat()) {
            $refCons = $this->_contratExec['idConsultation'];
        }

        $lot = null;
        if (is_array($this->_lots) && count((array) $this->_lots) > 0) {
            $lot = $this->_lots[0];
        }
        $attibutaires = (new Atexo_Consultation_Decision())->retrieveAttributaireInformation($this->_organisme, $refCons, $lot);
        $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($attibutaires[0]['id_enveloppe'], Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'));
        $idsBlobNonZip = '';
        $idsBlobZip = '';
        $idsBlobSignatureZip = '';
        foreach ($piecesAttributaires as $OnePiece) {
            if ($OnePiece instanceof CommonFichierEnveloppe && $OnePiece->getIdBlob()) {
                if ('ACE' == $OnePiece->getTypeFichier()) {
                    if ('' != $this->_echange->getIdsEnvAe()) {
                        $this->panelACE->setVisible(true);
                        $this->nameACE->Text = '-&nbsp; '.$OnePiece->getNomFichier().' ('.Atexo_Util::arrondirSizeFile($OnePiece->getTailleFichier() / 1024).')';
                    }
                    if ('0' != $this->_echange->getSignAce()) {
                        $this->panelACESign->setVisible(true);
                        $this->signACE->Text = '-&nbsp; '."Signature titulaire de l'Acte : ".$OnePiece->getCnSubject($this->_consultation->getSignatureOffre(), $OnePiece->getTypeSignature()).' ('.Atexo_Util::arrondirSizeFile(strlen($this->_consultation->getSignatureOffre())).')';
                    }
                } else {
                    $this->panelPRI->setVisible(true);
                    $idBlob = $OnePiece->getIdBlob();
                    $this->idBlob->Value = $idBlob;
                    $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $this->_organisme);
                    $blob_file = $blobResource['blob']['pointer'];
                    if ('zip' == Atexo_Util::getExtension($OnePiece->getNomFichier())) {
                        if ((new Atexo_Zip())->verifyZipIntegrity($blob_file)) {
                            //$arrayIndexItems = explode(';', $this->_echange->getIdsEnvItems());
                            $tableauIndexItems = [];
                            $tableauIndexSignature = [];
                            if (!str_contains($this->_echange->getIdsEnvItems(), '#')) {//Cas des traitements existants
                                $tableauIndexItems = explode(';', $this->_echange->getIdsEnvItems());
                                $tableauIndexSignature = explode(';', $this->_echange->getIdsEnvSignItems());
                            } else {//Nouveaux cas
                                $listeIndex = explode('#', $this->_echange->getIdsEnvItems());
                                foreach ($listeIndex as $index) {
                                    if ($listeIndex[0] != $index) {
                                        $arrayIndex = explode('_', $index);
                                        if ($arrayIndex[0] == $idBlob) {
                                            $tableauIndexItems = explode(';', $arrayIndex[1]);
                                        }
                                    }
                                }
                            }
                            $s = (new Atexo_Zip())->getArborescence($blobResource, $idBlob, $tableauIndexItems, null, 'env_item_'.$OnePiece->getIdBlob(), true);
                            //$this->scriptArborescenceEnveloppe->Text = $this->scriptArborescenceEnveloppe->Text.$s;
                            $idsBlobZip = $idsBlobZip.'#'.$idBlob;
                            $htmlSignature = '';
                            $arrayZipFile = self::getFileSignature($OnePiece, '.zip');
                            $idBlobSignature = '';
                            if ($arrayZipFile) {
                                $zipFile = $arrayZipFile[0].$arrayZipFile[1];
                                //Début insertion de la signature du fichier dans les blob
                                $atexoBlob = new Atexo_Blob();
                                if ($OnePiece->getIdBlobSignature()) {
                                    $idBlobSignature = $OnePiece->getIdBlobSignature();
                                } else {
                                    $idBlobSignature = $atexoBlob->insert_blob($arrayZipFile[1], $zipFile, $OnePiece->getOrganisme());
                                }
                                $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlobSignature, $OnePiece->getOrganisme());
                                $blob_file = $blobResource['blob']['pointer'];
                                $OnePiece->setIdBlobSignature($idBlobSignature);
                                $OnePiece->save($connexion);
                                //Fin insertion de la signature du fichier dans les blob
                                $listeSignIndex = explode('#', $this->_echange->getIdsEnvSignItems());
                                foreach ($listeSignIndex as $indexSign) {
                                    if ($listeSignIndex[0] != $indexSign) {
                                        $arrayIndex = explode('_', $indexSign);
                                        if ($arrayIndex[0] == $idBlobSignature) {
                                            $tableauIndexSignature = explode(';', $arrayIndex[1]);
                                        }
                                    }
                                }
                                $htmlSignature = (new Atexo_Zip())->getArborescence($blobResource, $idBlobSignature, $tableauIndexSignature, null, 'env_item_'.$OnePiece->getIdBlobSignature(), true);
                            }
                            $this->scriptArborescenceEnveloppe->Text = $this->scriptArborescenceEnveloppe->Text.$s.$htmlSignature;
                            $idsBlobSignatureZip = $idsBlobSignatureZip.'#'.$idBlobSignature;
                        }
                    } else {
                        if (!$firstAcces) {
                            $this->scriptArborescenceEnveloppeNonZip->Text = '<ul>';
                        }
                        $idsBlobNonZip = $idsBlobNonZip.'#'.$idBlob;
                        $checked = '';
                        $arrayIdsBlob = explode(';', $this->_echange->getIdsBlobEnv());
                        if (in_array($idBlob, $arrayIdsBlob)) {
                            $checked = 'checked="checked"';
                            $this->scriptArborescenceEnveloppeNonZip->Text = $this->scriptArborescenceEnveloppeNonZip->Text.'<li><span class="check-bloc"></span><label class="content-bloc bloc-580" for="env_nonZip_'.$OnePiece->getIdBlob().'">
						  	<span class="float-left">'.'- '.$OnePiece->getNomFichier().'&nbsp;( '.Atexo_Util::getTailleBlob($OnePiece->getOrganisme(), $OnePiece->getIdBlob()).' )</span></label><li>';
                        }
                        //Début cas signature fichier
                        $chorusEchange = CommonChorusEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEchange']), $OnePiece->getOrganisme(), $connexion);
                        $echangeIdsBlobSignEnv = '';
                        if ($chorusEchange instanceof CommonChorusEchange) {
                            $echangeIdsBlobSignEnv = $chorusEchange->getIdsBlobSignEnv();
                        }
                        if ('' != $echangeIdsBlobSignEnv && '' != $OnePiece->getSignatureFichier() && strstr($echangeIdsBlobSignEnv, (string) $OnePiece->getIdBlobSignature())) {
                            $idBlobSignature = '';
                            $fileContent = base64_decode($OnePiece->getSignatureFichier());
                            $atexoBlob = new Atexo_Blob();
                            $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time();
                            touch($tmpPath);
                            $fp = fopen($tmpPath, 'w');
                            fwrite($fp, $fileContent);
                            fclose($fp);
                            if ($OnePiece->getIdBlobSignature()) {
                                $idBlobSignature = $OnePiece->getIdBlobSignature();
                            } else {
                                $idBlobSignature = $atexoBlob->insert_blob($OnePiece->getNomFichier(), $tmpPath, $OnePiece->getOrganisme());
                            }
                            $OnePiece->setIdBlobSignature($idBlobSignature);
                            $OnePiece->save($connexion);
                            $checkedFileSign = '';
                            $arrayIdsBlobSign = explode(';', $this->_echange->getIdsBlobSignEnv());
                            if (in_array($idBlobSignature, $arrayIdsBlobSign)) {
                                $checkedFileSign = 'checked="checked"';
                            }
                            $this->scriptArborescenceEnveloppeNonZip->Text = $this->scriptArborescenceEnveloppeNonZip->Text.'<li><span class="check-bloc"></span><label class="content-bloc bloc-580" for="env_nonZip_'.$idBlobSignature.'">
						  	<span class="float-left">'.'- '.$OnePiece->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier()).'&nbsp;( '.Atexo_Util::getTailleBlob($OnePiece->getOrganisme(), $idBlobSignature).' )</span></label><li>';
                        }
                        //Début cas signature fichier
                        $firstAcces = true;
                    }
                }
            }
        }
    }

    public function displayDocumentExterne()
    {
        $documentExterne = (new Atexo_Chorus_Echange())->retreiveSelectedDocumentExterne($this->_organisme, $this->_echange->getId());
        if ($documentExterne) {
            $data = [];
            $i = 0;
            foreach ($documentExterne as $oneDoc) {
                $data[$i]['id'] = $oneDoc->getType();
                $data[$i]['doc'] = $oneDoc;
                if ('1' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('TEXT_PUBLICITES');
                } elseif ('2' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('DEFINE_CONSULTATION');
                } elseif ('3' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('DEFINE_REGISTRES');
                } elseif ('4' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('DESCRIPTION_COMMISSION_APPEL_OFFRES');
                } elseif ('5' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('DESCRIPTION_NOTIFICATION');
                } elseif ('6' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('TEXT_CANDIDATURES_OFFRES_NON_RETENUES');
                } elseif ('7' == $oneDoc->getType()) {
                    $data[$i]['type'] = Prado::localize('TEXT_CANDIDATURES_OFFRES_RETENUES');
                }
                ++$i;
            }
            $listType = [];
            $lisIdType = [];
            $j = 0;
            foreach ($data as $dataRow) {
                if (!in_array($dataRow['id'], $lisIdType)) {
                    $listType[$j]['Id'] = $dataRow['id'];
                    $listType[$j]['type'] = $dataRow['type'];
                    ++$j;
                }
                $lisIdType[] = $dataRow['id'];
            }
            $this->repeaterType->DataSource = $listType;
            $this->repeaterType->dataBind();
            foreach ($this->repeaterType->getItems() as $item) {
                $idType = $this->repeaterType->DataKeys[$item->itemIndex];
                $newData = [];
                foreach ($data as $dataDoc) {
                    if ($dataDoc['id'] == $idType) {
                        $newData[] = $dataDoc['doc'];
                    }
                }
                $item->repeaterTypeDoc->DataSource = $newData;
                $item->repeaterTypeDoc->dataBind();
            }
        } else {
            $this->labelPieceConsultation->setVisible(false);
            $this->panelPieceConsultation->setVisible(false);
        }
    }

    /**
     * affiche l'arboressance du DCE.
     */
    public function displayPiecesDce()
    {
        $scriptDce = null;
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_consultation->getId(), $this->_organisme);
        if ($dce instanceof CommonDCE) {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $this->_organisme);
            $arDceItems = explode(';', $this->_echange->getDceItems());
            if (is_array($arDceItems)) {
                $scriptDce = (new Atexo_Zip())->getArborescence($blobResource, $dce->getDce(), $arDceItems, null, 'dce_item', true);
            }
        }
        // echo $scriptDce;exit;
        if (' <ul></ul>' != $scriptDce) {
            $this->panelPieceDCE->setVisible(true);
            $this->scriptDce->Text = $scriptDce;
        } else {
            $this->labelPieceDce->setVisible(false);
        }
    }

    /**
     * retourne la taille du blob.
     */
    public function getFileTaille($idBlob)
    {
        return Atexo_Util::getTailleBlob($this->_organisme, $idBlob);
    }

    public function displaySizeFile($idBlob, $nameFile)
    {
        if ('ZIP' != strtoupper(Atexo_Util::getExtension($nameFile))) {
            return ' ('.$this->getFileTaille($idBlob).')';
        }
    }

    /* La fonction qui affiche l'arbo du zip - document de la consultation
     *
     *
     */
    public function getJustSelectedDocCons($idBlob, $itemIndex)
    {
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $this->_organisme);
        $arrayItems = explode(';', $this->_echange->getIdsPiecesExternes());
        $arrayFilesItem = [];
        foreach ($arrayItems as $item) {
            if ($item) {
                $arrayItem = explode('_', $item);
                $arrayFilesItem[] = $arrayItem[1];
            }
        }
        $s = (new Atexo_Zip())->getArborescence($blobResource, $idBlob, $arrayFilesItem, null, 'zip_item', true);

        return $s;
    }

    /**
     * affiche l'arboressance des pièces de notification.
     */
    public function displayPiecesNotification()
    {
        $arrayItemsNotif = explode(';', $this->_echange->getPiecesNotifItems());

        //On recupere le fichier dans les blobs
        $atexoBlob = new Atexo_Blob();
        $fileContent = $atexoBlob->return_blob_to_client($this->_decision->getIdBlobPiecesNotification(), Atexo_CurrentUser::getCurrentOrganism());
        //On stock le fichier sur un dossier tmp
        $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.date('Y-m-d_h_i_s').microtime().$this->_decision->getIdBlobPiecesNotification();
        if (!is_dir($tmpPath) && !is_file($tmpPath)) {
            mkdir($tmpPath);
        }
        if (!is_file($tmpPath)) {
            touch($tmpPath.'/pieces_notification.zip');
        }
        $fp = fopen($tmpPath.'/pieces_notification.zip', 'w');
        fwrite($fp, $fileContent);
        fclose($fp);

        if (is_array($arrayItemsNotif)) {
            $scriptNotif = (new Atexo_Zip())->getArborescence(null, null, $arrayItemsNotif, $tmpPath.'/pieces_notification.zip', 'notif_item', true);
            // echo $scriptNotif;exit;
        }

        $html = ' <ul><ul style="display:block"></li><ul style="display:block"></li></ul></ul><ul style="display:block"></li>';
        $html .= '<ul style="display:block"></ul></ul><ul style="display:block"></li><ul style="display:block"></li></ul></ul><ul style="display:block"></li>';
        $html .= '<ul style="display:block"></ul></ul><ul style="display:block"></li><ul style="display:block"></li></ul></ul><ul style="display:block"></li></ul>';
        $html .= '<ul style="display:block"></li><ul style="display:block"></ul></ul></ul></ul>';
        if ($scriptNotif != $html) {
            $this->panelNotification->setVisible(true);
            $this->scriptNotification->Text = $scriptNotif;
        } else {
            $this->labelNotification->setVisible(false);
        }
    }

    /**
     * Permet de contruire un fichier zip contenant le fichier de signature.
     *
     * @param $fichier: objet de la classe CommonFichierEnveloppe
     *
     * @return : un tableau contenant le nom du fichier de signature et son chemin
     */
    public function getFileSignature(CommonFichierEnveloppe $fichier, $extension)
    {
        if (0 != strcmp($fichier->getSignatureFichier(), '')) {
            $fileName = Atexo_Config::getParameter('COMMON_TMP').'/'.$fichier->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($fichier->getSignatureFichier());
            Atexo_Util::write_file($fileName, $fichier->getSignatureFichier());
            (new Atexo_Zip())->addFileToZip($fileName, $fileName.$extension);

            return [0 => Atexo_Config::getParameter('COMMON_TMP').'/', 1 => $fichier->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($fichier->getSignatureFichier()).$extension];
        }

        return false;
    }

    /**
     * permet de lister les Pjs de la fiche modifictaive.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function displayPiecesFicheModificative()
    {
        $ficheModificative = $this->_echange->getCommonTChorusFicheModificatives()[0];
        if ($ficheModificative instanceof CommonTChorusFicheModificative) {
            $pjFicheModificative = $ficheModificative->getCommonTChorusFicheModificativePjs();
            $this->labelFicheModificative->setVisible(true);
            $this->repeaterPiecesFicheModificative->DataSource = $pjFicheModificative;
            $this->repeaterPiecesFicheModificative->dataBind();
        }
    }

    public function displayPiecesDume($typeDume, $dumeItems = null)
    {
        if (
            $this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation() ||
            $this->usingExecContrat() && isset($this->_contratExec['idConsultation']) && !empty($this->_contratExec['idConsultation'])
        ) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $scriptDume = '';
                if ($typeDume == Atexo_Config::getParameter('TYPE_DUME_ACHETEUR')) {
                    $this->scriptDumeAcheteur->Text = '';
                    if (!$dumeItems) {
                        $dumeItems = $this->_echange->getDumeAcheteurItems();
                        if ('' == $dumeItems) {
                            $this->panelPieceConsultationDumeAcheteur->setVisible(false);
                        }
                    }
                } elseif ($typeDume == Atexo_Config::getParameter('TYPE_DUME_OE')) {
                    $this->scriptDumeOe->Text = '';
                    if (!$dumeItems) {
                        $dumeItems = $this->_echange->getDumeOeItems();
                        if ('' == $dumeItems) {
                            $this->panelPieceConsultationDumeOe->setVisible(false);
                        }
                    }
                }

                if ('' != $dumeItems) {
                    $arDumeItems = explode(';', $dumeItems);
                    foreach ($arDumeItems as $dume) {
                        $blobResource = Atexo_Blob::acquire_lock_on_blob(
                            $dume,
                            $this->_organisme
                        );

                        $scriptDume .= $this->generateHtml($blobResource);
                    }
                }

                if ($typeDume == Atexo_Config::getParameter('TYPE_DUME_ACHETEUR')) {
                    $this->scriptDumeAcheteur->Text = $scriptDume.'</ul>';
                } elseif ($typeDume == Atexo_Config::getParameter('TYPE_DUME_OE')) {
                    $this->scriptDumeOe->Text = $scriptDume.'</ul>';
                }
            } catch (\Exception $e) {
                $message = "Erreur lors de l'affichage des pieces du DUME : $dumeItems ".PHP_EOL.
                    'Erreur: '.$e->getMessage().PHP_EOL.
                    'Trace: '.$e->getTraceAsString();
                $logger->error($message);
            }
        }
    }

    private function generateHtml($blobResource)
    {
        $scriptDume = '<li><span class="check-bloc"></span><label class="content-bloc bloc-580" for="dume_'.
            $blobResource['blob']['id'].'">- '.
            $blobResource['blob']['name'].
            '&nbsp;( '.Atexo_Util::getTailleBlob($blobResource['blob']['organisme'], $blobResource['blob']['id']).' )';
        $scriptDume .= '</label></li>';

        return $scriptDume;
    }

    private function usingExecContrat()
    {
        return Atexo_Config::getParameter('ACTIVE_EXEC_V2')
            && is_array($this->_contratExec)
            && !empty($this->_contratExec);
    }
}
