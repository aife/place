<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDestinataireAnnonceJAL;
use Application\Propel\Mpe\CommonDestinataireCentralePub;
use Application\Propel\Mpe\CommonDestinataireCentralePubPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;

/**
 * Popup de chois des destinatires.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupDestinatairePublicite extends MpeTPage
{
    private $_idFormXml = '';
    private $_idPubJal = '';
    private $_destinataireJal = '';
    private $_destinataire = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->_idFormXml = Atexo_Util::atexoHtmlEntities($_GET['idFormXml']);
        $this->_idPubJal = Atexo_Util::atexoHtmlEntities($_GET['idPubJal']);
        if (!$this->IsPostBack) {
            if (isset($_GET['FormatLibre'])) {
                if (isset($_GET['CentralePublication'])) {
                    $this->displayListDestinataireCentralePub();
                } else {
                    $dest = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idPubJal);
                    $this->displayListDestinataireFormLibre($dest->getConsultationId());
                }
            } else {
                $this->displayListDestinataire();
            }
        }
    }

    /**
     * afficher la liste des déstinataire existrant dans la base.
     */
    public function displayListDestinataire()
    {
        $listDestinataire = (new Atexo_Publicite_Destinataire())->retreiveDestinataireType();
        $this->listeDestinataires->DataSource = $listDestinataire;
        $this->listeDestinataires->DataBind();
    }

    /**
     * Ajouter destinataire dans la base.
     */
    public function addDestinataire()
    {
        $arrayDest = $this->checkedDestinataire();
        foreach ($arrayDest as $oneDest) {
            if ($oneDest == Atexo_Config::getParameter('IDENTIFIANT_GROUPE_MONITEUR')) {
                (new Atexo_Publicite_Destinataire())->addAnnonceMoniteur($this->_idFormXml, $oneDest);
            } else {
                (new Atexo_Publicite_Destinataire())->addAnnonceBoamp($this->_idFormXml, $oneDest);
            }
            $this->scriptClose->Text = '<script>refreshRepeater();</script>';
        }
    }

    /**
     * Ajouter les destinataires choisi dans la base.
     */
    public function checkedDestinataire()
    {
        $arCheckedDestinataire = [];
        $index = 0;
        foreach ($this->listeDestinataires->getItems() as $item) {
            if ($item->destinataire->Checked) {
                $arCheckedDestinataire[$index] = $item->idDestinataire->getValue();
                ++$index;
            }
        }

        return $arCheckedDestinataire;
    }

    /**
     * afficher la liste des déstinataire existrant dans la base Format Libre.
     */
    public function displayListDestinataireFormLibre($consultationId)
    {
        $listDestinataire = (new Atexo_Publicite_DestinataireJAL())->retreiveJals($this->retrieveConsultation($consultationId));
        $this->listeDestinatairesLibre->DataSource = $listDestinataire;
        $this->listeDestinatairesLibre->DataBind();
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }

    /**
     * ajouter les destinataire du format libre sélectionnés.
     */
    public function addDestinataireLibre()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->listeDestinatairesLibre->getItems() as $item) {
            if ($item->destinataire->Checked) {
                $newDestAnnJal = new CommonDestinataireAnnonceJAL();
                $newDestAnnJal->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $newDestAnnJal->setIdannoncejal($this->_idPubJal);
                $newDestAnnJal->setIdjal($item->idDestinataire->getValue());
                $newDestAnnJal->setStatut(Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
                $newDestAnnJal->save($connexionCom);
            }
        }
        $this->scriptClose->Text = '<script>refreshRepeaterFormLibre();</script>';
    }

    /**
     * ajout selon le type de format sélectionné.
     */
    public function onValider($sender, $param)
    {
        if (isset($_GET['FormatLibre'])) {
            if (isset($_GET['CentralePublication'])) {
                $this->addDestinataireCentralePub();
            } else {
                self::addDestinataireLibre();
            }
        } else {
            self::addDestinataire();
        }
    }

    /**
     * cocher les destinataire correspendont au type du formualaire xml.
     */
    public function existingDestinataireLibre($idJal)
    {
        $this->_destinataireJal = (new Atexo_Publicite_DestinataireJAL())->retreiveListDestinataireByIdAnnonceJal($this->_idPubJal);
        if ($this->_destinataireJal) {
            $listDest = $this->_destinataireJal;
            foreach ($listDest as $oneDestinataire) {
                if ($idJal == $oneDestinataire->getIdJal()) {
                    return false;
                }
            }
        }

        return true;
    }

    public function displayListDestinataireCentralePub()
    {
        $comptePub = (new Atexo_Publicite_CentralePublication())->retreiveListeComptesCentraleByService(Atexo_CurrentUser::getIdServiceAgentConnected());
        $this->listeComptesCentralePub->DataSource = $comptePub;
        $this->listeComptesCentralePub->DataBind();
    }

    /**
     * cocher les destinataire correspendont au type du formualaire xml.
     */
    public function existingCentralePublicite($idCompte)
    {
        $this->_destinataire = (new Atexo_Publicite_CentralePublication())->retreiveListeDestinataireCentraleByIdAnnonce($this->_idPubJal);
        if ($this->_destinataire) {
            foreach ($this->_destinataire as $oneDestinataire) {
                if ($idCompte == $oneDestinataire->getIdCompte()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * ajouter les destinataire du format libre sélectionnés Centrale de publication.
     */
    public function addDestinataireCentralePub()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->listeComptesCentralePub->getItems() as $item) {
            if ($item->destinataire->Checked) {
                if ('0' == $item->existingCentralePublicite->getValue()) {
                    $newDest = new CommonDestinataireCentralePub();
                    $newDest->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $newDest->setStatut(Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
                } else {
                    $destinataire = (new Atexo_Publicite_CentralePublication())->retreiveDestinataireByIdCompte($item->idCompte->getValue(), $this->_idPubJal);
                    $newDest = CommonDestinataireCentralePubPeer::retrieveByPK($destinataire->getId(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    $newDest->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                }
                $newDest->setIdAnnonceJal($this->_idPubJal);
                $newDest->setIdCompte($item->idCompte->getValue());
                $idsJournaux = '';
                $separateur = ',';
                foreach ($item->listeJournauxCentralePub->getItems() as $item) {
                    if ($item->journal->Checked) {
                        $idsJournaux .= $item->idjournal->Value.$separateur;
                    }
                }
                $newDest->setIdsJournaux($idsJournaux);
                $newDest->save($connexionCom);
            //print_r($connexionOrg);exit;
            } else {
                if ('1' == $item->existingCentralePublicite->getValue()) {
                    $destinataire = (new Atexo_Publicite_CentralePublication())->retreiveDestinataireByIdCompte($item->idCompte->getValue(), $this->_idPubJal);
                    if ($destinataire) {
                        $criteria = new Criteria();
                        $criteria->add(CommonDestinataireCentralePubPeer::ID, $destinataire->getId());
                        $criteria->add(CommonDestinataireCentralePubPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
                        CommonDestinataireCentralePubPeer::doDelete($criteria, $connexionCom);
                    }
                }
            }
        }//exit;
        $this->scriptClose->Text = '<script>refreshRepeaterFormLibre();</script>';
    }

    /**
     * cocher les Journaux correspendont déjà séléctioné.
     */
    public function existingJournal($idCompte, $idJournal)
    {
        $destinataire = (new Atexo_Publicite_CentralePublication())->retreiveDestinataireByIdCompte($idCompte, $this->_idPubJal);
        if ($destinataire) {
            if (in_array($idJournal, explode(',', $destinataire->getIdsJournaux()))) {
                return true;
            }
        }

        return false;
    }

    public function sendToCentralePublicite($idCompte)
    {
        $destinataire = (new Atexo_Publicite_CentralePublication())->retreiveDestinataireByIdCompte($idCompte, $this->_idPubJal);
        if ($destinataire) {
            if ($destinataire->getStatut() == Atexo_Config::getParameter('DESTINATAIRE_ENVOYE')) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function getIdFormXml()
    {
        return $this->_idFormXml;
    }
}
