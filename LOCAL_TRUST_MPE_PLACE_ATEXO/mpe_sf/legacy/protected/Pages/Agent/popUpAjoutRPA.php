<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonRPA;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe popUpAjoutRPA.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpAjoutRPA extends MpeTPage
{
    protected $_IdEntite;
    protected $_organisme;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['IdEntite'])) {
            $this->_IdEntite = Atexo_Util::atexoHtmlEntities($_GET['IdEntite']);
        } else {
            $this->_IdEntite = 0;
        }
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
            $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        $this->Page->setViewState('organismeRpa', $this->_organisme);

        if (!$this->IsPostBack) {
            if (isset($_GET['IdRpa'])) {
                self::displayRpa(Atexo_Util::atexoHtmlEntities($_GET['IdRpa']));
            } else {
                $this->non->checked = true;
            }
        }
        $this->listPays->setPostBack($this->IsPostBack);
    }

    public function createRPA($sender, $param)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $RpaBean = new CommonRPA();

            if (!isset($_GET['IdRpa'])) {
                $RpaBean->setServiceId($this->_IdEntite);
            } else {
                $RpaBean = Atexo_EntityPurchase_RPA::retrieveRpaById(Atexo_Util::atexoHtmlEntities($_GET['IdRpa']), $this->_organisme);
            }
            $RpaBean->setAcronymeorg($this->_organisme);
            $RpaBean->setNom(($this->nom->Text));
            $RpaBean->setPrenom(($this->prenom->Text));
            $RpaBean->setFonction(($this->fonction->Text));
            $RpaBean->setOrganisme(($this->nomOrga->Text));
            $RpaBean->setAdresse1(($this->adresse1->Text));
            $RpaBean->setAdresse2(($this->adresse2->Text));
            $RpaBean->setCodepostal(($this->cp->Text));
            $RpaBean->setVille(($this->ville->Text));
            $RpaBean->setDateModification(date('Y-m-d H:s'));
            $RpaBean->setDateCreation(date('Y-m-d H:s'));
            $RpaBean->setEmail((trim($this->mail->Text)));
            $RpaBean->setTelephone(($this->telephone->Text));
            $RpaBean->setFax(($this->fax->Text));
            $pays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->listPays->getSelectedValue());
            $RpaBean->setPays($pays);

            if ($this->oui->checked) {
                $RpaBean->setResponsableArchive('1');
            } else {
                $RpaBean->setResponsableArchive('0');
            }

            $RpaBean->save($connexionCom);

            if (!isset($_GET['IdRpa'])) {
                Atexo_EntityPurchase_RPA::setCachedRpas($this->_IdEntite, $this->_organisme);
            } else {
                Atexo_EntityPurchase_RPA::setCachedRpas($RpaBean->getServiceId(), $this->_organisme);
            }

            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_IDTRepeaterRPA_validateRpa').click();window.close();</script>";
        } catch (Exception $e) {
            echo $e;
            Prado::log("Impossible de creer un RPA'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function displayRpa($IdRpa)
    {
        $RpaBean = Atexo_EntityPurchase_RPA::retrieveRpaById($IdRpa, $this->_organisme);

        if ($RpaBean) {
            $this->nom->Text = $RpaBean->getNom();
            $this->prenom->Text = $RpaBean->getPrenom();
            $this->fonction->Text = $RpaBean->getFonction();
            $this->nomOrga->Text = $RpaBean->getOrganisme();
            $this->mail->Text = $RpaBean->getEmail();
            $this->telephone->Text = $RpaBean->getTelephone();
            $this->fax->Text = $RpaBean->getFax();
            $this->adresse1->Text = $RpaBean->getAdresse1();
            $this->adresse2->Text = $RpaBean->getAdresse2();
            $this->cp->Text = $RpaBean->getCodepostal();
            $this->ville->Text = $RpaBean->getVille();

            $this->listPays->SelectedValue = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdCountry($RpaBean->getPays());

            if ('1' == $RpaBean->getResponsableArchive()) {
                $this->oui->checked = true;
            } else {
                $this->non->checked = true;
            }
        }
    }
}
