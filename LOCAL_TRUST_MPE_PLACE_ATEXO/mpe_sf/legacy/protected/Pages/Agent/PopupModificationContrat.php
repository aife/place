<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Etablissement;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ModificationContrat;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * @author Florian MARTIN <florian.martin@atexo.com>
 * @copyright Atexo 2018
 *
 * @version 1.0
 *
 * @since
 */
class PopupModificationContrat extends MpeTPage
{
    protected $contrat;
    protected ?string $typeContrat = null;
    protected $entreprise;
    protected ?string $nomEntreprise = null;

    protected ?bool $close = null;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (1 == Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn')) {
            $this->buttonValidate->Visible = false;
        } else {
            $this->buttonValidateWithDE->Visible = false;
        }

        try {
            $this->close = false;
            if ($_GET['close']) {
                $this->close = true;
            }
            $this->nomEntreprise = '';

            if ($_GET['idContrat']) {
                $idContrat = base64_decode($_GET['idContrat']);
                $this->panelInfoEntrepriseAttributaire->setDisplay('None');
                $this->contrat = (new Atexo_ModificationContrat())->retrieveContratOneByIdContratTitulaire($idContrat);
                $objetTypeContrat = (new Atexo_TypeContrat())->retrieveContratOneByIdTypeContrat($this->contrat->getIdTypeContrat());

                $this->typeContrat = Prado::Localize(
                    $objetTypeContrat->getLibelleTypeContrat()
                );

                $this->dateDefinitiveNotification->Text = $this->contrat->getDateNotification();
            } elseif ($_GET['idModificationContrat']) {
                $idModifcationContrat = base64_decode($_GET['idModificationContrat']);
                $modificationContrat = (new Atexo_ModificationContrat())->retrieveContratOneById($idModifcationContrat);
                $this->contrat = (new Atexo_Contrat())->retrieveContratOneByIdContratTitulaire($modificationContrat->getIdContratTitulaire());
                $objetTypeContrat = (new Atexo_TypeContrat())->retrieveContratOneByIdTypeContrat($this->contrat->getIdTypeContrat());

                $this->typeContrat = Prado::Localize(
                    $objetTypeContrat->getLibelleTypeContrat()
                );

                if (!$this->Page->IsPostBack) {
                    $this->textObjetMC->text = $modificationContrat->getObjetModification();
                    $this->dateSignature->text = date('d/m/Y', strtotime($modificationContrat->getDateSignature()));
                    $this->montantMarche->text = $modificationContrat->getMontant();
                    $this->dateDuree->text = $modificationContrat->getDureeMarche();

                    if ($modificationContrat->getIdEtablissement()) {
                        $this->panelInfoEntrepriseSirenSiret->setDisplay('None');
                        $etablissement = (new Atexo_Etablissement())->getEntripseByIdEtablissement($modificationContrat->getIdEtablissement());
                        $this->idEtablissement->value = $etablissement->getIdEtablissement();
                        $this->idEntreprise->value = $etablissement->getIdEntreprise();
                        $entreprise = (new Atexo_Entreprise())->getEntripseByIdEtablissement($etablissement->getIdEntreprise());
                        $this->nomEntreprise = $entreprise->getNom();
                        $this->siren->text = $entreprise->getSiren();
                        $this->siret->text = $etablissement->getCodeEtablissement();
                    } else {
                        $this->panelInfoEntrepriseAttributaire->setDisplay('None');
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $exception) {
            Prado::log("Erreur lors de la création ou modification d'une modification de contrat : ".$exception->getMessage().$exception->getTraceAsString(), TLogger::ERROR, 'ModificationContrat.php');
            throw new Exception();
        }

        if ($objetTypeContrat->getConcession()) {
            $this->labelMontantContratLight->setDisplay('None');
            $this->labelMontantContratValeurGlobale->setDisplay('Dynamic');

            $this->labelDateNotificationSignature->setDisplay('None');
            $this->labelDateSignature->setDisplay('Dynamic');

            $this->ctl0_CONTENU_PAGE_TITULAIRE->setVisible(false);
        }
    }

    /**
     * Permet de synchroniser avec Api Gouv Entreprise
     * Si l'entreprise existe dans la BD on le recuperer si non on passe à SGMAP.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function synchroniserAvecApiGouvEntreprise($sender, $param)
    {
        $dateSignature = null;
        $arrayMsgErreur = [];
        $isValid = true;
        $siren = $this->siren->getSafeText();
        $siret = $this->siret->getSafeText();
        $script = '';

        if ('' == $siren || '' == $siret) {
            if (empty($dateSignature)) {
                $script .= "J('#ctl0_CONTENU_PAGE_panelInfoEntrepriseAttributaire').css({ 'display': 'none' }); ";
                $script .= "document.getElementById('".$this->objetContratImgError->getClientId()."').style.display = 'none';";
                $arrayMsgErreur[] = Prado::localize('SIREN_SIRET');
                $isValid = false;
            } else {
                $dateSignatureIsValid = true;
            }

            $this->afficherMessagesValidationServeur($param, $isValid, $script, $arrayMsgErreur, $sender);
        } else {
            $company = Atexo_Entreprise::retrieveCompanyBySiren($siren);

            if (!$company) {
                //SynchronisationSGMAP
                if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                    $company = Atexo_Entreprise::recupererEntrepriseApiGouvEntreprise($siren, true);

                    if ($company) {
                        $listeEtablissements = $company->getEtablissements();
                        $newEtablissement = is_array($listeEtablissements) ? array_pop($listeEtablissements) : '';

                        if ($company->getNicsiege() != $siret) {
                            $newEtablissement = (new Atexo_Entreprise_Etablissement())->recupererEtablissementApiGouvEntreprise($siret);

                            if ($newEtablissement) {
                                $newEtablissement->setSaisieManuelle('0');
                                $company->setEtablissements($newEtablissement);
                            }
                        }

                        $company->setListeEtablissements($company->getEtablissements());
                        $company->setSaisieManuelle('0');
                        $company->setDateModification(date('Y-m-d H:i:s'));
                        $company->setDateCreation(date('Y-m-d H:i:s'));
                        $company->setCreatedFromDecision('1');
                        $company = (new Atexo_Entreprise())->save($company);
                    }
                }
            }

            if ($company && $company->getId()) {
                $this->idEntreprise->value = $company->getId();
                $etablissementVo = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);

                if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                    $this->idEtablissement->value = $etablissementVo->getIdEtablissement();
                }

                $nomEntreprise = $company->getNom();
                $this->panelInfoEntrepriseAttributaire->setDisplay('Dynamic');
                $script .= "J('#ctl0_CONTENU_PAGE_ENTREPRISE').html('".htmlspecialchars($nomEntreprise, ENT_QUOTES)."'); ";
                $script .= "J('#ctl0_CONTENU_PAGE_panelInfoEntrepriseAttributaire').css({'display': ''}); ";
                $this->scriptLable->Text = '<script>'.$script.'</script>';
            }
        }
    }

    /**
     * @param $param
     * @param $isValid
     * @param $script
     * @param $arrayMsgErreur
     * @param $sender
     *
     * @throws PropelException
     */
    public function afficherMessagesValidationServeur($param, $isValid, $script, $arrayMsgErreur, $sender)
    {
        if ($isValid) {
            $script .= "document.getElementById('divValidationSummary').style.display='none'";
            $this->scriptLable->Text = '<script>'.$script.'</script>';
            $this->enregistrerModificationContrat($sender, $param, $script);
        } else {
            $script .= "document.getElementById('divValidationSummary').style.display='';";
            $this->scriptLable->Text = "<script>$script</script>";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }

            return;
        }
    }

    /**
     * @param $sender
     * @param $param
     * @param $script
     *
     * @throws PropelException
     */
    public function enregistrerModificationContrat($sender, $param, $script)
    {
        $commonModificationContrat = null;
        $numOrdre = null;
        $idContrat = null;
        if ($this->isValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            try {
                if ($_GET['idContrat']) {
                    $idContrat = base64_decode($_GET['idContrat']);
                    $commonModificationContratQuery = new CommonModificationContratQuery();
                    $commonModificationContratQueries = $commonModificationContratQuery->findByIdContratTitulaire($idContrat);
                    $numOrdre = count($commonModificationContratQueries) + 1;
                    $commonModificationContrat = new CommonModificationContrat();
                    $commonModificationContrat->setDateCreation(date('Y-m-d H:i:s'));
                } elseif ($_GET['idModificationContrat']) {
                    $idModifcationContrat = base64_decode($_GET['idModificationContrat']);
                    $commonModificationContrat = (new Atexo_ModificationContrat())->retrieveContratOneById($idModifcationContrat);
                    $idContrat = $commonModificationContrat->getIdContratTitulaire();
                    $numOrdre = $commonModificationContrat->getNumOrdre();
                }

                $commonModificationContrat->setObjetModification($this->textObjetMC->text);
                $commonModificationContrat->setNumOrdre($numOrdre);
                $commonModificationContrat->setDateModification(date('Y-m-d H:i:s'));
                $dateSignature = Atexo_Util::frnDate2iso($this->dateSignature->text);
                $commonModificationContrat->setDateSignature($dateSignature);
                $commonModificationContrat->setIdContratTitulaire($idContrat);
                $idAgent = Atexo_CurrentUser::getId();
                $commonModificationContrat->setIdAgent($idAgent);
                if ('' != $this->dateDuree->text) {
                    $commonModificationContrat->setDureeMarche($this->dateDuree->text);
                }

                if ('' != $this->montantMarche->text) {
                    $commonModificationContrat->setMontant($this->montantMarche->text);
                }

                if ('' != $this->idEtablissement->value) {
                    $commonModificationContrat->setIdEtablissement($this->idEtablissement->value);
                }

                $commonModificationContrat->save();

                $contrat = (new Atexo_ModificationContrat())->retrieveContratOneByIdContratTitulaire($idContrat);

                $millesime = $contrat->getDateNotification('Y');
                $numeroContrat = $contrat->getNumeroContrat();

                if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) {
                    //dans le cas ou le numéro de contrat est NULL cf MPE-5991
                    if (null == $numeroContrat || '' == $numeroContrat) {
                        $numeroContrat = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contrat, $connexion);
                        $contrat->setNumeroContrat($numeroContrat);
                    }
                    //dans le cas ou le numéro long oeap est NULL cf MPE-5991
                    $numeroLongOeap = $contrat->getNumLongOeap();
                    if (null == $numeroLongOeap || '' == $numeroLongOeap) {
                        $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($contrat, $connexion);
                        $contrat->setNumLongOeap($numeroLong.'01');
                    }
                }

                $commonModificationContratQuery = new CommonModificationContratQuery();
                $commonModificationContratQueries = $commonModificationContratQuery->findByIdContratTitulaire($contrat->getIdContratTitulaire());

                if (count($commonModificationContratQueries) > 99) {
                    throw new Exception('Cas non géré : nombre de modifications supérieur à 99');
                }

                $nbModification = sprintf('%02d', count($commonModificationContratQueries));

                $numIdUnique = $millesime.$numeroContrat.($nbModification);
                if (Atexo_Module::isEnabled('NumDonneesEssentiellesManuel')) {
                    $contrat->setNumIdUniqueMarchePublic($contrat->getReferenceLibre());
                } else {
                    $contrat->setNumIdUniqueMarchePublic($numIdUnique);
                }

                $contrat->setDateFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMarche->text));
                $contrat->setDateMaxFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMaxMarche->text));

                $contrat->save();
                $connexion->commit();
                $validSnStatus = [
                    Atexo_Config::getParameter('STATUT_EN_ATTENTE_SN'),
                    Atexo_Config::getParameter('STATUT_PUBLIE_SN'),
                    Atexo_Config::getParameter('STATUT_NON_PUBLIE_SN')
                ];

                $urlRedirection = $_SERVER['REQUEST_URI'].'&close=true';
                $this->response->redirect($urlRedirection);
            } catch (Exception $exception) {
                $connexion->rollBack();
                Prado::log("Erreur lors de la création d'une modification de contrat : ".$exception->getMessage().$exception->getTraceAsString(), TLogger::ERROR, 'ModificationContrat.php');
            }
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function validerModificationContrat($sender, $param)
    {
        $isValid = true;
        $script = '';
        $arrayMsgErreur = [];

        $dateSignature = $this->dateSignature->text;
        $dateSignatureIsValid = false;
        $textObjetMC = $this->textObjetMC->text;
        $textObjetMCIsValid = false;
        $dateDefinitiveFinMarche = false;
        $dateDefinitiveFinMaxMarche = false;

        if (empty($dateSignature)) {
            $script .= "document.getElementById('".$this->objetContratImgError->getClientId()."').style.display = 'none';";
            $arrayMsgErreur[] = Prado::localize('MSG_VALIDATION_DATE_SIGNATURE');
        } else {
            $dateSignatureIsValid = true;
        }

        if (empty($textObjetMC)) {
            $script .= "document.getElementById('".$this->objetContratImgError->getClientId()."').style.display = 'none';";
            $arrayMsgErreur[] = Prado::localize('MSG_VALIDATION_OBJET_MODIFIFCATION_CONTRAT');
        } else {
            $textObjetMCIsValid = true;
        }

        if (empty($this->dateDefinitiveFinMarche->Text)) {
            $script .= "document.getElementById('".$this->objetContratImgError->getClientId()."').style.display = 'none';";
            $arrayMsgErreur[] = Prado::localize('DATE_DEFINITIVE_FIN_MARCHE_OBLIGATOIRE');
        } else {
            $dateDefinitiveFinMarche = true;
        }

        if (empty($this->dateDefinitiveFinMaxMarche->Text)) {
            $script .= "document.getElementById('".$this->objetContratImgError->getClientId()."').style.display = 'none';";
            $arrayMsgErreur[] = Prado::localize('DATE_DEFINITIVE_FIN_MAXIMALE_MARCHE_OBLIGATOIRE');
        } else {
            $dateDefinitiveFinMaxMarche = true;
        }

        if (!$dateSignatureIsValid || !$textObjetMCIsValid || !$dateDefinitiveFinMaxMarche || !$dateDefinitiveFinMarche) {
            $isValid = false;
        }

        $this->afficherMessagesValidationServeur($param, $isValid, $script, $arrayMsgErreur, $sender);
    }

    /**
     * @param $sender
     * @param $param
     */
    public function validerModificationContratNotification($sender, $param)
    {
        $this->script->Text = "<script>openModal('demander-validation','popup-small2',document.getElementById('panelValidationNt'), 'Demande de validation');</script>";
    }
}
