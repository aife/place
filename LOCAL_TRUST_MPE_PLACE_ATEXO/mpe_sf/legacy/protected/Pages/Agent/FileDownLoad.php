<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger le DCE.
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FileDownLoad extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['org']) {
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $idfichier = Atexo_Util::atexoHtmlEntities($_GET['idFichier']);
        $fileName = Atexo_Util::atexoHtmlEntities($_GET['nomFichier']);
        $this->downloadFiles($idfichier, $fileName, $organisme);
    }
}
