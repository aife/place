<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Created on 13 dec. 2011.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 */
class EnvoiCourrierElectroniqueChoixDestinataire extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                $this->AdressesRegistreRetraits->Text = Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->AdressesRegistreQuestions->Text = Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->AdressesRegistreDepots->Text = Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), true);
            }
            if (isset($_GET['IdEchange'])) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $echange = CommonEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['IdEchange']), $connexionCom);
                $this->adressesLibres->Text = $echange->getDestinatairesLibres();
                $this->AdressesBdFournisseurs->Text = $echange->getDestinatairesBdFournisseurs();
                $this->pageSource->setValue($echange->getPageSource());
                if ($echange->getDestinatairesDepots()) {
                    $this->checkAdressesRegistreDepots->Checked = 1;
                }
                if ($echange->getDestinatairesLibres()) {
                    $this->checkAdressesLibres->Checked = 1;
                }
                if ($echange->getDestinatairesQuestions()) {
                    $this->checkAdressesRegistreQuestions->Checked = 1;
                }
                if ($echange->getDestinatairesRetraits()) {
                    $this->checkAdressesRegistreRetraits->Checked = 1;
                }
                if ($echange->getDestinatairesBdFournisseurs()) {
                    $this->checkAdressesBdFournisseurs->Checked = 1;
                }
            }
        }

        $script = '<script>checkAdresse(); ';
        if ($_GET['forInvitationConcourir']) {
            $script .= " document.getElementById('ctl0_CONTENU_PAGE_adressesLibres').value = opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value;";
        }
        $this->javascript->Text = $script.'</script>';
    }

    public function saveMailsDestinataire()
    {
        $destinataires = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echange = CommonEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['IdEchange']), $connexionCom);
        $tabMails = [];
        $tabMailsFrn = [];
        $tabMailsretrait = [];
        $tabMailsQuestions = [];
        $tabMailsLibres = [];
        $tabMailsDepots = [];
        if ($this->checkAdressesRegistreDepots->Checked) {
            $trimmedRetrait = str_replace(' ', '', $this->AdressesRegistreDepots->Text);
            $mailsDepots = explode(',', $trimmedRetrait);
            if (is_array($mailsDepots) && 0 != count($mailsDepots)) {
                foreach ($mailsDepots as $mail) {
                    if (!in_array($mail, $tabMailsDepots)) {
                        $tabMailsDepots[] = $mail;
                    }
                }
            }
            $listeDestinatairesLibres = implode(',', $tabMailsDepots);
            $echange->setDestinatairesDepots($listeDestinatairesLibres);
            $destinataires .= $listeDestinatairesLibres;
        } else {
            $echange->setDestinatairesDepots('');
        }
        if ($this->checkAdressesLibres->Checked) {
            $trimmedLibre = str_replace(' ', '', $this->adressesLibres->Text);
            $mailsLibres = explode(',', $trimmedLibre);
            if (is_array($mailsLibres) && 0 != count($mailsLibres)) {
                foreach ($mailsLibres as $mail) {
                    if (!in_array($mail, $tabMailsLibres)) {
                        $tabMailsLibres[] = $mail;
                    }
                }
            }
            $listeDestinatairesLibres = implode(',', $tabMailsLibres);
            $echange->setDestinatairesLibres($listeDestinatairesLibres);
            if ('' != $destinataires) {
                $destinataires .= ',';
            }
            $destinataires .= $listeDestinatairesLibres;
        } else {
            $echange->setDestinatairesLibres('');
        }
        if ($this->checkAdressesRegistreQuestions->Checked) {
            $trimmedQuestions = str_replace(' ', '', $this->AdressesRegistreQuestions->Text);
            $mailsQuestions = explode(',', $trimmedQuestions);
            if (is_array($mailsQuestions) && 0 != count($mailsQuestions)) {
                foreach ($mailsQuestions as $mail) {
                    if (!in_array($mail, $tabMailsQuestions)) {
                        $tabMailsQuestions[] = $mail;
                    }
                }
            }
            $listeDestinatairesQuestions = implode(',', $tabMailsQuestions);
            $echange->setDestinatairesQuestions($listeDestinatairesQuestions);
            if ('' != $destinataires) {
                $destinataires .= ',';
            }
            $destinataires .= $listeDestinatairesQuestions;
        } else {
            $echange->setDestinatairesQuestions('');
        }
        if ($this->checkAdressesRegistreRetraits->Checked) {
            $trimmedRetrait = str_replace(' ', '', $this->AdressesRegistreRetraits->Text);
            $mailsRetrait = explode(',', $trimmedRetrait);
            if (is_array($mailsRetrait) && 0 != count($mailsRetrait)) {
                foreach ($mailsRetrait as $mail) {
                    if (!in_array($mail, $tabMailsretrait)) {
                        $tabMailsretrait[] = $mail;
                    }
                }
            }
            $listeDestinatairesRetrait = implode(',', $tabMailsretrait);
            $echange->setDestinatairesRetraits($listeDestinatairesRetrait);
            if ('' != $destinataires) {
                $destinataires .= ',';
            }
            $destinataires .= $listeDestinatairesRetrait;
        } else {
            $echange->setDestinatairesRetraits('');
        }
        if ($this->checkAdressesBdFournisseurs->Checked) {
            $trimmedBdFrn = str_replace(' ', '', $this->AdressesBdFournisseurs->Text);
            $mailsBdFrn = explode(',', $trimmedBdFrn);
            if (is_array($mailsBdFrn) && 0 != count($mailsBdFrn)) {
                foreach ($mailsBdFrn as $mail) {
                    if (!in_array($mail, $tabMailsFrn)) {
                        $tabMailsFrn[] = $mail;
                    }
                }
            }
            $listeDestinatairesBdFrn = implode(',', $tabMailsFrn);
            $echange->setDestinatairesBdFournisseurs($listeDestinatairesBdFrn);
            if ('' != $destinataires) {
                $destinataires .= ',';
            }
            $destinataires .= $listeDestinatairesBdFrn;
        } else {
            $echange->setDestinatairesBdFournisseurs('');
        }
        $mailsDestinataire = explode(',', $destinataires);
        if (is_array($mailsDestinataire) && 0 != count($mailsDestinataire)) {
            foreach ($mailsDestinataire as $mail) {
                if (!in_array($mail, $tabMails)) {
                    $tabMails[] = $mail;
                }
            }
        }
        $listeDestinataires = implode(' , ', $tabMails);
        $echange->setDestinataires($listeDestinataires);
        $echange->save($connexionCom);

        return $echange;
    }

    public function Valider($sender, $param)
    {
        if ($this->IsValid) {
            $echange = self::saveMailsDestinataire();
            if ($echange instanceof CommonEchange) {
                $this->response->redirect('index.php'.$echange->getPageSource().'&IdEchange='.Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
            }
        }
    }

    public function editerDestinatairesFournisseur($sender, $param)
    {
        self::saveMailsDestinataire();
        $this->response->redirect('index.php?page=Agent.ChoixDestinataireBdFournisseur&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&forInvitationConcourir='.Atexo_Util::atexoHtmlEntities($_GET['forInvitationConcourir']).'&IdEchange='.Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
    }

    public function checkMultipleEmailAdresseBF($sender, $param)
    {
        if ($this->checkAdressesBdFournisseurs->Checked) {
            if ('' !== trim($this->AdressesBdFournisseurs->Text)) {
                $emailsErrone = Atexo_Util::checkMultipleEmail($this->AdressesBdFournisseurs->Text);
                if (is_array($emailsErrone) && count($emailsErrone) > 0) {
                    $this->emailFournisseurValidator->ErrorMessage = Prado::localize('TEXT_ADRESSES_BD_FOURNISSEURS').' : '.implode(',', $emailsErrone);
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                } else {
                    $param->IsValid = true;
                }
            }
        }
    }

    public function checkMultipleEmailAdresseLibre($sender, $param)
    {
        if ($this->checkAdressesLibres->Checked) {
            if ('' !== trim($this->adressesLibres->Text)) {
                $emailsErrone = Atexo_Util::checkMultipleEmail($this->adressesLibres->Text);
                if (is_array($emailsErrone)) {
                    $this->emailFormatValidator->ErrorMessage = Prado::localize('DEFINE_TEXT_ADRESSE_LIBRE').' : '.implode(',', $emailsErrone);
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                } else {
                    $param->IsValid = true;
                }
            }
        }
    }

    public function checkMultipleEmailAdresseRR($sender, $param)
    {
        if ($this->checkAdressesRegistreRetraits->Checked) {
            if ('' !== trim($this->AdressesRegistreRetraits->Text)) {
                $emailsErrone = Atexo_Util::checkMultipleEmail($this->AdressesRegistreRetraits->Text);
                if (is_array($emailsErrone)) {
                    $this->emailFormatValidatorRR->ErrorMessage = Prado::localize('DEFINE_TEXT_ADRESSE_REGISTRE_RETRAIT').' : '.implode(',', $emailsErrone);
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                } else {
                    $param->IsValid = true;
                }
            }
        }
    }

    public function checkMultipleEmailAdresseRQ($sender, $param)
    {
        if ($this->checkAdressesRegistreQuestions->Checked) {
            if ('' !== trim($this->AdressesRegistreQuestions->Text)) {
                $emailsErrone = Atexo_Util::checkMultipleEmail($this->AdressesRegistreQuestions->Text);
                if (is_array($emailsErrone)) {
                    $this->emailFormatValidatorRQ->ErrorMessage = Prado::localize('DEFINE_TEXT_ADRESSE_REGISTRE_QUESTIONS').' : '.implode(',', $emailsErrone);
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                } else {
                    $param->IsValid = true;
                }
            }
        }
    }

    public function checkMultipleEmailAdresseRD($sender, $param)
    {
        if ($this->checkAdressesRegistreDepots->Checked) {
            if ('' !== trim($this->AdressesRegistreDepots->Text)) {
                $emailsErrone = Atexo_Util::checkMultipleEmail($this->AdressesRegistreDepots->Text);
                if (is_array($emailsErrone)) {
                    $this->emailFormatValidatorRD->ErrorMessage = Prado::localize('DEFINE_TEXT_ADRESSE_REGISTRE_DEPOT').' : '.implode(',', $emailsErrone);
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                } else {
                    $param->IsValid = true;
                }
            }
        }
    }
}
