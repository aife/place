<?php

namespace Application\Pages\Agent;

use App\Event\PublicationConsultationEvent;
use App\Exception\ConnexionWsException;
use App\Service\Publicite\EchangeConcentrateur;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCertificatChiffrement;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationPlateforme;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonCertificatPermanent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\Om\BaseCommonConfigurationOrganisme;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Traduction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPdf;
use Exception;
use Prado\Prado;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ValiderConsultation extends MpeTPage
{
    private $_organisme;
    private $_idService;
    private $_consultation;
    private $_typeValidation;
    private $_showBlocAffectationCle = false;
    private $urlParam = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function onLoad($param)
    {
        $messageError = '';
        $this->panelMessageErreurDume->setDisplay('None');
        if (isset($_GET['id'])) {
            $this->panelMessageErreur->setVisible(false);
            $this->panelRetour->setVisible(false);

            $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->_idService = Atexo_CurrentUser::getIdServiceAgentConnected();
            $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($this->_organisme);
            $criteria->setIdReference($idReference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getIdAgentConnected());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $this->_consultation = array_shift($consultation);
                $this->setViewState('consultation', $this->_consultation);
                $this->dateRemisePlis->text = Atexo_Util::iso2frnDateTime(substr($this->_consultation->getDatefin(), 0, 16));
                $this->datemiseEnLigne->value = Atexo_Util::iso2frnDateTime(substr($this->_consultation->getDatemiseenligne(), 0, 16));
                if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')
                   || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                   || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV')
                   || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT')
                   || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION')
                   ) {
                    $this->idValidateDateLimiteEchantillion->setVisible(false);
                    $this->idValidateDateReunion->setVisible(false);
                    $this->idValidateDateVisiteLieux->setVisible(false);
                    $this->dateRemisePlisValidateur->setVisible(false);
                }

                if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                    if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                        $this->displayStatutConsultation($this->_consultation);
                        if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                            if (!$this->getIsPostBack()) {
                                $this->rechargerPub();
                            }
                        }
                    } else {
                        $this->etape1->setVisible(false);
                    }

                    if ('1' != $this->_consultation->getEtatValidation()) {
                        if ($this->_consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')
                            && '1' == $this->_consultation->getChiffrementOffre()) {
                            $messageError = Prado::localize('VALIDATION_IMPOSSIBLE');
                        } else {
                            $this->_typeValidation = (new Atexo_Consultation())->retrieveTypeValidation($this->_consultation);
                            if ($this->_typeValidation) {
                                if (!$this->IsPostBack) {
                                    $this->displayAffichage();
                                    $this->displayBlocsValidations();
                                } else {
                                    $this->consultationSummary->setWithException(false);
                                    $this->consultationAdditionalInformations->setWithException(false);
                                    $this->consultationReplyTerms->setWithException(false);
                                }
                            } else {
                                $messageError = Prado::localize('ERREUR_HABILITATION_VALIDATION');
                            }
                        }

                        $this->validationTncpPublic->setVisible(false);
                        $configurationOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
                        if (empty($this->_consultation->getCodeProcedure()) && $configurationOrganisme instanceof BaseCommonConfigurationOrganisme && $configurationOrganisme->getModuleTncp()) {
                            $this->validationTncpPublic->setVisible(true);
                        }
                    } else {
                        $messageError = Prado::localize('CONSULTATION_DEJA_VALIDER');
                    }
                } else {
                    $messageError = Prado::localize('CONSULTATION_PAS_PREPARATION');
                }
            } else {
                $messageError = Prado::localize('TEXT_AUCUNE_CONSULTATION');
            }
        } else {
            $messageError = Prado::localize('ERREUR_AUCUNE_CONSULTATION');
        }

        if ($messageError) {
            $this->consultationSummary->setWithException(false);
            $this->consultationAdditionalInformations->setWithException(false);
            $this->consultationReplyTerms->setWithException(false);
            $this->validationTncpPublic->setVisible(false);
            $this->panelRegleValidation->setVisible(false);
            $this->panelCandidature->setVisible(false);
            $this->panelOffre->setVisible(false);
            $this->panelAnonymat->setVisible(false);
            $this->panelOffreTechnique->setVisible(false);
            $this->panelButtons->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelRetour->setVisible(true);
            $this->panelMessageErreur->setMessage($messageError);
        }

        $this->verifierTraductionObligatoire();
    }

    public function displayStatutConsultation($consultation)
    {
        $dateAjourdhui = date('Y-m-d H:i:s');
        if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            $this->etape1->SetId('etape0');
        } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            $this->etape1->SetId('etape1');
        } elseif ($consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1') && $consultation->getDatefin() > $dateAjourdhui) {
            $this->etape1->SetId('etape234');
        } elseif (($consultation->getStatusConsultation() == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')))
        || ($consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_0') && $consultation->getDatefin() <= $dateAjourdhui)) {
            $this->etape1->SetId('etape34');
        } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            $this->etape1->SetId('etape2');
        } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            $this->etape1->SetId('etape3');
        } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
            $this->etape1->SetId('etape4');
        }
    }

    public function displayAffichage()
    {
        $this->consultationSummary->setConsultation($this->_consultation);
        if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $this->consultationAdditionalInformations->setConsultation($this->_consultation);
            $this->consultationReplyTerms->setConsultation($this->_consultation);
        } else {
            $this->consultationAdditionalInformations->setWithException(false);
            $this->consultationReplyTerms->setWithException(false);
        }

        if ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_SIMPLE')) {
            $pathEnttityPurchase = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme);
            $this->panelValidationSimple->setVisible(true);
            $this->panelValidationSimple->setCssClass('validation-etape-on');
            $this->panelValidationIntermediaire->setVisible(false);
            $this->panelValidationFinale->setVisible(false);
            $this->eniteAchat3->Text = $pathEnttityPurchase;

            $this->panelCandidature->setVisible(false);
            $this->panelOffre->setVisible(false);
            $this->panelAnonymat->setVisible(false);
            $this->panelOffreTechnique->setVisible(false);
        } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE')) {
            $pathEnttityPurchase = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme);
            $this->numeroEtape->Text = '1/1';
            $this->panelValidationIntermediaire->setVisible(false);
            $this->panelValidationSimple->setVisible(false);

            $this->panelValidationFinale->setVisible(true);
            $this->panelValidationFinale->setCssClass('validation-etape-on');
            $this->eniteAchat2->Text = $pathEnttityPurchase;
            if ('1' == $this->_consultation->getChiffrementOffre() && '1' == $this->_consultation->getAutoriserReponseElectronique()) {
                $this->_showBlocAffectationCle = true;
            }
        } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_INTERMEDIAIRE_ETAPE1')) {
            $this->numeroEtape->Text = '2/2';
            $pathEnttityPurchaseFinal = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme);
            $pathEnttityPurchaseIntermidiaire = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidationIntermediaire(), $this->_organisme);
            $this->panelValidationSimple->setVisible(false);

            $this->panelValidationIntermediaire->setVisible(true);
            $this->panelValidationIntermediaire->setCssClass('validation-etape-on');
            $this->eniteAchat1->Text = $pathEnttityPurchaseIntermidiaire;

            $this->panelValidationFinale->setVisible(true);
            $this->panelValidationFinale->setCssClass('validation-etape');
            $this->eniteAchat2->Text = $pathEnttityPurchaseFinal;

            $this->panelCandidature->setVisible(false);
            $this->panelOffre->setVisible(false);
            $this->panelAnonymat->setVisible(false);
            $this->panelOffreTechnique->setVisible(false);
        } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE_ETAPE2')) {
            $pathEnttityPurchaseFinal = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme);
            $pathEnttityPurchaseIntermidiaire = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidationIntermediaire(), $this->_organisme);

            $this->numeroEtape->Text = '2/2';

            $this->panelValidationSimple->setVisible(false);

            $this->panelValidationIntermediaire->setVisible(true);
            $this->panelValidationIntermediaire->setCssClass('validation-etape');
            $this->eniteAchat1->Text = $pathEnttityPurchaseIntermidiaire;

            $this->panelValidationFinale->setVisible(true);
            $this->panelValidationFinale->setCssClass('validation-etape-on');
            $this->eniteAchat2->Text = $pathEnttityPurchaseFinal;

            if ('1' == $this->_consultation->getChiffrementOffre() && '1' == $this->_consultation->getAutoriserReponseElectronique()) {
                $this->_showBlocAffectationCle = true;
            }
        }
    }

    public function displayBlocsValidations()
    {
        if ($this->_showBlocAffectationCle) {
            $services = [];
            $services[] = $this->_consultation->getServiceId();
            if ($this->_consultation->getServiceAssocieId() != $this->_consultation->getServiceId()) {
                $services[] = $this->_consultation->getServiceAssocieId();
            }
            if ($this->_consultation->getServiceValidation() != $this->_consultation->getServiceId()) {
                $services[] = $this->_consultation->getServiceValidation();
            }

            if ('0' != $this->_consultation->getEnvCandidature() && !$this->_consultation->getModeOuvertureReponse()) {
                $this->panelCandidature->setVisible(true);
                $this->BiCleCandidature->setIdService($this->_consultation->getServiceId());
                $this->BiCleCandidature->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->BiCleCandidature->setIdServiceValidation($this->_consultation->getServiceValidation());
                $this->BiCleCandidature->fillRepeater();
                $this->BiCleUniverselleCandidature->fillRepeaterCleUniverselle($this->_consultation->getOrganisme());

                $this->biCleCandidaturePermanent->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleCandidaturePermanent->setServicesIds($services);
                $this->biCleCandidaturePermanent->setOrganisme($this->_organisme);
                $this->biCleCandidaturePermanent->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_REQUIS'));
                $this->biCleCandidaturePermanent->fillDataList();

                $this->biCleCandidatureSuppleant1->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleCandidatureSuppleant1->setServicesIds($services);
                $this->biCleCandidatureSuppleant1->setOrganisme($this->_organisme);
                $this->biCleCandidatureSuppleant1->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleCandidatureSuppleant1->fillDataList();

                $this->biCleCandidatureSuppleant2->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleCandidatureSuppleant2->setServicesIds($services);
                $this->biCleCandidatureSuppleant2->setOrganisme($this->_organisme);
                $this->biCleCandidatureSuppleant2->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleCandidatureSuppleant2->fillDataList();

                $this->biCleCandidatureSuppleant3->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleCandidatureSuppleant3->setServicesIds($services);
                $this->biCleCandidatureSuppleant3->setOrganisme($this->_organisme);
                $this->biCleCandidatureSuppleant3->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleCandidatureSuppleant3->fillDataList();
            } else {
                $this->panelCandidature->setVisible(false);
            }

            if ('0' != $this->_consultation->getEnvOffre() && !$this->_consultation->getModeOuvertureReponse()) {
                $this->panelOffre->setVisible(true);
                $this->BiCleOffre->setIdService($this->_consultation->getServiceId());
                $this->BiCleOffre->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->BiCleOffre->fillRepeater();
                $this->BiCleUniverselleOffre->fillRepeaterCleUniverselle($this->_consultation->getOrganisme());

                $this->biCleOffrePermanent->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffrePermanent->setServicesIds($services);
                $this->biCleOffrePermanent->setOrganisme($this->_organisme);
                $this->biCleOffrePermanent->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_REQUIS'));
                $this->biCleOffrePermanent->fillDataList();

                $this->biCleOffreSuppleant1->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreSuppleant1->setServicesIds($services);
                $this->biCleOffreSuppleant1->setOrganisme($this->_organisme);
                $this->biCleOffreSuppleant1->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreSuppleant1->fillDataList();

                $this->biCleOffreSuppleant2->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreSuppleant2->setServicesIds($services);
                $this->biCleOffreSuppleant2->setOrganisme($this->_organisme);
                $this->biCleOffreSuppleant2->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreSuppleant2->fillDataList();

                $this->biCleOffreSuppleant3->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreSuppleant3->setServicesIds($services);
                $this->biCleOffreSuppleant3->setOrganisme($this->_organisme);
                $this->biCleOffreSuppleant3->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreSuppleant3->fillDataList();
            } else {
                $this->panelOffre->setVisible(false);
            }

            if ('0' != $this->_consultation->getEnvAnonymat() && !$this->_consultation->getModeOuvertureReponse()) {
                $this->panelAnonymat->setVisible(true);
                $this->BiCleAnonymat->setIdService($this->_consultation->getServiceId());
                $this->BiCleAnonymat->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->BiCleAnonymat->fillRepeater();
                $this->BiCleUniverselleAnonymat->fillRepeaterCleUniverselle($this->_consultation->getOrganisme());

                $this->biCleAnonymatPermanent->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleAnonymatPermanent->setServicesIds($services);
                $this->biCleAnonymatPermanent->setOrganisme($this->_organisme);
                $this->biCleAnonymatPermanent->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_REQUIS'));
                $this->biCleAnonymatPermanent->fillDataList();

                $this->biCleAnonymatSuppleant1->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleAnonymatSuppleant1->setServicesIds($services);
                $this->biCleAnonymatSuppleant1->setOrganisme($this->_organisme);
                $this->biCleAnonymatSuppleant1->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleAnonymatSuppleant1->fillDataList();

                $this->biCleAnonymatSuppleant2->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleAnonymatSuppleant2->setServicesIds($services);
                $this->biCleAnonymatSuppleant2->setOrganisme($this->_organisme);
                $this->biCleAnonymatSuppleant2->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleAnonymatSuppleant2->fillDataList();

                $this->biCleAnonymatSuppleant3->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleAnonymatSuppleant3->setServicesIds($services);
                $this->biCleAnonymatSuppleant3->setOrganisme($this->_organisme);
                $this->biCleAnonymatSuppleant3->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleAnonymatSuppleant3->fillDataList();
            } else {
                $this->panelAnonymat->setVisible(false);
            }

            if ('0' != $this->_consultation->getEnvOffreTechnique() && !$this->_consultation->getModeOuvertureReponse()) {
                $this->panelOffreTechnique->setVisible(true);
                $this->BiCleOffreTechnique->setIdService($this->_consultation->getServiceId());
                $this->BiCleOffreTechnique->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->BiCleOffreTechnique->setIdServiceValidation($this->_consultation->getServiceValidation());
                $this->BiCleOffreTechnique->fillRepeater();
                $this->BiCleUniverselleOffreTechnique->fillRepeaterCleUniverselle($this->_consultation->getOrganisme());

                $this->biCleOffreTechniquePermanent->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreTechniquePermanent->setServicesIds($services);
                $this->biCleOffreTechniquePermanent->setOrganisme($this->_organisme);
                $this->biCleOffreTechniquePermanent->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_REQUIS'));
                $this->biCleOffreTechniquePermanent->fillDataList();

                $this->biCleOffreTechniqueSuppleant1->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreTechniqueSuppleant1->setServicesIds($services);
                $this->biCleOffreTechniqueSuppleant1->setOrganisme($this->_organisme);
                $this->biCleOffreTechniqueSuppleant1->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreTechniqueSuppleant1->fillDataList();

                $this->biCleOffreTechniqueSuppleant2->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreTechniqueSuppleant2->setServicesIds($services);
                $this->biCleOffreTechniqueSuppleant2->setOrganisme($this->_organisme);
                $this->biCleOffreTechniqueSuppleant2->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreTechniqueSuppleant2->fillDataList();

                $this->biCleOffreTechniqueSuppleant3->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleOffreTechniqueSuppleant3->setServicesIds($services);
                $this->biCleOffreTechniqueSuppleant3->setOrganisme($this->_organisme);
                $this->biCleOffreTechniqueSuppleant3->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleOffreTechniqueSuppleant3->fillDataList();
            } else {
                $this->panelOffreTechnique->setVisible(false);
            }

            if ($this->_consultation->getModeOuvertureReponse()) {
                $this->panelReponse->setVisible(true);
                $this->BiCleReponse->setIdService($this->_consultation->getServiceId());
                $this->BiCleReponse->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->BiCleReponse->setIdServiceValidation($this->_consultation->getServiceValidation());
                $this->BiCleReponse->fillRepeater();
                $this->BiCleUniverselleReponse->fillRepeaterCleUniverselle($this->_consultation->getOrganisme());

                $this->biCleReponsePermanent->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleReponsePermanent->setServicesIds($services);
                $this->biCleReponsePermanent->setOrganisme($this->_organisme);
                $this->biCleReponsePermanent->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_REQUIS'));
                $this->biCleReponsePermanent->fillDataList();

                $this->biCleReponseSuppleant1->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleReponseSuppleant1->setServicesIds($services);
                $this->biCleReponseSuppleant1->setOrganisme($this->_organisme);
                $this->biCleReponseSuppleant1->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleReponseSuppleant1->fillDataList();

                $this->biCleReponseSuppleant2->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleReponseSuppleant2->setServicesIds($services);
                $this->biCleReponseSuppleant2->setOrganisme($this->_organisme);
                $this->biCleReponseSuppleant2->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleReponseSuppleant2->fillDataList();

                $this->biCleReponseSuppleant3->setTypeBiCle(Atexo_Config::getParameter('BI_CLE_PERMANENT'));
                $this->biCleReponseSuppleant3->setServicesIds($services);
                $this->biCleReponseSuppleant3->setOrganisme($this->_organisme);
                $this->biCleReponseSuppleant3->setEntete('--- '.Prado::localize('SELECTIONNEZ_BI_CLE_PERMANENT_OPTIONEL'));
                $this->biCleReponseSuppleant3->fillDataList();
            } else {
                $this->panelReponse->setVisible(false);
            }
        } else {
            $this->panelCandidature->setVisible(false);
            $this->panelOffre->setVisible(false);
            $this->panelAnonymat->setVisible(false);
            $this->panelOffreTechnique->setVisible(false);
            $this->panelReponse->setVisible(false);
        }
    }

    /**
     * @param $cons
     */
    public function envoyerAnnonce($cons)
    {
        if (true === $cons->getIsEnvoiPubliciteValidation()) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($cons instanceof CommonConsultation) {
                $serviceSF = Atexo_Util::getSfService(EchangeConcentrateur::class);
                try {
                    $serviceSF->envoyerAnnonces($cons->getId(), Atexo_Config::getParameter('UID_PF_MPE'));
                } catch (ConnexionWsException $e) {
                    $logger->error($e->getMessage().' '.$e->getTraceAsString());
                } catch (Exception $e) {
                    $logger->error(
                        "Probleme lors de l'envoi des annonces "
                        .$e->getMessage()
                        .' '.$e->getTraceAsString()
                    );
                }
            }
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @return bool|void
     *
     * @throws \Application\Library\Propel\Exception\PropelException
     */
    public function validateConsultation($sender, $param)
    {
        if ($this->isValid) {
            $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $commonConnexion->beginTransaction();

            try {
                if (!$this->verifierTraductionObligatoire()) {
                    return;
                }

                $commonCons = CommonConsultationPeer::retrieveByPK($this->_consultation->getId(), $commonConnexion);


            //On sauvegarde le mode chiffrement PKCS7 ou non
            $pkcs7 = false;
            $configOrganisme =
                (new Atexo_Module())->getModuleOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            if ($configOrganisme instanceof CommonConfigurationOrganisme) {
                $pkcs7 = $configOrganisme->getCmsActif();
            }
            $commonCons->setCmsActif($pkcs7);

            //On fixe la taille des fichiers pour le dépôt
            $controleTailleDepot = null;
            $res = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
            if ($res instanceof CommonConfigurationPlateforme
                && !is_null($res->getControleTailleDepot())) {
                $controleTailleDepot = $res->getControleTailleDepot();
            }
            $commonCons->setControleTailleDepot($controleTailleDepot);

            if(!Atexo_Util::isSiretValide($commonCons->getSiretAcheteur())) {
                if(Atexo_Module::isEnabled('InterfaceDume')) {
                    $this->panelMessageErreurDume->setMessage(
                        Prado::localize('MSG_CONTROLE_SIRET_ACHETEUR_CREATION_CONSULTATION')
                        );

                        $this->panelMessageErreurDume->setDisplay('Dynamic');
                        $this->panelBlocErreurDume->render($param->getNewWriter());
                    } else {
                        $this->panelMessageErreur->setVisible(true);

                        $this->panelMessageErreur->setMessage(
                            Prado::localize('MSG_CONTROLE_SIRET_ACHETEUR_CREATION_CONSULTATION')
                        );
                    }

                    return false;
                }

                if (Atexo_Module::isEnabled('InterfaceDume')) {
                    if (1 == $commonCons->getDumeDemande()) {
                        if (!Atexo_Util::isSiretValide($commonCons->getSiretAcheteur())) {
                            $tDumeContexte = Atexo_Dume_AtexoDume::getTDumeContexte($commonCons);
                            if ($tDumeContexte instanceof CommonTDumeContexte) {
                                $tDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_SUPPRIMER'));
                                $tDumeContexte->save($commonConnexion);
                                $commonCons->setDumeDemande(0);
                            }
                        }

                        $retour = Atexo_Dume_AtexoDume::validerDumeAcheteur($commonCons, 'VALIDATION');

                        if (is_array($retour) && true == $retour['erreur']) {
                            $this->panelMessageErreurDume->setMessage($retour['messageErreur']);
                            $this->panelMessageErreurDume->setDisplay('Dynamic');
                            $this->panelBlocErreurDume->render($param->getNewWriter());

                            return false;
                        }
                    }
                }

                if ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_SIMPLE')) {
                    $commonCons->setDatevalidation(date('Y-m-d H:i'));
                    $commonCons->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                    $commonCons->setServiceValidation(Atexo_CurrentUser::getCurrentServiceId());
                    $commonCons->setIdValideur(Atexo_CurrentUser::getIdAgentConnected());
                    $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne($commonCons);
                    $commonCons->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                    $this->envoyerAnnonce($commonCons);
                    $commonCons->save($commonConnexion);
                } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_INTERMEDIAIRE_ETAPE1')) {
                    $dateValidationIntermediaire = date('Y-m-d H:i');
                    $commonCons->setEtatApprobation('1');
                    $commonCons->setIdApprobateur(Atexo_CurrentUser::getIdServiceAgentConnected());
                    $commonCons->setDateValidationIntermediaire($dateValidationIntermediaire);
                    $this->envoyerAnnonce($commonCons);
                    $commonCons->save($commonConnexion);
                //Atexo_
                } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE') ||
                     $this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE_ETAPE2')) {
                    //$biCleSecours=Atexo_Certificat::retrieveReliefPermanentCertificatByService($this->_idService,  $this->_organisme);
                    $biCleSecours = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($commonCons->getServiceId(), $this->_organisme);
                    $certificatsUniverselle = (new Atexo_Certificat())->retrievePermanentCertificatUniverselle($commonCons->getOrganisme());
                    if ($commonCons->getServiceAssocieId() && $commonCons->getServiceAssocieId() != $commonCons->getServiceId()) {
                        $biCleSecourServiceAssocie = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($commonCons->getServiceAssocieId(), $this->_organisme);
                        $biCleSecours = array_merge($biCleSecours, $biCleSecourServiceAssocie);
                    }
                    if ($commonCons->getServiceValidation() && $commonCons->getServiceValidation() != $commonCons->getServiceId() && $commonCons->getServiceValidation() != $commonCons->getServiceAssocieId()) {
                        $biCleSecourServiceValidation = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($commonCons->getServiceValidation(), $this->_organisme);
                        $biCleSecours = array_merge($biCleSecours, $biCleSecourServiceValidation);
                    }
                    $biCleSecours = (new Atexo_Certificat())->removeDoublons($biCleSecours);

                    $lots = $commonCons->getAllLots();
                    $nombreLots = is_countable($lots) ? count($lots) : 0;

                    if('1' == $commonCons->getChiffrementOffre()) {
                        if ('0' != $commonCons->getEnvCandidature()) {
                            $indexCandidature = 1;
                            if (!empty($this->biCleCandidaturePermanent->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponsePermanent->getSelectedValue()))) {
                                $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                $commonCertificatChiffrement->setSousPli('0');
                                $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponsePermanent->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleCandidaturePermanent->getSelectedValue();
                                }
                                $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                    $certificat = $certificatPermanent->getCertificat();
                                } else {
                                    throw new Exception('Error');
                                }
                                $commonCertificatChiffrement->setCertificat($certificat);
                                $commonCertificatChiffrement->save($commonConnexion);
                            }

                            if (!empty($this->biCleCandidatureSuppleant1->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant1->getSelectedValue()))) {
                                ++$indexCandidature;
                                $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                $commonCertificatChiffrement->setSousPli('0');
                                $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant1->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleCandidatureSuppleant1->getSelectedValue();
                                }
                                $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                    $certificat = $certificatPermanent->getCertificat();
                                } else {
                                    throw new Exception('Error');
                                }
                                $commonCertificatChiffrement->setCertificat($certificat);
                                $commonCertificatChiffrement->save($commonConnexion);
                            }

                            if (!empty($this->biCleCandidatureSuppleant2->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant2->getSelectedValue()))) {
                                ++$indexCandidature;
                                $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                $commonCertificatChiffrement->setSousPli('0');
                                $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant2->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleCandidatureSuppleant2->getSelectedValue();
                                }
                                $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                    $certificat = $certificatPermanent->getCertificat();
                                } else {
                                    throw new Exception('Error');
                                }
                                $commonCertificatChiffrement->setCertificat($certificat);
                                $commonCertificatChiffrement->save($commonConnexion);
                            }

                            if (!empty($this->biCleCandidatureSuppleant3->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant3->getSelectedValue()))) {
                                ++$indexCandidature;
                                $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                $commonCertificatChiffrement->setSousPli('0');
                                $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant3->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleCandidatureSuppleant3->getSelectedValue();
                                }
                                $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                    $certificat = $certificatPermanent->getCertificat();
                                } else {
                                    throw new Exception('Error');
                                }
                                $commonCertificatChiffrement->setCertificat($certificat);
                                $commonCertificatChiffrement->save($commonConnexion);
                            }

                            if (is_array($biCleSecours)) {
                                foreach ($biCleSecours as $biCleSecour) {
                                    ++$indexCandidature;
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                    $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                    $commonCertificatChiffrement->save($commonConnexion);
                                }
                            }

                            if ($certificatsUniverselle && is_array($certificatsUniverselle)) {
                                foreach ($certificatsUniverselle as $certificatUniverselle) {
                                    ++$indexCandidature;
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexCandidature);
                                    $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                    $commonCertificatChiffrement->save($commonConnexion);
                                }
                            }
                        }

                        if ('0' != $commonCons->getEnvOffre()) {
                            $indexOffre = 1;
                            if (!empty($this->biCleOffrePermanent->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponsePermanent->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponsePermanent->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffrePermanent->getSelectedValue();
                                }
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreSuppleant1->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant1->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant1->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreSuppleant1->getSelectedValue();
                                }
                                ++$indexOffre;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreSuppleant2->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant2->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant2->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreSuppleant2->getSelectedValue();
                                }
                                ++$indexOffre;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreSuppleant3->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant3->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant3->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreSuppleant3->getSelectedValue();
                                }
                                ++$indexOffre;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (is_array($biCleSecours)) {
                                foreach ($biCleSecours as $biCleSecour) {
                                    ++$indexOffre;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                            $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }

                            if ($certificatsUniverselle && is_array($certificatsUniverselle)) {
                                foreach ($certificatsUniverselle as $certificatUniverselle) {
                                    ++$indexOffre;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                        $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexOffre);
                                            $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }
                        }

                        if ('0' != $commonCons->getEnvAnonymat()) {
                            $indexAnonymat = 1;
                            $nombreLots = is_countable($commonCons->getAllLots()) ? count($commonCons->getAllLots()) : 0;

                            if (!empty($this->biCleAnonymatPermanent->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponsePermanent->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponsePermanent->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleAnonymatPermanent->getSelectedValue();
                                }
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleAnonymatSuppleant1->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant1->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant1->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleAnonymatSuppleant1->getSelectedValue();
                                }
                                ++$indexAnonymat;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleAnonymatSuppleant2->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant2->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant2->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleAnonymatSuppleant2->getSelectedValue();
                                }
                                ++$indexAnonymat;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleAnonymatSuppleant3->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant3->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant3->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleAnonymatSuppleant3->getSelectedValue();
                                }
                                ++$indexAnonymat;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (is_array($biCleSecours)) {
                                foreach ($biCleSecours as $biCleSecour) {
                                    ++$indexAnonymat;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                            $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }

                            if ($certificatsUniverselle && is_array($certificatsUniverselle)) {
                                foreach ($certificatsUniverselle as $certificatUniverselle) {
                                    ++$indexAnonymat;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                        $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexAnonymat);
                                            $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }
                        }

                        if ('0' != $commonCons->getEnvOffreTechnique()) {
                            $indexOffreTechnique = 1;
                            $nombreLots = is_countable($commonCons->getAllLots()) ? count($commonCons->getAllLots()) : 0;
                            if (!empty($this->biCleOffreTechniquePermanent->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponsePermanent->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponsePermanent->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreTechniquePermanent->getSelectedValue();
                                }
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreTechniqueSuppleant1->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant1->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant1->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreTechniqueSuppleant1->getSelectedValue();
                                }
                                ++$indexOffreTechnique;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreTechniqueSuppleant2->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant2->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant2->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreTechniqueSuppleant2->getSelectedValue();
                                }
                                ++$indexOffreTechnique;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (!empty($this->biCleOffreTechniqueSuppleant3->getSelectedValue()) || ($commonCons->getModeOuvertureReponse(
                                    ) && !empty($this->biCleReponseSuppleant3->getSelectedValue()))) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    $biCleSelected = $this->biCleReponseSuppleant3->getSelectedValue();
                                } else {
                                    $biCleSelected = $this->biCleOffreTechniqueSuppleant3->getSelectedValue();
                                }
                                ++$indexOffreTechnique;
                                if (0 == $nombreLots) {
                                    $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                    $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                    $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                    $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                    $commonCertificatChiffrement->setSousPli('0');
                                    $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                    $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                    if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                        $certificat = $certificatPermanent->getCertificat();
                                    } else {
                                        throw new Exception('Error');
                                    }
                                    $commonCertificatChiffrement->setCertificat($certificat);
                                    $commonCertificatChiffrement->save($commonConnexion);
                                } else {
                                    foreach ($lots as $lot) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli($lot->getLot());
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $certificatPermanent = (new Atexo_Certificat())->retrieveCertificatById($biCleSelected, $this->_organisme);
                                        if ($certificatPermanent instanceof CommonCertificatPermanent) {
                                            $certificat = $certificatPermanent->getCertificat();
                                        } else {
                                            throw new Exception('Error');
                                        }
                                        $commonCertificatChiffrement->setCertificat($certificat);
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    }
                                }
                            }

                            if (is_array($biCleSecours)) {
                                foreach ($biCleSecours as $biCleSecour) {
                                    ++$indexOffreTechnique;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                            $commonCertificatChiffrement->setCertificat($biCleSecour->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }

                            if ($certificatsUniverselle && is_array($certificatsUniverselle)) {
                                foreach ($certificatsUniverselle as $certificatUniverselle) {
                                    ++$indexOffreTechnique;
                                    if (0 == $nombreLots) {
                                        $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                        $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                        $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                        $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                        $commonCertificatChiffrement->setSousPli('0');
                                        $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                        $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                        $commonCertificatChiffrement->save($commonConnexion);
                                    } else {
                                        foreach ($lots as $lot) {
                                            $commonCertificatChiffrement = new CommonCertificatChiffrement();
                                            $commonCertificatChiffrement->setOrganisme($this->_organisme);
                                            $commonCertificatChiffrement->setConsultationId($commonCons->getId());
                                            $commonCertificatChiffrement->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                            $commonCertificatChiffrement->setSousPli($lot->getLot());
                                            $commonCertificatChiffrement->setIndexCertificat($indexOffreTechnique);
                                            $commonCertificatChiffrement->setCertificat($certificatUniverselle->getCertificat());
                                            $commonCertificatChiffrement->save($commonConnexion);
                                        }
                                    }
                                }
                            }
                        }

                        if (0 != $this->_consultation->getAutoriserReponseElectronique()) {
                            if (!$commonCons->getEnvCandidature()
                                && !$commonCons->getEnvOffre()
                                && !$commonCons->getEnvAnonymat()
                                && !$commonCons->getEnvOffreTechnique()
                            ) {
                                $this->panelMessageErreur->setMessage(
                                    Prado::localize('ERREUR_AUCUNE_ENVELOPPE')
                                );
                                $this->panelMessageErreur->setVisible(true);

                                return false;
                            }
                            if ('1' == $commonCons->getChiffrementOffre()) {
                                if ($commonCons->getModeOuvertureReponse()) {
                                    if (0 == $this->biCleReponsePermanent->getSelectedValue()) {
                                        $this->panelMessageErreur->setMessage(
                                            Prado::localize('ERREUR_PAS_DE_CLE_CHIFFREMENT')
                                        );
                                        $this->panelMessageErreur->setVisible(true);

                                        return false;
                                    }
                                } else {
                                    if ((
                                            '0' != $commonCons->getEnvCandidature() &&
                                            0 == $this->biCleCandidaturePermanent->getSelectedValue()
                                        )
                                        ||
                                        (
                                            '0' != $commonCons->getEnvOffre() &&
                                            0 == $this->biCleOffrePermanent->getSelectedValue()
                                        )
                                        ||
                                        (
                                            '0' != $commonCons->getEnvAnonymat() &&
                                            0 == $this->biCleAnonymatPermanent->getSelectedValue()
                                        )
                                        ||
                                        (
                                            '0' != $commonCons->getEnvOffreTechnique() &&
                                            0 == $this->biCleOffreTechniquePermanent->getSelectedValue()
                                        )
                                    ) {
                                        $this->panelMessageErreurDume->setDisplay('Dynamic');
                                        $this->panelMessageErreurDume->setMessage(
                                            Prado::localize('ERREUR_PAS_DE_CLE_CHIFFREMENT')
                                        );
                                        $this->panelBlocErreurDume->render($param->getNewWriter());

                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    $commonCons->setDatevalidation(date('Y-m-d H:i'));
                    $commonCons->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                    $commonCons->setServiceValidation(Atexo_CurrentUser::getCurrentServiceId());
                    $commonCons->setIdValideur(Atexo_CurrentUser::getIdAgentConnected());
                    $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne($commonCons);
                    $commonCons->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                    $this->envoyerAnnonce($commonCons);
                    $commonCons->save($commonConnexion);
                }
                // Traitement des alertes
                if (Atexo_Module::isEnabled('AlerteMetier')) {
                    $alertesBloquante = Atexo_AlerteMetier::lancerAlerte('ValiderConsultation', $commonCons, $commonConnexion);
                    if (is_array($alertesBloquante) && count($alertesBloquante) > 0) {
                        $this->panelMessageErreur->setVisible(true);
                        $messageErreur = Atexo_Util::getMessageFromListe('Alertes :', $alertesBloquante);
                        $this->panelMessageErreur->setMessage($messageErreur);
                        $commonConnexion->rollback();

                        return false;
                    }
                }
                if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $commonCons->getDumeDemande()
                &&
                (
                   Atexo_Util::validateDate($commonCons->getDateMiseEnLigneCalcule(), 'Y-m-d H:i:s')
                   &&
                       !empty($commonCons->getDateMiseEnLigneCalcule())
                   &&
                       '0000-00-00 00:00:00' != $commonCons->getDateMiseEnLigneCalcule()
                )
                ) {
                    $retour = Atexo_Dume_AtexoDume::publierDumeAcheteur($commonCons);
                    if (is_array($retour) && true == $retour['erreur']) {
                        $this->panelMessageErreurDume->setMessage($retour['messageErreur']);
                        $this->panelMessageErreurDume->setDisplay('Dynamic');
                        $this->panelBlocErreurDume->render($param->getNewWriter());
                        $commonConnexion->rollback();

                        return false;
                    } else {
                        if (!$retour['isStandard']) {
                            Atexo_Dume_AtexoDume::createJmsJobToPublishDume($commonCons);
                        }
                    }
                }

                // Fin traitement des alertes
                $commonConnexion->commit();

                if ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_INTERMEDIAIRE_ETAPE1')) {
                    $service = $commonCons->getServiceValidation();
                    (new Atexo_Message())->sendMailToAgentsAlertConsAttenteValidation($commonCons, $service);
                }

                $this->_consultation = $commonCons;
                if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $this->urlParam = '';
                    if (Atexo_Config::getParameter('DELAI_DLRO')
                       && $this->_consultation->getDatefin()
                       && Atexo_Util::diffJours(Atexo_Util::iso2frnDate($this->_consultation->getDatedebut()), Atexo_Util::iso2frnDate($this->_consultation->getDatefin())) < Atexo_Config::getParameter('DELAI_DLRO')) {
                        $this->urlParam .= '&etatDateCreationDateFin';
                    }
                    $statut = $this->bloc_PubliciteConsultation->envoyerAnnonce($this->_consultation);
                    if (!$statut) {
                        $this->urlParam .= '&EchecPub';
                    }
                    try {
                        if (Atexo_Module::isEnabled('GenerationAvis')) {

						// debut Recuperation de la liste des langue actives :tableau de langues
						 $languesActives = Atexo_Languages::retrieveActiveLangages();
                            //Fin Recuperation de la liste des langue actives
                            foreach ($languesActives as $lang) {
                                $pathFile = '';
                                $pathFile = (new Atexo_Publicite_AvisPdf())->generateAvis($commonCons, Atexo_Config::getParameter('FICHIER_TEMPLATE_AVIS'), trim($lang->getLangue()), true);
                                if (-1 != $pathFile) {
                                    if (!(new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($pathFile, $commonCons, trim($lang->getLangue()))) {
                                        $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&noAvisGenerated=1'.$this->urlParam);
                                    }
                                }
                            }
                        }
                    } catch (\Exception) {
                        $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&noAvisGenerated=1'.$this->urlParam);
                    }
                    $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).$this->urlParam);
                } else {
                    $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
                }
            } catch (Exception $e) {
                $commonConnexion->rollback();
                $this->displayBlocsValidations();
                $this->consultationSummary->setWithException(false);
                $this->consultationAdditionalInformations->setWithException(false);
                $this->consultationReplyTerms->setWithException(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelRetour->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('VALIDATION_ERREUR'));
                $this->bloc_PubliciteConsultation->setVisible(false);

                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error("Erreur survenue lors de la validation d'une consultation : ".$e->getMessage().$e->getTraceAsString());
            }
        }
    }

    public function redirectToTableauDeBord()
    {
        if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        } else {
            $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    /**
     * Afficher Message d'avertissement pour traduction.
     */
    public function verifierTraductionObligatoire()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->_consultation->getId(), $connexion);
        $validationAutorisee = true;
        $languesObligatoires = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
        foreach ($languesObligatoires as $langueObligatoire) {
            if (!Atexo_Traduction::getTraduction($consultation->getIdTrObjet(), $langueObligatoire->getLangue())) {
                $validationAutorisee = false;
            }
        }
        if (!$validationAutorisee) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_TADUCTION'));
        }

        return $validationAutorisee;
    }

    public function validateDateVisiteLieux($sender, $param)
    {
        if ($this->_consultation) {
            if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                $visitesLieu = (new Atexo_Consultation())->retreiveVisitesLieuxWhereDateIs($this->_consultation->getId(), $this->_consultation->getDatefin(), $this->_organisme, '1');
                $visitesLieuAuj = (new Atexo_Consultation())->retreiveVisitesLieuxWhereDateIs($this->_consultation->getId(), date('Y-m-d h:i:s'), $this->_organisme, '-1');
                if ($visitesLieu) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateVisiteLieux->ErrorMessage = Prado::localize('DATE_VISITE_LIEUX_POSTERIEUR_DATE_FIN');

                    return;
                }
                if ($visitesLieuAuj) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateVisiteLieux->ErrorMessage = Prado::localize('DATE_VISITE_LIEUX_POSTERIEUR_AUJOURDHUI');

                    return;
                }
            } else {
                $param->IsValid = true;

                return;
            }
        }
    }

    public function validateDateReunion($sender, $param)
    {
        if ($this->_consultation) {
            if (Atexo_Module::isEnabled('ConsultationReunion')) {
                $reunion = false;
                $reunionAuj = false;
                if ($this->_consultation->getAlloti()) {
                    $reunion = (new Atexo_DonnesComplementaires())->getReunionLotsWhereDateIs($this->_consultation->getIdDonneeComplementaire(), $this->_consultation->getDatefin(), '1');
                    $reunionAuj = (new Atexo_DonnesComplementaires())->getReunionLotsWhereDateIs($this->_consultation->getIdDonneeComplementaire(), date('Y-m-d h:i:s'), '-1');
                } else {
                    $donnesComplementaitre = $this->_consultation->getDonneComplementaire();
                    if ($donnesComplementaitre instanceof CommonTDonneeComplementaire && $donnesComplementaitre->getReunion()) {
                        if ($donnesComplementaitre->getDateReunion() > $this->_consultation->getDatefin()) {
                            $reunion = true;
                        } elseif ($donnesComplementaitre->getDateReunion() < date('Y-m-d h:i:s')) {
                            $reunionAuj = true;
                        }
                    }
                }

                if ($reunion) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateReunion->ErrorMessage = Prado::localize('DATE_REUNION_POSTERIEUR_DATE_FIN');

                    return '';
                }
                if ($reunionAuj) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateReunion->ErrorMessage = Prado::localize('DATE_REUNION_POSTERIEUR_AUJOURDHUI');

                    return '';
                }
            } else {
                $param->IsValid = true;

                return '';
            }
        }
    }

    public function validateDateLimiteEchantillion($sender, $param)
    {
        if ($this->_consultation) {
            if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                $echantillion = false;
                $echantillionAuj = false;

                if ($this->_consultation->getAlloti()) {
                    $echantillion = (new Atexo_DonnesComplementaires())->getEchantillionLotsWhereDateIs($this->_consultation->getIdDonneeComplementaire(), $this->_consultation->getDatefin(), '1');
                    $echantillionAuj = (new Atexo_DonnesComplementaires())->getEchantillionLotsWhereDateIs($this->_consultation->getIdDonneeComplementaire(), date('Y-m-d h:i:s'), '-1');
                } else {
                    $donnesComplementaitre = $this->_consultation->getDonneComplementaire();
                    if ($donnesComplementaitre instanceof CommonTDonneeComplementaire && $donnesComplementaitre->getEchantillon()) {
                        if ($donnesComplementaitre->getDateLimiteEchantillon() > $this->_consultation->getDatefin()) {
                            $echantillion = true;
                        } elseif ($donnesComplementaitre->getDateLimiteEchantillon() < date('Y-m-d h:i:s')) {
                            $echantillionAuj = true;
                        }
                    }
                }

                if ($echantillion) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateLimiteEchantillion->ErrorMessage = Prado::localize('DATE_ECHANTILLON_POSTERIEUR_DATE_FIN');

                    return '';
                }
                if ($echantillionAuj) {
                    $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->idValidateDateLimiteEchantillion->ErrorMessage = Prado::localize('DATE_ECHANTILLON_POSTERIEUR_DATE_AUJOURDHUI');

                    return '';
                }
            } else {
                $param->IsValid = true;

                return '';
            }
        }
    }

    public function rechargerPub()
    {
        if (Atexo_Module::isEnabled('Publicite')) {
            if ($this->_consultation instanceof CommonConsultation && '1' == $this->_consultation->getDonneePubliciteObligatoire()) {
                if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                    $this->bloc_PubliciteConsultationConcentrateurv2->setVisible(true);
                    $this->bloc_PubliciteConsultationConcentrateurv2->charger($this->_consultation, $this->_consultation->getDonneComplementaire());
                } else {
                    $this->bloc_PubliciteConsultation->setVisible(true);
                    $this->bloc_PubliciteConsultation->charger($this->_consultation, $this->_consultation->getDonneComplementaire());
                }
            }
        }
    }

    /**
     * Permet de faire le controle du siret avec la validation.
     *
     * @param mixed the event sender object $sender
     * @param TEventParameter               $param
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function demandeValidation($sender, $param)
    {
        $validerCons = true;
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $cons = CommonConsultationPeer::retrieveByPK($this->_consultation->getId(), $commonConnexion);
            if ($cons instanceof CommonConsultation) {
                if (1 == $cons->getDumeDemande()) {
                    if (!Atexo_Util::isSiretValide($cons->getSiretAcheteur())) {
                        $validerCons = false;
                        $this->script->text = "<script>openModal('modal-confirmation-validsation','popup-800',document.getElementById('ctl0_CONTENU_PAGE_titrePopin'));</script>";
                    }
                }
            }
        }

        if ($validerCons) {
            $this->validateConsultation($sender, $param);
        }
    }
}
