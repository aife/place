<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe AgentDetailEntiteAchat.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentDetailEntiteAchat extends MpeTPage
{
    protected $critereTri;
    protected $triAscDesc;
    protected $org;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        (new Atexo_EntityPurchase())->reloadCachedEntities(Atexo_CurrentUser::getCurrentOrganism());
        $this->panelMessageErreur->setVisible(false);
        if (isset($_GET['idEntite'])) {
            if ($_GET['idEntite'] != Atexo_CurrentUser::getCurrentServiceId()) {
                if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin') && !Atexo_CurrentUser::hasHabilitation('GestionAgentPole') && !Atexo_CurrentUser::hasHabilitation('GestionAgentPoleSocle')) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelDetailEntiteAchat->setVisible(false);
                    $this->panelMessageErreur->setMessage(Prado::localize('PAS_HABILITATION_POUR_GERER_CE_SERVICE'));
                }
            }
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
            } else {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
            }

            $this->DetailEntiteAchat->_IdEntite = Atexo_Util::atexoHtmlEntities($_GET['idEntite']);
            $this->DetailEntiteAchat->_org = $organisme;
            $this->IDTRepeaterRPA->_ID = $organisme;
            $this->IDTRepeaterRPA->_organisme = $organisme;
            $this->IDTRepeaterRPA->detailRPA(Atexo_Util::atexoHtmlEntities($_GET['idEntite']));

            $this->idretour->NavigateUrl = '?page=Agent.GestionEntitesAchat&org='.$organisme;
        } else {
            exit;
        }
    }
}
