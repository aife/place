<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Prado\Prado;

/**
 * Permet de télécharger l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadArchive extends MpeTPage
{
    private $_connexion;
    private $_refConsultation;
    private $_consultation = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function getConnexion()
    {
        if (null == $this->_connexion) {
            $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }

        return $this->_connexion;
    }

    /**
     * Retourne la consultation (objet Propel).
     *
     * @return consultation
     */
    public function getConsultation()
    {
        if (null == $this->_consultation) {
            $this->consultation = CommonConsultationPeer::retrieveByPK($this->getViewState('refConsultation'), $this->getConnexion());
        }

        return $this->_consultation;
    }

	public function onLoad($param){
        try {
            if (isset($_GET['id'])) {
                $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $this->setViewState("refConsultation", Atexo_Util::atexoHtmlEntities($_GET['id']));
            }

        $fileLog = Atexo_LoggerManager::getLogger('archivage');

        $acronymeOrganisme = Atexo_CurrentUser::getOrganismAcronym();
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme($acronymeOrganisme);
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (is_array($consultation) && 1 == count($consultation)) {
            $consultation = array_shift($consultation);
            if ($consultation instanceof CommonConsultation && ($consultation->isDicTelechargeable() || $consultation->isDacTelechargeable())) {
                $this->_consultation = $consultation;
                // Identification
                if (Atexo_Module::isEnabled('InterfaceArchiveArcadePmi')) {
                    $rpa = (new Atexo_EntityPurchase_RPA())->getResponsabeArchiveByIdService(Atexo_CurrentUser::getIdServiceAgentConnected(), $acronymeOrganisme);
                    if (($rpa instanceof CommonRPA) && $rpa->getOrganisme()) {
                        $nomFichier = $rpa->getOrganisme().'_';
                    }
                }

                if (isset($_GET['DAC'])) {
                    $org = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

                    //initialisation des chemins de stockage racine
                    $cheminDirRac = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$org->getAcronyme();
                    if (Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && $consultation->getDateFin()) {
                        $cheminDirAnnee = Atexo_Util::getAnneeFromDate($consultation->getDateFin());
                    }

                    //l'identifiant du service de rattachement de la consultation s'il n'existe pas alors vaut 0
                    $consultationServiceId = $consultation->getServiceId() ?: 0;
                    //initialisation du nom du fichier de l'archive
                    $downloadedName = (new Atexo_Files())->getNomFileXmlAndZip($consultation, $consultationServiceId);

                    //on vérifie si l'archive est stockée dans la table consultation_archive, que la fragmentation a bien eu lieu et que son état de transmission est finalisée
                    $consultationArchive = (new CommonConsultationArchiveQuery())
                        ->filterByOrganisme($acronymeOrganisme)
                        ->filterByConsultationId($consultation->getId())
                        ->findOne($connexion);

                    //3 cas possibles et ils sont exclusifs : si elle figure dans la table consultation_archive
                    // cas 1 : l'archive est à récupérer en local et il ne s'agit pas d'un téléchargement par lot, $cheminFichierZip est lu directement en BD (cas par défaut)
                    // cas 2 : l'archive est à récupérer à distance et il ne s'agit pas d'un téléchargement par lot, l'archive est téléchargée à distance de manière asynchrone
                    // cas 3 : l'archive est à récupérer en local et il s'agit d'un téléchargement par lot
                    if ($consultationArchive instanceof CommonConsultationArchive) {
                        //cas 1 : par défaut on indique que le chemin de l'archive est en local, la valeur est écrasée si on arrive à la récupérer depuis ATLAS
                        $cheminFichierZip = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();

                        //cas 2 : téléchargement de l'archive à distance de manière asynchrone
                        if (Atexo_Module::isEnabled('ArchivageConsultationSaeExterneTelechargementArchive')) {
                            $fileLog->info('Récupération du DAC depuis ATLAS, module ArchivageConsultationSaeExterneTelechargementArchive actif');
                            if (self::preparerTelechargementAsynchrone($consultation->getId(), $fileLog)) {
                                $this->confirmationPart->setVisible(true);
                                $this->panelConfirmation->setVisible(true);
                                $msgConfirmation = '<span>'.Prado::localize('MESSAGE_CONFIRMATION_DEMANDE_TELECHARGEMENT_PRISE_EN_COMPTE_1').'</span><br/>';
                                $msgConfirmation .= '<span>'.Prado::localize('MESSAGE_CONFIRMATION_DEMANDE_TELECHARGEMENT_PRISE_EN_COMPTE_2').'</span>';
                                $this->panelConfirmation->setMessage($msgConfirmation);

                                return;
                            } else {
                                $this->errorPart->setVisible(true);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::Localize('ERREUR_TELECHARGEMENT_DAC'));
                            }
                        }

                        //cas 3 : l'archive est à récupérer en local et il s'agit d'un téléchargement par lot
                        if (Atexo_Module::isEnabled('ArchiveParLot') && $_GET['lots']) {
                            $fileLog->info('Récupération du DAC en local, module ArchiveParLot actif');

                            $downloadedName = 'DI_'.$this->_consultation->getReferenceUtilisateur().'.zip';
                            $cheminFichierSansZip = rtrim($cheminFichierZip, '.zip');

                            $numerosLots = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
                            $zipFile = Atexo_Config::getParameter('COMMON_TMP').'zipDicArchives'.session_id().time().'.zip';
                            $pathZip = null;

                            foreach ($numerosLots as $oneLot) {
                                if ('0' != $oneLot) {
                                    $nomFichierZip = Atexo_Consultation_Archive::getNomArchiveLot($cheminFichierSansZip, $oneLot);
                                }
                                if (is_file($nomFichierZip)) {
                                    if (!$pathZip) {
                                        $mode = 'CREATE';
                                    }
                                    $pathZip = (new Atexo_Zip())->addFile2Zip($zipFile, $nomFichierZip, $consultation->getId().'_Lot_'.$oneLot.'.zip', $mode);
                                }
                            }
                            $cheminFichierZip = $zipFile;
                            $fileLog->info('Récupération du DAC en local, chemin fichier : '.$cheminFichierZip);
                        }

                        //si jamais l'archive existe (une fois récupérée depuis ATLAS ou bien en local), on l'envoi via le navigateur
                        if (is_file($cheminFichierZip)) {
                            $TOKEN = 'downloadToken';
                            $this->setCookieToken($TOKEN, 'done', false);
                            $this->send($downloadedName.'.zip', $cheminFichierZip, false);
                        } else {
                            $fileLog->error('Erreur telechargement DAC : Le dossier '.$cheminFichierZip.".zip n'existe pas ");
                            $this->errorPart->setVisible(true);
                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::Localize('ERREUR_TELECHARGEMENT_DAC'));
                        }
                    }
                    //soit l'archive ne figure pas dans la table consultation_archive, dans ce cas il faut construire dynamiquement le chemin avec l'ancienne méthode
                    else {
                        $fileLog->info("L'archive n'est pas stockée dans la table consultation_archive pour la consultation ref ".$consultation->getId());

                        $sigleService = '';
                        $cheminDirService = '';
                        $pathZip = null;
                        if ($consultation->getServiceId()) {
                            $c = new Criteria();
                            $c->add(CommonServicePeer::ID, $consultation->getServiceId());
                            $c->add(CommonServicePeer::ORGANISME, $org->getAcronyme());
                            $service = CommonServicePeer::doSelectOne($c, $connexion);
                            if ($service) {
                                $sigleService = trim(Atexo_Util::OterAccents($service->getSigle()));
                                $listeParent = Atexo_EntityPurchase::getAllParents($service->getId(), $org->getAcronyme());
                                for ($index = count($listeParent) - 1; $index >= 0; --$index) {
                                    if ($listeParent[$index]['id']) {
                                        $cheminDirService .= '/'.(str_replace('/', '_', trim(Atexo_Util::OterAccents($listeParent[$index]['libelle']))));
                                    }
                                }
                                $cheminDirService .= '/'.(str_replace('/', '_', trim(Atexo_Util::OterAccents($service->getSigle()))));
                            }
                        } else {
                            $sigleService = trim(Atexo_Util::OterAccents($org->getSigle()));
                        }

                        $nomFichierArchive = (new Atexo_Files())->getOldNomFileXmlAndZip($consultation, $sigleService, $nomFichier);
                        $nomFichierZip = $nomFichierArchive.'.zip';
                        $cheminDir = $cheminDirRac.'/'.$cheminDirAnnee.'/'.$cheminDirService;
                        if (!is_file($cheminDir.'/'.$nomFichierZip)) {
                            $cheminDir = $cheminDirRac.'/'.$cheminDirService;
                        }

                        $TOKEN = 'downloadToken';

                        $numerosLots = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
                        $downloadedName = $nomFichierZip;
                        if (Atexo_Module::isEnabled('ArchiveParLot') && $_GET['lots'] && count($numerosLots) > 0) {
                            $zipFile = Atexo_Config::getParameter('COMMON_TMP').'zipDicArchives'.session_id().time().'.zip';
                            foreach ($numerosLots as $oneLot) {
                                if ('0' != $oneLot) {
                                    $nomFichierZip = Atexo_Consultation_Archive::getNomArchiveLot($nomFichierArchive, $oneLot);
                                }
                                if (is_file($cheminDir.'/'.$nomFichierZip)) {
                                    if (!$pathZip) {
                                        $mode = 'CREATE';
                                    }
                                    $pathZip = (new Atexo_Zip())->addFile2Zip($zipFile, $cheminDir.'/'.$nomFichierZip, $nomFichierZip, $mode);
                                } else {
                                    $fileLog->error("L'archive ZIP du lot : ".$oneLot." n'existe pas, chemin attendu :".$cheminDir.'/'.$nomFichierZip);
                                }
                            }
                            $downloadedName = 'DI_'.$this->_consultation->getReferenceUtilisateur().'.zip';
                            $this->setCookieToken($TOKEN, 'done', false);
                            if (is_file($pathZip)) {
                                $this->send($downloadedName, $pathZip, false);
                            } else {
                                $fileLog->error('Erreur telechargement DAC : Le dossier '.$pathZip." n'existe pas ");
                                $this->errorPart->setVisible(true);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::Localize('ERREUR_TELECHARGEMENT_DAC'));
                            }
                        } else {
                            $this->setCookieToken($TOKEN, 'done', false);
                            $cheminFile = $cheminDir.'/'.$nomFichierZip;
                            if (is_file($cheminFile)) {
                                $this->send($downloadedName.'.zip', $cheminFile, false);
                            } else {
                                $fileLog->error('Erreur telechargement DAC : Le dossier '.$cheminFile." n'existe pas ");
                                $this->errorPart->setVisible(true);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::Localize('ERREUR_TELECHARGEMENT_DAC'));
                            }
                        }
                    }
                } else {
                    $acronymeOrganisme = Atexo_CurrentUser::getOrganismAcronym();
                    $TOKEN = 'downloadToken';
                    $pathZip = null;

                    $numerosLots = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
                    $tmpDir = Atexo_Config::getParameter('COMMON_TMP');
                    if (Atexo_Module::isEnabled('ArchiveParLot') && $_GET['lots'] && count($numerosLots) > 0) {
                        $mode = false;
                        foreach ($numerosLots as $oneLot) {
                            $tree = new Atexo_Consultation_Tree_Arborescence($this->_consultation->getId(), $this->getConnexion(), $acronymeOrganisme, true, $oneLot);
                            $pathZipLot = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $this->_consultation, $acronymeOrganisme, 'archive.xml', true, $oneLot);
                            $zip[$oneLot]['path'] = $pathZipLot;
                            $zip[$oneLot]['nameFile'] = $tree->getNom();
                        }
                        $zipFile = $tmpDir.'zipDacArchives'.session_id().time().'.zip';
                        foreach ($zip as $numLot => $uneArchive) {
                            if (!$pathZip) {
                                $mode = 'CREATE';
                            }
                            $pathZip = (new Atexo_Zip())->addFile2Zip($zipFile, $uneArchive['path'], $nomFichier.$uneArchive['nameFile'].'.zip', $mode);
                        }
                        $downloadedName = 'DI_'.$this->_consultation->getReferenceUtilisateur();
                    } else {
                        $tree = new Atexo_Consultation_Tree_Arborescence($this->_consultation->getId(), $this->getConnexion(), $acronymeOrganisme, true, Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
                        $pathZip = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $this->_consultation, $acronymeOrganisme, 'archive.xml', true, base64_decode($_GET['lots']));
                        $downloadedName = $nomFichier.$this->_consultation->getReferenceUtilisateur(true);
                    }
                    $this->setCookieToken($TOKEN, 'done', false);
                    $this->send($downloadedName.'.zip', $pathZip, false);
                }
            } else {
                echo '1';
                exit;
            }
        } else {
            echo '2';
            exit;
        }
        }catch (\Exception $e) {

            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Le téléchargement du DIC de la consultation id = ' . $this->_consultation->getId() .
                ' à échoué avec pour Erreur : ' . $e->getMessage()
            );

            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::Localize('ERREUR_TELECHARGEMENT_DAC'));
        }
    }

    public function send($nomZip, $pathZip, $delete = true)
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/'.(('.zip' == strrchr($nomZip, '.')) ? 'zip' : 'octet-stream'));
        header('Content-Disposition: attachment; filename="'.$nomZip.'";');
        header('Content-Transfer-Encoding: binary');

        header('Content-Length: '.@filesize($pathZip));
        $fp = @fopen($pathZip, 'r');
        $block_count = floor(@filesize($pathZip) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        $bytes_remaining = @filesize($pathZip) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        for ($i = 0; $i < $block_count; ++$i) {
            echo @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            ob_end_flush();
            flush();
        }
        echo @fread($fp, $bytes_remaining);
        @fclose($fp);
        if ($delete) {
            system('rm -f '.$pathZip.' > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');
        }
        exit;
    }

    public function setCookieToken($cookieName, $cookieValue, $httpOnly = true, $secure = false)
    {
        setcookie(
            $cookieName,
            $cookieValue,
            2_147_483_647,            // expires January 1, 2038
            '/',                   // your path
            $_SERVER['HTTP_HOST'], // your domain
            $secure,               // Use true over HTTPS
            $httpOnly              // Set true for $AUTH_COOKIE_NAME
        );
    }

    /**
     * Méthode qui permet d'initialiser le téléchargement asynchrone d'une archive de consultation.
     *
     * @param $referenceConsultation référence de la consultation
     * @param $fileLog pointeur vers le fichier de log
     *
     * @return bool true si tout se passe bien, false sinon
     */
    private function preparerTelechargementAsynchrone($referenceConsultation, $fileLog)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $listeTelechargementAsynchroneFichiers = new PropelCollection();
            $telechargementAsynchroneFichiers = new CommonTTelechargementAsynchroneFichier();
            $telechargementAsynchroneFichiers->setIdReferenceTelechargement($referenceConsultation);
            $listeTelechargementAsynchroneFichiers[] = $telechargementAsynchroneFichiers;

            if (count($listeTelechargementAsynchroneFichiers) && $listeTelechargementAsynchroneFichiers[0]) {
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
                if ($agent instanceof CommonAgent) {
                    $telechargementAsynchrone = new CommonTTelechargementAsynchrone();
                    $telechargementAsynchrone->setIdAgent($agent->getId());
                    $telechargementAsynchrone->setEmailAgent($agent->getEmail());
                    $telechargementAsynchrone->setNomPrenomAgent($agent->getNom().' '.$agent->getPrenom());
                    $telechargementAsynchrone->setIdServiceAgent($agent->getServiceId());
                    $telechargementAsynchrone->setOrganismeAgent($agent->getOrganisme());
                    $telechargementAsynchrone->setTypeTelechargement(Atexo_Config::getParameter('TYPE_TELECHARGEMENT_ASYNCHRONE_ARCHIVE'));
                    $telechargementAsynchrone->setCommonTTelechargementAsynchroneFichiers($listeTelechargementAsynchroneFichiers, $connexion);
                    $telechargementAsynchrone->save($connexion);
                    $fileLog->error("Preparation du telechargement d'archive asynchrone pour la consultation  ".$referenceConsultation.' OK');
                }

                return true;
            }

            return false;
        } catch (\Exception $e) {
            $fileLog->error("Erreur lors de la preparation du telechargement d'archive asynchrone pour la consultation  ".$referenceConsultation.' : '.$e->getMessage());

            return false;
        }
    }
}
