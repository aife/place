<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Propel\Mpe\CommonAnnonceBoampPeer;
use Application\Propel\Mpe\CommonAnnonceMoniteur;
use Application\Propel\Mpe\CommonAnnonceMoniteurPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupBoampError extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ('BOAMP' == $_GET['annonce'] && (isset($_GET['idAnnonce']))) {
            $annonce = CommonAnnonceBoampPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
            if ($annonce instanceof CommonAnnonceBoamp) {
                $this->TextError->Text = ($annonce->getAnnError());
            }
        } elseif ('MONITEUR' == $_GET['annonce'] && (isset($_GET['idAnnonce']))) {
            $annonce = CommonAnnonceMoniteurPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
            if ($annonce instanceof CommonAnnonceMoniteur) {
                $this->TextError->Text = ($annonce->getMessageError());
            }
        }
    }
}
