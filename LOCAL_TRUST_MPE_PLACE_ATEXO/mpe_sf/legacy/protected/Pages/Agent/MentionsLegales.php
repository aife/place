<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

/*session_name("duae_session");
session_start();*/

class MentionsLegales extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->MentionLegale->Text = Prado::localize('MENTION_LEGALE', [], 'MentionLegale.'.$this->user->getLang());
    }
}
