<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DetailEntrepriseMere;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;

/**
 * Composant Detail de l'Entreprise.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailEntreprise extends DetailEntrepriseMere
{
    public function onPreInit($param)
    {
        parent::onPreInit($param);

        $this->MasterClass = 'Application.layouts.'.(Atexo_Config::getParameter('AFFICHAGE_RESPONSIVE') ? 'ResponsivePopupLayout' : 'PopUpLayout');
    }
}
