<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVISPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupPj extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    /**
     * Verifier la taille maximum du document à ajouter
     * ( 2Mo maximum).
     */
    public function verifyDocTailleCreation($sender, $param)
    {
        $sizedocAttache = $this->ajoutFichier->FileSize;
        if (0 == $sizedocAttache) {
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_FICHIER_VIDE').'<br/>';
        } elseif ($sizedocAttache >= 2 * 1024 * 1024) {
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_TAILLE_DOC', ['ajoutFichier' => $this->ajoutFichier->FileName]).'<br/>';
        }
    }

    /**
     *ajouter un document dans la base.
     */
    public function addPieceJointe($sender, $param)
    {
        //if ($this->IsValid) {
        if ($this->ajoutFichier->HasFile) {
            $infile = Atexo_Config::getParameter('COMMON_TMP').'pieceJointe'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                //commencer le scan Antivirus
                $msg = Atexo_ScanAntivirus::startScan($infile);
                if ($msg) {
                    $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                    return;
                }
                //Fin du code scan Antivirus

                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $atexoBlob = new Atexo_Blob();
                $atexoCrypto = new Atexo_Crypto();
                $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                $avis = CommonAVISPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if (is_array($arrayTimeStampAvis)) {
                    $avis->setHorodatage($arrayTimeStampAvis['horodatage']);
                    $avis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                }
                $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, Atexo_CurrentUser::getCurrentOrganism());
                $avis->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                $avis->setAvis($avisIdBlob);
                $avis->setNomFichier($this->ajoutFichier->FileName);
                $avis->setStatut(Atexo_Config::getParameter('STATUT_EN_ATTENTE'));
                $avis->save($connexionCom);
            }
            @unlink($infile);
        }
        $this->labelClose->Text = '<script>refreshRepeaterFormLibre();window.close();</script>';
        // }
    }
}
