<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiConnecteur extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        echo '<html> '
            .'<body> '
            .'	<form name="the_form"> '
            .'		<input type="hidden" name="xml_registre " value="'.base64_encode(html_entity_decode(base64_decode($_POST['xmlReg']))).'"> '
            .'		<input type="hidden" name="xml_error" value="'.base64_encode(html_entity_decode(base64_decode($_POST['xmlErr']))).'">'
            .'	</form> '
            .'</body>'
            .'</html> ';
        exit;
    }
}
