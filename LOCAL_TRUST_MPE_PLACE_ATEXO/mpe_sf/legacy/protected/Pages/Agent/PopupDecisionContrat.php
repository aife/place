<?php

namespace Application\Pages\Agent;

use App\Service\Clauses\ClauseUsePrado;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTConsLotContratQuery;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTReferentielNace;
use Application\Propel\Mpe\CommonTReferentielNaceQuery;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Departement;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Application\Service\Atexo\Consultation\InvitePermanentContrat;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupDecisionContrat extends MpeTPage
{
    protected $connexion;
    protected $consultation;
    protected $contrat;
    protected $reference;
    protected $lots;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->accessContract();
        (Atexo_Util::hasSiretCheckeSiretCentralise()) ? $this->warningSiret->displayStyle = 'None' : $this->warningSiret->displayStyle = 'Dynamic';

        $messageErreur = null;
        $this->panelMessageErreur->setVisible(false);
        $logger = Atexo_LoggerManager::getLogger('contrat');
        if (isset($_GET['idContrat'])) {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->contrat = $contratQuery->getTContratTitulaireById($idContrat, $this->connexion);
            if ($this->contrat instanceof CommonTContratTitulaire) {
                $this->reference = (isset($_GET['id'])) ? Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])) : 0;
                $this->lots = (isset($_GET['lots'])) ? Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])) : 0;
                if (!$this->contrat->getHorsPassation()) {
                    if (!$this->lots) {
                        $lots = CommonTConsLotContratQuery::create()->getLotsByIdContrat($idContrat, $this->connexion);
                        if (is_array($lots) && count($lots)) {
                            $this->lots = implode('#', $lots);
                        }
                    }
                    if (!$this->reference) {
                        $this->reference = CommonTConsLotContratQuery::create()->getConsultationRefByIdContratTitulaire($idContrat, $this->connexion);
                    }
                }

                if (!$this->getPage()->getIsCallBack()) {
                    $this->idIdentificationEntreprise->setIdEntreprise($this->contrat->getIdTitulaire());
                    $this->idIdentificationEntreprise->setSoumissionnaire($this->contrat->getCommonTContactContrat());
                    $this->dateExecution->Text = $this->contrat->getDateDebutExecution();
                    if ($this->contrat->getMarcheInnovant()) {
                        $this->attributaire->marcheInnovant_Oui->Checked = true;
                    } else {
                        $this->attributaire->marcheInnovant_Non->Checked = true;
                    }
                    $this->recapConsultation->dateDecisionAttribution->Text = $this->contrat->getDateAttribution();

                    $this->entitesEligibles($this->contrat);
                    $this->panelBlocMessages->setDisplay('None');
                    $this->recapConsultation->initialiserComposant($this->reference, $this->lots, $this->contrat->getIdContratTitulaire());

                    if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($this->contrat->getIdTypeContrat())) {
                        $this->attributaire->blocContratMulti->initialiserComposant($this->contrat->getCommonTContratMulti(), $this->contrat->getIdTypeContrat(), $this->contrat);
                    } else {
                        $this->attributaire->blocContratMulti->setVisible(false);
                    }
                    $this->attributaire->setContrat($this->contrat);
                    $this->attributaire->initialiserComposant();

                    if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR')) {
                        $this->buttonValidate->setVisible(true);
                        $this->buttonSave->setVisible(false);
                    } else {
                        $this->buttonValidate->setVisible(false);
                        $this->buttonSave->setVisible(true);
                    }
                    $this->datesDefinitives->setVisible(false);
                    $this->initaliserDureeMarche($this->contrat);
                    if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
                        || $this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
                        $this->initaliserDatesDefinitives($this->contrat);
                        $this->datesDefinitives->setVisible(true);
                        if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
                            $this->datesDefinitives->setEnabled(false);
                            $this->calendarDateNotification->attributes->onclick = '';
                            $this->calendarDateFin->attributes->onclick = '';
                            $this->calendarDateFinMax->attributes->onclick = '';
                        }
                    }
                }
            } else {
                $logger->error("Le contrat dont l'id = ".$idContrat." n'existe pas");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }
        } else {
            $logger->error("Erreur d'accés à la PopupDecisionContrat sans le parametre idContrat");
            $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
        }

        if ($messageErreur) {
            $this->afficherMessageErreur($messageErreur);
        }
    }

    protected function saveContrat($sender, $param)
    {
        try {
            if ($this->isValid) {
                $this->connexion->beginTransaction();
                $organisme = $this->contrat->getOrganisme();
                $serviceId = $this->contrat->getServiceId();
                $contratMulti = $this->attributaire->blocContratMulti->saveContratMulti($this->connexion, $organisme, $serviceId, $this->contrat->getIdTypeContrat());
                $this->saveInvitationConsultationTransverse($contratMulti, $this->connexion);
                if ($this->contrat->getUuid() === null) {
                    $this->contrat->setUuid(Atexo_MpeSf::uuid());
                }
                $this->attributaire->saveContrat($this->contrat);
                $this->contrat->setDureeInitialeContrat($this->dureeMarche->Text);

                if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) {
                    $numeroContrat = $this->contrat->getNumeroContrat();

                    //dans le cas ou le numéro de contrat est NULL cf MPE-5991
                    if (null == $numeroContrat || '' == $numeroContrat) {
                        $numeroContrat = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($this->contrat, $this->connexion);
                        $this->contrat->setNumeroContrat($numeroContrat);
                    }
                    //dans le cas ou le numéro long oeap est NULL cf MPE-5991
                    $numeroLongOeap = $this->contrat->getNumLongOeap();
                    if (null == $numeroLongOeap || '' == $numeroLongOeap) {
                        $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($this->contrat, $this->connexion);
                        $this->contrat->setNumLongOeap($numeroLong.'01');
                    }
                    $this->contrat->setDateNotification(Atexo_Util::frnDate2iso($this->dateDefinitiveNotification->Text));
                    $this->contrat->setDateFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMarche->Text));
                    $this->contrat->setDateMaxFinContrat(Atexo_Util::frnDate2iso($this->dateDefinitiveFinMaxMarche->Text));
                }
                if ($this->attributaire->marcheInnovant_Oui->Checked) {
                    $this->contrat->setMarcheInnovant('1');
                } else {
                    $this->contrat->setMarcheInnovant('0');
                }

                $this->contrat->setDateDebutExecution(Atexo_Util::frnDate2iso($this->dateExecution->Text));
                $this->contrat->setDateAttribution(Atexo_Util::frnDate2iso($this->recapConsultation->dateDecisionAttribution->Text));
                if (Atexo_Module::isEnabled('NumDonneesEssentiellesManuel')) {
                    $this->contrat->setNumIdUniqueMarchePublic($this->contrat->getReferenceLibre());
                }

                $this->contrat->save($this->connexion);
                $this->saveInvitationConsultationTransverse($this->contrat, $this->connexion);
                $this->saveInfoContratFromContratMulti($contratMulti, $this->connexion);
                $this->connexion->commit();
                self::afficherMessageEnregistrement();
                $script = '';
                if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
                    || $this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) {
                    $script = 'window.close();';
                }
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $data = empty($this->attributaire->achatResponsableConsultation->achatResponsableFormValue->Data) ?
                        $this->attributaire->blocContratMulti->achatResponsableConsultation->achatResponsableFormValue->Data :
                        $this->attributaire->achatResponsableConsultation->achatResponsableFormValue->Data
                    ;

                    $this->saveAchatResponsable($this->contrat->getIdContratTitulaire(), $data);
                }
                $this->scriptJS->Text = "<script type='text/javascript'>window.opener.document.getElementById('ctl0_CONTENU_PAGE_refresh').click();".$script.'</script>';
            }
        } catch (\Exception $e) {
            $this->connexion->rollback();
            Prado::log('ERREUR enregistrement du contrat '.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionContrat');
        }
    }

    protected function validateContrat()
    {
        if ($this->isValid) {
            try {
                $this->connexion->beginTransaction();
                $organisme = $this->contrat->getOrganisme();
                $serviceId = $this->contrat->getServiceId();
                $contratMulti = $this->attributaire->blocContratMulti->saveContratMulti($this->connexion, $organisme, $serviceId, $this->contrat->getIdTypeContrat());
                $this->saveInvitationConsultationTransverse($contratMulti, $this->connexion);
                $this->contrat->setStatutContrat(Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'));
                $this->contrat->setDureeInitialeContrat($this->dureeMarche->Text);

                $this->attributaire->saveContrat($this->contrat);

                if (!empty($contratMulti)) {
                    $criteriaVo = new Atexo_Contrat_CriteriaVo();
                    $criteriaVo->setIdContratMulti($contratMulti->getIdContratTitulaire());
                    $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $contrats = $commonTContratTitulaireQuery->getContratByCriteres($criteriaVo);
                    $nomLieuPrincipalExecution = $contratMulti->getNomLieuPrincipalExecution();
                    $lieuExecutionId = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($nomLieuPrincipalExecution);
                    $lieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($lieuExecutionId);
                    foreach ($contrats as $contrat) {
                        $contrat->setNomLieuPrincipalExecution($nomLieuPrincipalExecution);
                        $contrat->setCodeLieuPrincipalExecution($lieuExecution[0]->getDenomination2());
                        $contrat->setTypeCodeLieuPrincipalExecution(1); // todo récupérer valeur dynamiquement (DB)
                        $contrat->save($this->connexion);
                    }
                }

                $millesime = $this->contrat->getDateNotification('Y');
                $numeroContrat = $this->contrat->getNumeroContrat();

                if ($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) {
                    //dans le cas ou le numéro de contrat est NULL cf MPE-5991
                    if (null == $numeroContrat || '' == $numeroContrat) {
                        $numeroContrat = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($this->contrat, $this->connexion);
                        $this->contrat->setNumeroContrat($numeroContrat);
                    }
                    //dans le cas ou le numéro long oeap est NULL cf MPE-5991
                    $numeroLongOeap = $this->contrat->getNumLongOeap();
                    if (null == $numeroLongOeap || '' == $numeroLongOeap) {
                        $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($this->contrat, $this->connexion);
                        $this->contrat->setNumLongOeap($numeroLong.'01');
                    }
                }
                $numIdUnique = $millesime.$numeroContrat.'00';
                if (Atexo_Module::isEnabled('NumDonneesEssentiellesManuel')) {
                    $this->contrat->setNumIdUniqueMarchePublic($this->contrat->getReferenceLibre());
                } else {
                    $this->contrat->setNumIdUniqueMarchePublic($numIdUnique);
                }

                if ($this->contrat->getUuid() === null) {
                    $this->contrat->setUuid(Atexo_MpeSf::uuid());
                }

                $this->contrat->save($this->connexion);
                $this->saveInvitationConsultationTransverse($this->contrat, $this->connexion);
                $this->saveInfoContratFromContratMulti($contratMulti, $this->connexion);
                $this->connexion->commit();
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $data = empty($this->attributaire->achatResponsableConsultation->achatResponsableFormValue->Data) ?
                        $this->attributaire->blocContratMulti->achatResponsableConsultation->achatResponsableFormValue->Data :
                        $this->attributaire->achatResponsableConsultation->achatResponsableFormValue->Data
                    ;

                    $this->saveAchatResponsable($this->contrat->getIdContratTitulaire(), $data);
                }
            } catch (Exception $e) {
                $this->connexion->rollback();
                Prado::log('ERREUR validation du contrat '.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionContrat');
            }
            $this->scriptJS->Text = "<script type='text/javascript'>window.opener.document.getElementById('ctl0_CONTENU_PAGE_refresh').click();window.close();</script>";
        }
    }

    public function validerDonneesDecision($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $scriptJs = '';

        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($this->contrat->getIdTypeContrat())) {
            $this->attributaire->blocContratMulti->validerDonneeContratMulti($isValid, $arrayMsgErreur, $scriptJs);
        } else {
            $this->attributaire->validerDonneesDecisionContrat($this->contrat, $isValid, $arrayMsgErreur, $scriptJs);
        }

        $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs);
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param bool   $isValid
     * @param array  $arrayMsgErreur tableau des message d'erreurs
     * @param string $scriptJs       le script js
     *
     * @return void
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs)
    {
        if ($isValid) {
            $param->IsValid = true;
            $this->scriptJS->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
        } else {
            $param->IsValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $this->scriptJS->Text = "<script>$scriptJs</script>";
            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }
        }
    }

    /**
     * Peremt d'afficher le message de confirmation d'enregistrement du contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function afficherMessageEnregistrement()
    {
        $this->messageConfirmationEnregistrement->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONTRAT'));
        $this->panelBlocMessages->setDisplay('Dynamic');
    }

    /*
     * Permet d'afficher le message d'erreur et cacher les autres composants
     *
     * @param string $messageErreur le message d'erreur
     * @return void
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function afficherMessageErreur($messageErreur)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageErreur);
        $this->messageConfirmationEnregistrement->setVisible(false);
        $this->panelPopupDecisionContrat->setVisible(false);
    }

    /**
     * Permet de charger les comoposant selon le type de contrat.
     *
     * @param int $idTypeContrat
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function entitesEligibles($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire && Atexo_Module::isEnabled('AcSadTransversaux')
            && ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($contrat->getIdTypeContrat()) || (new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat()))
        ) {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:block;');
            $this->decisionEntitesEligibles->setIdContrat($contrat->getIdContratTitulaire());
            $this->decisionEntitesEligibles->displayListEntityEligible();
        } else {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:none;');
        }
    }

    /**
     * Permet d'enregistrer les entités éligible.
     *
     * @param CommonTContratTitulaire $contrat
     * @param $connexion
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function saveInvitationConsultationTransverse($contrat, $connexion)
    {
        $lots = explode('#', $this->lots);
        if ($contrat instanceof CommonTContratTitulaire || $contrat instanceof CommonTContratMulti) {
            $contrats = [];
            $contrats[] = $contrat;
            if ($contrat instanceof CommonTContratTitulaire && $contrat->getIdContratMulti()) {
                $contrats = (array) CommonTContratTitulaireQuery::create()->filterByIdContratMulti($contrat->getIdContratMulti())->find();
            }
            if (Atexo_Module::isEnabled('AcSadTransversaux')
                && ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($contrat->getIdTypeContrat()) || (new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat()))
            ) {
                foreach ($contrats as $c) {
                    if ($c instanceof CommonTContratTitulaire || $c instanceof CommonTContratMulti) {
                        if (is_array($lots)) {
                            foreach ($lots as $lot) {
                                (new Atexo_Consultation_InvitationConsTransverse())->deleteInvitationConsultationTransverse($this->reference, $lot, $c->getOrganisme(), $c->getIdContratTitulaire());
                                if ('1' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                                    $this->decisionEntitesEligibles->addInvitationConsultationTransverse($this->reference, $contrat->getDateAttribution('Y-m-d'), $lot, $c->getIdContratTitulaire(), $connexion);
                                }

                                /**
                                 * Suppresion des données invité permanent sur le contrat !
                                 */
                                $criteres = [
                                    'idConsultation' => $this->reference,
                                    'organisme' => $contrat->getOrganisme(),
                                    'lot' => $lot,
                                    'idContratTitulaire' => $contrat->getIdContratTitulaire(),
                                ];
                                (new InvitePermanentContrat())->deleteForCriteres($criteres);
                                if ('2' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                                    $this->decisionEntitesEligibles->addInvitePermanentContrat($this->reference, null, $lot, $c->getIdContratTitulaire());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet d'enregistrer lles info du contrat multi dans les contras.
     *
     * @param CommonTContratMulti $contrat
     * @param $connexion
     *
     * @return void
     *
     * @author Loubna EZZIANi <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function saveInfoContratFromContratMulti($contrat, $connexion)
    {
        if ($contrat instanceof CommonTContratMulti) {
            $contrats = [];
            $contrats[] = $contrat;
            $contrats = (array) CommonTContratTitulaireQuery::create()->filterByIdContratMulti($contrat->getIdContratTitulaire())->find();
            foreach ($contrats as $c) {
                if ($c instanceof CommonTContratTitulaire) {
                    $c->setObjetContrat($contrat->getObjetContrat());
                    $c->setIntitule($contrat->getIntitule());
                    $c->setCategorie($contrat->getCategorie());
                    $c->setCcagApplicable($contrat->getCcagApplicable());
                    $c->setCodeCpv1($contrat->getCodeCpv1());
                    $c->setCodeCpv2($contrat->getCodeCpv2());
                    // les clauses
                    $c->setClauseEnvSpecsTechniques($contrat->getClauseEnvSpecsTechniques());
                    $c->setClauseEnvCondExecution($contrat->getClauseEnvCondExecution());
                    $c->setClauseEnvCriteresSelect($contrat->getClauseEnvCriteresSelect());
                    $c->setClauseEnvironnementale($contrat->getClauseEnvironnementale());
                    $c->setClauseSocialeConditionExecution($contrat->getClauseSocialeConditionExecution());
                    $c->setClauseSocialeInsertion($contrat->getClauseSocialeInsertion());
                    $c->setClauseSocialeAteliersProteges($contrat->getClauseSocialeAteliersProteges());
                    $c->setClauseSocialeSiae($contrat->getClauseSocialeSiae());
                    $c->setClauseSocialeEss($contrat->getClauseSocialeEss());
                    $c->setClauseSociale($contrat->getClauseSociale());

                    $c->save($connexion);
                }
            }
        }
    }

    /**
     * Permet d'initialiser les dates definitives.
     *
     * @param $contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initaliserDatesDefinitives($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->dateDefinitiveNotification->Text = $contrat->getDateNotification() ? Atexo_Util::iso2frnDate($contrat->getDateNotification()) : '';
            $this->dateDefinitiveFinMarche->Text = $contrat->getDateFinContrat() ? Atexo_Util::iso2frnDate($contrat->getDateFinContrat()) : '';
            $this->dateDefinitiveFinMaxMarche->Text = $contrat->getDateMaxFinContrat() ? Atexo_Util::iso2frnDate($contrat->getDateMaxFinContrat()) : '';
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function chargerLieuxExecution($sender, $param)
    {
        // Get the token
        $token = $param->getToken();

        // Sender is the Suggestions repeater
        $sender->DataSource = $this->chargerDepartementsListe($token);
        $sender->lieuPrincipalExecution->dataBind();
    }

    public function chargerDepartementsListe($name)
    {
        $departements = (new Atexo_Departement())->getListDepartements(true);

        if ($name) {
            return array_filter($departements, fn ($el) => false !== stripos($el['name'], (string) $name));
        } else {
            return $departements;
        }
    }

    /**
     * Permet de récupérer la forme du prix.
     */
    public function chargerFormePrix($contrat)
    {
        if (!empty($contrat->getFormePrix())) {
            if (1 == $contrat->getFormePrix()) {
                $this->formePrix_ferme->setChecked(true);
            } elseif (2 == $contrat->getFormePrix()) {
                $this->formePrix_ferme_actualisable->setChecked(true);
            } elseif (3 == $contrat->getFormePrix()) {
                $this->formePrix_revisable->setChecked(true);
            }
        }

        $this->formePrix_ferme->dataBind();
        $this->formePrix_ferme_actualisable->dataBind();
        $this->formePrix_revisable->dataBind();
    }

    /**
     * Permet d'initialiser la durée du marché.
     *
     * @param $contrat
     *
     * @return void
     */
    public function initaliserDureeMarche($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            if (null != $contrat->getDureeInitialeContrat()) {
                $this->dureeMarche->Text = $contrat->getDureeInitialeContrat();
            } elseif (null != $contrat->getDatePrevueFinContrat() && null != $contrat->getDatePrevueNotification()) {
                $this->dureeMarche->Text = null;
            }
        }
    }

    /**
     * Tester sur le statut du contrat pour afficher le bouton de modification des attributaires.
     */
    public function isEditAttributaireAvailable()
    {
        $isEditable = false;
        if (!empty($this->contrat->getIdTitulaire())) {
            $entrepriseQuery = new EntrepriseQuery();
            $entreprise = $entrepriseQuery->getEntrepriseById($this->contrat->getIdTitulaire());
            if (Atexo_Module::isEnabled('SynchronisationSgmap')
                && !($this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
                    || $this->contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'))
                && !empty($entreprise->getSiren())
            ) {
                $isEditable = true;
            }
        }

        return $isEditable;
    }

    /**
     * @param $sender
     * @param $param
     */
    public function annulerAttributaire($sender, $param)
    {
        $this->hideAllErrorMessage();
        $this->destroyDialog();
    }

    /**
     * Permet de valider la liste des attributaires selon le type de contrat.
     */
    public function saveAttributaire()
    {
        $siret = $this->idIdentificationEntreprise->siret->Text;
        $siren = $this->idIdentificationEntreprise->siren->Text;

        if ('' === $siret || '' === $siren) {
            //Régle de gestion 32 Afficher message d'erreur
            $this->displayErrorMessage('divValidationSiretNotNull');
        } else {
            // verifier que le siret est valide selon la formule de Luhn
            if (!Atexo_Util::isSiretValide($siren.$siret)) {
                //Régle de gestion 33: Afficher message d'erreur Siret non valide
                $this->displayErrorMessage('divValidationSiretNotValid');
            } else {
                //Régle de gestion 34 Entreprise et établissement n'existent pas dans la BDD
                if (empty(Atexo_Entreprise::retrieveCompanyBySiren($siren))) {
                    //SynchronisationSGMAP
                    if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                        $company = Atexo_Entreprise::recupererEntrepriseApiGouvEntreprise($siren, true);

                        if ($company instanceof Atexo_Entreprise_EntrepriseVo) {
                            $listeEtablissements = $company->getEtablissements();
                            $newEtablissement = is_array($listeEtablissements) ? array_pop($listeEtablissements) : '';
                            if ($company->getNicsiege() != $siret) {
                                $newEtablissement = (new Atexo_Entreprise_Etablissement())->recupererEtablissementApiGouvEntreprise($siret);
                                if ($newEtablissement instanceof Atexo_Entreprise_EtablissementVo) {
                                    $newEtablissement->setSaisieManuelle('0');
                                    $company->setEtablissements($newEtablissement);
                                }
                            }
                            $company->setListeEtablissements($company->getEtablissements());
                            $company->setSaisieManuelle('0');
                            $company->setDateModification(date('Y-m-d H:i:s'));
                            $company->setDateCreation(date('Y-m-d H:i:s'));
                            $company->setCreatedFromDecision('1');
                            $company = (new Atexo_Entreprise())->save($company);
                            if (!empty($company->getCommonTEtablissements()->getArrayCopy())) {
                                $this->editAttributaireContrat($company, $company->getCommonTEtablissements()->getArrayCopy()[0]);
                            } else {
                                $this->editAttributaireContrat($company, $newEtablissement);
                            }

                            $this->hideAllErrorMessage();
                            $this->destroyDialog();
                        } else {
                            $this->displayErrorMessage('divErrorSGMAP');
                        }
                    }
                } else {
                    //Entreprise existe dans BDD, Tester existance Etablissement
                    $company = Atexo_Entreprise::retrieveCompanyBySiren($siren);

                    $listeEtablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise($company->getId());
                    $listeEtablissements = (new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($listeEtablissements);

                    //Etablissement n'existe pas dans la BDD => recupération des informations de SGMAP
                    if (!isset($listeEtablissements[$siret])) {
                        $etablissement = (new Atexo_Entreprise_Etablissement())->synchroEtablissementAvecApiGouvEntreprise($siren.$siret, null);
                        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
                            $this->editAttributaireContrat($company, $etablissement);
                            $this->hideAllErrorMessage();
                            $this->destroyDialog();
                        } else {
                            //si une erreur existe afficher ERREUR
                            $this->displayErrorMessage('divErrorSGMAP');
                        }
                    } else {
                        //etablissement existe dans la BDD
                        $this->editAttributaireContrat($company, $listeEtablissements[$siret]);
                        $this->hideAllErrorMessage();
                        $this->destroyDialog();
                    }
                }
            }
        }
    }

    /**
     * @param $div
     */
    private function displayErrorMessage($div)
    {
        $this->hideAllErrorMessage();
        $script = 'J('.$div.").removeAttr('style');";
        $script .= "J('#imgValidationSIRET').removeAttr('style');";
        $this->scriptJS->Text = '<script>'.$script.'</script>';
    }

    private function hideAllErrorMessage()
    {
        $script = "J('#divValidationSiretNotNull').css('display', 'none');";
        $script .= "J('#divValidationSiretNotValid').css('display', 'none');";
        $script .= "J('#divErrorSGMAP').css('display', 'none');";
        $script .= "J('#imgValidationSIRET').css('display', 'none');";
        $this->scriptJS->Text = '<script>'.$script.'</script>';
    }

    private function destroyDialog()
    {
        $this->idIdentificationEntreprise->siret->Text = '';
        $this->idIdentificationEntreprise->siren->Text = '';
        $script = "J('.modal-modification-attributaire').dialog('destroy');";
        $this->scriptJS->Text = '<script>'.$script.'</script>';
    }

    /**
     * @param $company
     * @param $etab
     */
    private function editAttributaireContrat($company, $etab)
    {
        $this->initialiserComposant();
        $this->contrat->setIdTitulaire($company->getId());
        $this->contrat->setIdTitulaireEtab($etab->getIdEtablissement());
        $this->contrat->save($this->connexion);

        //display info contrat dans popupDecision
        $this->getIdentificationEntreprise($company);
    }

    private function initialiserComposant()
    {
        return true;
    }

    /**
     * @param $entreprise
     *
     * @throws Atexo_Config_Exception
     */
    public function getIdentificationEntreprise($entreprise)
    {
        $this->idIdentificationEntreprise->idRaisonSociale->Text = $entreprise->getNom();
        $this->idIdentificationEntreprise->adresse->Text = $entreprise->getAdresse();
        $this->idIdentificationEntreprise->adresseSuite->Text = $entreprise->getAdresse2();
        $this->idIdentificationEntreprise->codePostal->Text = $entreprise->getCodepostal();
        $this->idIdentificationEntreprise->ville->Text = $entreprise->getVilleadresse();
        $this->idIdentificationEntreprise->pays->Text = $entreprise->getPaysadresse();
        $this->idIdentificationEntreprise->telephone->Text = $entreprise->getTelephone();
        $this->idIdentificationEntreprise->fax->Text = $entreprise->getFax();
        $this->idIdentificationEntreprise->siretSiege->Text = $entreprise->getSiren().' '.$entreprise->getNicSiege();

        $this->idIdentificationEntreprise->idNational->Text = $entreprise->getSirenEtranger();
        $this->idIdentificationEntreprise->formeJuridique->Text = $this->getLibelleFormeJuridique($entreprise->getFormejuridique());
        $this->idIdentificationEntreprise->categorieEntreprise->Text = ($entreprise->getCategorieEntreprise() == Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME')) ? Prado::Localize('DEFINE_OUI') : Prado::Localize('DEFINE_NON');
        $libelleNace = $entreprise->getLibelleApe();
        if (!$libelleNace) {
            $refNace = CommonTReferentielNaceQuery::create()->filterByCodeNace5($entreprise->getCodeape())->findOne();
            if ($refNace instanceof CommonTReferentielNace) {
                $libelleNace = $refNace->getLibelleActiviteDetaillee();
            }
        }
        $this->idIdentificationEntreprise->codeApeNafNace->Text = $entreprise->getCodeape() ? $entreprise->getCodeape().($libelleNace ? ' - '.$libelleNace : '') : ' - ';

        $this->idIdentificationEntreprise->region->Text = (new Atexo_Geolocalisation_GeolocalisationN1())->retrieveDenomination1ByIdGeoN1($entreprise->getRegion());
        $this->idIdentificationEntreprise->province->Text = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($entreprise->getProvince());
        $this->idIdentificationEntreprise->telephone2->Text = $entreprise->getTelephone2();
        $this->idIdentificationEntreprise->telephone3->Text = $entreprise->getTelephone3();
        $this->idIdentificationEntreprise->idtaxeprof->Text = $entreprise->getNumTax();
        $this->idIdentificationEntreprise->numRc->Text = $entreprise->getRcNum();
        $this->idIdentificationEntreprise->villeRc->Text = $entreprise->getRcVille();
        $this->idIdentificationEntreprise->idsDomaines->Value = $entreprise->getDomainesActivites();
        $this->idIdentificationEntreprise->idcnss->Text = $entreprise->getCnss();
        $this->idIdentificationEntreprise->idcapital->Text = $entreprise->getCapitalSocial();
        $this->idIdentificationEntreprise->ifu->Text = $entreprise->getIfu();

        if ($entreprise->getDescriptionActivite()) {
            $this->idIdentificationEntreprise->description->text = $entreprise->getDescriptionActivite() ? str_replace("\n", '<br/>', Atexo_Util::replaceCharactersMsWordWithRegularCharacters($entreprise->getDescriptionActivite())) : ' -';
            $this->idIdentificationEntreprise->panelDescription->visible = true;
        } else {
            $this->idIdentificationEntreprise->panelDescription->visible = false;
        }

        $siteInternet = $entreprise->getSiteInternet();
        if ($siteInternet) {
            $this->idIdentificationEntreprise->panelSiteInternet->setVisible(true);
            $this->idIdentificationEntreprise->siteInternetLabel->text = $siteInternet;
            if (!strstr($siteInternet, 'http://') && !strstr($siteInternet, 'https://')) {
                $urlSiteInternet = 'http://'.$siteInternet;
            } else {
                $urlSiteInternet = $siteInternet;
            }
            $this->idIdentificationEntreprise->siteInternet->NavigateUrl = $urlSiteInternet;
        } else {
            $this->idIdentificationEntreprise->panelSiteInternet->setVisible(false);
        }

        $adress = [];
        $etablissementSiege = $entreprise->getMonEtablissementSiege();
        if ($etablissementSiege instanceof CommonTEtablissement) {
            if ($etablissementSiege->getAdresse() || $etablissementSiege->getAdresse2()) {
                $adress[] = $etablissementSiege->getAdresse().' '.$etablissementSiege->getAdresse2();
            }

            if ($etablissementSiege->getCodepostal() || $etablissementSiege->getVille()) {
                $adress[] = $etablissementSiege->getCodepostal().' '.$etablissementSiege->getVille();
            }

            if ($etablissementSiege->getPays()) {
                $adress[] = $etablissementSiege->getPays();
            }

            $this->idIdentificationEntreprise->siegeSocial->text = implode(' - ', $adress);
        } else {
            $this->idIdentificationEntreprise->siegeSocial->text = '-';
        }

        $paysSiren = $entreprise->getPaysenregistrement().($entreprise->getSiren() ? ' - '.$entreprise->getSiren() : '');
        if ($paysSiren) {
            $this->idIdentificationEntreprise->paysSiren->text = '('.$paysSiren.')';
        }
        $this->idIdentificationEntreprise->idRaisonSociale->Text = $entreprise->getNom();
    }

    /**
     * @param $FJ
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getLibelleFormeJuridique($FJ)
    {
        $formeJuridique = (new Atexo_Entreprise())->retrieveFormeJuridiqueBySigle($FJ);

        if ($formeJuridique) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $getLibelleFormejuridique = 'getLibelleFormejuridique'.ucwords($langue);
            if ($formeJuridique->$getLibelleFormejuridique()) {
                return $formeJuridique->$getLibelleFormejuridique();
            } elseif (!$formeJuridique->getLibelleFormejuridique() && !$formeJuridique->$getLibelleFormejuridique()) {
                return $formeJuridique->getFormejuridique();
            } elseif (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) && $formeJuridique->getLibelleFormejuridique()) {
                return $formeJuridique->getLibelleFormejuridique();
            }
        } else {
            return $FJ ?: '-';
        }
    }

    private function accessContract()
    {
        $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setFindOne(true);
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setIdAgentConnecte(Atexo_CurrentUser::getId());
        $criteriaVo->setIdContratTitulaire($idContrat);
        $propelConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contract = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo, $propelConnexion);

        if (!$contract instanceof CommonTContratTitulaire) {
            if ($this->accessContratForDecision()) {
                return $this->getResponse()->redirect('/agent/');
            }
        }
    }

    private function accessContratForDecision()
    {
        $noAccess = true;
        $consultationId = base64_decode($_GET['id']);
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService($idService);
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if ('decision' === $_GET['action']
            && count($consultation) > 0
            && $consultation[0] instanceof CommonConsultation) {
            $noAccess = false;
        }

        return $noAccess;
    }

    protected function saveAchatResponsable(int $idContrat, string $values): void
    {
        /** @var ClauseUsePrado $clause */
        $clause = Atexo_Util::getSfService(ClauseUsePrado::class);
        $data = [
            'values' => json_decode($values, true),
            'id_contrat' => $idContrat
        ];

        $clause->getContent('sendContrat', $data);
    }
}
