<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/**
 * commentaires.
 *
 * @author khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-4.0
 */
class ConsultationDetailReponse extends MpeTPage
{
    public bool $_enveloppeOuverte = false;
    public $_avecSignature;
    public ?string $_typeSignature = null;
    public ?string $_enveloppeCrypte = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $message = null;
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->setViewState('consultation_id', Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        $this->errorPart->setVisible(false);
        $this->mainPart->setVisible(true);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);
            $this->_avecSignature = $consultation->getSignatureOffre();
            $this->setViewState('avecSignature', $this->_avecSignature);
            $this->consultationSummury->setConsultation($consultation);
            $this->detailPli->setConsultation($consultation);
            if (!$this->isPostBack) {
                if (isset($_GET['idPli']) && isset($_GET['type'])) {
                    $this->detailPli->setIdPli(Atexo_Util::atexoHtmlEntities($_GET['idPli']));
                    $this->detailPli->setTypeEnv(Atexo_Util::atexoHtmlEntities($_GET['type']));
                    $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById(Atexo_Util::atexoHtmlEntities($_GET['idPli']), Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
                    $arrayAllFichiers = [];
                    $this->chargerEnveloppe($enveloppe, $arrayAllFichiers);
                    $this->idRepeaterFichiers->DataSource = [$enveloppe];
                    $this->idRepeaterFichiers->DataBind();
                    $this->setViewState('infosFichieParEnveloppe', $arrayAllFichiers);
                } elseif (isset($_GET['idOffre'])) {
                    $this->detailPli->setIdOffre(Atexo_Util::atexoHtmlEntities($_GET['idOffre']));
                    $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre(Atexo_Util::atexoHtmlEntities($_GET['idOffre']), Atexo_CurrentUser::getCurrentOrganism());
                    $arrayAllFichiers = [];
                    foreach ($enveloppes as $enveloppe) {
                        $this->chargerEnveloppe($enveloppe, $arrayAllFichiers);
                    }
                    $this->idRepeaterFichiers->DataSource = $enveloppes;
                    $this->idRepeaterFichiers->DataBind();
                    $this->setViewState('infosFichieParEnveloppe', $arrayAllFichiers);
                }
            }
        }
        if ($message) {
            $this->errorPart->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
        }
    }

    public function exportRapportSignaturePdf($sender, $param)
    {
        if ($param->CommandParameter) {
            $envloppe = $param->CommandParameter;
            $idEnv = $envloppe->getIdEnveloppeElectro();
            $arrayInfosCons = $this->getViewState('infoRapportPdfCons');
            $arrayInfosPli = $this->getViewState('infoRapportPdfPli');
            $arrayInfosPli['typeEnveloppe'] = $envloppe->getLibelleTypeEnveloppe($envloppe->getTypeEnv());
            $arrayInfosFichierSoumis = $this->getViewState('infosFichieParEnveloppe');
            (new Atexo_Consultation_Responses())->printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis[$idEnv]);
        }
    }

    public function chargerEnveloppe($enveloppe, &$arrayAllFichiers = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($enveloppe instanceof CommonEnveloppe && !($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME') ||
                                                            $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') ||
                                                            $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'))) {
            $this->_enveloppeCrypte = $enveloppe->getCryptage();
            $offre = $enveloppe->getOffre($connexionCom);

            if ($offre instanceof CommonOffres && $offre->getConsultationId() == $_GET['id']) {
                $xmlStr = $offre->getXmlString();
                $pos = strpos($xmlStr, 'typeSignature');
                if ($pos > 0) {
                    $this->setViewState('typeSignature', 'XML');
                    $this->_typeSignature = 'XML';
                }
                $this->_enveloppeOuverte = ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'));
                if ($enveloppe instanceof CommonEnveloppe) {
                    $c = new Criteria();
                    $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                    $c->add(CommonFichierEnveloppePeer::ORGANISME, Atexo_CurrentUser::getOrganismAcronym());
                    $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexionCom);
                    if ($fichiers && is_array($fichiers)) {
                        $arrayInfos = (new Atexo_Consultation_Responses())->getInfosCertifForRapportVerifSignature($fichiers, $this->_avecSignature, $this->_enveloppeCrypte, 'agent');
                        $arrayAllFichiers[$enveloppe->getIdEnveloppeElectro()] = $arrayInfos;
                    }
                }
            }
        }
    }
}
