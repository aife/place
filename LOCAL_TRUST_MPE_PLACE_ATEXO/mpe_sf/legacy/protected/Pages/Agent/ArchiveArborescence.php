<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchiveArborescence extends MpeTPage
{
    private $_connexion;
    private $_refConsultation;
    private $_consultation = null;

    /**
     * Permet de retourner la connexion.
     *
     * @return PropelPDO la connexion
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getConnexion()
    {
        if (null == $this->_connexion) {
            $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        return $this->_connexion;
    }

    /**
     * Permet de retourner la consultation.
     *
     * @return CommonConsultation la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getConsultation()
    {
        if (null == $this->_consultation) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $this->_consultation = CommonConsultationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->getConnexion());
        }

        return $this->_consultation;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Permet de charger les info de la page.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $this->retour->attachEventHandler('OnClick', [$this, 'retour']);
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $this->_consultation = CommonConsultationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->getConnexion());

        $this->infoConsultation->setConsultation($this->_consultation);

        if (!$this->isCallBack) {
            $this->fillRepeaterArborescenceArchive();
        }
    }

    /**
     * Permet de retourner vers le tableau de bord des archives.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retour()
    {
        if (isset($_GET['archiveRappel']) && true == $_GET['archiveRappel']) {
            $this->response->Redirect('?page=Agent.ArchiveRappelTableauDeBord&id='.$this->_refConsultation);
        }
        $this->response->Redirect('?page=Agent.ArchiveTableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    /**
     * Permet de remplir le repeater arborescence archive.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillRepeaterArborescenceArchive()
    {
        $dataSource = [];
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $i = 0;
            $referenceUtilisateur = $consultation->getRealReferenceUtilisateur();
            $dataSource[$i]['libelle'] = Prado::localize('DEFINE_CONSULTATION').' '.$referenceUtilisateur;
            $dataSource[$i]['lot'] = 0;
            ++$i;
            if (Atexo_Module::isEnabled('ArchiveParLot') && $consultation->getAlloti()) {
                $lots = $consultation->getAllLots();
                if (is_array($lots)) {
                    foreach ($lots as $lot) {
                        $dataSource[$i]['libelle'] = Prado::localize('DEFINE_CONSULTATION').' '.$referenceUtilisateur.' - '.Prado::localize('LOT').' '.$lot->getLot();
                        $dataSource[$i]['lot'] = $lot->getLot();
                        $dataSource[$i]['loaded'] = false;
                        ++$i;
                    }
                }
            }
            $this->repeaterArborescenceArchive->DataSource = $dataSource;
            $this->repeaterArborescenceArchive->DataBind();
        } else {
            $this->infoConsultation->setVisible(false);
            $this->panelArbo->setVisible(false);
            $this->panelErreur->setVisible(true);
            $this->panelErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
        }
    }

    /**
     * Permet de remplir le bloc d'arborescence de l'archive.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillDataArbo($sender, $param)
    {
        $numLot = $param->CallbackParameter;
        foreach ($this->repeaterArborescenceArchive->getItems() as $item) {
            if ($item->numLot->value == $numLot) {
                if (!$item->loaded->value) {
                    $item->loaded->value = true;
                    $item->arborescenceBolc->displayArborescence(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot);
                    $item->arborescencePanel->render($param->getNewWriter());
                    $this->scriptJs->Text = '<script>loadArborecence();</script>';
                }
            }
        }
    }
}
