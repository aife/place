<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceMoniteur;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Moniteur;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupMoniteur extends MpeTPage
{
    private string $_annonceMoniteur = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idAnnonce'])) {
            $this->_annonceMoniteur = (new Atexo_Publicite_Destinataire())->retrieveAnnonceMoniteurById(Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']), Atexo_CurrentUser::getOrganismAcronym());
            if ($this->_annonceMoniteur instanceof CommonAnnonceMoniteur) {
                $formulaireDejaEdite = strcmp($this->_annonceMoniteur->getDateMaj(), '0000-00-00 00:00:00'); //=0 si les deux chaînes sont égales
                if ($this->haveCompteMoniteur() && '1' != $this->_annonceMoniteur->getStatutXml() && !$formulaireDejaEdite) {
                    if (!$this->IsPostBack) {
                        $this->displayGroupeMoniteurs();
                    }
                } else {
                    $this->donneesModificationMoniteur->text = str_replace('&', '&amp;', $this->_annonceMoniteur->getXmlMoniteur());
                    $this->scriptClose->Text = '<script language="javascript">gotoMoniteur('.$this->_annonceMoniteur->getId().');</script>';
                }
            }
        }
    }

    public function getUrlStyleBoamp()
    {
        return Atexo_Util::getAbsoluteUrl().$this->themesPath().'/boamp/css/';
    }

    public function isXmlValid()
    {
        if ('1' != $this->_annonceMoniteur->getStatutXml()) {
            return false;
        } else {
            return true;
        }
    }

    public function displayGroupeMoniteurs()
    {
        $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
        $listAcheteur = (new Atexo_Publicite_Moniteur())->retrieveAllGroups($idService, Atexo_CurrentUser::getOrganismAcronym());
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        if ($listAcheteur) {
            foreach ($listAcheteur as $one) {
                $data[$one->getId()] = $one->getIdentifiant().' - '.$one->getMdp();
            }
        }
        $this->groupeMoniteur->DataSource = $data;
        $this->groupeMoniteur->DataBind();
    }

    public function gotoMoniteur($sender, $param)
    {
        $xml = $this->addGroupeMoniteurInXml($this->groupeMoniteur->SelectedValue);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->donneesModificationMoniteur->Text = htmlentities($xml);
        $this->_annonceMoniteur->setXmlMoniteur($xml);
        $this->_annonceMoniteur->save($connexion);
        $this->scriptClose->Text = '<script language="javascript">gotoMoniteur('.$this->_annonceMoniteur->getId().');</script>';
    }

    public function addGroupeMoniteurInXml($idCompte)
    {
        $compteMoniteur = Atexo_Publicite_Moniteur::retrieveGroupeById($idCompte);
        $xmlObject = simplexml_load_string($this->_annonceMoniteur->getXmlMoniteur());
        $xmlObject->user->login = $compteMoniteur->getIdentifiant();
        $xmlObject->user->password = Atexo_Util::toUtf8($compteMoniteur->getMdp());
        $xmlObject->diffusionMOL->numeroAbonne = Atexo_Util::toUtf8($compteMoniteur->getNumAbonne());

        return Atexo_Util::fixCDATA($xmlObject->asXML());
    }

    /**
     * Si le service de l'agent connecté à des comptes moniteur on affiche la liste sinon on passe.
     */
    public function haveCompteMoniteur()
    {
        $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
        $listAcheteur = (new Atexo_Publicite_Moniteur())->retrieveAllGroups($idService, Atexo_CurrentUser::getOrganismAcronym());
        if ((is_countable($listAcheteur) ? count($listAcheteur) : 0) > 0) {
            return true;
        } else {
            return false;
        }
    }
}
