<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonSocietesExclues;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_SocietesExclues;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de Création des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FormulaireSocietesExclues extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        self::customizeForm();
    }

    public function customizeForm()
    {
        self::isPorteeTotaleClickable();
        if ($_GET['idSocieteExclue']) {
            if (!$this->isPostBack) {
                self::chargerFormulaireCreation();
            }
        }
    }

    public function isPorteeTotaleClickable()
    {
        if (Atexo_CurrentUser::hasHabilitation('PorteeSocietesExcluesTousOrganismes')) {
            $this->portee_totale->setEnabled(true);
        } else {
            $this->portee_totale->setEnabled(false);
        }
    }

    public function SaveUpdateSocietesExclues()
    {
        //Scan antivirus
        if ($this->document_attacher->HasFile) {
            //Début scan antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->document_attacher->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.' "'.$this->document_attacher->FileName.'"');

                return;
            }
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($_GET['idSocieteExclue']) {
            $societesExcluesCom = (new Atexo_SocietesExclues())->retrieveSocietesExcluesByPk(Atexo_Util::atexoHtmlEntities($_GET['idSocieteExclue']));
        } else {
            $societesExcluesCom = new CommonSocietesExclues();
        }
        $societesExcluesCom = self::chargerInformationSocietesExclues($societesExcluesCom, Atexo_Config::getParameter('COMMON_BLOB'));

        $societesExcluesCom->save($connexionCom);
        $dernierSociete = (new Atexo_SocietesExclues())->getdernierSocieteExclue();

        $idSociete = null;
        if ($_GET['idSocieteExclue']) {
            $idSociete = Atexo_Util::atexoHtmlEntities($_GET['idSocieteExclue']);
        } else {
            $idSociete = $dernierSociete->getIdSocietesExclues();
        }
        $this->response->redirect('?page=Agent.AgentRechercherSocietesExclues&save=1&idSocieteExclue='.$idSociete);
    }

    public function chargerInformationSocietesExclues($societesExcluesObjet, $dest)
    {
        $societesExcluesObjet->setOrganismeAcronyme(Atexo_CurrentUser::getOrganismAcronym());
        $societesExcluesObjet->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $societesExcluesObjet->setIdAgent(Atexo_CurrentUser::getId());

        $infile = Atexo_Config::getParameter('COMMON_TMP').'docAttaches'.session_id().time();

        if ($this->document_attacher->HasFile) {
            if (move_uploaded_file($this->document_attacher->LocalName, $infile)) {
                $societesExcluesObjet->setNomDocument($this->document_attacher->FileName);
                $atexoBlob = new Atexo_Blob();
                $docIdBlob = $atexoBlob->insert_blob($this->document_attacher->FileName, $infile, $dest);
                $societesExcluesObjet->setIdBlob($docIdBlob);
                $societesExcluesObjet->setTailleDocument($this->document_attacher->FileSize);
            }
        }

        $societesExcluesObjet->setRaisonSociale($this->libelleFournisseurFr->Text);
        $societesExcluesObjet->setMotif($this->motifFr->Text);
        $societesExcluesObjet->setRaisonSocialeFr($this->libelleFournisseurFr->Text);
        $societesExcluesObjet->setMotifFr($this->motifFr->Text);

        if (true == $this->exclusion_temporaire->Checked) {
            $societesExcluesObjet->setDateDebutExclusion(Atexo_Util::frnDate2iso($this->exclusion_temporaire_dateStart->text));
            $societesExcluesObjet->setDateFinExclusion(Atexo_Util::frnDate2iso($this->exclusion_temporaire_dateEnd->text));
            $societesExcluesObjet->setTypeExclusion(Atexo_Config::getParameter('EXCLUSION_TEMPORAIRE'));
        } elseif (true == $this->exclusion_definitive->Checked) {
            $societesExcluesObjet->setTypeExclusion(Atexo_Config::getParameter('EXCLUSION_DEFINITIF'));
            $societesExcluesObjet->setDateDebutExclusion(Atexo_Util::frnDate2iso($this->exclusion_definitive_dateStart->text));
        }

        if (true == $this->portee_partielle->Checked) {
            $societesExcluesObjet->setTypePortee(Atexo_Config::getParameter('PORTEE_PARTIELLE'));
        } elseif (true == $this->portee_totale->Checked) {
            $societesExcluesObjet->setTypePortee(Atexo_Config::getParameter('PORTEE_TOTALE'));
        }

        return $societesExcluesObjet;
    }

    public function chargerFormulaireCreation()
    {
        $societeExclue = (new Atexo_SocietesExclues())->retrieveSocietesExcluesByPk(Atexo_Util::atexoHtmlEntities($_GET['idSocieteExclue']));
        if ($societeExclue) {
            $identifiantEtp = explode('-', $societeExclue->getIdentifiantEntreprise());
            $this->libelleFournisseurFr->Text = $societeExclue->getRaisonSocialeFr();
            $this->motifFr->Text = $societeExclue->getMotif();
            if ($societeExclue->getTypeExclusion() == Atexo_Config::getParameter('EXCLUSION_TEMPORAIRE')) {
                $this->exclusion_temporaire->Checked = true;
                $this->exclusion_temporaire_dateStart->text = Atexo_Util::iso2frnDate($societeExclue->getDateDebutExclusion());
                $this->exclusion_temporaire_dateEnd->text = Atexo_Util::iso2frnDate($societeExclue->getDateFinExclusion());
            } elseif ($societeExclue->getTypeExclusion() == Atexo_Config::getParameter('EXCLUSION_DEFINITIF')) {
                $this->exclusion_definitive->Checked = true;
                $this->exclusion_definitive_dateStart->text = Atexo_Util::iso2frnDate($societeExclue->getDateDebutExclusion());
            }
            if ($societeExclue->getTypePortee() == Atexo_Config::getParameter('PORTEE_PARTIELLE')) {
                $this->portee_partielle->Checked = true;
            } elseif ($societeExclue->getTypePortee() == Atexo_Config::getParameter('PORTEE_TOTALE')) {
                $this->portee_totale->Checked = true;
            }
        }
    }

    public function afficherErreur($msg)
    {
        $this->erreurScanAntivirus->setMessage($msg);
        $this->erreurScanAntivirus->setVisible(true);
    }
}
