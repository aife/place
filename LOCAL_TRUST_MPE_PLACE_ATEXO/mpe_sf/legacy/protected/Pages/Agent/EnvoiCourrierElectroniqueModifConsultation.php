<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Prado\Web\UI\TPage;
use Prado\Prado;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe EnvoiCourrierElectroniqueModifConsultation.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiCourrierElectroniqueModifConsultation extends MpeTPage
{
    protected ?string $_typeMsg = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $message = null;
        $this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_MODIFICATION_CONSULTATION');
        $this->TemplateEnvoiCourrierElectronique->messageType->setEnabled(false);
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->_typeMsg = 'modifConsult';

        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);

        $langueOblPublication = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
        $autreLangObligExiste = false;
        if (is_countable($langueOblPublication) ? count($langueOblPublication) : 0) {
            $listeLangues = "<ul class='default-list liste-langues'>";
            foreach ($langueOblPublication as $oneLangue) {
                if (Atexo_CurrentUser::readFromSession('lang') != $oneLangue->getLangue()) {
                    $urlTraduction = 'index.php?page=Agent.TraduireConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&langue='.$oneLangue->getLangue();
                    $listeLangues .= "<li> <a href='".$urlTraduction."' >".ucfirst(strtolower($oneLangue->getLibelle())).'</a></li>';
                }
            }
            $listeLangues .= '</ul>';
            if (isset($_GET['traductionOgl'])) {
                $autreLangObligExiste = true;
                $message = '<span>'.Prado::localize('REPORTER_MODIFICATIONS_DANS_LANGUES', ['langues' => $listeLangues]).'</span>';
            }
            $this->panelMessageAvertissement->setVisible($autreLangObligExiste);
            $this->panelMessageAvertissement->setMessage($message);
        }
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.ChangingConsultation&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    public function remplirListeDestinataires()
    {
        $listeDestinataires = '';
        $mailsDestinataire = [];
        $tabMails = [];

        if (Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            $listeDestinataires = Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        }
        if (Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }
        if (Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism())) {
            if (!$listeDestinataires) {
                $listeDestinataires .= Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $listeDestinataires .= ' , '.Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            }
        }

        $mailsDestinataire = explode(' , ', $listeDestinataires);
        if (is_array($mailsDestinataire) && 0 != count($mailsDestinataire)) {
            foreach ($mailsDestinataire as $mail) {
                if (!in_array($mail, $tabMails)) {
                    $tabMails[] = $mail;
                }
            }
        }
        $listeDestinataires = implode(' , ', $tabMails);

        return $listeDestinataires;
    }
}
