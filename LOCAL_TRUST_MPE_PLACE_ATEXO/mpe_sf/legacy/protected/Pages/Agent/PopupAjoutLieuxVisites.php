<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupAjoutLieuxVisites extends MpeTPage
{
    private string $_idVisite = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['data'])) {
            $array = explode('#_#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['data'])));
            $this->_idVisite = $array[2];
            if (!$this->isPostBack) {
                $this->addVisite->Text = $array[0];
                $this->dateRemisePlis->Text = Atexo_Util::iso2frnDateTime($array[1]);
            }
        }
    }

    /**
     *ajouter d'url dans la table Avis.
     */
    public function addVisite($sender, $param)
    {
        $adresse = $this->addVisite->Text;
        $date = Atexo_Util::frnDateTime2iso($this->dateRemisePlis->Text);
        if ($_GET['clientId']) {
            $clientId = str_replace(strstr(Atexo_Util::atexoHtmlEntities($_GET['clientId']), '_visiteRepeater'), '', Atexo_Util::atexoHtmlEntities($_GET['clientId']));
        } else {
            $clientId = 'ctl0_CONTENU_PAGE';
        }
        $addVisite = '1';
        if (isset($_GET['data'])) {
            $addVisite = '0';
        }
        $this->script->Text = "<script>returnLieuxVisites('".base64_encode($adresse.'#_#'.$date.'#_#'.$this->_idVisite)."','".$clientId."','".$addVisite."');</script>";
    }
}
