<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Agent\Atexo_Agent_Access;
use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

class SuiviAcces extends MpeTPage
{
    public $critereTri;
    public $triAscDesc;
    public $organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->IdSuiviAccesSearch->visible = true;
            $this->IdSuiviAccesResult->visible = false;
            $this->initialEntityPurchaseChoice();
        }
    }

    public function constructCriteriaSearchVo()
    {
        $criteria = new Atexo_Agent_CriteriaVo();
        $criteria->setNom($this->IdSuiviAccesSearch->nomAgent->Text);
        $criteria->setPrenom($this->IdSuiviAccesSearch->prenomAgent->Text);

        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && 1 == $_GET['all']) {
            $criteria->setOrganisme($this->IdSuiviAccesSearch->IdChoixOrganisme->organisme->getSelectedValue());
            $criteria->setService($this->IdSuiviAccesSearch->IdChoixOrganisme->listeEntites->getSelectedValue());
        } else {
            $criteria->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $criteria->setService($this->IdSuiviAccesSearch->IdChoixEntiteAchat->getSelectedEntityPurchase());
        }
        if ('' != $this->IdSuiviAccesSearch->datemiseAcccesStart->Text) {
            $criteria->setDateDebut(Atexo_Util::frnDateTime2iso($this->IdSuiviAccesSearch->datemiseAcccesStart->Text).' 00-00-00');
        }
        if ('' != $this->IdSuiviAccesSearch->datemiseAcccesEnd->Text) {
            $criteria->setDateFin(Atexo_Util::frnDateTime2iso($this->IdSuiviAccesSearch->datemiseAcccesEnd->Text).' 23-59-00');
        }

        $this->setViewState('criteresRecherche', $criteria);

        return $criteria;
    }

    public function search($sender, $param)
    {
        $this->IdSuiviAccesSearch->visible = false;
        $this->IdSuiviAccesResult->visible = true;
        $criteriaVo = $this->constructCriteriaSearchVo();
        $acces = Atexo_Agent_Access::search($criteriaVo);
        $this->dataBindRepeaterResult($acces);
    }

    public function dataBindRepeaterResult($acces)
    {
        $this->setViewState('listeAgents', $acces);
        if ((is_countable($acces) ? count($acces) : 0) >= 1) {
            $nombreElement = is_countable($acces) ? count($acces) : 0;
            $this->IdSuiviAccesResult->nombreElement1->Text = $nombreElement;
            $this->IdSuiviAccesResult->nombreElement2->Text = $nombreElement;

            $this->IdSuiviAccesResult->PartionneurButtom->setVisible(true);
            $this->IdSuiviAccesResult->PartionneurTop->setVisible(true);
            $this->setViewState('nombreResultats', $nombreElement);

            $this->IdSuiviAccesResult->nombrePageTop->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);
            $this->IdSuiviAccesResult->nombrePageBottom->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setVirtualItemCount($nombreElement);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex(0);
            $this->IdSuiviAccesResult->IdMessageErreur->setVisible(false);
            $this->IdSuiviAccesResult->IdMessageErreur->setVisible(false);
            $this->IdSuiviAccesResult->libelleNbrResultats->Visible = true;
            $this->IdSuiviAccesResult->nombreElement1->Visible = true;
        } else {
            $this->IdSuiviAccesResult->nombreElement1->Visible = false;
            $this->IdSuiviAccesResult->libelleNouvelleRecherche->Visible = true;
            $this->IdSuiviAccesResult->libelleNbrResultats->Visible = false;
            $this->IdSuiviAccesResult->PartionneurButtom->setVisible(false);
            $this->IdSuiviAccesResult->PartionneurTop->setVisible(false);
            $this->IdSuiviAccesResult->IdMessageErreur->setVisible(true);
            $this->IdSuiviAccesResult->IdMessageErreur->setVisible(true);
        }
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $acces;
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->dataBind();
    }

    public function getOrganismeAccronymeLibelle()
    {
        $organismeAcronyme = Atexo_CurrentUser::getOrganismAcronym();
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($organismeAcronyme);
        if ($organisme) {
            return $organisme->getAcronyme().' - '.$organisme->getDenominationOrg();
        } else {
            return ' ';
        }
    }

    public function deleteSearchCriteria()
    {
        $this->initialEntityPurchaseChoice();
        $this->IdSuiviAccesSearch->nomAgent->Text = '';
        $this->IdSuiviAccesSearch->prenomAgent->Text = '';
        $this->IdSuiviAccesSearch->datemiseAcccesStart->Text = '';
        $this->IdSuiviAccesSearch->datemiseAcccesEnd->Text = '';
    }

    /**
     * Trier un tableau des invités sélcetionnés.
     *
     * @param sender
     * @param param
     */
    public function sortAgents($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $agents = $this->getViewState('listeAgents');

        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'Nom', '');
        } elseif ('DateAcces' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'DateAcces', '');
        } else {
            return;
        }
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        $this->setViewState('listeAgents', $dataSourceTriee[0]);

        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $dataSourceTriee[0];
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();

        // Re-affichage du TActivePanel
        $this->IdSuiviAccesResult->panelRefresh->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function pageChanged($sender, $param)
    {
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->CurrentPageIndex = $param->NewPageIndex;
        $this->IdSuiviAccesResult->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->IdSuiviAccesResult->numPageTop->Text = $param->NewPageIndex + 1;
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $this->getViewState('listeAgents');
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'PageSizeBottom':
                $pageSize = $this->IdSuiviAccesResult->PageSizeBottom->getSelectedValue();
                $this->IdSuiviAccesResult->PageSizeTop->setSelectedValue($pageSize);
                break;

            case 'PageSizeTop':
                $pageSize = $this->IdSuiviAccesResult->PageSizeTop->getSelectedValue();
                $this->IdSuiviAccesResult->PageSizeBottom->setSelectedValue($pageSize);
                break;
        }

        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreResultats');

        $this->IdSuiviAccesResult->nombrePageTop->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);
        $this->IdSuiviAccesResult->nombrePageBottom->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);

        $this->IdSuiviAccesResult->numPageTop->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);
        $this->IdSuiviAccesResult->numPageBottom->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);

        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex(0);
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $this->getViewState('listeAgents');
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->IdSuiviAccesResult->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->IdSuiviAccesResult->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->IdSuiviAccesResult->nombrePageTop->Text) {
                $numPage = $this->IdSuiviAccesResult->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->CurrentPageIndex = $numPage - 1;
            $this->IdSuiviAccesResult->numPageTop->Text = $numPage;
            $this->IdSuiviAccesResult->numPageBottom->Text = $numPage;
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $this->getViewState('listeAgents');
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
        } else {
            $this->IdSuiviAccesResult->numPageTop->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->CurrentPageIndex + 1;
            $this->IdSuiviAccesResult->numPageBottom->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->CurrentPageIndex + 1;
        }
    }

    public function changePageResults($sender, $param)
    {
        $agents = $this->getViewState('listeAgents');
        $nombreElement = $this->getViewState('nombreResultats');
        $this->IdSuiviAccesResult->nombrePageTop->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);
        $this->IdSuiviAccesResult->nombrePageBottom->Text = ceil($nombreElement / $this->IdSuiviAccesResult->idRepeaterResultatsAcces->PageSize);

        // si on click sur suivant
        if ($param->getCommandSource()->getText() == $sender->getNextPageText()) {
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex($sender->getCurrentPageIndex() + 1);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $agents;
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
            if ($this->IdSuiviAccesResult->numPageBottom->Text < $nombreElement && $this->IdSuiviAccesResult->numPageTop->Text < $nombreElement) {
                $this->IdSuiviAccesResult->numPageBottom->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;
                $this->IdSuiviAccesResult->numPageTop->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;
            } else {
                $this->IdSuiviAccesResult->numPageBottom->Text = $nombreElement;
                $this->IdSuiviAccesResult->numPageTop->Text = $nombreElement;
            }

            return;
        }
        // click sur precedent
        if ($param->getCommandSource()->getText() == $sender->getPrevPageText()) {
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex($sender->getCurrentPageIndex() - 1);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $agents;
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
            $this->IdSuiviAccesResult->numPageBottom->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;
            $this->IdSuiviAccesResult->numPageTop->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;

            return;
        }
        // click sur premier ou 1
        if (($param->getCommandSource()->getText() == $sender->getFirstPageText())
                    || (1 == $param->getCommandSource()->getText())) {
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex(0);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $agents;
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
            $this->IdSuiviAccesResult->numPageBottom->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;
            $this->IdSuiviAccesResult->numPageTop->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;

            return;
        }
        // click sur dernier
        if ($param->getCommandSource()->getText() == $sender->getLastPageText()) {
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex($this->IdSuiviAccesResult->idRepeaterResultatsAcces->getPageCount() - 1);
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $agents;
            $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
            $this->IdSuiviAccesResult->numPageBottom->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;
            $this->IdSuiviAccesResult->numPageTop->Text = $this->IdSuiviAccesResult->idRepeaterResultatsAcces->getCurrentPageIndex() + 1;

            return;
        }
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->setCurrentPageIndex($param->getCommandSource()->getText() - 1);
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataSource = $agents;
        $this->IdSuiviAccesResult->idRepeaterResultatsAcces->DataBind();
    }

    public function initialEntityPurchaseChoice()
    {
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && 1 == $_GET['all']) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $this->IdSuiviAccesSearch->IdChoixOrganisme->organisme->setSelectedValue(Atexo_CurrentUser::getOrganismAcronym());
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($organisme, true);
            $this->IdSuiviAccesSearch->IdChoixOrganisme->listeEntites->dataSource = $listEntityPurchase;
            $this->IdSuiviAccesSearch->IdChoixOrganisme->listeEntites->DataBind();
            $this->IdSuiviAccesSearch->IdChoixOrganisme->listeEntites->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
        }
    }

    public function modifySearchCriteria($sender, $param)
    {
        $criteriaVo = $this->Page->getViewState('criteresRecherche');

        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && 1 == $_GET['all']) {
            $this->IdSuiviAccesSearch->IdChoixOrganisme->organisme->setSelectedValue($criteriaVo->getOrganisme());
            $this->IdSuiviAccesSearch->IdChoixOrganisme->listeEntites->setSelectedValue($criteriaVo->getService());
        }
        $this->IdSuiviAccesSearch->nomAgent->Text = $criteriaVo->getNom();
        $this->IdSuiviAccesSearch->prenomAgent->Text = $criteriaVo->getPrenom();

        $this->IdSuiviAccesSearch->datemiseAcccesStart->Text = Atexo_Util::iso2frnDate(substr($criteriaVo->getDateDebut(), 0, -9));
        $this->IdSuiviAccesSearch->datemiseAcccesEnd->Text = Atexo_Util::iso2frnDate(substr($criteriaVo->getDateFin(), 0, -9));

        $this->IdSuiviAccesSearch->visible = true;
        $this->IdSuiviAccesResult->visible = false;
    }

    public function getOrganismeAgent($idAgent)
    {
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            return $agent->getOrganisme();
        }

        return '';
    }
}
