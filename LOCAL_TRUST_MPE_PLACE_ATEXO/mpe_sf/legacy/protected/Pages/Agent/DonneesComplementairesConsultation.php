<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonPassationConsultation;
use Application\Propel\Mpe\CommonPassationMarcheAVenir;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Contrat;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Passation;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DonneesComplementairesConsultation extends MpeTPage
{
    public $_consultation = null;
    public bool $_showErreurSurFormat = true;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $message = '';
        if (isset($_GET['id'])) {
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (is_array($consultation) && 1 == count($consultation)) {
                $consultation = array_shift($consultation);
                if ($consultation instanceof CommonConsultation) {
                    $referenceUtilisateur = $consultation->getReferenceUtilisateur();
                    $referenceConsultationPremierePhase = (new Atexo_Consultation())->retrieveFirstPhaseConsultation($referenceUtilisateur, Atexo_CurrentUser::getCurrentOrganism());
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($referenceConsultationPremierePhase, Atexo_CurrentUser::getCurrentOrganism());
                    if ($consultation instanceof CommonConsultation) {
                        $this->_consultation = $consultation;
                        $this->displayCartouche($consultation);
                        if (!$this->isPostBack) {
                            $this->fillModeExecutionContrat();
                            $this->fillNatureActeJuridique();
                            $this->fillFcspMandataire();
                            $this->fillFcspUnite();
                            $this->displayCartoucheLot($consultation);
                            $this->displayInfosConsultation($consultation);

                            if ('1' == $consultation->getAlloti() && $this->isConsultationHasLots()) {
                                $selectedLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultation->getId(), $this->lot->selectedValue, Atexo_CurrentUser::getCurrentOrganism());
                                $this->displayInfoMarche($selectedLot);
                                $lot = $selectedLot->getLot();
                            } else {
                                $this->displayInfoMarche($consultation);
                                $lot = '0';
                            }
                            $passationConsultation = (new Atexo_SuiviPassation_Passation())->retrievePassationByReferenceConsultation($consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
                            if (!($passationConsultation instanceof CommonPassationConsultation)) {
                                $passationConsultation = (new Atexo_SuiviPassation_Passation())->createEmptyPassationConsultation($consultation, Atexo_CurrentUser::getCurrentOrganism());
                            }
                            $this->setViewState('passationConsultation', $passationConsultation);
                            $this->setViewState('idPassationConsultation', $passationConsultation->getId());
                            $this->setViewState('numLot', $lot);
                            $lots = $consultation->getAllLots();
                            $nombreLots = is_countable($lots) ? count($lots) : 0;
                            $arrayNumLots = [];
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            if (is_array($lots) && $nombreLots > 0) {
                                foreach ($lots as $unLot) {
                                    $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $unLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
                                    if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
                                        $passationMarcheAVenir = new CommonPassationMarcheAVenir();
                                        $passationMarcheAVenir->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                                        $passationMarcheAVenir->setIdPassationConsultation($this->getViewState('idPassationConsultation'));
                                        $passationMarcheAVenir->setLot($unLot->getLot());
                                        $passationMarcheAVenir->save($connexion);
                                    }
                                    $arrayNumLots[$unLot->getLot()] = $unLot->getLot();
                                }
                            } else {
                                $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), '0', Atexo_CurrentUser::getCurrentOrganism());
                                if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
                                    $passationMarcheAVenir = new CommonPassationMarcheAVenir();
                                    $passationMarcheAVenir->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                                    $passationMarcheAVenir->setIdPassationConsultation($this->getViewState('idPassationConsultation'));
                                    $passationMarcheAVenir->setLot('0');
                                    $passationMarcheAVenir->save($connexion);
                                }
                            }
                            $passationMarcheAVenirExistants = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultation($this->getViewState('idPassationConsultation'), Atexo_CurrentUser::getCurrentOrganism());
                            if (is_array($passationMarcheAVenirExistants)) {
                                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                                foreach ($passationMarcheAVenirExistants as $onePassationMAV) {
                                    if ((0 == $onePassationMAV->getLot() && $nombreLots > 0) // cas consultation allotit et on a une PMAV pour le lot 0 on supprime la PMAV
                                        || (0 == $nombreLots && 0 != $onePassationMAV->getLot()) // cas consultation non allotit et on a une PMAV pour le lot différent de 0 on supprime la PMA
                                        || (0 != $nombreLots && 0 != $onePassationMAV->getLot() && !in_array($onePassationMAV->getLot(), $arrayNumLots))// cas consultation allotit et PMAV existe pour un lot supprimé
                                    ) {
                                        $onePassationMAV->delete($connexion);
                                    }
                                }
                            }
                            $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $lot, Atexo_CurrentUser::getCurrentOrganism());
                            $this->setViewState('passationMarcheAVenir'.$lot, $passationMarcheAVenir);
                            $this->displayCaracteristiqueConsultation($passationConsultation);
                            $this->displayCaracteristiqueMarche($lot);
                            $this->renseignerTotalLot($consultation->getAlloti());
                            $this->fillListContrat($passationMarcheAVenir->getId());
                            $this->fillVuPar($passationConsultation, $passationMarcheAVenir);
                        }
                    } else {
                        $message = ''; //$message="Vous n'êtes '";
                    }
                } else {
                    $message = "Vous n'êtes pas autorisé à accéder à cette consultation.";
                }
            } else {
                $message = "Aucune consultation n'a été trouvé.";
            }
        } else {
            $message = "Aucune consultation n'a été selectionnée";
        }
        if ($message) {
            $this->errorPart->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
            $this->consultationSummary->setWithException(false);
        }
        $this->customizeForm();
    }

    public function displayCartouche($consultation)
    {
        $this->consultationSummary->setConsultation($consultation);
    }

    public function displayCartoucheLot($consultation)
    {
        if ('1' == $consultation->getAlloti()) {
            $this->panelAlloti->setVisible(true);
            $arrayLot = (new Atexo_Consultation_Lots())->retrieveArrayIntituleLot($consultation->getId(), Atexo_CurrentUser::getCurrentOrganism(), true);
            if (is_array($arrayLot)) {
                $this->lot->DataSource = $arrayLot;
                $this->lot->DataBind();
                $this->nombreLots->Text = count($arrayLot);
                $arrayKeys = array_keys($arrayLot);
                $selectedLot = array_shift($arrayKeys);
                $this->lot->SetSelectedValue($selectedLot);

                $arrayLots = (new Atexo_Consultation_Lots())->retrieveArrayIntituleLot($consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
                $this->intituleLot->Text = trim($arrayLots[$selectedLot]);
            }
        } else {
            $this->nombreLots->Text = '-';
            $this->panelAlloti->setVisible(false);
        }
    }

    public function displayInfosConsultation($consultation)
    {
        //$consultation = new Consultation();
        $this->referenceUtilisateur->Text = $consultation->getReferenceUtilisateur();
        $this->objetConsultation->Text = (new Atexo_Config())->toPfEncoding($consultation->getObjet());
        $this->typeProcedure->Text = (new Atexo_Config())->toPfEncoding($consultation->getLibelleTypeProcedure());
        if ('1' == $consultation->getAlloti()) {
            $this->formeConsultation->Text = 'Alloti';
        } else {
            $this->formeConsultation->Text = 'Non alloti';
        }
    }

    public function displayInfoMarche($objet, $utf8encode = false)
    {
        if ($objet instanceof CommonConsultation) {
            $this->intituleMarche->Text = ($utf8encode) ? utf8_encode($objet->getIntitule()) : $objet->getIntitule();
        } elseif ($objet instanceof CommonCategorieLot) {
            if ($utf8encode) {
                $this->intituleMarche->Text = utf8_encode($objet->getDescription(Atexo_CurrentUser::readFromSession('lang')));
            } else {
                $this->intituleMarche->Text = $objet->getDescription(Atexo_CurrentUser::readFromSession('lang'));
            }

            $this->categorie->Text = Atexo_Consultation_Category::retrieveLibelleCategorie($objet->getCategorie(), false, Atexo_CurrentUser::getOrganismAcronym());
        }
    }

    public function displayCaracteristiqueConsultation($passationConsultation)
    {
        //Layer 1
        if ($passationConsultation->getUnite()) {
            $this->unite->SelectedValue = $passationConsultation->getUnite();
        }
        if ($passationConsultation->getMandataire()) {
            $this->mandataire->SelectedValue = $passationConsultation->getMandataire();
        }
        $this->commentairesDefinitionConsultation->Text = $passationConsultation->getCommentaires();

        //Layer 2
        $this->dateReceptionSV->Text = $passationConsultation->getDateReceptionProjetDceServiceValidateur();
        $this->dateObservationsSV->Text = $passationConsultation->getDateFormulationsPremieresObservations();
        $this->dateRetourProjetDce->Text = $passationConsultation->getDateRetourProjetDceFinalise();
        $this->dateValidationProjetDce->Text = $passationConsultation->getDateValidationProjetDceParServiceValidateur();

        $this->FCSPdateEnvoiPub->Text = $passationConsultation->getDateEnvoiPublicite();
        $this->FCSPdateLimiteRemiseOffres->Text = $passationConsultation->getDateLimiteRemiseOffres();
        $this->FCSPdelaiValiditeOffres->Text = $passationConsultation->getDelaiValiditeOffres();

        if ($passationConsultation->getDateLimiteRemiseOffres() && $passationConsultation->getDateEnvoiInvitationsRemettreOffre()) {
            $delai = ceil(Atexo_Util::diffJours(Atexo_Util::iso2frnDate($passationConsultation->getDateLimiteRemiseOffres()), Atexo_Util::iso2frnDate($passationConsultation->getDateEnvoiInvitationsRemettreOffre()))) - 1;
        } elseif ($passationConsultation->getDateLimiteRemiseOffres() && $passationConsultation->getDateEnvoiPublicite()) {
            $delai = ceil(Atexo_Util::diffJours(Atexo_Util::iso2frnDate($passationConsultation->getDateLimiteRemiseOffres()), Atexo_Util::iso2frnDate($passationConsultation->getDateEnvoiPublicite()))) - 1;
        } else {
            $delai = '-';
        }
        $this->FCSPdelaiRemiseOffre->Text = $delai;

        $this->calculateDatePeremmption();

        $this->commentairesPhaseConsultation->Text = $passationConsultation->getCommentairesPhaseConsultation();
    }

    public function displayCaracteristiqueMarche($lot)
    {
        //Layer 1
        $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$lot);

        if ($passationMarcheAVenir->getIdNatureActeJuridique()) {
            $this->natureActe->SelectedValue = $passationMarcheAVenir->getIdNatureActeJuridique();
        } else {
            $this->natureActe->SelectedValue = $this->getViewState('firstElementNatureActeJuridique');
        }

        if ($passationMarcheAVenir->getModeExecutionContrat()) {
            $this->modeExeContrat->SelectedValue = $passationMarcheAVenir->getModeExecutionContrat();
        } else {
            $this->modeExeContrat->SelectedValue = $this->getViewState('firstElementModeExecution');
        }

        $this->dureeTotalMarche->Text = $passationMarcheAVenir->getDureeTotalMarche();

        //Layer 3
        $this->dateReceptionRapportSV->Text = $passationMarcheAVenir->getDateReceptionAnalyseOffre();
        $this->dateObservationsSVProjetRapport->Text = $passationMarcheAVenir->getDateFormulationObservationProjetRapport();
        $this->dateRetourProjetRapportFinalise->Text = $passationMarcheAVenir->getDateRetourProjetRapportFinalise();
        $this->dateValidationProjetRapportFinalise->Text = $passationMarcheAVenir->getDateValidationProjetRapport();

        $this->dateReceptionDossierSV->Text = $passationMarcheAVenir->getDateReceptionControleLegalite();
        $this->dateObservationsSVDossier->Text = $passationMarcheAVenir->getDateFormulationObservationDossier();
        $this->dateRetourDossierFinalise->Text = $passationMarcheAVenir->getDateRetourDossierFinalise();
        $this->dateTransmissionPrefecture->Text = $passationMarcheAVenir->getDateTransmissionPrefecture();

        $this->commentairesAttribution->Text = $passationMarcheAVenir->getCommentaire();
    }

    public function fillNatureActeJuridique()
    {
        $natures = (new Atexo_SuiviPassation_Passation())->retrieveNatureActeJuridique(Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($natures)) {
            $this->natureActe->Datasource = $natures;
            $this->natureActe->dataBind();
            $arrayKeys = array_keys($natures);
            $this->setViewState('firstElementNatureActeJuridique', array_shift($arrayKeys));
        }
    }

    public function fillModeExecutionContrat()
    {
        $modesExecutions = (new Atexo_SuiviPassation_Passation())->retrieveModeExecutionContrat(Atexo_CurrentUser::getOrganismAcronym());

        if (is_array($modesExecutions)) {
            $this->modeExeContrat->Datasource = $modesExecutions;
            $this->modeExeContrat->dataBind();
            $arrayKeys = array_keys($modesExecutions);
            $this->setViewState('firstElementModeExecution', array_shift($arrayKeys));
        }
    }

    public function fillFcspMandataire()
    {
        $fcspMandataires = (new Atexo_SuiviPassation_Passation())->retrieveFcspMandataire(Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($fcspMandataires)) {
            $this->mandataire->Datasource = $fcspMandataires;
            $this->mandataire->dataBind();
        }
    }

    public function fillFcspUnite()
    {
        $fcspUnites = (new Atexo_SuiviPassation_Passation())->retrieveFcspUnite(Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($fcspUnites)) {
            $this->unite->Datasource = $fcspUnites;
            $this->unite->dataBind();
        }
    }

    public function fillVuPar($passationConsultation, $passationMarcheAVenir)
    {
        $agents = Atexo_Agent::retriveAgentsByOrganisme(Atexo_CurrentUser::getOrganismAcronym(), false);
        if (is_array($agents)) {
            $agents = Atexo_Util::arrayUnshift($agents, Prado::localize('TEXT_SELECTIONNER').'...');

            $this->validationProjetDceVuPar->Datasource = $agents;
            $this->validationProjetDceVuPar->dataBind();
            $validationProjetDceVuPar = $passationConsultation->getDateValidationProjetDceVuePar();
            if ($validationProjetDceVuPar && isset($agents[$validationProjetDceVuPar])) {
                $this->validationProjetDceVuPar->SelectedValue = $validationProjetDceVuPar;
            }

            $this->validationProjetRapportVuPar->Datasource = $agents;
            $this->validationProjetRapportVuPar->dataBind();
            if ($passationMarcheAVenir->getProjetRapportVuPar()) {
                $validationProjetRapportVuPar = $passationMarcheAVenir->getProjetRapportVuPar();
            } else {
                $validationProjetRapportVuPar = $this->getViewState('firstElementAgents');
            }
            if ($validationProjetRapportVuPar && isset($agents[$validationProjetRapportVuPar])) {
                $this->validationProjetRapportVuPar->SelectedValue = $validationProjetRapportVuPar;
            }

            $this->notificationVuPar->Datasource = $agents;
            $this->notificationVuPar->dataBind();
            if ($passationMarcheAVenir->getDossierVuPar()) {
                $notificationVuPar = $passationMarcheAVenir->getDossierVuPar();
            } else {
                $notificationVuPar = $this->getViewState('firstElementAgents');
            }
            if ($notificationVuPar && isset($agents[$notificationVuPar])) {
                $this->notificationVuPar->SelectedValue = $notificationVuPar;
            }
            $arrayKeys = array_keys($agents);

            $this->setViewState('firstElementAgents', array_shift($arrayKeys));
        }
    }

    public function renseignerTotalLot($alloti)
    {
        if ($alloti) {
            $total = 0;
            $passationMarchesAvenirs = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultation($this->getViewState('idPassationConsultation'), Atexo_CurrentUser::getCurrentOrganism());
            if (is_array($passationMarchesAvenirs)) {
                foreach ($passationMarchesAvenirs as $passationMarcheAVenir) {
                    if ($passationMarcheAVenir instanceof CommonPassationMarcheAVenir) {
                        $total += $passationMarcheAVenir->getMontantEstime();
                    }
                }
            }
        }
    }

    public function onClikOk($sender, $param)
    {
        $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$this->getViewState('numLot'));
        if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
            $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $this->getViewState('numLot'), Atexo_CurrentUser::getCurrentOrganism());
        }
        $this->fillPassationMarcheAVenir($passationMarcheAVenir);
        $this->setViewState('passationMarcheAVenir'.$this->getViewState('numLot'), $passationMarcheAVenir);

        $consultation = $this->_consultation;
        $selectedLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultation->getId(), $this->lot->selectedValue, Atexo_CurrentUser::getCurrentOrganism());

        $this->intituleLot->Text = $this->intituleMarche->Text = (new Atexo_Config())->toPfEncoding($selectedLot->getDescription());
        $this->displayInfoMarche($selectedLot);

        $this->setViewState('numLot', $selectedLot->getLot());
        $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$selectedLot->getLot());

        if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
            $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $selectedLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
        }
        $this->setViewState('passationMarcheAVenir'.$selectedLot->getLot(), $passationMarcheAVenir);

        $this->displayCaracteristiqueMarche($selectedLot->getLot());
        $this->renseignerTotalLot(true);

        $this->fillListContrat($passationMarcheAVenir->getId());

        $this->panelCaracteristiqueMarche->render($param->NewWriter);
        $this->panelOperationValidation->render($param->NewWriter);
        $this->panelCommentaires->render($param->NewWriter);
        $this->operationValidation->render($param->NewWriter);
        $this->panelIntituleLot->render($param->NewWriter);
        $this->panelContrats->render($param->NewWriter);
    }

    public function fillPassationMarcheAVenir(&$passationMarcheAVenir)
    {
        $passationMarcheAVenir->setCommentaire($this->commentairesAttribution->Text);
        $passationMarcheAVenir->setDateFormulationObservationDossier($this->dateObservationsSVDossier->Text);
        $passationMarcheAVenir->setDateFormulationObservationProjetRapport($this->dateObservationsSVProjetRapport->Text);
        $passationMarcheAVenir->setDateReceptionAnalyseOffre($this->dateReceptionRapportSV->Text);
        $passationMarcheAVenir->setDateReceptionControleLegalite($this->dateReceptionDossierSV->Text);
        $passationMarcheAVenir->setDateRetourDossierFinalise($this->dateRetourDossierFinalise->Text);
        $passationMarcheAVenir->setDateRetourProjetRapportFinalise($this->dateRetourProjetRapportFinalise->Text);
        $passationMarcheAVenir->setDateTransmissionPrefecture($this->dateTransmissionPrefecture->Text);
        $passationMarcheAVenir->setDateValidationProjetRapport($this->dateValidationProjetRapportFinalise->Text);

        //if($this->notificationVuPar->SelectedValue) {
        $passationMarcheAVenir->setDossierVuPar($this->notificationVuPar->SelectedValue);
        //}
        $passationMarcheAVenir->setDureeTotalMarche($this->dureeTotalMarche->Text);

        //if($this->natureActe->SelectedValue) {
        $passationMarcheAVenir->setIdNatureActeJuridique($this->natureActe->SelectedValue);
        //}

        //if($this->modeExeContrat->SelectedValue) {
        $passationMarcheAVenir->setModeExecutionContrat($this->modeExeContrat->SelectedValue);
        //}
        //if($this->validationProjetRapportVuPar->SelectedValue) {
        $passationMarcheAVenir->setProjetRapportVuPar($this->validationProjetRapportVuPar->SelectedValue);
        //}
    }

    public function fillPassationConsultation(&$passationConsultation)
    {
        $passationConsultation->setCommentaires($this->commentairesDefinitionConsultation->Text);
        $passationConsultation->setCommentairesPhaseConsultation($this->commentairesPhaseConsultation->Text);
        $passationConsultation->setDateEnvoiPublicite($this->FCSPdateEnvoiPub->Text);
        $passationConsultation->setDateFormulationsPremieresObservations($this->dateObservationsSV->Text);
        $passationConsultation->setDateLimiteRemiseOffres($this->FCSPdateLimiteRemiseOffres->Text);
        $passationConsultation->setDateReceptionProjetDceServiceValidateur($this->dateReceptionSV->Text);
        $passationConsultation->setDateRetourProjetDceFinalise($this->dateRetourProjetDce->Text);
        $passationConsultation->setDateValidationProjetDceParServiceValidateur($this->dateValidationProjetDce->Text);
        $passationConsultation->setDateValidationProjetDceVuePar($this->validationProjetDceVuPar->Text);

        $passationConsultation->setDelaiValiditeOffres($this->FCSPdelaiValiditeOffres->Text);
        //if($this->mandataire->SelectedValue) {
        $passationConsultation->setMandataire($this->mandataire->SelectedValue);
        //}
        //if($this->unite->SelectedValue) {
        $passationConsultation->setUnite($this->unite->SelectedValue);
        //}
    }

    public function onEnregistrerClick($sender, $param)
    {
        if ($this->_consultation->getAlloti()) {
            $lotChoisi = $this->lot->selectedValue;
        } else {
            $lotChoisi = '0';
        }
        $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$lotChoisi);
        if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
            $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $lotChoisi, Atexo_CurrentUser::getCurrentOrganism());
        }

        $this->fillPassationMarcheAVenir($passationMarcheAVenir);
        $this->setViewState('passationMarcheAVenir'.$lotChoisi, $passationMarcheAVenir);

        $passationConsultation = $this->getViewState('passationConsultation');
        $this->fillPassationConsultation($passationConsultation);

        $propelConnecion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $passationConsultation->save($propelConnecion);
        if ($this->_consultation->getAlloti()) {
            $listeLots = $this->_consultation->getAllLots();
            if (is_array($listeLots)) {
                foreach ($listeLots as $lot) {
                    $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$lot->getLot());
                    if ($passationMarcheAVenir instanceof CommonPassationMarcheAVenir) {
                        $passationMarcheAVenir->save($propelConnecion);
                    }
                }
            }
        } else {
            $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir0');
            if ($passationMarcheAVenir instanceof CommonPassationMarcheAVenir) {
                $passationMarcheAVenir->save($propelConnecion);
            }
        }

        $this->response->redirect('index.php?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function calculateDatePeremmption()
    {
        $date = $this->FCSPdateLimiteRemiseOffres->Text;
        $nombreJours = $this->FCSPdelaiValiditeOffres->Text ?: 0;
        if ($date) {
            [$j, $m, $y] = explode('/', $date);
            $timeStamp = mktime(0, 0, 0, $m, $j, $y) + ($nombreJours * 24 * 60 * 60);
            $resultat = date('d/m/Y', $timeStamp);
        } else {
            $resultat = '-';
        }

        $this->datePeremptionOffre->Text = $resultat;
    }

    public function fillListContrat($idPassation)
    {
        $listeContrats = (new Atexo_SuiviPassation_Contrat())->retrieveContratByIdPassation($idPassation, Atexo_CurrentUser::getCurrentOrganism());
        $arrayContratTitulaire = [];
        if (is_array($listeContrats)) {
            foreach ($listeContrats as $contrat) {
                $tConsLotContrat = (new Atexo_Consultation_Contrat())->getConsLotsContratByIdContratAndLot($contrat->getIdDecision(), $this->lot->selectedValue);
                if ($tConsLotContrat instanceof CommonTConsLotContrat) {
                    $arrayContratTitulaire[] = $tConsLotContrat;
                }
            }
        }
        $this->contrats->DataSource = $arrayContratTitulaire;
        $this->contrats->DataBind();
    }

    public function calculDelaiValiditeOffre($sender, $param)
    {
        if ($this->FCSPdateLimiteRemiseOffres->Text && $this->FCSPdateEnvoiPub->Text) {
            $delai = ceil(Atexo_Util::diffJours(Atexo_Util::iso2frnDate($this->FCSPdateLimiteRemiseOffres->Text), Atexo_Util::iso2frnDate($this->FCSPdateEnvoiPub->Text))) - 1;
        } else {
            $delai = '-';
        }
        $this->FCSPdelaiRemiseOffre->Text = $delai;
        $this->panelDelaiRemiseOffre->render($param->NewWriter);
    }

    public function saveMandataire($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationConsultation = $this->getViewState('passationConsultation');
        $passationConsultation->setMandataire($sender->getSelectedValue());
        $passationConsultation->save($connexion);
        $this->setViewState('passationConsultation', $passationConsultation);
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatDate')) {
            $this->Validateur1->Enabled = false;
            $this->Validateur2->Enabled = false;
            $this->Validateur3->Enabled = false;
            $this->Validateur4->Enabled = false;
            $this->Validateur5->Enabled = false;
            $this->Validateur6->Enabled = false;
            $this->Validateur7->Enabled = false;
            $this->Validateur8->Enabled = false;
            $this->Validateur9->Enabled = false;
            $this->Validateur10->Enabled = false;
            $this->Validateur11->Enabled = false;
            $this->Validateur12->Enabled = false;
            $this->Validateur13->Enabled = false;
            $this->Validateur14->Enabled = false;
            $this->Validateur15->Enabled = false;
            $this->Validateur16->Enabled = false;
            $this->Validateur17->Enabled = false;
            $this->Validateur18->Enabled = false;
            $this->Validateur19->Enabled = false;
            $this->Validateur20->Enabled = false;
            $this->Validateur21->Enabled = false;
            $this->Validateur22->Enabled = false;
            $this->Validateur23->Enabled = false;
            $this->Validateur24->Enabled = false;
            $this->Validateur25->Enabled = false;
            $this->Validateur26->Enabled = false;
            $this->Validateur27->Enabled = false;
            $this->Validateur28->Enabled = false;
        }
    }

    public function appliquerModifEnsembleLots($sender, $param)
    {
        $passationMarcheAVenir = $this->getViewState('passationMarcheAVenir'.$this->getViewState('numLot'));
        if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
            $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($this->getViewState('idPassationConsultation'), $this->getViewState('numLot'), Atexo_CurrentUser::getCurrentOrganism());
        }
        $this->fillPassationMarcheAVenir($passationMarcheAVenir);
        $lots = $this->_consultation->getAllLots();
        $nombreLots = is_countable($lots) ? count($lots) : 0;
        if (is_array($lots) && $nombreLots > 0) {
            foreach ($lots as $unLot) {
                $passationMarcheAVenirLotEnCour = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($passationMarcheAVenir->getIdPassationConsultation(), $unLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
                if ($passationMarcheAVenirLotEnCour instanceof CommonPassationMarcheAVenir) {
                    $idPassationMAV = $passationMarcheAVenirLotEnCour->getId();
                    $passationMarcheAVenirLotEnCour = $passationMarcheAVenir->copy();
                    $passationMarcheAVenirLotEnCour->setId($idPassationMAV);
                    $passationMarcheAVenirLotEnCour->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $passationMarcheAVenirLotEnCour->setLot($unLot->getLot());
                    $passationMarcheAVenirLotEnCour->setNew(false);
                }
                $this->setViewState('passationMarcheAVenir'.$unLot->getLot(), $passationMarcheAVenirLotEnCour);
            }
        }
    }

    /**
     * Permet de verifier si une consultation a au moins 1 lot.
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @return true si la consultation a au moins 1 lot, false sinon
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isConsultationHasLots()
    {
        if ($this->_consultation instanceof CommonConsultation) {
            return is_array($this->_consultation->getAllLots()) && count($this->_consultation->getAllLots());
        }
    }
}
