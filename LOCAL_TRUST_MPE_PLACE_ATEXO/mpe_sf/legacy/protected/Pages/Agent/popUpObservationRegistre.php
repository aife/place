<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Propel\Mpe\CommonRetraitPapier;
use Application\Propel\Mpe\CommonRetraitPapierPeer;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpObservationRegistre extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->panelMessageErreur->setVisible(false);
            $this->panelErreur->setVisible(false);
            $this->panelResizeErreur->setVisible(false);
            $this->panelResizeContainer->setVisible(false);
            $messageError = '';
            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsElectronique') ||
              Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsElectronique') ||
              Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsElectronique') ||
              Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier') ||
              Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier') ||
              Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                if (isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['type']) && isset($_GET['callBackButton'])) {
                    $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);
                    $idRegistre = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                          $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER') ||
                          $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                          $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER') ||
                          $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
                          $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                        if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsElectronique') &&
                             $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE')) {
                            $registre = CommonTelechargementPeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($registre instanceof CommonTelechargement) {
                                $this->setViewState('registre', $registre);
                                $this->observations->Text = $registre->getObservation();
                            } else {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_RETRAIT');
                            }
                        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                            $registre = CommonRetraitPapierPeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($registre instanceof CommonRetraitPapier) {
                                $this->setViewState('registre', $registre);
                                $this->observations->Text = $registre->getObservation();
                            } else {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_RETRAIT');
                            }
                        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                                    $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')
                                   ) {
                            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $registre = CommonQuestionsDcePeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($registre instanceof CommonQuestionsDce) {
                                $this->setViewState('registre', $registre);
                                $this->observations->Text = $registre->getObservation();
                                $this->agent->Text = $registre->getPersonneRepondu();
                                if ('0000-00-00' != $registre->getDateReponse()) {
                                    $this->dateTraitement->Text = Atexo_Util::iso2frnDate($registre->getDateReponse());
                                }
                            } else {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_QUESTION');
                            }
                        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE')) {
                            $registre = CommonOffresPeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($registre instanceof CommonOffres) {
                                $this->setViewState('registre', $registre);
                                $this->observations->Text = $registre->getObservation();
                            } else {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_DEPOT');
                            }
                        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                            $registre = CommonOffrePapierPeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($registre instanceof CommonOffrePapier) {
                                $this->setViewState('registre', $registre);
                                $this->observations->Text = $registre->getObservation();
                            } else {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_DEPOT');
                            }
                        }
                        $this->panelResizeContainer->setVisible(true);
                    } else {
                        $messageError = Prado::localize('ERREUR_TYPE_REGISTRE');
                    }
                }
            } else {
                $messageError = Prado::localize('ERREUR_HABILITATION_REGISTRES');
            }
            if ($messageError) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelErreur->setVisible(true);
                $this->container->setVisible(false);
                $this->panelMessageErreur->setMessage($messageError);
                $this->panelResizeErreur->setVisible(true);
            }
        }
    }

    public function onValiderClick()
    {
        $registre = $this->getViewState('registre');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $registre->setObservation($this->observations->Text);
        if ($this->isRegistreQuestion()) {
            $registre->setPersonneRepondu($this->agent->Text);
            $registre->setDateReponse(Atexo_Util::frnDate2iso($this->dateTraitement->Text));
        }
        $registre->save($connexionCom);
        $this->userScript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackButton'])."').click();window.close();</script>";
    }

    public function isRegistreQuestion()
    {
        $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
          $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            return true;
        } else {
            return false;
        }
    }
}
