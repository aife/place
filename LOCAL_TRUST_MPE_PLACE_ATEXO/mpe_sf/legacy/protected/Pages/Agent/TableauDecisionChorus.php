<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauDecisionChorus extends MpeTPage
{
    protected string $_critereTri = '';
    protected string $_triAscDesc = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    private $_idService;
    public $_consultation;
    public $_refConsultation;
    public $_thColspan;
    public $_nombreLots;

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        if (isset($_GET['telechargerOffres'])) {
            if (!Atexo_CurrentUser::hasHabilitation('TelechargementUnitairePlisChiffres')) {
                $this->ConsultationSummary->setWithException(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_TELECHARER_PLI_PAR_PLI'));
            }
        }

        if (isset($_GET['id'])) {
            $this->javascript->Text = '';
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->setViewState('organisme', Atexo_CurrentUser::getCurrentOrganism());

            $this->_idService = Atexo_CurrentUser::getIdServiceAgentConnected();
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService($this->_idService);
            $criteria->setAcronymeOrganisme($this->getViewState('organisme'));
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $this->_consultation = array_shift($consultation);

                if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') ||
                   $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                   || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')
                     || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')
                      || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
                    $this->ConsultationSummary->setConsultation($this->_consultation);
                    $this->consultationReplyTerms->setConsultation($this->_consultation);

                    if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                        $this->etape3->setId('etape234');
                    } elseif ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')) {
                        $this->etape3->setId('etape34');
                    } elseif ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                        $this->etape3->setId('etape4');
                    }

                    if (!$this->IsPostBack) {
                        $lotsConsultation = $this->_consultation->getAllLots();
                        $nombreLot = is_countable($lotsConsultation) ? count($lotsConsultation) : 0;
                        if (1 == $nombreLot) {
                            $nombreLot = 0;
                        }
                        $this->_nombreLots = $nombreLot;
                        $this->setViewState('nombreLot', $nombreLot);
                        if (!is_array($lotsConsultation)) {
                            throw new Atexo_Exception('Expected function to return an array');
                        }

                        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                            $constitutionDossierReponse = (new Atexo_Consultation_Responses())->getTypesEnveloppes($this->_consultation->getEnvCandidature(), $this->_consultation->getEnvOffre(), $this->_consultation->getEnvAnonymat(), $this->_consultation->getEnvOffreTechnique());

                            $this->setViewState('typesEnveloppes', $constitutionDossierReponse);

                            $this->setViewState('dateFin', $this->_consultation->getDatefin());

                            $this->BlocTableauDecisionsChorus->setVisible(false);

                            if (isset($_GET['decision']) && Atexo_CurrentUser::hasHabilitation('AttributionMarche') &&
                                       ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                                        || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                                        || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')
                                         || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')
                                         || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'))) {
                                $this->BlocTableauDecisionsChorus->setVisible(true);
                                $this->chargerListeDecisionsChorus();
                            } elseif (isset($_GET['decision'])) {
                                $this->ConsultationSummary->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_DECISION'));
                            }
                        } else {
                            if (isset($_GET['decision']) && Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
                                $this->BlocTableauDecisionsChorus->setVisible(true);
                                $this->chargerListeDecisionsChorus();
                            } elseif (isset($_GET['decision'])) {
                                $this->ConsultationSummary->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_DECISION'));
                            } else {
                                $this->ConsultationSummary->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DROIT_OA'));
                            }
                        }
                    }
                } else {
                    $this->ConsultationSummary->setWithException(false);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_CONSULTATION_PAS_OA'));
                }
            } else {
                $this->ConsultationSummary->setWithException(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_CONSULTATION_PAS_OA'));
            }
        } else {
            $this->ConsultationSummary->setWithException(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
        }

        if ($this->_consultation
        && ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')
        || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'))) {
            $this->linkRetour->NavigateUrl = 'index.php?page=Agent.ArchiveTableauDeBord&id='.$this->_consultation->getId();
        } else {
            $this->linkRetour->NavigateUrl = 'index.php?page=Agent.TableauDeBord&id='.$this->_consultation->getId();
        }
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setNombreLots($nombreLot)
    {
        $this->_nombreLots = $nombreLot;
    }

    public function getNumeroMarche($numeroMarche, $statut)
    {
        $numMarche = '';
        $array = (new Atexo_Chorus_Util())->returnInformationRetourChorus($numeroMarche, $statut);
        if (is_array($array) && count($array)) {
            foreach ($array as $tab) {
                $numMarche = $tab['numero'];
            }
        }

        return $numMarche;
    }

    /**
     * Permet de recuperer la liste des contrats pour la consultation.
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function chargerListeDecisionsChorus()
    {
        $listeContrats = $this->getDataSource();
        $this->tableauDecisionsChorus->DataSource = $listeContrats;
        $this->tableauDecisionsChorus->DataBind();
    }

    /**
     * Permet de recuperer la source des donnees.
     *
     * @return null
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getDataSource()
    {
        if (!$this->getViewState('listeContrats') && $this->getConsultation() instanceof CommonConsultation) {
            $listeContrats = (new Atexo_Consultation_Contrat())->recupererListeContratsParConsultation($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme());
            $this->setViewState('listeContrats', $listeContrats);
        }

        return $this->getViewState('listeContrats');
    }

    /**
     * Permet de recuperer la date de notification.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return mixed|string|null
     *
     * @throws PropelException
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getDateNotification($contrat)
    {
        $dateNotif = null;
        if ($contrat instanceof CommonTContratTitulaire) {
            if ($contrat->getDateNotification()) {
                $dateNotif = $contrat->getDateNotification();
            } elseif ($contrat->getDatePrevueNotification()) {
                $dateNotif = $contrat->getDatePrevueNotification().' (P)';
            }
        }

        return $dateNotif;
    }

    /**
     * Construit le titre de la date de notification.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return string|null
     *
     * @throws PropelException
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getTitreDateNotification($contrat)
    {
        $libelleDateNotif = null;
        if ($contrat instanceof CommonTContratTitulaire) {
            if ($contrat->getDateNotification()) {
                $libelleDateNotif = Prado::localize('DATE_NOTIFICATION');
            } elseif ($contrat->getDatePrevueNotification()) {
                $libelleDateNotif = Prado::localize('DATE_NOTIFICATION_PROVISOIRE');
            }
        }

        return $libelleDateNotif;
    }

    /**
     * Permet de construire le lien d'acces a la liste des echanges chorus.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat titulaire
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getLienPageEchangesChorus($contrat)
    {
        if ($this->getConsultation() instanceof CommonConsultation && $contrat instanceof CommonTContratTitulaire) {
            return 'index.php?page=Agent.ChorusEchanges&id='.base64_encode($this->getConsultation()->getId()).'&idContrat='.base64_encode($contrat->getIdContratTitulaire()).'&fromTablDeci=1&action=decision';
        }
    }

    /**
     * Permet de gerer l'affichage du picto d'acces aux details de l'echange chorus.
     *
     * @param CommonTContratTitulaire $contrat: objet contrat
     *
     * @return bool
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getActionAccesDetailsEchangeChorus($contrat)
    {
        $listeStatuts = [Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'), Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')];
        if ($contrat instanceof CommonTContratTitulaire && (new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected()) && '' != $contrat->getModeEchangeChorus() && in_array($contrat->getStatutContrat(), $listeStatuts)) {
            return true;
        }

        return false;
    }

    /**
     * Precise si le contrat est accord cadre/SAD.
     *
     * @param string $idContrat : identifiant du contrat
     *
     * @return bool : true si AC/SAD false sinon
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function isContratAcSad($idContrat)
    {
        try {
            return (new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($idContrat);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la determination du type de contrat : '.$e->getMessage());
        }
    }
}
