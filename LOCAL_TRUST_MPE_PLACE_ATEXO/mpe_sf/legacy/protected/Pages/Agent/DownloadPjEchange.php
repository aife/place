<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

/**
 * Permet de télécharger le DCE.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadPjEchange extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultationId = false;
        $idPj = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
        if ($_GET['org']) {
            $org = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $pj = Atexo_Message::getPjByIdPiece($idPj, $org);

        if ($pj instanceof CommonEchangePieceJointe) {
            $echange = Atexo_Message::retrieveMessageById($pj->getIdMessage(), $org);
            if ($echange) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($echange->getConsultationId());
                if ($consultation) {
                    $consultationId = $consultation->getId();
                }
            }

            if ($consultationId) {
                $criteriaVo = new Atexo_Consultation_CriteriaVo();
                $criteriaVo->setIdReference($consultationId);
                $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
                $criteriaVo->setAcronymeOrganisme($org);

                $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);

                if (1 == count($arrayConsultation)) {
                    $this->_idFichier = $idPj;
                    $this->_nomFichier = $pj->getNomFichier();
                    $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
                } else {
                    exit;
                }
            }
        } else {
            if (!$idPj) {
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
                if ($consultation instanceof CommonConsultation) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_Util::atexoHtmlEntities($_GET['id']));
                    if ($avisPub instanceof CommonAvisPub) {
                        $pieceJointe = (new Atexo_Publicite_AvisPub())->CommonAnnoncePressPieceJointeByIdAndOrganisme($avisPub->getIdAvisPresse(), $avisPub->getOrganisme(), $connexion);
                        if ($pieceJointe instanceof CommonAnnoncePressPieceJointe) {
                            $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avisPub->getTypeAvis());
                            if ($modele) {
                                $fileRtf = (new Atexo_Publicite_AvisPub())->getFileContent($avisPub, $consultation, true);
                                static::downloadFileContent($pieceJointe->getNomFichier(), file_get_contents($fileRtf));
                            }
                        }
                    }
                }
            } else {
                $pj = (new Atexo_Publicite_AvisPub())->getAnnoncePressPjByIdPj($idPj, $org);
                if (Atexo_Module::isEnabled('PubliciteOpoce') && ($pj instanceof CommonAnnoncePressPieceJointe)) {
                    $this->downloadFiles($idPj, $pj->getNomFichier(), $org);
                }
            }
        }
    }
}
