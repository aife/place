<?php

namespace Application\Pages\Agent;

use App\Entity\ContratTitulaire;
use App\Service\AtexoEchangeChorus;
use App\Service\Routing\RouteBuilderInterface;
use App\Service\WebServices\WebServicesExec;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonChorusPjPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ChorusEchanges extends MpeTPage
{
    private $_organisme = '';
    private $_contratExec = null;
    protected $_critereTri = '';
    protected $_triAscDesc = '';
    private $_decision = '';
    private $_contrat = '';
    private $_consultation = '';
    private $_lots = null;
    private string $typeFlux = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * @throws Atexo_Consultation_Exception
     */
    public function onLoad($param)
    {
        $lots = [];
        $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
        $this->displayEchanges();
        $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        if (isset($_GET['idContrat']) || isset($_GET['uuid'])) {

            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                $uuidContratExec = $_GET['uuid'];
                if ($uuidContratExec) {
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);
                    if ($contratExec['type']['codeExterne']) {
                        $this->typeFlux = $this->isContratAcSadExec($contratExec['type']['codeExterne']) ? 'FEN211' : 'FEN111';
                    }
                    $this->_contratExec = $contratExec;
                    $this->infosContrat->initialiserComposant(implode('#', $lots), 5108);
                } else {
                    $msgErreur = "Aucune décision n'a été faites";
                    $this->infosContrat->setVisible(false);
                }

                $this->infosContrat->initialiserComposant(implode('#', $lots), null);
            } else {
                $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
                $this->accessContract($idContrat);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $this->_contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);
                if ($this->_contrat instanceof CommonTContratTitulaire) {
                    $lotConsContrat = (new Atexo_Consultation_Contrat())->getListeConsLotsContratByIdContrat($idContrat);
                    if (is_array($lotConsContrat)) {
                        foreach ($lotConsContrat as $oneEl) {
                            if ($oneEl instanceof CommonTConsLotContrat) {
                                $consultationId = $oneEl->getConsultationId();
                                $lots[] = $oneEl->getLot();
                            }
                        }
                    }
                    $this->_lots = $lots;
                    $this->infosContrat->initialiserComposant(implode('#', $lots), $this->_contrat->getIdContratTitulaire());
                } else {
                    $msgErreur = "Aucune décision n'a été faites";
                    $this->infosContrat->setVisible(false);
                }
            }
        } else {
            $msgErreur = "Impossible d'accéder à la fiche du marché. Paramètre manquant."; //TODO msg err
            $this->infosContrat->setVisible(false);
        }

        if (isset($_GET['fromTablDeci'])) {
            $this->linkRetour->NavigateUrl = 'index.php?page=Agent.TableauDecisionChorus&id='.$consultationId.'&decision';
        } elseif (isset($_GET['fromCons'])) {
            $this->linkRetour->NavigateUrl = 'index.php?page=Agent.ouvertureEtAnalyse&id='.$consultationId.'&decision';
        } else {
            $this->linkRetour->NavigateUrl = '?page=Agent.ResultatRechercheContrat&retour';
        }
    }

    private function accessContract($idContrat)
    {
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setFindOne(true);
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setIdAgentConnecte(Atexo_CurrentUser::getId());
        $criteriaVo->setIdContratTitulaire($idContrat);
        $propelConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contract = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo, $propelConnexion);

        if (!$contract instanceof CommonTContratTitulaire) {
            if ($this->accessContratForDecision()) {
                return $this->getResponse()->redirect('/agent/');
            }
        }
    }

    private function accessContratForDecision()
    {
        $noAccess = true;
        $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService($idService);
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if ('decision' === $_GET['action']
            && count($consultation) > 0
            && $consultation[0] instanceof CommonConsultation) {
            $noAccess = false;
        }

        return $noAccess;
    }

    /**
     * Ajoutr un echange dans la base de donnée.
     */
    public function addEchangeChorus($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $newEchange = new CommonChorusEchange();
        $newEchange->setOrganisme($this->_organisme);

        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
            $uuidContratExec = $_GET['uuid'];
            $lastEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreatedWithUuidExec($this->_organisme, $uuidContratExec);
        } else {
            $lastEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreated($this->_organisme, $this->_contrat->getIdContratTitulaire());
        }

        // @TODO OK KBE $lastEchange = Atexo_Chorus_Echange::retreiveLastEchangeCreated($this->_organisme,$this->_decision->getIdDecisionEnveloppe());
        $numOrdre = 0;
        if ($lastEchange) {
            $numOrdre = $lastEchange->getNumOrdre();
        }
        $newEchange->setNumOrdre(1 + intval($numOrdre));

        /** @var AtexoEchangeChorus $chorusEchangeService */
        $chorusEchangeService = Atexo_Util::getSfService(AtexoEchangeChorus::class);

        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
            /** @var $execWS WebServicesExec */
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);

            if ($contratExec) {
                $newEchange->setCodePaysTitulaire($contratExec['attributaire']['etablissement']['fournisseur']['pays'] ?? null);
                $newEchange->setNumeroSirenTitulaire($contratExec['attributaire']['etablissement']['fournisseur']['siren']);
                $newEchange->setRaisonSocialeAttributaire($contratExec['attributaire']['etablissement']['fournisseur']['raisonSociale']);
                $newEchange->setFormeJuridique($contratExec['attributaire']['etablissement']['fournisseur']['formeJuridique']);

                $organismeAcronyme = $contratExec['createur']['service']['organisme']['idExterne'] ?? null;
                if ($organismeAcronyme) {
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($organismeAcronyme);
                    if ($organisme instanceof CommonOrganisme) {
                        $newEchange->setSiren($organisme->getSiren());
                        $newEchange->setSiret($organisme->getComplement());
                    }
                }

                $newEchange->setPme($contratExec['attributaire']['etablissement']['fournisseur']['categorieEntreprise'] === ContratTitulaire::CATEGORIE_ENTREPRISE_PME);
                if ($sirenEtrange = $contratExec['attributaire']['etablissement']['fournisseur']['numeroNational']) {
                    $newEchange->setNumeroNationalAttributaire($sirenEtrange);
                }

                $idOffre = $contratExec['idOffre'];
                if ($idOffre) {
                    $offre = (new Atexo_Interfaces_Offre())->getOffreByIdOffre(8);
                    if ($offre instanceof CommonOffres) {
                        $enveloppes = $offre->getCommonEnveloppes(false, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $this->_lots[0]);
                        if ($enveloppes && ($this->_consultation instanceof CommonConsultation)) {
                            if ($lastEchange) {
                                $notChecked = true;
                            } else {
                                $notChecked = false;
                            }
                            $idsRapport = (new Atexo_Consultation_Responses())->getListeRapportSignatures($enveloppes, $this->_consultation, $offre->getTypeSignature(), $notChecked, 'agent');
                            $newEchange->setIdsRapportSignature($idsRapport);
                        }
                    }
                }

                if (
                    $contratExec['type']['codeExterne']
                    && !$this->isContratAcSadExec($contratExec['type']['codeExterne'])
                    && $this->isContratNotifieEtEjCommandeExec($contratExec['statutEj'], $contratExec['statutContrat'])
                ) {
                    if ($this->typeEnvoiPj->checked) {
                        $newEchange->setTypeEnvoi(Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_PJ'));
                    } elseif ($this->typeEnvoiActeModif->checked) {
                        $newEchange->setTypeEnvoi(Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_ACTE_MODIF'));
                    }
                }

                if ($contratExec['lienAcSad']) {
                    $lienAcSadExec = $execWS->getContent('getContratByUuid', $contratExec['lienAcSad']);
                    if ($lienAcSadExec) {
                        $newEchange->setIdentifiantAccordCadre($lienAcSadExec['numeroLong']);
                    }
                }
                $newEchange->setPaysTerritoire($contratExec['attributaire']['etablissement']['adresse']['pays']['label'] ?? null);
                $newEchange->setCodeApe($contratExec['attributaire']['etablissement']['fournisseur']['codeApeNafNace'] ?? null);

                $codeSiret = $contratExec['attributaire']['etablissement']['siret'] ?? null;
                $newEchange->setNumeroSiretTitulaire($codeSiret ? substr($codeSiret, -5) : null);
                $newEchange->setSiretAttributaire($contratExec['attributaire']['etablissement']['fournisseur']['siren'] ?? null);

                $newEchange->setDateFinMarche(
                    $contratExec['datePrevisionnelleFinMarche']
                        ? (new \DateTime($contratExec['datePrevisionnelleFinMarche']))->format('Y-m-d')
                        : null
                );
                $newEchange->setDateFinMarcheReelle(
                    $contratExec['dateFinContrat']
                        ? (new \DateTime($contratExec['dateFinContrat']))->format('Y-m-d')
                        : null
                );
                $newEchange->setDateNotification(
                    $contratExec['datePrevisionnelleNotification']
                        ? (new \DateTime($contratExec['datePrevisionnelleNotification']))->format('Y-m-d')
                        : null
                );
                $newEchange->setDateNotificationReelle(
                    $contratExec['dateNotification']
                        ? (new \DateTime($contratExec['dateNotification']))->format('Y-m-d')
                        : null
                );
            }

        } else {
            // @TODO OK KBE if(($this->_decision instanceof CommonDecisionEnveloppe) ){
            if (($this->_contrat instanceof CommonTContratTitulaire)) {
                /* @TODO KBE if($this->_decision->getPaysAttributaire()){
                $codePays =  Atexo_Geolocalisation_GeolocalisationN2::retrieveAccronymeCountryByDenomination($this->_decision->getPaysAttributaire());
                $newEchange->setCodePaysTitulaire($codePays);
                }else{
                $newEchange->setCodePaysTitulaire(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                }
                if($this->_decision->getSirenAttributaire() && $this->_decision->getNicAttributaire()) {
                $newEchange->setNumeroSirenTitulaire($this->_decision->getSirenAttributaire());
                $newEchange->setNumeroSiretTitulaire($this->_decision->getNicAttributaire());
                } KBE */
                $titulaire = $this->_contrat->getTitulaireContrat();
                $etabTitilaire = $this->_contrat->getTitulaireEtablissementContrat();
                if ($titulaire instanceof Entreprise) {
                    $newEchange->setCodePaysTitulaire($titulaire->getPaysenregistrement());
                    $newEchange->setNumeroSirenTitulaire($titulaire->getSiren());
                    $newEchange->setRaisonSocialeAttributaire($titulaire->getNom());
                    $newEchange->setFormeJuridique($titulaire->getFormejuridique());
                    $newEchange->setPme($titulaire->getCategorieEntreprise() === ContratTitulaire::CATEGORIE_ENTREPRISE_PME);
                    $newEchange->setPaysTerritoire($titulaire->getPaysadresse());
                    $newEchange->setNumeroNationalAttributaire($titulaire->getSirenetranger());
                    $newEchange->setCodeApe($chorusEchangeService->getCodeApe(trim($titulaire->getCodeape())));
                }
                if ($etabTitilaire instanceof CommonTEtablissement) {
                    $newEchange->setNumeroSiretTitulaire($etabTitilaire->getCodeEtablissement());
                }

                if ($titulaire && $etabTitilaire) {
                    $newEchange->setSiretAttributaire($chorusEchangeService->getSiretAttributaire(
                        $titulaire->getSiren(),
                        $etabTitilaire->getCodeEtablissement()
                    ));
                }

                if($this->_contrat->getTypeDepotReponse() == Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE')) {
                    $offre = $this->_contrat->getOffresElectronique();
                    if ($offre instanceof CommonOffres) {
                        // @TODO KBE filtre lot $enveloppes = $this->_decision->getEnveloppes();
                        $enveloppes = $offre->getCommonEnveloppes(false, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $this->_lots[0]);
                        if ($enveloppes && ($this->_consultation instanceof CommonConsultation)) {
                            if ($lastEchange) {
                                $notChecked = true;
                            } else {
                                $notChecked = false;
                            }
                            $idsRapport = (new Atexo_Consultation_Responses())->getListeRapportSignatures($enveloppes, $this->_consultation, $offre->getTypeSignature(), $notChecked, 'agent');
                            $newEchange->setIdsRapportSignature($idsRapport);
                        }
                    }
                }
                $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $contratLien = $contratQuery->getTContratTitulaireById($this->_contrat->getLienAcSad());
                if ($contratLien instanceof CommonTContratTitulaire) {
                    $identifiantAC = $contratLien->getNumLongOeap();
                } else {
                    $identifiantAC = '';
                }
                $newEchange->setIdentifiantAccordCadre($identifiantAC);
            }

            // permet d'initialiser les clauses sociales et environnementales si la consultation en fait l'objet
            /* @TODO  OK KBE
             *
            if(Atexo_Consultation::hasClauseSocialeConditionExecution($this->_consultation) || Atexo_Consultation::hasClauseSocialeAteliersProteges($this->_consultation)) {
            $newEchange->setClauseSociale('1');
            }
            else {
            $newEchange->setClauseSociale('0');
            }
            if(Atexo_Consultation::hasClauseEnvironnementales($this->_consultation)) {
            $newEchange->setClauseEnvironnementale('1');
            }
            else {
            $newEchange->setClauseEnvironnementale('0');
            }
             * KBE */

            if (Atexo_Consultation::hasClauseSociale($this->_contrat)) {
                $newEchange->setClauseSociale('1');
            } else {
                $newEchange->setClauseSociale('0');
            }
            if (Atexo_Consultation::hasClauseEnvironnementales($this->_contrat)) {
                $newEchange->setClauseEnvironnementale('1');
            } else {
                $newEchange->setClauseEnvironnementale('0');
            }
        }

        //si jamais on a plus de 1 échange on copie les autres infos dedans
        //notamment OA/GA/Type de marché/date de notification qui ne doit pas changer pour un EJ
        if ($lastEchange) {
            $newEchange->setIdOa($lastEchange->getIdOa());
            $newEchange->setIdGa($lastEchange->getIdGa());
            $newEchange->setIdTypeMarche($lastEchange->getIdTypeMarche());
            $newEchange->setIdTypeGroupement($lastEchange->getIdTypeGroupement());
            $newEchange->setIdRegroupementComptable($lastEchange->getIdRegroupementComptable());
            //Informations dela fiche de recensement
            $newEchange->setSiren($lastEchange->getSiren());
            $newEchange->setSiret($lastEchange->getSiret());
            $newEchange->setIdActeJuridique($lastEchange->getIdActeJuridique());
            $newEchange->setCpv1($lastEchange->getCpv1());
            $newEchange->setCpv2($lastEchange->getCpv2());
            $newEchange->setIdTypeProcedure($lastEchange->getIdTypeProcedure());
            $newEchange->setIdFormePrix($lastEchange->getIdFormePrix());
            $newEchange->setNbrEntreprisesCotraitantes((int) $lastEchange->getNbrEntreprisesCotraitantes());
            $newEchange->setSousTraitanceDeclaree($lastEchange->getSousTraitanceDeclaree());
            $newEchange->setCarteAchat($lastEchange->getCarteAchat());
            $newEchange->setClauseSociale($lastEchange->getClauseSociale());
            $newEchange->setClauseEnvironnementale($lastEchange->getClauseEnvironnementale());
            $newEchange->setNbrPropositionRecues($lastEchange->getNbrPropositionRecues());
            $newEchange->setNbrPropositionDematerialisees($lastEchange->getNbrPropositionDematerialisees());
            $newEchange->setMontantHt($lastEchange->getMontantHt());
            $newEchange->setCodePaysTitulaire($lastEchange->getCodePaysTitulaire());
            $newEchange->setNumeroSiretTitulaire($lastEchange->getNumeroSiretTitulaire());
            $newEchange->setNumeroSirenTitulaire($lastEchange->getNumeroSirenTitulaire());
            $newEchange->setCodesPaysCoTitulaire($lastEchange->getCodesPaysCoTitulaire());
            $newEchange->setNumeroSiretCoTitulaire($lastEchange->getNumeroSiretCoTitulaire());
            $newEchange->setNumeroSirenCoTitulaire($lastEchange->getNumeroSirenCoTitulaire());
            $newEchange->setCcagReference($lastEchange->getCcagReference());
            $newEchange->setPourcentageAvance($lastEchange->getPourcentageAvance());
            $newEchange->setTypeAvance($lastEchange->getTypeAvance());
            $newEchange->setConditionsPaiement($lastEchange->getConditionsPaiement());
            $newEchange->setIdentifiantAccordCadre($lastEchange->getIdentifiantAccordCadre());
        }
        $newEchange->setDateCreation(date('Y-m-d H:i:s'));
        // @TODO  OK KBE $newEchange->setIdDecision(Atexo_Util::atexoHtmlEntities($_GET['idDE']));

        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
            $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);

            $newEchange->setUuidExterneExec($_GET['uuid']);
            $newEchange->setIdDecision($contratExec['id']);
        } else {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $newEchange->setIdDecision($idContrat);

            if ($this->_contrat instanceof CommonTContratTitulaire) {
                $newEchange->setDateNotification(Atexo_Util::frnDate2iso($this->_contrat->getDatePrevueNotification()));
                $newEchange->setDateFinMarche(Atexo_Util::frnDate2iso($this->_contrat->getDatePrevueFinContrat()));
                $newEchange->setDateFinMarcheReelle(Atexo_Util::frnDate2iso($this->_contrat->getDateFinContrat()));
                $newEchange->setDateNotificationReelle(Atexo_Util::frnDate2iso($this->_contrat->getDateNotification()));
            }

            if (!$this->isContratAcSad() && $this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande()) {
                if ($this->typeEnvoiPj->checked) {
                    $newEchange->setTypeEnvoi(Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_PJ'));
                } elseif ($this->typeEnvoiActeModif->checked) {
                    $newEchange->setTypeEnvoi(Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_ACTE_MODIF'));
                }
            }
        }

        $newEchange->setIdCreateur(Atexo_CurrentUser::getIdAgentConnected());
        $newEchange->setNomCreateur(Atexo_CurrentUser::getLastNameAgentConnected());
        $newEchange->setPrenomCreateur(Atexo_CurrentUser::getFirstNameAgentConnected());
        $newEchange->setStatutechange(Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON'));
        $newEchange->setRetourChorus(Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON'));

        //print_r($newEchange);exit;
        $newEchange->save($connexion);
        if ($lastEchange) {
            //Récupération de la fiche de navette
            $lastFicheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($lastEchange->getId(), $lastEchange->getOrganisme());
            if ($lastFicheNavette instanceof CommonChorusFicheNavette) {
                $newFicheNavette = $lastFicheNavette->copy();
                $newFicheNavette->setIdChorusEchange($newEchange->getId());
                $newFicheNavette->setOrganisme($newEchange->getOrganisme());
                $newFicheNavette->setIdDocument(null);
                $newFicheNavette->setNomDocument(null);
                $newFicheNavette->save($connexion);
            }
        }
        $this->labelJavascript->Text = '<script>showModalLoader();</script>';
    }

    public function onCallBackRefreshRepeater($sender, $param)
    {
        $this->displayEchanges();
        $this->panelListeEchanges->render($param->NewWriter);
    }

    /** Pour insérer un retour à la ligne (<br>), tous les 22 caractères, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine après y avoir insérer un retour à la lligne tous les $everyHowManyCaracter caractères
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    /**
     * afficher les echange deja crées.
     */
    public function displayEchanges()
    {
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $uuidContratExec = $_GET['uuid'];

            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);
            if ($contratExec['type']['codeExterne']) {
                $this->typeFlux = $this->isContratAcSadExec($contratExec['type']['codeExterne']) ? 'FEN211' : 'FEN111';
            }
            $listeEchanges = (new Atexo_Chorus_Echange())->retreiveEchangesByUiidDecisionContractExterne(
                $this->_organisme,
                $uuidContratExec
            );
        } else {
            // @TODO OK KBE $idContrat = Atexo_Util::atexoHtmlEntities($_GET['idDE']);
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $listeEchanges = (new Atexo_Chorus_Echange())->retreiveEchangesByIdDecision($this->_organisme, $idContrat);
        }

        if (is_array($listeEchanges)) {
            $datatriee = Atexo_Util::sortArrayOfObjects($listeEchanges, 'DateCreation', 'ASC');
            Atexo_CurrentUser::writeToSession('cachedEchanges', $datatriee);
            if (!is_array($datatriee)) {
                $datatriee = [];
            }
            Atexo_CurrentUser::writeToSession('cachedEchanges', $datatriee);
            $this->repeaterEchanges->DataSource = $datatriee;
            $this->repeaterEchanges->DataBind();
        }
    }

    public function deleteEchange($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexion->beginTransaction();
        $piecesExternes = (new Atexo_Chorus_Echange())->retreivePiecesExternes($param->CommandName, $this->_organisme);
        if ($piecesExternes) {
            foreach ($piecesExternes as $onePiece) {
                CommonChorusPjPeer::doDelete([$onePiece->getId(), $this->_organisme], $connexion);
            }
        }
        CommonChorusEchangePeer::doDelete([$param->CommandName, $this->_organisme], $connexion);
        $connexion->commit();
    }

    /**
     * Trier le tableau des formulaires.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->_critereTri = $this->getViewState('critereTriLibre');
        $this->_triAscDesc = $this->getViewState('triAscDescLibre');
        $cachedEchange = Atexo_CurrentUser::readFromSession('cachedEchanges');
        if ('NumOrdre' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEchange, 'NumOrdre');
        } elseif ('NomCreateur' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEchange, 'PrenomCreateur', '');
        } elseif ('Statut' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEchange, 'StatutEchange', '');
        } else {
            return;
        }

        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedEchanges');
        Atexo_CurrentUser::writeToSession('cachedEchanges', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTriLibre', $this->_critereTri);
        $this->setViewState('triAscDescLibre', $this->_triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->repeaterEchanges->DataSource = $dataSourceTriee[0];
        $this->repeaterEchanges->DataBind();

        // Re-affichage du TActivePanel
        $this->panelListeEchanges->render($param->getNewWriter());
    }

    /* Retourne un tableau de colonnes et un tableau d'objets tries
     * @param array $dataSource le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param integer $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->_triAscDesc, $typeCritere);
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /* Définit les critères et les types de tri
     * @param string $critere
     * @param integer $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->_critereTri != $critere || !$this->_critereTri) {
            $this->_triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->_triAscDesc) {
                $this->_triAscDesc = 'DESC';
            } else {
                $this->_triAscDesc = 'ASC';
            }
        }
        $this->_critereTri = $critere;
    }

    public function isPictoAddEchangeVisible()
    {
        if ($this->usingExecContrat()) {
            $uuidContratExec = $_GET['uuid'];
            $lastEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreatedWithUuidExec($this->_organisme, $uuidContratExec);
        } else {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $lastEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreated($this->_organisme, $idContrat);
        }

        //par défaut visible uniquement quand le dernier échange a un retour chorus
        //"intégré" et que le flux de suivi comprend le statut d'EJ commandé ou supprimé
        $bool = false;
        if ($lastEchange) {
            if ($lastEchange->getStatutEchange() == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER')
            && $lastEchange->getRetourChorus() == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')) {
                $bool = true;
            }
        } else {
            $bool = true;
        }

        return $bool;
    }

    public function displayStatutEJ($statut)
    {
        if (!$statut || '0' == $statut) {
            return Prado::localize('TEXT_SANS_OBJET');
        } elseif (strstr($statut, Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE'))) {
            return Prado::localize('TEXT_COMMANDE');
        } elseif (strstr($statut, Atexo_Config::getParameter('CHORUS_STATUT_EJ_CLOTURE'))) {
            return Prado::localize('TEXT_CLOTURE');
        } elseif (strstr($statut, Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME'))) {
            return Prado::localize('TEXT_SUPPRIME');
        }

        return '-';
    }

    public function displayRetourChorus($statut, $numOrdre)
    {
        if ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET')) {
            return Prado::localize('TEXT_SANS_OBJET');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')) {
            return Prado::localize('TEXT_INTEGRE');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')) {
            return Prado::localize('TEXT_REJETE');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE')) {
            return Prado::localize('TEXT_IRRECEVABLE');
        }
    }

    public function displayLinkErreur($retour, $erreur)
    {
        if ('' != $erreur && ($retour == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE') || $retour == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE'))) {
            return true;
        } else {
            return false;
        }
    }

    public function displayLinkErreurByStatut($statut)
    {
        if ($statut == Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR') || $statut == Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_TECHNIQUE')
            || $statut == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER')) {
            return true;
        } else {
            return false;
        }
    }

    public function displayLinks($statut, $retour)
    {
        return Atexo_Chorus_Echange::VerifierAccesChorusDetailsEchange($statut, $retour);
    }

    public function hideLinks($statut, $retour)
    {
        return !self::displayLinks($statut, $retour);
    }

    public function getLibelleGaById($idGa)
    {
        $serviceId = Atexo_CurrentUser::getCurrentServiceId();
        $groupementAchat = (new Atexo_Chorus_Echange())->retrieveGroupementAchatById($idGa, $this->_organisme, $serviceId);
        if ($groupementAchat) {
            return $groupementAchat->getLibelle();
        }
    }

    public function getLibelleOaById($idOa)
    {
        $organisationAchat = (new Atexo_Chorus_Echange())->retrieveOrganisationAchatById($idOa, $this->_organisme);
        if ($organisationAchat) {
            return $organisationAchat->getLibelle();
        }
    }

    public function getLibelleTypeMarcheById($idTypeMarche)
    {
        $typeMarche = (new Atexo_Chorus_Echange())->retreiveTypeMarche($idTypeMarche);
        if ($typeMarche) {
            return $typeMarche->getLibelle();
        }
    }

    /*
     * Permet d'afficher la date d'echange
     */
    public function getDateEchangeAAfficher($echange)
    {
        $return = '';
        if ($echange instanceof CommonChorusEchange) {
            switch ($echange->getstatutechange()) {
                case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON'):
                    $return = $this->insertBrWhenTextTooLong(Atexo_Util::iso2frnDateTime($echange->getDateCreation(), false), 10);
                    break;
                case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER'):
                    if ($echange->getTypeFluxAEnvoyer() != Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR')) {
                        $return = $this->insertBrWhenTextTooLong(Atexo_Util::iso2frnDateTime($echange->getDateEnvoi(), false), 10);
                    }
                    break;
                case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER'):
                    $return = $this->insertBrWhenTextTooLong(Atexo_Util::iso2frnDateTime($echange->getDateEnvoi(), false), 10);
                    break;
            }
        }

        return $return;
    }

    public function getFluxChorus()
    {
        $libelle = '';
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            return $this->typeFlux;
        } else {
            $idContrat = base64_decode($_GET['idContrat']);
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $contrat = $contratQuery->getTContratTitulaireById($idContrat);
            if ($contrat instanceof CommonTContratTitulaire) {
                if ((new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($idContrat)) {
                    $libelle = 'FEN211';
                } else {
                    $libelle = 'FEN111';
                }
            }
        }

        return $libelle;
    }

    public function afficherReferentielsModeIntegre()
    {
        $idContrat = base64_decode($_GET['idContrat']);
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat);
        if ($contrat instanceof CommonTContratTitulaire) {
            if ((new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($idContrat)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Precise si le contrat est accord cadre/SAD.
     *
     * @return bool : true si AC/SAD false sinon
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function isContratAcSad()
    {
        try {
            if ($this->usingExecContrat()) {
                return $this->isContratAcSadExec($this->_contratExec['type']['codeExterne']);
            } elseif ($this->_contrat instanceof CommonTContratTitulaire) {
                return (new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($this->_contrat->getIdContratTitulaire());
            } else {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error('Contrat introuvable');
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la determination du type de contrat : '.$e->getMessage());
        }
    }

    /**
     * Permet de gerer la visibilite de la fiche modificative.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiFicheModificative()
    {
        try {
            return $this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande();
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::isTypeEnvoiFicheModificative");
        }
    }

    /**
     * Permet de verifier les conditions d'affichage du bouton de telechargement des actes modificatifs du contrat
     * Verifier s'il y a au moins un acte modificatif pour l'EJ.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function isBoutonTelechargerTousActesVisible()
    {
        try {
            if (!($this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande())) {
                return false;
            }
            $hasActeModificatif = false;
            $echanges = $this->_contrat->getEchangesChorus();
            if (is_array($echanges) && !empty($echanges)) {
                foreach ($echanges as $echange) {
                    if ($echange instanceof CommonChorusEchange) {
                        $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($echange->getId());
                        if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                            if (!empty($ficheModificativeEchange->getIdBlobFicheModificative())) {
                                $hasActeModificatif = true;
                                break;
                            }
                        }
                    }
                }
            }

            return $hasActeModificatif;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::isBoutonTelechargerTousActesVisible");
        }
    }

    /**
     * Permet de gerer la visibilite de la fiche modificative pour un echange chorus.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isFicheModificativeEchangeVisible($idEchange)
    {
        try {
            if ($this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande()) {
                $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($idEchange);
                if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                    if (!empty($ficheModificativeEchange->getIdBlobFicheModificative())) {
                        return true;
                    }
                }
            }

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::isTypeEnvoiFicheModificative");
        }
    }

    /**
     * Calcule la valeur du colspan.
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function getColSpan()
    {
        try {
            if ($this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande()) {
                return 8;
            }

            return 7;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::getColSpan");
        }
    }

    /**
     * Permet de recuperer le type d'acte modificatif de l'echange chorus.
     *
     * @param $idEchange
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getTypeActeEchange($idEchange)
    {
        try {
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur recuperation type d'acte de l'echange CHORUS : id_echange = $idEchange \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::getTypeActeEchange");
        }
    }

    /**
     * Permet de recuperer le libelle du type d'acte de l'echange CHORUS.
     *
     * @param string $idTypeActe : identifiant technique du type d'acte
     * @param string $idEchange  : identifiant technique de l'echange chorus
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getLibelleTypeActe($idEchange)
    {
        try {
            $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($idEchange);
            if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                $referentielTypeActe = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_TYPE_MODIFICATION'));
                $listeTypesActes = [];
                if (is_array($referentielTypeActe) && count($referentielTypeActe)) {
                    foreach ($referentielTypeActe as $typeActe) {
                        $listeTypesActes[$typeActe->getId()] = $typeActe->getLibelleValeurReferentiel();
                    }
                }
                if (!empty($listeTypesActes[$ficheModificativeEchange->getTypeModification()])) {
                    return $listeTypesActes[$ficheModificativeEchange->getTypeModification()];
                }
            }

            return '-';
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur recuperation libelle du type d'acte de l'echange CHORUS : ID Echange = $idEchange \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::getLibelleTypeActe");
        }
    }

    /**
     * Permet de telecharger l'acte modificatif d'un echange chorus.
     *
     * @param $sender
     * @param $param
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function telechargerTypeActe($sender, $param)
    {
        $idEchange = $param->CommandParameter;
        try {
            $chorusEchangeQuery = new CommonChorusEchangeQuery();
            $echangeChorus = $chorusEchangeQuery->recupererEchangeChorus($idEchange, Atexo_CurrentUser::getCurrentOrganism());
            if ($echangeChorus instanceof CommonChorusEchange) {
                $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($idEchange);
                if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                    $atexoBlob = new Atexo_Blob();
                    $blob = $atexoBlob->return_blob_to_client($ficheModificativeEchange->getIdBlobFicheModificative(), $ficheModificativeEchange->getOrganisme(), true);
                    $blobName = $blob['name'];
                    $blobContent = $blob['content'];
                    if (!empty($blobContent)) {
                        DownloadFile::downloadFileContent($blobName, $blobContent);
                    }

                    return false;
                }
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur Telechargement Acte Modificatif de l'echange CHORUS : ID Echange = $idEchange \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::telechargerTypeActe");
        }
    }

    /**
     * Permet de telecharger un archive ZIP contenant l'ensemble des actes modificatifs et des pj libres.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function telechargerTousActesModificatifs($sender, $param)
    {
        try {
            if ($this->_contrat instanceof CommonTContratTitulaire) {
                $echanges = $this->_contrat->getEchangesChorus();
                if (is_array($echanges) && !empty($echanges)) {
                    $tmpZipFile = Atexo_Config::getParameter('COMMON_TMP').'pieces_jointes_liste_fiches_modificatives_'.session_id().time().'.zip';
                    foreach ($echanges as $echange) {
                        if ($echange instanceof CommonChorusEchange) {
                            $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($echange->getId());
                            if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                                if (!empty($ficheModificativeEchange->getIdBlobFicheModificative())) {
                                    //Creation de l'archive ZIP et l'ajout du repertoire de l'echange chorus
                                    $nomRepertoire = 'FEN111 - '.$echange->getNumOrdre();
                                    $tmpZipFile = (new Atexo_Zip())->addFolder2Zip($tmpZipFile, $nomRepertoire, 'CREATE');

                                    //Ajout de l'acte modificatif dans le repertoire de l'echange chorus
                                    $atexoBlob = new Atexo_Blob();
                                    $blob = $atexoBlob->return_blob_to_client($ficheModificativeEchange->getIdBlobFicheModificative(), $ficheModificativeEchange->getOrganisme(), true);
                                    $blobName = $blob['name'];
                                    $blobContent = $blob['content'];
                                    if (!empty($blobContent)) {
                                        $tmpActeModificatif = Atexo_Config::getParameter('COMMON_TMP').'fiches_modificatives_'.session_id().time();
                                        $fp = fopen($tmpActeModificatif, 'w');
                                        fwrite($fp, $blobContent);
                                        fclose($fp);
                                        (new Atexo_Zip())->addFile2Zip($tmpZipFile, $tmpActeModificatif, $nomRepertoire.'/'.$blobName);
                                        unlink($tmpActeModificatif);
                                    }

                                    //Ajout du repertoire des pjs libres dans le repertoire des echanges chorus
                                    $listePj = (array) $ficheModificativeEchange->getCommonTChorusFicheModificativePjs();
                                    if (is_array($listePj) && !empty($listePj)) {
                                        $repertoirePjs = $nomRepertoire.'/'.Prado::localize('DEFINE_PIECES_JOINTES');
                                        if (is_dir($repertoirePjs)) {
                                            rmdir($repertoirePjs);
                                        }
                                        mkdir($repertoirePjs);
                                        foreach ($listePj as $pj) {
                                            if ($pj instanceof CommonTChorusFicheModificativePj) {
                                                $blobContentPj = $atexoBlob->return_blob_to_client($pj->getFichier(), $ficheModificativeEchange->getOrganisme());
                                                if (!empty($blobContentPj)) {
                                                    $tmpPj = Atexo_Config::getParameter('COMMON_TMP').'pj_fiches_modificatives_'.session_id().time();
                                                    $fp = fopen($tmpPj, 'w');
                                                    fwrite($fp, $blobContentPj);
                                                    fclose($fp);
                                                    (new Atexo_Zip())->addFile2Zip($tmpZipFile, $tmpPj, $repertoirePjs.'/'.$pj->getNomFichier());
                                                    unlink($tmpPj);
                                                }
                                            }
                                        }
                                        $tmpZipFile = (new Atexo_Zip())->addFolder2Zip($tmpZipFile, $repertoirePjs);
                                    }
                                }
                            }
                        }
                    }
                    $nomZip = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($this->_contrat->getNumeroContrat());
                    if (empty($nomZip)) {
                        $nomZip = 'Actes modificatifs contrat '.$this->_contrat->getNumeroContrat();
                    }
                    $contentFileZip = file_get_contents($tmpZipFile);
                    unlink($tmpZipFile);
                    DownloadFile::downloadFileContent("$nomZip.zip", $contentFileZip);

                    return;
                }
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur Telechargement Tous Actes Modificatifs contrat : \n\nContrat = ".print_r($this->_contrat, true)." \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusEchanges::telechargerTousActesModificatifs");
        }
    }

    /**
     * Permet de gerer la visibilite du picto d'ajout d'un echange chorus avant la notification du contrat.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function isPictoAddEchangeAvantNotificationContratVisible()
    {
        if (
            $this->isContratAcSad()
            || (isset($_GET['uuid']))
            || (!$this->isContratAcSad() && $this->_contrat instanceof CommonTContratTitulaire && !$this->_contrat->isContratNotifieEtEjCommande())
        ) {
            return $this->isPictoAddEchangeVisible();
        }

        return false;
    }

    /**
     * Permet de gerer la visibilite du picto d'ajout d'un echange chorus apres la notification du contrat.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function isPictoAddEchangeApresNotificationContratVisible()
    {
        if (!$this->isContratAcSad() && $this->_contrat instanceof CommonTContratTitulaire && $this->_contrat->isContratNotifieEtEjCommande()) {
            return $this->isPictoAddEchangeVisible();
        }

        return false;
    }

    /**
     * Retrourne la remarque s'il s'agit d'un echange avec fiche modificative si non la remaque de la fiche navette.
     *
     * @param $echange
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function getRemarquesEchange($echange)
    {
        if (!$this->isContratAcSad() && ($echange instanceof CommonChorusEchange)) {
            $ficheModificativeEchange = (new Atexo_Chorus_Util())->recupererFicheModificative($echange->getId());
            if ($ficheModificativeEchange instanceof CommonTChorusFicheModificative) {
                return $ficheModificativeEchange->getRemarque();
            }
            $ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($echange->getId(), $echange->getOrganisme());
            if ($ficheNavette instanceof CommonChorusFicheNavette) {
                return $ficheNavette->getRemarques();
            }
        }

        return '';
    }

    public function isContratAcSadExec(?string $typeContrat): bool
    {
        $typeContratQuery = new CommonTTypeContratQuery();
        $typeContrat = $typeContratQuery->findOneByLibelleTypeContrat('TYPE_CONTRAT_' . $typeContrat);
        if ($typeContrat instanceof CommonTTypeContrat) {
            return Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER') == $typeContrat->getModeEchangeChorus();
        }

        return false;
    }

    public function isContratNotifieEtEjCommandeExec(?string $statutEj, string|null|int $statutContrat): bool
    {
        if(
            strstr($statutEj, Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE'))
            && $statutContrat == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
        ) {
            return true;
        }

        return false;
    }

    private function usingExecContrat()
    {
        return Atexo_Config::getParameter('ACTIVE_EXEC_V2')
            && is_array($this->_contratExec)
            && !empty($this->_contratExec);
    }
}
