<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LieuxExecution;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

class ParametragesLieuxExecution extends MpeTPage
{
    private $_agent;
    public $langue;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected(), true);
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$this->isPostBack) {
            if ($this->_agent) {
                if (Atexo_Module::isEnabled('LieuxExecution')) {
                    if ($this->_agent->getLieuExecution()) {
                        $this->displayGeoN2InModification($this->_agent->getLieuExecution());
                    } else {
                        $this->denominationGeo2T->Text = Prado::localize('AUCUNE_PREFERENCE_N_A_ETE_ENREGISTREE');
                    }
                }

                if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                    // Affichage code NUTS
                    if (!$this->_agent->getCodesNuts()) {
                        $this->labelCodesNuts->Text = Prado::localize('AUCUNE_PREFERENCE_N_A_ETE_ENREGISTREE');
                    }
                    $atexoCodesNutsRef = new Atexo_Ref();
                    $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                    $atexoCodesNutsRef->setSecondaires($this->_agent->getCodesNuts());
                    $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                }
            }
        }
    }

    /**
     * affiche les dénominations des lieux d'éxecutions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = (new Atexo_Config())->toPfEncoding($this->getSelectedGeoN2($ids));

        // Re-affichage du TActivePanel pour cause de problème d'accent
    }

    /**
     *afficher les lieu d'execution déjà enregistré pour l'agent connecté.
     */
    public function displayGeoN2InModification($lieuExecution)
    {
        $this->idsSelectedGeoN2->Value = $lieuExecution;
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = (new Atexo_Config())->toPfEncoding($this->getSelectedGeoN2($ids));
        $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
    }

    /**
     * Permet de charger les lieux d'exécution preferes au bon format pour la carte.
     */
    public function getNumSelectedGeoN2($buffer)
    {
        $text = '';
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];
        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }
        $text = implode('_', $new_arrayId);

        return $text;
    }

    /***
     * ajouter un ou des lieu d'execution pour l'agent connecté
     */
    public function addPlaceOfExecution()
    {
        if ($this->_agent) {
            $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $agent2Modify = CommonAgentPeer::retrieveByPK($this->_agent->getId(), $connexionOrg);
            if (Atexo_Module::isEnabled('LieuxExecution')) {
                $agent2Modify->setLieuExecution($this->idsSelectedGeoN2->Value);
            }
            if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                $agent2Modify->setCodesNuts($this->idAtexoRefNuts->codesRefSec->value);
            }
            $agent2Modify->save($connexionOrg);
            Atexo_Agent::retrieveAgent(Atexo_CurrentUser::getIdAgentConnected(), true);
            $this->response->redirect('agent');
        }
    }

    /***
     * Annuler et retour à la page d'accueil agent
     */
    public function goBack($sender, $param)
    {
        $this->response->redirect('agent');
    }

    public function getLieuExecutionUrl()
    {
        $param = '';
        if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
            $param = $this->numSelectedGeoN2->Value;
        }

        return Atexo_LieuxExecution::getLieuExecutionUrl($param);
    }

    /**
     * affiche les dénominations des lieux d'éxecutions.
     */
    public function refreshDisplaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = (new Atexo_Config())->toPfEncoding($this->getSelectedGeoN2($ids));
    }

    public function getSelectedGeoN2($buffer)
    {
        $lieuxExecutionsType1 = null;
        $text = '';
        $arIds = explode(',', $buffer);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        if ($list) {
            $getDenomination1 = 'getDenomination1'.Atexo_Languages::getLanguageAbbreviation($this->langue);
            foreach ($list as $oneGeoN2) {
                if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$oneGeoN2->$getDenomination1()) {
                    $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1().', ';
                } else {
                    $lieuxExecutionsType1 .= $oneGeoN2->$getDenomination1().', ';
                }
            }
            if ($lieuxExecutionsType1) {
                $this->denominationGeo2T->Text = utf8_encode(substr($lieuxExecutionsType1, 0, -2));
                if ('utf-8' == Atexo_Config::getParameter('HTTP_ENCODING')) {
                    $text = (substr($lieuxExecutionsType1, 0, -2));
                } else {
                    $text = Atexo_Util::toUtf8(substr($lieuxExecutionsType1, 0, -2));
                }
            }
        }

        return $text;
    }
}
