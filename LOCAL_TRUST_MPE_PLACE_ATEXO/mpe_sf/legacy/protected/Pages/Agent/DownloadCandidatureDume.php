<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use ZipArchive;

/*
 * permet de recuperer le fichier CandidatureDume d'une offre
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @version 1.0
 * @since 2017-esr
 * @copyright Atexo 2018
 * @package pages/agent
 */
class DownloadCandidatureDume extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Cette methode permet telecharger la candidature Dume lie a l'offre passee en param(url).
     *
     * @param void
     *
     * @return stream candidatureDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function onLoad($param)
    {
        $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        // Seul les agents ayant le droit de voir la consultation qui peuvent télécharger autres pieces
        $consultation = new Atexo_Consultation();
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($consultationId);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationFound = $consultation->search($consultationCriteria);

        if ($consultationFound[0] instanceof CommonConsultation) {
            $idOffre = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOffre']));
            $organisme = $consultationFound[0]->getOrganisme();
            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
            $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);

            if ($candidature instanceof CommonTCandidature) {
                $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise($candidature->getIdDumeContexte());

                if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                    $directory = Atexo_Config::getParameter('COMMON_TMP').'DUME'.session_name().session_id().time().md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
                    mkdir($directory);
                    $zip = new ZipArchive();
                    $zipFileName = 'Dume_'.$offre->getNumeroReponse().'.zip';
                    $memebreGroupements = (new Atexo_Groupement())->retrieveMemebreGroupementSansMondataire($offre->getId(), Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'));
                    $res = $zip->open($directory.$zipFileName, ZipArchive::CREATE);
                    if (true === $res) {
                        $atexoBlob = new Atexo_Blob();
                        $directoryMondataire = $offre->getNom();
                        $zip->addEmptyDir($directoryMondataire);
                        foreach ($tDumeNumeros as $dumeNumero) {
                            if ($dumeNumero instanceof CommonTDumeNumero) {
                                $blobresource = $atexoBlob->acquire_lock_on_blob($dumeNumero->getBlobId(), $organisme);
                                if(!empty($blobresource)) {
                                    $zip->addFile(
                                        $blobresource['blob']['pointer'],
                                        $directoryMondataire.'/'.
                                        $dumeNumero->getNumeroDumeNational().'.pdf'
                                    );
                                }

                                $blobresource = $atexoBlob->acquire_lock_on_blob($dumeNumero->getBlobIdXml(), $organisme);
                                if(!empty($blobresource)) {
                                    $zip->addFile(
                                        $blobresource['blob']['pointer'],
                                        $directoryMondataire.'/'.
                                        $dumeNumero->getNumeroDumeNational().'.xml'
                                    );
                                }
                            }
                        }

                        if (is_array($memebreGroupements) && count($memebreGroupements)) {
                            foreach ($memebreGroupements as $memebreGroupement) {
                                if ($memebreGroupement instanceof CommonTMembreGroupementEntreprise && $memebreGroupement->getNumerosn()) {
                                    $dumeNumeroMembreGroupement = (new Atexo_Dume_AtexoDume())->getDumeNumeroByNumeroSn($memebreGroupement->getNumerosn());
                                    if ($dumeNumeroMembreGroupement instanceof CommonTDumeNumero) {
                                        $directoryMembre = $memebreGroupement->getNomEntreprise();
                                        $zip->addEmptyDir($directoryMembre);
                                        $atexoBlob = new Atexo_Blob();
                                        if ($dumeNumeroMembreGroupement->getBlobId()) {
                                            $blobresource = $atexoBlob->acquire_lock_on_blob($dumeNumeroMembreGroupement->getBlobId(), $organisme);
                                            if(!empty($blobresource)) {
                                                $zip->addFile(
                                                    $blobresource['blob']['pointer'],
                                                    $directoryMembre.'/'.
                                                    $dumeNumeroMembreGroupement->getNumeroDumeNational().'.pdf'
                                                );
                                            }
                                        }
                                        if ($dumeNumeroMembreGroupement->getBlobIdXml()) {
                                            $blobresource = $atexoBlob->acquire_lock_on_blob($dumeNumeroMembreGroupement->getBlobIdXml(), $organisme);
                                            if(!empty($blobresource)) {
                                                $zip->addFile(
                                                    $blobresource['blob']['pointer'],
                                                    $directoryMembre.'/'.
                                                    $dumeNumeroMembreGroupement->getNumeroDumeNational().'.xml'
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $zip->close();
                        static::downloadFileContent($zipFileName, file_get_contents($directory.$zipFileName));
                    }
                }
            }
        }

        exit;
    }
}
