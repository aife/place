<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

class RapportStatistiqueConsultUser extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $uri = '/reports/ORME_STAT/Cons_utilis_ministere';
        } else {
            $uri = '/reports/ORME_STAT/Cons_service';
        }

        $this->criteresStatistiques->setUri($uri);
        $this->criteresStatistiques->setNomStat(Prado::localize('TEXT_CONSULTATIONS_UTILISATEURS'));
    }
}
