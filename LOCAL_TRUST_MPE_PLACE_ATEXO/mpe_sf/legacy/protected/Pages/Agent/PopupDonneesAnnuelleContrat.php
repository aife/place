<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcession;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarif;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * @author Florian MARTIN <florian.martin@atexo.com>
 * @copyright Atexo 2018
 *
 * @version 1.0
 *
 * @since
 */
class PopupDonneesAnnuelleContrat extends MpeTPage
{
    protected $contrat;

    public function onInit($param = null)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function getListeTarifs()
    {
        return $this->getViewState('listeTarifs');
    }

    public function setListeTarifs($listeTarifs)
    {
        return $this->setViewState('listeTarifs', $listeTarifs);
    }

    public function getNumOrdre()
    {
        return $this->getViewState('numOrdre');
    }

    public function setNumOrdre($numOrdre)
    {
        return $this->setViewState('numOrdre', $numOrdre);
    }

    public function onLoad($param = null)
    {
        $messageErreur = null;
        try {
            if (!$this->getIsCallback() && !$this->getIsPostBack()) {
                $messageErreur = null;
                $this->panelMessageErreur->setVisible(false);
                if ($_GET['idContrat']) {
                    $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $this->contrat = (new Atexo_Contrat())->retrieveContratOneByIdContratTitulaire($idContrat, $connexion);
                    if ($this->contrat instanceof CommonTContratTitulaire && $this->contrat->isTypeContratConcession()) {
                        $this->recapConsultation->initialiserComposant(null, null, $idContrat);
                        if (isset($_GET['idDonneesContrat'])) {
                            $idDonneesAnnuelles = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idDonneesContrat']));
                            $donneesAnnuellesContrat = (new Atexo_Contrat())->getOneDonneesAnnuellesConcessionById($idDonneesAnnuelles, $connexion);
                            if ($donneesAnnuellesContrat instanceof CommonDonneesAnnuellesConcession) {
                                $this->fillDataDonneesAnnuelles($donneesAnnuellesContrat);
                                $this->titrePopup->setText(Prado::Localize('VISUALISER_DONNEES_EXECUTION_CONTRAT'));
                                $this->hideComponent();
                            } else {
                                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                            }
                        } else {
                            $this->titrePopup->setText(Prado::Localize('AJOUTER_DONNEES_EXECUTION_CONTRAT'));
                            $this->showComponent();
                        }
                    } else {
                        $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                    }
                } else {
                    $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                }
            }
            if ($messageErreur) {
                $this->afficherMessageErreur($messageErreur);
            }
        } catch (Exception $exception) {
            Prado::log("Erreur lors de la création ou modification des données d'excécution du contrat : ".$exception->getMessage().$exception->getTraceAsString(), TLogger::ERROR, 'PopupDonneesAnnuelleContrat.php');
        }
    }

    public function afficherMessageErreur($messageErreur)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageErreur);
        $this->panelDonneesAnnuelles->setVisible(false);
        $this->panelBoutons->setVisible(false);
    }

    public function hideComponent()
    {
        $this->panelDonneesAnnuelles->setEnabled(false);
        $this->buttonSave->setVisible(false);
        $this->buttonSaveWithDE->setVisible(false);
        $this->ajouterTarifs->setVisible(false);
        $this->labelAjouterTarif->setVisible(false);
        $this->buttonAnnuler->setVisible(false);
        $this->buttonFermerr->setVisible(true);
    }

    public function showComponent()
    {
        $this->panelDonneesAnnuelles->setEnabled(true);
        $this->buttonSaveWithDE->setVisible(boolval(Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn')));
        $this->buttonSave->setVisible(boolval(!Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn')));
        $this->ajouterTarifs->setVisible(true);
        $this->labelAjouterTarif->setVisible(true);
        $this->buttonAnnuler->setVisible(true);
        $this->buttonFermerr->setVisible(false);
    }

    public function saveDonneesExecution($sender, $param)
    {
        $connexion = null;
        $listeTarifs = $this->getListeTarifs();
        if (is_array($listeTarifs) && count($listeTarifs)) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                $donneesAnnuellesContrat = new CommonDonneesAnnuellesConcession();
                $donneesAnnuellesContrat->setValeurDepense(Atexo_Util::getMontantArronditSansEspace($this->valeurDepense->getText()));
                $donneesAnnuellesContrat->setDateSaisie(date('Y-m-d H:i:s'));
                $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
                $donneesAnnuellesContrat->setIdContrat($idContrat);
                $lastNumOrdre = (new Atexo_Contrat())->getLastNumordreDonneesAnnuelles($idContrat);
                $lastNumOrdre = $lastNumOrdre ? ($lastNumOrdre + 1) : 1;
                $donneesAnnuellesContrat->setNumOrdre($lastNumOrdre);
                $donneesAnnuellesContrat->save($connexion);

                if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
                    $this->contrat = (new Atexo_Contrat())->retrieveContratOneByIdContratTitulaire($idContrat, $connexion);
                }

                foreach ($listeTarifs as $oneTarif) {
                    if ($oneTarif instanceof CommonDonneesAnnuellesConcessionTarif) {
                        $oneTarif->setCommonDonneesAnnuellesConcession($donneesAnnuellesContrat);
                        $oneTarif->save($connexion);
                    }
                }
                $connexion->commit();
                $this->labelScript->Text = "<script type='text/javascript'>window.opener.location.replace(window.opener.location.href);window.close();</script>";
            } catch (Exception $exception) {
                $connexion->rollBack();
                Prado::log("Erreur lors de la validation des données d'excécution du contrat : ".$exception->getMessage().$exception->getTraceAsString(), TLogger::ERROR, 'PopupDonneesAnnuelleContrat.php');
            }
        }
    }

    public function fillDataDonneesAnnuelles($donneesAnnuellesContrat)
    {
        if ($donneesAnnuellesContrat instanceof CommonDonneesAnnuellesConcession) {
            $this->valeurDepense->setText(Atexo_Util::getMontantArronditEspace($donneesAnnuellesContrat->getValeurDepense()));
            $listeTarifs = (array) $donneesAnnuellesContrat->getCommonDonneesAnnuellesConcessionTarifs();
            $this->setListeTarifs($listeTarifs);
            $lastId = (new Atexo_Contrat())->getLastIdTarif($donneesAnnuellesContrat->getId());
            if (!$lastId) {
                $numOrdre = 1;
                $this->hasTarif->setValue(0);
            } else {
                $this->hasTarif->setValue(1);
                $numOrdre = $lastId + 1;
            }
            $this->setNumOrdre($numOrdre);
            $this->refreshRepeaterTarifs();
        }
    }

    public function ajouterTarif()
    {
        if ($this->intitule->getText() && $this->montantTarif->getText()) {
            $listeTarifs = $this->getListeTarifs();
            $numOrdre = $this->getNumOrdre();
            if (!is_array($listeTarifs) || !count($listeTarifs)) {
                $listeTarifs = [];
                $numOrdre = 1;
                $this->hasTarif->setValue(1);
            }
            $newTarif = new CommonDonneesAnnuellesConcessionTarif();
            $newTarif->setIntituleTarif(html_entity_decode($this->intitule->getText()));
            $newTarif->setNumOrdre($numOrdre);
            $newTarif->setMontant(Atexo_Util::getMontantArronditSansEspace($this->montantTarif->getText()));
            $listeTarifs[$numOrdre++] = $newTarif;
            $this->setNumOrdre($numOrdre);
            $this->setListeTarifs($listeTarifs);
            $this->refreshRepeaterTarifs();
            $this->intitule->setText('');
            $this->montantTarif->setText('');
        }
    }

    public function refreshRepeaterTarifs()
    {
        $listeTarifs = $this->getListeTarifs();
        if (!is_array($listeTarifs) || count($listeTarifs)) {
            $this->hasTarif->setValue(0);
        }
        $this->listeTarifs->DataSource = $listeTarifs;
        $this->listeTarifs->DataBind();
    }

    public function supprimerTarif($sender, $param)
    {
        $listeTarifs = $this->getListeTarifs();
        $key = $param->CallBackParameter;
        unset($listeTarifs[$key]);
        $this->setListeTarifs($listeTarifs);
        $this->refreshRepeaterTarifs();
    }

    /**
     * permet d'afficher la popup de confirmation du save.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-roadmap
     */
    public function aprouveSaveDonneesExecution($sender, $param)
    {
        $this->labelScript->Text = "<script>openModal('demander-validation','popup-small2',document.getElementById('panelValidationNt'), 'Demande de validation');</script>";
    }
}
