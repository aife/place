<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/**
 * commentaires Admministration des  Journaux d'Annonces Legales.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionJal extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (Atexo_Module::isEnabled('GestionJalMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            if (isset($_GET['entitePublic'])) {
                $this->panelEntiteAchat->setVisible('false');
                $this->setViewState('idService', null);
            } else {
                $this->panelEntiteAchat->setVisible('true');
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
            }
            $this->lienAjouterJal->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpAjoutJal&idService=".$this->getViewState('idService')."','yes');";
            $jals = Atexo_Jal::search($this->getViewState('idService'));
            $this->RepeaterJal->DataSource = $jals;
            $this->RepeaterJal->DataBind();
        }
    }

    public function getSelectedEntityPurchase()
    {
        return $this->_selectedEntity;
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouterJal->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpAjoutJal&idService=".$this->_selectedEntity."','yes');";
        $jals = Atexo_Jal::search($this->getViewState('idService'));
        $this->RepeaterJal->DataSource = $jals;
        $this->RepeaterJal->DataBind();
        // Rafraichissement du ActivePanel
        $this->ApRepeaterJal->render($param->NewWriter);
    }

    public function onDeleteClick($sender, $param)
    {
        $idJal = $param->CommandParameter;
        Atexo_Jal::delete($idJal);
        $Jals = Atexo_Jal::search($this->getViewState('idService'));
        $this->RepeaterJal->DataSource = $Jals;
        $this->RepeaterJal->DataBind();
    }

    public function refreshRepeaterJal($sender, $param)
    {
        // Rafraichissement du ActivePanel
        $jals = Atexo_Jal::search($this->getViewState('idService'));
        $this->RepeaterJal->DataSource = $jals;
        $this->RepeaterJal->DataBind();
        $this->ApRepeaterJal->render($param->NewWriter);
    }
}
