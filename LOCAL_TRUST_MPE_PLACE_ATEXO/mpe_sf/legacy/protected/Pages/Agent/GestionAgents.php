<?php

namespace Application\Pages\Agent;

use App\Service\AgentService;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierQuery;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonServiceMertierProfils;
use Application\Propel\Mpe\CommonServiceMertierProfilsQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_AssociationComptes;
use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionAgents extends MpeTPage
{
    private $_selectedEntity;
    public $critereTri;
    public $triAscDesc;
    public $organisme;
    public $nbrElementRepeater;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->SearchAgent->fillData();
        $this->panelConfirmation->setDisplay('None');
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && isset($_GET['all']) && 1 == $_GET['all']) {
            // Gestion de tous les agents
            $this->hideComponent();
        } else {
            // Gestion des agents d'un organisme
            if (!$this->isPostBack) {
                $this->repeaterDataBind();
            }
        }
        $this->scriptJs->Text = '';
        if (!$this->isPostBack) {
            $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
            $serviceAgent = Atexo_CurrentUser::getCurrentServiceId();
            $this->lienAjouterAgent->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=".$serviceAgent."','yes');";
        } elseif (!$this->isCallBack) {
            $this->repeaterDataBind();
        }
        if (!($this->getUserRoleAdminCompte() && $this->manageLinks())) {
            $this->lienAjouterAgent->visible = false;
        }
        if (Atexo_CurrentUser::hasHabilitation('DroitGestionServices')) {
            $this->panelExcelAllAgents->setVisible(true);
        }
    }

    /**
     * to change.
     */
    public function getServiceMetier($idAgent)
    {
        $listeService = (new Atexo_Socle_AgentServicesMetiers())->retreiveListeServiceAgent($idAgent);
        $sigleServices = '';
        if (is_array($listeService)) {
            foreach ($listeService as $oneService) {
                $sigleServices .= $oneService->getSigle();
                $sigleServices .= ' / ';
            }
        }

        return substr(trim($sigleServices, ' '), 0, -1);
    }

    public function repeaterDataBind()
    {
        $this->SearchAgent->fillCriteriaVoAgent();
        $criteriaVoAgent = $this->getViewState('CriteriaVoAgent');
        if ($criteriaVoAgent instanceof Atexo_Agent_CriteriaVo) {
            $nombreElement = (new Atexo_Agent())->search($criteriaVoAgent, true);
            $this->nbrElementRepeater = $nombreElement;
            $this->setViewState('nbrElements', $nombreElement);
            $this->setViewState('listeAgents', (new Atexo_Agent())->search($criteriaVoAgent, false));
            if ($nombreElement && !is_array($nombreElement)) {
                $element = (float) $nombreElement / $this->RepeaterAgent->PageSize;
                $this->nombrePageTop->Text = ceil($element);
                $this->nombrePageBottom->Text = ceil($element);
                $this->RepeaterAgent->setVirtualItemCount($nombreElement);
                $this->RepeaterAgent->setCurrentPageIndex(0);
                self::showComponent();
                self::populateData();
            } else {
                $this->setViewState('listeAgents', []);
                $this->RepeaterAgent->DataSource = [];
                $this->RepeaterAgent->DataBind();
                self::hideComponent();
            }
        }
    }

    public function populateData()
    {
        $offset = $this->RepeaterAgent->CurrentPageIndex * $this->RepeaterAgent->PageSize;
        $limit = $this->RepeaterAgent->PageSize;
        if ($offset + $limit > $this->RepeaterAgent->getVirtualItemCount()) {
            $limit = $this->RepeaterAgent->getVirtualItemCount() - $offset;
        }
        $criteriaVoAgent = $this->getViewState('CriteriaVoAgent');
        $criteriaVoAgent->setLimit($limit);
        $criteriaVoAgent->setOffset($offset);
        if ($this->getViewState('listeAgentsTriee')) {
            $dataAllAgent = $this->getViewState('listeAgentsTriee');
            $dataAgent = [];
            $j = 0;
            for ($i = $offset; $i < (is_countable($dataAllAgent) ? count($dataAllAgent) : 0) && $i < ($limit + $offset); ++$i) {
                $dataAgent[$j++] = $dataAllAgent[$i];
            }
        } else {
            $dataAgent = (new Atexo_Agent())->search($criteriaVoAgent, false);
        }
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        if (count($dataAgent)) {
            $this->showComponent();
            $this->RepeaterAgent->DataSource = $dataAgent;
            $this->RepeaterAgent->DataBind();
        } else {
            $this->hideComponent();
        }
    }

    public function getUserRoleAgentPole()
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionHabilitations')) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserRoleAdminCompte()
    {
        if ((Atexo_CurrentUser::hasHabilitation('GestionAgents') || Atexo_CurrentUser::hasHabilitation('GestionAgentsSocle'))
            && $this->manageLinks()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retourne les données de l'agent.
     *
     * @return array objets agents
     */
    public function getDataAgents($organisme = false, $forNombreResultats = false, $limit = 0, $offset = 0)
    {
        if (!$organisme) {
            $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        } else {
            $this->_selectedEntity = $this->ChoixOrganisme->getSelectedEntityPurchase();
        }

        $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($this->_selectedEntity, $organisme);

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonAgentPeer::SERVICE_ID, $this->_selectedEntity, Criteria::EQUAL);
        $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        if (!$forNombreResultats) {
            $agents = CommonAgentPeer::doSelect($c, $connexionCom);
            if ($agents) {
                foreach ($agents as $agent) {
                    $agent->setEntityPath($entityPath);
                }

                return $agents;
            } else {
                return [];
            }
        } else {
            return CommonAgentPeer::doCount($c, true, $connexionCom);
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->intialiserRepeater();

        //       $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        //       $this->setViewState('idService', $this->_selectedEntity);
        //       $this->lienAjouterAgent->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=". $this->_selectedEntity. "','yes');";
        self::repeaterDataBind();
        //       $this->RepeaterAgent->DataSource = $this->getViewState("listeAgents");
        //       $this->RepeaterAgent->DataBind();
        // Rafraichissement du ActivePanel
        $this->panelConfirmation->render($param->NewWriter);
        $this->panelRefresh->render($param->NewWriter);
    }

    public function onClickGlobalSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixOrganisme->getSelectedEntityPurchase();
        $this->organisme = $this->ChoixOrganisme->organisme->getSelectedValue();
        $this->setViewState('idService', $this->_selectedEntity);
        if ($this->organisme) {
            $url = 'index.php?page=Agent.PopUpAjoutAgent&idService='.$this->_selectedEntity.'&organisme='.$this->organisme;
        } else {
            $url = 'index.php?page=Agent.PopUpAjoutAgent&idService='.$this->_selectedEntity;
        }
        $this->lienAjouterAgent->NavigateUrl = "javascript:popUp('$url','yes');";
        $this->setViewState('listeAgents', $this->getDataAgents($this->organisme));
        if (count($this->getDataAgents($this->organisme))) {
            self::showComponent();
        } else {
            self::hideComponent();
        }
        self::repeaterDataBind();
        $this->RepeaterAgent->DataSource = $this->getDataAgents($this->organisme);
        $this->RepeaterAgent->DataBind();
        // Rafraichissement du ActivePanel
        $this->panelRefresh->render($param->NewWriter);
    }

    public function refreshRepeater($sender, $param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->repeaterDataBind();
        } else {
            $nbreElementOld = $this->getViewState('nbrElements');

            $nbreElement = $this->SearchAgent->getDataAgents(true);

            $repeaterSize = $this->RepeaterAgent->PageSize;

            if (0 == $nbreElement) {
                self::hideComponent();
                $this->RepeaterAgent->DataSource = [];
                $this->RepeaterAgent->DataBind();
            } elseif ($nbreElementOld < $nbreElement && ($nbreElementOld % $repeaterSize) == 0) {
                self::repeaterDataBind();
                $page = ceil($nbreElement / $repeaterSize);
                self::toPage($page);
            } elseif ($nbreElementOld > $nbreElement && ($nbreElement % $repeaterSize) == 0) {
                $page = ceil($this->pageNumberTop->text) - 1;
                self::toPage($page);
            } else {
                $page = ceil($this->pageNumberTop->text);
                self::repeaterDataBind();
                self::toPage($page);
            }
        }
        $this->panelRefresh->render($param->getNewWriter());
        $this->panelConfirmation->render($param->getNewWriter());
    }

    /**
     * Trier un tableau des invités sélectionnés.
     *
     * @param sender
     * @param param
     */
    public function sortAgents($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $agents = $this->getViewState('listeAgents');
        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'Nom', '');
        } elseif ('Prenom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'Nom', '');
        } elseif ('ServiceId' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'ServiceId', '');
        } elseif ('DateConnexion' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'DateConnexion', '');
        } else {
            return;
        }
        $this->setViewState('listeAgentsTriee', $dataSourceTriee[0]);
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);
        //Remplissage du tabeau des invités
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->RepeaterAgent->getCurrentPageIndex() + 1);

        $this->setViewState('listeAgents', $dataSourceTriee[0]);

        self::populateData();
        //      $this->RepeaterAgent->DataSource = $dataSourceTriee[0];
        //      $this->RepeaterAgent->DataBind();

        // Re-affichage du TActivePanel
        $this->panelRefresh->render($param->getNewWriter());
        self::showComponent();
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function supprimerAgent($sender, $param)
    {
        $id = $param->CommandParameter;
        $agentConnectedO = new CommonAgent();
        $agentConnectedO->setId(Atexo_CurrentUser::getIdAgentConnected());
        $agentConnectedO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $agentConnectedO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $agentToUpO = (new CommonAgentQuery())->findOneById($id);
        if (!(new Atexo_Agent())->verifyCapacityToModify($agentConnectedO, $agentToUpO) && !Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->panelConfirmation->setDisplay('Dynamic');
            $this->confirmationDuplication->setTypeMessage('msg-erreur');
            $this->confirmationDuplication->setMessage(Prado::localize('TEXT_PAS_AUTORISE_SUPPRIMER_INFORMATIONS_AGENT'));
        } else {
            (new Atexo_Agent_AssociationComptes())->deleteCompteAssocie($id);
            (new Atexo_Agent())->deleteAgent($id, Atexo_CurrentUser::getIdAgentConnected());
        }
        self::repeaterDataBind();
    }

    public function genererExcel()
    {
        $criteriaAgent = $this->getViewState('CriteriaVoAgent');
        $organisme = $criteriaAgent->getOrganisme();
        $serviceId = $criteriaAgent->getService();
        $inclureDesc = $criteriaAgent->getInclureDesc();

        //Recuperation de la liste des agents pour le service (id=$serviceId)
        $this->SearchAgent->fillCriteriaVoAgent();
        $criteriaVoAgent = $this->getViewState('CriteriaVoAgent');
        $agents = (new Atexo_Agent())->search($criteriaVoAgent);

        (new Atexo_GenerationExcel())->generateExcelAgents($agents, $organisme, $serviceId, false);
    }

    /***
     * gérerer un excel qui contient tout les agents de l'organisme en cours
     */
    public function genererExcelAllAgent()
    {
        $organisme = $this->SearchAgent->getOrganisme();
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if (Atexo_Module::isEnabled('SocleInterne') && isset($_GET['socle']) && 1 == $_GET['socle']) {
            (new Atexo_GenerationExcel())->generateExcelAgentsSocle($organisme, 0, false);
        } else {
            //La liste de tous les agents de l'organisme en cours
            $agents = Atexo_Agent::retriveAgentsByOrganisme($organisme, true, false, true);
            (new Atexo_GenerationExcel())->generateExcelAgents($agents, $organisme, 0, false);
        }
    }

    /***
     * la gestion des icones entre le socle et mpe
     */
    public function manageLinks()
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            return false;
        } else {
            if (Atexo_Module::isEnabled('SocleInterne') && 'socle' == Atexo_CurrentUser::readFromSession('ServiceMetier')) {
                return true;
            } elseif (Atexo_Module::isEnabled('SocleInterne') && 'mpe' == Atexo_CurrentUser::readFromSession('ServiceMetier')) {
                return false;
            }
        }

        if (!Atexo_Module::isEnabled('SocleInterne') && !Atexo_Module::isEnabled('SocleExternePpp')) {
            return true;
        }
    }

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterAgent->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);
        $this->populateData();
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $this->populateData();
        $this->panelRefresh->render($param->NewWriter);
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }

        self::toPage($numPage);
    }

    public function showComponent()
    {
        $this->Page->PagerTop->visible = true;
        $this->Page->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    /*
    * Permet de dupliquer un agent
    */
    public function dupliquerAgent($sender, $param)
    {
        /** @var AgentService $agentService */
        $agentService = Atexo_Util::getSfService(AgentService::class);
        $idAgentADupliquer = $param->CommandParameter;
        $dumplicateResult = $agentService->duplicate($idAgentADupliquer);
        $newAgent = $dumplicateResult['agent'];
        $password = $dumplicateResult['password'];
        $message = Prado::localize('TEXT_CONFIRMATION_DUPLICATION_AGENT').' : <br/>';
        $message .= Prado::localize('LOGIN').' : '.$newAgent->getLogin() ." <br/>";
        $message .= Prado::localize('EMAIL').' : '.$newAgent->getEmail()." <br/>";
        $message .= Prado::localize('TEXT_PASSWORD')." : $password";

        $this->confirmationDuplication->setMessage($message);
        $this->panelConfirmation->setDisplay('Dynamic');
        self::repeaterDataBind();
    }

    public function toPage($numPage)
    {
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nbResultsTop->getSelectedValue(), $numPage);
            $this->populateData();
        } else {
            $this->pageNumberTop->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
        }
    }

    public function getUrlAjoutAgent($organisme, $service)
    {
        if ($organisme) {
            $url = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=".$service.'&organisme='.$organisme."','yes');";
        } else {
            $url = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=".$service."','yes');";
        }
        $this->lienAjouterAgent->NavigateUrl = $url;
    }

    public function getEncodageCorrecte($value)
    {
        if ($this->IsCallback) {
            $value = Atexo_Util::toUtf8($value);
        }

        return $value;
    }

    /*
     * Permet d'intialiser les valeurs de pagination et le repeater
     */
    public function intialiserRepeater()
    {
        $this->RepeaterAgent->DataSource = [];
        $this->RepeaterAgent->DataBind();
        $this->nbResultsTop->setSelectedValue(10);
        $this->nbResultsBottom->setSelectedValue(10);
        $this->pageNumberTop->Text = 1;
        $this->pageNumberBottom->Text = 1;
        $this->RepeaterAgent->setCurrentPageIndex(0);
    }

    /*
     * Permet de mettre à jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nbrElements');
        $this->RepeaterAgent->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->RepeaterAgent->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
    }

    public function ajoutCompteSecondaire($sender, $param)
    {
        if ($param->CallbackParameter) {
            $idAgent = $param->CallbackParameter;
            $this->informationsComptePrincipale->getInformationsAgentPrincipale($idAgent);
            $this->informationsComptePrincipale->displayOrganismes();
            $this->informationsComptePrincipale->showBlocCompteExistant();
            $this->scriptJs->Text = <<<SCRIPT
			<script> document.getElementById('{$this->informationsComptePrincipale->error->getClientId()}').style.display='none';
					 document.getElementById('{$this->informationsComptePrincipale->compteAssocie->getClientId()}').value = '' ;
					 openModal('compte-association','popup-800',document.getElementById('{$this->scriptJs->getClientId()}'));</script>"
SCRIPT;
        }
    }

    /**
     * permet de tronquer le texte.
     *
     * @param string $texte
     * @param int    $maxLenghtString
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since   4.11
     *
     * @copyright Atexo 2015
     */
    public function truncate($texte, $maxLenghtString = null)
    {
        if (!$maxLenghtString) {
            $maxLenghtString = 230;
        }
        $truncatedText = '';
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maxLenghtString) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maxLenghtString) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    /**
     * gere l'affichage du div infos bulle des information.
     *
     * @param string $texte
     * @param int    $nbrCaractere
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since   4.11
     *
     * @copyright Atexo 2015
     */
    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > $nbrCaractere) ? '' : 'display:none';
    }

    /**
     * charge les informations de l'etablissement dans le formulaire.
     *
     * @param int $idAgent
     *
     * @return array tableau des service accessible avec profil et date modification
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since   4.11
     *
     * @copyright Atexo 2015
     */
    public function getDataSourceServiceAccessible($idAgent)
    {
        $listeService = (new Atexo_Socle_AgentServicesMetiers())->retreiveListeServiceAgent($idAgent);
        $infosAccessibiliteServices = [];
        $dateModification = '';
        $profil = '';
        if (is_array($listeService)) {
            foreach ($listeService as $oneService) {
                $idServiceMetier = $oneService->getId();
                $serviceMetierAgentQuery = new CommonAgentServiceMetierQuery();
                $sreviceMetierAgentObject = $serviceMetierAgentQuery->filterByIdAgent($idAgent)->filterByIdServiceMetier($idServiceMetier)->findOne();
                if ($sreviceMetierAgentObject instanceof CommonAgentServiceMetier) {
                    $idProfil = $sreviceMetierAgentObject->getIdProfilService();
                    $dateModification = $sreviceMetierAgentObject->getDateModification();
                }
                $serviceMetierProfilQuery = new CommonServiceMertierProfilsQuery();
                $serviceMetierProfilObject = $serviceMetierProfilQuery->filterByIdInterne($idProfil)->filterByIdServiceMetier($idServiceMetier)->findOne();
                if ($serviceMetierProfilObject instanceof CommonServiceMertierProfils) {
                    $profil = $serviceMetierProfilObject->getLibelle();
                }

                $infosAccessibiliteServices[$idServiceMetier] .= '<strong>'.$oneService->getSigle().'</strong> : ';
                $infosAccessibiliteServices[$idServiceMetier] .= $profil;
                $infosAccessibiliteServices[$idServiceMetier] .= ' ('.Atexo_Util::iso2frnDateTime($dateModification, false).') ';
                $infosAccessibiliteServices[$idServiceMetier] .= ' <br>';
            }
        }

        return $infosAccessibiliteServices;
    }
}
