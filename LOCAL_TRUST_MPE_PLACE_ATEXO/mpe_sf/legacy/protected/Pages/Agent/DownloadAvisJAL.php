<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;

/**
 * Permet de télécharger le AVIS.
 *
 * @author mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadAvisJAL extends DownloadFile
{
    private $_idAvis;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_idAvis = Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
        $avis = (new Atexo_Publicite_Avis())->retreiveAvisById($this->_idAvis, $connexionCom);
        if ($avis) {
            $this->_idFichier = $avis->getAvis();
            $this->_nomFichier = $avis->getNomFichier();
            if (isset($_GET['orgAcronyme'])) {
                $acronyme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            } else {
                $acronyme = Atexo_CurrentUser::getOrganismAcronym();
            }
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $acronyme);
        }
    }
}
