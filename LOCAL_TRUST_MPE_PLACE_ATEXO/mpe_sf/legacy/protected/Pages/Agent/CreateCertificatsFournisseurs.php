<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCertificatsEntreprises;
use Application\Propel\Mpe\CommonCertificatsEntreprisesPeer;
use Application\Propel\Mpe\CommonInscrit;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CreateCertificatsFournisseurs extends MpeTPage
{
    public string $_company = '';
    public string $_inscrit = '';
    public $_connexionCom;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->vbScript->Text = '';

        if (isset($_GET['idInscrit']) && '' != $_GET['idInscrit']) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['idInscrit']));
            $this->nomInscrit->Text = $this->_inscrit->getNom();
            $this->prenomInscrit->Text = $this->_inscrit->getPrenom();
            $this->emailInscrit->Text = $this->_inscrit->getEmail();
            if ($this->_inscrit instanceof CommonInscrit) {
                $this->_company = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->_inscrit->getEntrepriseId());
                $this->nomEntreprise->Text = $this->_company->getNom();
                if ($this->_company->getSiren()) {
                    $this->rc->Text = $this->_company->getAcronymePays().' - '.$this->_company->getSiren();
                    $this->intituleIdEntreprise->Text = Prado::localize('REGISTRE_DU_COMMERCE');
                } else {
                    $this->rc->Text = $this->_company->getAcronymePays().Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->_company->getSirenetranger();
                    $this->intituleIdEntreprise->Text = Prado::localize('TEXT_IDENTIFIANT_NATIONNAL');
                }
                if (!$this->IsPostBack) {
                    $certificat = (new Atexo_Entreprise_Inscrit())->retrieveCertificatInscritById($this->_inscrit->getId());
                    if ($certificat instanceof CommonCertificatsEntreprises) {
                        CommonCertificatsEntreprisesPeer::doDelete($certificat->getId(), $this->_connexionCom);
                    }
                }
            }
        }
        if ('OK' == $_POST['install_result']) {
            $idCompany = $this->getIdCompany();
            $this->response->redirect('index.php?page=Agent.ResultCertificatsFournisseurs&idCompany='.$idCompany);
        }
        if (!$this->IsPostBack) {
            $this->vbScript->Text = '<script language="VBSCRIPT">postLoad</script>';
        }
    }

    public function generateCert()
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (Atexo_Util::isCurrentClientWindows()) {
            if (!$_POST['pkcs10_req']) {
                self::writeErrorMessage(Prado::localize('REQUETE_CERT_INTROUVABLE'));

                return;
            }

            $arrayCert = (new Atexo_Crypto_Certificat())->generateCertificat($_POST['pkcs10_req'], $org);
            if (is_array($arrayCert)) {
                if ($arrayCert['pemCert']) {
                    $newCertificat = new CommonCertificatsEntreprises();
                    $newCertificat->setIdInscrit($this->_inscrit->getId());
                    $newCertificat->setNomInscrit($this->_inscrit->getNom());
                    $newCertificat->setPrenomInscrit($this->_inscrit->getPrenom());
                    $newCertificat->setMailInscrit($this->_inscrit->getEmail());
                    $newCertificat->setIdEntreprise($this->_inscrit->getEntrepriseId());
                    $newCertificat->setDateDebut(date('Y-m-d H:i:s'));
                    $newCertificat->setDateFin((new Atexo_Crypto_Certificat())->getDateExpiration($arrayCert['pemCert']));
                    $newCertificat->setStatutRevoque((Atexo_Config::getParameter('STATUT_CERTIFICAT_NON_REVOQUEE')));
                    $newCertificat->setCertificat($arrayCert['pemCert']);
                    $newCertificat->save($this->_connexionCom);
                }

                $this->certificate->Value = $arrayCert['pemCert'];
                $this->vbScript->Text = '<script language="VBSCRIPT">
												  		postLoad
												  		CertAcceptSub2
														</script>';
            } else {
                self::writeErrorMessage($arrayCert);

                return;
            }
        }
    }

    public function writeErrorMessage($errorMessage)
    {
        $this->javaScript->Text = "<script>document.getElementById('divValidationSummary').style.display='';document.getElementById('sslError').style.display='';document.getElementById('erreurSaisie').style.display='none';</script>";
        $this->error->Text = $errorMessage;
        $this->vbScript->Text = '<script language="VBSCRIPT">postLoad</script>';
    }

    public function getIdCompany()
    {
        $idCompany = '';
        if ($this->_company->getSiren()) {
            $idCompany = $this->_company->getSiren();
        } else {
            $idCompany = $this->_company->getSirenetranger().'&pays='.$this->_company->getPaysadresse();
        }

        return $idCompany;
    }
}
