<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DecisionAnnulation extends MpeTPage
{
    public $_lotOrConsultation;
    public $_consultation;
    public $_categories;
    public $_trancheBudgetaire;
    public bool $_creation = true;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('AnnulerConsultation')) {
            if (Atexo_CurrentUser::hasHabilitation('AnnulerConsultation')) {
                Atexo_CurrentUser::deleteFromSession('idsOrg');
                $this->panelMessageErreur->setVisible(false);
                $messageErreur = '';
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);
                if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                    $this->consultationSummary->setConsultation($consultation);
                    $this->_consultation = $consultation;
                    if ($this->_consultation->getIdFichierAnnulation()) {
                        $this->_creation = false;
                    } else {
                        $this->_creation = true;
                    }
                } else {
                    $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            } else {
                $messageErreur = Prado::localize('ERREUR_HABILITATION_DECISION');
            }

            if ($messageErreur) {
                $this->panelMessageErreur->setVisible(true);
                $this->consultationSummary->setWithException(false);
                $this->panelMessageErreur->setMessage($messageErreur);
            }
        } else {
            exit;
        }
    }

    /*
     * sauvegarder la decision d annulation de la consultation
     */
    public function saveDecision($sender, $param)
    {
        $connexionCOm = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCOm->beginTransaction();
        try {
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $this->panelMessageErreur->setMessage($msg.'"'.$this->ajoutFichier->FileName.'"');
                $this->panelMessageErreur->setVisible(true);
            }
            $avisIdBlob = $this->annulerConsultation($connexionCOm);
            //Envoie du mail
            $echange = new CommonEchange();
            $echange->setIdCreateur(Atexo_CurrentUser::getId());
            $echange->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            if ($this->ajoutFichier->HasFile) {
                if ($avisIdBlob) {
                    $echangePj = new CommonEchangePieceJointe();
                    $echangePj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $echangePj->setPiece($avisIdBlob);
                    $echangePj->setTaille((new Atexo_Blob())->getTailFile($avisIdBlob, Atexo_CurrentUser::getCurrentOrganism()));
                    $echangePj->setNomFichier($this->ajoutFichier->FileName);
                    $echange->addCommonEchangePieceJointe($echangePj);
                }
            }

            $idEchange = '';
            if ($echange->save($connexionCOm)) {
                $idEchange = $echange->getId();
            }
            $connexionCOm->commit();
            $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueAnnulationConsultation&id='.$this->_consultation->getId()."&IdMsg=$idEchange");
        } catch (Exception $e) {
            $connexionCOm->rollBack();
            throw $e;
        }
    }

    public function annulerConsultation($connexionCom)
    {
        $consultation = $this->_consultation;
        $consultation->setConsultationAnnulee('1');
        $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
        $consultation->setDateDecision(date('Y-m-d'));
        $consultation->setDateDecisionAnnulation(Atexo_Util::frnDate2iso($this->dateDecision->Text));
        $consultation->setCommentaireAnnulation($this->commentaire->Text);
        $avisIdBlob = (new Atexo_Consultation())->ajouterPj($consultation->getId(), $consultation->getOrganisme());
        $consultation->setIdFichierAnnulation($avisIdBlob);
        $consultation->save($connexionCom);

        return $avisIdBlob;
    }

    /*
     * retourne le lien de redirection
     */
    public function getLienretour()
    {
        if ($this->_consultation->getConsultationAnnulee()) {
            return 'index.php?page=Agent.ouvertureEtAnalyse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&decision';
        } else {
            return 'index.php?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']);
        }
    }
}
