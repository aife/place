<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVISPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupAddUrl extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
    }

    /**
     *ajouter d'url dans la table Avis.
     */
    public function addUrl($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $avis = CommonAVISPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idAvis']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        $avis->setUrl(Atexo_Util::atexoHtmlEntities($this->url->Text));
        $avis->setStatut(Atexo_Config::getParameter('STATUT_EN_ATTENTE'));
        $avis->save($connexion);
        $this->labelClose->Text = '<script>refreshRepeaterFormLibre();window.close();</script>';
        // }
    }
}
