<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Exception;
use Prado\Prado;

/**
 * Page acces au formulaire de la publicite.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-develop
 *
 * @copyright Atexo 2016
 */
class PopupFormulaireModificationPublicite extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->panelMessageAvertissement->setMessage(Prado::localize('MESSAGES_MODIFICATION_ANNONCE_SUB'));
        if (!$this->getIsPostBack()) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($ref);
                $org = Atexo_CurrentUser::getOrganismAcronym();
                if ($consultation instanceof CommonConsultation) {
                    $logger->info('Accés au formulaire publicité pour consultation '.$ref);
                    if (isset($_GET['idDestinataire'])) {
                        $idDestinataire = Atexo_Util::atexoHtmlEntities($_GET['idDestinataire']);
                        $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, $org);
                        if (!$destinataire instanceof CommonDestinatairePub) {
                            $this->panelBlocErreur->setVisible(true);
                            $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                            $this->frameAnnonce->setVisible(false);
                            $logger->error('Accés au formulaire publicité pour le destinataire '.$idDestinataire." de la consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                                .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                        }
                    }
                } else {
                    $this->panelBlocErreur->setVisible(true);
                    $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                    $this->frameAnnonce->setVisible(false);
                    $logger->error("Accés au formulaire publicité pour consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                        .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                }
            } else {
                $logger->error("Accés au formulaire publicité, module plateforme 'PubliciteOpoce' non active");
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        }
    }

    public function updateStatutAnnonce()
    {
        $logger = null;
        $idDestinataire = null;
        try {
            $statut = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_BROUILLON');
            $scriptferme = '';
            if ('1' == $this->statutFormulaire->value) {
                $statut = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET');
                $scriptferme = 'window.close();';
            }
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $idDestinataire = Atexo_Util::atexoHtmlEntities($_GET['idDestinataire']);
            (new Atexo_Publicite_AvisPub())->updateDestAvis($idDestinataire, $statut);
            $logger->info('Le destinataire  '.$idDestinataire.' a été mise à jour statut : '.$statut);
            $this->scriptSaveClose->text = "<script>if(opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater'))opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();".$scriptferme.'</script>';
        } catch (Exception $e) {
            $logger->error("une erreur s'est produite lors de la mise à jour du statut de l'annonce ".$idDestinataire.' consultation '.$_GET['id'].' '.$e->getMessage().' '.$e->getTraceAsString());
            $this->panelBlocErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('MESSAGES_ERREUR_VALIDATION_ANNONCE_SUB'));
            $this->frameAnnonce->setVisible(false);
        }
    }

    public function getUrlFrame()
    {
        $logger = null;
        $idDestinataire = null;
        try {
            if (!$this->getIsPostBack()) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                if (isset($_GET['id']) && isset($_GET['idDestinataire'])) {
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getOrganismAcronym();
                    $idDestinataire = Atexo_Util::atexoHtmlEntities($_GET['idDestinataire']);
                    $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, $org);
                    if ($destinataire instanceof CommonDestinatairePub) {
                        $logger->info("Consutruction de l'annonce".$idDestinataire.' pour consultation '.$ref);
                        $jeton = Atexo_FormulaireSub_AnnonceEchange::authentification($logger);
                        $urlFrame = Atexo_Config::getParameter('URL_SUB');
                        $urlFrame .= Atexo_Config::getParameter('URL_SUB_ANNONCE_FORMULAIRE').'?jeton='.$jeton;
                        if ($jeton) {
                            $urlFrame .= '&idDispositif='.$destinataire->getIdDispositif();
                            $urlFrame .= '&idDossier='.$destinataire->getIdDossier();
                            $logger->info("URL iframe construit formulaire publicité de l'annonce ".$idDestinataire.' de la consultation '.$ref.' : '.$urlFrame);

                            return $urlFrame;
                        } else {
                            $this->panelBlocErreur->setVisible(true);
                            $this->messageErreur->setMessage(Prado::localize('MESSAGES_ERREUR_JETON_VIDE_ANNONCE_SUB'));
                            $this->frameAnnonce->setVisible(false);
                            $logger->error("Pas de jeton pour l'accés au formulaire publicité pour une annonce ".$idDestinataire." de la consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                                .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                        }
                    } else {
                        $this->panelBlocErreur->setVisible(true);
                        $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                        $this->frameAnnonce->setVisible(false);
                        $logger->error('Accés au formulaire publicité pour une annonce '.$idDestinataire." de la consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                            .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                    }
                }
            }
        } catch (Exception) {
            $logger->error("Erreur est survenu lors de la construction de l'url du iframe de l'annonce ".$idDestinataire);
        }
    }
}
