<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\MessageStatusCheckerUtil;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionBiCle extends MpeTPage
{
    /**
     * @var int
     */
    protected $sessionId;

    protected ?string $callBackUrl = null;

    /**
     * Initialiser les données à envoyer à l'application MAMP.
     *
     * @throws \Exception
     */
    private function initGenerateBiCLeData(): void
    {
        if (Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::isAgent()) {
            if (!empty(Atexo_CurrentUser::getId())) {
                //recupérer l'agent connecté
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId(), true);
                $url = Atexo_Config::getParameter('PF_URL_AGENT') != "/"?Atexo_Config::getParameter('PF_URL_AGENT'):Atexo_Config::getParameter('PF_URL_REFERENCE');
                if ($agent) {
                    $this->callBackUrl = $url . 'cryptographie/mpeMock/dual-key?agentId=' . $agent->getId();
                }
            }
        }
        $this->sessionId = (new Atexo_Config())->getTokenForMampAgent();
    }

    public function onInit($param)
    {
        $this->initGenerateBiCLeData();


        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
        MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionBiCles')) {
            $this->panelValidationSummary->visible = false;
            $this->gestionBiCle->visible = true;
            if (Atexo_Module::isEnabled('GestionBiCleMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            if (!$this->IsPostBack) {
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
                $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getOrganismAcronym());
                $this->biClePersonnels->DataBind();
                $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getOrganismAcronym());
                $this->biCleSecours->DataBind();
            }
        } else {
            $this->panelValidationSummary->visible = true;
            $this->gestionBiCle->visible = false;
        }
    }

    public function onClickSearch($sender, $param)
    {
        $selectedIdService = $this->ChoixEntiteAchat->getSelectedEntityPurchase() !== '0'
            ? $this->ChoixEntiteAchat->getSelectedEntityPurchase()
            : null
        ;

        $this->setViewState('idService', $selectedIdService);

        $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($selectedIdService, Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataBind();
        $this->persoPanel->render($param->NewWriter);

        $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($selectedIdService, Atexo_CurrentUser::getCurrentOrganism());
        $this->biCleSecours->DataBind();
        $this->SecoursPanel->render($param->NewWriter);
    }

    public function refreshRepeaters($sender, $param)
    {
        $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataBind();
        $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biCleSecours->DataBind();
        $this->SecoursPanel->render($param->NewWriter);
        $this->persoPanel->render($param->NewWriter);
    }

    public function reloadRepeaters()
    {
        $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataBind();
        $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biCleSecours->DataBind();
    }

    public function refreshRepeaterRelief($sender, $param)
    {
        $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biCleSecours->DataBind();
        $this->SecoursPanel->render($param->NewWriter);
    }

    public function refreshRepeaterPerso($sender, $param)
    {
        $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataBind();
        $this->persoPanel->render($param->NewWriter);
    }

    public function deleteCertificate($sender, $param)
    {
        $idCertificat = $param->CommandParameter;
        (new Atexo_Certificat())->deleteCertificat($idCertificat, Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataSource = (new Atexo_Certificat())->retrievePersonalPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biClePersonnels->DataBind();
        $this->biCleSecours->DataSource = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->biCleSecours->DataBind();
    }

    public function getDateExpiration($certificat)
    {
        $dateExpiration = (new Atexo_Crypto_Certificat())->getDateExpiration($certificat);
        if ($dateExpiration) {
            return Atexo_Util::iso2frnDateTime($dateExpiration) . ' GMT';
        } else {
            return '-';
        }
    }
}
