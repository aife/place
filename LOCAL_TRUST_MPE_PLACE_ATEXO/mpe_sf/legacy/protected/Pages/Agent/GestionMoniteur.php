<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Moniteur;

/**
 * gestion du compte moniteur.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionMoniteur extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
            $this->lienAjouterCompte->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopUpGroupeMoniteur&idService=".$this->getViewState('idService')."','yes');";
            $moniteurs = Atexo_Publicite_Moniteur::search($this->getViewState('idService')); //la fonctions erach a deplacer apres
            $this->RepeaterMoniteur->DataSource = $moniteurs;
            $this->RepeaterMoniteur->DataBind();
        }
    }

    public function getSelectedEntityPurchase()
    {
        return $this->_selectedEntity;
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouterCompte->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopUpGroupeMoniteur&idService=".$this->_selectedEntity."','yes');";
        $jals = Atexo_Publicite_Moniteur::search($this->getViewState('idService'));
        $this->RepeaterMoniteur->DataSource = $jals;
        $this->RepeaterMoniteur->DataBind();
        // Rafraichissement du ActivePanel
        $this->ApRepeaterMoniteur->render($param->NewWriter);
    }

    public function onDeleteClick($sender, $param)
    {
        $idGroupe = $param->CommandParameter;
        Atexo_Publicite_Moniteur::delete($idGroupe);
        $moniteurs = Atexo_Publicite_Moniteur::search($this->getViewState('idService'));
        $this->RepeaterMoniteur->DataSource = $moniteurs;
        $this->RepeaterMoniteur->DataBind();
    }

    public function refreshRepeaterMoniteur($sender, $param)
    {
        // Rafraichissement du ActivePanel
        $moniteurs = Atexo_Publicite_Moniteur::search($this->getViewState('idService'));
        $this->RepeaterMoniteur->DataSource = $moniteurs;
        $this->RepeaterMoniteur->DataBind();
        $this->ApRepeaterMoniteur->render($param->NewWriter);
    }
}
