<?php

namespace Application\Pages\Agent;

use App\Service\Autoformation\ModuleAutoformationRubrique as ModuleAutoformationRubriqueService;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MessageStatusCheckerUtil;

class ModuleAutoformationRubrique extends MpeTPage
{
    /**
     * @var string
     */
    private $access;

    public function onInit($param)
    {

        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        MessageStatusCheckerUtil::initEntryPoints('module-autoformation-rubrique');
    }

    public function onLoad($param){
        if(Atexo_CurrentUser::hasHabilitation('ModuleAutoformation') && $_GET['access']) {
            $this->access = $_GET['access'];
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT')."?page=Agent.AgentHome");
        }
    }

    /**
     * @return string
     */
    public function getAccess(): string
    {
        return $this->access;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        /**
         * @var ModuleAutoformationRubriqueService $moduleAutoformation
         */
        $moduleAutoformationRubrique = Atexo_Util::getSfService(ModuleAutoformationRubriqueService::class);

        if($_GET['methode'] === 'modifier' && $_GET['id']) {
            $moduleAutoformationRubrique->setIdRubrique($_GET['id']);
        }

        return $moduleAutoformationRubrique->getBody($this->access);
    }
}