<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServicePeer;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Service;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe CreationEntityPurchase.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CreationEntityPurchase extends MpeTPage
{
    public $serviceValide = true;
    public $idCreation = false;
    public $idModfication = false;
    public $_organisme;
    public $_codeAcheteur = false;
    public $_affichageService = '1';

    public function getListeServicesSynchronized()
    {
        $this->getViewState('ListeServicesSynchronized');
    }

    public function setListeServicesSynchronized($listeServices)
    {
        $this->setViewState('ListeServicesSynchronized', $listeServices);
    }

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->disableForms();
        }
        $this->_codeAcheteur = Atexo_Module::isEnabled('AfficherCodeService');
        self::customizeForm();
        if (isset($_GET['idEntite'])) {
            $this->idModfication = Atexo_Util::atexoHtmlEntities($_GET['idEntite']);
            $IdEntite = Atexo_Util::atexoHtmlEntities($_GET['idEntite']);
        } else {
            $this->idCreation = Atexo_Util::atexoHtmlEntities($_GET['idParent']);
            $IdEntite = Atexo_Util::atexoHtmlEntities($_GET['idParent']);
        }

        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
            $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        $org = $this->_organisme;
        $this->panelChorus->Visible = Atexo_Module::isEnabled('InterfaceChorusPmi', $org);

        if (!$this->IsPostBack) {
            if (!$this->getIsCallback()) {
                $this->panelMessageAvertissement->setDisplay('None');
            }
            if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $currentServiceId = Atexo_CurrentUser::getCurrentServiceId();
                if (!empty($currentServiceId)) {
                    $arrayChilds = Atexo_EntityPurchase::getSubServices($currentServiceId, $org, true);
                    if (!in_array($IdEntite, $arrayChilds)) {
                        exit;
                    }
                } else {
                    $Services = Atexo_EntityPurchase::getCachedEntities($org);
                    if (!$Services[$IdEntite] && 0 != $IdEntite) {
                        exit;
                    }
                }
            } else {
                $Services = Atexo_EntityPurchase::getCachedEntities($org);
                if (!$Services[$IdEntite] && 0 != $IdEntite) {
                    exit;
                }
            }
            $this->remplirListeEntites($this->idModfication);
            if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())) {
                $arrayDecalage = self::dataSourceDecalageFuseauHoraire();
                $this->decalageParRapportGmt->dataSource = $arrayDecalage;
                $this->decalageParRapportGmt->dataBind();
            }
            if ($this->idModfication) {
                if ($this->idModfication == Atexo_CurrentUser::getCurrentServiceId()) {
                    $this->panellistentite->Visible = false;
                    $this->panellibelleentite->Visible = true;
                    $this->entiteRattachementlabel->Text = Atexo_EntityPurchase::getPathEntityById(Atexo_EntityPurchase::getParent($this->idModfication, $org), $org);
                } else {
                    if (Atexo_EntityPurchase::getParent($this->idModfication, $org)) {
                        $this->entiteRattachement->SelectedValue = Atexo_EntityPurchase::getParent($this->idModfication, $org);
                    }
                }
                $this->displayEA($this->idModfication);
            } elseif ($this->idCreation) {
                $this->entiteRattachement->SelectedValue = $this->idCreation;
            }
            if (!$this->idModfication) {
                if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())) {
                    $this->activerFuseauHoraireNon->checked = 'true';
				}

                $this->activerChorusOui->checked = Atexo_Module::isEnabled('InterfaceChorusPmi', $org);
			}
		}
		$this->listPays->setPostBack($this->IsPostBack);
	}

    /**
     * createEA sauvegarde les données d'une entité d'achat.
     **/
    public function createEA()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexion->beginTransaction();
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            try {
                $EntitePurchaseBean = Atexo_EntityPurchase::retrieveEntityById(Atexo_Util::atexoHtmlEntities($_GET['idEntite']), $this->_organisme);
                $EntitePurchaseBean->setIdEntite($this->idEntite->Text);
                $EntitePurchaseBean->saveService($connexion);
                (new Atexo_EntityPurchase())->reloadCachedEntities($this->_organisme);
                $connexion->commit();
                $this->response->redirect('?page=Agent.GestionEntitesAchat&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
            } catch (Exception $e) {
                $connexion->rollBack();
                Prado::log("Impossible de mettre à jour l'id Entité de l'entité d'achat'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
            }
        } else {
            if ($this->IsValid) {
                try {
                    $EntitePurchaseBean = new CommonService();
                    if ($this->idModfication) {
                        $EntitePurchaseBean = Atexo_EntityPurchase::retrieveEntityById($this->idModfication, $this->_organisme);
                    } else {
                        $EntitePurchaseBean->setSynchronisationExec((Atexo_Module::isEnabled('OrganisationCentralisee') ? '1' : '0'));
                        $EntitePurchaseBean->setDateCreation(date('Y-m-d H:i:s'));
                    }

                    $EntitePurchaseBean->setOrganisme($this->_organisme);
                    $EntitePurchaseBean->setDateModification(date('Y-m-d H:i:s'));
                    $EntitePurchaseBean->setSigle($this->sigle->Text);
                    $EntitePurchaseBean->setLibelle($this->libelle->Text);
                    $EntitePurchaseBean->setFormeJuridique($this->formeJuridique->Text);
                    $EntitePurchaseBean->setFormeJuridiqueCode($this->formeJuridiqueCode->Text);
                    $EntitePurchaseBean->setAdresse($this->adresse->Text);
                    $EntitePurchaseBean->setAdresseSuite($this->adressesuite->Text);
                    $EntitePurchaseBean->setCp($this->cp->Text);
                    $EntitePurchaseBean->setTelephone($this->tel->Text);
                    $EntitePurchaseBean->setVille($this->ville->Text);
                    $EntitePurchaseBean->setFax($this->fax->Text);
                    $EntitePurchaseBean->setMail(trim($this->mail->Text));
                    if ((new Atexo_Module())->isSaisieManuelleActive($this->_organisme)) {
                        $EntitePurchaseBean->setIdExterne(trim($this->idExterneSSO->Text));
                    }
                    if (true == $this->affichageServiceNon->Checked) {
                        $this->_affichageService = '0';
                    }
                    $EntitePurchaseBean->setAffichageService($this->_affichageService);
                    $EntitePurchaseBean->setSiren($this->siren->Text);
                    if ($this->_codeAcheteur) {
                        $EntitePurchaseBean->setSiren($this->codeAcheteur->Text);
                    }
                    $EntitePurchaseBean->setComplement($this->siret->Text);
                    $EntitePurchaseBean->setLibelleAr($this->libelle_ar->Text);
                    $EntitePurchaseBean->setAdresseAr($this->adresse_ar->Text);
                    $EntitePurchaseBean->setAdresseSuiteAr($this->adressesuite_ar->Text);
                    $EntitePurchaseBean->setVilleAr($this->ville_ar->Text);
                    $EntitePurchaseBean->setIdEntite($this->idEntite->Text);

                    $pays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->listPays->getSelectedValue());

                    $EntitePurchaseBean->setPays($pays);
                    $pathServiceParent = explode(' - ', Atexo_EntityPurchase::getPathServiceById($this->entiteRattachement->SelectedValue, $this->_organisme));
                    if ($pathServiceParent) {
                        $pathService = $pathServiceParent[0].' / '.$this->sigle->Text.' - '.$this->libelle->Text;
                        $pathServiceAr = $pathServiceParent[0].' / '.$this->sigle->Text.' - '.$this->libelle_ar->Text;
                    }

                    $EntitePurchaseBean->setCheminComplet($pathService);
                    $EntitePurchaseBean->setCheminCompletAr($pathServiceAr);

                    if (($this->idCreation && 0 != $this->idCreation)) {
                        if ('0' !== $this->entiteRattachement->SelectedValue && 0 !== $this->entiteRattachement->SelectedValue) {
                            $affiliationService = new CommonAffiliationService();
                            $affiliationService->setServiceParentId($this->entiteRattachement->SelectedValue);
                            $EntitePurchaseBean->addCommonAffiliationServiceRelatedByIdService($affiliationService);
                        }
                    } elseif ($this->idModfication && $this->idModfication != Atexo_CurrentUser::getIdServiceAgentConnected()) {
                        $this->deleteAffiliationService($this->idModfication, $this->_organisme, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')));
                        if ('0' !== $this->entiteRattachement->SelectedValue && 0 !== $this->entiteRattachement->SelectedValue) {
                            $affiliationService = new CommonAffiliationService();
                            $affiliationService->setServiceParentId($this->entiteRattachement->SelectedValue);
                            $EntitePurchaseBean->addCommonAffiliationServiceRelatedByIdService($affiliationService);
                        }
                    }

                    $EntitePurchaseBean->setNomServiceArchiveur($this->nomServiceArchive->Text);
                    $EntitePurchaseBean->setIdentifiantServiceArchiveur($this->idServiceArchive->Text);

                    if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())) {
                        $activationFuseauHoraire = '';
                        if (true == $this->activerFuseauHoraireOui->checked) {
                            $activationFuseauHoraire = 1;
                        } elseif (true == $this->activerFuseauHoraireNon->checked) {
                            $activationFuseauHoraire = 0;
                        }
                        $EntitePurchaseBean->setActivationFuseauHoraire($activationFuseauHoraire);
                        $EntitePurchaseBean->setDecalageHoraire($this->decalageParRapportGmt->Text);
                        $EntitePurchaseBean->setLieuResidence($this->LieuResidence->Text);
                    }

                    $EntitePurchaseBean->saveService($connexion);

                    $activationChorus = $this->activerChorusOui->checked === true ? '1' : '0';
                    $EntitePurchaseBean->setAccesChorus($activationChorus);
                    $EntitePurchaseBean->save($connexion);

                    $marchePublie = (new Atexo_Marche())->creerMarchePublieIfNotExiste(date('Y'), $this->_organisme, $EntitePurchaseBean->getId(), $connexion);
                    (new Atexo_EntityPurchase())->reloadCachedEntities($this->_organisme);
                    $connexion->commit();
                    $this->response->redirect('?page=Agent.GestionEntitesAchat&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
                } catch (Exception $e) {
                    $connexion->rollBack();
                    Prado::log("Impossible de creer une entité d'achat'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
                }
            }
        }
    }

    /**
     * supprime l'objet affiliation à partir de l'id passé en paramètre.
     *
     * @param  $id id de la pj à supprimer
     * @param $org Organismes
     *
     * @return bool
     */
    public function deleteAffiliationService($idservice, $org, $connexion)
    {
        $c = new Criteria();
        $c->add(CommonAffiliationServicePeer::SERVICE_ID, $idservice, CRITERIA::EQUAL);
        $c->add(CommonAffiliationServicePeer::ORGANISME, $org, CRITERIA::EQUAL);
        $AffiliationServ = CommonAffiliationServicePeer::doSelectOne($c, $connexion);
        if (is_object($AffiliationServ)) {
            $AffiliationServ->delete($connexion);
        }
    }

    /**
     * displayEA remplie la page de detail d'une entité d'achat.
     **/
    public function displayEA($IdEntite)
    {
        try {
            $AcrconnexionOrg = $this->_organisme;
            $EntitePurchaseBean = Atexo_EntityPurchase::retrieveEntityById($IdEntite, $AcrconnexionOrg);

            if ($EntitePurchaseBean && $EntitePurchaseBean instanceof CommonService) {
                $this->sigle->Text = $EntitePurchaseBean->getSigle();
                $this->libelle->Text = $EntitePurchaseBean->getLibelle();
                $this->formeJuridique->Text = $EntitePurchaseBean->getFormeJuridique();
                $this->formeJuridiqueCode->Text = $EntitePurchaseBean->getFormeJuridiqueCode();
                $this->adresse->Text = $EntitePurchaseBean->getAdresse();
                $this->adressesuite->Text = $EntitePurchaseBean->getAdresseSuite();
                $this->cp->Text = $EntitePurchaseBean->getCp();
                $this->tel->Text = $EntitePurchaseBean->getTelephone();
                $this->ville->Text = $EntitePurchaseBean->getVille();

                if ((new Atexo_Module())->isSaisieManuelleActive($AcrconnexionOrg)) {
                    $this->idExterneSSO->Text = $EntitePurchaseBean->getIdExterne();
                }
                if ('0' == $EntitePurchaseBean->getAffichageService()) {
					$this->affichageServiceOui->setChecked(false);
					$this->affichageServiceNon->setChecked(true);
				}
				$this->listPays->SelectedValue = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdCountry($EntitePurchaseBean->getPays());
				$this->fax->Text = $EntitePurchaseBean->getFax();
				$this->mail->Text = $EntitePurchaseBean->getMail();
				if(Atexo_Module::isEnabled('SiretDetailEntiteAchat')){
					$this->siren->Text = $EntitePurchaseBean->getSiren();
                }
                if($this->_codeAcheteur){
                    $this->codeAcheteur->Text = $EntitePurchaseBean->getSiren();
                }
                $this->siret->Text = $EntitePurchaseBean->getComplement();
                $this->libelle_ar->Text = $EntitePurchaseBean->getLibelleAr();
                $this->adresse_ar->Text = $EntitePurchaseBean->getAdresseAr();
                $this->adressesuite_ar->Text = $EntitePurchaseBean->getAdressesuiteAr();
                $this->ville_ar->Text = $EntitePurchaseBean->getVilleAr();
                $this->nomServiceArchive->Text = $EntitePurchaseBean->getNomServiceArchiveur();
                $this->idEntite->Text = $EntitePurchaseBean->getIdEntite();
                $this->idServiceArchive->Text = $EntitePurchaseBean->getIdentifiantServiceArchiveur();
				
				if(Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())) {
                    if (0 == strcmp($EntitePurchaseBean->getActivationFuseauHoraire(), '1')) {
				        $this->activerFuseauHoraireOui->checked = true;
				    } else {
				    	$this->activerFuseauHoraireNon->checked = true;
				    }
					$this->decalageParRapportGmt->Text = $EntitePurchaseBean->getDecalageHoraire();
					$this->LieuResidence->Text = $EntitePurchaseBean->getLieuResidence();
				}

				if ($EntitePurchaseBean->getAccesChorus() === '1') {
				    $this->activerChorusOui->checked = true;
                } else {
                    $this->activerChorusNon->checked = true;
                }
				$this->panelInfoService->setDisplay('Dynamic');
				$this->buttonValider->setDisplay('Dynamic');
			}
        }catch(Exception $e){
			Prado::log("Impossible de récuperer cette entité d'achat'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function RedirectReturn()
    {
        if (isset($_GET['idEntite'])) {
            $this->response->redirect('?page=Agent.AgentDetailEntiteAchat&idEntite='.Atexo_Util::atexoHtmlEntities($_GET['idEntite']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        } elseif ($_GET['idParent']) {
            $this->response->redirect('?page=Agent.AgentDetailEntiteAchat&idEntite='.Atexo_Util::atexoHtmlEntities($_GET['idParent']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        } else {
            $this->response->redirect('?page=Agent.AgentDetailOrganisme&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        }
    }

    public function remplirListeEntites($idService = null)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $currentServiceId = Atexo_CurrentUser::getCurrentServiceId();
            if (empty($currentServiceId)) {
                // Si l'agent n'est pas affecté à un service on affiche tout les services
                $listeChilds = Atexo_EntityPurchase::getEntityPurchase($this->_organisme, true);
            } else {
                // Si l'agent est affecté à un service onb affiche tout les services en doussous de son pôle
                $listeChilds = Atexo_EntityPurchase::getListSubServices($currentServiceId, $this->_organisme);
            }
        } else {
            $listeChilds = Atexo_EntityPurchase::getEntityPurchase($this->_organisme, true);
        }

        $listeServiceAExclure = Atexo_EntityPurchase::getListSubServices($idService, $this->_organisme);

        if ($idService) {
            unset($listeChilds[$idService]);
        }
        if (is_array($listeServiceAExclure)) {
            $serviceName = '';
            foreach ($listeServiceAExclure as $serviceId => $serviceName) {
                unset($listeChilds[$serviceId]);
            }
        }

        $this->entiteRattachement->DataSource = $listeChilds;
        $this->entiteRattachement->DataBind();
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) {
            $this->validateur_libelle_ar->setEnabled(true);
            $this->validateur_adresse_ar->setEnabled(true);
            $this->validateur_ville_ar->setEnabled(true);
        } else {
            $this->validateur_libelle_ar->setEnabled(false);
            $this->validateur_adresse_ar->setEnabled(false);
            $this->validateur_ville_ar->setEnabled(true);
        }
        if (Atexo_Module::isEnabled('SiretDetailEntiteAchat')) {
            $this->panelSiret->setVisible(true);
        } else {
            $this->panelSiret->setVisible(false);
        }

        if ($this->_codeAcheteur) {
            $this->panelCodeAcheteur->setVisible(true);
        } else {
            $this->panelCodeAcheteur->setVisible(false);
        }
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
        }
    }

    /**
     * Verifier que le code acheteur  est unique.
     */
    public function verifyCodeAcheteur($sender, $param)
    {
        if ($this->_codeAcheteur) {
            $codeAcheteur = $this->codeAcheteur->getText();
            if ('' != $codeAcheteur) {
                $services = (new Atexo_EntityPurchase())->retrieveServiceByCodeAcheteur($codeAcheteur);
                if (is_array($services) && count($services)) {
                    foreach ($services as $service) {
                        if ($service->getId() != $_GET['idEntite'] || $service->getOrganisme() != $_GET['org']) {
                            $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                            $param->IsValid = false;
                            $this->validatorCodeAcheteur->ErrorMessage = Prado::localize('CODE_ACHETEUR_EXISTE_DEJA', ['code acheteur' => $codeAcheteur]);

                            return '';
                        }
                    }
                }
            } else {
                $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->validatorCodeAcheteur->ErrorMessage = Prado::localize('CODE_ACHETEUR');

                return '';
            }
        }
    }

    /**
     * Permet de remplir les decalages horaires par rapport au GMT.
     */
    public function dataSourceDecalageFuseauHoraire()
    {
        $arrayDecalage = [];
        $arrayDecalage['Selectionnez'] = Prado::localize('TEXT_SELECTION_LISTE');
        for ($i = -12; $i <= 0; ++$i) {
            $arrayDecalage[$i] = $i;
        }
        for ($i = 1; $i <= 12; ++$i) {
            $arrayDecalage[$i] = '+'.$i;
        }

        return $arrayDecalage;
    }

    public function activerFuseauHoraire()
    {
        if ('true' == $this->activerFuseauHoraireOui->Checked) {
            return true;
        } elseif ('false' == $this->activerFuseauHoraireOui->Checked) {
            return false;
        }
    }

    /**
     * TODO.
     */
    public function synchroniserAvecApiGouvEntreprise()
    {
        $siren = $this->siren->getSafeText();
        $siret = $this->siret->getSafeText();
        self::synchronizeService($siren, $siret);
    }

    public function synchronizeService($siren, $siret, $majInfo = true)
    {
        $serviceEtabApiGouvEntreprise = null;
        $loggerWs = Atexo_LoggerManager::getLogger();
        $idObjet = null;
        $this->formeJuridique->setEnabled(false);
        if ($this->idModfication) {
            $service = Atexo_EntityPurchase::retrieveEntityById($this->idModfication, $this->_organisme);
            if ($service) {
                $idObjet = $service->getId();
            }
        }
        $message = null;
        $apiGouvEntreprise = (new Atexo_EntityPurchase())->recupererServiceApiGouvEntreprise($siren, $message, null, $idObjet);
        if (!$message) {
            $serviceEtabApiGouvEntreprise = (new Atexo_EntityPurchase())->recupererServiceApiGouvEntrepriseBySiret($siren.$siret, $message, null, $idObjet);
        }
        if ($message) {
            $this->panelMessageAvertissement->setDisplay('Dynamic');
            $this->messageAvertissement->setMessage($message);
            $this->script->text = "<script>document.getElementById('ctl0_CONTENU_PAGE_panelInfoService').style.display='none';document.getElementById('ctl0_CONTENU_PAGE_buttonValider').style.display='none';</script>";
        } elseif ($apiGouvEntreprise && $serviceEtabApiGouvEntreprise) {
            $listeServicesSynchronized = $this->getViewState('ListeServicesSynchronized');
            if (!is_array($listeServicesSynchronized)) {
                $listeServicesSynchronized = [];
            }
            if (!array_key_exists($siren.$siret, $listeServicesSynchronized)) {
                $listeServicesSynchronized[$siren.$siret] = true;
            }
            $this->setViewState('ListeServicesSynchronized', $listeServicesSynchronized);

            if ($majInfo) {
                $this->libelle->setText($apiGouvEntreprise->getNom());
                $this->formeJuridique->setText(Atexo_Util::utf8ToIso($apiGouvEntreprise->getFormejuridique()));
                $this->formeJuridiqueCode->setText($apiGouvEntreprise->getFormejuridiqueCode());
                $serviceEtabApiGouvEntreprise = (new Atexo_Entreprise_Etablissement())->recupererEtablissementApiGouvEntreprise($siren.$siret, null, $idObjet);
                if ($serviceEtabApiGouvEntreprise) {
                    $this->adresse->setText($serviceEtabApiGouvEntreprise->getAdresse());
                    $this->adressesuite->setText($serviceEtabApiGouvEntreprise->getAdresse2());
                    $this->cp->setText($serviceEtabApiGouvEntreprise->getCodePostal());
                    $this->ville->setText($serviceEtabApiGouvEntreprise->getVille());
                }
            }
            $this->messageAvertissement->setMessage(Prado::localize('SIRET_EXISTE_DANS_INSEE'));
            $this->panelMessageAvertissement->setDisplay('Dynamic');
            $this->script->text = "<script>J('#ctl0_CONTENU_PAGE_formeJuridique').attr('readonly', 'readonly');document.getElementById('ctl0_CONTENU_PAGE_panelInfoService').style.display='';document.getElementById('ctl0_CONTENU_PAGE_buttonValider').style.display='';</script>";

            return true;
        } else {
            $this->messageAvertissement->setMessage(Prado::localize('SIRET_N_EXISTE_PAS_DANS_INSEE'));
            $this->panelMessageAvertissement->setDisplay('Dynamic');
            $this->formeJuridique->setEnabled(true);
            $this->script->text = "<script>J('#ctl0_CONTENU_PAGE_formeJuridique').removeAttr('readonly');document.getElementById('ctl0_CONTENU_PAGE_panelInfoService').style.display='';document.getElementById('ctl0_CONTENU_PAGE_buttonValider').style.display='';</script>";

            return true;
        }

        return false;
    }

    public function verifySynchroSiret($sender, $param)
    {
        $siren = $this->siren->getSafeText();
        $siret = $this->siret->getSafeText();
        if ($siren && $siret) {
            $listeServicesSynchronized = $this->getViewState('ListeServicesSynchronized');
            if (!is_array($listeServicesSynchronized) || !array_key_exists($siren.$siret, $listeServicesSynchronized)) {
                $resultatSynchronize = self::synchronizeService($siren, $siret, false);
                if (!$resultatSynchronize) {
                    $param->IsValid = false;
                }
            }
        }
    }

    public function disableForms()
    {
        $this->siren->enabled = 'false';
        $this->siret->enabled = 'false';
        $this->synchroniserAvecApiGouvEntreprise->setVisible(false);
        $this->libelle->enabled = 'false';
        $this->sigle->enabled = 'false';
        $this->adresse->enabled = 'false';
        $this->entiteRattachement->enabled = 'false';
        $this->adresse_ar->enabled = 'false';
        $this->adressesuite->enabled = 'false';
        $this->cp->enabled = 'false';
        $this->ville->enabled = 'false';
        $this->ville_ar->enabled = 'false';
        $this->listPays->enabled = 'false';
        $this->tel->enabled = 'false';
        $this->fax->enabled = 'false';
        $this->mail->enabled = 'false';
        $this->codeAcheteur->enabled = 'false';
        $this->affichageService->enabled = 'false';
        $this->activerFuseauHoraireOui->enabled = 'false';
        $this->LieuResidence->enabled = 'false';
        $this->nomServiceArchive->enabled = 'false';
        $this->idServiceArchive->enabled = 'false';

        if (Atexo_Module::isEnabled('SiretDetailEntiteAchat')) {
            $this->synchroniserAvecSGMAP->setVisible(false);
        }
    }

    /**
     * Verifier que l'id externe est unique.
     */
    public function verifyIdExterneSsoServiceCreation($sender, $param)
    {
        $this->script->Text = '';
        $idExterneSSO = $this->idExterneSSO->getText();
        $idEntite = null;
        if (Atexo_Util::atexoHtmlEntities($_GET['idEntite'])) {
            $idEntite = Atexo_Util::atexoHtmlEntities($_GET['idEntite']);
        }

        if ($idExterneSSO) {
            if (strstr($idExterneSSO, ' ')) {
                $param->IsValid = false;
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $this->divValidationSummary->addServerError(Prado::localize('ID_EXTERNE_SSO'), ['idExterneSSO' => $idExterneSSO]);

                return;
            }
            $services = Atexo_Service::retrieveServicesByIdExterne(
                $idExterneSSO,
                Atexo_CurrentUser::getOrganismAcronym(),
                $idEntite
            );
            if (($services === null ? 0 : count($services)) > 0) {
                $param->IsValid = false;
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';document.getElementById('ctl0_CONTENU_PAGE_ctl57').style='color:red;';goToTopPage('divValidationSummary');</script>";
                $this->divValidationSummary->addServerError(Prado::localize('SERVICE_ID_EXTERNE_SSO_ALREADY_EXISTS'), ['idExterneSSO' => $idExterneSSO]);

                return;
            }
        }
    }
}
