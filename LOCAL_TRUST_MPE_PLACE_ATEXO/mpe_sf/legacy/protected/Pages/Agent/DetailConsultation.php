<?php

namespace Application\Pages\Agent;

use App\Entity\Consultation;
use App\Repository\ConsultationRepository;
use App\Service\ClausesService;
use App\Utils\Encryption;
use Application\Controls\MenuGaucheAgent;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Validation;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_ImpressionODT;
use AtexoPdf\PdfGeneratorClient;
use AtexoPdf\RtfGeneratorClient;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Contient le detail d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailConsultation extends MpeTPage
{
    public $_consultation;
    public $_refConsultation;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $message = null;
        if (0 == strcmp($_GET['id'], $_GET['id'])) {
            $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->DroitsAcces->setReference($this->_refConsultation);

            if ($this->_consultation instanceof CommonConsultation) {
                $this->DroitsAcces->setIdService($this->_consultation->getServiceId());
                $this->DroitsAcces->setIdRpa($this->_consultation->getIdRpa());
                $this->DroitsAcces->setIdServiceAssocie($this->_consultation->getServiceAssocieId());
                $this->IdConsultationSummary->setConsultation($this->_consultation);
                $this->consultationAdditionalInformations->setConsultation($this->_consultation);
                $this->consultationReplyTerms->setConsultation($this->_consultation);
                $this->calendrierReel->setConsultation($this->_consultation);
                $this->consultationAlertes->setConsultation($this->_consultation);
                $this->consultationValidationEnLigne->setConsultation($this->_consultation);
                $this->displayActions($this->_consultation);
                $this->setEnvoiMessageNavigateUrl();
                $this->setSuiviEchangesUrl();
                $idService = $this->_consultation->getServiceId();
                //autorise publicite
                if (Atexo_Module::isEnabled('ParametragePubliciteParTypeProcedure')) {
                    if ('0' == $this->_consultation->getAutoriserPublicite()) {
                        $this->panelPubConsultation->setVisible(false);
                        $this->panelPubOuverture->setVisible(false);
                        $this->panelPubDecision->setVisible(false);
                        $this->panelPublicitePreparation->setVisible(false);
                        /* Debut Modification LEZ */
                        $this->panelPubliciteElaboration->setVisible(false);
                        /* Modification LEZ Fin */
                    }
                }

                if (isset($_GET['errorDeleteConsultation'])) {
                    $this->errorPanel->Visible = true;
                    $this->panelErrorLabel->Text = Prado::localize('TEXT_SUPPRESSION_CONSULTATION_AVAL');
                }

                /*
                 $this->displayConsultation($this->_consultation);
                 */
            } else {
                $idService = 0;
                $this->panelDetailsConsultation->visible = false;
                $this->errorPanel->visible = true;
                $this->panelErrorLabel->Text = Prado::localize('TEXT_PAS_AUTORISE_DETAIL');
                $this->IdConsultationSummary->setWithException(false);
                $this->consultationAdditionalInformations->setWithException(false);
                $this->consultationReplyTerms->setWithException(false);
            }

            if (!$this->isPostBack) {
                $this->DroitsAcces->remplirMonEntite();
                $this->DroitsAcces->remplirEntiteAssociee();
                if (Atexo_Module::isEnabled('GestionMandataire') || Atexo_CurrentUser::hasHabilitation('RattachementService')) {
                    $this->DroitsAcces->remplirEntite($idService);
                }
                $this->DroitsAcces->fillListRpa();
                $this->DroitsAcces->setPostBack(true);
            } else {
                $this->DroitsAcces->setPostBack(false);
            }

            $langueOblPublication = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
            $autreLangObligExiste = false;
            if (is_countable($langueOblPublication) ? count($langueOblPublication) : 0) {
                $listeLangues = "<ul class='default-list liste-langues'>";
                foreach ($langueOblPublication as $oneLangue) {
                    if (Atexo_CurrentUser::readFromSession('lang') != $oneLangue->getLangue()) {
                        $urlTraduction = 'index.php?page=Agent.TraduireConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&langue=' . $oneLangue->getLangue();
                        $listeLangues .= "<li> <a href='" . $urlTraduction . "' >" . ucfirst(strtolower($oneLangue->getLibelle())) . '</a></li>';
                    }
                }
                $listeLangues .= '</ul>';
                if (isset($_GET['traductionOgl'])) {
                    $autreLangObligExiste = true;
                    $message = '<span>' . Prado::localize('REPORTER_MODIFICATIONS_DANS_LANGUES', ['langues' => $listeLangues]) . '</span>';
                }
                $this->panelMessageAvertTraduction->setVisible($autreLangObligExiste);
                $this->panelMessageAvertTraduction->setMessage($message);
            }
            if ('1' == $_GET['suivi']) {
                $this->TextJsSuivi->Text = "<script>togglePanel(this,'suiviEchanges');</script>";
            }
        }
    }

    public function setSuiviEchangesUrl()
    {
        $id = $this->_consultation->getId();
        $messecVersion = $this->_consultation->getVersionMessagerie();
        $this->idSuiviEchanges->NavigateUrl = 'index.php?page=Agent.SuiviEchanges&id=' . $id;
        if (2 === $messecVersion) {
            $encrypte = Atexo_Util::getSfService(Encryption::class);
            $idCrypte = $encrypte->cryptId($id);

            $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_SUIVI_MESSAGE'));
            $this->idSuiviEchanges->NavigateUrl = $pfUrl
                . '/'
                . $idCrypte;
            $this->idSuiviEchanges->Target = '_blank';
        }
    }

    /**
     * @throws Atexo_Config_Exception
     */
    public function setEnvoiMessageNavigateUrl()
    {
        $id = $this->_consultation->getId();
        $messeVersion = $this->_consultation->getVersionMessagerie();
        $this->idMessageSimple->NavigateUrl = 'index.php?page=Agent.EnvoiCourrierElectroniqueSimple&id=' . $id;
        $this->idMessageInvitationConcourir->NavigateUrl = 'index.php?page=Agent.EnvoiCourrierElectroniqueInvitationConcourir&id=' . $this->_consultation->getId() . '&invitation';
        if (2 === $messeVersion) {
            $encrypte = Atexo_Util::getSfService(Encryption::class);
            $idCrypte = $encrypte->cryptId($id);

            $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_NEW_MESSAGE'));
            $this->idMessageSimple->NavigateUrl = $pfUrl
                . '/'
                . $idCrypte;
            $this->idMessageSimple->Target = '_blank';

            $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_INVITATION'));
            $this->idMessageInvitationConcourir->NavigateUrl = $pfUrl
                . '/'
                . $idCrypte;
            $this->idMessageInvitationConcourir->Target = '_blank';
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /**
     * Actions sur la consultation selon les habilitation de l'agent et selon l'etat de la consultation.
     *
     * @param
     */
    public function displayActions($consultation)
    {
        $etatConsultation = $consultation->getStatusConsultation();
        $dateAjourdhui = date('Y-m-d H:i:s');
        $depouiablePhaseCons = $consultation->getDepouillablePhaseConsultation();
        ////////////////////////////////Preparation///////////////////////////////////

        if ($consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
            $this->suiviDoc->setVisible(false);
        }

        // Le nombre de retraits electroniques et papiers
        $registreRetaitsElectronique = $consultation->getNombreTelecharegementElectronique();
        $registreRetaitsPapier = $consultation->getNombreTelecharegementPapier();
        // Le nombre des questions electroniques et papiers
        $questionsElectronique = $consultation->getNombreQuestionElectroniqueDces();
        $questionsPapiers = $consultation->getNombreQuestionPapierDces();
        //Le nombre des depots electronique et papiers
        $registreDepotsElectroniques = $consultation->getNombreOffreElectronique();
        $registreDepotsPapiers = $consultation->getNombreOffrePapier();

        $this->labelRegistreRetraitsConsultation->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
        $this->labelQuestionsConsultation->setText($questionsElectronique . ' + ' . $questionsPapiers);
        $this->labelRegistreDepot->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);

        $this->labelRegistreRetraisOuverture->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
        $this->labelQuestionsOuverture->setText($questionsElectronique . ' + ' . $questionsPapiers);
        $this->labelRegistreDepotOuverture->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);

        $this->labelRegitreRetaitsDecision->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
        $this->labelQuestionDecision->setText($questionsElectronique . ' + ' . $questionsPapiers);
        $this->labelRegistreDepotDecision->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);

        //$activeLangues=Atexo_Languages::getAllActiveLanguagesButDefaultOne();
        $activeLangues = (new Atexo_Languages())->getAllActiveLanguagesButCurrentOne();

        if (!is_array($activeLangues)) {
            $activeLangues = [];
        }
        /* Modification LEZ */
        $this->repeaterTraduireElaboration->DataSource = $activeLangues;
        $this->repeaterTraduireElaboration->DataBind();
        /* Modification LEZ Fin*/
        $this->repeaterTraduirePreparation->DataSource = $activeLangues;
        $this->repeaterTraduirePreparation->DataBind();

        $this->repeaterTraduireConsultation->DataSource = $activeLangues;
        $this->repeaterTraduireConsultation->DataBind();

        if (!Atexo_Module::isEnabled('TraduireConsultation')) {
            foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                $item->panelTraduire->setVisible(false);
            }
            foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                $item->panelTraduire->setVisible(false);
            }
            /* Debut :Modification LEZ */
            foreach ($this->repeaterTraduireElaboration->getItems() as $item) {
                $item->panelTraduire->setVisible(false);
            }
            /* Fin: Modification LEZ */
        }
        //////////////////////////////////Elaboration /////////////////////////////
        if ($etatConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) { //ETAT_ELABORATION
            $this->etape1->SetId('etape0');
            if (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                if (!$consultation->getCurrentUserReadOnly()) {
                    $this->panelModifierElaboration->setCssClass('picto-link');
                    $this->ImageModifierElaboration->setActivate('true');
                    $this->ImageModifierElaboration->setClickable('true');
                    $this->linkModifierElaboration->setVisible(true);
                    $this->labelModifierElaboration->setVisible(false);
                } else {
                    $this->ImageModifierElaboration->setActivate('false');
                    $this->ImageModifierElaboration->setClickable('false');
                }
                $this->linkModifierElaboration->Text = Prado::localize('BOUTON_MODIFIER');
            }
            if (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation')) {
                if (!$consultation->getCurrentUserReadOnly()) {
                    $this->panelCreerSuiteElaboration->setCssClass('picto-link');
                    $this->imageCreerSuiteElaboration->setActivate('true');
                    $this->imageCreerSuiteElaboration->setClickable('true');
                    $this->linkCreerSuiteElaboration->setVisible(true);
                    $this->labelCreerSuiteElaboration->setVisible(false);
                } else {
                    $this->imageCreerSuiteElaboration->setActivate('false');
                    $this->imageCreerSuiteElaboration->setClickable('false');
                }
            }
            if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                if (!$consultation->getCurrentUserReadOnly()) {
                    $this->panelPubliciteElaboration->setCssClass('picto-link action-mpe');
                    $this->ImagePubliciteElaboration->setActivate('true');
                    $this->ImagePubliciteElaboration->setClickable('true');
                    $this->linkPubliciteElaboration->setVisible(true);
                    $this->labelPubliciteElaboration->setVisible(false);
                } else {
                    $this->ImagePubliciteElaboration->setActivate('false');
                    $this->ImagePubliciteElaboration->setClickable('false');
                }
            }

            if (Atexo_Module::isEnabled('TraduireConsultation')) {
                if (!$consultation->getCurrentUserReadOnly()) {
                    foreach ($this->repeaterTraduireElaboration->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $item->panelTraduire->setCssClass('picto-link action-mpe');
                        $item->ImageTraduire->setActivate('true');
                        $item->ImageTraduire->setClickable('true');
                        $item->linkTraduire->setVisible(true);
                        $item->labelTraduire->setVisible(false);
                    }
                } else {
                    foreach ($this->repeaterTraduireElaboration->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $item->ImageTraduire->setActivate('false');
                        $item->ImageTraduire->setClickable('false');
                    }
                }
            }

            if (
                !$consultation->getCurrentUserReadOnly()
                && Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')
                && $this->isDeleteConsultation($consultation->getId())
            ) {
                $this->panelDeleteConsultationElaboration->setCssClass('picto-link');
                $this->ImageDeleteConsultationElaboration->setActivate('true');
                $this->ImageDeleteConsultationElaboration->setClickable('true');
                $this->ImageDeleteConsultationElaboration->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->linkDeleteConsultationElaboration->setVisible(true);
                $this->linkDeleteConsultationElaboration->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->labelDeleteConsultationElaboration->setVisible(false);
            } else {
                $this->ImageDeleteConsultationElaboration->setActivate('false');
                $this->ImageDeleteConsultationElaboration->setClickable('false');
            }
            if (Atexo_Module::isEnabled('GestionModelesFormulaire')) {
                $this->panelFormulaireEtCriteresElaboration->setStyle("display:''");
            }

            if (
                (Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac') || Atexo_CurrentUser::hasHabilitation('ValidationDocumentsRedac'))
                && Atexo_Module::isEnabled('InterfaceModuleRsem')
                && $consultation->getRedactionDocumentsRedac()
                && !$consultation->getCurrentUserReadOnly()
                && $consultation->getDonneeComplementaireObligatoire()
            ) {
                $this->panelRedactionElaboration->setCssClass('picto-link action-redac');
                $this->ImageRedactionElaboration->setActivate('true');
                $this->ImageRedactionElaboration->setClickable('true');
                $this->linkRedactionElaboration->setVisible(true);
                $this->labelRedactionElaboration->setVisible(false);
            } else {
                $this->ImageRedactionElaboration->setActivate('false');
                $this->ImageRedactionElaboration->setClickable('false');
            }
        } else {
            ///////////////////////////////Preparation//////////////////////////////////
            if ($etatConsultation == Atexo_Config::getParameter('ETAT_EN_ATTENTE')) {
                $this->etape1->SetId('etape1');
                if (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                    if (!$consultation->getCurrentUserReadOnly()) {
                        $this->panelModifierPreparation->setCssClass('picto-link');
                        $this->ImageModifierPreparation->setActivate('true');
                        $this->ImageModifierPreparation->setClickable('true');
                        $this->linkModifierPreparation->setVisible(true);
                        $this->labelModifierPreparation->setVisible(false);
                    } else {
                        $this->ImageModifierPreparation->setActivate('false');
                        $this->ImageModifierPreparation->setClickable('false');
                    }
                    $this->linkModifierPreparation->Text = Prado::localize('BOUTON_MODIFIER');
                }
                if (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation')) {
                    if (!$consultation->getCurrentUserReadOnly()) {
                        $this->panelCreerSuitePreparation->setCssClass('picto-link');
                        $this->imageCreerSuitePreparation->setActivate('true');
                        $this->imageCreerSuitePreparation->setClickable('true');
                        $this->linkCreerSuitePreparation->setVisible(true);
                        $this->labelCreerSuitePreparation->setVisible(false);
                    } else {
                        $this->imageCreerSuitePreparation->setActivate('false');
                        $this->imageCreerSuitePreparation->setClickable('false');
                    }
                }
                if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                    if (!$consultation->getCurrentUserReadOnly()) {
                        $this->panelPublicitePreparation->setCssClass('picto-link action-mpe');
                        $this->ImagePublicitePreparation->setActivate('true');
                        $this->ImagePublicitePreparation->setClickable('true');
                        $this->linkPublicitePreparation->setVisible(true);
                        $this->labelPublicitePreparation->setVisible(false);
                    } else {
                        $this->ImagePublicitePreparation->setActivate('false');
                        $this->ImagePublicitePreparation->setClickable('false');
                    }
                }

                if ($consultation->getValidationHabilitation()) {
                    $this->panelValidate->setVisible(true);
                    $this->panelValidate->setCssClass('picto-link action-mpe');
                    $this->ImageValidate->setActivate('true');
                    $this->ImageValidate->setClickable('true');
                    $this->linkValidate->setVisible(true);
                    $this->linkValidate->setNavigateUrl(Atexo_Consultation_Validation::validateConsultation($this->_consultation->getId()));
                    $this->labelValidate->setVisible(false);
                } else {
                    $this->panelValidate->setVisible(true);
                    $this->ImageValidate->setActivate('false');
                    $this->ImageValidate->setClickable('false');
                }

                if (Atexo_Module::isEnabled('TraduireConsultation')) {
                    if (!$consultation->getCurrentUserReadOnly()) {
                        foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                            $item->panelTraduire->setVisible(true);
                            $item->panelTraduire->setCssClass('picto-link action-mpe');
                            $item->ImageTraduire->setActivate('true');
                            $item->ImageTraduire->setClickable('true');
                            $item->linkTraduire->setVisible(true);
                            $item->labelTraduire->setVisible(false);
                        }
                    } else {
                        foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                            $item->panelTraduire->setVisible(true);
                            $item->ImageTraduire->setActivate('false');
                            $item->ImageTraduire->setClickable('false');
                        }
                    }
                }

                if (
                    !$consultation->getCurrentUserReadOnly()
                    && Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')
                    && $this->isDeleteConsultation($consultation->getId())
                ) {
                    $this->panelDeleteConsultation->setCssClass('picto-link');
                    $this->ImageDeleteConsultation->setActivate('true');
                    $this->ImageDeleteConsultation->setClickable('true');
                    $this->ImageDeleteConsultation->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                    $this->linkDeleteConsultation->setVisible(true);
                    $this->linkDeleteConsultation->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                    $this->labelDeleteConsultation->setVisible(false);
                } else {
                    $this->ImageDeleteConsultation->setActivate('false');
                    $this->ImageDeleteConsultation->setClickable('false');
                }
                if (Atexo_Module::isEnabled('GestionModelesFormulaire')) {
                    $this->panelFormulaireEtCriteres->setStyle("display:''");
                }

                if (
                    (Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac') || Atexo_CurrentUser::hasHabilitation('ValidationDocumentsRedac'))
                    && Atexo_Module::isEnabled('InterfaceModuleRsem')
                    && $consultation->getRedactionDocumentsRedac()
                    && !$consultation->getCurrentUserReadOnly()
                    && $consultation->getDonneeComplementaireObligatoire()
                ) {
                    $this->panelRedactionPreparation->setCssClass('picto-link action-redac');
                    $this->ImageRedactionPreparation->setActivate('true');
                    $this->ImageRedactionPreparation->setClickable('true');
                    $this->linkRedactionPreparation->setVisible(true);
                    $this->labelRedactionPreparation->setVisible(false);
                } else {
                    $this->ImageRedactionPreparation->setActivate('false');
                    $this->ImageRedactionPreparation->setClickable('false');
                }
            } else {
                ///////////////////////////////////Consultation//////////////////////////////////////////
                if (($depouiablePhaseCons && $consultation->getDatefin() > $dateAjourdhui) || ($etatConsultation == Atexo_Config::getParameter('ETAT_EN_LIGNE'))) {
                    $this->etape1->SetId('etape2');
                    if ($consultation->hasHabilitationModifierApresValidation()) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelModifierConsultation->setCssClass('picto-link');
                            $this->ImageModifierConsultation->setActivate('true');
                            $this->ImageModifierConsultation->setClickable('true');
                            $this->linkModifierConsultation->setVisible(true);
                            $this->labelModifierConsultation->setVisible(false);
                        } else {
                            $this->ImageModifierConsultation->setActivate('false');
                            $this->ImageModifierConsultation->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelPubConsultation->setCssClass('picto-link action-mpe');
                            $this->ImagePubConsultation->setActivate('true');
                            $this->ImagePubConsultation->setClickable('true');
                            $this->linkPubConsultation->setVisible(true);
                            $this->labelPubConsultation->setVisible(false);
                        } else {
                            $this->ImagePubConsultation->setActivate('false');
                            $this->ImagePubConsultation->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreRetraitsConsultation->setCssClass('registre-line');
                            $this->ImageRegistreRetraitsConsultation->setActivate('true');
                            $this->ImageRegistreRetraitsConsultation->setClickable('true');
                            $this->linkRegistreRetraitsConsultation->setVisible(true);
                            $this->labelRegistreRetraitsConsultation->setVisible(false);
                        } else {
                            $this->ImageRegistreRetraitsConsultation->setActivate('false');
                            $this->ImageRegistreRetraitsConsultation->setClickable('false');
                        }
                        $this->linkRegistreRetraitsConsultation->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelQuestionsConsultation->setCssClass('registre-line');
                            $this->ImageQuestionsConsultation->setActivate('true');
                            $this->ImageQuestionsConsultation->setClickable('true');
                            $this->linkQuestionsConsultation->setVisible(true);
                            $this->labelQuestionsConsultation->setVisible(false);
                        } else {
                            $this->ImageQuestionsConsultation->setActivate('false');
                            $this->ImageQuestionsConsultation->setClickable('false');
                        }
                        $this->linkQuestionsConsultation->setText($questionsElectronique . ' + ' . $questionsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreDepot->setCssClass('registre-line');
                            $this->ImagepanelRegistreDepot->setActivate('true');
                            $this->ImagepanelRegistreDepot->setClickable('true');
                            $this->linkRegistreDepot->setVisible(true);
                            $this->labelRegistreDepot->setVisible(false);
                        } else {
                            $this->ImagepanelRegistreDepot->setActivate('false');
                            $this->ImagepanelRegistreDepot->setClickable('false');
                        }
                        $this->linkRegistreDepot->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerSuite->setCssClass('picto-link');
                            $this->ImageCreerSuite->setActivate('true');
                            $this->ImageCreerSuite->setClickable('true');
                            $this->linkCreerSuite->setVisible(true);
                            $this->labelCreerSuite->setVisible(false);
                        } else {
                            $this->ImageCreerSuite->setActivate('false');
                            $this->ImageCreerSuite->setClickable('false');
                        }
                    }
                    if (Atexo_Module::isEnabled('TraduireConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                                $item->panelTraduire->setVisible(true);
                                $item->panelTraduire->setCssClass('picto-link action-mpe');
                                $item->ImageTraduire->setActivate('true');
                                $item->ImageTraduire->setClickable('true');
                                $item->linkTraduire->setVisible(true);
                                $item->labelTraduire->setVisible(false);
                            }
                        } else {
                            foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                                $item->panelTraduire->setVisible(true);
                                $item->ImageTraduire->setActivate('false');
                                $item->ImageTraduire->setClickable('false');
                            }
                        }
                    }

                    if (Atexo_Module::isEnabled('AnnulerConsultation') && !$consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('AnnulerConsultation')) {
                        $this->panelAnnulerConsultation->setCssClass('picto-link action-mpe');
                        $this->ImageAnnulerConsultation->setActivate('true');
                        $this->ImageAnnulerConsultation->setClickable('true');
                        $this->linkAnnulerConsultation->setVisible(true);
                        $this->labelAnnulerConsultation->setVisible(false);
                    } else {
                        $this->ImageAnnulerConsultation->setActivate('false');
                        $this->ImageAnnulerConsultation->setClickable('false');
                    }
                    if (
                        (Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac') || Atexo_CurrentUser::hasHabilitation('ValidationDocumentsRedac'))
                        && Atexo_Module::isEnabled('InterfaceModuleRsem')
                        && $consultation->getRedactionDocumentsRedac() && !$consultation->getCurrentUserReadOnly()
                        && $consultation->getDonneeComplementaireObligatoire()
                    ) {
                        $this->panelRedactionConsultation->setCssClass('picto-link action-redac');
                        $this->ImageRedactionConsultation->setActivate('true');
                        $this->ImageRedactionConsultation->setClickable('true');
                        $this->linkRedactionConsultation->setVisible(true);
                        $this->labelRedactionConsultation->setVisible(false);
                    } else {
                        $this->ImageRedactionConsultation->setActivate('false');
                        $this->ImageRedactionConsultation->setClickable('false');
                    }
                }
                //////////////////////////////Ouverture et analyse///////////////////////////////////////////////
                if (
                    ($depouiablePhaseCons) ||
                    ($etatConsultation == Atexo_Config::getParameter('ETAT_CLOTURE')) ||
                    ($etatConsultation == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION')))
                ) {
                    $this->etape1->SetId('etape3');
                    if ($consultation->hasHabilitationModifierApresValidation()) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelModifierOuverture->setCssClass('picto-link');
                            $this->ImageModifierOuverture->setActivate('true');
                            $this->ImageModifierOuverture->setClickable('true');
                            $this->linkModifierOuverture->setVisible(true);
                            $this->labelModifierOuverture->setVisible(false);
                        } else {
                            $this->ImageModifierOuverture->setActivate('false');
                            $this->ImageModifierOuverture->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelPubOuverture->setCssClass('picto-link action-mpe');
                            $this->ImagePubOuverture->setActivate('true');
                            $this->ImagePubOuverture->setClickable('true');
                            $this->linkPubOuverture->setVisible(true);
                            $this->labelPubOuverture->setVisible(false);
                        } else {
                            $this->ImagePubOuverture->setActivate('false');
                            $this->ImagePubOuverture->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelAccesRepOuverture->setCssClass('picto-link action-mpe');
                            $this->imageAccesRepOuverture->setActivate('true');
                            $this->imageAccesRepOuverture->setClickable('true');
                            $this->linkAccesRepOuverture->setVisible(true);
                            $this->labelAccesRepOuverture->setVisible(false);
                        } else {
                            $this->imageAccesRepOuverture->setActivate('false');
                            $this->imageAccesRepOuverture->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreRetraisOuverture->setCssClass('registre-line');
                            $this->imageRegistreRetraisOuverture->setActivate('true');
                            $this->imageRegistreRetraisOuverture->setClickable('true');
                            $this->linkRegistreRetraisOuverture->setVisible(true);
                            $this->labelRegistreRetraisOuverture->setVisible(false);
                        } else {
                            $this->imageRegistreRetraisOuverture->setActivate('false');
                            $this->imageRegistreRetraisOuverture->setClickable('false');
                        }
                        $this->linkRegistreRetraisOuverture->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelQuestionsOuverture->setCssClass('registre-line');
                            $this->ImageQuestionsOuverture->setActivate('true');
                            $this->ImageQuestionsOuverture->setClickable('true');
                            $this->linkQuestionsOuverture->setVisible(true);
                            $this->labelQuestionsOuverture->setVisible(false);
                        } else {
                            $this->ImageQuestionsOuverture->setActivate('false');
                            $this->ImageQuestionsOuverture->setClickable('false');
                        }

                        $this->linkQuestionsOuverture->setText($questionsElectronique . ' + ' . $questionsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreDepotOuverture->setCssClass('registre-line');
                            $this->ImageRegistreDepotOuverture->setActivate('true');
                            $this->ImageRegistreDepotOuverture->setClickable('true');
                            $this->linkRegistreDepotOuverture->setVisible(true);
                            $this->labelRegistreDepotOuverture->setVisible(false);
                        } else {
                            $this->ImageRegistreDepotOuverture->setActivate('false');
                            $this->ImageRegistreDepotOuverture->setClickable('false');
                        }
                        $this->linkRegistreDepotOuverture->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('GererEncheres')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelOrganiserOuverture->setCssClass('registre-line');
                            $this->ImageOrganiserOuverture->setActivate('true');
                            $this->ImageOrganiserOuverture->setClickable('true');
                            $this->linkOrganiserOuverture->setVisible(true);
                            $this->labelOrganiserOuverture->setVisible(false);
                        } else {
                            $this->ImageOrganiserOuverture->setActivate('false');
                            $this->ImageOrganiserOuverture->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerSuiteOuverture->setCssClass('picto-link');
                            $this->ImageCreerSuiteOuverture->setActivate('true');
                            $this->ImageCreerSuiteOuverture->setClickable('true');
                            $this->linkCreerSuiteOuverture->setVisible(true);
                            $this->labelCreerSuiteOuverture->setVisible(false);
                        } else {
                            $this->ImageCreerSuiteOuverture->setActivate('false');
                            $this->ImageCreerSuiteOuverture->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('TelechargementUnitairePlisChiffres')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelTelechargerPli->setCssClass('picto-link action-mpe');
                            $this->ImageTelechargerPli->setActivate('true');
                            $this->ImageTelechargerPli->setClickable('true');
                            $this->linkTelechargerPli->setVisible(true);
                            $this->labelTelechargerPli->setVisible(false);
                        } else {
                            $this->ImageTelechargerPli->setActivate('false');
                            $this->ImageTelechargerPli->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('ResultatAnalyse')) {
                        $this->panelResultatAnalyseOuverture->setVisible(true);
                        $this->imageResultatAnalyseOuverture->setActivate('true');
                        $this->imageResultatAnalyseOuverture->setClickable('true');
                        $this->linkResultatAnalyseOuverture->setVisible(true);
                        $this->labelResultatAnalyseOuverture->setVisible(false);
                    } else {
                        $this->panelResultatAnalyseOuverture->setVisible(false);
                    }

                    if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv') && Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerExtraitPv->setCssClass('picto-link action-mpe');
                            $this->ImageCreerExtraitPv->setActivate('true');
                            $this->ImageCreerExtraitPv->setClickable('true');
                            $this->linkCreerExtraitPv->setVisible(true);
                            $this->labelCreerExtraitPv->setVisible(false);
                        } else {
                            $this->ImageCreerExtraitPv->setActivate('false');
                            $this->ImageCreerExtraitPv->setClickable('false');
                        }
                    } else {
                        $this->panelCreerExtraitPv->setVisible(false);
                    }
                    if (Atexo_Module::isEnabled('AnnulerConsultation') && !$consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('AnnulerConsultation')) {
                        $this->panelAnnulerConsultationOuverture->setCssClass('picto-link action-mpe');
                        $this->ImageAnnulerConsultationOuverture->setActivate('true');
                        $this->ImageAnnulerConsultationOuverture->setClickable('true');
                        $this->linkAnnulerConsultationOuverture->setVisible(true);
                        $this->labelAnnulerConsultationOuverture->setVisible(false);
                    } else {
                        $this->ImageAnnulerConsultationOuverture->setActivate('false');
                        $this->ImageAnnulerConsultationOuverture->setClickable('false');
                    }
                }

                ////////////////////////////////Decision/////////////////////////////////////////////
                if (
                    ($depouiablePhaseCons) ||
                    ($etatConsultation == Atexo_Config::getParameter('ETAT_DEPOUILLE') ||
                    ($etatConsultation == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION')))) ||
                    $etatConsultation == Atexo_Config::getParameter('ETAT_ARCHIVE') ||
                    $etatConsultation == Atexo_Config::getParameter('ETAT_DEPOUILLE')
                ) {
                    $this->etape1->SetId('etape4');
                    /*Evoultion mise e la disposition des pieces marches*/
                    if (Atexo_Module::isEnabled('MiseDispositionPiecesMarche') && Atexo_CurrentUser::hasHabilitation('GestionMiseDispositionPiecesMarche')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelMiseDisposition->setCssClass('picto-link action-mpe');
                            $this->panelMiseDisposition->setVisible(true);
                            $this->ImagePiecesMarches->setActivate('true');
                            $this->ImagePiecesMarches->setClickable('true');
                            $this->linkPieceMarches->setVisible(true);
                            $this->labelPieceMarche->setVisible(false);
                        } else {
                            $this->ImagePiecesMarches->setActivate('false');
                            $this->ImagePiecesMarches->setClickable('false');
                            $this->linkPieceMarches->setVisible(false);
                            $this->labelPieceMarche->setVisible(true);
                        }
                    } else {
                        $this->panelMiseDisposition->setVisible(false);
                    }
                    if ($consultation->hasHabilitationModifierApresValidation()) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelModiferDecision->setCssClass('picto-link');
                            $this->ImageModifierDecision->setActivate('true');
                            $this->ImageModifierDecision->setClickable('true');
                            $this->linkModifierDecision->setVisible(true);
                            $this->labelModifierDecision->setVisible(false);
                        } else {
                            $this->ImageModifierDecision->setActivate('false');
                            $this->ImageModifierDecision->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelPubDecision->setCssClass('picto-link action-mpe');
                            $this->ImagePubDecision->setActivate('true');
                            $this->ImagePubDecision->setClickable('true');
                            $this->linkPubDecision->setVisible(true);
                            $this->labelPubDecision->setVisible(false);
                        } else {
                            $this->ImagePubDecision->setActivate('false');
                            $this->ImagePubDecision->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelStatutChorus->setCssClass('picto-link action-mpe');
                            $this->ImageStatutChorus->setActivate('true');
                            $this->ImageStatutChorus->setClickable('true');
                            $this->linkStatutChorus->setVisible(true);
                            $this->labelStatutChorus->setVisible(false);
                        } else {
                            $this->ImageStatutChorus->setActivate('false');
                            $this->ImageStatutChorus->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelAttribuerDecision->setCssClass('picto-link action-mpe');
                            $this->imageAttriberDecision->setActivate('true');
                            $this->linkAttriberDecision->setVisible(true);
                            $this->labelAttriberDecision->setVisible(false);
                        } else {
                            $this->imageAttriberDecision->setActivate('false');
                            $this->linkAttriberDecision->setVisible('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreRetaitsDecision->setCssClass('registre-line');
                            $this->imageRegitreRetaitsDecision->setActivate('true');
                            $this->imageRegitreRetaitsDecision->setClickable('true');
                            $this->linkRegitreRetaitsDecision->setVisible(true);
                            $this->labelRegitreRetaitsDecision->setVisible(false);
                        } else {
                            $this->imageRegitreRetaitsDecision->setActivate('false');
                            $this->imageRegitreRetaitsDecision->setClickable('false');
                        }
                        $this->linkRegitreRetaitsDecision->setText($registreRetaitsElectronique . ' + ' . $registreRetaitsPapier);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelQuestionDecision->setCssClass('registre-line');
                            $this->imageQuestionDecision->setActivate('true');
                            $this->imageQuestionDecision->setClickable('true');
                            $this->linkQuestionDecision->setVisible(true);
                            $this->labelQuestionDecision->setVisible(false);
                        } else {
                            $this->imageQuestionDecision->setActivate('false');
                            $this->imageQuestionDecision->setClickable('false');
                        }
                        $this->linkQuestionDecision->setText($questionsElectronique . ' + ' . $questionsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRegistreDepotDecision->setCssClass('registre-line');
                            $this->imageRegistreDepotDecision->setActivate('true');
                            $this->imageRegistreDepotDecision->setClickable('true');
                            $this->linkRegistreDepotDecision->setVisible(true);
                            $this->labelRegistreDepotDecision->setVisible(false);
                        } else {
                            $this->imageRegistreDepotDecision->setActivate('false');
                            $this->imageRegistreDepotDecision->setClickable('false');
                        }
                        $this->linkRegistreDepotDecision->setText($registreDepotsElectroniques . ' + ' . $registreDepotsPapiers);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelDeclarerInfrucuteuxDecision->setCssClass('picto-link action-mpe');
                            $this->imageDeclarerInfructeuxDecision->setActivate('true');
                            $this->imageDeclarerInfructeuxDecision->setClickable('true');
                            $this->linkDeclarerInfrucuteuxDecision->setVisible(true);
                            $this->labelDeclarerInfrucuteuxDecision->setVisible(false);
                        } else {
                            $this->imageDeclarerInfructeuxDecision->setActivate('false');
                            $this->linkDeclarerInfrucuteuxDecision->setVisible('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelSansSuiteDecision->setCssClass('picto-link action-mpe');
                            $this->imageSansSuiteDecision->setActivate('true');
                            $this->linkSansSuiteDecision->setVisible(true);
                            $this->labelSansSuiteDecision->setVisible(false);
                        } else {
                            $this->imageSansSuiteDecision->setActivate('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerSuiteDecision->setCssClass('picto-link');
                            $this->imageCreerSuiteDecision->setActivate('true');
                            $this->imageCreerSuiteDecision->setClickable('true');
                            $this->linkCreerSuiteDecision->setVisible(true);
                            $this->labelCreerSuiteDecision->setVisible(false);
                        } else {
                            $this->imageCreerSuiteDecision->setActivate('false');
                            $this->imageCreerSuiteDecision->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('TelechargementUnitairePlisChiffres')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelTelechargerPliDecision->setCssClass('picto-link action-mpe');
                            $this->ImageTelechargerPliDecision->setActivate('true');
                            $this->ImageTelechargerPliDecision->setClickable('true');
                            $this->linkTelechargerPliDecision->setVisible(true);
                            $this->labelTelechargerPliDecision->setVisible(false);
                        } else {
                            $this->ImageTelechargerPliDecision->setActivate('false');
                            $this->ImageTelechargerPliDecision->setClickable('false');
                        }
                    }

                    if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerAnnonceAttributionDecision->setVisible(true);
                            $this->imageAnnonceAttributionDecision->setActivate('true');
                            $this->imageAnnonceAttributionDecision->setClickable('true');
                            $this->linkAnnonceAttributionDecision->setVisible(true);
                            $this->labelAnnonceAttributionDecision->setVisible(false);
                        } else {
                            $this->imageAnnonceAttributionDecision->setActivate('false');
                            $this->imageAnnonceAttributionDecision->setClickable('false');
                            $this->linkAnnonceAttributionDecision->setVisible(false);
                            $this->labelAnnonceAttributionDecision->setVisible(true);
                        }
                    } else {
                        $this->panelCreerAnnonceAttributionDecision->setVisible(false);
                    }

                    if (Atexo_CurrentUser::hasHabilitation('ResultatAnalyse')) {
                        $this->panelResultatAnalyseDecision->setVisible(true);
                        $this->imageResultatAnalyseDecision->setActivate('true');
                        $this->imageResultatAnalyseDecision->setClickable('true');
                        $this->linkResultatAnalyseDecision->setVisible(true);
                        $this->labelResultatAnalyseDecision->setVisible(false);
                    } else {
                        $this->panelResultatAnalyseDecision->setVisible(false);
                    }

                    if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv') && Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelCreerExtraitPvDecision->setVisible(true);
                            $this->ImageCreerExtraitPvDecision->setActivate('true');
                            $this->ImageCreerExtraitPvDecision->setClickable('true');
                            $this->linkCreerExtraitPvDecision->setVisible(true);
                            $this->labelCreerExtraitPvDecision->setVisible(false);
                        } else {
                            $this->ImageCreerExtraitPvDecision->setActivate('false');
                            $this->ImageCreerExtraitPvDecision->setClickable('false');
                            $this->linkCreerExtraitPvDecision->setVisible(false);
                            $this->labelCreerExtraitPvDecision->setVisible(true);
                        }
                    } else {
                        $this->panelCreerExtraitPvDecision->setVisible(false);
                    }

                    if (Atexo_Module::isEnabled('AutreAnnonceRapportAchevement') && Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelRapportAchevementDecision->setVisible(true);
                            $this->ImageRapportAchevementDecision->setActivate('true');
                            $this->ImageRapportAchevementDecision->setClickable('true');
                            $this->linkRapportAchevementDecision->setVisible(true);
                            $this->labelRapportAchevementDecision->setVisible(false);
                        } else {
                            $this->ImageRapportAchevementDecision->setActivate('false');
                            $this->ImageRapportAchevementDecision->setClickable('false');
                            $this->linkRapportAchevementDecision->setVisible(false);
                            $this->labelRapportAchevementDecision->setVisible(true);
                        }
                    } else {
                        $this->panelRapportAchevementDecision->setVisible(false);
                    }

                    if (Atexo_Module::isEnabled('AutreAnnonceDecisionResiliation') && Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')) {
                        if (!$consultation->getCurrentUserReadOnly()) {
                            $this->panelDecisionResiliation->setVisible(true);
                            $this->ImageDecisionResiliation->setActivate('true');
                            $this->ImageDecisionResiliation->setClickable('true');
                            $this->linkDecisionResiliation->setVisible(true);
                            $this->labelDecisionResiliation->setVisible(false);
                        } else {
                            $this->ImageDecisionResiliation->setActivate('false');
                            $this->ImageDecisionResiliation->setClickable('false');
                            $this->linkDecisionResiliation->setVisible(false);
                            $this->labelDecisionResiliation->setVisible(true);
                        }
                    } else {
                        $this->panelDecisionResiliation->setVisible(false);
                    }

                    if (Atexo_Module::isEnabled('AnnulerConsultation') && !$consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('AnnulerConsultation')) {
                        $this->panelAnnulerConsultationDecision->setCssClass('picto-link action-mpe');
                        $this->ImageAnnulerConsultationDecision->setActivate('true');
                        $this->ImageAnnulerConsultationDecision->setClickable('true');
                        $this->linkAnnulerConsultationDecision->setVisible(true);
                        $this->labelAnnulerConsultationDecision->setVisible(false);
                    } else {
                        $this->ImageAnnulerConsultationDecision->setActivate('false');
                        $this->ImageAnnulerConsultationDecision->setClickable('false');
                    }
                }
                if ($depouiablePhaseCons == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1') && $consultation->getDatefin() > $dateAjourdhui) {
                    $this->etape1->SetId('etape234');
                } elseif (
                    ($etatConsultation == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION'))) ||
                        ($depouiablePhaseCons == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1') && $consultation->getDatefin() <= $dateAjourdhui)
                ) {
                    $this->etape1->SetId('etape34');
                }

                if ($this->isArchivedCons()) {
                    $this->etape1->SetId('etapeNone');
                }
            }
        }
    }

    public function validateWithoutChiffrement()
    {
        $consultationId = $this->_consultation->getId();
        $atexoValidation = new Atexo_Consultation_Validation($consultationId, Atexo_CurrentUser::getOrganismAcronym());
        $idUser = Atexo_CurrentUser::getIdAgentConnected();
        $atexoValidation->validateWithoutChiffrement($idUser);
        $this->response->redirect("index.php?page=Agent.TableauDeBord&id=$consultationId");
    }

    public function approve()
    {
        $consultationId = $this->_consultation->getId();

        $atexoValidation = new Atexo_Consultation_Validation($consultationId, Atexo_CurrentUser::getOrganismAcronym());
        $idUser = Atexo_CurrentUser::getIdAgentConnected();
        $atexoValidation->approve($idUser);
        $this->response->redirect("index.php?page=Agent.TableauDeBord&id=$consultationId");
    }

    public function redirectToPublicite()
    {
        $this->response->redirect(MenuGaucheAgent::activateMenu('publication'));
    }

    public function redirectToApprouverValider()
    {
        $this->response->redirect('index.php?page=Agent.ValiderConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function redirectToGetionFormulairesEtCriteres()
    {
        $this->response->redirect('index.php?page=Agent.GestionFormulairesEtCriteres&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function redirectToCreerSuite()
    {
        //$this->response->redirect(MenuGaucheAgent::activateMenu('show_suite_cons'));
        $this->response->redirect('/agent/consultation/creation?consultationParentId=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function redirectToTelechargerPli()
    {
        $this->response->redirect('index.php?page=Agent.ouvertureEtAnalyse&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&telechargerOffres');
    }

    public function redirectToEnchere()
    {
        $this->response->redirect('index.php?page=Agent.GererEnchere&id=' . base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    public function redirectTo($sender, $param)
    {
        $url = $param->CommandName;
        $this->response->redirect($url);
    }

    public function getConsultationObject()
    {
        return $this->_consultation;
    }

    public function getNombreLots($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $lots = (new Atexo_Consultation())->getAllLots($connexionCom, $consultationId, $organisme);
        $nbrLots = is_countable($lots) ? count($lots) : 0;
        if ($nbrLots) {
            return $nbrLots;
        }
    }

    /**
     * generation de fichier odt de detail de la consultation.
     */
    public function printConsultationOdt()
    {
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        if ($this->_consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            $tableKeys['acDefine'] = ' ' . Prado::Localize('DEFINE_ACCES_RESTREINT') . ' - ' .
                Prado::Localize('DEFINE_CODE_ACCES') . ' : ' .
                $this->_consultation->getCodeProcedure();
        } else {
            $tableKeys['acDefine'] = ' ' . Prado::Localize('TEXT_ACCES_PUBLIC');
        }

        if ('1' == $this->_consultation->getReponseObligatoire()) {
            $tableKeys['RepElectronique'] = Prado::Localize('DEFINE_OBLIGATOIRE');
        } elseif ('1' == $this->_consultation->getAutoriserReponseElectronique()) {
            $tableKeys['RepElectronique'] = Prado::Localize('DEFINE_AUTORISEE');
        } else {
            $tableKeys['RepElectronique'] = Prado::Localize('DEFINE_REFUSEE');
        }

        if ('1' == $this->_consultation->getModeOuvertureReponse()) {
            $tableKeys['modeOuvertureRep'] = Prado::Localize('DEFINE_PAR_REPONSE');
        } else {
            $tableKeys['modeOuvertureRep'] = Prado::Localize('DEFINE_PAR_DOSSIER');
        }

        if ((1 == $this->_consultation->getSignatureOffre()) && (1 == $this->_consultation->getAutoriserReponseElectronique())) {
            $tableKeys['signatureElectronique'] = Prado::Localize('DEFINE_REQUISE');
        } elseif (
            (2 == $this->_consultation->getSignatureOffre())
            && (1 == $this->_consultation->getAutoriserReponseElectronique())
        ) {
            $tableKeys['signatureElectronique'] = Prado::Localize('DEFINE_AUTORISEE');
        } else {
            $tableKeys['signatureElectronique'] = Prado::Localize('DEFINE_NON_REQUISE');
        }

        if ((1 == $this->_consultation->getChiffrementOffre()) && (1 == $this->_consultation->getAutoriserReponseElectronique())) {
            $tableKeys['chiffrementPlis'] = Prado::Localize('DEFINE_OUI');
        } else {
            $tableKeys['chiffrementPlis'] = Prado::Localize('DEFINE_NON');
        }
        if (1 == $this->_consultation->getAutoriserReponseElectronique()) {
            if ('1' == $this->_consultation->getEnvCandidature()) {
                $tableKeys['<!--envCandidature-->'] = '<text:p text:style-name="P7">' . Prado::localize('DEFINE_ENVELOPPE_CONDIDATURE') . '</text:p>';
            } else {
                $tableKeys['<!--envCandidature-->'] = '';
            }

            if ('1' == $this->_consultation->getEnvOffre()) {
                $tableKeys['<!--envOffre-->'] = '<text:p text:style-name="P7">' . Prado::localize('DEFINE_ENVELOPPE_OFFRE') .
                    ' - ' . Prado::localize('DEFINE_ENVELOPPE_UNIQUE') .
                    '</text:p>';
            }
            if ($this->_consultation->getEnvOffre() >= 2) {
                $lots = $this->_consultation->getAllLots();
                $tableKeys['<!--envOffre-->'] = '<text:p text:style-name="P7">' . Prado::localize('DEFINE_ENVELOPPE_OFFRE') . ' - ' .
                    Prado::localize('DEFINE_ENVELOPPE_PAR_LOT') . ' - ' . Prado::localize('DEFINE_NOMBRE_LOT') . ' : ' .
                    (is_countable($lots) ? count($lots) : 0) .
                    '</text:p>';
            }
            if ('0' == $this->_consultation->getEnvOffre()) {
                $tableKeys['<!--envOffre-->'] = '';
            }
            if ('1' == $this->_consultation->getEnvAnonymat()) {
                $tableKeys['<!--envAnonymat-->'] = '<text:p text:style-name="P7">' . Prado::localize('DEFINE_TROISIEME_ENVELOPPE') . '</text:p>';
            } else {
                $tableKeys['<!--envAnonymat-->'] = '';
            }
        }
        //code CPV
        if (Atexo_Module::isEnabled('DonneesComplementaires') && $this->_consultation->getDonneeComplementaireObligatoire()) {
            $styleColonne = 'P7';
        } else {
            $styleColonne = 'P5';
        }
        if (Atexo_Module::isEnabled('codeCpv')) {
            $this->addCodeCpv($this->_consultation, $tableKeys, $styleColonne);
        }
        //Constitution dossier de reponse
        $this->getConstitutionDossier($this->_consultation, $tableKeys, $styleColonne);

        // categorie principale
        if ($this->_consultation->getCategorie()) {
            if ('1' == $this->_consultation->getCategorie()) {
                $tableKeys['categorie'] = Prado::localize('DEFINE_TRAVAUX');
            } elseif ('2' == $this->_consultation->getCategorie()) {
                $tableKeys['categorie'] = Prado::localize('DEFINE_FOURNITURES');
            } elseif ('3' == $this->_consultation->getCategorie()) {
                $tableKeys['categorie'] = Prado::localize('DEFINE_SERVICES');
            }
        }
        $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DETAIL_CONSULTATION');
        if (Atexo_Module::isEnabled('DonneesComplementaires') && $this->_consultation->getDonneeComplementaireObligatoire()) {
            //Bloc donnees complementaires
            $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DETAIL_CONSULTATION_AVEC_DONNEES_COMPLEMENTAIRES');
            $this->AddDonneesComplementaires($this->_consultation, $tableKeys, $impr);
        }
        $consSammury = $this->arrayConsultationSummary();
        $tableKeys = array_merge($consSammury, $tableKeys);
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }

    /**
     * telecharger la verstion pdf de detail de la consultation.
     */
    public function printConsultationPdf($sender, $param)
    {
        $odtFilePath = $this->printConsultationOdt();
        if ($odtFilePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DETAIL_CONSULTATION') . '.pdf', $pdfContent);
        }
    }

    /**
     * telecharger la verstion rtf de detail de la consultation.
     */
    public function printConsultationRtf($sender, $param)
    {
        $odtFilePath = $this->printConsultationOdt();
        if ($odtFilePath) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DETAIL_CONSULTATION') . '.rtf', $rtfContent);
        }
    }

    /**
     * generer le fichier odt de Depouillement de la consultation.
     */
    public function printDepouillementConsOdt($organisme = null)
    {
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        if ('0' != $this->_consultation->getEnvCandidature()) {
            $tableKeys['<!--lineContentCandidature-->'] = $impr->imprimerTableauCandidature($this->_consultation, $organisme);
        }
        if ('0' != $this->_consultation->getEnvOffre()) {
            $tableKeys['<!--lineContentOffres-->'] = $impr->imprimerTableauxOffres($this->_consultation, $organisme);
        }
        if ('0' != $this->_consultation->getEnvAnonymat()) {
            $tableKeys['<!--lineContentAnonymat-->'] = $impr->imprimerTableauxAnonymat($this->_consultation, $organisme);
        }
        $consSammury = $this->arrayConsultationSummary($organisme);
        $tableKeys = array_merge($consSammury, $tableKeys);
        $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION');
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }

    /**
     * telecharger la verstion rtf de Depouillement de la consultation.
     */
    public function printDepouillementConsPdf($sender, $param)
    {
        $odtFilePath = $this->printDepouillementConsOdt();
        if ($odtFilePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DEPOUILLEMENT_N_LOTS_CONSULTATION') . '.pdf', $pdfContent);
        }
    }

    /**
     * telecharger la verstion rtf de Depouillement de la consultation.
     */
    public function printDepouillementConsRtf($sender, $param)
    {
        $odtFilePath = $this->printDepouillementConsOdt();
        if ($odtFilePath) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DEPOUILLEMENT_N_LOTS_CONSULTATION') . '.rtf', $rtfContent);
        }
    }

    /**
     * retourne un tableau d'Informations complementaires de la consultation.
     */
    public function arrayConsultationSummary($org = null)
    {
        $tableKeys = [];
        if (!$org) {
            $org = Atexo_CurrentUser::getOrganismAcronym();
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
        $tableKeys['entitePublique'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg()));
        $tableKeys['entiteAchat'] = Atexo_Util::replaceSpecialCharacters(
            Atexo_Util::escapeXmlChars(Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceId(), $org))
        );

        if (Atexo_Module::isEnabled('NumeroProjetAchat')) {
            $numeroProjetAchat = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($this->_consultation->getCodeExterne()));
            $labelNumeroProjetAchat = prado::localize('DEFINE_FULL_NUMERO_PROJET_ACHAT') . ' : ';
            $tableKeys['BALISE_TO_REPLACE_FOR_NUMERO_PROJET_ACHAT'] = '<text:p text:style-name="P5">' . $labelNumeroProjetAchat . $numeroProjetAchat . '</text:p>';
        } else {
            $tableKeys['BALISE_TO_REPLACE_FOR_NUMERO_PROJET_ACHAT'] = '';
        }

        $tableKeys['refConsultation'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($this->_consultation->getReferenceUtilisateur()));
        $tableKeys['intituleConsultation'] = Atexo_Util::replaceSpecialCharacters(
            Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($this->_consultation->getIntitule()))
        );
        $tableKeys['objetConsultation'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($this->_consultation->getObjet())));
        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($this->_consultation->getIdTypeAvis());
        if ($typeAvis) {
            $tableKeys['typeAnnonce'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($typeAvis->getIntituleAvis()));
        }
        $libelleProcedure = str_replace(
            '&#8804;',
            Prado::localize('MAPA_INF_OU_EGALE'),
            (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->_consultation->getIdTypeProcedureOrg(), false, $org)
        );
        $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
        $tableKeys['typeProcedure'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($libelleProcedure));
        $tableKeys['dateRemisePlis'] = Atexo_Util::iso2frnDateTime($this->_consultation->getDatefin());
        $dateMiseEnLigne = Atexo_Util::iso2frnDateTime(Atexo_Consultation::getDateMiseEnLigne($this->_consultation));
        if ($dateMiseEnLigne) {
            $tableKeys['dateMiseLigne'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($dateMiseEnLigne));
        } else {
            $tableKeys['dateMiseLigne'] = '-';
        }
        if (
            Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())
            && (new Atexo_EntityPurchase())->getEtatActivationFuseauHoraire(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism())
        ) {
            $scriptXmlFuseauHoraire = '';
            //Date heure limite remise des plis heure locale
            if ('0000-00-00 00:00:00' != $this->_consultation->getDatefinLocale() && '' != $this->_consultation->getDatefinLocale()) {
                $scriptXmlFuseauHoraire .= '<text:p text:style-name="P5">' . Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE') .
                    ' : ' . Atexo_Util::iso2frnDateTime(substr($this->_consultation->getDatefinLocale(), 0, 16)) .
                    '</text:p>';
            }
            //Lieu de residence
            if ('' != $this->_consultation->getLieuResidence()) {
                $scriptXmlFuseauHoraire .= '<text:p text:style-name="P5">' . Prado::localize('DEFINE_LIEU_RESIDENCE') .
                    ' : ' . $this->_consultation->getLieuResidence() . '</text:p>';
            }
            $tableKeys['<!--dateHeureLocaleLieuResidence-->'] = $scriptXmlFuseauHoraire;
        }

        $lots = $this->_consultation->getAllLots();
        $scriptXmlLots = '';
        if ($this->_consultation->getAlloti()) {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_OUI');
            foreach ($lots as $oneLot) {
                //<text:p text:style-name="P7">lot</text:p>
                $scriptXmlLots .= '<text:p text:style-name="P7">Lot :' . $oneLot->getLot() . ' ' .
                    Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($oneLot->getDescription())) .
                    '</text:p>';
            }
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        } else {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_NON');
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        }

        return $tableKeys;
    }

    public function deleteConsultation()
    {
        try {
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            if (!$this->_consultation->isConsultationAval()) {
                $consultation = new Atexo_Consultation();
                $this->deleteClauses($consultationId);
                $consultation->deleteConsultation($consultationId, $organisme);
            } else {
                $this->response->redirect('index.php?page=Agent.DetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($consultationId) . '&errorDeleteConsultation');
            }
        } catch (Exception $e) {
            Prado::log('Erreur DetailConsultation.php' . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, 'ATEXO');
        }
        $this->response->redirect('index.php?page=Agent.AccueilAgentAuthentifie&deleted');
    }

    public function redirectToAnnonceAttribution()
    {
        $this->response->redirect('index.php?page=Agent.FormulaireAnnonce&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&newAttribution');
    }

    public function redirectToCreateExtraitPv()
    {
        $this->response->redirect('index.php?page=Agent.FormulaireAnnonce&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&newExtraitPv');
    }

    public function redirectToCreateExtraitPvDecision()
    {
        $this->response->redirect('index.php?page=Agent.FormulaireAnnonce&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&newExtraitPv');
    }

    public function redirectToRapportAchevementDecision()
    {
        $this->response->redirect('index.php?page=Agent.FormulaireAnnonce&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&newRapportAchevement');
    }

    public function redirectToDecisionResiliation()
    {
        $this->response->redirect('index.php?page=Agent.FormulaireAnnonce&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&newDecisionResiliation');
    }

    public function isArchivedCons()
    {
        if ($this->_consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Verifie si l'agent connecte a l'habilitation modifier une consultation
     * @return true s'il a l'habilitation, non sinon
     */
    public function hasHabilitationModifierConsultation()
    {
        if ($this->_consultation->getCurrentUserReadOnly()) {
            return false;
        }
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE')) {
            return Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation');
        } else {
            return $this->_consultation->hasHabilitationModifierApresValidation();
        }
    }

    public function annulerConsultation()
    {
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->response->redirect('index.php?page=Agent.DecisionAnnulation&id=' . $consultationId);
    }

    public function redactionIsVisible()
    {
        if (
            (Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac') || Atexo_CurrentUser::hasHabilitation('ValidationDocumentsRedac'))
                      && Atexo_Module::isEnabled('InterfaceModuleRsem') && $this->_consultation->getRedactionDocumentsRedac() && !$this->_consultation->getCurrentUserReadOnly()
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *Fonction permet d'ajout le code dans le rapport de detail consultation.
     */
    public function addCodeCpv(&$consultation, &$tableKeys, $styleColone)
    {
        // code principal
        if ($consultation->getCodeCpv1()) {
            $tableKeys['<!--codeCpvPrincipal-->'] = '<text:p text:style-name="' . $styleColone . '">' . Prado::localize('TEXT_CODE_CPV') . ' : ' . $consultation->getCodeCpv1() .
            Prado::localize('DEFINE_CODE_PRINCIPAL') . '</text:p>';
        } else {
            $tableKeys['<!--codeCpvPrincipal-->'] = '';
        }
        //code secondaire
        if ($consultation->getCodeCpv2()) {
            $arrayCodeSecondaire = explode('#', $consultation->getCodeCpv2());
            foreach ($arrayCodeSecondaire as $value) {
                if ($value) {
                    $tableKeys['<!--codeCpvSecondaire-->'] .= '<text:p text:style-name="' . $styleColone . '">' .
                        $value . ' ' . Prado::localize('DEFINE_CODE_SECONDAIRE') . '</text:p>';
                }
            }
        } else {
            $tableKeys['<!--codeCpvSecondaire-->'] = '';
        }
    }

    /*
    * Fonction permet d'ajouter les donnees complementaires
    * @param un objet Consultation
    * @param table de cles (passage pas reference)
    * @param Objet Impression Odt
    */
    public function AddDonneesComplementaires($consultation, &$tableKeys, $impr)
    {
        // les valeurs par defauts de Marche / Tranche / Marche reconductible
        $tableKeys['Marchereconductible'] = Prado::Localize('DEFINE_NON');
        $tableKeys['tranche'] = Prado::Localize('DEFINE_NON');
        $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_NON');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $donneeComplementaire = CommonTDonneeComplementairePeer::retrieveByPK($consultation->getIdDonneeComplementaire(), $connexion);
        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
            //Ajout de la duree de marche
            $this->getDureemarche($donneeComplementaire, $tableKeys);
        }
        // le Cas d'une consultatio Alloti
        if ('1' == $consultation->getAlloti()) {
            $lots = $consultation->getAllLots();
            if ($lots) {
                $hasTranche = false;
                foreach ($lots as $lot) {
                    if ($lot->hasTranches()) {
                        $hasTranche = true;
                    }
                }
                if ($hasTranche) {
                    $tableKeys['tranche'] = Prado::Localize('DEFINE_OUI');
                    $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->ajoutTableauDonnesComplementaireAllotiAvecTranche($tableKeys, $lots);
                } else {
                    $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->ajoutTableauDonnesComplementaireAllotiSansTranche($tableKeys, $lots);
                }
            } else {
                $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->getEnteteTableauAllotiSansTranche(true);
            }
        } else {
            //  le Cas d'une consultatio Non Alloti
            $donneeComplementaire = CommonTDonneeComplementairePeer::retrieveByPK($consultation->getIdDonneeComplementaire(), $connexion);
            //marche reconductible
            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                if ($donneeComplementaire->getReconductible() == Atexo_Config::getParameter('DONNEE_COMPLEMENTAIRE_MARCHE_RECONDUCTIBLE')) {
                    $tableKeys['Marchereconductible'] = Prado::Localize('DEFINE_OUI');
                }

                if ($donneeComplementaire->getIdFormePrix()) {
                    $formePrix = $donneeComplementaire->getFormePrix();
                    if ('bon_commande' == $formePrix->getModalite()) {
                        $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                    }
                    $tableKeys['tranche'] = Prado::Localize('DEFINE_NON');
                    $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->ajoutTableauDonnesComplementaireNonAllotiSansTranche($donneeComplementaire, $tableKeys);
                } elseif (!$donneeComplementaire->getIdFormePrix()) {
                    $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->ajoutTableauDonnesComplementaireNonAllotiAvecTranche($donneeComplementaire, $tableKeys);
                }
            } else {
                $tableKeys['<!-- lineDonneesComplementaire -->'] = $impr->getEnteteNonAllotiSansTranche(true);
            }
        }
    }

    /*
     * Fonction permet d'ajouter la duree de marche dans le rapport
     * @param un objet Donnees complementaire
     * @param table de cles (passage pas reference)
     */
    public function getDureemarche($donneeComplementaire, &$tableKeys)
    {
        //Duree
        if ($donneeComplementaire->getIdDureeDelaiDescription() == Atexo_Config::getParameter('DUREE_MARCHE')) {
            if ($donneeComplementaire->getDureeMarche()) {
                if ($donneeComplementaire->getIdChoixMoisJour() == Atexo_Config::getParameter('DUREE_JOURS')) {
                    $tableKeys['<!-- lineDureeMarche -->'] = '<text:p text:style-name="P7">' . Prado::Localize('DEFINE_DUREE_MARCHE') . ' : ' .
                    $donneeComplementaire->getDureeMarche() . ' ' . Prado::Localize('TEXT_JOURS') . '</text:p>';
                } elseif ($donneeComplementaire->getIdChoixMoisJour() == Atexo_Config::getParameter('DUREE_MOIS')) {
                    $tableKeys['<!-- lineDureeMarche -->'] = '<text:p text:style-name="P7">' . Prado::Localize('DEFINE_DUREE_MARCHE') . ' : ' .
                    $donneeComplementaire->getDureeMarche() . ' ' . Prado::Localize('FCSP_MOIS') . '</text:p>';
                }
            }
        } elseif ($donneeComplementaire->getIdDureeDelaiDescription() == Atexo_Config::getParameter('DELAI_EXECUTION')) {
            if ($donneeComplementaire->getDureeDateFin() && $donneeComplementaire->getDureeDateDebut()) {
                $tableKeys['<!-- lineDureeMarche -->'] = '<text:p text:style-name="P7">' . Prado::Localize('DEFINE_DELAI_EXECUTION_PRESTATION') .
                 ' : ' .
                 Prado::Localize('ICONE_CALENDRIER') . ' : ' .
                 $donneeComplementaire->getDureeDateDebut('d/m/Y') .
                 ' - ' . Prado::Localize('DEFINE_FIN') .
                 ' : ' . $donneeComplementaire->getDureeDateFin('d/m/Y') . '</text:p>';
            }
        } elseif ($donneeComplementaire->getIdDureeDelaiDescription() == Atexo_Config::getParameter('DESCRIPTION_LIBRE')) {
            if ($donneeComplementaire->getDureeDescription()) {
                $tableKeys['<!-- lineDureeMarche -->'] = '<text:p text:style-name="P7">' . Prado::Localize('DEFINE_LIBRE_DUREE_DELAI') . ' : ' .
                $donneeComplementaire->getDureeDescription() . '</text:p>';
            }
        }
    }

    public function getConstitutionDossier($consultation, &$tableKeys, $styleColone)
    {
        $tableKeys['<!-- constitution -->'] = '<text:p text:style-name="' . $styleColone . '">' .
                                                Prado::localize('DEFINE_CONSTITUTION_DOSSIERS_REPONSES') . ' : ';
        if (1 == $consultation->getAutoriserReponseElectronique()) {
            $tableKeys['<!-- constitution -->'] .= '</text:p>';
            $nbrLots = is_countable($consultation->getAllLots()) ? count($consultation->getAllLots()) : 0;
            if ('1' == $consultation->getEnvCandidature()) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' . Prado::localize('DEFINE_ENVELOPPE_CONDIDATURE') . '</text:p>';
            }
            if ('1' == $consultation->getEnvOffre()) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' . Prado::localize('DEFINE_ENVELOPPE_OFFRE') . ' - ' .
                                                            Prado::localize('DEFINE_ENVELOPPE_UNIQUE') . '</text:p>';
            }
            if ($consultation->getEnvOffre() >= 2) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' .
                    Prado::localize('DEFINE_ENVELOPPE_OFFRE') . ' - ' .
                    Prado::localize('DEFINE_ENVELOPPE_PAR_LOT') . ' ' .
                    Prado::localize('DEFINE_NOMBRE_LOT') . ' : ' . $nbrLots . '</text:p>';
            }
            if ('1' == $consultation->getEnvOffreTechnique()) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' . Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE') .
                                                            ' - ' . Prado::localize('DEFINE_ENVELOPPE_UNIQUE') . '</text:p>';
            }
            if ($consultation->getEnvOffreTechnique() >= 2) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' .
                    Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE') . ' - ' .
                    Prado::localize('DEFINE_ENVELOPPE_PAR_LOT') . ' ' .
                    Prado::localize('DEFINE_NOMBRE_LOT') . ' : ' . $nbrLots . '</text:p>';
            }
            if ('1' == $consultation->getEnvAnonymat()) {
                $tableKeys['<!-- constitution -->'] .= '<text:p text:style-name="' . $styleColone . '"> ' . Prado::localize('DEFINE_TROISIEME_ENVELOPPE') . '</text:p>';
            }
        } else {
            $tableKeys['<!-- constitution -->'] .= '-' . '</text:p>';
        }
    }


    private function deleteClauses(int $consultationId): void
    {
        /** @var ConsultationRepository $consultationRepository */
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        /** @var Consultation $consultationSf */
        $consultationSf = $consultationRepository->find($consultationId);

        if ($consultationSf instanceof Consultation) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_ONLY')
            );

            $lots = $this->getNombreLots($consultationId, $consultationSf->getOrganisme());
            if ($lots > 0) {
                // Ici on vide tout les critère (social et environ)
                (new Atexo_Consultation_Lots())->deleteCommonLots(
                    $consultationSf->getOrganisme(),
                    $consultationSf->getId(),
                    $connexion
                );
            } else {
                /** @var ClausesService $clausesService */
                $clausesService = Atexo_Util::getSfService(ClausesService::class);
                $clausesService->deleteClausesRelatedTo($consultationSf);
            }
        }
    }

    public function isDeleteConsultation(int $consultationId): bool
    {
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        $consultationSf = $consultationRepository->find($consultationId);
        return !(count($consultationSf->getOffres()) > 0);
    }
}
