<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

/**
 * page de diagnostic (applet, java, Os...).
 *
 * @author adil el kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DiagnosticPoste extends MpeTPage
{

	public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['callFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());

        if($this->isMAMPEnabled()){
            MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
            MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
        }
    }

    /**
     * @return string
     */
    public function isCmsActif(): string
    {
        return $this->isMAMPEnabled() ? 'visible;' : 'none;';
    }

    /**
     * @return string
     */
    public function isCmsInactif(): string
    {
        return !$this->isMAMPEnabled() ? 'visible;' : 'none;';
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (!$this->getModeServeurCrypto()) {
                $infosBrowser = Atexo_Util::getInfosBrowserVisiteur();
                $this->versionNavigateur->Text = $infosBrowser['name'].' '.$infosBrowser['version'];
                if ('agent' == $_GET['callFrom']) {
                    //$this->testConfiguration->Text = Prado::localize('TEXT_TEST_CONFIGURATION_AGENT');
                    $this->textConfigurationOk->Text = Prado::localize('CONFIGURATION_OK_AGENT');
                    $this->imageConsultationTest1->visible = false;
                    $this->imageConsultationTest2->visible = false;
                    $this->msgCertificatImportant->setVisible(false);
                } else {
                    //$this->testConfiguration->Text = Prado::localize('TEXT_TEST_CONFIGURATION');
                    $this->textConfigurationOk->Text = Prado::localize('CONFIGURATION_OK');
                    $this->infoCertif->Text = str_replace('#HtmlAnchorClose', '</a>', str_replace('#HtmlAnchorOpen', "<a target='_blank' href='index.php?page=Entreprise.ConditionsUtilisation&calledFrom=entreprise#rubrique_1_paragraphe_3_2'>", Prado::localize('TEXT_TEST_CONFIGURATION_P3')));
                    $this->imageConsultationTest1->visible = true;
                    $this->imageConsultationTest2->visible = true;
                    $this->msgCertificatImportant->setVisible(false);
                }
            }
        }
    }

    /*
    * Permet d'enregister la trace de diagnoqtique de post
    */
    public function saveTraceDiagInscrit($sender, $param)
    {
        $logApplet = '';
        $description = '';
        $dateDebAction = date('Y-m-d H:i:s');
        if ('1' == $param->CallbackParameter) {
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION2', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        } else {
            $logApplet = $this->logApplet->Value;
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION3', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        }
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = '';
        $arrayDonnees['org'] = '';
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = $logApplet;
        $arrayDonnees['description'] = $description;

        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $this->resDiag->value,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
        $this->PanelButtons->render($param->NewWriter);
    }

    /**
     * @return bool
     */
    public function isAgent()
    {
        if ('agent' == strtolower(Atexo_Util::atexoHtmlEntities($_GET['callFrom']))) {
            return true;
        }

        return false;
    }

    public function getModeServeurCrypto()
    {
        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
                || ($this->isAgent() && Atexo_CurrentUser::getId() && Atexo_Module::isEnabled('ModeApplet'))
        ) {
            return 0;
        } else {
            return 1;
        }
    }
}
