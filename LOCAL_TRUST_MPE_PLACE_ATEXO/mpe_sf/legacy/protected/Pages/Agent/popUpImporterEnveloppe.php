<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpImporterEnveloppe extends MpeTPage
{
    // public $_acteEngagement='1';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $dataPieces = [];
								if (!$this->IsPostBack) {
            $this->panelMessageErreur->setVisible(false);
            $this->panelErreur->setVisible(false);
            $this->panelResizeErreur->setVisible(false);
            $this->panelResizeContainer->setVisible(false);

            $messageError = '';

            if (Atexo_CurrentUser::hasHabilitation('ImporterEnveloppe') && isset($_GET['id']) && isset($_GET['idEnveloppe']) && isset($_GET['callbackButton'])) {
                $idEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']);
                $referenceConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);

                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteria->setIdReference($referenceConsultation);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);

                if (is_array($consultation) && count($consultation) > 0) {
                    //Acte d'egagement
                    if ('1' == $consultation[0]->getSignatureActeEngagement() && $_GET['typeEnv'] == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                        $this->PanelAE->setVisible(true);
                    } else {
                        $this->PanelAE->setVisible(false);
                    }
                    $this->setViewState('acteEngagementImportee', $consultation[0]->getSignatureActeEngagement());
                    //Acte d'egagement
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                    $registre = CommonEnveloppePeer::retrieveByPK($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if (!$registre instanceof CommonEnveloppe) {
                        $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_DEPOT');
                    }
                    $offre = CommonOffresPeer::retrieveByPK($registre->getOffreId(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    $this->nomEntreprise->Text = $offre->getNomEntrepriseInscrit();
                    $this->panelResizeContainer->setVisible(true);

                    //Recuperation du nobre de fichiers enveloppe.
                    $nbreFichiersEnveloppe = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppeNonAE($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism());
                    $indexPiece = 0;
                    $nbrePiecesArajouter = is_countable($nbreFichiersEnveloppe) ? count($nbreFichiersEnveloppe) : 0;
                    for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
                        $dataPieces[$indexPiece]['pieceText'] = Atexo_Util::toUtf8($nbreFichiersEnveloppe[$i]->getNomFichier());

                        ++$indexPiece;
                    }
                    if ($nbrePiecesArajouter) {
                        $this->listePiecesRepeater->dataSource = $dataPieces;
                        $this->listePiecesRepeater->dataBind();
                        $indexPiece = 0;
                        foreach ($this->listePiecesRepeater->getItems() as $item) {
                            $item->pieceText->Text = $dataPieces[$indexPiece]['pieceText'];
                            ++$indexPiece;
                        }
                    }
                } else {
                    $messageError = Prado::localize('NON_AUTORISE_A_ANNULER');
                }
            }

            if ($messageError) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelErreur->setVisible(true);
                $this->container->setVisible(false);
                $this->panelMessageErreur->setMessage($messageError);
                $this->panelResizeErreur->setVisible(true);
            }
        }
    }

    public function afficherErreur($msg)
    {
        $this->messageErreur->setVisible(true);
        $this->messageErreur->setMessage($msg);
    }

    public function onImporterClick()
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCom->beginTransaction();
        $messageError = '';
        $tablesFiles = [];
        //plsrs fichiers de réponse.
        $i = 0;
        foreach ($this->listePiecesRepeater->Items as $one) {
            if ($one->enveloppeImportee->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'enveloppeImportee_'.$one->ItemIndex.session_id().time();
                if (move_uploaded_file($one->enveloppeImportee->LocalName, $infile)) {
                    //commencer le scan Antivirus
                    $msg = Atexo_ScanAntivirus::startScan($infile);
                    if ($msg) {
                        $this->afficherErreur($msg.'"'.$one->enveloppeImportee->FileName.'"');

                        return;
                    }
                    //Fin du code scan Antivirus
                    $hash = sha1_file($infile);

                    $tablesFiles[$i]['infile'] = $infile;
                    $tablesFiles[$i]['hash'] = $hash;
                    $tablesFiles[$i]['ae'] = '0';
                    $tablesFiles[$i]['xml'] = '0';
                    $tablesFiles[$i]['fileName'] = $one->pieceText->getText();
                } else {
                    //                  $fichierErrone.=$one->enveloppeImportee->FileName;
                    $messageError .= Prado::localize('PROBLEME_COPIE_FICHIER');
                }
            }
            ++$i;
        }
        if ('1' == $this->getViewState('acteEngagementImportee') && $_GET['typeEnv'] == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            if ($this->acteEngagementImportee->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'acteEngagementImportee'.session_id().time();
                if (move_uploaded_file($this->acteEngagementImportee->LocalName, $infile)) {
                    //commencer le scan Antivirus
                    $msg = Atexo_ScanAntivirus::startScan($infile);
                    if ($msg) {
                        $this->afficherErreur($msg.'"'.$this->acteEngagementImportee->FileName.'"');

                        return;
                    }
                    //Fin du code scan Antivirus
                    $hash = sha1_file($infile);

                    $tablesFiles[$i]['infile'] = $infile;
                    $tablesFiles[$i]['hash'] = $hash;
                    $tablesFiles[$i]['ae'] = '1';
                    $tablesFiles[$i]['xml'] = '0';
                    $tablesFiles[$i]['fileName'] = $this->acteEngagementImportee->FileName;
                } else {
                    $messageError .= Prado::localize('PROBLEME_COPIE_FICHIER');
                }
            }
            if (Atexo_Module::isEnabled('GenererActeDengagement')) {
                if ($this->acteEngagementXmlImportee->HasFile) {
                    $infile = Atexo_Config::getParameter('COMMON_TMP').'acteEngagementXmlImportee'.session_id().time();
                    if (move_uploaded_file($this->acteEngagementXmlImportee->LocalName, $infile)) {
                        //commencer le scan Antivirus
                        $msg = Atexo_ScanAntivirus::startScan($infile);
                        if ($msg) {
                            $this->afficherErreur($msg.'"'.$this->acteEngagementXmlImportee->FileName.'"');

                            return;
                        }
                        //Fin du code scan Antivirus
                        $hash = sha1_file($infile);

                        $tablesFiles[$i + 1]['infile'] = $infile;
                        $tablesFiles[$i + 1]['hash'] = $hash;
                        $tablesFiles[$i + 1]['ae'] = '1';
                        $tablesFiles[$i + 1]['xml'] = '1';
                        $tablesFiles[$i + 1]['fileName'] = $this->acteEngagementXmlImportee->FileName;
                    } else {
                        $messageError .= Prado::localize('PROBLEME_COPIE_FICHIER');
                    }
                } else {
                    $messageError .= Prado::localize('PROBLEME_COPIE_FICHIER');
                }
            }
        }
        $idEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']);
        $fichiersEnveloppeNonAE = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppeNonAE($idEnveloppe, $organisme); //type fichier PRI
        foreach ($fichiersEnveloppeNonAE as $un) {
            $fichiersEnveloppeNonAE['hash'][] = strtoupper($un->getHash());
            $fichiersEnveloppeNonAE['obj'][] = $un;
        }
        foreach ($tablesFiles as $un) {
            if ('1' == $un['ae']) {
                if ('1' == $un['xml']) {
                    $fichierEnveloppe = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppe($idEnveloppe, $organisme, Atexo_Config::getParameter('TYPE_FICHIER_ACTE_ENGAGEMENT'), '1');
                    if ($fichierEnveloppe) {
		        		$fichiersEnveloppeNonAE['hash'][]= strtoupper($fichierEnveloppe->getHash());
                        $fichiersEnveloppeNonAE['obj'][] = $fichierEnveloppe;
                    }
                } else {
                    $fichierEnveloppe = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppe($idEnveloppe, $organisme, Atexo_Config::getParameter('TYPE_FICHIER_ACTE_ENGAGEMENT'));
                    if ($fichierEnveloppe) {
		        		$fichiersEnveloppeNonAE['hash'][]= strtoupper($fichierEnveloppe->getHash());
                        $fichiersEnveloppeNonAE['obj'][] = $fichierEnveloppe;
                    }
                }
            }
        }
        foreach ($tablesFiles as $un) {
            if (in_array(strtoupper($un['hash']), $fichiersEnveloppeNonAE['hash'])) {
                $atexoBlob = new Atexo_Blob();
                $objEnveloppe = $this->getObjectByFileName($fichiersEnveloppeNonAE['obj'], $un['fileName']);
                if ($objEnveloppe) {
                    if ($objEnveloppe->getIdBlob()) {
                        $objEnveloppe->setIdBlob(NULL);
                        $objEnveloppe->save();
                        $atexoBlob->deleteBlobFile($objEnveloppe->getIdBlob(), $organisme);
                    }
                    $idBlob = $atexoBlob->insert_blob($objEnveloppe->getNomFichier(), $un['infile'], $organisme);
                    // si on oublie un fichier??
                    if ($idBlob) {
                        $objEnveloppe->setIdBlob($idBlob);
                        $objEnveloppe->save($connexionCom);

                        $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
                        $enveloppe->setCryptage('0');
                        $enveloppe->save($connexionCom);
                    } else {
                        $messageError = Prado::localize('PROBLEME_COPIE_FICHIER');
                        break;
                    }
                } else {
                    $messageError = Prado::localize('FICHIERS_NE_CORRESPONDENT_PAS');
                    break;
                }
            } else {
                $messageError = Prado::localize('FICHIERS_NE_CORRESPONDENT_PAS');
                break;
            }
        }
        if ($messageError) {
            $connexionCom->rollback();
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage($messageError);
        } else {
            $connexionCom->commit();
            $this->userScript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callbackButton'])."').click();window.close();</script>";
        }
    }

    public function getObjectByFileName($tabObjs, $fileName)
    {
        foreach ($tabObjs as $one) {
            if (Atexo_Util::toUtf8($one->getNomFichier()) == $fileName) {
                return $one;
            }
        }
    }
}
