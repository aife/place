<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe PopUpDetailMail.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpDetailMail extends MpeTPage
{
    protected $orgAcronyme;
    protected $idMsg;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->orgAcronyme = Atexo_CurrentUser::getCurrentOrganism();
            $this->idMsg = Atexo_Util::atexoHtmlEntities($_GET['idMsgDest']);

            $echangeDest = Atexo_Message::getEchangeDestinataireByid($this->idMsg, $this->orgAcronyme);

            // recuperer l'echange
            if ($echangeDest) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $idRef = $echangeDest->getCommonEchange($connexionCom)->getConsultationId();

                // verification de l'autorisation d'acces de l'agent connecte au message accede
                if ((new Atexo_Consultation())->retreiveConsultationAgent(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getCurrentOrganism(), Atexo_CurrentUser::getId(), $idRef)) {
                    $this->accusereception->remplirInfoEchange($echangeDest, $this->orgAcronyme);
                }
            }
        }
    }
}
