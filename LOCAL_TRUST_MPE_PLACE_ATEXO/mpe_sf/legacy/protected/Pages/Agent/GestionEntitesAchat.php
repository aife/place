<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class GestionEntitesAchat extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
            } else {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
            }
            $this->ChoixOrganisme->setDefaultOrganisme($organisme);
            $this->displayInfosOrganisme($organisme);
            $this->TreeServices->Text = self::displayEntitiesTree($organisme);
        }
    }

    public function displayInfosOrganisme($organisme)
    {
        (new Atexo_EntityPurchase())->reloadCachedEntities($organisme);
        $atexoOrganisme = new Atexo_Organismes();
        $organismeO = $atexoOrganisme->retrieveOrganismeByAcronyme($organisme);
        $this->sigleorganisme->Text = $organismeO->getSigle();
        $this->libelleorganisme->Text = $organismeO->getDenominationOrg();
        $this->linkDetailOrganisme->NavigateUrl = "?page=Agent.AgentDetailOrganisme&org=$organisme";
    }

    public function displayEntitiesTree($oragnisme)
    {
        $onClick = null;
        $id = null;
        $style = null;
        $allEntities = [];
        Atexo_EntityPurchase::getAllChilds(0, $allEntities, $oragnisme);

        $htmlStr = '<ul style="display:block" id=\'all-cats\'>';
        $nbAllEntities = is_countable($allEntities) ? count($allEntities) : 0;
        for ($i = 0; $i < $nbAllEntities; ++$i) {
            $curService = $allEntities[$i];
            $curDepth = $curService->getDepth();

            if ($i < $nbAllEntities - 1) { // Ce n'est pas le dernier élément
                $nextService = $allEntities[$i + 1];
                $nextDepth = $nextService->getDepth();

                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $picto = 'picto-plus.gif';
                    $id = 'id=sous-cat_'.$i;
                    $style = 'style="display:none"';
                    $onClick = ' onclick="openSousCat(this,\'sous-cat_'.$i.'\')" ';
                } else {
                    $picto = 'picto-tiret.gif';
                }
                $htmlStr .= '<li ><img '.$onClick.' src="'.$this->themesPath().'/images/'.$picto.'" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" />'
                            .'<a href="?page=Agent.AgentDetailEntiteAchat&idEntite='.$curService->getId()."&org=$oragnisme".'" title="'.Prado::localize('CLIQUER_ACCEDER_DETAIL').'"><strong>'
                            .$curService->getSigle().'</strong>'.' - '.$curService->getLibelle().'</a>';

                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $htmlStr .= '<ul '.$id.' '.$style.'>';
                }

                if ($curDepth > $nextDepth) { // le prochain n'est ni un fils ni un parent (fin d'une branche)
                    $differenceDepth = $curDepth - $nextDepth;
                    for ($j = 0; $j < $differenceDepth; ++$j) {
                        $htmlStr .= '</li></ul>';
                    }
                }
                if ($curDepth == $nextDepth) { //le prochain est un frère, on ferme le li
                    $htmlStr .= '</li>';
                }
            } else { // C'est le dernier élément
                $htmlStr .= '<li><img src="'.$this->themesPath().'/images/picto-tiret.gif" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" />'
                            .'<a href="?page=Agent.AgentDetailEntiteAchat&idEntite='.$curService->getId()."&org=$oragnisme".'" title="'.Prado::localize('CLIQUER_ACCEDER_DETAIL').'"><strong>'
                            .$curService->getSigle().'</strong>'.' - '.$curService->getLibelle().'</a>';
                $htmlStr .= '</li>';
                for ($j = 0; $j < ($curDepth); ++$j) {
                    $htmlStr .= "</ul>\n";
                }
            }
        }

        $htmlStr .= '';

        return $htmlStr;
    }

    public function onCallBackOrganismeSelected($sender, $param, $selectedOrganisme)
    {
        $this->displayInfosOrganisme($selectedOrganisme);
        $this->TreeServices->Text = $this->displayEntitiesTree($selectedOrganisme);
        $this->panelRefreshTreeServices->render($param->NewWriter);
    }
}
