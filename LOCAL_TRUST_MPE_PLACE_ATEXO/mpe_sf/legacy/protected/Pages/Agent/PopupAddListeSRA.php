<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTSyntheseRapportAudit;
use Application\Propel\Mpe\CommonTSyntheseRapportAuditPeer;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */

class PopupAddListeSRA extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        self::fillAnneeSRA();
    }

    /**
     * Verifier la taille maximum du document à ajouter
     * ( 2Mo maximum).
     */
    public function verifyDocTailleCreation($sender, $param)
    {
        $sizedocAttache = $this->ajoutFichier->FileSize;
        if (0 == $sizedocAttache) {
            $this->labelClose->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_FICHIER_VIDE');
        } elseif ($sizedocAttache >= 2 * 1024 * 1024) {
            $this->labelClose->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_TAILLE_DOC');
        }
    }

    /**
     * Verifier les éxtensions acceptées.
     */
    public function verifyExtensionDoc($sender, $param)
    {
        $extensionFile = strtoupper(Atexo_Util::getExtension($this->ajoutFichier->FileName));

        $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
        if (!in_array($extensionFile, $data)) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = str_replace('[__EXTENSION_FILES__]', implode(',', $data), Prado::localize('TEXT_MSG_EXTESION_NON_VALIDE'));

            return;
        }
    }

    /* ajouter un document dans la base
     *
     *
     */
    public function addPieceJointe($sender, $param)
    {
        if ($this->ajoutFichier->HasFile) {
            $infile = Atexo_Config::getParameter('COMMON_TMP').'listeSRA'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $atexoBlob = new Atexo_Blob();
                $atexoCrypto = new Atexo_Crypto();
                $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                if (isset($_GET['id'])) {
                    $pj = CommonTSyntheseRapportAuditPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                } else {
                    $pj = new CommonTSyntheseRapportAudit();
                }
                if (is_array($arrayTimeStampAvis)) {
                    $pj->setHorodatage($arrayTimeStampAvis['horodatage']);
                    $pj->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                }
                $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, Atexo_CurrentUser::getCurrentOrganism());
                $pj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $pj->setNomFichier($this->ajoutFichier->FileName);
                $pj->setTaille($this->ajoutFichier->FileSize);
                $pj->setFichier($avisIdBlob);
                $pj->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
                $pj->setDate(date('Y-m-d H:i:s'));
                $pj->setAnnee($this->anneeSRA->getSelectedValue());
                $pj->save($connexion);
                if (Atexo_Module::isEnabled('AlerteMetier')) {
                    $service = CommonServicePeer::retrieveByPK(Atexo_CurrentUser::getIdServiceAgentConnected(), $connexion);
                    if ($service instanceof CommonService) {
                        Atexo_AlerteMetier::lancerAlerte('CronService', $service);
                    }
                }
            }
            @unlink($infile);
        }
        $this->labelClose->Text = '<script>refreshRepeaterListeSRA();</script>';
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    public function fillAnneeSRA()
    {
        $listesAnnees = [];
        $listesAnnees[0] = '---'.Prado::localize('TEXT_SELECTIONNER').'---';
        for ($i = Atexo_Config::getParameter('NBRS_ANNEE_ANTERIEUR'); $i >= 0; --$i) {
            $listesAnnees[date('Y') - $i] = date('Y') - $i;
        }
        $this->anneeSRA->dataSource = $listesAnnees;
        $this->anneeSRA->dataBind();
    }
}
