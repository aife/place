<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchiveRappelTableauDeBord extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            //$launchSearch=true;
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteriaVo->setFromArchive(true);
            $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
            $criteriaVo->setConsultationAArchiver(true);

            if (isset($_GET['id'])) {
                $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }

            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->dataForSearchResult($criteriaVo);
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->ArchiveTableauBord->setVisible(true);
        $this->ArchiveResultSearch->fillRepeaterWithDataForArchiveSearchResult($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $criteriaVoNew = new Atexo_Consultation_CriteriaVo();
        $criteriaVo = $this->getViewState('CriteriaVo', $criteriaVoNew);
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                $this->firstDiv->setCssClass('tab-on');
                                $this->secondDiv->setCssClass('tab');
                                $this->thirdDiv->setCssClass('tab');
                                $this->fourthDiv->setCssClass('tab');
                                $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE');
                break;
            case 'firstBisTab':
                $this->firstDiv->setCssClass('tab');
                               $this->firstBisDiv->setCssClass('tab-on');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab');
                               $this->fifthDiv->setCssClass('tab');
                               $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_ELABORATION');
                               // no break
            case 'secondTab':
                $this->firstDiv->setCssClass('tab');
                                $this->secondDiv->setCssClass('tab-on');
                                $this->thirdDiv->setCssClass('tab');
                                $this->fourthDiv->setCssClass('tab');
                                $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_PREPARATION');
                break;
            case 'thirdTab':
                $this->firstDiv->setCssClass('tab');
                                $this->secondDiv->setCssClass('tab');
                                $this->thirdDiv->setCssClass('tab-on');
                                $this->fourthDiv->setCssClass('tab');
                                $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
                break;
            case 'fourthTab':
                $this->firstDiv->setCssClass('tab');
                                $this->secondDiv->setCssClass('tab');
                                $this->thirdDiv->setCssClass('tab');
                                $this->fourthDiv->setCssClass('tab-on');
                                $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_DECISION');
                break;
            default:
                $nouveauStatutArchive = '';
                break;
        }

        if ($nouveauStatutArchive == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') ||
            $nouveauStatutArchive == Atexo_Config::getParameter('STATUS_DECISION') ||
            $nouveauStatutArchive == Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE')) {
            $this->Page->ArchiveResultSearch->panelArchivage->setVisible(true);
        } else {
            $this->Page->ArchiveResultSearch->panelArchivage->setVisible(false);
        }
        $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutArchive);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    /**
     * Fonctione dse tri du tableau de bord.
     */
    public function Trier($sender, $param)
    {
        $this->ArchiveResultSearch->Trier($param->CommandName);
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }
}
