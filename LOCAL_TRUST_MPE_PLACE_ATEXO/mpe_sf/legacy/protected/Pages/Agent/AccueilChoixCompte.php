<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Agent\Atexo_Agent_Authentifier;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/**
 * Page de choix de comptes (agent multipart).
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 0
 *
 * @since MPE-4.4
 */
class AccueilChoixCompte extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('multiAgent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $idAgent = Atexo_CurrentUser::getIdAgentConnected();
            if (!$this->repeaterInformations->repeaterDataBind($idAgent)) {
                if (Atexo_Module::isEnabled('SocleInterne')) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AccueilAgentAuthentifieSocleinterne');
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
                }
            }
        }
    }

    public function connecter($idAgent)
    {
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            $login = $agent->getLogin();
            $pwd = $agent->getPassword();
            if ((new Atexo_Agent_Authentifier())->VerifyLoginPwd($login, $pwd)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $agent->setDateConnexion(date('Y-m-d H:i:s'));
                $agent->save($connexion);
                if (Atexo_Module::isEnabled('SocleInterne')) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AccueilAgentAuthentifieSocleinterne');
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
                }
            }
        }
    }
}
