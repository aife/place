<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Publicite\Atexo_Publicite_RenseignementsBoamp;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionBoamp extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionCompteBoamp')) {
            $this->gestionCompteBoamp->Visible = true;
            $this->panelValidationSummary->visible = false;
            if (Atexo_Module::isEnabled('GestionBoampMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            if (!$this->IsPostBack) {
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
                $this->boampRepeater->DataSource = (new Atexo_PublicPurchaser())->retrievePublicPurchasers(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getCurrentOrganism(), false);
                $this->boampRepeater->DataBind();
                $this->addBoampCountLink->Attributes->onclick = "javascript:popUp('index.php?page=Agent.popUpGestionBoamp&serv=".Atexo_CurrentUser::getIdServiceAgentConnected()."','yes');";
            }
        } else {
            $this->gestionCompteBoamp->Visible = false;
            $this->panelValidationSummary->visible = true;
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->addBoampCountLink->Attributes->onclick = "javascript:popUp('index.php?page=Agent.popUpGestionBoamp&serv=".$this->ChoixEntiteAchat->getSelectedEntityPurchase()."','yes');";
        $this->setViewState('idService', $this->ChoixEntiteAchat->getSelectedEntityPurchase());
        $this->boampRepeater->DataSource = (new Atexo_PublicPurchaser())->retrievePublicPurchasers($this->ChoixEntiteAchat->getSelectedEntityPurchase(), Atexo_CurrentUser::getCurrentOrganism(), false);
        $this->boampRepeater->DataBind();
        $this->boampPanel->render($param->NewWriter);
    }

    public function decisionOrProduction($BoampTarget)
    {
        return ('1' == $BoampTarget) ? Prado::localize('TEXT_PRODUCTION') : Prado::localize('TEXT_DEMO');
    }

    public function refreshBoampRepeater($sender, $param)
    {
        $this->boampRepeater->DataSource = (new Atexo_PublicPurchaser())->retrievePublicPurchasers($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism(), false);
        $this->boampRepeater->DataBind();
        $this->boampPanel->render($param->NewWriter);
    }

    public function refreshRepeater($sender, $param)
    {
        $this->boampRepeater->DataSource = (new Atexo_PublicPurchaser())->retrievePublicPurchasers($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism(), false);
        $this->boampRepeater->DataBind();
    }

    public function deleteBoamp($sender, $param)
    {
        $idPublicPurchaser = $param->CommandParameter;
        $deleteAffected = (new Atexo_PublicPurchaser())->delete($idPublicPurchaser);
        if ($deleteAffected) {
            (new Atexo_Publicite_RenseignementsBoamp())->deleteRenseignementsBoampByIdCompte($idPublicPurchaser);
        }
        $this->boampRepeater->DataSource = (new Atexo_PublicPurchaser())->retrievePublicPurchasers($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism(), false);
        $this->boampRepeater->DataBind();
    }
}
