<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/*
 * Created on 30 août 2012
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @package
 */
class StatutOffres extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $json = '';
        $logger = Atexo_LoggerManager::getLogger('depot');
        $logger->info('json envoye : '.$_POST['json']);
        if (isset($_POST['json'])) {
            $jsonArray = json_decode($_POST['json'], true, 512, JSON_THROW_ON_ERROR);
            //$json = json_decode('{"repeaterName":"enveloppeCandidatureElectronique","enveloppes":[{"id":746,"statut":"2"}]}', true);
            //$json = json_decode('{"repeaterName":"enveloppeReponsesElectronique","offres":[{"id":329,"statut":"1"}]}', true);
            $offres = $jsonArray['offres'] ?? [];
            $enveloppes = $jsonArray['enveloppes'] ?? [];
            if ($this->verifyStatut($offres, $enveloppes)) {
                $jsonArray['change'] = 'true';
            } else {
                $jsonArray['change'] = 'false';
            }
            $jsonArray['offres'] = $offres;
            $jsonArray['enveloppes'] = $enveloppes;
            $json = json_encode($jsonArray, JSON_THROW_ON_ERROR);
        }
        $logger->info('json return '.$json);
        echo $json;
        exit;
    }

    public function verifyStatut(&$offres, &$enveloppes)
    {
        $logger = Atexo_LoggerManager::getLogger('depot');
        $change = false;
        $offresReturn = [];
        $enveloppeReturn = [];
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $formeEnveloppe = Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE');
        if (!empty($enveloppes) && is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                $enveloppeObj = (new Atexo_Consultation_Responses())->retrieveEnveloppeById($enveloppe['id'], $formeEnveloppe, $organisme);
                $logger->info('Enveloppe id '.$enveloppe['id'].' objet '.print_r($enveloppeObj, true));
                if ($enveloppeObj instanceof CommonEnveloppe) {
                    if ($enveloppeObj->getStatutEnveloppe() != $enveloppe['statut']) {
                        $change = true;
                        $enveloppe['statut'] = $enveloppeObj->getStatutEnveloppe();
                    }
                    $enveloppeReturn[$enveloppeObj->getIdEnveloppeElectro()] = $enveloppe;
                }
            }
        } elseif (!empty($offres) && is_array($offres)) {
            foreach ($offres as $offre) {
                $offreObj = (new Atexo_Consultation_Responses())->retrieveOffreById($offre['id'], $organisme);
                $logger->info('Offre id '.$offre['id'].' objet '.print_r($offreObj, true));
                if ($offreObj instanceof CommonOffres) {
                    if ($offreObj->getStatutOffres() != $offre['statut']) {
                        $change = true;
                        $offre['statut'] = $offreObj->getStatutOffres();
                    }
                    $offresReturn[$offreObj->getId()] = $offre;
                }
            }
        }
        $enveloppes = $enveloppeReturn;
        $offres = $offresReturn;
        $logger->info('Offres return '.print_r($offres, true));

        return $change;
    }
}
