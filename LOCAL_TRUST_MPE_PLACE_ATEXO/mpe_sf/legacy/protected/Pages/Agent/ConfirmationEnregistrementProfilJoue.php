<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

/**
 * Classe de gestion du message de confirmation de l'enregistrement du profil Joue.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @since MPE-4
 */
class ConfirmationEnregistrementProfilJoue extends MpeTPage
{
    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        $this->confirmationEnregistrement->Message = Prado::localize('MESSAGE_ENREGISTREMENT_PROFIL_JOUE');
    }
}
