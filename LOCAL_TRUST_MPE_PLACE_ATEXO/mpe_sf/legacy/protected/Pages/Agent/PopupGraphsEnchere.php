<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Popup contenant les graphiques de suivi d'une enchere.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupGraphsEnchere extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
    }

    public function getIdEnchere()
    {
        if (isset($_GET['idEnchere'])) {
            return Atexo_Util::atexoHtmlEntities($_GET['idEnchere']);
        } else {
            return 0;
        }
    }

    public function getTypeGraph()
    {
        if (isset($_GET['type'])) {
            return Atexo_Util::atexoHtmlEntities($_GET['type']);
        } else {
            return 0;
        }
    }

    public function callBackTimer($sender, $param)
    {
        $this->CallbackClient->callClientFunction('popupResize');
    }
}
