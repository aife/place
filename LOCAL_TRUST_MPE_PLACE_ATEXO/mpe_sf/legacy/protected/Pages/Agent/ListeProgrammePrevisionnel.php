<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

class ListeProgrammePrevisionnel extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->tableauPP->setPostBack($this->IsPostBack);
        $this->tableauPP->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $this->tableauPP->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
    }
}
