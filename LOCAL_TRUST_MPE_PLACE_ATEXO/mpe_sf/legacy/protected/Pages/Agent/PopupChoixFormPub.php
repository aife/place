<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceJAL;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonReferentielFormXml;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_ReferentielFormXml;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChoixFormPub extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['PubLibre'])) {
                $this->displayModeleFormLibre();
            } else {
                $this->displayModeleForm();
            }
        }
    }

    /**
     * Affiche la liste des types de formulaire.
     */
    public function displayModeleForm()
    {
        $arListFormXml = [];
        $arListFormXml['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $listFormXml = [];
        //Début recuperation des types de formulaire xml publicité parametrés dans les types procedures
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        if ($consultation) {
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObject($consultation->getIdTypeProcedureOrg(), false);
            if ($typeProcedure) {
                $listeTypeFormXmlPub = explode('#', $typeProcedure->getPubliciteTypesFormXml());
                if ($listeTypeFormXmlPub && $listeTypeFormXmlPub[0]) {
                    foreach ($listeTypeFormXmlPub as $idTypeForm) {
                        if ($idTypeForm) {
                            $typeFormXml = (new Atexo_Publicite_ReferentielFormXml())->retrieveTypeFormXmlById($idTypeForm);
                            $listFormXml[] = $typeFormXml;
                        }
                    }
                }
            }
        }//Fin recuperation des types de formulaire xml publicité parametrés dans les types procedures

        //Si les types de formulaire xml ne sont pas parametrés dans les types de procedure, on recupère tous les types de formulaires xml
        if (!$listFormXml) {
            $listFormXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveListTypeFormulaireXml();
        }

        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getLibelleType = 'getLibelleType'.ucwords($langue);
        foreach ($listFormXml as $oneFormXml) {
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$oneFormXml->$getLibelleType()) {
                $arListFormXml[$oneFormXml->getId().'-'.$oneFormXml->getIdDestinataire()] = $oneFormXml->getLibelleType();
            } else {
                $arListFormXml[$oneFormXml->getId().'-'.$oneFormXml->getIdDestinataire()] = $oneFormXml->$getLibelleType();
            }
        }
        $this->modelForm->DataSource = $arListFormXml;
        $this->modelForm->dataBind();
    }

    /**
     * Action sur le boutton valider.
     */
    public function doValider($sender, $param)
    {
        if (isset($_GET['PubLibre'])) {
            $formLibre = $this->addNewModeleLibre();
            if ($formLibre >= 0) {
                $this->scriptClose->Text = '<script>refreshRepeaterFormLibre();</script>';
            }
        } else {
            $formXml = self::addNewModeleXml();
            //Début ajout des destinataires possibles
            if (-1 != $formXml) {
                $arrayDest = (new Atexo_Publicite_Destinataire())->retreiveDestinataireType();
                if ($arrayDest && $arrayDest[0]) {
                    foreach ($arrayDest as $oneDest) {
                        if ((new Atexo_Publicite_Destinataire())->existingDestinataire($formXml->getId(), $oneDest->getId())) {
                            if ($oneDest->getId() == Atexo_Config::getParameter('IDENTIFIANT_GROUPE_MONITEUR')) {
                                (new Atexo_Publicite_Destinataire())->addAnnonceMoniteur($formXml->getId(), $oneDest->getId());
                            } else {
                                (new Atexo_Publicite_Destinataire())->addAnnonceBoamp($formXml->getId(), $oneDest->getId());
                            }
                        }
                    }
                }
            }
            //Fin ajout des destinataires possibles
            $this->scriptClose->Text = '<script>refreshRepeater();</script>';
        }
    }

    /**
     * Ajouter un type de formulaire.
     */
    public function addNewModeleXml()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $newFormXml = new CommonReferentielFormXml();
        $newFormXml->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $newFormXml->setConsultationId(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        $newFormXml->setDateCreation(date('Y-m-d H:i'));
        $arSelectedValue = explode('-', $this->modelForm->SelectedValue);
        $newFormXml->setIdTypeXml($arSelectedValue[0]);
        $newFormXml->setStatut(Atexo_Config::getParameter('ANNONCE_BROUILLON'));
        $arIdsAttribution = explode('#', Atexo_Config::getParameter('IDENTIFIANTS_AVIS_ATTRIBUTION_BOAMP'));
        if (!in_array($arSelectedValue[0], $arIdsAttribution)) {
            $xml = $this->generateXml(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        } else {
            $xml = $this->generateXmlAttribution(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        }
        if ($xml) {
            $newFormXml->setXml($xml);
        }
        $res = $newFormXml->save($connexion);
        if ($res >= 0) {
            return $newFormXml;
        } else {
            return -1;
        }
    }

    /**
     * Affiche la liste des types de formulaire.
     */
    public function displayModeleFormLibre()
    {
        $arListFormLirbe = [0 => Prado::localize('TEXT_SELECTIONNER').' ...',
        1 => Prado::localize('TEXT_ANNONCE_EXTRAIT'),
        2 => Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'),
        3 => Prado::localize('TEXT_URL_ACCES_DIRECT'), ];
        $this->modelForm->DataSource = $arListFormLirbe;
        $this->modelForm->dataBind();
    }

    public function addNewModeleLibre()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (1 == $this->modelForm->SelectedValue) {
            $newAnnoncejal = new CommonAnnonceJAL();
            $newAnnoncejal->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $newAnnoncejal->setConsultationId(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            $newAnnoncejal->setDateCreation(date('Y-m-d H:i'));
            $newAnnoncejal->setObjet(Prado::localize('TEXT_OBJET_MAIL_DEMANDE_PUBLICATION_JAL'));
            $textAnnonce = Prado::localize('DEFINE_TEXT_BONJOUR');
            $textAnnonce .= '
';
            $textAnnonce .= '
';
            $textAnnonce .= Prado::localize('TEXT_CORPS_MAIL_DEMANDE_PUBLICATION_JAL');
            $textAnnonce .= '
';
            $textAnnonce .= '
';
            $textAnnonce .= (new Atexo_Message())->getInfoConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism(), true);
            $textAnnonce .= '
';
            $textAnnonce .= Prado::localize('TEXT_NOMBRE_DE_JUSTIFICATIFS').' : -';
            $textAnnonce .= '
';
            $newAnnoncejal->setTexte($textAnnonce);
            $newAnnoncejal->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));

            $res = $newAnnoncejal->save($connexionCom);
            if ($res >= 0) {
                return $newAnnoncejal;
            } else {
                return -1;
            }
        } else {
            $newAvis = new CommonAVIS();
            $newAvis->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $newAvis->setConsultationId(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            $newAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_VIDE'));
            $newAvis->setDateCreation(date('Y-m-d H:i'));
            if (2 == $this->modelForm->SelectedValue) {
                $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            } else {
                $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
            }
            //print_r($newAvis);exit;
            $res = $newAvis->save($connexionCom);
            if ($res >= 0) {
                return $newAvis;
            } else {
                return -1;
            }
        }
    }

    /**
     * remplir l'xml modele par les informations sur la consultation.
     */
    public function generateXml($consRef)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consRef, null);
        $lots = $consultation->getAllLots();
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_XML_BOAMP');
        $handle = fopen($filePath, 'r');
        $contents = fread($handle, filesize($filePath));
        fclose($handle);
        if ((is_countable($lots) ? count($lots) : 0) > 0) {
            $scriptLotHeader = '<lots>
								<typeLots> ';
            $scriptLot = '';
            foreach ($lots as $oneLot) {
                $scriptLot .= '<lotAvisMarche>';
                $scriptLot .= '<numLot>'.Atexo_Util::escapeXmlChars($oneLot->getLot()).'</numLot>
							<CPV>
								<ObjetPrincipal>
									<ClassPrincipale>'.Atexo_Util::escapeXmlChars($oneLot->getCodeCpv1()).'</ClassPrincipale>
								</ObjetPrincipal>';
                $arrayCodeCpvSec = explode('#', $oneLot->getCodeCpv2());
                if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                    foreach ($arrayCodeCpvSec as $codeCpvSec) {
                        if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                            $scriptLot .= '<ObjetComplementaire><ClassPrincipale>'.Atexo_Util::escapeXmlChars($codeCpvSec).'</ClassPrincipale></ObjetComplementaire>';
                        }
                    }
                }
                $scriptLot .= '</CPV> <description>'.utf8_decode(($oneLot->getDescriptionDetail())).'</description>';
                $scriptLot .= ' <intitule>'.utf8_decode(($oneLot->getDescription())).'</intitule>';
                $scriptLot .= '</lotAvisMarche>';
            }
            $scriptLotFooter = '</typeLots>
								 </lots>';
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--allotissement-->', ($scriptLotHeader.$scriptLot.$scriptLotFooter));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--allotissement-->', '<MarcheUnique/>');
        } ///caterogiePrincipale
        if ('' != $consultation->getCategorie()) {
            $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--caterogiePrincipale-->', '<'.$categorie.'/>');
        } ///TypeProcedure
        $typeProcedureObject = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObject($consultation->getIdTypeProcedureOrg());
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--typeProcedure-->', $typeProcedureObject->getTagBoamp());
        // Ajout des codes CPV Complementaires
        $xmlCpv = '';
        $arrayCodeCpvSec = explode('#', $consultation->getCodeCpv2());
        if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
            foreach ($arrayCodeCpvSec as $codeCpvSec) {
                if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                    $xmlCpv .= '<ObjetComplementaire><ClassPrincipale>'.$codeCpvSec.'</ClassPrincipale></ObjetComplementaire>';
                }
            }
        }
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--cpvComplementaire-->', $xmlCpv);
        if (0 != $consultation->getEnvOffre()) {
            $xmlDateRecetion = '<ReceptionOffres>'.Atexo_Util::escapeXmlChars(Atexo_Util::iso2iso8601DateTime($consultation->getDateFin())).'</ReceptionOffres>';
        } else {
            $xmlDateRecetion = '<ReceptCandidatures>'.Atexo_Util::escapeXmlChars(Atexo_Util::iso2iso8601DateTime($consultation->getDateFin())).'</ReceptCandidatures>';
        }
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--dateReception-->', $xmlDateRecetion);
        if ('' != $consultation->getObjet()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', utf8_decode(($consultation->getObjet())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', '');
        }
        if ('' != $consultation->getCodeCpv1()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', Atexo_Util::escapeXmlChars(($consultation->getCodeCpv1())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', '');
        }
        if ('' != $consultation->getReferenceUtilisateur()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', utf8_decode(($consultation->getReferenceUtilisateur())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', '');
        } //Ajout de l'url d'accès direct de la consultation
        $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
        if ('' != $urlAccesDirect) {
            $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
            $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
        } else {
            $urlEncode = '';
        }
        $adressesComplt = '';
        $adressesComplt .= '<adressesComplt>
        						<document>
        							<coord>
        								<url>'.$urlEncode.'</url>
        							</coord>
        						</document>';
        $adressesComplt .= '<envoi>
	    							<coord>
	    								<url>'.$urlEncode.'</url>
	    							</coord>
	    				   		</envoi>';
        $adressesComplt .= '</adressesComplt>';
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);

        return $contents;
    }

    /**
     * permet de Generer un flux xml pour un avis d'attribution.
     */
    public function generateXmlAttribution($consRef)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consRef, null);
        //$filePath =  Atexo_Config::getParameter('CHEMIN_MODEL_INIT_ATTRIBUTION_XML_BOAMP');
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_ATTRIBUTION_BOAMP');
        $handle = fopen($filePath, 'r');
        $contents = fread($handle, filesize($filePath));
        fclose($handle);
        ///TypeProcedure
        $typeProcedureObject = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObject($consultation->getIdTypeProcedureOrg());
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--typeProcedure-->', $typeProcedureObject->getTagBoamp());
        // Ajout des codes CPV Complementaires
        $xmlCpv = '';

        $arrayCodeCpvSec = explode('#', $consultation->getCodeCpv2());
        if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
            foreach ($arrayCodeCpvSec as $codeCpvSec) {
                if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                    $xmlCpv .= '<ObjetComplementaire><ClassPrincipale>'.$codeCpvSec.'</ClassPrincipale></ObjetComplementaire>';
                }
            }
        }

        $contents = Atexo_Util::xmlStrReplace($contents, '<!--cpvComplementaire-->', $xmlCpv);
        if ('' != $consultation->getObjet()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', utf8_decode(($consultation->getObjet())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', '');
        }
        if ('' != $consultation->getCodeCpv1()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', Atexo_Util::escapeXmlChars(($consultation->getCodeCpv1())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', '');
        }
        if ('' != $consultation->getReferenceUtilisateur()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', utf8_decode(($consultation->getReferenceUtilisateur())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', '');
        }

        if ('' != $consultation->getIntitule()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TITRE_MARCHE', utf8_decode(($consultation->getIntitule())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TITRE_MARCHE', '');
        }
        if ('' != $consultation->getDetailConsultation()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'DETAIL_ANNONCE', utf8_decode(($consultation->getDetailConsultation())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'DETAIL_ANNONCE', '');
        }
        //adressesComplt
        // Ajout de l'url d'accès direct de la consultation
        $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
        if ('' != $urlAccesDirect) {
            $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
            $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
        } else {
            $urlEncode = '';
        }
        $adressesComplt = '';
        $adressesComplt .= '<adressesComplt>
        						<document>
        							<coord>
        								<url>'.$urlEncode.'</url>
        							</coord>
        						</document>';
        $adressesComplt .= '<envoi>
	    							<coord>
	    								<url>'.$urlEncode.'</url>
	    							</coord>
	    				   		</envoi>';
        $adressesComplt .= '</adressesComplt>';
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);

        return $contents;
    }
}
