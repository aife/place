<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratEtProcedure;
use Application\Propel\Mpe\CommonTTypeContratEtProcedureQuery;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\CommonTypeProcedureOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpGestionMapa extends MpeTPage
{
    private ?\Application\Propel\Mpe\CommonTypeProcedureOrganisme $_mapa = null;

    public function getMapaToUpdate()
    {
        return $this->getViewState('mapaToUpdate');
    }

    public function setMapaToUpdate($mapa)
    {
        $this->setViewState('mapaToUpdate', $mapa);
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->getIsPostBack() && !$this->getIsCallback()) {
            $this->panelMessageErreur->setVisible(false);

            if (Atexo_Module::isEnabled('GestionMapa')) {
                if (Atexo_CurrentUser::hasHabilitation('GestionMapa')) {
                    if (isset($_GET['id'])) {
                        $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
                            if ($typeProcedure->getMapa() == Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA')) {
                                $this->_mapa = $typeProcedure;
                                $this->setMapaToUpdate($typeProcedure);
                                $this->showMapa->setVisible(true);
                            } else {
                                $this->showMapa->setVisible(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_NON_MAPA'));
                            }
                        } else {
                            $this->showMapa->setVisible(false);
                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::localize('TYPE_PROCEDURE_INCONNU'));
                        }
                    } else {
                        $this->_mapa = null;
                    }
                } else {
                    $this->showMapa->setVisible(false);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_MAPA'));
                }
            } else {
                $this->showMapa->setVisible(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('MODULE_MAPA_DESACTIVER'));
            }
        }
    }

    public function onValiderMapaClick($sender, $param)
    {
        $procedureEquivalence = null;
        $connexionCom = null;
        $arrayMsgErreur = [];
        if ($this->IsValid) {
            try {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $connexionCom->beginTransaction();

                if (isset($_GET['id'])) {
                    $typeProcedure = CommonTypeProcedureOrganismePeer::retrieveByPk(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                } else {
                    $typeProcedure = new CommonTypeProcedureOrganisme();
                    $procedureEquivalence = Atexo_Config::getProcedureEquivalenceMAPA();
                }
                $typeProcedure->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $typeProcedure->setLibelleTypeProcedure((new Atexo_Config())->toPfEncoding($this->libelleMapa->Text));
                $typeProcedure->setAbbreviation((new Atexo_Config())->toPfEncoding($this->sigle->Text));
                $typeProcedure->setMapa(Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA'));
                $typeProcedure->setIdTypeProcedurePortail(Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_POTAIL'));

                if ($this->inferieurBareme->Checked) {
                    $typeProcedure->setIdMontantMapa(Atexo_Config::getParameter('MONTANT_MAPA_INF_90'));
                    $typeProcedure->setAbreviationInterface('MAI');
                    $typeProcedure->setIdExterne(Atexo_Config::getParameter('ID_EXTERNE_MAPA_INFERIEUR'));
                } elseif ($this->superieurBareme->Checked) {
                    $typeProcedure->setIdMontantMapa(Atexo_Config::getParameter('MONTANT_MAPA_SUP_90'));
                    $typeProcedure->setAbreviationInterface('MAS');
                    $typeProcedure->setIdExterne(Atexo_Config::getParameter('ID_EXTERNE_MAPA_SUPERIEUR'));
                }

                if ($this->statutActif->Checked) {
                    $typeProcedure->setActiverMapa(Atexo_Config::getParameter('MAPA_ACTIF'));
                } elseif ($this->statutVerrouille->Checked) {
                    $typeProcedure->setActiverMapa(Atexo_Config::getParameter('MAPA_VEROUIILLE'));
                }
                $typeProcedure->setIdTypeProcedurePortail(Atexo_Config::getParameter('ID_MAPA_PORTAIL'));
                $typeProcedure->setTagBoamp('<ProcedureAdaptee/>');
                $typeProcedure->setTagNameMesureAvancement('4');
                $typeProcedure->setTagNameChorus('9');
                $typeProcedure->setCodeRecensement('9');

                $typeProcedure->save($connexionCom);
                if (!isset($_GET['id'])) {
                    $this->InsertReferentielCalendrier($typeProcedure->getIdTypeProcedure(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    $procedureEquivalence->setIdTypeProcedure($typeProcedure->getIdTypeProcedure());
                    $procedureEquivalence->save($connexionCom);
                    $this->parametrerTypeContrat($typeProcedure, $connexionCom);
                }
                $connexionCom->commit();
                //Mise à jour de la session
                (new Atexo_Consultation_ProcedureType())->reloadCacheTypeProcedureOrganisme();

                $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refresh').click();window.close();</script>";
            } catch (Exception $e) {
                if ($connexionCom) {
                    $connexionCom->rollBack();
                }
                Prado::log('Erreur  Mapa : '.$e->getMessage().' trace :'.$e->getTraceAsString(), TLogger::ERROR, 'Atexo');
                $arrayMsgErreur[] = Prado::localize('ERREUR_ENREGISTREMENT');
                $this->afficherMessagesValidationServeur($param, false, $arrayMsgErreur, '');
            }
        }
    }

    public function getLibelleTypeProcedure()
    {
        if ($this->_mapa instanceof CommonTypeProcedureOrganisme) {
            return $this->_mapa->getLibelleTypeProcedure();
        } else {
            return '';
        }
    }

    public function getAbbreviation()
    {
        if ($this->_mapa instanceof CommonTypeProcedureOrganisme) {
            return $this->_mapa->getAbbreviation();
        } else {
            return '';
        }
    }

    public function isBaremeInferieure()
    {
        if ($this->_mapa instanceof CommonTypeProcedureOrganisme) {
            if ($this->_mapa->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                return true;
            } elseif ($this->_mapa->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')) {
                return false;
            }
        } else {
            return true;
        }
    }

    public function isMapaActif()
    {
        if ($this->_mapa instanceof CommonTypeProcedureOrganisme) {
            if ($this->_mapa->getActiverMapa() == Atexo_Config::getParameter('MAPA_ACTIF')) {
                return true;
            } elseif ($this->_mapa->getActiverMapa() == Atexo_Config::getParameter('MAPA_VEROUIILLE')) {
                return false;
            }
        } else {
            return true;
        }
    }

    /***
     * Fonction permet la creation des referentiels etape et transitions pour le type de
     * procedure en parametre et un organisme
     */
    public function InsertReferentielCalendrier($idTypeProcedure, $acronyme, $connexionCom)
    {
        $requete = "SELECT * FROM t_calendrier_etape_referentiel WHERE ORGANISME = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' and ID_TYPE_PROCEDURE = '1' ";

        $statement = $connexionCom->query($requete, PDO::FETCH_ASSOC);
        foreach ($statement as $res) {
            $res['ORGANISME'] = $acronyme;
            $res['ID_TYPE_PROCEDURE'] = $idTypeProcedure;
            $champs = '';
            $valeurs = '';
            $i = 0;
            foreach ($res as $key => $value) {
                if ($i > 0) {
                    $champs .= ',';
                    $valeurs .= ',';
                }
                $champs .= '`'.$key.'`';
                $valeurs .= "'".addslashes($value)."'";
                ++$i;
            }
            $reqEtape = 'insert into t_calendrier_etape_referentiel ('.$champs.') VALUES ('.$valeurs.')';
            $connexionCom->query($reqEtape);
        }
        // valeur par defaut des transitions Mapa BOW
        $defautlParam = [0 => '-30', 1 => '1', 2 => '15', 3 => '1', 4 => '3', 5 => '23', 6 => '0'];
        $requeteTransition = "SELECT * FROM t_calendrier_transition_referentiel WHERE ORGANISME = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' and ID_TYPE_PROCEDURE = '1'";
        $statementTransition = $connexionCom->query($requeteTransition, PDO::FETCH_ASSOC);
        $j = 0;
        // insertions des transitions
        foreach ($statementTransition as $res) {
            $res['ORGANISME'] = $acronyme;
            $res['ID_TYPE_PROCEDURE'] = $idTypeProcedure;
            $res['VALEUR_FIXE'] = 0;
            $res['VALEUR_VARIABLE'] = $defautlParam[$j];
            $champs = '';
            $valeurs = '';
            $i = 0;
            foreach ($res as $key => $value) {
                if ($i > 0) {
                    $champs .= ',';
                    $valeurs .= ',';
                }
                $champs .= '`'.$key.'`';
                $valeurs .= "'".addslashes($value)."'";
                ++$i;
            }
            $reqTransition = 'insert into t_calendrier_transition_referentiel ('.$champs.') VALUES ('.$valeurs.')';
            $connexionCom->query($reqTransition);
            ++$j;
        }
    }

    /**
     * cree la relation entre type contrat et type procedure.
     *
     * @param CommonTypeProcedureOrganisme $typeProcedure type procedure
     * @param PDO                          $connexion     A database connection
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function parametrerTypeContrat($typeProcedure, $connexion)
    {
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            $typeContratQuery = new CommonTTypeContratQuery();
            $typesContrats = (array) $typeContratQuery->find($connexion);
            if (is_array($typesContrats)) {
                foreach ($typesContrats as $typeContrat) {
                    if ($typeContrat instanceof CommonTTypeContrat) {
                        $tptc = new CommonTTypeContratEtProcedureQuery();
                        $relation = $tptc->filterByIdTypeContrat($typeContrat->getIdTypeContrat())->filterByOrganisme($typeProcedure->getIdTypeProcedure())->filterByIdTypeProcedure($typeProcedure->getOrganisme())->findOne($connexion);
                        if (!$relation instanceof CommonTTypeContratEtProcedure) {
                            $relation = new CommonTTypeContratEtProcedure();
                            $relation->setOrganisme($typeProcedure->getOrganisme());
                            $relation->setIdTypeProcedure($typeProcedure->getIdTypeProcedure());
                            $relation->setIdTypeContrat($typeContrat->getIdTypeContrat());
                            $relation->save($connexion);
                        }
                    }
                }
            }
        }
    }

    public function validerDonneesMapa($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $scriptJs = '';
        if (!trim($this->libelleMapa->getText())) {
            $isValid = false;
            $arrayMsgErreur[] = Prado::localize('NOM_MAPA_OBLIGATOIRE');
            $scriptJs .= "document.getElementById('".$this->ImgErrorLibelleMapa->getClientId()."').style.display = '';";
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorLibelleMapa->getClientId()."').style.display = 'none';";
        }

        $sigle = trim($this->sigle->getText());
        if (!$sigle) {
            $isValid = false;
            $arrayMsgErreur[] = Prado::localize('DEFINE_TEXT_SIGLE_EA_PMI');
            $scriptJs .= "document.getElementById('".$this->ImgErrorSigle->getClientId()."').style.display = '';";
        } else {
            $idTypeProc = null;
            if ($this->getMapaToUpdate() instanceof CommonTypeProcedureOrganisme) {
                $idTypeProc = $this->getMapaToUpdate()->getIdTypeProcedure();
            }
            $typeProcedure = Atexo_Consultation_ProcedureType::getTypeProcedureOrganismeByAbbreviation($sigle, Atexo_CurrentUser::getCurrentOrganism(), $idTypeProc);
            if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
                $isValid = false;
                $arrayMsgErreur[] = Prado::localize('SIGLE_EXISTE_DEJA');
                $scriptJs .= "document.getElementById('".$this->ImgErrorSigle->getClientId()."').style.display = '';";
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgErrorSigle->getClientId()."').style.display = 'none';";
            }
        }
        $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs);
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param $isValid
     * @param $arrayMsgErreur
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs)
    {
        if ($isValid) {
            $param->IsValid = true;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='none'";
        } else {
            $param->IsValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }
        }
        $this->javascript->Text = "<script>$scriptJs</script>";
    }
}
