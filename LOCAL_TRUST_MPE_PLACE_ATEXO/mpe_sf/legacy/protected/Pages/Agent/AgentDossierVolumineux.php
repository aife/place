<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

class AgentDossierVolumineux extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('ModuleEnvol')) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
        }
        if($this->isMAMPEnabled()){
            MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
            MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
        }
        $this->labelDossierVolumineux->Text = Prado::localize('DOSSIER_VOLUMINEUX');
        $this->labelMessage->Text = Prado::localize('DOSSIER_VOLUMINEUX_MESSAGE');
    }
    public function isCmsActif(): string
    {
        return $this->isMAMPEnabled() ? 'visible;' : 'none;';
    }

    /**
     * @return string
     */
    public function isCmsInactif(): string
    {
        return !$this->isMAMPEnabled() ? 'visible;' : 'none;';
    }
    public function getModeServeurCrypto()
    {
        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            || ($this->isAgent() && Atexo_CurrentUser::getId() && Atexo_Module::isEnabled('ModeApplet'))
        ) {
            return 0;
        } else {
            return 1;
        }
    }
}
