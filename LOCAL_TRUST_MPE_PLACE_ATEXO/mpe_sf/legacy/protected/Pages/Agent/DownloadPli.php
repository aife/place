<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;
use ZipArchive;

/**
 * Permet de télécharger le DCE.
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadPli extends DownloadFile
{
    public ?string $_typeSignature = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $fileName = null;
        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
            $zipFileName = Atexo_Util::atexoHtmlEntities($_GET['nomFichier']);
            $directory = Atexo_Config::getParameter('COMMON_TMP').session_name().session_id().time().md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
            if (isset($_GET['id'])) {
                /* Recherche de la consultation via la méthode Atexo_Consultation::search pour voir si l'utilisateur a accés
                 à la consultation */
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($organisme);
                $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);

                if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                    /* Si une consultation trouvé il faut vérifier si un des état de la consultation est égale à
                     ouverture net analyse ou décision */
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') ||
                    $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                    || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                    || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                        /*
                         - Le paramètre $_GET['idEnveloppe'] permette d'identifier l'enveloppe à telecharger
                         - Le paramètre $_GET['nomFichier'] contient le nom du zip principal, celui qui sera telechargé
                         - Le paramètre $_GET['nomEntreprise'] permet de savoir si les fichiers déposé seront mis dans le zip principal
                         en gardant leur nom d'origine (si nom entreprise visible dans le module OA) ou modifier le nom si le nom de l'entreprise est invisble
                         - Le paramètre $_GET['typeEnv'] permet d'identifier le type d'enveloppe, ça évite d'avoir à aller chercher
                         l'enveloppe de la base de données
                         */
                        $idEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['idEnveloppe']);
                        $entrepriseName = Atexo_Util::atexoHtmlEntities($_GET['nomEntreprise']);
                        $typeEnv = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);

                        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                        $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
                        if ($enveloppe instanceof CommonEnveloppe) {
                            $offre = CommonOffresPeer::retrieveByPK($enveloppe->getOffreId(), $organisme, $connexionCom);
                            if ($offre instanceof CommonOffres) {
                                $xmlStr = $offre->getXmlString();
                                $pos = strpos($xmlStr, 'typeSignature');
                                if ($pos > 0) {
                                    $this->_typeSignature = 'XML';
                                }
                            }
                        }

                        $atexoBlob = new Atexo_Blob();
                        //Récupération des fichiers composant l'enveloppe téléchargé
                        $fichiers = (new Atexo_Consultation_Responses())->retrieveEnveloppeFiles($idEnveloppe, $organisme);
                        if ($fichiers && is_array($fichiers)) {
                            //Si au moins un fichier trouvé

                            //Création du repertoire où sera mis le fichier zip principal
                            //$directory=Atexo_Config::getParameter('COMMON_TMP').session_name().session_id().time().md5(uniqid(mt_rand(), true))."/";
                            mkdir($directory);

                            //Si plusieurs fichiers trouvés
                            if (count($fichiers) > 1) {
                                //Variable servant à énumerer les fichiers de type AUTRE
                                $numeroFichierAutre = 1;
                                /*Tableau contenant la liste des fichiers mis dans le zip princiapl, permet de savoir si deux fichiers portant le même nom
                                 ont été mis dans le zip , si oui un numéro ($numeroFichierMemeNom) est ajouté avant l'extension du fichier pour faire la différence */
                                $filesAddedToZip = [];
                                $numeroFichierMemeNom = 1;
                                //$zipName = "zip_" . md5(uniqid(rand(), true)) . ".zip";
                                $zip = new ZipArchive();
                                $res = $zip->open($directory.$zipFileName, ZipArchive::CREATE);
                                if (true === $res) {
                                    foreach ($fichiers as $fichier) {
                                        //Si le nom de l'enreprise est visible ou le paramètre NomEntrepriseToujoursVisible est à 1 (toujours afficher le nom d'entreprise)
                                        if ('' != $entrepriseName || '-' != $entrepriseName || Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
                                            //On garde le vrai nom du fichier
                                            $fileName = $fichier->getNomFichier();
                                            $originalFileName = true;
                                        } else {
                                            $originalFileName = false;
                                            // Si nom entreprise nom visible les fichiers déposés sont renomés selon leur type et leur catégorie
                                            if ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_FICHIER_PRINCIPAL')) {
                                                if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                                                    $fileName = Prado::localize('ENVELOPPE_CANDIDATURE');
                                                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                                                    $fileName = Prado::localize('ENVELOPPE_OFFRE');
                                                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                                                    $fileName = Prado::localize('ENVELOPPE_ANONYMAT');
                                                }
                                            } elseif ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_FICHIER_ACTE_ENGAGEMENT')) {
                                                $fileName = Prado::localize('ACTE_ENGAGEMENT');
                                            } else {
                                                $fileName = Prado::localize('AUTRE_FICHIER').$numeroFichierAutre;
                                                ++$numeroFichierAutre;
                                            }
                                        }

                                        $infosFile = pathinfo($fichier->getNomFichier());
                                        //Si le nom du fichier a été modifié il faut garder la même extension que le vrai fichier
                                        if (!$originalFileName) {
                                            $fileName = $fileName.'.'.$infosFile['extension'];
                                        }

                                        //Si un même fichier a déjà été inséré ajouter un numéro avant l'extension
                                        if (isset($filesAddedToZip[$fileName])) {
                                            $fileName = $infosFile['filename'].'_'.$numeroFichierMemeNom.'.'.$infosFile['extension'];
                                            ++$numeroFichierMemeNom;
                                        }
                                        $fileName = Atexo_Util::replaceCharactersMsWordWithRegularCharacters($fileName);
                                        $fileName = Atexo_Util::encodeToOsVisiteur($fileName);
                                        //Création d'un fichier contenant la signature du fichier en cours
                                        //$signatureFile=$directory.$fileName." - Signature 1.p7s";
                                        $signature = $fichier->getSignatureFichier();

                                        if ((!$fichier->getIdFichierSignature()) && '' != $signature) {
                                            if ('XML' == $this->_typeSignature) {
                                                $jetonName = $fichier->getJetonFileName(true, $fileName);
                                                $zip->addFromString($jetonName, base64_decode($signature));
                                            } else {
                                                $zip->addFromString($fileName.' - Signature 1.p7s', $signature);
                                            }
                                            //Atexo_Util::write_file($signatureFile, $signature);
                                        }
                                        $blobresource = $atexoBlob->acquire_lock_on_blob($fichier->getIdBlob(), $organisme);
                                        $zip->addFile($blobresource['blob']['pointer'], $fileName);
                                        //Création du fichier
                                        //$atexoBlob->copyBlob($fichier->getIdBlob(), $directory.$fileName, $organisme);

                                        //Insertion du fichier de la signature dans le zip
                                        /*Atexo_Zip::addFileToZip($directory.$fileName, $directory.$zipFileName);
                                         if(is_file($signatureFile)) {
                                         Atexo_Zip::addFileToZip($signatureFile, $directory.$zipFileName);
                                         }*/
                                        $filesAddedToZip[$fileName] = $fileName;
                                    }
                                    $zip->close();
                                }
                            } else {
                                $fichier = array_shift($fichiers);
                                if ('-' != $entrepriseName || Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
                                    $fileName = $fichier->getNomFichier();
                                    $originalFileName = true;
                                } else {
                                    $originalFileName = false;
                                    if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                                        $fileName = Prado::localize('ENVELOPPE_CANDIDATURE');
                                    } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                                        $fileName = Prado::localize('ENVELOPPE_OFFRE');
                                    } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                                        $fileName = Prado::localize('ENVELOPPE_ANONYMAT');
                                    }
                                }
                                if (!$originalFileName) {
                                    $infosFile = pathinfo($fichier->getNomFichier());
                                    $fileName = $fileName.'.'.$infosFile['extension'];
                                }
                                if ('XML' == $this->_typeSignature) {
                                    $signature = trim($fichier->getSignatureFichier());
                                } else {
                                    $signature = "-----BEGIN PKCS7-----\n".trim($fichier->getSignatureFichier())."\n-----END PKCS7-----";
                                }
                                $atexoBlob = new Atexo_Blob();
                                $blobresource = $atexoBlob->acquire_lock_on_blob($fichier->getIdBlob(), $organisme);
                                $zip = new ZipArchive();
                                $res = $zip->open($directory.$zipFileName, ZipArchive::CREATE);
                                $fileName = Atexo_Util::replaceCharactersMsWordWithRegularCharacters($fileName);
                                $fileName = Atexo_Util::encodeToOsVisiteur($fileName);

                                if (true === $res) {
                                    if ((!$fichier->getIdFichierSignature()) && '' != $signature) {
                                        if ('XML' == $this->_typeSignature) {
                                            $jetonName = $fichier->getJetonFileName(true, $fileName);
                                            $zip->addFromString($jetonName, base64_decode($signature));
                                        } else {
                                            if ("-----BEGIN PKCS7-----\n\n-----END PKCS7-----" != $signature) {
                                                $zip->addFromString($fileName.' - Signature 1.p7s', $signature);
                                            }
                                        }
                                    }
                                    $zip->addFile($blobresource['blob']['pointer'], $fileName);
                                    $zip->close();
                                } else {
                                    // TODO Gestion erreur dans la création du zip
                                }
                            }
                            if (Atexo_Util::isSafeFileName($zipFileName) && Atexo_Util::isSafePath($directory, $zipFileName)) {
                                header('Pragma: public');
                                header('Expires: 0');
                                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                                header('Cache-Control: private', false);
                                header('Content-Type: application/octet-stream');
                                header('Content-Disposition: attachment; filename="'.$zipFileName.'";');
                                header('Content-Transfer-Encoding: binary');
                                //header("Content-Length: " . filesize($directory.$zipFileName));
                                $fp = fopen($directory.$zipFileName, 'rb');
                                while (!feof($fp) && 0 == connection_status()) {
                                    set_time_limit(0);
                                    echo fread($fp, 1024 * 8);
                                    flush();
                                    ob_flush();
                                }
                                fclose($fp);
                                Atexo_Util::removeDirectoryRecursively($directory);
                                exit;
                            } else {
                                echo '0';
                            }
                        }
                    } else {
                        echo '1';
                    }
                } else {
                    echo '2';
                }
            } else {
                echo '3';
            }
        } else {
            echo '4';
        }
    }
}
