<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Propel\Mpe\CommonDocumentExternePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_DocumentExterneVo;
use Prado\Prado;

/**
 * Class pour le suivi et l'ajout de documents externes.
 *
 * @author Clément Deust <clement.deust@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchiveSuiviDocsExterne extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function getReference()
    {
        return Atexo_Util::atexoHtmlEntities($_GET['id']);
    }

    public function onLoad($param)
    {
        $this->retour->attachEventHandler('OnClick', [$this, 'retour']);
        if (!$this->IsPostBack) {
            $criteriaVo = new Atexo_Consultation_DocumentExterneVo();
            $criteriaVo->setRefConsultation($this->getReference());
            $this->dataForDocsExterneSearchResult($criteriaVo);
        }
    }

    public function dataForDocsExterneSearchResult(Atexo_Consultation_DocumentExterneVo $criteriaVo)
    {
        $this->fillRepeaterWithDataForDocsExterneSearchResult($criteriaVo);
    }

    public function retour()
    {
        if (isset($_GET['archiveRappel']) && true == $_GET['archiveRappel']) {
            $this->response->Redirect('?page=Agent.ArchiveRappelTableauDeBord&id='.$this->getReference());
        }
        if (isset($_GET['detail'])) {
            $this->response->Redirect('?page=Agent.DetailConsultation&id='.$this->getReference());
        } else {
            $this->response->Redirect('?page=Agent.ArchiveTableauDeBord&id='.$this->getReference());
        }
    }

    public function fillRepeaterWithDataForDocsExterneSearchResult($criteriaVo)
    {
        $nombreElement = null;
        $this->setViewState('CriteriaVo', $criteriaVo);
        $consultation = new Atexo_Consultation();
        $documentExterne = $consultation->searchDocument($criteriaVo);
        if ('0' != $nombreElement) {
            $this->setViewState('nombreElement', $documentExterne);
            $this->ArchiveTableauDocumentRepeater->setVisible(true);
            $this->ArchiveTableauDocumentRepeater->setVirtualItemCount($documentExterne);
            $this->ArchiveTableauDocumentRepeater->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        }
    }

    public function downloadDocExterneFile($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $blob_id = $param->CommandParameter;
        $fileName = $param->CommandName;
        $blob = new Atexo_Blob();
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Type: application/octet-stream');
        header('Content-Tranfert-Encoding: binary');
        $blob->sendBlobToClient($blob_id, $fileName, $org);
        exit;
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->ArchiveTableauDocumentRepeater->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function populateData(Atexo_Consultation_DocumentExterneVo $criteriaVo)
    {
        $offset = $this->ArchiveTableauDocumentRepeater->CurrentPageIndex * $this->ArchiveTableauDocumentRepeater->PageSize;
        $limit = $this->ArchiveTableauDocumentRepeater->PageSize;
        if ($offset + $limit > $this->ArchiveTableauDocumentRepeater->getVirtualItemCount()) {
            $limit = $this->ArchiveTableauDocumentRepeater->getVirtualItemCount() - $offset;
        }
        $consultation = new Atexo_Consultation();
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayDocumentExterne = $consultation->searchDocument($criteriaVo);
        $this->ArchiveTableauDocumentRepeater->DataSource = $arrayDocumentExterne;
        $this->ArchiveTableauDocumentRepeater->dataBind();
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function deleteDocExterne($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idDoc = $param->CommandParameter;
        CommonDocumentExternePeer::doDelete([$idDoc, $org], $connexion);
        $this->populateData($this->getViewState('CriteriaVo'));
    }

    public function refresh($sender, $param)
    {
        $this->activePanel->render($param->NewWriter);
    }

    public function truncate($texte)
    {
        $truncatedText = null;
        $maximumNumberOfCaracterToReturn = 65;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 60) ? '' : 'display:none';
    }

    public function refreshRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->ArchiveTableauDocumentRepeater->setCurrentPageIndex(0);
        $this->fillRepeaterWithDataForDocsExterneSearchResult($criteriaVo);
        $this->populateData($criteriaVo);
    }

    /*
     *  Permet d'avoir le lot lié au document
     *
     *  @param CommonDocumentExterne $document le document concerne
     *  @return String le lot concerne
     *  @author : loubna.ezziani@atexom.com
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function getLotDocument($document)
    {
        if ($document->getLot()) {
            return Prado::localize('TEXT_LOT').' '.$document->getLot();
        } else {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->getReference());
            if (($consultation instanceof CommonConsultation) && $consultation->getAlloti()) {
                return Prado::localize('DEFINE_TOUS');
            }
        }

        return '-';
    }
}
