<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_FormulaireVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_MesuresAvancement;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Nabil BELLAHCEN <nabil.bellahcen@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupBoampMesureAvancement extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idService'])) {
            $this->displayAcheteurPublic(Atexo_Util::atexoHtmlEntities($_GET['idService']));
        }
    }

    /*
     * Permet de recuperer la liste des acheteurs publics associés à un service
     */
    public function displayAcheteurPublic($idService)
    {
        $listAcheteur = (new Atexo_PublicPurchaser())->retrievePublicPurchasersCons($idService, $idService);
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        foreach ($listAcheteur as $oneAcheteur) {
            $siteBoamp = '';
            $mail = '';
            if ('0' == $oneAcheteur->getBoampTarget()) {
                $siteBoamp = ' - '.Prado::localize('TEXT_DEMONSTRATION');
            } else {
                $siteBoamp = ' - '.Prado::localize('TEXT_PRODUCTION');
            }
            if ('' != $oneAcheteur->getBoampMail()) {
                $mail = ' - '.$oneAcheteur->getBoampMail();
            }
            $dataSource[$oneAcheteur->getId()] = $oneAcheteur->getDenomination().$mail.$siteBoamp;
        }
        $this->acheteurPublic->DataSource = $dataSource;
        $this->acheteurPublic->DataBind();
    }

    /*  Envoi du formlaire XML au BOAMP
    *
    *
    */
    public function sendBoamp($sender, $param)
    {
        $this->panelMessageErreur->setMessage('');
        $this->panelMessageErreur->setVisible(false);
        $identifiant = $this->acheteurPublic->SelectedValue;
        $idService = Atexo_Util::atexoHtmlEntities($_GET['idService']);

        if (isset($idService) && isset($identifiant) && isset($_GET['idMesure'])) {
            $idMesure = Atexo_Util::atexoHtmlEntities($_GET['idMesure']);
            $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($identifiant, Atexo_CurrentUser::getOrganismAcronym());
            if ($acheteurPublic) {
                $formulaireVo = new Atexo_Publicite_FormulaireVo();

                $authCode = base64_encode($acheteurPublic->getBoampLogin().':'.$acheteurPublic->getBoampPassword());
                $formulaireVo->setAuthentificate($authCode);
                $nameChamp = 'File';
                $formulaireVo->setNameChamp($nameChamp);
                $fileName = (new Atexo_Publicite_Boamp())->getBoampXmlName($identifiant, Atexo_CurrentUser::getOrganismAcronym());

                $formulaireVo->setNameFile($fileName);
                $formulaireVo->setUrlHost((new Atexo_Publicite_Boamp())->boampHost());
                $formulaireVo->setUrlSendPath((new Atexo_Publicite_Boamp())->boampSendPath($acheteurPublic));

                // partie reservée pr l'envois BOAMP mesre demat.
                $MesureAvancement = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementById($idMesure, Atexo_CurrentUser::getCurrentOrganism());
                if ($MesureAvancement->getXml()) {
                    $formulaireVo->setXml(utf8_encode($MesureAvancement->getXml()));

                    $formulaireVo->setLibelleService((new Atexo_EntityPurchase())->getEntityPathById($idService, Atexo_CurrentUser::getCurrentOrganism(), true, true));
                    $formulaireVo->setAnnee($MesureAvancement->getAnnee());
                    $formulaireVo->setTrimestre($MesureAvancement->getTrimestre());
                    $fileNameGenere = 'MesureDematEnvoiBoamp_'.$idMesure.'_'.$idService.'_'.Atexo_CurrentUser::getCurrentOrganism().'_'.$MesureAvancement->getAnnee().'_'.$MesureAvancement->getTrimestre().'_'.date('YmdHis').'.log';

                    $formulaireVo->setNameFile($fileName);
                    $response = [];
                    $response = (new Atexo_Publicite_Boamp())->sendXml($formulaireVo, true);

                    if (true == $response[0]) {
                        if ((new Atexo_Statistiques_MesuresAvancement())->UpdateEtatFormulaire(Atexo_CurrentUser::getCurrentOrganism(), $idMesure, Atexo_Config::getParameter('ETAT_ENVOYE_BOAMP_MESURE_DEMAT'), $fileNameGenere)) {
                            $this->scriptClose->Text = "<script>javascript:window.close();window.opener.document.getElementById('ctl0_CONTENU_PAGE_IdAnnuler').click();</script>";
                        } else {
                            $this->panelMessageErreur->setMessage('<div class="line"><div>'.Prado::localize('ECHEC_SAUV_ETAT_BOAMP_FOURMULAIRE_DEMAT').'</div> </div>');
                            $this->panelMessageErreur->setVisible(true);
                        }
                    } else {
                        $this->panelMessageErreur->setMessage('<div class="line"><div>'.Prado::localize('ECHEC_ENVOI_BOAMP_FOURMULAIRE_AVANCEMENT').'</div> </div>');
                        $this->panelMessageErreur->setVisible(true);
                    }
                }
            }
        } else {
            $this->panelMessageErreur->setMessage('<div class="line"><div>'.Prado::localize('ECHEC_ENVOI_BOAMP_FOURMULAIRE_AVANCEMENT').'</div> </div>');
            $this->panelMessageErreur->setVisible(true);
        }
    }
}
