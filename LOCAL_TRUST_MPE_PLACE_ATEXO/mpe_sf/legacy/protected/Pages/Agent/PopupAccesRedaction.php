<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Exception;
use Prado\Prado;

/**
 * Popup.
 *
 * @author  Ayoub SOUID AHMED<ayoub.souidahmed@atexo.com>
 * @copyright Atexo
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupAccesRedaction extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        header('Pragma:nocache');
        $consRef = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $consultation = $this->retrieveConsultation($consRef);
        if ($consultation instanceof CommonConsultation) {
            $this->gotoRedaction($consultation);
        }
    }

    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function gotoRedaction($consultation)
    {
        try {
            $msgLog = '';
            $messageErreur = '';
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
            $redaction = new Atexo_Redaction($consultation, $agent);
            if ($redaction->ticket) {
                $ticket = $redaction->intialiseConsultation();
                if ($ticket) {
                    $urlReditect = Atexo_Config::getParameter('URL_RSEM_GESTION_DOCUMENTS_CONSULTATION')
                        . '?identifiantSaaS=';
                    $urlReditect .= $ticket;
                    $this->script->setText("<script>window.showLoader();window.location.replace('"
                        . $urlReditect . "');</script>");
                } else {
                    if ($redaction->codeErreur && $redaction->messageErreur) {
                        $msgLog = 'Code erreur ' . $redaction->codeErreur . ' : ' . $redaction->messageErreur;
                    }
                    $messageErreur = $redaction->messageErreur;
                }
            } else {
                if ($redaction->codeErreur && $redaction->messageErreur) {
                    $msgLog = 'Code erreur ' . $redaction->codeErreur . ' : ' . $redaction->messageErreur;
                }
                $messageErreur = $redaction->messageErreur;
            }
        } catch (Exception $e) {
            $messageErreur = Prado::localize('DEFINE_ERREUR_REDAC');
            $msgLog = $e->getMessage();
        }
        if ('' != $messageErreur) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->script->setText('<script>window.showLoader();</script>');
        }
        if ('' != $msgLog) {
            $filePath = 'mpeRsemAcces.log';
            $log = '[ ' . date('d-M-Y H:i:s') . ' ] ' . $msgLog . "\n";
            Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR') . 'logs/' . $filePath, $log, 'a');
            $this->script->setText('<script>window.showLoader();</script>');
        }
    }
}
