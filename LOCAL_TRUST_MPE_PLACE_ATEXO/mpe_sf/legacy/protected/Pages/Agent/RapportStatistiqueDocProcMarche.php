<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

class RapportStatistiqueDocProcMarche extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $uri = '/reports/ORME_STAT/Doc_TypeProc_CatMarch_Statut';
        $this->criteresStatistiques->setUri($uri);
        $this->criteresStatistiques->setNomStat(Prado::localize('TEXT_DOCUMENTS'));
    }
}
