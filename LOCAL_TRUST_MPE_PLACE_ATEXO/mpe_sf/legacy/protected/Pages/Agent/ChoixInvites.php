<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_CriteriaVo;

/**
 * Classe de la page des invités d'une consultation.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ChoixInvites extends MpeTPage
{
    private string $_nom = '';
    private string $_pole = '';
    private array $critereTri = [];
    private array $triAscDesc = [];

    public function setCriteriaVoGuests($value)
    {
        $this->setViewState('CriteriaVoGuests', $value);
    }

    public function getCriteriaVoGuests()
    {
        return $this->getViewState('CriteriaVoGuests');
    }

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->setServicesAndNbrResults();
            //On affiche par défaut les invités du pôle de l'agent connecté.
            $idServiceAgent = Atexo_CurrentUser::getCurrentServiceId();
            if (!empty($idServiceAgent)) {
                $this->entiteAchat->SelectedValue = $idServiceAgent;
            }

            $arrayGuests = [];
            if (isset($_GET['inv'])) {
                $arrayGuests = (new Atexo_Consultation_Guests())->createArrayGuestFromValueString(Atexo_Util::atexoHtmlEntities($_GET['inv']));
            }
            if (!is_array($arrayGuests)) {
                $arrayGuests = [];
            }
            $guests = Atexo_Consultation_Guests::toArrayGuestsVo($arrayGuests); //Atexo_CurrentUser::readFromSession("invitesChoisis"));
            $this->setViewState('invitesChoisis', $arrayGuests);

            $this->onSearchClick();
            // La liste des invités sélectionnés
            /*if(Atexo_CurrentUser::readFromSession("invitesChoisis"))
            {
               $guests = Atexo_Consultation_Guests::toArrayGuestsVo(Atexo_CurrentUser::readFromSession("invitesChoisis"));
            } */
            $this->RepeaterUtilisateursEntite->dataSource = $guests;
            $this->RepeaterUtilisateursEntite->dataBind();

            $this->setViewState('nbreInvites', is_countable($guests) ? count($guests) : 0);
            self::hideComponent();
        }
    }

    /**
     * Remplir la liste des combo box.
     */
    public function setServicesAndNbrResults()
    {
        // Remplir la liste des pôles
        $services = Atexo_EntityPurchase::getEntityPurchase(
            Atexo_CurrentUser::getOrganismAcronym(),
            true,
            Atexo_CurrentUser::readFromSession('lang')
        );
        $this->entiteAchat->dataSource = $services;
        $this->entiteAchat->dataBind();
        $this->entiteAchat->SelectedValue = Atexo_CurrentUser::getCurrentServiceId();
        Atexo_CurrentUser::writeToSession('services', $services);
    }

    /**
     * Recherche des invités.
     */
    public function onSearchClick()
    {
        $guests = null;
        // la liste de la selection comprenne les invités permanent
        $this->_nom = $this->userName->Text;
        $this->_pole = $this->entiteAchat->SelectedValue;

        $arrayInvites = $this->getViewState('invitesChoisis');

        $excludedPermanentsGuests = [];
        if (is_array($arrayInvites)) {
            $invite = '';
            foreach ($arrayInvites as $idInvite => $invite) {
                $excludedPermanentsGuests[$idInvite] = $idInvite;
            }
        }
        $excludedPermanentsGuests = array_keys($excludedPermanentsGuests);
        $criteriaVo3 = new Atexo_Consultation_Guests_CriteriaVo();
        $pole = $this->_pole == '0' ? null : $this->_pole;
        $criteriaVo3->setCurAgentServiceId($pole);
        $criteriaVo3->setOrgAcronym(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo3->setName($this->_nom);
        if (is_array($excludedPermanentsGuests)) {
            $criteriaVo3->setExludedAgents($excludedPermanentsGuests);
        }

        $this->setCriteriaVoGuests($criteriaVo3);
        $allGuests = Atexo_Consultation_Guests::searchNonPermanentGuests($criteriaVo3);
        $numberOfGuests = is_countable($allGuests) ? count($allGuests) : 0;

        $this->NombreResultats->Text = $numberOfGuests;
        $nombreElement = $numberOfGuests;

        $this->setViewState('invites', $allGuests);

        if (!is_array($allGuests)) {
            $allGuests = [];
        }
        $this->setViewState('nbrElements', $nombreElement);
        if ($nombreElement) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterResultats->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterResultats->PageSize);
            $this->RepeaterResultats->setVirtualItemCount($nombreElement);
            $this->RepeaterResultats->setCurrentPageIndex(0);
            $this->RepeaterResultats->dataSource = $allGuests;
            $this->RepeaterResultats->dataBind();
            self::showComponent();
        //self::populateData($criteriaVo3);
        } else {
            //$this->setViewState("listeAgents",array());
            $this->RepeaterResultats->DataSource = [];
            $this->RepeaterResultats->DataBind();
            self::hideComponent();
        }

        if ($arrayInvites) {//Atexo_CurrentUser::readFromSession("invitesChoisis"))
            $guests = Atexo_Consultation_Guests::toArrayGuestsVo($arrayInvites); //Atexo_CurrentUser::readFromSession("invitesChoisis"));
        }
        if (!is_array($guests)) {
            $guests = [];
        }
        $this->RepeaterUtilisateursEntite->dataSource = $guests;
        $this->RepeaterUtilisateursEntite->dataBind();
    }

    /**
     * Supprimer un invité.
     */
    public function onDeleteClick($sender, $param)
    {
        $guests = null;
        if ('delete' == $param->CommandName) {
            $idInvite = $param->CommandParameter;
            $arrayInvites = $this->getViewState('invitesChoisis');
            unset($arrayInvites[$idInvite]);
            $this->setViewState('invitesChoisis', $arrayInvites);
            //Atexo_CurrentUser::deleteFromSession("invitesChoisis", $idInvite);
            if ($arrayInvites) {//Atexo_CurrentUser::readFromSession("invitesChoisis"))
                $guests = Atexo_Consultation_Guests::toArrayGuestsVo($arrayInvites); //Atexo_CurrentUser::readFromSession("invitesChoisis"));
            }
            if (!is_array($guests)) {
                $guests = [];
            }
            $this->RepeaterUtilisateursEntite->dataSource = $guests;
            $this->RepeaterUtilisateursEntite->dataBind();
        }
        $this->onSearchClick();
    }

    /**
     *  Ajouter un invité à la selection.
     */
    public function onAddClick()
    {
        $arrayInvites = $this->getViewState('invitesChoisis');
        foreach ($this->RepeaterResultats->getItems() as $item) {
            $idInvite = $this->RepeaterResultats->DataKeys[$item->ItemIndex];
            if ($item->inviteSelection_1->checked) {
                // On ajoute l'invité à la selection
                $inviteArray = [];
                if (0 == $item->permanentInvitation->Value) {
                    $suiviSeul = 1;
                    $suiviHabilitation = 0;
                } else {
                    $suiviSeul = 0;
                    $suiviHabilitation = 1;
                }
                $inviteArray = [$idInvite => ['Nom' => $item->nomInvite->Text,
                                                             'Prenom' => $item->prenomInvite->Text,
                                                             'InvitePermanent' => $item->permanentInvitation->Value,
                                                             'SuiviSeul' => $suiviSeul,
                                                             'SuiviHabilitation' => $suiviHabilitation,
                                                             'EntiteCom' => $item->entite->Text,
                                                             'CheminEntite' => '',
                                                             ]];
                // ajouter à la selection des invités
                $arrayInvites[$idInvite] = $inviteArray[$idInvite];
            }
        }

        $this->setViewState('invitesChoisis', $arrayInvites);
        // Mettre à jour la liste de la selection
            $invites = Atexo_Consultation_Guests::toArrayGuestsVo($arrayInvites); //Atexo_CurrentUser::readFromSession('invitesChoisis'));

        if (!is_array($invites)) {
            $invites = [];
        }

        $this->RepeaterUtilisateursEntite->dataSource = $invites;
        $this->RepeaterUtilisateursEntite->dataBind();
        $this->onSearchClick();
    }

    /**
     * Modifie la taille de tableau de résultat des invités.
     */
    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }

        $this->RepeaterResultats->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nbrElements');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterResultats->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterResultats->PageSize);
        if ($this->pageNumberTop->Text > $this->nombrePageTop->Text) {
            $this->pageNumberTop->Text = $this->nombrePageTop->Text;
            $this->pageNumberBottom->Text = $this->nombrePageBottom->Text;
        }
        $this->RepeaterResultats->setCurrentPageIndex($this->pageNumberTop->Text - 1);
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));

        $this->populateData($this->getCriteriaVoGuests());
    }

    /**
     * Modifie la taille de tableau des invités sélectionnés.
     */
    public function changeNbrResultatsInvites()
    {
        $arrayInvites = $this->getViewState('invitesChoisis');
        if ($this->RepeaterUtilisateursEntite->PageSize != $this->nbResultsBottom->SelectedValue) {
            $this->RepeaterUtilisateursEntite->PageSize = $this->nbResultsBottom->SelectedValue;
            $this->RepeaterUtilisateursEntite->CurrentPageIndex = 0;
        }
        if ($arrayInvites) {//Atexo_CurrentUser::readFromSession("invitesChoisis"))
            $guests = Atexo_Consultation_Guests::toArrayGuestsVo($arrayInvites); //Atexo_CurrentUser::readFromSession("invitesChoisis"));
        }
        $this->RepeaterUtilisateursEntite->dataSource = $guests;
        $this->RepeaterUtilisateursEntite->dataBind();
    }

    /**
     * Trier un tableau des invités sélcetionnés.
     *
     * @param sender
     * @param param
     */
    public function sortGuests($sender, $param)
    {
        $this->critereTri[1] = $this->getViewState('critereTri1');
        $this->triAscDesc[1] = $this->getViewState('triAscDesc1');
        $guests = $this->getViewState('invites');
        if ('FirstName' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($guests, 'FirstName', '', 1);
        } elseif ('LastName' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($guests, 'LastName', '', 1);
        } elseif ('EntityPurchase' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($guests, 'EntityPurchase', '', 1);
        } else {
            return;
        }

        $this->setViewState('invites', $guests);

        $this->setViewState('critereTri1', $this->critereTri[1]);
        $this->setViewState('triAscDesc1', $this->triAscDesc[1]);

        if (!is_array($guests)) {
            $guests = [];
        }

        $this->RepeaterResultats->dataSource = $guests;
        $this->RepeaterResultats->dataBind();
        $this->panelRefresh->render($param->getNewWriter());

        $guests = $this->getViewState('invitesChoisis');
        if ($guests) {//Atexo_CurrentUser::readFromSession("invitesChoisis")) {
            $guests = Atexo_Consultation_Guests::toArrayGuestsVo($guests); //Atexo_CurrentUser::readFromSession("invitesChoisis"));
        }
        $this->RepeaterUtilisateursEntite->dataSource = $guests;
        $this->RepeaterUtilisateursEntite->dataBind();
    }

    /**
     * Trier un tableau des invités sélcetionnés.
     *
     * @param sender
     * @param param
     */
    public function sortCheckedGuests($sender, $param)
    {
        $this->critereTri[2] = $this->getViewState('critereTri2');
        $this->triAscDesc[2] = $this->getViewState('triAscDesc2');

        $guests = $this->getViewState('invitesChoisis');

        $checkedGuests = Atexo_Consultation_Guests::toArrayGuestsVo($guests); //Atexo_CurrentUser::readFromSession('invitesChoisis'));

        if ('FirstName' == $param->CommandName) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'FirstName', '', 2);
        } elseif ('LastName' == $param->CommandName) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'LastName', '', 2);
        } elseif ('EntityPurchase' == $param->CommandName) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'EntityPurchase', '', 2);
        } else {
            return;
        }

        //Atexo_CurrentUser::writeToSession("invitesChoisis",Atexo_Consultation_Guests::guestVoToArrayGuests($dataSourceTriee[0]));
        $this->setViewState('invitesChoisis', Atexo_Consultation_Guests::guestVoToArrayGuests($dataSourceTriee[0]));

        $this->setViewState('critereTri2', $this->critereTri[2]);
        $this->setViewState('triAscDesc2', $this->triAscDesc[2]);

        $this->RepeaterUtilisateursEntite->setCurrentPageIndex(0);
        $this->RepeaterUtilisateursEntite->DataSource = $dataSourceTriee[0];
        $this->RepeaterUtilisateursEntite->DataBind();
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere, $indiceElement)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere, $indiceElement);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc[$indiceElement], 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc[$indiceElement]);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere, $indiceElement)
    {
        if ($this->critereTri[$indiceElement] != $critere || !$this->critereTri[$indiceElement]) {
            $this->triAscDesc[$indiceElement] = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc[$indiceElement]) {
                $this->triAscDesc[$indiceElement] = 'DESC';
            } else {
                $this->triAscDesc[$indiceElement] = 'ASC';
            }
        }
        $this->critereTri[$indiceElement] = $critere;
    }

    /**
     * Valider la selection des invités.
     */
    public function onValidate()
    {
        $invitesSession = [];
        //$invitesSession = Atexo_CurrentUser::readFromSession("invitesChoisis");
        $invitesSession = $this->getViewState('invitesChoisis');
        foreach ($this->RepeaterUtilisateursEntite->getItems() as $item) {
            $idInvite = '';
            $idInvite = $this->RepeaterUtilisateursEntite->DataKeys[$item->ItemIndex];
            if ($item->autreEntiteSuivi_seul_1->Checked) {
                $invitesSession[$idInvite]['SuiviSeul'] = '1';
                $invitesSession[$idInvite]['SuiviHabilitation'] = '0';
            } elseif ($item->autreEntiteSuiviHabilitation_1->Checked) {
                $invitesSession[$idInvite]['SuiviSeul'] = '0';
                $invitesSession[$idInvite]['SuiviHabilitation'] = '1';
            }
        }

        $invites = '';
        if (is_array($invitesSession)) {
            foreach ($invitesSession as $idAgent => $guest) {
                if ('1' == $guest['InvitePermanent']) {
                    $type = '1';
                } elseif ('1' == $guest['SuiviHabilitation']) {
                    $type = '2';
                } else {
                    $type = '3';
                }

                $invites .= $idAgent.'-'.$type.',';
            }
        }
        //Atexo_CurrentUser::deleteFromSession("invitesChoisis");
        //Atexo_CurrentUser::writeToSession("invitesChoisis",$invitesSession);
        //if(Atexo_CurrentUser::readFromSession("invitesChoisis"))
        //{
        //  $guests = Atexo_Consultation_Guests::toArrayGuestsVo(Atexo_CurrentUser::readFromSession("invitesChoisis"));
        // }
        //  $this->RepeaterUtilisateursEntite->dataSource =  $guests ;
        //  $this->RepeaterUtilisateursEntite->dataBind();
        $this->labelClose->Text = '';
        $this->labelClose->Text = "<script>if(opener.document.getElementById('ctl0_CONTENU_PAGE_nbrInviteAvant')) opener.setNbreInvitesAvantMAJ('".$this->getViewState('nbreInvites')."');opener.refreshOpener('$invites');window.close();</script>";
    }

    /**
     * Si on a une organisation centralisée.
     */
    public function isOrganisationCentralised()
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            return true;
        } else {
            return false;
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterResultats->CurrentPageIndex = $param->NewPageIndex;
        $this->pageNumberBottom->Text = $param->NewPageIndex + 1;
        $this->pageNumberTop->Text = $param->NewPageIndex + 1;
        $this->RepeaterResultats->PageSize = $this->nbResultsTop->getSelectedValue();
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        $this->populateData($this->getCriteriaVoGuests());
    }

    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->PagerTop->setVisible(false);
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        //$this->nbrElementRepeater = 0;
    }

    public function populateData(Atexo_Consultation_Guests_CriteriaVo $criteriaVoGuest)
    {
        $offset = $this->RepeaterResultats->CurrentPageIndex * $this->RepeaterResultats->PageSize;
        $limit = $this->RepeaterResultats->PageSize;
        if ($offset + $limit > $this->RepeaterResultats->getVirtualItemCount()) {
            $limit = $this->RepeaterResultats->getVirtualItemCount() - $offset;
        }
        $criteriaVoGuest->setLimit($limit);
        $criteriaVoGuest->setOffset($offset);

        $allGuests = Atexo_Consultation_Guests::searchNonPermanentGuests($criteriaVoGuest);
        $numberOfGuests = is_countable($allGuests) ? count($allGuests) : 0;

        if ($numberOfGuests) {
            $this->nbrElementRepeater = $this->getViewState('nbrElements');
            $this->RepeaterResultats->DataSource = $allGuests;
            $this->RepeaterResultats->DataBind();
            self::showComponent();
        } else {
            self::hideComponent();
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }
        self::toPage($numPage);
    }

    public function toPage($numPage)
    {
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterResultats->CurrentPageIndex = $numPage - 1;
            $this->pageNumberBottom->Text = $numPage;
            $this->pageNumberTop->Text = $numPage;
            $this->RepeaterResultats->PageSize = $this->nbResultsTop->getSelectedValue();
            $this->populateData($this->getCriteriaVoGuests());
        } else {
            $this->pageNumberTop->Text = $this->RepeaterResultats->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->RepeaterResultats->CurrentPageIndex + 1;
        }
    }
}
