<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use DOMDocument;

class AuthentificationProxyForMarcoAndSISForPPPEB extends MpeTPage
{
    public function onLoad($param)
    {
        if (Atexo_Config::getParameter('ENABLE_MARCO') === '0') {
            header('Location: /?page=Agent.AgentHome');
            exit();
        }

        if (Atexo_Module::isEnabled('SocleExternePpp') && !Atexo_CurrentUser::getIdAgentConnected()) {
            $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
            if ((isset($headersArray['externalId']) && isset($headersArray['userType'])) || (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE']))) {
                $externalId = $headersArray['externalId'] ?? $headersArray['EXTERNALID'];
                $userType = $headersArray['userType'] ?? $headersArray['USERTYPE'];
                Atexo_CurrentUser::writeToSession('externalId', $externalId);
                Atexo_CurrentUser::writeToSession('userType', $userType);
                if ('AGENT' == $userType) {
                    self::authenticateViaPPPSocleAgent($externalId);
                }
            }
        }
        $xmlInput = Atexo_CurrentUser::readFromSession('xml');
        $xml = base64_decode($xmlInput);
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xml);
        $transaction = $domDocument->documentElement;
        $control = $transaction->getElementsByTagName('Control')->item(0);
        $from = $control->getElementsByTagName('From')->item(0);
        $this->ipEmail->Text = $from->getAttribute('login');
        $this->password->Text = $from->getAttribute('password');
        $this->goto->Text = base64_encode(Atexo_Config::getParameter('PF_URL').'?page=Agent.FormulaireConsultation');
        $this->script->Text = '<script language="javascript">document.getElementById(\'ctl0_CONTENU_PAGE_buttonSend\').click();</script>';
    }

    public function authenticateViaPPPSocleAgent($idExterne)
    {
        // Coquille vide car authentification captée par SF
    }
}
