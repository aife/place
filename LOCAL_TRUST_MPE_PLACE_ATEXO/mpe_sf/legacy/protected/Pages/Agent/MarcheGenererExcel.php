<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use Prado\Prado;
use Spreadsheet_Excel_Writer;

/*
 * Created on 3 nov. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class MarcheGenererExcel extends MpeTPage
{
    public $_annee;
    public $_idServiceAgent;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['annee'])) {
            $this->setViewState('annee', Atexo_Util::atexoHtmlEntities($_GET['annee']));
        }
        if (isset($_GET['toPdf'])) {
            $filePath = $this->genererFichierExcel(true);
            $nomFichier = 'Extrait_Article_133_'.$this->getAnnee().'.pdf';
            if ($filePath) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
                @unlink($filePath);
                DownloadFile::downloadFileContent($nomFichier, $pdfContent);
            }
        } else {
            $this->genererFichierExcel();
            exit;
        }
    }

    public function getAnnee()
    {
        if (null == $this->_annee) {
            $this->_annee = $this->getViewState('annee');
        }

        return $this->_annee;
    }

    public function getIdServiceAgent()
    {
        if (null == $this->_idServiceAgent) {
            $this->_idServiceAgent = Atexo_CurrentUser::getIdServiceAgentConnected();
        }

        return $this->_idServiceAgent;
    }

    public function genererFichierExcel($toPdf = false)
    {
        try {
            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';
            $file = Atexo_Config::getParameter('COMMON_TMP').'Extrait_Article_133_'.$this->getAnnee().'.xls';
            $nomFile = basename($file);
            //Initialisation du fichier excel
            $workbook = new Spreadsheet_Excel_Writer($file);
            $format_bold = &$workbook->addFormat();
            $format_bold->setColor('black');
            $format_bold->setSize(8);
            $format_bold->setAlign('left');
            $format_bold->setBold(1);
            // format normal
            $format = &$workbook->addFormat();
            $format->setColor('black');
            $format->setSize(8);
            $format->setAlign('left');
            $format->setalign('vjustify');
            $stylePolice10BlackSimple = &$workbook->addFormat();
            $stylePolice10BlackSimple->setColor('black');
            $stylePolice10BlackSimple->setSize(8);
            $stylePolice10BlackSimple->setAlign('left');
            $format_center = &$workbook->addFormat();
            $format_center = &$workbook->addFormat();
            $format_center->setColor('black');
            $format_center->setFgColor('white');
            $format_center->setFontFamily('Arial');
            $format_center->setSize(8);
            $format_center->setAlign('vcenter');
            $format_center->setalign('vjustify');
            $format_center->setAlign('center');
            //Style des cellules d'entete
            $format_head = &$workbook->addFormat();
            $format_head->setBold();
            $format_head->setColor('black');
            $format_head->setFgColor(15);
            $format_head->setAlign('center');
            $format_head->setFontFamily('Arial');
            $format_head->setSize(8);
            $format_head->setalign('vjustify');
            /*$format_montant = &$workbook->addFormat();
            $format_montant->setNumFormat(4);
            $format_montant->setColor('black');
            $format_montant->setFgColor('white');
            $format_montant->setFontFamily('Arial');
            $format_montant->setSize(8);
            $format_montant->setAlign('vcenter');
            $format_montant->setalign('vjustify');*/
            $format_montant = &$workbook->addFormat();
            $format_montant->setColor('black');
            $format_montant->setSize(8);
            $format_montant->setAlign('right');
            $format_montant->setalign('vjustify');
            $format_date = &$workbook->addFormat();
            $format_date->setColor('black');
            $format_date->setSize(8);
            $format_date->setAlign('left');
            $format_date->setalign('vjustify');
            // la feuille
            $worksheet = &$workbook->addWorkSheet($this->getAnnee());
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
            $worksheet->setLandscape();
            $worksheet->setPrintScale(100);
            //$worksheet->hideGridlines();
            $worksheet->setHeader('&"Arial"&8'.$nomFile.' - '.$this->getAnnee(), '0.20');
            $worksheet->setFooter('&"Arial"&L&8'.Prado::localize('IMPRIME_LE').' &D&8 '.Prado::localize('A_POUR_IMPRIMER_LE_A_DATE').' &T&R&8'.Prado::localize('PAGE').' &P/&N', '0.20');
            $worksheet->setMarginLeft('0.20');
            $worksheet->setMarginRight('0.20');
            $worksheet->setMarginTop('0.40');
            $worksheet->setMarginBottom('0.40');
            $worksheet->setRow('6', '22.75');
            //Creation de l'entete du fichier excel
            $worksheet->setColumn(0, 0, 6);
            $worksheet->setColumn(0, 1, 7);
            $worksheet->setColumn(0, 2, 12);
            $worksheet->setColumn(0, 3, 6);
            $worksheet->setColumn(0, 4, 6);
            $worksheet->setColumn(0, 5, 10);
            $worksheet->setColumn(0, 6, 10);
            $worksheet->setColumn(0, 7, 23);
            $worksheet->setColumn(0, 8, 9);
            $worksheet->setColumn(0, 9, 6);
            $worksheet->setColumn(0, 10, 43.14);
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            $entityPublique = $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
            $worksheet->write(0, 0, Prado::localize('TEXT_LISTE_MARCHES_133'), $format_bold);
            $worksheet->write(1, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $stylePolice10BlackSimple);
            $worksheet->write(1, 2, $entityPublique, $stylePolice10BlackSimple);
            $worksheet->write(2, 0, Prado::localize('DEFINE_ENTITE_ACHAT').' :', $stylePolice10BlackSimple);
            $worksheet->write(2, 2, Atexo_EntityPurchase::getPathEntityById($this->getIdServiceAgent(), Atexo_CurrentUser::getCurrentOrganism()), $stylePolice10BlackSimple);
            $worksheet->write(3, 0, Prado::localize('ANNEE').' :', $stylePolice10BlackSimple);
            $worksheet->write(3, 2, $this->getAnnee(), $stylePolice10BlackSimple);
            $worksheet->write(6, 0, Prado::localize('DEFINE_VALIDE'), $format_head);
            $worksheet->write(6, 1, Prado::localize('DEFINE_ORIGINE'), $format_head);
            $worksheet->write(6, 2, Prado::localize('DEFINE_ATTRIBUTAIRE'), $format_head);
            $worksheet->write(6, 3, Prado::localize('CODE_POSTAL'), $format_head);
            $worksheet->write(6, 4, Prado::localize('TEXT_VILLE'), $format_head);
            $worksheet->write(6, 5, Prado::localize('PME_SLASH_PMI'), $format_head);
            $worksheet->write(6, 6, Prado::localize('DATE_NOTIFICATION'), $format_head);
            $worksheet->write(6, 7, Prado::localize('TRANCHE_BUDGETAIRE'), $format_head);
            $worksheet->write(6, 8, Prado::localize('MONTANT_EUR_HT'), $format_head);
            $worksheet->write(6, 9, Prado::localize('DEFINE_NATURE'), $format_head);
            $worksheet->write(6, 10, Prado::localize('DEFINE_OBJET_MARCHE'), $format_head);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonMarchePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $c->addAscendingOrderByColumn(CommonMarchePeer::NATUREMARCHE);
            $c->addAscendingOrderByColumn(CommonMarchePeer::IDMARCHETRANCHEBUDGETAIRE);
            if ('0' == $this->getIdServiceAgent()) {
                $idService = null;
            } else {
                $idService = $this->getIdServiceAgent();
            }
            $c->add(CommonMarchePeer::IDSERVICE, $idService);
            $c->add(CommonMarchePeer::NUMEROMARCHEANNEE, $this->getAnnee());
            $allMarches = CommonMarchePeer::doSelect($c, $connexionCom);
            if (null == $allMarches) {
                throw new Atexo_Exception('');
            }
            $row = 7;
            foreach ($allMarches as $marche) {
                $col = 0;
                $worksheet->setRow($row, '33.75');
                $dateNotification = Atexo_Util::iso2frnDate($marche->getDatenotification());
                $worksheet->write($row, $col++, $marche->getEtatLibelle(), $format);
                $worksheet->write($row, $col++, $marche->getOrigineLibelleExcel(), $format);
                $worksheet->write($row, $col++, html_entity_decode(html_entity_decode($marche->getNomattributaire())), $format);
                $worksheet->writeString($row, $col++, $marche->getCodepostal(), $format);
                $worksheet->write($row, $col++, $marche->getVille(), $format);
                $worksheet->write($row, $col++, $marche->getPmePmiLabel(), $format);
                $worksheet->write($row, $col++, $dateNotification, $format_date);
                $worksheet->write($row, $col++, $marche->getMarchetranchebudgetaire(Atexo_CurrentUser::getCurrentOrganism(), $marche->getNumeroMarcheAnnee()), $format);
                $worksheet->write($row, $col++, $marche->getMontantmarche(), $format_montant);
                $worksheet->write($row, $col++, $marche->getAbbrNaturePrestation(), $format);
                $worksheet->write($row, $col++, html_entity_decode(html_entity_decode($marche->getObjetmarche())), $format);
                ++$row;
            }
            $workbook->close();
            if ($toPdf) {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomFile;
                system('mv '.$file." '".$pathFichier."'  > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }
            $workbook->send($nomFile);
            $fh = fopen($file, 'rb');
            while (!feof($fh)) {
                $buf = fread($fh, 4096);
                echo $buf;
            }
            fclose($fh);
            header('Connection: close');
        } catch (\Exception $e) {
            throw new Exception("Erreur d'export excel.  ".$e->getMessage());
        }
    }
}
