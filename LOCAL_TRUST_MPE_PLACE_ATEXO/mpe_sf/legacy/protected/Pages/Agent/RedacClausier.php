<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 3.1
 *
 * @since MPE-4.0
 */
class RedacClausier extends MpeTPage
{
    private ?string $url = null;
    private $titre;
    private $sousTitre;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $codePage = Atexo_Util::atexoHtmlEntities($_GET['cp']);
        if ($arrayDonnes = $this->getPageNameRedirect($codePage)) {
            $this->scriptJs->Text = '<script>showLoader();</script>';
            $pageName = $arrayDonnes['0'];
            $this->titre = $arrayDonnes['1'];
            $this->sousTitre = $arrayDonnes['2'];
            $this->gotoRedaction($pageName);
        }
    }

    public function getPageNameRedirect($codePage)
    {
        $arrayPage = [
                     'RechercherClauseEditeur' => ['listClauses.htm?editeur=yes&accesModeSaaS=yes', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('RECHERCHER')],
                     'CreationClauseEditeur' => ['CreationClauseEditeurInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('DEFINE_TEXT_CREER')],
                     'exportClauseEditeurAction' => ['exporterClause.htm?editeur=yes', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('TEXT_SOUS_MENU_EXPORTER')],
                     'RechercherCanevasEditeur' => ['listCanevas.htm?editeur=yes&accesModeSaaS=yes', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('RECHERCHER')],
                     'CreationCanevasEditeur' => ['CreationCanevas1EditeurInit.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('DEFINE_TEXT_CREER')],
                     'PublicationClausier' => ['createPublicationClausier.htm', Prado::localize('TEXT_MENU_VERSION_CLAUSIER'), Prado::localize('TEXT_MENU_CREER_VERSION')],
                     'HistoriqueClausier' => ['listPublicationsClausier.htm', Prado::localize('TEXT_MENU_VERSION_CLAUSIER'), Prado::localize('TEXT_MENU_HISTORIQUES_VERSION')],
                     'RechercherClause' => ['listClauses.htm?editeur=no&accesModeSaaS=yes', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('RECHERCHER')],
                     'CreationClause' => ['CreationClauseInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('DEFINE_TEXT_CREER')],
                     'exportClauseAction' => ['exporterClause.htm?editeur=no', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('TEXT_SOUS_MENU_EXPORTER')],
                     'RechercherCanevas' => ['listCanevas.htm?editeur=no&accesModeSaaS=yes', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('RECHERCHER')],
                     'CreationCanevas' => ['CreationCanevas1Init.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('DEFINE_TEXT_CREER')],
                     'ParametrageClause' => ['ParametrageClauseInit.epm?preferences=direction', Prado::localize('TEXT_MENU_CLAUSE_PERSONNALISEES'), Prado::localize('TEXT_MENU_CLAUSE_ENTITE_ACHAT')],
                     'ParametrageClauseAgent' => ['ParametrageClauseInit.epm?preferences=agent', Prado::localize('TEXT_MENU_CLAUSE_PERSONNALISEES'), Prado::localize('TEXT_MENU_MES_CLAUSES')],
                     'AdministrerGabaritsClauseEditeur' => ['gabaritInterministerielInit.epm?initialisationSession=true', Prado::localize('TEXT_MENU_GABARITS_DE_REDACTION'), Prado::localize('MODIFIER')],
                     'AdministrerGabaritsClausierClient' => ['gabaritMinisterielInit.epm?initialisationSession=true', Prado::localize('TEXT_MENU_GABARITS_DE_REDACTION'), Prado::localize('MODIFIER')],
                     'AdministrerGabaritsClauseEntiteAchat' => ['gabaritDirectionInit.epm?initialisationSession=true', Prado::localize('DEFINE_TEXT_ENTITE_ACHAT'), Prado::localize('DIFINE_GABARITS')],
                     'AdministrerGabaritsClauseAgent' => ['gabaritAgentInit.epm?initialisationSession=true', Prado::localize('TEXT_AGENT'), Prado::localize('DIFINE_GABARITS')],
        ];

        foreach ($arrayPage as $key => $value) {
            if ($key == $codePage) {
                return $value;
            }
        }

        return false;
    }

    public function gotoRedaction($pageName)
    {
        $messageErreur = null;
        try {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
            $redaction = new Atexo_Redaction('', $agent);
            $messageErreur = '';
            if ($redaction->ticket) {
                $ticket = $redaction->initialiseContext();
                if ($ticket) {
                    $chaine = '?';
                    if (str_contains($pageName, '?')) {
                        $chaine = '&';
                    }
                    $urlReditect = Atexo_Config::getParameter('URL_RSEM_REDACTION').$pageName.$chaine.'identifiantSaaS=';
                    $urlReditect .= $ticket;
                    $this->url = $urlReditect;

                    return;
                }
                $messageErreur = Prado::localize('DEFINE_ERREUR_REDAC');
            } else {
                if ($redaction->codeErreur && $redaction->messageErreur) {
                    $messageErreur = 'Code erreur '.$redaction->codeErreur.' : '.$redaction->messageErreur;
                } else {
                    $messageErreur = Prado::localize('DEFINE_ERREUR_REDAC');
                }
            }

            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->frameRedac->Visible = false;
        } catch (Exception) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_ERREUR_REDAC'));
            $this->frameRedac->Visible = false;
        }
        if ($messageErreur) {
            $this->scriptJs->Text = '<script>hideLoader();</script>';
        }
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getSousTitre()
    {
        return $this->sousTitre;
    }

    public function getTitre()
    {
        return $this->titre;
    }
}
