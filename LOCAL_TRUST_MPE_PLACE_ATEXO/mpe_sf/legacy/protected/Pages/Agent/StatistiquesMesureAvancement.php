<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Referentiel;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_MesuresAvancement;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesMesureAvancement extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->EntiteCourante->text = Atexo_EntityPurchase::getPathEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
            $this->idService->text = $this->EntiteCourante->text;
            //Choix Annee
            $this->anneeChoix->DataSource = Atexo_Util::arrayAnneeLimited(3);
            $this->anneeChoix->DataBind();
            $this->anneeChoix->SelectedValue = date('Y');

            //Choix Trimestre
            $trimestres = Atexo_Util::getTrimestresUntilNow(date('Y'));
            $this->trimestreChoix->DataSource = $trimestres;
            $this->trimestreChoix->DataBind();
            if ((is_countable($trimestres) ? count($trimestres) : 0) > 0) {
                $this->trimestreChoix->SelectedValue = $trimestres['1'];
            } else {
                $this->trimestreChoix->SelectedValue = $trimestres['Aucun_trimistre'];
            }

            //choix type PA
            $listeDepartements = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveArrayDepartements();
            $listeDepartements = Atexo_Util::arrayUnshift($listeDepartements, '--- '.Prado::localize('DEFINE_SELECTIONNER_DEPARTEMENT').' ---');
            $this->departement->DataSource = $listeDepartements;
            $this->departement->DataBind();

            $listeTypeOrg = (new Atexo_Referentiel())->retrieveArrayValEurReferentielByReferentiel(Atexo_Config::getParameter('REFERENCE_TYPE_ORGANISME'), Atexo_CurrentUser::getCurrentOrganism());

            $this->typeRPa->DataSource = $listeTypeOrg;
            $this->typeRPa->DataBind();

            if (isset($_GET['idService']) && isset($_GET['annee']) && isset($_GET['trim'])) {
                $this->anneeChoix->SelectedValue = Atexo_Util::atexoHtmlEntities($_GET['annee']);
                $this->trimestreChoix->DataSource = Atexo_Util::getTrimestresUntilNow(Atexo_Util::atexoHtmlEntities($_GET['annee']));
                $this->trimestreChoix->DataBind();
                $this->trimestreChoix->SelectedValue = Atexo_Util::atexoHtmlEntities($_GET['trim']);
                //$this->getTrimestres();
                $this->fillListeMesures(Atexo_Util::atexoHtmlEntities($_GET['idService']), Atexo_Util::atexoHtmlEntities($_GET['annee']), Atexo_Util::atexoHtmlEntities($_GET['trim']));
            } else {
                $this->fillListeMesures(Atexo_CurrentUser::getIdServiceAgentConnected(), $this->anneeChoix->SelectedValue, $this->trimestreChoix->SelectedValue);
            }
        }
        $this->panelMessageErreur->setVisible(false);
        $this->panelMessage->setVisible(false);
    }

    public function fillListeMesures($idService, $anne, $trimistre)
    {
        if (isset($idService)) {
            $Service = Atexo_EntityPurchase::retrieveEntityById($idService, Atexo_CurrentUser::getCurrentOrganism());
            if ($Service) {
                $this->siren->Text = $Service->getSiren();
                $this->siret->Text = $Service->getComplement();
                $this->emailRetour->Text = $Service->getMail();
                $cp = substr((string) $Service->getCp(), 0, 2);
                if ($cp) {
                    $this->departement->SelectedValue = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryLikeCp($cp);
                } else {
                    $this->departement->SelectedValue = '0';
                }
            }
        }
        if (isset($idService)
        && isset($anne)
        && isset($trimistre)
        ) {
            $mesures = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementByServices($idService, Atexo_CurrentUser::getCurrentOrganism(), $anne, $trimistre);

            if ($mesures[0]['siren']
            && $mesures[0]['nic']
            && $mesures[0]['identifiant_service']
            && $mesures[0]['mail']) {
                $this->anneeChoix->SelectedValue = $anne;
                $this->trimestreChoix->SelectedValue = $trimistre;

                $this->siren->Text = $mesures[0]['siren'];
                $this->siret->Text = $mesures[0]['nic'];
                if ($mesures[0]['identifiant_service']) {
                    $this->idService->Text = $mesures[0]['identifiant_service'];
                } else {
                    $this->idService->text = $this->EntiteCourante->text;
                }
                $this->emailRetour->Text = $mesures[0]['mail'];
                if ($mesures[0]['type_pouvoir_adjudicateur']) {
                    $this->typeRPa->SelectedValue = $mesures[0]['type_pouvoir_adjudicateur'];
                }
                if ($mesures[0]['departement']) {
                    $this->departement->SelectedValue = $mesures[0]['departement'];
                } else {
                    $this->departement->SelectedValue = '0';
                }

                $this->mesures->DataSource = $mesures;
                $this->mesures->DataBind();
            } else {
                $this->mesures->DataSource = $mesures;
                $this->mesures->DataBind();
            }
        } else {
            $this->mesures->DataSource = '';
            $this->mesures->DataBind();
            $this->idService->text = $this->EntiteCourante->text;
        }
    }

    public function onModifierFormulaireClick($sender, $param)
    {
        $mesure = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementByServiceAnneeTrimestre(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism(), $this->anneeChoix->SelectedValue, $this->trimestreChoix->SelectedValue);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($mesure) {
            $mesure->setSiren($this->siren->Text);
            $mesure->setNic($this->siret->Text);
            $mesure->setIdentifiantService($this->idService->Text);
            $mesure->setMail(trim($this->emailRetour->Text));
            $mesure->setTypePouvoirAdjudicateur($this->typeRPa->SelectedValue);
            $mesure->setDepartement($this->departement->SelectedValue);
            $mesure->setDateCreation(date('Y-m-d H:i:s'));
            $mesure->save($connexion);

            $this->panelMessage->setMessage('<div class="line"><div class="intitule-auto">'.Prado::localize('CONFIRMATION_MODIFICATION_FOURMULAIRE_AVANCEMENT').' : </div> </div>');
            $this->panelMessage->setVisible(true);
            $this->panelMessageErreur->setVisible(false);
        } else {
            $this->panelMessageErreur->setMessage('<div class="line"><div class="intitule-auto">'.Prado::localize('ERREUR_MODIFICATION_FOURMULAIRE_AVANCEMENT').'</div> </div>');
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessage->setVisible(false);
        }

        $this->fillListeMesures(Atexo_CurrentUser::getIdServiceAgentConnected(), $this->anneeChoix->SelectedValue, $this->trimestreChoix->SelectedValue);
    }

    public function getTrimestres()
    {
        $this->vider();
        if ($this->anneeChoix->SelectedValue) {
            $this->trimestreChoix->DataSource = Atexo_Util::getTrimestresUntilNow($this->anneeChoix->SelectedValue);
            $this->trimestreChoix->DataBind();
            if (!$this->trimestreChoix->SelectedValue) {
                if (isset($_GET['trim'])) {
                    $valueTrimistre = Atexo_Util::atexoHtmlEntities($_GET['trim']);
                } else {
                    $valueTrimistre = 1;
                }
            } else {
                $valueTrimistre = $this->trimestreChoix->SelectedValue;
            }
            $this->fillListeMesures(Atexo_CurrentUser::getIdServiceAgentConnected(), $this->anneeChoix->SelectedValue, $valueTrimistre);
            $_GET['trim'] = $valueTrimistre;
            $_GET['annee'] = $this->anneeChoix->SelectedValue;
            $_GET['idService'] = Atexo_CurrentUser::getIdServiceAgentConnected();
        }
    }

    public function getIdService()
    {
        return Atexo_CurrentUser::getIdServiceAgentConnected();
    }

    public function redirectVersFormulaireXml($sender, $param)
    {
        $arrayCrit = [];
        if ('redirect' == $param->CommandName) {
            $arrayCrit['idService'] = $this->getIdService();
            $arrayCrit['annee'] = $this->anneeChoix->SelectedValue;
            $arrayCrit['trimestre'] = $this->trimestreChoix->SelectedValue;
            $arrayCrit['siren'] = $this->siren->Text;
            $arrayCrit['nic'] = $this->siret->Text;
            $arrayCrit['mail'] = $this->emailRetour->Text;
            $arrayCrit['identifiantservice'] = $this->idService->text;
            $arrayCrit['typepouvoir'] = $this->typeRPa->SelectedValue;
            $arrayCrit['depart'] = $this->departement->SelectedValue;

            $mesure = (new Atexo_Statistiques_MesuresAvancement())->retreiveMesureAvancementMultiCritere(Atexo_CurrentUser::getCurrentOrganism(), $arrayCrit);

            if ($mesure && ($mesure->getSiren() && $mesure->getNic()
            && $mesure->getIdentifiantService() && $mesure->getMail()
            && $mesure->getTypePouvoirAdjudicateur() && $mesure->getDepartement())) {
                $this->response->redirect('index.php?page=Agent.StatistiquesMesureAvancementFormulaire&id='.$param->CommandParameter.'&idService='.$this->getIdService().'&annee='.$this->anneeChoix->SelectedValue.'&trim='.$this->trimestreChoix->SelectedValue.'');
            } else {
                $this->panelMessageErreur->setMessage('<div class="line"><div class="intitule-auto">'.Prado::localize('ERREUR_SAISIE_INFOR_MESURE_DEMAT').'</div> </div>');
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessage->setVisible(false);

                $this->fillListeMesures(Atexo_CurrentUser::getIdServiceAgentConnected(), $this->anneeChoix->SelectedValue, $this->trimestreChoix->SelectedValue);
            }
        }
    }

    public function callfillListeMesures()
    {
        $this->vider();
        if ($this->trimestreChoix->SelectedValue) {
            $_GET['trim'] = $this->trimestreChoix->SelectedValue;
        }
        if (Atexo_CurrentUser::getIdServiceAgentConnected() != $_GET['idService']) {
            $_GET['idService'] = Atexo_CurrentUser::getIdServiceAgentConnected();
        }

        if (isset($_GET['idService']) && isset($_GET['annee']) && isset($_GET['trim'])) {
            $this->fillListeMesures(Atexo_Util::atexoHtmlEntities($_GET['idService']), Atexo_Util::atexoHtmlEntities($_GET['annee']), Atexo_Util::atexoHtmlEntities($_GET['trim']));
        } else {
            $this->fillListeMesures(Atexo_CurrentUser::getIdServiceAgentConnected(), $this->anneeChoix->SelectedValue, $this->trimestreChoix->SelectedValue);
        }
    }

    public function vider()
    {
        $this->departement->SelectedValue = '0';
        $this->siren->Text = '';
        $this->siret->Text = '';
        $this->emailRetour->Text = '';
    }
}
