<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonGestionAdresses;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonVisiteLieux;
use Application\Propel\Mpe\CommonVisiteLieuxPeer;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Traduction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author zaki anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TraduireConsultation extends MpeTPage
{
    public $reference;
    public $organisme;
    public ?array $consultationObject = null;
    public $categorieLots;
    public bool $_langueObligatoire = false;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->organisme = Atexo_CurrentUser::getOrganismAcronym();
        $this->resumeConsultation->setReference($this->reference);
        $accessibilite = 'getAccessibilite'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $detailAnnonceTraduit = 'getDetailConsultation'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseRetraisDossiers = 'getAdresseRetraisDossiers';
        $adresseDepotOffre = 'getAdresseDepotOffres';
        $lieuOuverture = 'getLieuOuverturePlis';
        $adresseEchantillon = 'getAddEchantillion';
        $adresseReunion = 'getAddReunion';
        //Pieces
        $piecesDossierAdmin = 'getPiecesDossierAdmin'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $piecesDossierTech = 'getPiecesDossierTech'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $piecesDossierAdditif = 'getPiecesDossierAdditif'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        //Pieces
        $adresseRetraisDossiers .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseDepotOffre .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $lieuOuverture .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseEchantillon .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseReunion .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $this->controlObjet->SetEnabled(false);
        $langue = (new Atexo_Languages())->getLangueByAbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        if ('1' == $langue->getObligatoirePourPublicationConsultation()) {
            $this->visibleEntreprise_langue_1->setChecked(true);
            $this->visibleEntreprise_langue_1->SetEnabled(false);

            $this->controlObjet->SetEnabled(true);
            $this->_langueObligatoire = true;
        }
        if (!$this->isPostBack) {
            $consultation = $this->getConsultation();
            if ($consultation) {
                $this->displayFormulaireAPP($consultation[0]->getIdTypeAvis());
                $this->idIntituleConsultation->Text = $consultation[0]->getIntitule();
                $this->idObjetConsultation->Text = $consultation[0]->getObjet();
                $this->intituleConsultation_langue->Text = Atexo_Traduction::getTraduction($consultation[0]->getIdTrIntitule(), Atexo_Util::atexoHtmlEntities($_GET['langue']));
                $this->objetConsultation_langue->Text = Atexo_Traduction::getTraduction($consultation[0]->getIdTrObjet(), Atexo_Util::atexoHtmlEntities($_GET['langue']));
                $this->visibleEntreprise_langue_1->checked = ('1' == $consultation[0]->$accessibilite());
                $this->visiblePortailEntreprise->checked = ('1' == $consultation[0]->getPublicationEurope());
                // Afficher les adresses parametres du service connecte
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
                $adresse = (new Atexo_Consultation())->retreiveAdressesServices($idService, $organisme);
                if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                    $this->panelAdresseRetrait->setVisible(true);
                    $this->adresseRetrait->Text = $consultation[0]->getAdresseRetraisDossiers();
                    if (($adresse instanceof CommonGestionAdresses) && ('' != $adresse->$adresseRetraisDossiers()) && ('' == $consultation[0]->$adresseRetraisDossiers())) {
                        $this->traductionAdresseRetraitDossiers->Text = $adresse->$adresseRetraisDossiers();
                    } else {
                        $this->traductionAdresseRetraitDossiers->Text = $consultation[0]->$adresseRetraisDossiers();
                    }
                }

                if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                    $this->panelAdresseDepotOffre->setVisible(true);
                    $this->adresseDepotOffre->Text = $consultation[0]->getAdresseDepotOffres();
                    if (($adresse instanceof CommonGestionAdresses) && ('' != $adresse->$adresseDepotOffre()) && ('' == $consultation[0]->$adresseDepotOffre())) {
                        $this->traductionAdresseDepotOffre->Text = $adresse->$adresseDepotOffre();
                    } else {
                        $this->traductionAdresseDepotOffre->Text = $consultation[0]->$adresseDepotOffre();
                    }
                }

                if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                    $this->panelLieuOuverturePlis->setVisible(true);
                    $this->lieuOuverturePlis->Text = $consultation[0]->getLieuOuverturePlis();
                    if (($adresse instanceof CommonGestionAdresses) && '' != $adresse->$lieuOuverture() && '' == $consultation[0]->$lieuOuverture()) {
                        $this->traductionLieuOuverturePlis->Text = $adresse->$lieuOuverture();
                    } else {
                        $this->traductionLieuOuverturePlis->Text = $consultation[0]->$lieuOuverture();
                    }
                }
                if (Atexo_Module::isEnabled('ConsultationPiecesDossiers') && $consultation[0]->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $this->panelPiecesDossierse->setVisible(true);
                    $this->idpiecesDossierAdmin->Text = $consultation[0]->getPiecesDossierAdmin();
                    $this->idpiecesDossierTech->Text = $consultation[0]->getPiecesDossierTech();
                    $this->idpiecesDossierAdditif->Text = $consultation[0]->getPiecesDossierAdditif();
                    $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_PIECES_ADRESSES'));
                    if ($valeur) {
                        if ($valeur[0]) {
                            $piecesDossierAdminTraduit = $valeur[0]->getLibelleValeurReferentielTraduitByLang(Atexo_Util::atexoHtmlEntities($_GET['langue']));
                        }
                        if ($valeur[1]) {
                            $piecesDossierTechTraduit = $valeur[1]->getLibelleValeurReferentielTraduitByLang(Atexo_Util::atexoHtmlEntities($_GET['langue']));
                        }
                        if ($valeur[2]) {
                            $piecesDossierAdditifTraduit = $valeur[2]->getLibelleValeurReferentielTraduitByLang(Atexo_Util::atexoHtmlEntities($_GET['langue']));
                        }
                    }
                    if ('' != $consultation[0]->$piecesDossierAdmin()) {
                        $this->piecesDossierAdmin_langue->Text = $consultation[0]->$piecesDossierAdmin();
                    } else {
                        $this->piecesDossierAdmin_langue->Text = $piecesDossierAdminTraduit;
                    }
                    if ('' != $consultation[0]->$piecesDossierTech()) {
                        $this->piecesDossierTech_langue->Text = $consultation[0]->$piecesDossierTech();
                    } else {
                        $this->piecesDossierTech_langue->Text = $piecesDossierTechTraduit;
                    }

                    if ('' != $consultation[0]->$piecesDossierAdditif()) {
                        $this->piecesDossierAdditif_langue->Text = $consultation[0]->$piecesDossierAdditif();
                    } else {
                        $this->piecesDossierAdditif_langue->Text = $piecesDossierAdditifTraduit;
                    }
                }
                if ('0' == $consultation[0]->getAlloti()) {
                    $this->blocNonAlloti->setVisible(true);
                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && '1' == $consultation[0]->getEchantillon()) {
                        $this->panelEchantillonsDemandes->setVisible(true);
                        $this->dateLimiteEchantillon->Text = Atexo_Util::iso2frnDateTime($consultation[0]->getDateLimiteEchantillion());
                        $this->adresseDepotEchantillon->Text = $consultation[0]->getAddEchantillion();
                        $this->traductionAdresseDepotEchantillon->Text = $consultation[0]->$adresseEchantillon();
                    }
                    if (Atexo_Module::isEnabled('ConsultationReunion') && '1' == $consultation[0]->getReunion()) {
                        $this->panelReunion->setVisible(true);
                        $this->dateReunion->Text = Atexo_Util::iso2frnDateTime($consultation[0]->getDateReunion());
                        $this->adresseReunion->Text = $consultation[0]->getAddReunion();
                        $this->traductionAdresseReunion->Text = $consultation[0]->$adresseReunion();
                    }
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && $consultation[0]->getVisitesLieux()) {
                        $this->panelVisiteLieux->setVisible(true);
                        $visiteLieux = $consultation[0]->getCommonVisiteLieuxs(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
                        $visiteLieux = is_array($visiteLieux) ? $visiteLieux : [];
                        $this->RepeaterVisiteLieux->DataSource = $visiteLieux;
                        $this->RepeaterVisiteLieux->DataBind();
                    }
                } else {
                    $this->blocNonAlloti->setVisible(false);
                }
                if ($consultation[0]->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $this->setViewState('isAnnonce', true);
                    $this->retour->setNavigateUrl('index.php?page=Agent.DetailAnnonce&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
                    if (Atexo_Module::isEnabled('TraduireAnnonces')) {
                        $this->detailAnnonce->Text = $consultation[0]->getDetailConsultation();
                        $this->detailAnnonceTraduction->Text = $consultation[0]->$detailAnnonceTraduit();
                        $this->panelTraduireAnnonce1->setVisible(true);
                        $this->panelTraduireAnnonce2->setVisible(true);
                        $this->panelAdresseRetrait->setVisible(false);
                        $this->panelLieuOuverturePlis->setVisible(false);
                        $this->panelAdresseDepotOffre->setVisible(false);
                    } else {
                        $this->errorPart->setVisible(true);
                        $this->mainPart->setVisible(false);
                        $this->panelMessageErreur->setMessage('Impossible de traduire');
                    }
                } else {
                    $this->retour->setNavigateUrl('index.php?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
                }
            }
            $lots = $this->getAllLots();
            if (is_array($lots) && count($lots) > 0) {
                $this->blocDetailLots->visible = true;
                $this->setViewState('alloti', true);
                $this->lotsRepeater->dataSource = $lots;
                $this->lotsRepeater->dataBind();
            } else {
                $this->blocDetailLots->visible = false;
                $this->setViewState('alloti', false);
            }
        }
    }

    public function getConsultation()
    {
        $consultation = new Atexo_Consultation();
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($this->reference);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationFound = $consultation->search($consultationCriteria);
        $this->consultationObject = $consultationFound;

        //      print_r($consultationFound);exit;

        return $consultationFound;
    }

    public function getAllLots()
    {
        $lots = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->reference, $connexionCom);
        $lots = $consultation->getAllLots();
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            foreach ($lots as $lot) {
                $c = new Criteria();
                $c->add(CommonVisiteLieuxPeer::ORGANISME, $this->organisme);
                $c->add(CommonVisiteLieuxPeer::CONSULTATION_ID, $lot->getConsultationId());
                $c->add(CommonVisiteLieuxPeer::LOT, $lot->getLot());
                $visiteLieux = CommonVisiteLieuxPeer::doSelect($c, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
                $visiteLieux = is_array($visiteLieux) ? $visiteLieux : [];
                $lot->setColVisitesLieux($visiteLieux);
            }
        }

        return $lots;
    }

    public function saveTranductionConsultation()
    {
        //$adresseRetraisDossiers="setAdresseRetraisDossiers".Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseVisiteLieux = 'setAdresse'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $intitule = $this->intituleConsultation_langue->Text;
        $objet = $this->objetConsultation_langue->Text;
        $detailAnnonce = $this->detailAnnonceTraduction->Text;
        $accessibilite = 'setAccessibilite'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseRetraisDossiers = 'setAdresseRetraisDossiers'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $adresseDepotOffre = 'setAdresseDepotOffres'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        $lieuOuverture = 'setLieuOuverturePlis'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $consultationCom = CommonConsultationPeer::retrieveByPK($this->reference, $connexionCom);
            if ($consultationCom instanceof CommonConsultation) {
                $donneeComplementaire = $consultationCom->getDonneComplementaire();
                $consultationCom->setIntituleTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $intitule);
                $consultationCom->setObjetTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $objet);
                $consultationCom->setDetailConsultationTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $detailAnnonce);
                if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                    $consultationCom->$adresseRetraisDossiers($this->traductionAdresseRetraitDossiers->Text);
                }
                if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                    $consultationCom->$adresseDepotOffre($this->traductionAdresseDepotOffre->Text);
                }
                if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                    $consultationCom->$lieuOuverture($this->traductionLieuOuverturePlis->Text);
                }
                if (Atexo_Module::isEnabled('ConsultationPiecesDossiers') && $consultationCom->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $donneeComplementaire->setPiecesDossierAdminTraduit($this->piecesDossierAdmin_langue->Text, Atexo_Util::atexoHtmlEntities($_GET['langue']));
                    $donneeComplementaire->setPiecesDossierTechTraduit($this->piecesDossierTech_langue->Text, Atexo_Util::atexoHtmlEntities($_GET['langue']));
                    $donneeComplementaire->setPiecesDossierAdditifTraduit($this->piecesDossierAdditif_langue->Text, Atexo_Util::atexoHtmlEntities($_GET['langue']));
                }
                if ('0' == $consultationCom->getAlloti()) {
                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && '1' == $consultationCom->getEchantillon()) {
                        $donneeComplementaire->setAddEchantillonTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $this->traductionAdresseDepotEchantillon->Text);
                    }
                    if (Atexo_Module::isEnabled('ConsultationReunion') && '1' == $consultationCom->getReunion()) {
                        $donneeComplementaire->setAddReunionTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $this->traductionAdresseReunion->Text);
                    }
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && $consultationCom->getVisitesLieux()) {
                        foreach ($this->RepeaterVisiteLieux->getItems() as $item) {
                            $idVisiteLieu = $item->idVisite->Value;
                            $visiteLieux = CommonVisiteLieuxPeer::retrieveByPK($idVisiteLieu, $this->organisme, $connexionCom);
                            if ($visiteLieux instanceof CommonVisiteLieux) {
                                $visiteLieux->$adresseVisiteLieux($item->traductionAdresseReunion->Text);
                                if (strtolower(Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']))) == strtolower(Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
                                    $visiteLieux->setAdresse($item->traductionAdresseReunion->Text);
                                }
                                $visiteLieux->save($connexionCom);
                            }
                        }
                    }
                }
                $consultationCom->$accessibilite((int) $this->visibleEntreprise_langue_1->checked);
                $consultationCom->setPublicationEurope((int) $this->visiblePortailEntreprise->Checked);
                if (strtolower(Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']))) == strtolower(Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
                    $consultationCom->setIntitule($intitule);
                    $consultationCom->setObjet($objet);
                    if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                        if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                            $consultationCom->setAdresseRetraisDossiers($this->traductionAdresseRetraitDossiers->Text);
                        }
                        if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                            $consultationCom->setAdresseDepotOffres($this->traductionAdresseDepotOffre->Text);
                        }
                        if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                            $consultationCom->setLieuOuverturePlis($this->traductionLieuOuverturePlis->Text);
                        }
                        if ('0' == $consultationCom->getAlloti()) {
                            if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && '1' == $consultationCom->getEchantillon()) {
                                $donneeComplementaire->setAddEchantillion($this->traductionAdresseDepotEchantillon->Text);
                            }
                            if (Atexo_Module::isEnabled('ConsultationReunion') && '1' == $consultationCom->getReunion()) {
                                $donneeComplementaire->setAddReunion($this->traductionAdresseReunion->Text);
                            }
                        }
                        $donneeComplementaire->save($connexionCom);
                        $consultationCom->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());
                    }
                }
                $consultationCom->save($connexionCom);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function saveTraductionLotsConsultation()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        foreach ($this->lotsRepeater->getItems() as $item) {
            $idLot = $item->numLot->Text;
            $consultationId = $this->reference;
            $description = $item->intituleConsultation_lot_1_langue_1->Text;
            $descriptionDetail = $item->Description_lot_1_langue_1->Text;

            $adresseVisiteLieux = 'setAdresse'.Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));
            $lotCom = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($organisme, $consultationId, $idLot);
            //$lotCom = CommonCategorieLotPeer::retrieveByPK($organisme, $consultationId, $idLot, $connexionCom);

            try {
                if ($lotCom instanceof CommonCategorieLot) {
                    $donneeComplementaire = $lotCom->getDonneComplementaire();
                    $lotCom->setDescriptionTraduite(Atexo_Util::atexoHtmlEntities($_GET['langue']), $description);
                    $lotCom->setDescriptionDetailTraduite(Atexo_Util::atexoHtmlEntities($_GET['langue']), $descriptionDetail);

                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && '1' == $lotCom->getEchantillon()) {
                        $donneeComplementaire->setAddEchantillonTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $item->traductionAdresseEchantillon->Text);
                    }
                    if (Atexo_Module::isEnabled('ConsultationReunion') && '1' == $lotCom->getReunion()) {
                        $donneeComplementaire->setAddReunionTraduit(Atexo_Util::atexoHtmlEntities($_GET['langue']), $item->traductionAdresseReunion->Text);
                    }
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && '1' == $lotCom->getVisitesLieux()) {
                        foreach ($item->RepeaterVisiteLieuxLots->getItems() as $visiteLieuxitem) {
                            $idVisiteLieu = $visiteLieuxitem->idVisite->Value;
                            $visiteLieux = CommonVisiteLieuxPeer::retrieveByPK($idVisiteLieu, $organisme, $connexionCom);
                            if ($visiteLieux instanceof CommonVisiteLieux) {
                                $visiteLieux->$adresseVisiteLieux($visiteLieuxitem->traductionAdresseVisite->Text);
                                if (strtolower(Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']))) == strtolower(Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
                                    $visiteLieux->setAdresse($visiteLieuxitem->traductionAdresseVisite->Text);
                                }
                                $visiteLieux->save($connexionCom);
                            }
                        }
                    }

                    if (strtolower(Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']))) == strtolower(Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
                        $lotCom->setDescription($description);
                        $lotCom->setDescriptionDetail($descriptionDetail);
                        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && '1' == $lotCom->getEchantillon()) {
                            $donneeComplementaire->setAddEchantillon($item->traductionAdresseEchantillon->Text);
                        }
                        if (Atexo_Module::isEnabled('ConsultationReunion') && '1' == $lotCom->getReunion()) {
                            $donneeComplementaire->setAddReunion($item->traductionAdresseReunion->Text);
                        }
                    }
                    $donneeComplementaire->save($connexionCom);
                    $lotCom->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());

                    $lotCom->save($connexionCom);
                }
            } catch (Exception $e) {
                echo $e;
                exit;
            }
        }
    }

    public function save($sender, $param)
    {
        $this->saveTranductionConsultation();
        if ($this->getViewState('alloti')) {
            $this->saveTraductionLotsConsultation();
        }

        if (Atexo_Module::isEnabled('TraduireAnnonces') && $this->getViewState('isAnnonce', false)) {
            $this->response->redirect('index.php?page=Agent.DetailAnnonce&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        } else {
            $this->response->redirect('index.php?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    public function getFlag($lang)
    {
        return "flag-$lang.png";
    }

    public function getLanguageAbbreviationMaj($lang)
    {
        return strtoupper($lang);
    }

    public function getLangueLibelle($lang)
    {
        $langueMajuscule = strtoupper($lang);

        return Prado::localize('DEFINE_'.$langueMajuscule);
    }

    public function getAccessibiliteLangue($lang)
    {
        $langueMajuscule = strtoupper($lang);

        return Prado::localize('DEFINE_ACCESSIBLITE_'.$langueMajuscule);
    }

    public function getLanguageAbbreviation($lang)
    {
        return ucwords($lang);
    }

    public function getDescriptionTraduction($lang)
    {
        $description = 'getDescriptionTraduite('.$lang.')';

        return $description;
    }

    public function getAdresseVisiteLieux(CommonVisiteLieux $visiteLieux)
    {
        $adresseVisiteLieux = 'getAdresse';
        $adresseVisiteLieux .= Atexo_Languages::getLanguageAbbreviation(Atexo_Util::atexoHtmlEntities($_GET['langue']));

        return $visiteLieux->$adresseVisiteLieux();
    }

    public function getAdresseEchantillon(CommonCategorieLot $lot)
    {
        $donneeComplementaire = $lot->getDonneComplementaire();

        return Atexo_Traduction::getTraduction($donneeComplementaire->getIdTrAddEchantillon(), Atexo_Util::atexoHtmlEntities($_GET['langue']));
    }

    public function getAdresseReunion(CommonCategorieLot $lot)
    {
        $donneeComplementaire = $lot->getDonneComplementaire();

        return Atexo_Traduction::getTraduction($donneeComplementaire->getIdTrAddReunion(), Atexo_Util::atexoHtmlEntities($_GET['langue']));
    }

    /**
     * affiche seul le bloc pour une annonce programme provisionnel.
     */
    public function displayFormulaireAPP($idAvis)
    {
        $this->divIntituleConsultation->setVisible(true);
        $this->divObjConsultation->setVisible(true);
        $this->divIntituleConsultationTra->setVisible(true);
        $this->divObjConsultationTra->setVisible(true);
        $this->controlObjet->setEnabled(true);
    }
}
