<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Application\Service\Atexo\MiseDispositionPieces\Atexo_MiseDispositionPieces_MiseDisposition;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class MiseDispositionPiecesMarche extends MpeTPage
{
    public $_consultation;
    public $_refConsultation;

    public function onLoad($param)
    {
        $consultationId = $_GET['id'];
        if (0 == strcmp($consultationId, $consultationId)) {
            $this->_refConsultation = Atexo_Util::atexoHtmlEntities($consultationId);
            $this->_consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($consultationId));
            if ($this->_consultation instanceof CommonConsultation) {
                $this->IdConsultationSummary->setConsultation($this->_consultation);
            } else {
                $this->IdConsultationSummary->setWithException(false);
            }
        }
        if (!$this->isPostBack) {
            //Destinataires
            $var = Atexo_MiseDispositionPieces_DestinataireMiseDisposition::getDestinatireMiseDispositionByOrg(Atexo_CurrentUser::getCurrentOrganism(), Prado::localize('TEXT_DEFINE_SELECTIONNER'));
            $this->destination->dataSource = $var;
            $this->destination->dataBind();

            //les attributaires
            if ($this->_consultation instanceof CommonConsultation) {
                $arrayAttributaires = [];
                $attributaires = (new Atexo_Consultation_Decision())->retrieveDecisionEnvoloppeByReference($this->_consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
                foreach ($attributaires as $attributaire) {
                    $id_offre = $attributaire->getIdOffre();
                    $lot = $attributaire->getLot();
                    $offre = (new Atexo_Interfaces_Offre())->getOffreByIdOffre($id_offre);
                    if ($offre instanceof CommonOffres) {
                        $entrepriseName = $offre->getNomEntrepriseInscrit();
                    }
                    if (!$marche = $attributaire->getNumeroMarche()) {
                        $marche = 'N° de marché non renseigné ';
                    }

                    if ($lot) {
                        $Numlot = '(Lot n°'.$lot.')';
                    } else {
                        $lot = 0;
                    }

                    $arrayAttributaires[$attributaire->getIdDecisionEnveloppe().'-'.$lot.'-'.$id_offre] = $entrepriseName.' / '.$marche.$Numlot;
                }
                array_unshift($arrayAttributaires, Prado::localize('TEXT_DEFINE_SELECTIONNER'));
                $this->selectionAttributaire->dataSource = $arrayAttributaires;
                $this->selectionAttributaire->dataBind();
            }
        }
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function getDestinataire()
    {
        return $this->destination->SelectedValue;
    }

    public function getAttributaire()
    {
        $arrayResult = explode('-', $this->selectionAttributaire->SelectedValue);

        return $arrayResult;
    }

    public function displayPanelsPieces($sender, $param)
    {
        if ('0' != $this->destination->SelectedValue && '0' != $this->selectionAttributaire->SelectedValue) {
            $this->PieceDce->panelPiece->style = "display:''";
            $this->AutresPiece->panelPiece->style = "display:''";
            $this->PiecesDepot->panelPiece->style = "display:''";
            $this->documentsExternes->panelPiece->style = "display:''";

            $this->PieceDce->typePiece = Atexo_Config::getParameter('TYPE_PIECES_DCE');
            $this->AutresPiece->typePiece = Atexo_Config::getParameter('TYPE_AUTRE_PIECES');
            $this->PiecesDepot->typePiece = Atexo_Config::getParameter('TYPE_PIECES_DEPOT_ATTRIBUTAIRE');
            $this->PiecePublicite->typePiece = Atexo_Config::getParameter('TYPE_PIECES_PUBLICITE');
            $this->documentsExternes->typePiece = Atexo_Config::getParameter('TYPE_DOCUMENT_EXTERNE');

            $this->panelActionGroupe->style = "display:''";
            $this->PiecesDepot->fillRepeaterPiecesMiseDisposition();
            $this->PieceDce->fillRepeaterPiecesMiseDisposition();
            $this->AutresPiece->fillRepeaterPiecesMiseDisposition();
            $this->documentsExternes->fillRepeaterPiecesMiseDisposition();
            if (Atexo_Module::isEnabled('PubliciteFormatXml')) {
                $this->PiecePublicite->panelPiece->style = "display:''";
                $this->PiecePublicite->fillRepeaterPiecesMiseDisposition();
            }
            //$this->PanelAllPieces->style = "display:''";
        } else {
            $this->PiecesDepot->panelPiece->style = 'display:none';
            $this->PieceDce->panelPiece->style = 'display:none';
            $this->AutresPiece->panelPiece->style = 'display:none';
            $this->PiecePublicite->panelPiece->style = 'display:none';
            $this->documentsExternes->panelPiece->style = 'display:none';
            $this->panelActionGroupe->style = 'display:none';
            //$this->PanelAllPieces->style = "display:none";
        }
        $this->PiecesDepot->panelPiece->render($param->getNewWriter());
        $this->AutresPiece->panelPiece->render($param->getNewWriter());
        $this->documentsExternes->panelPiece->render($param->getNewWriter());
        $this->PieceDce->panelPiece->render($param->getNewWriter());
        $this->PiecePublicite->panelPiece->render($param->getNewWriter());
        $this->panelActionGroupe->render($param->getNewWriter());
    }

    public function MettreAdisponibleSelectionnes($sender, $param)
    {
        //dce
        $arrayPiecesDce = $this->PieceDce->getPiecesSelect();
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->insertArrayPieces($arrayPiecesDce)) {
            $this->PieceDce->panelPiece->render($param->getNewWriter());
            $this->PieceDce->fillRepeaterPiecesMiseDisposition();
        }
        //pieces depot
        $arrayPiecesDepot = $this->PiecesDepot->getPiecesSelect();
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->insertArrayPieces($arrayPiecesDepot)) {
            $this->PiecesDepot->panelPiece->render($param->getNewWriter());
            $this->PiecesDepot->fillRepeaterPiecesMiseDisposition();
        }
        //publicite
        $arrayPiecesPub = $this->PiecePublicite->getPiecesSelect();
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->insertArrayPieces($arrayPiecesPub)) {
            $this->PiecePublicite->panelPiece->render($param->getNewWriter());
            $this->PiecePublicite->fillRepeaterPiecesMiseDisposition();
        }
        //Autres pieces
        $arrayPiecesAutres = $this->AutresPiece->getPiecesSelect();
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->insertArrayPieces($arrayPiecesAutres)) {
            $this->AutresPiece->panelPiece->render($param->getNewWriter());
            $this->AutresPiece->fillRepeaterPiecesMiseDisposition();
        }
        //Docuement externe
        $arrayPiecesAutres = $this->documentsExternes->getPiecesSelect();
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->insertArrayPieces($arrayPiecesAutres)) {
            $this->documentsExternes->panelPiece->render($param->getNewWriter());
            $this->documentsExternes->fillRepeaterPiecesMiseDisposition();
        }
    }

    public function AnnulerMettreAdisponibleSelectionnes($sender, $param)
    {
        //dce
        $arrayPiecesDce = $this->PieceDce->getPiecesSelect(true);
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->deleteArrayPieces($arrayPiecesDce)) {
            $this->PieceDce->panelPiece->render($param->getNewWriter());
            $this->PieceDce->fillRepeaterPiecesMiseDisposition();
        }
        //pieces depot
        $arrayPiecesDepot = $this->PiecesDepot->getPiecesSelect(true);
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->deleteArrayPieces($arrayPiecesDepot)) {
            $this->PiecesDepot->panelPiece->render($param->getNewWriter());
            $this->PiecesDepot->fillRepeaterPiecesMiseDisposition();
        }
        //publicite
        $arrayPiecesPub = $this->PiecePublicite->getPiecesSelect(true);
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->deleteArrayPieces($arrayPiecesPub)) {
            $this->PiecePublicite->panelPiece->render($param->getNewWriter());
            $this->PiecePublicite->fillRepeaterPiecesMiseDisposition();
        }
        //Autres pieces
        $arrayPiecesAutres = $this->AutresPiece->getPiecesSelect(true);
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->deleteArrayPieces($arrayPiecesAutres)) {
            $this->AutresPiece->panelPiece->render($param->getNewWriter());
            $this->AutresPiece->fillRepeaterPiecesMiseDisposition();
        }
        //Docuement externe
        $arrayPiecesAutres = $this->documentsExternes->getPiecesSelect(true);
        if ((new Atexo_MiseDispositionPieces_MiseDisposition())->deleteArrayPieces($arrayPiecesAutres)) {
            $this->documentsExternes->panelPiece->render($param->getNewWriter());
            $this->documentsExternes->fillRepeaterPiecesMiseDisposition();
        }
    }
}
