<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Newsletter\Atexo_Newsletter_Newsletter;
use Prado\Prado;

/**
 * Permet de gerer les Newsletters.
 *
 * @author Houriya MAATALLA <Houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class NewsletterMessagesAgents extends MpeTPage
{
    public $service;
    protected $critereTri = '';
    protected $triAscDesc = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('GestionNewsletter')) {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        } elseif (Atexo_CurrentUser::hasHabilitation('GererNewsletter') || Atexo_CurrentUser::hasHabilitation('GererNewsletterRedac')) {
            $this->panelValidationSummary->visible = false;
            if (Atexo_Module::isEnabled('GestionBoampMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            $isRedac = false;
            if (!$this->IsPostBack) {
                if ($_GET['service']) {
                    $this->ChoixEntiteAchat->autreEntite->Checked = true;
                    $this->ChoixEntiteAchat->monEntite->Checked = false;
                    $this->service = Atexo_Util::atexoHtmlEntities($_GET['service']);
                    if ($this->service != Atexo_CurrentUser::getCurrentServiceId()) {
                        $this->ChoixEntiteAchat->listeAutreEntites->setSelectedValue($this->service);
                    } else {
                        $this->ChoixEntiteAchat->autreEntite->Checked = false;
                        $this->ChoixEntiteAchat->monEntite->Checked = true;
                    }
                    if (isset($_GET['Redac'])) {
                        if (Atexo_CurrentUser::hasHabilitation('GererNewsletterRedac')) {
                            $isRedac = true;
                        } else {
                            $this->response->redirect('index.php?page=Agent.AgentHome');
                        }
                    }
                    $dataSource = (new Atexo_Newsletter_Newsletter())->retrieveNewsletter(Atexo_Util::atexoHtmlEntities($_GET['service']), Atexo_CurrentUser::getOrganismAcronym(), $isRedac);
                    Atexo_CurrentUser::writeToSession('cachedMessages', $dataSource);
                    $this->messageRepeater->DataSource = $dataSource;
                    $this->messageRepeater->DataBind();
                } else {
                    if (isset($_GET['Redac'])) {
                        if (Atexo_CurrentUser::hasHabilitation('GererNewsletterRedac')) {
                            $isRedac = true;
                        } else {
                            $this->response->redirect('index.php?page=Agent.AgentHome');
                        }
                    }
                    $this->service = Atexo_CurrentUser::getCurrentServiceId();
                    $dataSource = (new Atexo_Newsletter_Newsletter())->retrieveNewsletter(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getOrganismAcronym(), $isRedac);
                    Atexo_CurrentUser::writeToSession('cachedMessages', $dataSource);
                    $this->messageRepeater->DataSource = $dataSource;
                    $this->messageRepeater->DataBind();
                }
            }
        } else {
            $this->gestionMessages->Visible = true;
            $this->panelValidationSummary->visible = true;
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->service = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $dataSource = (new Atexo_Newsletter_Newsletter())->retrieveNewsletter($this->service, Atexo_CurrentUser::getOrganismAcronym());
        Atexo_CurrentUser::writeToSession('cachedMessages', $dataSource);
        $this->messageRepeater->DataSource = $dataSource;
        $this->messageRepeater->DataBind();
        $this->messagePanel->render($param->NewWriter);
    }

    public function deleteNewsletter($sender, $param)
    {
        $id = $param->CommandParameter;
        (new Atexo_Newsletter_Newsletter())->deleteNewsletter($id);
        $this->setViewState('idService', $this->ChoixEntiteAchat->getSelectedEntityPurchase());
        $serviceSelectionne = $this->getViewState('idService');
        if ($serviceSelectionne && ($serviceSelectionne != Atexo_CurrentUser::getCurrentServiceId())) {
            $this->ChoixEntiteAchat->autreEntite->Checked = true;
            $this->service = $serviceSelectionne;
        } else {
            $this->ChoixEntiteAchat->monEntite->Checked = true;
        }
        $isRedac = false;
        if (isset($_GET['Redac'])) {
            $isRedac = true;
        }
        $dataSource = (new Atexo_Newsletter_Newsletter())->retrieveNewsletter($serviceSelectionne, Atexo_CurrentUser::getOrganismAcronym(), $isRedac);
        Atexo_CurrentUser::writeToSession('cachedMessages', $dataSource);
        $this->messageRepeater->DataSource = $dataSource;
        $this->messageRepeater->DataBind();
    }

    public function getStatutMessage($statutMessage)
    {
        if ($statutMessage == Atexo_Config::getParameter('STATUS_BROUILLON')) {
            return Prado::localize('TEXT_BROUILLON');
        } elseif ($statutMessage == Atexo_Config::getParameter('STATUS_ENVOI_PLANIFIE')) {
            return Prado::localize('TEXT_ENVOI_PLANIFIE');
        } elseif ($statutMessage == Atexo_Config::getParameter('STATUS_ENVOYE')) {
            return Prado::localize('TEXT_ENVOYE');
        }
    }

    /**
     * Trier le tableau des messages.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');

        $cachedMessages = Atexo_CurrentUser::readFromSession('cachedMessages');

        if ('redacteur' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'sigleRedacteur', '');
        } elseif ('auteur' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'redacteur', '');
        } elseif ('envoyePar' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'envoyePar', '');
        } elseif ('statut' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'statutMessage', '');
        } elseif ('dateEnvoi' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'dateEnvoi', '');
        } elseif ('objet' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedMessages, 'objet', '');
        } else {
            return;
        }
        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedMessages');
        Atexo_CurrentUser::writeToSession('cachedMessages', $dataSourceTriee);
        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des messages
        $this->messageRepeater->DataSource = $dataSourceTriee;
        $this->messageRepeater->DataBind();

        // Re-affichage du TActivePanel
        $this->messagePanel->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, $typeCritere);
        $resultat = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    /**
     * permet de retourner l'url d'ajout d'un message.
     *
     * @return string url
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public function getUrlAddMessage()
    {
        $redac = null;
        if (isset($_GET['Redac'])) {
            $redac = '&Redac';
        }

        return 'index.php?page=Agent.EditionMessageAgent&action=2&service='.$this->service.$redac;
    }

    /**
     * permet de retourner l'url de modification d'un message.
     *
     * @param int $idMessage
     *
     * @return string url
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public function getUrlEditMessage($idMessage)
    {
        $redac = null;
        if (isset($_GET['Redac'])) {
            $redac = '&Redac';
        }

        return 'index.php?page=Agent.EditionMessageAgent&id='.$idMessage.'&action=1&service='.$this->service.$redac;
    }

    /**
     * permet de retourner l'url d'affichge d'un message.
     *
     * @param int $idMessage
     *
     * @return string url
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public function getUrlShowMessage($idMessage)
    {
        $redac = null;
        if (isset($_GET['Redac'])) {
            $redac = '&Redac';
        }

        return 'index.php?page=Agent.EditionMessageAgent&id='.$idMessage.'&action=0&service='.$this->service.$redac;
    }
}
