<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

class ConsultationsDetailAccordCadre extends MpeTPage
{
    protected $_lotOrConsultation;
    protected $_consultation;
    protected $critereTri;
    protected $triAscDesc;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['acrOrg']));
        $this->consultationSummary->setConsultation($consultation);

        if ((new Atexo_Consultation_Responses())->ifOrganismeInvite(Atexo_Util::atexoHtmlEntities($_GET['acrOrg']), Atexo_CurrentUser::getCurrentOrganism(), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']))) {
            $this->panelMessageErreur->setVisible(false);
            $lotOrConsultation = (new Atexo_Consultation_Responses())->retrieveLotsForDecision(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_Util::atexoHtmlEntities($_GET['acrOrg']));
            if (is_array($lotOrConsultation) && 1 == count($lotOrConsultation)) {
                $lotOrConsultation = array_shift($lotOrConsultation);
                if ($lotOrConsultation->getDecisionLot() instanceof CommonDecisionLot) {
                    $this->consultationSummary->setConsultation($consultation);
                    $this->_lotOrConsultation = $lotOrConsultation;
                    $this->_consultation = $consultation;

                    $this->dateDecision->Text = $this->_lotOrConsultation->getDecisionLot()->getDateDecisionAffiche();
                    $this->commentaireDecision->Text = $this->_lotOrConsultation->getDecisionLot()->getCommentaire();
                    $this->lotDecision->Text = Prado::localize('LOT').' '.$this->_lotOrConsultation->getLot();
                    if (0 != $this->_lotOrConsultation->getLot()) {
                        $this->lotDecision->setVisible(true);
                    } else {
                        $this->lotDecision->setVisible(false);
                    }
                    $this->typeDecision->Text = $this->_lotOrConsultation->getDecisionLot()->getLibelleTypeDecision();
                    $this->descriptionDecision->Text = $this->_lotOrConsultation->getDescription(Atexo_CurrentUser::readFromSession('lang'));
                }
            }

            $this->RepeaterListEntrepriseSelectionne->dataSource = (new Atexo_Consultation_Responses())->allEnveloppeDecisionByReference(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['acrOrg']), Atexo_Util::atexoHtmlEntities($_GET['lot']));
            $this->RepeaterListEntrepriseSelectionne->dataBind();
        } else {
            $this->panelConsultationsDetailAccordCadre->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('INVITE_CONSULTATION'));
        }
    }

    /**
     * Trier un tableau des entreprises sélctionnés.
     *
     * @param sender
     * @param param
     */
    public function sortEntreprise($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $entreprise = (new Atexo_Consultation_Responses())->allEnveloppeDecisionByReference(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['acrOrg']));

        if ('entreprise' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($entreprise, 'nomEntreprise');
        } else {
            return;
        }
        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->RepeaterListEntrepriseSelectionne->DataSource = $dataSourceTriee;
        $this->RepeaterListEntrepriseSelectionne->DataBind();

        // Re-affichage du TActivePanel
        $this->PanelListEntreprise->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere)
    {
        $this->setCritereAndTypeTri($critere);
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);

        return $datatriee;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }
}
