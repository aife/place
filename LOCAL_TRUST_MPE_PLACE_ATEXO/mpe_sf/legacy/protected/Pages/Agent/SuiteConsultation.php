<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SuiteConsultation extends MpeTPage
{
    public string $_refConsultation = '';
    public $_organisme = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $lots = null;
        $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        if (isset($_GET['org']) && '' != $_GET['org']) {
            $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if (!$this->isPostBack) {
            $consultation = (new Atexo_Consultation())->retrieveCommonConsultation(base64_decode($this->_refConsultation), $this->_organisme);
            if ($consultation instanceof CommonConsultation) {
                $lots = $consultation->getAllLots();
            }
            if (is_countable($lots) ? count($lots) : 0) {
                $this->suiteConsultation->DataSource = $lots;
                $this->suiteConsultation->DataBind();
            } else {
                $this->response->redirect('?page=Agent.FormulaireConsultation&fullWidth=true&continuation=1&id='.$this->_refConsultation);
            }
        }
    }

    public function redirectToTableauDeBord()
    {
        $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.base64_decode($this->_refConsultation));
    }

    public function selectLots()
    {
        if ($this->tousLots->Checked) {
            $this->response->redirect('?page=Agent.FormulaireConsultation&continuation=1&fullWidth=true&id='.$this->_refConsultation);
        } else {
            $lots = '';
            foreach ($this->suiteConsultation->getItems() as $item) {
                if ($item->lot->Checked) {
                    $lots .= $item->numLot->Value.'#';
                }
            }
            $this->response->redirect('?page=Agent.FormulaireConsultation&continuation=1&fullWidth=true&id='.$this->_refConsultation.'&lots='.base64_encode(substr($lots, 0, -1)));
        }
    }
}
