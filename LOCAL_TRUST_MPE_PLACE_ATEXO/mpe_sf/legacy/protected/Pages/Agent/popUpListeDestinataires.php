<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe popUpListeDestinataires.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpListeDestinataires extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                $this->AdressesRegistreRetraits->Text = Atexo_Message::getAdressesRegistreRetraitAndTelechargement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->AdressesRegistreQuestions->Text = Atexo_Message::getAdressesRegistreQuestion(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
                $this->AdressesRegistreDepots->Text = Atexo_Message::getAdressesRegistreDepot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), true);
            }
        }
        $script = '<script>checkAdresse(); ';
        if ($_GET['forInvitationConcourir']) {
            $script .= " document.getElementById('ctl0_CONTENU_PAGE_adressesLibres').value = opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value;";
        }

        $this->javascript->Text = $script.'</script>';
    }

    public function isRegistreRetraitVisible()
    {
        if ('modifConsult' == $_GET['typeMsg']) {
            return true;
        }

        return true;
    }

    public function isRegistreQuestionVisible()
    {
        if ('modifConsult' == $_GET['typeMsg']) {
            return true;
        }

        return true;
    }

    public function isRegistreDepotVisible()
    {
        if ('modifConsult' == $_GET['typeMsg']) {
            return true;
        }

        return true;
    }

    public function isAdresseLibreVisible()
    {
        if ('modifConsult' == $_GET['typeMsg']) {
            return true;
        }

        return true;
    }

    /**
     * retourne toutes les adresse sélectionnées par l'utilisateur.
     */
    private function getAllSelectedEmails()
    {
        $message = '';
        if ($this->isRegistreRetraitVisible() && $this->checkAdressesRegistreRetraits->Checked) {
            if ('' !== trim($this->AdressesRegistreRetraits->Text)) {
                $message .= $this->AdressesRegistreRetraits->Text;
            }
        }
        if ($this->isRegistreQuestionVisible() && $this->checkAdressesRegistreQuestions->Checked) {
            if ('' !== trim($this->AdressesRegistreQuestions->Text)) {
                if ($message) {
                    $message .= ',';
                }
                $message .= $this->AdressesRegistreQuestions->Text;
            }
        }
        if ($this->isRegistreDepotVisible() && $this->checkAdressesRegistreDepots->Checked) {
            if ('' !== trim($this->AdressesRegistreDepots->Text)) {
                if ($message) {
                    $message .= ',';
                }
                $message .= $this->AdressesRegistreDepots->Text;
            }
        }
        if ($this->isAdresseLibreVisible() && $this->checkAdressesLibres->Checked) {
            if ('' !== trim($this->adressesLibres->Text)) {
                if ($message) {
                    $message .= ',';
                }
                $message .= $this->adressesLibres->Text;
            }
        }

        return $message;
    }

    public function Valider()
    {
        if ($this->IsValid) {
            $message = $this->getAllSelectedEmails();
            $AdrLibre = $this->adressesLibres->Text;
            $this->javascript->Text = "<script>remplirHiddenCheckedAdresse();
			opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adressesLibresHidden').value = '".$AdrLibre."';
			opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value = '".$message."';window.close();
			</script>";
            //echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_adressesLibresHidden').value = '$AdrLibre';</script>";
            //echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value = '$message';window.close();</script>";
        }
    }
}
