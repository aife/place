<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;

/**
 * popup Ajout Attributaire.
 *
 * @author Fatima Ezzahra <fatima.izgua@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 4.11.0
 */
class PopUpAjoutAttributaire extends MpeTPage
{
}
