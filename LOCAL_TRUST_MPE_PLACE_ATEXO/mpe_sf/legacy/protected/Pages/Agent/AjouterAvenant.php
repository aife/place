<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAvenant;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Avenant;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Passation;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AjouterAvenant extends MpeTPage
{
    public bool $_showErreurSurFormat = true;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->fillVuPar();
            $this->fillTypeAvenant();
            $this->fillAvisCao();
            if (isset($_GET['idA'])) {
                $avenant = (new Atexo_SuiviPassation_Avenant())->retrieveAvenantByPk(Atexo_Util::atexoHtmlEntities($_GET['idA']), Atexo_CurrentUser::getCurrentOrganism());

                $this->numAvenant->Text = $avenant->getNumeroAvenant();

                if ($avenant->getTypeAvenant()) {
                    $this->typeAvenant->SelectedValue = $avenant->getTypeAvenant();
                }
                $this->objetAvenant->Text = $avenant->getObjetAvenant();
                $this->montantAvenantHt->Text = $avenant->getMontantAvenantHt();
                $this->montantAvenantTtc->Text = $avenant->getMontantAvenantTtc();
                $this->pourcentAugmentationMarcheInitial->Text = $avenant->getPourcentageAugmentationMarcheInitial();
                $this->pourcentAugmentationCumul->Text = $avenant->getPourcentageAugmentationCumule();
                $this->montantAvenantCumulHt->Text = $avenant->getMontantTotalMarcheToutAvenantCumule();
                $this->dateReceptionProjet->Text = $avenant->getDateReceptionProjetParSecretaireCao();
                $this->dateReceptionRapportAvenantSV->Text = $avenant->getDateReceptionProjetParChargeEtude();
                $this->dateObservationsSVProjetRapportAvenant->Text = $avenant->getDateObservationParSv();
                $this->dateRetourProjetRapportAvenant->Text = $avenant->getDateRetourProjet();
                $this->validationProjetRapportAvenant->Text = $avenant->getDateValidationProjet();
                if ($avenant->getDateValidationProjetVuPar()) {
                    $this->validationProjetRapportAvenantVuPar->SelectedValue = $avenant->getDateValidationProjetVuPar();
                }
                $this->dateCao->Text = $avenant->getDateCao();
                if ($avenant->getAvisCao()) {
                    $this->avisCAO->SelectedValue = $avenant->getAvisCao();
                }
                $this->dateCp->Text = $avenant->getDateCp();
                $this->dateSignatureAvenantParPA->Text = $avenant->getDateSignatureAvenant();
                $this->dateReceptionDossierCLParSV->Text = $avenant->getDateReceptionDossier();
                $this->dateObservationsSVDossierAvenant->Text = $avenant->getDateFormulationObservationParSvSurSdossier();
                $this->dateRetourDossierFinalAvenant->Text = $avenant->getDateRetourDossierFinalise();
                $this->dateValidationDossierFinaliseAvenant->Text = $avenant->getDateValidationDossierFinalise();
                $this->dateTransmissionPrefectureAvenant->Text = $avenant->getDateTransmissionPrefecture();
                $this->dateNotificationAvenant->Text = $avenant->getDateNotification();
                if ($avenant->getOperationValidationVuesPar()) {
                    $this->dateNotificationAvenantVuPar->SelectedValue = $avenant->getOperationValidationVuesPar();
                }
                $this->commentairesAvenant->Text = $avenant->getCommentaires();
            }
        }
    }

    public function EnregistrerAvenant()
    {
        if (isset($_GET['idA'])) {
            $avenant = (new Atexo_SuiviPassation_Avenant())->retrieveAvenantByPk($_GET['idA'], Atexo_CurrentUser::getCurrentOrganism());
        } else {
            $avenant = new CommonAvenant();
            $avenant->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $avenant->setIdContrat(Atexo_Util::atexoHtmlEntities($_GET['idC']));
        }

        $avenant->setNumeroAvenant($this->numAvenant->Text);
        //if($this->typeAvenant->SelectedValue) {
        $avenant->setTypeAvenant($this->typeAvenant->SelectedValue);
        //}
        $avenant->setObjetAvenant($this->objetAvenant->Text);
        $avenant->setMontantAvenantHt(str_replace('.', ',', $this->montantAvenantHt->Text));
        $avenant->setMontantAvenantTtc(str_replace('.', ',', $this->montantAvenantTtc->Text));
        $avenant->setPourcentageAugmentationMarcheInitial(str_replace('.', ',', $this->pourcentAugmentationMarcheInitial->Text));
        $avenant->setPourcentageAugmentationCumule(str_replace('.', ',', $this->pourcentAugmentationCumul->Text));
        $avenant->setMontantTotalMarcheToutAvenantCumule(str_replace('.', ',', $this->montantAvenantCumulHt->Text));
        $avenant->setDateReceptionProjetParSecretaireCao($this->dateReceptionProjet->Text);
        $avenant->setDateReceptionProjetParChargeEtude($this->dateReceptionRapportAvenantSV->Text);
        $avenant->setDateObservationParSv($this->dateObservationsSVProjetRapportAvenant->Text);
        $avenant->setDateRetourProjet($this->dateRetourProjetRapportAvenant->Text);
        $avenant->setDateValidationProjet($this->validationProjetRapportAvenant->Text);
        //if($this->validationProjetRapportAvenantVuPar->SelectedValue) {
        $avenant->setDateValidationProjetVuPar($this->validationProjetRapportAvenantVuPar->SelectedValue);
        //}
        $avenant->setDateCao($this->dateCao->Text);
        //if($this->avisCAO->SelectedValue) {
        $avenant->setAvisCao($this->avisCAO->SelectedValue);
        // }
        $avenant->setDateCp($this->dateCp->Text);
        $avenant->setDateSignatureAvenant($this->dateSignatureAvenantParPA->Text);
        $avenant->setDateReceptionDossier($this->dateReceptionDossierCLParSV->Text);
        $avenant->setDateFormulationObservationParSvSurSdossier($this->dateObservationsSVDossierAvenant->Text);
        $avenant->setDateRetourDossierFinalise($this->dateRetourDossierFinalAvenant->Text);
        $avenant->setDateValidationDossierFinalise($this->dateValidationDossierFinaliseAvenant->Text);
        $avenant->setDateTransmissionPrefecture($this->dateTransmissionPrefectureAvenant->Text);
        $avenant->setDateNotification($this->dateNotificationAvenant->Text);
        //if($this->dateNotificationAvenantVuPar->SelectedValue) {
        $avenant->setOperationValidationVuesPar($this->dateNotificationAvenantVuPar->SelectedValue);
        //}
        $avenant->setCommentaires($this->commentairesAvenant->Text);

        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avenant->save($propelConnection);

        $this->response->redirect('index.php?page=Agent.FicheMarche&idDE='.Atexo_Util::atexoHtmlEntities($_GET['idDE']).'&idLot='.Atexo_Util::atexoHtmlEntities($_GET['idLot']));
    }

    public function fillVuPar()
    {
        $agents = Atexo_Agent::retriveAgentsByOrganisme(Atexo_CurrentUser::getOrganismAcronym(), false);
        if (is_array($agents)) {
            $agents = Atexo_Util::arrayUnshift($agents, Prado::localize('TEXT_SELECTION_LISTE'));
            $this->validationProjetRapportAvenantVuPar->Datasource = $agents;
            $this->validationProjetRapportAvenantVuPar->dataBind();

            $this->dateNotificationAvenantVuPar->Datasource = $agents;
            $this->dateNotificationAvenantVuPar->dataBind();
        }
    }

    public function fillTypeAvenant()
    {
        $typeAvenant = (new Atexo_SuiviPassation_Avenant())->retriveTypesAvenant(Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($typeAvenant)) {
            $typeAvenant = Atexo_Util::arrayUnshift($typeAvenant, Prado::localize('TEXT_SELECTION_LISTE'));
            $this->typeAvenant->Datasource = $typeAvenant;
            $this->typeAvenant->dataBind();
        }
    }

    public function fillAvisCao()
    {
        $avis = (new Atexo_SuiviPassation_Passation())->retrieveAvisCao(Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($avis)) {
            $avis = Atexo_Util::arrayUnshift($avis, '---'.Prado::localize('A_RENSEIGNER').'---');
            $this->avisCAO->Datasource = $avis;
            $this->avisCAO->dataBind();
        }
    }
}
