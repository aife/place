<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Propel\Mpe\CommonReferentielTypeXml;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_ReferentielFormXml;
use Application\Service\Atexo\Publicite\Atexo_Publicite_RenseignementsBoamp;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupBoamp extends MpeTPage
{
    private $_formXml;
    private $_acheteurPublic;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        header('Pragma:nocache');
        if (isset($_GET['idFormXml'])) {
            $this->_formXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveReferentielFormXmlById(Atexo_Util::atexoHtmlEntities($_GET['idFormXml']));
            $this->_acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($this->_formXml[0]->getIdCompteBoamp(), Atexo_CurrentUser::getOrganismAcronym());
        }
        if ($this->_formXml) {
            $this->idTypeFormulaire->Text = $this->_formXml[0]->getIdTypeXml();
            $this->typeRetour->Text = Atexo_Util::atexoHtmlEntities($_GET['itemId']);
            $xml = ($this->_formXml[0]->getXml());
            if (true == strstr($xml, 'Version="1.5">')) {
                // print_r($this->_formXml[0]->getXml());exit;
                $this->donneesModification->setVisible(false);
                $this->donneesInitialisation->setVisible(true);
                $this->donneesInitialisation->Text = htmlentities($xml);
                $this->displayAcheteurPublic();
            } else {
                $this->panelChoixAcheteur->setVisible(false);
                $this->donneesModification->setVisible(true);
                $this->donneesInitialisation->setVisible(false);
                $this->donneesModification->Text = htmlentities($xml);
                $this->script->Text = '<script language="javascript">document.getElementById(\'ctl0_CONTENU_PAGE_buttonSend\').click();</script>';
            }
        }
    }

    public function gotoBoamp($sender, $param)
    {
        $xml = $this->addAcheteurInfoInXml($this->acheteurPublic->SelectedValue);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $formXml = CommonReferentielFormXmlPeer::retrieveByPK($this->_formXml[0]->getId(), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        if ($formXml) {
            $formXml->setIdCompteBoamp($this->acheteurPublic->SelectedValue);
            $formXml->setXml($xml);
            $formXml->save($connexion);
        }
        $this->donneesInitialisation->Text = htmlentities($xml);
        $this->script->Text = '<script language="javascript">document.getElementById(\'ctl0_CONTENU_PAGE_buttonSend\').click();</script>';
    }

    /**
     * afficher les acheteurs publics.
     */
    public function displayAcheteurPublic()
    {
        $idServiceRattach = Atexo_CurrentUser::getIdServiceAgentConnected();
        $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_formXml[0]->getConsultationId(), Atexo_CurrentUser::getOrganismAcronym());
        $idServiceAssocie = $consultation->getServiceAssocieId();
        $listAcheteur = (new Atexo_PublicPurchaser())->retrievePublicPurchasersCons($idServiceRattach, $idServiceAssocie);
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        foreach ($listAcheteur as $oneAcheteur) {
            // $siteBoamp = '';
            $mail = '';
            /*if ($oneAcheteur->getBoampTarget()=='0') {
                $siteBoamp = ' - '.Prado::localize('TEXT_DEMONSTRATION');
            }
            else {
                $siteBoamp = ' - '.Prado::localize('TEXT_PRODUCTION');
            }*/
            if ('' != $oneAcheteur->getBoampMail()) {
                $mail = ' - '.$oneAcheteur->getBoampMail();
            }
            $dataSource[$oneAcheteur->getId()] = $oneAcheteur->getBoampLogin().$mail; //$oneAcheteur->getDenomination() . $mail . $siteBoamp;
        }
        $this->acheteurPublic->DataSource = $dataSource;
        $this->acheteurPublic->DataBind();
    }

    /**
     * Ajouter les informations de l'acheteur public dans l'xml initial.
     */
    public function addAcheteurInfoInXml($idAcheteur)
    {
        $arrayAnnonce = [];
        $this->_acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($idAcheteur, Atexo_CurrentUser::getOrganismAcronym());
        $contents = $this->_formXml[0]->getXml();
        if ('' != $this->_acheteurPublic->getBoampLogin()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LOGIN', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getBoampLogin())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LOGIN', '');
        }
        if ('' != $this->_acheteurPublic->getBoampMail()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MAIL', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getBoampMail())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MAIL', '');
        }

        if ('' != $this->_acheteurPublic->getTypeOrg()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TYPEORG', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getTypeOrg())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TYPEORG', '');
        }

        if ('' != $this->_acheteurPublic->getPrm()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_NOM', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getPrm())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_NOM', '');
        }
        if ('' != $this->_acheteurPublic->getDenomination()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ACHETEUR_PUBLIC', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getDenomination())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ACHETEUR_PUBLIC', '');
        }
        if ('' != $this->_acheteurPublic->getCp()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_CP', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getCp())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_CP', '');
        }
        if ('' != $this->_acheteurPublic->getAdresse()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LIEU_DIT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getAdresse())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LIEU_DIT', '');
        }
        if ('' != $this->_acheteurPublic->getVille()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_VILLE', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getVille())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_VILLE', '');
        }
        if ('' != $this->_acheteurPublic->getTelephone()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_TEL', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getTelephone())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_TEL', '');
        }
        if ('' != $this->_acheteurPublic->getFax()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_FAX', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFax())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_FAX', '');
        }
        if ('' != $this->_acheteurPublic->getMail()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MEL', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getMail())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MEL', '');
        }
        if ('' != $this->_acheteurPublic->getUrl()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_URL', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getUrl())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_URL', '');
        }
        if ('' != $this->_acheteurPublic->getUrlAcheteur()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'PROFILURL', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getUrlAcheteur())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'PROFILURL', '');
        }
        ///////////////////////////Service Facturation////////////////////////////
        if ('' != $this->_acheteurPublic->getFactureNumero()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFactureNumero())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_FACT', '');
        }
        if ('' != $this->_acheteurPublic->getFactureDenomination()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'DENOM_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFactureDenomination())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'DENOM_FACT', '');
        }
        if ('' != $this->_acheteurPublic->getFacturationService()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFacturationService())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_FACT', '');
        }
        if ('' != $this->_acheteurPublic->getFactureAdresse()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFactureAdresse())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_FACT', '');
        }
        if ('' != $this->_acheteurPublic->getFactureCp()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CP_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFactureCp())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CP_FACT', '');
        }
        if ('' != $this->_acheteurPublic->getFactureVille()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFactureVille())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_FACT', '');
        }

        if ('' != $this->_acheteurPublic->getFacturePays()) {
            //$acronyme = Atexo_Geolocalisation_GeolocalisationN2::retrieveAccronymeCountryById($this->_acheteurPublic->getFacturePays());
            $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_FACT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getFacturePays())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_FACT', '');
        }
        ///////////////////////////Adresse de livraison ////////////////////////////

        if ('' != $this->_acheteurPublic->getLivraisonService()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_LIV', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getLivraisonService())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_LIV', '');
        }
        if ('' != $this->_acheteurPublic->getLivraisonAdresse()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_LIV', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getLivraisonAdresse())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_LIV', '');
        }
        if ('' != $this->_acheteurPublic->getLivraisonCodePostal()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CP_LIV', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getLivraisonCodePostal())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CP_LIV', '');
        }
        if ('' != $this->_acheteurPublic->getLivraisonVille()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_LIV', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getLivraisonVille())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_LIV', '');
        }
        if ('' != $this->_acheteurPublic->getLivraisonPays()) {
            //$acronyme = Atexo_Geolocalisation_GeolocalisationN2::retrieveAccronymeCountryById($this->_acheteurPublic->getLivraisonPays());
            $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_LIV', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getLivraisonPays())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_LIV', '');
        }
        $arrayAttributionsIds = explode('#', Atexo_Config::getParameter('IDENTIFIANTS_AVIS_ATTRIBUTION_BOAMP'));
        //if(false){
        if (in_array($this->_formXml[0]->getIdTypeXml(), $arrayAttributionsIds)) {
            $consultationId = $this->_formXml[0]->getConsultationId();
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $typeForm = (new Atexo_Publicite_ReferentielFormXml())->retreiveIdTypeFormXmlByIdAvisMarche($this->_formXml[0]->getIdTypeXml());
            if ($typeForm instanceof CommonReferentielTypeXml) {
                $idTypeAvis = $typeForm->getIdAvisMarche();
                $arrayAnnonce = (new Atexo_Publicite_Destinataire())->retreiveAnnonceWithFormXml($consultationId, $organisme, $idTypeAvis);
            }
            if (1 == (is_countable($arrayAnnonce) ? count($arrayAnnonce) : 0)) {
                $annoncePu = $arrayAnnonce[0];
                $contents = $this->initialisationAttribution($contents, $annoncePu);
                $formulaire = $annoncePu->getReferentielformxml();
                // Pour des informations supplémentaires sont hérités dans la descritption du marché
                $this->donneesPubPourHeritageAvisAttribution->Text = htmlentities($formulaire->getXml());
            }
        }
        $contents = $this->addAdresseInformation($contents);
        if ('' != $this->_acheteurPublic->getCodeNuts()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NUTSVALEUR', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getCodeNuts())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'NUTSVALEUR', '');
        }

        if ('' != $this->_acheteurPublic->getModalitesFinancement()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'MODALITES_FINANCEMENT', Atexo_Util::escapeXmlChars(($this->_acheteurPublic->getModalitesFinancement())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'MODALITES_FINANCEMENT', '');
        }
        //print_r($contents);exit;
        return $contents;
    }

    public function getUrlStyleBoamp()
    {
        return Atexo_Util::getAbsoluteUrl().$this->themesPath().'/boamp/css/';
    }

    public function getUrlStyleCpv()
    {
        return Atexo_Util::getAbsoluteUrl().$this->themesPath().'/cpv';
    }

    public function addAdresseInformation($contents)
    {
        // Type de pouvoir adjudicateur et activité(s) principale(s)
        if ('' != $this->_acheteurPublic->getTypePouvoirActivite()) {
            $idsPouvoirActivite = explode('#', $this->_acheteurPublic->getTypePouvoirActivite());
            foreach ($idsPouvoirActivite as $oneId) {
                $idsArray[] = str_replace('_', '', strstr($oneId, '_'));
            }
            $typePouvoirActivite = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByArrayId($idsArray);
            $activites = '';
            $activitesSecteursSpeciaux = '';

            foreach ($typePouvoirActivite as $oneType) {
                if ($oneType->getIdReferentiel() == Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR')) {
                    $activites .= $oneType->getLibelle2();
                } elseif ($oneType->getIdReferentiel() == Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE')) {
                    $activitesSecteursSpeciaux .= $oneType->getLibelle2();
                }
            }
            if ('' != $activites) {
                $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ACTIVITES', $activites);
            } else {
                $contents = Atexo_Util::xmlStrReplace($contents, '<activites>TEXT_ACTIVITES</activites>', '');
            }
            if ('' != $activitesSecteursSpeciaux) {
                $contents = Atexo_Util::xmlStrReplace($contents, 'SECTEURS_SPECIAUX', $activitesSecteursSpeciaux);
            } else {
                $contents = Atexo_Util::xmlStrReplace($contents, '<activitesSecteursSpeciaux>SECTEURS_SPECIAUX</activitesSecteursSpeciaux>', '');
            }
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, '<activites>TEXT_ACTIVITES</activites>', '');
            $contents = Atexo_Util::xmlStrReplace($contents, '<activitesSecteursSpeciaux>SECTEURS_SPECIAUX</activitesSecteursSpeciaux>', '');
        }

        // Adresses complémentaires  &  Procédures de recours
        $renseignementsBoamp = (new Atexo_Publicite_RenseignementsBoamp())->retrieveRenseignementsBoampByIdType($this->_acheteurPublic->getId());
        if ((is_countable($renseignementsBoamp) ? count($renseignementsBoamp) : 0) > 0) {
            $adressesComplt = '<adressesComplt>';
            $admOuTech = '<AdmOuTech>';
            $adresseRecours = '<adresseRecours>';
            $blocComplt = '';
            $typeRensA = '';
            //print_r($renseignementsBoamp);exit;
            foreach ($renseignementsBoamp as $oneType) {
                $blocComplt .= '<PersonnePhysique><nom>'.Atexo_Util::escapeXmlChars($oneType->getCorrespondant()).'</nom></PersonnePhysique>';
                $blocComplt .= '<PersonneMorale>'.Atexo_Util::escapeXmlChars($oneType->getOrganisme()).'</PersonneMorale>';
                $blocComplt .= '<adr>';
                $blocComplt .= '<cp>'.$oneType->getCp().'</cp>';
                $blocComplt .= '<lieu_dit>'.Atexo_Util::escapeXmlChars($oneType->getAdresse()).'</lieu_dit>';
                $blocComplt .= '<ville>'.Atexo_Util::escapeXmlChars($oneType->getVille()).'</ville>';
                //$blocComplt .= "<pays>".$oneType->getPays()."</pays>";
                $blocComplt .= '</adr>';
                $blocComplt .= '<coord>';
                $blocComplt .= '<tel>'.$oneType->getTelephone().'</tel>';
                $blocComplt .= '<fax>'.$oneType->getFax().'</fax>';
                $blocComplt .= '<poste>'.$oneType->getPoste().'</poste>';
                $blocComplt .= '<mel>'.$oneType->getMail().'</mel>';

                $urlAccesDirect = '';
                //type 3 et 4 : adresses obtenir les documents et déposer les offres
                if (($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3'))
                    || ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4'))
                    ) {
                    // Ajout de l'url d'accès direct de la consultation par défaut
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_formXml[0]->getConsultationId(), null);
                    $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
                }
                //type 1,2,5: autres adresses administratif et technique
                else {
                    if ('' != $oneType->getUrl()) {
                        $urlAccesDirect = $oneType->getUrl();
                    } else {
                        if ('' != $oneType->getCorrespondant() ||
                            '' != $oneType->getOrganisme() ||
                            '' != $oneType->getCp() ||
                            '' != $oneType->getAdresse() ||
                            '' != $oneType->getVille() ||
                            '' != $oneType->getTelephone() ||
                            '' != $oneType->getFax() ||
                            '' != $oneType->getPoste() ||
                            '' != $oneType->getMail()
                        ) {
                            // Ajout de l'url d'accès direct de la consultation si elle est vide uniquement s'il existe une donnée saisie dans le bloc d'info
                            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_formXml[0]->getConsultationId(), null);
                            $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
                        }
                    }
                }
                //bug fix en attendant v1.9 module BOAMP car problème de validation des URLs
                $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
                $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
                $blocComplt .= '<url>'.$urlEncode.'</url>';

                $blocComplt .= '</coord>';
                // Obtenir des renseignements d'ordre administratif
                if ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1')) {
                    $admOuTech .= '<adm>';
                    $admOuTech .= $blocComplt;
                    $admOuTech .= '</adm>';
                }

                // Obtenir des renseignements d'ordre technique
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2')) {
                    $admOuTech .= '<tech>';
                    $admOuTech .= $blocComplt;
                    $admOuTech .= '</tech>';
                }

                /////////////////////////////////////////////////////////////////
                // Obtenir les documents
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3')) {
                    $typeRensA .= '<document>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</document>';
                }

                // Envoyer les offres/candidatures/projets/demandes de participation
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4')) {
                    $typeRensA .= '<envoi>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</envoi>';
                }

                // Obtenir des renseignements complémentaires
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5')) {
                    $typeRensA .= '<rensComplementaires>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</rensComplementaires>';
                }
                /////////////////////////////////////////////////////////////////

                // Instance chargée des procédures de recours
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_1')) {
                    $adresseRecours .= '<instanceRecours>';
                    $adresseRecours .= $blocComplt;
                    $adresseRecours .= '</instanceRecours>';
                }

                // Organe chargé des procédures de médiation
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_2')) {
                    $adresseRecours .= '<mediation>';
                    $adresseRecours .= $blocComplt;
                    $adresseRecours .= '</mediation>';
                }

                // Service auprès duquel des renseignements
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_4')) {
                    $adresseRecours .= '<serviceIntroduction>';
                    $adresseRecours .= $blocComplt;
                    $adresseRecours .= '</serviceIntroduction>';
                }

                // Introduction des recours
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_3')) {
                    $adresseRecours .= '<introductionRecours>';
                    $adresseRecours .= $oneType->getOrganeChargeProcedure();
                    $adresseRecours .= '</introductionRecours>';
                }

                $blocComplt = '';
            }
            $admOuTech .= '</AdmOuTech>';
            $adressesComplt .= $admOuTech;
            $adressesComplt .= $typeRensA;
            $adressesComplt .= '</adressesComplt>';

            $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);
            $adresseRecours .= '</adresseRecours>';
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--adresseRecours-->', $adresseRecours);
        }
        //ancien cas, les informations des adresses ne sont pas complétées
        else {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_formXml[0]->getConsultationId(), null);
            $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
            if ('' != $urlAccesDirect) {
                $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
                $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
            } else {
                $urlEncode = '';
            }
            $adressesComplt = '';
            $adressesComplt .= '<adressesComplt>
	        						<document>
	        							<coord>
	        								<url>'.$urlEncode.'</url>
	        							</coord>
	        						</document>';
            $adressesComplt .= '<envoi>
		    							<coord>
		    								<url>'.$urlEncode.'</url>
		    							</coord>
		    				   		</envoi>';
            $adressesComplt .= '</adressesComplt>';

            $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);
        }

        return $contents;
    }

    /**
     * Si avis d'attribution initialisation du premier block par les identifiants du premier avis PU.
     */
    public function initialisationAttribution($contents, $annoncePu)
    {
        // recherche de XML Avis initial (Avis de marché BOAMP) déjà validé
        //$formulaire = $annoncePu->getReferentielformxml();
        //$xml = $formulaire->getXml();
        $avisInitial = '<avisInitial>';
        $rectif = '';
        if ($annoncePu->getParution()) {
            $rectif .= '<parution>'.$annoncePu->getParution().'</parution>';
        }
        if ($annoncePu->getNumAnn()) {
            $rectif .= '<num_ann_par>'.$annoncePu->getNumAnn().'</num_ann_par>';
        }
        if ($annoncePu->getDatePub()) {
            $rectif .= '<datePub>'.Atexo_Util::iso2frnDate($annoncePu->getDatePub()).'</datePub>';
        }
        if ('A' == $annoncePu->getTypeBoamp()) {
            $avisInitial .= '<A>';
            $avisInitial.$rectif;
            $avisInitial .= '</A>';
            $avisInitial .= '<rectifA>';
            $avisInitial.$rectif;
            $avisInitial .= '</rectifA>';
        } elseif ('B' == $annoncePu->getTypeBoamp()) {
            $avisInitial .= '<B>';
            $avisInitial .= $rectif;
            $avisInitial .= '</B>';
            $avisInitial .= '<rectifB>';
            $avisInitial .= $rectif;
            $avisInitial .= '</rectifB>';
        }
        $avisInitial .= '</avisInitial>';
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--avisInitial-->', $avisInitial);

        return $contents;
    }
}
