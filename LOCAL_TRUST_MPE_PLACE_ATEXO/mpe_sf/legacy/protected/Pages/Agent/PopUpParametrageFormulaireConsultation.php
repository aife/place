<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/**
 * Permet de paramétrer un formulaire de consultation.
 *
 * @author Houriya MAATALLA <Houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class PopUpParametrageFormulaireConsultation extends MpeTPage
{
    private $_formulaireConsultation;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $formulaireConsultation = (new Atexo_FormConsultation())->retrieveFormulaireConsultationById(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->_formulaireConsultation = $formulaireConsultation;
        if ($formulaireConsultation) {
            $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($formulaireConsultation->getConsultationId());
            if ($consultation instanceof CommonConsultation) {
                $this->IdConsultationSummary->setConsultation($consultation);
                if (!$this->isPostBack) {
                    $this->setViewState('typeFormulaire', $formulaireConsultation->getTypeFormulaire());
                    if ($this->isBoredereauPrix()) {
                        $this->titre->Text = Prado::localize('TEXT_BORDEREAU_PRIX');
                    } elseif ($this->isQuestionnaire()) {
                        $this->titre->Text = Prado::localize('TEXT_QUESTIONNAIRE');
                    }
                    $this->refFormulaire->Text = $formulaireConsultation->getConsultationId();
                    $this->enveloppeFormulaire->Text = $formulaireConsultation->getLibelleTypeEnveloppe();
                    if ($formulaireConsultation->getLot()) {
                        $this->numLot->Text = Prado::localize('TEXT_LOT').' '.$formulaireConsultation->getLot().' - ';
                    }
                }
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function isQuestionnaire()
    {
        return $this->getViewState('typeFormulaire') == Atexo_Config::getParameter('QUESTIONNAIRE');
    }

    public function isBoredereauPrix()
    {
        return $this->getViewState('typeFormulaire') == Atexo_Config::getParameter('BORDEREAU_DE_PRIX');
    }

    /**
     * Permet de mettre à jour le statut d'une formulaire.
     *
     * @param
     */
    public function mettreAJourStatutFormulaire($sender, $param)
    {
        if ($this->statutFormulaire->value && $this->uidFormulaire->value) {
            $idForm = Atexo_Util::builIdFromUidPlateforme($this->uidFormulaire->value);
            if ($idForm) {
                (new Atexo_FormConsultation())->updateStatutFormulaireConsultation($this->statutFormulaire->value, $idForm);
            }
            $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_AtexoFormulaire_refreshPanel').click();window.close();</script>";
        }
    }
}
