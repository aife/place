<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EnvoiCourrierElectroniqueNotification.
 *
 * @author Khalid Atlassi <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 */
class EnvoiCourrierElectroniqueNotification extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode(($_GET['idContrat'])));
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat);
        if ($contrat instanceof CommonTContratTitulaire) {
            $refConsultation = $contrat->getRefConsultation();
            $contact = $contrat->getCommonTContactContrat();
            if ($contact instanceof CommonTContactContrat) {
                $mail = $contact->getEmail();
            }
        }
        $_GET['id'] = $refConsultation;
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('TYPE_MESSAGE_NOTIFICATION');
        $this->TemplateEnvoiCourrierElectronique->messageType->setEnabled(false);
        $this->TemplateEnvoiCourrierElectronique->setRedirect('?page=Agent.ouvertureEtAnalyse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&decision');
        if (!$this->IsPostBack) {
            $this->TemplateEnvoiCourrierElectronique->destinataires->Text = $mail;
            $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueUniquementLienTelechargementObligatoire->checked = true;
        }
        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }
}
