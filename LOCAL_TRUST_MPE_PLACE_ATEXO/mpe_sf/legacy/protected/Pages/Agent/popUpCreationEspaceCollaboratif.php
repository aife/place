<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTEspaceCollaboratif;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EspaceCollaboratif;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 29 mai 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class popUpCreationEspaceCollaboratif extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->nameEspaceCollaboratif->Text = self::getSiteName();
        }
    }

    public function getSiteName()
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(base64_decode($_GET['id']));
        if ($consultation instanceof CommonConsultation) {
            $siteName = $consultation->getReferenceUtilisateur().' - '.$consultation->getIntituleTraduit();

            return $siteName;
        }
    }

    public function onValider()
    {
        $espaceCollaboratif = (new CommonTEspaceCollaboratif())->getTEspaceCollaboratifByName($this->nameEspaceCollaboratif->getText());
        if ($espaceCollaboratif) {
            $this->msgErreurEspaceCollaboratif->Style = 'block';

            return false;
        } else {
            if ($_GET['id']) {
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(base64_decode($_GET['id']));
                (new Atexo_EspaceCollaboratif())->createConsultationSite($consultation);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $espaceCollaboratif = new CommonTEspaceCollaboratif();
                $espaceCollaboratif->setReference(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
                $espaceCollaboratif->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                $espaceCollaboratif->setNomEspace($this->nameEspaceCollaboratif->getText());
                $espaceCollaboratif->save($connexion);
                $this->script->Text = "<script language='javascript'>".
                                         "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_hiddenConsulter').click();".
                                         //"window.opener.document.location.reload(true);" .
                                         'window.opener.location.replace(window.opener.location.href);'.
                                         'window.close();'.
                                     '</script>';
            }
        }
    }
}
