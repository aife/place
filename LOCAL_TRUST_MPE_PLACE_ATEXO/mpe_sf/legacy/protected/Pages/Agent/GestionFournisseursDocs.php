<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SuppliersOfDocuments;

/**
 * Admministration des fournisseurs de documents.
 */
class GestionFournisseursDocs extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        // AEL : 04/06/2008
        if (Atexo_CurrentUser::hasHabilitation('GestionFournisseursEnvoisPostaux')) {
            $this->panelValidationSummary->visible = false;
            $this->gestionFournissseurs->visible = true;
            if (Atexo_Module::isEnabled('GestionFournisseursDocsMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            if (!$this->IsPostBack) {
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
                $this->lienAjouterFournisseur->NavigateUrl = "javascript:popUp('index.php?page=Agent.AjoutFournisseur&idService=".$this->getViewState('idService')."','yes');";
                $fournisseursDocs = Atexo_Consultation_SuppliersOfDocuments::search($this->getViewState('idService'));
                $this->RepeaterFournisseursDocs->DataSource = $fournisseursDocs;
                $this->RepeaterFournisseursDocs->DataBind();
            }
        } else {
            $this->panelValidationSummary->visible = true;
            $this->gestionFournissseurs->visible = false;
        }
    }

    public function getSelectedEntityPurchase()
    {
        return $this->getViewState('idService');
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouterFournisseur->NavigateUrl = "javascript:popUp('index.php?page=Agent.AjoutFournisseur&idService=".$this->_selectedEntity."','yes');";
        $fournisseursDocs = Atexo_Consultation_SuppliersOfDocuments::search($this->getViewState('idService'));
        $this->RepeaterFournisseursDocs->DataSource = $fournisseursDocs;
        $this->RepeaterFournisseursDocs->DataBind();
        // Rafraichissement du ActivePanel
        $this->ApRepeaterFournisseursDocs->render($param->NewWriter);
    }

    public function onDeleteClick($sender, $param)
    {
        $idFournisseur = $param->CommandParameter;
        Atexo_Consultation_SuppliersOfDocuments::delete($idFournisseur);
        $fournisseursDocs = Atexo_Consultation_SuppliersOfDocuments::search($this->getViewState('idService'));
        $this->RepeaterFournisseursDocs->DataSource = $fournisseursDocs;
        $this->RepeaterFournisseursDocs->DataBind();
    }

    public function refreshRepeaterFournisseurs($sender, $param)
    {
        // Rafraichissement du ActivePanel
        $fournisseursDocs = Atexo_Consultation_SuppliersOfDocuments::search($this->getViewState('idService'));
        $this->RepeaterFournisseursDocs->DataSource = $fournisseursDocs;
        $this->RepeaterFournisseursDocs->DataBind();
        $this->ApRepeaterFournisseursDocs->render($param->NewWriter);
    }
}
