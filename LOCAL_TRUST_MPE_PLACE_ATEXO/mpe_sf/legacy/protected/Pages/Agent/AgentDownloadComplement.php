<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Permet de télécharger autres pièces.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentDownloadComplement extends DownloadFile
{
    private $_reference;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $complement = '';
        $idSeviceAgent = Atexo_CurrentUser::getCurrentServiceId();
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $complement = (new Atexo_Consultation_Complements())->getComplement($this->_reference, Atexo_CurrentUser::getOrganismAcronym());
        // Seul les agents ayant le droit de voir la consultation qui peuvent télécharger autres pieces
        $consultation = new Atexo_Consultation();
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($this->_reference);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService($idSeviceAgent);
        $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationFound = $consultation->search($consultationCriteria);
        if (count($consultationFound) > 0) {
            if ($complement) {
                $this->_idFichier = $complement->getComplement();
                $this->_nomFichier = $complement->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getOrganismAcronym());
                exit;
            }
        }
    }
}
