<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_OuverturePlis;
use Exception;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ReceptionBlocDechiffre extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_POST['blocDechiffre'])) {
            try {
                (new Atexo_Consultation_OuverturePlis())->receptionBlocDechiffre(Atexo_Util::atexoHtmlEntities($_POST['idBlob']), Atexo_Util::atexoHtmlEntities($_POST['blocDechiffre']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                echo 'OK';
            } catch (Exception $e) {
                echo 'ERR';
                echo "\n".$e->getMessage()."\n".$e->getTraceAsString();
            }
        } elseif (isset($_POST['endDecrypt'])) {
            if (1 == $_POST['endDecrypt']) { // ouverture en ligne
                try {
                    (new Atexo_Consultation_OuverturePlis())->blocsDechiffres(Atexo_Util::atexoHtmlEntities($_POST['idBlob']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'ERR';
                    echo "\n".$e->getMessage()."\n".$e->getTraceAsString();
                }
            } elseif (2 == $_POST['endDecrypt']) { // Ouverture mixte
                try {
                    Atexo_Consultation_OuverturePlis::enveloppeOuverteAdistance(Atexo_Util::atexoHtmlEntities($_POST['idEnveloppe']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'ERR';
                    echo $e->getMessage().Atexo_Util::atexoHtmlEntities($_POST['organisme'])."\n".$e->getTraceAsString();
                }
            }
        }
        exit;
    }
}
