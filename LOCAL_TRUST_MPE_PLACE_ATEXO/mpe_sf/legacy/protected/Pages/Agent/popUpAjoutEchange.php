<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe popUpAjoutEchange.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpAjoutEchange extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $objetMessage = null;
        $this->panelMessageErreur->SetVisible(false);

        if (!$this->IsPostBack) {
            $this->remplirListAR(Atexo_Message::getAllTypesAR(Atexo_CurrentUser::getCurrentOrganism()), true);
            $this->remplirListFormat(Atexo_Message::getAllFormatsEchange(Atexo_CurrentUser::getCurrentOrganism()), true);

            if (isset($_GET['idEchangeDestinataire'])) {
                $this->setViewState('idEchangeDestinataire', Atexo_Util::atexoHtmlEntities($_GET['idEchangeDestinataire']));
                $objetEchangeDestinataire = Atexo_Message::getEchangeDestinataireByid(
                    $this->getViewState('idEchangeDestinataire'),
                    Atexo_CurrentUser::getCurrentOrganism()
                );
                // recuperer l'echange
                if ($objetEchangeDestinataire) {
                    $idRef = $objetEchangeDestinataire->getCommonEchange(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')))->getConsultationId();

                    // vérification de l'autorisation d'acces de l'agent connecte au message accedé
                    if ((new Atexo_Consultation())->retreiveConsultationAgent(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getCurrentOrganism(), Atexo_CurrentUser::getId(), $idRef)) {
                        $this->getObjetEchangeDestinataire($objetEchangeDestinataire);
                    } else {
                        exit;
                    }
                }

                if ($objetEchangeDestinataire) {
                    $objetMessage = Atexo_Message::retrieveMessageById($objetEchangeDestinataire->getIdEchange(), Atexo_CurrentUser::getCurrentOrganism());
                    $this->getObjetEchange($objetMessage);
                }
                if ($objetEchangeDestinataire->getIdEchange()) {
                    $this->setViewState('idMsg', $objetEchangeDestinataire->getIdEchange());
                }
                if ($objetMessage) {
                    $this->remplirTableauPJ($objetMessage->getCommonEchangePieceJointes(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'))));
                }
            }
        }
    }

    public function remplirListAR($listeAR, $withSelc = null)
    {
        $dataAR = [];
        if ($withSelc) {
            $dataAR[' '] = Prado::localize('DEFINE_TEXT_PRECISER').'...';
        }
        if (is_array($listeAR)) {
            foreach ($listeAR as $key => $Value) {
                $dataAR[$key] = $Value;
            }
        }
        $this->AR->dataSource = $dataAR;
        $this->AR->dataBind();
    }

    public function remplirListFormat($listeFormat, $withSelc = null)
    {
        $dataFormat = [];
        if ($withSelc) {
            $dataFormat[' '] = Prado::localize('TEXT_SELECTIONNER').'...';
        }
        if (is_array($listeFormat)) {
            foreach ($listeFormat as $key => $Value) {
                $dataFormat[$key] = $Value;
            }
        }
        $this->fileFormat->dataSource = $dataFormat;
        $this->fileFormat->dataBind();
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Message::DeletePj($idpj, Atexo_CurrentUser::getCurrentOrganism());
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMsg'), Atexo_CurrentUser::getCurrentOrganism());
        if ($objetMessage) {
            $this->remplirTableauPJ($objetMessage->getCommonEchangePieceJointes(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'))));
        }
        $this->PanelPJ->render($param->NewWriter);
    }

    public function onAjoutClick($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($this->ajoutFichier->HasFile) {
            //Début scan antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $pjs = $this->getViewState('Pjs');
                $this->remplirTableauPJ($pjs);
                $this->afficherErreur($msg.' "'.$this->ajoutFichier->FileName.'"');

                return;
            }
            //Fin scan Antivirus

            $infilepj = Atexo_Config::getParameter('COMMON_TMP').'pj'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infilepj)) {
                $atexoBlob = new Atexo_Blob();
                $pjIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infilepj, Atexo_CurrentUser::getCurrentOrganism());

                //ajout piece jointe
                $c = new Criteria();
                $PJs = [];
                if ($this->getViewState('idMsg')) {
                    $ObjetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMsg'), Atexo_CurrentUser::getCurrentOrganism());
                    if ($ObjetMessage) {
                        $PJs = $ObjetMessage->getCommonEchangePieceJointes($c, $connexionCom);
                    }
                } else {
                    $ObjetMessage = new CommonEchange();
                    $ObjetMessage->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    if (isset($_GET['id'])) {
                        $ObjetMessage->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $ObjetMessage->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE'));
                    $ObjetMessage->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
                }

                $Pj = new CommonEchangePieceJointe();
                $Pj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $Pj->setNomFichier($this->ajoutFichier->FileName);
                $Pj->setPiece($pjIdBlob);
                $Pj->setTaille($atexoBlob->getTailFile($pjIdBlob, Atexo_CurrentUser::getCurrentOrganism()));
                $ObjetMessage->addCommonEchangePieceJointe($Pj);
                if ($ObjetMessage->save($connexionCom)) {
                    $PJs[] = $Pj;
                    $this->setViewState('idMsg', $ObjetMessage->getId());
                }
                $this->setViewState('Pjs', $PJs);
                $this->remplirTableauPJ($PJs);
            }
        } else {
            $pjs = $this->getViewState('Pjs');
            $this->remplirTableauPJ($pjs);
        }
    }

    public function getObjetEchange($echange)
    {
        if ($echange) {
            $this->commentaires->Text = $echange->getCorps();
            $this->expediteur->Text = $echange->getExpediteur();
            $this->objet->Text = $echange->getObjet();
            $this->dateEchange->Text = Atexo_Util::iso2frnDateTime($echange->getDateMessage());
            $this->fileFormat->SelectedValue = $echange->getFormat();
        }
    }

    public function getObjetEchangeDestinataire($echangedestinataire)
    {
        if ($echangedestinataire) {
            $this->destinataire->Text = $echangedestinataire->getMailDestinataire();
            $this->AR->SelectedValue = $echangedestinataire->getTypeAr();
            $this->dateAR->Text = Atexo_Util::iso2frnDateTime($echangedestinataire->getDateAr());
        }
    }

    public function setObjetEchange($idServiceConnected, $org, $echange = null)
    {
        if (!$echange) {
            $echange = new CommonEchange();
            $echange->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            if (isset($_GET['id'])) {
                $echange->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
            $echange->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE'));
            $echange->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        }

        $echange->setCorps($this->commentaires->Text);
        $echange->setExpediteur($this->expediteur->Text);
        $echange->setObjet($this->objet->Text);
        if ($this->dateEchange->Text) {
            $echange->setDateMessage(Atexo_Util::frnDateTime2iso($this->dateEchange->Text));
        }
        $echange->setFormat($this->fileFormat->SelectedValue);

        return $echange;
    }

    public function setObjetEchangeDestinataire($echangedestinataire = null)
    {
        if (!$echangedestinataire) {
            $echangedestinataire = new CommonEchangeDestinataire();
            $echangedestinataire->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $echangedestinataire->setAr(1);
        }

        $echangedestinataire->setMailDestinataire(trim($this->destinataire->Text));
        $echangedestinataire->setTypeAr($this->AR->SelectedValue);
        if ($this->dateAR->Text) {
            $echangedestinataire->setDateAr(Atexo_Util::frnDateTime2iso($this->dateAR->Text));
        }

        return $echangedestinataire;
    }

    public function AddEchange()
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idServiceConnected = Atexo_CurrentUser::getIdServiceAgentConnected();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::onAjoutClick(null, null);
        if ($this->getViewState('idEchangeDestinataire') && $this->getViewState('idMsg')) {
            $echangeFromDB = Atexo_Message::retrieveMessageById($this->getViewState('idMsg'), $org);
            $echange = $this->setObjetEchange($idServiceConnected, $org, $echangeFromDB);
            $echangedestinataireFromDB = Atexo_Message::getEchangeDestinataireByid($this->getViewState('idEchangeDestinataire'), $org);
            $echangedestinataire = $this->setObjetEchangeDestinataire($echangedestinataireFromDB);
            $echange->save($connexionCom);
            $echangedestinataire->save($connexionCom);
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateEchange').click();window.close();</script>";
        } elseif (!$this->getViewState('idEchangeDestinataire') && $this->getViewState('idMsg')) {
            $echangeFromDB = Atexo_Message::retrieveMessageById($this->getViewState('idMsg'), $org);
            $echange = $this->setObjetEchange($idServiceConnected, $org, $echangeFromDB);
            $echangedestinataire = $this->setObjetEchangeDestinataire();
            $echange->addCommonEchangeDestinataire($echangedestinataire);
            $echange->save($connexionCom);
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateEchange').click();window.close();</script>";
        } else {
            $echange = $this->setObjetEchange($idServiceConnected, $org);
            $echangedestinataire = $this->setObjetEchangeDestinataire();
            $echange->addCommonEchangeDestinataire($echangedestinataire);
            $echange->save($connexionCom);
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateEchange').click();window.close();</script>";
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
