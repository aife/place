<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonDecisionLotPeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_RegistreVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\InvitePermanentContrat;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_ImpressionODT;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use AtexoPdf\RtfGeneratorClient;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DecisionAttribution extends MpeTPage
{
    public $_lotOrConsultation;
    public $_consultation;
    public $_categories;
    public $_trancheBudgetaire;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul')) {
            Atexo_CurrentUser::deleteFromSession('idsOrg');
            $this->decisionEntitesEligibles->setVisible(false);
            $this->panelMessageErreur->setVisible(false);
            $messageErreur = '';
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                    || ((
                        $consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                            && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    )
                || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION'))
                ) {
                    $lotOrConsultation = (new Atexo_Consultation_Responses())->retrieveLotsForDecision(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
                    if (is_array($lotOrConsultation) && 1 == count($lotOrConsultation)) {
                        $lotOrConsultation = array_shift($lotOrConsultation);
                        if ($lotOrConsultation->getDecisionLot() instanceof CommonDecisionLot) {
                            $this->setViewState('typeDecision', $lotOrConsultation->getDecisionLot()->getIdTypeDecision());
                            $this->consultationSummary->setConsultation($consultation);
                            $this->_lotOrConsultation = $lotOrConsultation;
                            $this->_consultation = $consultation;
                            if (!$this->IsPostBack) {
                                $this->fillEntrepriseAttributaire();
                            }

                            if (Atexo_Module::isEnabled('AcSadTransversaux') && $this->_consultation->getConsultationTransverse() == Atexo_Config::getParameter('CONSULTATION_TRANSVERSE')) {
                                $this->decisionEntitesEligibles->setVisible(true);
                                $this->decisionEntitesEligibles->setIdRef(Atexo_Util::atexoHtmlEntities($_GET['id']));
                                $this->decisionEntitesEligibles->setNumLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));
                                $this->decisionEntitesEligibles->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                                $this->decisionEntitesEligibles->setPostBack($this->IsPostBack);
                            }
                        } else {
                            $messageErreur = Prado::localize('DECISION_IMPOSSIBLE');
                        }
                    } else {
                        $messageErreur = Prado::localize('DECISION_IMPOSSIBLE_AUCUN_LOT');
                    }
                } else {
                    $messageErreur = Prado::localize('DECISION_IMPOSSIBLE');
                }
            } else {
                $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
            }
        } else {
            $messageErreur = Prado::localize('ERREUR_HABILITATION_DECISION');
        }

        if ($messageErreur) {
            $this->panelMessageErreur->setVisible(true);
            $this->consultationSummary->setWithException(false);
            $this->panelMessageErreur->setMessage($messageErreur);
        }
    }

    public function fillEntrepriseAttributaire()
    {
        if ($this->_lotOrConsultation->getDecisionLot()->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX') ||
        $this->_lotOrConsultation->getDecisionLot()->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE') ||
        $this->_lotOrConsultation->getDecisionLot()->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_AUTRE')) {
            $reponses = [];
        } else {
            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($this->_consultation->getEnvCandidature(), $this->_consultation->getEnvOffre(), $this->_consultation->getEnvAnonymat(), $this->_consultation->getEnvOffreTechnique());
            if ($typeEnveloppe && $typeEnveloppe != Atexo_Config::getParameter('CANDIDATURE_SEUL')) {
                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $criteriaResponse->setSousPli(Atexo_Util::atexoHtmlEntities($_GET['lot']));
                $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteriaResponse->setReturnReponses(false);
                $criteriaResponse->setNomEntrepriseVisible(true);
                $criteriaResponse->getEnveloppeStatutOuverte();
                $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(Atexo_Util::atexoHtmlEntities($_GET['id']), $typeEnveloppe, Atexo_Util::atexoHtmlEntities($_GET['lot']), '', '', Atexo_CurrentUser::getCurrentOrganism(), false, false, true, true);
                $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
            } else {
                $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveReponseAdmissible(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
                $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveReponsePapierAdmissible(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
                $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
            }

            if (is_array($reponses) && count($reponses)) {
                try {
                    (new Atexo_Consultation_Responses())->addResultatAnalyse($reponses);

                    $sortedBy = 'NumPli';
                    $nestedObjects = null;
                    if (Atexo_Module::isEnabled('DecisionClassement')) {
                        $sortedBy = 'Classement';
                        $nestedObjects = 'ResultatAnalyse';
                    }
                    $reponsesSorted = Atexo_Util::sortArrayOfObjects($reponses, $sortedBy, 'ASC', $nestedObjects);
                    $reponses = $reponsesSorted;
                } catch (Exception $e) {
                    Prado::log('Erreur DecisionAttribution.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'DecisionAttribution.php');
                }
            }
        }
        $this->entreprisesAttributaire->DataSource = $reponses;
        $this->entreprisesAttributaire->DataBind();

        $pmiPmeReferentiel = $this->getPmePmi();

        $categorie = $this->getCategories();
        if (!$this->isAccordCadre()) {
            foreach ($this->entreprisesAttributaire->getItems() as $item) {
                $this->getTrancheBudgetaire($item, true);
                $item->pmiPme->DataSource = $pmiPmeReferentiel;
                $item->pmiPme->DataBind();
                $item->naturePrestations->DataSource = $categorie;
                $item->naturePrestations->DataBind();
            }
        }
    }

    public function getCategories()
    {
        return Atexo_Consultation_Category::retrieveCategories(false, false, Atexo_CurrentUser::readFromSession('lang'));
    }

    public function getTrancheBudgetaire($sender, $param)
    {
        $annee = $sender->dateNotification->Text;
        if (!$annee) {
            $annee = $sender->dateNotificationReelle->Text;
        }
        if ('encod' == (string) $param) {
            $idTranche = $sender->trancheBudgetaire->SelectedValue;
        } else {
        	$idTranche = $sender->Data->getCommonDecisionEnveloppe()->getTrancheBudgetaire();
        }
        $tranchesBudg = $this->fillListeTrancheBudegaire($annee, $sender->trancheBudgetaire);
        if ($idTranche) {
            foreach ($tranchesBudg as $KeyTranche => $value) {
                if ($KeyTranche == $idTranche) {
                    $sender->trancheBudgetaire->SelectedValue = $idTranche;
                    $arrayBorns = $this->setBorne($idTranche);
                    $sender->bornSuperieur->Value = $arrayBorns[0];
                    $sender->bornInferieur->Value = $arrayBorns[1];
                }
            }
        }
    }

    public function fillListeTrancheBudegaire($annee, $ObjetListe)
    {
        $tranchesBudg = [];
        if ($annee) {
            $annee = Atexo_Util::getAnneeFromDate2($annee);
        }
        $tranchesBudg = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireByOrgAnne($annee, Atexo_CurrentUser::getCurrentOrganism(), Prado::localize('MESSAGE_VALEUR_TOUTE_TRANCHES'));
        $ObjetListe->setDataSource($tranchesBudg);
        $ObjetListe->dataBind();

        return $tranchesBudg;
    }

    public function changeTrancheBudgetaire($sender, $param)
    {
        $annee = $sender->Parent->Parent->Parent->dateNotification->Text;
        if (!$annee) {
            $annee = $sender->Parent->Parent->Parent->dateNotificationReelle->Text;
        }
        $this->fillListeTrancheBudegaire($annee, $sender->Parent->Parent->Parent->trancheBudgetaire);
    }

    public function getPmePmi()
    {
        return (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_PME_PMI'), Atexo_CurrentUser::getCurrentOrganism());
    }

    public function isAccordCadre()
    {
        if ($this->_lotOrConsultation->getDecisionLot()->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_ACCORD_CADRE') ||
        $this->_lotOrConsultation->getDecisionLot()->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
            return true;
        } else {
            return false;
        }
    }

    public function saveDecision($sender, $param)
    {
        if ($this->isValid) {
            $connexionCOm = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexionCOm->beginTransaction();
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            try {
                $decisionLot = CommonDecisionLotPeer::retrieveByPK($this->_lotOrConsultation->getDecisionLot()->getIdDecisionLot(), $organisme, $connexionCOm);
                $decisionLot->setDateDecision(Atexo_Util::frnDate2iso($this->dateDecision->Text));
                $decisionLot->setCommentaire($this->commentaire->Text);
                $decisionLot->save($connexionCOm);
                $atexoBlob = new Atexo_Blob();
                $index = 0;
                foreach ($this->entreprisesAttributaire->getItems() as $item) {
                    if ($item->attributaire->Checked) {
                        if ($item->idDecision->Value) {
                            $decisionEnveloppe = CommonDecisionEnveloppePeer::retrieveByPK($item->idDecision->Value, $organisme, $connexionCOm);
                            if (!($decisionEnveloppe instanceof CommonDecisionEnveloppe)) {
                                throw new Exception('Error');
                            }
                        } else {
                            $decisionEnveloppe = new CommonDecisionEnveloppe();
                        }
                        $decisionEnveloppe->setOrganisme($organisme);
                        $decisionEnveloppe->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
                        $decisionEnveloppe->setIdOffre($item->idOffre->Value);
                        $decisionEnveloppe->setLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));
                        $decisionEnveloppe->setDecision('1');
                        $decisionEnveloppe->setTypeEnveloppe($item->typeEnveloppe->Value);

                        if ($this->isAccordCadre()) {
                            $decisionEnveloppe->setDateNotification(Atexo_Util::frnDate2iso($item->dateNotif->Text));
                            $decisionEnveloppe->setCommentaire($item->commentaire->Text);
                            $infile = Atexo_Config::getParameter('COMMON_TMP').'accord_cadre'.session_id().time().$index;
                            if (move_uploaded_file($item->ajoutFichier->LocalName, $infile)) {
                                $idBlob = $atexoBlob->insert_blob($item->ajoutFichier->FileName, $infile, $organisme);
                                $decisionEnveloppe->setFichierJoint($idBlob);
                                $decisionEnveloppe->setNomFichierJoint($item->ajoutFichier->FileName);
                            }
                        } else {
                            $decisionEnveloppe->setDateNotification(Atexo_Util::frnDate2iso($item->dateNotification->Text));
                            $decisionEnveloppe->setDateFinMarchePrevisionnel(Atexo_Util::frnDate2iso($item->datePrevisionnelleFinMarche->Text));
                            $decisionEnveloppe->setDateNotificationReelle(Atexo_Util::frnDate2iso($item->dateNotificationReelle->Text));
                            $decisionEnveloppe->setDateFinMarcheReelle(Atexo_Util::frnDate2iso($item->dateFinMarcheReelle->Text));

                            //si jamais le module Interface_CHORUS_PMI n'est pas activé alors on ré-enregistre l'information en provenance de la page
                            //sinon on ne fait rien, car le numéro de marché est mis à jour et enregistré via les échanges ave Chorus
                            if (!(new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
                                if (Atexo_Module::isEnabled('IdentificationContrat', Atexo_CurrentUser::getCurrentOrganism())) {
                                    $idContrat = Atexo_Util::atexoHtmlEntities($item->idContratAnnee->Text)
                                           .Atexo_Util::atexoHtmlEntities($item->idContratNumeroInterne->Text)
                                           .Atexo_Util::atexoHtmlEntities($item->idContratAvenant->Text)
                                           .Atexo_Util::atexoHtmlEntities($item->idContratAS->Text);
                                    $decisionEnveloppe->setNumeroMarche($idContrat);
                                } else {
                                    $decisionEnveloppe->setNumeroMarche($item->numMarche->Text);
                                }
                            }
                            if (!$item->pmiPme->getSelectedValue()) {
                                $decisionEnveloppe->setPmepmi(Atexo_Config::getParameter('PME_PMI_A_RENSEIGNER'));
                            } else {
                                $decisionEnveloppe->setPmepmi($item->pmiPme->getSelectedValue());
                            }
                            $decisionEnveloppe->setTrancheBudgetaire($item->trancheBudgetaire->getSelectedValue());
                            $decisionEnveloppe->setMontantMarche(str_replace('.', ',', $item->montant->Text));
                            $decisionEnveloppe->setCategorie($item->naturePrestations->getSelectedValue());
                            $decisionEnveloppe->setObjetMarche($item->objetMarche->Text);
                            $decisionEnveloppe->setNote(str_replace(',', '.', $item->note->Text));
                            $decisionEnveloppe->setClassement($item->classement->Text);
                            if ($item->cp->Text) {
                                $decisionEnveloppe->setCodePostal($item->cp->Text);
                            }
                            if ($item->ville->Text) {
                                $decisionEnveloppe->setVille($item->ville->Text);
                            }
                            //Infos entreprise
                            $item->blocEntreprise->Enregister($decisionEnveloppe);
                        }
                        $decisionEnveloppe->save($connexionCOm);
                    } else {
                        if ($item->idDecision->Value) {
                            $decisionEnveloppe = CommonDecisionEnveloppePeer::retrieveByPK($item->idDecision->Value, $organisme, $connexionCOm);
                            if (!($decisionEnveloppe instanceof CommonDecisionEnveloppe)) {
                                throw new Exception('Error 3');
                            }
                            $decisionEnveloppe->delete($connexionCOm);
                        }
                    }
                }

                $connexionCOm->commit();
                if ($this->_consultation->getConsultationTransverse() == Atexo_Config::getParameter('CONSULTATION_TRANSVERSE')) {
                    $ref = $this->_consultation->getId();
                    $dateDecision = Atexo_Util::frnDate2iso($this->dateDecision->Text);
                    $lot = Atexo_Util::atexoHtmlEntities($_GET['lot']);
                    if (('' != $ref) && ('' != $lot)) {
                        $org = Atexo_CurrentUser::getCurrentOrganism();
                        (new Atexo_Consultation_InvitationConsTransverse())->deleteInvitationConsultationTransverse($ref, $lot, $org);
                        if ('1' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                            $this->decisionEntitesEligibles->addInvitationConsultationTransverse($ref, $dateDecision, $lot);
                        }

                        /**
                         * Suppresion des données invité permanent sur le contrat !
                         */
                        $criteres = [
                        'idConsultation' => $ref,
                        'organisme' => $org,
                        'lot' => $lot,
                        ];
                        (new InvitePermanentContrat())->deleteForCriteres($criteres);
                        if ('2' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                            $this->decisionEntitesEligibles->addInvitePermanentContrat($ref, $dateDecision, $lot);
                        }
                    }
                }
                if ('linkFicheRecensement' != $sender->getId()) {
                    $this->response->redirect('index.php?page=Agent.ouvertureEtAnalyse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&decision');
                }
            } catch (Exception $e) {
                $connexionCOm->rollBack();
                throw $e;
            }
        }
    }

    public function getTextCommentaire($texte)
    {
        if ($texte) {
            return $texte;
        } else {
            return $this->_consultation->getObjetTraduit().' - '.$this->_lotOrConsultation->getDescriptionTraduite();
        }
    }

    public function getCategorieToSelect($categorieDecisionEnveloppe)
    {
        if ($categorieDecisionEnveloppe) {
            return $categorieDecisionEnveloppe;
        } else {
            return $this->_consultation->getCategorie();
        }
    }

    public function getDecisionEnveloppeExist($idDecision)
    {
        if ($idDecision) {
            return true;
        } else {
            return false;
        }
    }

    public function getDivNonAccordCadreStyle($idDecision)
    {
        if (!$this->isAccordCadre() && Atexo_Module::isEnabled('DecisionAfficherDetailCandidatParDefaut')) {
            return '';
        }

        if ($idDecision && !$this->isAccordCadre()) {
            return '';
        }

        if (!$idDecision) {
            return 'display:none';
        }

        if ($this->isAccordCadre()) {
            return 'display:none';
        }
    }

    public function getIsNotAccordCadre()
    {
        if ($this->isAccordCadre()) {
            return 'false';
        } else {
            return 'true';
        }
    }

    public function getDivAccordCadreStyle($idDecision)
    {
        if ($this->isAccordCadre() && Atexo_Module::isEnabled('DecisionAfficherDetailCandidatParDefaut')) {
            return '';
        }

        if ($idDecision && $this->isAccordCadre()) {
            return '';
        }

        if (!$idDecision) {
            return 'display:none';
        }

        if (!$this->isAccordCadre()) {
            return 'display:none';
        }
    }

    public function onCallBackRefresh($sender, $param)
    {
        $this->fillEntrepriseAttributaire();
        $this->panelEntrepriseAttributaire->render($param->NewWriter);
    }

    public function deletePj($sender, $param)
    {
        $idDecision = $param->commandParameter;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $decisionEnveloppe = CommonDecisionEnveloppePeer::retrieveByPK($idDecision, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        if ($decisionEnveloppe instanceof CommonDecisionEnveloppe) {
            $decisionEnveloppe->setFichierJoint(null);
            $decisionEnveloppe->setNomFichierJoint(null);
            $decisionEnveloppe->save($connexion);
        }
    }

    /**
     * Géneration de fichier odt de la fiche de recensement.
     */
    public function generateFicheRessencementOdt($idContrat = false)
    {
        $siren = [];
        $siret = [];
        $dateNotification = [];
        $dateAchevement = null;
        $montant = null;
        $contrat = null;
        $sirenAp = [];
        $complementAp = [];
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];

        $consultation = $this->_consultation;
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $lot = Atexo_Util::atexoHtmlEntities($_GET['lot']);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($idContrat) {
            $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);
            if ($contrat instanceof CommonTContratTitulaire) {
                $dateNotification = $contrat->getDateNotification('%Y-%m-%d');
                $montant = str_replace(' ', '', str_replace(',', '.', $contrat->getMontantContrat()));
                //A verifier avec KBE : getDatePrevueFinContrat au lieu de getDateFinMarchePrevisionnel
                $dateAchevement = $contrat->getDatePrevueFinContrat();
                $idOffre = $contrat->getIdOffre();
                if ($contrat->getTitulaireContrat()) {
                    $siren = $contrat->getTitulaireContrat()->getSiren();
                    $siret = $contrat->getTitulaireContrat()->getNicsiege();
                } elseif ($contrat->getIdTypeContrat() == Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE')) {
                    $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, Atexo_CurrentUser::getCurrentOrganism());
                    if ($offre instanceof CommonOffres) {
                        $entreprise = (new Atexo_Entreprise())->getEntrepriseByIdOffre($idOffre, $consultationId);
                        if ($entreprise) {
                            $siren = $entreprise->getSiren();
                            $siret = $entreprise->getNicSiege();
                        }
                    }
                } else {
                    $registreVO = (new Atexo_Consultation_Register())->retrieveOffrePapier($idOffre, [], Atexo_CurrentUser::getCurrentOrganism(), false);
                    $registreVO = array_shift($registreVO);
                    if ($registreVO instanceof Atexo_Consultation_RegistreVo) {
                        $siren = $registreVO->getSirenEntreprise();
                        $siret = $registreVO->getNicEntreprise();
                    }
                }
            }
        }

        //Siren titulaire/Mandataire
        $tableKeys['SiretCase1'] = $siren[0];
        $tableKeys['SiretCase2'] = $siren[1];
        $tableKeys['SiretCase3'] = $siren[2];
        $tableKeys['SiretCase4'] = $siren[3];
        $tableKeys['SiretCase5'] = $siren[4];
        $tableKeys['SiretCase6'] = $siren[5];
        $tableKeys['SiretCase7'] = $siren[6];
        $tableKeys['SiretCase8'] = $siren[7];
        $tableKeys['SiretCase9'] = $siren[8];
        //Nic titulaire/Mandataire
        $tableKeys['NicCase1'] = $siret[0];
        $tableKeys['NicCase2'] = $siret[1];
        $tableKeys['NicCase3'] = $siret[2];
        $tableKeys['NicCase4'] = $siret[3];
        $tableKeys['NicCase5'] = $siret[4];

        $dureeContrat = str_pad(ceil(Atexo_Util::diffMois($dateNotification, $dateAchevement)), 3, '0', STR_PAD_LEFT);
        $tableKeys['Duree1'] = $dureeContrat[0];
        $tableKeys['Duree2'] = $dureeContrat[1];
        $tableKeys['Duree3'] = $dureeContrat[2];

        $tableKeys['MontantCase1'] = ($montant / 10_000_000_000) % 10;
        $tableKeys['MontantCase2'] = ($montant / 1_000_000_000) % 10;
        $tableKeys['MontantCase3'] = ($montant / 100_000_000) % 10;
        $tableKeys['MontantCase4'] = ($montant / 10_000_000) % 10;
        $tableKeys['MontantCase5'] = ($montant / 1_000_000) % 10;
        $tableKeys['MontantCase6'] = ($montant / 100000) % 10;
        $tableKeys['MontantCase7'] = ($montant / 10000) % 10;
        $tableKeys['MontantCase8'] = ($montant / 1000) % 10;
        $tableKeys['MontantCase9'] = ($montant / 100) % 10;
        $tableKeys['MontantC1'] = ($montant / 10) % 10;
        $tableKeys['MontantC2'] = $montant % 10;

        $cpv1 = $consultation->getCodeCpv1();
        $charControleCpv = Atexo_Util::returnValueFromCsv(Atexo_Config::getParameter('PATH_FILE_CPV_CONTROLE'), $cpv1);
        //<!-- Module referentiel CPV
        if ($cpv1) {
            $tableKeys['CpvCase1'] = $cpv1[0];
            $tableKeys['CpvCase2'] = $cpv1[1];
            $tableKeys['CpvCase3'] = $cpv1[2];
            $tableKeys['CpvCase4'] = $cpv1[3];
            $tableKeys['CpvCase5'] = $cpv1[4];
            $tableKeys['CpvCase6'] = $cpv1[5];
            $tableKeys['CpvCase7'] = $cpv1[6];
            $tableKeys['CpvCase8'] = $cpv1[7];
            $tableKeys['CpvCase0'] = $charControleCpv;
        } else {
            $tableKeys['CpvCase1'] = '';
            $tableKeys['CpvCase2'] = '';
            $tableKeys['CpvCase3'] = '';
            $tableKeys['CpvCase4'] = '';
            $tableKeys['CpvCase5'] = '';
            $tableKeys['CpvCase6'] = '';
            $tableKeys['CpvCase7'] = '';
            $tableKeys['CpvCase8'] = '';
            $tableKeys['CpvCase0'] = '';
        }

        //traitement des CPVs secondaires
        $cpv2 = $consultation->getCodeCpv2();
        if ($cpv2) {
            $codesCpvSec = explode('#', $cpv2);
            if ($codesCpvSec && is_array($codesCpvSec)) {
                //on est dans cette partie si au moins un CPV secondaire
                $i = 0;
                foreach ($codesCpvSec as $codeCpv) {
                    if ($codeCpv) {
                        $charControleCpv2 = Atexo_Util::returnValueFromCsv(Atexo_Config::getParameter('PATH_FILE_CPV_CONTROLE'), $codeCpv);
                        $tableKeys['Cpv'.$i.'Case1'] = $codeCpv[0];
                        $tableKeys['Cpv'.$i.'Case2'] = $codeCpv[1];
                        $tableKeys['Cpv'.$i.'Case3'] = $codeCpv[2];
                        $tableKeys['Cpv'.$i.'Case4'] = $codeCpv[3];
                        $tableKeys['Cpv'.$i.'Case5'] = $codeCpv[4];
                        $tableKeys['Cpv'.$i.'Case6'] = $codeCpv[5];
                        $tableKeys['Cpv'.$i.'Case7'] = $codeCpv[6];
                        $tableKeys['Cpv'.$i.'Case8'] = $codeCpv[7];
                        $tableKeys['Cpv'.$i.'Case0'] = $charControleCpv2;
                    }
                    ++$i;
                }
                //à la fin il faut mettre à vide ceux qui n'ont pas été remplis
                for ($j = 3; $i <= $j; --$j) {
                    $tableKeys['Cpv'.$j.'Case1'] = '';
                    $tableKeys['Cpv'.$j.'Case2'] = '';
                    $tableKeys['Cpv'.$j.'Case3'] = '';
                    $tableKeys['Cpv'.$j.'Case4'] = '';
                    $tableKeys['Cpv'.$j.'Case5'] = '';
                    $tableKeys['Cpv'.$j.'Case6'] = '';
                    $tableKeys['Cpv'.$j.'Case7'] = '';
                    $tableKeys['Cpv'.$j.'Case8'] = '';
                    $tableKeys['Cpv'.$j.'Case0'] = '';
                }
            }
        }
        //si aucun CPV secondaire alors tout est à vide
        else {
            for ($i = 1; $i < 4; ++$i) {
                $tableKeys['Cpv'.$i.'Case1'] = '';
                $tableKeys['Cpv'.$i.'Case2'] = '';
                $tableKeys['Cpv'.$i.'Case3'] = '';
                $tableKeys['Cpv'.$i.'Case4'] = '';
                $tableKeys['Cpv'.$i.'Case5'] = '';
                $tableKeys['Cpv'.$i.'Case6'] = '';
                $tableKeys['Cpv'.$i.'Case7'] = '';
                $tableKeys['Cpv'.$i.'Case8'] = '';
                $tableKeys['Cpv'.$i.'Case0'] = '';
            }
        }

        $codeRecensementpeProc = (new Atexo_Consultation_ProcedureType())->retrieveCodeRecensementProcedureById($consultation->getIdTypeProcedureOrg(), Atexo_CurrentUser::getOrganismAcronym());

        $nbrOffresElec = (string) $consultation->getNombreOffreElectronique();
        $nbrOffresPapier = (string) $consultation->getNombreOffrePapier();
        $nbrOffres = (string) ($nbrOffresElec + $nbrOffresPapier);
        $tableKeys['Mois_Case_1'] = $dateNotification[5];
        $tableKeys['Mois_Case_2'] = $dateNotification[6];

        $tableKeys['AnneeCase1'] = $dateNotification[0];
        $tableKeys['AnneeCase2'] = $dateNotification[1];
        $tableKeys['AnneeCase3'] = $dateNotification[2];
        $tableKeys['AnneeCase4'] = $dateNotification[3];

        $tableKeys['ProcedureCase1'] = $codeRecensementpeProc[0];
        $tableKeys['ProcedureCase2'] = $codeRecensementpeProc[1];

        //Total des offres reçues
        if ($nbrOffres < 10) {
            $tableKeys['NbrTRCase1'] = '0';
            $tableKeys['NbrTRCase2'] = '0';
            $tableKeys['NbrTRCase3'] = $nbrOffres;
        } elseif ($nbrOffres < 100) {
            $tableKeys['NbrTRCase1'] = '0';
            $tableKeys['NbrTRCase2'] = $nbrOffres[0];
            $tableKeys['NbrTRCase3'] = $nbrOffres[1];
        } elseif ($nbrOffres > 100) {
            $tableKeys['NbrTRCase1'] = $nbrOffres[0];
            $tableKeys['NbrTRCase2'] = $nbrOffres[1];
            $tableKeys['NbrTRCase3'] = $nbrOffres[2];
        }

        //Total des offres reçues dématérialisées (éléctronique)
        if ($nbrOffresElec < 10) {
            $tableKeys['NbrTDCase1'] = '0';
            $tableKeys['NbrTDCase2'] = '0';
            $tableKeys['NbrTDCase3'] = $nbrOffresElec;
        } elseif ($nbrOffresElec < 100) {
            $tableKeys['NbrTDCase1'] = '0';
            $tableKeys['NbrTDCase2'] = $nbrOffresElec[0];
            $tableKeys['NbrTDCase3'] = $nbrOffresElec[1];
        } elseif ($nbrOffresElec > 100) {
            $tableKeys['NbrTDCase1'] = $nbrOffresElec[0];
            $tableKeys['NbrTDCase2'] = $nbrOffresElec[1];
            $tableKeys['NbrTDCase3'] = $nbrOffresElec[2];
        }

        if (Atexo_Module::isEnabled('ConsultationClause')) {
            if ($lot > 0) {
                $lotO = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultation->getId(), $lot, Atexo_CurrentUser::getCurrentOrganism());
                if ($lotO->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER')) {
                    $tableKeys['ClauseSociale'] = 'X';
                } else {
                    $tableKeys['ClauseSociale'] = '';
                }
                if ($lotO->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER')
                && ($lotO->getClauseEnvSpecsTechniques() || $lotO->getClauseEnvCondExecution())) {
                    $tableKeys['ClauseEnv'] = 'X';
                } else {
                    $tableKeys['ClauseEnv'] = '';
                }
            } else {
                if ($consultation->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER')) {
                    $tableKeys['ClauseSociale'] = 'X';
                } else {
                    $tableKeys['ClauseSociale'] = '';
                }
                if ($consultation->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER')
                && ($consultation->getClauseEnvSpecsTechniques() || $consultation->getClauseEnvCondExecution())) {
                    $tableKeys['ClauseEnv'] = 'X';
                } else {
                    $tableKeys['ClauseEnv'] = '';
                }
            }
        } else {
            $tableKeys['ClauseSociale'] = '';
            $tableKeys['ClauseEnv'] = '';
        }

        if (!(new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            if (Atexo_Module::isEnabled('IdentificationContrat', Atexo_CurrentUser::getCurrentOrganism())) {
                if ($contrat instanceof CommonTContratTitulaire) {
                    $numMarche = $contrat->getNumeroContrat();
                    $tableKeys['Annee1'] = $numMarche[0];
                    $tableKeys['Annee2'] = $numMarche[1];
                    $tableKeys['Annee3'] = $numMarche[2];
                    $tableKeys['Annee4'] = $numMarche[3];
                    $tableKeys['Interne1'] = $numMarche[4];
                    $tableKeys['Interne2'] = $numMarche[5];
                    $tableKeys['Interne3'] = $numMarche[6];
                    $tableKeys['Interne4'] = $numMarche[7];
                    $tableKeys['Interne5'] = $numMarche[8];
                    $tableKeys['Interne6'] = $numMarche[9];
                    $tableKeys['Interne7'] = $numMarche[10];
                    $tableKeys['Interne8'] = $numMarche[11];
                    $tableKeys['Interne9'] = $numMarche[12];
                    $tableKeys['Interne0'] = $numMarche[13];
                    $tableKeys['Avenant1'] = $numMarche[14];
                    $tableKeys['Avenant2'] = $numMarche[15];
                    $tableKeys['AS1'] = $numMarche[16];
                    $tableKeys['AS2'] = $numMarche[17];
                }
            } else {
                //ancien affichage (!identification_contrat)
                $tableKeys['Annee1'] = '2';
                $tableKeys['Annee2'] = '0';
                $tableKeys['Annee3'] = '';
                $tableKeys['Annee4'] = '';
                $tableKeys['Interne1'] = '';
                $tableKeys['Interne2'] = '';
                $tableKeys['Interne3'] = '';
                $tableKeys['Interne4'] = '';
                $tableKeys['Interne5'] = '';
                $tableKeys['Interne6'] = '';
                $tableKeys['Interne7'] = '';
                $tableKeys['Interne8'] = '';
                $tableKeys['Interne9'] = '';
                $tableKeys['Interne0'] = '';
                $tableKeys['Avenant1'] = '';
                $tableKeys['Avenant2'] = '';
                $tableKeys['AS1'] = '';
                $tableKeys['AS2'] = '';
            }
        } else {
            //ancien affichage (!identification_contrat)
            $tableKeys['Annee1'] = '2';
            $tableKeys['Annee2'] = '0';
            $tableKeys['Annee3'] = '';
            $tableKeys['Annee4'] = '';
            $tableKeys['Interne1'] = '';
            $tableKeys['Interne2'] = '';
            $tableKeys['Interne3'] = '';
            $tableKeys['Interne4'] = '';
            $tableKeys['Interne5'] = '';
            $tableKeys['Interne6'] = '';
            $tableKeys['Interne7'] = '';
            $tableKeys['Interne8'] = '';
            $tableKeys['Interne9'] = '';
            $tableKeys['Interne0'] = '';
            $tableKeys['Avenant1'] = '';
            $tableKeys['Avenant2'] = '';
            $tableKeys['AS1'] = '';
            $tableKeys['AS2'] = '';
        }
        if (Atexo_Module::isEnabled('OrganisationCentralisee', Atexo_CurrentUser::getCurrentOrganism()) || '0' == $consultation->getServiceId()) {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            $sirenAp = $organismeO->getSiren();
            $complementAp = $organismeO->getComplement();
        } else {
            $serviceObj = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), Atexo_CurrentUser::getOrganismAcronym());
            if ($serviceObj instanceof CommonService) {
                $sirenAp = $serviceObj->getSiren();
                $complementAp = $serviceObj->getComplement();
            }
        }
        //Siren
        $tableKeys['sirenAPCase1'] = $sirenAp[0];
        $tableKeys['sirenAPCase2'] = $sirenAp[1];
        $tableKeys['sirenAPCase3'] = $sirenAp[2];
        $tableKeys['sirenAPCase4'] = $sirenAp[3];
        $tableKeys['sirenAPCase5'] = $sirenAp[4];
        $tableKeys['sirenAPCase6'] = $sirenAp[5];
        $tableKeys['sirenAPCase7'] = $sirenAp[6];
        $tableKeys['sirenAPCase8'] = $sirenAp[7];
        $tableKeys['sirenAPCase9'] = $sirenAp[8];
        //Nic
        $tableKeys['nicAPCase1'] = $complementAp[0];
        $tableKeys['nicAPCase2'] = $complementAp[1];
        $tableKeys['nicAPCase3'] = $complementAp[2];
        $tableKeys['nicAPCase4'] = $complementAp[3];
        $tableKeys['nicAPCase5'] = $complementAp[4];

        $modelePath = './ressources/consultation/Fiche_recensement_PMI.zip';
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }

    /**
     * récupère.
     */
    public function getDecision($lot, $consultationId, $idOffre)
    {
        $lot = Atexo_Util::atexoHtmlEntities($_GET['lot']);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonDecisionEnveloppePeer::LOT, $lot);
        $c->add(CommonDecisionEnveloppePeer::ID_OFFRE, $idOffre);
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $decisionEnveloppe = CommonDecisionEnveloppePeer::doSelectOne($c, $connexion);

        if ($decisionEnveloppe instanceof CommonDecisionEnveloppe) {
            return $decisionEnveloppe;
        } else {
            return false;
        }
    }

    /**
     * Télécharger la verstion rtf de détail de la fiche de recensement.
     */
    public function printFicheRessencementRtf($sender, $param)
    {
        $this->saveDecision($sender, $param);
        $params = explode('##', $sender->CommandParameter);
        $idContrat = $params[1];
        $odtFilePath = $this->generateFicheRessencementOdt($idContrat);
        if ($odtFilePath) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent('Fiche_recensement.rtf', $rtfContent);
        }
    }

    public function showFicheMarcheLink($decision)
    {
        return $decision->getIdDecisionEnveloppe() && $this->getViewState('typeDecision') == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE') && Atexo_Module::isEnabled('SuiviPassation', Atexo_CurrentUser::getCurrentOrganism());
    }

    /**
     * Afficher le lien vers Chorus si le module est activé et si la decision est enregistrée.
     */
    public function showInterfaceChorusPmi($decision)
    {
        return $decision->getIdDecisionEnveloppe() && $this->getViewState('typeDecision') == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE') && (new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    public function showEnvoiNotification($decision)
    {
        return $decision->getIdDecisionEnveloppe() && $this->getViewState('typeDecision') == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE');
    }

    public function getClassEtapeProcess()
    {
        $cssClass = 'etapes-process';
        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
            $cssClass = 'etapes-5-process';
        }

        return $cssClass;
    }

    public function getUrlEnvoiNotification($dataItem)
    {
        if ($dataItem->getTypeEnveloppe() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            $refCons = $dataItem->getCommonDecisionEnveloppe()->getConsultationId();
            $desision = $dataItem->getCommonDecisionEnveloppe()->getIdDecisionEnveloppe();
            $idOffre = $dataItem->getId();
            $url = 'index.php?page=Agent.EnvoiCourrierElectroniqueNotification&id='.$refCons.'&Decision='.$desision.'&lot='.Atexo_Util::atexoHtmlEntities($_GET['lot']).'&IdOffreEl='.$idOffre;
        } else {
            $refCons = $dataItem->getCommonDecisionEnveloppe()->getConsultationId();
            $desision = $dataItem->getCommonDecisionEnveloppe()->getIdDecisionEnveloppe();
            $idOffre = $dataItem->getId();
            $url = 'index.php?page=Agent.EnvoiCourrierElectroniqueNotification&id='.$refCons.'&Decision='.$desision.'&lot='.Atexo_Util::atexoHtmlEntities($_GET['lot']).'&IdOffrePa='.$idOffre;
        }

        return $url;
    }

    /***
     * retourne la ville selon le type d'enveloppe
     * si papier le champs est ville (offre_papier)
     * sinon ville_inscrit (offres)
     */
    public function getVille($objetDecision)
    {
        if ($objetDecision->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENVELOPPE_PAPIER')) {
            return $objetDecision->getVille();
        } else {
            return $objetDecision->getVilleInscrit();
        }
    }

    /**
     * permet de gérer le mode d'affichage selon l'habillitation.
     */
    public function getEnabledElement()
    {
        if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
            return true;
        }

        return false;
    }

    /**
     * permet de intialiser le style du champ cp.
     */
    public function getStyleChampCp($offre, $enabled = false)
    {
        $objetDecision = $offre->getCommonDecisionEnveloppe();
        $result = '';
        if ($offre instanceof CommonOffres) {
            $result = $this->getCssCpOffres($offre, $objetDecision);
        }
        if ($offre instanceof CommonOffrePapier) {
            $result = $this->getCssCpOffrepapier($offre, $objetDecision);
        }
        $habilitation = Atexo_CurrentUser::hasHabilitation('AttributionMarche');
        if ($habilitation && $enabled && ('cp-long' == $result)) {
            $result = true;
        } elseif ($enabled) {
            $result = false;
        }

        return $result;
    }

    /**
     * Permet de retourner la css du code postal.
     *
     * @param object $offre l'offre electroniue ou papier, Object CommonDecesionEnveloppe $objetDecision la decision
     *
     * @return string css code postal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCssCpOffrepapier($offre, $objetDecision)
    {
        if ($objetDecision->getSirenAttributaire()) {
            $result = 'cp-long';
        } elseif ($offre->getSiret() && !$objetDecision->getPaysAttributaire()) {
            $result = 'cp-long';
        } elseif ($objetDecision->getPaysAttributaire()) {
            $result = 'cp-long disabled';
        } elseif ($offre->getPays() && $offre->getNumPays() != Atexo_Config::getParameter('ID_GEON2_FRANCE')) {
            $result = 'cp-long disabled';
        } else {
            $result = 'cp-long';
        }

        return $result;
    }

    public function getCssCpOffres($offre, $objetDecision)
    {
        if ($objetDecision->getSirenAttributaire()) {
            $result = 'cp-long';
        } elseif ($offre->getSiretEntreprise() && !$objetDecision->getPaysAttributaire()) {
            $result = 'cp-long';
        } elseif ($objetDecision->getPaysAttributaire()) {
            $result = 'cp-long disabled';
        } elseif ($offre->getPaysInscrit()) {
            $result = 'cp-long disabled';
        } else {
            $result = 'cp-long';
        }

        return $result;
    }

    /**
     * permet de remplir les valeurs des bornes inferieur / superieur au changement de la tranche.
     */
    public function setBornsTrancheBudgetaire($sender, $param)
    {
        $idTranche = $sender->SelectedValue;
        $borne = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireById($idTranche, Atexo_CurrentUser::getCurrentOrganism());
        $borneSup = $borne['BorneSup'];
        if ($borneSup) {
            $borneSup = Atexo_Util::formatterMontant($borne['BorneSup'], 2, true);
        }
        $borneInf = Atexo_Util::formatterMontant($borne['BorneInf'], 2, true);
        $sender->Parent->Parent->Parent->bornSuperieur->Value = $borneSup;
        $sender->Parent->Parent->Parent->bornInferieur->Value = $borneInf;
    }

    /**
     * permet de remplir les valeurs des bornes inferieur / superieur au chargement de la poge.
     */
    public function setBorne($idTranche)
    {
        $arrayBorns = [];
        $borne = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireById($idTranche, Atexo_CurrentUser::getCurrentOrganism());
        $borneSup = $borne['BorneSup'];
        if ($borneSup) {
            $borneSup = Atexo_Util::formatterMontant($borne['BorneSup'], 2, true);
        }
        $borneInf = Atexo_Util::formatterMontant($borne['BorneInf'], 2, true);
        $arrayBorns[0] = $borneSup;
        $arrayBorns[1] = $borneInf;

        return $arrayBorns;
    }

    /**
     * Permet de valider la décision, en effectuant une validation coté serveur.
     *
     * @param $sender
     * @param $param
     */
    public function validerDonneesDecision($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $scriptJs = '';
        foreach ($this->entreprisesAttributaire->getItems() as $item) {
            if ($item->attributaire->checked) {
                $index = $item->ItemIndex + 1;
                if ('true' == $item->nonAccordCadreHidden->value) {
                    if (Atexo_Module::isEnabled('DecisionDateNotification')) {
                        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
                            $this->validerFormatDate(
                                Atexo_Util::iso2frnDate($item->dateNotification->Text),
                                $index,
                                $isValid,
                                $arrayMsgErreur,
                                'ImgError',
                                $scriptJs,
                                Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')
                            );
                        }
                        $this->validateDateNotificationFinMarche(
                            Atexo_Util::iso2frnDate($item->dateNotification->Text),
                            Atexo_Util::iso2frnDate($item->datePrevisionnelleFinMarche->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorDatePrevFMarcheNotif',
                            $scriptJs,
                            Prado::localize('ERREUR_DATE_FIN_MARCHE_NOTIFICATION_PREVISIONNELLE')
                        );
                        $this->validerFormatDate(
                            Atexo_Util::iso2frnDate($item->dateNotificationReelle->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorNotifReelle',
                            $scriptJs,
                            Prado::localize('DATE_REELLE_NOTIFICATION')
                        );
                        $this->validateDateNotificationFinMarche(
                            Atexo_Util::iso2frnDate($item->dateNotificationReelle->Text),
                            Atexo_Util::iso2frnDate($item->dateFinMarcheReelle->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorDateFinReelle',
                            $scriptJs,
                            Prado::localize('ERREUR_DATE_FIN_MARCHE_NOTIFICATION_REELLE')
                        );
                        $this->validerFormatDate(
                            Atexo_Util::iso2frnDate($item->dateFinMarcheReelle->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorDateFinReelle',
                            $scriptJs,
                            Prado::localize('DATE_REELLE_FIN_MARCHE')
                        );
                        $this->validateDateNotifFinMarcheReelle(
                            Atexo_Util::iso2frnDate($item->dateNotificationReelle->Text),
                            Atexo_Util::iso2frnDate($item->dateFinMarcheReelle->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorDateFinReelle2',
                            $scriptJs
                        );
                    }
                    if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
                        $this->validerDate(
                            Atexo_Util::iso2frnDate($item->datePrevisionnelleFinMarche->Text),
                            $index,
                            $isValid,
                            $arrayMsgErreur,
                            'ImgErrorDatePrevFMarche',
                            $scriptJs,
                            Prado::localize('DATE_PREVISIONNELLE_FIN_MARCHE')
                        );
                    }
                    $item->blocEntreprise->validateBlocIdentificationEntreprise($isValid, $arrayMsgErreur, $scriptJs);
                    if (Atexo_Module::isEnabled('DecisionCp')) {
                        if ($item->blocEntreprise->isPaysNationaleChecked()) {
                            $this->validerValeurNotNull($item->cp->Text, $index, $isValid, $arrayMsgErreur, 'ImgErrorCp', $scriptJs, Prado::localize('CODE_POSTAL'));
                        }
                        $this->validerValeurNotNull($item->ville->Text, $index, $isValid, $arrayMsgErreur, 'ImgErrorVille', $scriptJs, Prado::localize('TEXT_VILLE'));
                    }
                    if (Atexo_Module::isEnabled('DecisionTrancheBudgetaire')) {
                        if (Atexo_Module::isEnabled('Article133GenerationPf')) {
                            $this->validerValeurNotNull($item->trancheBudgetaire->getSelectedValue(), $index, $isValid, $arrayMsgErreur, 'ImgErrortranche', $scriptJs, Prado::localize('TRANCHE_BUDGETAIRE'));
                        }
                        if ($item->trancheBudgetaire->getSelectedValue()) {
                            $this->validerMontantAvecBorn($item->montant->Text, $item->bornInferieur->Value, $item->bornSuperieur->Value, $index, $isValid, $arrayMsgErreur, 'ImgErrorMontantTranche', $scriptJs);
                        }
                    }
                    $this->validerValeurFloatNotNull($item->montant->Text, $index, $isValid, $arrayMsgErreur, 'ImgErrorMontant', $scriptJs, Prado::localize('MONTANT_MARCHE'));
                    $this->validerValeurNotNull($item->objetMarche->Text, $index, $isValid, $arrayMsgErreur, 'ImgErrorObMarche', $scriptJs, Prado::localize('DEFINE_OBJET_MARCHE'));
                    $this->validerValeurNotNull($item->naturePrestations->getSelectedValue(), $index, $isValid, $arrayMsgErreur, 'ImgErrorPrest', $scriptJs, Prado::localize('NATURE_DES_PRESTATIONS'));
                }
            }
        }
        //Messages validation
        $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs);
    }

    /*
     * permet de valider une valeur
     */
    public function validerValeurNotNull($objetMarche, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs, $message)
    {
        if (trim($objetMarche)) {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
            $arrayMsgErreur[] = $message;
            $isValid = false;
        }
    }

    /**
     * permet de valider une valeur decimal.
     *
     * @param string $objetMarche    la valeur a verifier
     * @param int    $index          l'index de l'element
     * @param bool   $isValid        is valid
     * @param array  $arrayMsgErreur tableau des messages
     * @param string $imgError       le nom de l'image d'erreur
     * @param string $scriptJs       script js
     * @param string $message        le message d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function validerValeurFloatNotNull($objetMarche, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs, $message)
    {
        if (trim($objetMarche) && floatval($objetMarche)) {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
            $arrayMsgErreur[] = $message;
            $isValid = false;
        }
    }

    /*
     * Permet de valider une date
     */
    public function validerDate($date, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs, $message)
    {
        if ($date && Atexo_Util::isDate($date)) {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
            $arrayMsgErreur[] = $message;
            $isValid = false;
        }
    }

    /*
     * Permet de valider la date de notification et fin du marche prevesionnelle
     */
    public function validateDateNotificationFinMarche($dateNotifPrev, $dateFinPrev, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs, $message)
    {
        if ($dateNotifPrev && $dateFinPrev) {
            if (1 != Atexo_Util::diffDate($dateNotifPrev, $dateFinPrev)) {
                $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
                $arrayMsgErreur[] = $message;
                $isValid = false;
            }
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        }
    }

    /*
     * valider la date de notification et fin marche reelle
     */
    public function validateDateNotifFinMarcheReelle($dateNotifR, $dateFinR, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs)
    {
        if (($dateNotifR && !$dateFinR) || (!$dateNotifR && $dateFinR)) {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('ERREUR_DATE_FIN_ET_NOTIFICATION_REELLE_LIEES');
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        }
    }

    /*
     * valider le montant s'il est entre les borne si on a les tranches budgetaire
     */
    public function validerMontantAvecBorn($montant, $borneInfer, $borneSuper, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs)
    {
        if ($montant) {
            $montant = (float) str_replace(',', '.', str_replace(' ', '', $montant));
            $borneInfer = (float) str_replace(' ', '', $borneInfer);
            $borneSuper = (float) str_replace(' ', '', $borneSuper);
            if ((((0 == $borneSuper) || (null == $borneSuper)) && ($montant >= $borneInfer)) || (($montant >= $borneInfer) && ($montant <= $borneSuper))) {
                $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
            } else {
                $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('MESSAGE_ERREUR_VALEUR_SAISIE_ENTRE_LES_BORNES');
                $isValid = false;
            }
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        }
    }

    /*
     * Permet de valider une date si non null
     */
    public function validerFormatDate($date, $index, &$isValid, &$arrayMsgErreur, $imgError, &$scriptJs, $message)
    {
        if ($date && !Atexo_Util::isDate($date)) {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = '';";
            $arrayMsgErreur[] = $message;
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_entreprisesAttributaire_ctl".$index.'_'.$imgError."').style.display = 'none';";
        }
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param $isValid
     * @param $arrayMsgErreur
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs)
    {
        if ($isValid) {
            $param->IsValid = true;
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
        } else {
            $param->IsValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_boutonEnregistrer').style.display='';";
            $this->labelServerSide->Text = "<script>$scriptJs</script>";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }
        }
    }

    /**
     * Permet de recuperer l'url d'accés au donnees candidat.
     *
     * @param CommonOffres ou CommonOffrePapier $offre l'offre
     *
     * @return string l'url Donnees candidat
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlDonneesCandidat($offre)
    {
        return (Atexo_Module::isEnabled('DonneesCandidat') && ($offre instanceof CommonOffres)) ? 'index.php?page=Agent.DonneesCandidat&idEse='.base64_encode($offre->getEntrepriseId()).'&idEtab='
        .base64_encode($offre->getIdEtablissement()).'&id='.base64_encode($offre->getConsultationId()).'&calledFrom=decision&lot='.Atexo_Util::atexoHtmlEntities($_GET['lot']) : '';
    }
}
