<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpRenseignerDecision extends MpeTPage
{
    public $_procedureEquivalence;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->erreur->setVisible(false);
        if (!$this->IsPostBack) {
            $messageErreur = '';
            if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);

                if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION') ||
                              ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI') && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'))
                             || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                               || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                        $lotOrConsultation = (new Atexo_Consultation_Responses())->retrieveLotsForDecision(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());

                        if (is_array($lotOrConsultation) && 1 == count($lotOrConsultation)) {
                            $lotOrConsultation = array_shift($lotOrConsultation);
                            if ($lotOrConsultation->getDecisionLot() instanceof CommonDecisionLot) {
                                if ($lotOrConsultation->getDecisionLot()->getIdDecisionLot()) {
                                    //TODO erreur
                                    $messageErreur = Prado::localize('DECISION_DEJA_FAITE');
                                } else {
                                    $this->reference->Text = $consultation->getReferenceUtilisateur();
                                    if ($lotOrConsultation->getLot()) {
                                        $this->idLot->Visible = true;
                                        $this->numLot->Text = $lotOrConsultation->getLot().' - ';
                                        $this->descriptionLot->Text = $lotOrConsultation->getDescriptionTraduite();
                                    } else {
                                        $this->idConsultation->Visible = true;
                                        $this->descriptionCons->Text = $lotOrConsultation->getDescriptionTraduite();
                                    }

                                    $this->aTraiter->Checked = true;

                                    $this->panelaTraiter->setVisible(('1' == $consultation->getTypeDecisionARenseigner()));
                                    $this->spaceraTraiter->setVisible(('1' == $consultation->getTypeDecisionARenseigner()));

                                    $this->panelattributionMarche->setVisible(('1' == $consultation->getTypeDecisionAttributionMarche()));
                                    $this->spacerattributionMarche->setVisible(('1' == $consultation->getTypeDecisionAttributionMarche()));

                                    $this->panelsansSuite->setVisible(('1' == $consultation->getTypeDecisionAttributionMarche()));
                                    $this->spacersansSuite->setVisible(('1' == $consultation->getTypeDecisionAttributionMarche()));

                                    $this->panelinfructueux->setVisible(('1' == $consultation->getTypeDecisionDeclarationSansSuite()));
                                    $this->spacerinfructueux->setVisible(('1' == $consultation->getTypeDecisionDeclarationSansSuite()));

                                    $this->panelselectionEnVuePhaseSuivante->setVisible(('1' == $consultation->getTypeDecisionSelectionEntreprise()));
                                    $this->spacerselectionEnVuePhaseSuivante->setVisible(('1' == $consultation->getTypeDecisionSelectionEntreprise()));

                                    $this->panelattributionAccordCadre->setVisible(('1' == $consultation->getTypeDecisionAttributionAccordCadre()));
                                    $this->spacerattributionAccordCadre->setVisible(('1' == $consultation->getTypeDecisionAttributionAccordCadre()));

                                    $this->paneladmissionSAD->setVisible(('1' == $consultation->getTypeDecisionAdmissionSad()));
                                    $this->spaceradmissionSAD->setVisible(('1' == $consultation->getTypeDecisionAdmissionSad()));

                                    $this->panelautre->setVisible(('1' == $consultation->getTypeDecisionAutre()));
                                }
                            } else {
                                $messageErreur = Prado::localize('DECISION_IMPOSSIBLE');
                            }
                        } else {
                            $messageErreur = Prado::localize('DECISION_IMPOSSIBLE_AUCUN_LOT');
                        }
                    } else {
                        $messageErreur = Prado::localize('DECISION_IMPOSSIBLE');
                    }
                } else {
                    $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            } else {
                $messageErreur = Prado::localize('ERREUR_HABILITATION_DECISION');
            }

            if ($messageErreur) {
                $this->panelMessageErreur->setVisible(true);
                $this->erreur->setVisible(true);
                $this->container->setVisible(false);
                $this->panelMessageErreur->setMessage($messageErreur);
            }
        }
    }

    public function saveDecision()
    {
        if (!$this->aTraiter->Checked) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $decisionLot = new CommonDecisionLot();
            $decisionLot->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $decisionLot->setLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));

            if ($this->attributionMarche->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE'));
            } elseif ($this->sansSuite->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE'));
            } elseif ($this->infructueux->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX'));
            } elseif ($this->selectionEnVuePhaseSuivante->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE'));
            } elseif ($this->attributionAccordCadre->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_ATTRIBUTION_ACCORD_CADRE'));
            } elseif ($this->admissionSAD->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_ADMISSION_SAD'));
            } elseif ($this->autre->Checked) {
                $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_AUTRE'));
                $decisionLot->setAutreAPreciser($this->precision->Text);
            }
            $decisionLot->setDateMaj(date('Y-m-d H:i:s'));
            $decisionLot->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $decisionLot->save($connexion);
        }

        $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackbutton'])."').click();window.close();</script>";
    }
}
