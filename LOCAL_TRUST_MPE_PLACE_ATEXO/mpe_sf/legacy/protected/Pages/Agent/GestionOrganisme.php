<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class GestionOrganisme extends MpeTPage
{
    private $_organisme;
    public bool $_codeAcheteur = false;

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_codeAcheteur = Atexo_Module::isEnabled('AfficherCodeService');
        if (isset($_GET['listeOrg']) && 1 == $_GET['listeOrg']) {
            if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin') || (!Atexo_Module::isEnabled('GestionOrganismes')) || (!Atexo_CurrentUser::hasHabilitation('GererOrganismes'))) {
                $this->response->redirect('agent');
            }
        } else {
            if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $this->response->redirect('index.php?page=Agent.GestionEntitesAchat');
            }
        }
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && (Atexo_Module::isEnabled('GestionOrganismeParAgent') || (Atexo_Module::isEnabled('GestionOrganismes') && Atexo_CurrentUser::hasHabilitation('GererOrganismes'))) && isset($_GET['org']) && $_GET['org']) {
            $orga = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $orga = Atexo_CurrentUser::getCurrentOrganism();
        }
        $this->_organisme = $orga;
        self::customizeForm();
        $this->displayBlocLogos($orga);
        if (!$this->isPostBack) {
            $this->servicesMetiersAccessibles->setOrg($orga);
            $this->servicesMetiersAccessibles->displayOrganismeCompenenant();

            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $this->acronyme->Text = $orga;
            $my_array = [];
            $my_array[] = ' -- Choisissez une catégorie -- ';
            $c = new Criteria();

            $arrayTypeAvis = CommonCategorieINSEEPeer::doSelect($c, $connexionCom);
            foreach ($arrayTypeAvis as $elt) {
                $tab = substr_count($elt->getId(), '.');
                if (0 == $tab) {
                    $tab = '';
                } elseif (1 == $tab) {
                    $tab = '&nbsp;&nbsp;';
                } else {
                    $tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
                }
                $my_array[$elt->getId()] = $tab.$elt->getId().' - '.$elt->getLibelle();
            }

            $this->categorie->DataSource = $my_array;
            $this->categorie->DataBind();

            $article = ['&nbsp;', 'Le', 'La', 'L\''];
            $this->article->DataSource = $article;
            $this->article->DataBind();

            if ($orga) {
                $c = new Criteria();
                $c->add(CommonOrganismePeer::ACRONYME, $orga, Criteria::EQUAL);
                $organisme = CommonOrganismePeer::doSelectOne($c, $connexionCom);
                $this->sigle->Text = $organisme->getSigle();
                $this->categorie->selectedValue = $organisme->getCategorieInsee();
                $this->article->selectedValue = $organisme->getTypeArticleOrg();
                $this->denomination->Text = $organisme->getDenominationOrg();
                $this->description->Text = $organisme->getDescriptionOrg();
                $this->email->Text = $organisme->getEmail();
                $this->url->Text = $organisme->getUrl();
                $this->adresse->Text = $organisme->getAdresse();
                $this->adresse2->Text = $organisme->getAdresse2();
                $this->cp->Text = $organisme->getCp();
                $this->ville->Text = $organisme->getVille();
                $this->pays->Text = $organisme->getPays();
                $this->tel->Text = $organisme->getTel();
                $this->telecopie->Text = $organisme->getTelecopie();
                $this->pays_ar->Text = $organisme->getPaysAr();
                $this->ville_ar->Text = $organisme->getVilleAr();
                $this->adresse2_ar->Text = $organisme->getAdresse2Ar();
                $this->adresse_ar->Text = $organisme->getAdresseAr();
                $this->denomination_ar->Text = $organisme->getDenominationOrgAr();
                $this->description_ar->Text = $organisme->getDescriptionOrgAr();
                $this->siren->Text = $organisme->getSiren();
                $this->siret->Text = $organisme->getComplement();
                $this->updateForm->setVisible(true);
                if ('1' == $organisme->getActive()) {
                    $this->actif->setChecked(true);
                    $this->nonActif->setChecked(false);
                } else {
                    $this->actif->setChecked(false);
                    $this->nonActif->setChecked(true);
                }
            } else {
                $this->sigle->Text = strtoupper($orga);
                $this->pays->Text = 'France';
            }

            //set heure de remise de plis par défaut
            $configOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme($orga);
            if ($configOrganisme instanceof CommonConfigurationOrganisme) {
                $this->heureRemisDesPlisParDefaut->Text = $configOrganisme->getHeureLimiteDeRemiseDePlisParDefaut() ?? '';
            }
        }
    }

    public function update()
    {
        if ($this->petitLogo->HasFile) {//commencer le scan Antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->petitLogo->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->petitLogo->FileName.'"');

                return;
            }
        }
        if ($this->grandLogo->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->grandLogo->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->grandLogo->FileName.'"');

                return;
            }
        }//Fin du code scan Antivirus
        $infos = [];
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && (Atexo_Module::isEnabled('GestionOrganismeParAgent') || (Atexo_Module::isEnabled('GestionOrganismes') && Atexo_CurrentUser::hasHabilitation('GererOrganismes'))) && isset($_GET['org']) && $_GET['org']) {
            $orga = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $orga = Atexo_CurrentUser::getOrganismAcronym();
        }
        if ('' != $this->description->Text) {
            $infos['description'] = ($this->description->Text);
        }
        if ('' != $this->adresse->Text) {
            $infos['adresse'] = ($this->adresse->Text);
        }
        if ('' != $this->adresse2->Text) {
            $infos['adresse2'] = ($this->adresse2->Text);
        }
        if ('' != $this->cp->Text) {
            $infos['cp'] = ($this->cp->Text);
        }
        if ('' != $this->ville->Text) {
            $infos['ville'] = ($this->ville->Text);
        }
        if ('' != $this->tel->Text) {
            $infos['tel'] = ($this->tel->Text);
        }
        if ('' != $this->telecopie->Text) {
            $infos['telecopie'] = ($this->telecopie->Text);
        }
        if ('' != $this->email->Text) {
            $infos['email'] = ($this->email->Text);
        }
        if ('' != $this->url->Text) {
            $infos['url'] = ($this->url->Text);
        }
        if ('' != $this->pays->Text) {
            $infos['pays'] = ($this->pays->Text);
        }
        if ('' != $this->pays_ar->Text) {
            $infos['pays_ar'] = ($this->pays_ar->Text);
        }
        if ('' != $this->ville_ar->Text) {
            $infos['ville_ar'] = ($this->ville_ar->Text);
        }
        if ('' != $this->adresse2_ar->Text) {
            $infos['adresse2_ar'] = ($this->adresse2_ar->Text);
        }
        if ('' != $this->adresse_ar->Text) {
            $infos['adresse_ar'] = ($this->adresse_ar->Text);
        }
        if ('' != $this->description_ar->Text) {
            $infos['description_ar'] = ($this->description_ar->Text);
        }
        if ('' != $this->denomination_ar->Text) {
            $infos['denomination_ar'] = ($this->denomination_ar->Text);
        }
        if ('' != $this->siren->Text) {
            $infos['siren'] = ($this->siren->Text);
        }
        if ('' != $this->siret->Text) {
            $infos['siret'] = ($this->siret->Text);
        }
        $listesServices = false;
        if (isset($_GET['listeOrg']) && 1 == $_GET['listeOrg']) {
            $listesServices = [];
            foreach ($this->servicesMetiersAccessibles->serviceMetier->getItems() as $item) {
                $listesServices[$item->idService->Value] = $item->service->Checked;
            }
        }
        (new Atexo_Organismes())->update($orga, $this->sigle->Text, ($this->denomination->Text), $this->article->getSelectedValue(), $this->categorie->getSelectedValue(), $infos, $listesServices);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($orga);
        if ($this->actif->Checked) {
            $organisme->setActive('1');
        } else {
            $organisme->setActive('0');
        }
        $organisme->save($connexionCom);
        $message = $this->gestionImages($orga);
        if ($message) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
        }

        if ('' != $this->heureRemisDesPlisParDefaut->Text) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            try {
                $connexion->beginTransaction();
                $configOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme($orga);
                if ($configOrganisme instanceof CommonConfigurationOrganisme) {
                    $configOrganisme->setHeureLimiteDeRemiseDePlisParDefaut($this->heureRemisDesPlisParDefaut->Text);
                    $configOrganisme->save($connexion);
                    $connexion->commit();
                    Prado::getApplication()->Cache->set('cachedConfigurationOrganisme'.$orga, $configOrganisme, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
                }
            } catch (Exception $e) {
                $connexion->rollback();
                throw $e;
            }
        }

        $this->displayBlocLogos($orga);
        if (isset($_GET['listeOrg']) && 1 == $_GET['listeOrg']) {
            $this->response->redirect('index.php?page=Agent.GestionAllOrganismes&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        } else {
            $this->response->redirect('index.php?page=Agent.AgentDetailOrganisme&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        }
    }

    public function annuler()
    {
        if (isset($_GET['listeOrg']) && 1 == $_GET['listeOrg']) {
            $this->response->redirect('index.php?page=Agent.GestionAllOrganismes&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        } else {
            $this->response->redirect('?page=Agent.AgentDetailOrganisme&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
        }
    }

    public function gestionImages($orga)
    {
        $message = '';
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && (Atexo_Module::isEnabled('GestionOrganismeParAgent') || (Atexo_Module::isEnabled('GestionOrganismes') && Atexo_CurrentUser::hasHabilitation('GererOrganismes'))) && isset($_GET['org']) && $_GET['org']) {
            $orga = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $orga = Atexo_CurrentUser::getOrganismAcronym();
        }
        if (Atexo_Util::isSafePath(Atexo_Config::getParameter('BASE_ROOT_DIR'), $orga)) {
            if (Atexo_Module::isEnabled('AfficherImageOrganisme')) {
                if (!is_dir(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE'))) {
                    system('mkdir '.escapeshellarg(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE')), $res);
                    system('chmod 777 '.escapeshellarg(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE')), $res);
                }

                if ($this->petitLogo->HasFile) {
                    if ('JPG' == strtoupper(Atexo_Util::getExtension($this->petitLogo->FileName))) {
                        if (!move_uploaded_file($this->petitLogo->LocalName, Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg')) {
                            $message .= ($message ? '<br>' : '')."Problème lors de la copie de l'image de l'organisme (petit format)";
                        } else {
                            @unlink("themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg');
                            @copy(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg', "themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg');
                        }
                    } else {
                        $message .= ($message ? '<br>' : '')."Image de l'organisme (petit format) doit être de type .jpg";
                    }
                }

                if ($this->grandLogo->HasFile) {
                    if ('JPG' == strtoupper(Atexo_Util::getExtension($this->grandLogo->FileName))) {
                        if (!move_uploaded_file($this->grandLogo->LocalName, Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg')) {
                            $message .= ($message ? '<br>' : '')."Problème lors de la copie de l'image de l'organisme (grand format)";
                        } else {
                            @unlink("themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg');
                            @copy(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$orga.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg', "themes/$orga/".Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-grand.jpg');
                        }
                    } else {
                        $message .= ($message ? '<br>' : '')."Image de l'organisme (grand format) doit être de type .jpg";
                    }
                }
            }
        } else {
            $message .= ($message ? '<br>' : '')."L'acronyme de l'organisme n'est pas correct";
        }

        return $message;
    }

	public function displayBlocLogos($orga)
	{
		$this->panelModifierPetiteImage->setVisible(false);
		$this->panelAjoutPetiteImage->setVisible(false);
		$this->panelModifierGrandeImage->setVisible(false);
		$this->panelAjoutGrandeImage->setVisible(false);
		if(Atexo_Module::isEnabled('AfficherImageOrganisme')) {

			if(is_file(Atexo_Config::getParameter('BASE_ROOT_DIR') ."/".$orga. Atexo_Config::getParameter('PATH_ORGANISME_IMAGE')."logo-organisme-petit.jpg")){
				$this->panelModifierPetiteImage->setVisible(true);
				$this->smallLogo->ImageUrl = Atexo_Util::getUrlLogoOrganisme($orga);
			} else {
				$this->panelAjoutPetiteImage->setVisible(true);
			}
			if(is_file(Atexo_Config::getParameter('BASE_ROOT_DIR') ."/".$orga. Atexo_Config::getParameter('PATH_ORGANISME_IMAGE')."logo-organisme-grand.jpg")){
				$this->panelModifierGrandeImage->setVisible(true);
				$this->bigLogo->ImageUrl = Atexo_Util::getUrlLogoOrganisme($orga,true);
			} else {
				$this->panelAjoutGrandeImage->setVisible(true);
			}
		}
	}

    public function reloadLogos($sender, $param)
    {
        $this->panelModifierPetiteImage->render($param->NewWriter);
        $this->panelModifierGrandeImage->render($param->NewWriter);
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('TraduireOrganismeArabe')) {
            $this->validateur_denomination_ar->setEnabled(true);
            $this->validateur_description_ar->setEnabled(true);
        } else {
            $this->validateur_denomination_ar->setEnabled(false);
            $this->validateur_description_ar->setEnabled(false);
        }
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')
        && Atexo_Module::isEnabled('GestionOrganismes') && Atexo_CurrentUser::hasHabilitation('GererOrganismes')
        && isset($_GET['listeOrg']) && 1 == $_GET['listeOrg']) {
            $this->servicesMetiersAccessibles->setVisible('true');
        } else {
            $this->servicesMetiersAccessibles->setVisible('false');
        }

        if (Atexo_Module::isEnabled('SiretDetailEntiteAchat')) {
            $this->panelSiret->setVisible(true);
        } else {
            $this->panelSiret->setVisible(false);
        }
        if ($this->_codeAcheteur) {
            $this->panelCodeAcheteur->setVisible(true);
        } else {
            $this->panelCodeAcheteur->setVisible(false);
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
        $this->confirmationPart->setVisible(true);
        $this->panelMessage->setVisible(false);
    }


}
