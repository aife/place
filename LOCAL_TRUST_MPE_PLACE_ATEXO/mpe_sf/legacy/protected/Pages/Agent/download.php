<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;

class download extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $pathFile = null;
        $nomfile = null;
        if (1 == $_GET['downloadFile']) {
            $pathFile = Atexo_Config::getParameter('FICHIER_EXCEL_GENERE');
            $nomfile = 'ListeMarches.xls';
        }

        if (2 == $_GET['downloadFile']) {
            $pathFile = Atexo_Config::getParameter('FICHIER_LISTE_MARCHES_USER');
            $acronyme = $this->User->getOrganisme();
            $acronyme = strtolower($acronyme);
            $nomfile = 'marches_conclus_'.$acronyme;

            $file1 = $pathFile.$nomfile.'.pdf';
            $file2 = $pathFile.$nomfile.'.xls';

            if (file_exists($file1)) {
                $nomfile .= '.pdf';
                $pathFile .= $nomfile;
            } elseif (file_exists($file2)) {
                $nomfile .= '.xls';
                $pathFile .= $nomfile;
            }
        }
        $this->createHeader($pathFile, $nomfile);
    }

    public function createHeader($pathFile, $nomFile)
    {
        header('Content-disposition: attachment; filename="'.$nomFile.'"');
        header('Content-Type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($pathFile));

        // Internet Explorer nécessite des en-têtes spécifiques
        if (preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT'])) {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Pragma: no-cache');
        }
        //header("Pragma: no-cache");
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        readfile($pathFile);
    }

    public function isDownload($type)
    {
        return $_GET['type'] == $type;
    }
}
