<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_Mandataire;

/**
 * commentaires.
 *
 * @author Maatalla Houriya <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class GestionMandataires extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionMandataire')) {
            $this->gestionMandataires->Visible = true;
            $this->panelValidationSummary->visible = false;
            if (Atexo_Module::isEnabled('GestionBoampMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            if (!$this->IsPostBack) {
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
                $this->mandataireRepeater->DataSource = (new Atexo_EntityPurchase_Mandataire())
                    ->retrieveMandataire(
                        Atexo_CurrentUser::getCurrentServiceId(),
                        Atexo_CurrentUser::getCurrentOrganism()
                    );
                $this->mandataireRepeater->DataBind();
                $this->addMandataireLink->Attributes->onclick = "javascript:popUp('index.php?page=Agent.popUpGestionMandataire&serv=".Atexo_CurrentUser::getCurrentServiceId()."','yes');";
            }
        } else {
            $this->gestionMandataires->Visible = false;
            $this->panelValidationSummary->visible = true;
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->addMandataireLink->Attributes->onclick = "javascript:popUp('index.php?page=Agent.popUpGestionMandataire&serv=".$this->ChoixEntiteAchat->getSelectedEntityPurchase()."','yes');";
        $this->setViewState('idService', $this->ChoixEntiteAchat->getSelectedEntityPurchase());
        $res = (new Atexo_EntityPurchase_Mandataire())->retrieveMandataire($this->ChoixEntiteAchat->getSelectedEntityPurchase(), Atexo_CurrentUser::getCurrentOrganism());
        $this->mandataireRepeater->DataSource = (new Atexo_EntityPurchase_Mandataire())->retrieveMandataire($this->ChoixEntiteAchat->getSelectedEntityPurchase(), Atexo_CurrentUser::getCurrentOrganism());
        $this->mandataireRepeater->DataBind();
        $this->mandatairePanel->render($param->NewWriter);
    }

    public function refreshMandataireRepeater($sender, $param)
    {
        $this->mandataireRepeater->DataSource = (new Atexo_EntityPurchase_Mandataire())->retrieveMandataire($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->mandataireRepeater->DataBind();
        $this->mandatairePanel->render($param->NewWriter);
    }

    public function refreshRepeater($sender, $param)
    {
        $this->mandataireRepeater->DataSource = (new Atexo_EntityPurchase_Mandataire())->retrieveMandataire($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->mandataireRepeater->DataBind();
    }

    public function deleteMandataire($sender, $param)
    {
        $id = $param->CommandParameter;
        (new Atexo_EntityPurchase_Mandataire())->deleteMandataire($id);
        $this->mandataireRepeater->DataSource = (new Atexo_EntityPurchase_Mandataire())->retrieveMandataire($this->getViewState('idService'), Atexo_CurrentUser::getCurrentOrganism());
        $this->mandataireRepeater->DataBind();
    }
}
