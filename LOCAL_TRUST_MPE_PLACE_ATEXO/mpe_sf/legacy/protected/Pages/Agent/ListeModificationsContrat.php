<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTConsLotContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Prado\Prado;

/**
 * Class ListeModificationsContrat.
 */
class ListeModificationsContrat extends MpeTPage
{
    protected $connexion;
    protected $consultation;
    protected $contrat;

    public bool $_calledFrom = false;

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('ModifierContrat')) {
            $this->linkModification->setVisible(true);
        }
        $messageErreur = null;
        $logger = Atexo_LoggerManager::getLogger('contrat');

        if (isset($_GET['idContrat'])) {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->contrat = $contratQuery->getTContratTitulaireById($idContrat, $this->connexion);
            $listeLots = 0;
            $reference = 0;

            if (!$this->contrat->getHorsPassation()) {
                if (0 == $listeLots) {
                    $lots = CommonTConsLotContratQuery::create()->getLotsByIdContrat($idContrat, $this->connexion);

                    if (is_array($lots) && count($lots)) {
                        $listeLots = implode('#', $lots);
                    }
                }

                if (0 == $reference) {
                    $reference = CommonTConsLotContratQuery::create()->getConsultationRefByIdContratTitulaire($idContrat, $this->connexion);
                }
            }

            $this->recapConsultation->initialiserComposant($reference, $listeLots, $idContrat);

            if ($this->contrat instanceof CommonTContratTitulaire) {
                $this->setViewState('idContrat', $idContrat);
                $this->fillRepeater($idContrat);
                if ($this->contrat->isTypeContratConcession()) {
                    $donneesAnnuelle = (array) $this->contrat->getCommonDonneesAnnuellesConcessions();
                    $this->panelDonneesAnnuellesConcession->setVisible(true);
                    $this->fillRepeaterDonneesAnnuelles($donneesAnnuelle);
                } else {
                    $this->panelDonneesAnnuellesConcession->setVisible(false);
                }
            } else {
                $logger->error("Le contrat dont l'id = " . $idContrat . " n'existe pas");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }
        } else {
            $logger->error("Erreur d'accès à la page ListeModificationsContrat sans le parametre idContrat");
            $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
        }
    }

    /**
     * @param $idContrat
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function fillRepeater($idContrat)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $commonModificationContratQuery = new CommonModificationContratQuery();

        $nombreElement = $commonModificationContratQuery->getModificationsContrat(
            $idContrat,
            null,
            null,
            true,
            $connexion
        );

        if ($nombreElement >= 1) {
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->listeModificationContrat->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeModificationContrat->PageSize);
            $this->listeModificationContrat->setVirtualItemCount($nombreElement);
            $this->listeModificationContrat->setCurrentPageIndex(0);
            $this->panelResult->setVisible(true);
            $this->populateData();
        } else {
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->panelResult->setVisible(false);
        }
    }

    /**
     * @param $idEtablissement
     *
     * @return Entreprise
     *
     * @throws PropelException
     */
    public function getNomAttributaire($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();

        return $entrepriseQuery->findOneById($etablissement->getIdEntreprise())->getNom();
    }

    /**
     * @param $idEtablissement
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getSiretEntreprise($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();
        $entreprise = $entrepriseQuery->findOneById($etablissement->getIdEntreprise());

        return $entreprise->getSiren() . $etablissement->getCodeEtablissement();
    }

    public function retour()
    {
        $this->response->redirect('index.php?page=Agent.ResultatRechercheContrat');
    }

    /**
     * @param $modificationContrat
     *
     * @return int
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getIconeStatutDonneesEssentiellesPubliees($modificationContrat)
    {
        $enAttenteSn = Atexo_Config::getParameter('STATUT_EN_ATTENTE_SN');
        $publieSn = Atexo_Config::getParameter('STATUT_PUBLIE_SN');

        $retourOui = 'oui';
        $retourNon = 'non';
        $retourEnAttente = 'en_attente';

        if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
            if ($modificationContrat->getStatutPublicationSn() == $enAttenteSn) {
                $statut = $retourEnAttente;
            } elseif ($modificationContrat->getstatutPublicationSn() == $publieSn) {
                $statut = $retourOui;
            } else {
                $statut = $retourNon;
            }
        } else {
            $statut = $retourOui;
        }

        return $statut;
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }

        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }

            $this->listeModificationContrat->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->listeModificationContrat->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->listeModificationContrat->CurrentPageIndex + 1;
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->listeModificationContrat->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->populateData();
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        self::updatePagination($pageSize, $this->numPageTop->Text);
        $this->populateData();
    }

    public function populateData()
    {
        $offset = $this->listeModificationContrat->CurrentPageIndex * $this->listeModificationContrat->PageSize;
        $limit = $this->listeModificationContrat->PageSize;

        if ($offset + $limit > $this->listeModificationContrat->getVirtualItemCount()) {
            $limit = $this->listeModificationContrat->getVirtualItemCount() - $offset;
        }

        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $commonModificationContratQuery = new CommonModificationContratQuery();
        $idContrat = $this->getViewState('idContrat');

        $dataContrat = $commonModificationContratQuery->getModificationsContrat(
            $idContrat,
            $limit,
            $offset,
            false,
            $connexion
        );

        $this->listeModificationContrat->DataSource = $dataContrat;
        $this->listeModificationContrat->DataBind();
    }

    /**
     * @param $pageSize
     * @param $numPage
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nombreElement');
        $this->listeModificationContrat->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->listeModificationContrat->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeModificationContrat->PageSize);

        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }

        $this->listeModificationContrat->setCurrentPageIndex($numPage - 1);
        $this->numPageBottom->Text = $numPage;
        $this->numPageTop->Text = $numPage;
    }

    /**
     * @param $contrat
     *
     * @return bool
     */
    public function getStatutDonneesAPublier($modificationContrat)
    {
        $contratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contrat = $contratTitulaireQuery->findOneByIdContratTitulaire($modificationContrat->getIdContratTitulaire());

        return 0 === $contrat->getPublicationContrat();
    }

    /**
     * @param $modificationContrat
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function displayDonneeEssentielle($modificationContrat)
    {
        $contratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contrat = $contratTitulaireQuery->findOneByIdContratTitulaire($modificationContrat->getIdContratTitulaire());

        if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
            return (0 === $contrat->getPublicationContrat())
                && ($modificationContrat->getStatutPublicationSn() != Atexo_Config::getParameter('STATUT_JAMAIS_PUBLIE_SN'));
        } else {
            return false;
        }
    }

    /**
     * @return bool
     *
     * @throws PropelException
     */
    public function isConcession()
    {
        return ($this->contrat instanceof CommonTContratTitulaire) ? (new Atexo_TypeContrat())->retrieveContratOneByIdTypeContrat($this->contrat->getIdTypeContrat())->getConcession() : false;
    }

    /**
     * @param $modifications
     */
    public function fillRepeaterDonneesAnnuelles($donneesAnnuelles)
    {
        if (is_array($donneesAnnuelles) && count($donneesAnnuelles)) {
            $nombreElementDonneesAnnuelles = count($donneesAnnuelles);
            $this->nombreElementDonneesAnnuelles->Text = $nombreElementDonneesAnnuelles;
            $this->PagerDonneesAnnuellesBottom->setVisible(true);
            $this->PagerDonneesAnnuellesTop->setVisible(true);
            $this->setViewState('nombreElementDonneesAnnuelles', $nombreElementDonneesAnnuelles);
            $this->nombrePageDonneesAnnuellesTop->Text = ceil($nombreElementDonneesAnnuelles / $this->listeDonneesAnnuellesContrat->PageSize);
            $this->nombrePageDonneesAnnuellesBottom->Text = ceil($nombreElementDonneesAnnuelles / $this->listeDonneesAnnuellesContrat->PageSize);
            $this->listeDonneesAnnuellesContrat->setVirtualItemCount($nombreElementDonneesAnnuelles);
            $this->listeDonneesAnnuellesContrat->setCurrentPageIndex(0);
            $this->panelResultDonneesAnnuelles->setVisible(true);
            $this->populateDataDonneesAnnuelles();
        } else {
            $this->PagerDonneesAnnuellesBottom->setVisible(false);
            $this->PagerDonneesAnnuellesTop->setVisible(false);
            $this->panelResultDonneesAnnuelles->setVisible(false);
        }
    }

    public function populateDataDonneesAnnuelles()
    {
        $offset = $this->listeDonneesAnnuellesContrat->CurrentPageIndex * $this->listeDonneesAnnuellesContrat->PageSize;
        $limit = $this->listeDonneesAnnuellesContrat->PageSize;
        if ($offset + $limit > $this->listeDonneesAnnuellesContrat->getVirtualItemCount()) {
            $limit = $this->listeDonneesAnnuellesContrat->getVirtualItemCount() - $offset;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $idContrat = $this->getViewState('idContrat');
        $dataContrat = (new Atexo_Contrat())->getDonneesAnnuellesConcessionContrat($idContrat, $limit, $offset, $connexion);
        $this->listeDonneesAnnuellesContrat->DataSource = $dataContrat;
        $this->listeDonneesAnnuellesContrat->DataBind();
    }

    public function changePagerDonneesAnnuellesLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nombreResultatAfficherDonneesAnnuellesBottom':
                $pageSize = $this->nombreResultatAfficherDonneesAnnuellesBottom->getSelectedValue();

                $this->nombreResultatAfficherDonneesAnnuellesTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherDonneesAnnuellesTop':
                $pageSize = $this->nombreResultatAfficherDonneesAnnuellesTop->getSelectedValue();
                $this->nombreResultatAfficherDonneesAnnuellesBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePaginationDonneesAnnuelles($pageSize, $this->numPageDonneesAnnuellesTop->Text);
        $this->populateDataDonneesAnnuelles();
    }

    public function updatePaginationDonneesAnnuelles($pageSize, $numPage)
    {
        $nombreElementDonneesAnnuelles = $this->getViewState('nombreElementDonneesAnnuelles');
        $this->listeDonneesAnnuellesContrat->PageSize = $pageSize;
        $this->nombrePageDonneesAnnuellesTop->Text = ceil($nombreElementDonneesAnnuelles / $this->listeDonneesAnnuellesContrat->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElementDonneesAnnuelles / $this->listeDonneesAnnuellesContrat->PageSize);
        if ($numPage > $this->nombrePageDonneesAnnuellesTop->Text) {
            $numPage = $this->nombrePageDonneesAnnuellesTop->Text;
        }
        $this->listeDonneesAnnuellesContrat->setCurrentPageIndex($numPage - 1);
        $this->numPageDonneesAnnuellesBottom->Text = $numPage;
        $this->numPageDonneesAnnuellesTop->Text = $numPage;
    }

    public function goToPageDonneesAnnuelles($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonDonneesAnnuellesTop':
                $numPage = $this->numPageDonneesAnnuellesTop->Text;
                break;
            case 'DefaultButtonDonneesAnnuellesBottom':
                $numPage = $this->numPageDonneesAnnuellesBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageDonneesAnnuellesTop->Text) {
                $numPage = $this->nombrePageDonneesAnnuellesTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->listeDonneesAnnuellesContrat->CurrentPageIndex = $numPage - 1;
            $this->numPageDonneesAnnuellesBottom->Text = $numPage;
            $this->numPageDonneesAnnuellesTop->Text = $numPage;
            $this->populateDataDonneesAnnuelles();
        } else {
            $this->numPagenDonneesAnnuellesTop->Text = $this->listeDonneesAnnuellesContrat->CurrentPageIndex + 1;
            $this->numPagenDonneesAnnuellesBottom->Text = $this->listeDonneesAnnuellesContrat->CurrentPageIndex + 1;
        }
    }

    public function pageChangedDonneesAnnuelles($sender, $param)
    {
        $this->listeDonneesAnnuellesContrat->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageDonneesAnnuellesBottom->Text = $param->NewPageIndex + 1;
        $this->numPageDonneesAnnuellesTop->Text = $param->NewPageIndex + 1;
        $this->populateDataDonneesAnnuelles();
    }

    public function getIconeStatutDonneesAnnuelles($donneesAnnuelles)
    {
        if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
            if ($donneesAnnuelles->getSuiviPublicationSn() == Atexo_Config::getParameter('STATUT_EN_ATTENTE_SN')) {
                $statut = 'clock-circular-outline.png';
            } elseif ($donneesAnnuelles->getSuiviPublicationSn() == Atexo_Config::getParameter('STATUT_PUBLIE_SN')) {
                $statut = 'picto-check-ok.png';
            } else {
                $statut = 'close.png';
            }
        } else {
            $statut = 'picto-check-ok.png';
        }

        return $statut;
    }

    public function getStatutDonneesAnnuelles($donneesAnnuelles)
    {
        $retourOui = Prado::localize('DEFINE_OUI');
        $retourNon = Prado::localize('DEFINE_NON');
        $retourEnAttente = Prado::localize('EN_ATTENTE');

        if (Atexo_Module::isEnabled('donneesEssentiellesSuiviSn')) {
            if ($donneesAnnuelles->getSuiviPublicationSn() == Atexo_Config::getParameter('STATUT_EN_ATTENTE_SN')) {
                $statut = $retourEnAttente;
            } elseif ($donneesAnnuelles->getSuiviPublicationSn() == Atexo_Config::getParameter('STATUT_PUBLIE_SN')) {
                $statut = $retourOui;
            } else {
                $statut = $retourNon;
            }
        } else {
            $statut = $retourOui;
        }

        return $statut;
    }
}
