<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Newsletter\Atexo_Newsletter_Newsletter;

/**
 * Permet de télécharger la newsletter.
 *
 * @author Houriya Maatalla <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class DownloadPjNewsletter extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idPj = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $pj = Atexo_Newsletter_Newsletter::getPjByIdPiece($idPj, $org);
        if ($pj) {
            $this->_idFichier = $idPj;
            $this->_nomFichier = $pj->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
        } else {
            exit;
        }
    }
}
