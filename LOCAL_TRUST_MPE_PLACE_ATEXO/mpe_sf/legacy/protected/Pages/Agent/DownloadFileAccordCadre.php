<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * Created on 24 déc. 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class DownloadFileAccordCadre extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('ExtractionAccordsCadres') && Atexo_CurrentUser::hasHabilitation('TelechargerAccordsCadres')) {
            if (is_dir(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES'))) {
                $pointeur = opendir(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES'));
                while ($fichier = readdir($pointeur)) {
                    if (('.' != $fichier) && ('..' != $fichier)) {
                        if (is_file(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES').'/'.$fichier)) {
                            Atexo_Util::downloadLocalFile(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES').'/'.$fichier, $fichier);
                            exit;
                        } else {
                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::localize('MESSAGE_FICHIER_INTROUVABLE'));
                        }
                    }
                }
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentHome');
        }
    }
}
