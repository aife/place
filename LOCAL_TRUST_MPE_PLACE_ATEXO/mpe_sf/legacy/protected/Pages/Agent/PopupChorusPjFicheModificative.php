<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 27/11/2015
 * Time: 18:04.
 */
class PopupChorusPjFicheModificative extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        $this->ajoutFichier->setFileSizeMax(Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE'));
    }

    /**
     * permet de verifier la taille maximale du document à ajouter.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function verifySizeDoc($sender, $param)
    {
        $sizeDocAttache = $this->ajoutFichier->FileSize;
        if ((!$this->ajoutFichier->HasFile && '1' == $this->hasFile->Value) || ($sizeDocAttache > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE'))) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->docValidator->ErrorMessage = Prado::localize('TEXT_TAILLE_AUTORISEE_DOC', ['size' => Atexo_Util::arrondirSizeFile((Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE') / 1024))]);
        }
    }

    /**
     * permet de verifier l'extension du fichier à ajouter.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function verifyExtensionDoc($sender, $param)
    {
        $extensionFile = strtoupper(Atexo_Util::getExtension($this->ajoutFichier->FileName));

        $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
        if (!in_array($extensionFile, $data)) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->docValidator->ErrorMessage = str_replace('[__EXTENSION_FILES__]', implode(', ', $data), Prado::localize('TEXT_MSG_EXTESION_NON_VALIDE'));
        }
    }

    /**
     * permet de verifier le fichier à ajouter.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function verifyDocAttache($sender, $param)
    {
        $this->verifyExtensionDoc($sender, $param);
        $this->verifySizeDoc($sender, $param);
    }

    /**
     * permet d'ajouter un objet CommonTChorusFicheModificativePj.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function addPieceJointe($sender, $param)
    {
        if ($this->IsValid) {
            $idFicheModificative = Atexo_Util::atexoHtmlEntities($_GET['idFicheModificative']);
            try {
                (new Atexo_Chorus_Util())->loggerInfos(" Ajout Pj a la Fiche Modificative dont l'id : $idFicheModificative \n");
                if ($this->ajoutFichier->HasFile) {
                    //Début scan antivirus
                    $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
                    if ($msg) {
                        $this->afficherErreur($msg.' "'.$this->ajoutFichier->FileName.'"');

                        return;
                    }
                    //Fin scan antivirus
                    $infile = Atexo_Config::getParameter('COMMON_TMP').'pieceJointe'.session_id().time();
                    if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                        $atexoBlob = new Atexo_Blob();
                        $atexoCrypto = new Atexo_Crypto();
                        $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                        $pj = new CommonTChorusFicheModificativePj();
                        if (is_array($arrayTimeStampAvis)) {
                            $pj->setHorodatage($arrayTimeStampAvis['horodatage']);
                            $pj->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                        }
                        $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, Atexo_CurrentUser::getCurrentOrganism());
                        $pj->setIdFicheModificative($idFicheModificative);
                        $pj->setNomFichier($this->ajoutFichier->FileName);
                        $pj->setTaille($this->ajoutFichier->FileSize);
                        $pj->setFichier($avisIdBlob);
                        $pj->save($connexion);
                    }
                    @unlink($infile);
                }
                $this->labelClose->Text = '<script>refreshRepeaterPjFicheModificative();window.close();</script>';
            } catch (Exception $e) {
                $msgErreur = "Erreur lors de l'ajout de l'objet CommonTChorusFicheModificativePj à Fiche Modificative dont l'id =  $idFicheModificative \n".$e->getMessage()."\n";
                Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
                (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
            }
        }
    }

    /**
     * permet de visualiser l'erreur rencontrée en ajoutant.
     *
     * @param string $msg
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
