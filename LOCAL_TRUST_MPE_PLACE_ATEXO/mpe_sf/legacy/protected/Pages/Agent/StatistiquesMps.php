<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use Prado\Prado;

/**
 * Classe de gestion des statistiques métiers des marchés publics simplifiés (MPS) en date du 2014-09-09.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4.7
 */
class StatistiquesMps extends MpeTPage
{
    /**
     * Permet de gerer la visibilite du bloc de selection des organismes/services dans les criteres de recherche.
     */
    public bool $visible = true;

    /**
     * Permet de stocker de nombre de consultations passés en mode MPS.
     */
    public array $nbrConsultationsMps = [];

    /**
     * Permet de stocker de nombre de consultations passés en mode MPS ayant au moins une réponse en mode MPS.
     */
    public array $nbrConsultationsMpsAyantReponseMps = [];

    /**
     * Permet de stocker de nombre de réponses MPS récues pour les consultations MPS.
     */
    public array $nbrReponsesMps = [];

    /**
     * Permet de stocker le nombre total de consultations MPS.
     *
     * @var int|string
     */
    public $nbrTotalConsultationsMps = null;

    /**
     * Permet de stocker le nombre total de consultations MPS ayant au moins une reponse MPS.
     *
     * @var int|string
     */
    public $nbrTotalConsultationsAyantReponseMps = null;

    /**
     * Permet de stocker le nombre total de reponses MPS recues pour les consultations MPS.
     *
     * @var int|string
     */
    public $nbrTotalReponsesMps = null;

    /**
     * Initialisation de la classe.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism())) {
            echo Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            exit;
        }
        if (isset($_GET['allOrgs'])) {
            $this->visible = false;
        }

        if (!$this->IsPostBack) {
            // remplissage de la liste des entités
            $this->remplirListEntiteAssociee();
            $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
            $this->onSearchClick();
        }
    }

    /**
     * Permet de remplir la liste des entites associees.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function remplirListEntiteAssociee()
    {
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        $this->entiteeAssociee->Datasource = $entities;
        $this->entiteeAssociee->dataBind();
        $this->entiteeAssociee->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    /**
     * S'execute lorsque l'on clique sur le bouton "valider"
     * Construit l'objet critere recherche et recupere les donnees a afficher.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onSearchClick()
    {
        //Initialisation de variables
        $nombreReponsesMps = [];
        $nombreConsMps = [];
        $nbrConsMpsAvecReponse = [];

        //Critères de recherche
        $criteria = $this->getCriteresRecherche();

        //Nombre de consultations passées en mode MPS
        $nombreConsMps = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationsMps($criteria);
        //Nombre de consultations passées en mode MPS
        $nbrConsMpsAvecReponse = (new Atexo_Statistiques_RequetesStatistiques())->getNbreConsultationsMpsAvecReponsesMps($criteria);
        //Nombre de réponses MPS reçues pour les consultations MPS
        $nombreReponsesMps = (new Atexo_Statistiques_RequetesStatistiques())->getNbreReponsesMps($criteria);
        self::remplirChamps($nombreConsMps, $nbrConsMpsAvecReponse, $nombreReponsesMps);
    }

    /**
     * Construit l'objet criteres de recherche et le place dans la session de la page.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteresRecherche()
    {
        $criteria = new Atexo_Statistiques_CriteriaVo();
        $criteria->setOrganisme(null);
        if ('' != $this->critereFiltre_1_dateStart->Text) {
            $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
        }
        if ('' != $this->critereFiltre_1_dateEnd->Text) {
            $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
        }
        if (!isset($_GET['allOrgs'])) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
            $service = 'IN';
            if ($this->cumulValeurs->Checked) {
                $criteria->setValeurCumule(true);
                if (0 != $this->entiteeAssociee->SelectedValue) {
                    $service .= Atexo_EntityPurchase::retrieveAllChildrenServices($this->entiteeAssociee->SelectedValue, $org);
                } else {
                    $i = 1;
                    $service .= '(';
                    $entities = Atexo_EntityPurchase::getEntityPurchase($org, true);
                    $nbr = count($entities);

                    foreach ($entities as $key => $value) {
                        if ($i != $nbr) {
                            $service .= $key.',';
                        } else {
                            $service .= $key;
                        }

                        ++$i;
                    }

                    $service .= ')';
                }
            } else {
                $criteria->setValeurCumule(false);
                $service .= '('.$this->entiteeAssociee->SelectedValue.')';
            }
            $criteria->setSelectedEntity($this->entiteeAssociee->SelectedValue);
            $criteria->setOrganisme($org);
            $criteria->setIdService($service);
        }
        $this->setViewState('criteria', $criteria);

        return $this->getViewState('criteria');
    }

    /**
     * Permet de recuperer les donnees a afficher sur la page les resultats des statistiques
     * Et d'organiser les donnees pour la generation de l'excel.
     *
     * @param array $nombreConsMps:         Nombre de consultations passées en mode MPS
     * @param array $nbrConsMpsAvecReponse: Nomdre de consultations MPS ayant reçu au moins une réponse en mode MPS
     * @param array $nombreReponsesMps:     Nombre de réponses MPS reçues pour les consultations MPS
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function remplirChamps($nombreConsMps, $nbrConsMpsAvecReponse, $nombreReponsesMps)
    {
        //Nombre de consultations tagguées MPS
        $this->nbrConsultationsMps = [];
        $this->nbrConsultationsMps['travaux'] = $nombreConsMps[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
        $this->nbrConsultationsMps['fournitures'] = $nombreConsMps[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
        $this->nbrConsultationsMps['services'] = $nombreConsMps[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
        $this->nbrTotalConsultationsMps = $nombreConsMps[null];
        //Nomdre de consultations MPS ayant reçu au moins une réponse en mode MPS
        $this->nbrConsultationsMpsAyantReponseMps = [];
        $this->nbrConsultationsMpsAyantReponseMps['travaux'] = $nbrConsMpsAvecReponse[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
        $this->nbrConsultationsMpsAyantReponseMps['fournitures'] = $nbrConsMpsAvecReponse[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
        $this->nbrConsultationsMpsAyantReponseMps['services'] = $nbrConsMpsAvecReponse[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
        $this->nbrTotalConsultationsAyantReponseMps = $nbrConsMpsAvecReponse[null];
        //Nombre de reponses MPS pour les consultations MPS
        $this->nbrReponsesMps = [];
        $this->nbrReponsesMps['travaux'] = $nombreReponsesMps[Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')];
        $this->nbrReponsesMps['fournitures'] = $nombreReponsesMps[Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')];
        $this->nbrReponsesMps['services'] = $nombreReponsesMps[Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')];
        $this->nbrTotalReponsesMps = $nombreReponsesMps[null];

        //Sauvegarde des données pour la génération excel
        self::organiserDonneesStatPourExcel();
    }

    /**
     * Permet d'organiser les données des statistiques pour la génération excel.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function organiserDonneesStatPourExcel()
    {
        $valeurCumule = '';
        $entiteName = '';
        $acronymeOrganisme = Atexo_CurrentUser::getCurrentOrganism();
        $dataStat = [];
        //Donnees des statistiques
        $dataStat['nbrConsultationsMps'] = $this->nbrConsultationsMps;
        $dataStat['nbrConsMpsAyantReponseMps'] = $this->nbrConsultationsMpsAyantReponseMps;
        $dataStat['nbrReponsesMps'] = $this->nbrReponsesMps;
        $dataStat['nbrConsultationsMps']['total'] = $this->nbrTotalConsultationsMps;
        $dataStat['nbrConsMpsAyantReponseMps']['total'] = $this->nbrTotalConsultationsAyantReponseMps;
        $dataStat['nbrReponsesMps']['total'] = $this->nbrTotalReponsesMps;
        //Criteres de recherche
        $objetRecherche = Prado::localize('STATISTIQUES_CONCERNANT_PROCEDURES_MISE_LIGNE').' ';
        if ($this->getViewState('criteria')->getDateMiseEnLigneStart() && $this->getViewState('criteria')->getDateMiseEnLigneEnd()) {
            $objetRecherche .= Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE').' '.Atexo_Util::iso2frnDate($this->getViewState('criteria')->getDateMiseEnLigneStart())
                        .' '.Prado::localize('ET_LE').' '.Atexo_Util::iso2frnDate($this->getViewState('criteria')->getDateMiseEnLigneEnd());
        } elseif ($this->getViewState('criteria')->getDateMiseEnLigneStart() && !$this->getViewState('criteria')->getDateMiseEnLigneEnd()) {
            $objetRecherche .= Prado::localize('TEXT_A_PARTIR_DU').' '.Atexo_Util::iso2frnDate($this->getViewState('criteria')->getDateMiseEnLigneStart());
        } elseif (!$this->getViewState('criteria')->getDateMiseEnLigneStart() && $this->getViewState('criteria')->getDateMiseEnLigneEnd()) {
            $objetRecherche .= Prado::localize('TEXT_JUSQU_AU').' '.Atexo_Util::iso2frnDate($this->getViewState('criteria')->getDateMiseEnLigneEnd());
        }

        if (isset($_GET['allOrgs'])) {
            $entiteName = Prado::localize('DEFINE_TOUS_SERVICES_ORGANISMES_CONFONDUS');
        } else {
            $service = $this->getViewState('criteria')->getSelectedEntity();
            if ('0' === $service || 0 === $service) {
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                if ($organisme instanceof CommonOrganisme) {
                    $entiteName = $organisme->getDenominationOrg();
                }
            } else {
                $entite = Atexo_EntityPurchase::retrieveEntityById($service, $acronymeOrganisme);
                if ($entite instanceof CommonService) {
                    $entiteName = $entite->getLibelle();
                }
            }
            if ($this->getViewState('criteria')->getValeurCumule()) {
                $valeurCumule = ' / Valeurs cumulées';
            }
            $entiteName = Prado::localize('TEXT_ENTITE_ACHAT').' : '.$entiteName.(($valeurCumule) ? ' / '.Prado::localize('TEXT_VALEUR_CUMULE') : '');
        }
        $dataStat['recapRecherche']['entiteName'] = $entiteName;
        $dataStat['recapRecherche']['objetRecherche'] = $objetRecherche;
        $this->setViewState('dataStatistiques', $dataStat);
    }

    /**
     * Execute l'action du bouton annuler et reinitialise le formulaire de recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onEffaceClick()
    {
        $this->critereFiltre_comp_1->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
        $this->cumulValeurs->Checked = false;
    }

    /**
     * Permet de determiner le nombre de consultations passes en mode MPS
     * Utilisee pour l'affichage.
     *
     * @param string $categorie
     * @return: le nombre de consultations MPS pour la catégorie
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNbrConsultationsMps($categorie)
    {
        if ($this->nbrConsultationsMps[$categorie]) {
            return $this->nbrConsultationsMps[$categorie];
        }

        return '0';
    }

    /**
     * Permet de determiner le nomdre de consultations MPS ayant reçu au moins une reponse en mode MPS
     * Utilisee pour l'affichage.
     *
     * @param string $categorie
     * @return: le nombre de consultations MPS pour la catégorie, ayant au moins un reponse en mode MPS
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNbrConsultationsMpsAyantReponseMps($categorie)
    {
        if ($this->nbrConsultationsMpsAyantReponseMps[$categorie]) {
            return $this->nbrConsultationsMpsAyantReponseMps[$categorie];
        }

        return '0';
    }

    /**
     * Permet de determiner le nombre de reponses MPS reçues pour les consultations MPS
     * Utilisee pour l'affichage.
     *
     * @param string $categorie
     * @return: nombre de réponses MPS recues pour les consultations MPS
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNbrReponsesMps($categorie)
    {
        if ($this->nbrReponsesMps[$categorie]) {
            return $this->nbrReponsesMps[$categorie];
        }

        return '0';
    }

    /**
     * Calcul le nombre total de consultations passés en mode MPS
     * Utilisee pour l'affichage.
     *
     * @return number|string
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getTotalConsultationsMps()
    {
        $total = $this->nbrTotalConsultationsMps;
        if ($total) {
            return $total;
        }

        return '0';
    }

    /**
     * Permet de determiner le nombre total de consultations MPS ayant recu au moins une reponse en mode MPS
     * Utilisee pour l'affichage.
     *
     * @return number|string
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getTotalConsultationsAyantReponseMps()
    {
        $total = $this->nbrTotalConsultationsAyantReponseMps;
        if ($total) {
            return $total;
        }

        return '0';
    }

    /**
     * Permet de determiner le nombre total de reponses MPS recues pour les consultations MPS
     * Utilisee pour l'affichage.
     *
     * @return number|string
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getTotalReponsesMps()
    {
        $total = $this->nbrTotalReponsesMps;
        if ($total) {
            return $total;
        }

        return '0';
    }

    /**
     * Permet de generer l'excel des statistiques.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function genererExcelStatistiques()
    {
        (new Atexo_GenerationExcel())->genererExcelStatistiquesMps($this->getViewState('dataStatistiques'));
    }
}
