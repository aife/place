<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonGestionAdresses;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Mouslim MITALI
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionDesAdresses extends MpeTPage
{
    private string $_adresse = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessage->visible = false;
        if (Atexo_CurrentUser::hasHabilitation('GererAdressesService')) {
            //$this->panelPiecesAdresse->Visible = true;
            $this->panelMessageErreur->visible = false;
        } else {
            //$this->panelPiecesAdresse->Visible = false;
            $this->panelMessageErreur->visible = true;
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_PARAMETRAGES_ADRESSES'));
        }
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
        $this->_adresse = (new Atexo_Consultation())->retreiveAdressesServices($idService, $organisme);
        if (!$this->IsPostBack) {
            $this->displayAdresse();
        }
    }

    public function onClickSave($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $adresse = new CommonGestionAdresses();
        if ($this->_adresse) {
            $adresse = $this->_adresse;
        }
        $adresse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $adresse->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
        $adresse->setNomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
        $adresse->setPrenomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
        $adresse->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $adresse->setAdresseRetraisDossiers($this->retraitDossiers->Text);
        $adresse->setAdresseDepotOffres($this->addDepotDossiers->Text);
        $adresse->setLieuOuverturePlis($this->lieuOuverturePlis->Text);

        // Francais
        $adresse->setAdresseRetraisDossiersFr($this->retraitDossiers->Text);
        $adresse->setAdresseDepotOffresFr($this->addDepotDossiers->Text);
        $adresse->setLieuOuverturePlisFr($this->lieuOuverturePlis->Text);

        // Arabe
        $adresse->setAdresseRetraisDossiersAr($this->addRetraitDossiersAr->Text);
        $adresse->setAdresseDepotOffresAr($this->addDepotDossiersAr->Text);
        $adresse->setLieuOuverturePlisAr($this->lieuOuverturePlisAr->Text);

        if ($adresse->save($connexionCom)) {
            $this->panelMessage->visible = true;
            $this->panelPiecesAdresse->Visible = true;
            $this->buttonSave->Visible = true;
            $this->panelMessage->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
        } else {
            $this->panelMessageErreur->visible = true;
            $this->panelPiecesAdresse->Visible = true;
            $this->panelMessageErreur->setMessage(Prado::localize('AJOUT_ADRESSES_ECHOUE'));
        }
    }

    public function displayAdresse()
    {
        if ($this->_adresse) {
            $this->retraitDossiers->Text = $this->_adresse->getAdresseRetraisDossiers();
            $this->addDepotDossiers->Text = $this->_adresse->getAdresseDepotOffres();
            $this->lieuOuverturePlis->Text = $this->_adresse->getLieuOuverturePlis();

            // Afficher les champs en arabe
            $this->addRetraitDossiersAr->Text = $this->_adresse->getAdresseRetraisDossiersAr();
            $this->addDepotDossiersAr->Text = $this->_adresse->getAdresseDepotOffresAr();
            $this->lieuOuverturePlisAr->Text = $this->_adresse->getLieuOuverturePlisAr();
        }
    }
}
