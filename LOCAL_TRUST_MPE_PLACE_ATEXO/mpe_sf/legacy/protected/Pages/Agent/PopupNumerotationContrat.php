<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupNumerotationContrat extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat);
        $this->idCartoucheAttributaire->setContrat($contrat);
        if (!$this->getIsPostBack()) {
            $consultationId = Atexo_Util::atexoHtmlEntities(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            $lots = Atexo_Util::atexoHtmlEntities(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
            $this->recapConsultation->initialiserComposant($consultationId, $lots, $idContrat);

            if ($contrat instanceof CommonTContratTitulaire) {
                $this->setIdContrat($idContrat);
                if (Atexo_Config::getParameter('AFFICHER_NUMERO_COURT_CONTRAT')) {
                    $this->numCourt->Text = $contrat->getNumeroContrat();
                    $this->numCourt->enabled = false;
                    /*
                    if($contrat->isContratChapeau() || !Atexo_Consultation_Contrat::getCasTypesMarchesPourNumerotation($contrat))
                    {
                        $this->numCourt->enabled = false;
                    }
                    */
                }
                if (Atexo_Config::getParameter('AFFICHER_NUMERO_LONG_CONTRAT')) {
                    $this->numLong->Text = $contrat->getNumLongOeap();
                    $this->numLong->enabled = false;
                    /*
                    if($contrat->isContratChapeau() || !Atexo_Consultation_Contrat::getCasTypesMarchesPourNumerotation($contrat))
                    {
                        $this->numLong->enabled = false;
                    }
                    */
                }
                if (Atexo_Config::getParameter('AFFICHER_NUMERO_CHAPEAU_CONTRAT') && $this->isContratMultiAttributaire()) {
                    $this->numChapeau->Text = $this->recupererChapeauContratMulti();
                    $this->numChapeau->enabled = false;
                }
                if (Atexo_Config::getParameter('AFFICHER_NUMERO_REF_INTERNE_CONTRAT')) {
                    $this->referenceInterne->Text = $contrat->getReferenceLibre();
                    //$this->referenceInterne->enabled = false;
                }

                //Activer/desactiver validateurs
                $this->validateurRefInterne->enabled = true;
                $this->validateurNumeroCourt->enabled = false;
                $this->validateurNumeroLong->enabled = false;
                $this->validateurNumeroLong->enabled = false;
            }
        }
    }

    public function setIdContrat($value)
    {
        $this->setViewState('idContrat', $value);
    }

    public function getIdContrat()
    {
        $this->getViewState('idContrat');
    }

    /**
     * Permet de sauvegarde l'attribution.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function save($sender, $param)
    {
        try {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);
            if ($contrat instanceof CommonTContratTitulaire) {
                if (Atexo_Config::getParameter('AFFICHER_NUMERO_REF_INTERNE_CONTRAT')) {
                    $contrat->setReferenceLibre($this->referenceInterne->Text);

                    if (Atexo_Module::isEnabled('NumDonneesEssentiellesManuel')) {
                        $contrat->setNumIdUniqueMarchePublic($this->referenceInterne->Text);
                    }
                }
                if ($contrat->isContratHorsPassationAndAcSad()) {
                    if (Atexo_Config::getParameter('AFFICHER_NUMERO_COURT_CONTRAT')) {
                        $contrat->setNumeroContrat($this->numCourt->Text);
                    }
                    if (Atexo_Config::getParameter('AFFICHER_NUMERO_LONG_CONTRAT')) {
                        $contrat->setNumLongOeap($this->numLong->Text);
                    }
                }
                $contrat->setDateAttribution($this->frnDate2iso($this->recapConsultation->getDateAttribution()));

                $contrat->save($connexion);


                $this->labelScript->Text = "<script type='text/javascript'>var str = window.opener.location.href; window.opener.location.href = str;window.close();</script>";
            }
        } catch (\Exception $e) {
            Prado::log('Erreur de numérotation du contrat : '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionAttribution.php');
        }
    }

    /**
     * Permet de verifier si un contrat est multi attributaire.
     *
     * @return bool : true si contrat multi attributaire, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isContratMultiAttributaire()
    {
        return (new Atexo_Consultation_Contrat())->isContratMultiAttributaireByIdContrat(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat'])));
    }

    /**
     * Permet de recuperer le chapeau du contrat multi.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function recupererChapeauContratMulti()
    {
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById(base64_decode($_GET['idContrat']));
        if ($contrat && ($contrat->getCommonTContratMulti() instanceof CommonTContratMulti)) {
            return $contrat->getCommonTContratMulti()->getNumeroContrat();
        }

        return '';
    }
}
