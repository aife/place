<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de ArchiveMetadonnee.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchiveMetadonnee extends MpeTPage
{
    private $_consultation = null;

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultationId = $this->getReference();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultation = CommonConsultationPeer::retrieveByPK($consultationId, $connexion);
        $this->setConsultation($consultation);
        if ($consultation instanceof CommonConsultation) {
            if (!$this->IsPostBack) {
                $this->description->Text = $consultation->getArchivemetadescription();
                $this->keywords->Text = $consultation->getArchivemetamotsclef();
            }
            $this->panelInfoConsultation->setVisible(true);
            $this->panelEnrichissement->setVisible(true);
            $this->panelBoutonValiderEtAnnuler->setVisible(true);
            $this->panelMessage->setVisible(false);
            $this->panelQuitter->setVisible(false);
        } else {
            $this->panelErreur->setVisible(true);
            $this->panelErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
        }
        $this->quitter->attachEventHandler('OnClick', [$this, 'retour']);
        $this->retour->attachEventHandler('OnClick', [$this, 'retour']);
    }

    public function retour()
    {
        if (isset($_GET['archiveRappel']) && true == $_GET['archiveRappel']) {
            $this->response->Redirect('?page=Agent.ArchiveRappelTableauDeBord&id='.$this->getReference());
        }
        if (isset($_GET['detail'])) {
            $this->response->Redirect('?page=Agent.DetailConsultation&id='.$this->getReference());
        } else {
            $this->response->Redirect('?page=Agent.ArchiveTableauDeBord&id='.$this->getReference());
        }
    }

    public function saveMeta()
    {
        $connexion = null;
        $consultation = $this->getConsultation();
        $consultation->setArchivemetadescription($this->description->Text);
        $consultation->setArchivemetamotsclef($this->keywords->Text);
        $consultation->save($connexion);
        $this->panelInfoConsultation->setVisible(false);
        $this->panelEnrichissement->setVisible(false);
        $this->panelBoutonValiderEtAnnuler->setVisible(false);
        $this->panelMessage->setVisible(true);
        $this->panelMessage->setMessage(Prado::localize('CONFIRMATION_ENREGISTREMENT_METADONNEES'));
        $this->panelQuitter->setVisible(true);
    }

    public function getReference()
    {
        return Atexo_Util::atexoHtmlEntities($_GET['id']);
    }
}
