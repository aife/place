<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Page de gestion des certificats -recherche entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SearchCertificatsFournisseurs extends MpeTPage
{
    public string $_company = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->displayVilleRc();
        $this->displayEntrepriseEtrangere();
        if (isset($_GET['idCompany']) && '' != $_GET['idCompany']) {
            $arIdentifiants = explode('-', Atexo_Util::atexoHtmlEntities($_GET['idCompany']));
            if (is_array($arIdentifiants)) {
                if (isset($_GET['etranger'])) {
                    $this->etranger->checked = true;
                    $this->nonEtablieMaroc->SetDisplay('Dynamic');
                    $this->etablieMaroc->SetDisplay('None');
                    $this->idNational->Text = $arIdentifiants[0];
                //$this->pays->setSelectedItem()->setText($arIdentifiants[1]);
                } else {
                    $this->maroc->checked = true;
                    $this->etablieMaroc->SetDisplay('Dynamic');
                    $this->nonEtablieMaroc->SetDisplay('None');
                    $this->RcVille->SelectedValue = $arIdentifiants[0];
                    $this->RcNumero->Text = $arIdentifiants[1];
                }
            }
        }
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    public function displayEntrepriseEtrangere()
    {
        $datasource = [];
        $datasource[0] = Prado::localize('TEXT_SELECTIONNER');
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveArrayCountries();
        $datasource = array_merge($datasource, is_array($listCountries) ? $listCountries : []);
        $this->pays->DataSource = $datasource;
        $this->pays->DataBind();
    }

    public function onSearchClick($sender, $param)
    {
        $rc = null;
        $idNational = null;
        $paysEtranger = null;
        if ($this->maroc->Checked) {
            $villeRc = $this->RcVille->getSelectedValue();
            $numeroRc = $this->RcNumero->Text;
            $rc = $villeRc.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$numeroRc;
            $company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($rc);
        } else {
            $paysEtranger = $this->pays->getSelectedItem()->getText();
            $idNational = $this->idNational->Text;
            $company = (new Atexo_Entreprise())->retrieveCompanyByIdNational($idNational, $paysEtranger);
        }
        if ($company instanceof Entreprise) {
            if ($this->maroc->Checked) {
                $url = '?page=Agent.ResultCertificatsFournisseurs&idCompany='.$rc;
            } else {
                $url = '?page=Agent.ResultCertificatsFournisseurs&idCompany='.$idNational.'&pays='.$paysEtranger;
            }
            $this->response->redirect($url);
        } else {
            if ($this->maroc->Checked) {
                $url = '?page=Agent.ResultCertificatsFournisseurs&rc='.$rc;
            } else {
                $url = '?page=Agent.ResultCertificatsFournisseurs&idCompany='.$idNational.'&pays='.$paysEtranger;
            }
            $this->response->redirect($url);
        }
    }
}
