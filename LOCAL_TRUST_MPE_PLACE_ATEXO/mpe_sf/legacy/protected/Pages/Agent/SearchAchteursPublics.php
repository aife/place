<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/**
 * Classe de.
 *
 * @author loubna EZZIAI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class SearchAchteursPublics extends MpeTPage
{
    public function onInit($param)
    {

        if ((!Atexo_Module::isEnabled('AnnuaireAcheteursPublics')) && (Atexo_Module::isEnabled('SocleInterne'))) {
            $this->Master->setCalledFrom('annuaire');
        } else {
            $this->Master->setCalledFrom('agent');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->SearchAgent->fillData();
        $this->panelConfirmation->setDisplay('None');
    }

    public function onClickSearch($sender, $param)
    {
        $this->SearchAgent->fillCriteriaVoAgent();
        $criteriaVoAgent = $this->getViewState('CriteriaVoAgent');

        $this->resultSearchAgent->intialiserRepeater();
        $this->resultSearchAgent->setCriteriaVoAgent($criteriaVoAgent);
        $this->resultSearchAgent->repeaterDataBind();
        $this->resultSearchAgent->panelRefresh->render($param->NewWriter);
    }

    /*
     *  methode qui stock l'url de la page d'ajou agent
     * (Remarque : dans cette page on l'utilise pas c'est juste pour utiliser le meme composant SearchAgent)
     */
    public function getUrlAjoutAgent($organisme, $service)
    {
        if ($organisme) {
            $url = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=".$service.'&organisme='.$organisme."','yes');";
        } else {
            $url = "javascript:popUp('index.php?page=Agent.PopUpAjoutAgent&idService=".$service."','yes');";
        }
        $this->lienAjouterAgent->NavigateUrl = $url;
    }

    public function sortAgents($sender, $param)
    {
        $this->resultSearchAgent->sortAgents($sender, $param);
    }
}
