<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonProcedureEquivalencePeer;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDume;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDumeQuery;
use Application\Propel\Mpe\CommonTypeProcedureOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GestionCalendrier;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use AtexoCrypto\Dto\Chiffrement;
use AtexoCrypto\Dto\Enveloppe;
use Prado\Prado;
use Prado\Web\UI\WebControls\TDisplayStyle;

/**
 * Composant choix des types de procédures.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ParametrerProcedures extends MpeTPage
{
    private $_selectedProcedureType;
    private ?\Application\Propel\Mpe\CommonProcedureEquivalence $_procedureEquivalence = null;
    private ?\Application\Propel\Mpe\CommonTypeProcedureOrganisme $_procedureTypeOrganisme = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->panelParametrageTypeProcedure->setDisplay('None');
            $proceduresType = $this->getProceduresType();
            if (is_array($proceduresType) && count($proceduresType) > 0) {
                $this->procedureType->DataSource = $proceduresType;
                $this->procedureType->DataBind();
            }
            // remplissage de la liste du délai d'affichage après la date de mise ligne
            $nombreJour = [];
            $valeursPoursuivreAffichage = json_decode(str_replace("'", '"', Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE')), null, 512, JSON_THROW_ON_ERROR);
            foreach ($valeursPoursuivreAffichage as $valeur) {
                $nombreJour[$valeur[0].$valeur[1]] = $valeur[0].' '.Prado::localize($valeur[1].($valeur[0] > 1 ? 'S' : ''));
            }
            $this->poursuivreAffichageNbJour->Datasource = $nombreJour;
            $this->poursuivreAffichageNbJour->dataBind();
        } else {
            $this->panelParametrageTypeProcedure->setDisplay('Dynamic');
        }
        $this->customizeForm();
    }

    /**
     * Valide le choix du type de la procédure.
     */
    public function validateProcedureType($sender, $param)
    {
        $this->getProcedureTypeParameters();
        $this->_selectedProcedureType = $this->procedureType->getSelectedValue();
        $this->panelChoixTypeProcedure->Visible = false;
        $this->panelParametrageTypeProcedure->Visible = true;
        if (Atexo_Module::isEnabled('CalendrierDeLaConsultation')) {
            $this->parametrage_calendrier->calendrierParametre->render($param->getNewWriter());
            $this->fillCalender($this->_selectedProcedureType, Atexo_CurrentUser::getCurrentOrganism());
            $this->panel_calendrier->Visible = true;
        }
    }

    /*  Retourne la liste des procédures
    *
    */
    public function getProceduresType()
    {
         return (new Atexo_Consultation_ProcedureType())->getTypeProcedureByOrganisme(Atexo_CurrentUser::getOrganismAcronym(), true);
    }

    /**
     * retourne la valeur de la procédure quivalente pour un champ donné {-1,1,+1,-0,0,+0,''}.
     *
     * @param ineteger $elementAffiche affiché
     * @param ineteger $elementFige    figé
     * @param string   $active         element selectionnée ou pas
     */
    public function getStateOfElement($elementAffiche, $elementFige, $active)
    {
        if (null == $active) {
            $active = 0;
        }
        if ($elementAffiche && $elementFige) {
            $typeAffichage = '';
        } elseif ($elementAffiche && !$elementFige) {
            $typeAffichage = '+';
        } elseif (!$elementAffiche && $elementFige) {
            $typeAffichage = '-';
        } elseif (!$elementAffiche && !$elementFige) {
            $typeAffichage = '-';
        }

        return $typeAffichage.(string) $active;
    }

    /**
     * Affiche les paramètres de la procédure séléctionnée.
     */
    public function getProcedureTypeParameters()
    {
        $idProcedureType = $this->procedureType->getSelectedValue();
        $this->libelleProcedure->Text = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($idProcedureType);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_procedureEquivalence = CommonProcedureEquivalencePeer::retrieveByPk($idProcedureType, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);

        if ($this->_procedureEquivalence) {
            $scriptJs = '';
            // Identification de la consultation : non allotissement
            $this->getActivateElement('marcheUnique', $this->_procedureEquivalence->getEnvOffreTypeUnique());
            $this->getStyleDisplayAffiche('marcheUniqueAffiche', $this->_procedureEquivalence->getEnvOffreTypeUnique());
            $this->getStyleDisplayFige('marcheUniqueFige', $this->_procedureEquivalence->getEnvOffreTypeUnique());

            $this->getActivateElement('alloti', $this->_procedureEquivalence->getEnvOffreTypeMultiple());
            $this->getStyleDisplayAffiche('allotiAffiche', $this->_procedureEquivalence->getEnvOffreTypeMultiple());
            $this->getStyleDisplayFige('allotiFige', $this->_procedureEquivalence->getEnvOffreTypeMultiple());

            // Téléchargement partiel du DCE
            $this->getActivateElement('telechargementPartiel', $this->_procedureEquivalence->getPartialDceDownload());
            $this->getStyleDisplayAffiche('telechargmementPartielAffiche', $this->_procedureEquivalence->getPartialDceDownload());
            $this->getStyleDisplayFige('telechargmementPartielFige', $this->_procedureEquivalence->getPartialDceDownload());

            $this->getActivateElement('accesPublic', $this->_procedureEquivalence->getProcedurePublicite());
            $this->getStyleDisplayAffiche('accesPublicAffiche', $this->_procedureEquivalence->getProcedurePublicite());
            $this->getStyleDisplayFige('accesPublictFige', $this->_procedureEquivalence->getProcedurePublicite());

            $this->getActivateElement('accesRestreint', $this->_procedureEquivalence->getProcedureRestreinte());
            $this->getStyleDisplayAffiche('accesRestreintAffiche', $this->_procedureEquivalence->getProcedureRestreinte());
            $this->getStyleDisplayFige('accesRestreintFige', $this->_procedureEquivalence->getProcedureRestreinte());

            $this->getActivateElement('envoisPostaux', $this->_procedureEquivalence->getGestionEnvoisPostaux());
            $this->getStyleDisplayAffiche('envoisPostauxPrevuAffiche', $this->_procedureEquivalence->getGestionEnvoisPostaux());
            $this->getStyleDisplayFige('envoisPostauxPrevuFige', $this->_procedureEquivalence->getGestionEnvoisPostaux());

            $this->getActivateElement('envoiType', $this->_procedureEquivalence->getConstitutionDossierReponse());
            $this->getStyleDisplayAffiche('formDocAffiche', $this->_procedureEquivalence->getConstitutionDossierReponse());
            $this->getStyleDisplayFige('formDocFige', $this->_procedureEquivalence->getConstitutionDossierReponse());

            $this->getActivateElement('papier', $this->_procedureEquivalence->getTireurPlanPapier());
            $this->getStyleDisplayAffiche('formatPapierAffiche', $this->_procedureEquivalence->getTireurPlanPapier());
            $this->getStyleDisplayFige('formatPapierFige', $this->_procedureEquivalence->getTireurPlanPapier());

            $this->getActivateElement('cdRom', $this->_procedureEquivalence->getTireurPlanCdrom());
            $this->getStyleDisplayAffiche('formatCdRomAffiche', $this->_procedureEquivalence->getTireurPlanCdrom());
            $this->getStyleDisplayFige('formatCdRomFige', $this->_procedureEquivalence->getTireurPlanCdrom());
            // Réponse électronique
            $this->getActivateElement('repAutorisee', $this->_procedureEquivalence->getElecResp());
            $this->getStyleDisplayAffiche('autoriseAffiche', $this->_procedureEquivalence->getElecResp());
            $this->getStyleDisplayFige('autoriseFige', $this->_procedureEquivalence->getElecResp());

            $this->getActivateElement('obligatoireOui', $this->_procedureEquivalence->getRepObligatoire());
            $this->getStyleDisplayAffiche('obligOuiAffiche', $this->_procedureEquivalence->getRepObligatoire());
            $this->getStyleDisplayFige('obligOuiFige', $this->_procedureEquivalence->getRepObligatoire());

            //$this->getActivateElement('obligatoireNon', $this->_procedureEquivalence->getNoRepObligatoire());
            //$this->getStyleDisplayAffiche('obligNonAffiche', $this->_procedureEquivalence->getNoRepObligatoire());
            //$this->getStyleDisplayFige('obligNonFige', $this->_procedureEquivalence->getNoRepObligatoire());

            //$this->getActivateElement('repElecAutreAutorisee', $this->_procedureEquivalence->getRespElecAutrePlateforme());
            //$this->getStyleDisplayAffiche('autoriseAutreAffiche', $this->_procedureEquivalence->getRespElecAutrePlateforme());
            //$this->getStyleDisplayFige('autoriseAutreFige', $this->_procedureEquivalence->getRespElecAutrePlateforme());

            $this->getActivateElement('signaturePropre', $this->_procedureEquivalence->getSignaturePropre());
            $this->getStyleDisplayAffiche('signatureePropreAffiche', $this->_procedureEquivalence->getSignaturePropre());
            $this->getStyleDisplayFige('signatureePropreFige', $this->_procedureEquivalence->getSignaturePropre());

            $this->getActivateElement('annexeFinanciere', $this->_procedureEquivalence->getAnnexeFinanciere());
            $this->getStyleDisplayAffiche('annexeFinanciereAffiche', $this->_procedureEquivalence->getAnnexeFinanciere());
            $this->getStyleDisplayFige('annexeFinanciereFige', $this->_procedureEquivalence->getAnnexeFinanciere());

            $this->getActivateElement('repRefusee', $this->_procedureEquivalence->getNoElecResp());
            $this->getStyleDisplayAffiche('refuseAffiche', $this->_procedureEquivalence->getNoElecResp());
            $this->getStyleDisplayFige('refuseFige', $this->_procedureEquivalence->getNoElecResp());

            //  Caractéristiques des réponses électroniques
            $this->getActivateElement('signatureElectroniqueOui', $this->_procedureEquivalence->getSignatureEnabled());
            $this->getStyleDisplayAffiche('signatureElectroniqueOuiAffiche', $this->_procedureEquivalence->getSignatureEnabled());
            $this->getStyleDisplayFige('signatureElectroniqueOuiFige', $this->_procedureEquivalence->getSignatureEnabled());

            $this->getActivateElement('signatureElectroniqueNon', $this->_procedureEquivalence->getSignatureDisabled());
            $this->getStyleDisplayAffiche('signatureElectroniqueNonAffiche', $this->_procedureEquivalence->getSignatureDisabled());
            $this->getStyleDisplayFige('signatureElectroniqueNonFige', $this->_procedureEquivalence->getSignatureDisabled());

            $this->getActivateElement('signatureElectroniqueAutoriser', $this->_procedureEquivalence->getSignatureAutoriser());
            $this->getStyleDisplayAffiche('signatureElectroniqueAutoriserAffiche', $this->_procedureEquivalence->getSignatureAutoriser());
            $this->getStyleDisplayFige('signatureElectroniqueAutoriserFige', $this->_procedureEquivalence->getSignatureAutoriser());

            // Chiffrement des enveloppes électroniques
            $this->getActivateElement('chiffrementeOui', $this->_procedureEquivalence->getCipherEnabled());
            $this->getStyleDisplayAffiche('chiffrementeOuiAffiche', $this->_procedureEquivalence->getCipherEnabled());
            $this->getStyleDisplayFige('chiffrementeOuiFige', $this->_procedureEquivalence->getCipherEnabled());

            $this->getActivateElement('poursuivreAffichage', $this->_procedureEquivalence->getDelaiDateLimiteRemisePli());
            $this->getStyleDisplayAffiche('poursuivreAffiche', $this->_procedureEquivalence->getDelaiDateLimiteRemisePli());
            $this->getStyleDisplayFige('poursuivreFige', $this->_procedureEquivalence->getDelaiDateLimiteRemisePli());
            $this->getActivateElement('poursuivreAffichageNon', $this->_procedureEquivalence->getPoursuiteDateLimiteRemisePli());
            $this->getStyleDisplayAffiche('poursuivreNonAffiche', $this->_procedureEquivalence->getPoursuiteDateLimiteRemisePli());
            $this->getStyleDisplayFige('poursuivreNonFige', $this->_procedureEquivalence->getPoursuiteDateLimiteRemisePli());
            if ($this->_procedureEquivalence->getDelaiPoursuiteAffichage()) {
                $this->poursuivreAffichageNbJour->setSelectedValue($this->_procedureEquivalence->getDelaiPoursuiteAffichage().$this->_procedureEquivalence->getDelaiPoursuivreAffichageUnite());
            }

            $this->getActivateElement('chiffrementNon', $this->_procedureEquivalence->getCipherDisabled());
            $this->getStyleDisplayAffiche('chiffrementeNonAffiche', $this->_procedureEquivalence->getCipherDisabled());
            $this->getStyleDisplayFige('chiffrementeNonFige', $this->_procedureEquivalence->getCipherDisabled());

            $this->getActivateElement('modeOuvertureDossier', $this->_procedureEquivalence->getModeOuvertureDossier());
            $this->getStyleDisplayAffiche('modeOuvertureDossierAffiche', $this->_procedureEquivalence->getModeOuvertureDossier());
            $this->getStyleDisplayFige('modeOuvertureDossierFige', $this->_procedureEquivalence->getModeOuvertureDossier());

            $this->getActivateElement('modeOuvertureReponse', $this->_procedureEquivalence->getModeOuvertureReponse());
            $this->getStyleDisplayAffiche('modeOuvertureReponseAffiche', $this->_procedureEquivalence->getModeOuvertureReponse());
            $this->getStyleDisplayFige('modeOuvertureReponseFige', $this->_procedureEquivalence->getModeOuvertureReponse());
            // Enveloppe de condidature
            $this->getActivateElement('envelopCandidature', $this->_procedureEquivalence->getEnvCandidature());
            $this->getStyleDisplayAffiche('envelopCandidatureAffiche', $this->_procedureEquivalence->getEnvCandidature());
            $this->getStyleDisplayFige('envelopCandidatureFige', $this->_procedureEquivalence->getEnvCandidature());
            if (
                '+1' === $this->_procedureEquivalence->getEnvCandidature()
                || '1' === $this->_procedureEquivalence->getEnvCandidature()
                || '-1' === $this->_procedureEquivalence->getEnvCandidature()
            ) {
                $scriptJs .= "document.getElementById('panel_dossier_candidature').className= 'content';";
            } else {
                $scriptJs .= "document.getElementById('panel_dossier_candidature').className= 'content hidden';";
            }
            // Enveloppe d'offre
            $this->getActivateElement('envelopOffre', $this->_procedureEquivalence->getEnvOffre());
            $this->getStyleDisplayAffiche('envelopOffreAffiche', $this->_procedureEquivalence->getEnvOffre());
            $this->getStyleDisplayFige('envelopOffreFige', $this->_procedureEquivalence->getEnvOffre());
            if (
                '+1' === $this->_procedureEquivalence->getEnvOffre()
                || '1' === $this->_procedureEquivalence->getEnvOffre()
                | '-1' === $this->_procedureEquivalence->getEnvOffre()
            ) {
                $scriptJs .= "document.getElementById('panel_dossier_offre').className= 'content';";
            } else {
                $scriptJs .= "document.getElementById('panel_dossier_offre').className= 'content hidden';";
            }

            // Constitution des dossiers de réponse
            // Sans allotissement
            //$this->getActivateElement('envelopUnique', $this->_procedureEquivalence->getEnvOffreTypeUnique2());
            //$this->getStyleDisplayAffiche('envelopUniqueAffiche', $this->_procedureEquivalence->getEnvOffreTypeUnique2());
            //$this->getStyleDisplayFige('envelopUniqueFige', $this->_procedureEquivalence->getEnvOffreTypeUnique2());

            //$this->getActivateElement('envelopParLot', $this->_procedureEquivalence->getEnvOffreTypeMultiple2());
            //$this->getStyleDisplayAffiche('envelopLotAffiche', $this->_procedureEquivalence->getEnvOffreTypeMultiple2());
            //$this->getStyleDisplayFige('envelopLotFige', $this->_procedureEquivalence->getEnvOffreTypeMultiple2());

            // Enveloppe d'offre technique
            $this->getActivateElement('envelopOffreTechnique', $this->_procedureEquivalence->getEnvOffreTechnique());
            $this->getStyleDisplayAffiche('envelopOffreTechniqueAffiche', $this->_procedureEquivalence->getEnvOffreTechnique());
            $this->getStyleDisplayFige('envelopOffreTechniqueFige', $this->_procedureEquivalence->getEnvOffreTechnique());
            if (
                '+1' === $this->_procedureEquivalence->getEnvOffreTechnique()
                || '1' === $this->_procedureEquivalence->getEnvOffreTechnique()
                || '-1' === $this->_procedureEquivalence->getEnvOffreTechnique()
            ) {
                $scriptJs .= "document.getElementById('panel_dossier_offreTech').className= 'content';";
            } else {
                $scriptJs .= "document.getElementById('panel_dossier_offreTech').className= 'content hidden';";
            }

            // Constitution des dossiers de réponse
            // Sans allotissement offre technique
            //$this->getActivateElement('envelopTechniqueUnique', $this->_procedureEquivalence->getEnvOffreTechniqueTypeUnique());
            //$this->getStyleDisplayAffiche('envelopUniqueAffiche', $this->_procedureEquivalence->getEnvOffreTypeUnique2());
            //$this->getStyleDisplayFige('envelopUniqueFige', $this->_procedureEquivalence->getEnvOffreTypeUnique2());

            //$this->getActivateElement('envelopTechniqueParLot', $this->_procedureEquivalence->getEnvOffreTechniqueTypeMultiple());
            //$this->getStyleDisplayAffiche('envelopLotAffiche', $this->_procedureEquivalence->getEnvOffreTypeMultiple2());
            //$this->getStyleDisplayFige('envelopLotFige', $this->_procedureEquivalence->getEnvOffreTypeMultiple2());

            $this->getActivateElement('anonymat', $this->_procedureEquivalence->getEnvAnonymat());
            $this->getStyleDisplayAffiche('envelopeAnonymatAffiche', $this->_procedureEquivalence->getEnvAnonymat());
            $this->getStyleDisplayFige('envelopeAnonymatFige', $this->_procedureEquivalence->getEnvAnonymat());
            if (
                '+1' === $this->_procedureEquivalence->getEnvAnonymat()
                || '1' === $this->_procedureEquivalence->getEnvAnonymat()
                || '-1' === $this->_procedureEquivalence->getEnvAnonymat()
            ) {
                $scriptJs .= "document.getElementById('panel_dossier_anonymat').className= 'content';";
            } else {
                $scriptJs .= "document.getElementById('panel_dossier_anonymat').className= 'content hidden';";
            }

            $this->getStyleDisplayAffiche('reglementAffiche', $this->_procedureEquivalence->getReglementCons());
            $this->getStyleDisplayFige('reglementFige', $this->_procedureEquivalence->getReglementCons());

            $this->getStyleDisplayAffiche('autrePieceAffiche', $this->_procedureEquivalence->getAutrePieceCons());
            $this->getStyleDisplayFige('autrePieceFige', $this->_procedureEquivalence->getAutrePieceCons());
            // Calendrier de la consultation

            $this->getActivateElement('miseEnLigne', $this->_procedureEquivalence->getMiseEnLigne1());
            $this->getStyleDisplayAffiche('dateMiseEnLigneAffiche', $this->_procedureEquivalence->getMiseEnLigne1());
            $this->getStyleDisplayFige('dateMiseEnLigneFige', $this->_procedureEquivalence->getMiseEnLigne1());

            $this->getActivateElement('validationDate', $this->_procedureEquivalence->getMiseEnLigne2());
            $this->getStyleDisplayAffiche('dateValidationAffiche', $this->_procedureEquivalence->getMiseEnLigne2());
            $this->getStyleDisplayFige('dateValidationFige', $this->_procedureEquivalence->getMiseEnLigne2());
            $this->getActivateElement('dateEnvoiBOAMP', $this->_procedureEquivalence->getMiseEnLigne3());
            $this->getStyleDisplayAffiche('dateEnvoiBoampAffiche', $this->_procedureEquivalence->getMiseEnLigne3());
            $this->getStyleDisplayFige('dateEnvoiBoampFige', $this->_procedureEquivalence->getMiseEnLigne3());

            $this->getActivateElement('datePubBOAMP', $this->_procedureEquivalence->getMiseEnLigne4());
            $this->getStyleDisplayAffiche('datePublicationBoampAffiche', $this->_procedureEquivalence->getMiseEnLigne4());
            $this->getStyleDisplayFige('datePublicationBoampFige', $this->_procedureEquivalence->getMiseEnLigne4());

            $this->getActivateElement('miseEnLigneParEntiteCoordinatrice', $this->_procedureEquivalence->getMiseEnLigneEntiteCoordinatrice());
            $this->getStyleDisplayAffiche('datemiseEnLigneParEntiteCoordinatriceAffiche', $this->_procedureEquivalence->getMiseEnLigneEntiteCoordinatrice());
            $this->getStyleDisplayFige('datemiseEnLigneParEntiteCoordinatriceFige', $this->_procedureEquivalence->getMiseEnLigneEntiteCoordinatrice());

            if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                if ('1' == $this->_procedureEquivalence->getAfficherCodeCpv()) {
                    $this->afficherCodeCPVOui->setChecked(true);
                } else {
                    $this->afficherCodeCPVNon->setChecked(true);
                    if (Atexo_Module::isEnabled('CodeCpv')) {
                        $this->codeCPVObligatoireNon->enabled = false;
                        $this->codeCPVObligatoireOui->enabled = false;
                    }
                }
                if (Atexo_Module::isEnabled('CodeCpv')) {
                    if ($this->_procedureEquivalence->getAfficherCodeCpv() && '1' == $this->_procedureEquivalence->getCodeCpvObligatoire()) {
                        $this->codeCPVObligatoireOui->setChecked(true);
                    } else {
                        $this->codeCPVObligatoireNon->setChecked(true);
                    }
                }
            }
            if (Atexo_Module::isEnabled('MarchePublicSimplifie')) {
                // Marche public simplifie
                $this->getActivateElement('marchePublicSimplifie', $this->_procedureEquivalence->getMarchePublicSimplifie());
                $this->getStyleDisplayAffiche('afficheMPS', $this->_procedureEquivalence->getMarchePublicSimplifie());
                $this->getStyleDisplayFige('figeMPS', $this->_procedureEquivalence->getMarchePublicSimplifie());
            }
            if (Atexo_Module::isEnabled('InterfaceDume')) {
                $this->getActivateElement('dumeDemande', $this->_procedureEquivalence->getDumeDemande());
                $this->getStyleDisplayAffiche('afficheDUME', $this->_procedureEquivalence->getDumeDemande());
                $this->getStyleDisplayFige('figeDUME', $this->_procedureEquivalence->getDumeDemande());

                $this->getActivateElement('formulaireDumeStandard', $this->_procedureEquivalence->getTypeFormulaireDumeStandard());
                $this->getStyleDisplayAffiche('formulaireDumeStandardAffiche', $this->_procedureEquivalence->getTypeFormulaireDumeStandard());
                $this->getStyleDisplayFige('formulaireDumeStandardFige', $this->_procedureEquivalence->getTypeFormulaireDumeStandard());

                $this->getActivateElement('formulaireDumeSimplifie', $this->_procedureEquivalence->getTypeFormulaireDumeSimplifie());
                $this->getStyleDisplayAffiche('formulaireDumeSimplifieAffiche', $this->_procedureEquivalence->getTypeFormulaireDumeSimplifie());
                $this->getStyleDisplayFige('formulaireDumeSimplifieFige', $this->_procedureEquivalence->getTypeFormulaireDumeSimplifie());

            }

            //Données complémentaire REDAC
            if (Atexo_Module::isEnabled('DonneesRedac')) {
                $this->getActivateElement('donneesComplementaireNon', $this->_procedureEquivalence->getDonneesComplementaireNon());
                $this->getStyleDisplayAffiche('donneesComplementaireNonAffiche', $this->_procedureEquivalence->getDonneesComplementaireNon());
                $this->getStyleDisplayFige('donneesComplementaireNonFige', $this->_procedureEquivalence->getDonneesComplementaireNon());
                $this->getActivateElement('donneesComplementaireOui', $this->_procedureEquivalence->getDonneesComplementaireOui());
                $this->getStyleDisplayAffiche('donneesComplementaireOuiAffiche', $this->_procedureEquivalence->getDonneesComplementaireOui());
                $this->getStyleDisplayFige('donneesComplementaireOuiFige', $this->_procedureEquivalence->getDonneesComplementaireOui());
            }

            if (Atexo_Module::isEnabled('Publicite')) {
                $this->getActivateElement('autoriserPubliciteNon', $this->_procedureEquivalence->getAutoriserPubliciteNon());
                $this->getStyleDisplayAffiche('autoriserPubliciteNonAffiche', $this->_procedureEquivalence->getAutoriserPubliciteNon());
                $this->getStyleDisplayFige('autoriserPubliciteNonFige', $this->_procedureEquivalence->getAutoriserPubliciteNon());
                $this->getActivateElement('autoriserPubliciteOui', $this->_procedureEquivalence->getAutoriserPubliciteOui());
                $this->getStyleDisplayAffiche('autoriserPubliciteOuiAffiche', $this->_procedureEquivalence->getAutoriserPubliciteOui());
                $this->getStyleDisplayFige('autoriserPubliciteOuiFige', $this->_procedureEquivalence->getAutoriserPubliciteOui());
            }

            if (Atexo_Module::isEnabled('ModuleEnvol')) {
                $this->getActivateElement('annexesTechniqueDceNo', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolNon());
                $this->getStyleDisplayAffiche('annexesTechniqueDceNoAffiche', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolNon());
                $this->getStyleDisplayFige('annexesTechniqueDceNoFige', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolNon());

                $this->getActivateElement('annexesTechniqueDceYes', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolOui());
                $this->getStyleDisplayAffiche('annexesTechniqueDceYesAffiche', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolOui());
                $this->getStyleDisplayFige('annexesTechniqueDceYesFige', $this->_procedureEquivalence->getAutorisationDossiersVolumineuxEnvolOui());
            }

            $this->scriptJs->Text = "<script>
             J( document ).ready(function() {
               $scriptJs
            });
            </script>";

            // Procédure simplifié
            $this->_procedureTypeOrganisme = CommonTypeProcedureOrganismePeer::retrieveByPk($idProcedureType, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
            if ($this->_procedureTypeOrganisme && '1' !== $this->_procedureTypeOrganisme->getMapa()) {
                $this->blocProcedureSimplifie->setDisplay(TDisplayStyle::None); // On affiche le bloc que si on est en MAPA
            } else {
                if (true === $this->_procedureTypeOrganisme->getProcedureSimplifie()) {
                    $this->procedureSimplifieOui->setChecked(true);
                    $this->procedureSimplifieNon->setChecked(false);
                } else {
                    $this->procedureSimplifieOui->setChecked(false);
                    $this->procedureSimplifieNon->setChecked(true);
                }
                $this->changeProcedureSimplifie();
            }
         if (Atexo_Module::isEnabled('InterfaceDume')) {
             $this->chargerTypeProcedureDume($this->_procedureEquivalence,$idProcedureType);
         }
        }
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $this->chargerTypeProcedureDume($this->_procedureEquivalence,$idProcedureType);
        }
    }

    /**
     * Active ou désactive un element.
     *
     * @param string $element l'id de l'element
     * @param string $value   la valeur de la procédure équivalente
     */
    public function getActivateElement($element, $value)
    {
        $value = trim((string) $value);

        if ((!strnatcasecmp($value, '1')) || (!strnatcasecmp($value, '+1')) || (!strnatcasecmp($value, '-1'))) {
            $this->$element->setChecked(true);
        } elseif ((!strnatcasecmp($value, '0')) || (!strnatcasecmp($value, '+0')) || (!strnatcasecmp($value, '-0'))) {
            $this->$element->setChecked(false);
        }
    }

    /**
     * Active ou désactive un chech box de type "Affiché".
     *
     * @param string $elementOption l'id de l'element
     * @param string $value         valeur de la procedure équivalente
     */
    public function getStyleDisplayAffiche($element, $value)
    {
        $value = trim((string) $value);
        if ((!strnatcasecmp($value, '1')) || (!strnatcasecmp($value, '+1')) || (!strnatcasecmp($value, '0')) || (!strnatcasecmp($value, '+0'))) {
            // Element Affiché
            $this->$element->setChecked(true);
        } else {   // Element non Affiché
            $this->$element->setChecked(false);
        }
    }

    /**
     * Active ou désactive un chech box de type "Figé".
     *
     * @param string $elementOption l'id de l'element
     * @param string $value         valeur de la procedure équivalente
     */
    public function getStyleDisplayFige($element, $value)
    {
        $value = trim((string) $value);

        if ((!strnatcasecmp($value, '+1')) || (!strnatcasecmp($value, '+0')) || (!strnatcasecmp($value, '-0')) || (!strnatcasecmp($value, '-1')) || (!strnatcasecmp($value, ''))) {   // Element non figé
            $this->$element->setChecked(false);
        } else {
            // Element Figé
            $this->$element->setChecked(true);
        }
    }

    // Enrgistre les paramètres d'une procédure
    public function save($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $this->_procedureEquivalence = CommonProcedureEquivalencePeer::retrieveByPk($this->procedureType->getSelectedValue(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $this->_selectedProcedureType = $this->procedureType->getSelectedValue();
        if (!isset($this->_procedureEquivalence)) {
            $this->_procedureEquivalence = new CommonProcedureEquivalence();
            $this->_procedureEquivalence->setIdTypeProcedure($this->_selectedProcedureType);
            $this->_procedureEquivalence->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        }

        // Identification de la consultation : Allotissement
        $this->_procedureEquivalence->setEnvOffreTypeUnique($this->getStateOfElement($this->marcheUniqueAffiche->checked, $this->marcheUniqueFige->checked, $this->marcheUnique->checked));
        $this->_procedureEquivalence->setEnvOffreTypeMultiple($this->getStateOfElement($this->allotiAffiche->checked, $this->allotiFige->checked, $this->alloti->checked));
        // Calendrier de la consultation
        $this->_procedureEquivalence->setMiseEnLigne1($this->getStateOfElement($this->dateMiseEnLigneAffiche->checked, $this->dateMiseEnLigneFige->checked, $this->miseEnLigne->checked));
        $this->_procedureEquivalence->setMiseEnLigne2($this->getStateOfElement($this->dateValidationAffiche->checked, $this->dateValidationFige->checked, $this->validationDate->checked));
        $this->_procedureEquivalence->setMiseEnLigne3($this->getStateOfElement($this->dateEnvoiBoampAffiche->checked, $this->dateEnvoiBoampFige->checked, $this->dateEnvoiBOAMP->checked));
        $this->_procedureEquivalence->setMiseEnLigne4($this->getStateOfElement($this->datePublicationBoampAffiche->checked, $this->datePublicationBoampFige->checked, $this->datePubBOAMP->checked));
        if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
            $this->_procedureEquivalence->setMiseEnLigneEntiteCoordinatrice($this->getStateOfElement($this->datemiseEnLigneParEntiteCoordinatriceAffiche->checked, $this->datemiseEnLigneParEntiteCoordinatriceFige->checked, $this->miseEnLigneParEntiteCoordinatrice->checked));
        } else {
            $this->_procedureEquivalence->setMiseEnLigneEntiteCoordinatrice($this->getStateOfElement(false, false, false));
        }

        if (Atexo_Module::isEnabled('MarchePublicSimplifie')) {
            $this->_procedureEquivalence->setMarchePublicSimplifie($this->getStateOfElement($this->afficheMPS->checked, $this->figeMPS->checked, $this->marchePublicSimplifie->checked));
        }
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            if ($this->afficherCodeCPVOui->checked) {
                $this->_procedureEquivalence->setAfficherCodeCpv('1');
            } else {
                $this->_procedureEquivalence->setAfficherCodeCpv('0');
            }
            if (Atexo_Module::isEnabled('CodeCpv')) {
                if ($this->afficherCodeCPVOui->checked && $this->codeCPVObligatoireOui->checked) {
                    $this->_procedureEquivalence->setCodeCpvObligatoire('1');
                } else {
                    $this->_procedureEquivalence->setCodeCpvObligatoire('0');
                }
            }
        }
        // Téléchargement partiel du DCE
        $this->_procedureEquivalence->setPartialDceDownload($this->getStateOfElement($this->telechargmementPartielAffiche->checked, $this->telechargmementPartielFige->checked, $this->telechargementPartiel->checked));
        $this->_procedureEquivalence->setProcedurePublicite($this->getStateOfElement($this->accesPublicAffiche->checked, $this->accesPublictFige->checked, $this->accesPublic->checked));
        $this->_procedureEquivalence->setProcedureRestreinte($this->getStateOfElement($this->accesRestreintAffiche->checked, $this->accesRestreintFige->checked, $this->accesRestreint->checked));
        $this->_procedureEquivalence->setGestionEnvoisPostaux($this->getStateOfElement($this->envoisPostauxPrevuAffiche->checked, $this->envoisPostauxPrevuFige->checked, $this->envoisPostaux->checked));
        $this->_procedureEquivalence->setConstitutionDossierReponse($this->getStateOfElement($this->formDocAffiche->checked, $this->formDocFige->checked, $this->envoiType->checked));
        $this->_procedureEquivalence->setTireurPlanPapier($this->getStateOfElement($this->formatPapierAffiche->checked, $this->formatPapierFige->checked, $this->papier->checked));
        $this->_procedureEquivalence->setTireurPlanCdrom($this->getStateOfElement($this->formatCdRomAffiche->checked, $this->formatCdRomFige->checked, $this->cdRom->checked));
        // Réponse électronique
        $this->_procedureEquivalence->setElecResp($this->getStateOfElement($this->autoriseAffiche->checked, $this->autoriseFige->checked, $this->repAutorisee->checked));
        $this->_procedureEquivalence->setRepObligatoire($this->getStateOfElement($this->obligOuiAffiche->checked, $this->obligOuiFige->checked, $this->obligatoireOui->checked));
        //$this->_procedureEquivalence->setNoRepObligatoire($this->getStateOfElement($this->obligNonAffiche->checked,$this->obligNonFige->checked,$this->obligatoireNon->checked));
        //$this->_procedureEquivalence->setRespElecAutrePlateforme($this->getStateOfElement($this->autoriseAutreAffiche->checked,$this->autoriseAutreFige->checked,$this->repElecAutreAutorisee->checked));
        $this->_procedureEquivalence->setDelaiDateLimiteRemisePli($this->getStateOfElement($this->poursuivreAffiche->checked, $this->poursuivreFige->checked, $this->poursuivreAffichage->checked));
        $this->_procedureEquivalence->setPoursuiteDateLimiteRemisePli($this->getStateOfElement($this->poursuivreNonAffiche->checked, $this->poursuivreNonFige->checked, $this->poursuivreAffichageNon->checked));
        //$this->_procedureEquivalence->setDelaiPoursuiteAffichage($this->poursuivreAffichageNbJour->getSelectedValue());
        $this->_procedureEquivalence->setDelaiPoursuiteAffichage((int) $this->poursuivreAffichageNbJour->getSelectedValue());
        $this->_procedureEquivalence->setDelaiPoursuivreAffichageUnite(preg_replace('#[0-9]#', '', $this->poursuivreAffichageNbJour->getSelectedValue()));

        $this->_procedureEquivalence->setNoElecResp($this->getStateOfElement($this->refuseAffiche->checked, $this->refuseFige->checked, $this->repRefusee->checked));
        //  Caractéristiques des réponses électroniques
        $this->_procedureEquivalence->setSignatureEnabled($this->getStateOfElement($this->signatureElectroniqueOuiAffiche->checked, $this->signatureElectroniqueOuiFige->checked, $this->signatureElectroniqueOui->checked));
        $this->_procedureEquivalence->setSignaturePropre($this->getStateOfElement($this->signatureePropreAffiche->checked, $this->signatureePropreFige->checked, $this->signaturePropre->checked));
        $this->_procedureEquivalence->setAnnexeFinanciere($this->getStateOfElement($this->annexeFinanciereAffiche->checked, $this->annexeFinanciereFige->checked, $this->annexeFinanciere->checked));

        $this->_procedureEquivalence->setSignatureDisabled($this->getStateOfElement($this->signatureElectroniqueNonAffiche->checked, $this->signatureElectroniqueNonFige->checked, $this->signatureElectroniqueNon->checked));

        $this->_procedureEquivalence->setSignatureAutoriser(
            $this->getStateOfElement(
                $this->signatureElectroniqueAutoriserAffiche->checked,
                $this->signatureElectroniqueAutoriserFige->checked,
                $this->signatureElectroniqueAutoriser->checked
            )
        );

        // Chiffrement des enveloppes électroniques
        $this->_procedureEquivalence->setCipherEnabled($this->getStateOfElement($this->chiffrementeOuiAffiche->checked, $this->chiffrementeOuiFige->checked, $this->chiffrementeOui->checked));
        $this->_procedureEquivalence->setCipherDisabled($this->getStateOfElement($this->chiffrementeNonAffiche->checked, $this->chiffrementeNonFige->checked, $this->chiffrementNon->checked));

        $this->_procedureEquivalence->setModeOuvertureReponse($this->getStateOfElement(
            $this->modeOuvertureReponseAffiche->checked,
            $this->modeOuvertureReponseFige->checked,
            $this->modeOuvertureReponse->checked
        ));

        $this->_procedureEquivalence->setModeOuvertureDossier($this->getStateOfElement(
            $this->modeOuvertureDossierAffiche->checked,
            $this->modeOuvertureDossierFige->checked,
            $this->modeOuvertureDossier->checked
        ));

        // Enveloppe de condidature
        $this->_procedureEquivalence->setEnvCandidature($this->getStateOfElement($this->envelopCandidatureAffiche->checked, $this->envelopCandidatureFige->checked, $this->envelopCandidature->checked));
        // Enveloppe d'offre Technique
        $this->_procedureEquivalence->setEnvOffreTechnique($this->getStateOfElement($this->envelopOffreTechniqueAffiche->checked, $this->envelopOffreTechniqueFige->checked, $this->envelopOffreTechnique->checked));
        // Constitution des dossiers de réponse
        // Sans allotissement
        //$this->_procedureEquivalence->setEnvOffreTechniqueTypeUnique($this->envelopTechniqueUnique->checked);
        //$this->_procedureEquivalence->setEnvOffreTechniqueTypeMultiple($this->envelopTechniqueParLot->checked);

        // Enveloppe d'offre
        $this->_procedureEquivalence->setEnvOffre($this->getStateOfElement($this->envelopOffreAffiche->checked, $this->envelopOffreFige->checked, $this->envelopOffre->checked));

        //reglement consultation
        $this->_procedureEquivalence->setReglementCons($this->getStateOfElement($this->reglementAffiche->checked, $this->reglementFige->checked, null));

        //Autre pieces consultation
        $this->_procedureEquivalence->setAutrePieceCons($this->getStateOfElement($this->autrePieceAffiche->checked, $this->autrePieceFige->checked, null));

        // Constitution des dossiers de réponse
        // Sans allotissement
        //$this->_procedureEquivalence->setEnvOffreTypeUnique2($this->envelopUnique->checked);
        //$this->_procedureEquivalence->setEnvOffreTypeMultiple2($this->envelopParLot->checked);
        $this->_procedureEquivalence->setEnvAnonymat($this->getStateOfElement($this->envelopeAnonymatAffiche->checked, $this->envelopeAnonymatFige->checked, $this->anonymat->checked));
        $connexionCom->beginTransaction();
        $erreurDume = $this->saveProcedureEquivalenceDume($this->_procedureEquivalence, $connexionCom);

        if (Atexo_Module::isEnabled('DonneesRedac')) {
            $this->_procedureEquivalence->setDonneesComplementaireNon(
                $this->getStateOfElement(
                    $this->donneesComplementaireNonAffiche->checked,
                    $this->donneesComplementaireNonFige->checked,
                    $this->donneesComplementaireNon->checked
                )
            );
            $this->_procedureEquivalence->setDonneesComplementaireOui(
                $this->getStateOfElement(
                    $this->donneesComplementaireOuiAffiche->checked,
                    $this->donneesComplementaireOuiFige->checked,
                    $this->donneesComplementaireOui->checked
                )
            );
        }

        if (Atexo_Module::isEnabled('Publicite')) {
            $this->_procedureEquivalence->setAutoriserPubliciteNon(
                $this->getStateOfElement(
                    $this->autoriserPubliciteNonAffiche->checked,
                    $this->autoriserPubliciteNonFige->checked,
                    $this->autoriserPubliciteNon->checked
                )
            );
            $this->_procedureEquivalence->setAutoriserPubliciteOui(
                $this->getStateOfElement(
                    $this->autoriserPubliciteOuiAffiche->checked,
                    $this->autoriserPubliciteOuiFige->checked,
                    $this->autoriserPubliciteOui->checked
                )
            );
        }

        if (Atexo_Module::isEnabled('ModuleEnvol')) {
            $this->_procedureEquivalence->setAutorisationDossiersVolumineuxEnvolNon(
                $this->getStateOfElement(
                    $this->annexesTechniqueDceNoAffiche->checked,
                    $this->annexesTechniqueDceNoFige->checked,
                    $this->annexesTechniqueDceNo->checked
                )
            );
            $this->_procedureEquivalence->setAutorisationDossiersVolumineuxEnvolOui(
                $this->getStateOfElement(
                    $this->annexesTechniqueDceYesAffiche->checked,
                    $this->annexesTechniqueDceYesFige->checked,
                    $this->annexesTechniqueDceYes->checked
                )
            );
        }

        $procedureSimplifie = $this->procedureSimplifieOui->getChecked();
        if ($procedureSimplifie || (!$procedureSimplifie && !$erreurDume)) {
            // Procédure équivalence
            (new Atexo_Consultation_ProcedureEquivalence())->saveProcedureEquivalence($this->_procedureEquivalence, $connexionCom);
            // ProcédureTypeOrganisme (pour la procédure simplifié)
            $this->_procedureTypeOrganisme = CommonTypeProcedureOrganismePeer::retrieveByPk($this->procedureType->getSelectedValue(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
            $this->_procedureTypeOrganisme->setProcedureSimplifie($procedureSimplifie);
            $this->_procedureTypeOrganisme->save($connexionCom);
            (new Atexo_Consultation_ProcedureType())->reloadCacheTypeProcedureOrganisme();
        }

        $connexionCom->commit();
        //Enregister calendrier
        if (Atexo_Module::isEnabled('CalendrierDeLaConsultation') && $this->jsonFile->value) {
            (new Atexo_GestionCalendrier())->saveJsonReferentiel(base64_decode($this->jsonFile->value), $this->_selectedProcedureType, Atexo_CurrentUser::getOrganismAcronym());
        }

        if (!$procedureSimplifie && $erreurDume) {
            $this->messageErreurDume->setVisible(true);
            $this->messageErreurDume->focus();
        } else {
            $this->messageErreurDume->setVisible(false);
            $this->messageConfirmation->visible = true;
            $this->messageConfirmation->focus();
            $this->panelParametrageTypeProcedure->Visible = false;
        }
    }

    public function retour($sender, $param)
    {
        $this->messageConfirmation->focus();
        $this->panelChoixTypeProcedure->Visible = true;
        $this->panelParametrageTypeProcedure->Visible = false;
        $this->messageConfirmation->Visible = false;
        $this->messageErreurDume->Visible = false;
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
            $this->panelOffreTechnique->setDisplay('Dynamic');
            $this->panelAffichageOffreTechnique->setDisplay('Dynamic');
        //$this->panelSpacerOffreTechnique->setDisplay("Dynamic");
        } else {
            $this->panelOffreTechnique->setDisplay('None');
            $this->panelAffichageOffreTechnique->setDisplay('None');
        }
        if (!Atexo_Module::isEnabled('EnveloppeAnonymat')) {
            $this->panelAnonymat->setDisplay('None');
            $this->panelAffichageAnonymat->setDisplay('None');
        }
        if (!Atexo_Module::isEnabled('PubliciteFormatXml')) {
            $this->panelSpacerDateEnvoiBOAMP->setDisplay('None');
            $this->panelDateEnvoiBOAMP->setDisplay('None');
            $this->panelSpacerdatePubBOAMP->setDisplay('None');
            $this->paneldatePubBOAMP->setDisplay('None');
            $this->panelAffichageDatesBOAMP->setDisplay('None');
        }

        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->panelDateEnvoiBOAMP->setEnabled(false);
            $this->panelAffichageDatesEnvoiBOAMP->setEnabled(false);
        }
    }

    public function fillCalender($idTypeProcedure, $org)
    {
        if (Atexo_Module::isEnabled('CalendrierDeLaConsultation')) {
            $this->scriptJs->Text .= "<script>
			element = document.getElementById('ctl0_CONTENU_PAGE_organisme');
			element.value = '".$org."'
			element = document.getElementById('ctl0_CONTENU_PAGE_IdTypeProcedure');
			element.value = '".$idTypeProcedure."';
			afficheCalendrier();</script>";
        }
    }

    /**
     * Permet de modifier les modalite de reponse selon l'autorisation du marche public simplifie.
     *
     * @param $sender , $param
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function changeModaliteReponse($sender, $param)
    {
        if ($this->marchePublicSimplifie->checked) {
            // Réponse électronique
            $this->getActivateElement('repRefusee', '0');
            $this->getStyleDisplayAffiche('refuseAffiche', '+1');
            $this->getStyleDisplayFige('refuseFige', '0');

            $this->getActivateElement('repAutorisee', '0');
            $this->getStyleDisplayAffiche('autoriseAffiche', '+1');
            $this->getStyleDisplayFige('autoriseFige', '0');

            $this->getActivateElement('obligatoireOui', '1');
            $this->getStyleDisplayAffiche('obligOuiAffiche', '+1');
            $this->getStyleDisplayFige('obligOuiFige', '0');

            //  Caractéristiques des réponses électroniques
            $this->getActivateElement('signatureElectroniqueOui', '0');
            $this->getStyleDisplayAffiche('signatureElectroniqueOuiAffiche', '-0');
            $this->getStyleDisplayFige('signatureElectroniqueOuiFige', '+1');

            $this->getActivateElement('signatureElectroniqueNon', '1');
            $this->getStyleDisplayAffiche('signatureElectroniqueNonAffiche', '+1');
            $this->getStyleDisplayFige('signatureElectroniqueNonFige', '0');
        } else {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $procedureEquivalence = CommonProcedureEquivalencePeer::retrieveByPk($this->procedureType->getSelectedValue(), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
            if ($procedureEquivalence instanceof CommonProcedureEquivalence) {
                // Réponse électronique
                $this->getActivateElement('repRefusee', $procedureEquivalence->getNoElecResp());
                $this->getStyleDisplayAffiche('refuseAffiche', $procedureEquivalence->getNoElecResp());
                $this->getStyleDisplayFige('refuseFige', $procedureEquivalence->getNoElecResp());

                $this->getActivateElement('repAutorisee', $procedureEquivalence->getElecResp());
                $this->getStyleDisplayAffiche('autoriseAffiche', $procedureEquivalence->getElecResp());
                $this->getStyleDisplayFige('autoriseFige', $procedureEquivalence->getElecResp());

                $this->getActivateElement('obligatoireOui', $procedureEquivalence->getRepObligatoire());
                $this->getStyleDisplayAffiche('obligOuiAffiche', $procedureEquivalence->getRepObligatoire());
                $this->getStyleDisplayFige('obligOuiFige', $procedureEquivalence->getRepObligatoire());

                //  Caractéristiques des réponses électroniques
                $this->getActivateElement('signatureElectroniqueOui', $procedureEquivalence->getSignatureEnabled());
                $this->getStyleDisplayAffiche('signatureElectroniqueOuiAffiche', $procedureEquivalence->getSignatureEnabled());
                $this->getStyleDisplayFige('signatureElectroniqueOuiFige', $procedureEquivalence->getSignatureEnabled());

                $this->getActivateElement('signatureElectroniqueNon', $procedureEquivalence->getSignatureDisabled());
                $this->getStyleDisplayAffiche('signatureElectroniqueNonAffiche', $procedureEquivalence->getSignatureDisabled());
                $this->getStyleDisplayFige('signatureElectroniqueNonFige', $procedureEquivalence->getSignatureDisabled());
            }
        }
    }

    /**
     * Permet de charger type procedure dume.
     *
     * @param CommonProcedureEquivalence $procedureEquivalence
     * @param $idTypeProcedure
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     */
    public function chargerTypeProcedureDume($procedureEquivalence,$idTypeProcedure)
    {
        if ($procedureEquivalence instanceof CommonProcedureEquivalence) {
            $this->getStyleDisplayAffiche('typeProcedureDumeAffiche', $procedureEquivalence->getTypeProcedureDume());
        }
            $typesProcedureDume = (new Atexo_Consultation_ProcedureEquivalence())->getProcedureAndParametrageDume($idTypeProcedure, Atexo_CurrentUser::getCurrentOrganism());
        $this->typeProcedureDumeRepeater->setDataSource($typesProcedureDume);
        $this->typeProcedureDumeRepeater->DataBind();

            $this->procedureEquivalenceDumeRepeater->setDataSource($typesProcedureDume);
            $this->procedureEquivalenceDumeRepeater->DataBind();
    }

    /**
     * Permet d'enregistrer procedure equivalence dume.
     *
     * @param CommonProcedureEquivalence $procedureEquivalence
     * @param PDOConnection              $connexion
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     */
    public function saveProcedureEquivalenceDume($procedureEquivalence, $connexion)
    {
        if (Atexo_Module::isEnabled('InterfaceDume') && $procedureEquivalence instanceof CommonProcedureEquivalence) {
            $typeDumeSelected = 0;
            $typeDumeDisplayed = 0;
            $erreurDume = 0;
            $radioCheck = 0;

            $procedureEquivalence->setDumeDemande($this->getStateOfElement($this->afficheDUME->checked, $this->figeDUME->checked, $this->dumeDemande->checked));
            $procedureEquivalence->setTypeFormulaireDumeStandard($this->getStateOfElement($this->formulaireDumeStandardAffiche->checked, $this->formulaireDumeStandardFige->checked, $this->formulaireDumeStandard->checked));
            $procedureEquivalence->setTypeFormulaireDumeSimplifie($this->getStateOfElement($this->formulaireDumeSimplifieAffiche->checked, $this->formulaireDumeSimplifieFige->checked, $this->formulaireDumeSimplifie->checked));

            $procedureEquivalence->setTypeProcedureDume($this->getStateOfElement($this->typeProcedureDumeAffiche->checked, 0, 0));
            $arrayProcedureDume = [];

            foreach ($this->typeProcedureDumeRepeater->getItems() as $item) {
                $procedureEquivalenceDume = CommonTProcedureEquivalenceDumeQuery::create()
                                            ->filterByOrganisme($procedureEquivalence->getOrganisme())
                                            ->filterByIdTypeProcedure($procedureEquivalence->getIdTypeProcedure())
                                            ->filterByIdTypeProcedureDume($item->idTypeProcedureDume->value)
                                            ->findOne($connexion);
                if (!$procedureEquivalenceDume instanceof CommonTProcedureEquivalenceDume) {
                    $procedureEquivalenceDume = new CommonTProcedureEquivalenceDume();
                    $procedureEquivalenceDume->setIdTypeProcedure($procedureEquivalence->getIdTypeProcedure());
                    $procedureEquivalenceDume->setOrganisme($procedureEquivalence->getOrganisme());
                    $procedureEquivalenceDume->setIdTypeProcedureDume($item->idTypeProcedureDume->value);
                }

                if ($item->typeProcedureDume->checked) {
                    $typeDumeSelected = 1;
                    $radioCheck = $item->idTypeProcedureDume->value;
                }

                $procedureEquivalenceDume->setSelectionner(($item->typeProcedureDume->checked) ? 1 : 0);
                $arrayProcedureDume[$item->idTypeProcedureDume->value] = $procedureEquivalenceDume;
            }

            foreach ($this->procedureEquivalenceDumeRepeater->getItems() as $item) {
                $procedureEquivalenceDume = $arrayProcedureDume[$item->idTypeProcedureDume->value];
                if ($procedureEquivalenceDume instanceof CommonTProcedureEquivalenceDume) {
                    if ($item->typeProcedureDumeAffiche->checked) {
                        $typeDumeDisplayed = 1;
                    }
                }
            }

            if (0 == $typeDumeDisplayed || !$this->typeProcedureDumeAffiche->checked) {
                $typeDumeDisplayed = 0;
            }

            if ((
                    !$this->dumeDemande->checked
                    &&
                      !$this->afficheDUME->checked
                    &&
                      $typeDumeDisplayed
                ) ||
                (
                    (
                        $this->dumeDemande->checked ||
                        $this->afficheDUME->checked
                    ) &&
                    (
                        0 == $typeDumeSelected &&
                        0 == $typeDumeDisplayed
                    )
                )
            ) {
                $erreurDume = 1;
            } else {
                foreach ($this->procedureEquivalenceDumeRepeater->getItems() as $item) {
                    $procedureEquivalenceDume = $arrayProcedureDume[$item->idTypeProcedureDume->value];

                    if ($procedureEquivalenceDume instanceof CommonTProcedureEquivalenceDume) {
                        if ($item->idTypeProcedureDume->value == $radioCheck) {
                            $procedureEquivalenceDume->setAfficher(1);
                            $procedureEquivalenceDume->setFiger(($item->typeProcedureDumeFige->checked) ? 1 : 0);
                        } else {
                            $procedureEquivalenceDume->setAfficher(($item->typeProcedureDumeAffiche->checked) ? 1 : 0);
                            $procedureEquivalenceDume->setFiger(($item->typeProcedureDumeFige->checked) ? 1 : 0);
                        }

                        $procedureEquivalenceDume->save($connexion);
                    }
                }
            }

            return $erreurDume;
        }
    }

    /**
     * TODO.
     */
    public function afficherCodeCPVChanged()
    {
        if (Atexo_Module::isEnabled('AffichageCodeCpv')
            && Atexo_Module::isEnabled('CodeCpv')
            && (
                TDisplayStyle::None === $this->blocProcedureSimplifie->getDisplay()
                || (TDisplayStyle::None !== $this->blocProcedureSimplifie && $this->procedureSimplifieNon->checked)
            )
        ) {
            if ($this->afficherCodeCPVOui->checked) {
                $this->codeCPVObligatoireOui->enabled = true;
                $this->codeCPVObligatoireNon->enabled = true;
            } else {
                $this->codeCPVObligatoireNon->setChecked(true);
                $this->codeCPVObligatoireOui->enabled = false;
                $this->codeCPVObligatoireNon->enabled = false;
            }
        }
    }

    /**
     * Charge les valeurs par defaut si procédure simplifié est choisie.
     */
    public function changeProcedureSimplifie()
    {
        $defaultValues = [
            'marcheUnique' => true,
            'marcheUniqueAffiche' => false,
            'marcheUniqueFige' => false,
            'alloti' => false,
            'allotiAffiche' => false,
            'allotiFige' => false,
            'codeCPVObligatoireOui' => false,
            'codeCPVObligatoireNon' => true,
            'afficherCodeCPVOui' => false,
            'afficherCodeCPVNon' => true,
            'dumeDemande' => false,
            'afficheDUME' => false,
            'figeDUME' => false,
            'formulaireDumeStandard' => false,
            'formulaireDumeStandardAffiche' => false,
            'formulaireDumeStandardFige' => false,
            'formulaireDumeSimplifie' => true,
            'formulaireDumeSimplifieAffiche' => false,
            'formulaireDumeSimplifieFige' => false,
            'typeProcedureDumeAffiche' => false,
            'donneesComplementaireNon' => true,
            'donneesComplementaireNonAffiche' => false,
            'donneesComplementaireNonFige' => false,
            'donneesComplementaireOui' => false,
            'donneesComplementaireOuiAffiche' => false,
            'donneesComplementaireOuiFige' => false,
            'miseEnLigne' => false,
            'dateMiseEnLigneAffiche' => false,
            'dateMiseEnLigneFige' => false,
            'validationDate' => true,
            'dateValidationAffiche' => false,
            'dateValidationFige' => false,
            'dateEnvoiBOAMP' => false,
            'dateEnvoiBoampAffiche' => false,
            'dateEnvoiBoampFige' => false,
            'datePubBOAMP' => false,
            'datePublicationBoampAffiche' => false,
            'datePublicationBoampFige' => false,
            'reglementAffiche' => false,
            'reglementFige' => false,
            'telechargementPartiel' => true,
            'telechargmementPartielAffiche' => false,
            'telechargmementPartielFige' => false,
            'autrePieceAffiche' => false,
            'autrePieceFige' => false,
            'envoisPostaux' => true,
            'envoisPostauxPrevuAffiche' => false,
            'envoisPostauxPrevuFige' => false,
            'envoiType' => false,
            'formDocAffiche' => false,
            'formDocFige' => false,
            'papier' => false,
            'formatPapierAffiche' => false,
            'formatPapierFige' => false,
            'cdRom' => false,
            'formatCdRomAffiche' => false,
            'formatCdRomFige' => false,
            'repRefusee' => false,
            'refuseAffiche' => false,
            'refuseFige' => false,
            'repAutorisee' => true,
            'autoriseAffiche' => false,
            'autoriseFige' => false,
            'obligatoireOui' => false,
            'obligOuiAffiche' => false,
            'obligOuiFige' => false,
            'poursuivreAffichage' => false,
            'poursuivreAffiche' => false,
            'poursuivreFige' => false,
            'poursuivreAffichageNon' => true,
            'poursuivreNonAffiche' => false,
            'poursuivreNonFige' => false,
            'signatureElectroniqueNon' => true,
            'signatureElectroniqueNonAffiche' => false,
            'signatureElectroniqueNonFige' => false,
            'signatureElectroniqueOui' => false,
            'signatureElectroniqueOuiAffiche' => false,
            'signatureElectroniqueOuiFige' => false,
            'signatureElectroniqueAutoriser' => false,
            'signatureElectroniqueAutoriserAffiche' => false,
            'signatureElectroniqueAutoriserFige' => false,
            'modeOuvertureDossier' => true,
            'modeOuvertureDossierAffiche' => false,
            'modeOuvertureDossierFige' => false,
            'modeOuvertureReponse' => false,
            'modeOuvertureReponseAffiche' => false,
            'modeOuvertureReponseFige' => false,
            'chiffrementNon' => true,
            'chiffrementeNonAffiche' => false,
            'chiffrementeNonFige' => false,
            'chiffrementeOui' => false,
            'chiffrementeOuiAffiche' => false,
            'chiffrementeOuiFige' => false,
            'envelopCandidature' => false,
            'envelopCandidatureAffiche' => false,
            'envelopCandidatureFige' => false,
            'envelopOffre' => true,
            'envelopOffreAffiche' => false,
            'envelopOffreFige' => false,
            'anonymat' => false,
            'envelopeAnonymatAffiche' => false,
            'envelopeAnonymatFige' => false,
            'autoriserPubliciteNon' => true,
            'autoriserPubliciteNonAffiche' => false,
            'autoriserPubliciteNonFige' => false,
            'autoriserPubliciteOui' => false,
            'autoriserPubliciteOuiAffiche' => false,
            'autoriserPubliciteOuiFige' => false,
        ];

        // On charge les valeurs par défaut et on rend non modifiable
        if (true === $this->procedureSimplifieOui->getChecked()) {
            foreach ($defaultValues as $field => $value) {
                $this->$field->setChecked($value);
                $this->$field->setEnabled(false);
            }
            // Cas particulier pour "Type de procédure DUME"
            foreach ($this->typeProcedureDumeRepeater->getItems() as $item) {
                if (1 == $item->idTypeProcedureDume->value) {
                    $item->typeProcedureDume->setChecked(true);
                } else {
                    $item->typeProcedureDume->setChecked(false);
                }
                $item->typeProcedureDume->setEnabled(false);
            }
            foreach ($this->procedureEquivalenceDumeRepeater->getItems() as $item) {
                $item->typeProcedureDumeAffiche->setChecked(false);
                $item->typeProcedureDumeAffiche->setEnabled(false);
                $item->typeProcedureDumeFige->setChecked(false);
                $item->typeProcedureDumeFige->setEnabled(false);
            }
            // Cas particulier pour "Accès aux informations / DCE par les entreprises"
            if (null === $this->_procedureEquivalence ) {
                $idProcedureType = $this->procedureType->getSelectedValue();
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $this->_procedureEquivalence = CommonProcedureEquivalencePeer::retrieveByPk($idProcedureType, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
            }
            if ($this->_procedureEquivalence->getProcedureRestreinte() == '') {
                $this->accesRestreint->setChecked(true);
            }

            // Cas particulier pour "Accès à la consultation pour les entreprises"
            $this->poursuivreAffichageNbJour->setEnabled(false);
            // Cas particulier pour "Enveloppe d'offre"
            if ($this->envelopOffre->getChecked()) {
                $this->scriptJsProcedureSimplifie->Text = "<script>
                    J( document ).ready(function() {
                        document.getElementById('panel_dossier_offre').className= 'content';
                    });
                </script>";
                $this->signaturePropre->setChecked(false);
                $this->signaturePropre->setEnabled(false);
                $this->signatureePropreAffiche->setChecked(false);
                $this->signatureePropreAffiche->setEnabled(false);
                $this->signatureePropreFige->setChecked(false);
                $this->signatureePropreFige->setEnabled(false);

                $this->annexeFinanciere->setChecked(false);
                $this->annexeFinanciere->setEnabled(false);
                $this->annexeFinanciereAffiche->setChecked(false);
                $this->annexeFinanciereAffiche->setEnabled(false);
                $this->annexeFinanciereFige->setChecked(false);
                $this->annexeFinanciereFige->setEnabled(false);
            } else {
                $this->scriptJsProcedureSimplifie->Text = "<script>
                    J( document ).ready(function() {
                        document.getElementById('panel_dossier_offre').className= 'content hidden';
                    });
                </script>";
            }
        } else {
            foreach ($defaultValues as $field => $value) {
                $this->$field->setEnabled(true);
            }
            // Cas particulier pour "Type de procédure DUME"
            foreach ($this->typeProcedureDumeRepeater->getItems() as $item) {
                $item->typeProcedureDume->setEnabled(true);
            }
            foreach ($this->procedureEquivalenceDumeRepeater->getItems() as $item) {
                $item->typeProcedureDumeAffiche->setEnabled(true);
                $item->typeProcedureDumeFige->setEnabled(true);
            }
            // Cas particulier pour "Accès à la consultation pour les entreprises"
            $this->poursuivreAffichageNbJour->setEnabled(true);
            // Cas particulier pour "Enveloppe d'offre"
            if ($this->envelopOffre->getChecked()) {
                $this->scriptJsProcedureSimplifie->Text = "<script>
                    J( document ).ready(function() {
                        document.getElementById('panel_dossier_offre').className= 'content';
                    });
                </script>";
                $this->signaturePropre->setEnabled(true);
                $this->signatureePropreAffiche->setEnabled(true);
                $this->signatureePropreFige->setEnabled(true);
                $this->annexeFinanciere->setEnabled(true);
                $this->annexeFinanciereAffiche->setEnabled(true);
                $this->annexeFinanciereFige->setEnabled(true);
            } else {
                $this->scriptJsProcedureSimplifie->Text = "<script>
                    J( document ).ready(function() {
                        document.getElementById('panel_dossier_offre').className= 'content hidden';
                    });
                </script>";
            }
        }
    }
}
