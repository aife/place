<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchiveTableauDeBord extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->ArchiveResultSearch->scriptJsStatutsArchives->Text = '';
        if (!$this->IsPostBack) {
            $launchSearch = true;
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteriaVo->setFromArchive(true);
            $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_APRES_DECISION'));

            if (isset($_GET['keyWord'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->ArchiveTableauBord->setVisible(true);
                $keyWord = Atexo_Util::atexoHtmlEntities($_GET['keyWord']);
                $criteriaVo->setKeyWordRechercheRapide($keyWord);
            } elseif (isset($_GET['id'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->ArchiveTableauBord->setVisible(true);
                $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            } else {
                $criteriaVo->setTypeSearch('tout');
                if (isset($_GET['AS']) && '1' == $_GET['AS']) {
                    $launchSearch = false;
                    $this->rechercheAvancee->setVisible(true);
                    $this->ArchiveTableauBord->setVisible(false);
                } else {
                    $this->rechercheAvancee->setVisible(false);
                    $this->ArchiveTableauBord->setVisible(true);
                }
            }
            if ($launchSearch) {
                $this->setViewState('CriteriaVo', $criteriaVo);
                $this->dataForSearchResult($criteriaVo);
            }
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->ArchiveTableauBord->setVisible(true);
        $this->ArchiveResultSearch->fillRepeaterWithDataForArchiveSearchResult($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo', new Atexo_Consultation_CriteriaVo());
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                $this->firstDiv->setCssClass('tab-on');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab');
                               //$nouveauStatutArchive='';
                               $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_APRES_DECISION');
                               $this->afficherBoutonsPassageAArchiverTelechargerDossiers();
                break;
            case 'secondTab':
                $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab-on');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab');
                               $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_JUSTE_DECISION');
                               $this->afficherBoutonPasserAArchiver();
                break;
            case 'thirdTab':
                $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab-on');
                               $this->fourthDiv->setCssClass('tab');
                               $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_A_ARCHIVER');
                               $this->afficherBoutonTelechargerDossiersSelectionnes();
                break;
            case 'fourthTab':
                $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab-on');
                               $nouveauStatutArchive = Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE');
                               $this->masquerBoutonsPassageAArchiverTelechargerDossiers();
                break;
            default:
                $nouveauStatutArchive = '';
                break;
        }

        $this->Page->ArchiveResultSearch->panelArchivage->setVisible(true);
        if ($nouveauStatutArchive == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
            $this->Page->ArchiveResultSearch->panelArchivage->setVisible(false);
        }
        $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutArchive);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    /**
     * Fonctione dse tri du tableau de bord.
     */
    public function Trier($sender, $param)
    {
        $this->ArchiveResultSearch->Trier($param->CommandName);
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    /**
     * Permet d'afficher le bouton qui permet de passer au statut "A Archiver".
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function afficherBoutonPasserAArchiver()
    {
        $script = "<script>
         				document.getElementById('boutonPasserAArchiver').style.display = 'block';
         				document.getElementById('boutonTelechargementDossiersSelectionnes').style.display = 'none';
         			</script>";
        $this->scriptJs->Text = $script;
    }

    /**
     * Permet d'afficher le bouton qui permet de stocker les dossiers selectionnes pour le telechargement asynchrone.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function afficherBoutonTelechargerDossiersSelectionnes()
    {
        $script = "<script>
         				document.getElementById('boutonTelechargementDossiersSelectionnes').style.display = 'block';
         				document.getElementById('boutonPasserAArchiver').style.display = 'none';
         			</script>";
        $this->scriptJs->Text = $script;
    }

    /**
     * Permet d'afficher les boutons "Telecharger les dossiers selectionnes" et "Passer au statut A Archiver".
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function afficherBoutonsPassageAArchiverTelechargerDossiers()
    {
        $script = "<script>
         				document.getElementById('boutonTelechargementDossiersSelectionnes').style.display = 'block';
         				document.getElementById('boutonPasserAArchiver').style.display = 'block';
         			</script>";
        $this->scriptJs->Text = $script;
    }

    /**
     * Permet de masquer les boutons "Telecharger les dossiers selectionnes" et "Passer au statut A Archiver".
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function masquerBoutonsPassageAArchiverTelechargerDossiers()
    {
        $script = "<script>
         				document.getElementById('boutonTelechargementDossiersSelectionnes').style.display = 'none';
         				document.getElementById('boutonPasserAArchiver').style.display = 'none';
         			</script>";
        $this->scriptJs->Text = $script;
    }
}
