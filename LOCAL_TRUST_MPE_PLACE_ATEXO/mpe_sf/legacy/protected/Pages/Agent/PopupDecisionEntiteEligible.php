<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonInvitePermanentContratQuery;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Exceptions\THttpException;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author mouslim mitali <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpDecisionEntiteEligible extends MpeTPage
{
    private ?array $_listeEntiteInvite = null;
    protected $typeEntiteInvite;
    // protected $ChoixOrganisme;
    /**
     * @var mixed|string
     */
    protected $codeRandom;
    protected ?string $base64 = null;
    protected ?string $selecte = null;

    protected $entitesEligibles;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->myEntityPurchese->Text = null;
        $this->typeEntiteInvite = 0;
        if (isset($_GET['idEntite']) && isset($_GET['isMyEntity'])) {
            $isMyEntity = $_GET['isMyEntity'];
            $this->labelUpdate->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_decisionEntitesEligibles_labelUpdate').value='".$isMyEntity."'</script>";
            $idEntite = $_GET['idEntite'];
            $tabidEntite = explode(',', $idEntite);
            $this->_listeEntiteInvite = $tabidEntite;
            $this->typeEntiteInvite = $isMyEntity;
        }

        if (Atexo_CurrentUser::getOrganismDesignation()) {
            $this->myEntityPurchese->Text = Atexo_CurrentUser::getOrganismDesignation();
        }

        $organismeUser = Atexo_CurrentUser::getCurrentOrganism();
        $string = $organismeUser.'_0_'.$_GET['id'];
        $this->base64 = base64_encode($string);
        $this->codeRandom = $this->generateRandomString();
        $this->selecte = '';
        $commonInviteTransverse = (new CommonInvitePermanentContratQuery())->getSelectedServiceForContrat(base64_encode($_GET['id']));

        foreach ($commonInviteTransverse as $invite) {
            if ($organismeUser == $invite['acronyme'] &&
                null == $invite['serviceId'] &&
                $_GET['id'] == $invite['agentId']) {
                $this->selecte = "checked='checked'";
            }
        }

        if (in_array('0', explode(',', $_GET['idEntite']))) {
            $this->selecte = "checked='checked'";
        }

        if (!$this->IsPostBack
            || ($this->IsPostBack && true == $this->invitePermanent->checked && empty($this->entitesEligibles))) {
            $this->displayOrganismes();
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $this->displayInfosOrganisme($organisme);
            $this->TreeServices->Text = self::displayEntitiesTree($organisme);
        }
    }

    /**
     * Liste des Entités éligibles aux marchés issus de cette première phase.
     */
    public function displayOrganismes()
    {
        $this->repeaterEntitePublique->DataSource = Atexo_Organismes::retrieveOrganismes(true);
        $this->repeaterEntitePublique->DataBind();
    }

    /**
     * Choix des Entités éligibles aux marchés issus de cette première phase.
     */
    public function checkedEntity()
    {
        $checkedEntity = '';
        foreach ($this->repeaterEntitePublique->getItems() as $item) {
            if (true == $item->entite->Checked) {
                $checkedEntity .= $item->idEntite->getValue().', ';
            }
        }

        return $checkedEntity;
    }

    /**
     * selectionner LES Entités eligibles.
     */
    public function selectedEntiteEligible($sender, $param)
    {
        $this->entitesEligibles = '';
        $valueRadioBox = '';
        if (true == $this->monEntiteAchat->checked) {
            $valueRadioBox = 0;
            $this->entitesEligibles = '_##_';
        } elseif ($this->invitePermanent->checked) {
            $valueRadioBox = 2;
            $this->entitesEligibles = $this->listServicesChecked();
        } else {
            $valueRadioBox = 1;
            $this->entitesEligibles = $this->checkedEntity();
        }

        $this->scriptClose->Text = "<script>selectedEntiteEligible('".$this->entitesEligibles."', '".$valueRadioBox."');</script>";
        //Atexo_CurrentUser::deleteFromSession('idsOrg');
    }

    /**
     * cocher les entités déjà invitées.
     */
    public function existingEntity($acronymeOrg)
    {
        if ($this->_listeEntiteInvite) {
            foreach ($this->_listeEntiteInvite as $oneEntity) {
                if ($oneEntity == $acronymeOrg && 1 == Atexo_Util::atexoHtmlEntities($_GET['isMyEntity'])) {
                    return true;
                }
            }

            return false;
        }
    }

    /**
     * Cocher et .... Current Organisme.
     */
    public function isCurrentOrganisme($acronymeOrg)
    {
        return $acronymeOrg == Atexo_CurrentUser::getCurrentOrganism();
    }

    /**
     * @param $organisme
     */
    public function displayInfosOrganisme($organisme)
    {
        (new Atexo_EntityPurchase())->reloadCachedEntities($organisme);
        $atexoOrganisme = new Atexo_Organismes();
        $organismeO = $atexoOrganisme->retrieveOrganismeByAcronyme($organisme);
        $this->sigleorganisme->Text = $organismeO->getSigle();
        $this->libelleorganisme->Text = $organismeO->getDenominationOrg();
    }

    /**
     * @param $organisme
     * @param null $organismeUser
     *
     * @return string
     */
    public function displayEntitiesTree($organisme, $organismeUser = null)
    {
        $id = null;
        $style = null;
        $allEntities = [];
        Atexo_EntityPurchase::getAllChilds(0, $allEntities, $organisme);
        $isEntitiesTreeSelected = isset($_GET['isMyEntity']) && 2 == $_GET['isMyEntity'];
        if ($isEntitiesTreeSelected) {
            $htmlStr = '<ul style="display:block" id=\'all-cats\'>';
        } else {
            $htmlStr = '<ul style="display:none" id=\'all-cats\'>';
        }
        $nbAllEntities = is_countable($allEntities) ? count($allEntities) : 0;
        for ($i = 0; $i < $nbAllEntities; ++$i) {
            $curService = $allEntities[$i];
            $curDepth = $curService->getDepth();
            $subServicesIds = Atexo_EntityPurchase::getSubServices($curService->getId(), $organisme, true);
            $selected = '';
            $isSubServiceSelected = false;
            $hasChildren = is_array($subServicesIds) && count($subServicesIds) > 1;

            if ($hasChildren && !empty(array_intersect($subServicesIds, array_map('intval', explode(',', $_GET['idEntite']))))) {
                $isSubServiceSelected = true;
            }

            if (in_array(
                (string) $curService->getId(),
                explode(',', Atexo_Util::atexoHtmlEntities($_GET['idEntite']))
            )
                && 2 == Atexo_Util::atexoHtmlEntities($_GET['isMyEntity'])) {
                $selected = "checked='checked'";
            }

            $idRandom = $this->generateRandomString();

            if ($i < $nbAllEntities - 1) { // Ce n'est pas le dernier élément
                $nextService = $allEntities[$i + 1];
                $nextDepth = $nextService->getDepth();

                $onClick = '';
                $picto = 'picto-tiret.gif';
                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $picto = 'picto-plus.gif';
                    $id = 'id=sous-cat_'.$i;
                    $style = 'style="display:none"';
                    $onClick = ' onclick="openSousCat(this,\'sous-cat_'.$i.'\')" ';
                    if ($isEntitiesTreeSelected && ("checked='checked'" == $selected || $isSubServiceSelected)) {
                        $picto = 'picto-moins.gif';
                        $style = 'style="display:block"';
                    }
                }

                $string = $organisme.'_'.$curService->getId().'_'.$_GET['id'];
                $base64 = base64_encode($string);

                $htmlStr .= '<li >
                    <img '.$onClick.' src="'
                    .$this->themesPath().'/images/'
                    .$picto.'" alt="'.Prado::localize('TEXT_OUVRIR_FERMER').'" title="'.Prado::localize('TEXT_OUVRIR_FERMER').'" />'
                    .'<input type="checkbox"  
                    data-selected="'.$base64.'"  
                    data-children="sous-cat_'.$i.'" 
                    id="'.$idRandom.'" 
                    name="service[]"
                    value="'.$base64.'"
                    class="checkOrg" '.$selected.'>'

                    .'<strong>'
                    .$curService->getSigle().'</strong>'.' - '.$curService->getLibelle().'';

                if ($curDepth < $nextDepth) { // l'élément a un fils
                    $htmlStr .= '<ul '.$id.' '.$style.'>';
                }

                if ($curDepth > $nextDepth) { // le prochain n'est ni un fils ni un parent (fin d'une branche)
                    $differenceDepth = $curDepth - $nextDepth;
                    for ($j = 0; $j < $differenceDepth; ++$j) {
                        $htmlStr .= '</li></ul>';
                    }
                }
                if ($curDepth == $nextDepth) { //le prochain est un frère, on ferme le li
                    $htmlStr .= '</li>';
                }
            } else { // C'est le dernier élément
                $string = $organisme.'_'.$curService->getId().'_'.$_GET['id'];
                $base64 = base64_encode($string);
                $htmlStr .= '<li><img src="'.$this->themesPath().'/images/picto-tiret.gif" alt="'.Prado::localize('TEXT_OUVRIR_FERMER').'" title="'.Prado::localize('TEXT_OUVRIR_FERMER').'" />'
                    .' <input type="checkbox" 
                    data-selected="'.$base64.'" data-children="sous-cat_'.$i.'" 
                    id="'.$idRandom.'" name="service[]" 
                    value="'.$base64.'" '.$selected.'>'
                    .'<strong>'
                    .$curService->getSigle()
                    .'</strong>'
                    .' - '
                    .$curService->getLibelle();
                $htmlStr .= '</li>';
                for ($j = 0; $j < ($curDepth); ++$j) {
                    $htmlStr .= "</ul>\n";
                }
            }
        }

        $htmlStr .= '';

        return $htmlStr;
    }

    public function onCallBackOrganismeSelected($sender, $param, $selectedOrganisme)
    {
        $organismeUser = Atexo_CurrentUser::getCurrentOrganism();
        $this->displayInfosOrganisme($selectedOrganisme);
        $this->TreeServices->Text = $this->displayEntitiesTree($selectedOrganisme, $organismeUser);
        $this->panelRefreshTreeServices->render($param->NewWriter);
    }

    /**
     * @param $commonTInviteTransverse
     * @param $commonOrganisme
     * @param $curService
     *
     * @return string
     */
    protected function selectedInviteTransverse($commonInviteTransverse, $organisme, $curService)
    {
        $selected = '';
        foreach ($commonInviteTransverse as $invite) {
            if ($organisme == $invite['acronyme'] &&
                $curService->getId() == $invite['serviceId'] &&
                $_GET['id'] == $invite['agentId']) {
                $selected = "checked='checked'";
            }
        }

        return $selected;
    }

    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    private function setData()
    {
        $this->clearInvitePermanentTransverse();
        foreach ($_POST['service'] as $post) {
            $data = explode('_', base64_decode($post));
            if (3 === count($data)) {
                $serviceId = null;
                if ('0' != $data[1]) {
                    $serviceId = $data[1];
                }
                $commonInvitePermanentTransverse = new CommonInvitePermanentTransverse();
                $commonInvitePermanentTransverse->setAgentId((int) $data[2]);
                $commonInvitePermanentTransverse->setAcronyme($data[0]);
                $commonInvitePermanentTransverse->setServiceId($serviceId);
                $commonInvitePermanentTransverse->save();
            } else {
                throw new THttpException(403, 'Donnees incompletes');
            }
        }
    }

    private function clearInvitePermanentTransverse()
    {
        $commonInvitePermanentTransverseQuery = new CommonInvitePermanentTransverseQuery();
        $invitePermanentTransverse = $commonInvitePermanentTransverseQuery->findByArray([
            'agentId' => $_GET['id'],
            'acronyme' => Atexo_CurrentUser::getCurrentOrganism(),
        ]);

        $invitePermanentTransverse->delete();
    }

    private function listServicesChecked()
    {
        $checkedEntity = '';
        $count = is_countable($_POST['service']) ? count($_POST['service']) : 0;
        $i = 1;
        foreach ($_POST['service'] as $post) {
            $data = explode('_', base64_decode($post));
            if (3 === count($data)) {
                $serviceId = null;
                $checkedEntity .= $data[1];
                if ($i < $count) {
                    $checkedEntity .= ', ';
                }
            } else {
                throw new THttpException(403, 'Donnees incompletes');
            }
            ++$i;
        }

        return $checkedEntity;
    }
}
