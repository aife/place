<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierQuery;
use Application\Propel\Mpe\CommonServiceMertier;
use Application\Propel\Mpe\CommonServiceMertierQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TiersAuthentification;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class SearchDCE extends MpeTPage
{
    /**
     * @author ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     */
    public function onInit($param)
    {


        if ((!Atexo_Module::isEnabled('BaseDce')) && (Atexo_Module::isEnabled('SocleInterne') || Atexo_Module::isEnabled('SocleExternePpp'))) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $service = CommonServiceMertierQuery::create()->filterByUrlAcces('%page=Agent.SearchDCE%', Criteria::LIKE)->findOne($connexion);
            $agentServiceMetier = null;
            if ($service instanceof CommonServiceMertier) {
                $agentServiceMetier = CommonAgentServiceMetierQuery::create()->filterByIdAgent(Atexo_CurrentUser::getId())
                                                                             ->filterByIdServiceMetier($service->getId())
                                                                             ->findOne($connexion);
            }
            if ($agentServiceMetier instanceof CommonAgentServiceMetier) {
                $this->Master->setCalledFrom('baseDce');
            } else {
                if (Atexo_Module::isEnabled('SocleExternePpp')) {
                    $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentHome');
                }
            }
        } else {
            $this->Master->setCalledFrom('agent');
        }
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $login = Atexo_Config::getParameter('LOGIN_PF_AUTHENTIFICATION_REST_BASE_DCE');
        $pwd = Atexo_Config::getParameter('PASSWORD_PF_AUTHENTIFICATION_REST_BASE_DCE');
        $ticket = (new Atexo_TiersAuthentification())->getTicketAuthentification($login, $pwd);
        $fonctionnalite = Atexo_Config::getParameter('ID_FONCTIONNALITE_BASE_DCE');
        if ($ticket) {
            if (isset($_GET['keyWord'])) {
                $url = Atexo_Config::getParameter('URL_BASE_DCE').'&keyWord='.Atexo_Util::atexoHtmlEntities($_GET['keyWord']).'&ticket='.$ticket.'&fonctionnalite='.$fonctionnalite.'&BaseDce=';
            } else {
                $url = Atexo_Config::getParameter('URL_BASE_DCE').'&ticket='.$ticket.'&fonctionnalite='.$fonctionnalite.'&BaseDce=';
            }
            $this->scriptJS->text = "<script>document.getElementById('iframeDce').src = '".$url."';</script>";
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TICKET_INVALIDE'));
        }
    }
}
