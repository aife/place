<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

class Disconnect extends MpeTPage
{
    public function onLoad($param)
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'agent/logout');
    }
}
