<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Prado\Prado;

/**
 * Class pour l'affichage du tableau de bord.
 *
 * @author Adil El Kanabi <adil.kanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauDeBordAnnonce extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $criteriaVo = null;
        $this->panelMessageErreur->setVisible(false);
        $this->panelErreur->setVisible(false);
        if (!$this->IsPostBack) {
            if ((
                isset($_GET['typeAnnonce'])
                 &&
                 (
                     $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')
                   || $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                   || $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV')
                   || $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT')
                   || $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION')
                   || $_GET['typeAnnonce'] == Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT')
                 )
            )
               || isset($_GET['AS'])
               || isset($_GET['idRecherche'])
            ) {
                $launchSearch = true;
                if (isset($_GET['idRecherche'])) {
                    $RechercheObj = (new Atexo_Entreprise_Recherches())->retreiveRecherchesByIdIdCreateur(Atexo_Util::atexoHtmlEntities($_GET['idRecherche']), Atexo_CurrentUser::getIdAgentConnected());
                    if ($RechercheObj) {
                        $criteriaVo = (new Atexo_Entreprise_Recherches())->getCriteriaFromObject($RechercheObj);
                    } else {
                        $this->response->redirect('?page=Agent.AgentHome');
                    }
                } else {
                    $criteriaVo = new Atexo_Consultation_CriteriaVo();
                    $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                    $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
                    if (isset($_GET['typeAnnonce'])) {
                        $criteriaVo->setIdTypeAvis(Atexo_Util::atexoHtmlEntities($_GET['typeAnnonce']));
                    }
                    if (isset($_GET['keyWord'])) {
                        $this->rechercheAvancee->setVisible(false);
                        $this->TableauDeBord->setVisible(true);
                        $keyWord = Atexo_Util::atexoHtmlEntities($_GET['keyWord']);
                        $criteriaVo->setKeyWordRechercheRapide($keyWord);
                    } elseif (isset($_GET['id'])) {
                        $this->rechercheAvancee->setVisible(false);
                        $this->TableauDeBord->setVisible(true);
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    } else {
                        if (isset($_GET['AS']) && '1' == $_GET['AS']) {
                            $launchSearch = false;
                            $this->rechercheAvancee->setVisible(true);
                            $this->TableauDeBord->setVisible(false);
                        } else {
                            $this->rechercheAvancee->setVisible(false);
                            $this->TableauDeBord->setVisible(true);
                        }
                    }
                }

                if ($launchSearch) {
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                }
            } else {
                $this->rechercheAvancee->setVisible(false);
                $this->TableauDeBord->setVisible(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TYPE_ANNONCE_INCONNU'));
            }
            $langueOblPublication = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
            //print_r($langueOblPublication);exit;
            if ((is_countable($langueOblPublication) ? count($langueOblPublication) : 0) && isset($_GET['creationSucces'])) {
                $listeLangues = "<ul class='default-list liste-langues'>";
                $autreLangObligExiste = false;
                foreach ($langueOblPublication as $oneLangue) {
                    if (Atexo_CurrentUser::readFromSession('lang') != $oneLangue->getLangue()) {
                        $autreLangObligExiste = true;
                        $urlTraduction = 'index.php?page=Agent.TraduireConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&langue='.$oneLangue->getLangue();
                        $listeLangues .= "<li> <a href='".$urlTraduction."' >".ucfirst(strtolower($oneLangue->getLibelle())).'</a></li>';
                    }
                }
                $listeLangues .= '</ul>';
                //$this->panelMessageAvertTraduction->setVisible($autreLangObligExiste);
                   //$this->panelMessageAvertTraduction->setMessage("<span>".Prado::localize('MSG_OBLIGATION_TRADUIRE_CONSULTATION', array("langues"=>$listeLangues))."</span>");
            }
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->TableauDeBord->setVisible(true);
        $this->ResultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo', new Atexo_Consultation_CriteriaVo());
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                 $this->firstDiv->setCssClass('tab-on');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab');
                               $nouveauStatutAnnonce = '';
                break;
            case 'secondTab':
                 $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab-on');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab');
                               $nouveauStatutAnnonce = Atexo_Config::getParameter('STATUS_PREPARATION');
                break;
            case 'thirdTab':
                 $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab-on');
                               $this->fourthDiv->setCssClass('tab');
                               $nouveauStatutAnnonce = Atexo_Config::getParameter('STATUS_CONSULTATION');
                break;
            case 'fourthTab':
                 $this->firstDiv->setCssClass('tab');
                               $this->secondDiv->setCssClass('tab');
                               $this->thirdDiv->setCssClass('tab');
                               $this->fourthDiv->setCssClass('tab-on');
                               $nouveauStatutAnnonce = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
                break;
            default:
                 $nouveauStatutAnnonce = '';
                break;
        }

        $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutAnnonce);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    /**
     * Fonctione dse tri du tableau de bord.
     */
    public function Trier($sender, $param)
    {
        $this->ResultSearch->Trier($param->CommandName);
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }
}
