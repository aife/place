<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCertificatsEntreprises;
use Application\Propel\Mpe\CommonCertificatsEntreprisesPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use AtexoCrypto\Dto\Certificat;
use Prado\Prado;

/**
 * Page de gestion des Certificats des Fournisseurs.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 20010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ResultCertificatsFournisseurs extends MpeTPage
{
    private string|\Application\Propel\Mpe\Entreprise|bool $_company = '';
    protected $critereTri = '';
    protected $triAscDesc = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ((!isset($_GET['pays']) && '' == $_GET['pays']) && (isset($_GET['idCompany']) && '' != $_GET['idCompany'])) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(Atexo_Util::atexoHtmlEntities($_GET['idCompany']));
        } elseif ((isset($_GET['idCompany']) && '' != $_GET['idCompany']) && (isset($_GET['pays']) && '' != $_GET['pays'])) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyByIdNational(Atexo_Util::atexoHtmlEntities($_GET['idCompany']), Atexo_Util::atexoHtmlEntities($_GET['pays']));
        }

        $this->panelMessageErreur->setVisible(false);

        if ($this->_company instanceof Entreprise) {
            $this->nomEntreprise->Text = $this->_company->getNom();
            if ($this->_company->getSiren()) {
                $this->rc->Text = $this->_company->getPaysenregistrement().' - '.$this->_company->getSiren();
                $this->intituleIdEntreprise->Text = prado::localize('REGISTRE_DU_COMMERCE');
            } else {
                $this->rc->Text = $this->_company->getPaysenregistrement().' - '.$this->_company->getSirenetranger();
                $this->intituleIdEntreprise->Text = prado::localize('TEXT_IDENTIFIANT_NATIONNAL');
            }
            if (!$this->IsPostBack) {
                $this->displyUtilisateurs();
            }
        } else {
            $this->panelNoCompany->setVisible(true);
            $this->panelUsers->setVisible(false);
            $this->legende->setVisible(false);
            $this->identificationCompany->setVisible(false);
        }
    }

    /**
     * affiche la liste des utilisateurs d'une entreprise.
     */
    public function displyUtilisateurs()
    {
        $dataSource = Atexo_Entreprise_Inscrit::retrieveInscritCertificatByEntrepriseId($this->_company->getId());
        //print_r($dataSource);exit;
        if ($dataSource) {
            $this->repeaterUsers->DataSource = $dataSource;
            $this->repeaterUsers->DataBind();
        }
    }

    /* Actualiser le tableau des utilisateurs dans le cas d'une nouvelle action
      *
      */
    public function refreshRepeater($sender, $param)
    {
        $this->displyUtilisateurs();
        $this->panelUsers->render($param->NewWriter);
    }

    /**
     * Trier le tableau des utlisateurs.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        if ('nom' == $param->CommandName) {
            $this->setViewState('sortByElement', 'nom');
            $this->setCritereAndTypeTri('nom');
            $dataSourceTriee = Atexo_Entreprise_Inscrit::retrieveInscritCertificatByEntrepriseId($this->_company->getId(), 'nom', $this->triAscDesc);
        } elseif ('email' == $param->CommandName) {
            $this->setViewState('sortByElement', 'email');
            $this->setCritereAndTypeTri('email');
            $dataSourceTriee = Atexo_Entreprise_Inscrit::retrieveInscritCertificatByEntrepriseId($this->_company->getId(), 'email', $this->triAscDesc);
        } elseif ('Certificat' == $param->CommandName) {
            $this->setViewState('sortByElement', 'Certificat');
            $this->setCritereAndTypeTri('date_fin');
            $dataSourceTriee = Atexo_Entreprise_Inscrit::retrieveInscritCertificatByEntrepriseId($this->_company->getId(), 'date_fin', $this->triAscDesc);
        } else {
            return;
        }

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        //  print_r($dataSourceTriee);exit;
        $this->repeaterUsers->DataSource = $dataSourceTriee;
        $this->repeaterUsers->DataBind();
    }

    public function onCallBackRefresh($sender, $param)
    {
        // Re-affichage du TActivePanel
        $this->repeaterUsers->render($param->getNewWriter());
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'DESC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function sortedWithNom()
    {
        if ('nom' == $this->getViewState('sortByElement')) {
            return 'on';
        } else {
            return '';
        }
    }

    public function sortedWithEmail()
    {
        if ('email' == $this->getViewState('sortByElement')) {
            return 'on';
        } else {
            return '';
        }
    }

    public function sortedWithCertificat()
    {
        if ('Certificat' == $this->getViewState('sortByElement')) {
            return 'on';
        } else {
            return '';
        }
    }

    /**
     * pour le GET et revenir en page de recherche.
     */
    public function getIdentifiantCompany()
    {
        if ($this->_company instanceof Entreprise) {
            if ($this->_company->getSiren()) {
                return $this->_company->getSiren();
            } else {
                return $this->_company->getSirenetranger().Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->_company->getPaysAdresse().'&etranger';
            }
        } else {
            return Atexo_Util::atexoHtmlEntities($_GET['rc']);
        }
    }

    // revoquation du certificat
    public function revokeCertificat($sender, $param)
    {
        $opensslConfigFile = Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_CONFIG');
        if (true or !is_file($opensslConfigFile)) {
            if (!Atexo_Crypto::generateCompanyPkiOpensslConfigFile()) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 1');
            }
        }
        $opensslDbFile = Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_DB');
        if (!is_file($opensslDbFile)) {
            if (!Atexo_Crypto::generateCompanyPkiOpensslDbFile()) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 2');
            }
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idCertif = $param->CommandParameter;
        $certificat = CommonCertificatsEntreprisesPeer::retrieveByPK($idCertif, $connexionCom);
        if ($certificat instanceof CommonCertificatsEntreprises) {
            $connexionCom->beginTransaction();

            // Revocation en base
            $certificat->setStatutRevoque(Atexo_Config::getParameter('STATUT_CERTIFICAT_REVOQUEE'));
            $certificat->setDateRevoquation(date('Y-m-d H:i:s'));
            $certificat->save($connexionCom);

            // revocation openssl
            // creation de la requete OPENSSL ca -revoke
            $tmpRevFilename = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'_rev.pem';
            $rev_fp = fopen($tmpRevFilename, 'w');
            if (!fwrite($rev_fp, $certificat->getCertificat())) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_INTERNE_ECRITURE_REQUETE'));
            }
            fclose($rev_fp);
            $revoke = Atexo_Config::getParameter('OPENSSL').' ca -config '.$opensslConfigFile.' -revoke  '.$tmpRevFilename;
            $commande = $revoke.' &> '.Atexo_Config::getParameter('COMMON_TMP').'revoke_cert';
            system($commande, $res);
            if (0 != $res && 1 != $res) { // $res=1 => ALREADY REVOKED
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 3');
                $connexionCom->rollback();
            }
            //@unlink($tmpRevFilename);

            $genCrl = Atexo_Config::getParameter('OPENSSL').' ca -gencrl -config '.$opensslConfigFile.' -out '.Atexo_Config::getParameter('PKI_ENTREPRISE_CRL');
            system($genCrl.' &> '.Atexo_Config::getParameter('COMMON_TMP').'generate_crl', $res);
            if (0 != $res) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_LORS_DE_GENERATION_DU_CRL'));
                $connexionCom->rollback();
            }
        }
        $connexionCom->commit();
        $this->displyUtilisateurs();
    }
}
