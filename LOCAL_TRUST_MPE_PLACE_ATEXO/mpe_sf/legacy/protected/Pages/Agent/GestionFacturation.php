<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdresseFacturationJalPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/*
 * Created on 25 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class GestionFacturation extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function getSelectedEntityPurchase()
    {
        return $this->_selectedEntity;
    }

    public function onLoad($param)
    {
        //self::replirRepeaterFacturation();
        if (!$this->IsPostBack) {
            if (Atexo_Module::isEnabled('GestionJalMesSousServices')) {
                $this->ChoixEntiteAchat->setVisible(true);
            } else {
                $this->ChoixEntiteAchat->setVisible(false);
            }
            $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
            $this->lienAjouterAdresseFacturation->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpAjoutAdresseFacturationJal&idService=".$this->getViewState('idService')."','yes');";
            $this->replirRepeaterFacturation($this->getViewState('idService'));
        }
    }

    public function replirRepeaterFacturation($idservice)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAdresseFacturationJalPeer::SERVICE_ID, $idservice, Criteria::EQUAL);
        $c->add(CommonAdresseFacturationJalPeer::ORGANISME, Atexo_CurrentUser::getOrganismAcronym(), Criteria::EQUAL);
        $adressesFacturation = CommonAdresseFacturationJalPeer::doSelect($c, $connexionCom);
        $this->RepeaterFacturation->DataSource = $adressesFacturation;
        $this->RepeaterFacturation->DataBind();
    }

    public function onDeleteClick($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idadresseFact = $param->CommandParameter;
        $adresseFacturation = CommonAdresseFacturationJalPeer::retrieveByPK($idadresseFact, Atexo_CurrentUser::getOrganismAcronym(), $connexionCom);
        CommonAdresseFacturationJalPeer::doDelete($adresseFacturation, $connexionCom);
        $this->replirRepeaterFacturation($this->getViewState('idService'));
    }

    public function refreshRepeaterFacturation($sender, $param)
    {
        self::replirRepeaterFacturation($this->getViewState('idService'));
        $this->ApRepeaterFacturation->render($param->NewWriter);
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouterAdresseFacturation->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpAjoutAdresseFacturationJal&idService=".$this->_selectedEntity."','yes');";
        $this->replirRepeaterFacturation($this->getViewState('idService'));
        $this->ApRepeaterFacturation->render($param->NewWriter);
    }
}
