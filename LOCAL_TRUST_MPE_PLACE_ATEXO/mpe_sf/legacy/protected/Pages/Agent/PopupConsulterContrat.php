<?php

namespace Application\Pages\Agent;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupConsulterContrat extends PopupDecisionContrat
{
    public function onLoad($param)
    {
        $this->accessContract();
        $this->attributaire->blocContratMulti->referentielCPV->setModeAffichage('lecture');
        $this->attributaire->referentielCPV->setModeAffichage('lecture');
        $this->idIdentificationEntreprise->atexoSoumissionnaire->updateContact->setVisible(false);
        parent::onLoad($param); // TODO: Change the autogenerated stub
        $this->buttonSave->setVisible(false);
        $this->buttonValidate->setVisible(false);
        $this->panelPopupDecisionContrat->setEnabled(false);
        $this->calendarDateNotification->setVisible(false);
        $this->calendarDateFin->setVisible(false);
        $this->calendarDateFinMax->setVisible(false);
        $this->attributaire->calendarDateNotification->setVisible(false);
        $this->attributaire->calendarDateFin->setVisible(false);
        $this->attributaire->calendarDateFinMax->setVisible(false);
        $this->decisionEntitesEligibles->urlChoixEntites->setVisible(false);
        $this->decisionEntitesEligibles->urlChoixEntitesNew->setVisible(false);
        $this->attributaire->calendarDateExecution->setVisible(false);
    }
}
