<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonDecisionLotQuery;
use Application\Propel\Mpe\CommonTContactContratQuery;
use Application\Propel\Mpe\CommonTDecisionSelectionEntrepriseQuery;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;

class ResetAnalyse extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['idDecision']) {
            $decisionLotQuery = CommonDecisionLotQuery::create()
                ->filterByIdDecisionLot($_GET['idDecision'])
                ->filterByOrganisme(Atexo_CurrentUser::getOrganismAcronym());

            $decisionLot = $decisionLotQuery->findOne();
            if (CommonDecisionLot::DECISION_ENTREPRISE_SELECTIONNEES === $decisionLot->getIdTypeDecision()) {
                $consultationId = $decisionLot->getConsultationId();
                $decisionSelectionEntrepriseQuery = CommonTDecisionSelectionEntrepriseQuery::create()
                    ->filterByConsultationId($consultationId);
                $decisionSelectionEntreprises = $decisionSelectionEntrepriseQuery->find();
                foreach ($decisionSelectionEntreprises as $decisionSelectionEntreprise) {
                    $idContactContrat = $decisionSelectionEntreprise->getIdContactContrat();
                    CommonTContactContratQuery::create()
                        ->filterByIdContactContrat($idContactContrat)
                        ->delete();
                }
                $decisionSelectionEntrepriseQuery->delete();
            }
            $decisionLotQuery->delete();
        }

        $this->Response->redirect($_SERVER['HTTP_REFERER']);
    }
}
