<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupRedacDR extends MpeTPage
{
    public $consultationId;
    public string $consultation = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
            $consultation = $this->retrieveConsultation($this->consultationId);
            $this->composantAjouter->_consultation = $consultation[0];
            if (!$this->isPostBack) {
                $this->composantAjouter->displayDocsRedac();
            }
        }
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }

    public function onValide($sender, $param)
    {
        $nameFile = null;
        $filePath = '';
        $infileReglement = '';
        $infoFileRG = '';
        /* Source */
        if ($this->composantAjouter->pieceRedac->checked) {
            foreach ($this->composantAjouter->repeaterDocsRedac->Items as $item) {
                if ($item->idPieceRedacRadio->checked) {
                    $infoFileRG = $item->idRedac->getValue().'|'.$item->nomRedac->getValue().'|'.'1';
                    $nameFile = $item->nomRedac->getValue();
                }
            }
        } else {
            if ($this->composantAjouter->pieceLibre->checked && $this->composantAjouter->fileExterne->HasFile) {
                if ('1' == Atexo_Config::getParameter('IS_COMMON_TMP_SHARED')) {
                    $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP_SHARED').'fileUploaded/'
                        .session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
                } else {
                    $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP').'fileUploaded/'
                        .session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
                }
                Atexo_Files::createDir($pathDirectoryTmpDceFile);
                $filePath = $pathDirectoryTmpDceFile;
                $infileReglement = $pathDirectoryTmpDceFile.'tmpReglementFile';
                if (!move_uploaded_file($this->composantAjouter->fileExterne->LocalName, $infileReglement)) {
                    return;
                }
                //$this->panelMessageErreur->setVisible(false);
                $msg = Atexo_ScanAntivirus::startScan($infileReglement);
                if ($msg) {
                    $this->afficherErreur($msg.' "'.$this->composantAjouter->fileExterne->getFileName().'"');

                    return;
                }
                $infoFileRG = $infileReglement.'|'.$this->composantAjouter->fileExterne->getFileName().'|'.'0';
                $nameFile = $this->composantAjouter->fileExterne->getFileName();
            }
        }
        /*
         *  les élement de la source sont séparés par des #
         *  les élements de même categorie sont separés par des ,
         */
        if ($_GET['response']) {
            $prefix = $this->getPrefix(Atexo_Util::atexoHtmlEntities($_GET['response']));
            $this->scriptJS->text = '<script>';
            $this->scriptJS->text .= $nameFile ? "getRGFileChange('".$nameFile."','".$prefix."');" : '';
            $this->scriptJS->text .= "opener.document.getElementById('".
                Atexo_Util::atexoHtmlEntities($_GET['response'])."').value ='".$infoFileRG.
                "';window.close();</script>";
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($msg);
    }

    public function getPrefix($parm)
    {
        $arrayElement = explode('_', $parm);
        array_pop($arrayElement);

        return implode('_', $arrayElement);
    }
}
