<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/*
 * Created on 26 juin 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class PopupVerificationCertifPeppol extends MpeTPage
{
    public $_infosStatuts;
    public ?string $_typeSignature = null;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $idFichierEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['idFichierEnveloppe']);
        $fiechierEnveloppe = CommonFichierEnveloppePeer::retrieveByPK($idFichierEnveloppe, $organisme, $connexionCom);
        $enveloppe = CommonEnveloppePeer::retrieveByPK($fiechierEnveloppe->getIdEnveloppe(), $organisme, $connexionCom);
        $offre = CommonOffresPeer::retrieveByPK($enveloppe->getOffreId(), $organisme, $connexionCom);
        $dateValidation = str_replace(' ', 'T', trim($offre->getUntrusteddate()));
        $refConsultation = $offre->getConsultationId();
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $criteriaVo->setcalledFromPortail(false);
        $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setAcronymeOrganisme($organisme);
        $criteriaVo->setIdReference($refConsultation);
        $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteriaVo);
        if ($consultation[0] instanceof CommonConsultation) {
            if ($consultation[0] instanceof CommonConsultation) {
                if ($offre instanceof CommonOffres) {
                    $xmlStr = $offre->getXmlString();
                    $pos = strpos($xmlStr, 'typeSignature');
                    if ($pos > 0) {
                        $this->_typeSignature = 'XML';
                    }
                }
                if ($fiechierEnveloppe instanceof CommonFichierEnveloppe) {
                    $signature = $fiechierEnveloppe->getSignatureFichier();
                    if ('XML' == $this->_typeSignature) {
                        $certif = (new Atexo_Crypto())->getCertificatFromSignature(base64_decode($signature), false, true, $this->_typeSignature);
                    } else {
                        $signature = "-----BEGIN PKCS7-----\n".$signature."\n-----END PKCS7-----";
                        $certif = (new Atexo_Crypto())->getCertificatFromSignature($signature);
                    }

                    $certifContents = utf8_encode(file_get_contents($certif));
                    $responseSoap = self::invoke($certifContents, $dateValidation);

                    if ($responseSoap) {
                        $infos = self::getInfosFromResponsePeppol($responseSoap);
                        if (!$infos) {
                            $msgErreur = self::getMsgErreurFromResponsePeppol($responseSoap);
                            $this->errorPart->setVisible(true);
                            $this->panelMessageErreur->setVisible(true);
                            $this->panelInfosCertif->setVisible(false);
                            $this->panelMessageErreur->setMessage(Prado::localize('VALIDATION_DU_CERTIFICAT_IMPOSSIBLE').' : </br>'.$msgErreur);
                            Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').'/logPeppol', "\n\n".Prado::localize('VALIDATION_DU_CERTIFICAT_IMPOSSIBLE').' : id Fichier Enveloppe = '.$_GET['idFichierEnveloppe'].' ---'.date('dmy_His')." \n".$responseSoap, 'a');
                        }
                        if ('ValidReason' != $infos['IssuerTrust']) {
                            $infos['RevocationStatus'] = 'IndeterminateReason';
                        }
                        $this->_infosStatuts = $infos;
                    } else {
                        $this->errorPart->setVisible(true);
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelInfosCertif->setVisible(false);
                        $this->panelMessageErreur->setMessage(Prado::localize('TEXT_PAS_DE_REPONSE_DU_PEPPOL'));
                        Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').'/logPeppol', Prado::localize('TEXT_PAS_DE_REPONSE_DU_PEPPOL')."\n\n", 'a');
                    }
                } else {
                    $this->response->redirect('agent');
                }
            } else {
                $this->response->redirect('agent');
            }
        }
    }

    /*
     * invoque le WS du concentrateur pour la verification de signature avec Peppol
     * retourne le contenu du fichier Xml reponse(String)
     */
    public function invoke($certifContents, $dateValidation)
    {
        $urlLocation = Atexo_Config::getParameter('URL_CONCENTRATEUR_WS_VERIFIER_CERTIF');
        $soapClient = new \SoapClient(
            null,
            [
                                            'location' => $urlLocation,
                                            'uri' => '',
                                            'trace' => 1,
                                            'exceptions' => 0,
            ]
        );
        $responseSoap = $soapClient->SendCertifToWsPeppol(base64_encode($certifContents), $dateValidation);

        return base64_decode($responseSoap);
    }

    public function getInfosFromResponsePeppol($responseSoap)
    {
        $infos = [];
        $xmlResponse = new \DOMDocument();
        $xmlResponse->loadXML($responseSoap);
        $element = $xmlResponse->getElementsByTagNameNS('*', 'Status')->item(0);
        //      $statutCert = explode("#",$element->getAttribute('StatusValue'));
        //      $infos['statutCertif'] = $statutCert[1];
        if ($element) {
            foreach ($element->childNodes as $child) {
                $name = explode(':', $child->nodeName);
                $etat = explode('#', $child->nodeValue);
                $infos[$etat[1]] = $name[1];
            }

            return $infos;
        }

        return false;
    }

    public function getMsgErreurFromResponsePeppol($responseSoap)
    {
        $msgErreur = '';
        $xmlResponse = new \DOMDocument();
        $xmlResponse->loadXML($responseSoap);
        $element = $xmlResponse->getElementsByTagNameNS('*', 'ErrorExtension')->item(0);
        if ($element) {
            foreach ($element->childNodes as $child) {
                if (strpos($child->nodeName, 'Detail')) {
                    $msgErreur = $child->nodeValue;
                }
            }
        }

        return $msgErreur;
    }

    public function getPictoStatut($c)
    {
        if ($this->_infosStatuts[$c]) {
            switch ($this->_infosStatuts[$c]) {
                case 'ValidReason':
                    return Atexo_Config::getParameter('OK');
                case 'InvalidReason':
                    return Atexo_Config::getParameter('NOK');
                case 'IndeterminateReason':
                    return Atexo_Config::getParameter('UNKNOWN');
            }
        }
    }
}
