<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTProfilJoue;
use Application\Propel\Mpe\CommonTProfilJoueQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Classe de gestion du profil JOUE.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @since MPE-4
 */
class MonProfilJoue extends MpeTPage
{
    private ?\Application\Propel\Mpe\CommonTProfilJoue $_profilJoue = null;

    /**
     * Récupère l'objet profil Joue.
     */
    private function getProfilJoue()
    {
        return $this->_profilJoue;
    }

    /**
     * Affectes l'objet profil Joue.
     *
     * @param CommonTProfilJoue $profilJoue
     */
    private function setProfilJoue($profilJoue)
    {
        $this->_profilJoue = $profilJoue;
    }

    /**
     * Permet de recuperer l'organisme.
     */
    private function getOrganisme()
    {
        return Atexo_CurrentUser::getCurrentOrganism();
    }

    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        $profilJoueQuery = new CommonTProfilJoueQuery(Atexo_Config::getParameter('COMMON_DB'));
        $collectionProfilJoue = $profilJoueQuery->recupererProfilJoueByIdAgentAndOrganisme($this->getIdAgent(), $this->getOrganisme());
        if ($collectionProfilJoue instanceof PropelObjectCollection && $collectionProfilJoue->getData()) {
            $profilsJoue = $collectionProfilJoue->getData();
            $this->setProfilJoue($profilsJoue[0]);
        } else {
            $this->InitialiserProfilJoue();
        }
        if (isset($_GET['init'])) {
            $this->InitialiserProfilJoue();
        }
        if (!$this->isPostBack) {
            $this->chargementInfosJoue();
        }
    }

    /**
     * Permet de recuperer l'id de l'agent.
     */
    public function getIdAgent()
    {
        return Atexo_CurrentUser::getIdAgentConnected();
    }

    /**
     * Permet de charger les infos JOUE de la page.
     */
    public function chargementInfosJoue()
    {
        $this->chargerListePays();
        $profilJoue = $this->getProfilJoue();
        if ($profilJoue instanceof CommonTProfilJoue) {
            $this->listePays->SelectedValue = $profilJoue->getPays();
            $this->nomOfficiel->Text = $profilJoue->getNomOfficiel();
            $this->localiteVille->Text = $profilJoue->getVille();
            $this->adresse->Text = $profilJoue->getAdresse();
            $this->codePostal->Text = $profilJoue->getCodePostal();
            $this->pointContact->Text = $profilJoue->getPointContact();
            $this->numeroNationalIdentification->Text = $profilJoue->getNumeroNationalIdentification();
            $this->telephone->Text = $profilJoue->getTelephone();
            $this->fax->Text = $profilJoue->getFax();
            $this->email->Text = $profilJoue->getEmail();
            $this->adressePouvoirAdj->Text = $profilJoue->getAdressePouvoirAdjudicateur();
            $this->adresseProfilAch->Text = $profilJoue->getAdresseProfilAcheteur();
            if ('1' == $profilJoue->getAutreTypePouvoirAdjudicateur()) {
                $this->autreLibellePouvoirAdj->Text = $profilJoue->getAutreLibelleTypePouvoirAdjudicateur();
            } else {
                $this->autreLibellePouvoirAdj->Text = '';
            }
            if ('1' == $profilJoue->getAutreActivitesPrincipales()) {
                $this->autreActivitesPrinc->Text = $profilJoue->getAutreLibelleActivitesPrincipales();
            } else {
                $this->autreActivitesPrinc->Text = '';
            }
            $this->scriptAffichagePrecisionAutre->Text = '';
            $this->chargerTypePouvoirAdjudicateur();
            $this->chargerActivitePrincipale();
            $this->chargerTypeOrganismeAcheteur();
        }
    }

    /**
     * Permet de charger la liste des pays.
     */
    public function chargerListePays()
    {
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListCountries(false);
        $dataSource = [];
        $dataSource['0'] = (new Atexo_Config())->toPfEncoding(Prado::localize('TEXT_SELECTIONNER').'...');
        if ($listCountries) {
            foreach ($listCountries as $oneCountrie) {
                $dataSource[$oneCountrie->getId()] = (new Atexo_Config())->toPfEncoding($oneCountrie->getDenomination1());
            }
        }
        $this->listePays->DataSource = $dataSource;
        $this->listePays->DataBind();
    }

    /**
     * Permet de charger le bloc "Type pouvoir adjudicateur".
     */
    public function chargerTypePouvoirAdjudicateur()
    {
        $this->chargerMinistereAutoriteNationale();
        $this->chargerOfficeNationale();
        $this->chargerOfficeRegionale();
        $this->chargerOrganismePublic();
        $this->chargerOrganisationEuropeenne();
        $this->chargerAutreTypePouvoirAdjudicateur();
        $this->chargerPouvoirAdjudicateurAgit();
    }

    /**
     * Permet de charger le  bloc "Le pouvoir adjudicateur agit pour le compte d'autres personnes".
     */
    public function chargerPouvoirAdjudicateurAgit()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getPouvoirAdjudicateurAgit()) {
            $this->pouvoirAdjAgitOui->checked = true;
        } elseif ('0' == $profilJoue->getPouvoirAdjudicateurAgit()) {
            $this->pouvoirAdjAgitNon->checked = true;
        }
    }

    /**
     * Permet de charger le bloc "Type d'organisme acheteur".
     */
    public function chargerTypeOrganismeAcheteur()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getPouvoirAdjudicateurMarcheCouvert()) {
            $this->pouvoirAdjMarcheCouvert->checked = true;
        }
        if ('1' == $profilJoue->getEntiteAdjudicatriceMarcheCouvert()) {
            $this->entiteAdjudicatriceMarcheCouvert->checked = true;
        }
    }

    /**
     * Permet de charger le bloc "Type de pouvoir adjudicateur - Autre, veuillez préciser".
     */
    public function chargerAutreTypePouvoirAdjudicateur()
    {
        $profilJoue = $this->getProfilJoue();
        $script = '<script>';
        if ('1' == $profilJoue->getAutreTypePouvoirAdjudicateur()) {
            $this->autrePouvoirAdj->checked = true;
            $script .= "document.getElementById('ctl0_CONTENU_PAGE_autreLibellePouvoirAdj').style.display = 'block';";
        } else {
            $script .= "document.getElementById('ctl0_CONTENU_PAGE_autreLibellePouvoirAdj').style.display = 'none';";
        }
        $script .= '</script>';
        $this->scriptAffichagePrecisionAutre->Text .= $script;
    }

    /**
     * Permet de charger le bloc "Institution/agence européenne ou organisation européenne".
     */
    public function chargerOrganisationEuropeenne()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getOrganisationEuropenne()) {
            $this->agenceEuropeenne->checked = true;
        }
    }

    /**
     * Permet de charger le bloc "Organisme de droit public".
     */
    public function chargerOrganismePublic()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getOrganismePublic()) {
            $this->organismePublic->checked = true;
        }
    }

    /**
     * Permet de charger le bloc "Agence/office régional(e) ou local(e)".
     */
    public function chargerOfficeRegionale()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getOfficeRegionale()) {
            $this->officeRegionale->checked = true;
        }
    }

    /**
     * Permet de charger le bloc "Agence/office national(e) ou fédéral(e)".
     */
    public function chargerOfficeNationale()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getOfficeNationale()) {
            $this->officeNationale->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Ministère ou toute autre autorité nationale ou fédérale,
     * y compris leurs subdivisions régionales ou locales".
     */
    public function chargerMinistereAutoriteNationale()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getAutoriteNationale()) {
            $this->autoriteNationale->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Services généraux des administrations publiques".
     */
    public function chargerServicesGeneraux()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getServicesGeneraux()) {
            $this->servicesGeneraux->checked = true;
        }
    }

    /**
     * Permet de charger les infos de la defense.
     */
    public function chargerDefense()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getDefense()) {
            $this->defense->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Ordre et sécurité publics".
     */
    public function chargerSecuritePublique()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getSecuritePublic()) {
            $this->securitePublic->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Environnement".
     */
    public function chargerEnvironnement()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getEnvironnement()) {
            $this->environnement->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Affaires économiques et financières".
     */
    public function chargerAffairesEconomiques()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getAffairesEconomiques()) {
            $this->affaireEconomique->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Santé".
     */
    public function chargerSante()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getSante()) {
            $this->sante->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Logement et développement collectif".
     */
    public function chargerDevCollectif()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getDeveloppementCollectif()) {
            $this->devCollectif->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Protection sociale".
     */
    public function chargerProtectionSociale()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getProtectionSociale()) {
            $this->protectionSociale->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Loisirs".
     */
    public function chargerLoisirs()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getLoisirs()) {
            $this->loisirs->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Education".
     */
    public function chargerEducation()
    {
        $profilJoue = $this->getProfilJoue();
        if ('1' == $profilJoue->getEduction()) {
            $this->education->checked = true;
        }
    }

    /**
     * Permet de charger les infos de "Activités(s) principales(s) - Autres".
     */
    public function chargerAutresActivitesPrincipales()
    {
        $profilJoue = $this->getProfilJoue();
        $script = '<script>';
        if ('1' == $profilJoue->getAutreActivitesPrincipales()) {
            $this->autreActivitePrinc->checked = true;
            $script .= "document.getElementById('ctl0_CONTENU_PAGE_autreActivitesPrinc').style.display = 'block';";
        } else {
            $script .= "document.getElementById('ctl0_CONTENU_PAGE_autreActivitesPrinc').style.display = 'none';";
        }
        $script .= '</script>';
        $this->scriptAffichagePrecisionAutre->Text .= $script;
    }

    /**
     * Permet de charger l'activité principale.
     */
    public function chargerActivitePrincipale()
    {
        $this->chargerServicesGeneraux();
        $this->chargerDefense();
        $this->chargerSecuritePublique();
        $this->chargerEnvironnement();
        $this->chargerAffairesEconomiques();
        $this->chargerSante();
        $this->chargerDevCollectif();
        $this->chargerProtectionSociale();
        $this->chargerLoisirs();
        $this->chargerEducation();
        $this->chargerAutresActivitesPrincipales();
    }

    /**
     * Permet de sauvegarder les infos de "Adresse postale".
     */
    public function saveAdressePostale()
    {
        $profilJoue = $this->getProfilJoue();
        if ($this->localiteVille->Text && '' != $this->localiteVille->Text) {
            $profilJoue->setVille($this->localiteVille->Text);
        }
        if ($this->adresse->Text && '' != $this->adresse->Text) {
            $profilJoue->setAdresse($this->adresse->Text);
        }
        if ($this->codePostal->Text && '' != $this->codePostal->Text) {
            $profilJoue->setCodePostal($this->codePostal->Text);
        } else {
            $profilJoue->setCodePostal('');
        }
        if ($this->listePays->SelectedValue && '0' != $this->listePays->SelectedValue) {
            $profilJoue->setPays($this->listePays->SelectedValue);
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder le nom officiel.
     */
    public function saveNomOfficiel()
    {
        $profilJoue = $this->getProfilJoue();
        if ($this->nomOfficiel->Text && '' != $this->nomOfficiel->Text) {
            $profilJoue->setNomOfficiel($this->nomOfficiel->Text);
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos du bloc "Points de contacts".
     */
    public function savePointsContacts()
    {
        $profilJoue = $this->getProfilJoue();
        if ($this->pointContact->Text && '' != $this->pointContact->Text) {
            $profilJoue->setPointContact($this->pointContact->Text);
        }
        if ($this->numeroNationalIdentification->Text && '' != $this->numeroNationalIdentification->Text) {
            $profilJoue->setNumeroNationalIdentification($this->numeroNationalIdentification->Text);
        }
        if ($this->telephone->Text && '' != $this->telephone->Text) {
            $profilJoue->setTelephone($this->telephone->Text);
        } else {
            $profilJoue->setTelephone('');
        }
        if ($this->fax->Text && '' != $this->fax->Text) {
            $profilJoue->setFax($this->fax->Text);
        } else {
            $profilJoue->setFax('');
        }
        if ($this->email->Text && '' != $this->email->Text) {
            $profilJoue->setEmail(trim($this->email->Text));
        } else {
            $profilJoue->setEmail('');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Adresse internet (le cas échéant)".
     */
    public function adressesInternet()
    {
        $profilJoue = $this->getProfilJoue();
        if ($this->adressePouvoirAdj->Text && '' != $this->adressePouvoirAdj->Text) {
            $profilJoue->setAdressePouvoirAdjudicateur($this->adressePouvoirAdj->Text);
        } else {
            $profilJoue->setAdressePouvoirAdjudicateur('');
        }
        if ($this->adresseProfilAch->Text && '' != $this->adresseProfilAch->Text) {
            $profilJoue->setAdresseProfilAcheteur($this->adresseProfilAch->Text);
        } else {
            $profilJoue->setAdresseProfilAcheteur('');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Type de pouvoir adjudicateur et activités(s) principales(s)".
     */
    public function saveLibellesAutresTypesPouvoirsAdjudicateurs()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->autrePouvoirAdj->checked && $this->autreLibellePouvoirAdj->Text && '' != $this->autreLibellePouvoirAdj->Text) {
            $profilJoue->setAutreLibelleTypePouvoirAdjudicateur($this->autreLibellePouvoirAdj->Text);
        } else {
            $profilJoue->setAutreLibelleTypePouvoirAdjudicateur('');
        }
        if (true == $this->autreActivitePrinc->checked && $this->autreActivitesPrinc->Text && '' != $this->autreActivitesPrinc->Text) {
            $profilJoue->setAutreLibelleActivitesPrincipales($this->autreActivitesPrinc->Text);
        } else {
            $profilJoue->setAutreLibelleActivitesPrincipales('');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Fonction de sauvegarde du profil JOUE.
     *
     * @param $sender
     * @param $param
     */
    public function saveProfilJoue($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $profilJoue = $this->getProfilJoue();
        if (!$profilJoue instanceof CommonTProfilJoue) {
            $profilJoue = new CommonTProfilJoue();
        }
        $profilJoue->setIdAgent($this->getIdAgent());
        $profilJoue->setOrganisme($this->getOrganisme());
        $this->setProfilJoue($profilJoue);
        $this->saveAdressePostale();
        $this->saveNomOfficiel();
        $this->savePointsContacts();
        $this->adressesInternet();
        $this->saveLibellesAutresTypesPouvoirsAdjudicateurs();
        $this->saveTypePouvoirAdjudicateur();
        $this->saveActivitePrincipale();
        $this->saveTypeOrganismeAcheteur();
        $this->savePouvoirAdjudicateurAgit();
        $profilJoue = $this->getProfilJoue();
        $profilJoue->save($connexion);
        //Redirection vers la page de confirmation
        $this->response->redirect('?page=Agent.ConfirmationEnregistrementProfilJoue');
    }

    /**
     * Permet de sauvegarder les infos de "Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales".
     */
    public function saveAutoriteNationale()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->autoriteNationale->checked) {
            $profilJoue->setAutoriteNationale('1');
        } else {
            $profilJoue->setAutoriteNationale('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Agence/office national(e) ou fédéral(e)".
     */
    public function saveOfficeNationale()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->officeNationale->checked) {
            $profilJoue->setOfficeNationale('1');
        } else {
            $profilJoue->setOfficeNationale('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Agence/office régional(e) ou local(e)".
     */
    public function saveOfficeRegionale()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->officeRegionale->checked) {
            $profilJoue->setOfficeRegionale('1');
        } else {
            $profilJoue->setOfficeRegionale('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Organisme de droit public".
     */
    public function saveOrganismePublic()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->organismePublic->checked) {
            $profilJoue->setOrganismePublic('1');
        } else {
            $profilJoue->setOrganismePublic('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Institution/agence européenne ou organisation européenne".
     */
    public function saveAgenceEuropeenne()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->agenceEuropeenne->checked) {
            $profilJoue->setOrganisationEuropenne('1');
        } else {
            $profilJoue->setOrganisationEuropenne('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Type de pouvoir adjudicateur - Autres".
     */
    public function saveTypePouvoirAdjudicateurAutre()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->autrePouvoirAdj->checked) {
            $profilJoue->setAutreTypePouvoirAdjudicateur('1');
        } else {
            $profilJoue->setAutreTypePouvoirAdjudicateur('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos du bloc "Type d'organisme acheteur".
     */
    public function saveTypeOrganismeAcheteur()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->pouvoirAdjMarcheCouvert->checked) {
            $profilJoue->setPouvoirAdjudicateurMarcheCouvert('1');
        } else {
            $profilJoue->setPouvoirAdjudicateurMarcheCouvert('0');
        }
        if (true == $this->entiteAdjudicatriceMarcheCouvert->checked) {
            $profilJoue->setEntiteAdjudicatriceMarcheCouvert('1');
        } else {
            $profilJoue->setEntiteAdjudicatriceMarcheCouvert('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Le pouvoir adjudicateur agit pour le compte d'autres personnes".
     */
    public function savePouvoirAdjudicateurAgit()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->pouvoirAdjAgitOui->checked) {
            $profilJoue->setPouvoirAdjudicateurAgit('1');
        } elseif (true == $this->pouvoirAdjAgitNon->checked) {
            $profilJoue->setPouvoirAdjudicateurAgit('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de charger le bloc "Type pouvoir adjudicateur".
     *
     * @param $connexion: objet connexion
     */
    public function saveTypePouvoirAdjudicateur()
    {
        $this->saveAutoriteNationale();
        $this->saveOfficeNationale();
        $this->saveOfficeRegionale();
        $this->saveOrganismePublic();
        $this->saveAgenceEuropeenne();
        $this->saveTypePouvoirAdjudicateurAutre();
    }

    /**
     * Permet de sauvegarder les infos de "Services généraux des administrations publiques".
     */
    public function saveServicesGeneraux()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->servicesGeneraux->checked) {
            $profilJoue->setServicesGeneraux('1');
        } else {
            $profilJoue->setServicesGeneraux('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Defense".
     */
    public function saveDefense()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->defense->checked) {
            $profilJoue->setDefense('1');
        } else {
            $profilJoue->setDefense('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Ordre et sécurité publics".
     */
    public function saveSecuritePublic()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->securitePublic->checked) {
            $profilJoue->setSecuritePublic('1');
        } else {
            $profilJoue->setSecuritePublic('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Environnement".
     */
    public function saveEnvironnement()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->environnement->checked) {
            $profilJoue->setEnvironnement('1');
        } else {
            $profilJoue->setEnvironnement('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Affaires économiques et financières".
     */
    public function saveAffaireEconomique()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->affaireEconomique->checked) {
            $profilJoue->setAffairesEconomiques('1');
        } else {
            $profilJoue->setAffairesEconomiques('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Santé".
     */
    public function saveSante()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->sante->checked) {
            $profilJoue->setSante('1');
        } else {
            $profilJoue->setSante('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Logement et développement collectif".
     */
    public function saveDevCollectif()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->devCollectif->checked) {
            $profilJoue->setDeveloppementCollectif('1');
        } else {
            $profilJoue->setDeveloppementCollectif('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Protection Sociale".
     */
    public function saveProtectionSociale()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->protectionSociale->checked) {
            $profilJoue->setProtectionSociale('1');
        } else {
            $profilJoue->setProtectionSociale('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Loisirs, culture et religion".
     */
    public function saveLoisirs()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->loisirs->checked) {
            $profilJoue->setLoisirs('1');
        } else {
            $profilJoue->setLoisirs('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "Education".
     */
    public function saveEducation()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->education->checked) {
            $profilJoue->setEduction('1');
        } else {
            $profilJoue->setEduction('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos de "types d'activités(s) principales(s)".
     */
    public function saveActivitesPrincipalesAutres()
    {
        $profilJoue = $this->getProfilJoue();
        if (true == $this->autreActivitePrinc->checked) {
            $profilJoue->setAutreActivitesPrincipales('1');
        } else {
            $profilJoue->setAutreActivitesPrincipales('0');
        }
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de sauvegarder les infos du bloc "Activités principales".
     */
    public function saveActivitePrincipale()
    {
        $this->saveServicesGeneraux();
        $this->saveDefense();
        $this->saveSecuritePublic();
        $this->saveEnvironnement();
        $this->saveAffaireEconomique();
        $this->saveSante();
        $this->saveDevCollectif();
        $this->saveProtectionSociale();
        $this->saveLoisirs();
        $this->saveEducation();
        $this->saveActivitesPrincipalesAutres();
    }

    /**
     * Permet d'initialiser l'objet CommonTProfilJoue.
     */
    public function InitialiserProfilJoue()
    {
        $profilJoue = $this->getProfilJoue();
        if (!$profilJoue) {
            $profilJoue = new CommonTProfilJoue();
            $this->setProfilJoue($profilJoue);
        }
        $this->effacerChampsObjetProfilJoue();
        $profilJoue->setIdAgent($this->getIdAgent());
        $profilJoue->setOrganisme($this->getOrganisme());
        $profilJoue->setPays(Atexo_Config::getParameter('VALEUR_DEFAUT_LISTE_PAYS'));
        $this->setProfilJoue($profilJoue);
    }

    /**
     * Permet de réinitialiser le formulaire du profil JOUE.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function reInitialiserFormulaire($sender, $param)
    {
        //Redirection vers la page de confirmation
        $this->response->redirect('?page=Agent.MonProfilJoue&init');
    }

    /**
     * Permet d'effacer l'objet profil Joue.
     */
    public function effacerChampsObjetProfilJoue()
    {
        $profilJoue = $this->getProfilJoue();
        $profilJoue->setIdAgent('');
        $profilJoue->setOrganisme('');
        $profilJoue->setNomOfficiel('');
        $profilJoue->setPays('');
        $profilJoue->setVille('');
        $profilJoue->setAdresse('');
        $profilJoue->setCodePostal('');
        $profilJoue->setPointContact('');
        $profilJoue->setNumeroNationalIdentification('');
        $profilJoue->setTelephone('');
        $profilJoue->setFax('');
        $profilJoue->setEmail('');
        $profilJoue->setAdressePouvoirAdjudicateur('');
        $profilJoue->setAdresseProfilAcheteur('');
        $profilJoue->setAutoriteNationale('');
        $profilJoue->setOfficeNationale('');
        $profilJoue->setCollectiviteTerritoriale('');
        $profilJoue->setOfficeRegionale('');
        $profilJoue->setOrganismePublic('');
        $profilJoue->setOrganisationEuropenne('');
        $profilJoue->setAutreTypePouvoirAdjudicateur('');
        $profilJoue->setAutreLibelleTypePouvoirAdjudicateur('');
        $profilJoue->setServicesGeneraux('');
        $profilJoue->setDefense('');
        $profilJoue->setSecuritePublic('');
        $profilJoue->setEnvironnement('');
        $profilJoue->setAffairesEconomiques('');
        $profilJoue->setSante('');
        $profilJoue->setDeveloppementCollectif('');
        $profilJoue->setProtectionSociale('');
        $profilJoue->setLoisirs('');
        $profilJoue->setEduction('');
        $profilJoue->setAutreActivitesPrincipales('');
        $profilJoue->setAutreLibelleActivitesPrincipales('');
        $profilJoue->setPouvoirAdjudicateurAgit('');
        $profilJoue->setPouvoirAdjudicateurMarcheCouvert('');
        $profilJoue->setEntiteAdjudicatriceMarcheCouvert('');
    }

    /**
     * Permet d'annuler l'opération.
     *
     * @param $sender
     * @param $param
     */
    public function annuler($sender, $param)
    {
        $this->response->redirect('agent');
    }
}
