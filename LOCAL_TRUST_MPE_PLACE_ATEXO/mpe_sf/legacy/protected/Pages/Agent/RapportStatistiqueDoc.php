<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

class RapportStatistiqueDoc extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $uri = '/reports/ORME_STAT/Doc_ministere_V2';
        } else {
            $uri = '/reports/ORME_STAT/Doc_entite_achat';
        }

        $this->criteresStatistiques->setUri($uri);
        $this->criteresStatistiques->setNomStat(Prado::localize('DOCUMENTS_PAR_ORGANISMES'));
    }
}
