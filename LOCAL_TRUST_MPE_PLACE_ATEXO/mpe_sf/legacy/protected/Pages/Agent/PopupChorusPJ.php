<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusPj;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChorusPJ extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);

        $this->uploadBar->setFileSizeMax(Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE'));
    }

    /**
     * Verifier la taille maximum du document à ajouter.
     */
    public function verifySizeDoc($sender, $param)
    {
        $sizedocsAttache = $this->uploadBar->getFilesSize();
        if (is_array($sizedocsAttache) && count($sizedocsAttache)) {
            foreach ($sizedocsAttache as $sizedocAttache) {
                if ($sizedocAttache > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                    $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->docValidator->ErrorMessage = Prado::localize('TEXT_TAILLE_AUTORISEE_DOC', ['size' => Atexo_Util::arrondirSizeFile((Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE') / 1024))]);

                    return false;
                }
            }
        }
    }

    /**
     * Verifier les éxtensions acceptées.
     */
    public function verifyExtensionDoc($sender, $param)
    {
        if (!$this->uploadBar->HasFile()) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->docValidator->ErrorMessage = Prado::localize('VEUILLEZ_SELECTIONNEZ_LES_FICHIERS_A_AJOUTER');

            return false;
        }
        $filesName = $this->uploadBar->getFilesNames();
        if (is_array($filesName) && count($filesName)) {
            foreach ($filesName as $fileName) {
                $extensionFile = strtoupper(Atexo_Util::getExtension($fileName));
                $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                if (!in_array($extensionFile, $data)) {
                    $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->docValidator->ErrorMessage = str_replace('[__EXTENSION_FILES__]', implode(', ', $data), Prado::localize('TEXT_MSG_EXTESION_NON_VALIDE'));
                }
            }
        }
    }

    public function verifyDocAttache($sender, $param)
    {
        $this->verifyExtensionDoc($sender, $param);
        $this->verifySizeDoc($sender, $param);
    }

    /* ajouter un document dans la base
     *
       *
       */
    public function addPieceJointe($sender, $param)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->IsValid) {
                if ($this->uploadBar->HasFile()) {
                    //Début scan antivirus
                    $localeNames = $this->uploadBar->getLocalNames();
                    $filesname = $this->uploadBar->getFilesNames();
                    $filesSize = $this->uploadBar->getFilesSize();
                    foreach ($localeNames as $k => $localeName) {
                        $msg = Atexo_ScanAntivirus::startScan($localeName);
                        if ($msg) {
                            $this->afficherErreur($msg.' "'.$filesname[$k].'"');

                            return;
                        }
                    }
                    //Fin scan antivirus
                    foreach ($localeNames as $k => $localeName) {
                        $infile = Atexo_Config::getParameter('COMMON_TMP').'pieceJointe'.$k.session_id().time();
                        if (copy($localeName, $infile)) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                            $atexoBlob = new Atexo_Blob();
                            $atexoCrypto = new Atexo_Crypto();
                            $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                            $pj = new CommonChorusPj();
                            $pj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                            if (is_array($arrayTimeStampAvis)) {
                                $pj->setHorodatage($arrayTimeStampAvis['horodatage']);
                                $pj->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                            }
                            $avisIdBlob = $atexoBlob->insert_blob($filesname[$k], $infile, Atexo_CurrentUser::getCurrentOrganism());
                            $pj->setIdEchange(Atexo_Util::atexoHtmlEntities($_GET['idEchange']));
                            $pj->setNomFichier($filesname[$k]);
                            $pj->setTaille($filesSize[$k]);
                            $pj->setFichier($avisIdBlob);
                            $pj->save($connexion);
                        }
                        @unlink($infile);
                    }
                }
                $this->labelClose->Text = '<script>refreshRepeaterFormLibre();window.close();</script>';
            }
        } catch (Exception $e) {
            $logger->error('Error => '.$e->getMessage()."\n ".$e->getTraceAsString());
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
