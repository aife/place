<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use Exception;
use Prado\Prado;

/**
 * Classe de gestion des statistiques de recherche avancee.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesRechercheAvance extends MpeTPage
{
    public array $_ligneTotal = [];
    public ?bool $_pourcentage = null;
    public ?int $_decimal = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la classe.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
            $this->critereFiltre_2_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_2_dateEnd->Text = date('d/m/Y');

            //$this->critereFiltre_4_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_4_dateEnd->Text=date('d/m/Y');
            //$this->critereFiltre_5_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_5_dateEnd->Text=date('d/m/Y');
            //$this->critereFiltre_6_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_6_dateEnd->Text=date('d/m/Y');
            //$this->critereFiltre_7_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_7_dateEnd->Text=date('d/m/Y');
            //$this->critereFiltre_8_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_8_dateEnd->Text=date('d/m/Y');
            //$this->critereFiltre_9_dateStart->Text="01/01/".date('Y');
            //$this->critereFiltre_9_dateEnd->Text=date('d/m/Y');

            $this->resultat->setVisible(false);
            $this->recherche->setDisplay('Dynamic');

            $this->displayClauses();

            if (!isset($_GET['allOrgs'])) {
                $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
                if (!is_array($entities)) {
                    $entities = [];
                }
                //$entities=Atexo_Util::arrayUnshift($entities, Prado::localize('TOUTES_LES_ENTITES'));
                $this->entitesAchat->DataSource = $entities;
                $this->entitesAchat->DataBind();
                $this->entitesAchat->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
            }

            //Statut de contrat
            $status = [
                '0' => Prado::localize('TEXT_TOUT_STATUT'),
                Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => Prado::Localize('DONNEES_DU_CONTRAT_A_SAISIR'),
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') => Prado::Localize('ETAPE_NOTIFICATION'),
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => Prado::Localize('ETAPE_NOTIFICATION_EFFECTUEE'),
            ];
            $this->statutContrat->DataSource = $status;
            $this->statutContrat->DataBind();
            $this->statutContrat->setSelectedValue(array_values($status));

            ///Charger les catégories des statistiques
            $this->displayCategoriesStatistiques();
        } else {
            if (empty($this->getViewState('objetDeLaRecherche')) && '' != $this->statsType->SelectedValue && null != $this->statsType->SelectedValue) {
                $dataRefCategoryStats = $this->getViewState('dataRefCategoryStats');
                $this->setViewState('objetDeLaRecherche', $dataRefCategoryStats[$this->statsType->Text]);
            }
        }
        if (isset($_GET['allOrgs'])) {
            $this->filtresComplementaire->setVisible(false);
        }
        $scriptJs = '<script type="text/javascript">';
        $scriptJs .= 'updatePanelRecherche(false);';
        $scriptJs .= '</script>';
        $this->javascript->Text = $scriptJs;
    }

    /**
     * Permet de recuperer les criteres de recherche saisis par l'utilisateur dans un objet Atexo_Statistiques_CriteriaVo.
     *
     * @param string $atexoOrg: organisme pour la recherche
     *
     * @return Atexo_Statistiques_CriteriaVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteriaSearch($atexoOrg)
    {
        $criteria = new Atexo_Statistiques_CriteriaVo();
        if (!isset($_GET['allOrgs'])) {
            $service = 'IN';
            if ($this->cumulValeurs->Checked) {
                if (0 != $this->entitesAchat->SelectedValue) {
                    $service .= Atexo_EntityPurchase::retrieveAllChildrenServices($this->entitesAchat->SelectedValue, $atexoOrg);
                } else {
                    $i = 1;
                    $service .= '(';
                    $entities = Atexo_EntityPurchase::getEntityPurchase($atexoOrg, true);
                    $nbr = count($entities);
                    $value = '';
                    foreach ($entities as $key => $value) {
                        if ($i != $nbr) {
                            $service .= $key.',';
                        } else {
                            $service .= $key;
                        }

                        ++$i;
                    }

                    $service .= ')';
                }
            } else {
                $service = '0' !== $this->entitesAchat->getSelectedValue() ?
                    $service .  '('.$this->entitesAchat->getSelectedValue().')' : 'IS NULL';
            }

            $criteria->setIdService($service);
        }
        $criteria->setRechercheTag($this->statsType->SelectedIndex);
        $criteria->setSelectedEntity($this->entitesAchat->getSelectedValue());
        $criteria->setValeurCumule($this->cumulValeurs->Checked);
        $criteria->setOrganisme($atexoOrg);

        if ($this->critereFiltre_1->checked) {
            if ('' != $this->critereFiltre_1_dateStart->Text) {
                $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
            }
            if ('' != $this->critereFiltre_1_dateEnd->Text) {
                $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
            }
        }
        if ($this->critereFiltre_2->checked) {
            if ('' != $this->critereFiltre_2_dateStart->Text) {
                $criteria->setDateRemisePlisStart(Atexo_Util::frnDate2iso($this->critereFiltre_2_dateStart->Text));
            }
            if ('' != $this->critereFiltre_2_dateEnd->Text) {
                $criteria->setDateRemisePlisEnd(Atexo_Util::frnDate2iso($this->critereFiltre_2_dateEnd->Text));
            }
        }
        if ($this->searchType() && $this->critereFiltre_3->checked) {
            $criteria->setDateMiseEnLigneAujourdhui(true);
        }
        if (('Contrats' == $_POST['objetRecheche']) || ('Interface PLACE-CHORUS' == $_POST['objetRecheche'])) {
            if ($this->critereFiltre_4->checked) {
                if ('' != $this->critereFiltre_4_dateStart->Text) {
                    $criteria->setDatePrevisionnelleNotificationStart(Atexo_Util::frnDate2iso($this->critereFiltre_4_dateStart->Text));
                }
                if ('' != $this->critereFiltre_4_dateEnd->Text) {
                    $criteria->setDatePrevisionnelleNotificationEnd(Atexo_Util::frnDate2iso($this->critereFiltre_4_dateEnd->Text));
                }
            }
            if ($this->critereFiltre_5->checked) {
                if ('' != $this->critereFiltre_5_dateStart->Text) {
                    $criteria->setDatePrevisionnelleFinMarcheStart(Atexo_Util::frnDate2iso($this->critereFiltre_5_dateStart->Text));
                }
                if ('' != $this->critereFiltre_5_dateEnd->Text) {
                    $criteria->setDatePrevisionnelleFinMarcheEnd(Atexo_Util::frnDate2iso($this->critereFiltre_5_dateEnd->Text));
                }
            }
            if ($this->critereFiltre_6->checked) {
                if ('' != $this->critereFiltre_6_dateStart->Text) {
                    $criteria->setDatePrevisionnelleFinMaximaleMarcheStart(Atexo_Util::frnDate2iso($this->critereFiltre_6_dateStart->Text));
                }
                if ('' != $this->critereFiltre_6_dateEnd->Text) {
                    $criteria->setDatePrevisionnelleFinMaximaleMarcheEnd(Atexo_Util::frnDate2iso($this->critereFiltre_6_dateEnd->Text));
                }
            }
            if ($this->critereFiltre_7->checked) {
                if ('' != $this->critereFiltre_7_dateStart->Text) {
                    $criteria->setDateDefinitiveNotificationStart(Atexo_Util::frnDate2iso($this->critereFiltre_7_dateStart->Text));
                }
                if ('' != $this->critereFiltre_7_dateEnd->Text) {
                    $criteria->setDateDefinitiveNotificationEnd(Atexo_Util::frnDate2iso($this->critereFiltre_7_dateEnd->Text));
                }
            }
            if ($this->critereFiltre_8->checked) {
                if ('' != $this->critereFiltre_8_dateStart->Text) {
                    $criteria->setDateDefinitiveFinMarcheStart(Atexo_Util::frnDate2iso($this->critereFiltre_8_dateStart->Text));
                }
                if ('' != $this->critereFiltre_8_dateEnd->Text) {
                    $criteria->setDateDefinitiveFinMarcheEnd(Atexo_Util::frnDate2iso($this->critereFiltre_8_dateEnd->Text));
                }
            }
            if ($this->critereFiltre_9->checked) {
                if ('' != $this->critereFiltre_9_dateStart->Text) {
                    $criteria->setDateMaximaleFinMarcheStart(Atexo_Util::frnDate2iso($this->critereFiltre_9_dateStart->Text));
                }
                if ('' != $this->critereFiltre_9_dateEnd->Text) {
                    $criteria->setDateMaximaleFinMarcheEnd(Atexo_Util::frnDate2iso($this->critereFiltre_9_dateEnd->Text));
                }
            }

            if ('' != $this->minTrancheBudgetaire->Text) {
                $criteria->setMontantMin($this->minTrancheBudgetaire->Text);
            }
            if ('' != $this->maxTrancheBudgetaire->Text) {
                $criteria->setMontantMax($this->maxTrancheBudgetaire->Text);
            }

            if ($this->statutContrat->SelectedValue && '0' != $this->statutContrat->SelectedValue) {
                $criteria->setStatusContrat($this->statutContrat->SelectedValue);
            }

            if ($this->objetRecherche_30->checked) {
                $criteria->setContratMulti(true);
            }
        }
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $criteria->setClauseSociale($this->clauseSociales->getSelectedValue());
            $criteria->setClauseEnvironnement($this->clauseEnvironnementale->getSelectedValue());

            /*if($this->clauseSocialeConditionExecution->checked){
                $criteria->setClauseSocialeConditionExecution('1');
            }

            if($this->clauseSocialeInsertion->checked){
                $criteria->setClauseSocialeInsertion('1');
            }

            if($this->clauseSocialeAteliersProteges->checked){
                $criteria->setAtelierProtege('1');
            }

            if($this->clauseSocialeSIAE->checked){
                $criteria->setSiae('1');
            }

            if($this->clauseSocialeESS->checked){
                $criteria->setEss('1');
            }

            if($this->clauseEnvSpecsTechniques->checked){
                $criteria->setClauseEnvSpecsTechniques('1');
            }
            if($this->clauseEnvCondExecution->checked){
                $criteria->setClauseEnvCondExecution('1');
            }
            if($this->clauseEnvCriteresSelect->checked){
                $criteria->setClauseEnvCriteresSelect('1');
            }*/
        }

        $criteria = $this->getCriteriaSearchBlocRecap($criteria);

        return $criteria;
    }

    /**
     * Permet d'afficher le recapitulatif de la recherche pour lecture à l'utilisateur.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getRecapitulatifRecherche()
    {
        //Affichage de l'objet et de l'intervalle de la recherche pour le bloc recapitulatif
        $criteria = $this->getViewState('criteres');
        if ($criteria instanceof Atexo_Statistiques_CriteriaVo) {
            $this->objetRecherche->Text = $criteria->getObjetRecherche();
            $this->intervalRecherche->Text = $criteria->getIntervalleRechercheBlocRecap();
        }
    }

    /**
     * Methode invoquee lorsque l'utilisateur clique sur le bouton "Rechercher"
     * Execute la requete et organise les donnees.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onSearchClick()
    {
        $criteria = null;
        $totalElement = [];
        $this->setViewState('objetDeLaRecherche', $_POST['objetRecheche']);
        self::isPourcentage();
        $this->_ligneTotal = [];
        $organismes = null;
        $dataSourceDetail = [];
        $total = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'pn' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0];
        if (isset($_GET['allOrgs'])) {
            $this->entite->Text = Prado::localize('PLATEFOME_PMI');
            $results = ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => 0, 'totalAccordCadreSad' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'grandTotal' => 0];
            $resultsStats = ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => 0, 'totalAccordCadreSad' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'grandTotal' => 0];
            $organismes = Atexo_Organismes::retrieveOrganismes(); //CommonOrganismePeer::doSelect(new Criteria(), $connexionCom);
            $tabNbr = ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => 0, 'totalAccordCadreSad' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'grandTotal' => 0];
            foreach ($organismes as $organisme) {
                if ($organisme instanceof CommonOrganisme) {
                    $criteria = self::getCriteriaSearch($organisme->getAcronyme());
                    $resultsStats = self::getResultatStatistique($criteria);
                    $this->setViewState('criteres', clone $criteria);
                    if ($this->_pourcentage) {
                        if (is_numeric($resultsStats['ao'])) {
                            ++$tabNbr['ao'];
                        }
                        if (is_numeric($resultsStats['mn'])) {
                            ++$tabNbr['mn'];
                        }
                        if (is_numeric($resultsStats['pn'])) {
                            ++$tabNbr['pn'];
                        }
                        if (is_numeric($resultsStats['dc'])) {
                            ++$tabNbr['dc'];
                        }
                        if (is_numeric($resultsStats['autre'])) {
                            ++$tabNbr['autre'];
                        }
                        if (is_numeric($resultsStats['total'])) {
                            ++$tabNbr['total'];
                        }
                        if (is_numeric($resultsStats['sad'])) {
                            ++$tabNbr['sad'];
                        }
                        if (is_numeric($resultsStats['accordcadre'])) {
                            ++$tabNbr['accordcadre'];
                        }
                        if (is_numeric($resultsStats['totalAccordCadreSad'])) {
                            ++$tabNbr['totalAccordCadreSad'];
                        }
                        if (is_numeric($resultsStats['mapaInf'])) {
                            ++$tabNbr['mapaInf'];
                        }
                        if (is_numeric($resultsStats['mapaSupp'])) {
                            ++$tabNbr['mapaSupp'];
                        }
                        if (is_numeric($resultsStats['totalMapa'])) {
                            ++$tabNbr['totalMapa'];
                        }
                        if (is_numeric($resultsStats['grandTotal'])) {
                            ++$tabNbr['grandTotal'];
                        }
                    }
                    if ($criteria->estRechercheObjetContrat()) {
                        $results['Public'] += $resultsStats['Public'];
                        $results['MS'] += $resultsStats['MS'];
                        $results['Autres'] += $resultsStats['Autres'];
                        $results['sousTotal1'] += $resultsStats['sousTotal1'];
                        $results['ac'] += $resultsStats['ac'];
                        $results['sad'] += $resultsStats['sad'];
                        $results['sousTotal2'] += $resultsStats['sousTotal2'];
                        $results['total'] += $resultsStats['total'];
                    } elseif ($criteria->estRechercheObjetContratChorus()) {
                        $results['Public'] += $resultsStats['Public'];
                        $results['MS'] += $resultsStats['MS'];
                        $results['Autres'] += $resultsStats['Autres'];
                        $results['sousTotal1'] += $resultsStats['sousTotal1'];
                    } else {
                        $results['mapaInf'] += $resultsStats['mapaInf'];
                        $results['mapaSupp'] += $resultsStats['mapaSupp'];
                        $results['ao'] += $resultsStats['ao'];
                        $results['mn'] += $resultsStats['mn'];
                        $results['pn'] += $resultsStats['pn'];
                        $results['dc'] += $resultsStats['dc'];
                        $results['autre'] += $resultsStats['autre'];
                        $results['sad'] += $resultsStats['sad'];
                        $results['accordcadre'] += $resultsStats['accordcadre'];
                        $results['total'] += $resultsStats['total'];
                        $results['totalAccordCadreSad'] += $resultsStats['totalAccordCadreSad'];
                        $results['totalMapa'] += $resultsStats['totalMapa'];
                        $results['grandTotal'] += $resultsStats['grandTotal'];
                    }
                    $resultsStats['entite'] = $organisme->getDenominationOrg();
                    $dataSourceDetail[] = $resultsStats;
                }
            }
            if ($this->_pourcentage) {
                $results = self::getPercentValue($tabNbr, $results);
            }
            $total = $results;
            $totalElement = $total;
        } else {
            $this->entite->Text = $this->entitesAchat->getSelectedItem()->getText();
            $atexoOrg = Atexo_CurrentUser::getCurrentOrganism();
            $criteria = self::getCriteriaSearch($atexoOrg);
            $this->setViewState('criteres', clone $criteria);

            if (0 != $this->entitesAchat->SelectedValue) {
                $arrayChilds = [];
                $services = Atexo_EntityPurchase::getAllChilds($this->entitesAchat->SelectedValue, $arrayChilds, $atexoOrg);
            } else {
                $pathService = Atexo_EntityPurchase::getPathEntityById('0', $atexoOrg);
                $services = (new Atexo_EntityPurchase())->getAllServices($atexoOrg);
                $serviceSelected = (new Atexo_EntityPurchase())->getEntityById($this->entitesAchat->SelectedValue, $atexoOrg);
                $service0 = new CommonService();
                $service0->setId(0);
                $service0->setLibelle($serviceSelected->getDenominationOrg());
                $service0->setCheminComplet($pathService);
                array_unshift($services, $service0);
            }
            $tabNbr = ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => 0, 'totalAccordCadreSad' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'grandTotal' => 0];
            $tabNbrContrats = ['p' => 0, 'ms' => 0, 'autre' => 0, 'total' => 0, 'accordcadre' => 0, 'sad' => 0, 'totalAccordCadreSad' => 0];
            $oldService = $criteria->getIdService();
            foreach ($services as $service) {
                $resultat = [];
                $criteria->setIdService('0' !== $service->getId() ?'IN('.$service->getId().')' : 'IS NULL');
                $resultat = self::getResultatStatistique($criteria);

                $resultat['entite'] = $service->getCheminComplet();
                if ($this->_pourcentage) {
                    if (is_numeric($resultat['ao'])) {
                        ++$tabNbr['ao'];
                    }
                    if (is_numeric($resultat['mn'])) {
                        ++$tabNbr['mn'];
                    }
                    if (is_numeric($resultat['pn'])) {
                        ++$tabNbr['pn'];
                    }
                    if (is_numeric($resultat['dc'])) {
                        ++$tabNbr['dc'];
                    }
                    if (is_numeric($resultat['autre'])) {
                        ++$tabNbr['autre'];
                    }
                    if (is_numeric($resultat['total'])) {
                        ++$tabNbr['total'];
                    }
                    if (is_numeric($resultat['sad'])) {
                        ++$tabNbr['sad'];
                    }
                    if (is_numeric($resultat['accordcadre'])) {
                        ++$tabNbr['accordcadre'];
                    }
                    if (is_numeric($resultat['totalAccordCadreSad'])) {
                        ++$tabNbr['totalAccordCadreSad'];
                    }
                    if (is_numeric($resultat['mapaInf'])) {
                        ++$tabNbr['mapaInf'];
                    }
                    if (is_numeric($resultat['mapaSupp'])) {
                        ++$tabNbr['mapaSupp'];
                    }
                    if (is_numeric($resultat['totalMapa'])) {
                        ++$tabNbr['totalMapa'];
                    }
                    if (is_numeric($resultat['grandTotal'])) {
                        ++$tabNbr['grandTotal'];
                    }
                }

                if (!$criteria->getValeurCumule() && $criteria->getIdService() == $oldService) {
                    if ($criteria->estRechercheObjetContrat()) {
                        $totalElement['Public'] = $resultat['Public'];
                        $totalElement['MS'] = $resultat['MS'];
                        $totalElement['Autres'] = $resultat['Autres'];
                        $totalElement['sousTotal1'] = $resultat['sousTotal1'];
                        $totalElement['ac'] = $resultat['ac'];
                        $totalElement['sad'] = $resultat['sad'];
                        $totalElement['sousTotal2'] = $resultat['sousTotal2'];
                        $totalElement['total'] = $resultat['total'];
                    } elseif ($criteria->estRechercheObjetContratChorus()) {
                        $totalElement['Public'] = $resultat['Public'];
                        $totalElement['MS'] = $resultat['MS'];
                        $totalElement['Autres'] = $resultat['Autres'];
                        $totalElement['sousTotal1'] = $resultat['sousTotal1'];
                    } else {
                        $totalElement['ao'] = $resultat['ao'];
                        $totalElement['mn'] = $resultat['mn'];
                        $totalElement['pn'] = $resultat['pn'];
                        $totalElement['dc'] = $resultat['dc'];
                        $totalElement['autre'] = $resultat['autre'];
                        $totalElement['total'] = $resultat['total'];
                        $totalElement['sad'] = $resultat['sad'];
                        $totalElement['accordcadre'] = $resultat['accordcadre'];
                        $totalElement['totalAccordCadreSad'] = $resultat['totalAccordCadreSad'];
                        $totalElement['mapaInf'] = $resultat['mapaInf'];
                        $totalElement['mapaSupp'] = $resultat['mapaSupp'];
                        $totalElement['totalMapa'] = $resultat['totalMapa'];
                        $totalElement['grandTotal'] = $resultat['grandTotal'];
                    }
                }

                if ($criteria->estRechercheObjetContrat()) {
                    $total['Public'] += $resultat['Public'];
                    $total['MS'] += $resultat['MS'];
                    $total['Autres'] += $resultat['Autres'];
                    $total['sousTotal1'] += $resultat['sousTotal1'];
                    $total['ac'] += $resultat['ac'];
                    $total['sad'] += $resultat['sad'];
                    $total['sousTotal2'] += $resultat['sousTotal2'];
                    $total['total'] += $resultat['total'];
                } elseif ($criteria->estRechercheObjetContratChorus()) {
                    $total['Public'] += $resultat['Public'];
                    $total['MS'] += $resultat['MS'];
                    $total['Autres'] += $resultat['Autres'];
                    $total['sousTotal1'] += $resultat['sousTotal1'];
                } else {
                    $total['Public'] += $resultat['Public'];
                    $total['ao'] += $resultat['ao'];
                    $total['mn'] += $resultat['mn'];
                    $total['pn'] += $resultat['pn'];
                    $total['dc'] += $resultat['dc'];
                    $total['autre'] += $resultat['autre'];
                    $total['total'] += $resultat['total'];
                    $total['sad'] += $resultat['sad'];
                    $total['accordcadre'] += $resultat['accordcadre'];
                    $total['totalAccordCadreSad'] += $resultat['totalAccordCadreSad'];
                    $total['mapaInf'] += $resultat['mapaInf'];
                    $total['mapaSupp'] += $resultat['mapaSupp'];
                    $total['totalMapa'] += $resultat['totalMapa'];
                    $total['grandTotal'] += $resultat['grandTotal'];
                }
                $dataSourceDetail[] = $resultat;
            }
            $criteria->setIdService($oldService);
            $this->setViewState('criteres', clone $criteria);
            if ($this->_pourcentage) {
                $total = self::getPercentValue($tabNbr, $total);
            }
            if ($this->cumulValeurs->Checked) {
                $results = $total;
            }
            if ($criteria->getValeurCumule()) {
                $totalElement = $total;
            }
        }
        $this->getRecapitulatifRecherche();
        if ($criteria->estRechercheObjetContrat()) {
            $this->nbrPublic->Text = self::getNumber($totalElement['Public'], $this->_decimal, $this->_pourcentage);
            $this->nbrMs->Text = self::getNumber($totalElement['MS'], $this->_decimal, $this->_pourcentage);
            $this->nbrAutre->Text = self::getNumber($totalElement['Autres'], $this->_decimal, $this->_pourcentage);
            $this->totalNbr->Text = self::getNumber($totalElement['sousTotal1'], $this->_decimal, $this->_pourcentage);

            $this->nbrAccordCadre->Text = self::getNumber($totalElement['ac'], $this->_decimal, $this->_pourcentage);
            $this->nbrSad->Text = self::getNumber($totalElement['sad'], $this->_decimal, $this->_pourcentage);
            $this->totalAccordCadreSad->Text = self::getNumber($totalElement['sousTotal2'], $this->_decimal, $this->_pourcentage);
            $this->grandTotal->Text = self::getNumber($totalElement['total'], $this->_decimal, $this->_pourcentage);
        } elseif ($criteria->estRechercheObjetContratChorus()) {
            $this->nbrPublic->Text = self::getNumber($totalElement['Public'], $this->_decimal, $this->_pourcentage);
            $this->nbrMs->Text = self::getNumber($totalElement['MS'], $this->_decimal, $this->_pourcentage);
            $this->nbrAutre->Text = self::getNumber($totalElement['Autres'], $this->_decimal, $this->_pourcentage);
            $this->totalNbr->Text = self::getNumber($totalElement['sousTotal1'], $this->_decimal, $this->_pourcentage);
        } else {
            $this->nbrMapinf->Text = self::getNumber($totalElement['mapaInf'], $this->_decimal, $this->_pourcentage);
            $this->nbrMapsup->Text = self::getNumber($totalElement['mapaSupp'], $this->_decimal, $this->_pourcentage);
            $this->nbrAo->Text = self::getNumber($totalElement['ao'], $this->_decimal, $this->_pourcentage);
            $this->nbrMn->Text = self::getNumber($totalElement['mn'], $this->_decimal, $this->_pourcentage);
            $this->nbrPn->Text = self::getNumber($totalElement['pn'], $this->_decimal, $this->_pourcentage);
            $this->nbrDc->Text = self::getNumber($totalElement['dc'], $this->_decimal, $this->_pourcentage);
            $this->nbrAutre->Text = self::getNumber($totalElement['autre'], $this->_decimal, $this->_pourcentage);
            $this->totalNbr->Text = self::getNumber($totalElement['total'], $this->_decimal, $this->_pourcentage);
            $this->totalnbrMap->Text = self::getNumber($totalElement['totalMapa'], $this->_decimal, $this->_pourcentage);
            $this->nbrAccordCadre->Text = self::getNumber($totalElement['accordcadre'], $this->_decimal, $this->_pourcentage);
            $this->nbrSad->Text = self::getNumber($totalElement['sad'], $this->_decimal, $this->_pourcentage);
            $this->totalAccordCadreSad->Text = self::getNumber($totalElement['totalAccordCadreSad'], $this->_decimal, $this->_pourcentage);
            $this->grandTotal->Text = self::getNumber($totalElement['grandTotal'], $this->_decimal, $this->_pourcentage);
        }
        if (is_array($dataSourceDetail) && count($dataSourceDetail) > 0) {
            foreach ($total as $key => $value) {
                $totalPercent[$key] = self::getNumber($total[$key], $this->_decimal, $this->_pourcentage);
            }
            $total = $totalPercent;
            $this->_ligneTotal = $total;
            if ($this->_pourcentage) {
                foreach ($dataSourceDetail as $one) {
                    foreach ($one as $key => $value) {
                        if (is_numeric($value)) {
                            $one[$key] = number_format($one[$key], 2, ',', ' ').'%';
                        }
                    }
                    $tabResultat[] = $one;
                }
                $dataSourceDetail = $tabResultat;
            }
            $this->repeaterDetail->DataSource = $dataSourceDetail;
            $this->repeaterDetail->DataBind();
            $this->exportXlsDetail->setVisible(true);
        } else {
            $this->exportXlsDetail->setVisible(false);
        }
        $this->resultat->setVisible(true);
        $this->recherche->setDisplay('None');
    }

    /**
     * Permet de generer l'excel des statistiques des recherches avancees.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function genererExcel($sender, $param)
    {
        $entiteName = null;
        $nomEntite = null;
        $denom = [];
        $afficherCoordonnees = true;
        self::isPourcentage();
        $acronymeOrganisme = Atexo_CurrentUser::getCurrentOrganism();

        $criteria = $this->getViewState('criteres');
        $service = $criteria->getSelectedEntity();

        $dataStats = [];
        $nomPf = Prado::localize('TEXT_PLATEFORME_PMI');
        if (isset($_GET['allOrgs'])) {
            $denom = [];
            $res = [];
            if ('myEntity' == $param->CommandName) {
                $afficherCoordonnees = false;
                self::isPourcentage();
                $entiteName = $nomPf;
                $nomEntite = $nomPf;
                $criteria->setIdService('');
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $organismes = Atexo_Organismes::retrieveOrganismes();
                foreach ($organismes as $organisme) {
                    if ($organisme instanceof CommonOrganisme) {
                        $criteria->setOrganisme($organisme->getAcronyme());
                        $values = self::getResultatStatistique($criteria, true, true);
                        $arrayTypeProcedure = (new Atexo_Consultation_ProcedureType())->rerieveAllTypeProcedureAndTypeProcedurePortail($organisme->getAcronyme());
                        ///!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
                        foreach ($values as $idTypeProcedure => $nombreConsultation) {
                            if (!$this->_pourcentage && !isset($results[$arrayTypeProcedure[$idTypeProcedure]])) {
                                $results[$arrayTypeProcedure[$idTypeProcedure]] = 0;
                            }
                            if ('total' == $idTypeProcedure) {
                                $results['total'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['total'];
                                }
                            } elseif ('grandTotal' == $idTypeProcedure) {
                                $results['grandTotal'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['grandTotal'];
                                }
                            } elseif ('totalMapa' == $idTypeProcedure) {
                                $results['totalMapa'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['totalMapa'];
                                }
                            } else {
                                $results[$arrayTypeProcedure[$idTypeProcedure]] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom[$arrayTypeProcedure[$idTypeProcedure]];
                                }
                            }
                        }
                        $dataStats[$entiteName] = $results;
                    }
                }
                if ($this->_pourcentage) {
                    if (is_array($dataStats[$entiteName])) {
                        $value = '';
                        foreach ($dataStats[$entiteName] as $key => $value) {
                            if ($denom[$key] > 0) {
                                $res[$key] = $dataStats[$entiteName][$key] / $denom[$key];
                            } else {
                                $res[$key] = 0;
                            }
                        }
                        $results = $res;
                    }
                    $dataStats[$entiteName] = $results;
                    $denom = [];
                }
            } else {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $organismes = Atexo_Organismes::retrieveOrganismes();
                $arrayInitResults = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(isset($_GET['allOrgs']));
                self::isPourcentage();
                foreach ($organismes as $organisme) {
                    if ($organisme instanceof CommonOrganisme && 'org-mpe-3_0' != $organisme->getAcronyme()) {
                        $criteria->setOrganisme($organisme->getAcronyme());
                        $values = self::getResultatStatistique($criteria, true, true);
                        $arrayTypeProcedure = (new Atexo_Consultation_ProcedureType())->rerieveAllTypeProcedureAndTypeProcedurePortail($organisme->getAcronyme());
                        $results = $arrayInitResults;
                        foreach ($values as $idTypeProcedure => $nombreConsultation) {
                            if (!$this->_pourcentage && !isset($results[$arrayTypeProcedure[$idTypeProcedure]])) {
                                $results[$arrayTypeProcedure[$idTypeProcedure]] = 0;
                            }
                            if ('total' == $idTypeProcedure) {
                                $results['total'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['total'];
                                }
                            } elseif ('grandTotal' == $idTypeProcedure) {
                                $results['grandTotal'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['grandTotal'];
                                }
                            } elseif ('totalMapa' == $idTypeProcedure) {
                                $results['totalMapa'] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom['totalMapa'];
                                }
                            } else {
                                $results[$arrayTypeProcedure[$idTypeProcedure]] += $nombreConsultation;
                                if (is_numeric($nombreConsultation)) {
                                    ++$denom[$arrayTypeProcedure[$idTypeProcedure]];
                                }
                            }
                        }
                        $dataStats[$organisme->getAcronyme().'#'.$organisme->getDenominationOrg()] = $results;
                    }
                }
            }
        } else {
            if ('myEntity' == $param->CommandName) {
                if ('0' === $service || 0 === $service) {
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                    if ($organisme instanceof CommonOrganisme) {
                        $entiteName = $organisme->getDenominationOrg();
                    }
                } else {
                    $entite = Atexo_EntityPurchase::retrieveEntityById($service, $acronymeOrganisme);
                    if ($entite instanceof CommonService) {
                        $entiteName = $entite->getLibelle();
                    }
                }
                $results = self::getResultatStatistique($criteria, true, false); //print_r($results);exit;
                if ($this->_pourcentage) {
                    $resultForTotalMapa = self::getResultatStatistique($criteria, true, true);
                    //                          $critere = self::getCriteriaSearch(Atexo_CurrentUser::getCurrentOrganism());
                    //                          $resultForTotalMapa = self::getResultatStatistique($critere, true, true);print_r($resultForTotalMapa);exit;
                    $results['totalMapa'] = $resultForTotalMapa['totalMapa'];
                    $results['total'] = $resultForTotalMapa['total'];
                }
                $dataStats[$entiteName] = $results; //print_r($dataStats);exit;
                $nomEntite = $entiteName.' ,'.$nomPf;
            } else {
                $denom = [];
                $arrayChilds = [];
                if (0 != $criteria->getSelectedEntity()) {
                    $services = Atexo_EntityPurchase::getAllChilds($criteria->getSelectedEntity(), $arrayChilds, $acronymeOrganisme);
                } else {
                    $services = (new Atexo_EntityPurchase())->getAllServices($acronymeOrganisme);
                    $serviceSelected = (new Atexo_EntityPurchase())->getEntityById($this->entitesAchat->SelectedValue, $acronymeOrganisme);
                    $service0 = new CommonService();
                    $service0->setId(0);
                    $service0->setLibelle($serviceSelected->getDenominationOrg());
                    $service0->setCheminComplet($serviceSelected->getDenominationOrg());
                    array_unshift($services, $service0);
                }
                $arrayInitResults = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(isset($_GET['allOrgs']), $acronymeOrganisme);
                foreach ($services as $service) {
                    $results = $arrayInitResults;
                    $criteria->setIdService('IN ('.$service->getId().')');
                    $values = self::getResultatStatistique($criteria, true, false);
                    foreach ($values as $idTypeProcedure => $nombreConsultation) {
                        $results[$idTypeProcedure] = $nombreConsultation;
                    }
                    ////////////////////
                    if ($this->_pourcentage) {
                        $resultForTotalMapa = self::getResultatStatistique($criteria, true, true);
                        $results['totalMapa'] = $resultForTotalMapa['totalMapa'];
                        $results['total'] = $resultForTotalMapa['total'];
                    }
                    foreach ($results as $idTypeProcedure => $nombreConsultation) {
                        if ('total' == $idTypeProcedure) {
                            $results['total'] = $nombreConsultation;
                            if (is_numeric($nombreConsultation)) {
                                ++$denom['total'];
                            }
                        } elseif ('grandTotal' == $idTypeProcedure) {
                            $results['grandTotal'] = $nombreConsultation;
                            if (is_numeric($nombreConsultation)) {
                                ++$denom['grandTotal'];
                            }
                        } elseif ('totalMapa' == $idTypeProcedure) {
                            $results['totalMapa'] = $nombreConsultation;
                            if (is_numeric($nombreConsultation)) {
                                ++$denom['totalMapa'];
                            }
                        } else {
                            $results[$idTypeProcedure] = $nombreConsultation;
                            if (is_numeric($nombreConsultation)) {
                                ++$denom[$idTypeProcedure];
                            }
                        }
                    }
                    $dataStats[$service->getCheminComplet()] = $results;
                }
                $nomEntite = 'Détail des entités';
            }
        }

        //Recuperation de l'objet de la recherche pour l'affichage dans l'excel
        $objetRecherche = '';
        $valeurCumule = '';
        $intervalRecherche = '';
        if ($criteria instanceof Atexo_Statistiques_CriteriaVo) {
            $objetRecherche = $criteria->getObjetRecherche();
            $intervalRecherche = $criteria->getIntervalleRechercheBlocRecap();
            $valeurCumule = $criteria->getValeursCumuleesBlocRecap();
        }

        (new Atexo_GenerationExcel())->genererExcelStatistiquesRechercheAvance($acronymeOrganisme, $dataStats, $objetRecherche, $intervalRecherche, $valeurCumule, $nomEntite, isset($_GET['allOrgs']), $this->_pourcentage, $denom, $afficherCoordonnees);
    }

    /**
     * Invoque la methode qui execute la requete et recupere les donnees pour traitement ulterieur.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteria:             objet contenant les criteres de la recherche
     * @param bool                          $groupByTypeProcedure: precise si l'on doit grouper par type de procedure (true si oui, false sinon)
     * @param bool                          $forAllOrgs:           precise si la requete concerne tous la synthese des donnees concernant toute l'organisme (true si oui, false sinon)
     *
     * @return array: tableau contenant les donnees de la requete
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getResultatStatistique($criteria, $groupByTypeProcedure = false, $forAllOrgs = false)
    {
        $results = null;
        if ($this->objetRecherche_1->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_PROCEDURES'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_24->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATIONS_NON_ALLOTIES'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteria->setAlloti(0);
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_25->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATIONS_ALLOTIES'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteria->setAlloti(1);
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_2->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NOMBRE_LOT'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreLots($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_3->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_AVIS_ATTRIBUTION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreAvis($criteria, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_4->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_AVIS_PRE_INFORMATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreAvis($criteria, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_5->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_DCE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreDces($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_6->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_TOTALE_TELECHARGEMENT_DCE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_7->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_TELECHAREMENT_DCE_OE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreTelechargementParOeAvecCompte($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_8->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_TELECHAREMENT_DCE_RAPPORTE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargementDCERaportNombreDCE($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_9->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_QUESTION_POSE'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreQuestionPosees($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_10->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_REPONSES_ELECTRONIQUES_DEPOSE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_11->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_PROCEDURE_AUCUN_DEPOT'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreProcedureSansReponse($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_12->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_PROCEDURE_AU_MOINS_DEPOT'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreProcedureAvecAuMoinsUneReponse($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_13->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATION_CLASSE_SANS_SUITE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationSansSuite($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_14->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATION_CLASSE_INFRUCTUEUSES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationInfructueuse($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_15->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATION_NOTIFIE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationNotifies($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_16->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATION_NOTIFIE_PLATE_FORME'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationNotifiesPlatForme($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_17->checked) {
            $criteria->setObjetRecherche(Prado::localize('TEXT_NOMBRE_DCE_COMPLETS'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreDceComplets($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_18->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBR_REPONSE_OBLIGATOIRE'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNbreReponseObligatoire($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_19->checked) {
            $criteria->setObjetRecherche(Prado::localize('TEXT_POURCENTAGE_DEPOT_ELECTRONIQUE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getPourcentageDepotElectronique($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_20->checked) {
            $criteria->setObjetRecherche(Prado::localize('TEXT_POURCENTAGE_DEPOT_ELECTRONIQUE_SIGNATURE_PLIS_AU_MOMENT_REPONSE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getPourcentageDepotElectroniqueWithInstantSignature($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_21->checked) {
            $criteria->setObjetRecherche(Prado::localize('TEXT_POURCENTAGE_DEPOT_ELECTRONIQUE_SIGNATURE_PLIS_NOT_AU_MOMENT_REPONSE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getPourcentageReponseObligatoireSansSignature($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_22->checked) {
            $criteria->setObjetRecherche(Prado::localize('TEXT_NBRE_CONSULTATIONS_WITH_ELECTRONIQUE_RESPONSE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationsWithReponsesElectroniqueRequise($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_23->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NOMBRE_TELECHARGEMENT_ANONYME_DCE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargementsAnonymesDCE($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_2->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NOMBRE_TELECHARGEMENT_ANONYME_DCE'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargementsAnonymesDCE($criteria, $groupByTypeProcedure, $forAllOrgs);
        //Objet de recherche rajouté depuis le 12-05-2017 dans le cadre de la MPE-3087
        } elseif ($this->objetRecherche_26->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONSULTATION_POUR_LESQUELLS_LA_SIGNATURE_ELETRONIQUE_EST_AUTORISEE'));
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationsWithReponsesElectroniqueAutorisee($criteria, $groupByTypeProcedure, $forAllOrgs);
        } elseif ($this->objetRecherche_27->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_28->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_NOTIFIES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_29->checked) {
            $criteria->setContratTitulairePme(true);
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_NOTIFIES_PME'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_30->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_MULTIATTRIBUTAIRES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        //Objet de recherche rajouté depuis le 05-11-2017 dans le cadre de la MPE-3088
        } elseif ($this->objetRecherche_32->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_NON_COMMANDES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_33->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_COMMANDES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_34->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_CLOTURES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        } elseif ($this->objetRecherche_35->checked) {
            $criteria->setObjetRecherche(Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_SUPPRIMES'));
            $results = (new Atexo_Statistiques_RequetesStatistiques())->nombreContrats($criteria);
        }
        $this->setViewState('criteres', clone $criteria);
        $this->gererAffichageBlocRecap();

        return $results;
    }

    /**
     * Affiche la page de selection des criteres de la recherche pour modification.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function modifierRecherche()
    {
        $this->resultat->setVisible(false);
        $this->recherche->setDisplay('Dynamic');
        $scriptJs = '<script type="text/javascript">';
        $scriptJs .= 'updatePanelRecherche(document.getElementById("ctl0_CONTENU_PAGE_statsType"));';
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $scriptJs .= ' updateRechercheParClause();';
        }
        $scriptJs .= '</script>';
        $this->javascript->Text = $scriptJs;
    }

    public function redirectToSelf()
    {
        $this->response->redirect('?page=Agent.StatistiquesRechercheAvance'.((isset($_GET['allOrgs'])) ? '&allOrgs' : ''));
    }

    public function getNumber($val, $decimal, $pourcent = false)
    {
        if (!is_numeric($val)) {
            return $val;
        }
        $number = number_format($val, $decimal, ',', ' ');

        if ($pourcent) {
            $number .= '%';
        }

        return $number;
    }

    public function isPourcentage()
    {
        if (true == $this->objetRecherche_19->checked
            || true == $this->objetRecherche_20->checked
            || true == $this->objetRecherche_21->checked) {
            $this->_pourcentage = true;
            $this->_decimal = 2;
        } else {
            $this->_pourcentage = false;
            $this->_decimal = 0;
        }
    }

    public function getPercentValue($tabNbr, $results)
    {
        foreach ($tabNbr as $key => $value) {
            if (0 != $value) {
                $results[$key] /= $tabNbr[$key];
            }
        }

        return $results;
    }

    /**
     * Permet de charger les categories des statistiques.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function displayCategoriesStatistiques()
    {
        $withoutChorus = true;
        //on affiche dans la liste des statistiques l'interface PLACE-CHORUS si le service de l'agent connecté est rattaché à Chorus ou qu'il s'agit d'un hyperadmin connecté
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism()) || isset($_GET['allOrgs'])) {
            $withoutChorus = false;
        }

        $valeursRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielStats($withoutChorus);
        $data = [];
        if ($valeursRef) {
            foreach ($valeursRef as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->setViewState('dataRefCategoryStats', $data);
        $this->statsType->DataSource = $data;
        $this->statsType->DataBind();
    }

    /**
     * Permet de recuperer des infos du bloc recapitulatif de recherche pour les mettre dans les criteres de recherche.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteria: objet criteres de la recherche
     *
     * @return Atexo_Statistiques_CriteriaVo: objet criteres de la recherche
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteriaSearchBlocRecap($criteria = null)
    {
        //Recuperation du criteriaVo
        if (null === $criteria) {
            $criteria = $this->getViewState('criteres');
        }
        if ($criteria instanceof Atexo_Statistiques_CriteriaVo) {
            $intervalRecherche = '';
            if (!$this->searchType()) {
                if ($this->critereFiltre_1->checked) {
                    if ($criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_MISE_ENLIGNE').' '.Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
                    } elseif ($criteria->getDateMiseEnLigneStart() && !$criteria->getDateMiseEnLigneEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_MISE_ENLIGNE').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart());
                    } elseif (!$criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_MISE_ENLIGNE').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
                    }
                } elseif ($this->critereFiltre_2->checked) {
                    if ($criteria->getDateRemisePlisStart() && $criteria->getDateRemisePlisEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_LIMITE_REMISE_PLIS').' '.Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateRemisePlisStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateRemisePlisEnd());
                    } elseif ($criteria->getDateRemisePlisStart() && !$criteria->getDateRemisePlisEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_LIMITE_REMISE_PLIS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDateRemisePlisStart());
                    } elseif (!$criteria->getDateRemisePlisStart() && $criteria->getDateRemisePlisEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_PROCEDURE_DATE_LIMITE_REMISE_PLIS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateRemisePlisEnd());
                    }
                } /*else if ($this->searchType() && $this->critereFiltre_3->checked) {
                    $intervalRecherche = Prado::localize('DEFINE_PROCEDURE_EN_LIGNE_AUJOURDHUI');
                } */ else {
                    $intervalRecherche = '';
                }
            } else {
                if ($this->critereFiltre_4->checked) {
                    if ($criteria->getDatePrevisionnelleNotificationStart() && $criteria->getDatePrevisionnelleNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_NOTIFICATION').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleNotificationStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleNotificationEnd());
                    } elseif ($criteria->getDatePrevisionnelleNotificationStart() && !$criteria->getDatePrevisionnelleNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_NOTIFICATION_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleNotificationStart());
                    } elseif (!$criteria->getDatePrevisionnelleNotificationStart() && $criteria->getDatePrevisionnelleNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_NOTIFICATION_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleNotificationEnd());
                    }
                } elseif ($this->critereFiltre_5->checked) {
                    if ($criteria->getDatePrevisionnelleFinMarcheStart() && $criteria->getDatePrevisionnelleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MARCHE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMarcheStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMarcheEnd());
                    } elseif ($criteria->getDatePrevisionnelleFinMarcheStart() && !$criteria->getDatePrevisionnelleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MARCHE_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMarcheStart());
                    } elseif (!$criteria->getDatePrevisionnelleFinMarcheStart() && $criteria->getDatePrevisionnelleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MARCHE_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMarcheEnd());
                    }
                } elseif ($this->critereFiltre_6->checked) {
                    if ($criteria->getDatePrevisionnelleFinMaximaleMarcheStart() && $criteria->getDatePrevisionnelleFinMaximaleMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MAXIMALE_MARCHE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMaximaleMarcheStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMaximaleMarcheEnd());
                    } elseif ($criteria->getDatePrevisionnelleFinMaximaleMarcheStart() && !$criteria->getDatePrevisionnelleFinMaximaleMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MAXIMALE_MARCHE_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMaximaleMarcheStart());
                    } elseif (!$criteria->getDatePrevisionnelleFinMaximaleMarcheStart() && $criteria->getDatePrevisionnelleFinMaximaleMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_PREVISIONELLE_FIN_MAXIMALE_MARCHE_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDatePrevisionnelleFinMaximaleMarcheEnd());
                    }
                } elseif ($this->critereFiltre_7->checked) {
                    if ($criteria->getDateDefinitiveNotificationStart() && $criteria->getDateDefinitiveNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_DEFINITIVE_NOTIFICATION').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateDefinitiveNotificationStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateDefinitiveNotificationEnd());
                    } elseif ($criteria->getDateDefinitiveNotificationStart() && !$criteria->getDateDefinitiveNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_DEFINITIVE_NOTIFICATION_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDateDefinitiveNotificationStart());
                    } elseif (!$criteria->getDateDefinitiveNotificationStart() && $criteria->getDateDefinitiveNotificationEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DATE_DEFINITIVE_NOTIFICATION_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateDefinitiveNotificationEnd());
                    }
                } elseif ($this->critereFiltre_8->checked) {
                    if ($criteria->getDateDefinitiveFinMarcheStart() && $criteria->getDateDefinitiveFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DEFINITIVE_FIN_MARCHE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateDefinitiveFinMarcheStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateDefinitiveFinMarcheEnd());
                    } elseif ($criteria->getDateDefinitiveFinMarcheStart() && !$criteria->getDateDefinitiveFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DEFINITIVE_FIN_MARCHE_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDateDefinitiveFinMarcheStart());
                    } elseif (!$criteria->getDateDefinitiveFinMarcheStart() && $criteria->getDateDefinitiveFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_CONTRAT_DEFINITIVE_FIN_MARCHE_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateDefinitiveFinMarcheEnd());
                    }
                } elseif ($this->critereFiltre_9->checked) {
                    if ($criteria->getDateMaximaleFinMarcheStart() && $criteria->getDateMaximaleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_DATE_MAXIMALE_FIN_MARCHE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateMaximaleFinMarcheStart()).' '
                            .Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE').' '
                            .Atexo_Util::iso2frnDate($criteria->getDateMaximaleFinMarcheEnd());
                    } elseif ($criteria->getDateMaximaleFinMarcheStart() && !$criteria->getDateMaximaleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_DATE_MAXIMALE_FIN_MARCHE_RECAP_STATS').' '.'à partir du '.Atexo_Util::iso2frnDate($criteria->getDateMaximaleFinMarcheStart());
                    } elseif (!$criteria->getDateMaximaleFinMarcheStart() && $criteria->getDateMaximaleFinMarcheEnd()) {
                        $intervalRecherche .= Prado::localize('DEFINE_DATE_MAXIMALE_FIN_MARCHE_RECAP_STATS').' '."jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateMaximaleFinMarcheEnd());
                    }
                } else {
                    $intervalRecherche = '';
                }
            }

            if (!isset($_GET['allOrgs'])) {
                $criteria->setEntiteAchatBlocRecap($this->entitesAchat->getSelectedItem()->getText());
                if ($this->cumulValeurs->Checked) {
                    $criteria->setValeursCumuleesBlocRecap('/ Valeurs cumulées');
                }
            } else {
                $criteria->setEntiteAchatBlocRecap(Prado::localize('TEXT_PLATEFORME_PMI'));
            }
            $criteria->setIntervalleRechercheBlocRecap($intervalRecherche);
        }

        return $criteria;
    }

    /**
     * Permet de gerer l'affichage du bloc recapitulatif des criteres de recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function gererAffichageBlocRecap()
    {
        $criteria = $this->getViewState('criteres');
        if ($criteria instanceof Atexo_Statistiques_CriteriaVo) {
            $this->panelIntervalRecherche->setVisible(true);
            if (($this->critereFiltre_1->checked && !$this->critereFiltre_1_dateStart->Text && !$this->critereFiltre_1_dateEnd->Text)
                || ($this->critereFiltre_2->checked && !$this->critereFiltre_2_dateStart->Text && !$this->critereFiltre_2_dateEnd->Text)
                    || ($this->critereFiltre_4->checked && !$this->critereFiltre_4_dateStart->Text && !$this->critereFiltre_4_dateEnd->Text)
                        || ($this->critereFiltre_5->checked && !$this->critereFiltre_5_dateStart->Text && !$this->critereFiltre_5_dateEnd->Text)
                            || ($this->critereFiltre_6->checked && !$this->critereFiltre_6_dateStart->Text && !$this->critereFiltre_6_dateEnd->Text)
                                || ($this->critereFiltre_7->checked && !$this->critereFiltre_7_dateStart->Text && !$this->critereFiltre_7_dateEnd->Text)
                                    || ($this->critereFiltre_8->checked && !$this->critereFiltre_8_dateStart->Text && !$this->critereFiltre_8_dateEnd->Text)
                                        || ($this->critereFiltre_9->checked && !$this->critereFiltre_9_dateStart->Text && !$this->critereFiltre_9_dateEnd->Text)) {
                $this->panelIntervalRecherche->setVisible(false);
            } elseif (!$this->critereFiltre_1->checked
                && !$this->critereFiltre_2->checked
                    && !$this->critereFiltre_3->checked
                        && !$this->critereFiltre_4->checked
                            && !$this->critereFiltre_5->checked
                                && !$this->critereFiltre_6->checked
                                    && !$this->critereFiltre_7->checked
                                        && !$this->critereFiltre_8->checked
                                            && !$this->critereFiltre_9->checked) {
                $this->panelIntervalRecherche->setVisible(false);
            }

            if (isset($_GET['allOrgs'])) {
                $this->panelEntiteAchatRecap->setVisible(false);
            } else {
                $this->panelEntiteAchatRecap->setVisible(true);
                $this->entiteRecap->Text = $criteria->getEntiteAchatBlocRecap();

                if ($this->cumulValeurs->Checked) {
                    $this->valeurCumuleRecap->Text = $criteria->getValeursCumuleesBlocRecap();
                } else {
                    $this->valeurCumuleRecap->Text = '';
                }
            }
        }
    }

    public function searchType()
    {
        $this->setViewState('objetDeLaRecherche', '' != $_POST['objetRecheche'] ? $_POST['objetRecheche'] : $this->getViewState('objetDeLaRecherche'));
        if (('Contrats' == $this->getViewState('objetDeLaRecherche')) || ('Interface PLACE-CHORUS' == $this->getViewState('objetDeLaRecherche'))) {
            return true;
        } else {
            return false;
        }
    }

    public function estRechercheObjetContrat()
    {
        $this->setViewState('objetDeLaRecherche', '' != $_POST['objetRecheche'] ? $_POST['objetRecheche'] : $this->getViewState('objetDeLaRecherche'));
        if ('Contrats' == $this->getViewState('objetDeLaRecherche')) {
            return true;
        } else {
            return false;
        }
    }

    public function estRechercheObjetContratChorus()
    {
        $this->setViewState('objetDeLaRecherche', '' != $_POST['objetRecheche'] ? $_POST['objetRecheche'] : $this->getViewState('objetDeLaRecherche'));
        if ('Interface PLACE-CHORUS' == $this->getViewState('objetDeLaRecherche')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de generer l'excel des statistiques pour le contrat.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function genererExcelStatistiquesContrats($sender, $param)
    {
        $entityName = null;
        $criteresRecherche = null;
        try {
            $dataStats = [];
            $avecDetailsEntites = false;
            $addColsSupplementaires = false;
            $showPfShortName = false;
            $criteresRecherche = $this->getViewState('criteres');
            if ($criteresRecherche instanceof Atexo_Statistiques_CriteriaVo) {
                switch ($param->CommandName) {
                    case 'myEntity':
                        if ($this->forStatsTransverses()) {
                            $showPfShortName = true;
                            $entityName = Prado::localize('TEXT_PLATEFORME_PMI');
                            $criteresRecherche->setIdService('');
                            $organismes = Atexo_Organismes::retrieveOrganismes();
                            $datasQueryAllOrg = [];
                            foreach ($organismes as $organisme) {
                                if ($organisme instanceof CommonOrganisme) {
                                    $criteresRecherche->setOrganisme($organisme->getAcronyme());
                                    $datasQuery = Atexo_Statistiques_RequetesStatistiques::nombreContratsStatsExportExcel($criteresRecherche);
                                    foreach ($datasQuery as $idTypeContrat => $nombreContrats) {
                                        $datasQueryAllOrg[$idTypeContrat]['nombreContrats'] += $nombreContrats['nombreContrats'];
                                        $datasQueryAllOrg[$idTypeContrat]['libelleTypeContrat'] = $nombreContrats['libelleTypeContrat'];
                                    }
                                }
                            }
                            $dataStats[$entityName] = $datasQueryAllOrg;
                        } else {
                            if ('0' === (string) $criteresRecherche->getSelectedEntity()) {
                                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                                if ($organisme instanceof CommonOrganisme) {
                                    $entityName = $organisme->getDenominationOrg();
                                }
                            } else {
                                $entite = Atexo_EntityPurchase::retrieveEntityById($criteresRecherche->getSelectedEntity(), Atexo_CurrentUser::getCurrentOrganism());
                                if ($entite instanceof CommonService) {
                                    $entityName = $entite->getLibelle();
                                }
                            }
                            $datasQuery = Atexo_Statistiques_RequetesStatistiques::nombreContratsStatsExportExcel($criteresRecherche);
                            $dataStats[$entityName] = $datasQuery;
                        }
                        break;
                    case 'otherEntities':
                        if ($this->forStatsTransverses()) {
                            $organismes = Atexo_Organismes::retrieveOrganismes();
                            if (!empty($organismes)) {
                                foreach ($organismes as $organisme) {
                                    if ($organisme instanceof CommonOrganisme && 'org-mpe-3_0' != $organisme->getAcronyme()) {
                                        $criteresRecherche->setOrganisme($organisme->getAcronyme());
                                        $datasQuery = Atexo_Statistiques_RequetesStatistiques::nombreContratsStatsExportExcel($criteresRecherche, true);
                                        $dataStats[$organisme->getDenominationOrg()] = $datasQuery;
                                    }
                                }
                            }
                            $addColsSupplementaires = true;
                        } else {
                            $arrayChilds = [];
                            if ('0' != (string) $criteresRecherche->getSelectedEntity()) {
                                $services = Atexo_EntityPurchase::getAllChilds($criteresRecherche->getSelectedEntity(), $arrayChilds, Atexo_CurrentUser::getCurrentOrganism());
                            } else {
                                $services = (new Atexo_EntityPurchase())->getAllServices(Atexo_CurrentUser::getCurrentOrganism());
                                $serviceSelected = (new Atexo_EntityPurchase())->getEntityById($this->entitesAchat->SelectedValue, Atexo_CurrentUser::getCurrentOrganism());
                                $service0 = new CommonService();
                                $service0->setId(0);
                                $service0->setLibelle($serviceSelected->getDenominationOrg());
                                $service0->setCheminComplet($serviceSelected->getDenominationOrg());
                                array_unshift($services, $service0);
                            }
                            foreach ($services as $service) {
                                if ($service instanceof CommonService) {
                                    $criteresRecherche->setIdService('IN ('.$service->getId().')');
                                    $datasQuery = Atexo_Statistiques_RequetesStatistiques::nombreContratsStatsExportExcel($criteresRecherche);
                                    $dataStats[$service->getCheminComplet()] = $datasQuery;
                                }
                            }
                        }
                        $avecDetailsEntites = true;
                        break;
                }
            }
            $arrayKeys = array_keys($dataStats);

            //Debut ajout de l'entete (le but est d'ajouter uniquement les libelles des colonnes dynamiques qui sont les types de contrats)
            $firstKeyDataStats = array_shift($arrayKeys);
            $firstElementDataStats = array_shift($dataStats);
            $arrayEnteteDataStats = ['entete' => $firstElementDataStats];
            $arrayFirstElementDataStats = [$firstKeyDataStats => $firstElementDataStats];
            $dataStats = array_merge($arrayEnteteDataStats, $arrayFirstElementDataStats, $dataStats);
            //Fin ajout de l'entete

            Atexo_GenerationExcel::genererExcelStatistiquesContratsRechercheAvance($dataStats, $criteresRecherche, $avecDetailsEntites, $addColsSupplementaires, $showPfShortName);
        } catch (Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la generation de l'excel des statistiques pour les contrats. ".PHP_EOL.'Exception : '.$exception->getMessage().PHP_EOL.' Objet Critere: '.PHP_EOL.print_r($criteresRecherche, true));
        }
    }

    /**
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    private function forStatsTransverses()
    {
        return isset($_GET['allOrgs']);
    }

    /**
     * @return string|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getValueHiddenObjetRecherche()
    {
        return '' != $this->getViewState('objetDeLaRecherche') ? $this->getViewState('objetDeLaRecherche') : 'Consultations';
    }

    public function displayClauses()
    {
        $arrayClausesSociales = [Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesSociales = Atexo_Util::arrayUnshift($arrayClausesSociales, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseSociales->DataSource = $arrayClausesSociales;
        $this->clauseSociales->DataBind();

        $arrayClausesEnv = [Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesEnv = Atexo_Util::arrayUnshift($arrayClausesEnv, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseEnvironnementale->DataSource = $arrayClausesEnv;
        $this->clauseEnvironnementale->DataBind();
    }
}
