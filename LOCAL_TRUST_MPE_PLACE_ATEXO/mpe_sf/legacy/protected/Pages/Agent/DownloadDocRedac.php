<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Permet de télécharger le document REDAC.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadDocRedac extends DownloadFile
{
    private $_reference;
    private $_consultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['id']) && isset($_GET['IdBlob']) && isset($_GET['nomDoc'])) {
            $idSeviceAgent = Atexo_CurrentUser::getCurrentServiceId();
            $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            /**
             * Seul les agents ayant le droit de voir la consultation qui peuvent télécharger la pièce redac.
             */
            $consultation = new Atexo_Consultation();
            $consultationCriteria = new Atexo_Consultation_CriteriaVo();
            $consultationCriteria->setIdReference($this->_reference);
            $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $consultationCriteria->setIdService($idSeviceAgent);
            $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultationFound = $consultation->search($consultationCriteria);

            if (is_array($consultationFound) && count($consultationFound) > 0) {
                $this->_consultation = $consultationFound[0];
                if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                    $redaction = new Atexo_Redaction($this->_consultation);
                    $this->_nomFichier = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['nomDoc']));
                    $atexoBlob = new Atexo_Blob();
                    $idBlob = $atexoBlob->insert_blob(
                        Atexo_Util::atexoHtmlEntities(base64_decode($_GET['nomDoc'])),
                        $redaction->getContentDocument(
                            Atexo_Util::atexoHtmlEntities($_GET['IdBlob']),
                            Atexo_Util::atexoHtmlEntities(base64_decode($_GET['nomDoc'])),
                            Atexo_CurrentUser::getCurrentOrganism()
                        ),
                        Atexo_CurrentUser::getCurrentOrganism()
                    );
                    if (0 != $idBlob) {
                        $this->_idFichier = $idBlob;
                        $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getCurrentOrganism());
                    } else {
                        $this->response->redirect('index.php?page=Agent.AgentHome');
                    }
                } else {
                    $this->response->redirect('index.php?page=Agent.AgentHome');
                }
            }
        } else {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        }
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }
}
