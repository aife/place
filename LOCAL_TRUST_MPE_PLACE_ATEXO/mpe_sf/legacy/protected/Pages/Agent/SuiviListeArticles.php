<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Marche\Atexo_Marche_Annee;
use Application\Service\Atexo\Marche\Atexo_Marche_CriteriaVo;

/**
 * Page de suivi de la liste des marches - Article 133.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SuiviListeArticles extends MpeTPage
{
    public $_annee;
    public $_idServiceAgent;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {

        if (!$this->isPostBack) {
            $this->fillListYear((new Atexo_Marche_Annee())->getYearToDisplay());
        }
    }

    public function getAnnee()
    {
        if (null == $this->_annee) {
            $this->_annee = $this->annee->selectedValue;
        }

        return $this->_annee;
    }

    public function getIdServiceAgent()
    {
        if (null == $this->_idServiceAgent) {
            $this->_idServiceAgent = Atexo_CurrentUser::getCurrentServiceId();
        }

        return $this->_idServiceAgent;
    }

    public function migrateData()
    {
        //dans tous les cas (saisie ou la validation) on sauvegarde en base que l'année est démarrée en saise
        //et on copie de la base des decisions les caracteristiques du marchés (mode modification ou mode création)
        $atexoMarche = new Atexo_Marche();
        $marchePublier = $atexoMarche->saveSelectedYear($this->getIdServiceAgent(), $this->getAnnee(), Atexo_CurrentUser::getCurrentOrganism());
        if ('1' == $marchePublier->getNewversion()) {
            $atexoMarche->saveMarche($this->getIdServiceAgent(), $this->getAnnee(), Atexo_CurrentUser::getCurrentOrganism());
        }
        //afficher les états et actions quand processus de validation
        if (isset($_GET['valider'])) { // valable juste pour la saisie de liste marchés
            $this->panelBlocAnnee->setVisible(false);
            $this->panelEtat->setVisible(true);
            $this->panelAction->setVisible(false);
        }
        $this->setVisibleControls();
    }

    public function setVisibleControls()
    {
        $this->setVisbleBlocYear();
        $this->setVisibleLabelYear();
        $this->panelLegende->setVisible(true);
        $this->tabNav->setVisible(true);
        $this->addCriteria();
        $criteria = $this->getViewState('critere');
        $atexoMarche = new Atexo_Marche();
        $arrayMarches = $atexoMarche->search($criteria);
        $countResult = is_countable($arrayMarches) ? count($arrayMarches) : 0;
        if (0 == $countResult) {
            $this->panelcountResult->setVisible(false);
            $this->panelmsgResult->setVisible(true);
            $this->panelpartitioner1->setVisible(false);
            $this->panelpartitioner2->setVisible(false);
        //$this->panelRefresh->setVisible(false);
        } else {
            $this->nbrResultats->Text = $countResult;
        }
        $this->repeaterListMarches->setVirtualItemCount($countResult);
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->nombrePageTop->Text = ceil($countResult / $this->repeaterListMarches->PageSize);
        $this->nombrePageBottom->Text = ceil($countResult / $this->repeaterListMarches->PageSize);

        $this->populateData();
    }

    /***
     * Remplir la list des années à afficher
     * @param data source de la list
     */
    public function fillListYear($dataSource)
    {
        $this->annee->dataSource = $dataSource;
        $this->annee->dataBind();
    }

    /***
    * Rendre le bloc de séléction de l'année invisible
    */
    public function setVisbleBlocYear()
    {
        $this->annee->setVisible(false);
        $this->buttonOk->setVisible(false);
    }

    /***
     * Rendre le label de l'année séléctionnée visible
     */
    public function setVisibleLabelYear()
    {
        $this->yearSelected->setVisible(true);
        $this->yearSelected->setText($this->getAnnee());
    }

    /***
     * Remplir le repeater avec la liste des marches
     */
    public function fillRepeaterListeMarches($dataSource)
    {
        $this->repeaterListMarches->dataSource = $dataSource;
        $this->repeaterListMarches->dataBind();
    }

    public function addCriteria()
    {
        $criteriaVo = new Atexo_Marche_CriteriaVo();
        $criteriaVo->setNumeroMarcheAnnee($this->getAnnee());
        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaVo->setIdService($this->getIdServiceAgent());
        $criteriaVo->setSortByElement('natureMarche');
        $this->setViewState('critere', $criteriaVo);
    }

    private function populateData()
    {
        $offset = $this->repeaterListMarches->CurrentPageIndex * $this->repeaterListMarches->PageSize;
        $limit = $this->repeaterListMarches->PageSize;
        if ($offset + $limit > $this->repeaterListMarches->getVirtualItemCount()) {
            $limit = $this->repeaterListMarches->getVirtualItemCount() - $offset;
        }

        $criteriaVo = $this->getViewState('critere');
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);

        $atexoMarche = new Atexo_Marche();
        $arrayMarches = $atexoMarche->search($criteriaVo);
        $this->fillRepeaterListeMarches($arrayMarches);
    }

    public function pageChanged($sender, $param)
    {
        $this->repeaterListMarches->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->populateData();
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->repeaterListMarches->PageSize = $pageSize;
        //$nombreElement=$this->getViewState("nombreElement");
        $nombreElement = $this->repeaterListMarches->getVirtualItemCount();
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
        // $criteriaVo=$this->getViewState("criteriaVo");
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->populateData();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterListMarches->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->repeaterListMarches->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->repeaterListMarches->CurrentPageIndex + 1;
        }
    }

    public function refreshRepeater($sender, $param)
    {
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->setVisibleControls();
    }

    public function deleteMarche($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMarchePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $idMarcheToDelete = $param->CommandParameter;
        $c->add(CommonMarchePeer::ID, $idMarcheToDelete);
        CommonMarchePeer::doDelete($c, $connexionCom);

        //$this->repeaterListMarches->setCurrentPageIndex(0);
        $this->setVisibleControls();
    }

    public function refreshAfterDelete($sender, $param)
    {
        $this->panelRefresh->render($param->NewWriter);
        $this->panelpartitioner1->render($param->NewWriter);
        $this->panelpartitioner2->render($param->NewWriter);
        $this->nbrResultats->render($param->NewWriter);
    }

    /***
     * permet de cocher tous les marchés
     * @param $sender
     * @param $param
     */
    public function checkAll($sender, $param)
    {
        foreach ($this->repeaterListMarches->Items as $itemMarche) {
            $itemMarche->toUpdate->checked = $sender->checked;
        }
    }

    public function validerMarches($sender, $param)
    {
        if ('valider' == $param->getCommandName()) {
            $valide = '1';
        } else {
            $valide = '0';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        foreach ($this->repeaterListMarches->Items as $itemMarche) {
            if ($itemMarche->toUpdate->checked) {
                $marche = CommonMarchePeer::retrieveByPK($itemMarche->toUpdate->Value, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                $marche->setValide($valide);
                $marche->save($connexionCom);
            }
        }

        $this->populateData();
    }

    /***
     * trier par colonne
     */
    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->getCommandName();
        $this->setViewState('sortByElement', $champsOrderBy);
        //$this->addCriteria();
        $criteriaVo = $this->getViewState('critere');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('critere', $criteriaVo);
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->populateData();
    }
}
