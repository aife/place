<?php

namespace Application\Pages\Agent;

use Application\Controls\BandeauAgent;
use Application\Controls\MpeTPage;
use Application\Layouts\MainLayout;
use Application\Propel\Mpe\CommonTMessageAccueil;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Prado\Prado;

/**
 * Page d'accueil non authentifié des agents.
 *
 * @author
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AccueilAgentAuthentifieSocleinterne extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom(MainLayout::CALLED_FROM_SOCLE_INTERNE);
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        /**
         * Suppression des cles session SIP / ECO
         */
        Atexo_CurrentUser::deleteFromSession('acces_gestion_publicite');
    }

    public function onLoad($param)
    {
        Atexo_CurrentUser::writeToSession('ServiceMetier', 'socle');
        $this->nomPrenom->Text = (new BandeauAgent())->getNomPrenomUser();
        $this->idMonEntitePublic->Text = Atexo_EntityPurchase::getPathEntityById('0', Atexo_CurrentUser::getOrganismAcronym());
        $this->idMonEntiteAchat->Text = Atexo_EntityPurchase::getPathEntityById(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getOrganismAcronym());
        //$this->alertArchivage->Visible = $this->isFirstOpenDayOfTheMonth();
        if (isset($_GET['changeMDP'])) {
            $this->scriptJavascript->Text = "<script>openModalOnLoad('demander-validation','popup-small2',document.getElementById('ctl0_CONTENU_PAGE_titrePopin'));</script>";
        }
        if (isset($_GET['deleted'])) {
            $this->panelMessage->setVisible(true);
            $this->panelMessage->setMessage(Prado::localize('ACTION_SUPPRESSION_REALISE'));
        }
        $this->displayListeService();

        $message = Atexo_Message::retrieveMessageAccueilByDestinataire('agent', 1);
        $cssClass = 'bloc-message form-bloc-conf msg-';
        if ($message instanceof CommonTMessageAccueil) {
            if (!empty($message->getContenu())) {
                $this->lblMsgAccueilAgentAuthentifie->setText($message->getContenu());
                $this->labelAccueilPonctuel->setCssClass($cssClass.$message->getTypeMessage());
                $this->labelAccueilPonctuel->setVisible(true);
            } else {
                $this->labelAccueilPonctuel->setVisible(false);
            }
        } else {
            if ('MSG_ACCUEIL_PONCTUEL' != Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT')) {
                $this->lblMsgAccueilAgentAuthentifie->setText((new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT')));
                $this->labelAccueilPonctuel->setCssClass($cssClass.Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT_TYPE'));
                $this->labelAccueilPonctuel->setVisible(true);
            } else {
                $this->labelAccueilPonctuel->setVisible(false);
            }
        }
    }

    /**
     * affiche la liste des services disponible pour l'agent connécté.
     */
    public function displayListeService()
    {
        $services = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesOrganisme(Atexo_CurrentUser::getIdAgentConnected(), Atexo_CurrentUser::getOrganismAcronym(), true);
        $this->repeaterListeServices->DataSource = $services;
        $this->repeaterListeServices->DataBind();
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function generateUrlAccesWithSso($sender, $param)
    {
        $params = explode('#__#', $param->CommandName);
        $urlAcces = $params[0];
        $idServiceMetier = $params[1];
        // TODO SOCLE mpe3
        $sso = (new Atexo_Agent())->getSsoForSocle(Atexo_CurrentUser::getId(), Atexo_CurrentUser::getOrganismAcronym(), $idServiceMetier);
        if (strstr($urlAcces, '?')) {
            $separateur = '&';
        } else {
            $separateur = '?';
        }
        $this->response->redirect($urlAcces.$separateur.'sso='.$sso);
    }

    public function accederMonCompte($sender, $param)
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentModifierCompte');
    }
}
