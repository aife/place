<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnchereEntreprisePmi;
use Application\Propel\Mpe\CommonEnchereTranchesBaremeNETC;
use Application\Propel\Mpe\CommonEnchereValeursInitiales;
use Application\Propel\Mpe\CommonParametrageEnchere;
use Application\Propel\Mpe\CommonParametrageEncherePeer;
use Application\Propel\Mpe\CommonParametrageEnchereReference;
use Application\Propel\Mpe\CommonParametrageEnchereTrancheBaremeReference;
use Application\Propel\Mpe\CommonParametrageEnchereTranchesBaremeNETC;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Enchere;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Exception;
use Prado\Prado;

/**
 * Class pour l'affichage de la création/modification d'une enchère.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ParametrerEnchere extends MpeTPage
{
    private $triAscDesc = 'ASC';
    private $_connexionCom;
    //private $nbLots;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $this->getPage()->getClientScript()->registerPradoScript('prado');
    }

    public function getConnexionCom()
    {
        if (null == $this->_connexionCom) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }

        return $this->_connexionCom;
    }

    public function onLoad($param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $urlRedirect = 'index.php?page=Agent.AgentHome';
            $this->response->redirect($urlRedirect);
        }
        if (!$this->isPostBack) {
            $this->formuleCalculBaremeGlobal->dataSource = $this->getTypesBaremeGlobal();
            $this->formuleCalculBaremeGlobal->dataBind();
            $this->formuleCalcul->dataSource = $this->getTypesBaremeEnchere();
            $this->formuleCalcul->dataBind();

            $this->monEntiteAchat->Text = Atexo_EntityPurchase::getPathEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getOrganismAcronym());

            $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
            $this->entiteAssociees->DataSource = $entities;
            $this->entiteAssociees->DataBind();
            $this->entiteAssociees->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());

            $c = new Criteria();
            $c->add(CommonParametrageEncherePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $enchereParametree = CommonParametrageEncherePeer::doSelectOne($c, self::getConnexionCom());
            if ($enchereParametree instanceof CommonParametrageEnchere) {
                // Recuperation des references
                $enchereReferences = $enchereParametree->getCommonParametrageEnchereReferences(null, self::getConnexionCom());

                // Recuperation des tranches du bareme NETC
                if ($enchereParametree->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
                    $tranchesBaremeEnchere = $enchereParametree->getCommonParametrageEncheretranchesbaremenetcs(null, self::getConnexionCom());

                    $this->repeaterTranchesBaremeEnchere->dataSource = $tranchesBaremeEnchere;
                    $this->repeaterTranchesBaremeEnchere->dataBind();
                    $this->setViewState('tranchesBaremeEnchere', $tranchesBaremeEnchere);
                    //$this->refreshApBaremeTranche(null, null);echo '11111111111111';exit;
                }

                // Selection des Bareme enchere et reference
                if ($enchereParametree->getTypebaremenetc()) {
                    $this->formuleCalcul->setSelectedValue($enchereParametree->getTypebaremenetc());
                }
                $this->formuleCalculBaremeGlobal->setSelectedValue($enchereParametree->getTypebaremeenchereglobale());

                $this->setViewState('enchereReferences', $enchereReferences);
                $this->setViewState('enchere', $enchereParametree);

                $this->etapeParametres();
            } else {
                $enchere = new CommonParametrageEnchere();
                $this->setViewState('enchere', $enchere);
                $this->etapeParametres();
            }
        }
        $this->customizeForm();
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     *
     * @return consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $critere->setIdReference($consultationId);
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());

        $consultationArray = (new Atexo_Consultation())->search($critere);
        $consultation = array_shift($consultationArray);

        return $consultation;
    }

    /**
     * Affichage du bloc récapitalatif de la consultation servant de base à l'enchère.
     *
     * @param Consultation $consultation
     */
    public function displayConsultationBlocRecap(CommonConsultation $consultation)
    {
        $lots = [];
								$this->referenceConsultation1->Text = $consultation->getRealReferenceUtilisateur();
        $this->referenceConsultation2->Text = $consultation->getRealReferenceUtilisateur();
        $this->objetConsultationResume->Text = $consultation->getResume();
        $this->entitePubliqueConsultation->Text = Atexo_CurrentUser::getOrganismDesignation();

        $this->entiteAchatConsultation->Text = Atexo_EntityPurchase::getServiceAgentById($consultation->getServiceId(), Atexo_CurrentUser::getOrganismAcronym());
        $this->typeAnnonceConsultation->Text = $consultation->getLibelleTypeAnnonce();
        $this->typeProcedureConsultation->Text = $consultation->getLibelleTypeProcedure();
        $this->intituleConsultationConsultation->Text = $consultation->getTitre();
        $this->objetConsultation->Text = $consultation->getResume();
        $this->categoriePrincipaleConsultation->Text = $consultation->getLibelleCategorieConsultation();
        $this->dateLimiteRemisePlisConsultation->Text = Atexo_Util::iso2frnDateTime($consultation->getDateFin());

        $nbLots = is_countable($consultation->countCategorielots()) ? count($consultation->countCategorielots()) : 0;

        if (0 == $nbLots) {
            $lots[0] = Prado::localize('TEXT_ENV_OFFRE');
        } else {
            $categorieLots = $consultation->getAllLots();
            foreach ($categorieLots as $lot) {
                $lots[$lot->getLot()] = $lot->getDescriptionTraduite();
            }
        }

        $this->envelop->Datasource = $lots;
        $this->envelop->dataBind();
    }

    /**
     * Retourne l'enchère en cour de création/modification.
     *
     * @return ParametrageEnchere
     */
    public function getEnchere()
    {
        return $this->getViewState('enchere');
    }

    public function getLibelleService()
    {
        $idService = $this->getEnchere()->getServiceId();
        if (!$idService) {
            $idService = '0';
        }

        return Atexo_EntityPurchase::getPathEntityById($idService, Atexo_CurrentUser::getOrganismAcronym());
    }

    /**
     * 1 ) Etape de création de l'enchere.
     */
    public function etapeCreation()
    {
        // Selection de la phase
        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(false);
        $this->ongletReferences->setVisible(false);
        $this->ongletSynthese->setVisible(false);
        $this->ongletConfirmation->setVisible(false);

        $this->popupDefinirBaremeEnchere->setVisible(true);
        $this->popupDefinirBaremeGlobal->setVisible(true);
    }

    /**
     * 2 ) Etape de choix des invité.
     */
    public function etapeChoixInvites()
    {
        // Sauvegarde des informations de l'enchere
        $enchere = $this->getEnchere();
        $enchere->setObjet($this->objetEnchereCreation->Text);
        $enchere->setReferenceutilisateur($this->referenceEnchereCreation->Text);
        /*if ($this->entiteAssociees->getSelectedValue() == 0) {
            $enchere->setIdentiteeassociee(null);
        } else  {
            $enchere->setIdentiteeassociee($this->entiteAssociees->getSelectedValue());
        }*/
        $this->setViewState('enchere', $enchere);

        // Selection de la phase
        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(false);
        $this->ongletReferences->setVisible(false);
        $this->ongletSynthese->setVisible(false);
        $this->ongletConfirmation->setVisible(false);

        // Remplir les listes des résultats par pages
        $listeResultatPage = ['10' => '10', '20' => '20', '50' => '50', '100' => '100', '500' => '500'];
        $this->nbResultsTop->dataSource = $listeResultatPage;
        $this->nbResultsTop->dataBind();
        $this->nbResultsTop2->dataSource = $listeResultatPage;
        $this->nbResultsTop2->dataBind();

        $allEntreprises = $this->getViewState('allEntreprises');

        // Si c'est la première fois que l'on atteint cette page
        // et si l'enchere est créée à partir d'une consultation
        if (null == $allEntreprises) {
            $consultationRef = $enchere->getConsultationId();
            if (null != $consultationRef) {
                // On récupére les entreprise ayant étés dépouillées et on les stoques dans la base EnchereEntreprise
                $sousPli = $this->envelop->getSelectedValue();
                $allEntreprises = Atexo_Enchere::getEntreprisesConsultation($consultationRef, true, $sousPli);
            }
        }

        $this->setViewState('allEntreprises', $allEntreprises);
        $this->updateActivePanels($allEntreprises);
    }

    /**
     * Retrourne les entreprises triées suivant le critère passé en paramètre.
     *
     * @param array  $allEntreprises
     * @param string $critere
     *
     * @return array
     */
    private function sortEntreprises($allEntreprises, $critere)
    {
        $arrayToSort = [];
								$this->triAscDesc = $this->getViewState('triAscDesc');
        $nomMethod = 'get'.ucfirst($critere);

        foreach ($allEntreprises as $entreprise) {
            $nomEntreprise = call_user_func([$entreprise, $nomMethod]);
            $arrayToSort[] = $nomEntreprise;
        }
        asort($arrayToSort);
        if (0 != strcmp($this->triAscDesc, 'ASC')) {
            $arrayToSort = array_reverse($arrayToSort);
            $this->triAscDesc = 'ASC';
        } else {
            $this->triAscDesc = 'DESC';
        }

        $allEntreprisesSorted = [];
        foreach ($arrayToSort as $item) {
            foreach ($allEntreprises as $entreprise) {
                if (0 == strcmp($item, call_user_func([$entreprise, $nomMethod]))) {
                    $allEntreprisesSorted[] = $entreprise;
                    continue;
                }
            }
        }
        $this->setViewState('triAscDesc', $this->triAscDesc);

        return $allEntreprisesSorted;
    }

    /**
     * Fonction triant le tableau des entreprises importées de la consultation.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function sortEntreprisesSelectionnees($sender, $param)
    {
        $allEntreprises = $this->getViewState('allEntreprises');
        $allEntreprisesSorted = $this->sortEntreprises($allEntreprises, $sender->getActiveControl()->CallbackParameter);
        $this->updateActivePanels($allEntreprisesSorted);
        $this->ApRepeaterSelectionnees->render($param->getNewWriter());
        $this->buttonAjouterEntreprisePanel->render($param->NewWriter);
    }

    /**
     * Fonction triant le tableau des entreprises invitées.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function sortEntreprisesInvitees($sender, $param)
    {
        $allEntreprises = $this->getViewState('allEntreprises');
        $allEntreprisesSorted = $this->sortEntreprises($allEntreprises, $sender->getActiveControl()->CallbackParameter);
        $this->updateActivePanels($allEntreprisesSorted);
        $this->ApRepeaterEntreprises->render($param->getNewWriter());
        $this->buttonAjouterEntreprisePanel->render($param->NewWriter);
    }

    /**
     * Fonction appellée lorsqu'on selectionne une entreprise (radio bouton)
     * Met a jours les tableau des entreprises.
     *
     * @param unknown_type $sender
     */
    public function selectEntreprise($sender)
    {
        $allEntreprises = $this->getViewState('allEntreprises');
        foreach ($allEntreprises as $entreprise) {
            if ($sender->getValue() == $entreprise->getIdentreprise()) {
                $entreprise->setIsselected($sender->Checked);
            }
        }

        $this->setViewState('allEntreprises', $allEntreprises);

        // Mise a jour du tableau récapitulatif
        $this->updateActivePanels($allEntreprises);
    }

    /**
     * Fonction appellée lorsque l'on clique sur le Radio Bouton en haut du tableau
     * Selectionne toutes les entreprises et met a jour les tableaux.
     *
     * @param unknown_type $sender
     */
    public function selectAllInvite($sender)
    {
        $allEntreprises = $this->getViewState('allEntreprises');
        foreach ($allEntreprises as $entreprise) {
            $entreprise->setIsselected($sender->Checked);
        }

        $this->setViewState('allEntreprises', $allEntreprises);

        // Mise a jour du tableau récapitulatif
        $this->updateActivePanels($allEntreprises);
    }

    /**
     * Retourne vrai si toutes les entreprises importées sont selectionnées.
     *
     * @return bool
     */
    public function isAllEntrepriseSelected()
    {
        $allEntreprises = $this->getViewState('allEntreprises');
        foreach ($allEntreprises as $entreprise) {
            if ($entreprise->getIsfromconsultation() && !$entreprise->getIsselected($sender->Checked)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Ajout d'une entreprise.
     */
    public function addEntreprise()
    {
        $allEntreprises = $this->getViewState('allEntreprises');

        $nom = $this->nomEntreprise->Text;
        $emailEnrichisseur = $this->emailEncherisseur->Text;
        $entreprise = new CommonEnchereEntreprisePmi();
        $entreprise->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $entreprise->setNom($nom);
        $entreprise->setEmail(trim($emailEnrichisseur));
        $entreprise->setIsfromconsultation(false);

        $allReadyIn = false;
        if ($allEntreprises) {
            foreach ($allEntreprises as $oneEntreprise) {
                if ($oneEntreprise->getEmail() == $entreprise->getEmail() || $oneEntreprise->getNom() == $entreprise->getNom()) {
                    $allReadyIn = true;
                }
            }
        }

        if (!$allReadyIn) {
            $allEntreprises[] = $entreprise;
            $this->setViewState('allEntreprises', $allEntreprises);
        }

        // Mise a jour du tableau récapitulatif
        $this->updateActivePanels($allEntreprises);
    }

    /**
     * Met a jour touts les 2 tableaux (repeaters) concernant les entreprises invitées.
     *
     * @param array $allEntreprises
     */
    public function updateActivePanels($allEntreprises)
    {
        $entreprisesSelectionnees = [];
        $entreprisesInvitees = [];

        if (null == $allEntreprises) {
            $allEntreprises = [];
        } else {
            foreach ($allEntreprises as $entreprise) {
                if ($entreprise->getIsfromconsultation()) {
                    $entreprisesSelectionnees[] = $entreprise;
                }
                if ($entreprise->getIsfromconsultation() && $entreprise->getIsselected()) {
                    $entreprisesInvitees[] = $entreprise;
                }
                if (!$entreprise->getIsfromconsultation()) {
                    $entreprisesInvitees[] = $entreprise;
                }
            }
        }

        if (0 != count($entreprisesSelectionnees)) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->RepeaterSelectionnees->dataSource = $entreprisesSelectionnees;
            $this->RepeaterSelectionnees->dataBind();
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
        }

        $this->RepeaterEntreprises->dataSource = $entreprisesInvitees;
        $this->RepeaterEntreprises->dataBind();

        $this->buttonAjouterEntreprise->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_SELECTION'));
    }

    public function reloadApRepeaterEntreprises($sender, $param)
    {
        $this->ApRepeaterEntreprises->render($param->NewWriter);
        $this->ApRepeaterSelectionnees->render($param->NewWriter);
        $this->buttonAjouterEntreprisePanel->render($param->NewWriter);
    }

    public function onDeleteEntrepriseClick($sender, $param)
    {
        $allEntreprises = $this->getViewState('allEntreprises');

        if (is_integer($param->CommandParameter)) {
            // L'entreprise a supprimer est une entreprise selectionnée à partir d'une consultation
            foreach ($allEntreprises as $entreprise) {
                if ($entreprise->getIdentreprise() == $param->CommandParameter) {
                    $entreprise->setIsselected(false);
                    $entreprise->delete($this->getConnexionCom());
                }
            }
        } else {
            foreach ($allEntreprises as $key => $entreprise) {
                if (0 == strcmp($entreprise->getNom(), $param->CommandParameter)) {
                    unset($allEntreprises[$key]);
                    $entreprise->delete($this->getConnexionCom());
                }
            }
        }

        $this->setViewState('allEntreprises', $allEntreprises);
        $this->updateActivePanels($allEntreprises);
    }

    public function changeNbrResultsGuests()
    {
    }

    public function getEntreprisesInvitees()
    {
        $allEntreprises = $this->getViewState('allEntreprises');

        if (!$allEntreprises) {
            return [];
        }

        $entreprisesInvitees = [];
        foreach ($allEntreprises as $entreprise) {
            if (!$entreprise->getIsfromconsultation() || true == $entreprise->getIsselected()) {
                $entreprisesInvitees[] = $entreprise;
            }
        }
        $this->setViewState('allEntreprises', $allEntreprises);

        return $entreprisesInvitees;
    }

    public function getTypesBaremeEnchere()
    {
        $typesBaremes = [];
								$typesBaremes[0] = Prado::localize('SELECTIONNEZ_BAREME');
        $typesBaremes[Atexo_Config::getParameter('BAREME_ENCHERE_PRIX_REFERENCE')] = Prado::localize('BAREME_PRIX_REFERENCE_LIBELLE');
        $typesBaremes[Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF')] = Prado::localize('BAREME_RELATIF_LIBELLE');
        $typesBaremes[Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE')] = Prado::localize('BAREME_SOMME_PONDEREE_LIBELLE');
        $typesBaremes[Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')] = Prado::localize('BAREME_TRANCHES_LIBELLE');

        return $typesBaremes;
    }

    public function getTypesBaremeReference()
    {
        $typesBaremes = [];
								$typesBaremes[0] = Atexo_Util::toUtf8(Prado::localize('SELECTIONNEZ_BAREME'));
        $typesBaremes[Atexo_Config::getParameter('BAREME_REFERENCE_PRIX_REFERENCE')] = Atexo_Util::toUtf8(Prado::localize('BAREME_REFERENCE_PRIX_REFERENCE_LIBELLE'));
        $typesBaremes[Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')] = Atexo_Util::toUtf8(Prado::localize('BAREME_REFERENCE_RELATIF_LIBELLE'));
        $typesBaremes[Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')] = Atexo_Util::toUtf8(Prado::localize('BAREME_REFERENCE_TRANCHES_LIBELLE'));

        return $typesBaremes;
    }

    public function getTypesBaremeGlobal()
    {
        $typesBaremes = [];
								$typesBaremes[0] = Prado::localize('SELECTIONNEZ_BAREME');
        $typesBaremes[Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')] = Prado::localize('BAREME_GLOBAL_SOMME_PONDEREE_LIBELLE');
        $typesBaremes[Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')] = Prado::localize('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE_LIBELLE');

        return $typesBaremes;
    }

    public function refreshApBaremeTranche($sender, $param)
    {
        $selectedValue = $this->formuleCalcul->getSelectedValue();
        if ($selectedValue == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
            $this->ApRepeaterTranchesBaremeEnchere->setDisplay('Dynamic');
            $this->apBaremeRelatif->setDisplay('None');
        } elseif ($selectedValue == Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF')) {
            $this->ApRepeaterTranchesBaremeEnchere->setDisplay('None');
            $this->apBaremeRelatif->setDisplay('Dynamic');
        } else {
            $this->apBaremeRelatif->setDisplay('None');
            $this->ApRepeaterTranchesBaremeEnchere->setDisplay('None');
        }
        $this->nomBaremeEnchere->Text = $this->formuleCalcul->SelectedItem->Text;
        $this->nomBaremePanel->render($param->getNewWriter());
    }

    public function refreshApBaremeGlobal($sender, $param)
    {
        $selectedValue = $this->formuleCalculBaremeGlobal->getSelectedValue();
        if ($selectedValue == Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')) {
            $this->apBaremeSommePonderee->setDisplay('Dynamic');
            $this->apBaremePrixCorrige->setDisplay('None');
        } elseif ($selectedValue == Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')) {
            $this->apBaremePrixCorrige->setDisplay('Dynamic');
            $this->apBaremeSommePonderee->setDisplay('None');
        } else {
            $this->apBaremePrixCorrige->setDisplay('None');
            $this->apBaremeSommePonderee->setDisplay('None');
        }

        $this->nomBaremeGlobal->Text = $this->formuleCalculBaremeGlobal->SelectedItem->Text;
        $this->nomBaremePanel->render($param->getNewWriter());
    }

    /**
     * Etape des paramètre liés a l'enchere.
     */
    public function etapeParametres()
    {
        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(true);
        $this->ongletReferences->setVisible(false);
        $this->ongletSynthese->setVisible(false);
        $this->ongletConfirmation->setVisible(false);

        // Popups Ajax
        $this->popupDefinirBaremeEnchere->setVisible(true);
        $this->popupDefinirBaremeGlobal->setVisible(true);
        $this->popupDefinirBaremeEnchere->setDisplay('Dynamic');
        $this->popupDefinirBaremeGlobal->setDisplay('Dynamic');

        $entreprisesInvitees = $this->getEntreprisesInvitees();
        $this->formuleCalcul->setSelectedValue($this->getEnchere()->getTypebaremenetc());
        $this->formuleCalculBaremeGlobal->setSelectedValue($this->getEnchere()->getTypebaremeenchereglobale());
        $this->noteEntreprises->Text = $this->getEnchere()->getNoteEntreprises();

        $this->RepeaterNotesTechniques->dataSource = $entreprisesInvitees;
        $this->RepeaterNotesTechniques->dataBind();
    }

    public function displayPopUpBaremeEnchere($sender, $param)
    {
        $this->popupDefinirBaremeEnchere->setDisplay('Dynamic');
        $this->CallbackClient->callClientFunction('overlay', [$this->popupDefinirBaremeEnchere->ClientID, 'overlayBaremeEnchere']);
        //$this->CallbackClient->callClientFunction('setIdPopupResize', $this->popupDefinirBaremeEnchere->ClientID);
        $this->CallbackClient->appear($this->popupDefinirBaremeEnchere);

        $tranchesBaremeEnchere = $this->getViewState('tranchesBaremeEnchere');

        if ($tranchesBaremeEnchere) {
            $this->repeaterTranchesBaremeEnchere->dataSource = $tranchesBaremeEnchere;
            $this->repeaterTranchesBaremeEnchere->dataBind();
        }
        $this->refreshApBaremeTranche($sender, $param);
    }

    public function displayPopUpBaremeGlobal($sender, $param)
    {
        $this->popupDefinirBaremeGlobal->setDisplay('Dynamic');
        $this->CallbackClient->callClientFunction('overlay', [$this->popupDefinirBaremeGlobal->ClientID, 'overlayBaremeGlobal']);
        $this->CallbackClient->appear($this->popupDefinirBaremeGlobal);
        $this->refreshApBaremeGlobal($sender, $param);
    }

    public function addTrancheBaremeEnchere($sender, $param)
    {
        $tranchesBaremeEnchere = [];
        foreach ($this->repeaterTranchesBaremeEnchere->Items as $tranche) {
            $tranchePrecedentes = new CommonParametrageEnchereTranchesBaremeNETC();
            $tranchePrecedentes->setBorneinf(($tranche->trancheMin->Text));
            $tranchePrecedentes->setBornesup(($tranche->trancheMax->Text));
            $tranchePrecedentes->setNote(($tranche->noteTranche->Text));
            $tranchePrecedentes->setTmpId($tranche->ItemIndex);
            $tranchePrecedentes->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $tranchesBaremeEnchere[$tranche->ItemIndex] = $tranchePrecedentes;
        }
        $nouvelleTranche = new CommonParametrageEnchereTranchesBaremeNETC();
        $tranchesBaremeEnchere[is_countable($this->repeaterTranchesBaremeEnchere->Items) ? count($this->repeaterTranchesBaremeEnchere->Items) : 0] = $nouvelleTranche;

        $this->setViewState('tranchesBaremeEnchere', $tranchesBaremeEnchere);
        $this->repeaterTranchesBaremeEnchere->dataSource = $tranchesBaremeEnchere;
        $this->repeaterTranchesBaremeEnchere->dataBind();
        $this->ApRepeaterTranchesBaremeEnchere->render($param->getNewWriter());

        $this->nomBaremePanel->render($param->getNewWriter());
    }

    public function deleteTrancheBaremeEnchere($sender, $param)
    {
        $idItem = $sender->getParent()->ItemIndex;
        $tranchesBaremeEnchere = [];
        foreach ($this->repeaterTranchesBaremeEnchere->Items as $tranche) {
            if ($tranche->ItemIndex == $idItem) {
                continue;
            }

            $nouvelleTranche = new CommonParametrageEnchereTranchesBaremeNETC();
            $nouvelleTranche->setBorneinf(($tranche->trancheMin->Text));
            $nouvelleTranche->setBornesup(($tranche->trancheMax->Text));
            $nouvelleTranche->setNote(($tranche->noteTranche->Text));
            $nouvelleTranche->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $tranchesBaremeEnchere[$tranche->ItemIndex] = $nouvelleTranche;
        }

        $this->setViewState('tranchesBaremeEnchere', $tranchesBaremeEnchere);
        $this->repeaterTranchesBaremeEnchere->dataSource = $tranchesBaremeEnchere;
        $this->repeaterTranchesBaremeEnchere->dataBind();
        $this->ApRepeaterTranchesBaremeEnchere->render($param->getNewWriter());

        $this->nomBaremePanel->render($param->getNewWriter());
    }

    public function validerPopupBaremeEnchere($sender, $param)
    {
        $enchere = $this->getEnchere();
        $selectedValue = $this->formuleCalcul->getSelectedValue();
        $enchere->setTypebaremenetc($selectedValue);
        $this->setViewState('enchere', $enchere);

        if ($selectedValue == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
            $tranchesBaremeEnchere = [];
            foreach ($this->repeaterTranchesBaremeEnchere->Items as $tranche) {
                $tranchePrecedentes = new CommonParametrageEnchereTranchesBaremeNETC();
                $tranchePrecedentes->setBorneinf(($tranche->trancheMin->Text));
                $tranchePrecedentes->setBornesup(($tranche->trancheMax->Text));
                $tranchePrecedentes->setNote(($tranche->noteTranche->Text));
                $tranchePrecedentes->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $tranchesBaremeEnchere[$tranche->ItemIndex] = $tranchePrecedentes;
            }

            $this->setViewState('tranchesBaremeEnchere', $tranchesBaremeEnchere);
            $this->repeaterTranchesBaremeEnchere->dataSource = $tranchesBaremeEnchere;
            $this->repeaterTranchesBaremeEnchere->dataBind();

            $this->CallbackClient->fade($this->popupDefinirBaremeEnchere);
            $this->CallbackClient->callClientFunction('overlay', [$this->popupDefinirBaremeEnchere->ClientID, 'overlayBaremeEnchere']);
        } elseif (0 == $selectedValue) {
            // Message d'avertissement
        } else {
            $this->CallbackClient->fade($this->popupDefinirBaremeEnchere);
            $this->CallbackClient->callClientFunction('overlay', [$this->popupDefinirBaremeEnchere->ClientID, 'overlayBaremeEnchere']);
        }
        $this->nomBaremePanel->render($param->getNewWriter());
    }

    public function validerPopupBaremeGlobal($sender, $param)
    {
        $enchere = $this->getEnchere();
        $selectedValue = $this->formuleCalculBaremeGlobal->getSelectedValue();
        $enchere->setTypebaremeenchereglobale($selectedValue);
        $this->setViewState('enchere', $enchere);

        $this->CallbackClient->fade($this->popupDefinirBaremeGlobal);
        $this->CallbackClient->callClientFunction('overlay', [$this->popupDefinirBaremeEnchere->ClientID, 'overlayBaremeGlobal']);
        $this->nomBaremePanel->render($param->getNewWriter());
    }

    public function sortEntreprisesNotesTechniques($sender, $param)
    {
        $entreprisesInvitees = $this->getEntreprisesInvitees();
        // Sauvegarde des notes techniques
        foreach ($this->RepeaterNotesTechniques->getItems() as $entrepriseNoteTechnique) {
            foreach ($entreprisesInvitees as $entrepriseInvitee) {
                if (0 == strcmp($entrepriseInvitee->getEmail(), $entrepriseNoteTechnique->emailEntrepriseHidden->Text)) {
                    $entrepriseInvitee->setNotetechnique(($entrepriseNoteTechnique->noteTech->Text));
                    continue;
                }
            }
        }
        $entreprisesInviteesSorted = $this->sortEntreprises($entreprisesInvitees, $sender->getActiveControl()->CallbackParameter);
        $this->RepeaterNotesTechniques->dataSource = $entreprisesInviteesSorted;
        $this->RepeaterNotesTechniques->dataBind();

        $this->ApRepeaterNotesTechniques->render($param->getNewWriter());
    }

    /**
     * Sauvegarde dans le view state les parametres de l'enchere.
     */
    public function saveEnchere()
    {
        // Sauvegarde des parametres
        $enchere = $this->getEnchere();
        $enchere->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $enchere->setDatedebut(Atexo_Util::frnDateTime2iso($this->dateDebutEnchere->Text));
        $enchere->setDatefin(Atexo_Util::frnDateTime2iso($this->dateFinEnchere->Text));
        $enchere->setDelaiprolongation(($this->delaiProlongation->Text));
        $enchere->setMontantreserve(($this->montantReserve->Text));
        $enchere->setCommentaire(($this->commentaire->Text));
        $enchere->setTypebaremenetc($this->formuleCalcul->getSelectedValue());
        $enchere->setTypebaremeenchereglobale($this->formuleCalculBaremeGlobal->getSelectedValue());
        $enchere->setNoteEntreprises($this->noteEntreprises->Text);

        $currentIdService = Atexo_CurrentUser::getCurrentServiceId();
        if (!empty($currentIdService)) {
            $enchere->setIdentiteeassociee($currentIdService);
        } else {
            $enchere->setIdentiteeassociee(null);
        }
        $enchere->setNoteMaxBaremeRelatif($this->valeurNoteMaxBarRelatif->Text);

        if ($this->nbCandidatsVisibleOui->Checked) {
            $enchere->setNbrcandidatsvisible('1');
        } else {
            $enchere->setNbrcandidatsvisible('0');
        }
        if ($this->listeAnonymeVisibleOui->Checked) {
            $enchere->setListecandidatsvisible('1');
        } else {
            $enchere->setListecandidatsvisible('0');
        }
        if ($this->meilleureEnchereNonObligatoire->Checked) {
            $enchere->setMeilleureenchereobligatoire('0');
        } else {
            $enchere->setMeilleureenchereobligatoire('1');
        }
        if ($this->detailInfosDispo1->Checked || $this->detailInfosDispo3->Checked) {
            $enchere->setRangvisible('1');
        } else {
            $enchere->setRangvisible('0');
        }
        if ($this->detailInfosDispo2->Checked || $this->detailInfosDispo3->Checked) {
            $enchere->setMeilleureoffrevisible('1');
        } else {
            $enchere->setMeilleureoffrevisible('0');
        }
        if ($this->meilleureNoteHaute->Checked) {
            $enchere->setMeilleurnotehaute('1');
        } else {
            $enchere->setMeilleurnotehaute('0');
        }

        // Note Maximale bareme relatif
        if ($enchere->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
            $enchere->setNotemaxbaremerelatif(($this->valeurNoteMaxBarRelatif->Text));
        }

        if ($enchere->getTypebaremeenchereglobale() == Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')) {
            $enchere->setCoeffa(($this->valeurCoeffA->Text));
            $enchere->setCoeffb(($this->valeurCoeffB->Text));
        }

        if ($enchere->getTypebaremeenchereglobale() == Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')) {
            $enchere->setCoeffc(($this->valeurCoeffC->Text));
        }

        // Mail par défault modifiable si l'enchere n'est pas encore en base :
        if (!$enchere->getMail()) {
            $mail = '';
            $mail .= Prado::localize('TEXT_MADAME_MONSIEUR');
            $mail .= $this->sautLigne().$this->sautLigne();

            $consultationId = $enchere->getRefconsultation();
            if ($consultationId) {
                $mail .= Prado::localize('VOUS_AVEZ_ETE_SELECTIONNE_CONSULTATION');
                $mail .= $this->retrieveConsultation($consultationId)->getTitre();
            } else {
                $mail .= Prado::localize('VOUS_AVEZ_ETE_SELECTIONNE_ENCHERE');
                $mail .= $enchere->getReferenceutilisateur();
            }

            $mail .= $this->sautLigne();
            $mail .= Prado::localize('CETTE_ENCHERE_SE_DEROULERA');
            $mail .= Atexo_Util::iso2frnDateTime($enchere->getDatedebut());
            $mail .= Prado::localize('DEFINE_AU');
            $mail .= Atexo_Util::iso2frnDateTime($enchere->getDatefin()).$this->sautLigne();

            $mail .= Prado::localize('SI_VOUS_N_ETES_PAS_INSCRITS');

            $enchere->setMail($mail);
        }

        $this->setViewState('enchere', $enchere);
    }

    /**
     * Etape de création des références et de leurs paramètres.
     */
    public function etapeReferences()
    {
        // Verification des Bareme
        $erreur = null;
        if ($this->formuleCalcul->getSelectedValue() == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
            if ($this->valeurNoteMaxBarRelatif->Text = '') {
                $erreur = Prado::localize('ERREUR_BAREME_ENCHERE_TRANCHES');
            }
        }
        if ($this->formuleCalculBaremeGlobal->getSelectedValue() == Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')) {
            if ('' == $this->valeurCoeffC->Text) {
                $erreur = Prado::localize('ERREUR_BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE');
            }
        }
        if ($this->formuleCalculBaremeGlobal->getSelectedValue() == Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')) {
            if ('' == $this->valeurCoeffA->Text || '' == $this->valeurCoeffB->Text) {
                $erreur = Prado::localize('ERREUR_BAREME_GLOBAL_SOMME_PONDEREE');
            }
        }

        if ($erreur) {
            $this->erreur->Text = $erreur;
            $this->labelServerSideParam->Text = "<script>document.getElementById('divValidationSummaryManuel').style.display='';</script>";
            $this->saveEnchere();
            $this->etapeParametres();

            return;
        } else {
            $this->labelServerSideParam->Text = "<script>document.getElementById('divValidationSummaryManuel').style.display='None';</script>";
        }

        // Sauvegarde des notes techniques
        $entreprisesInvitees = $this->getEntreprisesInvitees();
        foreach ($this->RepeaterNotesTechniques->getItems() as $entrepriseNoteTechnique) {
            foreach ($entreprisesInvitees as $entrepriseInvitee) {
                if (0 == strcmp($entrepriseInvitee->getEmail(), $entrepriseNoteTechnique->emailEntrepriseHidden->Text)) {
                    $entrepriseInvitee->setNotetechnique(($entrepriseNoteTechnique->noteTech->Text));
                    continue;
                }
            }
        }
        $this->popupDefinirBaremeEnchere->setVisible(true);
        $this->popupDefinirBaremeGlobal->setVisible(true);

        $this->saveEnchere();

        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(false);
        $this->ongletReferences->setVisible(true);
        $this->ongletSynthese->setVisible(false);
        $this->ongletConfirmation->setVisible(false);

        // Au moins une référence
        $enchereReferences = $this->getViewState('enchereReferences');
        if (!$enchereReferences) {
            $reference = new CommonParametrageEnchereReference();
            $entreprisesInvitees = $this->getEntreprisesInvitees();
            foreach ($entreprisesInvitees as $entrepriseInvitee) {
                $valInitiale = new CommonEnchereValeursInitiales();
                $valInitiale->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $valInitiale->setEmailentreprise(trim($entrepriseInvitee->getEmail()));
                $valInitiale->setNomentreprise($entrepriseInvitee->getNom());
                $reference->setValeurInitiale(trim($entrepriseInvitee->getEmail()), $valInitiale);
                $reference->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            }
            $enchereReferences[0] = $reference;
            $this->setViewState('enchereReferences', $enchereReferences);
        }

        $this->repeaterReferences->setDataSource($enchereReferences);
        $this->repeaterReferences->dataBind(false);

        $tmpId = 0;
        foreach ($this->repeaterReferences->Items as $itemReference) {
            $itemReference->tmpId->Value = $tmpId;
	        $itemReference->repeaterTranchesBaremeReference->setDataSource($itemReference->Data->getCommonParametrageEnchereTrancheBaremeReferences(null, $this->getConnexionCom()));
            $itemReference->repeaterTranchesBaremeReference->dataBind();

	        $itemReference->repeaterValeursInitiales->setDataSource($itemReference->Data->getCommonValeursInitiales());
            $itemReference->repeaterValeursInitiales->dataBind();

            $itemReference->valeurInitialeCommune->raiseEvent('OnCheckedChanged', $itemReference->valeurInitialeCommune, null);

            ++$tmpId;
        }
    }

    public function validerPopupBaremeReference($sender, $param)
    {
        $sender->getParent()->getParent()->getParent()->typeBaremeReference->Value = $sender->getParent()->getParent()->getParent()->formuleCalculBaremeReference->getSelectedValue();
        $this->CallbackClient->fade($sender->getParent());
        $this->CallbackClient->callClientFunction('overlay', [$sender->getParent()->getParent()->getParent()->popupDefinirBaremeReference->ClientID,
                           $sender->getParent()->getParent()->getParent()->overlay->ClientId, ]);

        $sender->getParent()->getParent()->getParent()->nomBaremeReferencePanel->render($param->getNewWriter());

        $this->addRefButton->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_REFERENCE'));
        $this->addRefButtonPanel->render($param->getNewWriter());

        $sender->getParent()->getParent()->getParent()->deleteReferenceButton->Text = utf8_encode(Prado::localize('SUPPRIMER_REFERENCE'));
        $sender->getParent()->getParent()->getParent()->deleteReferenceButtonPanel->render($param->getNewWriter());
    }

    public function displayPopUpBaremeReference($sender, $param)
    {
        $sender->getParent()->popupDefinirBaremeReference->setVisible(true);
        $this->CallbackClient->appear($sender->getParent()->popupDefinirBaremeReference);

        //echo $this->getParent()->popupDefinirBaremeReference->ID;
        $this->CallbackClient->callClientFunction('overlay', [$sender->getParent()->popupDefinirBaremeReference->ClientID, $sender->getParent()->overlay->ClientID]);
        $this->CallbackClient->callClientFunction('setIdPopupResize', $sender->getParent()->popupDefinirBaremeReference->ClientID);

        $sender->getParent()->formuleCalculBaremeReference->setDataSource($this->getTypesBaremeReference());
        $sender->getParent()->formuleCalculBaremeReference->dataBind();
        $sender->getParent()->formuleCalculBaremeReference->setSelectedIndex($sender->getParent()->typeBaremeReference->Value);
        $sender->getParent()->formuleCalculBaremeReference->raiseCallbackEvent(null);

        $sender->getParent()->nomBaremeReferencePanel->render($param->getNewWriter());

        $this->addRefButton->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_REFERENCE'));
        $this->addRefButtonPanel->render($param->getNewWriter());

        $sender->getParent()->deleteReferenceButton->Text = utf8_encode(Prado::localize('SUPPRIMER_REFERENCE'));
        $sender->getParent()->deleteReferenceButtonPanel->render($param->getNewWriter());
    }

    public function refreshApBaremeTrancheReference($sender, $param)
    {
        $selectedValue = $sender->getSelectedValue();
        if ($selectedValue == Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')) {
            $sender->getParent()->getParent()->getParent()->apBaremeRelatifReference->setDisplay('Dynamic');
            $sender->getParent()->getParent()->getParent()->ApRepeaterTranchesBaremeReference->setDisplay('None');
        } elseif ($selectedValue == Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')) {
            $sender->getParent()->getParent()->getParent()->ApRepeaterTranchesBaremeReference->setDisplay('Dynamic');
            $sender->getParent()->getParent()->getParent()->apBaremeRelatifReference->setDisplay('None');
        } else {
            $sender->getParent()->getParent()->getParent()->ApRepeaterTranchesBaremeReference->setDisplay('None');
            $sender->getParent()->getParent()->getParent()->apBaremeRelatifReference->setDisplay('None');
        }

        $sender->getParent()->getParent()->getParent()->nomBaremeReference->Text = utf8_decode($sender->SelectedItem->Text);
        if ($param) {
            $sender->getParent()->getParent()->getParent()->nomBaremeReferencePanel->render($param->getNewWriter());

            $this->addRefButton->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_REFERENCE'));
            $this->addRefButtonPanel->render($param->getNewWriter());

            $sender->getParent()->getParent()->getParent()->deleteReferenceButton->Text = utf8_encode(Prado::localize('SUPPRIMER_REFERENCE'));
            $sender->getParent()->getParent()->getParent()->deleteReferenceButtonPanel->render($param->getNewWriter());
        }
    }

    /**
     * Sauvegarde les tranches du bareme d'une reference
     * (identifiée par idTmpRef).
     *
     * @param int                         $tmpIdRef
     * @param Encheretranchebaremeenchere $tranchesBaremeReference
     */
    public function saveTranches($tmpIdRef, $tranchesBaremeReference)
    {
        $enchereReferences = $this->getViewState('enchereReferences');
        $enchereReferences[$tmpIdRef]->setCommonTranchesBaremeReference($tranchesBaremeReference);
        $this->setViewState('enchereReferences', $enchereReferences);
    }

    /**
     * Scan le repeater des tranches puis sauvegarde la liste des tranches.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function addTrancheBaremeReference($sender, $param)
    {
        //echo $sender->getParent()->getParent()->getParent()->getParent()->getParent()->ClientID;exit();

        $tranchesBaremeReference = [];
        foreach ($sender->getParent()->getParent()->getParent()->getParent()->repeaterTranchesBaremeReference->Items as $tranche) {
            echo 'hop';
            $tranchePrecedentes = new CommonParametrageEnchereTranchesBaremeNETC();
            $tranchePrecedentes->setBorneinf(($tranche->trancheMinReference->Text));
            $tranchePrecedentes->setBornesup(($tranche->trancheMaxReference->Text));
            $tranchePrecedentes->setNote(($tranche->noteTrancheReference->Text));
            $tranchePrecedentes->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $tranchesBaremeReference[] = $tranchePrecedentes;
        }
        $tranchesBaremeReference[] = new CommonParametrageEnchereTrancheBaremeReference();

        $sender->getParent()->getParent()->getParent()->getParent()->repeaterTranchesBaremeReference->dataSource = $tranchesBaremeReference;
        $sender->getParent()->getParent()->getParent()->getParent()->repeaterTranchesBaremeReference->dataBind();

        //$enchereref = new ParametrageEnchereReference();

        $tmpIdRef = $sender->getParent()->getParent()->getParent()->getParent()->tmpId->Value;
        $this->saveTranches($tmpIdRef, $tranchesBaremeReference);

        $sender->getParent()->render($param->getNewWriter());
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function deleteTrancheBaremeReference($sender, $param)
    {
        $activePanel = $sender->getParent()->getParent()->getParent();

        $idItem = $sender->getParent()->ItemIndex;
        $tranchesBaremeReference = [];
        foreach ($sender->getParent()->getParent()->Items as $tranche) {
            if ($tranche->ItemIndex == $idItem) {
                continue;
            }
            $nouvelleTranche = new CommonEnchereTranchesBaremeNETC();
            $nouvelleTranche->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $nouvelleTranche->setBorneinf(($tranche->trancheMinReference->Text));
            $nouvelleTranche->setBornesup(($tranche->trancheMaxReference->Text));
            $nouvelleTranche->setNote(($tranche->noteTrancheReference->Text));
            $tranchesBaremeReference[$tranche->ItemIndex] = $nouvelleTranche;
        }

        //$this->setViewState("tranchesBaremeEnchere" , $tranchesBaremeEnchere);
        $sender->getParent()->getParent()->dataSource = $tranchesBaremeReference;
        $sender->getParent()->getParent()->dataBind();

        $activePanel->render($param->getNewWriter());
    }

    /**
     * Ajoute une reference au repeater.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function addReference($sender, $param)
    {
        $enchereReferences = $this->saveReferences();
        $entreprisesInvitees = $this->getEntreprisesInvitees();

        // Initialisation de la nouvelle reference, de la liste des valeurs initiales :
        $reference = new CommonParametrageEnchereReference();
        foreach ($entreprisesInvitees as $entrepriseInvitee) {
            $valInitiale = new CommonEnchereValeursInitiales();
            $valInitiale->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $valInitiale->setEmailentreprise(trim($entrepriseInvitee->getEmail()));
            $valInitiale->setNomentreprise($entrepriseInvitee->getNom());
            $reference->setValeurInitiale($entrepriseInvitee->getEmail(), $valInitiale);
        }
        $enchereReferences[is_countable($enchereReferences) ? count($enchereReferences) : 0] = $reference;
        $this->setViewState('enchereReferences', $enchereReferences);
        $this->repeaterReferences->dataSource = $enchereReferences;
        $this->repeaterReferences->dataBind();

        $tmpId = 0;
        foreach ($this->repeaterReferences->Items as $itemReference) {
            $itemReference->tmpId->Value = $tmpId;
	        $itemReference->repeaterTranchesBaremeReference->setDataSource($itemReference->Data->getCommonTranchesBaremeReference());
            $itemReference->repeaterTranchesBaremeReference->dataBind();

	        $itemReference->repeaterValeursInitiales->setDataSource($itemReference->Data->getCommonValeursInitiales());
            $itemReference->repeaterValeursInitiales->dataBind();

            $itemReference->valeurInitialeCommune->raiseEvent('OnCheckedChanged', $itemReference->valeurInitialeCommune, null);

            ++$tmpId;
        }

        $sender->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_REFERENCE'));
        $this->ApRepeaterReferences->render($param->getNewWriter());
    }

    public function updateReferenceRepeater($param)
    {
        $this->ApRepeaterReferences->render($param->getNewWriter());
    }

    public function deleteReference($sender, $param)
    {
        $enchereReferences = $this->saveReferences();
        $idRefToDelete = $sender->getParent()->getParent()->ItemIndex;
        unset($enchereReferences[$idRefToDelete]);

        $this->repeaterReferences->setDataSource($enchereReferences);
        $this->repeaterReferences->dataBind();
        $tmpId = 0;
        foreach ($this->repeaterReferences->Items as $itemReference) {
            $itemReference->tmpId->Value = $tmpId;
	        $itemReference->repeaterTranchesBaremeReference->setDataSource($itemReference->Data->getCommonTranchesBaremeReference());
            $itemReference->repeaterTranchesBaremeReference->dataBind();

	        $itemReference->repeaterValeursInitiales->setDataSource($itemReference->Data->getCommonValeursInitiales());
            $itemReference->repeaterValeursInitiales->dataBind();

            $itemReference->valeurInitialeCommune->raiseEvent('OnCheckedChanged', $itemReference->valeurInitialeCommune, null);

            ++$tmpId;
        }

        $this->saveReferences();

        $this->ApRepeaterReferences->render($param->getNewWriter());
    }

    /**
     * Cache ou affiche la liste des valeurs initiales de chaque entreprise pour
     * la reference courante.
     *
     * @param TActiveRadioButton $sender
     */
    public function hideListeValInitiales($sender)
    {
        if ($sender->getParent()->valeurInitialeCommune->Checked) {
            $sender->getParent()->ApRepeaterValeursInitiales->setDisplay('None');
            $sender->getParent()->preciserValeursInitiales->setDisplay('None');
        } else {
            $sender->getParent()->ApRepeaterValeursInitiales->setDisplay('Dynamic');
            $sender->getParent()->preciserValeursInitiales->setDisplay('Dynamic');
        }
    }

    public function updateValInitialesAP($sender, $param)
    {
        $sender->getParent()->ApRepeaterValeursInitiales->render($param->getNewWriter());

        $this->addRefButton->Text = utf8_encode(Prado::localize('DEFINE_AJOUTER_REFERENCE'));
        $this->addRefButtonPanel->render($param->getNewWriter());

        $sender->getParent()->deleteReferenceButton->Text = utf8_encode(Prado::localize('SUPPRIMER_REFERENCE'));
        $sender->getParent()->deleteReferenceButtonPanel->render($param->getNewWriter());
    }

    /**
     * Sauvegarde des references entrées dans le Repeater.
     *
     * @return unknown
     */
    public function saveReferences()
    {
        $enchereReferences = [];
								$tmpId = 0;
        foreach ($this->repeaterReferences->Items as $referenceItem) {
            $enchereReference = new CommonParametrageEnchereReference();
            $enchereReference->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $enchereReference->setLibelle(($referenceItem->ref->Text));
            $enchereReference->setUnite(($referenceItem->uniteRef->Text));
            $enchereReference->setPasmin(($referenceItem->pasMin->Text));
            $enchereReference->setPasmax(($referenceItem->pasMax->Text));
            $enchereReference->setQuantite(($referenceItem->quantiteRef->Text));

            if ($this->formuleCalcul->getSelectedValue() == Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE')) {
                $enchereReference->setPonderationnotereference(($referenceItem->coeffPonderation->Text));
            }

            $enchereReference->setTypebaremereference($referenceItem->typeBaremeReference->Value);

            if ($referenceItem->IsRefMontantOui->Checked) {
                $enchereReference->setIsmontant('1');
            } else {
                $enchereReference->setIsmontant('0');
            }
            if ($referenceItem->valeurInitialeCommune->Checked) {
                $enchereReference->setValeurdepartcommune('1');
                $enchereReference->setValeurdepart(($referenceItem->valeurInitialeCommuneValeur->Text));
            } else {
                $enchereReference->setValeurdepartcommune('0');
            }
            // Dans les 2 cas on ajoute les valeurs initiales (eventuelement vides)
            foreach ($referenceItem->repeaterValeursInitiales->Items as $entrepriseValInitiale) {
                $enchereValeurInitiale = new CommonEnchereValeursInitiales();
                $enchereValeurInitiale->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $enchereValeurInitiale->setValeur(($entrepriseValInitiale->valeurInitialeRef->Text));
                $enchereValeurInitiale->setNomentreprise(($entrepriseValInitiale->nomEntrepriseValIn->Text));
                $enchereValeurInitiale->setEmailentreprise(trim($entrepriseValInitiale->valeurInitialeEmailEntreprise->Value));
                $enchereReference->setValeurInitiale($entrepriseValInitiale->valeurInitialeEmailEntreprise->Value, $enchereValeurInitiale);
            }

            if ($enchereReference->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')) {
                // On enregistre toutes les tranches

                foreach ($referenceItem->repeaterTranchesBaremeReference->Items as $trancheItem) {
                    //echo "hop";
                    $trancheReference = new CommonParametrageEnchereTrancheBaremeReference();
                    $trancheReference->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $trancheReference->setBorneinf(($trancheItem->trancheMinReference->Text));
                    $trancheReference->setBornesup(($trancheItem->trancheMaxReference->Text));
                    $trancheReference->setNote(($trancheItem->noteTrancheReference->Text));

                    $tranchesBaremeReference[] = $trancheReference;
                }
                $enchereReference->setCommonTranchesBaremeReference($tranchesBaremeReference);
            } elseif ($enchereReference->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')) {
                // On enregistre la note maximale de la référence
                $enchereReference->setNotemaxbaremerelatif(($referenceItem->valeurNoteMaxBarRelatifReference->Text));
            } else {
                // Rien...
            }

            $enchereReferences[$tmpId] = $enchereReference;
            $referenceItem->tmpId->Value = $tmpId;
            ++$tmpId;
        }
        $this->setViewState('enchereReferences', $enchereReferences);

        return $enchereReferences;
    }

    public function validateEncheres()
    {
    }

    /**
     * Etape de synthèses.
     */
    public function etapeSynthese()
    {
        $this->saveReferences();

        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(false);
        $this->ongletReferences->setVisible(false);
        $this->ongletSynthese->setVisible(true);
        $this->ongletConfirmation->setVisible(false);

        $entreprisesInvitees = $this->getEntreprisesInvitees();
        $this->RepeaterEntreprises2->setDataSource($entreprisesInvitees);
        $this->RepeaterEntreprises2->dataBind();

        $urlPf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
        $this->lien->Text = $urlPf.'?page=Entreprise.EntrepriseParticipationEnchere&orgAcronym='.Atexo_CurrentUser::getOrganismAcronym();
    }

    public function sortEntreprisesInvitees2($sender, $param)
    {
        $entreprisesInvitees = $this->getEntreprisesInvitees();
        $entreprisesInviteesSorted = $this->sortEntreprises($entreprisesInvitees, $sender->getActiveControl()->CallbackParameter);

        $this->RepeaterEntreprises2->setDataSource($entreprisesInviteesSorted);
        $this->RepeaterEntreprises2->dataBind();
        $this->ApRepeaterEntreprises2->render($param->getNewWriter());
    }

    public function getEntrepriseIdFromEmail($email)
    {
        $entreprises = $this->getEntreprisesInvitees();
        foreach ($entreprises as $entreprise) {
            if (0 == strcmp($entreprise->getEmail(), $email)) {
                return $entreprise->getId();
            }
        }

        return 0;
    }

    /**
     * Enregistrement en base
     * Affichage d'un message de confirmation.
     */
    public function etapeConfirmation($sender, $param)
    {
        //commencer le scan Antivirus
        if ($this->UserFile->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->UserFile->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->UserFile->FileName.'"');

                return -1;
            }
        }
        //Fin du code scan Antivirus

        //Début Verification de l'existence de la reference utilisateur
        //$enchere = $this->getEnchere();
        //$verifyEnchere = Atexo_Enchere::retrieveEnchereByRefUtilisateur($enchere->getReferenceUtilisateur());
        /*if($verifyEnchere && $verifyEnchere->getId() != $enchere->getId()) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ENCHERE_REF_EXISTE_DEJA'));
        } else {*/
        $this->ongletCreation->setStyle('display:none');
        $this->ongletChoixInvites->setStyle('display:none');
        $this->ongletParametres->setVisible(false);
        $this->ongletReferences->setVisible(false);
        $this->ongletSynthese->setVisible(false);
        $this->ongletConfirmation->setVisible(true);
        $this->messageConfirmation->setVisible(true);
        $this->messageConfirmation->setMessage(Prado::localize('PARAMETRAGE_EFFECTUE_AVEC_SUCCES'));

        if ('saveAndMail' == $sender->getCommandParameter()) {
            $sendMail = true;
        } else {
            $sendMail = false;
        }
        $this->saveAllEnchere($sendMail);
        //}
    }

    public function saveAllEnchere($sendMail)
    {
        $connexionCom = null;
								try {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexionCom->begin();
            // Sauvegarde de l'enchere :
            $enchere = $this->getEnchere();

            // Si c'est une mise à jour, on supprime toutes entrée des tables
            // relatives a cette enchere
            if ('' != $enchere->getId()) {
                $enchere->clean($connexionCom);
            }
            $consultationId = $enchere->getRefconsultation();
            if ($consultationId) {
                $enchere->setRefconsultation($consultationId);
                $enchere->setIdlot($this->envelop->getSelectedValue());
            }
            $nomAuteur = Atexo_CurrentUser::getLastName();
            $prenomAuteur = Atexo_CurrentUser::getFirstName();
            $enchere->setAuteur($nomAuteur.' '.$prenomAuteur);
            $enchere->setMail(($this->corpsMailModif->Text));
            $enchere->save($connexionCom);

            // Sauvegarde des tranches du Bareme Par tranche
            if ($enchere->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
                $tranchesBaremeEnchere = $this->getViewState('tranchesBaremeEnchere');
                foreach ($tranchesBaremeEnchere as $tranche) {//print_r($tranche);exit;
                    $tranche->setCommonParametrageEnchere($enchere);
                    $tranche->save($connexionCom);
                }
            }
            // Sauvegarde des entreprises selectionnees
            $entreprises = $this->getEntreprisesInvitees();
            $numeroAnonyme = 1;
            foreach ($entreprises as $entreprise) {
                if (strlen($entreprise->getMdp()) > 0) {
                    $entreprise->setMdp($entreprise->getMdp());
                } else {
                    $entreprise->setMdp($entreprise->generateMdp());
                }
                $entreprise->setIdenchere($enchere->getId());
                $entreprise->setNumeroAnonyme($numeroAnonyme);
                $entreprise->save($connexionCom);
                ++$numeroAnonyme;
            }
            // Sauvegarde des references de l'enchere et des valeurs initiales et des tranches du bareme
            // Si le bareme est par tranche
            $enchereReferences = $this->getViewState('enchereReferences');
            foreach ($enchereReferences as $reference) {
                $reference->setIdenchere($enchere->getId());
                /*if ($reference->getQuantite() == 0) {
                    throw new Atexo_Exception("Quantite nulle pour la référence " . $reference->getLibelle());
                } */
                if (0 == strcmp($reference->getTypebaremereference(), Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')) && null == $reference->getNotemaxbaremerelatif()) {
                    throw new Atexo_Exception('Note maximale du bareme relatif non précisée pour la référence '.$reference->getLibelle());
                }
                /*if ($reference->getValeurdepartcommune() == '1' && $reference->getValeurdepart() == null) {
                    throw new Atexo_Exception("Valeur de départ non précisée pour la référence " . $reference->getLibelle());
                }*/
                $reference->save($connexionCom);

                if (0 != strcmp($reference->getValeurdepartcommune(), '1')) {
                    foreach ($reference->getCommonValeursInitiales() as $valeurInitialeTmp) {
                        $valeurInitiale = new CommonEnchereValeursInitiales();
                        $valeurInitiale->setIdencherereference($reference->getId());
                        $valeurInitiale->setIdenchereentreprise($this->getEntrepriseIdFromEmail($valeurInitialeTmp->getEmailEntreprise()));
                        $valeurInitiale->setValeur($valeurInitialeTmp->getValeur());
                        $valeurInitiale->setOrganisme($organisme);
                        $valeurInitiale->save($connexionCom);
                    }
                }
                if ($reference->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')) {
                    if ((is_countable($reference->getCommonTranchesBaremeReference()) ? count($reference->getCommonTranchesBaremeReference()) : 0) >= 1) {
                        foreach ($reference->getCommonTranchesBaremeReference() as $tranche) {
                            $tranche->setIdreference($reference->getId());
                            $tranche->save($connexionCom);
                        }
                    }
                }
            }
            $connexionCom->commit();
        } catch (Exception $e) {
            echo 'erreur<br>';
            echo $e->getMessage();
            $connexionCom->rollback();
            throw $e;
        }
    }

    private function sautLigne()
    {
        return '
';
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
