<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonDCE;
use Application\Service\Atexo\Atexo_ConsultationRma;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * permet de telecharger le DCE a partir du tableau du bord RMA.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since place-2015
 *
 * @copyright Atexo 2016
 */
class DownloadDceRma extends DownloadFile
{
    private $_reference;
    private $_organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $this->_organisme = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['org']));
        //verifiication des droits d'acces à la consultation
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($this->_reference);
        $consultationCriteria->setAcronymeOrganisme($this->_organisme);
        $consultationCriteria->setIdService(false);
        $atexoConsultation = new Atexo_ConsultationRma();
        $consultationFound = $atexoConsultation->search($consultationCriteria);
        $dce = false;
        if (is_array($consultationFound) && count($consultationFound) > 0) {
            $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
            if ($dce instanceof CommonDCE) {
                $this->_idFichier = $dce->getDce();
                $this->_nomFichier = $dce->getNomDce();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $this->_organisme);
            }
        } else {
            $this->response->redirect('agent');
        }
    }
}
