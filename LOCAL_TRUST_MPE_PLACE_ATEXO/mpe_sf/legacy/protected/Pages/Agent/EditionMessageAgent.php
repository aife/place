<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonNewsletter;
use Application\Propel\Mpe\CommonNewsletterPeer;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Newsletter\Atexo_Newsletter_Newsletter;
use Prado\Prado;

/**
 * Permet d'éditer  les Newsletters.
 *
 * @author Houriya MAATALLA <Houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class EditionMessageAgent extends MpeTPage
{
    public $_organisme;
    public $_newsletter;
    public $_connexionCom;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('GestionNewsletter')) {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        }
        if (isset($_GET['Redac'])) {
            $this->messageInfosRedac->setMessage(Prado::localize('DEFINE_TEXT_INFOS_MESSAGE_REDACTEUR'));
            $this->messageInfosRedac->setVisible(true);
        }

        if (!$this->isPostBack) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (Atexo_CurrentUser::hasHabilitation('GererNewsletter') || Atexo_CurrentUser::hasHabilitation('GererNewsletterRedac')) {
                $this->panelValidationSummary->visible = false;
                $serviceId = Atexo_CurrentUser::getCurrentServiceId();
                $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
                if (isset($_GET['service']) && 0 != $_GET['service'] && !empty($_GET['service'])) {
                    $service = (new Atexo_EntityPurchase())->getEntityById(Atexo_Util::atexoHtmlEntities($_GET['service']), $this->_organisme);
                    if (!$service instanceof CommonService){
                        $this->response->redirect('index.php?page=Agent.AgentHome');
                    }
                }
                // Si l'agent est affecté à un service on affiche tous les services en dessous de son pôle
                if (Atexo_CurrentUser::getCurrentServiceId()) {
                    $entities = Atexo_EntityPurchase::getSubServices($serviceId, $this->_organisme, false, true);
                } else {
                    // Si l'agent n'est pas affecté à un service on affiche tout les services
                    $entities = Atexo_EntityPurchase::getEntityPurchase($this->_organisme, true);
                }
                unset($entities[$serviceId]);
                // Affichage de mon service
                $this->myEntity->Text = (new Atexo_EntityPurchase())->getEntityPathById($serviceId, $this->_organisme, true, true);
                $this->listeAutresEntites->Datasource = $entities;
                $this->listeAutresEntites->dataBind();
                //Cas de la modification ou de l'affichage des détails d'une newsletter donnée
                if (isset($_GET['id']) && $_GET['id']) {
                    //Cas de la modification
                    $this->_newsletter = (new Atexo_Newsletter_Newsletter())->retrieveNewsletterById(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), $this->_organisme);
                    $this->setViewState('idNewsletter', $this->_newsletter->getId());
                    //$service = $this->_newsletter->getIdServiceRedacteur();
                    if ($this->_newsletter->getTypeDestinataire() == Atexo_Config::getParameter('MON_ENTITE')) {
                        $this->choixMonEntite->Checked = true;
                        $this->layerMonEntiteInclureServicesDescendants->style = 'display:block';
                        $this->layerListeAutresEntitesInclureServicesDescendants->style = 'display:none';
                        if ('1' == $this->_newsletter->getInclureServicesDescendants()) {
                            $this->monEntiteInclureServicesDescendants->checked = true;
                        } else {
                            $this->monEntiteInclureServicesDescendants->checked = false;
                        }
                    } elseif ($this->_newsletter->getTypeDestinataire() == Atexo_Config::getParameter('AUTRE_ENTITE')) {
                        $this->autreEntite->Checked = true;
                        $this->layerMonEntiteInclureServicesDescendants->style = 'display:none';
                        $this->layerListeAutresEntitesInclureServicesDescendants->style = 'display:block';
                        $this->listeAutresEntites->setSelectedValue($this->_newsletter->getIdServiceDestinataire());
                        if ('1' == $this->_newsletter->getInclureServicesDescendants()) {
                            $this->listeAutresEntitesInclureServicesDescendants->checked = true;
                        } else {
                            $this->listeAutresEntitesInclureServicesDescendants->checked = false;
                        }
                    } elseif ($this->_newsletter->getTypeDestinataire() == Atexo_Config::getParameter('CHOIX_LIBRE')) {
                        $this->choixEntite->Checked = true;
                        $this->layerMonEntiteInclureServicesDescendants->style = 'display:none';
                        $this->layerListeAutresEntitesInclureServicesDescendants->style = 'display:none';
                        $this->listesDestinatairesChoixLibre->Text = $this->_newsletter->getDestinataires();
                    }

                    $this->objet->Text = $this->_newsletter->getObjet();
                    $this->textMail->Text = $this->_newsletter->getCorps();
                    $PJ = $this->_newsletter->getCommonNewsletterPieceJointes(null, $this->_connexionCom);
                    $this->remplirTableauPJ($PJ);

                    //Cas d'un affichage d'une newsletter
                    if (0 == $_GET['action']) {
                        $this->messagePanel->enabled = false;
                        $this->bouttons->visible = false;
                        $this->retour->visible = true;
                        $this->buttonEditPj->visible = false;
                    }
                }

                // Cas d'une création d'une nouvelle newsletter
                else {
                    $this->_newsletter = new CommonNewsletter();
                    $this->_newsletter->setOrganisme($this->_organisme);
                    $this->_newsletter->save($this->_connexionCom);
                    $this->setViewState('idNewsletter', $this->_newsletter->getId());
                }
            } else {
                $this->panelValidationSummary->visible = true;
                $this->messagePanel->visible = false;
                $this->bouttons->visible = false;
            }
        }
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
        $PJ = Atexo_Newsletter_Newsletter::retrieveNewsletterPiceceJointe($this->getViewState('idNewsletter'), $this->_organisme, $connexionCom);
        //Raffraichissement du tableau PJ:
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
        Atexo_Newsletter_Newsletter::DeletePj($idpj, $this->_organisme);
    }

    public function RedirectReturn()
    {
        $redac = null;
        //Cas d'une créaction de newsletter : effacer la newsletter de la base
        if (!(isset($_GET['id']) && $_GET['id'])) {
            (new Atexo_Newsletter_Newsletter())->deleteNewsletter($this->getViewState('idNewsletter'));
        }
        if (isset($_GET['Redac'])) {
            $redac = '&Redac';
        }
        $this->response->redirect('index.php?page=Agent.NewsletterMessagesAgents&service='.Atexo_Util::atexoHtmlEntities($_GET['service']).$redac.'');
    }

    public function save($statut = null)
    {
        $redac = null;
        if ($this->IsValid) {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());

            //Cas d'une modification
            if (isset($_GET['id']) && $_GET['id']) {
                $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
                $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $this->_newsletter = (new Atexo_Newsletter_Newsletter())->retrieveNewsletterById(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), $this->_organisme);
                $this->_newsletter->setDateDerniereModification(date('Y-m-d H:i:s'));
            }
            //Cas d'une création
            else {
                $this->_organisme = Atexo_CurrentUser::getOrganismAcronym();
                $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $this->_newsletter = CommonNewsletterPeer::retrieveByPK($this->getViewState('idNewsletter'), $this->_organisme, $this->_connexionCom);
                $this->_newsletter->setDateCreation(date('Y-m-d H:i:s'));
                $this->_newsletter->setStatut(Atexo_Config::getParameter('STATUS_BROUILLON'));
            }
            //Cas d'un envoi
            if (1 == $statut) {
                $this->_newsletter->setStatut(Atexo_Config::getParameter('STATUS_ENVOI_PLANIFIE'));
                $this->_newsletter->setDateEnvoi(date('Y-m-d 23:00:00'));
                $this->_newsletter->setEnvoyeParEmail(Atexo_Config::getParameter('PF_MAIL_FROM'));
                $this->_newsletter->setEnvoyeParNom($agent->getNom());
                $this->_newsletter->setEnvoyeParPrenom($agent->getPrenom());
            }

            if ($this->choixMonEntite->Checked) {
                $this->_newsletter->setIdServiceDestinataire(Atexo_CurrentUser::getCurrentServiceId());
                $this->_newsletter->setDestinataires('');
                $this->_newsletter->setTypeDestinataire(Atexo_Config::getParameter('MON_ENTITE'));
                if (($this->monEntiteInclureServicesDescendants->checked)) {
                    $this->_newsletter->setInclureServicesDescendants('1');
                } else {
                    $this->_newsletter->setInclureServicesDescendants('0');
                }
            } elseif ($this->autreEntite->Checked) {
                $service = $this->listeAutresEntites->getSelectedValue();
                if ($service && $service !== 0) {
                    $this->_newsletter->setIdServiceDestinataire($service);
                    $this->_newsletter->setDestinataires('');
                    $this->_newsletter->setTypeDestinataire(Atexo_Config::getParameter('AUTRE_ENTITE'));
                }

                if (($this->listeAutresEntitesInclureServicesDescendants->checked)) {
                    $this->_newsletter->setInclureServicesDescendants('1');
                } else {
                    $this->_newsletter->setInclureServicesDescendants('0');
                }
            } elseif ($this->choixEntite->Checked) {
                $this->_newsletter->setDestinataires($this->listesDestinatairesChoixLibre->Text);
                $this->_newsletter->setIdServiceDestinataire(null);
                $this->_newsletter->setInclureServicesDescendants('0');
                $this->_newsletter->setTypeDestinataire(Atexo_Config::getParameter('CHOIX_LIBRE'));
            }

            $this->_newsletter->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $this->_newsletter->setObjet($this->objet->Text);
            $this->_newsletter->setCorps($this->textMail->Text);

            if (isset($_GET['service']) && 0 != $_GET['service'] && !empty($_GET['service'])) {
                $this->_newsletter->setIdServiceRedacteur(Atexo_Util::atexoHtmlEntities($_GET['service']));
            }

            $this->_newsletter->setNomRedacteur($agent->getNom());
            $this->_newsletter->setPrenomRedacteur($agent->getPrenom());
            if (isset($_GET['Redac'])) {
                $this->_newsletter->setIsRedac('1');
                $redac = '&Redac';
            }
            $this->_newsletter->save($this->_connexionCom);
            $this->response->redirect('index.php?page=Agent.NewsletterMessagesAgents&service='.$this->_newsletter->getIdServiceRedacteur().$redac);
        }
    }

    public function sendNewsletter()
    {
        $this->save(1);
    }

    public function saveNewsletter()
    {
        $this->save();
    }

    /**
     * permet de retourner l'url du retour.
     *
     * @return string url
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public function getUrlReturn()
    {
        $redac = null;
        if (isset($_GET['Redac'])) {
            $redac = '&Redac';
        }

        return 'index.php?page=Agent.NewsletterMessagesAgents&service='.Atexo_Util::atexoHtmlEntities($_GET['service']).$redac;
    }
}
