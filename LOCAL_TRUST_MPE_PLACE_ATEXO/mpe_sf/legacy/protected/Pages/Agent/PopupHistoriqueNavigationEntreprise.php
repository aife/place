<?php

namespace Application\Pages\Agent;

use App\Utils\Encryption;
use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use AtexoPdf\PdfGeneratorClient;
use Prado\Prado;

/**
 * Classe PopupHistoriqueNavigationEntreprise.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class PopupHistoriqueNavigationEntreprise extends MpeTPage
{
    public $nbrElementRepeater;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                $objetTrace = (new Atexo_InscritOperationsTracker())->retrieveTraceById($_GET['id']);
                $this->dateAction->Text = Atexo_Util::iso2frnDate($objetTrace->getDate());
                $this->adresseIp->Text = $objetTrace->getAddrIp();
                $this->raisonSociale->Text = $objetTrace->getEntreprise()->getNom();
                $this->nomPrenom->Text = $objetTrace->getNomPrenomInscrit();
                $this->setViewState('idTrace', Atexo_Util::atexoHtmlEntities($_GET['id']));
                $this->fillRepeaterTrace(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $this->setViewState('RaisonSociale', $objetTrace->getEntreprise()->getNom());
            }
        }
    }

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function fillRepeaterTrace($idTrace)
    {
        $nombreElement = (new Atexo_InscritOperationsTracker())->retrieveDetailsTraceByRefConsultation($idTrace, true);
        $this->setViewState('nbrElements', $nombreElement);
        $this->historiquesActionInscrits->setVirtualItemCount($nombreElement);
        $this->historiquesActionInscrits->setCurrentPageIndex(0);
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->historiquesActionInscrits->getCurrentPageIndex() + 1);
        $this->populateData();
    }

    public function populateData($orderBy = '', $tri = '')
    {
        $offset = $this->historiquesActionInscrits->CurrentPageIndex * $this->historiquesActionInscrits->PageSize;
        $limit = $this->historiquesActionInscrits->PageSize;

        if ($offset + $limit > $this->historiquesActionInscrits->getVirtualItemCount()) {
            $limit = $this->historiquesActionInscrits->getVirtualItemCount() - $offset;
        }
        $idTrace = $this->getViewState('idTrace');
        $historques = (new Atexo_InscritOperationsTracker())->retrieveDetailsTraceByRefConsultation($idTrace, false, $orderBy, $tri, $limit, $offset);
        array_walk($historques, function(&$historique){
            if (null !== $historique['lien_download']) {
                $urlParams = [];
                $encrypte = Atexo_Util::getSfService(Encryption::class);
                parse_str( parse_url( $historique['lien_download'], PHP_URL_QUERY), $urlParams );
                if ('Agent.DownloadRecapReponse' === $urlParams['page'] && key_exists('idOffre', $urlParams)) {
                    $urlParams['idOffre'] = $encrypte->cryptId($urlParams['idOffre']);
                    $historique['lien_download'] = 'index.php?' . http_build_query($urlParams);
                }

                if ('Agent.DownloadRecapReponse' === $urlParams['page'] && key_exists('idFile', $urlParams)) {
                    $urlParams['idFile'] = $encrypte->cryptId($urlParams['idFile']);
                    $historique['lien_download'] = 'index.php?' . http_build_query($urlParams);
                }
            }
        });
        $this->setViewState('listeHistoriquesTrace', $historques);
        $nombreElement = $this->getViewState('nbrElements');

        $this->setNbrElementRepeater($nombreElement);
        if (is_countable($historques) ? count($historques) : 0) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->historiquesActionInscrits->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->historiquesActionInscrits->PageSize);
            $this->historiquesActionInscrits->setVirtualItemCount($nombreElement);
            $this->showComponent();

            $this->historiquesActionInscrits->DataSource = $historques;
            $this->historiquesActionInscrits->DataBind();
        } else {
            $this->setViewState('listeHistoriquesTrace', []);
            $this->historiquesActionInscrits->DataSource = [];
            $this->historiquesActionInscrits->DataBind();
            $this->hideComponent();
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->historiquesActionInscrits->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);
        $this->setSensTRi(true);
        if ($this->getViewState('orderBy')) {
            $this->populateData($this->getViewState('orderBy'), $this->getViewState('triInscrtis'));
        } else {
            $this->populateData();
        }
    }

    public function showComponent()
    {
        $this->Page->PagerTop->visible = true;
        $this->Page->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->Page->PagerTop->visible = false;
        $this->Page->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $this->setSensTRi(true);
        if ($this->getViewState('orderBy')) {
            $this->populateData($this->getViewState('orderBy'), $this->getViewState('triInscrtis'));
        } else {
            $this->populateData();
        }
    }

    /*
     * Permet de mettre à jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nbrElements');
        $this->historiquesActionInscrits->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->historiquesActionInscrits->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->historiquesActionInscrits->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->historiquesActionInscrits->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
    }

    public function setSensTRi($noChange = false)
    {
        if ('DESC' == $this->getViewState('triInscrtis') && $noChange) {
            $this->setViewState('triInscrtis', 'DESC');
        } elseif ('DESC' == $this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'ASC');
        } elseif ('ASC' == $this->getViewState('triInscrtis') && $noChange) {
            $this->setViewState('triInscrtis', 'ASC');
        } elseif ('ASC' == $this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'DESC');
        } else {
            $this->setViewState('triInscrtis', 'DESC');
        }
    }

    public function sortTraces()
    {
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->historiquesActionInscrits->getCurrentPageIndex() + 1);
        $this->setSensTRi();
        if (!$this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'ASC');
        }
        $colum = 'date_debut_action';
        $this->setViewState('orderBy', $colum);
        $this->populateData($colum, $this->getViewState('triInscrtis'));
    }

    public function genererExcelHistoriques()
    {
        $idtrace = Atexo_Util::atexoHtmlEntities($_GET['id']);
        (new Atexo_GenerationExcel())->generateExcelHistoriquesInscrits($idtrace, Atexo_Util::atexoHtmlEntities($_GET['consultationId']), Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    public function genererPdfDepot()
    {
        $idtrace = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $sensTri = $this->getViewState('triInscrtis');
        $orderBy = $this->getViewState('orderBy');
        $filePath = (new Atexo_GenerationExcel())->generateExcelHistoriquesInscrits($idtrace, Atexo_Util::atexoHtmlEntities($_GET['consultationId']), Atexo_Util::atexoHtmlEntities($_GET['org']), true, $orderBy, $sensTri);
        if ($filePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
            $nomFichier = Prado::localize('TITRE_HISTORIQUES_NAVIGATION_INSCRIT').'_'.$this->getViewState('RaisonSociale').'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf';
            @unlink($filePath);
            DownloadFile::downloadFileContent($nomFichier, $pdfContent);
            exit;
        }
    }

    /**
     * Permet de modifier l'affichage de la description.
     *
     * @param $description
     *
     * @return mixed
     */
    public function modifierAffichageDescriptionNavigation($description)
    {
        return str_replace("\n", '<br/>', $description);
    }
}
