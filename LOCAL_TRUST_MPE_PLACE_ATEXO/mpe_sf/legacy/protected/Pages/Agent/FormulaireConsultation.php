<?php

namespace Application\Pages\Agent;

use App\Service\AgentService;
use App\Service\AtexoFavoris;
use App\Service\Clauses\ClauseUsePrado;
use App\Service\Consultation\ConsultationService;
use App\Service\Consultation\FavorisService;
use App\Service\ConsultationTags;
use App\Service\ConsultationTagsUsePrado;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategoriesConsiderationsSociales;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_FormePrix;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Exception\ExceptionDceLimit;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Consultation;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Exception;
use Prado\Prado;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Formulaire de gestion des consultations.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version   4
 */
class FormulaireConsultation extends MpeTPage
{
    public const ACCORD_CADRE_MARCHE_SUBSEQUENT = 'AC-MS';
    public ?string $langue = null;
    public $consultation;
    public $categoriesConsiderationsSociales;
    public $organisme;
    public $reference;
    public $xml;
    public $mode;
    public $consultationServiceIdSave;

    /**
     * recupere la consultation.
     */
    public function getConsultation()
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->getViewState('consultation');
        }

        return $this->consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    // setConsultation()

    /**
     * @return CommonCategoriesConsiderationsSociales|mixed
     */
    public function getCategoriesConsiderationsSociales()
    {
        if (!($this->categoriesConsiderationsSociales instanceof CommonCategoriesConsiderationsSociales)) {
            $this->categoriesConsiderationsSociales = $this->getViewState('categoriesConsiderationsSociales');
        }

        return $this->categoriesConsiderationsSociales;
    }

    /**
     * recupere la langue.
     */
    public function getLangue()
    {
        return $this->langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->langue !== $langue) {
            $this->langue = $langue;
        }
    }

    // setLangue()

    /**
     * recupere l'organisme.
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    // getOrganisme()

    /**
     * Affecte la valeur de l'organisme.
     *
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        if ($this->organisme !== $organisme) {
            $this->organisme = $organisme;
        }
    }

    // setOrganisme()

    /**
     * recupere la reference.
     */
    public function getReference()
    {
        return $this->reference;
    }

    // getReference()

    /**
     * Affecte la valeur de la reference.
     *
     * @param string $reference
     */
    public function setReference($reference)
    {
        if ($this->reference !== $reference) {
            $this->reference = $reference;
        }
    }

    // setReference()

    /**
     * recupere l'xml.
     */
    public function getXml()
    {
        return $this->xml;
    }

    // getXml()

    /**
     * Affecte la valeur de l'xml.
     *
     * @param string $xml
     */
    public function setXml($xml)
    {
        if ($this->xml !== $xml) {
            $this->xml = $xml;
        }
    }

    // setXml()

    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        //Initialisation d'elements
        $this->initialisationElements();

        //Gestion acces ala page et fil d'ariane
        $this->gererAccesPageMessageErreurFilAriane();

        //Masquer messages
        $this->masquerMessages();

        if (!$this->isPostBack) {
            //Initialisation des etapes
            $this->initialiserEtapes();

            //Afficher et masquer des composants
            $this->customizeForm();

            //Recuperation de la consultation
            $this->recupererConsultation();

            //Chargement des composants avec l'objet consultation
            $this->chargerComposants();

            //Affichage message flottant pour la demande de la validation
            $this->afficherPopUpDemandeValidation();
        }

        //Affectation validation des etapes
        $this->etapesConsultation->setValidationGroup('validateSave');

        $this->consultationServiceIdSave = $this->consultation->getServiceId();
    }

    /**
     * Verifie que la consultation est en mode creation.
     */
    public function isModeCreationCons()
    {
        if (empty($this->reference) && empty($_GET['continuation']) && empty($this->xml)) {
            return true;
        }

        return false;
    }

    /**
     * Verifie que la consultation est en mode modification.
     */
    public function isModeModifCons()
    {
        if (!empty($this->reference) && empty($_GET['continuation'])) {
            return true;
        }

        return false;
    }

    /**
     * Verifie que la consultation est en mode creation de suite.
     */
    public function isModeCreationSuite()
    {
        if (isset($_GET['continuation']) && (1 == $_GET['continuation']) && !empty($this->reference)) {
            return true;
        }

        return false;
    }

    /**
     * Determine le mode de gestion.
     */
    public function getModeConsultation()
    {
        if (self::isModeCreationCons() || self::isModeCreationSuite()) {
            return Atexo_Config::getParameter('MODE_CREATION_CONSULTATION');
        } elseif (self::isModeModifCons()) {
            return Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION');
        }
    }

    /**
     * Permet de charger les composants.
     */
    public function chargerComposants()
    {
        //Consultation
        $consultation = $this->getConsultation();
        $this->bloc_etapeIdentification->setConsultation($consultation);
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->bloc_etapeDonneesComplementaires->setConsultation($consultation);
            $this->bloc_etapeDonneesComplementaires->chargerComposants();
        }
        $this->bloc_etapeDetailsLots->setConsultation($consultation);
        $this->bloc_etapeDetailsLots->setMode($this->getModeConsultation());
        if (!empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) || $this->isModeCreationSuite()) {
            $this->bloc_etapeDumeAcheteur->chargerDumeAcheteur($consultation);
        }
        $this->bloc_etapeCalendrier->setConsultation($consultation);
        $this->bloc_etapeDocumentsJoints->setConsultation($consultation);
        $this->bloc_etapeModalitesReponse->setConsultation($consultation);
        $this->loadDroitAcces($this->getModeConsultation(), $consultation);
        //Mode consultation
        $this->bloc_etapeIdentification->setMode($this->getModeConsultation());
        $this->bloc_etapeModalitesReponse->setMode($this->getModeConsultation());
    }

    /**
     * Permet de recuperer la consultation selon les cas.
     */
    public function recupererConsultation()
    {
        if (self::isModeModifCons()) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->reference, $this->organisme);
            $statutConsultation = Atexo_Consultation::getStatus($consultation, false);
            // Consultation simpliee - on ne gère pas :
            //  - PERMETTRE_ACCES_FORMULAIRE_AMONT_EN_MODIFICATION_APRES_VALIDATION (activé au SIMAP)
            //  - getEtatValidation()
            if (
                (
                    $consultation->getTypeProcedureOrg($this->organisme)
                    && true === $consultation->getTypeProcedureOrg($this->organisme)->getProcedureSimplifie()
                )
                && (
                    $statutConsultation === Atexo_Config::getParameter('STATUS_PREPARATION')
                    || $statutConsultation === Atexo_Config::getParameter('STATUS_ELABORATION')
                )
            ) {
                $link = 'agent/consultation/simplifiee/' . $consultation->getId() . '/modifier';
                $this->response->redirect($link);
            }
            if (
                ($statutConsultation !== Atexo_Config::getParameter('STATUS_PREPARATION') && $statutConsultation !== Atexo_Config::getParameter('STATUS_ELABORATION'))
                || (0 == Atexo_Config::getParameter('PERMETTRE_ACCES_FORMULAIRE_AMONT_EN_MODIFICATION_APRES_VALIDATION') && 1 == $consultation->getEtatValidation())
            ) {
                $link = 'agent/consultation/modification-en-ligne/' . $consultation->getUuid();
                if ((isset($_GET['xml']))) {
                    Atexo_CurrentUser::writeToSession('xml', $_GET['xml']);
                }
                $this->response->redirect($link);
            }
            $this->consultationExiste->Value = 1;
        } elseif (self::isModeCreationSuite()) {
            $oldConsultation = (new Atexo_Consultation())->retrieveConsultation($this->reference, $this->organisme);
            $consultation = $this->copierConsultation($oldConsultation);
            // Ajout des lots ala consultation
            $arrayLots = $this->recupererLotsAncienneConsultation($oldConsultation, $this->reference);
            $arrayCopieLots = [];
            $ltReferentielsLot = [];
            if (is_array($arrayLots) && count($arrayLots)) {
                foreach ($arrayLots as $lot) {
                    $numLot = $lot->getLot();
                    $copieLot = $lot->copy();
                    $copieLot->setLot($numLot);
                    $copieLot->setNew(true);
                    $arrayCopieLots[] = $copieLot;
                    $ltReferentielsLot[$numLot] = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielByLotouConsulation($oldConsultation->getOrganisme(), $oldConsultation->getId(), 1, null, $numLot);
                }
            }
            $this->setViewState('refConsParent', $oldConsultation->getId());
            $this->setViewState('idDonneeComplementaireParent', $oldConsultation->getIdDonneeComplementaire());
            $this->setViewState('listeDesLots', $arrayCopieLots);
            $this->setViewState('ReferentielLots', $ltReferentielsLot);

            //Debut continuation (suite) accord cadre - systemes d'acquisition dynamique
            $org_init = $this->recupererOrganismeInit();
            if ('' != $org_init) {
                $consultation->setOrganismeConsultationInit($org_init);
            }
            //Fin continuation (suite) accord cadre - systemes d'acquisition dynamique
        } elseif ($this->xml) {//Cas interface MARCO
            $xml = html_entity_decode(base64_decode(str_replace(' ', '+', $this->xml)));
            $interfaceConsultation = new Atexo_Interfaces_Consultation();
            $consultations = $interfaceConsultation->createConsultationObjectFromXml($xml, $this->organisme);
            $consultation = array_shift($consultations);
            Atexo_CurrentUser::writeToSession('ServiceMetier', 'mpe');
        } else {
            $consultation = new CommonConsultation();
            if (Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
                $adresse = (new Atexo_Consultation())->retreiveAdressesServices($idService, $organisme);
                if ($adresse) {
                    if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                        $consultation->setAdresseRetraisDossiers($adresse->getAdresseRetraisDossiersTraduit());
                        $adresseRetraisDossiers = 'setAdresseRetraisDossiers' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                        $consultation->$adresseRetraisDossiers($adresse->getAdresseRetraisDossiersTraduit());
                    }
                    if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                        $consultation->setAdresseDepotOffres($adresse->getAdresseDepotOffresTraduit());
                        $adresseDepotOffre = 'setAdresseDepotOffres' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                        $consultation->$adresseDepotOffre($adresse->getAdresseDepotOffresTraduit());
                    }
                    if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                        $consultation->setLieuOuverturePlis($adresse->getLieuOuverturePlisTraduit());
                        $lieuOuverture = 'setLieuOuverturePlis' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                        $consultation->$lieuOuverture($adresse->getLieuOuverturePlisTraduit());
                    }
                }
            }
        }

        $this->consultation = $consultation;
        $this->setViewState('consultation', $consultation);
    }

    /**
     * Permet de masquer et afficher des elements.
     */
    public function customizeForm()
    {
        $this->errorPart->visible = false;
    }

    /**
     * Permet d'initialiser des elements.
     */
    public function initialisationElements()
    {
        if (isset($_GET['id']) && $_GET['id']) {
            $this->reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        }
        if ($_GET['org']) {
            $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $this->organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $this->xml = Atexo_CurrentUser::readFromSession('xml');
    }

    /**
     * Permet de sauvegarder la consultation
     * Sauvegarde tous les onglets de la consultation.
     *
     * @param $sender
     * @param $param
     */
    public function enregistrerConsultation($sender, $param)
    {
        if ($this->isValid) {
            $validation = false;
            if ('validateBack' == $param->CallbackParameter) {
                $validation = true;
            }
            //Debut enregistrement des etapes
            $etapeSuivane = $_GET['etape'] ?? $this->getViewState('etapeCourante');

            $return = $this->enregistrer($validation, $etapeSuivane, $sender, $param);

            // On affiche de nouveau le bloc boutons après validation
            $scriptJs = "<script>document.querySelector('#ctl0_CONTENU_PAGE_blocBoutons').style.display = '';</script>";
            $this->scriptJs->Text .= $scriptJs;

            if (is_array($return) && true == $return['erreur']) {
                $this->afficherMessageErreur($return['messageErreur']);
                $this->raffraichirPanelBlocMessages($sender, $param);

                return;
            }

            //Redirection
            $this->rediriger($sender, $param);
        }
    }

    public function loadDroitAcces($mode, $consultation)
    {
        if ($mode == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
            $idServiceConsultation = Atexo_CurrentUser::getCurrentServiceId();
            $this->DroitsAcces->setIdService($idServiceConsultation);
            $this->DroitsAcces->setIdServiceAssocie($idServiceConsultation);
        } else {
            $this->DroitsAcces->setIdService($consultation->getServiceId());
            $this->DroitsAcces->setIdServiceAssocie($consultation->getServiceAssocieId());
            $this->DroitsAcces->setIdRpa($consultation->getIdRpa());
            $this->DroitsAcces->setReference($consultation->getId());
        }

        $this->DroitsAcces->setPostBack(true);
        $this->DroitsAcces->remplirMonEntite();
        $this->DroitsAcces->remplirEntiteAssociee();
        if (Atexo_Module::isEnabled('GestionMandataire') || Atexo_CurrentUser::hasHabilitation('RattachementService')) {
            $this->DroitsAcces->remplirEntite();
        }
        $this->DroitsAcces->fillListRpa();
        $this->DroitsAcces->displayGuests();
        $this->setViewState('nbreInvites', $this->DroitsAcces->nbrInvite->Text);
    }

    /**
     * Permet de masquer toutes les etapes.
     */
    public function masquerToutesEtapes()
    {
        $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_bloc_etapeIdentification').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_bloc_etapeDonneesComplementaires').style.display = 'none';</script>";
        }
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDetailsLots_bloc_etapeDetailsLots').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_bloc_etapeDumeAcheteur').style.display = 'none';</script>";
        }
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_bloc_etapeCalendrier').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_bloc_etapeDocumentsJoints').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_bloc_etapeModalitesReponse').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDroitsAcces_bloc_etapeDroitsAcces').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('Publicite')) {
            if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultationConcentrateurV2_bloc_PubliciteConsultationConcentrateurV2').style.display = 'none';</script>";
            } else {
                $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultation_bloc_PubliciteConsultation').style.display = 'none';</script>";
            }
        }
        $this->Page->scriptJs->Text .= $script;
    }

    public function saveDroisAcces(&$consultation, $modification = false)
    {
        if ($this->getModeConsultation() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
            if (Atexo_Module::isEnabled('GestionMandataire') || Atexo_CurrentUser::hasHabilitation('RattachementService')) {
                $serviceId = $this->DroitsAcces->entitee->getSelectedValue();
                $consultation->setServiceId($serviceId > 0 ? $serviceId : null);
            } else {
                $serviceId = Atexo_CurrentUser::getCurrentServiceId();
                $consultation->setServiceId($serviceId > 0 ? $serviceId : null);
            }
        } else {
            if (Atexo_Module::isEnabled('GestionMandataire') || Atexo_CurrentUser::hasHabilitation('RattachementService')) {
                $serviceId = $this->DroitsAcces->entitee->getSelectedValue();
                $consultation->setServiceId($serviceId > 0 ? $serviceId : null);
            }
        }

        $consultation->setServiceAssocieId($this->DroitsAcces->entiteeAssociee->getSelectedValue() == 0 ? null : $this->DroitsAcces->entiteeAssociee->getSelectedValue());
        if (!$modification) {
            $consultation->setIdCreateur(Atexo_CurrentUser::getIdAgentConnected());
            $consultation->setNomCreateur(Atexo_CurrentUser::getFirstNameAgentConnected());
            $consultation->setPrenomCreateur(Atexo_CurrentUser::getLastNameAgentConnected());
        }
        if (0 != $this->DroitsAcces->representantAdjudicateur->getSelectedValue()) {
            $idRpa = $this->DroitsAcces->representantAdjudicateur->getSelectedValue();
            $consultation->setIdRpa($idRpa);
        } else {
            $consultation->setIdRpa(null);
        }
        $invitesChoisis = Atexo_Consultation_Guests::toArrayGuestsVo((new Atexo_Consultation_Guests())->createArrayGuestFromValueString($this->Page->invites->Value));
        $this->setViewState('invitesChoisis', $invitesChoisis);
        Atexo_Consultation::saveConsultationGuests($consultation, $invitesChoisis);

        return $invitesChoisis;
    }

    /**
     * Permet d'enregistrer la consultation.
     *
     * @param $sender
     * @param $param
     * @param CommonConsultation $consultation
     */
    public function enregistrer($validation = false, $etapeSuivante = '', $sender = null, $param = null)
    {
        $connexion = null;
        if ($this->isValid) {
            $consultation = $this->getViewState('consultation'); //Recuperation de la consultation
            $etapeCourante = $_GET['etape'] ?? $this->getViewState('etapeCourante');
            $modification = true;
            $favoris = false;
            try {
                if ($consultation->getId()) {
                    $favoris = true;
                }

                //Sauvegarde de la consultation
                $statutPub = null;
                if ('validateBack' == $param->CallbackParameter) {
                    $statutPub = Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET');
                }
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexionRead = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
                $connexion->beginTransaction();
                $typeFormulaire = ($consultation instanceof CommonConsultation) ? $consultation->getTypeFormulaireDume() : 0;
                if ('etapeIdentification' == $etapeCourante) {
                    if (!$consultation->getId()) {
                        $modification = false;
                        $consultation->setVersionMessagerie(1);
                        if (Atexo_Module::isEnabled('messagerieV2')) {
                            $consultation->setVersionMessagerie(2);
                        }
                        $plateformeVirtuelle = (new CommonPlateformeVirtuelleQuery())
                            ->findOneByArray(
                                [
                                    'domain' => $_SERVER['SERVER_NAME'],
                                ],
                                $connexionRead
                            );
                        if ($plateformeVirtuelle instanceof CommonPlateformeVirtuelle) {
                            $consultation->setPlateformeVirtuelleId($plateformeVirtuelle->getId());
                        }
                    }
                    $this->bloc_etapeIdentification->save($consultation, $connexion, $modification);
                } elseif (Atexo_Module::isEnabled('DonneesComplementaires') && 'etapeDonneesComplementaires' == $etapeCourante) {
                    $this->bloc_etapeDonneesComplementaires->saveDonneComplementaire($consultation, true, $param);
                } elseif ('etapeDetailsLots' == $etapeCourante) {
                    $this->bloc_etapeDetailsLots->save($consultation);
                } elseif ('etapeCalendrier' == $etapeCourante) {
                    $this->bloc_etapeCalendrier->save($consultation);
                } elseif ('etapeDocumentsJoints' == $etapeCourante) {
                    $this->bloc_etapeDocumentsJoints->save($consultation);
                } elseif ('etapeModalitesReponse' == $etapeCourante) {
                    $this->bloc_etapeModalitesReponse->save($consultation);
                } elseif (('PubliciteConsultation' == $etapeCourante) && (!Atexo_Config::getParameter('CONCENTRATEUR_V2'))) {
                    if (Atexo_Module::isEnabled('Publicite')) {
                        $this->bloc_PubliciteConsultation->creerAnnonce($consultation, $statutPub);
                    }
                } elseif ('etapeDumeAcheteur' == $etapeCourante) {
                    if (Atexo_Module::isEnabled('interfaceDume')) {
                        $this->bloc_etapeDumeAcheteur->saveDumeAcheteur($consultation);
                    }
                }
                if (Atexo_Module::isEnabled('Publicite') && Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                    $this->publiciteRecap->envoyerAnnonce($consultation);
                }
                $invitesChoisis = $this->saveDroisAcces($consultation, $modification);
                (new Atexo_Consultation())->saveIdServiceValidation($consultation, $this->Page->DroitsAcces->entitee->getSelectedValue(), isset($_GET['continuation']), isset($_GET['id']));
                $returnValue = $this->verifierUniciteConsultation($consultation); //Verification de l'unicitade la consultation
                if (true == $returnValue) {
                    return ['erreur' => true, 'messageErreur' => Prado::localize('CONSULTATION_EXISTE')];
                }
                if (!$this->modeCreationSuiteAcSad()) {
                    $organismeCons = $this->organisme; //Organisme de rattachement de la consultation
                } else {
                    $organismeCons = Atexo_CurrentUser::getCurrentOrganism(); //Organisme de rattachement de la consultation
                }
                $consultation->setOrganisme($organismeCons);
                $consultation->saveConsultation($connexion);
                if ($this->getModeConsultation() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
                    if (!$consultation->getIdAffaire()) {//cas creation consultation normale, Pour la creation de la suite,nous avons $consultation->getIdAffaire renseignalors de la copie de consultation
                        $consultation->setIdAffaire($consultation->getId());
                    }
                } elseif ($this->bloc_etapeIdentification->idAffaire->value) {
                    $consultationParent = CommonConsultationPeer::retrieveByPK($this->reference, $connexion);
                    $consultation->setIdAffaire($consultationParent->getIdAffaire());
                }
                $etatEnAttenteValidation = '0';
                if ($validation) {
                    $etatEnAttenteValidation = '1';
                    //calcul de la variante et l'affectation ala consultation
                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                        $varianteCalculee = $this->calculerVarianteConsultation($consultation);
                        $consultation->setVarianteCalcule($varianteCalculee);
                    }
                }
                $consultation->setEtatEnAttenteValidation($etatEnAttenteValidation);
                $this->updateVarianteCalculeeDonneesComplementaires($consultation, $connexion); // la variante calcule
                if (Atexo_Module::isEnabled('Publicite') && empty($consultation->getIdDonneeComplementaire()) && !empty($this->Page->getViewState('idDonneeComplementaire'))) {
                    $consultation->setIdDonneeComplementaire($this->Page->getViewState('idDonneeComplementaire'));
                }
                $consultation->save($connexion); //Enregistrement de la consultation
                if (Atexo_Module::isEnabled('interfaceDume')) {
                    if ('etapeIdentification' == $etapeCourante) {
                        $this->bloc_etapeDumeAcheteur->createDumeAcheteur($consultation, ($typeFormulaire != $consultation->getTypeFormulaireDume()));
                    }
                }

                if (!Atexo_Util::isSiretValide($consultation->getSiretAcheteur())) {
                    $this->msgControlSiret->displayStyle = 'Dynamic';
                    if ($param) {
                        $this->msgControlSiret->render($param->getNewWriter());
                    }
                } else {
                    $this->msgControlSiret->displayStyle = 'None';
                }

                $this->DroitsAcces->saveHistoriqueListeInvites($consultation, $this->getViewState('nbreInvites'), is_countable($invitesChoisis) ? count($invitesChoisis) : 0, $invitesChoisis, $modification);
                if (!$modification) { // enregistrer pour la premiere fois avec statut creation
                    $this->bloc_etapeDonneesComplementaires->saveDonneComplementaire($consultation, $modification);
                    $this->bloc_etapeCalendrier->save($consultation, $modification);
                    $this->bloc_etapeDocumentsJoints->save($consultation, $modification);
                    $this->bloc_etapeModalitesReponse->save($consultation, $modification);

                    $consultation->saveConsultation($connexion);
                    $this->bloc_etapeIdentification->sauvegarderHistoriqueDonneesComplementaires($consultation, $connexion, $modification, []); //Sauvegarde de historique des donnees complementaires
                }
                if (self::isModeCreationSuite()) {
                    if ($this->getViewState('idDonneeComplementaireParent') == $consultation->getIdDonneeComplementaire()) {
                        (new Atexo_Consultation())->copierDonneesComplementaires($consultation, $connexion); //Sauvegarde des donnees complementaires de la consultation
                    }
                    $lots = $this->getViewState('listeDesLots');
                    $refLotsConsParent = $this->getViewState('ReferentielLots');
                    $this->setViewState('ReferentielLots', ''); //ecraser la valeur pour ne pas les enregister une 2eme fois
                    (new Atexo_Consultation_Lots())->saveLots($lots, $consultation, $connexion, true, $refLotsConsParent); //Enregistrement des lots avec leurs donnees complementaires
                }
                $lots = $consultation->getCommonCategorieLots(); // Le cas d'insertion des lots avec le connecteur MARCO/SIS
                if (is_array($lots) && count($lots)) {
                    (new Atexo_Consultation_Lots())->saveLots($lots, $consultation, $connexion);
                }

                try {
                    if ($favoris === false) {
                        $this->addConsultationFavoris(
                            Atexo_CurrentUser::getId(),
                            $consultation->getId(),
                            $connexion
                        );
                    } elseif ($consultation->getServiceId() != $this->consultationServiceIdSave) {
                        $this->repeatAddConsultationFavoris(
                            Atexo_CurrentUser::getId(),
                            $consultation->getId(),
                            $connexion
                        );
                    }
                } catch (Exception $exception) {
                    $logger = Atexo_LoggerManager::getLogger('consultation');
                    $logger->error("Problème lors de la création d'une consultation ." . $exception->__toString());
                }
                $connexion->commit();
                try {
                    $this->saveConsultationFavorisInvitePonctuel($consultation->getId(), Atexo_CurrentUser::getOrganismAcronym(), $connexion);
                } catch (Exception $exception) {
                    $logger = Atexo_LoggerManager::getLogger('consultation');
                    $logger->error("Problème lors de l'ajout favoris des invités ponctuels ." . $exception->__toString());
                }

                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $this->saveAchatResponsable($consultation->getId(), $this->bloc_etapeIdentification->achatResponsableConsultation->achatResponsableFormValue->Data);
                }
                if (Atexo_Module::isEnabled('typageJo2024')) {
                    $this->saveTypageJO2024($consultation->getId(), $this->bloc_etapeIdentification->typageJoFormValue->Data);
                }

                if ('validateBack' == $param->CallbackParameter) {
                    if (Atexo_Module::isEnabled('Publicite') && ('PubliciteConsultation' != $etapeCourante) && !Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                        $this->bloc_PubliciteConsultation->updateStatutAnnonce($consultation, $statutPub);
                    }
                    //get connected Agent
                    $agent = new CommonAgentQuery();
                    $agent = $agent->findPk(Atexo_CurrentUser::getId());
                    //check if agent has already this ability or not before sending the alert
                    if ($agent->getAlerteValidationConsultation()) {
                        $this->envoyerAlerteAgentValidation($consultation);
                    }
                    $retour = self::saveAlerteMetier($consultation, $connexion);
                    if (is_array($retour) && true == $retour['erreur']) {
                        return $retour;
                    }
                    if (Atexo_Module::isEnabled('interfaceDume')) {
                        $retour = $this->bloc_etapeDumeAcheteur->validateDumeAcheteur($consultation);
                        if (is_array($retour) && true == $retour['erreur']) {
                            return $retour;
                        }
                    }
                }
                $this->setViewState('consultation', $consultation);
                $this->setConsultation($consultation);
                $this->setViewState('mode', Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION'));
                if ('saveBack' != $param->CallbackParameter && 'validateBack' != $param->CallbackParameter) {
                    $this->chargerFormulaireEtape($etapeSuivante, $consultation, $sender, $param);
                }
                $this->setViewState('etapeCourante', $etapeSuivante);
                $this->etapesConsultation->onLoad($param);
                if ('save' == $param->CallbackParameter) {//Affichage message de confirmation de prise en compte
                    $this->messageConfirmationEnregistrement->setVisible(true);
                    $this->messageConfirmationEnregistrement->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
                    $this->labelServerSide->Text = "<script>goToTopPage('messageConfirmationEnregistrement');</script>";
                    $this->raffraichirPanelBlocMessages($sender, $param);
                }
                $this->consultationExiste->Value = 1;
                if (Atexo_Module::isEnabled('Publicite') && 'validateBack' == $param->CallbackParameter && !Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                    $this->bloc_PubliciteConsultation->deleteAnnonce($consultation);
                }
            } catch (ExceptionDceLimit $e) {
                $connexion->rollBack();
                $msg = Prado::localize(
                    'TEXT_TAILLE_AUTORISEE_DOC',
                    [
                        'size' => Atexo_Util::arrondirSizeFile((Atexo_Config::getParameter('MAX_UPLOAD_FILE_SIZE') / 1024)),
                    ]
                );
                $script = "window.document.getElementById('divLinkChangeFile').style.display = 'block';";
                $this->scriptJs->Text = '<script>' . $script . '</script>';

                return ['erreur' => true, 'messageErreur' => $msg];
            } catch (Exception $e) {
                $logger = Atexo_LoggerManager::getLogger('consultation');
                $logger->error("Problème lors de l'enregistrement de la consultation ." . $e->__toString());
                $connexion->rollBack();
                throw $e;
            }
        }
    }

    /*
    * Fonction qui lance les alertes metier de cette page
    */

    public function saveAlerteMetier($consultation, $connexion)
    {
        $return = [];
        if (Atexo_Module::isEnabled('AlerteMetier')) {
            $alertesBloquante = Atexo_AlerteMetier::lancerAlerte('FormulaireConsultation', $consultation, $connexion);
            if (is_array($alertesBloquante) && count($alertesBloquante) > 0) {
                $return['erreur'] = true;
                $return['messageErreur'] = Atexo_Util::getMessageFromListe(prado::localize('TEXT_ALERTES') . ' : ', $alertesBloquante);

                return $return;
            }
        }

        return false;
    }

    /**
     * Permet de faire la redirection apres enregistrement
     * Le parametre "save" refert au succes de l'enregistrement lors de la creation ou de la modification d'une consultation
     * Le parametre "checkedEnv" refert a la coche d'un type d'enveloppe dans l'onglet "Modalites de reponse" lors de la creation ou de la modification d'une consultation.
     *
     * @param $sender
     * @param $param
     * @param CommonConsultation $consultation
     */
    public function rediriger($sender, $param)
    {
        if ('saveBack' == $param->CallbackParameter || 'validateBack' == $param->CallbackParameter) {
            $consultation = $this->getConsultation();
            $redirectMol = false;
            //Verifier si aucune enveloppe n'est selectionnee
            if ('validateBack' == $param->CallbackParameter) {
                if (Atexo_Module::isEnabled('Publicite')) {
                    if ($consultation instanceof CommonConsultation) {
                        if ($consultation->getDonneePubliciteObligatoire()) {
                            if (!Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                                $supports = $this->bloc_PubliciteConsultation->supportSelected($consultation, $this->getViewState('etapeCourante'));
                                if (is_array($supports)) {
                                    foreach ($supports as $oneSupport) {
                                        if ('' == $oneSupport->getCodeSupport()) {
                                            $redirectMol = true;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $redirectMol = $this->publiciteRecap->isMolSelected();
                            }
                        }
                    }
                }
            }
            $noCheckedEnveloppe = 0;
            if (
                (!$consultation->getEnvCandidature()
                    && !$consultation->getEnvOffreTechnique()
                    && !$consultation->getEnvOffre())
                && $consultation->getAutoriserReponseElectronique()
            ) {
                $noCheckedEnveloppe = 1;
            }
            //Verifier si aucun DCE n'est joint ala consultation
            $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $consultation->getOrganisme());
            $withDce = '';
            if (!($dce instanceof CommonDCE) || !$dce->getDce()) {
                $withDce = '&withoutDce';
                if ($consultation->getIdDossierVolumineux() && Atexo_Module::isEnabled('ModuleEnvol')) {
                    $withDce .= '&addDv';
                }
            }

            //Verifier etat date fin
            $etatDateFin = '';
            if ($consultation->getDatefin() < date('Y-m-d h:i:s')) {
                $etatDateFin = '&etatDateFin';
            }

            //Verifier etat date fin et date création
            $etatDateCreationDateFin = '';
            if (
                Atexo_Config::getParameter('DELAI_DLRO')
                && $consultation->getDatefin()
                && Atexo_Util::diffJours(Atexo_Util::iso2frnDate($consultation->getDatedebut()), Atexo_Util::iso2frnDate($consultation->getDatefin())) < Atexo_Config::getParameter('DELAI_DLRO')
            ) {
                $etatDateCreationDateFin = '&etatDateCreationDateFin';
            }

            $dateFinContratNonValide = '';
            //verifier date de fin du contrat AC si consultation marche subséquent à AC
            if (
                ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($consultation->getTypeMarche())
                    || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($consultation->getTypeMarche()))
                && !is_null($consultation->getNumeroAc())
            ) {
                //recuperer le contrat
                $criteriaVo = new Atexo_Contrat_CriteriaVo();
                $criteriaVo->setNumeroContrat($consultation->getNumeroAc());
                $criteriaVo->setIdContratTitulaire($consultation->getIdContrat());
                $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                $contrats = $commonTContratTitulaireQuery->getContratByCriteres($criteriaVo);
                foreach ($contrats as $contrat) {
                    if (strtotime($contrat->getDateFinContrat('d-m-Y')) <= strtotime(date('d-m-Y')) && $contrat->getDateMaxFinContrat() !== null) {
                        $dateFinContratNonValide = '&dateFinContratInvalid';
                    }
                }
            }

            //verification Siret
            $siretNonValide = '';
            if (!Atexo_Util::isSiretValide($consultation->getSiretAcheteur())) {
                $siretNonValide = '&siretInvalid';
            }
            $urlRedirect = 'index.php?page=Agent.TableauDeBord&id=' . $this->getConsultation()->getId() . "&save=true&noCheckedEnv=$noCheckedEnveloppe&traductionOgl" . $withDce . $etatDateFin . $siretNonValide . $etatDateCreationDateFin . $dateFinContratNonValide;
            if ($redirectMol) {
                $urlRedirect = 'index.php?page=Agent.PubliciteConsultation&id=' . base64_encode($consultation->getId()) . '&justMol=1';
            }
            $this->response->redirect($urlRedirect);
        } elseif (
            'save' === $param->CallbackParameter
            && Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')
            && $this->isModeCreationSuite()
            && ($consultation = $this->getConsultation())
        ) {
            $this->response->redirect(
                'agent/consultation/modification/' . $consultation->getUuid()
            );
        }
    }

    /**
     * Permet de raffraichir le formulaire.
     *
     * @param $sender
     * @param $param
     */
    public function raffraichirFormulaire($sender, $param)
    {
        if ($param) {
            $this->panelBlocsEtapes->render($param->getNewWriter());
            $this->panelBlocsEtapesBas->render($param->getNewWriter());
            $this->panelBlocMessages->render($param->getNewWriter());
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    /**
     * Permet d'executer la fonction qui charge le formulaire.
     *
     * @param $blocEtapeCible: id du composant contenant le formulaire
     */
    public function chargerFormulaireEtape($etapeCible, $consultation, $sender, $param)
    {
        $blocEtapeCible = 'bloc_' . $etapeCible;
        switch ($etapeCible) {
            case 'etapeIdentification':
                $this->$blocEtapeCible->setConsultation($consultation);
                $this->$blocEtapeCible->chargerFormulaireIdentification();
                break;
            case 'etapeDonneesComplementaires':
                /* faire traitement ici */
                $this->$blocEtapeCible->setConsultation($consultation);
                $this->$blocEtapeCible->chargerComposants(true);
                break;
            case 'etapeDetailsLots':
                $this->$blocEtapeCible->setConsultation($consultation);
                if (self::isModeCreationSuite()) {
                    $lots = $this->getViewState('listeDesLots');
                    $this->$blocEtapeCible->setLots($lots);
                }
                $this->$blocEtapeCible->chargerComposant();
                $this->$blocEtapeCible->refreshRepeater($sender, $param);
                break;
            case 'etapeCalendrier':
                $this->$blocEtapeCible->setConsultation($consultation);
                $this->$blocEtapeCible->fillComposants($consultation);
                break;
            case 'etapeDocumentsJoints':
                $this->$blocEtapeCible->setConsultation($consultation);
                $this->$blocEtapeCible->chargerComposants();
                break;
            case 'etapeModalitesReponse':
                $this->$blocEtapeCible->setConsultation($consultation);
                $this->$blocEtapeCible->chargerFormulaireModalitesReponses();
                break;
            case 'etapeDroitsAcces':
                $this->DroitsAcces->setConsultation($consultation);
                $this->DroitsAcces->setReference($consultation->getId());
                $this->loadDroitAcces(Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION'), $consultation);
                break;
            case 'PubliciteConsultation':
                if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                    $this->bloc_PubliciteConsultationConcentrateurV2->bloc_PubliciteConsultationConcentrateurV2->setStyle("display:''");
                    $this->bloc_PubliciteConsultationConcentrateurV2->charger($param);
                } else {
                    $this->bloc_PubliciteConsultation->setMode(1);
                    $this->bloc_PubliciteConsultation->bloc_PubliciteConsultation->setStyle("display:''");
                    $this->bloc_PubliciteConsultation->charger($consultation, $consultation->getDonneComplementaire(), $param);
                    $this->bloc_PubliciteConsultation->scriptJs->text = '<script>chargerChosenDepartementParution();</script>';
                }
                break;
            case 'etapeDumeAcheteur':
                $this->bloc_etapeDumeAcheteur->chargerDumeAcheteur($consultation);
                break;
        }
    }

    public function afficherIdentification()
    {
        if (!Atexo_Config::getParameter('ACTIVE_ELABORATION_V2') || $this->isModeCreationSuite()) {
            $this->bloc_etapeIdentification->bloc_etapeIdentification->setStyle('display:block');
        } else {
            $this->bloc_etapeIdentification->bloc_etapeIdentification->setStyle('display:none');
        }
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->bloc_etapeDonneesComplementaires->bloc_etapeDonneesComplementaires->setStyle('display:none');
        }
        $this->bloc_etapeDetailsLots->bloc_etapeDetailsLots->setStyle('display:none');
        $this->bloc_etapeDumeAcheteur->bloc_etapeDumeAcheteur->setStyle('display:none');
        $this->bloc_etapeCalendrier->bloc_etapeCalendrier->setStyle('display:none');
        $this->bloc_etapeDocumentsJoints->bloc_etapeDocumentsJoints->setStyle('display:none');
        $this->bloc_etapeModalitesReponse->bloc_etapeModalitesReponse->setStyle('display:none');
        $this->bloc_etapeDroitsAcces_bloc_etapeDroitsAcces->setStyle('display:none');
        $this->bloc_etapeDumeAcheteur->bloc_etapeDumeAcheteur->setStyle('display:none');
        if (Atexo_Module::isEnabled('Publicite')) {
            if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $this->bloc_PubliciteConsultationConcentrateurV2->bloc_PubliciteConsultationConcentrateurV2->setStyle('display:none');
            } else {
                $this->bloc_PubliciteConsultation->bloc_PubliciteConsultation->setStyle('display:none');
            }
        }
    }

    /**
     * Permet d'initialiser et gerer l'affichage des etapes.
     */
    public function initialiserEtapes()
    {
        $this->mode = $this->getModeConsultation();
        $this->setViewState('mode', $this->getModeConsultation());
        $this->setViewState('etapeCourante', 'etapeIdentification');
        //Gestion des etapes
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape first current' : 'step first active';

        $this->etapesConsultation->afficherEtapeActive('etapeIdentification', $cssClass, null, null);

        $this->afficherIdentification();
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->errorPart->setVisible(true);
        $this->panelMessageErreur->setMessage($msg);
    }

    /**
     * Permet de gerer l'acces ala page et le fil d'ariane.
     */
    public function gererAccesPageMessageErreurFilAriane()
    {
        if (!($this->getConsultation() instanceof CommonConsultation) && !isset($_GET['continuation']) && isset($_GET['id'])) {
            $this->setConsultation((new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']))));
            if (!(new Atexo_Consultation())->hasHabilitationModifCons($this->getConsultation())) {
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_MESSAGE_VOUS_N_ETES_PAS_HABILITE_MODIFIDIER_CONSULTATION'));
                $this->labelConsultationError->Text = prado::localize('DEFINE_TEXT_CREER');

                return;
            } else {
                $this->errorPart->setVisible(false);
            }
        }

        if ($this->isModeCreationSuite()) {
            $this->labelConsultation->Text = prado::localize('DEFINE_TEXT_CREER_SUITE');
        } elseif ($this->isModeModifCons()) {
            $this->labelConsultation->Text = prado::localize('TEXT_MODIFIER_CONSULTATION_AVANT_VALIDATION');
        } else {
            $this->labelConsultation->Text = prado::localize('DEFINE_CREER_CONSULTATION');
        }

        if (!$this->IsPostBack) {
            if (
                (isset($_GET['xml']) && (Atexo_CurrentUser::hasHabilitation('GererMapaInferieurMontant')
                        || Atexo_CurrentUser::hasHabilitation('GererMapaSuperieurMontant') || Atexo_CurrentUser::hasHabilitation('AdministrerProceduresFormalisees')))
                || ((isset($_GET['id'])) && (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation') || $this->consultation->hasHabilitationModifierApresValidation()))
                || (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation') && isset($_GET['continuation']))
            ) {
                if (isset($_GET['continuation']) && isset($_GET['org']) && isset($_GET['lot'])) {
                    $messageError = '';
                    if ((new Atexo_Consultation_Responses())->ifOrganismeInvite(Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getCurrentOrganism(), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['lot']))) {
                        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), $_GET['org']);
                        if (!$consultation) {
                            $messageError = Prado::localize('TEXT_CONS_INTROUVABLE');
                        }
                    } else {
                        $messageError = Prado::localize('INVITE_CONSULTATION');
                    }

                    if ($messageError) {
                        $this->panelMessageErreur->setVisible(true);
                        $this->errorPart->setVisible(true);
                        $this->panelMessageErreur->setMessage($messageError);
                        $this->mainPart->setVisible(false);
                        $this->labelConsultationError->Text = prado::localize('DEFINE_TEXT_CREER_SUITE');

                        return;
                    }
                } else {
                    $criteriaVo = new Atexo_Consultation_CriteriaVo();
                    $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
                    $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                    $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
                    $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);
                    if (1 == count($arrayConsultation)) {
                        $consultation = array_shift($arrayConsultation);
                        (new Atexo_MpeSf())->consultationsAutorisees($consultation->getId());
                    } else {
                        $this->panelMessageErreur->setVisible(true);
                        $this->errorPart->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('TEXT_CONS_INTROUVABLE'));
                        $this->mainPart->setVisible(false);
                        $this->labelConsultationError->Text = prado::localize('DEFINE_TEXT_CREER_SUITE');

                        return;
                    }
                }
            }
        }
    }

    /**
     * Permet d'afficher de message flottant pour la demande de la validation.
     */
    public function afficherPopUpDemandeValidation()
    {
        //Demande de validation (message flottant)
        if (isset($_GET['validationElaboration']) && 1 == $_GET['validationElaboration'] && isset($_GET['id'])) {
            //Recuperation de la consultation via la fonction search()
            $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                $this->remplissagePopinDemandeValidationAction(null, null, true);
            }
        }
    }

    /**
     * Permet de valider la consultation.
     */
    public function demanderValidation($sender, $param)
    {
        if ($this->isValid) {
            $this->enregistrerConsultation($sender, $param);
        }
    }

    /**
     * Permet de raffraichir le bloc contenant les messages de confirmation et d'erreurs.
     *
     * @param $sender
     * @param $param
     */
    public function raffraichirPanelBlocMessages($sender, $param)
    {
        if ($param) {
            $this->panelBlocMessages->render($param->getNewWriter());
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    /**
     * Permet de masquer les messages.
     */
    public function masquerMessages()
    {
        $this->messageConfirmationEnregistrement->visible = false;
        $this->messageErreur->visible = false;
        $this->scriptJavascript->setText('');
    }

    /**
     * Permet de valider la consultation, en effectuant une validation cotaserveur.
     *
     * @param $sender
     * @param $param
     */
    public function validerConsultation($sender, $param)
    {
        $consultation = $this->getViewState('consultation');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $isValid = $this->isValid;
        $arrayMsgErreur = [];
        if ($consultation instanceof CommonConsultation) {
            //Validation des donnees complementaires
            $this->validerDonneesComplementairesConsultationLots($connexion, $isValid, $arrayMsgErreur);

            //Validation des donnees d'identification de la consultation
            $this->bloc_etapeIdentification->validerDonneesIdentificationConsultation($consultation, $isValid, $arrayMsgErreur);

            //Validation des Documents Joints de la consultation
            $this->bloc_etapeDocumentsJoints->validerDocumentsJoint($isValid, $arrayMsgErreur);

            //Messages validation
            $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur);
        }
    }

    /**
     * Permet de valider les donnees complementaires.
     *
     * @param $sender
     * @param $param
     */
    public function validerDonneesComplementairesConsultationLots($connexion, &$isValid, &$arrayMsgErreur)
    {
        $consultation = $this->getViewState('consultation');
        if ($consultation instanceof CommonConsultation) {
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                $donneesComplementairesCons = CommonTDonneeComplementairePeer::retrieveByPK($consultation->getIdDonneeComplementaire(), $connexion);
                if ('1' == $consultation->getAlloti()) {
                    //Je souhaite enrichir les donnees de ma consultation avec les donnees de l'onglet REDAC a'OUI'
                    if ($donneesComplementairesCons instanceof CommonTDonneeComplementaire) {
                        $listeLots = $consultation->getAllLots();
                        if (is_array($listeLots) && count($listeLots)) {
                            foreach ($listeLots as $lot) {
                                $donneesComplementaires = CommonTDonneeComplementairePeer::retrieveByPK($lot->getIdDonneeComplementaire(), $connexion);
                                $this->validerDonneesComplementaires($donneesComplementaires, $connexion, $isValid, $arrayMsgErreur, $lot->getLot());
                            }
                        }
                    }
                }
                $this->bloc_etapeDonneesComplementaires->validerDonneesComplementaires($isValid, $arrayMsgErreur);
            }
        }
    }

    /**
     * Permet de valider les donnees complementaires de la consultation et des lots.
     *
     * @param $donneesComplementaires: instance de CommonTDonneeComplementaire
     * @param $connexion
     * @param $isValid
     * @param $arrayMsgErreur
     * @param $lot:                    lot de la consultation, egal a0 pour une consultation non allotie
     */
    public function validerDonneesComplementaires($donneesComplementaires, $connexion, &$isValid, &$arrayMsgErreur, $lot)
    {
        if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
            if (Atexo_Module::isEnabled('DonneesRedac') && $this->bloc_etapeDonneesComplementaires->getDonneeOrmeChecked()) {// si les donnee complementaires est obligatoire
                //Duree du marche
                if ($donneesComplementaires->getIdDureeDelaiDescription()) {
                    $valRefDureeMarche = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $donneesComplementaires->getIdDureeDelaiDescription());
                    if ('DUREE_MARCHEE' == $valRefDureeMarche->getLibelle2() && !$donneesComplementaires->getDureeMarche()) {
                        $isValid = false;
                        $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_DUREE_MARCHE');
                    } elseif (
                        'DELAI_EXECUTION' == $valRefDureeMarche->getLibelle2()
                        && (!$donneesComplementaires->getDureeDateDebut() || !$donneesComplementaires->getDureeDateFin())
                    ) {
                        $isValid = false;
                        $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_DELAI_EXECUTION_PRESTATION');
                    } elseif ('DESCRIPTION_LIBRE' == $valRefDureeMarche->getLibelle2() && !$donneesComplementaires->getDureeDescription()) {
                        $isValid = false;
                        $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_LIBRE_DUREE_DELAI');
                    }
                } else {
                    $isValid = false;
                    $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_DUREE_MARCHE_OU_DELAI_EXECUTION');
                }

                if (
                    $donneesComplementaires->getVariantesTechniquesObligatoires()
                    && '' == $donneesComplementaires->getVariantesTechniquesDescription()
                ) {
                    $isValid = false;
                    $arrayMsgErreur[] = sprintf(
                        "%s %s - %s",
                        Prado::localize('TEXT_LOT'),
                        $lot,
                        Prado::localize('DEFINE_VARIANTES_TECHNIQUES_OBLIGATOIRES')
                    );
                }

                //Forme du marchaet forme(s) de prix
                $listeTranche = $donneesComplementaires->getAllTranches();
                //Avec FormePrix
                if ($donneesComplementaires->getFormePrix()) {
                    $formePrix = $donneesComplementaires->getFormePrix();
                    if ($formePrix instanceof CommonTFormePrix) {
                        (new Atexo_FormePrix())->validerFormePrix($formePrix, $formePrix->getAllVariationsPrixForfaitaire(), $formePrix->getAllVariationsPrixUnitaire(), $formePrix->getAllTypesPrix(), $isValid, $arrayMsgErreur, $lot);
                    } else {
                        $isValid = false;
                        $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('TEXT_FORME_DE_PRIX');
                    }
                } //Avec Tranches
                elseif (is_array($listeTranche) && count($listeTranche)) {
                    foreach ($listeTranche as $tranche) {
                        if ('0' == $tranche->getCodeTranche()) {
                            $code = Atexo_Config::getParameter('TRANCHE_FIXE');
                        } else {
                            $code = Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE') . ' ' . $tranche->getCodeTranche();
                        }
                        $trancheMsg = Prado::localize('DEFINE_TEXT_TRANCHE') . ' - ' . $code;
                        //Forme de prix de la tranche
                        $formePrix = $tranche->getFormePrix();
                        if ($formePrix instanceof CommonTFormePrix) {
                            (new Atexo_FormePrix())->validerFormePrix($formePrix, $formePrix->getAllVariationsPrixForfaitaire(), $formePrix->getAllVariationsPrixUnitaire(), $formePrix->getAllTypesPrix(), $isValid, $arrayMsgErreur, $lot, $trancheMsg);
                        } else {
                            $isValid = false;
                            $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . $trancheMsg . ' - ' . Prado::localize('TEXT_FORME_DE_PRIX');
                        }
                        //Identifiant de la tranche
                        if (!$tranche->getIdTranche()) {
                            $isValid = false;
                            $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_TEXT_TRANCHE') . ' ' . $tranche->getIdTranche() . ' - ' . Prado::localize('TEXT_IDENTIFIANT');
                        }
                        //Intitulade la tranche
                        if (!$tranche->getIntituleTranche()) {
                            $isValid = false;
                            $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_TEXT_TRANCHE') . ' ' . $tranche->getIdTranche() . ' - ' . Prado::localize('DEFINE_INTITULE_TRANCHE');
                        }
                    }
                } //Ni forme prix, ni tranche
                else {
                    $isValid = false;
                    $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_MARCHE_AVEC_TRANCHE_FERME_ET_TRANCHE_CONDITIONNELLE') . ' - ' . Prado::localize('TEXT_FORME_DE_PRIX');
                }
                if (!$this->bloc_etapeDonneesComplementaires->getCritereIdentiqueChecked()) {
                    //on met ajour l'id critere attribution selon ce qu'on a dans la consultation
                    $donneesComplementaires->setIdCritereAttribution($this->bloc_etapeDonneesComplementaires->getCurrentIdCritereAttribution());
                    (new Atexo_DonnesComplementaires())->validerCritereAttribution($donneesComplementaires, $isValid, $arrayMsgErreur, $lot);
                }
                //Validation CCAG_REFERENCE
                $ccagRef = $donneesComplementaires->getIdCcagReference();
                if (empty($ccagRef)) {
                    $isValid = false;
                    $arrayMsgErreur[] = Prado::localize('TEXT_LOT') . ' ' . $lot . ' - ' . Prado::localize('DEFINE_CCAG_REFERENCE');
                }
            }
        }
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param $isValid
     * @param $arrayMsgErreur
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur)
    {
        if ($isValid) {
            $param->IsValid = true;
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
        } else {
            $param->IsValid = false;
            $scriptJs = "document.getElementById('divValidationSummary').style.display='';";
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';";
            $scriptJs .= "goToTopPage('divValidationSummary');";
            $this->labelServerSide->Text = "<script>$scriptJs</script>";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg . '</li>';
                $this->validateDonneesComplementaires->ErrorMessage = '';
                $this->divValidationSummary->addServerError($errorMsg, false);
            }

            return;
        }
    }

    /**
     * Permet de recuperer les lots de l'ancienne consultation.
     *
     * @param $consultation    : l'objet consultation
     * @param $consultationId: reference de la consultation
     */
    public function recupererLotsAncienneConsultation($consultation, $consultationId)
    {
        $lots = [];
        if (isset($_GET['lots']) && '' != base64_decode($_GET['lots'])) {
            $numerosLots = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots'])));
            foreach ($numerosLots as $oneLot) {
                $categorieLots = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $oneLot, Atexo_CurrentUser::getCurrentOrganism());
                if ($categorieLots instanceof CommonCategorieLot) {
                    $lots[] = $categorieLots;
                }
            }
        } elseif (isset($_GET['org']) && isset($_GET['lot'])) {
            $categorieLots = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_Util::atexoHtmlEntities($_GET['org']));
            if ($categorieLots instanceof CommonCategorieLot) {
                $lots[] = $categorieLots;
            }
        } else {
            $lots = $consultation->getAllLots();
        }

        return $lots;
    }

    /**
     * Permet de copier l'objet consultation passee en parametres.
     *
     * @param CommonConsultation $consultation : objet a copier
     *
     * @return CommonConsultation $newConsultation : objet copie
     */
    public function copierConsultation($consultation)
    {
        $newConsultation = new CommonConsultation();
        $newConsultation->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $newConsultation->setReferenceUtilisateur($consultation->getReferenceUtilisateur());
        $newConsultation->setReferenceConsultationInit($consultation->getId());
        $newConsultation->setOrganismeConsultationInit($consultation->getOrganisme());
        $newConsultation->setIdTypeAvis($consultation->getIdTypeAvis());
        $newConsultation->setTypeMarche($consultation->getTypeMarche());
        $newConsultation->setEntiteAdjudicatrice($consultation->getEntiteAdjudicatrice());
        $newConsultation->setIdTypeProcedureOrg('0');
        $newConsultation->setCodeOperation($consultation->getCodeOperation());
        $newConsultation->setCategorie($consultation->getCategorie());
        $newConsultation->setObjet($consultation->getObjet());
        $newConsultation->setIntitule($consultation->getIntitule());
        $newConsultation->setChampSuppInvisible($consultation->getChampSuppInvisible());
        $newConsultation->setLieuExecution($consultation->getLieuExecution());
        $newConsultation->setCodesNuts($consultation->getCodesNuts());
        $newConsultation->setCodeCpv1($consultation->getCodeCpv1());
        $newConsultation->setCodeCpv2($consultation->getCodeCpv2());
        $newConsultation->setAlloti($consultation->getAlloti());
        $newConsultation->setClauseSociale($consultation->getClauseSociale());
        $newConsultation->setClauseEnvironnementale($consultation->getClauseEnvironnementale());
        $newConsultation->setClauseSocialeInsertion($consultation->getClauseSocialeInsertion());
        $newConsultation->setClauseSocialeConditionExecution($consultation->getClauseSocialeConditionExecution());
        $newConsultation->setClauseSocialeAteliersProteges($consultation->getClauseSocialeAteliersProteges());
        $newConsultation->setClauseSocialeSiae($consultation->getClauseSocialeSiae());
        $newConsultation->setClauseSocialeEss($consultation->getClauseSocialeEss());
        $newConsultation->setClauseEnvSpecsTechniques($consultation->getClauseEnvSpecsTechniques());
        $newConsultation->setClauseEnvCondExecution($consultation->getClauseEnvCondExecution());
        $newConsultation->setClauseEnvCriteresSelect($consultation->getClauseEnvCriteresSelect());
        $newConsultation->setDonneeComplementaireObligatoire($consultation->getDonneeComplementaireObligatoire());
        $newConsultation->setIdDonneeComplementaire($consultation->getIdDonneeComplementaire());
        $newConsultation->setIdAffaire($consultation->getIdAffaire());
        $newConsultation->setAdresseRetraisDossiers($consultation->getAdresseRetraisDossiers());
        $newConsultation->setAdresseDepotOffres($consultation->getAdresseDepotOffres());
        $newConsultation->setLieuOuverturePlis($consultation->getLieuOuverturePlis());
        $newConsultation->setNumeroAc($consultation->getNumeroAc());
        $newConsultation->setIdContrat($consultation->getIdContrat());
        $newConsultation->setCodeExterne($consultation->getCodeExterne());

        return $newConsultation;
    }

    /**
     * Verifier unicitaconsultation.
     *
     * @param $consultation: objet consultation
     */
    public function verifierUniciteConsultation($consultation)
    {
        $consultationExisteDeja = false;
        if (Atexo_Module::isEnabled('UniciteReferenceConsultation')) {
            $affaireExclu = null;
            if ($consultation instanceof CommonConsultation) {
                if ($consultation->getIdAffaire()) {
                    $affaireExclu = $consultation->getIdAffaire();
                }
            }

            if (Atexo_Module::isEnabled('OrganisationCentralisee', $this->organisme)) {
                if ((new Atexo_Consultation())->referenceUtilisateurExiste($this->organisme, $consultation->getReferenceUtilisateur(), null, $affaireExclu)) {
                    $consultationExisteDeja = true;
                }
            } else {
                if ((new Atexo_Consultation())->referenceUtilisateurExiste($this->organisme, $consultation->getReferenceUtilisateur(), Atexo_CurrentUser::getIdServiceAgentConnected(), $affaireExclu)) {
                    $consultationExisteDeja = true;
                }
            }

            return $consultationExisteDeja;
        }
    }

    /**
     * Permet d'envoyer une alerte al'agent pour la validation.
     *
     * @param $consultation: objet consultation
     */
    public function envoyerAlerteAgentValidation($consultation)
    {
        $typeValidation = $consultation->getIdRegleValidation();
        if (
            $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')
            || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2')
            || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')
        ) {
            $service = $consultation->getServiceValidation();
            (new Atexo_Message())->sendMailToAgentsAlertConsAttenteValidation($consultation, $service, $typeValidation);
        } else {
            $service = $consultation->getServiceValidationIntermediaire();
            (new Atexo_Message())->sendMailToAgentsAlertConsAttenteValidation($consultation, $service, $typeValidation, false);
        }
    }

    /**
     * Permet d'afficher le message d'erreur.
     *
     * @param $msg: le message aafficher
     */
    public function afficherMessageErreur($msg)
    {
        $this->messageErreur->setVisible(true);
        $this->messageErreur->setMessage($msg);
    }

    /**
     * Verifie qu'on est en mode creation suite pour les cas accord cadre et sad.
     */
    public function modeCreationSuiteAcSad()
    {
        if (isset($_GET['continuation']) && isset($_GET['org']) && isset($_GET['lot'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de recuperer l'organisme initial pour le cas de creation suite accord cadre - sad.
     */
    public function recupererOrganismeInit()
    {
        $org_init = '';
        if (isset($_GET['org'])) {
            if (!(new Atexo_Consultation_Responses())->ifOrganismeInvite(Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getCurrentOrganism(), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['lot']))) {
                $this->response->redirect('index.php?page=Agent.AgentHome');
            }
            $org_init = Atexo_Util::atexoHtmlEntities($_GET['org']);
        } else {
            $org_init = Atexo_CurrentUser::getOrganismAcronym();
        }

        return $org_init;
    }

    /**
     * Permet de preciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     *                       Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), isset($_GET['id']));
    }

    /**
     * Permet de mettre ajour la variante calculee dans la donnee complementaire de la consultation
     * Conditions: si consultation allotie et au moins un de ses lots a la variante.
     *
     * @param $consultation: objet consultation
     * @param $connexion:    objet connexion
     */
    public function updateVarianteCalculeeDonneesComplementaires($consultation, $connexion)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
            $donnecomplementaire = $consultation->getDonneComplementaire();
            if ($consultation->getAlloti() && $donnecomplementaire instanceof CommonTDonneeComplementaire) {
                if ($consultation->lotsHasVariante()) {
                    $donnecomplementaire->setVarianteCalcule('1');
                } else {
                    $donnecomplementaire->setVarianteCalcule('0');
                }
                $donnecomplementaire->save($connexion);
            }
        }
    }

    /**
     * Permet de calculer la variante.
     *
     * @param CommonConsultation $consultation: objet consultation
     *
     * @return variante: la variante calculee
     */
    public function calculerVarianteConsultation($consultation)
    {
        $varianteCalcule = '0';
        if ($consultation instanceof CommonConsultation) {
            if (0 == strcmp($consultation->getVariantes(), '1')) {
                $varianteCalcule = '1';
            }
            $listeLots = $consultation->getAllLots();
            if (is_array($listeLots) && count($listeLots)) {
                foreach ($listeLots as $lot) {
                    if ($lot instanceof CommonCategorieLot) {
                        $donneeComplementaireLot = $lot->getDonneComplementaire();
                        if (
                            $donneeComplementaireLot instanceof CommonTDonneeComplementaire
                            && 0 == strcmp($donneeComplementaireLot->getVariantes(), '1')
                        ) {
                            $varianteCalcule = '1';
                        }
                    }
                }
            }
        }

        return $varianteCalcule;
    }

    /**
     * Permet de recharger les donnees de la consultation selon l'allotissement.
     *
     * @param TActiveRadioButton $sender,TCallbackEventParameter $param
     *
     * @return void
     *
     * @author    LEZ <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.6.0
     *
     * @copyright Atexo 2014
     */
    public function refreshDonneesComplementaireConsultation($sender, $param)
    {
        $consultation = $this->getConsultation();

        $alloti = $param->CallbackParameter;
        $this->bloc_etapeIdentification->refreshDonneesComplementaireConsultation($param, $consultation, $alloti);
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->bloc_etapeDonneesComplementaires->refreshDonneesComplementaireConsultation($param, $consultation, $alloti);
        }
    }

    public function remplissagePopinDemandeValidation($sender, $param)
    {
        $this->remplissagePopinDemandeValidationAction($sender, $param);
    }

    public function remplissagePopinDemandeValidationAction($sender, $param, $fromConsultation = false)
    {
        if (Atexo_Module::isEnabled('Publicite')) {
            if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $this->publiciteRecap->setVisible(false);
            } else {
                $this->supportsSelectionnes->setVisible(false);
            }
            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $isPubAutorise = ($fromConsultation) ? $consultation->getDonneePubliciteObligatoire() : $this->bloc_etapeIdentification->isPubAutorise();
                if ($isPubAutorise) {
                    if (!Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                        $supports = $this->bloc_PubliciteConsultation->supportSelected($consultation, $this->getViewState('etapeCourante'));
                        if (is_array($supports) && count($supports)) {
                            $this->listeSupport->setDataSource($supports);
                            $this->listeSupport->DataBind();
                            $this->supportsSelectionnes->setVisible(true);
                        } else {
                            $this->listeSupport->setDataSource([]);
                            $this->listeSupport->DataBind();
                            $this->supportsSelectionnes->setVisible(false);
                        }
                    } else {
                        $this->publiciteRecap->setDisplay('Dynamic');
                        $this->publiciteRecap->charger($consultation, $param);
                    }
                } else {
                    $this->publiciteRecap->setDisplay('None');
                    $this->scriptJsDisabled->setText("<script>J(document).ready(function(){ J('#ctl0_CONTENU_PAGE_supportsSelectionnes').hide(); });</script>");
                }
            }
        }
        if (Atexo_Module::isEnabled('Publicite') && Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
            $script = "openModal('demander-validation','popup-small2',document.getElementById('" . $this->buttonValidate->getClientId() . "'));";
            if ($fromConsultation) {
                $script = 'J(document).ready(function(){' . $script . '});';
            }
            $this->scriptJavascript->setText("<script>$script</script>");
        } else {
            $this->scriptJavascript->setText("<script>openModal('demander-validation','popup-small2',document.getElementById('" . $this->buttonValidate->getClientId() . "'));</script>");
        }
    }

    /**
     * @param $idAgent
     * @param $reference
     * @param $connexion
     */
    protected function addConsultationFavoris($idAgent, $reference, $connexion)
    {
        try {
            $agent = new CommonAgentQuery();
            $agent = $agent->findPk(Atexo_CurrentUser::getId());

            $consultationFavorisQuery = new CommonConsultationFavorisQuery();
            $consultationFavorisExist = $consultationFavorisQuery
                ->findOneByArray(['idConsultation' => $reference, 'idAgent' => Atexo_CurrentUser::getId()], $connexion);


            if (
                !$consultationFavorisExist instanceof CommonConsultationFavoris
                && $agent->getAlerteMesConsultations() === '1'
            ) {
                $this->saveConsultationFavorisParCreateur($idAgent, $reference, $connexion);
            }

            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $service = $agent->getServiceId();
            $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
            $criteriaVo->setCurAgentServiceId($service);
            $criteriaVo->setOrgAcronym($organisme);
            if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
                $criteriaVo->setCentralizedOrganization(true);
            } else {
                $criteriaVo->setCentralizedOrganization(false);
            }
            if (Atexo_Module::isEnabled('GestionMandataire')) {
                $myService = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                if ($myService && '0' == $myService->getAffichageService() && $service == Atexo_CurrentUser::getIdServiceAgentConnected()) {
                    $criteriaVo->setCentralizedOrganization(false);
                }
            }

            $this->saveConsultationFavorisPermanentGuestsMonEntitee($criteriaVo, $reference, $connexion);
            $this->saveConsultationFavorisPermanentGuestsEntiteeDependante($criteriaVo, $reference, $connexion);
            $this->saveConsultationFavorisPermanentGuestsEntiteeTransverse($criteriaVo, $reference, $connexion);
            $this->saveConsultationFavorisInvitePonctuel($reference, Atexo_CurrentUser::getOrganismAcronym(), $connexion);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    protected function repeatAddConsultationFavoris(int $idAgent, int $reference, $connexion)
    {
        /** @var AtexoFavoris $atexoFavoris */
        $atexoFavoris = Atexo_Util::getSfService(AtexoFavoris::class);

        $atexoFavoris->deleteFavorisByConsultation($reference);

        $this->addConsultationFavoris(
            $idAgent,
            $reference,
            $connexion
        );
    }

    /**
     * @param  $idAgent
     * @param  $reference
     * @param  $connexion
     *
     * @throws PropelException
     */
    protected function saveConsultationFavorisParCreateur($idAgent, $reference, $connexion)
    {
        $consultationFavoris = new CommonConsultationFavoris();
        $consultationFavoris->setIdAgent($idAgent);
        $consultationFavoris->setIdConsultation($reference);
        $consultationFavoris->save($connexion);
    }

    /**
     * @param $criteriaVo
     * @param $reference
     * @param $connexion
     */
    protected function saveConsultationFavorisPermanentGuestsMonEntitee($criteriaVo, $reference, $connexion)
    {
        try {
            $permanentsGuestsMonEntitee = Atexo_Consultation_Guests::retrievePermanentGuestsMonEntitee($criteriaVo);
            foreach ($permanentsGuestsMonEntitee as $guest) {
                $consultationFavorisQuery = new CommonConsultationFavorisQuery();
                $consultationFavorisExist = $consultationFavorisQuery
                    ->findOneByArray(['idAgent' => $guest->getId(), 'idConsultation' => $reference], $connexion);
                if (!$consultationFavorisExist instanceof CommonConsultationFavoris) {
                    $agent = new CommonAgentQuery();
                    $agent = $agent->findPk($guest->getId());
                    if ('1' === $agent->getAlerteConsultationsMonEntite()) {
                        $this->saveConsultationFavorisParCreateur($guest->getId(), $reference, $connexion);
                    }
                }
            }
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * @param $criteriaVo
     * @param $reference
     * @param $connexion
     */
    protected function saveConsultationFavorisPermanentGuestsEntiteeDependante($criteriaVo, $reference, $connexion)
    {
        try {
            $permanentsGuestsEntiteeDependante = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeDependante($criteriaVo);
            foreach ($permanentsGuestsEntiteeDependante as $guest) {
                $consultationFavorisQuery = new CommonConsultationFavorisQuery();
                $consultationFavorisExist = $consultationFavorisQuery
                    ->findOneByArray(['idAgent' => $guest->getId(), 'idConsultation' => $reference], $connexion);
                if (!$consultationFavorisExist instanceof CommonConsultationFavoris) {
                    $agent = new CommonAgentQuery();
                    $agent = $agent->findPk($guest->getId());
                    if ('1' === $agent->getAlerteConsultationsDesEntitesDependantes()) {
                        $this->saveConsultationFavorisParCreateur($guest->getId(), $reference, $connexion);
                    }
                }
            }
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * @param $criteriaVo
     * @param $reference
     * @param $connexion
     */
    protected function saveConsultationFavorisPermanentGuestsEntiteeTransverse($criteriaVo, $reference, $connexion)
    {
        try {
            $permanentsGuestsEntiteeTransverse = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeTransverse($criteriaVo);
            foreach ($permanentsGuestsEntiteeTransverse as $guest) {
                $consultationFavorisQuery = new CommonConsultationFavorisQuery();
                $consultationFavorisExist = $consultationFavorisQuery
                    ->findOneByArray(['idAgent' => $guest->getId(), 'idConsultation' => $reference], $connexion);
                if (!$consultationFavorisExist instanceof CommonConsultationFavoris) {
                    $agent = new CommonAgentQuery();
                    $agent = $agent->findPk($guest->getId());
                    if ('1' === $agent->getAlerteConsultationsMesEntitesTransverses()) {
                        $this->saveConsultationFavorisParCreateur($guest->getId(), $reference, $connexion);
                    }
                }
            }
        } catch (Exception $exception) {
            return $exception;
        }
    }

    protected function saveConsultationFavorisInvitePonctuel(
        int $idConsultation,
        string $organismeAcronyme,
        $connexion
    ): void {
        try {
            /** @var ConsultationService $consultationService */
            $consultationService = Atexo_Util::getSfService(ConsultationService::class);
            $agentsToAdd = [];
            $agentsToAdd = $consultationService->addFavorisInvitePonctuel(
                $idConsultation,
                $organismeAcronyme,
                $agentsToAdd
            );

            foreach ($agentsToAdd as $agent) {
                $consultationFavorisQuery = new CommonConsultationFavorisQuery();
                $consultationFavorisExist = $consultationFavorisQuery
                    ->findOneByArray(['idAgent' => $agent->getId(), 'idConsultation' => $idConsultation], $connexion);
                if (!$consultationFavorisExist instanceof CommonConsultationFavoris) {
                    if ('1' === $agent->getAlerteConsultationsInvitePonctuel()) {
                        $this->saveConsultationFavorisParCreateur($agent->getId(), $idConsultation, $connexion);
                    }
                }
            }
        } catch (Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $logger->error(
                sprintf(
                    "Problème lors de l'ajout en favoris des invités ponctuels de la consultation %s : %s.",
                    $idConsultation,
                    $exception->__toString()
                )
            );
        }
    }

    protected function saveAchatResponsable(int $idConsultation, string $values): void
    {
        /** @var ClauseUsePrado $clause */
        $clause = Atexo_Util::getSfService(ClauseUsePrado::class);
        $data = [
            'values' => json_decode($values, true),
            'id_consultation' => $idConsultation
        ];

        $clause->getContent('sendConsultation', $data);
    }

    protected function saveTypageJO2024(int $consultationId, string $value): void
    {
        /** @var ConsultationTagsUsePrado $tagPradoService */
        $tagPradoService = Atexo_Util::getSfService(ConsultationTagsUsePrado::class);
        /** @var ConsultationTags $tagService */
        $tagService = Atexo_Util::getSfService(ConsultationTags::class);

        $consultationTagId = $tagService->getTagByConsultationId($consultationId, 'typageJo');

        if ($value == 'true' && empty($consultationTagId)) {
            $data = [
                'tagCode'           => 'typageJo',
                'consultationId'    => $consultationId
            ];

            $tagPradoService->getContent('createTag', $data);
        } elseif ($value == 'false' && !empty($consultationTagId)) {
            $data = [
                'consultationTagId' => $consultationTagId,
                'consultationId'    => $consultationId
            ];

            $tagPradoService->getContent('deleteTag', $data);
        }
    }

    public function addConsultationFavorisByWebService(int $idAgent, int $consultationId, $connexion): void
    {
        try {
            $agent = new CommonAgentQuery();
            $agent = $agent->findPk($idAgent);

            $consultationFavorisQuery = new CommonConsultationFavorisQuery();
            $consultationFavorisExist = $consultationFavorisQuery->findOneByArray(
                ['idConsultation' => $consultationId, 'idAgent' => $idAgent],
                $connexion
            );

            if (
                !$consultationFavorisExist instanceof CommonConsultationFavoris
                && $agent->getAlerteMesConsultations() === '1'
            ) {
                $this->saveConsultationFavorisParCreateur($idAgent, $consultationId, $connexion);
            }

            $organismeAcronyme = $agent->getOrganisme();
            $service = $agent->getServiceId();

            $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
            $criteriaVo->setCurAgentServiceId($service);
            $criteriaVo->setOrgAcronym($organismeAcronyme);

            if (Atexo_Module::isEnabled('OrganisationCentralisee', $organismeAcronyme)) {
                $criteriaVo->setCentralizedOrganization(true);
            } else {
                $criteriaVo->setCentralizedOrganization(false);
            }

            if (Atexo_Module::isEnabled('GestionMandataire')) {
                $myService = Atexo_EntityPurchase::retrieveEntityById($service, $organismeAcronyme);
                if ($myService && '0' == $myService->getAffichageService()) {
                    $criteriaVo->setCentralizedOrganization(false);
                }
            }

            $this->saveConsultationFavorisPermanentGuestsMonEntitee($criteriaVo, $consultationId, $connexion);
            $this->saveConsultationFavorisPermanentGuestsEntiteeDependante($criteriaVo, $consultationId, $connexion);
            $this->saveConsultationFavorisPermanentGuestsEntiteeTransverse($criteriaVo, $consultationId, $connexion);
            $this->saveConsultationFavorisInvitePonctuel($consultationId, Atexo_CurrentUser::getOrganismAcronym(), $connexion);
        } catch (Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $logger->error(
                sprintf(
                    "Problème lors de l'ajout en favoris de la consultation %s : %s.",
                    $consultationId,
                    $exception->__toString()
                )
            );
        }
    }
}
