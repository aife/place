<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_OuverturePlis;
use Exception;

class RetreiveBIn extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $invite = (new Atexo_Consultation_OuverturePlis())->AgentInviteConsultation(Atexo_Util::atexoHtmlEntities($_POST['idBlob']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
        if (isset($_POST['blocDechiffre'])) {
            try {
                (new Atexo_Consultation_OuverturePlis())->receptionBlocDechiffre(Atexo_Util::atexoHtmlEntities($_POST['idBlob']), Atexo_Util::atexoHtmlEntities($_POST['blocDechiffre']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                echo 'OK';
            } catch (Exception $e) {
                echo 'ERR';
                echo "\n".$e->getMessage()."\n".$e->getTraceAsString();
            }
        } elseif (isset($_POST['endDecrypt'])) {
            if (1 == $_POST['endDecrypt']) { // ouverture en ligne
                try {
                    $invite = (new Atexo_Consultation_OuverturePlis())->AgentInviteConsultation(null, Atexo_Util::atexoHtmlEntities($_POST['organisme']), Atexo_Util::atexoHtmlEntities($_POST['idEnveloppe']));
                    if ($invite && (Atexo_CurrentUser::hasHabilitation('OuvrirOffreEnLigne')
                        || Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureEnLigne')
                        || Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueEnLigne')
                          || Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatEnLigne'))) {
                        (new Atexo_Consultation_OuverturePlis())->blocsDechiffres(Atexo_Util::atexoHtmlEntities($_POST['idEnveloppe']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                        echo 'OK';
                    }
                } catch (Exception $e) {
                    echo 'ERR';
                    echo "\n".$e->getMessage()."\n".$e->getTraceAsString();
                }
            } elseif (2 == $_POST['endDecrypt']) { // Ouverture mixte
                try {
                    $invite = (new Atexo_Consultation_OuverturePlis())->AgentInviteConsultation(null, Atexo_Util::atexoHtmlEntities($_POST['organisme']), Atexo_Util::atexoHtmlEntities($_POST['idEnveloppe']));
                    if ($invite && (Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureADistance')
                        || Atexo_CurrentUser::hasHabilitation('OuvrirOffreADistance')
                        || Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatADistance')
                          || Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueADistance'))) {
                        Atexo_Consultation_OuverturePlis::enveloppeOuverteAdistance(Atexo_Util::atexoHtmlEntities($_POST['idEnveloppe']), Atexo_Util::atexoHtmlEntities($_POST['organisme']));
                        echo 'OK';
                    }
                } catch (Exception $e) {
                    echo 'ERR';
                    echo $e->getMessage().Atexo_Util::atexoHtmlEntities($_POST['organisme'])."\n".$e->getTraceAsString();
                }
            }
        }
        exit;
    }
}
