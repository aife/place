<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class downlaodFichierJointDecision extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $idEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['env']);
            $lot = Atexo_Util::atexoHtmlEntities($_GET['lot']);

            $downloadFile = false;

            if (isset($_GET['org'])) {
                $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
                if ((new Atexo_Consultation_Responses())->ifOrganismeInvite($organisme, Atexo_CurrentUser::getCurrentOrganism(), $consultationId, $lot)) {
                    $downloadFile = true;
                }
            } else {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($organisme);
                $criteria->setIdReference($consultationId);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);
                if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                    $downloadFile = true;
                }
            }

            if ($downloadFile) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $c = new Criteria();
                $c->add(CommonDecisionEnveloppePeer::ID_DECISION_ENVELOPPE, $idEnveloppe);
                $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme);
                $decisionEnveloppe = CommonDecisionEnveloppePeer::doSelectOne($c, $connexion);

                if ($decisionEnveloppe instanceof CommonDecisionEnveloppe) {
                    $idFichier = $decisionEnveloppe->getFichierJoint();
                    $nomFichier = $decisionEnveloppe->getNomFichierJoint();
                    $this->downloadFiles($idFichier, $nomFichier, $organisme);
                }
            }
        }
    }
}
