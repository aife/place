<?php

namespace Application\Pages\Agent;

use App\Service\StatistiquesMetier as StatistiquesMetierSF;
use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use Prado\Prado;

/**
 * Classe StatistiquesMetier.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesMetier extends MpeTPage
{
    public bool $visible = true;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['allOrgs'])) {
            $this->visible = false;
        }

        if (!$this->IsPostBack) {
            // remplissage de la liste des entités
            $this->remplirListEntiteAssociee();
            $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
        }
        $this->onSearchClick();
    }

    public function remplirListEntiteAssociee()
    {
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        $this->entiteeAssociee->Datasource = $entities;
        $this->entiteeAssociee->dataBind();
        $this->entiteeAssociee->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    public function onSearchClick()
    {
        $resultsServices = [];
        $resultsTravaux = [];
        $resultsFournitures = [];
        $resultsRetraitsServices = [];
        $resultsRetraitsTravaux = [];
        $resultsRetraitsFournitures = [];
        $resultsRetraitsServices = [];
        $resultsOffresTravaux = [];
        $resultsOffresFournitures = [];
        $resultsOffresServices = [];
        $criteria = new Atexo_Statistiques_CriteriaVo();
        $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));

        if (!isset($_GET['allOrgs'])) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
            /*
             * Critere de la recherche
             */

            $service = 'IN';

            if ($this->cumulValeurs->Checked) {
                $criteria->setValeurCumule(true);
                if (0 != $this->entiteeAssociee->SelectedValue) {
                    $service .= Atexo_EntityPurchase::retrieveAllChildrenServices($this->entiteeAssociee->SelectedValue, $org);
                } else {
                    $i = 1;
                    $service .= '(';
                    $entities = Atexo_EntityPurchase::getEntityPurchase($org, true);
                    $nbr = count($entities);

                    foreach ($entities as $key => $value) {
                        if ($i != $nbr) {
                            $service .= $key.',';
                        } else {
                            $service .= $key;
                        }

                        ++$i;
                    }

                    $service .= ')';
                }
            } else {
                $criteria->setValeurCumule(false);
                $service .= '('.$this->entiteeAssociee->SelectedValue.')';
            }
            $criteria->setSelectedEntity($this->entiteeAssociee->SelectedValue);
            $criteria->setOrganisme($org);
            $criteria->setIdService($service);
            if ('' != $this->critereFiltre_1_dateStart->Text) {
                $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
            }
            if ('' != $this->critereFiltre_1_dateEnd->Text) {
                $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
            }

            $this->setViewState('criteria', $criteria);

            /*
             * Resultat de la recherche pour procédure mise en ligne
             */

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultsTravaux[] = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultsFournitures[] = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultsServices[] = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, false);

            /*
                 * Resultat de la recherche pour les retraits
                 */
            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultsRetraitsTravaux[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultsRetraitsFournitures[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultsRetraitsServices[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, false, false, false);

            /*
             * Resultat de la recherche pour les retraits
             */
            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultsOffresTravaux[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultsOffresFournitures[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false, false);

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultsOffresServices[] = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, false, false, false);
            self::remplirChamps(
                $resultsTravaux,
                $resultsFournitures,
                $resultsServices,
                $resultsRetraitsTravaux,
                $resultsRetraitsFournitures,
                $resultsRetraitsServices,
                $resultsOffresTravaux,
                $resultsOffresFournitures,
                $resultsOffresServices
            );
        } else {
            /*
             * Critere de la recherche
            */
            if ('' != $this->critereFiltre_1_dateStart->Text) {
                $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
            }
            if ('' != $this->critereFiltre_1_dateEnd->Text) {
                $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
            }

            $this->setViewState('criteria', $criteria);
            self::remplirTab(self::getDataFromSf());
        }
    }

    public function remplirChamps(
        $resultsTravaux,
        $resultsFournitures,
        $resultsServices,
        $resultsRetraitsTravaux,
        $resultsRetraitsFournitures,
        $resultsRetraitsServices,
        $resultsOffresTravaux,
        $resultsOffresFournitures,
        $resultsOffresServices
    ) {
        /*
         * variables procédures mise en ligne
         */

        /*---Variable pour la categorie Travaux---*/
        $nbrMapinfTravaux = 0;
        $nbrMapsupTravaux = 0;
        $nbrAoTravaux = 0;
        $nbrMnTravaux = 0;
        $nbrPnTravaux = 0;
        $nbrDcTravaux = 0;
        $nbrAutreTravaux = 0;

        /*---Variable pour la categorie Fournitures---*/
        $nbrMapinfFournitures = 0;
        $nbrMapsupFournitures = 0;
        $nbrAoFournitures = 0;
        $nbrMnFournitures = 0;
        $nbrPnFournitures = 0;
        $nbrDcFournitures = 0;
        $nbrAutreFournitures = 0;

        /*---Variable pour la categorie Services---*/
        $nbrMapinfServices = 0;
        $nbrMapsupServices = 0;
        $nbrAoServices = 0;
        $nbrMnServices = 0;
        $nbrPnServices = 0;
        $nbrDcServices = 0;
        $nbrAutreServices = 0;

        /*
         * variables retraits
         */

        /*---Variable pour la categorie Travaux---*/
        $nbrMapinfTravauxRetraits = 0;
        $nbrMapsupTravauxRetraits = 0;
        $nbrAoTravauxRetraits = 0;
        $nbrMnTravauxRetraits = 0;
        $nbrPnTravauxRetraits = 0;
        $nbrDcTravauxRetraits = 0;
        $nbrAutreTravauxRetraits = 0;

        /*---Variable pour la categorie Fournitures---*/
        $nbrMapinfFournituresRetraits = 0;
        $nbrMapsupFournituresRetraits = 0;
        $nbrAoFournituresRetraits = 0;
        $nbrMnFournituresRetraits = 0;
        $nbrPnFournituresRetraits = 0;
        $nbrDcFournituresRetraits = 0;
        $nbrAutreFournituresRetraits = 0;

        /*---Variable pour la categorie Services---*/
        $nbrMapinfServicesRetraits = 0;
        $nbrMapsupServicesRetraits = 0;
        $nbrAoServicesRetraits = 0;
        $nbrMnServicesRetraits = 0;
        $nbrPnServicesRetraits = 0;
        $nbrDcServicesRetraits = 0;
        $nbrAutreServicesRetraits = 0;

        /*
         * variables offres
         */

        /*---Variable pour la categorie Travaux---*/
        $nbrMapinfTravauxOffres = 0;
        $nbrMapsupTravauxOffres = 0;
        $nbrAoTravauxOffres = 0;
        $nbrMnTravauxOffres = 0;
        $nbrPnTravauxOffres = 0;
        $nbrDcTravauxOffres = 0;
        $nbrAutreTravauxOffres = 0;

        /*---Variable pour la categorie Fournitures---*/
        $nbrMapinfFournituresOffres = 0;
        $nbrMapsupFournituresOffres = 0;
        $nbrAoFournituresOffres = 0;
        $nbrMnFournituresOffres = 0;
        $nbrPnFournituresOffres = 0;
        $nbrDcFournituresOffres = 0;
        $nbrAutreFournituresOffres = 0;

        /*---Variable pour la categorie Services---*/
        $nbrMapinfServicesOffres = 0;
        $nbrMapsupServicesOffres = 0;
        $nbrAoServicesOffres = 0;
        $nbrMnServicesOffres = 0;
        $nbrPnServicesOffres = 0;
        $nbrDcServicesOffres = 0;
        $nbrAutreServicesOffres = 0;

        /*
        * calcule et remplissage  des champs pour les procédures mises en ligne.
        */
        if (is_array($resultsTravaux)) {
            foreach ($resultsTravaux as $result) {
                $nbrMapinfTravaux += $result['mapaInf'];
                $nbrMapsupTravaux += $result['mapaSupp'];
                $nbrAoTravaux += $result['ao'];
                $nbrMnTravaux += $result['mn'];
                $nbrPnTravaux += $result['pn'];
                $nbrDcTravaux += $result['dc'];
                $nbrAutreTravaux += $result['autre'];
            }
        }

        if (is_array($resultsFournitures)) {
            foreach ($resultsFournitures as $result) {
                $nbrMapinfFournitures += $result['mapaInf'];
                $nbrMapsupFournitures += $result['mapaSupp'];
                $nbrAoFournitures += $result['ao'];
                $nbrMnFournitures += $result['mn'];
                $nbrPnFournitures += $result['pn'];
                $nbrDcFournitures += $result['dc'];
                $nbrAutreFournitures += $result['autre'];
            }
        }

        if (is_array($resultsServices)) {
            foreach ($resultsServices as $result) {
                $nbrMapinfServices += $result['mapaInf'];
                $nbrMapsupServices += $result['mapaSupp'];
                $nbrAoServices += $result['ao'];
                $nbrMnServices += $result['mn'];
                $nbrPnServices += $result['pn'];
                $nbrDcServices += $result['dc'];
                $nbrAutreServices += $result['autre'];
            }
        }

        $totalProcFormaliseTravaux = $nbrAoTravaux + $nbrMnTravaux + $nbrPnTravaux + $nbrDcTravaux + $nbrAutreTravaux;
        $totalProcAdapteTravaux = $nbrMapinfTravaux + $nbrMapsupTravaux;
        $totalProcFormaliseFournitures = $nbrAoFournitures + $nbrMnFournitures + $nbrPnFournitures + $nbrDcFournitures + $nbrAutreFournitures;
        $totalProcAdapteFournitures = $nbrMapinfFournitures + $nbrMapsupFournitures;
        $totalProcFormaliseServices = $nbrAoServices + $nbrMnServices + $nbrPnServices + $nbrDcServices + $nbrAutreServices;
        $totalProcAdapteServices = $nbrMapinfServices + $nbrMapsupServices;

        $this->proceduretravauxinf90->Text = number_format($nbrMapinfTravaux, '0', ',', ' ');
        $this->proceduretravauxsup90->Text = number_format($nbrMapsupTravaux, '0', ',', ' ');
        $this->proceduretravauxao->Text = number_format($nbrAoTravaux, '0', ',', ' ');
        $this->proceduretravauxmn->Text = number_format($nbrMnTravaux, '0', ',', ' ');
        $this->proceduretravauxpn->Text = number_format($nbrPnTravaux, '0', ',', ' ');
        $this->proceduretravauxdc->Text = number_format($nbrDcTravaux, '0', ',', ' ');
        $this->proceduretravauxautre->Text = number_format($nbrAutreTravaux, '0', ',', ' ');
        $this->proceduretravauxformalisetotal->Text = number_format($totalProcFormaliseTravaux, '0', ',', ' ');
        $this->proceduretravauxadaptetotal->Text = number_format($totalProcAdapteTravaux, '0', ',', ' ');
        $this->grandTotalConsTravaux->Text = number_format(($totalProcFormaliseTravaux + $totalProcAdapteTravaux), '0', ',', ' ');

        $this->procedurefournitureinf90->Text = number_format($nbrMapinfFournitures, '0', ',', ' ');
        $this->procedurefournituresup90->Text = number_format($nbrMapsupFournitures, '0', ',', ' ');
        $this->procedurefournitureao->Text = number_format($nbrAoFournitures, '0', ',', ' ');
        $this->procedurefournituremn->Text = number_format($nbrMnFournitures, '0', ',', ' ');
        $this->procedurefourniturepn->Text = number_format($nbrPnFournitures, '0', ',', ' ');
        $this->procedurefournituredc->Text = number_format($nbrDcFournitures, '0', ',', ' ');
        $this->procedurefournitureautre->Text = number_format($nbrAutreFournitures, '0', ',', ' ');
        $this->procedurefournitureformalisetotal->Text = number_format($totalProcFormaliseFournitures, '0', ',', ' ');
        $this->procedurefournitureadaptetotal->Text = number_format($totalProcAdapteFournitures, '0', ',', ' ');
        $this->grandTotalConsFournitures->Text = number_format(($totalProcFormaliseFournitures + $totalProcAdapteFournitures), '0', ',', ' ');

        $this->procedureservicesinf90->Text = number_format($nbrMapinfServices, '0', ',', ' ');
        $this->procedureservicessup90->Text = number_format($nbrMapsupServices, '0', ',', ' ');
        $this->procedureservicesao->Text = number_format($nbrAoServices, '0', ',', ' ');
        $this->procedureservicesmn->Text = number_format($nbrMnServices, '0', ',', ' ');
        $this->procedureservicespn->Text = number_format($nbrPnServices, '0', ',', ' ');
        $this->procedureservicesdc->Text = number_format($nbrDcServices, '0', ',', ' ');
        $this->procedureservicesautre->Text = number_format($nbrAutreServices, '0', ',', ' ');
        $this->procedureservicesformalisetotal->Text = number_format($totalProcFormaliseServices, '0', ',', ' ');
        $this->procedureservicesadaptetotal->Text = number_format($totalProcAdapteServices, '0', ',', ' ');
        $this->grandTotalConsServices->Text = number_format(($totalProcAdapteServices + $totalProcFormaliseServices), '0', ',', ' ');

        $this->soustotalAo->Text = number_format(($nbrAoServices + $nbrAoFournitures + $nbrAoTravaux), '0', ',', ' ');
        $this->soustotalMn->Text = number_format(($nbrMnServices + $nbrMnFournitures + $nbrMnTravaux), '0', ',', ' ');
        $this->soustotalPn->Text = number_format(($nbrPnServices + $nbrPnFournitures + $nbrPnTravaux), '0', ',', ' ');
        $this->soustotalDc->Text = number_format(($nbrDcServices + $nbrDcFournitures + $nbrDcTravaux), '0', ',', ' ');
        $this->soustotalAutre->Text = number_format(($nbrAutreServices + $nbrAutreFournitures + $nbrAutreTravaux), '0', ',', ' ');
        $this->sousTotalFormalises->Text = number_format(($totalProcFormaliseServices + $totalProcFormaliseFournitures + $totalProcFormaliseTravaux), '0', ',', ' ');
        $this->sousTotalinf90->Text = number_format(($nbrMapinfServices + $nbrMapinfFournitures + $nbrMapinfTravaux), '0', ',', ' ');
        $this->sousTotalsup90->Text = number_format(($nbrMapsupServices + $nbrMapsupFournitures + $nbrMapsupTravaux), '0', ',', ' ');
        $this->sousTotaladapte->Text = number_format(($totalProcAdapteServices + $totalProcAdapteFournitures + $totalProcAdapteTravaux), '0', ',', ' ');
        $this->sousTotalCons->Text = number_format(($totalProcAdapteServices + $totalProcFormaliseServices) + ($totalProcFormaliseFournitures + $totalProcAdapteFournitures) + ($totalProcFormaliseTravaux + $totalProcAdapteTravaux), '0', ',', ' ');

        /*
         * calcule et remplissage  des champs pour les retraits.
         */
        if (is_array($resultsRetraitsTravaux)) {
            foreach ($resultsRetraitsTravaux as $result) {
                $nbrMapinfTravauxRetraits += $result['mapaInf'];
                $nbrMapsupTravauxRetraits += $result['mapaSupp'];
                $nbrAoTravauxRetraits += $result['ao'];
                $nbrMnTravauxRetraits += $result['mn'];
                $nbrPnTravauxRetraits += $result['pn'];
                $nbrDcTravauxRetraits += $result['dc'];
                $nbrAutreTravauxRetraits += $result['autre'];
            }
        }

        if (is_array($resultsRetraitsFournitures)) {
            foreach ($resultsRetraitsFournitures as $result) {
                $nbrMapinfFournituresRetraits += $result['mapaInf'];
                $nbrMapsupFournituresRetraits += $result['mapaSupp'];
                $nbrAoFournituresRetraits += $result['ao'];
                $nbrMnFournituresRetraits += $result['mn'];
                $nbrPnFournituresRetraits += $result['pn'];
                $nbrDcFournituresRetraits += $result['dc'];
                $nbrAutreFournituresRetraits += $result['autre'];
            }
        }

        if (is_array($resultsRetraitsServices)) {
            foreach ($resultsRetraitsServices as $result) {
                $nbrMapinfServicesRetraits += $result['mapaInf'];
                $nbrMapsupServicesRetraits += $result['mapaSupp'];
                $nbrAoServicesRetraits += $result['ao'];
                $nbrMnServicesRetraits += $result['mn'];
                $nbrPnServicesRetraits += $result['pn'];
                $nbrDcServicesRetraits += $result['dc'];
                $nbrAutreServicesRetraits += $result['autre'];
            }
        }

        $totalProcFormaliseTravauxRetraits = $nbrAoTravauxRetraits + $nbrMnTravauxRetraits + $nbrPnTravauxRetraits + $nbrDcTravauxRetraits + $nbrAutreTravauxRetraits;
        $totalProcAdapteTravauxRetraits = $nbrMapinfTravauxRetraits + $nbrMapsupTravauxRetraits;
        $totalProcFormaliseFournituresRetraits = $nbrAoFournituresRetraits + $nbrMnFournituresRetraits + $nbrPnFournituresRetraits + $nbrDcFournituresRetraits + $nbrAutreFournituresRetraits;
        $totalProcAdapteFournituresRetraits = $nbrMapinfFournituresRetraits + $nbrMapsupFournituresRetraits;
        $totalProcFormaliseServicesRetraits = $nbrAoServicesRetraits + $nbrMnServicesRetraits + $nbrPnServicesRetraits + $nbrDcServicesRetraits + $nbrAutreServicesRetraits;
        $totalProcAdapteServicesRetraits = $nbrMapinfServicesRetraits + $nbrMapsupServicesRetraits;

        $this->retraittravauxinf90->Text = number_format($nbrMapinfTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxsup90->Text = number_format($nbrMapsupTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxao->Text = number_format($nbrAoTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxmn->Text = number_format($nbrMnTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxpn->Text = number_format($nbrPnTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxdc->Text = number_format($nbrDcTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxautre->Text = number_format($nbrAutreTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxformalisetotal->Text = number_format($totalProcFormaliseTravauxRetraits, '0', ',', ' ');
        $this->retraittravauxadaptetotal->Text = number_format($totalProcAdapteTravauxRetraits, '0', ',', ' ');
        $this->grandTotalRetraitsTravaux->Text = number_format(($totalProcAdapteTravauxRetraits + $totalProcFormaliseTravauxRetraits), '0', ',', ' ');

        $this->retraitfournitureinf90->Text = number_format($nbrMapinfFournituresRetraits, '0', ',', ' ');
        $this->retraitfournituresup90->Text = number_format($nbrMapsupFournituresRetraits, '0', ',', ' ');
        $this->retraitfournitureao->Text = number_format($nbrAoFournituresRetraits, '0', ',', ' ');
        $this->retraitfournituremn->Text = number_format($nbrMnFournituresRetraits, '0', ',', ' ');
        $this->retraitfourniturepn->Text = number_format($nbrPnFournituresRetraits, '0', ',', ' ');
        $this->retraitfournituredc->Text = number_format($nbrDcFournituresRetraits, '0', ',', ' ');
        $this->retraitfournitureautre->Text = number_format($nbrAutreFournituresRetraits, '0', ',', ' ');
        $this->retraitfournitureformalisetotal->Text = number_format($totalProcFormaliseFournituresRetraits, '0', ',', ' ');
        $this->retraitfournitureadaptetotal->Text = number_format($totalProcAdapteFournituresRetraits, '0', ',', ' ');
        $this->grandTotalRetraitsFournitures->Text = number_format($totalProcAdapteFournituresRetraits + $totalProcFormaliseFournituresRetraits, '0', ',', ' ');

        $this->retraitserviceinf90->Text = number_format($nbrMapinfServicesRetraits, '0', ',', ' ');
        $this->retraitservicesup90->Text = number_format($nbrMapsupServicesRetraits, '0', ',', ' ');
        $this->retraitserviceao->Text = number_format($nbrAoServicesRetraits, '0', ',', ' ');
        $this->retraitservicemn->Text = number_format($nbrMnServicesRetraits, '0', ',', ' ');
        $this->retraitservicepn->Text = number_format($nbrPnServicesRetraits, '0', ',', ' ');
        $this->retraitservicedc->Text = number_format($nbrDcServicesRetraits, '0', ',', ' ');
        $this->retraitserviceautre->Text = number_format($nbrAutreServicesRetraits, '0', ',', ' ');
        $this->retraitserviceformalisetotal->Text = number_format($totalProcFormaliseServicesRetraits, '0', ',', ' ');
        $this->retraitserviceadaptetotal->Text = number_format($totalProcAdapteServicesRetraits, '0', ',', ' ');
        $this->grandTotalRetraitsServices->Text = number_format($totalProcAdapteServicesRetraits + $totalProcFormaliseServicesRetraits, '0', ',', ' ');

        $this->soustotalRetraitsAo->Text = number_format($nbrAoServicesRetraits + $nbrAoFournituresRetraits + $nbrAoTravauxRetraits, '0', ',', ' ');
        $this->soustotalRetraitsMn->Text = number_format($nbrMnServicesRetraits + $nbrMnFournituresRetraits + $nbrMnTravauxRetraits, '0', ',', ' ');
        $this->soustotalRetraitsPn->Text = number_format($nbrPnServicesRetraits + $nbrPnFournituresRetraits + $nbrPnTravauxRetraits, '0', ',', ' ');
        $this->soustotalRetraitsDc->Text = number_format($nbrDcServicesRetraits + $nbrDcFournituresRetraits + $nbrDcTravauxRetraits, '0', ',', ' ');
        $this->soustotalRetraitsAutre->Text = number_format($nbrAutreServicesRetraits + $nbrAutreFournituresRetraits + $nbrAutreTravauxRetraits, '0', ',', ' ');
        $this->sousTotalRetraitsFormalises->Text = number_format($totalProcFormaliseServicesRetraits + $totalProcFormaliseFournituresRetraits + $totalProcFormaliseTravauxRetraits, '0', ',', ' ');
        $this->sousTotalRetraitsinf90->Text = number_format($nbrMapinfServicesRetraits + $nbrMapinfFournituresRetraits + $nbrMapinfTravauxRetraits, '0', ',', ' ');
        $this->sousTotalRetraitssup90->Text = number_format($nbrMapsupServicesRetraits + $nbrMapsupFournituresRetraits + $nbrMapsupTravauxRetraits, '0', ',', ' ');
        $this->sousTotalRetraitsadapte->Text = number_format($totalProcAdapteServicesRetraits + $totalProcAdapteFournituresRetraits + $totalProcAdapteTravauxRetraits, '0', ',', ' ');
        $this->sousTotalRetraits->Text = number_format(($totalProcAdapteServicesRetraits + $totalProcFormaliseServicesRetraits) + ($totalProcFormaliseFournituresRetraits + $totalProcAdapteFournituresRetraits) + ($totalProcFormaliseTravauxRetraits + $totalProcAdapteTravauxRetraits), '0', ',', ' ');

        /*
         * calcule et remplissage  des champs pour les procédures mises en ligne.
         */

        if (is_array($resultsOffresTravaux)) {
            foreach ($resultsOffresTravaux as $result) {
                $nbrMapinfTravauxOffres += $result['mapaInf'];
                $nbrMapsupTravauxOffres += $result['mapaSupp'];
                $nbrAoTravauxOffres += $result['ao'];
                $nbrMnTravauxOffres += $result['mn'];
                $nbrPnTravauxOffres += $result['pn'];
                $nbrDcTravauxOffres += $result['dc'];
                $nbrAutreTravauxOffres += $result['autre'];
            }
        }

        if (is_array($resultsOffresFournitures)) {
            foreach ($resultsOffresFournitures as $result) {
                $nbrMapinfFournituresOffres += $result['mapaInf'];
                $nbrMapsupFournituresOffres += $result['mapaSupp'];
                $nbrAoFournituresOffres += $result['ao'];
                $nbrMnFournituresOffres += $result['mn'];
                $nbrPnFournituresOffres += $result['pn'];
                $nbrDcFournituresOffres += $result['dc'];
                $nbrAutreFournituresOffres += $result['autre'];
            }
        }

        if (is_array($resultsOffresServices)) {
            foreach ($resultsOffresServices as $result) {
                $nbrMapinfServicesOffres += $result['mapaInf'];
                $nbrMapsupServicesOffres += $result['mapaSupp'];
                $nbrAoServicesOffres += $result['ao'];
                $nbrMnServicesOffres += $result['mn'];
                $nbrPnServicesOffres += $result['pn'];
                $nbrDcServicesOffres += $result['dc'];
                $nbrAutreServicesOffres += $result['autre'];
            }
        }

        $totalProcFormaliseTravauxOffres = $nbrAoTravauxOffres + $nbrMnTravauxOffres + $nbrPnTravauxOffres + $nbrDcTravauxOffres + $nbrAutreTravauxOffres;
        $totalProcAdapteTravauxOffres = $nbrMapinfTravauxOffres + $nbrMapsupTravauxOffres;
        $totalProcFormaliseFournituresOffres = $nbrAoFournituresOffres + $nbrMnFournituresOffres + $nbrPnFournituresOffres + $nbrDcFournituresOffres + $nbrAutreFournituresOffres;
        $totalProcAdapteFournituresOffres = $nbrMapinfFournituresOffres + $nbrMapsupFournituresOffres;
        $totalProcFormaliseServicesOffres = $nbrAoServicesOffres + $nbrMnServicesOffres + $nbrPnServicesOffres + $nbrDcServicesOffres + $nbrAutreServicesOffres;
        $totalProcAdapteServicesOffres = $nbrMapinfServicesOffres + $nbrMapsupServicesOffres;

        $this->offretravauxinf90->Text = number_format($nbrMapinfTravauxOffres, '0', ',', ' ');
        $this->offretravauxsup90->Text = number_format($nbrMapsupTravauxOffres, '0', ',', ' ');
        $this->offretravauxao->Text = number_format($nbrAoTravauxOffres, '0', ',', ' ');
        $this->offretravauxmn->Text = number_format($nbrMnTravauxOffres, '0', ',', ' ');
        $this->offretravauxpn->Text = number_format($nbrPnTravauxOffres, '0', ',', ' ');
        $this->offretravauxdc->Text = number_format($nbrDcTravauxOffres, '0', ',', ' ');
        $this->offretravauxautre->Text = number_format($nbrAutreTravauxOffres, '0', ',', ' ');
        $this->offretravauxformalisetotal->Text = number_format($totalProcFormaliseTravauxOffres, '0', ',', ' ');
        $this->offretravauxadaptetotal->Text = number_format($totalProcAdapteTravauxOffres, '0', ',', ' ');
        $this->grandTotalDepotTravaux->Text = number_format($totalProcAdapteTravauxOffres + $totalProcFormaliseTravauxOffres, '0', ',', ' ');

        $this->offrefournitureinf90->Text = number_format($nbrMapinfFournituresOffres, '0', ',', ' ');
        $this->offrefournituresup90->Text = number_format($nbrMapsupFournituresOffres, '0', ',', ' ');
        $this->offrefournitureao->Text = number_format($nbrAoFournituresOffres, '0', ',', ' ');
        $this->offrefournituremn->Text = number_format($nbrMnFournituresOffres, '0', ',', ' ');
        $this->offrefourniturepn->Text = number_format($nbrPnFournituresOffres, '0', ',', ' ');
        $this->offrefournituredc->Text = number_format($nbrDcFournituresOffres, '0', ',', ' ');
        $this->offrefournitureautre->Text = number_format($nbrAutreFournituresOffres, '0', ',', ' ');
        $this->offrefournitureformalisetotal->Text = number_format($totalProcFormaliseFournituresOffres, '0', ',', ' ');
        $this->offrefournitureadaptetotal->Text = number_format($totalProcAdapteFournituresOffres, '0', ',', ' ');
        $this->grandTotalDepotFournitures->Text = number_format($totalProcAdapteFournituresOffres + $totalProcFormaliseFournituresOffres, '0', ',', ' ');

        $this->offreserviceinf90->Text = number_format($nbrMapinfServicesOffres, '0', ',', ' ');
        $this->offreservicesup90->Text = number_format($nbrMapsupServicesOffres, '0', ',', ' ');
        $this->offreserviceao->Text = number_format($nbrAoServicesOffres, '0', ',', ' ');
        $this->offreservicemn->Text = number_format($nbrMnServicesOffres, '0', ',', ' ');
        $this->offreservicepn->Text = number_format($nbrPnServicesOffres, '0', ',', ' ');
        $this->offreservicedc->Text = number_format($nbrDcServicesOffres, '0', ',', ' ');
        $this->offreserviceautre->Text = number_format($nbrAutreServicesOffres, '0', ',', ' ');
        $this->offreserviceformalisetotal->Text = number_format($totalProcFormaliseServicesOffres, '0', ',', ' ');
        $this->offreserviceadaptetotal->Text = number_format($totalProcAdapteServicesOffres, '0', ',', ' ');
        $this->grandTotalDepotServices->Text = number_format($totalProcAdapteServicesOffres + $totalProcFormaliseServicesOffres, '0', ',', ' ');

        $this->soustotalDepotAo->Text = number_format(($nbrAoServicesOffres + $nbrAoFournituresOffres + $nbrAoTravauxOffres), '0', ',', ' ');
        $this->soustotalDepotMn->Text = number_format(($nbrMnServicesOffres + $nbrMnFournituresOffres + $nbrMnTravauxOffres), '0', ',', ' ');
        $this->soustotalDepotPn->Text = number_format(($nbrPnServicesOffres + $nbrPnFournituresOffres + $nbrPnTravauxOffres), '0', ',', ' ');
        $this->soustotalDepotDc->Text = number_format(($nbrDcServicesOffres + $nbrDcFournituresOffres + $nbrDcTravauxOffres), '0', ',', ' ');
        $this->soustotalDepotAutre->Text = number_format(($nbrAutreServicesOffres + $nbrAutreFournituresOffres + $nbrAutreTravauxOffres), '0', ',', ' ');
        $this->sousTotalDepotFormalises->Text = number_format(($totalProcFormaliseServicesOffres + $totalProcFormaliseFournituresOffres + $totalProcFormaliseTravauxOffres), '0', ',', ' ');
        $this->sousTotalDepotinf90->Text = number_format(($nbrMapinfServicesOffres + $nbrMapinfFournituresOffres + $nbrMapinfTravauxOffres), '0', ',', ' ');
        $this->sousTotalDepotsup90->Text = number_format(($nbrMapsupServicesOffres + $nbrMapsupFournituresOffres + $nbrMapsupTravauxOffres), '0', ',', ' ');
        $this->sousTotalDepotadapte->Text = number_format(($totalProcAdapteServicesOffres + $totalProcAdapteFournituresOffres + $totalProcAdapteTravauxOffres), '0', ',', ' ');
        $this->sousTotalDepotCons->Text = number_format((($totalProcAdapteServicesOffres + $totalProcFormaliseServicesOffres) + ($totalProcFormaliseFournituresOffres + $totalProcAdapteFournituresOffres) + ($totalProcFormaliseTravauxOffres + $totalProcAdapteTravauxOffres)), '0', ',', ' ');

        /*
         * calcule et remplissage  des champs sad et accord cadre
         */

        /*$nbrSadTotal = $nbrSadTravaux + $nbrSadFournitures + $nbrSadServices;
        $nbrAccordCadreTotal = $nbrAccordCadreTravaux + $nbrAccordCadreFournitures + $nbrAccordCadreServices;
        $this->nbrsad->Text = number_format($nbrSadTotal,'0',',',' ');
        $this->nbraccordcadre->Text = number_format($nbrAccordCadreTotal,'0',',',' ');*/
    }

    public function onEffaceClick()
    {
        $this->critereFiltre_comp_1->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
        $this->cumulValeurs->Checked = false;
    }

    public function genererExcel()
    {
        $resultTravaux = [];
        $resultFournitures = [];
        $resultServices = [];
        $resultRetraitsTravaux = [];
        $resultRetraitsFournitures = [];
        $resultRetraitsSevices = [];
        $resultsOffresTravaux = [];
        $resultsOffresFournitures = [];
        $resultsOffresSevices = [];

        $dataStatistique = [];

        if (!isset($_GET['allOrgs'])) {
            $criteria = $this->getViewState('criteria');

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, false);
            if ($resultTravaux['MAPA_INF']) {
                $resultTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultTravaux['MAPA_INF'];
                unset($resultTravaux['MAPA_INF']);
            }
            if ($resultTravaux['MAPA_SUPP']) {
                $resultTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultTravaux['MAPA_SUPP'];
                unset($resultTravaux['MAPA_SUPP']);
            }

            $dataStatistique['procedureMiseEnLigne']['travaux'] = $resultTravaux;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, false);
            if ($resultFournitures['MAPA_INF']) {
                $resultFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultFournitures['MAPA_INF'];
                unset($resultFournitures['MAPA_INF']);
            }
            if ($resultFournitures['MAPA_SUPP']) {
                $resultFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultFournitures['MAPA_SUPP'];
                unset($resultFournitures['MAPA_SUPP']);
            }
            $dataStatistique['procedureMiseEnLigne']['fournitures'] = $resultFournitures;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultServices = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, false);
            if ($resultServices['MAPA_INF']) {
                $resultServices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultServices['MAPA_INF'];
                unset($resultServices['MAPA_INF']);
            }
            if ($resultServices['MAPA_SUPP']) {
                $resultServices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultServices['MAPA_SUPP'];
                unset($resultServices['MAPA_SUPP']);
            }
            $dataStatistique['procedureMiseEnLigne']['services'] = $resultServices;

            /*
             * Resultat de la recherche pour les retraits
             */
            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultRetraitsTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
            $resultRetraitsTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultRetraitsTravaux['MAPA_INF'];
            $resultRetraitsTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultRetraitsTravaux['MAPA_SUPP'];
            unset($resultRetraitsTravaux['MAPA_INF']);
            unset($resultRetraitsTravaux['MAPA_SUPP']);
            $dataStatistique['retraits']['travaux'] = $resultRetraitsTravaux;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultRetraitsFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
            $resultRetraitsFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultRetraitsFournitures['MAPA_INF'];
            $resultRetraitsFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultRetraitsFournitures['MAPA_SUPP'];
            unset($resultRetraitsFournitures['MAPA_INF']);
            unset($resultRetraitsFournitures['MAPA_SUPP']);
            $dataStatistique['retraits']['fournitures'] = $resultRetraitsFournitures;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultRetraitsSevices = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
            $resultRetraitsSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultRetraitsSevices['MAPA_INF'];
            $resultRetraitsSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultRetraitsSevices['MAPA_SUPP'];
            unset($resultRetraitsSevices['MAPA_INF']);
            unset($resultRetraitsSevices['MAPA_SUPP']);
            $dataStatistique['retraits']['services'] = $resultRetraitsSevices;

            /*
             * Resultat de la recherche pour les retraits
             */

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
            $resultOffresTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
            $resultOffresTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultOffresTravaux['MAPA_INF'];
            $resultOffresTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultOffresTravaux['MAPA_SUPP'];
            unset($resultOffresTravaux['MAPA_INF']);
            unset($resultOffresTravaux['MAPA_SUPP']);
            $resultsOffresTravaux = $resultOffresTravaux;
            $dataStatistique['offres']['travaux'] = $resultsOffresTravaux;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
            $resultOffresFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
            $resultOffresFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultOffresFournitures['MAPA_INF'];
            $resultOffresFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultOffresFournitures['MAPA_SUPP'];
            unset($resultOffresFournitures['MAPA_INF']);
            unset($resultOffresFournitures['MAPA_SUPP']);
            $resultsOffresFournitures = $resultOffresFournitures;
            $dataStatistique['offres']['fournitures'] = $resultsOffresFournitures;

            $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
            $resultOffresSevices = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
            $resultOffresSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')] = $resultOffresSevices['MAPA_INF'];
            $resultOffresSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')] = $resultOffresSevices['MAPA_SUPP'];
            unset($resultOffresSevices['MAPA_INF']);
            unset($resultOffresSevices['MAPA_SUPP']);
            $resultsOffresSevices = $resultOffresSevices;
            $dataStatistique['offres']['services'] = $resultsOffresSevices;
        } else {
            $atexoOrgs = Atexo_Organismes::retrieveOrganismes();
            /*
             * Critere de la recherche
            */

            $criteria = $this->getViewState('criteria');

            /*
             * Resultat de la recherche pour procédure mise en ligne
             */
            $arrayInitResults = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(isset($_GET['allOrgs']));
            foreach ($atexoOrgs as $atexoOrg) {
                $data = [];

                $criteria->setOrganisme($atexoOrg->getAcronyme());

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
                $resultTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, true);
                /*$resultTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultTravaux['MAPA_INF'];
                $resultTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultTravaux['MAPA_SUPP'];
                unset($resultTravaux['MAPA_INF']);
                unset($resultTravaux['MAPA_SUPP']);*/
                $data['procedureMiseEnLigne']['travaux'] = $resultTravaux;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
                $resultFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, true);

                /*$resultFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultFournitures['MAPA_INF'];
                $resultFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultFournitures['MAPA_SUPP'];
                unset($resultFournitures['MAPA_INF']);
                unset($resultFournitures['MAPA_SUPP']);*/
                $data['procedureMiseEnLigne']['fournitures'] = $resultFournitures;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
                $resultServices = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, true);
                /*$resultServices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultServices['MAPA_INF'];
                $resultServices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultServices['MAPA_SUPP'];
                unset($resultServices['MAPA_INF']);
                unset($resultServices['MAPA_SUPP']);*/

                $data['procedureMiseEnLigne']['services'] = $resultServices;

                /*
                 * Resultat de la recherche pour les retraits
                 */
                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
                $resultRetraitsTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
                /* $resultRetraitsTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultRetraitsTravaux['MAPA_INF'];
                 $resultRetraitsTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultRetraitsTravaux['MAPA_SUPP'];
                 unset($resultRetraitsTravaux['MAPA_INF']);
                 unset($resultRetraitsTravaux['MAPA_SUPP']);*/

                $data['retraits']['travaux'] = $resultRetraitsTravaux;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
                $resultRetraitsFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
                /* $resultRetraitsFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultRetraitsFournitures['MAPA_INF'];
                 $resultRetraitsFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultRetraitsFournitures['MAPA_SUPP'];
                 unset($resultRetraitsFournitures['MAPA_INF']);
                 unset($resultRetraitsFournitures['MAPA_SUPP']);*/

                $data['retraits']['fournitures'] = $resultRetraitsFournitures;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
                $resultRetraitsSevices = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
                /*$resultRetraitsSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultRetraitsSevices['MAPA_INF'];
                $resultRetraitsSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultRetraitsSevices['MAPA_SUPP'];
                unset($resultRetraitsSevices['MAPA_INF']);
                unset($resultRetraitsSevices['MAPA_SUPP']);*/
                $data['retraits']['services'] = $resultRetraitsSevices;

                /*
                 * Resultat de la recherche pour les retraits
                 */

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
                $resultOffresTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
                /* $resultOffresTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultOffresTravaux['MAPA_INF'];
                 $resultOffresTravaux[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultOffresTravaux['MAPA_SUPP'];
                 unset($resultOffresTravaux['MAPA_INF']);
                 unset($resultOffresTravaux['MAPA_SUPP']);*/
                $data['offres']['travaux'] = $resultOffresTravaux;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
                $resultOffresFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
                /*$resultOffresFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultOffresFournitures['MAPA_INF'];
                $resultOffresFournitures[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultOffresFournitures['MAPA_SUPP'];
                unset($resultOffresFournitures['MAPA_INF']);
                unset($resultOffresFournitures['MAPA_SUPP']);*/
                $data['offres']['fournitures'] = $resultOffresFournitures;

                $criteria->setCategorieConsultation(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
                $resultOffresSevices = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
                /* $resultOffresSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_INF')]=$resultOffresSevices['MAPA_INF'];
                 $resultOffresSevices[Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA_SUPP')]=$resultOffresSevices['MAPA_SUPP'];
                 unset($resultOffresSevices['MAPA_INF']);
                 unset($resultOffresSevices['MAPA_SUPP']);*/
                $data['offres']['services'] = $resultOffresSevices;

                $arrayTypeProcedure = (new Atexo_Consultation_ProcedureType())->rerieveAllTypeProcedureAndTypeProcedurePortail($atexoOrg->getAcronyme());
                foreach (['procedureMiseEnLigne', 'retraits', 'offres'] as $typeDonnee) {
                    foreach (['travaux', 'fournitures', 'services'] as $categorie) {
                        $newData = [];
                        if (is_array($data[$typeDonnee][$categorie])) {
                            $newData = (new Atexo_Statistiques_RequetesStatistiques())->correspondanceTypeProcedure($data[$typeDonnee][$categorie], $arrayTypeProcedure, $arrayInitResults);
                            foreach ($newData as $key => $value) {
                                $dataStatistique[$typeDonnee][$categorie][$key] += $value;
                            }
                        }
                        //$dataStatistique['procedureMiseEnLigne'][$categorie]=$newData;
                    }
                }
            }
        }

        $objetRecherche = 'Statistiques concernant les procédures mise en lignes ';
        if ($criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= 'entre le '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart()).' et le '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
        } elseif ($criteria->getDateMiseEnLigneStart() && !$criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= 'à patrir du '.Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneStart());
        } elseif (!$criteria->getDateMiseEnLigneStart() && $criteria->getDateMiseEnLigneEnd()) {
            $objetRecherche .= "jusqu'au ".Atexo_Util::iso2frnDate($criteria->getDateMiseEnLigneEnd());
        }

        $valeurCumule = '';
        $entiteName = '';
        $acronymeOrganisme = Atexo_CurrentUser::getCurrentOrganism();

        if (isset($_GET['allOrgs'])) {
            $entiteName = Prado::localize('DEFINE_TOUS_SERVICES_ORGANISMES_CONFONDUS');
        } else {
            $service = $criteria->getSelectedEntity();
            if ('0' === $service || 0 === $service) {
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                if ($organisme instanceof CommonOrganisme) {
                    $entiteName = $organisme->getDenominationOrg();
                }
            } else {
                $entite = Atexo_EntityPurchase::retrieveEntityById($service, $acronymeOrganisme);
                if ($entite instanceof CommonService) {
                    $entiteName = $entite->getLibelle();
                }
            }

            if ($criteria->getValeurCumule()) {
                $valeurCumule = ' / Valeurs cumulées';
            }
        }

        (new Atexo_GenerationExcel())->genererExcelStatistiquesMetiers($acronymeOrganisme, $dataStatistique, $objetRecherche, $valeurCumule, $entiteName, isset($_GET['allOrgs']));
    }

    /**
     * @return array
     *
     * @throws Atexo_Config_Exception
     */
    public function getDataFromSf()
    {
        $statistiquesMetierService = Atexo_Util::getSfService(StatistiquesMetierSF::class);

        return $statistiquesMetierService->renvoiStatistiquesPrado(
            Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text),
            Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text)
        );
    }

    /**
     * @param $data
     */
    public function remplirTab($data)
    {
        $this->proceduretravauxao->Text = $data['resultatsTravaux']['ao'];
        $this->proceduretravauxmn->Text = $data['resultatsTravaux']['mn'];
        $this->proceduretravauxpn->Text = $data['resultatsTravaux']['pn'];
        $this->proceduretravauxdc->Text = $data['resultatsTravaux']['dc'];
        $this->proceduretravauxautre->Text = $data['resultatsTravaux']['autre'];
        $this->proceduretravauxformalisetotal->Text = $data['resultatsTravaux']['total'];
        $this->proceduretravauxinf90->Text = $data['resultatsTravaux']['mapaInf'];
        $this->proceduretravauxsup90->Text = $data['resultatsTravaux']['mapaSupp'];
        $this->proceduretravauxadaptetotal->Text = $data['resultatsTravaux']['totalMapa'];
        $this->grandTotalConsTravaux->Text = $data['resultatsTravaux']['total'] + $data['resultatsTravaux']['totalMapa'];
        $this->procedurefournitureao->Text = $data['resultatsFournitures']['ao'];
        $this->procedurefournituremn->Text = $data['resultatsFournitures']['mn'];
        $this->procedurefourniturepn->Text = $data['resultatsFournitures']['pn'];
        $this->procedurefournituredc->Text = $data['resultatsFournitures']['dc'];
        $this->procedurefournitureautre->Text = $data['resultatsFournitures']['autre'];
        $this->procedurefournitureformalisetotal->Text = $data['resultatsFournitures']['total'];
        $this->procedurefournitureinf90->Text = $data['resultatsFournitures']['mapaInf'];
        $this->procedurefournituresup90->Text = $data['resultatsFournitures']['mapaSupp'];
        $this->procedurefournitureadaptetotal->Text = $data['resultatsFournitures']['totalMapa'];
        $this->grandTotalConsFournitures->Text = $data['resultatsFournitures']['total'] + $data['resultatsFournitures']['totalMapa'];
        $this->procedureservicesao->Text = $data['resultatsServices']['ao'];
        $this->procedureservicesmn->Text = $data['resultatsServices']['mn'];
        $this->procedureservicespn->Text = $data['resultatsServices']['pn'];
        $this->procedureservicesdc->Text = $data['resultatsServices']['dc'];
        $this->procedureservicesautre->Text = $data['resultatsServices']['autre'];
        $this->procedureservicesformalisetotal->Text = $data['resultatsServices']['total'];
        $this->procedureservicesinf90->Text = $data['resultatsServices']['mapaInf'];
        $this->procedureservicessup90->Text = $data['resultatsServices']['mapaSupp'];
        $this->procedureservicesadaptetotal->Text = $data['resultatsServices']['totalMapa'];
        $this->grandTotalConsServices->Text = $data['resultatsServices']['total'] + $data['resultatsServices']['totalMapa'];
        //premiere Ligne  sous totaux
        $this->soustotalAo->Text = $data['resultatsServices']['ao'] + $data['resultatsFournitures']['ao'] + $data['resultatsTravaux']['ao'];
        $this->soustotalMn->Text = $data['resultatsServices']['mn'] + $data['resultatsFournitures']['mn'] + $data['resultatsTravaux']['mn'];
        $this->soustotalPn->Text = $data['resultatsServices']['pn'] + $data['resultatsFournitures']['pn'] + $data['resultatsTravaux']['pn'];
        $this->soustotalDc->Text = $data['resultatsServices']['dc'] + $data['resultatsFournitures']['dc'] + $data['resultatsTravaux']['dc'];
        $this->soustotalAutre->Text = $data['resultatsServices']['autre'] + $data['resultatsFournitures']['autre'] + $data['resultatsTravaux']['autre'];
        $this->sousTotalFormalises->Text = $data['resultatsServices']['total'] + $data['resultatsFournitures']['total'] + $data['resultatsTravaux']['total'];
        $this->sousTotalinf90->Text = $data['resultatsServices']['mapaInf'] + $data['resultatsFournitures']['mapaInf'] + $data['resultatsTravaux']['mapaInf'];
        $this->sousTotalsup90->Text = $data['resultatsServices']['mapaSupp'] + $data['resultatsFournitures']['mapaSupp'] + $data['resultatsTravaux']['mapaSupp'];
        $this->sousTotaladapte->Text = $data['resultatsServices']['totalMapa'] + $data['resultatsFournitures']['totalMapa'] + $data['resultatsTravaux']['totalMapa'];
        $this->sousTotalCons->Text = $data['resultatsServices']['total'] + $data['resultatsFournitures']['total'] + $data['resultatsTravaux']['total'] + $data['resultatsServices']['totalMapa'] + $data['resultatsFournitures']['totalMapa'] + $data['resultatsTravaux']['totalMapa'];
        /* SECONDE LIGNE */
        $this->retraittravauxao->Text = $data['resultatsRetraitsTravaux']['ao'];
        $this->retraittravauxmn->Text = $data['resultatsRetraitsTravaux']['mn'];
        $this->retraittravauxpn->Text = $data['resultatsRetraitsTravaux']['pn'];
        $this->retraittravauxdc->Text = $data['resultatsRetraitsTravaux']['dc'];
        $this->retraittravauxautre->Text = $data['resultatsRetraitsTravaux']['autre'];
        $this->retraittravauxformalisetotal->Text = $data['resultatsRetraitsTravaux']['total'];
        $this->retraittravauxinf90->Text = $data['resultatsRetraitsTravaux']['mapaInf'];
        $this->retraittravauxsup90->Text = $data['resultatsRetraitsTravaux']['mapaSupp'];
        $this->retraittravauxadaptetotal->Text = $data['resultatsRetraitsTravaux']['totalMapa'];
        $this->grandTotalRetraitsTravaux->Text = $data['resultatsRetraitsTravaux']['total'] + $data['resultatsRetraitsTravaux']['totalMapa'];
        $this->retraitfournitureao->Text = $data['resultatsRetraitsFournitures']['ao'];
        $this->retraitfournituremn->Text = $data['resultatsRetraitsFournitures']['mn'];
        $this->retraitfourniturepn->Text = $data['resultatsRetraitsFournitures']['pn'];
        $this->retraitfournituredc->Text = $data['resultatsRetraitsFournitures']['dc'];
        $this->retraitfournitureautre->Text = $data['resultatsRetraitsFournitures']['autre'];
        $this->retraitfournitureformalisetotal->Text = $data['resultatsRetraitsFournitures']['total'];
        $this->retraitfournitureinf90->Text = $data['resultatsRetraitsFournitures']['mapaInf'];
        $this->retraitfournituresup90->Text = $data['resultatsRetraitsFournitures']['mapaSupp'];
        $this->retraitfournitureadaptetotal->Text = $data['resultatsRetraitsFournitures']['totalMapa'];
        $this->grandTotalRetraitsFournitures->Text = $data['resultatsRetraitsFournitures']['total'] + $data['resultatsRetraitsFournitures']['totalMapa'];
        $this->retraitserviceao->Text = $data['resultatsRetraitsServices']['ao'];
        $this->retraitservicemn->Text = $data['resultatsRetraitsServices']['mn'];
        $this->retraitservicepn->Text = $data['resultatsRetraitsServices']['pn'];
        $this->retraitservicedc->Text = $data['resultatsRetraitsServices']['dc'];
        $this->retraitserviceautre->Text = $data['resultatsRetraitsServices']['autre'];
        $this->retraitserviceformalisetotal->Text = $data['resultatsRetraitsServices']['total'];
        $this->retraitserviceinf90->Text = $data['resultatsRetraitsServices']['mapaInf'];
        $this->retraitservicesup90->Text = $data['resultatsRetraitsServices']['mapaSupp'];
        $this->retraitserviceadaptetotal->Text = $data['resultatsRetraitsServices']['totalMapa'];
        $this->grandTotalRetraitsServices->Text = $data['resultatsRetraitsServices']['total'] + $data['resultatsRetraitsServices']['totalMapa'];
        //seconde Ligne  sous totaux
        $this->soustotalRetraitsAo->Text = $data['resultatsRetraitsServices']['ao'] + $data['resultatsRetraitsFournitures']['ao'] + $data['resultatsRetraitsTravaux']['ao'];
        $this->soustotalRetraitsMn->Text = $data['resultatsRetraitsServices']['mn'] + $data['resultatsRetraitsFournitures']['mn'] + $data['resultatsRetraitsTravaux']['mn'];
        $this->soustotalRetraitsPn->Text = $data['resultatsRetraitsServices']['pn'] + $data['resultatsRetraitsFournitures']['pn'] + $data['resultatsRetraitsTravaux']['pn'];
        $this->soustotalRetraitsDc->Text = $data['resultatsRetraitsServices']['dc'] + $data['resultatsRetraitsFournitures']['dc'] + $data['resultatsRetraitsTravaux']['dc'];
        $this->soustotalRetraitsAutre->Text = $data['resultatsRetraitsServices']['autre'] + $data['resultatsRetraitsFournitures']['autre'] + $data['resultatsRetraitsTravaux']['autre'];
        $this->sousTotalRetraitsFormalises->Text = $data['resultatsRetraitsServices']['total'] + $data['resultatsRetraitsFournitures']['total'] + $data['resultatsRetraitsTravaux']['total'];
        $this->sousTotalRetraitsinf90->Text = $data['resultatsRetraitsServices']['mapaInf'] + $data['resultatsRetraitsFournitures']['mapaInf'] + $data['resultatsRetraitsTravaux']['mapaInf'];
        $this->sousTotalRetraitssup90->Text = $data['resultatsRetraitsServices']['mapaSupp'] + $data['resultatsRetraitsFournitures']['mapaSupp'] + $data['resultatsRetraitsTravaux']['mapaSupp'];
        $this->sousTotalRetraitsadapte->Text = $data['resultatsRetraitsServices']['totalMapa'] + $data['resultatsRetraitsFournitures']['totalMapa'] + $data['resultatsRetraitsTravaux']['totalMapa'];
        $this->sousTotalRetraits->Text = $data['resultatsRetraitsServices']['total'] + $data['resultatsRetraitsFournitures']['total'] + $data['resultatsRetraitsTravaux']['total'] + $data['resultatsRetraitsServices']['totalMapa'] + $data['resultatsRetraitsFournitures']['totalMapa'] + $data['resultatsRetraitsTravaux']['totalMapa'];
        /* TROISIEME LIGNE */
        $this->offretravauxao->Text = $data['resultatsOffresTravaux']['ao'];
        $this->offretravauxmn->Text = $data['resultatsOffresTravaux']['mn'];
        $this->offretravauxpn->Text = $data['resultatsOffresTravaux']['pn'];
        $this->offretravauxdc->Text = $data['resultatsOffresTravaux']['dc'];
        $this->offretravauxautre->Text = $data['resultatsOffresTravaux']['autre'];
        $this->offretravauxformalisetotal->Text = $data['resultatsOffresTravaux']['total'];
        $this->offretravauxinf90->Text = $data['resultatsOffresTravaux']['mapaInf'];
        $this->offretravauxsup90->Text = $data['resultatsOffresTravaux']['mapaSupp'];
        $this->offretravauxadaptetotal->Text = $data['resultatsOffresTravaux']['totalMapa'];
        $this->grandTotalDepotTravaux->Text = $data['resultatsOffresTravaux']['total'] + $data['resultatsOffresTravaux']['totalMapa'];
        $this->offrefournitureao->Text = $data['resultatsOffresFournitures']['ao'];
        $this->offrefournituremn->Text = $data['resultatsOffresFournitures']['mn'];
        $this->offrefourniturepn->Text = $data['resultatsOffresFournitures']['pn'];
        $this->offrefournituredc->Text = $data['resultatsOffresFournitures']['dc'];
        $this->offrefournitureautre->Text = $data['resultatsOffresFournitures']['autre'];
        $this->offrefournitureformalisetotal->Text = $data['resultatsOffresFournitures']['total'];
        $this->offrefournitureinf90->Text = $data['resultatsOffresFournitures']['mapaInf'];
        $this->offrefournituresup90->Text = $data['resultatsOffresFournitures']['mapaSupp'];
        $this->offrefournitureadaptetotal->Text = $data['resultatsOffresFournitures']['totalMapa'];
        $this->grandTotalDepotFournitures->Text = $data['resultatsOffresFournitures']['total'] + $data['resultatsOffresFournitures']['totalMapa'];
        $this->offreserviceao->Text = $data['resultatsOffresServices']['ao'];
        $this->offreservicemn->Text = $data['resultatsOffresServices']['mn'];
        $this->offreservicepn->Text = $data['resultatsOffresServices']['pn'];
        $this->offreservicedc->Text = $data['resultatsOffresServices']['dc'];
        $this->offreserviceautre->Text = $data['resultatsOffresServices']['autre'];
        $this->offreserviceformalisetotal->Text = $data['resultatsOffresServices']['total'];
        $this->offreserviceinf90->Text = $data['resultatsOffresServices']['mapaInf'];
        $this->offreservicesup90->Text = $data['resultatsOffresServices']['mapaSupp'];
        $this->offreserviceadaptetotal->Text = $data['resultatsOffresServices']['totalMapa'];
        $this->grandTotalDepotServices->Text = $data['resultatsOffresServices']['total'] + $data['resultatsOffresServices']['totalMapa'];
        //troisieme Ligne  sous totaux
        $this->soustotalDepotAo->Text = $data['resultatsOffresServices']['ao'] + $data['resultatsOffresFournitures']['ao'] + $data['resultatsOffresTravaux']['ao'];
        $this->soustotalDepotMn->Text = $data['resultatsOffresServices']['mn'] + $data['resultatsOffresFournitures']['mn'] + $data['resultatsOffresTravaux']['mn'];
        $this->soustotalDepotPn->Text = $data['resultatsOffresServices']['pn'] + $data['resultatsOffresFournitures']['pn'] + $data['resultatsOffresTravaux']['pn'];
        $this->soustotalDepotDc->Text = $data['resultatsOffresServices']['dc'] + $data['resultatsOffresFournitures']['dc'] + $data['resultatsOffresTravaux']['dc'];
        $this->soustotalDepotAutre->Text = $data['resultatsOffresServices']['autre'] + $data['resultatsOffresFournitures']['autre'] + $data['resultatsOffresTravaux']['autre'];
        $this->sousTotalDepotFormalises->Text = $data['resultatsOffresServices']['total'] + $data['resultatsOffresFournitures']['total'] + $data['resultatsOffresTravaux']['total'];
        $this->sousTotalDepotinf90->Text = $data['resultatsOffresServices']['mapaInf'] + $data['resultatsOffresFournitures']['mapaInf'] + $data['resultatsOffresTravaux']['mapaInf'];
        $this->sousTotalDepotsup90->Text = $data['resultatsOffresServices']['mapaSupp'] + $data['resultatsOffresFournitures']['mapaSupp'] + $data['resultatsOffresTravaux']['mapaSupp'];
        $this->sousTotalDepotadapte->Text = $data['resultatsOffresServices']['totalMapa'] + $data['resultatsOffresFournitures']['totalMapa'] + $data['resultatsOffresTravaux']['totalMapa'];
        $this->sousTotalDepotCons->Text = $data['resultatsOffresServices']['total'] + $data['resultatsOffresFournitures']['total'] + $data['resultatsOffresTravaux']['total'] + $data['resultatsOffresServices']['totalMapa'] + $data['resultatsOffresFournitures']['totalMapa'] + $data['resultatsOffresTravaux']['totalMapa'];
    }
}
