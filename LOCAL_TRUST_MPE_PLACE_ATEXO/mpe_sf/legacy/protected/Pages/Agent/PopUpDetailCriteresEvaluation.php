<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonCriteresEvaluation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/*
 * Created on 21 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */
class PopUpDetailCriteresEvaluation extends MpeTPage
{
    public $_criteresEval;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);
            $this->ConsultationSummary->setConsultation($consultation);
            //Criteres Evaluation
            $this->_criteresEval = (new Atexo_CriteresEvaluation())->retrieveCriteresEvaluationById(Atexo_Util::atexoHtmlEntities($_GET['id']));
            if (!($this->_criteresEval instanceof CommonCriteresEvaluation)) {
                $this->contenteval->setVisible(false);
            }
        }
    }

    public function getDescriptionEnveloppe()
    {
        $descLot = '';
        if ($this->_criteresEval instanceof CommonCriteresEvaluation) {
            if ('0' != $this->_criteresEval->getLot()) {
                $descLot = (new Atexo_CriteresEvaluation())->getDescriptionLot($this->_criteresEval->getLot(), $this->_criteresEval->getOrganisme(), $this->_criteresEval->getConsultationId()).'- ';
            }

            return $descLot.$this->_criteresEval->getLibelleTypeEnveloppe();
        }

        return $descLot;
    }

    /**
     * Retourne le type de l'enveloppe encodé en base64.
     */
    public function getTypeEnveloppe()
    {
        return base64_encode($this->_criteresEval->getTypeEnveloppe());
    }
}
