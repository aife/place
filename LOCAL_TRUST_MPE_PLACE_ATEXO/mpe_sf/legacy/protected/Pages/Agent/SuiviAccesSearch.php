<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Template de recherche de la liste des accès.
 */
class SuiviAccesSearch extends MpeTPage
{
    public $_parentPage;
    public $_service;

    public function onLoad($param)
    {
        if (1 == $_GET['all']) {
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $this->IdChoixOrganisme->visible = true;
                $this->IdChoixEntiteAchat->visible = false;
            } else {
                $this->IdChoixEntiteAchat->visible = true;
                $this->IdChoixOrganisme->visible = false;
            }
        } else {
            $this->IdChoixEntiteAchat->visible = true;
            $this->IdChoixOrganisme->visible = false;
        }
    }
}
