<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulairePeer;
use Application\Propel\Mpe\CommonTParamDossierFormulaire;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_CriteriaVo;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Parametrage;
use Exception;
use Prado\Prado;

/**
 * Page de gestion des demandes de complément.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class TableauDeBordDemandesComplements extends MpeTPage
{
    private $_consultation;
    private $_cleDispositif;

    /**
     * Recupère la valeur.
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    /**
     * Affectes la valeur.
     */
    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getCleDispositif()
    {
        return $this->_cleDispositif;
    }

    /**
     * Affectes la valeur.
     */
    public function setCleDispositif($value)
    {
        $this->_cleDispositif = $value;
    }

    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['id']) {
            $this->setConsultation($this->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            if ($this->getConsultation() instanceof CommonConsultation) {
                $this->IdConsultationSummary->setConsultation($this->getConsultation());
                //Recuperation des dossiers en demande de complément
                $this->recupererDossiersDemandesComplementForUpdateDossier();
                //Afficher les demandes de complement
                $this->remplirCriteriaVoChargementPage();
                if (!$this->isPostBack) {
                    $criteriaVo = $this->getViewState('CriteriaVo');
                    if (!$criteriaVo) {
                        $criteriaVo = new Atexo_FormulaireSub_CriteriaVo();
                    }
                    $criteriaVo->setTypeDossier(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    $this->fillRepeaterWithDataForSearchResult($criteriaVo);
                    $this->remplirListeLots();
                    $this->remplirListeMessageInformation();
                    $this->remplirListeEtatReouverture();
                    $this->rempliMotifsReouverture();
                }
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /**
     * Permet de recuperer les dossiers en demande de compléments et les sauvegarder en base.
     */
    public function recupererDossiersDemandesComplementForUpdateDossier()
    {
        $cleExterneDispositif = '';
        $consultationId = $this->getConsultation()->getId();
        $org = $this->getConsultation()->getOrganisme();
        $envCand = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        $envOffre = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
        // Reccuperer le disposifif selon la consultation & le type d'enveloppe
        $paramDossierCand = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($consultationId, $org, $envCand);
        if ($paramDossierCand instanceof CommonTParamDossierFormulaire) {
            $cleExterneDispositif .= $paramDossierCand->getCleExterneDispositif();
        }
        $paramDossierOffre = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($consultationId, $org, $envOffre);
        if ($paramDossierOffre instanceof CommonTParamDossierFormulaire) {
            $cleExterneDispositif .= ','.$paramDossierOffre->getCleExterneDispositif();
        }
        $this->_cleDispositif = $cleExterneDispositif;
        //Recupere le flux xml de demandes de compléments
        $xml = (new Atexo_FormulaireSub_Echange())->recupererXmlDemandesComplement($cleExterneDispositif); //print_r($xml);exit;

        //Parse le flux de demandes de complément et retour un tableau de dossiers
        $listeDossiersDemandesComplement = (new Atexo_FormulaireSub_Dossier())->parseXmlDemandeComplementsReturnDossiers($xml); //print_r($listeDossiersDemandesComplement);exit;

        //Sauvegarde dossiers et demandes de complement
        self::updateDossiersDemandesComplements($listeDossiersDemandesComplement);
    }

    /**
     * Permet de sauvegarder dossier et demandes de complement.
     *
     * @param array $listeDossiersDemandesComplement: liste d'objet de dossier et de demandes de complements
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function updateDossiersDemandesComplements($listeDossiersDemandesComplement)
    {
        $connexion = null;
        try {
            if (is_array($listeDossiersDemandesComplement) && count($listeDossiersDemandesComplement)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $connexion->beginTransaction();

                foreach ($listeDossiersDemandesComplement as $compl) {
                    if ($compl['dossier'] instanceof CommonTDossierFormulaire) {
                        //Recuperation dossier principal
                        $cleExterneDossier = $compl['dossier']->getCleExterneDossier();
                        $numComplement = '';
                        if ($compl['complementFormulaire'] instanceof CommonTComplementFormulaire) {
                            $numComplement = $compl['complementFormulaire']->getNumeroComplement();
                        }

                        $typeReponseComp = Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT');
                        $dossierDemandeComp = (new Atexo_FormulaireSub_Dossier())->getDossierByCleExterneAndTypeReponse($cleExterneDossier, $typeReponseComp, $numComplement);
                        //print_r($dossierDemandeComp);exit;

                        if ($dossierDemandeComp instanceof CommonTDossierFormulaire) {//Une demande de complément existe deja pour ce dossier
                            //Recuperer l'objet "Demande de complément" et le mettre à jour
                            $idDossier = $dossierDemandeComp->getIdDossierFormulaire();
                            $complementFormulaire = (new Atexo_FormulaireSub_ComplementFormulaire())->getComplementFormulaireByIdDossier($idDossier);
                            if ($complementFormulaire) {
                                $this->sauvegarderComplementFormulaire($complementFormulaire, $compl['complementFormulaire'], $connexion);
                            }
                            $dossierDemandeComp->setReferenceDossierSub($compl['dossier']->getReferenceDossierSub());
                            $dossierDemandeComp->save($connexion);
                        } else {//Aucune demande de complément n'existe pour ce dossier
                            $typeReponsePrinc = Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE');
                            $dossierPrincipal = (new Atexo_FormulaireSub_Dossier())->getDossierByCleExterneAndTypeReponse($cleExterneDossier, $typeReponsePrinc); //print_r($dossierPrincipal);
                            if ($dossierPrincipal) {
                                //Copie et sauvegarde dossier principal en dossier demandes de complement
                                $dossierEnDemandeComplement = self::copierDossierPrincipalEnStatutDemandeComplement($dossierPrincipal, $connexion, $compl['dossier']->getReferenceDossierSub());
                                //print_r($dossierEnDemandeComplement);
                                //Sauvegarde du complement dossier (objet CommonTComplementFormulaire)
                                $this->sauvegarderComplementFormulaire($dossierEnDemandeComplement, $compl['complementFormulaire'], $connexion);
                                //print_r($complementFormulaire);
                            }
                        }
                    }
                }
                //Comitter la connexion
                $connexion->commit();
            }
        } catch (Exception $ex) {
            $connexion->rollback();
            echo $ex->getMessage();
        }
    }

    /**
     * Copie et sauvegarde dossier principal en dossier demandes de complement.
     *
     * @param CommonTDossierFormulaire $dossierPrincipal: dossier principal
     * @param unknown_type             $connexion:        objet connexion
     * @param string                   $numDossierSub:    numDossierSub
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function copierDossierPrincipalEnStatutDemandeComplement($dossierPrincipal, $connexion, $numDossierSub)
    {
        if ($dossierPrincipal instanceof CommonTDossierFormulaire) {
            $dossierEnDemandeComplement = $dossierPrincipal->copy();
            $dossierEnDemandeComplement->setNew(true);
            $dossierEnDemandeComplement->setTypeReponse(Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT'));
            $dossierEnDemandeComplement->setStatutValidation(Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON'));
            $dossierEnDemandeComplement->setReferenceDossierSub($numDossierSub);
            $dossierEnDemandeComplement->save($connexion);

            return $dossierEnDemandeComplement;
        }
    }

    /**
     * Sauvegarde du complement dossier (objet CommonTComplementFormulaire).
     *
     * @param $demandeComplement : instanceof CommonTDossierFormulaire pour la création, instanceof CommonTComplementFormulaire pour la mise à jour
     * Lors de la mise à jour $demandeComplement est le complément lié au dossier en demande de complément deja existant
     * @param $complementFormulaireParse : L'objet CommonTComplementFormulaire obtenu lors du parse de l'xml
     * @param $connexion : objet connexion
     */
    public function sauvegarderComplementFormulaire($demandeComplement, $complementFormulaireParse, $connexion)
    {
        $complementFormulaire = null;
        if ($demandeComplement instanceof CommonTDossierFormulaire) {
            $complementFormulaire = new CommonTComplementFormulaire();
            $complementFormulaire->setIdDossierFormulaire($demandeComplement->getIdDossierFormulaire());
        } elseif ($demandeComplement instanceof CommonTComplementFormulaire) {
            $complementFormulaire = $demandeComplement;
        }
        $complementFormulaire->setNumeroComplement($complementFormulaireParse->getNumeroComplement());
        $complementFormulaire->setDateARemettre(Atexo_Util::frnDateTime2iso($complementFormulaireParse->getDateARemettre()));
        $complementFormulaire->setMotif($complementFormulaireParse->getMotif());
        $complementFormulaire->setCommentaire($complementFormulaireParse->getCommentaire());
        $complementFormulaire->save($connexion); //print_r($complementFormulaire);exit;

        return $complementFormulaire;
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     */
    public function changeActiveTabAndDataForRepeater($sender)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        if (!$criteriaVo) {
            $criteriaVo = new Atexo_FormulaireSub_CriteriaVo();
        }
        $criteriaVo->setRefCons(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                $this->onglet1->setCssClass('tab-on');
                               $this->onglet2->setCssClass('tab');
                               $criteriaVo->setLot('0');
                               $criteriaVo->setTypeDossier(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                break;
            case 'secondTab':
                $this->onglet1->setCssClass('tab');
                               $this->onglet2->setCssClass('tab-on');
                               $criteriaVo->setTypeDossier(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                               $this->listeLot->SelectedValue = '0';
                break;
        }
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Pagination.
     */
    public function pageChanged($sender, $param)
    {
        $this->tableauDeBordRepeater->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    /**
     * Pagination.
     */
    public function goToPage($sender)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nombreResultatAfficherTop->getSelectedValue(), $numPage);
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
        }
    }

    /**
     * Pagination.
     */
    public function changePagerLenght($sender)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->numPageTop->Text);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    /*
     * Permet de mettre à jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nombreElement');
        $this->tableauDeBordRepeater->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->tableauDeBordRepeater->setCurrentPageIndex($numPage - 1);
        $this->numPageBottom->Text = $numPage;
        $this->numPageTop->Text = $numPage;
    }

    public function fillRepeaterWithDataForSearchResult(Atexo_FormulaireSub_CriteriaVo $criteriaVo)
    {
        $nombreElement = (new Atexo_FormulaireSub_ComplementFormulaire())->recupererDemandesComplement($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->gererAffichageResultatsTrouves();
            $this->nombreElement->Text = $nombreElement;
            $this->setViewState('nombreElement', $nombreElement);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->tableauDeBordRepeater->setVirtualItemCount($nombreElement);
            $this->tableauDeBordRepeater->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->gererAffichageResultatsNonTrouves();
        }
    }

    /**
     * Permet de gerer l'affichage si le tableau ramène des resultats.
     */
    public function gererAffichageResultatsTrouves()
    {
        $this->panelMoreThanOneElementFound->setVisible(true);
        $this->panelNbrResults->setVisible(true);
        $this->panelNoElementFound->setVisible(false);
        $this->PagerBottom->setVisible(true);
        $this->PagerTop->setVisible(true);
    }

    /**
     * Permet de gerer l'affichage si le tableau ne ramène pas de resultat.
     */
    public function gererAffichageResultatsNonTrouves()
    {
        $this->panelMoreThanOneElementFound->setVisible(false);
        $this->panelNbrResults->setVisible(false);
        $this->panelNoElementFound->setVisible(true);
        $this->PagerBottom->setVisible(false);
        $this->PagerTop->setVisible(false);
    }

    public function populateData(Atexo_FormulaireSub_CriteriaVo $criteriaVo)
    {
        $offset = $this->tableauDeBordRepeater->CurrentPageIndex * $this->tableauDeBordRepeater->PageSize;
        $limit = $this->tableauDeBordRepeater->PageSize;
        if ($offset + $limit > $this->tableauDeBordRepeater->getVirtualItemCount()) {
            $limit = $this->tableauDeBordRepeater->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $arrayDemandesComplement = (new Atexo_FormulaireSub_ComplementFormulaire())->recupererDemandesComplement($criteriaVo);
        $this->tableauDeBordRepeater->DataSource = $arrayDemandesComplement;
        $this->tableauDeBordRepeater->DataBind();
    }

    /**
     * Permet de recuperer l'enveloppe.
     */
    public function getDossierEnveloppe($idDossier)
    {
        //Recuperation dossier initial
        $dossierComplement = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($idDossier);
        if ($dossierComplement) {
            $dossierInitial = (new Atexo_FormulaireSub_Dossier())->getDossierByCleExterneAndTypeReponse($dossierComplement->getCleExterneDossier(), Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE'));
            if ($dossierInitial) {
                $dossierEnveloppe = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdDossierAndOrg($dossierInitial->getIdDossierFormulaire(), Atexo_CurrentUser::getCurrentOrganism());
                if ($dossierEnveloppe) {
                    return $dossierEnveloppe;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Permet de recuperer l'enveloppe.
     */
    public function getEnveloppe($idDossier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $dossierEnveloppe = $this->getDossierEnveloppe($idDossier);
        if ($dossierEnveloppe) {
            $enveloppe = CommonEnveloppePeer::retrieveByPK($dossierEnveloppe->getIdEnveloppe(), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
            if ($enveloppe instanceof CommonEnveloppe) {
                return $enveloppe;
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de recuperer l'entreprise.
     */
    public function getEntreprise($idDossier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $enveloppe = $this->getEnveloppe($idDossier);
        if ($enveloppe instanceof CommonEnveloppe) {
            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($enveloppe->getOffreId(), Atexo_CurrentUser::getCurrentOrganism());
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($offre->getEntrepriseId(), $connexion, false);
            if ($entreprise) {
                return $entreprise;
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de recuperer l'entreprise.
     */
    public function getIdInscritForThisClass($idDossier)
    {
        $enveloppe = $this->getEnveloppe($idDossier);
        if ($enveloppe instanceof CommonEnveloppe) {
            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($enveloppe->getOffreId(), Atexo_CurrentUser::getCurrentOrganism());
            if ($offre instanceof CommonOffres) {
                return $offre->getInscritId();
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de recuperer l'url du courrier de demandes de compléments.
     */
    public function getUrlCourrierDemandesComplements($idInscrit, $idComplement)
    {
        $url = '';
        if ($idInscrit && $this->getConsultation() instanceof CommonConsultation) {
            $ref = $this->getConsultation()->getId();
            $url = '?page=Agent.EnvoiCourrierElectroniqueComplementsFormulaires&id='.$ref.'&IdInscrit='.$idInscrit.'&idsCompl='.base64_encode($idComplement.'_'.$idInscrit);
        }

        return $url;
    }

    /**
     * Permet de recuperer le nom de l'entreprise.
     */
    public function getNomEntreprise($idDossier)
    {
        $entreprise = $this->getEntreprise($idDossier);
        if ($entreprise instanceof Entreprise) {
            return $entreprise->getNom();
        } else {
            return ' - ';
        }
    }

    /**
     * Recupère le nom du dossier.
     */
    public function getNomDossier($idDossier)
    {
        $enveloppe = $this->getEnveloppe($idDossier);
        if ($enveloppe instanceof CommonEnveloppe) {
            $nomDossier = '';
            if ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                $nomDossier .= Prado::localize('CANDIDATURE');
            } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                $nomDossier .= Prado::localize('OFFRE_TECHNIQUE');
            } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                $nomDossier .= Prado::localize('OFFRE');
            } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                $nomDossier .= Prado::localize('ANONYMAT');
            }
            $dossierEnveloppe = $this->getDossierEnveloppe($idDossier);
            if ($dossierEnveloppe) {
                $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->getConsultation()->getId(), $dossierEnveloppe->getIdLot(), $this->getConsultation()->getOrganisme());
                if ($lot instanceof CommonCategorieLot && $lot->getLot()) {
                    $nomDossier .= ' - '.Prado::localize('TEXT_LOT').' '.$lot->getLot();
                }
            }

            return $nomDossier;
        }
    }

    /**
     * Permet de recuperer la date limite de reponse.
     */
    public function getDateLimiteReponse($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateLimite = Atexo_Util::iso2frnDateTime($complementFormulaire->getDateARemettre(), true);
            if ($dateLimite) {
                return $dateLimite;
            } else {
                return ' - ';
            }
        } else {
            return ' - ';
        }
    }

    /**
     * Permet de recuperer la date d'envoi du complément dossier.
     */
    public function getDateReponseComplement($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateRemiseReponse = Atexo_Util::iso2frnDateTime($complementFormulaire->getDateRemisLe(), true);
            if ($dateRemiseReponse) {
                return $dateRemiseReponse;
            } else {
                return ' - ';
            }
        } else {
            return ' - ';
        }
    }

    /**
     * Permet de recuperer la date de premier accuse de reception du mail de demande de complements.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return string : date d'accuse de reception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getDateAccuseReception($complementFormulaire)
    {
        $dateAr = '';
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateAr = Atexo_Util::iso2frnDateTime($complementFormulaire->getDate1erAr(), true);
        }

        return $dateAr;
    }

    /**
     * Permet de recuperer la date d'envoi du premier mail de demande de complements.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return string : date d'envoi
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getDateEnvoiDossier($complementFormulaire)
    {
        $dateEnvoi = '';
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateEnvoi = Atexo_Util::iso2frnDateTime($complementFormulaire->getDateEnvoi1erMail(), true);
        }

        return $dateEnvoi;
    }

    /**
     * Permet de recuperer l'url du courrier de demandes de compléments.
     */
    public function goToEnvoiGroupeDemandesComplements()
    {
        $ref = $this->getConsultation()->getId();
        $url = '?page=Agent.EnvoiCourrierElectroniqueComplementsFormulaires&id='.$ref.'&IdInscrit&idsCompl=';
        $idsCompl = '';
        foreach ($this->tableauDeBordRepeater->getItems() as $item) {
            if ($item->idTcheckboxComplement->Checked && $this->getConsultation() instanceof CommonConsultation) {
                $idInscrit = $item->idInscrit->value;
                $idComplement = $item->idComplement->value;
                if ($idInscrit && $idComplement) {
                    $idsCompl .= $idComplement.'_'.$idInscrit.'#';
                }
            }
        }
        $url .= base64_encode($idsCompl);
        $this->response->redirect($url);
    }

    /**
     * Permet de determiner le picto de la date limite de demande de compléments.
     */
    public function getPictoDateLimiteComplement($complement)
    {
        if ($complement instanceof CommonTComplementFormulaire && $complement->getDateARemettre()) {
            if ($complement->getDateARemettre() < date('Y-m-d h:i:s')) {
                return '/images/picto-time-off.gif';
            }

            return '/images/picto-time-on.gif';
        }
    }

    /**
     * Permet de determiner la classe de la date limite de demande de compléments.
     */
    public function getClasseDateLimiteComplement($complement)
    {
        if ($complement instanceof CommonTComplementFormulaire && $complement->getDateARemettre()) {
            if ($complement->getDateARemettre() < date('Y-m-d h:i:s')) {
                return 'time-red';
            }

            return 'time-green';
        }
    }

    /**
     * Permet de gérer la visibilité du picto "envoyer".
     */
    public function getVisibilitePictoEnvoyer($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            if ($this->reponseDemandeComplementsEnvoyee($complementFormulaire) || $this->isDateLimiteReponseDepassee($complementFormulaire)) {
                return false;
            }
            if ($this->mailReouvertureJamaisEnvoye($complementFormulaire) || $this->mailReouvertureJamaisAccuseParOf($complementFormulaire)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de preciser si la date limite de reponse est cloturee.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return bool : true si date limite reponse depassee, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isDateLimiteReponseDepassee($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            if ($complementFormulaire->getDateARemettre() <= date('Y-m-d H:i')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de preciser si le mail de reouverture n'a jamais ete envoye.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return bool : true si le mail n'a jamais été envoyé, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function mailReouvertureJamaisEnvoye($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateEnvoi1erMail = $complementFormulaire->getDateEnvoi1erMail();
            if (null == $dateEnvoi1erMail || empty($dateEnvoi1erMail)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de preciser si l'OF n'a jamais accuse reception du mail.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return bool : true si l'OF n'a jamais accuse reception du mail, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function mailReouvertureJamaisAccuseParOf($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateEnvoi1erMail = $complementFormulaire->getDateEnvoi1erMail();
            $date1erAccuseReception = $complementFormulaire->getDate1erAr();
            if ((null != $dateEnvoi1erMail && !empty($dateEnvoi1erMail)) && (null == $date1erAccuseReception || empty($date1erAccuseReception))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de preciser si la reponse a la demande de complements a ete deja deposee.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return bool : true si reponse deja deposee, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function reponseDemandeComplementsEnvoyee($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            $dateReponse = $complementFormulaire->getDateRemisLe();
            if (null != $dateReponse && !empty($dateReponse)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Permet de remplir la liste des lots
     */
    public function remplirListeLots()
    {
        //Lot n° 1 : Réfection de la chaussée
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $listeLots = [];
        if ($this->getConsultation() instanceof CommonConsultation && $this->getConsultation()->getAlloti()) {
            $arrayLots = $this->getConsultation()->getAllLots($connexion);
            if (is_array($arrayLots) && count($arrayLots)) {
                $listeLots[0] = Prado::localize('TEXT_INDIFFERENT');
                foreach ($arrayLots as $lot) {
                    if ($lot instanceof CommonCategorieLot) {
                        $listeLots[$lot->getLot()] = Prado::localize('DEFINE_LOT_NUMERO').' '.$lot->getLot().' : '.$lot->getDescription();
                    }
                }
            }
        }
        $this->listeLot->DataSource = $listeLots;
        $this->listeLot->DataBind();
    }

    /**
     * Permet de remplir la liste du filtre de message d'information.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function remplirListeMessageInformation()
    {
        $liste = [];
        $liste[0] = Prado::localize('TEXT_INDIFFERENT');
        $liste[1] = Prado::localize('TEXT_MESSAGE_NON_ENVOYE');
        $liste[2] = Prado::localize('TEXT_MESSAGE_ENVOYE_AR_NON_RECU');
        $liste[3] = Prado::localize('TEXT_MESSAGE_AR_RECU');
        $this->listeMessageInformation->DataSource = $liste;
        $this->listeMessageInformation->DataBind();
    }

    /**
     * Permet de remplir la liste du filtre de reouverture.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function remplirListeEtatReouverture()
    {
        $liste = [];
        $liste[0] = Prado::localize('TEXT_INDIFFERENT');
        $liste[1] = Prado::localize('TEXT_MESSAGE_MODIFICATION_NON_TRANSMISE_DATE_REPONSE_NON_ECHUE');
        $liste[2] = Prado::localize('TEXT_MESSAGE_MODIFICATION_NON_TRANSMISE_DATE_REPONSE_ECHUE');
        $liste[3] = Prado::localize('TEXT_MODIFICATION_TRANSMISE');
        $this->listeEtatReouverture->DataSource = $liste;
        $this->listeEtatReouverture->DataBind();
    }

    /**
     * Permet d'effectuer l'action de la recherche
     * Rempli l'objet criteriaVo.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function lancerRecherche($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $criteriaVo->setLot(null);
        $this->listeLot->SelectedValue = '0';
        if ('' != $this->nomEntreprise->Text) {
            $criteriaVo->setNomEntreprise($this->nomEntreprise->Text);
        } else {
            $criteriaVo->setNomEntreprise(null);
        }
        if ('0' != $this->listeMessageInformation->getSelectedValue()) {
            $criteriaVo->setEtatMessageInformation($this->listeMessageInformation->getSelectedValue());
        } else {
            $criteriaVo->setEtatMessageInformation('0');
        }
        if ('0' != $this->listeEtatReouverture->getSelectedValue()) {
            $criteriaVo->setEtatReouverture($this->listeEtatReouverture->getSelectedValue());
        } else {
            $criteriaVo->setEtatReouverture('0');
        }
        if ('0' != $this->filtresMotifsReouverture->getSelectedValue()) {
            $criteriaVo->setMotif($this->filtresMotifsReouverture->getSelectedValue());
        } else {
            $criteriaVo->setMotif('0');
        }
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Permet de determiner l'onglet sur lequel on se situe ("Candidature" ou "Offres").
     *
     * @return : Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') si l'on clique sur l'onglet "Candidature"
     * @return : Atexo_Config::getParameter('TYPE_ENV_OFFRE') si l'on clique sur l'onglet "Offres"
     */
    public function getOngletCliquer()
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        if ($criteriaVo->getTypeDossier() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        } elseif ($criteriaVo->getTypeDossier() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            return Atexo_Config::getParameter('TYPE_ENV_OFFRE');
        }
    }

    /**
     * Permet d'afficher le bloc la liste des lots dans l'onglet "Offres".
     *
     * @return: true si liste des lots à afficher, false sinon
     */
    public function afficherListeLots()
    {
        if ($this->getConsultation()->getAlloti() && $this->getOngletCliquer() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            return true;
        }

        return false;
    }

    /*
     * Permet d'afficher le numéro du lot dans les resultats de la recherche
     * @return: true si numéro à afficher, false sinon
     */
    public function afficherNumLot()
    {
        return $this->afficherListeLots();
    }

    /**
     * Permet de changer le numéro de lot et raffraichir le tableau des "Offres" avec le lot ainsi sélectionné.
     *
     * @param $sender
     * @param $param
     */
    public function changerLot($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        if ('0' != $this->listeLot->getSelectedValue()) {
            $criteriaVo->setLot($this->listeLot->getSelectedValue());
        }
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Permet de remplir l'objet CriteriaVo au chargement de la page.
     */
    public function remplirCriteriaVoChargementPage()
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        if (!$criteriaVo) {
            $criteriaVo = new Atexo_FormulaireSub_CriteriaVo();
        }
        $criteriaVo->setRefCons(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaVo->setOrganisme($this->getConsultation()->getOrganisme());
        $criteriaVo->setTypeReponseDossier(Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT'));
        $criteriaVo->setOrderByColumn('ID_COMPLEMENT_FORMULAIRE');
        $criteriaVo->setSensTri('DESC');
        $criteriaVo->setControlWithDateRemettre(true);
        $criteriaVo->setDateReference(date('Y-m-d H:i:s'));
        $this->setViewState('CriteriaVo', $criteriaVo);
    }

    /**
     * Permet de determiner le numéro du dossier SUB.
     *
     * @param string $idDossier: numéro du dossier coté MPE
     */
    public function getNumeroDossier($idDossier)
    {
        if ($idDossier) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $dossier = CommonTDossierFormulairePeer::retrieveByPK($idDossier, $connexion);
            if ($dossier instanceof CommonTDossierFormulaire) {
                if ($dossier->getReferenceDossierSub()) {
                    return $dossier->getReferenceDossierSub();
                }

                return '<br>-</br>';
            }
        }
    }

    /**
     * Permet de recuperer le motif de reouverture.
     *
     * @param CommonTComplementFormulaire $idComplement: objet demande de compléments
     */
    public function getMotifReouverture($complement)
    {
        if ($complement instanceof CommonTComplementFormulaire) {
            return $complement->getMotif();
        }
    }

    /**
     * Permet de determiner le numéro du lot pour un dossier donné.
     *
     * @param string $idDossier
     */
    public function getNumeroLot($idDossier)
    {
        if ($idDossier) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $dossier = CommonTDossierFormulairePeer::retrieveByPK($idDossier, $connexion);
            if ($dossier instanceof CommonTDossierFormulaire) {
                return Prado::localize('OFFRE').' - '.Prado::localize('TEXT_LOT').' '.$dossier->getIdLot();
            }

            return false;
        }
    }

    /**
     * Permet de remplir le tableau contenant les motifs de reouverture.
     */
    public function rempliMotifsReouverture()
    {
        $motifs = [];
        $motifs[0] = Prado::localize('TEXT_INDIFFERENT');
        $criteriaVo = new Atexo_FormulaireSub_CriteriaVo();
        $criteriaVo->setRefCons($this->getConsultation()->getId());
        $criteriaVo->setOrganisme($this->getConsultation()->getOrganisme());
        $criteriaVo->setTypeReponseDossier(Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT'));
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $listeComplements = (new Atexo_FormulaireSub_ComplementFormulaire())->recupererDemandesComplement($criteriaVo);
        if (is_array($listeComplements) && count($listeComplements)) {
            foreach ($listeComplements as $complements) {
                if ($complements instanceof CommonTComplementFormulaire) {
                    $motifs[$complements->getMotif()] = $complements->getMotif();
                }
            }
        }
        $this->filtresMotifsReouverture->DataSource = $motifs;
        $this->filtresMotifsReouverture->DataBind();
    }

    /**
     * Permet de modifier la date limite de reponse
     * Appelle le ws de modification de la dite date au niveau de SUB.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function modifDateLimiteReponse($sender, $param)
    {
        try {
            $dateLimite = Atexo_Util::frnDateTime2iso($this->hidenDateLimiteReponse->value);
            $idComplement = $this->hidenIdComplementDateReponse->value;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (null != $idComplement) {
                $complementFormulaire = CommonTComplementFormulairePeer::retrieveByPK($idComplement, $connexion);
                if ($complementFormulaire instanceof CommonTComplementFormulaire) {
                    $complementFormulaire->setDateARemettre($dateLimite);
                    $complementFormulaire->save($connexion);
                    //appel du ws
                    $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($complementFormulaire->getIdDossierFormulaire());
                    if ($dossier instanceof CommonTDossierFormulaire) {
                        $arrayDate = explode(' ', $dateLimite);
                        (new Atexo_FormulaireSub_Echange())->updateDateLimiteComplement($dossier->getCleExterneDossier(), Atexo_Util::frnDate2iso($arrayDate[0]), substr($arrayDate[1], 0, 5));
                    }
                    $this->response->redirect('https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                }
            }
        } catch (Exception $ex) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors de la mise a jour de la date limite de reponse : '.$ex->getMessage());
        }
    }

    /**
     * Permet de gerer la visibilite du picto "Modifier date limite de reponse".
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return bool : true si picto a afficher, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function gererVisibilitePictoModifierDateLimite($complementFormulaire)
    {
        if ($complementFormulaire instanceof CommonTComplementFormulaire && ($this->reponseDemandeComplementsEnvoyee($complementFormulaire) || $this->isDateLimiteReponseDepassee($complementFormulaire))) {
            return false;
        }

        return true;
    }

    /**
     * permet de trier la liste des Complements.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function sortRepeater($sender, $param)
    {
        $champToOrderBy = match ($sender->CommandParameter) {
            'Entreprise' => EntreprisePeer::NOM,
            'Dossier' => CommonTDossierFormulairePeer::REFERENCE_DOSSIER_SUB,
            'Lot' => CommonTDossierFormulairePeer::ID_LOT,
            'Complement' => CommonTComplementFormulairePeer::NUMERO_COMPLEMENT,
            'Message' => CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL,
            'Accuse' => CommonTComplementFormulairePeer::DATE_1ER_AR,
            'Reponse' => CommonTComplementFormulairePeer::DATE_REMIS_LE,
            'LimiteReponse' => CommonTComplementFormulairePeer::DATE_A_REMETTRE,
            default => CommonTComplementFormulairePeer::ID_COMPLEMENT_FORMULAIRE,
        };
        $sensTri = 'ASC';
        if ($this->getViewState('critereTri') == $champToOrderBy) {
            if ('ASC' == $this->getViewState('triAscDesc')) {
                $sensTri = 'DESC';
            }
        }
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setOrderByColumn($champToOrderBy);
        $criteriaVo->setSensTri($sensTri);
        $this->setViewState('critereTri', $champToOrderBy);
        $this->setViewState('triAscDesc', $sensTri);
        $this->populateData($criteriaVo);
    }
}
