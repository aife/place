<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_AssociationComptes;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpDetailAuthentification extends MpeTPage
{
    private $idAgent;
    public $organisme;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $agentConnectedO = new CommonAgent();
        $agentConnectedO->setId(Atexo_CurrentUser::getIdAgentConnected());
        $agentConnectedO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $agentConnectedO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $agentToUpO = (new Atexo_Agent())->retrieveAgent($idAgent);
        if (!(new Atexo_Agent())->verifyCapacityToModify($agentConnectedO, $agentToUpO) && !Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->panelMessage->setVisible('true');
            $this->panelMessage->setMessage(Prado::localize('TEXT_PAS_AUTORISE_MODIFIER_INFORMATIONS_AGENT'));
            $this->panelDetailsCompte->visible = false;
        } else {
            $this->idAgent = $idAgent;
            $this->panelMessage->setVisible('false');
            if (!$this->isPostBack) {
                $this->getDataAgent();
            }
        }
        $this->organisme = Atexo_CurrentUser::getOrganismAcronym();
    }

    public function getDataAgent()
    {
        $agent = (new Atexo_Agent())->retrieveAgent($this->idAgent);

        if ($agent) {
            $this->identifiantAgent->Text = $agent->getLogin();
            $this->idExterneSSO->Text = $agent->getIdExterne();
            $this->setViewState('identifiant', $agent->getLogin());
            if ('1' == $agent->getActif()) {
                $this->actif->Checked = true;
                $this->verouille->Checked = false;
            } else {
                $this->actif->Checked = false;
                $this->verouille->Checked = true;
            }
        }
    }

    public function updateAccount($sender, $param)
    {
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $agent = CommonAgentPeer::retrieveByPK($this->idAgent, $connexionCom);
            if (Atexo_Module::isEnabled('AuthenticateAgentByCert')) {
                if ($this->authentificationByCer->hasFile()) {
                    $certif = $this->authentificationByCer->getCertificat();
                    $serial = $this->authentificationByCer->getSerial();
                    $agent->setCertificat($certif);
                    $agent->setNumCertificat($serial);
                    unlink($this->authentificationByCer->getCertFileTmp());
                }
            }
            $pwd = false;
            if (Atexo_Module::isEnabled('AuthenticateAgentByLogin')) {
                $agent->setLogin(trim($this->identifiantAgent->Text));
                if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpAgent') && true == $this->pwdAutomatique->checked) {
                    $pwd = Atexo_Util::randomMdp(8);
                } else {
                    if (('' != $this->passwordAgent->Text) && ('xxxxxxx' != $this->passwordAgent->Text)) {
                        $pwd = $this->passwordAgent->Text;
                    }
                }
                if ($pwd) {
                    Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $pwd);
                    $agent->setTentativesMdp('0');
                }
            }
            $actif = false;
            if ($this->actif->checked) {
                $actif = true;
                $agent->setActif('1');
            } elseif ($this->verouille->checked) {
                $agent->setActif('0');
                (new Atexo_Agent_AssociationComptes())->deleteCompteAssocie($this->idAgent);
            }

            if ((new Atexo_Module())->isSaisieManuelleActive($this->organisme)) {
                $agent->setIdExterne($this->idExterneSSO->Text);
            }
            $agent->setDateModification(date('Y-m-d H:i:s'));
            $agent->save($connexionCom);

            $message = Atexo_Message::getMessageModificationCompte($agent, $pwd, $actif);
            Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $agent->getEmail(), Prado::localize('DEFINE_TEXT_OBJET_CREATION_MODIFICATION_MON_COMPTE'), $message, '', '', false, true);

            $this->script->Text = '<script>'.$this->authentificationByCer->getMessage()."opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }

    /**
     * Verifier que le login est unique.
     */
    public function verifyLoginModification($sender, $param)
    {
        if ($this->getViewState('identifiant') != $this->identifiantAgent->getText()) {
            $login = $this->identifiantAgent->getText();
            if ($login) {
                $agent = (new Atexo_Agent())->retrieveAgentByLogin($login);
                if ($agent) {
                    $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                    return;
                }
            }
        }
    }

    /**
     * Verifier que l'id externe est unique.
     */
    public function verifyIdExterneSsoCreation($sender, $param)
    {
        $idExterneSSO = $this->idExterneSSO->Text;
        $idAgent = null;
        if (Atexo_Util::atexoHtmlEntities($_GET['id'])) {
            $idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
        }

        if ($idExterneSSO) {
            if (strstr($idExterneSSO, ' ')) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->IdExterneSSOCreationValidator->ErrorMessage = Prado::localize('ID_EXTERNE_SSO', ['idExterneSSO' => $idExterneSSO]);

                return;
            }
            $login = $this->identifiantAgent->getText();
            $agentUpdated = (new Atexo_Agent())->retrieveAgentByLogin($login);
            $agents = (new Atexo_Agent())->retrieveAgentsByExternalIdAndOrganisme($idExterneSSO, $agentUpdated->getOrganisme(), $idAgent);
            if ((is_countable($agents) ? count($agents) : 0) > 0) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->IdExterneSSOCreationValidator->ErrorMessage = Prado::localize('ID_EXTERNE_SSO_ALREADY_EXISTS', ['idExterneSSO' => $idExterneSSO]);

                return;
            }
        }
    }

    /**
     * check if we are updating the same Agent.
     *
     * @param $agents
     */
    private function checkIdAgent($agents): bool
    {
        $isSame = false;
        $login = $this->identifiantAgent->getText();
        $agentUpdated = (new Atexo_Agent())->retrieveAgentByLogin($login);

        if (1 == (is_countable($agents) ? count($agents) : 0) && $agentUpdated) {
            foreach ($agents as $key => $agent) {
                if ($agent->getId() == $agentUpdated->getId() && $agent->getOrganisme() == $agentUpdated->getOrganisme()) {
                    $isSame = true;
                }
            }
        }

        return $isSame;
    }
}
