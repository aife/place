<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Referentiel;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChorusRejet extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idEchange'])) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $idEchange = Atexo_Util::atexoHtmlEntities($_GET['idEchange']);
            $echange = (new Atexo_Chorus_Echange())->retreiveEchangeById($organisme, $idEchange);
            if ($echange instanceof CommonChorusEchange) {
                if ($echange->getStatutEchange() == Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR')) {
                    $this->fillRepeaterMotifRejet($echange);
                } elseif ('' != $echange->getErreurRejet()) {
                    $this->errorText->Text = $echange->getErreurRejet();
                }
            }
        }
    }

    /*
     * Permet de remplir le repeater des message de rejet ainsi le repeater des tiers rejete
     */
    public function fillRepeaterMotifRejet($echange)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $active = 1;
        $motifsRejet = (new Atexo_Chorus_Echange())->retreiveTiersRejeteByIdEchange($echange->getId(), $connexion, $active);
        $dataSourceMotif = [];
        $dataSourceMessageRejet = [];
        if ($motifsRejet) {
            foreach ($motifsRejet as $motifRejet) {
                $dataSourceMotif[$motifRejet->getCodeRejet()][$motifRejet->getId()] = $motifRejet;
            }
            $value = '';
            foreach ($dataSourceMotif as $dataKey => $value) {
                $valRef = (new Atexo_Referentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_CHORUS_PROBLEME_FOURNISSEUR'), $dataKey);
                if ($valRef) {
                    $dataSourceMessageRejet[] = $valRef;
                }
            }
        }
        $this->repeaterMotifRejet->DataSource = $dataSourceMessageRejet;
        $this->repeaterMotifRejet->DataBind();
        foreach ($this->repeaterMotifRejet->getItems() as $item) {
            $codeRejet = $this->repeaterMotifRejet->DataKeys[$item->itemIndex];
            $newData = $dataSourceMotif[$codeRejet];
            $item->repeaterTierRejete->DataSource = $newData;
            $item->repeaterTierRejete->dataBind();
        }
    }
}
