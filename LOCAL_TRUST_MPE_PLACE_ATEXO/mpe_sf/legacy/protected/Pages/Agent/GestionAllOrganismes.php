<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Organisme\Atexo_Organisme_CriteriaVo;

/**
 * Classe de.
 *
 * @author loubna EZZIANI loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class GestionAllOrganismes extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin') || (!Atexo_Module::isEnabled('GestionOrganismes')) || (!Atexo_CurrentUser::hasHabilitation('GererOrganismes'))) {
            $this->response->redirect('agent');
        }

        if (!$this->isPostBack) {
            $this->searchOrganisme->fillData();
            $this->listeOrganisme->hideComponent();
            if (isset($_GET['org']) && $_GET['org']) {
                $orga = Atexo_Util::atexoHtmlEntities($_GET['org']);
                $criteriaVoOrg = new Atexo_Organisme_CriteriaVo();
                $criteriaVoOrg->setAcronyme($orga);
                $criteriaVoOrg->setActive(0);
                $ArrayOrganismes = (new Atexo_Organismes())->search($criteriaVoOrg);
                $this->listeOrganisme->repeaterDataBind($ArrayOrganismes);
            }
            $this->defautSearch();
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->defautSearch();
        $this->listeOrganisme->panelRefresh->render($param->NewWriter);
    }

    /**
     * Trier un tableau des invites selcetionnes.
     *
     * @param sender
     * @param param
     */
    public function sortOrganisme($sender, $param)
    {
        $this->listeOrganisme->sortOrganisme($sender, $param);
    }

    /**
     * remplit le tableau par le resultat de la recherche par defaut.
     */
    public function defautSearch()
    {
        $this->listeOrganisme->intialiserRepeater();
        $criteriaVoOrg = $this->searchOrganisme->fillCriteriaVoOrganisme();
        $ArrayOrganismes = (new Atexo_Organismes())->search($criteriaVoOrg);
        $this->listeOrganisme->repeaterDataBind($ArrayOrganismes);
    }
}
