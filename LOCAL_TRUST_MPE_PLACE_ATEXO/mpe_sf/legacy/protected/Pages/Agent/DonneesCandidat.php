<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;

/**
 * permet de gérer les Donnees du Candidat.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.10.0
 *
 * @copyright Atexo 2015
 */
class DonneesCandidat extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesCandidat') && isset($_GET['idEse']) && isset($_GET['idEtab'])) {
            $entrepriseQuery = new EntrepriseQuery();
            $entreprise = $entrepriseQuery->getEntrepriseById(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
            if ($entreprise instanceof Entreprise) {
                $etablissementQuery = new CommonTEtablissementQuery();
                $etablissement = $etablissementQuery->getEtabissementbyIdEseAndIdEtab(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEtab'])), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
                if ($etablissement) {
                    $this->initInfosCandidat($entreprise);
                    $attestations = $this->getDataSource(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
                    self::fillRepeaterAttestations($attestations);
                } else {
                    $this->response->redirect('agent');
                }
            } else {
                $this->response->redirect('agent');
            }
        } else {
            $this->response->redirect('agent');
        }
    }

    /**
     * permet d'afficher la liste des attestaions disponible(remplire le repeater).
     *
     * @param array $attestations liste des attestaions
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function fillRepeaterAttestations($attestations)
    {
        $this->repeaterAttestations->DataSource = $attestations;
        $this->repeaterAttestations->DataBind();
    }

    /**
     * permet de telecharger la derniere version d'une attestation.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function downloadLastVersion($sender, $param)
    {
        $attr = explode('#', $param->CommandParameter);
        if ($attr[0]) {
            $docVesrionQuery = new CommonTEntrepriseDocumentVersionQuery();
            $docVersion = $docVesrionQuery->getDocVersionByIdAndIdDoc($attr[0], $attr[1]);
            if ($docVersion instanceof CommonTEntrepriseDocumentVersion) {
                $atexoBlob = new Atexo_Blob();
                $content = $atexoBlob->return_blob_to_client($docVersion->getIdBlob(), Atexo_Config::getParameter('COMMON_BLOB'));
                $fileName = $attr[2].'_'.date('Y_m_d_H_i_s').'.'.$docVersion->getExtensionDocument();
                DownloadFile::downloadFileContent($fileName, $content);
            }
        }
    }

    /**
     * permet d'initialiser le composant.
     *
     * @param Entreprise $entreprise
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function initInfosCandidat($entreprise)
    {
        $entrepriseVo = [];
        if ($entreprise instanceof Entreprise) {
            $entrepriseVo = (new Atexo_Entreprise())->getEntrepriseVoFromEntreprise($entreprise);
            $entrepriseVo->setMandataires($entreprise->getCommonResponsableengagements());
            $etabQuery = new CommonTEtablissementQuery();
            $etablissement = $etabQuery->getEtabissementbyIdEseAndIdEtab(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEtab'])), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
            if ($etablissement instanceof CommonTEtablissement) {
                $entrepriseVo->setEtablissements($etablissement->getEtablissementVo());
            }
        }
        $this->infosCandidat->setCandidat($entrepriseVo);
        $this->infosCandidat->initComposant();
    }

    /**
     * permet de de recuperer la liste des attestation a aficher (datasource).
     *
     * @param int $idEntreprise
     *
     * @return array liste des attestaion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getDataSource($idEntreprise)
    {
        return (new Atexo_Entreprise_Attestation())->getAttestaionsList($idEntreprise);
    }

    /**
     * permet de de recuperer la liste des attestation a aficher (datasource).
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function sortDataSource($sender, $param)
    {
        $this->setOdreTri();
        $attestationsTriees = Atexo_Util::trierTableauxMultiDimensionnels($this->getDataSource(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse']))), $param->CallbackParameter, $this->page->getViewState('ordreTri'));
        $this->fillRepeaterAttestations($attestationsTriees);
        $this->panelAttestations->render($param->getNewWriter());
    }

    /**
     * permet d enregistrer l'ordre du tri.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    private function setOdreTri()
    {
        if ('DESC' == $this->page->getViewState('ordreTri')) {
            $this->page->setViewState('ordreTri', 'ASC');
        } else {
            $this->page->setViewState('ordreTri', 'DESC');
        }
    }

    /**
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function afficherHistoriqueAttestation($sender, $param)
    {
        $attributs = explode('#', $param->CommandParameter);
        if (is_array($attributs)) {
            $idDocument = $attributs[0];
            $this->atexoHistoriquesDocument->setIdEntreprise(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
            $this->atexoHistoriquesDocument->setIdTypeDocument($attributs[1]);
            $this->atexoHistoriquesDocument->setRefConsultation($this->getReferenceConsultation());
            $this->atexoHistoriquesDocument->setOrganisme($this->getOrganismeConsultation());
            $dataSource = (array) (new Atexo_Entreprise_Attestation())->retrieveDocumentVersion($idDocument);
            $this->atexoHistoriquesDocument->initComposant($dataSource);
            $this->javascriptLabel->Text = "<script>openModal('modal-historique','modal-form popup-small popup-800',document.getElementById('".$this->titrePopin->getClientId()."'));</script>";
        }
    }

    public function refreshAttestations($sender, $param)
    {
        $attestations = $this->getDataSource(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idEse'])));
        self::fillRepeaterAttestations($attestations);
        $this->panelAttestations->render($param->getNewWriter());
    }

    /**
     * permet de recuperer l'url d'acces a la page des donnees du candidat.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlRetour()
    {
        $lienRetour = null;
        if ('ouverture' == $_GET['calledFrom']) {
            $lienRetour = 'index.php?page=Agent.ouvertureEtAnalyse&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        } elseif ('decision' == $_GET['calledFrom']) {
            $lienRetour = 'index.php?page=Agent.DecisionAttribution&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])).'&lot='.Atexo_Util::atexoHtmlEntities($_GET['lot']);
        }

        return $lienRetour;
    }

    /**
     * @return string
     */
    public function getReferenceConsultation()
    {
        return Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
    }

    public function getOrganismeConsultation(): false|string
    {
        return Atexo_CurrentUser::getCurrentOrganism();
    }
}
