<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_RegistreVo;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownLoadQuestion extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idQuestion = Atexo_Util::atexoHtmlEntities($_GET['idQuestion']);
        $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);

        if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier') ||
          Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier') ||
          Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsElectronique') ||
          Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsPapier')) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference($idReference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $registre = (new Atexo_Consultation_Register())->retrieveQuestions($idQuestion, [], $typeRegistre, Atexo_CurrentUser::getCurrentOrganism(), false);
                if (is_array($registre) && count($registre) > 0 && $registre[0] instanceof Atexo_Consultation_RegistreVo) {
                    $registre = array_shift($registre);
                    $arrayFichier = $registre->getFichiers();
                    $fichier = array_shift($arrayFichier);
                    $idFichier = $fichier['idFichier'];
                    $nomFichier = $fichier['nomFichier'];
                    $this->downloadFiles($idFichier, $nomFichier, Atexo_CurrentUser::getCurrentOrganism());
                } else {
                    echo 'errero 3';
                }
            } else {
                echo 'errero 2';
            }
        } else {
            echo 'errero 1';
        }
    }
}
