<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTCalendrierEtape;
use Application\Propel\Mpe\CommonTCalendrierEtapeReferentiel;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GestionCalendrier;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe responsable du chargement du calendrier GWT
 * en retournant un json correpondant au calendrier  en BD.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 */
class ChargementCalendrier extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
    }

    public function onLoad($param)
    {
        if ($_GET['id']) {
            $calendier = (new Atexo_GestionCalendrier())->retrieveCalendierByrefConsultationOrg(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());

            if ($calendier) {
                // recupere le calendrier : generation du json correspondant
                $this->chargerCalenderConsultaion($calendier);
            } else {    // creation d'un calendrier a partir du referenctiel : generation du json correspondant
                $this->generateCalendrerfromRefentiel();
            }
        } elseif ($_GET['typeProcedure']) {
            $this->chargerCalenderReferentiel(Atexo_Util::atexoHtmlEntities($_GET['typeProcedure']), Atexo_Util::atexoHtmlEntities($_GET['org']));
        }
    }

    /****
     * generation du Json du calendrier  si la consultation a deja un calendrier
     * @param  objetCalendrier
     */
    public function chargerCalenderConsultaion($calendier)
    {
        $objetEtape = [];
        $arrayEtapes = [];
        $listeEtapes = (new Atexo_GestionCalendrier())->retrieveCalendrierEtapeByIdCal($calendier->getIdCalendrier());
        foreach ($listeEtapes as $etape) {
            $objetEtape = $this->getEtapeArrayJson($etape);
            if ($trSortantes = $this->getTransitionByIdEtape($etape, false, true)) {
                $objetEtape['trSortantes'] = $trSortantes;
            }
            if ($trEntrantes = $this->getTransitionByIdEtape($etape, true, false)) {
                $objetEtape['trEntrantes'] = $trEntrantes;
            }
            $arrayEtapes[] = $objetEtape;
        }
        self::generateJsonCalender($calendier->getIdCalendrier(), $arrayEtapes);
    }

    /****
     * Creation du calendrier et generation du Json
     * du calendrier a partir du referentiel
     */
    public function generateCalendrerfromRefentiel()
    {
        $idcalender = null;
        $arrayEtapes = [];
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
        if ($consultation) {
            //$arrayEtape = Atexo_GestionCalendrier::retrieveEtapeRefrentielOrg($consultation->getIdTypeProcedure() ,Atexo_CurrentUser::getCurrentOrganism());
            $arrayEtape = (new Atexo_GestionCalendrier())->createCalendrierFomCalendrierRef($consultation->getIdTypeProcedureOrg(), Atexo_CurrentUser::getCurrentOrganism(), $consultation->getId());

            $objetEtape = [];
            //$idcalender = '0';
            foreach ($arrayEtape as $etape) {
                $idcalender = $etape->getIdCalendrier();
                $objetEtape = $this->getEtapeArrayJson($etape);
                if ($trSortantes = $this->getTransitionByIdEtape($etape, false, true)) {
                    $objetEtape['trSortantes'] = $trSortantes;
                }
                if ($trEntrantes = $this->getTransitionByIdEtape($etape, true, false)) {
                    $objetEtape['trEntrantes'] = $trEntrantes;
                }
                $arrayEtapes[] = $objetEtape;
            }
            self::generateJsonCalender($idcalender, $arrayEtapes);
        }
    }

    /****
    * generation du contenu json de la transaction d'une etape
    * dans un array
    * @param  objet etape , boolean cible ou source
    * @return array de donnee Json de la transition
    */
    public function getTransitionByIdEtape($etapeObjet, $idEtapeCible = false, $idEtapeSource = false)
    {
        $results = [];
        $arrayresults = [];
        $idEtape = $etapeObjet->getIdCalendrierEtape();
        if ($idEtapeSource) {
            $arratTransition = (new Atexo_GestionCalendrier())->retrieveTransationByIdEtapeSourceOrCible(false, $idEtape);
        } elseif ($idEtapeCible) {
            $arratTransition = (new Atexo_GestionCalendrier())->retrieveTransationByIdEtapeSourceOrCible($idEtape, false);
        }
        foreach ($arratTransition as $transition) {
            $results['id'] = (string) $transition->getIdCalendrierTransition();
            $results['valeurFixeInit'] = (string) $transition->getValeurFixe();
            $results['valeurVariableInit'] = (string) $transition->getValeurVariableInitiale();
            $results['valeurFixe'] = (string) $transition->getValeurFixe();
            $results['valeurVariable'] = (string) $transition->getValeurVariableInitiale();
            $etapeSource = (new Atexo_GestionCalendrier())->retrieveEtapeById($transition->getIdEtapeSource());
            $etapeCible = (new Atexo_GestionCalendrier())->retrieveEtapeById($transition->getIdEtapeCible());
            $results['source'] = $this->getEtapeArrayJson($etapeSource);
            $results['cible'] = $this->getEtapeArrayJson($etapeCible);
            $arrayresults[] = $results;
        }

        return $arrayresults;
    }

    // Fonction a supprime
    public function getTransitionReferentielByIdEtapeRef($etapeObjet, $idEtapeCible, $idEtapeSource, $idTypeProcedure, $org)
    {
        $results = [];
        $arrayresults = [];
        $idEtape = $etapeObjet->getIdEtapeCalendrierReferentiel();
        if ($idEtapeSource) {
            $arratTransition = (new Atexo_GestionCalendrier())->retrieveTransationReferentielByIdEtapeSourceOrCible(false, $idEtape, $idTypeProcedure, $org);
        } elseif ($idEtapeCible) {
            $arratTransition = (new Atexo_GestionCalendrier())->retrieveTransationReferentielByIdEtapeSourceOrCible($idEtape, false, $idTypeProcedure, $org);
        }
        foreach ($arratTransition as $transition) {
            $results['id'] = (string) $transition->getIdTransitionCalendrierReferentiel();
            $results['valeurFixeInit'] = (string) $transition->getValeurFixe();
            $results['valeurVariableInit'] = (string) $transition->getValeurVariable();
            $results['valeurFixe'] = (string) $transition->getValeurFixe();
            $results['valeurVariable'] = (string) $transition->getValeurVariable();
            $etapeSource = (new Atexo_GestionCalendrier())->retrieveEtapeRefById($transition->getIdCalendrierEtapeReferentielSource(), $idTypeProcedure, $org);
            $etapeCible = (new Atexo_GestionCalendrier())->retrieveEtapeRefById($transition->getIdCalendrierEtapeReferentielCible(), $idTypeProcedure, $org);
            $results['source'] = $this->getEtapeRefArrayJson($etapeSource);
            if ($this->getEtapeRefArrayJson($etapeCible)) {
                $results['cible'] = $this->getEtapeRefArrayJson($etapeCible);
            }
            $arrayresults[] = $results;
        }

        return $arrayresults;
    }

    /****
    * fonction de generation du flux json
    * @param  id calender ;array etapes
    */
    public function generateJsonCalender($idCalender, $arrayEtape)
    {
        $arrayValue = ['id' => (string) $idCalender,
                            'habilitationModificationDateReelles' => false,
                            'etapes' => $arrayEtape, //
                            //'phasePrev' => false,  //
                            'sensDirect' => true, //trjs a true dans MPE
                            'accesLectureSeule' => false,
                            'accesModification' => true,
                           ];

        echo json_encode($arrayValue, JSON_THROW_ON_ERROR);
        exit;
    }

    /****
    * fonction de generation du flux json d'une etape
    * @param  etape
    * array de donnee de l'etape referenriel
    */
    public function getEtapeRefArrayJson($etape)
    {
        $objetEtape = [];
        if ($etape  instanceof CommonTCalendrierEtapeReferentiel) {
            $objetEtape['id'] = (string) $etape->getIdEtapeCalendrierReferentiel();
            $objetEtape['code'] = (string) $etape->getCode();
            $objetEtape['libelle'] = (string) Atexo_Util::toUtf8($etape->getLibelle());
            $objetEtape['reelNonSaisissable'] = true; // verifier par Regis
            $objetEtape['initial'] = false;
            // Atexo_GestionCalendrier::isInitial($etape->getIdEtapeCalendrierReferentiel(),true);
            $objetEtape['dateInitiale'] = '';
            $objetEtape['datePrevue'] = '';
            $objetEtape['type'] = '0';
            $objetEtape['actif'] = '0';
            $objetEtape['regleCalcul'] = '0';
            $objetEtape['dateHeritable'] = false;
            $objetEtape['libre'] = false;
        }

        return $objetEtape;
    }

    /****
    * fonction de generation du flux json d'une etape
    * @param  etape
    * array de donnee de l'etape
    */
    public function getEtapeArrayJson($etape)
    {
        $objetEtape = [];
        $dareReelconfirme = false;
        if ($etape instanceof CommonTCalendrierEtape) {
            $objetEtape['id'] = (string) $etape->getIdCalendrierEtape();
            $objetEtape['code'] = (string) $etape->getCode();
            $objetEtape['reelNonSaisissable'] = true; // a verifier par Regis
            $objetEtape['libelle'] = (string) Atexo_Util::toUtf8($etape->getLibelle());
            $objetEtape['dateInitiale'] = (string) (Atexo_Util::iso2frnDate($etape->getDateInitiale()));
            $objetEtape['datePrevue'] = (string) (Atexo_Util::iso2frnDate($etape->getDatePrevue()));
            $objetEtape['initial'] = (new Atexo_GestionCalendrier())->isInitial($etape->getIdCalendrierEtape());
            $objetEtape['type'] = '0';
            $objetEtape['actif'] = '0';
            $objetEtape['regleCalcul'] = '0';
            $objetEtape['dateHeritable'] = false;
            if ($etape->getDateReelleConfirmee()) {
                $dareReelconfirme = true;
            }
            $objetEtape['dateReelleConfirmee'] = $dareReelconfirme;
            $objetEtape['libre'] = (bool) $etape->getLibre();
        }

        return $objetEtape;
    }

    /****
     * generation du Json du calendrier
     * @param  objetCalendrier
     */
    public function chargerCalenderReferentiel($idTypeProcedure, $org)
    {
        $objetEtape = [];
        $arrayEtapes = [];
        $listeEtapes = (new Atexo_GestionCalendrier())->fillArrayEtapeRefentiel($idTypeProcedure, $org);
        foreach ($listeEtapes as $etape) {
            $objetEtape = $this->getEtapeRefArrayJson($etape);

            if ($trSortantes = $this->getTransitionReferentielByIdEtapeRef($etape, false, true, $idTypeProcedure, $org)) {
                $objetEtape['trSortantes'] = $trSortantes;
            }
            if ($trEntrantes = $this->getTransitionReferentielByIdEtapeRef($etape, true, false, $idTypeProcedure, $org)) {
                $objetEtape['trEntrantes'] = $trEntrantes;
            }
            $arrayEtapes[] = $objetEtape;
        }

        self::generateJsonCalender(0, $arrayEtapes);
    }
}
