<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupChoixTypeAvis extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $Consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            if ($Consultation) {
                $arrayType = (new Atexo_Publicite_AvisPub())->getListeAvis($Consultation->getIdTypeProcedureOrg(), Atexo_CurrentUser::getOrganismAcronym());
                $this->modelForm->DataSource = $arrayType;
                $this->modelForm->dataBind();
            }
        }
    }

    /**
     * Action sur le boutton valider.
     */
    public function doValider($sender, $param)
    {
        $erreur = (new Atexo_Publicite_AvisPub())->createAvisPub(Atexo_CurrentUser::getOrganismAcronym(), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdAgentConnected(), $this->modelForm->selectedValue);

        if ($erreur) {
            $this->panelBlocErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('TEXT_ERREUR_CREATION_PUB_SUB'));
        } else {
            $this->scriptClose->Text = '<script>refreshRepeater();</script>';
        }
    }
}
