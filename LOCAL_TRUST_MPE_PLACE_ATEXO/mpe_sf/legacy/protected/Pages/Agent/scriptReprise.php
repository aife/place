<?php

namespace Application\Pages\Agent;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonGeolocalisationN2Peer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use PDO;

/**
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class scriptReprise
{
    public static function runScriptReprise()
    {
        self::addCodeinGeoN2();
        self::intialiserDateCreationModification();
        self::intialiserDateCreationModificationServiceAndRPA();
        self::intialiserDatesAgent();
        echo "\nMise à jour terminée\n";
    }

    /**
     * Ajoute le code du department dans la colone denomination2 dans la table Geo N2.
     */
    public function addCodeinGeoN2()
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $geoN2 = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        foreach ($geoN2 as $oneN2) {
            $denomination = explode(')', $oneN2->getDenomination1());
            if (2 == count($denomination)) {
                $oneN2->setDenomination2(str_replace('(', '', $denomination[0]));
                $oneN2->save($connexionCom);
            }
        }
    }

    //---------------------------------------------------

    public function getListeOragnismes($organisme)
    {
        $ListeOragnismes = [];
        $sql = 'SELECT id,acronyme,denomination_org  from Organisme ';
        if ($organisme) {
            $sql .= " WHERE acronyme = '".(new Atexo_Db())->quote($organisme)."' ";
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $i = 0;
        foreach ($statement as $row) {
            $ListeOragnismes[$i]['id'] = $row['id'];
            $ListeOragnismes[$i]['acronyme'] = $row['acronyme'];
            $ListeOragnismes[$i]['denomination_org'] = $row['denomination_org'];
            ++$i;
        }

        return $ListeOragnismes;
    }

    /*
     * fonction permet de initialise la date de creation et la date de modification des 3 tables Agent,Inscrit,Entreprise de la base commun
     */
    public function intialiserDateCreationModification()
    {
        $sql = " UPDATE Inscrit SET date_creation='2009-01-01 00:00:00',date_modification='2009-01-01 00:00:00';";
        $statement = Atexo_Db::getLinkCommon(false)->query($sql, PDO::FETCH_ASSOC);
        $sql = " UPDATE Entreprise SET date_creation='2009-01-01 00:00:00',date_modification='2009-01-01 00:00:00',";
        $sql .= " annee_cloture_exercice1 = '2006',annee_cloture_exercice2='2007',annee_cloture_exercice3='2008';";
        $statement = Atexo_Db::getLinkCommon(false)->query($sql, PDO::FETCH_ASSOC);
    }

    /*
    * fonction permet de initialise la date de creation et la date de modification  de la table service de la base de tt les bases organismes
    */
    public function intialiserDateCreationModificationServiceAndRPA()
    {
        $entitys = (new Atexo_Organismes())->retrieveAllEntity();
        foreach ($entitys as $oneEntity) {
            $entityAcronyme = $oneEntity->getAcronyme();
            $sql = " UPDATE Service SET date_creation='2009-01-01 00:00:00',date_modification='2009-01-01 00:00:00';";
            $statement = Atexo_Db::getLinkOrganism($entityAcronyme, false)->query($sql, PDO::FETCH_ASSOC);
            $sql = " UPDATE RPA SET date_creation='2009-01-01 00:00:00',date_modification='2009-01-01 00:00:00';";
            $statement = Atexo_Db::getLinkOrganism($entityAcronyme, false)->query($sql, PDO::FETCH_ASSOC);
        }
    }

    public function intialiserDatesAgent()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agents = CommonAgentPeer::doSelect(new Criteria(), $connexionCom);
        foreach ($agents as $oneAgent) {
            if ('0000-00-00 00:00:00' == $oneAgent->getDateCreation() || '' == $oneAgent->getDateCreation()) {
                $oneAgent->setDateCreation('2009-01-01 00:00:00');
            }
            if ('0000-00-00 00:00:00' == $oneAgent->getDateModification() || '' == $oneAgent->getDateModification()) {
                $oneAgent->setDateModification('2009-01-01 00:00:00');
            }
            $oneAgent->save($connexionCom);
        }
    }
}
