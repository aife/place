<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonCertificatAgent;
use Application\Propel\Mpe\CommonCertificatAgentPeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionCertificatsAgents extends MpeTPage
{
    private $_selectedEntity;
    public $critereTri;
    public $triAscDesc;
    public $organisme;
    public $nbrElementRepeater;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->ChoixOrganisme->labelTitileAdministration->setVisible(false);
        $this->ChoixOrganisme->labelTitileRecherche->setVisible(true);
        // Gestion de tous les agents
        $this->ChoixOrganisme->visible = true;
        $this->ChoixEntiteAchat->visible = false;
        $this->hideComponent();

        $this->RepeaterAgent->DataSource = [];
        $this->RepeaterAgent->DataBind();

        if (!$this->isPostBack) {
            $this->setViewState('idService', Atexo_CurrentUser::getIdServiceAgentConnected());
            if ((isset($_GET['idService']) && '' != $_GET['idService']) && (isset($_GET['acronyme']) && '' != $_GET['acronyme'])) {
                $this->_selectedEntity = Atexo_Util::atexoHtmlEntities($_GET['idService']);
                $this->ChoixOrganisme->organisme->SelectedValue = Atexo_Util::atexoHtmlEntities($_GET['acronyme']);
                $listEntityPurchase = Atexo_EntityPurchase::getEntityPurchase(Atexo_Util::atexoHtmlEntities($_GET['acronyme']), true);
                $this->ChoixOrganisme->listeEntites->dataSource = $listEntityPurchase;
                $this->ChoixOrganisme->listeEntites->DataBind();
                $this->ChoixOrganisme->listeEntites->SelectedValue = Atexo_Util::atexoHtmlEntities($_GET['idService']);
                $this->setViewState('idService', $this->_selectedEntity);
                $this->setViewState('listeAgents', $this->getDataAgents(Atexo_Util::atexoHtmlEntities($_GET['acronyme'])));
                self::repeaterDataBind(Atexo_Util::atexoHtmlEntities($_GET['acronyme']));
                // $this->RepeaterAgent->DataSource = $this->getViewState("listeAgents");
                // $this->RepeaterAgent->DataBind();
            }
        }
    }

    public function repeaterDataBind($organisme = false)
    {
        $nombreElement = $this->getDataAgents($organisme, true);
        $this->nbrElementRepeater = $nombreElement;
        $this->setViewState('nbrElements', $nombreElement);

        if ($nombreElement) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
            $this->RepeaterAgent->setVirtualItemCount($nombreElement);
            self::showComponent();
            self::populateData($organisme);
        } else {
            self::hideComponent();
        }
    }

    public function populateData($organisme = false)
    {
        $offset = $this->RepeaterAgent->CurrentPageIndex * $this->RepeaterAgent->PageSize;
        $limit = $this->RepeaterAgent->PageSize;
        if ($offset + $limit > $this->RepeaterAgent->getVirtualItemCount()) {
            $limit = $this->RepeaterAgent->getVirtualItemCount() - $offset;
        }

        $dataAgent = $this->getDataAgents($organisme, false, $limit, $offset);

        $this->setViewState('listeAgents', $dataAgent);
        if (count($dataAgent)) {
            $this->showComponent();
            $this->RepeaterAgent->DataSource = $dataAgent;
            $this->RepeaterAgent->DataBind();
        } else {
            $this->hideComponent();
        }
    }

    /**
     * retourne les données de l'agent.
     *
     * @return array objets agents
     */
    public function getDataAgents($organisme = false, $forNombreResultats = false, $limit = 0, $offset = 0)
    {
        if (!$organisme) {
            $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        } else {
            $this->_selectedEntity = $this->ChoixOrganisme->getSelectedEntityPurchase();
        }
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::SERVICE_ID, $this->_selectedEntity, Criteria::EQUAL);
        $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        $agents = CommonAgentPeer::doSelect($c, $connexionCom);
        //  print_r($agents);exit;
        foreach ($agents as $agent) {
            $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($agent->getServiceId(), $agent->getOrganisme());
            $agent->setEntityPath($entityPath);
            $certif = (new Atexo_Agent())->retrieveCertificatAgentById($agent->getId());
            if ($certif) {
                $agent->setIdCertificat($certif->getId());
                if ('' == $certif->getId()) {
                    $agent->setStatutCertificat(Prado::localize('NON_ATTRIBUE'));
                    $agent->setActionCreer(true);
                    $agent->setActionRevoquer(false);
                } elseif ($certif->getStatutRevoque() == Atexo_Config::getParameter('STATUT_CERTIFICAT_REVOQUEE')) {
                    $agent->setActionCreer(true);
                    $agent->setActionRevoquer(false);
                    $agent->setStatutCertificat(Prado::localize('REVOQUE_LE').' '.Atexo_Util::iso2frnDateTime($certif->getDateRevoquation()));
                } elseif ($certif->getDateFin() < date('Y-m-d H:i:s')) {
                    $agent->setActionCreer(true);
                    $agent->setActionRevoquer(false);
                    $agent->setStatutCertificat(Prado::localize('EXPIRE_LE').' '.Atexo_Util::iso2frnDateTime($certif->getDateFin()));
                } elseif ($certif->getDateFin() > date('Y-m-d H:i:s')) {
                    $agent->setActionCreer(false);
                    $agent->setActionRevoquer(true);
                    $agent->setStatutCertificat(Prado::localize('VALABLE_JUSQU_AU').' '.Atexo_Util::iso2frnDateTime($certif->getDateFin()));
                }
            } else {
                $agent->setStatutCertificat(Prado::localize('NON_ATTRIBUE'));
                $agent->setActionCreer(true);
                $agent->setActionRevoquer(false);
            }
        }
        //print_r($agents);exit;
        if (!$forNombreResultats) {
            if ($agents) {
                return $agents;
            } else {
                return [];
            }
        } else {
            return count($agents);
        }
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->setViewState('listeAgents', $this->getDataAgents());
        /* if($this->getViewState("listeAgents")) {
            self::showComponent();
         } else {
            self::hideComponent();
         }*/
        self::repeaterDataBind();
        $this->RepeaterAgent->DataSource = $this->getViewState('listeAgents');
        $this->RepeaterAgent->DataBind();
        // Rafraichissement du ActivePanel
        $this->panelRefresh->render($param->NewWriter);
    }

    public function onClickGlobalSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixOrganisme->getSelectedEntityPurchase();
        $this->organisme = $this->ChoixOrganisme->organisme->getSelectedValue();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->setViewState('listeAgents', $this->getDataAgents($this->organisme));
        if (count($this->getDataAgents($this->organisme))) {
            self::showComponent();
        } else {
            self::hideComponent();
        }
        self::repeaterDataBind($this->organisme);
        $this->RepeaterAgent->DataSource = $this->getDataAgents($this->organisme);
        $this->RepeaterAgent->DataBind();
        // Rafraichissement du ActivePanel
        $this->panelRefresh->render($param->NewWriter);
    }

    public function refreshRepeater($sender, $param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->repeaterDataBind();
        } else {
            $this->organisme = $this->ChoixOrganisme->organisme->getSelectedValue();
            $dataSource = $this->getDataAgents($this->organisme);
            $this->setNbrElementRepeater(count($dataSource));
            if (!count($dataSource)) {
                $this->hideComponent();
            } else {
                $this->showComponent();
            }
            $this->RepeaterAgent->DataSource = $dataSource;
            $this->RepeaterAgent->DataBind();
        }

        $this->panelRefresh->render($param->getNewWriter());
    }

    /**
     * Trier un tableau des invités sélcetionnés.
     *
     * @param sender
     * @param param
     */
    public function sortAgents($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $agents = $this->getViewState('listeAgents');
        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'Nom', '');
        } elseif ('Prenom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'Nom', '');
        } elseif ('ServiceId' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($agents, 'ServiceId', '');
        } else {
            return;
        }
        $this->setViewState('listeAgentsTriee', $dataSourceTriee[0]);
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);
        //Remplissage du tabeau des invités
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        $this->RepeaterAgent->setCurrentPageIndex(0);
        $this->RepeaterAgent->DataSource = $dataSourceTriee[0];
        $this->RepeaterAgent->DataBind();

        // Re-affichage du TActivePanel
        $this->panelRefresh->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function supprimerAgent($sender, $param)
    {
        $id = $param->CommandParameter;
        (new Atexo_Agent())->deleteAgent($id, Atexo_CurrentUser::getIdAgentConnected());
        self::repeaterDataBind();
    }

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterAgent->CurrentPageIndex = $param->NewPageIndex;
        $this->pageNumberBottom->Text = $param->NewPageIndex + 1;
        $this->pageNumberTop->Text = $param->NewPageIndex + 1;
        // self::afficherTableauResultat();
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        $this->populateData();
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }

        $this->RepeaterAgent->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nbrElements');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);

        //$critere=$this->Page->getViewState('critere');
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        $this->RepeaterAgent->setCurrentPageIndex(0);
        // self::afficherTableauResultat();
        $this->populateData();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterAgent->CurrentPageIndex = $numPage - 1;
            $this->pageNumberBottom->Text = $numPage;
            $this->pageNumberTop->Text = $numPage;
            //$criteriaVo=$this->Page->getViewState("CriteriaVo");
            $this->setNbrElementRepeater($this->getViewState('nbrElements'));
            $this->populateData();
        } else {
            $this->pageNumberTop->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
        }
    }

    public function showComponent()
    {
        $this->Page->PagerTop->visible = true;
        $this->Page->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    /*
    * Permet de dupliquer un agent
    */
    public function dupliquerAgent($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idAgentADupliquer = $param->CommandParameter;

        //Récupération de l'agent à dupliquer
        $agentADupliquer = (new Atexo_Agent())->retrieveAgent($idAgentADupliquer);

        if ($agentADupliquer instanceof CommonAgent) {
            //Recuperation de l'habilitation de l'agent à dupliquer
            $habilitationAgentADupliquer = $agentADupliquer->getCommonHabilitationAgents(new Criteria(), $connexionCom);
            //Recuperation des services metiers de l'agent à dupliquer
            $serviceMetierAgentADupliquer = (new Atexo_Socle_AgentServicesMetiers())->retreiveListeServicePourUnAgent($idAgentADupliquer);
            //Creation du nouvel agent et son habilitation
            $agentDuplique = new CommonAgent();
            $agentDuplique = $agentADupliquer->copy();
            $agentDuplique->setActif(0);
            $mdp = Atexo_Util::random(10);
            $agentDuplique->setPassword(sha1($mdp));
            $agentDuplique->setLogin($agentADupliquer->getLogin().'_copie_'.$mdp);

            //Sauvegarde de l'agent dupliqué et de son habilitation
            $agentDuplique->save($connexionCom);
            $agentDuplique->setNom($agentADupliquer->getNom().'_copie_'.$agentDuplique->getId());
            $agentDuplique->setPrenom($agentADupliquer->getPrenom().'_copie_'.$agentDuplique->getId());
            $agentDuplique->save($connexionCom);

            if ($habilitationAgentADupliquer[0] instanceof CommonHabilitationAgent) {
                $habilitationAgentDuplique = new CommonHabilitationAgent();
                $habilitationAgentDuplique = $habilitationAgentADupliquer[0]->copy();
                $habilitationAgentDuplique->setIdAgent($agentDuplique->getId());
                $habilitationAgentDuplique->save($connexionCom);
            }

            //Copie des services metiers de l'agent à dupliquer et leurs sauvegardes
            if ($serviceMetierAgentADupliquer) {
                foreach ($serviceMetierAgentADupliquer as $serviceMetier) {
                    $serviceMetierAgent = new CommonAgentServiceMetier();
                    //$serviceMetierAgent = $serviceMetier->copy();
                    $serviceMetierAgent->setIdAgent($agentDuplique->getId());
                    $serviceMetierAgent->setIdServiceMetier($serviceMetier->getIdServiceMetier());
                    $serviceMetierAgent->save($connexionCom);
                }
            }

            self::repeaterDataBind();
        }
    }

    // revoquation du certificat
    public function revokeCertificat($sender, $param)
    {
        $opensslConfigFile = Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_CONFIG');
        if (true or !is_file($opensslConfigFile)) {
            if (!Atexo_Crypto::generateCompanyPkiOpensslConfigFile()) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 1');
            }
        }
        $opensslDbFile = Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_DB');
        if (!is_file($opensslDbFile)) {
            if (!Atexo_Crypto::generateCompanyPkiOpensslDbFile()) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 2');
            }
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idCertif = $param->CommandParameter;
        $certificat = CommonCertificatAgentPeer::retrieveByPK($idCertif, $connexionCom);
        if ($certificat instanceof CommonCertificatAgent) {
            $connexionCom->beginTransaction();

            // Revocation en base
            $certificat->setStatutRevoque(Atexo_Config::getParameter('STATUT_CERTIFICAT_REVOQUEE'));
            $certificat->setDateRevoquation(date('Y-m-d H:i:s'));
            $certificat->save($connexionCom);

            // revocation openssl
            // creation de la requete OPENSSL ca -revoke
            $tmpRevFilename = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'_rev.pem';
            $rev_fp = fopen($tmpRevFilename, 'w');
            if (!fwrite($rev_fp, $certificat->getCertificat())) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_INTERNE_ECRITURE_REQUETE'));
            }
            fclose($rev_fp);
            $revoke = Atexo_Config::getParameter('OPENSSL').' ca -config '.$opensslConfigFile.' -revoke  '.$tmpRevFilename;
            $commande = $revoke.' &> '.Atexo_Config::getParameter('COMMON_TMP').'revoke_cert';
            system($commande, $res);
            if (0 != $res && 1 != $res) { // $res=1 => ALREADY REVOKED
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(prado::localize('ERREUR_LORS_DE_REVOQUATION').' 3');
                $connexionCom->rollback();
            }
            //@unlink($tmpRevFilename);

            $genCrl = Atexo_Config::getParameter('OPENSSL').' ca -gencrl -config '.$opensslConfigFile.' -out '.Atexo_Config::getParameter('PKI_ENTREPRISE_CRL');
            system($genCrl.' &> '.Atexo_Config::getParameter('COMMON_TMP').'generate_crl', $res);
            if (0 != $res) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_LORS_DE_GENERATION_DU_CRL'));
                $connexionCom->rollback();
            }
        }
        $connexionCom->commit();
        $this->repeaterDataBind($this->ChoixOrganisme->organisme->getSelectedValue());
    }

    public function onBackGenerateCert($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->setViewState('listeAgents', $this->getDataAgents());
        /* if($this->getViewState("listeAgents")) {
            self::showComponent();
         } else {
            self::hideComponent();
         }*/
        self::repeaterDataBind($this->organisme);
        $this->RepeaterAgent->DataSource = $this->getViewState('listeAgents');
        $this->RepeaterAgent->DataBind();
        // Rafraichissement du ActivePanel
        $this->panelRefresh->render($param->NewWriter);
    }
}
