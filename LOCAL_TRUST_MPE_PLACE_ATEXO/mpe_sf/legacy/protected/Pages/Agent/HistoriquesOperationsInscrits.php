<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-4.0
 */
class HistoriquesOperationsInscrits extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $consultation = (new Atexo_Consultation())->retrieveConsultation($idReference, $organisme);
        if ($consultation instanceof CommonConsultation) {
            $this->ConsultationSummary->setConsultation($consultation);
            $this->consultationReplyTerms->setConsultation($consultation);
            if (!$this->IsPostBack) {
                if (Atexo_Module::isEnabled('historiqueNavigationInscrits') && Atexo_CurrentUser::hasHabilitation('HistoriqueNavigationInscrits')) {
                    $this->tableauTraceInscrit->fillFitresEntreprises();
                }
            }
            $this->panelMessageErreur->setVisible(false);
        } else {
            $this->ConsultationSummary->setWithException(false);
            $this->consultationReplyTerms->setWithException(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
            $this->tableauTraceInscrit->panelRechercheHistique->Visible = false;
        }
    }
}
