<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Page d'accueil non authentifié des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Groupements extends MpeTPage
{
    protected $_consultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idReference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteria->setIdReference($idReference);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $this->_consultation = array_shift($consultation);
            $this->consultationSummary->setConsultation($this->_consultation);
        }
        self::fillGroupement();
    }

    public function fillGroupement()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->blocGroupementEntreprise->initialiserComposant();
        $groupement = (new Atexo_Groupement())->getGroupementByIdOffre(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOffre'])));
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membresGroupement = (new Atexo_Groupement())->getMembresGroupement($groupement->getIdGroupementEntreprise(), $connexion);
            foreach ($membresGroupement as $membre) {
                $sousMemebres = (new Atexo_Groupement())->getSousGroupementByIdParent($membre->getIdMembreGroupementEntreprise(), $connexion);
                if (is_array($sousMemebres) && count($sousMemebres)) {
                    $membre->setHasChild(true);
                    foreach ($sousMemebres as $sousMemebre) {
                        $sousMemebre->setIdEntrepriseParent($membre->getIdEntreprise());
                        $membre->addSousMembres($sousMemebre);
                    }
                } else {
                    $membre->setHasChild(false);
                }
                $groupement->addOneMembres($membre);
            }
            $this->blocGroupementEntreprise->setGroupement($groupement);
            $this->blocGroupementEntreprise->fillRepeaterGroupement();
        }
    }
}
