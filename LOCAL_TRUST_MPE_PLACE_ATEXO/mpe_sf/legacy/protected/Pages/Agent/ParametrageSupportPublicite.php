<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery;
use Application\Propel\Mpe\CommonTPreferenceOffreSupportPublication;
use Application\Propel\Mpe\CommonTPreferenceOffreSupportPublicationQuery;
use Application\Propel\Mpe\CommonTPreferenceSupportPublication;
use Application\Propel\Mpe\CommonTPreferenceSupportPublicationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 26/10/2015
 * Time: 09:58.
 */
class ParametrageSupportPublicite extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->getIsPostBack()) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if (Atexo_Module::isEnabled('publicite')
                && 1 != Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $support = Atexo_Publicite_AnnonceSub::getSupportsPublicationActifs();
                $this->listeSupports->setDataSource($support);
                $this->listeSupports->DataBind();
            } else {
                $logger->error("Accés au formulaire de parametrage de support, module plateforme 'Publicite' non active");
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        }
    }

    public function annuler()
    {
        $this->response->redirect('agent');
    }

    public function enregistrer()
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            foreach ($this->listeSupports->Items as $item) {
                $pref = CommonTPreferenceSupportPublicationPeer::getPreferenceAgentbyId($item->idSupport->value, Atexo_CurrentUser::getId(), Atexo_CurrentUser::getOrganismAcronym(), $connexion);
                if (!($pref instanceof CommonTPreferenceSupportPublication)) {
                    $pref = new CommonTPreferenceSupportPublication();
                    $pref->setIdAgent(Atexo_CurrentUser::getId());
                    $pref->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                    $pref->setIdSupport($item->idSupport->value);
                }
                if ($item->actif->Checked) {
                    $pref->setActive(1);
                } else {
                    $pref->setActive(0);
                }
                $pref->save($connexion);
                $this->enregistrerPreferenceOffres($item, $pref);
            }
            $this->response->redirect('agent');
        } catch (Exception $e) {
            Prado::log("une erreur s'est produite ".$e->getMessage(), TLogger::ERROR);
        }
    }

    public function enregistrerPreferenceOffres($itemSupport, $prefSupport)
    {
        try {
            if ($prefSupport instanceof CommonTPreferenceSupportPublication) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                foreach ($itemSupport->listeOffresSupport->Items as $item) {
                    $pref = CommonTPreferenceOffreSupportPublicationQuery::create()
                        ->filterByIdOffre($item->idOffreSupport->value)
                        ->filterByIdPreference($prefSupport->getId())
                        ->filterByIdSupport($prefSupport->getIdSupport())
                        ->findOne($connexion);
                    if (!($pref instanceof CommonTPreferenceOffreSupportPublication)) {
                        $pref = new CommonTPreferenceOffreSupportPublication();
                        $pref->setIdOffre($item->idOffreSupport->value);
                        $pref->setIdSupport($prefSupport->getIdSupport());
                        $pref->setIdPreference($prefSupport->getId());
                        $pref->setIdAgent($prefSupport->getIdAgent());
                        $pref->setOrganisme($prefSupport->getOrganisme());
                    }
                    if ($prefSupport->getActive() && $item->actifOffre->Checked) {
                        $pref->setActive(1);
                    } else {
                        $pref->setActive(0);
                    }
                    $pref->save($connexion);
                }
            }
        } catch (Exception $e) {
            Prado::log("une erreur s'est produite ".$e->getMessage(), TLogger::ERROR);
        }
    }

    public function getOffresSupport($idSupport)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $res = (array) CommonTOffreSupportPubliciteQuery::create()->filterByIdSupport($idSupport)->filterByActif(1)->orderByOrdre(Criteria::ASC)->find($connexion);

            return $res;
        } catch (Exception $e) {
            Prado::log("une erreur s'est produite ".$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR);
        }
    }
}
