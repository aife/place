<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Prado\Prado;

/**
 * Page acces a l'histoique de la publicite.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class PopupHistoriqueAnnonce extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->getIsPostBack()) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if (Atexo_Module::isEnabled('publicite')) {
                $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($ref);
                $org = Atexo_CurrentUser::getOrganismAcronym();
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                if ($consultation instanceof CommonConsultation) {
                    $logger->info("Accés a l'histoique de la publicité pour consultation ".$ref);
                    if (isset($_GET['idAnnonce'])) {
                        $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
                        $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org, $connexion);
                        if ($annonce instanceof CommonTAnnonceConsultation) {
                            $id = 0;
                            if (isset($_GET['idSupport'])) {
                                $id = $_GET['idSupport'];
                                $support = Atexo_Publicite_AnnonceSub::getSupportAnnonce($idAnnonce, $id, $connexion);
                                if ($support instanceof CommonTSupportAnnonceConsultation) {
                                    $supportPub = $support->getCommonTSupportPublication($connexion);
                                    if ($supportPub instanceof CommonTSupportPublication) {
                                        $this->libelleSupport->setText($supportPub->getNom());
                                    }
                                    if ($support->getNumeroAvis()) {
                                        $this->panelNumeroAvis->setVisible(true);
                                        $this->numeroAvis->setText($support->getNumeroAvis());
                                    } else {
                                        $this->panelNumeroAvis->setVisible(false);
                                    }
                                    $this->remplirMsgStatut($support);
                                } else {
                                    $this->detailSupport->setVisible(false);
                                }
                            } else {
                                $this->detailSupport->setVisible(false);
                            }
                        }
                        $this->populateHistorique($idAnnonce, $id);
                    }
                } else {
                    $this->panelBlocErreur->setVisible(true);
                    $this->messageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                    //  $this->frameAnnonce->setVisible(false);
                    $this->detailSupport->setVisible(false);
                    $this->historique->setVisible(false);
                    $logger->error("Accés a l'histoique publicité pour consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                        .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                }
            } else {
                $logger->error("Accés a l'histoique publicité, module plateforme 'Publicite' non active");
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        }
    }

    public function populateHistorique($idAnnonce, $idSupport, $sensOrdre = Criteria::DESC)
    {
        $donnees = Atexo_Publicite_AnnonceSub::getHistoriquesAnnonce($idAnnonce, $idSupport, $sensOrdre);
        $this->listeHistoriques->setDataSource($donnees);
        $this->listeHistoriques->DataBind();
    }

    public function remplirMsgStatut($support)
    {
        $text = null;
        if ($support instanceof CommonTSupportAnnonceConsultation) {
            switch ($support->getStatut()) {
                case Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER'):
                    $text = '<div>'.Prado::localize('MESSAGE_HISTORIQUE_STATUT_A_COMPLETER').'</div>';
                    break;
                case Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET'):
                    $text = '<div >'.Prado::localize('MESSAGE_HISTORIQUE_STATUT_COMPLET').'</div>';
                    break;
                case Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE'):
                    $text = '<div>'.Prado::localize('MESSAGE_HISTORIQUE_STATUT_ENVOYE').'</div>';
                    break;
                case Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION'):
                    $text = '<div>'.Prado::localize('MESSAGE_HISTORIQUE_STATUT_EN_ATTENTE_PUBLICATION').'</div>';
                    break;
                case Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE'):
                    $text = '<div class="statut-valide"><img src="'.$this->themesPath().'/images/picto-check-ok-small.gif" alt="" class="inline-img" />'.str_replace('#DATE#', $support->getDateStatut('d/m/Y'), Prado::localize('MESSAGE_HISTORIQUE_STATUT_PUBLIER')).'</div>';
                    break;
                case Atexo_Config::getParameter('ANNONCE_STATUT_REJETE'):
                    $text = '<div class="statut-erreur"><img src="'.$this->themesPath().'/images/picto-check-not-ok.gif" alt="" class="inline-img" />'.str_replace('#DATE#', $support->getDateStatut('d/m/Y'), Prado::localize('MESSAGE_HISTORIQUE_STATUT_REJETE'))
                        .($support->getMessageStatut() ? Prado::localize('MOTIF_HISTORIQUE_PUBLICATION').' : '.$support->getMessageStatut() : '').'</div>';
                    break;
            }
            if ($support->getLienPublication()) {
                $this->panelLienPublication->setVisible(true);
                $this->lienPublication->setNavigateUrl($support->getLienPublication());
            } else {
                $this->panelLienPublication->setVisible(false);
            }
            $this->libelleStatut->setText($text);
        }
    }

    public function trier($sender, $param)
    {
        $sens = $this->getViewState('sensTri');
        if (empty($sens) || Criteria::DESC == $sens) {
            $sens = Criteria::ASC;
        } else {
            $sens = Criteria::DESC;
        }
        $this->setViewState('sensTri', $sens);
        $idAnnonce = Atexo_Util::atexoHtmlEntities($_GET['idAnnonce']);
        $id = 0;
        if (isset($_GET['idSupport'])) {
            $id = $_GET['idSupport'];
        }
        $this->populateHistorique($idAnnonce, $id, $sens);
    }
}
