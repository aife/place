<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use Prado\Prado;

/**
 * Classe de gestion des statistiques métiers de la bourse à la co-traitance.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since 2017-esr
 */
class StatistiquesBourseCotraitance extends MpeTPage
{
    /**
     * Initialisation de la classe.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('BourseCotraitance', Atexo_CurrentUser::getCurrentOrganism())) {
            echo Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            exit;
        }
        if (!$this->IsPostBack) {
            // remplissage de la liste des entités
            $this->remplirListEntiteAssociee();
            $this->critereFiltre_1_dateStart->Text = '01/01/'.date('Y');
            $this->critereFiltre_1_dateEnd->Text = date('d/m/Y');
            $this->onSearchClick();
        }
    }

    /**
     * Permet de remplir la liste des entites associees.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function remplirListEntiteAssociee()
    {
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        $this->entiteeAssociee->Datasource = $entities;
        $this->entiteeAssociee->dataBind();
        $this->entiteeAssociee->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    /**
     * S'execute lorsque l'on clique sur le bouton "valider"
     * Construit l'objet critere recherche et recupere les donnees a afficher.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function onSearchClick()
    {
        $criteria = $this->getCriteresRecherche();

        $this->nbreInscriptionsEntreprises->Text = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesInscritesBourse($criteria);
        $this->nbreEntreprisesUniquesInscrits->Text = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesUniquesInscritesBourse($criteria);
        $this->nbreConsultationsConcernees->Text = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationsConcerneParBourse($criteria);
        $this->nbreReponseGroupementCons->Text = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponseEnGroupement($criteria);
    }

    /**
     * Construit l'objet criteres de recherche et le place dans la session de la page.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getCriteresRecherche()
    {
        $criteria = new Atexo_Statistiques_CriteriaVo();
        $criteria->setOrganisme(null);
        if ('' != $this->critereFiltre_1_dateStart->Text) {
            $criteria->setDateMiseEnLigneStart(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateStart->Text));
        }
        if ('' != $this->critereFiltre_1_dateEnd->Text) {
            $criteria->setDateMiseEnLigneEnd(Atexo_Util::frnDate2iso($this->critereFiltre_1_dateEnd->Text));
        }
        $this->setViewState('criteria', $criteria);

        return $this->getViewState('criteria');
    }

    /**
     * Execute l'action du bouton annuler et reinitialise le formulaire de recherche.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function onEffaceClick()
    {
        $this->critereFiltre_comp_1->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
        $this->cumulValeurs->Checked = false;
    }
}
