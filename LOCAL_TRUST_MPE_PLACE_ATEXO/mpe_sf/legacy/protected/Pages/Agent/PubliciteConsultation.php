<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonAnnonceMoniteurPeer;
use Application\Propel\Mpe\CommonAVISPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinataireAnnonceJALPeer;
use Application\Propel\Mpe\CommonDestinataireCentralePubPeer;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use Application\Service\Atexo\Publicite\Atexo_Publicite_FormatLibreVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_ReferentielFormXml;
use Application\Service\Atexo\Publicite\Atexo_Publicite_SendAnnonce;
use Exception;
use Prado\Prado;

/**
 * Page de publication d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PubliciteConsultation extends MpeTPage
{
    private $_consultation = '';
    protected $critereTri = '';
    protected $triAscDesc = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_consultation = $this->retrieveConsultation(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
        if ($this->_consultation instanceof CommonConsultation) {
            $this->mainPanel->visible = true;
            $this->errorPanel->visible = false;
            if (Atexo_CurrentUser::hasHabilitation('PublierConsultation') && !$this->_consultation->getCurrentUserReadOnly()) {
                $this->idConsultationSummary->setConsultation($this->_consultation);
                if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                    $urlRetour = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().
                                   '&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $urlRetour = 'index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                } else {
                    $urlRetour = '#';
                }

                //publicité format libre et xml
                if (!$this->IsPostBack) {
                    $this->displayArrayFormPub($this->_consultation->getId());
                    $this->visibiliteLienMolDouble();
                }
                if (Atexo_Module::isEnabled('Publicite') && !isset($_GET['justMol'])) {
                    $this->panelEchos->setVisible(true);
                    $this->displayPubEchos($this->_consultation);
                }
                $this->displayFormPubLibre($this->_consultation->getId());

                if (Atexo_Module::isEnabled('PubliciteFormatXml') && Atexo_Module::isEnabled('PubliciteMarchesEnLigne')) {
                    if ($this->_consultation->getCompteBoampAssocie()) {
                        $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($this->_consultation->getCompteBoampAssocie(), Atexo_CurrentUser::getOrganismAcronym());
                        if ($acheteurPublic instanceof CommonAcheteurPublic) {
                            $this->choixCompteBoamp->setVisible(false);
                            $this->buttonsChoixCompteBoamp->setVisible(false);
                            $this->frameMol->setVisible(true);
                        } else {
                            $this->errorPanel->setVisible(true);
                            $this->errorPanel->setMessage(Prado::localize('DEFINE_COMPTE_BOAMP_ASSOCIE_SUPPRIME'));
                            $this->choixCompteBoamp->setVisible(true);
                            $this->buttonsChoixCompteBoamp->setVisible(true);
                            $this->frameMol->setVisible(false);
                            if (!$this->isPostBack) {
                                $this->displayAcheteurPublic();
                            }
                        }
                    } else {
                        $this->choixCompteBoamp->setVisible(true);
                        $this->buttonsChoixCompteBoamp->setVisible(true);
                        $this->frameMol->setVisible(false);
                        if (!$this->isPostBack) {
                            $this->displayAcheteurPublic();
                        }
                    }
                }

                $this->boutonRetour->setNavigateUrl($urlRetour);
                $this->boutonRetourH->setNavigateUrl($urlRetour);
            } else {
                $this->response->redirect('agent');
            }
        } else {
            $this->mainPanel->visible = false;
            $this->errorPanel->visible = true;
            $this->errorPanel->setMessage('Error Consultation');

            return;
        }
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function displayArrayFormPub($consultationId)
    {
        $listeFormulaire = (new Atexo_Publicite_ReferentielFormXml())->retreiveListFormulaireXml($consultationId);
        if (is_array($listeFormulaire) && count($listeFormulaire) > 0) {
            $datatriee = Atexo_Util::sortArrayOfObjects($listeFormulaire, 'DateCreation', 'ASC');
            Atexo_CurrentUser::writeToSession('cachedFormPub', $datatriee);
            $this->repeaterFormPublicite->DataSource = $datatriee;
            $this->repeaterFormPublicite->DataBind();
        } else {
            $this->affichagePubFormatXml->Visible = false;
        }
    }

    public function displayPubEchos($consultation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifie($consultation->getId(), $consultation->getOrganisme(), $connexion);
        if ($annonce instanceof CommonTAnnonceConsultation) {
            $supportsAnnonces = Atexo_Publicite_AnnonceSub::getSupportAnnonceByIdAnnonceCons($annonce->getId());
            if (!empty($supportsAnnonces)) {
                $this->listeSupportPub->DataSource = $supportsAnnonces;
                $this->listeSupportPub->DataBind();
            }
        }
        if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
            $this->publiciteSuivi->charger($consultation);
        }
    }

    public function displayStatut($statut)
    {
        if ($statut == Atexo_Config::getParameter('ANNONCE_BROUILLON')) {
            return Prado::localize('TEXT_BROUILLON');
        } elseif ($statut == Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE')) {
            return Prado::localize('TEXT_CONFORME_ET_VALIDITE');
        } elseif ($statut == Atexo_Config::getParameter('ANNONCE_A_CORRIGER')) {
            return 'A corriger';
        }
    }

    /**
     * Trier le tableau des formulaires.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $cachedFormPub = Atexo_CurrentUser::readFromSession('cachedFormPub');

        if ('NameForm' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedFormPub, 'LibelleType', 'Referentieltypexml');
        } elseif ('DateCreation' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedFormPub, 'DateCreation', '');
        } elseif ('Statut' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedFormPub, 'Statut', '');
        } else {
            return;
        }

        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedFormPub');
        Atexo_CurrentUser::writeToSession('cachedFormPub', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->repeaterFormPublicite->DataSource = $dataSourceTriee[0];
        $this->repeaterFormPublicite->DataBind();

        // Re-affichage du TActivePanel
        $this->panelFormPub->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, $typeCritere);
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    /**
     * Afficher le nouveau tableau si nouveau Formulaire ajouté.
     */
    public function onCallBackRefreshRepeater($sender, $param)
    {
        $listeFormulaire = (new Atexo_Publicite_ReferentielFormXml())->retreiveListFormulaireXml(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        if (is_array($listeFormulaire)) {
            $datatriee = Atexo_Util::sortArrayOfObjects($listeFormulaire, 'DateCreation', 'ASC');
            Atexo_CurrentUser::writeToSession('cachedFormPub', $datatriee);
            if (!is_array($datatriee)) {
                $datatriee = [];
            }
            $this->repeaterFormPublicite->DataSource = $datatriee;
            $this->repeaterFormPublicite->DataBind();
            $this->panelFormPub->render($param->NewWriter);

            $this->displayFormPubLibre(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            $this->panelPubLibre->render($param->NewWriter);

            $this->displayPubEchos($this->_consultation);
            $this->panelEchos->render($param->getNewWriter());
        }
    }

    public function displayLibelleStatutDestinataire($typeAnnonce, $statut)
    {
        if ('MONITEUR' == $typeAnnonce) {
            return $this->displayLibelleStatutMoniteur($statut);
        }
        if ($statut == Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE')) {
            return Prado::localize('DESTINATAIRE_EN_ATTENTE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
            return Prado::localize('DESTINATAIRE_PUBLIE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_REFUSE')) {
            return Prado::localize('DESTINATAIRE_REFUSE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_ENVOYE')) {
            return Prado::localize('TEXT_ENVOYE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV')) {
            return Prado::localize('TEXT_EN_COURS_VALIDATION_EDITORIALE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AP')) {
            return Prado::localize('TEXT_EN_COURS_PUBLICATION');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_ENVOI_ECHOUE')) {
            return Prado::localize('TEXT_EN_ANOMALIE_ENVOI_ECHOUE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP')) {
            return Prado::localize('TEXT_EN_COURS_PUBLICATION');
        }
        //////////////////////////////////////
        elseif (($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RG'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RX'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RE'))) {
            return Prado::localize('DESTINATAIRE_REFUSE');
        }
        //////////////////////////////////////
        elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU')) {
            return Prado::localize('DESTINATAIRE_PUBLIE');
        }
    }

    /**
     * afficher le libelle de l'accusé de reception du destinataire.
     */
    public function displayLibelleAR($typeAnnonce, $destinataire, $accuse)
    {
        if ('BOAMP' == $typeAnnonce) {
            if ($destinataire == Atexo_Config::getParameter('ACCUSE_RECEPTION')) { // si Destinataire Portail Entreprise
                if ($accuse == Atexo_Config::getParameter('ACCUSE_RECEPTION_DESTINATAIRE')) {
                    return Prado::localize('ACCUSE_RECEPTION_NA');
                }
            }

            return '-';
        } elseif ('MONITEUR' == $typeAnnonce) {
            return $accuse;
        }

        return '-';
    }

    public function redirectTo($sender, $param)
    {
        $url = $param->CommandName;
        $this->response->redirect($url);
    }

    /**
     * Ajouter xml dans la base en mode brouillon.
     */
    public function addStringXml($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->repeaterFormPublicite->getItems() as $item) {
            $actionXml = $item->actionXml->Text;
            if ('1' == $actionXml) {
                $xmlString = $item->xmlString->Text;
                $idFormXml = $item->idFormXml->Text;
                $validateXml = $item->validateXml->Text;
                if (('' != $xmlString) && ('' != $idFormXml)) {
                    $formXml = CommonReferentielFormXmlPeer::retrieveByPK($idFormXml, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                    if ($formXml) {
                        // 1 utf8_decode à cause de Ajax
                        // le module BOAMP remplace les caractères '&' par '&amp;'
                        // il ne distingue pas la diffèrence entre les '&' dans une phrase et dans une url
                        // on fait la modification avant d'enregistrer en base pour remplacer le '&' d'une phrase par 'et'
                        $formXml->setXml(str_replace(' &amp; ', ' et ', utf8_decode($xmlString)));
                        if ('1' == $validateXml) {
                            $formXml->setStatut(Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE'));
                        }
                        $formXml->save($connexion);
                    }
                }
            }
        }
        $this->displayArrayFormPub($this->_consultation->getId());
        $this->panelFormPub->render($param->NewWriter);
    }

    public function displayFormPubLibre($consultationId)
    {
        $listeFormAnnonceJal = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibre($consultationId);
        $listeFormAvis = (new Atexo_Publicite_Avis())->retreiveListFormulaireLibre($consultationId);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, Atexo_CurrentUser::getCurrentOrganism());
        $dataSource = [];
        $index = 0;
        foreach ($listeFormAnnonceJal as $oneAnnoneJal) {
            $newFormLibre = new Atexo_Publicite_FormatLibreVo();
            $newFormLibre->setId($oneAnnoneJal->getId());
            $newFormLibre->setLibelleType(Prado::localize('TEXT_ANNONCE_EXTRAIT'));
            $newFormLibre->setDateCreation($oneAnnoneJal->getDateCreation());
            $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_ANNONCE'));
            $newFormLibre->setType('ANNONCE');
            $dataSource[$index] = $newFormLibre;
            ++$index;
        }
        foreach ($listeFormAvis as $oneAvis) {
            $newFormLibre = new Atexo_Publicite_FormatLibreVo();
            if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
                $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'));
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            } else {
                $newFormLibre->setLibelleType(Prado::localize('TEXT_URL_ACCES_DIRECT'));
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
            }
            $newFormLibre->setId($oneAvis->getId());
            $newFormLibre->setDestinataire(Prado::localize('TEXT_PORTAIL_ENTREPRISE'));
            if ('' != $oneAvis->getDatePub() && $consultation->getDateMiseEnLigneCalcule() && '0000-00-00 00:00:00' != $consultation->getDateMiseEnLigneCalcule()) {
                $dateMiseEnLigne = substr($consultation->getDateMiseEnLigneCalcule(), 0, 11);
                if (Atexo_Util::differenceJours(Atexo_Util::iso2frnDate($dateMiseEnLigne), Atexo_Util::iso2frnDate($oneAvis->getDatePub())) > 0) {
                    $newFormLibre->setDatePub(Atexo_Util::iso2frnDate($dateMiseEnLigne));
                } else {
                    $newFormLibre->setDatePub(Atexo_Util::iso2frnDate($oneAvis->getDatePub()));
                }
            } else {
                $newFormLibre->setDatePub(' - ');
            }
            $newFormLibre->setDateCreation($oneAvis->getDateCreation());
            $newFormLibre->setStatut($oneAvis->getStatut());
            $newFormLibre->setAccuse(Prado::localize('ACCUSE_RECEPTION_NA'));
            if ($oneAvis->getDatePub()) {
                $newFormLibre->setDateEnvoi(Atexo_Util::iso2frnDate($oneAvis->getDatePub()));
            } else {
                $newFormLibre->setDateEnvoi(' - ');
            }
            $newFormLibre->setType('AVIS');
            $dataSource[$index] = $newFormLibre;
            ++$index;
        }
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, 'DateCreation', 'ASC');
        Atexo_CurrentUser::writeToSession('cachedFormPubLibre', $datatriee);
        if (!is_array($datatriee)) {
            $datatriee = [];
        }
        $this->repeaterFormPubliciteLibre->DataSource = $datatriee;
        $this->repeaterFormPubliciteLibre->DataBind();
    }

    /**
     * Afficher le nouveau tableau  format libre si nouveau Formulaire ajouté.
     */
    public function onCallBackRefreshRepeaterFormLibre($sender, $param)
    {
        $this->onCallBackRefreshRepeater($sender, $param);
    }

    /**
     * Changer le statut de l'avis complémentaire en mode suspendu.
     */
    public function suspendreAvis($sender, $param)
    {
        $idAvis = $param->CommandName;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avis = CommonAVISPeer::retrieveByPK($idAvis, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_SUSPENDU'));
        $avis->save($connexionCom);
    }

    /**
     * Changer le statut de l'avis complémentaire en mode suspendu.
     */
    public function publierAvis($sender, $param)
    {
        $idAvis = $param->CommandName;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avis = CommonAVISPeer::retrieveByPK($idAvis, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
        $avis->setDatePub(date('Y-m-d'));
        $avis->save($connexionCom);
    }

    /**
     * Changer le statut de l'avis complémentaire en mode suspendu.
     */
    public function envoyerAvis($sender, $param)
    {
        $idAvis = $param->CommandName;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avis = CommonAVISPeer::retrieveByPK($idAvis, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        if ($avis) {
            $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            $avis->setDatePub(date('Y-m-d'));
            $avis->save($connexionCom);
        }
        //$this->labelRefrech->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_refreshRepeaterFormLibre').click();return false;</script>";
    }

    /**
     * Lien d'accès direct Avis de publicité Format libre.
     */
    public function lienAccesDirect($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idAvis = $param->CommandName;
        $avis = (new Atexo_Publicite_Avis())->retreiveAvisById($idAvis, $connexionCom);
        if ($avis) {
            $this->Response->Redirect($avis->getUrl());
        }
    }

    public function displayLibelleStatutDestinataireLibre($statut)
    {
        if (($statut == Atexo_Config::getParameter('DESTINATAIRE_VIDE'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'))) {
            return Prado::localize('DESTINATAIRE_EN_ATTENTE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
            return Prado::localize('TEXT_ENVOYE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_REFUSE')) {
            return Prado::localize('TEXT_SUSPENDU');
        }
    }

    /**
     * Trier le tableau des formulaires.
     *
     * @param sender
     * @param param
     */
    public function sortRepeaterFormatLibre($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTriLibre');
        $this->triAscDesc = $this->getViewState('triAscDescLibre');
        $cachedFormPubLibre = Atexo_CurrentUser::readFromSession('cachedFormPubLibre');
        if ('NameForm' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedFormPubLibre, 'LibelleType');
        } elseif ('DateCreation' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedFormPubLibre, 'DateCreation', '');
        } else {
            return;
        }

        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedFormPubLibre');
        Atexo_CurrentUser::writeToSession('cachedFormPubLibre', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTriLibre', $this->critereTri);
        $this->setViewState('triAscDescLibre', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->repeaterFormPubliciteLibre->DataSource = $dataSourceTriee[0];
        $this->repeaterFormPubliciteLibre->DataBind();

        // Re-affichage du TActivePanel
        $this->panelPubLibre->render($param->getNewWriter());
    }

    /*  Envoi de l'annonce au BOAMP
     *
     *
     */
    public function sendXml($sender, $param)
    {
        $parametters = explode('-', $param->CommandName);
        $idFormXml = $parametters[0];
        $idAnnonce = $parametters[1];
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $refCons = $this->_consultation->getId();
        (new Atexo_Publicite_SendAnnonce())->send('BOAMP', $idFormXml, $idAnnonce, $organisme, $refCons);
    }

    public function statutPictoEnvoyer($typeAnnonce, $statut, $statutForm)
    {
        if ('BOAMP' == $typeAnnonce) {
            //////////////////////////////////////
            if ($statutForm != Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE')) {
                return false;
            }
            //////////////////////////////////////

            elseif (($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'))
            || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV'))
            || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'))
            || ($statut == Atexo_Config::getParameter('DESTINATAIRE_ENVOYE'))
            || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AP'))) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function statutPictoCorriger($statut)
    {
        //////////////////////////////////////
        if (($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RG'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RX'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RE'))) {
            return true;
        }
        //////////////////////////////////////
        else {
            return false;
        }
    }

    public function statutMasque($typeAnnonce, $statut, $statutForm)
    {
        if ('BOAMP' == $typeAnnonce) {
            if (('1' == $statut) || ($this->statutPictoEnvoyer($typeAnnonce, $statut, $statutForm))) {
                return false;
            } else {
                return true;
            }
        } elseif ('MONITEUR' == $typeAnnonce) {
            if (!$this->statutPictoEditMoniteur($typeAnnonce, $statut)) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function onDetailClick($sender, $param)
    {
        $id = $param->CommandParameter;
        $this->Response->Redirect('?page=Agent.FormeEnvoiCourrierElectroniqueJAL&idAnnonceJAL='.$id.'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    public function getInfoJal($jalObject)
    {
        $string = '';
        if ($jalObject) {
            $string .= Prado::localize('TEXT_COORDONNEES_ANNONCEUR');
            $string .= '
';
            $string .= Prado::localize('NOM').' : '.$jalObject->getNom();
            $string .= '
';
            $string .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$jalObject->getEmail();
            $string .= '
';
            $string .= Prado::localize('EMAIL_AR_JAL').' : '.$jalObject->getEmailAr();
            $string .= '
';
            $string .= Prado::localize('DEFINE_FAX').' : '.$jalObject->getTelecopie();
            $string .= '
';
            $string .= Prado::localize('DEFINE_TEXT_INFORMATION_FACTURATION').' : '.$jalObject->getInformationFacturation();
        }

        return $string;
    }

    public function envoieMessage($sender, $param)
    {
        $subject = null;
        $message = null;
        $to = null;
        $c = new Criteria();
        $validePJ = false;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $destObject = (new Atexo_Publicite_DestinataireJAL())->retreiveDestinataireById($param->CommandParameter);
        $jalObject = $destObject[0]->getCommonJAL();
        $annonceJalObject = $destObject[0]->getAnnonceJAL($destObject[0], $connexionCom);
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        if ($jalObject) {
            $to = $jalObject->getEmail();
        }
        if ($annonceJalObject) {
            $subject = $annonceJalObject->getObjet();
            $message = $annonceJalObject->getTexte();
            $message .= '
';
            if ($jalObject) {
                $message .= $this->getInfoJal($jalObject);
            }
        }

        $PJs = $annonceJalObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);

        $echange = self::addEchange(Atexo_Util::atexoHtmlEntities($_GET['id']), $from, $subject, $message, $connexionCom, $to, $annonceJalObject->getOptionEnvoi(), $PJs);
        $destObject = CommonDestinataireAnnonceJALPeer::retrieveByPk($param->CommandParameter, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $destObject->setStatut(Atexo_Config::getParameter('DESTINATAIRE_ENVOYE'));
        $destObject->setDateEnvoi(date('Y-m-d'));
        $destObject->setIdEchange($echange->getId());
        $destObject->save($connexionCom);
        Atexo_Message::EnvoyerEmail($echange, Atexo_CurrentUser::getCurrentOrganism());
    }

    public function pdf()
    {
        return file('http://boamp.journal-officiel.gouv.fr/PDF/20060051-C-pk4yj5wVJRYP.pdf');
    }

    public function actionFormulaire($sender, $param)
    {
        (new Atexo_Publicite_ReferentielFormXml())->deleteFormulaireXml($param->CommandName);
    }

    public function actionFormulaireLibre($sender, $param)
    {
        $params = explode('-', $param->CommandName);
        if ('ANNONCE' == $params[1]) {
            (new Atexo_Publicite_DestinataireJAL())->deleteDestinataireJalAnnonce($params[0]);
            (new Atexo_Publicite_AnnonceJAL())->deleteAnnonce($params[0]);
            (new Atexo_Publicite_CentralePublication())->deleteDestinataireCentralePub($params[0]);
        } elseif ('AVIS' == $params[1]) {
            (new Atexo_Publicite_Avis())->deleteAvis($params[0]);
        }
        //$this->labelRefrech->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_refreshRepeaterFormLibre').click();</script>";
    }

    public function displayLinkPopupError($typeAnnonce, $annError, $statut, $statutForm)
    {
        if (('BOAMP' == $typeAnnonce)) {
            if (($statutForm == Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE') || $statutForm == Atexo_Config::getParameter('ANNONCE_A_CORRIGER'))
            && ($statut != Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'))
            && ('' != $annError)) {
                return true;
            }
        } elseif (('MONITEUR' == $typeAnnonce)) {
            if (($statut == Atexo_Config::getParameter('STATUT_MONITEUR_ENVOI_ECHOUE'))
            && ('' != $annError)) {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     *  Identifiant de l'annonce dans la PMI : id_boamp, Type de Publication : type_boamp,
     * Identifiant de parution : parution,  Numéro d'annonce dans la parution : num_ann ,
     *  Identifiant de l'annonce au BOAMP : id_jo.
     */
    public function displayInfoBulleAnnonce($typeAnnonce, $commentaire, $ann_error, $id_boamp, $type_boamp, $parution, $num_ann, $id_jo)
    {
        $text = '';
        if ('' != $id_boamp) {
            $text .= Prado::localize('TEXT_IDENTIFIANT_ANNONCE_PMI').' : '.$id_boamp;
            $text .= '<br/>';
        }
        if ('BOAMP' == $typeAnnonce) {
            if ('' != $type_boamp) {
                $text .= Prado::localize('TEXT_TYPE_PUBLICATION').' : '.$type_boamp;
                $text .= '<br/>';
            }
            if ('' != $parution) {
                $text .= Prado::localize('TEXT_IDENTIFIANT_PARUTION').' : '.$parution;
                $text .= '<br/>';
            }
            if ('' != $num_ann) {
                $text .= Prado::localize('TEXT_NUMERO_ANNONCE_PARUTION').' : '.$num_ann;
                $text .= '<br/>';
            }
            if ('' != $id_jo) {
                $text .= Prado::localize('TEXT_IDENTIFIANT_ANNONCE_BOAMP').' : '.$id_jo;
                $text .= '<br/>';
            } else {
                $text .= Prado::localize('TEXT_IDENTIFIANT_ANNONCE_BOAMP').':'.Prado::localize('TEXT_NON_DISPONIBLE');
                $text .= '<br/>';
            }
        } elseif ('MONITEUR' == $typeAnnonce) {
            if ('' != $num_ann) {
                $text .= Prado::localize('TEXT_IDENTIFIANT_TECHNIQUE_ANNONCE_MONITEUR').' : '.$num_ann;
                $text .= '<br/>';
            }
            if ('' != $commentaire) {
                $text .= Prado::localize('TEXT_COMMENTAIRE_ANNONCE_MONITEUR').' : '.$commentaire;
                $text .= '<br/>';
            }
        }

        return $text;
    }

    /**
     * afficher tableau des destinataires associé à un formulaire xml.
     */
    public function displayListeDistinataire($idFormXml)
    {
        $arrayAnnonce = (new Atexo_Publicite_Destinataire())->retreiveListDestinataire($idFormXml);
        $dataSource = [];
        $index = 0;
        foreach ($arrayAnnonce as $oneAnnonce) {
            $annonce = new Atexo_Publicite_AnnonceVo();
            $annonce->setId($oneAnnonce->getIdBoamp());
            $annonce->setLibelle($oneAnnonce->getCommonReferentielDestinationFormXml()->getDestinataire());
            $annonce->setStatutDestinataire($oneAnnonce->getStatutDestinataire());
            $annonce->setDateEnvoiAnnonce($oneAnnonce->retreiveDateEnvoi());
            $annonce->setDatePubAnnonce($oneAnnonce->retreiveDatePublication());
            $annonce->setAccuseReception($oneAnnonce->getAccuseReception());
            $annonce->setIdDestinataireFormXml($oneAnnonce->getCommonReferentielDestinationFormXml()->getId());
            $annonce->setAnnError($oneAnnonce->getAnnError());
            $annonce->setLienBoamp($oneAnnonce->getLienBoamp());
            $annonce->setIdBoamp($oneAnnonce->getIdBoamp());
            $annonce->setTypeBoamp($oneAnnonce->getTypeBoamp());
            $annonce->setParution($oneAnnonce->getParution());
            $annonce->setNumAnn($oneAnnonce->getNumAnn());
            $annonce->setIdJo($oneAnnonce->getIdJo());
            $annonce->setTypeAnnonce('BOAMP');
            $dataSource[$index] = $annonce;
            ++$index;
        }
        $arrayAnnMoniteur = (new Atexo_Publicite_Destinataire())->retreiveListDestinataireMoniteur($idFormXml);
        foreach ($arrayAnnMoniteur as $oneAnnonce) {
            $annonce = new Atexo_Publicite_AnnonceVo();
            $annonce->setId($oneAnnonce->getId());
            $annonce->setLibelle($oneAnnonce->getCommonReferentielDestinationFormXml()->getDestinataire());
            $annonce->setStatutDestinataire($oneAnnonce->getStatutDestinataire());
            $annonce->setDateEnvoiAnnonce($oneAnnonce->retreiveDateEnvoi());
            $annonce->setDatePubAnnonce($oneAnnonce->retreiveDatePublication());
            $annonce->setAccuseReception($oneAnnonce->retreiveAccuseReception());
            $annonce->setIdDestinataireFormXml($oneAnnonce->getCommonReferentielDestinationFormXml()->getId());
            $annonce->setTypeAnnonce('MONITEUR');
            $annonce->setStatutBoamp($oneAnnonce->getStatutXml());
            $annonce->setXml($oneAnnonce->getXmlMoniteur());
            $annonce->setAnnError($oneAnnonce->getMessageError());
            $annonce->setNumAnn($oneAnnonce->getNumAnnonce());
            $annonce->setCommentaire($oneAnnonce->getCommentaire());
            $dataSource[$index] = $annonce;
            ++$index;
        }

        return $dataSource;
    }

    /**
     * ajout du flux xml dans la table AnnonceMoniteur.
     */
    public function addXmlMoniteur($sender, $param)
    {
        $idAnnMoniteur = $this->IdAnnonceMoniteur->Text;
        $xmlMoniteur = $this->xmlMoniteur->Text;
        $statutXml = $this->statutXml->Text;

        if ('' != $statutXml && '' != $xmlMoniteur && '' != $idAnnMoniteur) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $annMoniteur = CommonAnnonceMoniteurPeer::retrieveByPK($idAnnMoniteur, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
            // 1 utf8_decode à cause de Ajax
            // le module BOAMP remplace les caractères '&' par '&amp;'
            // il ne distingue pas la diffèrence entre les '&' dans une phrase et dans une url
            // on fait la modification avant d'enregistrer en base pour remplacer le '&' d'une phrase par 'et'
            $annMoniteur->setXmlMoniteur(str_replace(' & ', ' et ', utf8_decode($xmlMoniteur)));
            $annMoniteur->setStatutXml($statutXml);
            $annMoniteur->setDateMaj(date('Y-m-d H:i:s'));
            $annMoniteur->save($connexion);
        }
        $this->displayArrayFormPub($this->_consultation->getId());
        $this->panelFormPub->render($param->NewWriter);
    }

    /**
     * afficher le boutton envoi au groupe moniteur si statut formulaire est valide
     * et l'annonce boamp est acceptée.
     */
    public function statutPictoEnvoyerMoniteur($xml, $typeAnnonce, $statutMoniteur, $statutXml, $statutForm)
    {
        if ('MONITEUR' == $typeAnnonce) {
            if ($statutForm != Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE')) {
                return false;
            } elseif ((
                ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE'))
                || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_REJETE'))
                || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_ECHEC_TRANSMISSION'))
                || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_ENVOI_ECHOUE'))
            )
            && ('' != $xml) && ('1' == $statutXml)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * afficher le boutton editer le formulaire du module.
     */
    public function statutPictoEditMoniteur($typeAnnonce, $statutMoniteur)
    {
        if ('MONITEUR' == $typeAnnonce) {
            if (($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS'))
            || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_POUR_PUBLICATION_AU_SUPPORT'))
            || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE_REEMISSION'))
            || ($statutMoniteur == Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE'))) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * envoi de la requette au groupe moniteur.
     */
    public function sendMoniteurXml($sender, $param)
    {
        $parametters = explode('-', $param->CommandName);
        $idFormXml = $parametters[0];
        $idAnnMoniteur = $parametters[1];
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $refCons = $this->_consultation->getId();
        (new Atexo_Publicite_SendAnnonce())->send('MONITEUR', $idFormXml, $idAnnMoniteur, $organisme, $refCons);
    }

    /**
     * affiche le statut de l'envoi au moniteur.
     */
    public function displayLibelleStatutMoniteur($statut)
    {
        switch ($statut) {
            // En attente d'envoi
            case Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE'):
                return Prado::localize('DESTINATAIRE_EN_ATTENTE');
                // Echec d'envoi
            case Atexo_Config::getParameter('STATUT_MONITEUR_ENVOI_ECHOUE'):
                return Prado::localize('TEXT_EN_ANOMALIE_ENVOI_ECHOUE');
                // Avis émis
            case Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS'):
                return Prado::localize('STATUT_MONITEUR_AVIS_EMIS');
                // Transmis pour publication au support
            case Atexo_Config::getParameter('STATUT_MONITEUR_POUR_PUBLICATION_AU_SUPPORT'):
                return Prado::localize('STATUT_MONITEUR_POUR_PUBLICATION_AU_SUPPORT');
                // Publié
            case Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE'):
                return Prado::localize('STATUT_MONITEUR_PUBLIE');
                // Rejeté
            case Atexo_Config::getParameter('STATUT_MONITEUR_REJETE'):
                return Prado::localize('STATUT_MONITEUR_REJETE');
                // En attente de ré-émission sur le support
                // La transmission de l'annonce à un support de presse a échouée.Une nouvelle tentative aura lieu.
            case Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE_REEMISSION'):
                return Prado::localize('STATUT_MONITEUR_EN_ATTENTE_REEMISSION');
        }
    }

    /**
     * afficher le boutton envoyer inactive le formulaire du module.
     */
    public function statutPictoSendInactive($typeAnnonce, $statutBoamp, $statutForm)
    {
        if ($statutForm != Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE')) {
            return true;
        }
        if ('MONITEUR' == $typeAnnonce) {
            if (($statutBoamp == Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'))
            || ($statutBoamp == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV'))
            || ($statutBoamp == Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'))) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function retreiveCentralePubByIdCompte($id)
    {
        $centralePub = (new Atexo_Publicite_CentralePublication())->retreiveComptePubById($id);
        if ($centralePub) {
            return $centralePub[0];
        }
    }

    public function retreiveNomMailCentraleByIdCompte($idCompte)
    {
        $compte = (new Atexo_Publicite_CentralePublication())->retreiveComptePubJoinCentraleById($idCompte);
        if ($compte[0]) {
            return $compte[0]->getCommonCentralePublication()->getNom().'<br/>'.$compte[0]->getCommonCentralePublication()->getMail();
        } else {
            return '-';
        }
    }

    public function displayInfoBulleCentralePub($idCompte, $idsJournaux, $mail = false)
    {
        $compte = (new Atexo_Publicite_CentralePublication())->retreiveComptePubJoinCentraleById($idCompte);
        $str = '';
        $breakSimple = '<br/>';
        $breakMail = '
';
        if ($compte[0]) {
            if ($mail) {
                $str .= Prado::localize('TEXT_COORDONNEES_ANNONCEUR');
                $str .= '
';
            }
            if ($compte[0]->getCommonCentralePublication()->getNom()) {
                $str .= Prado::localize('DEFINE_NOM').': '.$compte[0]->getCommonCentralePublication()->getNom();
                if ($mail) {
                    $str .= $breakMail;
                } else {
                    $str .= $breakSimple;
                }
            }
            if ($compte[0]->getCommonCentralePublication()->getMail()) {
                $str .= Prado::localize('ADRESSE_ELECTRONIQUE').': '.$compte[0]->getCommonCentralePublication()->getMail();
                if ($mail) {
                    $str .= $breakMail;
                } else {
                    $str .= $breakSimple;
                }
            }
            if ($compte[0]->getMail()) {
                $str .= Prado::localize('TEXT_ADRESSE_ACCUSE').': '.$compte[0]->getMail();
                if ($mail) {
                    $str .= $breakMail;
                } else {
                    $str .= $breakSimple;
                }
            }
            if ($compte[0]->getFax()) {
                $str .= Prado::localize('TEXT_TELECOPIEUR').': '.$compte[0]->getFax();
                if ($mail) {
                    $str .= $breakMail;
                } else {
                    $str .= $breakSimple;
                }
            }
            if ($compte[0]->getInfoCimplementaire()) {
                $str .= Prado::localize('TEXT_INFORMATION_FACTURATION').': '.$compte[0]->getInfoCimplementaire();
                if ($mail) {
                    $str .= $breakMail;
                } else {
                    $str .= $breakSimple;
                }
            }
        }
        if ($idsJournaux) {
            $chaineJ = (new Atexo_Publicite_CentralePublication())->retreiveNamesJournauxCentraleByIds($idsJournaux);
            $str .= Prado::localize('LISTE_JOURNAUX_SELECTIONNES').': '.$chaineJ;
            if ($mail) {
                $str .= $breakMail;
            } else {
                $str .= $breakSimple;
            }
        }

        return $str;
    }

    public function envoieMessageCentralePub($sender, $param)
    {
        $infoCentrale = null;
        $message = null;
        $subject = null;
        $to = null;
        $taille = 0;
        $validePJ = false;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $destObject = (new Atexo_Publicite_CentralePublication())->retreiveDestinataireById($param->CommandParameter);
        //$jalObject = $destObject[0]->getJal();
        $annonceJalObject = $destObject[0]->getAnnonceJAL($destObject[0], $connexionCom);
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        if ($destObject) {
            $compte = (new Atexo_Publicite_CentralePublication())->retreiveComptePubJoinCentraleById($destObject[0]->getIdCompte());
            $to = $compte[0]->getCommonCentralePublication()->getMail();
            $infoCentrale = $this->displayInfoBulleCentralePub($destObject[0]->getIdCompte(), $destObject[0]->getIdsJournaux(), true);
        }
        //if($jalObject){
        //    $to = $jalObject->getEmail();
        // }
        if ($annonceJalObject) {
            $subject = $annonceJalObject->getObjet();
            $message = $annonceJalObject->getTexte();
            $message .= '
';
            if ($infoCentrale) {
                $message .= $infoCentrale;
            }
        }
        $message .= '
';
        $message .= '
';
        $message .= Prado::localize('CORDIALEMENT');
        $message .= '
';
        $message .= '
';

        $message .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
        $message .= '
';
        $message .= '
';
        $message .= '
';
        $c = new Criteria();
        $PJs = $annonceJalObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);

        $echange = self::addEchange(Atexo_Util::atexoHtmlEntities($_GET['id']), $from, $subject, $message, $connexionCom, $to, $annonceJalObject->getOptionEnvoi(), $PJs);
        $destObject = CommonDestinataireCentralePubPeer::retrieveByPk($param->CommandParameter, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $destObject->setStatut(Atexo_Config::getParameter('DESTINATAIRE_ENVOYE'));
        $destObject->setDateEnvoi(date('Y-m-d'));
        $destObject->setIdEchange($echange->getId());
        $destObject->save($connexionCom);
        Atexo_Message::EnvoyerEmail($echange, Atexo_CurrentUser::getCurrentOrganism());
    }

    /**
     * masquer le boutton supprimer si l'annonce est déjà envoyé pour un destinataire.
     */
    public function hidePictoDeleteAnnonce($idFormXml)
    {
        $destinataires = (new Atexo_Publicite_Destinataire())->retreiveListeDestinataireByIdFormXml($idFormXml);
        $destinataireMoniteur = (new Atexo_Publicite_Destinataire())->retreiveListDestinataireMoniteur($idFormXml);
        $bool = true;
        if ($destinataires) {
            foreach ($destinataires as $oneDest) {
                if (($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AP'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_ENVOYE'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_XML_EN_ATTENTE_PUB'))
                || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('DESTINATAIRE_PUBLIE_AVEC_LIEN'))
                ) {
                    return false;
                } else {
                    $bool = true;
                }
            }
        }
        if ($destinataireMoniteur) {
            foreach ($destinataireMoniteur as $oneDest) {
                if (($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS'))
                 || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('STATUT_MONITEUR_POUR_PUBLICATION_AU_SUPPORT'))
                 || ($oneDest->getStatutDestinataire() == Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE'))) {
                    return false;
                } else {
                    $bool = true;
                }
            }
        }

        return $bool;
    }

    /***
     * creation d'un echange
     */

    public function addEchange($consultationId, $emailExpediteur, $subject, $message, $connexion, $destinataire, $optionEnvoi, $pjs)
    {
        $agentCon = Atexo_CurrentUser::getIdAgentConnected();
        $serviceCon = Atexo_CurrentUser::getIdServiceAgentConnected();
        $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
        if ($organismeObj instanceof CommonOrganisme) {
            $expediteur = $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
        }

        $echange = new CommonEchange();
        $echange->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $echange->setObjet($subject);
        $echange->setCorps($message);
        $echange->setExpediteur($expediteur);
        $echange->setIdCreateur($agentCon);
        if (isset($consultationId)) {
            $echange->setConsultationId(base64_decode($consultationId));
        }

        $echange->setOptionEnvoi($optionEnvoi);
        $echange->setFormat(Atexo_Config::getParameter('ECHANGE_PLATE_FORME'));
        $echange->setServiceId($serviceCon);
        $echange->setEmailExpediteur(trim($emailExpediteur));
        $echange->setIdTypeMessage(Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE'));

        $echange->save($connexion);
        $echangeBis = new CommonEchange();
        $echangeBis->setOrganisme($echange->getOrganisme());
        $echangeBis->setId($echange->getId());
        $echangeBis->setObjet(htmlspecialchars_decode($echange->getObjet()));
        $echangeBis->setCorps(htmlspecialchars_decode($echange->getCorps()));
        $echangeBis->setExpediteur($echange->getExpediteur());
        $echangeBis->setIdCreateur($echange->getIdCreateur());
        $echangeBis->setConsultationId($echange->getConsultationId());
        $echangeBis->setOptionEnvoi($echange->getOptionEnvoi());
        $echangeBis->setFormat($echange->getFormat());
        $echangeBis->setServiceId($echange->getServiceId());
        $echangeBis->setEmailExpediteur(trim($echange->getEmailExpediteur()));
        $echangeBis->setIdTypeMessage($echange->getIdTypeMessage());

        $echangedestinataire = new CommonEchangeDestinataire();
        $echangedestinataire->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $echangedestinataire->setMailDestinataire(trim($destinataire));
        $echangedestinataire->setUid((new Atexo_Message())->getUniqueId());
        if ($echangeBis->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
            $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_SANS_OBJET'));
        } else {
            $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE'));
        }
        $echangeBis->addCommonEchangeDestinataire($echangedestinataire);

        self::addPjsToEchangePjs($pjs, $echangeBis, $connexion);

        return $echangeBis;
    }

    /****
     * copie les pieces jointes sur la table Echangepiecejointe
     * @param $pjs les pieces jointes à copie
     */
    public function addPjsToEchangePjs($pjs, &$echange, $connexion)
    {
        if ($pjs) {
            foreach ($pjs as $onePJ) {
                $echangePieceJointe = new CommonEchangePieceJointe();
                $echangePieceJointe->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $echangePieceJointe->setNomFichier($onePJ->getNomFichier());
                $echangePieceJointe->setPiece($onePJ->getPiece());
                $echangePieceJointe->setHorodatage($onePJ->getHorodatage());
                $echangePieceJointe->setTaille($onePJ->getTaille());
                $echangePieceJointe->setIdMessage($echange->getId());

                $echangePieceJointe->save($connexion);
            }
        }
    }

    /**
     * afficher les acheteurs publics.
     */
    public function displayAcheteurPublic()
    {
        $idServiceRattach = Atexo_CurrentUser::getCurrentServiceId();
        $consultation = (new Atexo_Consultation())->retrieveConsultation(base64_decode($_GET['id']), Atexo_CurrentUser::getOrganismAcronym());
        $idServiceAssocie = $consultation->getServiceAssocieId();
        $listAcheteur = (new Atexo_PublicPurchaser())->retrievePublicPurchasersCons($idServiceRattach, $idServiceAssocie);
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        foreach ($listAcheteur as $oneAcheteur) {
            // $siteBoamp = '';
            $mail = '';
            /*if ($oneAcheteur->getBoampTarget()=='0') {
                $siteBoamp = ' - '.Prado::localize('TEXT_DEMONSTRATION');
            }
            else {
                $siteBoamp = ' - '.Prado::localize('TEXT_PRODUCTION');
            }*/
            if ('' != $oneAcheteur->getBoampMail()) {
                $mail = ' - '.$oneAcheteur->getBoampMail();
            }
            $dataSource[$oneAcheteur->getId()] = $oneAcheteur->getBoampLogin().$mail; //$oneAcheteur->getDenomination() . $mail . $siteBoamp;
        }
        $this->acheteurPublic->DataSource = $dataSource;
        $this->acheteurPublic->DataBind();
    }

    /*
     * Retour vers tableau de bord
     */
    public function actionBouttonAnnuler()
    {
        if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
        || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
            $link = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->response->redirect($link);
        } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $link = 'index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->response->redirect($link);
        }
    }

    public function onValider()
    {
        $value = $this->acheteurPublic->getSelectedValue();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultationComm = (new Atexo_Consultation())->retrieveConsultation(base64_decode($_GET['id']), null);
        $consultationComm->setCompteBoampAssocie($value);
        $consultationComm->save($connexionCom);
        $this->_consultation = $consultationComm;
        if ($this->_consultation->getCompteBoampAssocie()) {
            $this->choixCompteBoamp->setVisible(false);
            $this->buttonsChoixCompteBoamp->setVisible(false);
            $this->frameMol->setVisible(true);
            $this->errorPanel->visible = false;
            if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
            || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.
                   $this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } else {
                $this->boutonRetour->setNavigateUrl('#');
            }
        }
    }

    public function visibiliteLienMolDouble()
    {
        $condition1 = false;
        $visible = false;
        if ('ORME' == Atexo_Config::getParameter('PF_SHORT_NAME') || 'PLACE' == Atexo_Config::getParameter('PF_SHORT_NAME')) {
            if ('g7h' == Atexo_CurrentUser::getOrganismAcronym()) {
                $condition1 = true;
            }
        } else {
            $uidPFDouble = Atexo_Config::getParameter('UID_PF_MPE_DOUBLE');
            if ($uidPFDouble) {
                $condition1 = true;
            }
        }
        if ($condition1) {
            $dateDebut = $this->_consultation->getDateDebut();
            if ($dateDebut < Atexo_Config::getParameter('DATE_MEP_FONCTION_1') && $dateDebut >= Atexo_Config::getParameter('DATE_1_ERE_ANNONCE')) {
                $visible = true;
            }
        }
        $this->panelDoubleMole->setVisible($visible);
    }

    public function affichageFrameMolDouble($sender, $param)
    {
        if ($this->_consultation->getCompteBoampAssocie()) {
            $this->choixCompteBoamp->setVisible(false);
            $this->buttonsChoixCompteBoamp->setVisible(false);
            $this->errorPanel->visible = false;
            $showLoader = false;
            if ('lienAfficahgeFrameMolDouble' == $sender->id) {
                $this->frameMolDouble->setVisible(true);
                $this->frameMol->setVisible(false);
                $this->labelMEssage->setVisible(true);
                $this->lienAfficahgeFrameMol->setVisible(true);
                $this->panelDoubleMole->setVisible(false);
                $showLoader = true;
            }
            if ('lienAfficahgeFrameMol' == $sender->id) {
                $this->frameMolDouble->setVisible(false);
                $this->frameMol->setVisible(true);
                $this->labelMEssage->setVisible(false);
                $this->lienAfficahgeFrameMol->setVisible(false);
                $this->panelDoubleMole->setVisible(true);
                $showLoader = true;
            }
            if ($showLoader) {
                $this->scriptJs->Text = '<script>showLoader();</script>';
            }

            if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.
                    $this->_consultation->getIdTypeAvis().'&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $this->boutonRetour->setNavigateUrl('index.php?page=Agent.TableauDeBord&AS=0&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            } else {
                $this->boutonRetour->setNavigateUrl('#');
            }
        }
    }

    public function getCssStatut($statut)
    {
        $cssStatut = '';
        $cssStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => 'label label-default',
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => 'label label-primary',
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => 'label label-info',
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => 'label label-warning',
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => 'label label-success',
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => 'label label-danger',
            default => $cssStatut,
        };

        return $cssStatut;
    }

    public function getLibelleStatut($statut)
    {
        $libelleStatut = '';
        $libelleStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => Prado::localize('STATUT_ANNONCE_A_COMPLETER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => Prado::localize('STATUT_ANNONCE_COMPLET'),
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Prado::localize('STATUT_ANNONCE_ENVOYER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Prado::localize('STATUT_ANNONCE_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Prado::localize('STATUT_ANNONCE_PUBLIE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Prado::localize('STATUT_ANNONCE_REJETE'),
            default => $libelleStatut,
        };

        return $libelleStatut;
    }

    public function getLibelleDateStatut($statut)
    {
        $libelleDateStatut = '';
        $libelleDateStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => Prado::localize('DATE_ANNONCE_A_COMPLETER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => Prado::localize('DATE_ANNONCE_COMPLET'),
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Prado::localize('DATE_ANNONCE_ENVOYER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Prado::localize('DATE_ANNONCE_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Prado::localize('DATE_ANNONCE_PUBLIE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Prado::localize('DATE_ANNONCE_REJETE'),
            default => $libelleDateStatut,
        };

        return $libelleDateStatut;
    }

    public function getCssPictoStatut($statut)
    {
        $cssPictoStatut = '';
        $cssPictoStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => 'fa fa-pencil-square-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => 'fa fa-check-square-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => 'fa fa-share',
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => 'fa fa-clock-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => 'fa fa-check-circle',
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => 'fa fa-ban',
            default => $cssPictoStatut,
        };

        return $cssPictoStatut;
    }

    public function downloadPdfAnnonce($sende, $param)
    {
        try {
            $idAnnonce = $param->CommandParameter;
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $idDossier = $annonce->getIdDossierSub();
                Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($idDossier);
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur telechargement du document envoye : Erreur ='.$e->getMessage());
        }
    }

    public function envoyerAnnonce($sende, $param)
    {
        $logger = null;
        $annonce = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $idAnnonce = $param->CallbackParameter;
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org);
            $send = false;
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $lisSupportPub = $annonce->getListeSupportPub();
                if (!empty($lisSupportPub)) {
                    $send = Atexo_Publicite_AnnonceSub::envoyerAnnonce($annonce);
                }
            }
            if ($send) {
                $this->displayPubEchos($this->_consultation);
                $this->panelEchos->render($param->getNewWriter());
                $this->masquerMsg($param);
            } else {
                $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_ENVOI_ANNONCE'), $param);
                $logger->error("Erreur envoi de l'annonce ".$idAnnonce.' de la consultation '.$ref.' '.$org.' : Pas de support affecté');
            }
        } catch (Exception $e) {
            $logger->error("Erreur envoi de l'annonce ".($annonce instanceof CommonTAnnonceConsultation) ? $annonce->getId() : ''.' de la consultation '.print_r($this->_consultation, true).' '.$e->getMessage().' '.$e->getTraceAsString());
            throw $e;
        }
    }

    public function afficherMsgErreur($msg, $param = null)
    {
        $this->panelBlocErreur->setStyle("display:''");
        $this->messageErreur->setMessage($msg);
        if ($param) {
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    public function masquerMsg($param = null)
    {
        $this->panelBlocErreur->setStyle('display:none');
        $this->messageErreur->setMessage('');
        if ($param) {
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    public function aDroitEnvoyeAnnonce($support)
    {
        $droit = false;
        if ($support instanceof CommonTSupportAnnonceConsultation) {
            if ($support->getStatut() == Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET')) {
                $droit = true;
            }
        }

        return $droit;
    }
}
