<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_RegistreVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpAjoutRegistrePapier extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $registre = null;
        $this->customizeForm();
        $this->javascript->Text = '';
        if (!$this->IsPostBack) {
            $this->panelMessageErreur->setVisible(false);
            $this->panelErreur->setVisible(false);
            $this->panelResizeErreur->setVisible(false);
            $this->panelResizeContainer->setVisible(false);

            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')
            || Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier')
            || Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                if (isset($_GET['consultationId']) && isset($_GET['typeRegistre'])) {
                    $criteria = new Atexo_Consultation_CriteriaVo();
                    $criteria->setcalledFromPortail(false);
                    $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                    $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['consultationId']));
                    $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                    $consultation = (new Atexo_Consultation())->search($criteria);

                    if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                        $consultation = array_shift($consultation);

                        $this->panelResizeContainer->setVisible(true);
                        if ($_GET['typeRegistre'] != Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                            $this->Panelquestion->setVisible(false);
                            $this->Panelfichier->setVisible(false);
                        }
                        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                            if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && '1' == $consultation->getVarianteCalcule()) {
                                $this->panelVariante->setVisible(true);
                            }
                        }
                        if (isset($_GET['id'])) {
                            if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                                $quesionPapier = (new Atexo_Consultation_Register())->retrieveQuestions(Atexo_Util::atexoHtmlEntities($_GET['id']), [], Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), Atexo_CurrentUser::getCurrentOrganism(), false);
                                if (is_array($quesionPapier) && 1 == count($quesionPapier)) {
                                    $registre = array_shift($quesionPapier);
                                }
                            } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                                $retraitPapier = (new Atexo_Consultation_Register())->retrieveRetraitsPapier(Atexo_Util::atexoHtmlEntities($_GET['id']), [], Atexo_CurrentUser::getCurrentOrganism(), false);
                                if (is_array($retraitPapier) && 1 == count($retraitPapier)) {
                                    $registre = array_shift($retraitPapier);
                                }
                            } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                                $depotPapier = (new Atexo_Consultation_Register())->retrieveOffrePapier(Atexo_Util::atexoHtmlEntities($_GET['id']), [], Atexo_CurrentUser::getCurrentOrganism(), false);
                                if (is_array($depotPapier) && 1 == count($depotPapier)) {
                                    $registre = array_shift($depotPapier);
                                }
                            }
                            if ($registre instanceof Atexo_Consultation_RegistreVo) {
                                $this->nom->Text = $registre->getNomContact();
                                $this->prenom->Text = $registre->getPrenomContact();
                                $this->adresse1->Text = $registre->getAdresseContact();
                                $this->adresse2->Text = $registre->getAdresseContactSuite();
                                $this->nomEntreprise->Text = $registre->getNomEntreprise();
                                $this->horodatage->Text = $registre->getHorodatage();
                                if ($registre->getCodePostalContact()) {
                                    $this->cp->Text = $registre->getCodePostalContact();
                                }
                                $this->ville->Text = $registre->getVilleContact();
                                $numPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1(strtoupper($registre->getPaysEntreprise()));
                                if (!$numPays) {
                                    $this->france->Checked = true;
                                    $this->cp->enabled = true;
                                } elseif ($numPays == Atexo_Config::getParameter('ID_GEON2_FRANCE')) {
                                    $this->france->Checked = true;
                                    $this->cp->enabled = true;
                                    if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                                        $arraySiren = explode('-', $registre->getSirenEntreprise());
                                        //print_r($arraySiren);exit;
                                        $this->RcVille->SelectedValue = $arraySiren['0'];
                                        $this->RcNumero->Text = $arraySiren['1'];
                                    } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                                        $this->identifiantUnique->Text = $registre->getSirenEntreprise();
                                    } else {
                                        $this->siren->Text = $registre->getSirenEntreprise();
                                        $this->siret->Text = $registre->getNicEntreprise();
                                    }
                                } else {
                                    $this->etranger->Checked = true;
                                    $this->pays->SelectedValue = $numPays;
                                    $this->setViewState('numPays', $numPays);
                                    $this->idNational->Text = $registre->getSirenEntreprise();
                                    $this->javascript->Text = "<script>isCheckedShowDiv(document.getElementById('ctl0_CONTENU_PAGE_etranger'),'nonEtablieFrance');isCheckedHideDiv(document.getElementById('ctl0_CONTENU_PAGE_etranger'),'etablieFrance');enabledCodePostal('disabled');</script>";
                                }

                                $this->tel->Text = $registre->getTelephoneContact();
                                $this->fax->Text = $registre->getFaxContact();
                                $this->email->Text = $registre->getMailContact();
                                if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                                    $this->question->Text = $registre->getQuestion();
                                }
                                if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && '1' == $consultation->getVarianteCalcule()) {
                                        if ('1' == $registre->getOffreVariante()) {
                                            $this->offreVariante->checked = true;
                                        } else {
                                            $this->offreBase->checked = true;
                                        }
                                    }
                                }
                                if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                                    if (1 == $registre->getSupport()) {
                                        $this->formatpapier->checked = true;
                                    } elseif (2 == $registre->getSupport()) {
                                        $this->formatCDROM->checked = true;
                                    } elseif (3 == $registre->getSupport()) {
                                        $this->formatCDROM->checked = true;
                                        $this->formatpapier->checked = true;
                                    }
                                }
                            } else {
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelErreur->setVisible(true);
                                $this->container->setVisible(false);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUN_REGISTRE_QUESTION'));
                                $this->panelResizeErreur->setVisible(true);
                            }
                        } else {
                            $this->france->Checked = true;
                            $this->etranger->Checked = false;
                            if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                                $this->formatCDROM->checked = true;
                            }
                        }
                    } else {
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelErreur->setVisible(true);
                        $this->container->setVisible(false);
                        $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
                        $this->panelResizeErreur->setVisible(true);
                    }
                } else {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelErreur->setVisible(true);
                    $this->container->setVisible(false);
                    $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
                    $this->panelResizeErreur->setVisible(true);
                }
            } else {
                $this->panelMessageErreur->setVisible(true);
                $this->panelErreur->setVisible(true);
                $this->container->setVisible(false);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_AJOUT_REGISTRES_RERAIT_PAPIER'));
                $this->panelResizeErreur->setVisible(true);
            }
        }
    }

    public function onAjouterRegistreClick()
    {
        $idPays = null;
        $fichier = [];
        if ($this->fichier->HasFile) {
            //commencer le scan Antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->fichier->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->fichier->FileName.'"');

                return;
            }
            //Fin du code scan Antivirus
        }

        $registre = new Atexo_Consultation_RegistreVo();

        if (isset($_GET['id'])) {
            $registre->setIdRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']));
        } else {
            $registre->setIdRegistre(null);
        }
        //$quesionPapier->setTypeDepot(Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'));
        $registre->setReference(Atexo_Util::atexoHtmlEntities($_GET['consultationId']));
        $registre->setNomContact($this->nom->Text);
        $registre->setPrenomContact($this->prenom->Text);
        $registre->setAdresseContact($this->adresse1->Text);
        $registre->setAdresseContactSuite($this->adresse2->Text);
        $registre->setNomEntreprise($this->nomEntreprise->Text);
        $registre->setHorodatage(Atexo_Util::frnDateTime2iso($this->horodatage->Text));
        $registre->setCodePostalContact($this->cp->Text);
        $registre->setVilleContact($this->ville->Text);
        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            $support = 0;
            if ($this->formatCDROM->checked || $this->formatpapier->checked) {
                if ($this->formatCDROM->checked) {
                    $support += 2;
                }
                if ($this->formatpapier->checked) {
                    ++$support;
                }
                $registre->setTiragePlan(1);
            } else {
                $registre->setTiragePlan(0);
            }
            $registre->setSupport($support);
        }
        if ($this->france->Checked) {
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $registre->setSirenEntreprise($this->RcVille->SelectedValue.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->RcNumero->Text);
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $registre->setSirenEntreprise($this->identifiantUnique->Text);
            } else {
                $registre->setSirenEntreprise($this->siren->Text);
                $registre->setNicEntreprise($this->siret->Text);
            }
            $registre->setPaysEntreprise(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
            $registre->setAccronymePaysEntreprise(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
        } elseif ($this->etranger->Checked) {
            $registre->setNicEntreprise(null);
            $registre->setSirenEntreprise($this->idNational->Text);
            if ($this->pays->getSelectedValue()) {
                $idPays = $this->pays->getSelectedValue();
            } elseif (isset($_GET['id'])) {
                $idPays = $this->getViewState('numPays', null);
            }
            if ($idPays) {
                $geoN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($idPays);

                if (is_array($geoN2)) {
                    $geoN2 = array_shift($geoN2);
                    if ($geoN2 instanceof CommonGeolocalisationN2) {
                        $registre->setPaysEntreprise($geoN2->getDenomination1());
                        $registre->setAccronymePaysEntreprise($geoN2->getDenomination2());
                    } else {
                        throw new Atexo_Exception('unkown country');
                    }
                }
            }
        }

        $registre->setTelephoneContact($this->tel->Text);
        $registre->setFaxContact($this->fax->Text);
        $registre->setMailContact(trim($this->email->Text));

        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            $registre->setQuestion($this->question->Text);
            if ($this->fichier->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'question_'.session_id().time();
                while (is_file($infile)) {
                    $infile = Atexo_Config::getParameter('COMMON_TMP').'question_'.session_id().time();
                }
                if (move_uploaded_file($this->fichier->LocalName, $infile)) {
                    $atexoBlob = new Atexo_Blob();
                    $Org = Atexo_CurrentUser::getCurrentOrganism();
                    $idBlob = $atexoBlob->insert_blob($this->fichier->FileName, $infile, $Org);
                    $fichier['idFichier'] = $idBlob;
                    $fichier['nomFichier'] = $this->fichier->FileName;
                    $registre->setFichiers($fichier);
                }
                if (is_file($infile)) {
                    unlink($infile);
                }
            }
        }
        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['consultationId']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && '1' == $consultation->getVarianteCalcule()) {
                    if ($this->offreVariante->checked) {
                        $registre->setOffreVariante('1');
                    } else {
                        $registre->setOffreVariante('0');
                    }
                }
            }
        }

        (new Atexo_Consultation_Register())->saveRegistreVo($registre, Atexo_Util::atexoHtmlEntities($_GET['typeRegistre']), Atexo_CurrentUser::getCurrentOrganism());

        $this->userScript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackButton'])."').click();window.close();</script>";
    }

    public function getHeaderPopUp()
    {
        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            return Prado::localize('QUESTION_PAPIER');
        } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            return Prado::localize('RETRAIT_PAPIER');
        } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            return Prado::localize('DEPOT_PAPIER');
        }
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');

        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            $this->panelRc->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelIdentifiantUnique->setVisible(false);
            if (!$this->isPostBack) {
                $this->displayVilleRc();
            }
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelRc->setVisible(false);
            $this->panelSiren->setVisible(false);
            $this->validateurRc->setEnabled(false);
        } else {
            $this->panelRc->setVisible(false);
            $this->panelSiren->setVisible(true);
            $this->panelIdentifiantUnique->setVisible(false);
            $this->validateurRc->setEnabled(false);
        }
    }

    public function isConsultationWithEnvoiPostalComplementaire()
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['consultationId']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);

        if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);
            if (!$consultation->getTiragePlan()) {
                return false;
            }

            return true;
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
        $this->panelErreur->setVisible(true);
    }
}
