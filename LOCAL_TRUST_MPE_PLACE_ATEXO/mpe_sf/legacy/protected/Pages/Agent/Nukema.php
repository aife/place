<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Laminas\Http\Client;
use Laminas\Http\Request;

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

class Nukema extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $nukema = Atexo_Config::getParameter('DOMAINE_NUKEMA');
            $clientId = Atexo_Config::getParameter('UID_PF_MPE');
            $clientSecret = Atexo_Config::getParameter('SECRET_CLEF_NUKEMA');
            $uri = $nukema.'/trade/mpe-access?client_id='.$clientId.'&client_secret='.$clientSecret;

            $client = new Client();
            $client->setMethod(Request::METHOD_POST);
            $client->setUri($uri);
            $json = json_encode([
                'domain' => $_SERVER['HTTP_HOST'],
                'login' => (new Atexo_CurrentUser())->getLoginUser(),
                'firstName' => Atexo_CurrentUser::getFirstName(),
                'lastName' => Atexo_CurrentUser::getLastName(),
                'email' => Atexo_CurrentUser::getEmail(),
                'organization' => Atexo_CurrentUser::getOrganismDesignation(),
            ], JSON_THROW_ON_ERROR);
            $client->setEncType('application/json');
            $client->setRawBody($json);
            $request = $client->send();
            if (200 === $request->getStatusCode()) {
                $reponse = json_decode($request->getBody(), null, 512, JSON_THROW_ON_ERROR);
                $this->response->redirect($nukema.'/#/archived?access_token='.$reponse->access_token);
                exit;
            }
        } catch (\Exception $e) {
            Atexo_Util::write_file(
                Atexo_Config::getParameter('LOG_ERRORS'),
                date('Y-m-d H:i:s') . ' -- >  \n' . $e->getMessage() . '\n',
                'a+'
            );
        }
    }
}
