<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;

/**
 * Classe StatistiquesEntreprise.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesEntreprise extends MpeTPage
{
    public array $_tabValues = [];

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $organismes = [];
        $nombreDces = null;
        $nombreDcesParInscrit = null;
        $nombreQuestions = null;
        $nombreReponses = null;
        $nombreReponsesAvecSignature = null;
        $nombreReponsesSansSignature = null;
        $criteria = new Atexo_Statistiques_CriteriaVo();
        $criteria->setDateMiseEnLigneStart(date('Y').'-01-01');
        $nombreEntrepriseFrance = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(true);
        $nombreEntrepriseEtranger = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(false);
        $this->entreprise->Text = number_format(($nombreEntrepriseFrance + $nombreEntrepriseEtranger), '0', ',', ' ');
        $this->entrepriseFr->Text = number_format($nombreEntrepriseFrance, '0', ',', ' ');
        $this->entrepriseEtranger->Text = number_format($nombreEntrepriseEtranger, '0', ',', ' ');
        $this->identreprisetravaux->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')), '0', ',', ' ');
        $this->identreprisefournitures->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')), '0', ',', ' ');
        $this->identrepriseservice->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')), '0', ',', ' ');
        $nombreUtilisateurResponsable = (new Atexo_Statistiques_RequetesStatistiques())->getNbrResponsaleEntreprise(true);
        $this->idnbrresponsable->Text = number_format($nombreUtilisateurResponsable, '0', ',', ' ');
        $this->idnbrutilistaeur->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getNbrResponsaleEntreprise() + $nombreUtilisateurResponsable, '0', ',', ' ');
        $this->nbreinscritalerteelectronique->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getNbrInscritAlerteElectronique(), '0', ',', ' ');
        $this->nbrJust->Text = number_format((new Atexo_Statistiques_RequetesStatistiques())->getCountjustificatifs(), '0', ',', ' ');

        if (isset($_GET['allOrgs'])) {
            $organismes = Atexo_Organismes::retrieveOrganismes(true, null, true);
        } else {
            $org = new CommonOrganisme();
            $org->setAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            $organismes[] = $org;
        }
        if (is_array($organismes)) {
            $nombreDces = 0;
            $nombreDcesParInscrit = 0;
            $nombreReponses = 0;
            $nombreReponsesAvecSignature = 0;
            $nombreReponsesSansSignature = 0;
            $nombreQuestions = 0;

            foreach ($organismes as $organisme) {
                $criteria->setOrganisme($organisme->getAcronyme());

                $telechargements = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargementDCEStatEntreprise($criteria, false);

                if (is_array($telechargements)) {
                    $nombreDces += array_sum($telechargements);
                    $this->_tabValues['nbrDce'] += array_sum($telechargements);
                }
                $telechargementsParInscrit = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargementDCEStatEntreprise($criteria, true);
                if (is_array($telechargementsParInscrit)) {
                    $nombreDcesParInscrit += array_sum($telechargementsParInscrit);
                    $this->_tabValues['nbrDceParInscrit'] += array_sum($telechargementsParInscrit);
                }
                $questions = (new Atexo_Statistiques_RequetesStatistiques())->getNombreQuestionPosees($criteria, true, false, false);

                if (is_array($questions)) {
                    $nombreQuestions += array_sum($questions);
                    $this->_tabValues['nbrQestions'] += array_sum($questions);
                }
                $criteria->setReponsesAvecSignature(null);
                $reponses = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, false, false);

                if (is_array($reponses)) {
                    $nombreReponses += array_sum($reponses);
                    $this->_tabValues['nbreReponses'] += array_sum($reponses);
                }
                $criteria->setReponsesAvecSignature(true);
                $reponses = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, false, false);

                if (is_array($reponses)) {
                    $nombreReponsesAvecSignature += array_sum($reponses);
                    $this->_tabValues['nbreReponsesAvecSignature'] += array_sum($reponses);
                }
                $criteria->setReponsesAvecSignature(false);
                $reponses = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, false, false);

                if (is_array($reponses)) {
                    $nombreReponsesSansSignature += array_sum($reponses);
                    $this->_tabValues['nbreReponsesSansSignature'] += array_sum($reponses);
                }
                Atexo_Db::closeOrganism($organisme->getAcronyme());
            }
        }
        $this->idnombreretraitdce->Text = number_format($nombreDces, '0', ',', ' ');
        $this->idNbrretraitdctelechargeparinscrits->Text = number_format($nombreDcesParInscrit, '0', ',', ' ');
        $this->idNbrquestiondce->Text = number_format($nombreQuestions, '0', ',', ' ');
        $this->idNbrreponseselectronique->Text = number_format($nombreReponses, '0', ',', ' ');
        $this->idNbrreponseselectroniqueavecsignatureelectronique->Text = number_format($nombreReponsesAvecSignature, '0', ',', ' ');
        $this->idNbrreponseselectroniquesanssignatureelectronique->Text = number_format($nombreReponsesSansSignature, '0', ',', ' ');

        $unikDepotAuPlus = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesAyantFait1SeulEtUniqueDepotOffreConcernantAuPlus1eConsultation();
        $plsrsDepotsAuPlus = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesAyantFait1SeulEtUniqueDepotOffreConcernantAuPlus1eConsultation(true);

        $this->nbrEntrpPrimoDepos->Text = number_format($unikDepotAuPlus, '0', ',', ' ');
        $this->nbrEntrpMultiDepos->Text = number_format($plsrsDepotsAuPlus, '0', ',', ' ');
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            $nbrEntrpEA = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEA();
            $nbrEntrpSIAE = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseSIAE();
            $this->nbrEntrpEA->Text = number_format($nbrEntrpEA, '0', ',', ' ');
            $this->nbrEntrpSIAE->Text = number_format($nbrEntrpSIAE, '0', ',', ' ');
        }
    }

    public function genererExcel()
    {
        (new Atexo_GenerationExcel())->genererExcelStatistiquesEntreprise($this->_tabValues);
    }
}
