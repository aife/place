<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;

/**
 * Page d'accueil non authentifié des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class MesAlertes extends MpeTPage
{
    public ?bool $gestionCentralisee = null;
    public $idAgent;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->idAgent = Atexo_CurrentUser::getIdAgentConnected();

        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $this->gestionCentralisee = true;
        } else {
            $this->gestionCentralisee = false;
        }

        if (!$this->isPostBack) {
            $agent = $this->getAgent();
            $this->getAlertes($agent);
        }
    }

    /**
     * Retourne l'objet agent.
     */
    public function getAgent()
    {
        $this->idAgent = Atexo_CurrentUser::getIdAgentConnected();
        $agent = (new Atexo_Agent())->retrieveAgent($this->idAgent);

        return $agent;
    }

    /**
     * Rempli les alertes de l'agent.
     */
    public function getAlertes($agent)
    {
        if ($agent) {
            $this->checkOrUncheck($agent->getAlerteReponseElectronique(), 'regleAlerte_1');
            $this->checkOrUncheck($agent->getAlerteClotureConsultation(), 'regleAlerte_2');
            $this->checkOrUncheck($agent->getAlerteReceptionMessage(), 'regleAlerte_3');
            $this->checkOrUncheck($agent->getAlertePublicationBoamp(), 'regleAlerte_4');
            $this->checkOrUncheck($agent->getAlerteEchecPublicationBoamp(), 'regleAlerte_5');
            $this->checkOrUncheck($agent->getAlerteCreationModificationAgent(), 'regleAlerte_6');
            $this->checkOrUncheck($agent->getAlerteQuestionEntreprise(), 'regleAlerte_7');
            $this->checkOrUncheck($agent->getAlerteValidationConsultation(), 'regleAlerte_8');
            $this->checkOrUncheck($agent->getAlerteChorus(), 'regleAlerte_9');

            $this->checkOrUncheck($agent->getAlerteMesConsultations(), 'regleAlerteMesConsultations');
            $this->checkOrUncheck($agent->getAlerteConsultationsMonEntite(), 'regleAlerteConsultationsMonEntite');
            $this->checkOrUncheck($agent->getAlerteConsultationsDesEntitesDependantes(), 'regleAlerteConsultationsDesEntiteDependente');
            $this->checkOrUncheck($agent->getAlerteConsultationsMesEntitesTransverses(), 'regleAlerteConsultationsMesEntiteTransverse');
        }
    }

    /**
     * Select ou déselecte un checkbox.
     */
    public function checkOrUncheck($value, $element)
    {
        if ('1' == $value) {
            $this->$element->checked = true;
        } else {
            $this->$element->checked = false;
        }
    }

    /**
     * Annuler et retour à la page d'accueil agent.
     */
    public function annuler($sender, $param)
    {
        $this->response->redirect('agent');
    }

    /**
     * Enregistrer les alertes.
     */
    public function saveAlertes($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agent = $this->getAgent();
        if ($agent) {
            $agent->setAlerteReponseElectronique($this->setAlerte('regleAlerte_1'));
            $agent->setAlerteClotureConsultation($this->setAlerte('regleAlerte_2'));
            $agent->setAlerteReceptionMessage($this->setAlerte('regleAlerte_3'));
            $agent->setAlertePublicationBoamp($this->setAlerte('regleAlerte_4'));
            $agent->setAlerteEchecPublicationBoamp($this->setAlerte('regleAlerte_5'));
            $agent->setAlerteCreationModificationAgent($this->setAlerte('regleAlerte_6'));
            $agent->setAlerteQuestionEntreprise($this->setAlerte('regleAlerte_7'));
            $agent->setAlerteValidationConsultation($this->setAlerte('regleAlerte_8'));
            $agent->setAlerteChorus($this->setAlerte('regleAlerte_9'));

            $agent->setAlerteMesConsultations($this->setAlerte('regleAlerteMesConsultations'));
            $agent->setAlerteConsultationsMonEntite($this->setAlerte('regleAlerteConsultationsMonEntite'));
            $agent->setAlerteConsultationsDesEntitesDependantes($this->setAlerte('regleAlerteConsultationsDesEntiteDependente'));
            $agent->setAlerteConsultationsMesEntitesTransverses($this->setAlerte('regleAlerteConsultationsMesEntiteTransverse'));

            $agent->save($connexionCom);
        }

        $this->response->redirect('agent');
    }

    /**
     * retourne 0 ou 1 selon que l'element est choisi ou pas.
     *
     * @param string $idElement
     *
     * @return string 0 ou 1
     */
    public function setAlerte($idElement)
    {
        if ($this->$idElement->checked) {
            return '1';
        } else {
            return '0';
        }
    }
}
