<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Classe EnvoiCourrierElectroniqueSimple.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiCourrierElectroniqueSimple extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if (!$this->IsPostBack && !$_GET['IdEchange']) {
            $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueDeDemandeDeComplement->checked = true;
	   		$this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE');
        }
        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }
}
