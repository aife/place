<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_GestionCalendrier;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionMapa extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('GestionMapa')) {
            if (Atexo_CurrentUser::hasHabilitation('GestionMapa')) {
                if (!$this->IsPostBack) {
                    $this->fillRepeater();
                }
            }
        }
    }

    public function fillRepeater()
    {
        $listeMapa = (new Atexo_Consultation_ProcedureType())->retrieveAllMapa(Atexo_CurrentUser::getCurrentOrganism(), true, $this->getViewState('sensTri', 'ASC'));
        if (is_array($listeMapa)) {
            $this->repeaterMapa->DataSource = $listeMapa;
            $this->repeaterMapa->DataBind();
        } else {
            throw new Atexo_Exception('Expected function to return an array');
        }
    }

    public function refreshRepeater($sender, $param)
    {
        $this->fillRepeater();
        $this->panelMapa->render($param->NewWriter);
        $this->panelErreur->panelMessage->render($param->NewWriter);
    }

    public function isMapaActif($mapaActif)
    {
        if ($mapaActif == Atexo_Config::getParameter('MAPA_ACTIF')) {
            return true;
        } else {
            return false;
        }
    }

    public function getLibelleMontantMapa($idMontantMapa)
    {
        if ($idMontantMapa == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
            return Prado::localize('TEXT_INFERIEUR_90000_EURO_HT');
        } elseif ($idMontantMapa == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')) {
            return Prado::localize('TEXT_SUPERIEUR_90000_EURO_HT');
        }
    }

    public function deleteMapa($sender, $param)
    {
        $idProcedure = $param->CommandParameter;
        $consultations = (new Atexo_Consultation())->retrieveConsultationByProcedureType($idProcedure, Atexo_CurrentUser::getCurrentOrganism());
        if (is_array($consultations) && count($consultations) > 0) {
            $message = Prado::localize('SUPP_MAPA_IMPOSS');
            $this->panelErreur->setMessage($message);
            $this->panelErreur->showMessage();
        } else {
            $this->panelErreur->hideMessage();
            (new Atexo_Consultation_ProcedureType())->deleteProcedure($idProcedure, Atexo_CurrentUser::getCurrentOrganism());
            (new Atexo_GestionCalendrier())->deleteEtapeReferentiel($idProcedure, Atexo_CurrentUser::getCurrentOrganism());
            (new Atexo_GestionCalendrier())->deleteTransitionReferentiel($idProcedure, Atexo_CurrentUser::getCurrentOrganism());
        }
        (new Atexo_Consultation_ProcedureType())->reloadCacheTypeProcedureOrganisme();
    }

    public function Trier($sender, $param)
    {
        $sensTri = $this->getViewState('sensTri', 'ASC');
        $sensTri = ('ASC' == $sensTri) ? 'DESC' : 'ASC';
        $this->setViewState('sensTri', $sensTri);
    }
}
