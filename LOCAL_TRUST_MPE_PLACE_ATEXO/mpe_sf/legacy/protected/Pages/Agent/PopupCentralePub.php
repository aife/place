<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCompteCentrale;
use Application\Propel\Mpe\CommonCompteCentralePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;

/**
 * Popup.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupCentralePub extends MpeTPage
{
    private string|\Application\Propel\Mpe\CommonCompteCentrale $_compteCentrale = '';
    private $_connexionCom;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$this->IsPostBack) {
            $this->displatListeCentralePub();
            if (isset($_GET['idCompte']) && '' != $_GET['idCompte']) {
                $this->_compteCentrale = CommonCompteCentralePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompte']), Atexo_CurrentUser::getCurrentOrganism(), $this->_connexionCom);
                //  print_r($this->_compteCentrale);exit;
                $this->displayInfoComplementaire();
            }
        }

        $this->customizeForm();
    }

    public function displatListeCentralePub()
    {
        $this->repeaterDestinataire->DataSource = (new Atexo_Publicite_CentralePublication())->retreiveListCentralePub();
        $this->repeaterDestinataire->DataBind();
    }

    public function displayInfoComplementaire()
    {
        $this->emailAr->Text = $this->_compteCentrale->getMail();
        $this->telecopieur->Text = $this->_compteCentrale->getFax();
        $this->infosFacturation->Text = $this->_compteCentrale->getInfoCimplementaire();
    }

    public function onValider($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (isset($_GET['idCompte']) && $_GET['idCompte']) {
            $this->_compteCentrale = CommonCompteCentralePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompte']), $org, $this->_connexionCom);
        } else {
            $this->_compteCentrale = new CommonCompteCentrale();
        }
        $this->_compteCentrale->setOrganisme($org);
        if (isset($_GET['idService'])) {
            $this->_compteCentrale->setIdService(Atexo_Util::atexoHtmlEntities($_GET['idService']));
        }
        if ('' != $this->emailAr->Text) {
            $this->_compteCentrale->setMail(trim($this->emailAr->Text));
        }
        if ('' != $this->telecopieur->Text) {
            $this->_compteCentrale->setFax($this->telecopieur->Text);
        }
        if ('' != $this->infosFacturation->Text) {
            $this->_compteCentrale->setInfoCimplementaire($this->infosFacturation->Text);
        }
        $idCentrale = '';
        foreach ($this->repeaterDestinataire->getItems() as $oneItem) {
            if ($oneItem->destinataire->Checked) {
                $idCentrale = $oneItem->idDestinataire->Value;
            }
        }
        if ($idCentrale) {
            $this->_compteCentrale->setIdCentrale($idCentrale);
        }
        $this->_compteCentrale->save($this->_connexionCom);
        $this->scriptClose->Text = '<script>refreshRepeater();</script>';
    }

    public function selectedCentrale($id, $item)
    {
        //return false;
        if (isset($_GET['idCompte']) && $_GET['idCompte']) {
            $this->_compteCentrale = CommonCompteCentralePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idCompte']), Atexo_CurrentUser::getCurrentOrganism(), $this->_connexionCom);
            if ($this->_compteCentrale->getIdCentrale() == $id) {
                return true;
            } else {
                return false;
            }
        } else {
            if ('0' == $item) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
        }
    }
}
