<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFormulaire;
use Application\Propel\Mpe\CommonCriteresEvaluation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluation;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use DOMDocument;
use Prado\Prado;

/**
 * Synthese d'évaluation des enveloppes d'une consultation.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2011
 */
class PopUpSyntheseEvaluation extends MpeTPage
{
    public string $encoding = 'UTF-8';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
        if ($consultation instanceof CommonConsultation) {
            $this->IdConsultationSummary->setConsultation($consultation);
            if (!$this->isPostBack) {
                $this->infoFormulaire->text = '';
                $numLot = 0;
                if (isset($_GET['numLot']) && $_GET['numLot']) {
                    $numLot = Atexo_Util::atexoHtmlEntities($_GET['numLot']);
                    $this->infoFormulaire->text = Prado::localize('TEXT_LOT').' '.$numLot.' - ';
                }
                $typeEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);
                $libelleTypeEnv = '';
                switch ($typeEnveloppe) {
                    case Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'):
                        $libelleTypeEnv = Prado::localize('CANDIDATURES');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE'):
                        $libelleTypeEnv = Prado::localize('OFFRES');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'):
                        $libelleTypeEnv = Prado::localize('ENVELOPPE_ANONYMAT');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'):
                        $libelleTypeEnv = Prado::localize('OFFRES_TECHNIQUES');
                        break;
                }
                $this->infoFormulaire->text .= $libelleTypeEnv;
            }
        }
    }

    public function remplirRepeater($consultationId, $listEnveloppe)
    {
        $doc = new DOMDocument('1.0', $this->encoding);
        $racine = (new Atexo_Xml())->addElement($doc, $doc, 'criteresEvaluations');
        if (is_array($listEnveloppe)) {
            foreach ($listEnveloppe as $enveloppe) {
                if ($enveloppe instanceof CommonEnveloppe && $this->isPliOuvert($enveloppe->getStatutEnveloppe())) {
                    $nomEntreprise = $this->getNomEntreprise($enveloppe->getOffreId(), Atexo_CurrentUser::getCurrentOrganism()).'- El.'.$enveloppe->getNumPli();
                    $critereEavl = (new Atexo_Xml())->addElement($doc, $racine, 'critereEvaluation');
                    (new Atexo_Xml())->addElement($doc, $critereEavl, 'description', $nomEntreprise);
                    $criteresEval = (new Atexo_CriteresEvaluation())->retrieveCriteresEvaluationByRefConsAndTypeEnvAndLot($consultationId, Atexo_CurrentUser::getCurrentOrganism(), $enveloppe->getTypeEnv(), $enveloppe->getSousPli());
                    if ($criteresEval instanceof CommonCriteresEvaluation) {
                        (new Atexo_Xml())->addElement($doc, $critereEavl, 'uidCritereEval', Atexo_Util::builUidPlateforme($criteresEval->getId()));
                        $evalCritEval = (new Atexo_CriteresEvaluation())->retreiveEnveloppeCriteval($criteresEval->getId(), $enveloppe->getIdEnveloppeElectro());
                        if (!$evalCritEval) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $evalCritEval = new CommonEnveloppeCritereEvaluation();
                            $evalCritEval->setIdEnveloppe($enveloppe->getIdEnveloppeElectro());
                            $evalCritEval->setIdcritereevaluation($criteresEval->getId());
                            $evalCritEval->setRejet(0);
                            $evalCritEval->setStatutCritereEvaluation(Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME'));
                            $evalCritEval->setNoteTotale('0');
                            $evalCritEval->setCommentaireTotal('');
                            $evalCritEval->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                            $evalCritEval->save($connexion);
                        }
                        if ($evalCritEval instanceof CommonEnveloppeCritereEvaluation) {
                            (new Atexo_Xml())->addElement($doc, $critereEavl, 'uidEnvCritere', Atexo_Util::builUidPlateforme($evalCritEval->getId()));
                        }

                        $bordereauPrix = (new Atexo_FormConsultation())->getFormconsByTypeFormRefConsLotAndTypeEnv($consultationId, Atexo_CurrentUser::getCurrentOrganism(), $enveloppe->getTypeEnv(), Atexo_Config::getParameter('BORDEREAU_DE_PRIX'), $enveloppe->getSousPli());
                        if ($bordereauPrix instanceof CommonConsultationFormulaire) {
                            (new Atexo_FormConsultation())->getTotalHtBordereauPrixForFormCons($bordereauPrix->getId(), $enveloppe->getIdEnveloppeElectro());
                            $envForm = (new Atexo_FormConsultation())->retreiveEnveloppeformulaireCons($bordereauPrix->getId(), $enveloppe->getIdEnveloppeElectro());
                            if ($envForm instanceof CommonEnveloppeFormulaireConsultation) {
                                (new Atexo_Xml())->addElement($doc, $critereEavl, 'uidEnveloppeFormulaire', Atexo_Util::builUidPlateforme($envForm->getId()));
                            }
                        }
                    }
                }
            }
        }
        $docXml = $doc->saveXml();

        return base64_encode($docXml);
    }

    public function getNomEntreprise($offreID, $organisme)
    {
        $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($offreID, $organisme);

        return $offre->getNomEntrepriseInscrit();
    }

    public function isPliOuvert($statutEnveloppe)
    {
        if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME') ||
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') ||
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER')) {
            return false;
        } else {
            return true;
        }
    }

    public function remplirFileXml()
    {
        $listEnveloppe = null;
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
        if ($consultation instanceof CommonConsultation) {
            if (!$this->isPostBack) {
                $numLot = 0;
                if (isset($_GET['numLot']) && $_GET['numLot']) {
                    $numLot = Atexo_Util::atexoHtmlEntities($_GET['numLot']);
                }
                $typeOffre = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
                $typeEnveloppe = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);
                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference($consultationId);
                $criteriaResponse->setTypesEnveloppes($typeOffre);
                $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteriaResponse->setAdmissible(true);
                switch ($typeEnveloppe) {
                    case Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'):
                        $listEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE'):
                        $criteriaResponse->setSousPli($numLot);
                        $listEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'):
                        $criteriaResponse->setSousPli($numLot);
                        $listEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($consultationId, $numLot, $typeOffre, '', '', Atexo_CurrentUser::getCurrentOrganism());
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'):
                        $criteriaResponse->setSousPli($numLot);
                        $listEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques($consultationId, $numLot, $typeOffre, '', '', Atexo_CurrentUser::getCurrentOrganism());
                        break;
                }

                return $this->remplirRepeater($consultationId, $listEnveloppe);
            }
        }
    }

    /*
     * Permet de mettre à jour l'enveloppe de critère d'evaluation
     *
     */
    public function updateEnvCriteresEvaluation($sende, $param)
    {
        $arrayUidEnvCritereEval = explode('##_', $this->uidEnvCritereEvaluation->value);
        $arrayStatutEnv = explode('##_', $this->statutCritereEvaluation->value);
        $arrayNoteTotale = explode('##_', $this->noteTotale->value);
        $arrayTotalCommentaire = explode('##_', $this->totalCommentaireAcheteur->value);
        if (is_array($arrayUidEnvCritereEval)) {
            for ($i = 0; $i < count($arrayUidEnvCritereEval); ++$i) {
                $idEnvCritereEval = Atexo_Util::builIdFromUidPlateforme($arrayUidEnvCritereEval[$i]);
                (new Atexo_CriteresEvaluation())->updateEnveloppeCriteresEvaluationById($idEnvCritereEval, $arrayStatutEnv[$i], $arrayNoteTotale[$i], $arrayTotalCommentaire[$i]);
            }
        }
        $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackbutton'])."').click();window.close();</script>";
    }
}
