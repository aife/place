<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonJustificatifs;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CoffreFort;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CoffreFortElectronique extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $erreur = false;
        $typeDepot = strtoupper(Atexo_Util::atexoHtmlEntities($_GET['type']));
        $entreprise = null;
        if ('E' == $typeDepot) {
            $idEntreprise = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
        } elseif ('P' == $typeDepot) {
            $siretEntreprise = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            $siren = substr($siretEntreprise, 0, 9);
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($siren);
        } else {
            $erreur = true;
        }
        $idOffre = Atexo_Util::atexoHtmlEntities($_GET['idO']);
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['idC']);
        if (!$entreprise || !$idOffre || !$consultationId) {
            $erreur = true;
        }
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $showCoffreFort = false;
        if (!$erreur && (Atexo_Module::isEnabled('AccesAgentsCfeOuvertureAnalyse', $organisme) && (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('AccesReponses')))) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                if ('E' == $typeDepot) {
                    $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme);
                    $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                } else {
                    $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idOffre, $organisme);
                    $offre = CommonOffrePapierPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                }
                if (!is_array($enveloppes)) {
                    $enveloppes = [];
                }
                $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
                foreach ($enveloppes as $enveloppe) {
                    if ('E' == $typeDepot) {
                        $enveloppe->setCommonOffres($offre);
                    } else {
                        $enveloppe->setCommonOffrePapier($offre);
                    }
                    $nomEntreprise = (new Atexo_Consultation_Responses())->retrieveCompanyName($enveloppe, $typeEnveloppe, $organisme);
                    if ($nomEntreprise && '-' != $nomEntreprise) {
                        $showCoffreFort = true;
                    }
                }
                if ($showCoffreFort) {
                    if ($entreprise->getSiren()) {
                        $this->PanelSiege->setCalledFor('France');
                        $this->PanelSiege->setSiren($entreprise->getSiren());
                    } else {
                        $this->PanelSiege->setCalledFor('Etrangere');
                        $this->PanelSiege->setIdNational($entreprise->getSirenEtranger());
                    }
                    $this->PanelSiege->setRaisonSociale($entreprise->getNom());
                    $this->PanelSiege->setLieuEtablissement($entreprise->getPaysAdresse());
                    if (!$this->IsPostBack) {
                        $this->setViewState('idEntreprise', $entreprise->getId());
                        $this->setViewState('sortBy', 'DocumentName');
                        $this->setViewState('sortOrder', 'ASC');
                        if (Atexo_Module::isEnabled('AssocierDocumentsCfeConsultation')) {
                            $piecesConsultationDocumentCfe = (new Atexo_Entreprise_CoffreFort())->retrievePiecesConsultationDocumentCfeByIdEntreprise($entreprise->getId(), $consultationId, Atexo_CurrentUser::getCurrentOrganism(), $this->getViewState('sortBy'), $this->getViewState('sortOrder'));
                            $dataDocumentsCfe = [];
                            foreach ($piecesConsultationDocumentCfe as $onePiece) {
                                $dataDocumentsCfe[] = self::cloneAsJustificatifs($onePiece);
                            }
                            if (!is_array($dataDocumentsCfe)) {
                                $dataDocumentsCfe = [];
                            }
                            if (count($dataDocumentsCfe) > 0) {
                                $this->tableauDocsSpecifiquesEntreprise->setDataSource($dataDocumentsCfe);
                            } else {
                                $this->aucunDocRattacheConsultation->setVisible(true);
                                $this->aucunDocRattacheConsultation->Text = Prado::localize('TEXT_AUCUN_DOC_DISPONIBLE');
                            }
                        }
                        $dataDocumentsCfeVisibleAgent = (new Atexo_Entreprise_CoffreFort())->retrievePiecesCoffreFortByIdEntreprise($entreprise->getId(), $this->getViewState('sortBy'), $this->getViewState('sortOrder'));
                        if (!is_array($dataDocumentsCfeVisibleAgent)) {
                            $dataDocumentsCfeVisibleAgent = [];
                        }
                        if (count($dataDocumentsCfeVisibleAgent) > 0) {
                            $this->tableauDocsJustificatifsVisiblesAgent->setDataSource($dataDocumentsCfeVisibleAgent);
                        } else {
                            $this->aucunDocCoffreFort->setVisible(true);
                            $this->aucunDocCoffreFort->Text = Prado::localize('TEXT_AUCUN_DOC_DISPONIBLE');
                        }
                    }
                } else {
                    $erreur = true;
                }
            } else {
                $erreur = true;
            }
        } else {
            $erreur = true;
        }
        if ($erreur) {
            $this->errorPart->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->panelMessageErreur->setMessage(Prado::localize('NON_AUTORISE_A_VOIR_DOCUMENT'));
        }
    }

    public function downloadPiece($sender, $param)
    {
        $nomFichier = $param->CommandParameter;
        $idFichier = $param->CommandName;

        $atexoBlob = new Atexo_Blob();
        $tailleFichier = $atexoBlob->getTailFile($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        if ('.zip' == strrchr($nomFichier, '.')) {
            header('Content-Type: application/zip');
        } else {
            header('Content-Type: application/octet-stream');
        }
        $nomFichier = str_replace('\\', '', $nomFichier);
        header('Content-Disposition: attachment; filename="'.$nomFichier.'";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.$tailleFichier);

        $atexoBlob->send_blob_to_client($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    public function sortDocuments()
    {
        $sortOrder = $this->getViewState('sortOrder');
        $sortOrder = ('ASC' == $sortOrder) ? 'DESC' : 'ASC';
        $this->setViewState('sortOrder', $sortOrder);

        if (Atexo_Module::isEnabled('AssocierDocumentsCfeConsultation')) {
            $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesConsultationDocumentCfeByIdEntreprise($this->getViewState('idEntreprise'), Atexo_Util::atexoHtmlEntities($_GET['idC']), Atexo_CurrentUser::getCurrentOrganism(), $this->getViewState('sortBy'), $this->getViewState('sortOrder'));
            $dataDocumentsCfe = [];
            foreach ($piecesEntreprises as $onePiece) {
                $dataDocumentsCfe[] = self::cloneAsJustificatifs($onePiece);
            }
            if (!is_array($dataDocumentsCfe)) {
                $dataDocumentsCfe = [];
            }

            $this->tableauDocsSpecifiquesEntreprise->setDataSource($dataDocumentsCfe);
        }

        $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesCoffreFortByIdEntreprise($this->getViewState('idEntreprise'), $this->getViewState('sortBy'), $this->getViewState('sortOrder'));
        if (!is_array($piecesEntreprises)) {
            $piecesEntreprises = [];
        }

        $this->tableauDocsJustificatifsVisiblesAgent->setDataSource($piecesEntreprises);
    }

    public function cloneAsJustificatifs($objetConsultationDocumentCfe)
    {
        $objetJustificatif = new CommonJustificatifs();

        $objetJustificatif->setIntituleJustificatif($objetConsultationDocumentCfe->getNomFichier());
        $objetJustificatif->setNom($objetConsultationDocumentCfe->getTypeDocument());
        $objetJustificatif->setDateFinValidite($objetConsultationDocumentCfe->getDateFinValidite());
        $objetJustificatif->setJustificatif($objetConsultationDocumentCfe->getIdBlob());
        $objetJustificatif->setTaille($objetConsultationDocumentCfe->getTailleDocument());

        return $objetJustificatif;
    }
}
