<?php

namespace Application\Pages\Agent;

use App\Service\WebServices\WebServicesExec;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonChorusPjPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTChorusCcag;
use Application\Propel\Mpe\CommonTChorusCcagQuery;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTDonneesConsultationQuery;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_DocumentExterneVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_GenerationDocumentsChorus;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Helios\Atexo_Helios_PiecesAttributaires;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use AtexoCrypto\Dto\Signature;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ChorusDetailEchanges extends MpeTPage
{
    private $_donneesConsultation = '';
    private $_organisme = '';
    private $_echange = '';
    private $_decision = '';
    private $_contrat = '';
    private $_arborescencePiecesNotif = '';
    private $_ficheNavette;
    private $_lots;
    private $_serviceId = '';
    private ?array $_contratExec = null;

    public function setEchange($v)
    {
        $this->_echange = $v;
    }

    public function setOrganisme($v)
    {
        $this->_organisme = $v;
    }

    public function setDonneesConsultation($v)
    {
        $this->_donneesConsultation = $v;
    }

    public function getDonneesConsultation()
    {
        return $this->_donneesConsultation;
    }

    public function getIdContrat()
    {
        return $this->getViewState('idContrat');
    }

    public function setIdContrat($value)
    {
        $this->setViewState('idContrat', $value);
    }

    /**
     * @return string
     */
    public function getServiceId()
    {
        return $this->_serviceId;
    }

    /**
     * @param string $serviceId
     */
    public function setServiceId($serviceId)
    {
        $this->_serviceId = $serviceId;
    }

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $logger = Atexo_LoggerManager::getLogger('app');

        try {
            $this->setIdContrat(base64_decode($_GET['idContrat']));
            $this->panelMessageErreur->setVisible(false);

            if (isset($_GET['idEchange']) || isset($_GET['uuid'])) {
                $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
                $this->setServiceId(Atexo_CurrentUser::getCurrentServiceId());

                $contratExec = null;
                $uuidContratExec = null;
                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                    $uuidContratExec = $_GET['uuid'];
                    /** @var $execWS WebServicesExec */
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);
                    $this->_contratExec = $contratExec;
                }

                $idEchange = Atexo_Util::atexoHtmlEntities($_GET['idEchange']);
                $this->_echange = (new Atexo_Chorus_Echange())->retreiveEchangeByIdJoinDecisionenveloppe($idEchange, $this->_organisme);

                if ($this->_echange instanceof CommonChorusEchange) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

                    $this->numOrdre->Text = $this->_echange->getNumOrdre();

                    if (!Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $this->_contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($this->_echange->getIdDecision(), $connexion);
                    }

                    if ($this->_contrat instanceof CommonTContratTitulaire || $contratExec) {
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                            $lotConsContrat = [];

                            if ($uuidContratExec) {
                                $donneeConsultationQuery = new CommonTDonneesConsultationQuery();
                                $donneeConsultation = $donneeConsultationQuery->findOneByUuidExterneExec($uuidContratExec);

                                $this->setDonneesConsultation($donneeConsultation);
                            }

                            $this->numMarche->Text = $contratExec['numero'] ?? '';
                            $this->setDesabledCategorieAchat();
                        } else {
                            $this->setDonneesConsultation($this->_contrat->getDonneesConsultation($connexion, $logger));

                            $this->numMarche->Text = $this->_contrat->getNumeroContrat();
                            $this->setDesabledCategorieAchat();
                            $lotConsContrat = (new Atexo_Consultation_Contrat())->getListeConsLotsContratByIdContrat($this->_contrat->getIdContratTitulaire());
                        }

                        if (is_array($lotConsContrat)) {
                            foreach ($lotConsContrat as $oneEl) {
                                if ($oneEl instanceof CommonTConsLotContrat) {
                                    $lots[] = $oneEl->getLot();
                                }
                            }
                        }
                        $this->_lots = $lots;

                        // @TODO KBE $this->_arborescencePiecesNotif = self::genererArborescencePiecesNotification();
                        if (!$this->IsPostBack) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $this->_echange = CommonChorusEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idEchange']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                            $this->displayCategorieOA();
                            if ($this->isContratAcSad()) {
                                $this->displayTypeContrat();
                                $this->displaySelectedValueTypeContrat();
                            } else {
                                $this->fillCategorieGA();
                                $this->displayTypeMarche();
                                $this->displayTypeGroupement();
                                $this->displayRegroupementCompatable();
                                $this->displayCcagReference();
                                $this->chargerRepeatCodePays();
                                $this->displayFormePrix();
                                $this->displayNaturesActeJuridique();
                                $this->displayProcedureType();
                                $this->displayTypeAvance();
                                $this->displayPropositionsRecues();
                                //pre-remplissage du type de procedure
                                $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById($this->getIdTypeProcedureDonneesConsultation(), Atexo_CurrentUser::getCurrentOrganism());
                                if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
                                    $this->procedureType->SelectedValue = $typeProcedure->getTagNameChorus();
                                }
                            }
                            $this->displayEchange();
                            // Pièces Externe
                            $this->displayPiecesExternes();
                            //Afficher les pièces d'ouverture et analyse
                            $this->displayPiecesOuvertureAnalyse();
                            /* A REVOIR **/
                            $this->displayIdentifiantsUniques();

                            if (!$this->isContratAcSad()) {
                                $this->displayAutresInformations();
                                //Permet de charger les informations de la fiche de navette
                                $this->displayFicheNavette();

                                if ($this->isTypeEnvoiFicheModificative()) {
                                    $ficheModificative = CommonTChorusFicheModificativeQuery::create()->filterByIdEchange($this->_echange->getId())->findOne();
                                    if (!$ficheModificative instanceof CommonTChorusFicheModificative) {
                                        $ficheModificative = self::addFicheModificative($this->_echange, $connexion);
                                    }
                                    $this->setViewState('FicheModificativeObject', $ficheModificative);
                                    $this->FicheModificative->setFicheModificativeObjet($ficheModificative);

                                    if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                                        $this->FicheModificative->setContratExecArray($this->_contratExec);
                                    } else {
                                        $this->FicheModificative->setContratObjet($this->_contrat);
                                    }

                                    $this->FicheModificative->setChorusEchangeObjet($this->_echange);
                                    $this->FicheModificative->onInitComposant();
                                    self::displayFicheModificative();
                                }

                                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $contratExec) {
                                    $this->finMarche->Text = $contratExec['datePrevisionnelleFinMarche']
                                        ? (new \DateTime($contratExec['datePrevisionnelleFinMarche']))->format('d/m/Y')
                                        : null
                                    ;
                                    $this->dateFinMarcheReelle->Text =  $contratExec['dateFinContrat']
                                        ? (new \DateTime($contratExec['dateFinContrat']))->format('d/m/Y')
                                        : null
                                    ;
                                    $this->dateNotification->Text = $contratExec['datePrevisionnelleNotification']
                                        ? (new \DateTime($contratExec['datePrevisionnelleNotification']))->format('d/m/Y')
                                        : null
                                    ;
                                    $this->dateNotificationReelle->Text = $contratExec['dateNotification']
                                        ? (new \DateTime($contratExec['dateNotification']))->format('d/m/Y')
                                        : null
                                    ;
                                } elseif ($this->_contrat instanceof CommonTContratTitulaire) {
                                    //Renseignement de la date de notification
                                    if ($this->_contrat->getDatePrevueNotification()) {
                                        $this->dateNotification->Text = Atexo_Util::iso2frnDate($this->_contrat->getDatePrevueNotification());
                                    }
                                    if ($this->_contrat->getDatePrevueFinContrat()) {
                                        $this->finMarche->Text = Atexo_Util::iso2frnDate($this->_contrat->getDatePrevueFinContrat());
                                    }
                                    if ($this->_contrat->getDateNotification()) {
                                        $this->dateNotificationReelle->Text = Atexo_Util::iso2frnDate($this->_contrat->getDateNotification());
                                    }
                                    if ($this->_contrat->getDateFinContrat()) {
                                        $this->dateFinMarcheReelle->Text = Atexo_Util::iso2frnDate($this->_contrat->getDateFinContrat());
                                    }
                                }
                            }
                        } else {
                            //Sauvegarde des pièces du DCE
                            $this->sauvegarderPiecesDceSelectionnees();
                            //Sauvegarde pièces de réponse
                            $this->sauvegarderPiecesReponseSelectionnees();
                            //Début sauvegarde pièces notification
                            $this->sauvegarderPiecesNotificationSelectionnees();
                            //Sauvegarde pièces attachées à la consultation
                            $this->sauvegarderAttacheesConsultationSelectionnees();
                            // Pièces Externe
                            $this->displayPiecesExternes();

                            //arrayIndexSelectedFilesDumeAcheteur | arrayIndexSelectedFilesDumeOE
                            $this->sauvegarderPieceDumeSelectionnees('arrayIndexSelectedFilesDumeAcheteur', $this->idsBlobDumeAcheteur->Value);
                            $this->sauvegarderPieceDumeSelectionnees('arrayIndexSelectedFilesDumeOE', $this->idsBlobDumeOe->Value);

                            if (!$this->isContratAcSad()) {
                                $this->saveCodePaysTitulairesCoTraitants();
                                $this->chargerRepeatCodePays();
                                $this->chargerListePays();
                                $script = $this->displayCodePaysCoTitulaire($this->getViewState('codePaysTitulaire'), $this->getViewState('codePaysCoTitulaires'));
                                $this->javascript->Text .= '<script>'.$script.'</script>';
                            }
                        }

                        if ($this->isContratAcSad()) {
                            $this->compterCaracteresRestantsIntituleObjet();
                        }

                        //Affichage des pièces du DCE
                        $this->displayPiecesDce($this->getViewState('arrayIndexSelectedFilesDce'));

                        //Pièce DUME
                        $this->displayPiecesDume(
                            Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'),
                            $this->getViewState('arrayIndexSelectedFilesDumeAcheteur')
                        );

                        $this->displayPiecesDume(
                            Atexo_Config::getParameter('TYPE_DUME_OE'),
                            $this->getViewState('arrayIndexSelectedFilesDumeOE')
                        );

                        //Afficher les pièces de la reponse
                        $itemEnvZip = $this->getViewState('selectedFilesForEnveloppeZip');
                        $itemSignEnvZip = $this->getViewState('selectedFilesForSignatureEnveloppeZip');
                        $itemEnvNonZip = $this->getViewState('idsBlobEnveloppesNonZip');
                        $itemSignEnvNonZip = $this->getViewState('idsBlobSignEnveloppeNonZip');
                        $this->displayPiecesReponse($itemEnvZip, $itemSignEnvZip, $itemEnvNonZip, $itemSignEnvNonZip);
                        //Affichage des pièces de notification
                        $this->displayPiecesNotification($this->getViewState('indexSelectedFilesNotif'));
                        // Affichage des Document extèrne de la consultation
                        $this->displayDocumentExterne();

                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                            $contratFicheModificative = $this->_contratExec;
                        } else {
                            $contratFicheModificative = $this->_contrat;
                        }

                        if ((new Atexo_Chorus_Util())->isTypeEnvoiFicheModificative($contratFicheModificative, $this->_echange)) {
                            $this->scriptGestionOnglet->Text = '<script>gererAffichageOngletsTypeEnvoiChorusFicheModificatif();</script>';
                        } elseif ((new Atexo_Chorus_Util())->isTypeEnvoiPiecesJointes($contratFicheModificative, $this->_echange)) {
                            $this->scriptGestionOnglet->Text = '<script>gererAffichageOngletsTypeEnvoiChorusPiecesJointes();</script>';
                        }
                    } else {
                        $erreur = "Impossible d'accéder à la page.";
                        $this->afficherErreur($erreur);
                        $logger->error($erreur.PHP_EOL."Le contrat n'est pas une instance de CommonChorusEchange : ".print_r($this->_contratExec, true));

                        return;
                    }
                } else {
                    $erreur = "Impossible d'accéder à la page.";
                    $this->afficherErreur($erreur);
                    $logger->error($erreur.PHP_EOL."L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($this->_echange, true));

                    return;
                }
            } else {
                $erreur = "Impossible d'accéder à la page. Paramètre manquant.";
                $this->afficherErreur($erreur);
                $logger->error($erreur.PHP_EOL."Le parametre de l'url 'idEchange' est vide ou manquent");

                return;
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors du chargement de la page.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * affiche l'arboressance du DCE.
     */
    public function displayPiecesDce($dceItems = null)
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $dce = (new Atexo_Consultation_Dce())->getDce($this->getReferenceDonneesConsultation(), $this->_organisme);
                $scriptDce = '';
                if ($dce instanceof CommonDCE) {
                    $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $this->_organisme);
                    if (!$dceItems) {
                        $dceItems = $this->_echange->getDceItems();
                    }
                    $arDceItems = explode(';', $dceItems);
                    if (is_array($arDceItems)) {
                        $scriptDce = (new Atexo_Zip())->getArborescence($blobResource, $dce->getDce(), $arDceItems, null, 'dce_item', false, false, true);
                    }
                }

                if ('' != $scriptDce) {
                    $this->scriptDce->Text = $scriptDce;
                }
            } catch (\Exception $e) {
                $logger->error("Erreur lors de l'affichage des pieces du DCE : dceItems = $dceItems ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    /**
     * Boutton annuler
     * redirection vers la page des echanges.
     *  */
    public function navigateUrlAnnuler($sender, $param)
    {
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $this->response->redirect('index.php?page=Agent.ChorusEchanges&uuid=' . $this->_contratExec['uuid'] . (isset($_GET['fromCons']) ? '&fromCons' : '') . '&action=decision');
        } else {
            $this->response->redirect(
                'index.php?page=Agent.ChorusEchanges&idContrat=' . base64_encode(
                    $this->_contrat->getIdContratTitulaire()
                ) . (isset($_GET['fromCons']) ? '&fromCons' : '') . '&action=decision'
            );
        }
    }

    /**
     *  Autres pièces externes :.
     */
    public function displayPiecesExternes()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $piecesexternes = (new Atexo_Chorus_Echange())->retreivePiecesExternes(Atexo_Util::atexoHtmlEntities($_GET['idEchange']), $this->_organisme);
            $this->repeaterPiecesExternes->DataSource = $piecesexternes;
            $this->repeaterPiecesExternes->dataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage des pieces externes.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Rafrechir le tableau des pièces éxternes.
     */
    public function onCallBackRefreshPieceExternes($sender, $param)
    {
        $this->displayPiecesExternes();
        $this->panelPiecesExternes->render($param->getNewWriter());
    }

    public function deletePieceExterne($sender, $param)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($param->CommandName) {
                CommonChorusPjPeer::doDelete([$param->CommandName, Atexo_CurrentUser::getCurrentOrganism()], $connexion);
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la suppression de la piece externe : idPj = '.$param->CommandName.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function existingDocumentExterne($idDoc)
    {
        if ($this->_echange instanceof CommonChorusEchange && '' != $this->_echange->getIdsPiecesExternes()) {
            $arIds = explode(';', $this->_echange->getIdsPiecesExternes());
            if (in_array($idDoc, $arIds)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * retourne la taille du blob.
     */
    public function getFileTaille($idBlob)
    {
        return Atexo_Util::getTailleBlob($this->_organisme, $idBlob);
    }

    /**
     * @param null $itemEnvZip
     * @param null $itemSignEnvZip
     * @param null $itemEnvNonZip
     * @param null $itemSignEnvNonZip
     *
     * @throws Exception
     * @throws PropelException
     */
    public function displayPiecesReponse($itemEnvZip = null, $itemSignEnvZip = null, $itemEnvNonZip = null, $itemSignEnvNonZip = null)
    {
        $enveloppesOffre = null;
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            try {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->info('Debut affichage des pieces de reponse titulaire');

                if ($this->_echange instanceof CommonChorusEchange) {
                    if ($this->_contrat instanceof CommonTContratTitulaire) {
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                        $listeLots = $this->_contrat->getLots();
                        if (!empty($listeLots)) {
                            $c = new Criteria();
                            $c->add(CommonEnveloppePeer::OFFRE_ID, $this->_contrat->getIdOffre());
                            $c->add(CommonEnveloppePeer::TYPE_ENV, Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                            if (1 == (is_countable($listeLots) ? count($listeLots) : 0) && '0' == $listeLots[0]) {
                                $c->add(CommonEnveloppePeer::SOUS_PLI, '0');
                            } elseif (1 == (is_countable($listeLots) ? count($listeLots) : 0) && '0' != $listeLots[0]) {
                                $c->add(CommonEnveloppePeer::SOUS_PLI, $listeLots[0]);
                            } else {
                                //TODO : ici nous avons affiché les pieces correspondant au premier lot de la liste, en attendant une solution fonctionnelle
                                $c->add(CommonEnveloppePeer::SOUS_PLI, $listeLots[0]);
                            }
                            $c->add(CommonEnveloppePeer::ORGANISME, $this->_organisme);
                            $enveloppesOffre = CommonEnveloppePeer::doSelectOne($c, $connexion);
                        } else {
                            $logger->error('Aucune consultation ou lot trouve pour ce contrat : contrat_id = '.$this->_contrat->getIdContratTitulaire().', id_echange = '.$this->_echange->getId().', Organisme = '.$this->_echange->getOrganisme());
                        }
                        if ($enveloppesOffre instanceof CommonEnveloppe) {
                            $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($enveloppesOffre->getIdEnveloppeElectro(), Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), $this->_organisme);
                            if ($piecesAttributaires) {
                                $this->scriptArborescenceEnveloppe->Text = '';
                                $this->scriptArborescenceEnveloppeNonZip->Text = '';
                                $firstAcces = false;
                                $idsBlobNonZip = '';
                                $idsBlobZip = '';
                                $idsBlobSignatureZip = '';
                                $idsBlobSignatureNonZip = '';
                                $infoBulle = "<span class=\"float-left\"><img src=\"themes/images/picto-info.gif\"
                                            onmouseover=\"afficheBulle('msgInfoBulle', this)\"
                                            onmouseout=\"cacheBulle('msgInfoBulle')\"
                                            class=\"picto-info-intitule\"
                                            alt=\"Info-bulle\"
                                            style=\"margin-top:0;\"
                                            title=\"Info-bulle\" /> </span>";
                                foreach ($piecesAttributaires as $OnePiece) {
                                    if ($OnePiece instanceof CommonFichierEnveloppe && $OnePiece->getIdBlob()) {
                                        if ('ACE' == $OnePiece->getTypeFichier()) {
                                            $this->panelACE->setVisible(true);
                                            $this->idACE->Value = $OnePiece->getIdFichier();
                                            $disabledCss = 'check-bloc';
                                            if ($OnePiece->getTailleFichier() > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                                $this->acteEngagement->enabled = false;
                                                $this->infoBulleActeEngagement->setVisible(true);
                                                $disabledCss .= ' disabled-txt';
                                            }
                                            $this->acteEngagement->Text = $OnePiece->getNomFichier().' ('.Atexo_Util::arrondirSizeFile($OnePiece->getTailleFichier() / 1024).')';
                                            $this->acteEngagement->cssClass = $disabledCss;
                                            if ('' != $OnePiece->getSignatureFichier()) {
                                                $this->panelACESign->setVisible(true);
                                                $fileContent = base64_decode($OnePiece->getSignatureFichier());
                                                $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().date('Ymdhis').time();
                                                mkdir($tmpPath);
                                                $path = $tmpPath.'/'.$OnePiece->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier());
                                                Atexo_Util::write_file($path, $fileContent, 'w');
                                                $sizeFile = filesize($path);
                                                unlink($path);
                                                $disabledCssSignature = 'check-bloc';
                                                if ($sizeFile > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                                    $this->signActeEngagement->enabled = false;
                                                    $this->infoBulleSignActeEngagement->setVisible(true);
                                                    $disabledCssSignature .= ' disabled-txt';
                                                }
                                                $this->signActeEngagement->Text = "Signature titulaire de l'Acte : ".$OnePiece->getCnSubject($this->getSignatureOffresDonneesConsultation(), $OnePiece->getTypeSignature()).
                                                    ' ('.Atexo_Util::GetSizeName($sizeFile).')';
                                                $this->signActeEngagement->cssClass = $disabledCssSignature;
                                            }
                                        } else {
                                            $this->panelPRI->setVisible(true);
                                            $idBlob = $OnePiece->getIdBlob();
                                            $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $this->_organisme);
                                            $blob_file = $blobResource['blob']['pointer'];
                                            if ('zip' == Atexo_Util::getExtension($OnePiece->getNomFichier())) {
                                                if ((new Atexo_Zip())->verifyZipIntegrity($blob_file)) {
                                                    //$arrayIndexItems = explode(';', $this->_echange->getIdsEnvItems());
                                                    $tableauIndexItems = [];
                                                    $tableauIndexSignature = [];
                                                    if (!$itemEnvZip) {
                                                        $itemEnvZip = $this->_echange->getIdsEnvItems();
                                                    }
                                                    if (!$itemSignEnvZip) {
                                                        $itemSignEnvZip = $this->_echange->getIdsEnvSignItems();
                                                    }
                                                    if (!str_contains($itemEnvZip, '#')) {//Cas des traitements existants
                                                        $tableauIndexItems = explode(';', $itemEnvZip);
                                                        $tableauIndexSignature = explode(';', $itemSignEnvZip);
                                                    } else {//Nouveaux cas
                                                        $listeIndex = explode('#', $itemEnvZip);
                                                        foreach ($listeIndex as $index) {
                                                            if ($listeIndex[0] != $index) {
                                                                $arrayIndex = explode('_', $index);
                                                                if ($arrayIndex[0] == $idBlob) {
                                                                    $tableauIndexItems = explode(';', $arrayIndex[1]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $s = (new Atexo_Zip())->getArborescence($blobResource, $idBlob, $tableauIndexItems, null, 'env_item_'.$OnePiece->getIdBlob(), false, false, true);
                                                    $idsBlobZip = $idsBlobZip.'#'.$idBlob;

                                                    $htmlSignature = '';
                                                    $arrayZipFile = self::getFileSignature($OnePiece, '.zip');
                                                    $idBlobSignature = '';
                                                    if ($arrayZipFile) {
                                                        $zipFile = $arrayZipFile[0].$arrayZipFile[1];
                                                        //Début insertion de la signature du fichier dans les blob
                                                        $atexoBlob = new Atexo_Blob();
                                                        if ($OnePiece->getIdBlobSignature()) {
                                                            $idBlobSignature = $OnePiece->getIdBlobSignature();
                                                        } else {
                                                            $idBlobSignature = $atexoBlob->insert_blob($arrayZipFile[1], $zipFile, $OnePiece->getOrganisme());
                                                        }
                                                        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlobSignature, $OnePiece->getOrganisme());
                                                        $blob_file = $blobResource['blob']['pointer'];
                                                        $OnePiece->setIdBlobSignature($idBlobSignature);
                                                        $OnePiece->save($connexion);
                                                        //Fin insertion de la signature du fichier dans les blob
                                                        $listeSignIndex = explode('#', $this->_echange->getIdsEnvSignItems());
                                                        foreach ($listeSignIndex as $indexSign) {
                                                            if ($listeSignIndex[0] != $indexSign) {
                                                                $arrayIndex = explode('_', $indexSign);
                                                                if ($arrayIndex[0] == $idBlobSignature) {
                                                                    $tableauIndexSignature = explode(';', $arrayIndex[1]);
                                                                }
                                                            }
                                                        }
                                                        $htmlSignature = (new Atexo_Zip())->getArborescence($blobResource, $idBlobSignature, $tableauIndexSignature, null, 'env_item_'.$OnePiece->getIdBlobSignature(), false, false, true);
                                                    }
                                                    $this->scriptArborescenceEnveloppe->Text = $this->scriptArborescenceEnveloppe->Text.$s.$htmlSignature;
                                                    $idsBlobSignatureZip = $idsBlobSignatureZip.'#'.$idBlobSignature;
                                                }
                                            } else {
                                                if (!$firstAcces) {
                                                    $this->scriptArborescenceEnveloppeNonZip->Text = '<ul>';
                                                }
                                                $idsBlobNonZip = $idsBlobNonZip.'#'.$idBlob;

                                                $checked = '';
                                                $arrayIdsBlob = explode(';', $this->_echange->getIdsBlobEnv());
                                                if (in_array($idBlob, $arrayIdsBlob)) {
                                                    $checked = 'checked="checked"';
                                                }
                                                $disabled = '';
                                                $disabledCss = '';
                                                $infoBulleSignature = false;
                                                if (!$this->verifyExtenstionAndSize($OnePiece->getNomFichier(), $OnePiece->getTailleFichier())) {
                                                    $disabled = 'disabled="false"';
                                                    $disabledCss = ' disabled-txt';
                                                    $infoBulleSignature = true;
                                                }
                                                $this->scriptArborescenceEnveloppeNonZip->Text .= '<li><span class="check-bloc"><input '.$disabled.' type="checkbox" name="env_nonZip_'.$OnePiece->getIdBlob().
                                                    '"  id="env_nonZip_'.$OnePiece->getIdBlob().'" " '.$checked.'   />
                                        </span><label class="content-bloc bloc-580" for="env_nonZip_'.$OnePiece->getIdBlob().'"><span class="float-left'.$disabledCss.'">'.
                                                    $OnePiece->getNomFichier().'&nbsp;( '.Atexo_Util::getTailleBlob($OnePiece->getOrganisme(), $OnePiece->getIdBlob()).' )</span>';
                                                if ($infoBulleSignature) {
                                                    $this->scriptArborescenceEnveloppeNonZip->Text .= $infoBulle;
                                                }
                                                $this->scriptArborescenceEnveloppeNonZip->Text .= '</label></li>';
                                                //Début cas signature fichier
                                                if ('' != $OnePiece->getSignatureFichier()) {
                                                    $idBlobSignature = '';
                                                    $fileContent = base64_decode($OnePiece->getSignatureFichier());
                                                    $atexoBlob = new Atexo_Blob();
                                                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time();
                                                    touch($tmpPath);
                                                    $fp = fopen($tmpPath, 'w');
                                                    fwrite($fp, $fileContent);
                                                    fclose($fp);
                                                    (new Atexo_Zip())->addFileToZip($tmpPath, $tmpPath.'.zip');
                                                    if ($OnePiece->getIdBlobSignature()) {
                                                        $idBlobSignature = $OnePiece->getIdBlobSignature();
                                                    } else {
                                                        $idBlobSignature = $atexoBlob->insert_blob($OnePiece->getNomFichier(), $tmpPath.'.zip', $OnePiece->getOrganisme());
                                                    }
                                                    $OnePiece->setIdBlobSignature($idBlobSignature);
                                                    $OnePiece->save($connexion);
                                                    $checkedFileSign = '';
                                                    $arrayIdsBlobSign = explode(';', $this->_echange->getIdsBlobSignEnv());
                                                    if (in_array($idBlobSignature, $arrayIdsBlobSign)) {
                                                        $checkedFileSign = 'checked="checked"';
                                                    }
                                                    $disabled = '';
                                                    $disabledCss = '';
                                                    $infoBulleFile = false;
                                                    if (!$this->verifyExtenstionAndSize($OnePiece->getNomFichier(), Atexo_Util::getTailleBlob($OnePiece->getOrganisme(), $idBlobSignature, false))) {
                                                        $disabled = 'disabled="false"';
                                                        $disabledCss = ' disabled-txt';
                                                        $infoBulleFile = true;
                                                    }
                                                    $this->scriptArborescenceEnveloppeNonZip->Text .= '<li><span class="check-bloc"><input '.$disabled.' type="checkbox" name="env_nonZip_'.$idBlobSignature.
                                                        '"  id="env_nonZip_'.$idBlobSignature.'" " '.$checkedFileSign.'  />
                                        </span><label class="content-bloc bloc-580" for="env_nonZip_'.$idBlobSignature.'"><span class="float-left'.$disabledCss.'">'.
                                                        $OnePiece->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier()).
                                                        '&nbsp;( '.Atexo_Util::getTailleBlob($OnePiece->getOrganisme(), $idBlobSignature).')</span>';
                                                    if ($infoBulleFile) {
                                                        $this->scriptArborescenceEnveloppeNonZip->Text .= $infoBulle;
                                                    }
                                                    $this->scriptArborescenceEnveloppeNonZip->Text .= '</label></li>';
                                                    $idsBlobSignatureNonZip = $idsBlobSignatureNonZip.'#'.$idBlobSignature;
                                                }
                                                //Début cas signature fichier
                                                $firstAcces = true;
                                            }
                                        }
                                    }
                                }
                                if ($firstAcces) {
                                    $this->scriptArborescenceEnveloppeNonZip->Text = $this->scriptArborescenceEnveloppeNonZip->Text.'</ul>';
                                }
                                $this->idBlob->Value = $idsBlobZip;
                                $this->idsBlobNonZip->Value = $idsBlobNonZip;
                                $this->idBlobSignature->Value = $idsBlobSignatureZip;
                                $this->idsBlobSignatureNonZip->Value = $idsBlobSignatureNonZip;
                            }
                        }
                    } else {
                        $logger->error('Contrat introuvable : id_echange = '.$this->_echange->getId().', Organisme = '.$this->_echange->getOrganisme());
                    }
                } else {
                    $logger->error('Echange introuvable : contrat_id = '.base64_decode($_GET['idContrat']).', Organisme = '.Atexo_CurrentUser::getCurrentOrganism());
                }

                $logger->info('Fin affichage des pieces de reponse titulaire');
            } catch (\Exception $e) {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error("Erreur lors de l'affichage des pieces de reponse titulaire : itemEnvZip=$itemEnvZip, itemSignEnvZip=$itemSignEnvZip, itemEnvNonZip=$itemEnvNonZip, itemSignEnvNonZip=$itemSignEnvNonZip".PHP_EOL.$e->getMessage());
            }
        }
    }

    /*
     * retourne la liste des rapports de verfication de signature
     */
    public function displayPiecesOuvertureAnalyse()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $pieces = [];
                if ($this->_echange instanceof CommonChorusEchange) {
                    $pieces = (new Atexo_Chorus_Echange())->retriveListeRapportSignatureEchange($this->_echange, $this->_organisme);
                }
                $this->repeaterPiecesOuvertureAnalyse->DataSource = $pieces;
                $this->repeaterPiecesOuvertureAnalyse->dataBind();
            } catch (\Exception $e) {
                $logger->error("Erreur lors de l'affichage des pieces ouverture et analyse.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    public function displayProcedureType()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $listTypeProc = (new Atexo_Chorus_Echange())->retreiveTypeProcedure();
            $arrayTypeProc = Atexo_Util::arrayUnshift($listTypeProc, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES').' ---');
            $data = [];
            $data['0'] = Prado::localize('TEXT_SELECTIONNER').'...';
            if ($arrayTypeProc) {
                foreach ($arrayTypeProc as $one) {
                    $data[$one->getId()] = $one->getLibelle();
                }
            }
            $this->procedureType->DataSource = $data;
            $this->procedureType->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du type de procedure.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function displayNaturesActeJuridique()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $acteJuridique = (new Atexo_Chorus_Echange())->retreiveNatureActeJuridique();
            $data = [];
            $data['0'] = Prado::localize('TEXT_SELECTIONNER').'...';
            if ($acteJuridique) {
                foreach ($acteJuridique as $oneActe) {
                    $data[$oneActe->getId()] = $oneActe->getLibelle();
                }
            }
            $this->natureActe->dataSource = $data;
            $this->natureActe->dataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage de la nature de l'acte juridique.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function displayFormePrix()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $formePrix = (new Atexo_Chorus_Echange())->retreiveFormePrix();
            $data = [];
            $data['0'] = Prado::localize('TEXT_SELECTIONNER').'...';
            if ($formePrix) {
                foreach ($formePrix as $one) {
                    $data[$one->getId()] = $one->getLibelle();
                }
            }
            $this->formeprix->dataSource = $data;
            $this->formeprix->dataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage des formes de prix.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function selectedFilesForZip($zipFile, $zipItemHtml = 'dce_item')
    {
        if ($zipFile) {
            $arrayIndexSelectedFiles = Atexo_Files::getArrayIndexSelectedFiles($zipFile, $zipItemHtml);
            if ($arrayIndexSelectedFiles) {
                return $arrayIndexSelectedFiles;
            }
        }
    }

    /* affichage de toutes les organisations  d'achats
     *
     */
    public function displayCategorieOA()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $fluxFen111 = null;
            $fluxFen211 = null;
            if ($this->isContratAcSad()) {
                $fluxFen211 = '1';
            } else {
                $fluxFen111 = '1';
            }
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                $organisme = $this->getOrganismeCreateurFromWsExec($this->_contratExec['organisme'] ?? '');
            } else {
                $organisme = $this->_contrat->getOrganisme();
            }
            $listOA = (new Atexo_Chorus_Echange())->retreiveOrganisationAchat($organisme, $fluxFen111, $fluxFen211);
            $data = [];
            $data['0'] = Prado::localize('TEXT_SELECTIONNER').'...';
            if (is_array($listOA) && count($listOA)) {
                foreach ($listOA as $oneOA) {
                    $data[$oneOA->getId()] = $oneOA->getLibelle();
                }
            }
            $this->oa->dataSource = $data;
            $this->oa->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du referentiel OA".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function diplayCategorieGA()
    {
        $this->scriptNames->Text = "<script>document.getElementById('categorieGA').style.display = '';</script>";
        $this->fillCategorieGA();
    }

    /**
     * allimenter le menu des gategories d'achat.
     */
    public function fillCategorieGA()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                $dernierNumMarche = $this->_contratExec['numero'] ?? '';
                $statutEj = $this->_contratExec['statutEj'];
                $numEj = $this->_contratExec['numEj'] ?? '';
                $service = null;
                if(!empty($this->_contratExec['service']['id'])) {
                    $service = $this->_contratExec['service']['id'];
                }
                $organisme = $this->getOrganismeCreateurFromWsExec($this->_contratExec['organisme'] ?? '');
            } else {
                $dernierNumMarche = $this->_contrat->getNumeroContrat();
                $statutEj = $this->_contrat->getStatutej();
                $numEj = $this->_contrat->getNumEj();
                $organisme = $this->_contrat->getOrganisme();
                $service = $this->_contrat->getServiceId();
            }

            $dernierStatutEJDecision = (new Atexo_Chorus_Util())->retournLastStatutEJDecision($statutEj, $dernierNumMarche);

            $dernierFluxSupprime = strstr($dernierStatutEJDecision, Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME'));

            $actif = 1;
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                $actif = $this->_contratExec && $numEj && !$dernierFluxSupprime ? null : 1;
            } else {
                if ($this->_contrat && $numEj && !$dernierFluxSupprime) {
                    $actif = null;
                }
            }
            $listGA = (new Atexo_Chorus_Echange())->retreiveGroupementAchatByIdOA(
                $this->oa->SelectedValue,
                $organisme,
                $service,
                $actif
            );

            $data = [
                '0' => Prado::localize('TEXT_SELECTIONNER') . "..."
            ];
            if ($listGA) {
                foreach ($listGA as $oneGA) {
                    $data[$oneGA->getId()] = $oneGA->getLibelle();
                }
            }
            $this->ga->dataSource = $data;
            $this->ga->DataBind();
        } catch (Exception $e) {
            $logger->error('Erreur lors du chargement du referentiel GA'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    private function getOrganismeCreateurFromWsExec(string $organisme): string
    {
        $idAgentCreateur = $this->_contratExec['createur']['id'];
        if ($idAgentCreateur) {
            $agentCreator = (new CommonAgentQuery())->findOneById($idAgentCreateur);
            if ($agentCreator) {
                $organisme = $agentCreator->getOrganisme();
            }
        }

        return $organisme;
    }

    /* affichage de toutes les categorie d'achat selon l'organisation d'achat séléctionné
     *
     */
    public function onCallBackCategorieGA($sender, $param)
    {
        // script pour afficher liste des GA
        $this->diplayCategorieGA();
        $this->panelCategorieGA->render($param->NewWriter);
    }

    /* afficher les types de marché
     *
     */
    public function displayTypeMarche()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $listTypeMarche = (new Atexo_Chorus_Echange())->retreiveListeTypeMarche();
            $data = [];
            $data['0'] = Prado::localize('TEXT_SELECTIONNER').'...';
            if ($listTypeMarche) {
                foreach ($listTypeMarche as $oneType) {
                    $data[$oneType->getId()] = $oneType->getLibelle();
                }
            }
            $this->marcheType->dataSource = $data;
            $this->marcheType->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du referentiel des types de marches.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste des regroupements comptables.
     */
    public function displayRegroupementCompatable()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $dataDource = [];
            $listeRegroupementComptable = (new Atexo_Chorus_Echange())->retrieveRegroupementCompatable();
            if ($listeRegroupementComptable) {
                $dataDource[0] = Prado::localize('TEXT_SELECTIONNEZ');
                foreach ($listeRegroupementComptable as $regroupement) {
                    $dataDource[$regroupement->getCode()] = $regroupement->getLibelle();
                }
            }
            $this->groupementComptable->dataSource = $dataDource;
            $this->groupementComptable->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du referentiel des regroupements comptables.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste des regroupements comptables.
     */
    public function displayTypeGroupement()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $dataDource = [];
            $typeGroupement = (new Atexo_Chorus_Echange())->retrieveTypeGroupement();
            if ($typeGroupement) {
                $dataDource['0'] = Prado::localize('DEFINE_TEXT_SELECTIONNER');
                foreach ($typeGroupement as $regroupement) {
                    $dataDource[$regroupement->getCode()] = $regroupement->getLibelle();
                }
            }
            $this->TypeGroupement->dataSource = $dataDource;
            $this->TypeGroupement->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du referentiel type de groupement.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste des : CCAG de référence.
     */
    public function displayCcagReference()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $dataCcag = CommonTChorusCcagQuery::create()->find();
            if ($dataCcag) {
                foreach ($dataCcag as $oneCcag) {
                    $dataSource[$oneCcag->getReferenceCcagChorus()] = $oneCcag->getLibelleCcag();
                }
            }
            $this->ccgaReference->dataSource = $dataSource;
            $this->ccgaReference->DataBind();
            if ($this->_contrat instanceof CommonTContratTitulaire) {
                $ccag = $this->_contrat->getCcagApplicable();
                $ccagContratObject = CommonTChorusCcagQuery::create()->filterByReferenceCcagOrme($ccag)->findOne();
                if ($ccagContratObject instanceof CommonTChorusCcag) {
                    $this->ccgaReference->SelectedValue = $ccagContratObject->getReferenceCcagChorus();
                }
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du referentiel CCAG.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste des : Type d'avance.
     */
    public function displayTypeAvance()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $typeAvance = [];
            $typeAvance['0'] = Prado::localize('DEFINE_TEXT_SELECTIONNER');
            $typeAvance['M'] = Prado::localize('DEFINE_AVANCE_FORFAITAIRE');
            $typeAvance['V'] = Prado::localize('DEFINE_AVANCE_FACULTATIVE');
            $typeAvance['N'] = Prado::localize('DEFINE_N_A');
            $this->typeAvance->dataSource = $typeAvance;
            $this->typeAvance->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage des types d'avance.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function chargerRepeatCodePays()
    {
        $dataSource = [];
        for ($j = 0; $j < 50; ++$j) {
            $dataSource[$j] = $j;
        }
        $this->repeatCodePays->dataSource = $dataSource;
        $this->repeatCodePays->dataBind();
    }

    /**
     * Focntion enregistrer l'échange dans la base.
     */
    public function updateEchange()
    {
        $ancienIdBlob = null;
        $idBlobFicheNavette = null;
        $atexoBlob = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_WRITE'));
            //Sauvegarde du type de flux
            $typeFlux = ($this->isContratAcSad()) ? Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211') : Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111');
            $this->_echange->setTypeFlux($typeFlux);
            if ($this->isContratAcSad()) {
                //Type de contrat
                if (null != $this->typeContrat->SelectedValue && '0' != $this->typeContrat->SelectedValue) {
                    $this->_echange->setTypeContrat($this->typeContrat->SelectedValue);
                } else {
                    $this->_echange->setTypeContrat('0');
                }
                //Intitule
                if ('' != $this->intituleDonneesCandidat->Text) {
                    $this->_echange->setIntituleContrat($this->intituleDonneesCandidat->Text);
                }
                //Objet
                if ('' != $this->objetMarcheDonneesCandidat->Text) {
                    $this->_echange->setObjetContrat($this->objetMarcheDonneesCandidat->Text);
                }
                //Siren, siret
                if ('' != $this->sirenDonneesCandidat->Text) {
                    $this->_echange->setSiren($this->sirenDonneesCandidat->Text);
                }
                if ('' != $this->siretDonneesCandidat->Text) {
                    $this->_echange->setSiret($this->siretDonneesCandidat->Text);
                }

                $codePrincipal = $this->referentielCPVDonneesCandidat->getCpvPrincipale();
                $codesSec = $this->referentielCPVDonneesCandidat->getCpvSecondaires();

                if ('' != $codePrincipal) {
                    $this->_echange->setCpv1($codePrincipal);
                } else {
                    $this->_echange->setCpv1(null);
                }
                if ('' != $codesSec) {
                    $this->_echange->setCpv2($codesSec);
                } else {
                    $this->_echange->setCpv2(null);
                }
                //Montant max ht
                if ('' != $this->montantHtAccordDonneesCandidat->Text) {
                    $this->_echange->setMontantHt($this->montantHtAccordDonneesCandidat->Text);
                } else {
                    $this->_echange->setMontantHt('');
                }
                //Identifiant accord cadre
                if ('' != $this->idAccordCadreDonneesCandidat->Text) {
                    $this->_echange->setIdentifiantAccordCadre($this->idAccordCadreDonneesCandidat->Text);
                } else {
                    $this->_echange->setIdentifiantAccordCadre('');
                }
                //Identifiant accord cadre chapeau
                if ('' != $this->idAccordCadreChapeauDonneesCandidat->Text) {
                    $this->_echange->setIdentifiantAccordCadreChapeau($this->idAccordCadreChapeauDonneesCandidat->Text);
                } else {
                    $this->_echange->setIdentifiantAccordCadreChapeau('');
                }
                //Date de notification et de fin de marche
                if ('' != $this->dateNotificationDonneesCandidat->Text) {
                    $this->_echange->setDateNotification($this->dateNotificationDonneesCandidat->Text);
                } else {
                    $this->_echange->setDateNotification('');
                }
                if ('' != $this->finMarcheDonneesCandidat->Text) {
                    $this->_echange->setDateFinMarche($this->finMarcheDonneesCandidat->Text);
                } else {
                    $this->_echange->setDateFinMarche('');
                }
                if ('' != $this->dateNotificationReelleDonneesCandidat->Text) {
                    $this->_echange->setDateNotificationReelle($this->dateNotificationReelleDonneesCandidat->Text);
                } else {
                    $this->_echange->setDateNotificationReelle('');
                }
                if ('' != $this->dateFinMarcheReelleDonneesCandidat->Text) {
                    $this->_echange->setDateFinMarcheReelle($this->dateFinMarcheReelleDonneesCandidat->Text);
                } else {
                    $this->_echange->setDateFinMarcheReelle('');
                }
            } else {
                if ('' != $this->siren->Text) {
                    $this->_echange->setSiren($this->siren->Text);
                }
                if ('' != $this->siret->Text) {
                    $this->_echange->setSiret($this->siret->Text);
                }

                $codePrincipal = $this->referentielCPV->getCpvPrincipale();
                $codesSec = $this->referentielCPV->getCpvSecondaires();

                if ('' != $codePrincipal) {
                    $this->_echange->setCpv1($codePrincipal);
                } else {
                    $this->_echange->setCpv1(null);
                }
                if ('' != $codesSec) {
                    $this->_echange->setCpv2($codesSec);
                } else {
                    $this->_echange->setCpv2(null);
                }
                if ('' != $this->idAccordCadre->Text) {
                    $this->_echange->setIdentifiantAccordCadre($this->idAccordCadre->Text);
                } else {
                    $this->_echange->setIdentifiantAccordCadre('');
                }
                if ('' != $this->montantHtAccord->Text) {
                    $this->_echange->setMontantHt($this->montantHtAccord->Text);
                } else {
                    $this->_echange->setMontantHt('');
                }

                if ('0' != $this->ga->SelectedValue) {
                    $this->_echange->setIdGa($this->ga->SelectedValue);
                }
                if ('0' != $this->marcheType->SelectedValue) {
                    $this->_echange->setIdTypeMarche($this->marcheType->SelectedValue);
                }
                //Type de groupement
                if ('0' != $this->TypeGroupement->SelectedValue) {
                    $this->_echange->setIdTypeGroupement($this->TypeGroupement->SelectedValue);
                }
                //Regroupement comptable
                if ('0' != $this->groupementComptable->SelectedValue) {
                    $this->_echange->setIdRegroupementComptable($this->groupementComptable->SelectedValue);
                } else {
                    $this->_echange->setIdRegroupementComptable('0');
                }

                if ('0' != $this->natureActe->SelectedValue) {
                    $this->_echange->setIdActeJuridique($this->natureActe->SelectedValue);
                } else {
                    $this->_echange->setIdActeJuridique('0');
                }

                if ('0' != $this->procedureType->SelectedValue) {
                    $this->_echange->setIdTypeProcedure($this->procedureType->SelectedValue);
                } else {
                    $this->_echange->setIdTypeProcedure('0');
                }
                if ('0' != $this->formeprix->SelectedValue) {
                    $this->_echange->setIdFormePrix($this->formeprix->SelectedValue);
                } else {
                    $this->_echange->setIdFormePrix('0');
                }
                if ('TUAST' == $this->TypeGroupement->SelectedValue || 0 == $this->nbEntrepriseCotraitantes->SelectedValue) {
                    $this->_echange->setNbrEntreprisesCotraitantes('0');
                } else {
                    $this->_echange->setNbrEntreprisesCotraitantes($this->nbEntrepriseCotraitantes->SelectedValue);
                }
                if ($this->ssTraitanceDeclareeOui->checked) {
                    $this->_echange->setSousTraitanceDeclaree('1');
                } elseif ($this->ssTraitanceDeclareeNon->checked) {
                    $this->_echange->setSousTraitanceDeclaree('0');
                }

                if ($this->carteAchatOui->checked) {
                    $this->_echange->setCarteAchat('1');
                } elseif ($this->carteAchatNon->checked) {
                    $this->_echange->setCarteAchat('0');
                }

                if ($this->clauseSocialeOui->checked) {
                    $this->_echange->setClauseSociale('1');
                } elseif ($this->clauseSocialeNon->checked) {
                    $this->_echange->setClauseSociale('0');
                }
                if ($this->clauseEnvironnementaleOui->checked) {
                    $this->_echange->setClauseEnvironnementale('1');
                } elseif ($this->clauseEnvironnementaleNon->checked) {
                    $this->_echange->setClauseEnvironnementale('0');
                }
                if ('' != $this->nbPropositionsRecues->Text) {
                    $this->_echange->setNbrPropositionRecues($this->nbPropositionsRecues->Text);
                }
                if ('' != $this->nbPropositionsDemat->Text) {
                    $this->_echange->setNbrPropositionDematerialisees($this->nbPropositionsDemat->Text);
                }

                //Début Code pays titulaire et co-titulaires
                $this->saveCodePaysTitulairesCoTraitants();
                //Fin Code pays titulaire et co-titulaires

                //CCAG de référence
                if ($this->ccgaReference->SelectedValue) {
                    $this->_echange->setCcagReference($this->ccgaReference->SelectedValue);
                }
                //Pourcentage de l'avance
                if ('' != $this->pourcentageAvance->Text) {
                    $this->_echange->setPourcentageAvance($this->pourcentageAvance->Text);
                } else {
                    $this->_echange->setPourcentageAvance('');
                }
                //Type d'avance
                if ($this->typeAvance->SelectedValue) {
                    $this->_echange->setTypeAvance($this->typeAvance->SelectedValue);
                } else {
                    $this->_echange->setTypeAvance('0');
                }
                //Conditions de paiement
                if ($this->conditionsPaiementOui->checked) {
                    $this->_echange->setConditionsPaiement('1');
                } elseif ($this->conditionsPaiementNon->checked) {
                    $this->_echange->setConditionsPaiement('0');
                }

                //Permet de charger l'objet de la fiche de navette
                $this->_ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($this->_echange->getId(), $this->_echange->getOrganisme());
                if (!($this->_ficheNavette instanceof CommonChorusFicheNavette)) {
                    $this->_ficheNavette = new CommonChorusFicheNavette();
                    $this->_ficheNavette->setIdChorusEchange($this->_echange->getId());
                    $this->_ficheNavette->setOrganisme($this->_echange->getOrganisme());
                }
                $this->chargerObjetFicheNavette($this->_ficheNavette, $this->_echange);

                //Génération  pdf fiche de navette

                if (
                    $this->_contrat instanceof CommonTContratTitulaire
                || $this->usingExecContrat()
                ) {
                    $referenceLibre = '';
                    $contrat = null;
                    if ($this->_contrat instanceof CommonTContratTitulaire) {
                        $referenceLibre = $this->_contrat->getReferenceLibre();
                        $contrat = $this->_contrat;
                    } elseif (isset($this->_contratExec['numero'])) {
                        $referenceLibre = $this->_contratExec['referenceLibre'];
                        $contrat = $this->_contratExec;
                    }

                    $modele = Atexo_Config::getParameter('APP_BASE_ROOT_DIR')
                        . '/mpe_sf/legacy/ressources/interface/chorus/Modele_Fiche_Navette_';
                    $fileGenere = (new Atexo_GenerationFichier_GenerationDocumentsChorus())->generationFicheNavette($this->_ficheNavette, $contrat, $modele, true);
                    $pdfContent = (new PdfGeneratorClient())->genererPdf($fileGenere);
                    $nomFichier = (new Atexo_Chorus_Echange())->getNomFicheNavette(str_replace('/', '_', $referenceLibre));
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP').'/'.Atexo_Util::clean(Atexo_Util::OterAccents($nomFichier)).'.pdf';
                    Atexo_Util::write_file($tmpFile, $pdfContent);
                    $atexoBlob = new Atexo_Blob();
                    //Recupération de l'id_blob avant la génération d'un nouveau
                    $ancienIdBlob = '';
                    if ($this->_ficheNavette->getIdDocument()) {
                        $ancienIdBlob = $this->_ficheNavette->getIdDocument();
                    }
                    $idBlobFicheNavette = $atexoBlob->insert_blob($nomFichier, $tmpFile, $this->_ficheNavette->getOrganisme());
                    //Sauvegarde des informations dans la base de données
                    $nomFichier = $nomFichier.'.pdf';
                    $this->_ficheNavette->setIdDocument($idBlobFicheNavette);
                    $this->_ficheNavette->setNomDocument($nomFichier);
                }
                //Enregistrement des informations
                $this->_ficheNavette->save($connexion);
            }
            if ('0' != $this->oa->SelectedValue) {
                $this->_echange->setIdOa($this->oa->SelectedValue);
            }

            //Début sauvegarde DCE
            $this->sauvegarderPiecesDceSelectionnees();
            //Fin sauvegarde DCE

            //Début sauvegarde pièces notification
            $this->sauvegarderPiecesNotificationSelectionnees();
            //Fin sauvegarde pièces notification

            //Début sauvegarde fichiers enveloppe
            $this->sauvegarderPiecesReponseSelectionnees();
            //Fin sauvegarde fichiers enveloppe

            //Début sauvegarde pièces docs externes
            $this->sauvegarderAttacheesConsultationSelectionnees();
            //Fin sauvegarde pièces docs externes
            $idsRapportSignature = '';
            foreach ($this->repeaterPiecesOuvertureAnalyse->getItems() as $oneItemOuverture) {
                $idsRapportSignature .= $oneItemOuverture->idEnveloppe->value;
                if ($oneItemOuverture->pieceChecked->checked) {
                    $idsRapportSignature .= '-1';
                } else {
                    $idsRapportSignature .= '-0';
                }
                $idsRapportSignature .= '-'.$oneItemOuverture->sizeFile->value;
                $idsRapportSignature .= '#';
            }
            $this->_echange->setIdsRapportSignature($idsRapportSignature);
            if ('' != $this->dateNotification->Text) {
                $this->_echange->setDateNotification(Atexo_Util::frnDate2iso($this->dateNotification->Text));
            }
            if ('' != $this->finMarche->Text) {
                $this->_echange->setDateFinMarche(Atexo_Util::frnDate2iso(($this->finMarche->Text)));
            }
            if ('' != $this->dateNotificationReelle->Text) {
                $this->_echange->setDateNotificationReelle(Atexo_Util::frnDate2iso(($this->dateNotificationReelle->Text)));
            }
            if ('' != $this->dateFinMarcheReelle->Text) {
                $this->_echange->setDateFinMarcheReelle(Atexo_Util::frnDate2iso(($this->dateFinMarcheReelle->Text)));
            }
            $resultSave = $this->_echange->save($connexion);

            //Suppréssion de l'ancien id_blob
            if ('' != $ancienIdBlob && '' != $idBlobFicheNavette && $ancienIdBlob != $idBlobFicheNavette) {
                $atexoBlob->deleteBlobFile($ancienIdBlob, $this->_ficheNavette->getOrganisme());
            }
            if ($resultSave) {
                return true;
            }
        } catch (\Exception $e) {
            $messageErreur = PHP_EOL.'Exception = [ '.$e->getMessage().' ] , '.PHP_EOL.' Methode = ChorusDetailEchanges::updateEchange'.PHP_EOL;
            (new Atexo_Chorus_Util())->loggerErreur("Erreur lors de l'enregistrement des donnees de l'echange Chorus , Echange [Id = ".$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().'], '.$messageErreur);
            Prado::log("Erreur lors de l'enregistrement des donnees de l'echange Chorus , Echange [Id = ".$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().'], '.$messageErreur.'-', TLogger::ERROR, 'ERROR');

            return false;
        }

        return false;
    }

    public function doAddEchange($sender, $param)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->isTypeEnvoiFicheModificative()) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                self::updateFicheModificative($connexion);
            } else {
                $this->updateEchange();
            }
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
                $this->response->redirect('index.php?page=Agent.ChorusEchanges&uuid='. $_GET['uuid'] .(isset($_GET['fromCons']) ? '&fromCons' : ''));
            } else {
                $this->response->redirect('index.php?page=Agent.ChorusEchanges&idContrat='.base64_encode($this->_contrat->getIdContratTitulaire()).(isset($_GET['fromCons']) ? '&fromCons' : ''));
            }

        } catch (Exception $e) {
            $logger->error('Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de gerer la visibilite de la fiche modificative.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiFicheModificative()
    {
        try {
            return (new Atexo_Chorus_Util())->isTypeEnvoiFicheModificative($this->_contrat, $this->_echange);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusDetailEchanges::isTypeEnvoiFicheModificative");
        }
    }

    public function displayEchange()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $codeCpv1 = null;
            $codeCpv2 = null;

            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                $codeCpv1 = $this->_contratExec['cpv']['codePrincipal'] ?? '';
                $codeCpv2 = $this->_contratExec['cpv']['codeSecondaire1'] ?? '';
            } elseif ($this->_contrat instanceof CommonTContratTitulaire) {
                $codeCpv1 = $this->_contrat->getCodeCpv1();
                $codeCpv2 = $this->_contrat->getCodeCpv2();
            }

            if ('' != $this->_echange->getIdOa()) {
                $this->oa->SelectedValue = $this->_echange->getIdOa();
            }
            $this->referentielCPV->setModeAffichage(self::getModeReferentielCpv());
            $this->referentielCPV->onInitComposant();
            if ($this->isContratAcSad()) {
                $this->displayDonneesContrat();
            } else {
                if ('' != $codeCpv1 && !$this->_echange->getCpv1()) {
                    $this->referentielCPV->chargerReferentielCpv($codeCpv1, $codeCpv2);
                } else {
                    $this->referentielCPV->chargerReferentielCpv($this->_echange->getCpv1(), $this->_echange->getCpv2());
                }

                if ('' != $this->_echange->getIdGa()) {
                    $this->diplayCategorieGA();
                    $this->ga->SelectedValue = $this->_echange->getIdGa();
                }
                if ('' != $this->_echange->getIdTypeMarche()) {
                    $this->marcheType->SelectedValue = $this->_echange->getIdTypeMarche();
                }
                //Type de groupement
                if ('' != $this->_echange->getIdTypeGroupement()) {
                    $this->TypeGroupement->SelectedValue = $this->_echange->getIdTypeGroupement();
                }
                //Regroupement comptable
                if ('' != $this->_echange->getIdRegroupementComptable()) {
                    $this->groupementComptable->SelectedValue = $this->_echange->getIdRegroupementComptable();
                }

                if ($this->_echange->getIdTypeProcedure()) {
                    $this->procedureType->SelectedValue = $this->_echange->getIdTypeProcedure();
                }
                if ($this->_echange->getIdFormePrix()) {
                    $this->formeprix->Text = $this->_echange->getIdFormePrix();
                }
                if ($this->_echange->getNbrEntreprisesCotraitantes()) {
                    $this->nbEntrepriseCotraitantes->SelectedValue = $this->_echange->getNbrEntreprisesCotraitantes();
                }
                //Si le type de groupement est TUAST, alors griser la selection du nombre d'entreprises co-traitantes
                $script = '';
                if ('TUAST' == $this->_echange->getIdTypeGroupement() && '0' == $this->_echange->getNbrEntreprisesCotraitantes()) {
                    $script .= "document.getElementById('ctl0_CONTENU_PAGE_nbEntrepriseCotraitantes').disabled = 'disabled';";
                }
                $this->javascript->Text .= "<script>$script</script>";

                if ($this->_echange->getSousTraitanceDeclaree()) {
                    if ('1' == $this->_echange->getSousTraitanceDeclaree()) {
                        $this->ssTraitanceDeclareeOui->checked = true;
                    } elseif ('0' == $this->_echange->getSousTraitanceDeclaree()) {
                        $this->ssTraitanceDeclareeNon->checked = true;
                    }
                } else {
                    $this->ssTraitanceDeclareeNon->checked = true;
                }
                if ($this->_echange->getCarteAchat()) {
                    if ('1' == $this->_echange->getCarteAchat()) {
                        $this->carteAchatOui->checked = true;
                    } elseif ('0' == $this->_echange->getCarteAchat()) {
                        $this->carteAchatNon->checked = true;
                    }
                } else {
                    $this->carteAchatNon->checked = true;
                }
                if ($this->_echange->getClauseSociale()) {
                    if ('1' == $this->_echange->getClauseSociale()) {
                        $this->clauseSocialeOui->checked = true;
                    } elseif ('0' == $this->_echange->getClauseSociale()) {
                        $this->clauseSocialeNon->checked = true;
                    }
                } else {
                    $this->clauseSocialeNon->checked = true;
                }
                if ($this->_echange->getClauseEnvironnementale()) {
                    if ('1' == $this->_echange->getClauseEnvironnementale()) {
                        $this->clauseEnvironnementaleOui->checked = true;
                    } elseif ('0' == $this->_echange->getClauseEnvironnementale()) {
                        $this->clauseEnvironnementaleNon->checked = true;
                    }
                } else {
                    $this->clauseEnvironnementaleNon->checked = true;
                }
                //Conditions de paiement
                if (null != $this->_echange->getConditionsPaiement()) {
                    if ('1' == $this->_echange->getConditionsPaiement()) {
                        $this->conditionsPaiementOui->checked = true;
                    } elseif ('0' == $this->_echange->getConditionsPaiement()) {
                        $this->conditionsPaiementNon->checked = true;
                    }
                } else {
                    /*KBE if($this->_decision->getPmePmi() == Atexo_Config::getParameter('PME_PMI_OUI')) {
                        $this->conditionsPaiementOui->checked = true;
                    } else {
                        $this->conditionsPaiementNon->checked = true;
                    } KBE*/
                }
                $depotElectronique = (new Atexo_Consultation_Responses())->retrieveOffreByConsRef($this->getReferenceDonneesConsultation(), $this->_organisme);
                $depotPapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierByConsRef($this->getReferenceDonneesConsultation(), $this->_organisme);

                $nbrDepotElectronique = (is_array($depotElectronique)) ? count($depotElectronique) : 0;
                $nbrDepotPapier = is_countable($depotPapier) ? count($depotPapier) : 0;

                if ($this->_echange->getNbrPropositionRecues()) {
                    $this->nbPropositionsRecues->Text = $this->_echange->getNbrPropositionRecues();
                } else {
                    $this->nbPropositionsRecues->Text = $nbrDepotElectronique + $nbrDepotPapier;
                }
                if ($this->_echange->getNbrPropositionDematerialisees()) {
                    $this->nbPropositionsDemat->Text = $this->_echange->getNbrPropositionDematerialisees();
                } else {
                    //pre-remplir le nombre de propositions dématerialisées
                    $this->nbPropositionsDemat->Text = $nbrDepotElectronique;
                }

                if ($this->_echange->getIdActeJuridique()) {
                    $this->natureActe->SelectedValue = $this->_echange->getIdActeJuridique();
                }
            }

            if ($this->_echange->getIdsEnvAe()) {
                $this->acteEngagement->Checked = true;
            }
            if ($this->_echange->getSignAce()) {
                $this->signActeEngagement->Checked = true;
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage de l'echange CHORUS.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /* La fonction qui affiche l'arbo du zip - document de la consultation
     *
     *
     */
    public function getArborescenceZipDocCons($idBlob, $idsPiecesExternes = null)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $this->_organisme);
            if (!$idsPiecesExternes) {
                $idsPiecesExternes = $this->_echange->getIdsPiecesExternes();
            }
            $arrayItems = explode(';', $idsPiecesExternes);
            $arrayFilesItem = [];
            foreach ($arrayItems as $item) {
                if ($item) {
                    $arrayItem = explode('_', $item);
                    $arrayFilesItem[] = $arrayItem[1];
                }
            }

            return (new Atexo_Zip())->getArborescence($blobResource, $idBlob, $arrayFilesItem, null, 'zip_item', false, false, true);
        } catch (Exception $e) {
            $logger->error('Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function displayDocumentExterne()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $criteriaVo = new Atexo_Consultation_DocumentExterneVo();
                $criteriaVo->setRefConsultation($this->getReferenceDonneesConsultation());
                $atexoConsultation = new Atexo_Consultation();
                $documentExterne = $atexoConsultation->searchDocument($criteriaVo);
                if ($documentExterne) {
                    $data = [];
                    $i = 0;
                    foreach ($documentExterne as $oneDoc) {
                        $data[$i]['id'] = $oneDoc->getType();
                        $data[$i]['doc'] = $oneDoc;
                        if ('1' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('TEXT_PUBLICITES');
                        } elseif ('2' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('DEFINE_CONSULTATION');
                        } elseif ('3' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('DEFINE_REGISTRES');
                        } elseif ('4' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('DESCRIPTION_COMMISSION_APPEL_OFFRES');
                        } elseif ('5' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('DESCRIPTION_NOTIFICATION');
                        } elseif ('6' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('TEXT_CANDIDATURES_OFFRES_NON_RETENUES');
                        } elseif ('7' == $oneDoc->getType()) {
                            $data[$i]['type'] = Prado::localize('TEXT_CANDIDATURES_OFFRES_RETENUES');
                        }
                        ++$i;
                    }

                    $listType = [];
                    $lisIdType = [];
                    $j = 0;
                    foreach ($data as $dataRow) {
                        if (!in_array($dataRow['id'], $lisIdType)) {
                            $listType[$j]['Id'] = $dataRow['id'];
                            $listType[$j]['type'] = $dataRow['type'];
                            ++$j;
                        }
                        $lisIdType[] = $dataRow['id'];
                    }
                    $this->repeaterType->DataSource = $listType;
                    $this->repeaterType->dataBind();
                    foreach ($this->repeaterType->getItems() as $item) {
                        $idType = $this->repeaterType->DataKeys[$item->itemIndex];
                        $newData = [];
                        foreach ($data as $dataDoc) {
                            if ($dataDoc['id'] == $idType) {
                                $newData[] = $dataDoc['doc'];
                            }
                        }
                        $item->repeaterTypeDoc->DataSource = $newData;
                        $item->repeaterTypeDoc->dataBind();
                    }
                }
            } catch (\Exception $e) {
                $logger->error("Erreur lors de l'affichage des pieces externes.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    public function displayPropositionsRecues()
    {
        $offres = (new Atexo_Consultation_Responses())->retrieveOffreByConsRef($this->getReferenceDonneesConsultation(), $this->_organisme);
        $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierByConsRef($this->getReferenceDonneesConsultation(), $this->_organisme);
        $nbrDepotElectronique = (is_array($offres)) ? count($offres) : 0;
        $nbrDepotPapier = is_countable($offrePapier) ? count($offrePapier) : 0;
        $this->initNbPropositionsRecues->Text = $nbrDepotElectronique + $nbrDepotPapier;
        $this->initNbPropositionsDemat->Text = $nbrDepotElectronique;
    }

    public function verifyExtenstion($nameFile)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $extensionFile = strtoupper(Atexo_Util::getExtension($nameFile));
            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
            $data = [...$data, ...['0' => 'ZIP']];
            if (!in_array($extensionFile, $data)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $logger->error('Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function displaySizeFile($idBlob, $nameFile)
    {
        if ('ZIP' != strtoupper(Atexo_Util::getExtension($nameFile))) {
            return ' ('.$this->getFileTaille($idBlob).')';
        }
    }

    /**
     * Permet de sauvegarder l'echange CHORUS et modifie le statut de l'echange en 'STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'
     * Sauvegarde et genere eventuellement l'acte modificatif.
     *
     * @param $sender
     * @param $param
     */
    public function sendToChorus($sender, $param)
    {
        $contrat = $this->_contrat;
        if ($this->usingExecContrat()) {
            $contrat = $this->_contratExec;
        }

        if ($this->isValid) {
            (new Atexo_Chorus_Util())->loggerInfos("DEBUT planification de la transmission de flux par l'utilisateur vers Chorus");
            try {
                if (
                    $this->_contrat instanceof CommonTContratTitulaire
                        || !empty($this->_contratExec)
                ) {
                    if ($this->_echange instanceof CommonChorusEchange) {
                        (new Atexo_Chorus_Util())->loggerInfos('Debut traitement Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        if ($this->isTypeEnvoiFicheModificative()) {
                            (new Atexo_Chorus_Util())->loggerInfos('Debut sauvegarde et generation fiche modificative, Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                            $ficheModificative = self::updateFicheModificative($connexion);
                            self::generationFicheModificative($ficheModificative, $connexion);
                            (new Atexo_Chorus_Util())->loggerInfos('Fin sauvegarde et generation fiche modificative, Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                        } else {
                            //Enregistrement de l'echange CHORUS
                            (new Atexo_Chorus_Util())->loggerInfos("Debut enregistrement donnees de l'echange CHORUS - Echange [Id = ".$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                            $resultSave = $this->updateEchange();
                            if (true === $resultSave) {
                                (new Atexo_Chorus_Util())->loggerInfos('Echange Chorus enregistre avec *** SUCCES *** , Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                            }
                            (new Atexo_Chorus_Util())->loggerInfos("Fin enregistrement donnees de l'echange CHORUS - Echange [Id = ".$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');

                            //Verification blocage temporaire d'envoi des contrats AC/SAD
                            if ($this->_echange->isTypeFluxFen211($contrat) && Atexo_Config::getParameter('BLOCAGE_TEMPORAIRE_WS_ENVOI_FLUX_CHORUS_FEN211')) {
                                (new Atexo_Chorus_Util())->loggerInfos("*** ATTENTION : L'ENVOI DU FEN211 EST DESACTIVE TEMPORAIREMENT (PARAMETRE : 'BLOCAGE_TEMPORAIRE_WS_ENVOI_FLUX_CHORUS_FEN211' est active) ***");
                                $this->afficherErreur(Prado::localize('RETOUR_CHORUS_BLOCAGE_TEMPORAIRE_WS_ENVOI_FLUX_CHORUS_FEN211'));
                                $this->javascriptValidation->Text = '';
                                $this->fermerPopInConfirmation();

                                return;
                            }

                            //Verification de doublons
                            if (true === $this->verifierDoublonsAcSAD()) {
                                (new Atexo_Chorus_Util())->loggerInfos('Debut verification de doublons AC/SAD Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                $this->afficherErreur(Prado::localize('MESSAGE_ERREUR_DOUBLONS_ACCORD_CADRE_SAD'));
                                $this->javascriptValidation->Text = '';
                                $this->fermerPopInConfirmation();
                                (new Atexo_Chorus_Util())->loggerInfos('Fin verification de doublons AC/SAD Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');

                                return;
                            }
                        }

                        $typeActionEnvoiChorus = (new Atexo_Chorus_Echange())->typeActionEnvoiChorus($this->_echange, $this->getContrat(), intval($this->numOrdre->Text));
                        switch ($typeActionEnvoiChorus) {
                            case '1': //FIR
                                (new Atexo_Chorus_Util())->loggerInfos('Type Envoi = flux FIR [Id_echange = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($this->_echange->getId(), $this->_echange->getOrganisme(), Atexo_Config::getParameter('STATUT_TIERS_CHORUS_EN_COURS_VERFICATION'), Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR'));
                                break;
                            case '0': //FEN
                                if ($this->_echange->isTypeFluxFen211()) {
                                    (new Atexo_Chorus_Util())->loggerInfos('Type Envoi = flux FEN211 [Id_echange = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                    (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($this->_echange->getId(), $this->_echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'), Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211'));
                                } else {
                                    (new Atexo_Chorus_Util())->loggerInfos('Type Envoi = flux FEN111 [Id_echange = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                    (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($this->_echange->getId(), $this->_echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'), Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111'));
                                }
                                break;
                            case '2': //Changement de statut
                                (new Atexo_Chorus_Util())->loggerInfos('Type Envoi = cas maj statut [Id_echange = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($this->_echange->getId(), $this->_echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));
                                (new Atexo_Chorus_Util())->loggerInfos('Desactivation message rejet : [Id_echange = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                                (new Atexo_Chorus_Echange())->desactiverMessageRejetEchange($this->_echange, $connexion);
                                break;
                        }

                        (new Atexo_Chorus_Util())->loggerInfos('Fin traitement Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                        (new Atexo_Chorus_Util())->loggerInfos("FIN planification de la transmission de flux par l'utilisateur vers Chorus".PHP_EOL.PHP_EOL.PHP_EOL);

                        if ($this->usingExecContrat()) {
                            $this->response->redirect('index.php?page=Agent.ChorusEchanges&uuid='.$this->getContrat()['uuid'].(isset($_GET['fromCons']) ? '&fromCons' : ''));
                        } else {
                            $this->response->redirect('index.php?page=Agent.ChorusEchanges&idContrat='.base64_encode($this->_contrat->getIdContratTitulaire()).(isset($_GET['fromCons']) ? '&fromCons' : ''));
                        }
                    } else {
                        (new Atexo_Chorus_Util())->loggerErreur('Objet Echange CHORUS non trouve, Echange : '.PHP_EOL.print_r($this->_echange, true).' , '.PHP_EOL.' Methode = ChorusDetailEchanges::sendToChorus'.PHP_EOL);
                    }
                } else {
                    (new Atexo_Chorus_Util())->loggerErreur('Objet Contrat non trouve , contrat : '.PHP_EOL.print_r($this->_contrat, true).' , '.PHP_EOL.' Methode = ChorusDetailEchanges::sendToChorus'.PHP_EOL);
                }
            } catch (\Exception $e) {
                $messageErreur = 'Exception = [ '.$e->getMessage().' ] , '.PHP_EOL.' Methode = ChorusDetailEchanges::sendToChorus'.PHP_EOL;
                (new Atexo_Chorus_Util())->loggerErreur("Erreur lors de la planification de la transmission de flux par l'utilisateur vers Chorus , Echange [Id = ".$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().'], '.$messageErreur);
                Prado::log("Erreur planification de la transmission de flux par l'utilisateur vers CHORUS-".$e->__toString().'-', TLogger::ERROR, 'ERROR');
                $this->afficherErreur(Prado::localize('TEXT_MSG_ERREUR_TRANSMISSION_CHORUS'));
            }
            $this->javascriptValidation->Text = "<script>J('.demander-validation').dialog('close');return false;</script>";
            (new Atexo_Chorus_Util())->loggerInfos("FIN planification de la transmission de flux par l'utilisateur vers Chorus".PHP_EOL.PHP_EOL.PHP_EOL);
        }
    }

    /*
     * TO DELETE
     */
    public function validateNumeroSequance($numSeq)
    {
        $newNumSeq = '0000';
        if (5 == strlen($numSeq)) {
            $newNumSeq = $numSeq;
        } elseif (1 == strlen($numSeq)) {
            $newNumSeq = '0000'.$numSeq;
        } elseif (2 == strlen($numSeq)) {
            $newNumSeq = '000'.$numSeq;
        } elseif (3 == strlen($numSeq)) {
            $newNumSeq = '00'.$numSeq;
        } elseif (4 == strlen($numSeq)) {
            $newNumSeq = '0'.$numSeq;
        } elseif (5 == strlen($numSeq)) {
            $newNumSeq = $numSeq;
        }

        return $newNumSeq;
    }

    /**
     * Gere l'affichage/activation de la fiche de recensement/referentiel categories achats.
     */
    public function setDesabledCategorieAchat()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            //par défaut on ne peut pas changer les informations sauf pour les cas suivants
            //il s'agit du premier échange ou le dernier statut EJ est supprimé
            if (!self::isContratAcSad()) {
                $enabled = false;
                if (intval($this->_echange->getNumOrdre()) < 2) {
                    $enabled = true;
                } else {
                    // @TODO  KBE comprendre le traitement
                    //$dernierNumMarche = Atexo_Chorus_Util::retournLastNumeroMarcheDecision($this->_decision->getNumeroMarche());
                    //$dernierStatutEJDecision = Atexo_Chorus_Util::retournLastStatutEJDecision($this->_decision->getStatutej(), $dernierNumMarche);

                    if ($this->usingExecContrat()) {
                        $dernierNumMarche = $this->_contratExec['numero'] ?? '';
                        $dernierStatutEJDecision = (new Atexo_Chorus_Util())->retournLastStatutEJDecision($this->_contratExec['statutEj'], $dernierNumMarche);
                    } else {
                        $dernierNumMarche = $this->_contrat->getNumeroContrat();
                        $dernierStatutEJDecision = (new Atexo_Chorus_Util())->retournLastStatutEJDecision($this->_contrat->getStatutej(), $dernierNumMarche);
                    }

                    if (strstr($dernierStatutEJDecision, Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME'))) {
                        $enabled = true;
                    }
                }
            } else {
                $enabled = true;
            }
            $this->panelFicheRecensement->setEnabled($enabled);
            $this->panelCategorieAchat->setEnabled($enabled);
            $this->setViewState('DesabledCategorieAchat', $enabled);
        } catch (Exception $e) {
            $logger->error('Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function isMapaInf()
    {
        if ((new CommonConsultation())->isMapaInf($this->getIdTypeProcedureDonneesConsultation())) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * affiche l'arboressance des pièces de notification.
     */
    public function displayPiecesNotification($piecesNotifsItems = null)
    {
        $tmpPath = null;
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $org = Atexo_CurrentUser::getCurrentOrganism();
                $idBlob = $this->getArborescencePiecesNotif();
                //On recupere le fichier dans les blobs
                if ($idBlob) {
                    $atexoBlob = new Atexo_Blob();
                    $fileContent = $atexoBlob->return_blob_to_client($idBlob, $org);
                    //On stock le fichier sur un dossier tmp
                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().date('dmY-his').time().$idBlob;
                    mkdir($tmpPath);
                    $fp = fopen($tmpPath.'/pieces_notification.zip', 'w');
                    fwrite($fp, $fileContent);
                    fclose($fp);
                }

                if (!$piecesNotifsItems) {
                    $piecesNotifsItems = $this->_echange->getPiecesNotifItems();
                }
                $arrayItemsNotif = explode(';', $piecesNotifsItems);
                if (is_array($arrayItemsNotif)) {
                    $scriptNotification = (new Atexo_Zip())->getArborescence(null, null, $arrayItemsNotif, $tmpPath.'/pieces_notification.zip', 'notif_item', false, false, true);
                }
                if ('' != $scriptNotification) {
                    $this->scriptNotif->Text = $scriptNotification;
                }
            } catch (\Exception $e) {
                $logger->error("Erreur lors de l'affichage des pieces de notification.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    //TODO adapter cette methode a l'objet contrat
    public function genererArborescencePiecesNotification()
    {
        $listeEchange = [];
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_contrat instanceof CommonTContratTitulaire) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $org = Atexo_CurrentUser::getCurrentOrganism();
                $dossierTmpPiecesNotif = Atexo_Config::getParameter('COMMON_TMP').'/Pieces_notification';
                $cheminPiecesNotification = $dossierTmpPiecesNotif.'/Pieces_notification';
                $cheminDecompressionZip = Atexo_Config::getParameter('COMMON_TMP').'/Notification';
                //On recupère les échanges pour cette décision
                // @TODO KBE Relation Echange
                $relationEchange = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($this->_contrat->getIdContratTitulaire(), Atexo_Config::getParameter('TYPE_RELATION_NOTIFICATION'));
                if (is_array($relationEchange) && $relationEchange[0]) {
                    $listeEchange = [];
                    foreach ($relationEchange as $relation) {
                        $echange = CommonEchangePeer::retrieveByPK($relation->getIdEchange(), $connexion);
                        if ($echange instanceof CommonEchange) {
                            $listeEchange[] = $echange;
                        }
                    }
                }
                if (is_dir($dossierTmpPiecesNotif)) {//Suppression du dossier tmp des pièces de notification
                    system('rm -R '.$dossierTmpPiecesNotif);
                }
                $nbrDossiersEchangesZip = 0;
                if (is_array($listeEchange) && $listeEchange[0]) {
                    if (is_dir($cheminDecompressionZip)) {
                        system('rm -R '.$cheminDecompressionZip);
                    }//On crée le chemin de decompression
                    system('mkdir '.$cheminDecompressionZip.'/');
                    if (!is_dir($dossierTmpPiecesNotif)) {//On crée le repertoire des pièces de notification s'il n'existe pas
                        system('mkdir '.$dossierTmpPiecesNotif);
                    }
                    system('mkdir '.$cheminPiecesNotification.'/'); //On créée le fichier zip de notification
                    system(" zip -R $cheminPiecesNotification.zip $cheminPiecesNotification/");
                    system('rm -R '.$cheminPiecesNotification.'/');
                    if ($this->_decision->getIdBlobPiecesNotification()) {
                        $atexoBlob = new Atexo_Blob(); //On recupere le fichier dans les blobs
                        $fileContent = $atexoBlob->return_blob_to_client($this->_decision->getIdBlobPiecesNotification(), $org);
                        //On stock le fichier sur un dossier tmp
                        $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().$this->_decision->getIdBlobPiecesNotification();
                        mkdir($tmpPath);
                        $fp = fopen($tmpPath.'/pieces_notification.zip', 'w');
                        fwrite($fp, $fileContent);
                        fclose($fp); //Décompression du dossier ->
                        system(" unzip $tmpPath/pieces_notification.zip -d ".$cheminDecompressionZip.' > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');
                    } //On génére l'excel registre de notification puis on l'ajoute au zip
                    $nameFileRegister = str_replace(
                        ' ',
                        '_',
                        Prado::localize('DEFINE_REGISTRE_NOTIFICATION')
                    ).'_'.$this->_contrat->getReferenceLibre().'_'.$this->_decision->getIdDecisionEnveloppe().'.xls';
                    $cheminRegistreNotification = (new Atexo_GenerationExcel())->generateExcelRegistreNotification($this->_decision->getConsultationId(), $listeEchange, true, null, true, false, $nameFileRegister);
                    (new Atexo_Zip())->addFileToZip($cheminRegistreNotification, $cheminPiecesNotification.'.zip');
                    //on crée les échanges de notification
                    foreach ($listeEchange as $echange) {
                        if ($echange instanceof CommonEchange) {
                            $nameRepNotification = 'Notification_'.$echange->getId().'_'.substr($echange->getDateMessage(), '0', '10');
                            $repNotificationEchange = Atexo_Config::getParameter('COMMON_TMP').'/'.$nameRepNotification;
                            //Si le rep de notification existe alors on l'ignore
                            if (!is_dir($cheminDecompressionZip.'/'.$nameRepNotification)) {
                                system("mkdir $repNotificationEchange");
                                $fileNotification = (new Atexo_Message())->generateNotificationFile($echange, Atexo_CurrentUser::getCurrentOrganism(), Atexo_Config::getParameter('COMMON_TMP').'notice1_'.session_name().session_id().date('Y-m-d-H-i-s').'.txt');
                                //On copie le fichier contenant les informations de notification dans le rep $cheminPiecesNotification./Notification_idEchange_date
                                system('mv '.$fileNotification['mailNotification'].' '.$repNotificationEchange.'/Notice_'.$echange->getId().'.txt');
                                //On crée le dossier pour deposer les pièces jointes de l'échange, si ce dossier des pj existe alors on l'ignore
                                if (!is_dir($cheminDecompressionZip.'/'.$nameRepNotification.'/Pieces_jointes')) {
                                    system("mkdir $repNotificationEchange/Pieces_jointes");
                                    //On recupere les pj et les stocker dans le dossier $repNotificationEchange/Pieces_jointes
                                    $listePj = $fileNotification['piecesjointes'];
                                    if (is_array($listePj) && $listePj[0]) {
                                        foreach ($listePj as $pj) {
                                            if ($pj[0]) {
                                                $cheminPjTmp = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().$pj[0];
                                                $cheminFilePjTmp = str_replace(' ', '', Atexo_Util::OterAccents($cheminPjTmp.'/'.$pj[1]));
                                                mkdir($cheminPjTmp);
                                                $atexoBlob = new Atexo_Blob();
                                                $pjContent = $atexoBlob->return_blob_to_client($pj[0], $org);
                                                $fp = fopen($cheminFilePjTmp, 'w');
                                                fwrite($fp, $pjContent);
                                                fclose($fp);
                                                //On copie le pj dans $repNotificationEchange/Pieces_jointes
                                                system('mv '.$cheminFilePjTmp.' '.$repNotificationEchange.'/Pieces_jointes/');
                                            }
                                        }
                                    } else {
                                        //Si pas de Pj dans le zip, on supprime le dossier des Pj
                                        system("rm -R $repNotificationEchange/Pieces_jointes/");
                                    }
                                }
                            } else {
                                //On compte le nombre de dossier d'échanges dans le zip decompressé
                                ++$nbrDossiersEchangesZip;
                            }
                            //On ajoute le dossier $repNotificationEchange à $cheminPiecesNotification.zip
                            (new Atexo_Zip())->addDirToZip($nameRepNotification, $cheminPiecesNotification.'.zip', Atexo_Config::getParameter('COMMON_TMP').'/');
                        }
                    }
                }
                //Enregistrement des pieces du zip dans les blobs, ce controle permet de modifier le blob qu'en cas de modification de dossier
                if ($nbrDossiersEchangesZip != count($listeEchange)) {
                    $atexoBlob = new Atexo_Blob();
                    $idBlob = $atexoBlob->insert_blob('Pieces_notification.zip', $cheminPiecesNotification.'.zip', $org);
                    $this->_decision->setIdBlobPiecesNotification($idBlob);
                    $this->_decision->save($connexion);
                }

                return $this->_decision->getIdBlobPiecesNotification();
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage de l'arborescence des pieces de notification.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function setArborescencePiecesNotif($v)
    {
        $this->_arborescencePiecesNotif = $v;
    }

    public function getArborescencePiecesNotif()
    {
        return $this->_arborescencePiecesNotif;
    }

    /**
     * Permet de contruire un fichier zip contenant le fichier de signature.
     *
     * @param $fichier: objet de la classe CommonFichierEnveloppe
     *
     * @return : un tableau contenant le nom du fichier de signature et son chemin
     */
    public function getFileSignature(CommonFichierEnveloppe $fichier, $extension)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if (0 != strcmp($fichier->getSignatureFichier(), '')) {
                $fileName = Atexo_Config::getParameter('COMMON_TMP').'/'.$fichier->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($fichier->getSignatureFichier());
                Atexo_Util::write_file($fileName, $fichier->getSignatureFichier());
                (new Atexo_Zip())->addFileToZip($fileName, $fileName.$extension);

                return [0 => Atexo_Config::getParameter('COMMON_TMP').'/', 1 => $fichier->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($fichier->getSignatureFichier()).$extension];
            }

            return false;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la construction du zip contenant le fichhier signature.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger les autres informations de la fiche de recensement.
     */
    public function displayAutresInformations()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_echange->getMontantHt()) {//Montant HT
                $this->montantHtAccord->Text = $this->_echange->getMontantHt();
            } else {
                //format du montant attendu dans la fiche Chorus nombre sans espace et avec .
                // @TODO KBE $this->montantHtAccord->Text = str_replace(' ','',str_replace(',','.',$this->_decision->getMontantMarche()));
                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                    $montant = $this->_contratExec['montant'] ?? '';
                } else {
                    $montant = $this->_contrat->getMontantContrat();
                }
                $this->montantHtAccord->Text = str_replace(' ', '', str_replace(',', '.', $montant));
            }
            $this->chargerListePays(); //Code pays  titulaire
            //Code pays  co-titulaire
            $script = $this->displayCodePaysCoTitulaire($this->getViewState('codePaysTitulaire'), $this->getViewState('codePaysCoTitulaires'));

            $this->javascript->Text .= "<script>$script</script>";
            //CCAG de référence
            if ($this->_echange->getCcagReference()) {
                $this->ccgaReference->SelectedValue = $this->_echange->getCcagReference();
            }
            //Numéro de siret du titulaire
            if ($this->_echange->getPourcentageAvance()) {
                $this->pourcentageAvance->Text = $this->_echange->getPourcentageAvance();
            }
            //Type d'avance
            if ($this->_echange->getTypeAvance()) {
                $this->typeAvance->SelectedValue = $this->_echange->getTypeAvance();
            }
            //Conditions de paiement
            //Identifiant accord cadre
            $script = '';
            if ($this->_echange->getIdentifiantAccordCadre()) {
                $this->idAccordCadre->Text = $this->_echange->getIdentifiantAccordCadre();
                $script .= "document.getElementById('div_idAccordCadre_navette').style.display='';";
                $script .= "document.getElementById('div_libelleAccordCadre_navette').style.display='';";
                $script .= "document.getElementById('div_dateCreationAccord').style.display='';";
            } else {
                $script .= "document.getElementById('div_idAccordCadre_navette').style.display='none';";
                $script .= "document.getElementById('div_libelleAccordCadre_navette').style.display='none';";
                $script .= "document.getElementById('div_dateCreationAccord').style.display='none';";
            }
            $this->javascript->Text .= "<script>$script</script>";
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage des autres informations.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger les données de titulaire et co-titulaire.
     *
     * @param string $codePaysTitulaire:   Code Pays titulaire
     * @param string $codePaysCoTitulaire: Code pays co-titulaire
     */
    public function displayCodePaysCoTitulaire($codePaysTitulaire = null, $codePaysCoTitulaire = null)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $script = '';
            if (!$codePaysTitulaire) {
                $codePaysTitulaire = $this->_echange->getCodePaysTitulaire();
            }
            if (!$codePaysCoTitulaire) {
                $codePaysCoTitulaire = $this->_echange->getCodesPaysCoTitulaire();
            }
            if ($codePaysTitulaire) {
                $this->codePays->SelectedValue = $codePaysTitulaire;
            }
            //Numéro de siret du titulaire
            if ($this->_echange->getNumeroSirenTitulaire() && $this->_echange->getNumeroSiretTitulaire()) {
                $this->sirenTitulaire->Text = $this->_echange->getNumeroSirenTitulaire();
                $this->siretTitulaire->Text = $this->_echange->getNumeroSiretTitulaire();
            }
            if ($this->codePays->SelectedValue == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
                $script .= "document.getElementById('blocSirenSiretTitulaire').style.display='';";
            } else {
                $script .= "document.getElementById('blocSirenSiretTitulaire').style.display='none';";
            }

            if ($codePaysCoTitulaire ||
                ($this->_echange->getNumeroSirenCoTitulaire() && $this->_echange->getNumeroSiretCoTitulaire())
            ) {
                if ($codePaysCoTitulaire) {
                    $arrayListeCodesPaysCoTitulaires = explode('#', $codePaysCoTitulaire);
                }
                if ($this->_echange->getNumeroSirenCoTitulaire() && $this->_echange->getNumeroSiretCoTitulaire()) {
                    $arraySirenTitulaires = explode('#', $this->_echange->getNumeroSirenCoTitulaire());
                    $arraySiretTitulaires = explode('#', $this->_echange->getNumeroSiretCoTitulaire());
                }
                $i = 1; //Les codes selectionnés
                $nombreCoTitulaires = (int) $this->nbEntrepriseCotraitantes->SelectedValue;
                foreach ($this->repeatCodePays->Items as $item) {
                    $idBlocCodePaysCoTitulaire = 'blocCodePaysCoTitulaire_'.$i;
                    if ($i > $nombreCoTitulaires) {
                        $script .= "document.getElementById('".$idBlocCodePaysCoTitulaire."').style.display='none';";
                        $script .= "document.getElementById('blocSirenSiretTitulaire_".$i."').style.display='none';";
                    }
                    if (is_array($arrayListeCodesPaysCoTitulaires) && '' != $arrayListeCodesPaysCoTitulaires[$i]) {
                        $item->codePaysCoTitulaire->SelectedValue = $arrayListeCodesPaysCoTitulaires[$i];
                        //Affichage du bloc 'blocCodePaysCoTitulaire_'.$i
                        $script .= "document.getElementById('".$idBlocCodePaysCoTitulaire."').style.display='';";

                        if ($arrayListeCodesPaysCoTitulaires[$i] == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
                            $script .= "document.getElementById('blocSirenSiretTitulaire_".$i."').style.display='';";
                        }
                    } else {
                        $item->codePaysCoTitulaire->SelectedValue = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
                    }
                    if (is_array($arraySirenTitulaires) && '' != $arraySirenTitulaires[$i]) {
                        $item->sirenCoTitulaire->Text = $arraySirenTitulaires[$i];
                    }
                    if (is_array($arraySiretTitulaires) && '' != $arraySiretTitulaires[$i]) {
                        $item->siretCoTitulaire->Text = $arraySiretTitulaires[$i];
                        $idBlocSirenSiretTitulaire = 'blocSirenSiretTitulaire_'.$i;
                        $script .= "document.getElementById('".$idBlocSirenSiretTitulaire."').style.display='';";
                    }
                    ++$i;
                }
            } else {
                foreach ($this->repeatCodePays->Items as $item) {
                    $item->codePaysCoTitulaire->SelectedValue = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
                }
            }

            return $script;
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage de la liste des codes pays titulaires et co-titulaires.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste des pays avec le code des pays
     * Pour le titulaire et les co-titulaires.
     */
    public function chargerListePays()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $listePays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListCountries(false);
            $listePaysWithCode = [];
            if (is_array($listePays) && count($listePays)) {
                foreach ($listePays as $pays) {
                    if ($pays instanceof CommonGeolocalisationN2) {
                        $listePaysWithCode[$pays->getDenomination2()] = $pays->getDenomination1().' - ('.$pays->getDenomination2().')';
                    }
                }
            }
            //Titulaire
            $this->codePays->DataSource = $listePaysWithCode;
            $this->codePays->DataBind();

            foreach ($this->repeatCodePays->Items as $item) {
                $item->codePaysCoTitulaire->DataSource = $listePaysWithCode;
                $item->codePaysCoTitulaire->DataBind();
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors du chargement de la liste des pays.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger le contenu de la fiche de navette.
     */
    public function displayFicheNavette()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            //Récuperation de la fiche de navette
            if ($this->_echange instanceof CommonChorusEchange) {
                if (
                    $this->_contrat instanceof CommonTContratTitulaire
                    || (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec)
                ) {

                    if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                        $intituleContrat = $this->_contratExec['intitule'] ?? '';
                        $raisonSociale = $this->_contratExec['etablissementTitulaire']['fournisseur']['raisonSociale'] ?? '';
                        $objetContrat = $this->_contratExec['objet'] ?? '';
                        $montantContrat = $this->_contratExec['montant'] ?? '';
                    } else {
                        $intituleContrat = $this->_contrat->getIntitule();
                        $raisonSociale = $this->_contrat->getNomTitulaireContrat();
                        $objetContrat = $this->_contrat->getObjetContrat();
                        $montantContrat = $this->_contrat->getMontantContrat();
                    }

                    $this->_ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($this->_echange->getId(), $this->_echange->getOrganisme());
                    if (!($this->_ficheNavette instanceof CommonChorusFicheNavette)) {
                        $this->_ficheNavette = new CommonChorusFicheNavette();
                        $this->_ficheNavette->setIdChorusEchange($this->_echange->getId());
                        $this->_ficheNavette->setOrganisme($this->_echange->getOrganisme());
                        $this->_ficheNavette->setNomAcheteur($this->_echange->getNomCreateur());
                        $this->_ficheNavette->setIntituleMarche($intituleContrat);
                        $this->_ficheNavette->setRaisonSociale($raisonSociale);
                        $this->_ficheNavette->setObjet($objetContrat);
                        $this->_ficheNavette->setMontantHt(str_replace(' ', '', str_replace(',', '.', $montantContrat)));
                        $this->_ficheNavette->save($connexion);
                    }
                    $brouillon = ($this->_echange->getStatutechange() == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON')) ? true : false;
                    //Informations consultation
                    $listeServiceWithPath = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme(Atexo_CurrentUser::getCurrentOrganism(), true);
                    $serviceRattachement = '';
                    if (null != $this->getServiceIdDonneesConsultation()) {
                        $serviceRattachement = $listeServiceWithPath[$this->getServiceIdDonneesConsultation()];
                    }

                    //Représentant du pouvoir adjudicateur
                    if ($this->_ficheNavette->getRpa()) {
                        $this->representantPA->Text = $this->_ficheNavette->getRpa();
                    } else {
                        $this->representantPA->Text = $serviceRattachement;
                    }
                    //Nom de l'acheteur
                    $this->nomAcheteur->Text = $this->_ficheNavette->getNomAcheteur();
                    //Intitulé du marché
                    $this->intituleMarche->Text = $this->_ficheNavette->getIntituleMarche();
                    if (($brouillon && $intituleContrat) || !$this->_ficheNavette->getIntituleMarche()) {
                        $this->intituleMarche->Text = $intituleContrat;
                    } elseif ($this->_ficheNavette->getIntituleMarche()) {
                        $this->intituleMarche->Text = $this->_ficheNavette->getIntituleMarche();
                    }
                    //Objet du marché
                    if (($brouillon && $objetContrat) || !$this->_ficheNavette->getObjet()) {
                        $this->objetMarche->Text = $objetContrat;
                    } elseif ($this->_ficheNavette->getObjet()) {
                        $this->objetMarche->Text = $this->_ficheNavette->getObjet();
                    }
                    //Raison sociale tiers
                    $this->raisonSocialeTiers->Text = $this->_ficheNavette->getRaisonSociale();
                    //Fiche immobilisation
                    if ('1' == $this->_ficheNavette->getFicheImmobilisation()) {
                        $this->ficheImmobilisationOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getFicheImmobilisation()) {
                        $this->ficheImmobilisationNon->checked = true;
                    } else {
                        $this->ficheImmobilisationNonRenseigne->checked = true;
                    }
                    //En attente de PJ
                    if ('1' == $this->_ficheNavette->getAttentePj()) {
                        $this->attentePjOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getAttentePj()) {
                        $this->attentePjNon->checked = true;
                    } else {
                        $this->attentePjNonRenseigne->checked = true;
                    }
                    //Marché partagé
                    if ('1' == $this->_ficheNavette->getMarchePartage()) {
                        $this->marchePartageOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getMarchePartage()) {
                        $this->marchePartageNon->checked = true;
                    } else {
                        $this->marchePartageNonRenseigne->checked = true;
                    }
                    //Identifiant accord cadre
                    if ($this->_ficheNavette->getIdAccordCadre()) {
                        $this->idAccordCadre_navette->Text = $this->_ficheNavette->getIdAccordCadre();
                        $this->hiddenIdAccordCadre_navette->value = $this->_ficheNavette->getIdAccordCadre();
                    } else {
                        $this->idAccordCadre_navette->Text = $this->_echange->getIdentifiantAccordCadre();
                        $this->hiddenIdAccordCadre_navette->value = $this->_echange->getIdentifiantAccordCadre();
                    }
                    //Libellé accord cadre
                    if ($this->_ficheNavette->getLibAccordCadre()) {
                        $this->libelleAccordCadre_navette->Text = $this->_ficheNavette->getLibAccordCadre();
                    }
                    //Année de création de l'accord
                    $this->chargerListeAnneeAccordCadre();
                    if ($this->_ficheNavette->getAnneeCreationAccordCadre()) {
                        $this->dateCreationAccord->SelectedValue = $this->_ficheNavette->getAnneeCreationAccordCadre();
                    }
                    //Montant HT
                    if (($brouillon && $montantContrat) || !$this->_ficheNavette->getMontantHt()) {
                        //format du montant attendu dans la fiche Chorus nombre sans espace et avec .
                        // @TODO KBE $this->montantHtAccordFicheNavette->Text = str_replace(' ','',str_replace(',','.',$this->_decision->getMontantMarche()));
                        $this->montantHtAccordFicheNavette->Text = str_replace(' ', '', str_replace(',', '.', $montantContrat));
                    } elseif ($this->_ficheNavette->getMontantHt()) {
                        $this->montantHtAccordFicheNavette->Text = $this->_ficheNavette->getMontantHt();
                    }
                    //Reconductible
                    if ('1' == $this->_ficheNavette->getReconductible()) {
                        $this->ReconductibleOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getReconductible()) {
                        $this->ReconductibleNon->checked = true;
                    } else {
                        $this->ReconductibleNonRenseigne->checked = true;
                    }
                    //Visa ACCF
                    if ('1' == $this->_ficheNavette->getVisaAccf()) {
                        $this->visaACCFOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getVisaAccf()) {
                        $this->visaACCFNon->checked = true;
                    } else {
                        $this->visaACCFNonRenseigne->checked = true;
                    }
                    //Visa préfet
                    if ('1' == $this->_ficheNavette->getVisaPrefet()) {
                        $this->visaPrefetOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getVisaPrefet()) {
                        $this->visaPrefetNon->checked = true;
                    } else {
                        $this->visaPrefetNonRenseigne->checked = true;
                    }
                    //Retenue de garantie
                    if ('1' == $this->_ficheNavette->getRetenueGarantie()) {
                        $this->retenueGarantieOui->checked = true;
                    } elseif ('0' == $this->_ficheNavette->getRetenueGarantie()) {
                        $this->retenueGarantieNon->checked = true;
                    } else {
                        $this->retenueGarantieNonRenseigne->checked = true;
                    }
                    //Montant mini
                    if ($this->_ficheNavette->getMontantMini()) {
                        $this->montantMini->Text = $this->_ficheNavette->getMontantMini();
                    }
                    //Montant maxi
                    if ($this->_ficheNavette->getMontantMaxi()) {
                        $this->montantMaxi->Text = $this->_ficheNavette->getMontantMaxi();
                    }
                    //Montant partie forfaitaire
                    if ($this->_ficheNavette->getMontantForfaitaire()) {
                        $this->montantPartieForfaitaire->Text = $this->_ficheNavette->getMontantForfaitaire();
                    }
                    //Montant partie à BC
                    if ($this->_ficheNavette->getMontantBc()) {
                        $this->montantPartieBC->Text = $this->_ficheNavette->getMontantBc();
                    }
                    //Taux TVA
                    if ($this->_ficheNavette->getTauxTva()) {
                        $this->tauxTVA->Text = $this->_ficheNavette->getTauxTva();
                    }
                    //Centre de coût (si unique)
                    if ($this->_ficheNavette->getCentreCout()) {
                        $this->centreCout->Text = $this->_ficheNavette->getCentreCout();
                    }
                    //Centre financier (si unique)
                    if ($this->_ficheNavette->getCentreFinancier()) {
                        $this->centreFinancier->Text = $this->_ficheNavette->getCentreFinancier();
                    }
                    //Activité (si unique)
                    if ($this->_ficheNavette->getActivite()) {
                        $this->activite->Text = $this->_ficheNavette->getActivite();
                    }
                    //Domaine fonctionnel (si unique)
                    if ($this->_ficheNavette->getDomaineFonctionnel()) {
                        $this->domaineFonctionnel->Text = $this->_ficheNavette->getDomaineFonctionnel();
                    }
                    //Fond (si unique)
                    if ($this->_ficheNavette->getFond()) {
                        $this->fond->Text = $this->_ficheNavette->getFond();
                    }
                    //Localisation interministérielle (si unique)
                    if ($this->_ficheNavette->getLocalisationInterministerielle()) {
                        $this->localisationInterministerielle->Text = $this->_ficheNavette->getLocalisationInterministerielle();
                    }
                    //Nature (si unique)
                    if ($this->_ficheNavette->getNature()) {
                        $this->nature->Text = $this->_ficheNavette->getNature();
                    }

                    //Axe ministériel 1 (si unique)
                    if ($this->_ficheNavette->getAxeMinisteriel1()) {
                        $this->axeMinisteriel_01->Text = $this->_ficheNavette->getAxeMinisteriel1();
                    }
                    //Projet analytique (si unique)
                    if ($this->_ficheNavette->getProjetAnalytique()) {
                        $this->projetAnalytique->Text = $this->_ficheNavette->getProjetAnalytique();
                    }
                    //Localisation ministérielle (si unique)
                    if ($this->_ficheNavette->getLocalisationMinisterielle()) {
                        $this->localisationMinisterielle->Text = $this->_ficheNavette->getLocalisationMinisterielle();
                    }
                    //Axe ministériel 2 (si unique)
                    if ($this->_ficheNavette->getAxeMinisteriel2()) {
                        $this->axeMinisteriel_02->Text = $this->_ficheNavette->getAxeMinisteriel2();
                    }
                    //Remarques
                    if ($this->_ficheNavette->getRemarques()) {
                        $this->remarques->Text = $this->_ficheNavette->getRemarques();
                    }
                }
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage de la fiche navette.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger la liste de selection de l'identifiant de l'accord cadre.
     */
    public function chargerListeAnneeAccordCadre()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $listeIdAccordCadre = [];
            $listeIdAccordCadre['0'] = Prado::localize('DEFINE_TEXT_SELECTIONNER');
            for ($i = date('Y'); $i >= 2008; --$i) {
                $listeIdAccordCadre[$i] = $i;
            }
            $this->dateCreationAccord->DataSource = $listeIdAccordCadre;
            $this->dateCreationAccord->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors du chargement de la liste des annees de l'accord cadre/sad ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de charger l'objet fiche de navette.
     *
     * @param CommonChorusFicheNavette $ficheNavette: en passage par référence
     */
    public function chargerObjetFicheNavette(&$ficheNavette, $echange)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            //Représentant du pouvoir adjudicateur
            if ('' != $this->representantPA->Text) {
                $this->_ficheNavette->setRpa(($this->representantPA->Text));
            } else {
                $this->_ficheNavette->setRpa('');
            }
            //Nom de l'acheteur
            $this->_ficheNavette->setNomAcheteur(($this->nomAcheteur->Text));
            //Intitulé du marché
            $this->_ficheNavette->setIntituleMarche(($this->intituleMarche->Text));
            //Objet du marché
            if ('' != $this->objetMarche->Text) {
                $this->_ficheNavette->setObjet(($this->objetMarche->Text));
            } else {
                $this->_ficheNavette->setObjet('');
            }
            //Raison sociale tiers
            $this->_ficheNavette->setRaisonSociale(($this->raisonSocialeTiers->Text));
            //Fiche immobilisation
            if ($this->ficheImmobilisationOui->checked) {
                $this->_ficheNavette->setFicheImmobilisation('1');
            } elseif ($this->ficheImmobilisationNon->checked) {
                $this->_ficheNavette->setFicheImmobilisation('0');
            } elseif ($this->ficheImmobilisationNonRenseigne->checked) {
                $this->_ficheNavette->setFicheImmobilisation(null);
            }
            //En attente de PJ
            if ($this->attentePjOui->checked) {
                $this->_ficheNavette->setAttentePj('1');
            } elseif ($this->attentePjNon->checked) {
                $this->_ficheNavette->setAttentePj('0');
            } elseif ($this->attentePjNonRenseigne->checked) {
                $this->_ficheNavette->setAttentePj(null);
            }
            //Marché partagé
            if ($this->marchePartageOui->checked) {
                $this->_ficheNavette->setMarchePartage('1');
            } elseif ($this->marchePartageNon->checked) {
                $this->_ficheNavette->setMarchePartage('0');
            } elseif ($this->marchePartageNonRenseigne->checked) {
                $this->_ficheNavette->setMarchePartage(null);
            }
            //Identifiant accord cadre
            if ($echange->getIdentifiantAccordCadre() && $this->hiddenIdAccordCadre_navette->value) {
                $this->_ficheNavette->setIdAccordCadre(($this->hiddenIdAccordCadre_navette->value));
            } else {
                $this->_ficheNavette->setIdAccordCadre('');
            }
            //Libellé accord cadre
            if ($echange->getIdentifiantAccordCadre() && '' != $this->libelleAccordCadre_navette->Text) {
                $this->_ficheNavette->setLibAccordCadre(($this->libelleAccordCadre_navette->Text));
            } else {
                $this->_ficheNavette->setLibAccordCadre('');
            }
            //Année de création de l'accord
            if ($echange->getIdentifiantAccordCadre() && $this->dateCreationAccord->SelectedValue) {
                $this->_ficheNavette->setAnneeCreationAccordCadre($this->dateCreationAccord->SelectedValue);
            } else {
                $this->_ficheNavette->setAnneeCreationAccordCadre('');
            }
            //Montant HT
            if ('' != $this->montantHtAccordFicheNavette->Text) {
                $this->_ficheNavette->setMontantHt(($this->montantHtAccordFicheNavette->Text));
            } else {
                $this->_ficheNavette->setMontantHt('');
            }

            //Reconductible
            if ($this->ReconductibleOui->checked) {
                $this->_ficheNavette->setReconductible('1');
            } elseif ($this->ReconductibleNon->checked) {
                $this->_ficheNavette->setReconductible('0');
            } elseif ($this->ReconductibleNonRenseigne->checked) {
                $this->_ficheNavette->setReconductible(null);
            }
            //Visa ACCF
            if ($this->visaACCFOui->checked) {
                $this->_ficheNavette->setVisaAccf('1');
            } elseif ($this->visaACCFNon->checked) {
                $this->_ficheNavette->setVisaAccf('0');
            } elseif ($this->visaACCFNonRenseigne->checked) {
                $this->_ficheNavette->setVisaAccf(null);
            }
            //Visa préfet
            if ($this->visaPrefetOui->checked) {
                $this->_ficheNavette->setVisaPrefet('1');
            } elseif ($this->visaPrefetNon->checked) {
                $this->_ficheNavette->setVisaPrefet('0');
            } elseif ($this->visaPrefetNonRenseigne->checked) {
                $this->_ficheNavette->setVisaPrefet(null);
            }
            //Retenue de garantie
            if ($this->retenueGarantieOui->checked) {
                $this->_ficheNavette->setRetenueGarantie('1');
            } elseif ($this->retenueGarantieNon->checked) {
                $this->_ficheNavette->setRetenueGarantie('0');
            } elseif ($this->retenueGarantieNonRenseigne->checked) {
                $this->_ficheNavette->setRetenueGarantie(null);
            }
            //Montant mini
            if ('' != $this->montantMini->Text) {
                $this->_ficheNavette->setMontantMini(($this->montantMini->Text));
            } else {
                $this->_ficheNavette->setMontantMini('');
            }
            //Montant maxi
            if ('' != $this->montantMaxi->Text) {
                $this->_ficheNavette->setMontantMaxi(($this->montantMaxi->Text));
            } else {
                $this->_ficheNavette->setMontantMaxi('');
            }
            //Montant partie forfaitaire
            if ('' != $this->montantPartieForfaitaire->Text) {
                $this->_ficheNavette->setMontantForfaitaire(($this->montantPartieForfaitaire->Text));
            } else {
                $this->_ficheNavette->setMontantForfaitaire('');
            }
            //Montant partie à BC
            if ('' != $this->montantPartieBC->Text) {
                $this->_ficheNavette->setMontantBc(($this->montantPartieBC->Text));
            } else {
                $this->_ficheNavette->setMontantBc('');
            }
            //Taux TVA
            if ('' != $this->tauxTVA->Text) {
                $this->_ficheNavette->setTauxTva(($this->tauxTVA->Text));
            } else {
                $this->_ficheNavette->setTauxTva('');
            }
            //Centre de coût (si unique)
            if ('' != $this->centreCout->Text) {
                $this->_ficheNavette->setCentreCout(($this->centreCout->Text));
            } else {
                $this->_ficheNavette->setCentreCout('');
            }
            //Centre financier (si unique)
            if ('' != $this->centreFinancier->Text) {
                $this->_ficheNavette->setCentreFinancier(($this->centreFinancier->Text));
            } else {
                $this->_ficheNavette->setCentreFinancier('');
            }
            //Activité (si unique)
            if ('' != $this->activite->Text) {
                $this->_ficheNavette->setActivite(($this->activite->Text));
            } else {
                $this->_ficheNavette->setActivite('');
            }
            //Domaine fonctionnel (si unique)
            if ('' != $this->domaineFonctionnel->Text) {
                $this->_ficheNavette->setDomaineFonctionnel(($this->domaineFonctionnel->Text));
            } else {
                $this->_ficheNavette->setDomaineFonctionnel('');
            }
            //Fond (si unique)
            if ('' != $this->fond->Text) {
                $this->_ficheNavette->setFond(($this->fond->Text));
            } else {
                $this->_ficheNavette->setFond('');
            }
            //Localisation interministérielle (si unique)
            if ('' != $this->localisationInterministerielle->Text) {
                $this->_ficheNavette->setLocalisationInterministerielle(($this->localisationInterministerielle->Text));
            } else {
                $this->_ficheNavette->setLocalisationInterministerielle('');
            }
            //Nature (si unique)
            if ('' != $this->nature->Text) {
                $this->_ficheNavette->setNature(($this->nature->Text));
            } else {
                $this->_ficheNavette->setNature('');
            }
            //Axe ministériel 1 (si unique)
            if ('' != $this->axeMinisteriel_01->Text) {
                $this->_ficheNavette->setAxeMinisteriel1(($this->axeMinisteriel_01->Text));
            } else {
                $this->_ficheNavette->setAxeMinisteriel1('');
            }
            //Projet analytique (si unique)
            if ('' != $this->projetAnalytique->Text) {
                $this->_ficheNavette->setProjetAnalytique(($this->projetAnalytique->Text));
            } else {
                $this->_ficheNavette->setProjetAnalytique('');
            }
            //Localisation ministérielle (si unique)
            if ('' != $this->localisationMinisterielle->Text) {
                $this->_ficheNavette->setLocalisationMinisterielle(($this->localisationMinisterielle->Text));
            } else {
                $this->_ficheNavette->setLocalisationMinisterielle('');
            }
            //Axe ministériel 2 (si unique)
            if ('' != $this->axeMinisteriel_02->Text) {
                $this->_ficheNavette->setAxeMinisteriel2(($this->axeMinisteriel_02->Text));
            } else {
                $this->_ficheNavette->setAxeMinisteriel2('');
            }
            //Remarques
            if ('' != $this->remarques->Text) {
                $this->_ficheNavette->setRemarques(($this->remarques->Text));
            } else {
                $this->_ficheNavette->setRemarques('');
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors du chargement de la fiche navette.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de retourner s'il faut envoyer le fichier FIR
     *     => Pas d'envoi de FIR pour le cas du FEN211.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function echangeSendFluxFir()
    {
        if ($this->_echange instanceof CommonChorusEchange) {
            try {
                (new Atexo_Chorus_Util())->loggerInfos('Precision du type de flux : Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');
                $numOrdre = intval($this->numOrdre->Text);
                if ($this->isContratAcSad() || $this->_echange->hasToutPaysEtranger()) {
                    // envoyer FEN
                    (new Atexo_Chorus_Util())->loggerInfos('Type de flux a envoyer = FEN , Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');

                    return '0';
                } else {
                    $codePayFr = $this->_echange->hasCodePaysByType(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                    if ($numOrdre > 1 && !$codePayFr) {
                        (new Atexo_Chorus_Util())->loggerInfos('Type de flux = FEN (mise a jour de statut pour envoi via la routine), Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');

                        return '2'; //change statut
                    } else {
                        (new Atexo_Chorus_Util())->loggerInfos('Type de flux a envoyer = FIR , Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().']');

                        return '1'; //envoyer FIR
                    }
                }
            } catch (\Exception $e) {
                (new Atexo_Chorus_Util())->loggerErreur('Erreur lors de la precision du type de flux , Echange [Id = '.$this->_echange->getId().' , Organisme = '.$this->_echange->getOrganisme().'], Methode = ChorusDetailEchanges::echangeSendFluxFir , '.PHP_EOL.' Exception = [ '.$e->getMessage().' ]');
            }
        } else {
            (new Atexo_Chorus_Util())->loggerErreur('Echange Chorus introuvable');
        }
    }

    /*
     * Pemet de tester si les champs dans la fiche navette sont vide
     */
    public function isChampVideFicheNavette()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ('' == trim($this->montantMini->Text)
                && $this->ficheImmobilisationNonRenseigne->checked && $this->attentePjNonRenseigne->checked && $this->marchePartageNonRenseigne->checked && $this->ReconductibleNonRenseigne->checked
                && $this->visaACCFNonRenseigne->checked && $this->visaPrefetNonRenseigne->checked && $this->retenueGarantieNonRenseigne->checked && '' == trim($this->montantPartieBC->Text)
                && '' == trim($this->montantMaxi->Text) && '' == trim($this->montantPartieForfaitaire->Text) && '' == trim($this->tauxTVA->Text) && '' == trim($this->centreCout->Text)
                && '' == trim($this->centreFinancier->Text) && '' == trim($this->activite->Text) && '' == trim($this->domaineFonctionnel->Text) && '' == trim($this->fond->Text)
                && '' == trim($this->localisationInterministerielle->Text) && '' == trim($this->nature->Text) && '' == trim($this->axeMinisteriel_01->Text) && '' == trim($this->projetAnalytique->Text)
                && '' == trim($this->localisationMinisterielle->Text) && '' == trim($this->axeMinisteriel_02->Text) && '' == trim($this->remarques->Text)
                && ('' == trim($this->idAccordCadre->Text) || ('' != trim($this->idAccordCadre->Text) && '' == trim($this->libelleAccordCadre_navette->Text) && !$this->dateCreationAccord->SelectedValue))
            ) {
                return true;
            }

            return false;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la verification de la fiche navette.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /*
     * Peremet de valider les données echange chorus
     */
    public function validerDonneesEchangeChorus($sender, $param)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $numOrdre = (int) $this->numOrdre->Text;
            $erreur = false;
            $messagesPopin = [];
            $messages = [];
            $script = '';
            self::validerCategorieAchat($erreur, $messages);
            if (!$this->isContratAcSad()) {
                self::validerSirets($erreur, $messages);
            } else {
                self::validerSiretDonneesContrat($erreur, $messages);
            }
            if (!$this->isContratAcSad()) {
                if (!$this->isTypeEnvoiFicheModificative()) {
                    self::validerDonneesFicheRecenssement($erreur, $messages);
                }
            } else {
                self::validerDonneesContrat($erreur, $messages);
            }
            if ((!$this->isTypeEnvoiFicheModificative())) {
                $nbrElement = self::getNbrFileChecked();
                if (0 == $nbrElement) {
                    $erreur = true;
                    $messagesPopin[] = Prado::localize('AUCUNE_PIECE_JOINTE_CHORUS');
                }
                if (!$this->isContratAcSad()) {
                    if (1 == $numOrdre) {//1er envoi
                        $champVide = self::isChampVideFicheNavette();
                        if ($champVide) {
                            $erreur = true;
                            $messagesPopin[] = Prado::localize('FICHE_NAVETTE_NON_SAISIE');
                        }
                    } else {//prochains envois
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                            $isTypeEnvoiContratANotifieEtEjCommande = (new Atexo_Chorus_Util())->isTypeEnvoiContratANotifieEtEjCommande($this->_contratExec);
                        } else {
                            $isTypeEnvoiContratANotifieEtEjCommande = (new Atexo_Chorus_Util())->isTypeEnvoiContratANotifieEtEjCommande($this->_contrat);
                        }
                        if ($isTypeEnvoiContratANotifieEtEjCommande) {
                            $erreur = true;
                            $messagesPopin[] = Prado::localize('MESSAGE_AVERTISSEMENT_SAISIR_DATES_REELLES_NOTIFICATION_FIN_MARCHE');
                        }
                    }
                }
            } else {//Scenario fiche modificative
                if (!$this->isContratAcSad() && 0 == $this->getNbrPjsFicheModificative()) {
                    $erreur = true;
                    $messagesPopin[] = Prado::localize('MESSAGE_AVERTISSEMENT_AUCUNE_PIECE_JOINTE_SELECTIONNEE_DANS_ONGLET_FICHE_MODIFICATIVE');
                }
            }
            if (is_countable($messages) ? count($messages) : 0) {
                $errorMsg = implode('</li><li>', $messages);
                $errorMsg = '<li>'.$errorMsg.'</li>';
                $this->divValidationSummary->addServerError($errorMsg, false);
                $script = "document.getElementById('divValidationSummary').style.display='';".
                    "document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';".
                    "document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';";
            } elseif (is_countable($messagesPopin) ? count($messagesPopin) : 0) {
                $errorMsg = implode('</li><li>', $messagesPopin);
                $errorMsg = '<li>'.$errorMsg.'</li>';
                $this->panelInfo->setMessage('<ul>'.$errorMsg.'</ul>');
                $script = "openModal('demander-validation','popup-small2',document.getElementById('ctl0_CONTENU_PAGE_titrePopin'));";
                $script .= "document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';".
                    "document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';";
            }
            if ($erreur) {
                $param->IsValid = false;
                $this->javascriptValidation->Text = '<script>'.$script.'</script>';
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de la validation des donnees de l'echange CHORUS.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /*
     * Permet de retourner le nombre des fichiers checked
     */
    public function getNbrFileChecked()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $nbrElement = 0;
            $dce = (new Atexo_Consultation_Dce())->getPathBlobDCE($this->getReferenceDonneesConsultation(), $this->_organisme);
            $arrayIndexSelectedFilesDce = $this->selectedFilesForZip($dce);
            if (is_array($arrayIndexSelectedFilesDce)) {
                $nbrElement += count($arrayIndexSelectedFilesDce);
            }
            $idBLob = $this->getArborescencePiecesNotif();
            $arrayIndexSelectedFilesNotif = [];
            if ($idBLob) {
                $atexoBlob = new Atexo_Blob();
                $fileContent = $atexoBlob->return_blob_to_client($idBLob, $this->_organisme);
                $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().date('Ymdhis').time().$idBLob;
                if (!is_dir($tmpPath)) {
                    mkdir($tmpPath);
                }
                Atexo_Util::write_file($tmpPath.'/pieces_notification.zip', $fileContent, 'w');
                $arrayIndexSelectedFilesNotif = $this->selectedFilesForZip($tmpPath.'/pieces_notification.zip', 'notif_item');
            }
            if (is_array($arrayIndexSelectedFilesNotif)) {
                $nbrElement += count($arrayIndexSelectedFilesNotif);
            }
            $idsBlobZip = $this->idBlob->Value;
            $idsBlobSignatureZip = $this->idBlobSignature->Value;
            $arrayIdsBlobZip = explode('#', $idsBlobZip);
            if (is_array($arrayIdsBlobZip)) {
                foreach ($arrayIdsBlobZip as $oneIdBlobZip) {
                    if ($arrayIdsBlobZip[0] != $oneIdBlobZip) {
                        $blobResource = Atexo_Blob::acquire_lock_on_blob($oneIdBlobZip, $this->_organisme);
                        $blob_file = $blobResource['blob']['pointer'];
                        if ($this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobZip)) {
                            ++$nbrElement;
                        }
                    }
                }
            }
            $arrayIdsBlobSignatureZip = explode('#', $idsBlobSignatureZip);
            if (is_array($arrayIdsBlobSignatureZip)) {
                foreach ($arrayIdsBlobSignatureZip as $oneIdBlobSignatureZip) {
                    if ($arrayIdsBlobSignatureZip[0] != $oneIdBlobSignatureZip) {
                        $blobResource = Atexo_Blob::acquire_lock_on_blob($oneIdBlobSignatureZip, $this->_organisme);
                        $blob_file = $blobResource['blob']['pointer'];
                        if ($this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobSignatureZip)) {
                            ++$nbrElement;
                        }
                    }
                }
            }
            $arrayIdsBlobZip = explode('#', $this->idsBlobNonZip->Value);
            $idsBlobNonZip = [];
            if ($arrayIdsBlobZip && count($arrayIdsBlobZip)) {
                foreach ($arrayIdsBlobZip as $idBlobNonZip) {
                    if ($arrayIdsBlobZip[0] != $idBlobNonZip) {
                        if ($_POST['env_nonZip_'.$idBlobNonZip]) {
                            $idsBlobNonZip[] = $idBlobNonZip;
                        }
                    }
                }
            }
            $nbrElement += count($idsBlobNonZip);
            $arrayIdsBlobSignZip = explode('#', $this->idsBlobSignatureNonZip->Value);
            $idsBlobSignNonZip = [];
            if ($arrayIdsBlobSignZip && count($arrayIdsBlobSignZip)) {
                foreach ($arrayIdsBlobSignZip as $idBlobSignNonZip) {
                    if ($arrayIdsBlobSignZip[0] != $idBlobSignNonZip) {
                        if ($_POST['env_nonZip_'.$idBlobSignNonZip]) {
                            $idsBlobSignNonZip[] = $idBlobSignNonZip;
                        }
                    }
                }
            }
            $nbrElement += count($idsBlobSignNonZip);
            if ($this->acteEngagement->Checked) {
                ++$nbrElement;
            }
            if ($this->signActeEngagement->Checked) {
                ++$nbrElement;
            }
            foreach ($this->repeaterType->getItems() as $oneItemType) {
                foreach ($oneItemType->repeaterTypeDoc->getItems() as $oneItem) {
                    if ('ZIP' == strtoupper(Atexo_Util::getExtension($oneItem->nameFile->Value))) {
                        $blobResource = Atexo_Blob::acquire_lock_on_blob($oneItem->idBlob->Value, $this->_organisme);
                        $blob_file = $blobResource['blob']['pointer'];
                        $selectedIndex = $this->selectedFilesForZip($blob_file, 'zip_item');
                        if ($selectedIndex && is_array($selectedIndex)) {
                            $nbrElement += count($selectedIndex);
                        }
                    } elseif ($oneItem->pieceChecked->Checked) {
                        ++$nbrElement;
                    }
                }
            }
            foreach ($this->repeaterPiecesOuvertureAnalyse->getItems() as $oneItemOuverture) {
                if ($oneItemOuverture->pieceChecked->checked) {
                    ++$nbrElement;
                }
            }
            $pj = (new Atexo_Chorus_Echange())->retreivePiecesExternes($this->_echange->getId(), $this->_organisme);
            if ($pj) {
                $nbrElement += is_countable($pj) ? count($pj) : 0;
            }

            return $nbrElement;
        } catch (Exception $e) {
            $logger->error('Erreur lors du calcul du nombre de fichiers selectionnes.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de sauvegarder les codes pays, siren, siret pour les titutlaires et co-traitants
     * Puis mettre les codes pays dans le viewState de la  page.
     */
    public function saveCodePaysTitulairesCoTraitants()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            //Code pays titulaire
            if ($this->codePays->SelectedValue) {
                $this->_echange->setCodePaysTitulaire($this->codePays->SelectedValue);
            }
            //Siren et siret pays titulaire
            if ('FR' == $this->codePays->SelectedValue) {
                if ('' != $this->siretTitulaire->Text) {
                    $this->_echange->setNumeroSiretTitulaire(($this->siretTitulaire->Text));
                }
                if ('' != $this->sirenTitulaire->Text) {
                    $this->_echange->setNumeroSirenTitulaire(($this->sirenTitulaire->Text));
                }
            } else {
                $this->_echange->setNumeroSiretTitulaire('');
                $this->_echange->setNumeroSirenTitulaire('');
            }
            $this->setViewState('codePaysTitulaire', $this->codePays->SelectedValue);

            //Code pays co-titulaires
            $nombreCoTitulaires = (int) $this->nbEntrepriseCotraitantes->SelectedValue;
            $codePaysCoTitulaires = '#';
            $sirenCoTitulaire = '#';
            $siretCoTitulaire = '#';
            $i = 0;
            if ('TUAST' != $this->TypeGroupement->SelectedValue) {
                foreach ($this->repeatCodePays->Items as $item) {
                    if ($i >= $nombreCoTitulaires) {
                        break;
                    }
                    $codePaysCoTitulaires .= $item->codePaysCoTitulaire->SelectedValue.'#';
                    if ('FR' == $item->codePaysCoTitulaire->SelectedValue) {
                        $sirenCoTitulaire .= $item->sirenCoTitulaire->Text.'#';
                        $siretCoTitulaire .= $item->siretCoTitulaire->Text.'#';
                    } else {
                        $sirenCoTitulaire .= '#';
                        $siretCoTitulaire .= '#';
                    }
                    ++$i;
                }
            }
            if ('TUAST' == $this->TypeGroupement->SelectedValue || 0 == $nombreCoTitulaires) {
                $codePaysCoTitulaires = '';
                $siretCoTitulaire = '';
                $sirenCoTitulaire = '';
            }
            if ('#' != $codePaysCoTitulaires) {
                $this->_echange->setCodesPaysCoTitulaire($codePaysCoTitulaires);
            } else {
                $this->_echange->setCodesPaysCoTitulaire('');
            }
            $this->setViewState('codePaysCoTitulaires', $codePaysCoTitulaires);
            if ('#' != $sirenCoTitulaire && '#' != $siretCoTitulaire) {
                $this->_echange->setNumeroSiretCoTitulaire(($siretCoTitulaire));
                $this->_echange->setNumeroSirenCoTitulaire(($sirenCoTitulaire));
            } else {
                $this->_echange->setNumeroSiretCoTitulaire('');
                $this->_echange->setNumeroSirenCoTitulaire('');
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la sauvegarde des codes pays titulaires et co-titulaires '.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de sauvegarder les pièces de DCE selectionnées et met les index selectionnés dans le viewState de la page
     * $this->getViewState("arrayIndexSelectedFilesDce") est vide lorsqu'on n'est pas dans le postback de la page.
     */
    public function sauvegarderPiecesDceSelectionnees()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $dce = (new Atexo_Consultation_Dce())->getPathBlobDCE($this->getReferenceDonneesConsultation(), $this->_organisme);
                if ($dce) {
                    $arrayIndexSelectedFilesDce = $this->selectedFilesForZip($dce);
                    if (is_array($arrayIndexSelectedFilesDce) && count($arrayIndexSelectedFilesDce) > 0) {
                        $this->_echange->setDceItems(implode(';', $arrayIndexSelectedFilesDce));
                        $this->setViewState('arrayIndexSelectedFilesDce', implode(';', $arrayIndexSelectedFilesDce));
                    } else {
                        $this->_echange->setDceItems('');
                        $this->setViewState('arrayIndexSelectedFilesDce', '');
                    }
                }
            } catch (\Exception $e) {
                $logger->error('Erreur lors de la sauvegarde des pieces de DCE selectionnees.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    /**
     * Permet de sauvegarder les pièces enveloppes selectionnées.
     */
    public function sauvegarderPiecesReponseSelectionnees()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                //Les pièces enveloppes: cas des fichiers zip
                $idsBlobZip = $this->idBlob->Value;
                $idsBlobSignatureZip = $this->idBlobSignature->Value;
                $arrayIdsBlobZip = explode('#', $idsBlobZip);
                $selectedFilesForEnveloppe = '';
                if (is_array($arrayIdsBlobZip)) {
                    foreach ($arrayIdsBlobZip as $oneIdBlobZip) {
                        if ($arrayIdsBlobZip[0] != $oneIdBlobZip) {
                            $blobResource = Atexo_Blob::acquire_lock_on_blob($oneIdBlobZip, $this->_organisme);
                            $blob_file = $blobResource['blob']['pointer'];
                            if ($this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobZip)) {
                                $selectedFilesForEnveloppe .= '#'.$oneIdBlobZip.'_'.implode(';', $this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobZip));
                            }
                        }
                    }
                }
                //Cas des fichiers signatures
                $arrayIdsBlobSignatureZip = explode('#', $idsBlobSignatureZip);
                $selectedFilesForSignatureEnveloppe = '';
                if (is_array($arrayIdsBlobSignatureZip)) {
                    foreach ($arrayIdsBlobSignatureZip as $oneIdBlobSignatureZip) {
                        if ($arrayIdsBlobSignatureZip[0] != $oneIdBlobSignatureZip) {
                            $blobResource = Atexo_Blob::acquire_lock_on_blob($oneIdBlobSignatureZip, $this->_organisme);
                            $blob_file = $blobResource['blob']['pointer'];
                            if ($this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobSignatureZip)) {
                                $selectedFilesForSignatureEnveloppe .= '#'.$oneIdBlobSignatureZip.'_'.implode(';', $this->selectedFilesForZip($blob_file, 'env_item_'.$oneIdBlobSignatureZip));
                            }
                        }
                    }
                }
                if ($selectedFilesForEnveloppe) {
                    $this->_echange->setIdsEnvItems($selectedFilesForEnveloppe);
                } else {
                    $this->_echange->setIdsEnvItems('');
                }
                $this->setViewState('selectedFilesForEnveloppeZip', $selectedFilesForEnveloppe);
                //Cas des signatures des fichiers
                if ($selectedFilesForSignatureEnveloppe) {
                    $this->_echange->setIdsEnvSignItems($selectedFilesForSignatureEnveloppe);
                } else {
                    $this->_echange->setIdsEnvSignItems('');
                }
                $this->setViewState('selectedFilesForSignatureEnveloppeZip', $selectedFilesForSignatureEnveloppe);
                //Les pièces enveloppes: cas des fichiers non zip
                $arrayIdsBlobZip = explode('#', $this->idsBlobNonZip->Value);
                $idsBlobNonZip = [];
                if ($arrayIdsBlobZip && count($arrayIdsBlobZip)) {
                    foreach ($arrayIdsBlobZip as $idBlobNonZip) {
                        if ($arrayIdsBlobZip[0] != $idBlobNonZip) {
                            if ($_POST['env_nonZip_'.$idBlobNonZip]) {
                                $idsBlobNonZip[] = $idBlobNonZip;
                            }
                        }
                    }
                }
                if (is_array($idsBlobNonZip) && count($idsBlobNonZip) > 0) {
                    $this->_echange->setIdsBlobEnv(implode(';', $idsBlobNonZip));
                    $this->setViewState('idsBlobEnveloppesNonZip', implode(';', $idsBlobNonZip));
                } else {
                    $this->_echange->setIdsBlobEnv('');
                    $this->setViewState('idsBlobEnveloppesNonZip', '');
                }
                //Les pièces enveloppes: cas des fichiers signatures non zip
                $arrayIdsBlobSignZip = explode('#', $this->idsBlobSignatureNonZip->Value);
                $idsBlobSignNonZip = [];
                if ($arrayIdsBlobSignZip && count($arrayIdsBlobSignZip)) {
                    foreach ($arrayIdsBlobSignZip as $idBlobSignNonZip) {
                        if ($arrayIdsBlobSignZip[0] != $idBlobSignNonZip) {
                            if ($_POST['env_nonZip_'.$idBlobSignNonZip]) {
                                $idsBlobSignNonZip[] = $idBlobSignNonZip;
                            }
                        }
                    }
                }//print_r($idsBlobSignNonZip);exit;
                if (is_array($idsBlobSignNonZip) && count($idsBlobSignNonZip) > 0) {
                    $this->_echange->setIdsBlobSignEnv(implode(';', $idsBlobSignNonZip));
                    $this->setViewState('idsBlobSignEnveloppeNonZip', implode(';', $idsBlobSignNonZip));
                } else {
                    $this->_echange->setIdsBlobSignEnv('');
                    $this->setViewState('idsBlobSignEnveloppeNonZip', '');
                }
                if ($this->acteEngagement->Checked) {
                    $this->_echange->setIdsEnvAe($this->idACE->Value);
                    $this->setViewState('idsEnvAe', $this->idACE->Value);
                } else {
                    $this->_echange->setIdsEnvAe('');
                }

                if ($this->signActeEngagement->Checked) {
                    $this->_echange->setSignAce('1');
                    $this->setViewState('signEnvAe', '1');
                } else {
                    $this->_echange->setSignAce('0');
                }
            } catch (\Exception $e) {
                $logger->error('Erreur lors de la sauvegarde des pieces de reponses selectionnees.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    /**
     * Permet de sauvegarder les pièces de notification selectionnées
     * Puis mettre les index des pièces sélectionnées dans le viewState.
     */
    public function sauvegarderPiecesNotificationSelectionnees()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $idBLob = $this->getArborescencePiecesNotif();
                $arrayIndexSelectedFilesNotif = [];
                if ($idBLob) {
                    $atexoBlob = new Atexo_Blob();
                    $fileContent = $atexoBlob->return_blob_to_client($idBLob, $this->_organisme);
                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().date('Ymdhis').time().$idBLob;
                    if (!is_dir($tmpPath)) {
                        mkdir($tmpPath);
                    }
                    $fp = fopen($tmpPath.'/pieces_notification.zip', 'w');
                    fwrite($fp, $fileContent);
                    fclose($fp);
                    $arrayIndexSelectedFilesNotif = $this->selectedFilesForZip($tmpPath.'/pieces_notification.zip', 'notif_item');
                }
                if (is_array($arrayIndexSelectedFilesNotif) && count($arrayIndexSelectedFilesNotif) > 0) {
                    $this->_echange->setPiecesNotifItems(implode(';', $arrayIndexSelectedFilesNotif));
                    $this->setViewState('indexSelectedFilesNotif', implode(';', $arrayIndexSelectedFilesNotif));
                } else {
                    $this->_echange->setPiecesNotifItems('');
                    $this->setViewState('indexSelectedFilesNotif', '');
                }
            } catch (\Exception $e) {
                $logger->error('Erreur lors de la sauvegarde des pieces de notifications.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    /**
     * Permet de sauvegarder les pièces attachées à la consultation
     * Enregistre les index des pièces selectionnées dans le viewState.
     */
    public function sauvegarderAttacheesConsultationSelectionnees()
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $itemDoc = '';
                foreach ($this->repeaterType->getItems() as $oneItemType) {
                    foreach ($oneItemType->repeaterTypeDoc->getItems() as $oneItem) {
                        $idFile = $oneItem->idFile->Value;
                        if ('ZIP' == strtoupper(Atexo_Util::getExtension($oneItem->nameFile->Value))) {
                            $blobResource = Atexo_Blob::acquire_lock_on_blob($oneItem->idBlob->Value, $this->_organisme);
                            $selectedIndex = $this->selectedFilesForZip($blobResource['blob']['pointer'], 'zip_item');
                            if ($selectedIndex && is_array($selectedIndex)) {
                                $envListIndex = '';
                                foreach ($selectedIndex as $oneIndex) {
                                    $envListIndex .= $idFile.'_'.$oneIndex.';';
                                }
                                $itemDoc .= $envListIndex;
                            }
                        } else {
                            if ($oneItem->pieceChecked->Checked) {
                                $itemDoc .= $idFile.'_;';
                            }
                        }
                    }
                }
                $this->_echange->setIdsPiecesExternes($itemDoc);
                $this->setViewState('idsPiecesExternes', $itemDoc);
            } catch (\Exception $e) {
                $logger->error('Erreur lors de la sauvegarde des pieces attachees a la consultation.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        }
    }

    /*
     * Permet de verfier la taille et l'extension d'un fichier
     */
    public function verifyExtenstionAndSize($nameFile, $sizeFile)
    {
        if ($this->verifyExtenstion($nameFile) && (Atexo_Util::GetSizeFromName($sizeFile) < Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE'))) {
            return true;
        }

        return false;
    }

    /*
     * Permet de valider les données de la catégorie d'achat
     */
    public function validerCategorieAchat(&$erreur, &$messages)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if (!$this->oa->SelectedValue) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_ORGANISATION_ACHAT_OA');
            }
            if (!$this->isContratAcSad()) {
                if (!$this->ga->SelectedValue) {
                    $erreur = true;
                    $messages[] = Prado::localize('GROUPEMENT_ACHETEURS');
                }
                if (!$this->marcheType->SelectedValue) {
                    $erreur = true;
                    $messages[] = Prado::localize('TEXT_TYPE_MARCHE');
                }
                if (!$this->TypeGroupement->SelectedValue) {
                    $erreur = true;
                    $messages[] = Prado::localize('DEFINE_TYPE_GROUPEMENT');
                }
            } else {
                if (!$this->typeContrat->SelectedValue) {
                    $erreur = true;
                    $messages[] = Prado::localize('DEFINE_TYPE_CONTRAT');
                }
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la validation des categories achats (referentiel).'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /*
     * Permet de valider les sirets
     */
    public function validerSirets(&$erreur, &$messages)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $siren = $this->siren->Text;
            $siret = $this->siret->Text;
            if (!Atexo_Util::isSiretValide($siren.$siret)) {
                $erreur = true;
                $messages[] = Prado::localize('MSG_ERROR_SIRET_INVALIDE');
            }
            if ($this->codePays->SelectedValue == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
                $siren = $this->sirenTitulaire->Text;
                $siret = $this->siretTitulaire->Text;
                if (!Atexo_Util::isSiretValide($siren.$siret)) {
                    $erreur = true;
                    $messages[] = Prado::localize('DEFINE_NUMERO_SIRET_TITULAIRE');
                }
            }
            if (!$this->isContratAcSad()) {
                $nombreCoTitulaires = (int) $this->nbEntrepriseCotraitantes->SelectedValue;
                if (($this->TypeGroupement->SelectedValue != Atexo_Config::getParameter('ID_TYPE_GROUPEMEN_SANS_CO_TITULAIRE_CHORUS'))) {
                    if ($nombreCoTitulaires) {
                        $i = 0;
                        foreach ($this->repeatCodePays->Items as $item) {
                            if ($i >= $nombreCoTitulaires) {
                                break;
                            }
                            if ($item->codePaysCoTitulaire->SelectedValue == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
                                $sirenCoTitulaire = $item->sirenCoTitulaire->Text;
                                $siretCoTitulaire = $item->siretCoTitulaire->Text;
                                if ((!$sirenCoTitulaire || !$siretCoTitulaire) || !Atexo_Util::isSiretValide($sirenCoTitulaire.$siretCoTitulaire)) {
                                    $erreur = true;
                                    $messages[] = Prado::localize('DEFINE_SIRET_CO_TITULAIRE').' '.($i + 1);
                                }
                            }
                            ++$i;
                        }
                    } else {
                        $erreur = true;
                        $messages[] = Prado::localize('ALERTE_ERREUR_MESSAGE_SELECTION_TYPE_GROUPEMENT_CHORUS2');
                    }
                }
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la validation du siren et siret.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function validerSiretDonneesContrat(&$erreur, &$messages)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $siren = $this->sirenDonneesCandidat->Text;
            $siret = $this->siretDonneesCandidat->Text;
            if (!Atexo_Util::isSiretValide($siren.$siret)) {
                $erreur = true;
                $messages[] = Prado::localize('MSG_ERROR_SIRET_INVALIDE');
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la validation du siren et siret du contrat.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /*
     * Permet de valider les données de fiche de recenssement
     */
    public function validerDonneesFicheRecenssement(&$erreur, &$messages)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if (!$this->natureActe->SelectedValue) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_NATURE_ACTE_JURIDIQUE');
            }

            $codePrincipal = $this->referentielCPV->getCpvPrincipale();
            $codesSec = $this->referentielCPV->getCpvSecondaires();

            if (!$codePrincipal && !$codesSec) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_CODES_CPV');
            }
            if (!$this->procedureType->SelectedValue) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_PROCEDURE');
            }
            if (!$this->formeprix->SelectedValue) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_FORME_DE_PRIX');
            }
            if (!$this->montantHtAccord->Text) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_MONTANT_HT');
            }
            if ('' === $this->nbPropositionsRecues->Text) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_NOMBRE_PROPOSITION_RECUES');
            }
            if ('' === $this->nbPropositionsDemat->Text) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_NOMBRE_PROPOSITION_DEMATERIALISEES');
            }
            if (!$this->ccgaReference->SelectedValue) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_CCAG_REFERENCE');
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la validation de la fiche de recensement.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function validerDonneesContrat(&$erreur, &$messages)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $codePrincipal = $this->referentielCPVDonneesCandidat->getCpvPrincipale();
            $codesSec = $this->referentielCPVDonneesCandidat->getCpvSecondaires();

            if (!$codePrincipal && !$codesSec) {
                $erreur = true;
                $messages[] = Prado::localize('TEXT_CODES_CPV');
            }
            if (!$this->montantHtAccordDonneesCandidat->Text) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_MONTANT_MAX_HT');
            }

            if (!$this->idAccordCadreDonneesCandidat->Text) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_IDENTIFIANT_ACCORD_CADRE_TITULAIRE');
            }
            if (!$this->idAccordCadreChapeauDonneesCandidat->Text) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_IDENTIFIANT_ACCORD_CADRE_CHAPEAU');
            }

            //Intitule
            $intituleEchange = $this->intituleDonneesCandidat->Text;
            if (empty($intituleEchange)) {
                $erreur = true;
                $messages[] = Prado::localize('DEFINE_INTITULE');
            }

            //Objet
            $objetEchange = $this->objetMarcheDonneesCandidat->Text;
            if (empty($objetEchange)) {
                $erreur = true;
                $messages[] = Prado::localize('OBJET');
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors de la validation des donnees du contrat.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de verifier qu'un contrat est de type accord cadre/sad.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isContratAcSad()
    {
        $contrat = $this->getContrat();

        return $this->_echange instanceof CommonChorusEchange && $this->_echange->isTypeFluxFen211($contrat);
    }

    public function displayTypeContrat()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            //TODO : Vu avec KBE le 22/09/2015, nous allons gerer ce referentiel en dur le temps de trouver la solution
            $data = ['0' => Prado::localize('TEXT_SELECTIONNER').'...',
                Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE') => Prado::localize('DEFINE_ACCORD_CADRES'),
                Atexo_Config::getParameter('TYPE_CONTRAT_SAD') => Prado::localize('DEFINE_SYSTEME_ACQUISITION_DYNAMIQUE'), ];
            $this->typeContrat->dataSource = $data;
            $this->typeContrat->DataBind();
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage du type de contrat.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function displayIdentifiantsUniques()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->isContratAcSad()) {
                if ($this->_echange->getSiren() && $this->_echange->getSiret()) {
                    $this->sirenDonneesCandidat->Text = $this->_echange->getSiren();
                    $this->siretDonneesCandidat->Text = $this->_echange->getSiret();
                } else {
                    $sirenDonneesCandidat = '';
                    $siretDonneesCandidat = '';
                    $serviceAgentConnecte = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                    if ($serviceAgentConnecte instanceof CommonService) {
                        $sirenDonneesCandidat = $serviceAgentConnecte->getSiren();
                        $siretDonneesCandidat = $serviceAgentConnecte->getComplement();
                    } else {
                        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                        if ($organisme instanceof CommonOrganisme) {
                            $sirenDonneesCandidat = $organisme->getSiren();
                            $siretDonneesCandidat = $organisme->getComplement();
                        }
                    }
                    $this->sirenDonneesCandidat->Text = $sirenDonneesCandidat;
                    $this->siretDonneesCandidat->Text = $siretDonneesCandidat;
                }
            } else {
                if ($this->_echange->getSiren() && $this->_echange->getSiret()) {
                    $this->siren->Text = $this->_echange->getSiren();
                    $this->siret->Text = $this->_echange->getSiret();
                } else {
                    $serviceAgentConnecte = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                    if ($serviceAgentConnecte instanceof CommonService) {
                        $this->siren->Text = $serviceAgentConnecte->getSiren();
                        $this->siret->Text = $serviceAgentConnecte->getComplement();
                    }
                }
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de l'affichage des identifiants uniques.".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de determiner le nombre d'onglet des details de l'echange vers Chorus.
     *
     * @return int : nombre d'onglets
     */
    public function getNombreOnglets()
    {
        return 5;
    }

    /**
     * Permet d'afficher les donnees liees au contrat.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function displayDonneesContrat()
    {
        try {
            if ($this->_contrat instanceof CommonTContratTitulaire || $this->usingExecContrat()) {
                if ($this->_echange instanceof CommonChorusEchange) {
                    //Intitulé
                    if ($this->_echange->getIntituleContrat()) {
                        $this->intituleDonneesCandidat->Text = $this->_echange->getIntituleContrat();
                    } else {
                        $this->intituleDonneesCandidat->Text = mb_strimwidth($this->getIntituleAcSad(), 0, 50);
                    }
                    //Objet
                    if ($this->_echange->getObjetContrat()) {
                        $this->objetMarcheDonneesCandidat->Text = $this->_echange->getObjetContrat();
                    } else {
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                            $objet = $this->_contratExec['objet'] ?? '';
                        } else {
                            $objet = $this->_contrat->getObjetContrat();
                        }
                        $this->objetMarcheDonneesCandidat->Text = mb_strimwidth($objet, 0, 50);
                    }

                    //Identifiant accord-cadre titulaire
                    if ($this->_echange->getIdentifiantAccordCadre()) {
                        $this->idAccordCadreDonneesCandidat->Text = $this->_echange->getIdentifiantAccordCadre();
                    } else {
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                            $this->idAccordCadreDonneesCandidat->Text = $this->_contratExec['numeroLong'] ?? '';
                        } else {
                            $this->idAccordCadreDonneesCandidat->Text = $this->_contrat->getNumLongOeap();
                        }
                    }
                    //Identifiant accord-cadre chapeau
                    if ($this->_echange->getIdentifiantAccordCadreChapeau()) {
                        $this->idAccordCadreChapeauDonneesCandidat->Text = $this->_echange->getIdentifiantAccordCadreChapeau();
                    } else {
                        $numeroChapeau = $this->recupererChapeauContratMulti();
                        if (!$numeroChapeau) {
                            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                $contratExecChapeau = $execWS->getContent('getContratByUuid', $this->_contratExec['idChapeau']);
                                if ($contratExecChapeau['numero']) {
                                    $numeroChapeau = $contratExecChapeau['numero'];
                                } else {
                                    $numeroChapeau = $this->_contratExec['numeroLong'] ?? '';
                                }
                            } else {
                                $numeroChapeau = $this->_contrat->getNumLongOeap();
                            }
                        }
                        $this->idAccordCadreChapeauDonneesCandidat->Text = $numeroChapeau;
                    }
                    //Montant Max HT
                    if ($this->_echange->getMontantHt()) {
                        $this->montantHtAccordDonneesCandidat->Text = $this->_echange->getMontantHt();
                    } else {
                        $montantMaxHt = $this->recupererMontantContratMulti();
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                            $montantMaxHt =  $this->_contratExec['montantFacture'] ?? '';
                        } else {
                            $montantMaxHt = $this->_contrat->getMontantMaxEstime();
                        }

                        if (!$montantMaxHt) {
                            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                                $montantMaxHt =  $this->_contratExec['montant'] ?? '';
                            } else {
                                $montantMaxHt = $this->_contrat->getMontantContrat();
                            }

                        }
                        $this->montantHtAccordDonneesCandidat->Text = $montantMaxHt;
                    }
                    //Codes cpv
                    $this->referentielCPVDonneesCandidat->onInitComposant();

                    if (
                        Atexo_Config::getParameter('ACTIVE_EXEC_V2')
                        && $this->_contratExec
                        && !$this->_echange->getCpv1()
                    ) {
                        $codeCpv1 = $this->_contratExec['cpv']['codePrincipal'] ?? '';
                        $codeCpv2 = $this->_contratExec['cpv']['codeSecondaire1'] ?? '';
                        $this->referentielCPVDonneesCandidat->chargerReferentielCpv($codeCpv1, $codeCpv2);
                    } elseif ($this->_contrat && '' != $this->_contrat->getCodeCpv1() && !$this->_echange->getCpv1()) {
                        $this->referentielCPVDonneesCandidat->chargerReferentielCpv($this->_contrat->getCodeCpv1(), $this->_contrat->getCodeCpv2());
                    } else {
                        $this->referentielCPVDonneesCandidat->chargerReferentielCpv($this->_echange->getCpv1(), $this->_echange->getCpv2());
                    }


                    if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                        $datePrevueNotification = $this->_contratExec['datePrevisionnelleNotification'];
                        $datePrevueFinContrat = $this->_contratExec['datePrevisionnelleFinMarche'];
                        $dateNotification = $this->_contratExec['dateNotification'];
                        $dateFinContrat = $this->_contratExec['dateFinContrat'];
                    } else {
                        $datePrevueNotification = $this->_contrat->getDatePrevueNotification();
                        $datePrevueFinContrat = $this->_contrat->getDatePrevueFinContrat();
                        $dateNotification = $this->_contrat->getDateNotification();
                        $dateFinContrat = $this->_contrat->getDateFinContrat();
                    }

                    //Dates previsionnelles et reelles de notification et fin de marche
                    if ($this->_echange->getDateNotification()) {
                        $this->dateNotificationDonneesCandidat->Text = Atexo_Util::iso2frnDate($this->_echange->getDateNotification());
                    } else {
                        $this->dateNotificationDonneesCandidat->Text = Atexo_Util::iso2frnDate($datePrevueNotification);
                    }
                    if ($this->_echange->getDateFinMarche()) {
                        $this->finMarcheDonneesCandidat->Text = Atexo_Util::iso2frnDate($this->_echange->getDateFinMarche());
                    } else {
                        $this->finMarcheDonneesCandidat->Text = Atexo_Util::iso2frnDate($datePrevueFinContrat);
                    }
                    if ($this->_echange->getDateNotificationReelle()) {
                        $this->dateNotificationReelleDonneesCandidat->Text = Atexo_Util::iso2frnDate($this->_echange->getDateNotificationReelle());
                    } else {
                        $this->dateNotificationReelleDonneesCandidat->Text = Atexo_Util::iso2frnDate($dateNotification);
                    }
                    if ($this->_echange->getDateFinMarcheReelle()) {
                        $this->dateFinMarcheReelleDonneesCandidat->Text = Atexo_Util::iso2frnDate($this->_echange->getDateFinMarcheReelle());
                    } else {
                        $this->dateFinMarcheReelleDonneesCandidat->Text = Atexo_Util::iso2frnDate($dateFinContrat);
                    }
                    //Desactiver les champs
                    $this->idAccordCadreDonneesCandidat->enabled = false;
                    $this->idAccordCadreChapeauDonneesCandidat->enabled = false;
                } else {
                    $logger = Atexo_LoggerManager::getLogger('app');
                    $logger->error("L'echange Chorus n'est pas une instance de CommonChorusEchange");
                }
            } else {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error("Le contrat n'est pas une instance de CommonTContratTitulaire");
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'affichage des donnees du contrat : ".$e->getMessage());
        }
    }

    /**
     * Permet de recuperer le chapeau du contrat multi.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function recupererChapeauContratMulti()
    {
        if ($this->_contrat && ($this->_contrat->getCommonTContratMulti() instanceof CommonTContratMulti)) {
            return $this->_contrat->getCommonTContratMulti()->getNumeroContrat();
        }

        return '';
    }

    public function recupererMontantContratMulti()
    {
        if ($this->_contrat && ($this->_contrat->getCommonTContratMulti() instanceof CommonTContratMulti)) {
            return $this->_contrat->getCommonTContratMulti()->getMontantMaxEstime();
        }

        return '';
    }

    /**
     * Recupere la valeur selectionnee du type de contrat pour affichage.
     */
    public function displaySelectedValueTypeContrat()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_echange->getTypeContrat() && '0' != (string) $this->_echange->getTypeContrat()) {
                $this->typeContrat->SelectedValue = $this->_echange->getTypeContrat();
            } else {
                if (!empty($this->_contratExec) && is_array($this->_contratExec) && Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                    $typeContrat = (new Atexo_TypeContrat())->retrieveTypeContratByLibelle('TYPE_CONTRAT_' . $this->_contratExec['type']['codeExterne']);
                    $selectedValue = '';
                    if ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($typeContrat->getIdTypeContrat())) {
                        $selectedValue = Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE');
                    } elseif ((new Atexo_Consultation_Contrat())->isTypeContratSad($typeContrat->getIdTypeContrat())) {
                        $selectedValue = Atexo_Config::getParameter('TYPE_CONTRAT_SAD');
                    }
                    $this->typeContrat->SelectedValue = $selectedValue;
                } elseif ($this->_contrat instanceof CommonTContratTitulaire) {
                    $selectedValue = '';
                    if ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($this->_contrat->getIdTypeContrat())) {
                        $selectedValue = Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE');
                    } elseif ((new Atexo_Consultation_Contrat())->isTypeContratSad($this->_contrat->getIdTypeContrat())) {
                        $selectedValue = Atexo_Config::getParameter('TYPE_CONTRAT_SAD');
                    }
                    $this->typeContrat->SelectedValue = $selectedValue;
                }
            }
            $this->typeContrat->Enabled = false;
        } catch (Exception $e) {
            $logger->error('Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer l'intitulé de l'accord cadre/Sad.
     *
     * @return string : intitulé
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getIntituleAcSad()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $intitule = '';
            if ($this->_contrat instanceof CommonTContratTitulaire) {
                $intitule = $this->_contrat->getIntitule();
            } elseif (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->_contratExec) {
                $intitule = $this->_contratExec['intitule'];
            } else {
                $logger->error('Contrat non trouve');
            }

            return $intitule;
        } catch (\Exception $e) {
            $logger->error("Erreur lors de la recuperation de l'intitulé de l'accord cadre / sad : ".$e->getMessage());
        }
    }

    /**
     * Permet d'afficher le message d'erreur.
     *
     * @param string $message message
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    public function afficherErreur($message)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($message);
    }

    /**
     * Permet de fermer la popin de confirmation.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    public function fermerPopInConfirmation()
    {
        $this->javascript->Text .= "<script>J('.demander-validation').dialog('close');return false;</script>";
    }

    /**
     * permet d'ajouter une ficheModificative à l'echange.
     *
     * @param CommonChorusEchange $echangeChorus
     * @param PropelPDO           $connexion
     *
     * @return CommonTChorusFicheModificative $ficheModificative
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function addFicheModificative($echangeChorus, $connexion)
    {
        $ficheModificative = $this->FicheModificative->addFicheModificative($echangeChorus, $connexion);

        return $ficheModificative;
    }

    /**
     * permet de mettre à jour l'objet CommonTChorusFicheModificative.
     *
     * @param PropelPDO $connexion
     *
     * @return CommonTChorusFicheModificative $ficheModificative
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function updateFicheModificative($connexion)
    {
        return $this->FicheModificative->updateFicheModificative($connexion);
    }

    /**
     * permet d'afficher les infos de la ficheModificative.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function displayFicheModificative()
    {
        $this->FicheModificative->displayFicheModificative();
    }

    /**
     * permet de generer le document de la fiche Modificative.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     * @param CommonTContratTitulaire        $contrat
     * @param PropelPDO                      $connexion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function generationFicheModificative($ficheModificative, $connexion)
    {
        $contrat = $this->_contrat;

        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $contrat = $this->_contratExec;
        }

        $this->FicheModificative->generatePdfFicheModificative($ficheModificative, $contrat, $connexion);
    }

    /**
     * permet de retourner le mode d affichage du referentiel CPV.
     *
     * @return string $mode
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getModeReferentielCpv()
    {
        $mode = 'write';
        if (!self::isContratAcSad()) {
            if ($this->isTypeEnvoiFicheModificative() || intval($this->_echange->getNumOrdre()) > 1) {
                $mode = 'lecture';
            }
        }

        return $mode;
    }

    /**
     * Controle de securite pour verifier les doublons d'accord cadre (FEN211) de flux envoyes a CHORUS.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    public function verifierDoublonsAcSAD()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->isContratAcSad()) {
                (new Atexo_Chorus_Util())->loggerInfos('Debut verification de doublons de flux');
                try {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $echange = CommonChorusEchangeQuery::create()->filterById(Atexo_Util::atexoHtmlEntities($_GET['idEchange']))->filterByOrganisme($this->_organisme)->findOne($connexion);
                    if ($echange instanceof CommonChorusEchange && $echange->getStatutechange() == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER') && '1' == (string) $echange->getNumOrdre()) {
                        (new Atexo_Chorus_Util())->loggerInfos('Ce flux est un doublon');

                        return true;
                    }
                    (new Atexo_Chorus_Util())->loggerInfos("Ce flux n'est pas un doublon");
                } catch (\Exception $e) {
                    (new Atexo_Chorus_Util())->loggerErreur('Erreur verification de doublons de flux : {type_flux = '.(($this->isContratAcSad()) ? 'FEN211' : 'FEN111').'} , {id_echange = '.$_GET['idEchange'].'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());

                    return false;
                }
                (new Atexo_Chorus_Util())->loggerInfos('Fin verification de doublons de flux');
            }

            return false;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la verification des doublons AC/SAD'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet d'afficher un compteur du nombre de caracteres restants des champs intitule et objet.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    public function compterCaracteresRestantsIntituleObjet()
    {
        $this->javascript->Text .= '<script>
                                        document.getElementById("compteurIntituleDonneesContrat").innerHTML = nbrCaracteresSaisisRestants("'.$this->intituleDonneesCandidat->Text.'", 50);
                                        document.getElementById("compteurObjetDonneesContrat").innerHTML    = nbrCaracteresSaisisRestants("'.$this->objetMarcheDonneesCandidat->Text.'", 50);
                                    </script>';
    }

    /**
     * Permet de calculer le nombre de pieces jointes de l'onglet fiche modificative.
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getNbrPjsFicheModificative()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_echange instanceof CommonChorusEchange) {
                $arrayFicheModif = $this->_echange->getCommonTChorusFicheModificatives();
                if ($arrayFicheModif instanceof PropelObjectCollection && !empty($arrayFicheModif)) {
                    $dataFicheModif = $arrayFicheModif->getData();
                    if (is_array($dataFicheModif) && !empty($dataFicheModif)) {
                        $ficheModif = $dataFicheModif[0];
                        if ($ficheModif instanceof CommonTChorusFicheModificative) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                            $pjsFicheModif = CommonTChorusFicheModificativePjQuery::create()->filterByIdFicheModificative($ficheModif->getIdFicheModificative())->find($connexion);
                            if ($pjsFicheModif instanceof PropelObjectCollection && !empty($pjsFicheModif)) {
                                $listePjFicheModif = $pjsFicheModif->getData();

                                return count($listePjFicheModif);
                            }
                        }
                    }
                }
            }

            return 0;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation du nombre de PJ de la fiche modificative.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer l'identifiant du type de procedure dans les donnees de la consultation.
     *
     * @return int|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getIdTypeProcedureDonneesConsultation()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_donneesConsultation instanceof CommonTDonneesConsultation) {
                return $this->_donneesConsultation->getIdTypeProcedure();
            }

            return null;
        } catch (Exception $e) {
            $logger->error("Erreur lors de la recuperation de l'identifiant du type de procedure des donnees de la consultation (table: t_donnees_consultation) ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());

            return null;
        }
    }

    /**
     * Permet de recuperer la reference de la consultation dans les donnees de la consultation.
     *
     * @return int|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getReferenceDonneesConsultation()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_donneesConsultation instanceof CommonTDonneesConsultation) {
                return $this->_donneesConsultation->getReferenceConsultation();
            }

            return null;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation de la reference de la consultation des donnees de la consultation (table: t_donnees_consultation) '.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());

            return null;
        }
    }

    /**
     * Permet de recuperer la signature offres dans les donnees de la consultation.
     *
     * @return null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getSignatureOffresDonneesConsultation()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_donneesConsultation instanceof CommonTDonneesConsultation) {
                return $this->_donneesConsultation->getSignatureOffre();
            }

            return null;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation de la signature offres des donnees de la consultation (table: t_donnees_consultation) '.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());

            return null;
        }
    }

    /**
     * Permet de recuperer l'identifiant du service dans les donnees de la consultation.
     *
     * @return int|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getServiceIdDonneesConsultation()
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if ($this->_donneesConsultation instanceof CommonTDonneesConsultation) {
                return $this->_donneesConsultation->getServiceId();
            }

            return null;
        } catch (Exception $e) {
            $logger->error("Erreur lors de la recuperation de l'identifiant du service des donnees de la consultation (table: t_donnees_consultation) ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());

            return null;
        }
    }

    /**
     * @param $typeDume |TYPE_DUME_ACHETEUR|TYPE_DUME_OE
     * @param null $dumeItems
     */
    public function displayPiecesDume($typeDume, $dumeItems = null)
    {
        if ($this->_contrat instanceof CommonTContratTitulaire && '0' == $this->_contrat->getHorsPassation()) {
            $logger = Atexo_LoggerManager::getLogger('app');
            try {
                $idsBlobDume = '';
                $scriptDume = '';
                if ($typeDume == Atexo_Config::getParameter('TYPE_DUME_ACHETEUR')) {
                    $this->scriptDumeAcheteur->Text = '';
                    if (!$dumeItems) {
                        $dumeItems = $this->_echange->getDumeAcheteurItems();
                    }
                } elseif ($typeDume == Atexo_Config::getParameter('TYPE_DUME_OE')) {
                    $this->scriptDumeOe->Text = '';
                    if (!$dumeItems) {
                        $dumeItems = $this->_echange->getDumeOeItems();
                    }
                }

                $consultation = (new Atexo_Consultation())->retrieveConsultation($this->getReferenceDonneesConsultation());
                $tDumeNumeros = Atexo_Dume_AtexoDume::getListTDumeNumeros($consultation, $typeDume);
                foreach ($tDumeNumeros as $tDumeNumero) {
                    if ($tDumeNumero instanceof CommonTDumeNumero) {
                        if ($tDumeNumero->getBlobId()) {
                            $idsBlobDume = $idsBlobDume.'#'.$tDumeNumero->getBlobId();
                            $blobResourcePdf = Atexo_Blob::acquire_lock_on_blob(
                                $tDumeNumero->getBlobId(),
                                $this->_organisme
                            );
                        }

                        if ($tDumeNumero->getBlobIdXml()) {
                            $idsBlobDume = $idsBlobDume.'#'.$tDumeNumero->getBlobIdXml();
                            $blobResourceXml = Atexo_Blob::acquire_lock_on_blob(
                                $tDumeNumero->getBlobIdXml(),
                                $this->_organisme
                            );
                        }

                        $disabled = '';
                        $disabledCss = '';

                        if ($tDumeNumero->getBlobId()) {
                            $checked = '';
                            $arDumeItems = explode(';', $dumeItems);
                            if (in_array($tDumeNumero->getBlobId(), $arDumeItems)) {
                                $checked = 'checked="checked"';
                            }
                            $scriptDume .= $this->generateHtml($blobResourcePdf, $disabled, $disabledCss, $checked);
                        }
                        if ($tDumeNumero->getBlobIdXml()) {
                            $checked = '';
                            $arDumeItems = explode(';', $dumeItems);
                            if (in_array($tDumeNumero->getBlobIdXml(), $arDumeItems)) {
                                $checked = 'checked="checked"';
                            }
                            $scriptDume .= $this->generateHtml($blobResourceXml, $disabled, $disabledCss, $checked);
                        }
                    }
                }

                if ($typeDume == Atexo_Config::getParameter('TYPE_DUME_ACHETEUR')) {
                    $this->idsBlobDumeAcheteur->value = $idsBlobDume;
                    $this->scriptDumeAcheteur->Text = $scriptDume.'</ul>';
                } elseif ($typeDume == Atexo_Config::getParameter('TYPE_DUME_OE')) {
                    $this->idsBlobDumeOe->value = $idsBlobDume;
                    $this->scriptDumeOe->Text = $scriptDume.'</ul>';
                }
            } catch (\Exception $e) {
                $message = "Erreur lors de l'affichage des pieces du DUME : $dumeItems ".PHP_EOL.
                    'Erreur: '.$e->getMessage().PHP_EOL.
                    'Trace: '.$e->getTraceAsString();
                $logger->error($message);
            }
        }
    }

    private function generateHtml($blobResource, $disabled, $disabledCss, $checked)
    {
        $scriptDume = '<li>
<span class="check-bloc"><input '.$disabled.' type="checkbox" name="dume_'.$blobResource['blob']['id'].
            '"  id="dume_'.$blobResource['blob']['id'].'" " '.$checked.'   />
</span><label class="content-bloc bloc-580" for="dume_'.
            $blobResource['blob']['id'].'"><span class="float-left'.$disabledCss.'">'.
            $blobResource['blob']['name'].
            '&nbsp;( '.Atexo_Util::getTailleBlob($blobResource['blob']['organisme'], $blobResource['blob']['id']).' )</span>';
        $scriptDume .= '</label></li>';

        return $scriptDume;
    }

    /**
     * @param $arrayIndexSelectedFilesDume | arrayIndexSelectedFilesDumeAcheteur | arrayIndexSelectedFilesDumeOE
     */
    public function sauvegarderPieceDumeSelectionnees($arrayIndexSelectedFilesDume, $idsBlobDumeValue)
    {
        try {
            $arrayIdsBlobDume = explode('#', $idsBlobDumeValue);
            $idsBlobDume = [];
            if ($arrayIdsBlobDume && count($arrayIdsBlobDume)) {
                foreach ($arrayIdsBlobDume as $idBlobDume) {
                    if ($arrayIdsBlobDume[0] != $idBlobDume) {
                        if ($_POST['dume_'.$idBlobDume]) {
                            $idsBlobDume[] = $idBlobDume;
                        }
                    }
                }
            }

            if (is_array($idsBlobDume) && count($idsBlobDume) > 0) {
                if ('arrayIndexSelectedFilesDumeAcheteur' == $arrayIndexSelectedFilesDume) {
                    $this->_echange->setDumeAcheteurItems(implode(';', $idsBlobDume));
                }

                if ('arrayIndexSelectedFilesDumeOE' == $arrayIndexSelectedFilesDume) {
                    $this->_echange->setDumeOeItems(implode(';', $idsBlobDume));
                }

                $this->setViewState($arrayIndexSelectedFilesDume, implode(';', $idsBlobDume));
            } else {
                if ('arrayIndexSelectedFilesDumeAcheteur' == $arrayIndexSelectedFilesDume) {
                    $this->_echange->setDumeAcheteurItems('');
                }

                if ('arrayIndexSelectedFilesDumeOE' == $arrayIndexSelectedFilesDume) {
                    $this->_echange->setDumeOeItems('');
                }

                $this->setViewState($arrayIndexSelectedFilesDume, '');
            }
        } catch (\Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $message = "Erreur lors de l'affichage ou sauvegarde des pieces du DUME : ".PHP_EOL.
                'Erreur: '.$exception->getMessage().PHP_EOL.
                'Trace: '.$exception->getTraceAsString();
            $logger->error($message);
        }
    }

    private function usingExecContrat()
    {
        return Atexo_Config::getParameter('ACTIVE_EXEC_V2')
            && is_array($this->_contratExec)
            && !empty($this->_contratExec);
    }

    public function getContrat()
    {
        $contrat = $this->_contrat;

        if ($this->usingExecContrat()) {
            $contrat = $this->_contratExec;
        }

        return $contrat;
    }
}
