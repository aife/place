<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;

/**
 * Contient le detail des infos support.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.0
 *
 * @since 2017-esr
 */
class popUpAffichageInfoSupport extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idSupport'])) {
            $idSupport = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idSupport']));
            if ($idSupport) {
                $supportPub = (new Atexo_Publicite_AnnonceSub())->getSupportsPublicationById($idSupport);
                if ($supportPub instanceof CommonTSupportPublication) {
                    $this->logoSupport->setText('<img src='.$this->themesPath().'/images/'.$supportPub->getImageLogo().' />');
                    if ($supportPub->getAffichageInfos()) {
                        if ($supportPub->getTypeInfo() == Atexo_Config::getParameter('TYPE_INFO_TEXTE')) {
                            $this->detailInfo->setText($supportPub->getDetailInfo());
                        } elseif ($supportPub->getTypeInfo() == Atexo_Config::getParameter('TYPE_INFO_IMAGE')) {
                            $this->detailInfo->setText('<img src='.$this->themesPath().'/images/'.$supportPub->getDetailInfo().' />');
                        }
                    }
                }
            }
        }
    }
}
