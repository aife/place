<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonResultatAnalyse;
use Application\Propel\Mpe\CommonResultatAnalyseDecision;
use Application\Propel\Mpe\CommonResultatAnalyseDecisionPeer;
use Application\Propel\Mpe\CommonResultatAnalysePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailAnalyse extends MpeTPage
{
    public $_consultation;
    public $_categories;
    public $_trancheBudgetaire;
    public ?\Application\Propel\Mpe\CommonResultatAnalyseDecision $_decisionAnalyse = null;
    public $_nombreLot;
    public $_lotOrConsultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $messageErreur = null;
        if (!Atexo_CurrentUser::hasHabilitation('ResultatAnalyse')) {
            $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        } else {
            Atexo_CurrentUser::deleteFromSession('idsOrg');
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->panelMessageErreur->setVisible(false);
            $messageErreur = '';
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                    $this->etape3->setId('etape234');
                } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')) {
                    $this->etape3->setId('etape34');
                } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                    $this->etape3->setId('etape4');
                }
                if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION') ||
                      ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI') &&
                      $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                       || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                       || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION'))
                       ||
                       (
                           $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                       || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                               && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                               && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                       )
                     ) {
                    if ($_GET['lot']) {
                        $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
                        if ($lot) { // consultation allotie
                            $this->intituleLot->Text = $lot->getDescription(Atexo_CurrentUser::readFromSession('lang'));
                            // desactiver le bouton enregistrer si le lot est en decision
                            if ($lot->getDecision() == Atexo_Config::getParameter('LOT_EN_DECISION')) {
                                $this->desactiverBoutonEnregistrer();
                            }
                        }
                    } else { // consultation non allotie
                        // desactiver le bouton enregistrer si la consultation est en decision
                        if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                            $this->desactiverBoutonEnregistrer();
                        }
                    }
                    $c = new Criteria();
                    $c->add(CommonResultatAnalyseDecisionPeer::CONSULTATION_ID, Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $c->add(CommonResultatAnalyseDecisionPeer::LOT, Atexo_Util::atexoHtmlEntities($_GET['lot']));
                    $c->add(CommonResultatAnalyseDecisionPeer::ORGANISME, $organisme);
                    $this->_decisionAnalyse = CommonResultatAnalyseDecisionPeer::doSelectOne($c, $connexionCom);
                    if (!$this->_decisionAnalyse instanceof CommonResultatAnalyseDecision) {
                        $this->_decisionAnalyse = new CommonResultatAnalyseDecision();
                    }

                    $this->consultationSummary->setConsultation($consultation);
                    $this->_consultation = $consultation;
                    if (!$this->IsPostBack) {
                        if ($this->_decisionAnalyse->getTypeDecision() == Atexo_Config::getParameter('RESULTAT_ANALYSE_ATTRIBUTION_PROPOSEE')) {
                            $this->attribue->Checked = true;
                        } elseif ($this->_decisionAnalyse->getTypeDecision() == Atexo_Config::getParameter('RESULTAT_ANALYSE_INFRUCTUEUX')) {
                            $this->infructueux->Checked = true;
                        } elseif ($this->_decisionAnalyse->getTypeDecision() == Atexo_Config::getParameter('RESULTAT_ANALYSE_AUTRE')) {
                            $this->autre->Checked = true;
                            $this->precisionAutre->Text = $this->_decisionAnalyse->getAutreTypeDecision();
                        } else {
                            $this->aDefinir->Checked = true;
                        }
                        $this->dateDecision->Text = Atexo_Util::iso2frnDate($this->_decisionAnalyse->getDateDecision());
                        $this->commentaire->Text = $this->_decisionAnalyse->getCommentaire();
                        $this->fillEntrepriseAttributaire();
                    }
                } else {
                    $messageErreur = Prado::localize('DECISION_IMPOSSIBLE');
                }
            } else {
                $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
            }
        }

        if ($messageErreur) {
            $this->panelMessageErreur->setVisible(true);
            $this->consultationSummary->setWithException(false);
            $this->panelMessageErreur->setMessage($messageErreur);
        }
    }

    public function fillEntrepriseAttributaire()
    {
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($this->_consultation->getEnvCandidature(), $this->_consultation->getEnvOffre(), $this->_consultation->getEnvAnonymat(), $this->_consultation->getEnvOffreTechnique());
        if ($typeEnveloppe != Atexo_Config::getParameter('CANDIDATURE_SEUL')) {
            $criteriaResponse = new Atexo_Response_CriteriaVo();
            $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $criteriaResponse->setSousPli(Atexo_Util::atexoHtmlEntities($_GET['lot']));
            $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
            $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaResponse->setReturnReponses(false);
            $criteriaResponse->setNomEntrepriseVisible(true);
            $criteriaResponse->getEnveloppeStatutOuverte();
            $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
            $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(Atexo_Util::atexoHtmlEntities($_GET['id']), $typeEnveloppe, Atexo_Util::atexoHtmlEntities($_GET['lot']), '', '', Atexo_CurrentUser::getCurrentOrganism(), false, false, true, true);
            $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
        } else {
            $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveReponseAdmissible(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
            $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveReponsePapierAdmissible(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_CurrentUser::getCurrentOrganism());
            $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
        }

        if (is_array($reponses) && count($reponses)) {
            try {
                (new Atexo_Consultation_Responses())->addResultatAnalyse($reponses);

                $sortedBy = 'Classement';
                $nestedObjects = 'ResultatAnalyse';

                $reponsesSorted = Atexo_Util::sortArrayOfObjects($reponses, $sortedBy, 'ASC', $nestedObjects);
                $reponses = $reponsesSorted;
            } catch (Exception $e) {
                Prado::log('Erreur DetailAnalyse.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'DetailAnalyse.php');
            }
        } else {
            $reponses = [];
        }

        $this->entreprisesAttributaire->DataSource = $reponses;
        $this->entreprisesAttributaire->DataBind();
    }

    public function saveProposition($sender, $param)
    {
        $connexionCOm = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $connexionCOm->beginTransaction();
        try {
            if ($this->attribue->Checked) {
                $this->_decisionAnalyse->setTypeDecision(Atexo_Config::getParameter('RESULTAT_ANALYSE_ATTRIBUTION_PROPOSEE'));
            } elseif ($this->infructueux->Checked) {
                $this->_decisionAnalyse->setTypeDecision(Atexo_Config::getParameter('RESULTAT_ANALYSE_INFRUCTUEUX'));
            } elseif ($this->autre->Checked) {
                $this->_decisionAnalyse->setTypeDecision(Atexo_Config::getParameter('RESULTAT_ANALYSE_AUTRE'));
                $this->_decisionAnalyse->setAutreTypeDecision($this->precisionAutre->Text);
            } elseif ($this->aDefinir->Checked) {
                $this->_decisionAnalyse->setTypeDecision(Atexo_Config::getParameter('RESULTAT_ANALYSE_A_DEFINIR'));
            }
            if ($this->dateDecision->Text) {
                $this->_decisionAnalyse->setDateDecision(Atexo_Util::frnDate2iso($this->dateDecision->Text));
            }
            if ($this->commentaire->Text) {
                $this->_decisionAnalyse->setCommentaire($this->commentaire->Text);
            }

            $this->_decisionAnalyse->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->_decisionAnalyse->setLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));
            $this->_decisionAnalyse->setOrganisme($organisme);

            $this->_decisionAnalyse->save($connexionCOm);
            foreach ($this->entreprisesAttributaire->getItems() as $item) {
                if ($item->idProposition->Value) {
                    $proposition = CommonResultatAnalysePeer::retrieveByPK($item->idProposition->Value, $organisme, $connexionCOm);

                    if (!($proposition instanceof CommonResultatAnalyse)) {
                        throw new Exception('Error');
                    }
                } else {
                    $proposition = new CommonResultatAnalyse();
                }
                $proposition->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $proposition->setIdOffre($item->idOffre->Value);
                $proposition->setLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));
                $proposition->setTypeEnveloppe($item->typeEnveloppe->Value);
                $proposition->setNote(str_replace(',', '.', $item->note->Text));
                $proposition->setClassement($item->classement->Text ?: null);
                $proposition->setMontantOffre($item->montant->Text);
                $proposition->setObservation($item->observation->Text);
                $proposition->setOrganisme($organisme);

                $proposition->save($connexionCOm);
            }

            $connexionCOm->commit();
        } catch (Exception $e) {
            $connexionCOm->rollBack();
            throw $e;
        }

        $this->response->redirect('index.php?page=Agent.resultatAnalyse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&ok');
    }

    public function onCallBackRefresh($sender, $param)
    {
        $this->fillEntrepriseAttributaire();
        $this->panelEntrepriseAttributaire->render($param->NewWriter);
    }

    public function desactiverBoutonEnregistrer()
    {
        $this->boutonEnregistrer->setEnabled(false);
    }
}
