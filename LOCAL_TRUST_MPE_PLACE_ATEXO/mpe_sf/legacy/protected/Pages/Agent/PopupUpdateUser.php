<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * Page de modification du compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupUpdateUser extends MpeTPage
{
    private string $_company = '';
    private string $_inscrit = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        $this->panelInscrit->InfoProfil->setVisible(true);
        if (isset($_GET['rc']) && '' != $_GET['rc']) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(Atexo_Util::atexoHtmlEntities($_GET['rc']));
        } elseif ((isset($_GET['idCompany']) && '' != $_GET['idCompany']) && (isset($_GET['pays']) && '' != $_GET['pays'])) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyByIdNational(Atexo_Util::atexoHtmlEntities($_GET['idCompany']), Atexo_Util::atexoHtmlEntities($_GET['pays']));
        }
        if (isset($_GET['id']) && '' != $_GET['id']) {
            //$this->panelInscrit->panelInformationsPersonelles->setVisible(false);
            //$this->panelInscrit->panelElementIdentification->setVisible(false);
            //$this->panelInscrit->conformiteDossierPresente->setVisible(false);
            $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->_inscrit->getEntrepriseId());
            $this->panelInscrit->setInscrit($this->_inscrit);
            $this->panelInscrit->setPostBack($this->IsPostBack);
            $this->panelInscrit->emailCreationValidator->setEnabled(false);
            $this->panelInscrit->loginCreationValidator->setEnabled(false);
        } else {
            if (!$this->IsPostBack) {
                $this->panelInscrit->loginModificationValidator->setEnabled(false);
                $this->panelInscrit->emailModificationValidator->setEnabled(false);
                $this->panelInscrit->adminEntreprise->setChecked(true);
                $this->panelInscrit->nonConforme->setChecked(true);
            }
        }
    }

    /**
     * Modification de l'inscrit.
     */
    public function doUpdate()
    {
        if ('2' != $this->_inscrit->getProfil() && $this->panelInscrit->adminEntreprise->Checked && $this->companyHasAtes()) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ENTREPRISE_A_DEJA_ATES'));
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";

            return -1;
        } else {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = CommonInscritPeer::retrieveByPK($this->_inscrit->getId(), $connexion);
            $inscrit = $this->panelInscrit->setNewInscrit($inscrit);
            if ($this->panelInscrit->adminEntreprise->Checked) {
                $inscrit->setProfil('2'); //ATES
            } else {
                $inscrit->setProfil('1'); //UES
            }

            return $inscrit->save($connexion);
        }
    }

    /**
     * Ajouter inscrit en base.
     */
    public function addInscrit()
    {
        if ($this->panelInscrit->adminEntreprise->Checked && $this->companyHasAtes()) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ENTREPRISE_A_DEJA_ATES'));
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";

            return -1;
        } else {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = new CommonInscrit();
            $inscrit = $this->panelInscrit->setNewInscrit($inscrit);
            $inscrit->setPays($this->_company->getPaysadresse());
            $inscrit->setEntrepriseId($this->_company->getId());
            if ($this->panelInscrit->adminEntreprise->Checked) {
                $inscrit->setProfil('2'); //ATES
            } else {
                $inscrit->setProfil('1'); //UES
            }

            return $inscrit->save($connexion);
        }
    }

    /**
     * Action Boutton Enregister.
     */
    public function saveChange($sender, $param)
    {
        if ($this->IsValid) {
            if (isset($_GET['id']) && '' != $_GET['id']) {
                $result = self::doUpdate();
                if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit') && true == $this->panelInscrit->pwdAutomatique->checked) {
                    $pwd = $this->Page->getViewState('PwdInscrit');
                    $inscrit = $this->Page->getViewState('Inscrit');
                    (new AgentInscriptionUser())->EnvoiMailConfirmationInscrit($inscrit, $this->_company, $pwd, true);
                }
            } else {
                $result = self::addInscrit();
            }
            if ($result >= 0) {
                $this->labelClose->Text = '<script>closePopUpUpdateUser();</script>';
            }
        }
    }

    /**
     * Verifier si l'entreprise a déjà un administrateur.
     */
    public function companyHasAtes()
    {
        $ates = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins($this->_company->getId());
        if (is_countable($ates) ? count($ates) : 0) {
            return true;
        } else {
            return false;
        }
    }
}
