<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_CriteriaVo;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Prado\Prado;

/**
 * Classe SuiviEchanges.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauBordEchangesChorus extends MpeTPage
{
    protected string $_critereTri = '';
    protected string $_triAscDesc = '';
    protected array $criterTri = [];

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->idretour->NavigateUrl = '?page=Agent.TableauBordEchangesChorus&search=true';
        if (true == $_GET['search']) {
            self::afficherTableauRecherche();
            if (!$this->isPostBack) {
                $this->dateDebut->Text = Atexo_Util::iso2frnDate((Atexo_Util::getDateMonthsAgo(2)));
                $this->dateFin->Text = date('d/m/Y');
            }
        }
        if (!$this->isPostBack) {
            self::displayOrganismes();
            self::displayEntiteAchat();
            self::displayTypeFichiers();
            self::displayRetourChorus();
            self::fillTypeContrat();
            self::displayCategoriePrincipale();
        }
    }

    /**
     * affichage de toutes les ministères.
     */
    public function displayOrganismes()
    {
        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, Atexo_CurrentUser::readFromSession('lang'));
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->organisme->Enabled = false;
            //$this->choixInclusion->visible = false;
        }
        $this->organisme->SelectedValue = Atexo_CurrentUser::getCurrentOrganism();
        $this->organisme->dataSource = $ArrayOrganismes;
        $this->organisme->DataBind();
    }

    public function chargerListeService($sender, $param)
    {
        $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->organisme->SelectedValue, true);
        $this->choixEntiteAchat->dataSource = $listEntityPurchase;
        $this->choixEntiteAchat->DataBind();
        $this->panelChoixOrganisme->render($param->getNewWriter());
        $this->Page->panelChoixOrganisme->render($param->getNewWriter());
    }

    /**
     * affichage de tous les services.
     */
    public function displayEntiteAchat()
    {
        $listeChilds = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        $this->choixEntiteAchat->DataSource = $listeChilds;
        $this->choixEntiteAchat->DataBind();
    }

    /**
     * Permet de remplir la liste des types de fichiers.
     */
    public function displayTypeFichiers()
    {
        $arrayTypeFichiers = [];
        $arrayTypeFichiers['INDIF'] = Prado::localize('TEXT_INDIFFERENT');
        $arrayTypeFichiers['FEN'] = Prado::localize('DEFINE_FEN_PMI_CHORUS');
        $arrayTypeFichiers['CEN'] = Prado::localize('DEFINE_CEN_ACCUSE_RECEPTION_CHORUS_PMI');
        $arrayTypeFichiers['FSO'] = Prado::localize('DEFINE_FSO_FLUX_SORTANT_CHORUS_PMI');
        $this->typeFluxChorus->dataSource = $arrayTypeFichiers;
        $this->typeFluxChorus->dataBind();
    }

    /**
     * Permet de remplir la liste des retours chorus.
     */
    public function displayRetourChorus()
    {
        $retourChorus = [];
        $retourChorus['INDIF'] = Prado::localize('TEXT_INDIFFERENT');
        $retourChorus['COM'] = Prado::localize('DEFINE_INTEGRE');
        $retourChorus['REJ'] = Prado::localize('TEXT_REJETE');
        $retourChorus['IRR'] = Prado::localize('TEXT_IRRECEVABLE');
        $this->retourChorus->dataSource = $retourChorus;
        $this->retourChorus->dataBind();
    }

    /**
     * Permet de remplir la liste des Categories principales.
     */
    public function displayCategoriePrincipale()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories, Atexo_CurrentUser::readFromSession('lang'));
        $this->categorie->Datasource = $dataCategories;
        $this->categorie->dataBind();
    }

    /**
     * Permet de remplir l'objet critères de recherche et d'afficher le tableau de resultats.
     */
    public function onSearchClick()
    {
        $criteriaVo = new Atexo_Chorus_CriteriaVo();

        if ('' != $this->organisme->getSelectedValue()) {
            $criteriaVo->setOrganisme($this->organisme->getSelectedValue());
            if (true == $this->inclureDescendances->Checked) {
                //$listeChilds = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
                $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($this->choixEntiteAchat->getSelectedValue(), $criteriaVo->getOrganisme());
                $criteriaVo->setIdsService($listOfServicesAllowed);
            } elseif (true == $this->entiteSeule->Checked) {
                $criteriaVo->setIdService($this->choixEntiteAchat->getSelectedValue());
            }
        }
        if ('' != $this->typeFluxChorus->getSelectedValue()) {
            $criteriaVo->setTypeFichierEchange($this->typeFluxChorus->getSelectedValue());
        }
        if (trim($this->dateDebut->Text)) {
            $criteriaVo->setPeriodeEchangeStart(trim($this->dateDebut->Text));
        }
        if (trim($this->dateFin->Text)) {
            $criteriaVo->setPeriodeEchangeEnd(trim($this->dateFin->Text));
        }
        if ('' != $this->retourChorus->getSelectedValue()) {
            $criteriaVo->setRetourChorus($this->retourChorus->getSelectedValue());
        }
        if (trim($this->numMarche->Text)) {
            $criteriaVo->setNumeroMarche(trim($this->numMarche->Text));
        }

        if (trim($this->reference->Text)) {
            $criteriaVo->setReferenceLibre(trim($this->reference->Text));
        }

        if ('' != $this->categorie->getSelectedValue()) {
            $criteriaVo->setCategorie($this->categorie->getSelectedValue());
        }

        if ($this->typeContrat->getSelectedValue()) {
            $criteriaVo->setTypeContrat($this->typeContrat->getSelectedValue());
        }

        $this->setViewState('CriteriaVo', $criteriaVo);

        $this->remplirTableauListeEchanges($criteriaVo->getOrganisme());
        $this->remplirTableauListeEchangesFso($criteriaVo->getOrganisme());
    }

    public function remplirTableauListeEchanges($organisme, $limit = null, $offset = null)
    {
        self::afficherTableauResultat();
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $criteriaVo->setCommandeName($this->getViewState('commandeName'));
        $criteriaVo->setSensTri($this->getViewState('sensTri'));
        $criteriaVo->setOrganisme($organisme);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $nombreElementTotal = (new Atexo_Chorus_Util())->remplirTableauEchanges($criteriaVo, true);
        $listeEchanges = (new Atexo_Chorus_Util())->remplirTableauEchanges($criteriaVo, false);
        if (!$limit && !$offset) {
            $this->setViewState('listeEchanges', $listeEchanges);
        }
        if ($nombreElementTotal >= 1) {
            $this->panelMoreThanOneElementFound->setStyle('display:block');
            $this->panelNoElementFound->setStyle('display:none');
            $this->nombreElement->Text = $nombreElementTotal;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->nombrePageTop->Text = ceil($nombreElementTotal / $this->RepeaterListEchanges->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElementTotal / $this->RepeaterListEchanges->PageSize);
            $this->RepeaterListEchanges->setVirtualItemCount($nombreElementTotal);
            $this->RepeaterListEchanges->dataSource = $listeEchanges;
            $this->RepeaterListEchanges->dataBind();
        } else {
            $this->panelMoreThanOneElementFound->setStyle('display:none');
            $this->panelNoElementFound->setStyle('display:block');
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $iddest = $param->CommandParameter;
        Atexo_Message::deleteObjetDestinataire($iddest);
    }

    public function refreshRepeaterEchange($sender, $param)
    {
        $this->remplirDataSource();
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterListEchanges->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->populateData();
        $this->populateDataFso();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterListEchanges->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->populateData();
            $this->populateDataFso();
        } else {
            $this->numPageTop->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTop':
                $pageSize = $this->listePageSizeTop->getSelectedValue();
                $this->listePageSizeBottom->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottom':
                $pageSize = $this->listePageSizeBottom->getSelectedValue();
                $this->listePageSizeTop->setSelectedValue($pageSize);
                break;
        }
        // echo $pageSize;exit;
        $this->RepeaterListEchanges->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterListEchanges->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterListEchanges->PageSize);
        $this->RepeaterListEchanges->setCurrentPageIndex(0);
        $this->populateData();
        $this->populateDataFso();
    }

    public function populateData()
    {
        $offset = $this->RepeaterListEchanges->CurrentPageIndex * $this->RepeaterListEchanges->PageSize;
        $limit = $this->RepeaterListEchanges->PageSize;
        if ($offset + $limit > $this->RepeaterListEchanges->getVirtualItemCount()) {
            $limit = $this->RepeaterListEchanges->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->remplirTableauListeEchanges($criteriaVo->getOrganisme(), $limit, $offset);
    }

    public function onCallBackRefreshEchange($sender, $param)
    {
        $this->remplirDataSource();
        $this->panelMoreThanOneElementFound->render($param->getNewWriter());
        $this->panelNoElementFound->render($param->getNewWriter());
    }

    public function remplirDataSource()
    {
        $this->criterTri['offSet'] = '0';
        $this->criterTri['limit'] = $this->RepeaterListEchanges->PageSize;
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->RepeaterListEchanges->PageSize);
        $this->populateData();
        $this->numPageTop->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
        $this->numPageBottom->Text = $this->RepeaterListEchanges->CurrentPageIndex + 1;
    }

    public function critereTri($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTri', 'ASC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);
    }

    public function Trier($sender, $param)
    {
        $this->remplirDataSource();
        $this->PanelListEchanges->render($param->NewWriter);
    }

    public function afficherTableauResultat()
    {
        $this->panelSearch->visible = false;
        $this->panelResult->visible = true;
    }

    public function afficherTableauRecherche()
    {
        $this->panelSearch->visible = true;
        $this->panelResult->visible = false;
    }

    public function genererExcelEchanges()
    {
        (new Atexo_GenerationExcel())->genererCsvTableauBordEchangesChorus($this->getViewState('listeEchanges'));
    }

    public function remplirTableauListeEchangesFso($organisme, $limit = null, $offset = null)
    {
        self::afficherTableauResultat();
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $criteriaVo->setCommandeName($this->getViewState('commandeName'));
        $criteriaVo->setSensTri($this->getViewState('sensTri'));
        $criteriaVo->setOrganisme($organisme);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $nombreElementTotal = (new Atexo_Chorus_Util())->remplirTableauEchanges($criteriaVo, true, true);
        $listeEchanges = (new Atexo_Chorus_Util())->remplirTableauEchanges($criteriaVo, false, true);
        if (!$limit && !$offset) {
            $this->setViewState('listeEchangesFso', $listeEchanges);
        }
        if ($nombreElementTotal >= 1) {
            $this->panelMoreThanOneElementFoundFso->setStyle('display:block');
            $this->panelNoElementFoundFso->setStyle('display:none');
            $this->nombreElementFso->Text = $nombreElementTotal;
            $this->PagerBottomFso->setVisible(true);
            $this->PagerTopFso->setVisible(true);
            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->nombrePageTopFso->Text = ceil($nombreElementTotal / $this->RepeaterListEchangesFso->PageSize);
            $this->nombrePageBottomFso->Text = ceil($nombreElementTotal / $this->RepeaterListEchangesFso->PageSize);
            $this->RepeaterListEchangesFso->setVirtualItemCount($nombreElementTotal);
            $this->RepeaterListEchangesFso->dataSource = $listeEchanges;
            $this->RepeaterListEchangesFso->dataBind();
        } else {
            $this->panelMoreThanOneElementFoundFso->setStyle('display:none');
            $this->panelNoElementFoundFso->setStyle('display:block');
            $this->PagerBottomFso->setVisible(false);
            $this->PagerTopFso->setVisible(false);
        }
    }

    public function refreshRepeaterEchangeFso($sender, $param)
    {
        $this->remplirDataSourceFso();
    }

    public function pageChangedFso($sender, $param)
    {
        $this->RepeaterListEchangesFso->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTopFso->Text = $param->NewPageIndex + 1;
        $this->numPageBottomFso->Text = $param->NewPageIndex + 1;
        $this->populateDataFso();
        $this->populateData();
    }

    public function goToPageFso($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTopFso':
                $numPage = $this->numPageTopFso->Text;
                break;
            case 'DefaultButtonBottomFso':
                $numPage = $this->numPageBottomFso->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTopFso->Text) {
                $numPage = $this->nombrePageTopFso->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterListEchangesFso->CurrentPageIndex = $numPage - 1;
            $this->numPageBottomFso->Text = $numPage;
            $this->numPageTopFso->Text = $numPage;
            $this->populateDataFso();
            $this->populateData();
        } else {
            $this->numPageTopFso->Text = $this->RepeaterListEchangesFso->CurrentPageIndex + 1;
            $this->numPageBottomFso->Text = $this->RepeaterListEchangesFso->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenghtFso($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTopFso':
                $pageSize = $this->listePageSizeTopFso->getSelectedValue();
                $this->listePageSizeBottomFso->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottomFso':
                $pageSize = $this->listePageSizeBottomFso->getSelectedValue();
                $this->listePageSizeTopFso->setSelectedValue($pageSize);
                break;
        }
        // echo $pageSize;exit;
        $this->RepeaterListEchangesFso->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTopFso->Text = ceil($nombreElement / $this->RepeaterListEchangesFso->PageSize);
        $this->nombrePageBottomFso->Text = ceil($nombreElement / $this->RepeaterListEchangesFso->PageSize);
        $this->RepeaterListEchangesFso->setCurrentPageIndex(0);
        $this->populateDataFso();
        $this->populateData();
    }

    public function populateDataFso()
    {
        $offset = $this->RepeaterListEchangesFso->CurrentPageIndex * $this->RepeaterListEchangesFso->PageSize;
        $limit = $this->RepeaterListEchangesFso->PageSize;
        if ($offset + $limit > $this->RepeaterListEchangesFso->getVirtualItemCount()) {
            $limit = $this->RepeaterListEchangesFso->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->remplirTableauListeEchangesFso($criteriaVo->getOrganisme(), $limit, $offset);
    }

    public function onCallBackRefreshEchangeFso($sender, $param)
    {
        $this->remplirDataSourceFso();
        $this->panelMoreThanOneElementFoundFso->render($param->getNewWriter());
        $this->panelNoElementFoundFso->render($param->getNewWriter());
    }

    public function remplirDataSourceFso()
    {
        $this->criterTri['offSet'] = '0';
        $this->criterTri['limit'] = $this->RepeaterListEchangesFso->PageSize;
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->RepeaterListEchangesFso->PageSize);
        $this->populateDataFso();
        $this->numPageTopFso->Text = $this->RepeaterListEchangesFso->CurrentPageIndex + 1;
        $this->numPageBottomFso->Text = $this->RepeaterListEchangesFso->CurrentPageIndex + 1;
    }

    public function critereTriFso($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTri', 'ASC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);
    }

    public function TrierFso($sender, $param)
    {
        $this->remplirDataSourceFso();
        $this->PanelListEchangesFso->render($param->NewWriter);
    }

    public function genererExcelEchangesFso()
    {
        (new Atexo_GenerationExcel())->genererCsvTableauBordEchangesChorus($this->getViewState('listeEchangesFso'), true);
    }

    /**
     * Permet de remplir la liste de type de contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillTypeContrat()
    {
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat('TOUS_LES_TYPES_DE_CONTRAT');
        $this->typeContrat->DataSource = $listeContrats;
        $this->typeContrat->DataBind();
    }
}
