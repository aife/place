<?php

namespace Application\Pages\Agent;
use App\Entity\Agent;
use App\Service\PradoPasswordEncoderInterface;

use Application\Controls\MpeTPage;

/**
 * Ancienne page d'accueil non authentifié des agents.
 */
class AgentHome extends MpeTPage
{
    public function onInit($param)
    {
        $this->response->redirect('/agent');
    }
}
