<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTDispositifAnnonce;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Exception;
use Prado\Prado;

/**
 * Page de suivi de la publicite.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class SuiviPublicite extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        try {
            $this->panelBlocErreur->setStyle('display:none');
            $this->panelBlocMessages->setStyle('display:none');
            $this->scriptJs->setText('');
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if (Atexo_Module::isEnabled('publicite')) {
                $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($ref);
                $org = Atexo_CurrentUser::getOrganismAcronym();
                if ($consultation instanceof CommonConsultation) {
                    //Chargement du formulaire d'ajout de publicite
                    $this->IdConsultationSummary->setConsultation($consultation);
                    if (!$this->Page->isPostBack) {
                        $this->initialiserFormulaireAjoutPublicite($consultation);
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                        $this->populateRepaterAnnonceEncours($ref, $org, $connexion);
                        $this->populateRepaterAnnonceEnvoyes($ref, $org, $connexion);
                    }
                } else {
                    $this->afficherMsgErreur(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION'));
                    $this->mainPanel->setVisible(false);
                    $this->IdConsultationSummary->setWithException(false);
                    $logger->error("Accés au formulaire suivi de la publicité pour consultation qu'on a pas droit dessus ".$ref.' par Agent '.Atexo_CurrentUser::getLastName().'('.Atexo_CurrentUser::getId()
                        .') organisme = '.Atexo_CurrentUser::getOrganismAcronym());
                }
            } else {
                $logger->error("Accés au formulaire suivi de la publicité , module plateforme 'Publicite' non active");
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        } catch (Exception) {
        }
    }

    /**
     * Permet d'initialiser le formulaire d'ajout d'une publicite.
     *
     * @param CommonConsultation $consultation : consultation
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function initialiserFormulaireAjoutPublicite($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            try {
                $this->formulaireAjoutPublicite->setRefConsultation($consultation->getId());
                $this->formulaireAjoutPublicite->setOrganisme($consultation->getOrganisme());
                $this->formulaireAjoutPublicite->initialiserFormulaire();
                $this->formulaireAjoutPublicite->setFonctionAAppelerApresAjoutAnnonce('refrechTableAnnonceEncours()');
            } catch (Exception $e) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                $logger->error("Erreur lors de l'initialisation du formulaire d'ajout d'une publicite : RefCons = ".$consultation->getId().' , Org = '.$consultation->getOrganisme().' , Erreur = '.$e->getMessage());
            }
        } else {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("La consultation n'est pas une instance de CommonConsultation. Methode = SuiviPublicite::initialiserFormulaireAjoutPublicite");
        }
    }

    public function getCssStatut($statut)
    {
        $cssStatut = '';
        $cssStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => 'label label-default',
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => 'label label-primary',
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => 'label label-info',
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => 'label label-warning',
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => 'label label-success',
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => 'label label-danger',
            default => $cssStatut,
        };

        return $cssStatut;
    }

    public function getLibelleStatut($statut)
    {
        $libelleStatut = '';
        $libelleStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => Prado::localize('STATUT_ANNONCE_A_COMPLETER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => Prado::localize('STATUT_ANNONCE_COMPLET'),
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Prado::localize('STATUT_ANNONCE_ENVOYER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Prado::localize('STATUT_ANNONCE_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Prado::localize('STATUT_ANNONCE_PUBLIE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Prado::localize('STATUT_ANNONCE_REJETE'),
            default => $libelleStatut,
        };

        return $libelleStatut;
    }

    public function getLibelleDateStatut($statut)
    {
        $libelleDateStatut = '';
        $libelleDateStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => Prado::localize('DATE_ANNONCE_A_COMPLETER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => Prado::localize('DATE_ANNONCE_COMPLET'),
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Prado::localize('DATE_ANNONCE_ENVOYER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Prado::localize('DATE_ANNONCE_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Prado::localize('DATE_ANNONCE_PUBLIE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Prado::localize('DATE_ANNONCE_REJETE'),
            default => $libelleDateStatut,
        };

        return $libelleDateStatut;
    }

    public function getCssPictoStatut($statut)
    {
        $cssPictoStatut = '';
        $cssPictoStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => 'fa fa-pencil-square-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => 'fa fa-check-square-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => 'fa fa-share',
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => 'fa fa-clock-o',
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => 'fa fa-check-circle',
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => 'fa fa-ban',
            default => $cssPictoStatut,
        };

        return $cssPictoStatut;
    }

    public function populateRepaterAnnonceEncours($ref, $org, $connexion)
    {
        $listesAnnonces = Atexo_Publicite_AnnonceSub::getAnnoncesEncours($ref, $org, $connexion);
        if (!(is_array($listesAnnonces) && count($listesAnnonces))) {
            $listesAnnonces = [];
        }
        $this->listeAnnoncesEncours->setDataSource($listesAnnonces);
        $this->listeAnnoncesEncours->dataBind();
    }

    public function populateRepaterAnnonceEnvoyes($ref, $org, $connexion)
    {
        $listesAnnonces = Atexo_Publicite_AnnonceSub::getAnnoncesEnvoyer($ref, $org, $connexion);
        if (!(is_array($listesAnnonces) && count($listesAnnonces))) {
            $listesAnnonces = [];
        }
        $this->listeAnnoncesEnvoyes->setDataSource($listesAnnonces);
        $this->listeAnnoncesEnvoyes->dataBind();
    }

    public function refrechTableAnnonceEncours($sender, $param)
    {
        $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getOrganismAcronym();
        CommonTAnnonceConsultationPeer::clearInstancePool(true);
        CommonTSupportAnnonceConsultationPeer::clearInstancePool(true);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->populateRepaterAnnonceEncours($ref, $org, $connexion);
        if ($param) {
            $this->panelAvisEncours->render($param->getNewWriter());
            $this->masquerMsg($param);
        }
    }

    public function aDroitEnvoyeAnnonce($annonce)
    {
        $droit = false;
        if ($annonce instanceof CommonTAnnonceConsultation) {
            if ($annonce->getStatut() == Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET')) {
                $droit = true;
            }
        }

        return $droit;
    }

    public function envoyerAnnonce($sender, $param)
    {
        $save = null;
        $ref = null;
        $org = null;
        try {
            $this->masquerMsg($param);
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $idAnnonce = $param->CallbackParameter;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org, $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $lisSupportPub = $annonce->getListeSupportPub();
                if (!empty($lisSupportPub)) {
                    $save = Atexo_Publicite_AnnonceSub::envoyerAnnonce($annonce);
                }
            }
            if ($save) {
                $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_ENVOI_ANNONCE'), $param);
                $this->refrechTableAnnonceEncours(null, $param);
                $this->populateRepaterAnnonceEnvoyes($ref, $org, $connexion);
                $this->panelAvisEnvoyes->render($param->getNewWriter());
            } else {
                $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_ENVOI_ANNONCE'), $param);
                $logger->error("Erreur envoi de l'annonce ".$idAnnonce.' de la consultation '.$ref.' '.$org.' : Pas de support affecté');
            }
        } catch (Exception $e) {
            $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_ENVOI_ANNONCE'), $param);
            $logger->error("Erreur envoi de l'annonce ".$idAnnonce.' de la consultation '.$ref.' '.$org.' '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public function afficherMsgErreur($msg, $param = null)
    {
        $this->panelBlocErreur->setStyle("display:''");
        $this->messageErreur->setMessage($msg);
        if ($param) {
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    public function afficherMsgSucces($msg, $param = null)
    {
        $this->panelBlocMessages->setStyle("display:''");
        $this->messageConfirmationEnregistrement->setMessage($msg);
        if ($param) {
            $this->panelBlocMessages->render($param->getNewWriter());
        }
    }

    public function masquerMsg($param = null)
    {
        $this->panelBlocErreur->setStyle('display:none');
        $this->panelBlocMessages->setStyle('display:none');
        if ($param) {
            $this->panelBlocMessages->render($param->getNewWriter());
            $this->panelBlocErreur->render($param->getNewWriter());
        }
    }

    /**
     * Permet de supprimer une annonce de publicite.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function supprimerAnnonce($sender, $param)
    {
        $connexion = null;
        $ref = null;
        $org = null;
        try {
            $this->masquerMsg($param);
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $idAnnonce = $param->CallbackParameter;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexion->beginTransaction();
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $logger->info('Suppression annonce : id = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org);
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org, $connexion);
            $idDossierSub = null;
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $annonce->delete($connexion);
                $logger->info('SUCCES suppression annonce mpe : id = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org);

                $resultat = Atexo_FormulaireSub_AnnonceEchange::supprimerDossier($annonce->getIdDossierSub());
                if (true === $resultat) {
                    $logger->info('SUCCES suppression dossier SUB : id_dossier_Sub = '.$annonce->getIdDossierSub());
                } else {
                    $connexion->rollBack();
                    $logger->info("Suppression annulee par l'action : rollBack");
                    $logger->info('ECHEC suppression dossier SUB : id_dossier_Sub = '.$annonce->getIdDossierSub());
                    $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_SUPPRESSION_ANNONCE'), $param);

                    return;
                }

                $connexion->commit();
                $logger->info('Debut raffraichissement tableau de suivi des annonces');
                $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_SUPPRESSION_ANNONCE'), $param);
                $this->refrechTableAnnonceEncours(null, $param);
                $this->scriptJs->render($param->getNewWriter());
                $logger->info('Fin raffraichissement tableau de suivi des annonces');
            } else {
                $connexion->rollBack();
                $logger->info("Suppression annulee par l'action : rollBack");
                $logger->info("L'annonce de publicite n'est pas une instance de CommonTAnnonceConsultation : ".print_r($annonce, true));
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->info("Suppression annulee par l'action : rollBack");
            $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_SUPPRESSION_ANNONCE'), $param);
            $logger->error('Erreur suppression annonce : id = '.$idAnnonce.' de la consultation , refCons = '.$ref.' , org = '.$org.' , Erreur = '.$e->getMessage());
        }
    }

    /**
     * Permet de previsualiser le document envoye.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function previsualiserDocumentEnvoye($sender, $param)
    {
        try {
            $idAnnonce = $param->CommandParameter;
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $idDossier = $annonce->getIdDossierSub();
                Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($idDossier);
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur telechargement du document envoye : Erreur ='.$e->getMessage());
        }
    }

    /**
     * Permet de previsualiser le document brouillon.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function previsualiserDocumentBrouillon($sender, $param)
    {
        try {
            $idDossier = $param->CommandParameter;
            Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($idDossier);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur telechargement du document brouillon : iddossier = $idDossier , Erreur =".$e->getMessage());
        }
    }

    public function supprimerSupport($sender, $param)
    {
        $connexion = null;
        try {
            $this->masquerMsg($param);
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $parameter = explode('#', $param->CallbackParameter);
            $idSupport = $parameter[0];
            $idAnnonce = $parameter[1];
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org, $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                if (Atexo_Publicite_AnnonceSub::getCountSupportAnnonce($idAnnonce, $connexion) > 1) {
                    $logger->info('Suppression support annonce : id = '.$idSupport.'idAnnonce  = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org);
                    $connexion->beginTransaction();
                    $support = Atexo_Publicite_AnnonceSub::getSupportAnnonce($idAnnonce, $idSupport, $connexion);
                    if ($support instanceof CommonTSupportAnnonceConsultation) {
                        $support->delete($connexion);
                        $connexion->commit();
                        CommonTAnnonceConsultationPeer::clearInstancePool(true);
                        CommonTSupportAnnonceConsultationPeer::clearInstancePool(true);
                        $this->updateDossier($annonce->getIdDossierSub(), $annonce->getIdDispositif(), $annonce->getListeSupportPub(), $logger);
                        $this->refrechTableAnnonceEncours(null, $param);
                        $logger->info('SUCCES suppression support annonce : id = '.$idSupport.'idAnnonce  = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org);
                        $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_SUPRESSION_SUPPORT'), $param);
                    }
                } else {
                    $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_SUPPRIMER_SUPPORT_ANNONCE'), $param);
                    $logger->info('Erreur suppression support annonce : id = '.$idSupport.'idAnnonce  = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org.' cause : il reste un seul support');
                }
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error('Erreur suppression support annonc e: id = '.$idSupport." de l'annonce ".$idAnnonce.' de la consultation '.$ref.' '.$org.' '.$e->getMessage().' '.$e->getTraceAsString());
            $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_SUPPRIMER_SUPPORT_ANNONCE'), $param);
        }
    }

    public function updateDossier($idDossier, $idDispositif, $listeSupport, $logger)
    {
        try {
            $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlUpdatSupport($idDossier, $idDispositif, $listeSupport, $logger);

            return Atexo_FormulaireSub_AnnonceEchange::updateAnnonce($xml, $logger);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getDataSourceSupportChoix($idAnnonce)
    {
        $supports = Atexo_Publicite_AnnonceSub::getSupportsPublicationActifs();
        $dataSource = [];
        if (is_array($supports)) {
            foreach ($supports as $s) {
                if ($s instanceof CommonTSupportPublication) {
                    $dataSource[$s->getId()]['ID'] = $s->getId();
                    $dataSource[$s->getId()]['LIBELLE'] = $s->getNom();
                    $dataSource[$s->getId()]['IMAGE'] = $this->themesPath().'/images/'.$s->getImageLogo();
                    $dataSource[$s->getId()]['CHECKED'] = false;
                    $dataSource[$s->getId()]['ENABLED'] = true;
                    $supportAnn = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($idAnnonce, $s->getId());
                    if ($supportAnn instanceof CommonTSupportAnnonceConsultation) {
                        $dataSource[$s->getId()]['CHECKED'] = true;
                        $dataSource[$s->getId()]['ENABLED'] = false;
                    }
                }
            }
        }

        return $dataSource;
    }

    public function loadPopinChoixSupport($sender, $param)
    {
        $this->masquerMsg($param);
        $idAnnonce = $param->CallbackParameter;
        $this->hiddenIdAnnonce->setValue($idAnnonce);
        $dataSource = $this->getDataSourceSupportChoix($idAnnonce);
        $this->listeSupports->setDataSource($dataSource);
        $this->listeSupports->dataBind();
        $this->scriptJs->setText("<script>openModal('modal-choix-support','popup-moyen1',document.getElementById('".$sender->getClientId()."'));</script>");
        $this->panelChoixSupport->render($param->getNewWriter());
    }

    public function ajouterSupport($sender, $param)
    {
        $connexion = null;
        $id = null;
        try {
            $this->masquerMsg($param);
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $idAnnonce = $this->hiddenIdAnnonce->getValue();
            $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceById($idAnnonce, $ref, $org, $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $connexion->beginTransaction();
                foreach ($this->listeSupports->Items as $item) {
                    if ($item->support->enabled && $item->support->checked) {
                        $id = $item->hiddenIdSupport->getValue();
                        $sp = CommonTSupportPublicationPeer::retrieveByPK($id, $connexion);
                        if ($sp instanceof CommonTSupportPublication) {
                            $support = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($idAnnonce, $id, $connexion);
                            if (!($support instanceof CommonTSupportAnnonceConsultation)) {
                                $logger->info('Ajout support annonce : id support pub = '.$id.'idAnnonce  = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org);
                                Atexo_Publicite_AnnonceSub::ajouterSupport(
                                    $idAnnonce,
                                    $id,
                                    Atexo_CurrentUser::getId(),
                                    Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName(),
                                    $annonce->getStatut(),
                                    $annonce->getDateStatut(),
                                    $connexion
                                );
                            }
                        }
                    }
                }
                $connexion->commit();
                CommonTAnnonceConsultationPeer::clearInstancePool(true);
                CommonTSupportAnnonceConsultationPeer::clearInstancePool(true);
                $this->updateDossier($annonce->getIdDossierSub(), $annonce->getIdDispositif(), $annonce->getListeSupportPub(), $logger);
                $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_AJOUT_SUPPORT'), $param);
                $this->refrechTableAnnonceEncours(null, $param);
                $this->scriptJs->setText("<script>J('.modal-choix-support').dialog('close');</script>");
                $this->scriptJs->render($param->getNewWriter());
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error('Ajout support annonce : id support pub = '.$id.'idAnnonce  = '.$idAnnonce.' , refCons = '.$ref.' , org = '.$org.'  Erreur :'.$e->getMessage().' '.$e->getTraceAsString());
            $this->afficherMsgErreur(Prado::localize('MSG_ERREUR_AJOUT_SUPPORT_ANNONCE'), $param);
        }
    }

    /**
     * Permet de construire le message de confirmation de creation d'avis rectificatif/annulation.
     *
     * @param string $typeAvis           : type d'avis (valeurs possibles = AVIS_RECTIFICATIF, AVIS_ANNULATION)
     * @param string $libelleAvisInitial : libelle de l'avis initial
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getMessageConfirmationEmissionAvisRectificatifAnnulation($typeAvis, $libelleAvisInitial)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $message = '';
            if ('AVIS_RECTIFICATIF' == $typeAvis) {
                $message = Prado::localize('MESSAGE_CONFIRMATION_EMISSION_AVIS_RECTIFICATIF');
            } elseif ('AVIS_ANNULATION' == $typeAvis) {
                $message = Prado::localize('MESSAGE_CONFIRMATION_EMISSION_AVIS_ANNULATION');
            }

            return str_replace('[__LIBELLE_AVIS__]', $libelleAvisInitial, $message);
        } catch (Exception $e) {
            $logger->error("Erreur construction message de confirmation d'emission d'avis rectificatif/annulation : Type d'avis = $typeAvis , Avis initial = $libelleAvisInitial \n\nErreur :".$e->getMessage()." \n ".$e->getTraceAsString()."\n\n Methode = SuiviPublicite::getMessageConfirmationEmissionAvisRectificatifAnnulation");
        }
    }

    /**
     * Permet de creer un avis rectificatif.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function emettreAvisRectificatif($sender, $param)
    {
        $this->masquerMsg($param);
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $listeParametres = null;
        try {
            $parametres = $param->CallbackParameter;
            $listeParametres = explode('#', $parametres);
            $this->emettreAvisRectificatifAnnulation($listeParametres[0], $listeParametres[1], $listeParametres[2], $listeParametres[3]);
            //Raffraichir le bloc des publicites en cours et affichage message de confirmée
            $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_CREATION_AVIS_RECTIFICATIF'), $param);
            $this->refrechTableAnnonceEncours(null, $param);
            $this->scriptJs->render($param->getNewWriter());
        } catch (Exception $e) {
            $logger->error("Erreur emission avis rectificatif : Id avis initial = $listeParametres[1] , refCons = $listeParametres[2] , Org = $listeParametres[3] \n\n Erreur = ".$e->getMessage()." \n ".$e->getTraceAsString()."\n\n Methode = SuiviPublicite::emettreAvisRectificatif");
        }
    }

    /**
     * Permet de creer un avis d'annulation.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function emettreAvisAnnulation($sender, $param)
    {
        $this->masquerMsg($param);
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $listeParametres = null;
        try {
            $parametres = $param->CallbackParameter;
            $listeParametres = explode('#', $parametres);
            $this->emettreAvisRectificatifAnnulation($listeParametres[0], $listeParametres[1], $listeParametres[2], $listeParametres[3]);
            //Raffraichir le bloc des publicites en cours
            $this->afficherMsgSucces(Prado::localize('MSG_CONFIRMATION_CREATION_AVIS_ANNULATION'), $param);
            $this->refrechTableAnnonceEncours(null, $param);
            $this->scriptJs->render($param->getNewWriter());
        } catch (Exception $e) {
            $logger->error("Erreur emission avis d'annulation : Id avis initial = $listeParametres[1] , refCons = $listeParametres[2] , Org = $listeParametres[3] \n\n Erreur = ".$e->getMessage()." \n ".$e->getTraceAsString()."\n\n Methode = SuiviPublicite::emettreAvisAnnulation");
        }
    }

    /**
     * Permet de creer un avis rectificatif et d'annulation.
     *
     * @param string $typeAvis      : types d'avis (valeurs possibles = AVIS_RECTIFICATIF, AVIS_ANNULATION)
     * @param string $idAvisInitial : identifiant de l'annonce de publicite initial
     * @param string $refCons       : reference de la consultation
     * @param string $org           : organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function emettreAvisRectificatifAnnulation($typeAvis, $idAvisInitial, $refCons, $org)
    {
        $connexion = null;
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $logger->info("Debut emission $typeAvis ");
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $logger->info('Debut transaction');
            $connexion->beginTransaction();

            //Recuperer avis initial
            $avisInitial = Atexo_Publicite_AnnonceSub::getAnnonceById($idAvisInitial, $refCons, $org);
            if ($avisInitial instanceof CommonTAnnonceConsultation) {
                $dispositifAnnonceInitiale = $avisInitial->getDispositif();
                if ($dispositifAnnonceInitiale instanceof CommonTDispositifAnnonce) {
                    //Dupliquer dossier initial
                    $idDossier = Atexo_FormulaireSub_AnnonceEchange::dupliquerDossier($avisInitial, $typeAvis, $logger);

                    //Identifiant du dispositif et Identifiant du type d'annonce
                    $idDispositif = null;
                    $idTypeAnnonce = null;
                    if ('AVIS_RECTIFICATIF' == $typeAvis) {
                        $idDispositif = $dispositifAnnonceInitiale->getIdDispositifRectificatif();
                        $idTypeAnnonce = $dispositifAnnonceInitiale->getIdTypeAvisRectificatif();
                    } elseif ('AVIS_ANNULATION' == $typeAvis) {
                        $idDispositif = $dispositifAnnonceInitiale->getIdDispositifAnnulation();
                        $idTypeAnnonce = $dispositifAnnonceInitiale->getIdTypeAvisAnnulation();
                    }

                    //Creer et remplir objet annonce
                    $annonce = (new Atexo_Publicite_AnnonceSub())->creerAnnoncePublicite($idDispositif, $idDossier, $refCons, $org, $idTypeAnnonce, $avisInitial->getIdCompteBoamp(), $avisInitial->getIdDossierSub(), $avisInitial->getIdDispositif());

                    //Ajouter les supports de l'avis initial
                    $supportsAvisInitial = $avisInitial->getCommonTSupportAnnonceConsultations();
                    if ($supportsAvisInitial instanceof PropelObjectCollection) {
                        $arraySupportsAvisInitial = $supportsAvisInitial->getData();
                        if (is_array($arraySupportsAvisInitial) && !empty($arraySupportsAvisInitial)) {
                            foreach ($arraySupportsAvisInitial as $support) {
                                if ($support instanceof CommonTSupportAnnonceConsultation) {
                                    (new Atexo_Publicite_AnnonceSub())->addSupportsToAnnonce($annonce, $support->getIdSupport(), $support->getNumeroAvis());
                                }
                            }
                        }
                    }

                    //Enregistrement de l'annonce
                    $annonce->save($connexion);
                    $connexion->commit();

                    //historique apres le commite car il ya des info qui doivent s'enregistrer en base avant de creer l'historique
                    Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                    $logger->info('Connexion commitee');
                } else {
                    $logger->info('Dispositif non trouvé : ID dispositif = '.$avisInitial->getIdDispositif()." \n\nAvis initial = ".print_r($avisInitial, true));
                }
            } else {
                $logger->info('Avis initial non trouve : '.print_r($avisInitial, true));
                $connexion->rollBack();
                $logger->info('Transaction annulee');
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->info('Transaction annulee');
            $logger->error("Erreur emission avis rectificatif/annulation : Type d'avis = $typeAvis , Id avis initial = $idAvisInitial , refCons = $refCons , Org = $org \n\n Erreur = ".$e->getMessage()." \n ".$e->getTraceAsString()."\n\n Methode = SuiviPublicite::emettreAvisRectificatifAnnulation");
        }
        $logger->info("Fin emission $typeAvis ");
    }
}
