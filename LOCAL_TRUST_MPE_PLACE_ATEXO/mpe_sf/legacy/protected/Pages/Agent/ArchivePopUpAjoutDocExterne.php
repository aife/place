<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ArchivePopUpAjoutDocExterne extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('ArchiveParLot')) {
            $this->fillListeLots();
        }
        $this->loadFileListe();
    }

    /**
     *  Permet d'enregistrer le document externe.
     *
     *  @return void
     *
     *  @author LEZ <loubna.ezziani@atexom.com>
     *
     *  @version 1.0
     *
     *  @since 3.0
     *
     *  @copyright Atexo 2008
     */
    public function saveDocExterne()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId);
        $lot = 0;
        if (Atexo_Module::isEnabled('ArchiveParLot') && ($consultation instanceof CommonConsultation) && $consultation->getAlloti()) {
            $lot = $this->listeLots->getSelectedValue();
        }
        $blobExterne = new Atexo_Blob();
        $documentExterne = new CommonDocumentExterne();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idType = $this->fileLocation->getSelectedValue();

        $pathsExterne = Atexo_Config::getParameter('COMMON_TMP').'ext'.session_id().time();
        if (move_uploaded_file($this->ajoutDocument->LocalName, $pathsExterne)) {
            $documentExterne->setType($idType);
            $documentExterne->setConsultationId($consultationId);
            $documentExterne->setOrganisme($org);
            $idBlob = $blobExterne->insert_blob($this->ajoutDocument->FileName, $pathsExterne, $org);
            $documentExterne->setIdBlob($idBlob);
            $documentExterne->setNom(stripslashes($this->ajoutDocument->FileName));
            $documentExterne->setDescription($this->commentaires->Text);
            $documentExterne->setDate(Atexo_Util::frnDate2iso(trim($this->datePiece->Text)));
            $documentExterne->setLot($lot);
        }
        $documentExterne->save($connexion);
        $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
    }

    public function loadFileListe()
    {
        $fileExterne = new Atexo_Consultation_Archive();
        $paths = $fileExterne->getPathsDocExternes();
        $this->fileLocation->DataSource = $paths;
        $this->fileLocation->dataBind();
    }

    /**
     *  Permet de setter la liste des lots dans le cas d'une consultation alloti.
     *
     *  @return void
     *
     *  @author LEZ <loubna.ezziani@atexom.com>
     *
     *  @version 1.0
     *
     *  @since 4.6.0
     *
     *  @copyright Atexo 2014
     */
    public function fillListeLots()
    {
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId);
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getAlloti()) {
                $this->panelListeLots->visible = true;
                $listeLots = $consultation->getAllLots();
                $dataSource = [];
                foreach ($listeLots as $lot) {
                    $dataSource[$lot->getLot()] = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescription();
                }
                $dataSource = Atexo_Util::arrayUnshift($dataSource, Prado::localize('TOUS_LES_LOTS'));
                $this->listeLots->DataSource = $dataSource;
                $this->listeLots->dataBind();
            } else {
                $this->panelListeLots->visible = false;
            }
        }
    }
}
