<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadCa extends MpeTPage
{
    public function onLoad($param)
    {
        $fileName = Atexo_Config::getParameter('ROOT_CERT');

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=root_ca_cert.pem');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.@filesize($fileName));

        //echo file_get_contents($fileName);

        $fp = @fopen($fileName, 'r');
        $block_count = floor(@filesize($fileName) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        $bytes_remaining = @filesize($fileName) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        for ($i = 0; $i < $block_count; ++$i) {
            echo @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        }
        echo @fread($fp, $bytes_remaining);
        @fclose($fp);
        exit;
    }
}
