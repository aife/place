<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Prado\Prado;

/**
 * Dupliquer un lot d'une consultation.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since 2016-develop
 */
class PopupDupliquerLot extends MpeTPage
{
    protected $_consultation;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {

        $organisme = (isset($_GET['org'])) ? Atexo_Util::atexoHtmlEntities($_GET['org']) : '';
        $consultationId = (isset($_GET['id'])) ? Atexo_Util::atexoHtmlEntities($_GET['id']) : '';
        $numLot = (isset($_GET['lot'])) ? Atexo_Util::atexoHtmlEntities($_GET['lot']) : '';
        $this->_consultation = $this->retrieveConsultation($consultationId);

        //DUME
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $this->buttonSave->Visible = false;
        } else {
            $this->buttonSaveWithDume->Visible = false;
        }

        if ($this->_consultation instanceof CommonConsultation && $this->isModificationAllowed($this->_consultation)) {
            $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $numLot, $organisme);

            if ($categorieLot instanceof CommonCategorieLot) {
                $this->setViewState('lotACopier', $categorieLot);
            } else {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
        }
    }

    /**
     * Verifier le numèro du lot.
     *
     * @param $sender
     * @param $param
     */
    public function verifyNumLot($sender, $param)
    {
        $numLot = trim($this->numLot->getSafeText());

        if ($numLot) {
            $organisme = (isset($_GET['org'])) ? Atexo_Util::atexoHtmlEntities($_GET['org']) : '';
            $consultationId = (isset($_GET['id'])) ? Atexo_Util::atexoHtmlEntities($_GET['id']) : '';
            $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $numLot, $organisme);

            if ($categorieLot instanceof CommonCategorieLot) {
                $param->IsValid = false;
                $this->numLotValidator->ErrorMessage = Prado::localize('DEFINE_LOT_DEJA_EXISTE');
                $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId
     *
     * @return bool|mixed
     */
    public function retrieveConsultation($consultationId)
    {

        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, Atexo_CurrentUser::getOrganismAcronym());

        if ($consultation) {
            return $consultation;
        }

        return false;

    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation
     *
     * @return bool
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, '', isset($_GET['id']));
    }

    /**
     * @param $sender
     * @param $param
     */
    public function dupliquerLot($sender, $param)
    {
        if ($this->getIsValid()) {
            $lot = $this->getViewState('lotACopier');

            if (Atexo_Consultation_Lots::dupliquerLot(
                $lot,
                trim($this->numLot->getSafeText()),
                $this->intituleLot->getSafeText()
            )
            ) {
                $refresh = Atexo_Util::atexoHtmlEntities($_GET['idButtonRefresh']);
                $this->script->Text = "<script>opener.document.getElementById('".$refresh."').click();window.close();</script>";
                $this->messageErreur->setVisible(false);
                $this->panelBlocErreur->setStyle('display:none');
            } else {
                $this->messageErreur->setVisible(true);
                $this->panelBlocErreur->setStyle("display:''");

                $this->messageErreur->setMessage(
                    Prado::localize('ERREUR_DUPLICATION_LOT').Atexo_Util::atexoHtmlEntities($_GET['lot'])
                );

                $this->script->Text = '';
            }
        } else {
            $this->messageErreur->setVisible(true);
            $this->panelBlocErreur->setStyle("display:''");

            $this->messageErreur->setMessage(
                Prado::localize('ERREUR_DUPLICATION_LOT').Atexo_Util::atexoHtmlEntities($_GET['lot'])
            );

            $this->script->Text = '';
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function checkPurge($sender, $param)
    {
        $dupliquerLot = true;
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
        );

        //DUME
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $c = new Criteria();
            $c->add(CommonTDumeContextePeer::ORGANISME, $this->_consultation->getOrganisme());
            $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $this->_consultation->getId());
            $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
            $tDumeContexte = CommonTDumeContextePeer::doSelectOne($c, $connexion);

            if (null != $tDumeContexte) {
                $lot = [
                    'idLot' => null, //idLot présent uniquement dans le cas d'une modification
                    'numLot' => Atexo_Util::replaceSpecialCharacters($this->numLot->Text),
                    'intituleLot' => Atexo_Util::replaceSpecialCharacters($this->intituleLot->Text),
                ];

                $checkPurge = Atexo_Dume_AtexoDume::checkPurge(
                    $this->_consultation,
                    $tDumeContexte->getContexteLtDumeId(),
                    null,
                    $lot,
                    false
                );

                if ($checkPurge['statut'] == Atexo_Config::getParameter('CHECK_PURGE_STATUS_PURGE')) {
                    $dupliquerLot = false;
                    $this->script->Text = "<script>openModal('demander-validation','popup-small2',document.getElementById('panelValidationCheckPurge'), 'Demande de validation');</script>";
                }
            }
        }
        if ($dupliquerLot) {
            $this->dupliquerLot($sender, $param);
        }
    }
}
