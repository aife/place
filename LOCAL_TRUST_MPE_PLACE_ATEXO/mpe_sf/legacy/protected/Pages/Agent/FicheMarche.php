<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonContrat;
use Application\Propel\Mpe\CommonPassationConsultation;
use Application\Propel\Mpe\CommonPassationMarcheAVenir;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Avenant;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Contrat;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Passation;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FicheMarche extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $msgErreur = '';
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (Atexo_Module::isEnabled('SuiviPassation', Atexo_CurrentUser::getCurrentOrganism())) {
            if (isset($_GET['idDE'])) {
                $idContrat = Atexo_Util::atexoHtmlEntities($_GET['idDE']);
                $contratTitulaire = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($idContrat)->findOne();
                $contratConsLot = (new Atexo_Consultation_Contrat())->getConsLotsContratByIdContratAndLot($idContrat, Atexo_Util::atexoHtmlEntities($_GET['idLot']));
                if ($contratTitulaire instanceof CommonTContratTitulaire && $contratConsLot instanceof CommonTConsLotContrat) {
                    $criteria = new Atexo_Consultation_CriteriaVo();
                    $criteria->setcalledFromPortail(false);
                    $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                    $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteria->setIdReference($contratConsLot->getConsultationId());
                    $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                    $consultation = (new Atexo_Consultation())->search($criteria);
                    if (is_array($consultation)) {
                        $consultation = array_shift($consultation);
                        if ($contratTitulaire instanceof CommonTContratTitulaire) {
                            $this->setViewState('consRef', $contratConsLot->getConsultationId());
                            $this->setViewState('lot', $contratConsLot->getLot());
                            $this->cartoucheMarche->setConsLotContrat($contratConsLot);
                            $this->cartoucheMarche->setConsultation($consultation);
                            $this->cartoucheMarche->displayDecision();

                            if (!$this->IsPostBack) {
                                $passationConsultation = (new Atexo_SuiviPassation_Passation())->retrievePassationByReferenceConsultation($consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
                                if (!($passationConsultation instanceof CommonPassationConsultation)) {
                                    $passationConsultation = (new Atexo_SuiviPassation_Passation())->createEmptyPassationConsultation($consultation, Atexo_CurrentUser::getCurrentOrganism());
                                }
                                $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrievePassationMarcheAVenirByIdPassationConsultationAndLot($passationConsultation->getId(), $contratConsLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
                                if (!($passationMarcheAVenir instanceof CommonPassationMarcheAVenir)) {
                                    $passationMarcheAVenir = new CommonPassationMarcheAVenir();
                                    $passationMarcheAVenir->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                                    $passationMarcheAVenir->setIdPassationConsultation($passationConsultation->getId());
                                    $passationMarcheAVenir->setLot($contratConsLot->getLot());
                                    $passationMarcheAVenir->save($connexion);
                                }
                                $this->setViewState('idMarche', $passationMarcheAVenir->getId());

                                $contrat = (new Atexo_SuiviPassation_Contrat())->retrieveContratByIdDecision($contratTitulaire->getIdContratTitulaire(), Atexo_CurrentUser::getCurrentOrganism());
                                if ($contrat instanceof CommonContrat) {
                                    $this->setViewState('idContrat', $contrat->getIdContrat());
                                } else {
                                    $contrat = (new Atexo_SuiviPassation_Contrat())->createContrat($passationMarcheAVenir->getId(), $contratTitulaire->getIdContratTitulaire(), Atexo_CurrentUser::getCurrentOrganism());
                                    $this->setViewState('idContrat', $contrat->getIdContrat());
                                }
                                $this->infosComplementaires->Text = str_replace("\r\n", '<br />', $contrat->getInformaionsComplementaires());
                                $this->fillListAvenant();
                            }
                        } else {
                            $msgErreur = "Aucune décision n'a été faite.";
                        }
                    } else {
                        $msgErreur = "Impossible d'accéder à la fiche du marché. Vous n'avez pas accès à cette consultation.";
                    }
                } else {
                    $msgErreur = "Aucune décision n'a été faites";
                }
            } else {
                $msgErreur = "Impossible d'accéder à la fiche du marché. Paramètre manquant."; //TODO msg err
            }
        } else {
            $msgErreur = "Impossible d'accéder à la fiche du marché."; //TODO msg err
        }

        if ($msgErreur) {
            $this->errorPart->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($msgErreur);
        }
    }

    public function fillListAvenant()
    {
        $idContrat = $this->getViewState('idContrat');
        $listeAvenants = (new Atexo_SuiviPassation_Avenant())->retrieveAvenantsByIdContrat($idContrat, Atexo_CurrentUser::getCurrentOrganism());
        if (!is_array($listeAvenants)) {
            $listeAvenants = [];
        }
        $this->avenants->DataSource = $listeAvenants;
        $this->avenants->DataBind();
    }

    public function deleteAvenant($sender, $param)
    {
        $idAvenant = $param->CommandName;
        (new Atexo_SuiviPassation_Avenant())->deleteAvenantById($idAvenant, Atexo_CurrentUser::getCurrentOrganism());
    }

    public function refreshListAvenant($sender, $param)
    {
        $this->fillListAvenant();
        $this->panelAvenant->render($param->NewWriter);
    }

    public function refreshInfos($sender, $param)
    {
        $idContrat = $this->getViewState('idContrat');
        $contrat = (new Atexo_SuiviPassation_Contrat())->retrieveContratById($idContrat, Atexo_CurrentUser::getCurrentOrganism());
        if ($contrat instanceof CommonContrat) {
            $this->infosComplementaires->Text = str_replace("\r\n", '<br />', $contrat->getInformaionsComplementaires());
        }
        $this->panelInfos->render($param->NewWriter);
    }
}
