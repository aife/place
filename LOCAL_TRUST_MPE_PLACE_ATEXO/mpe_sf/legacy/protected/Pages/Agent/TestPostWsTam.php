<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;

/*
 * Created on 9 nov. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class TestPostWsTam extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }

    public function action()
    {
        $urlLocation = Atexo_Config::getParameter('PF_URL_REFERENCE').'index.php?soap=InterfaceAddAgentIam';
        echo $urlLocation;
        $soapClient = new \SoapClient(
            null,
            [
                                            'location' => $urlLocation,
                                            'uri' => 'ns1',
                                            'trace' => 1,
                                            'exceptions' => 0,
                                        ]
        );
        echo $this->IdTam->text."\n".$this->email->text."\n".$this->nom->text."\n".$this->prenom->text."\n".$this->tel->text."\n".$this->fax->text;
        $responseSoap = $soapClient->AddAgentIam($this->IdTam->text, $this->email->text, utf8_encode($this->nom->text), utf8_encode($this->prenom->text), $this->tel->text, $this->fax->text);
        var_dump($responseSoap);
        exit;
    }
}
