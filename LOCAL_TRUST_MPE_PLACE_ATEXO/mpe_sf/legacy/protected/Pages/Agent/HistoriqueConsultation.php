<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Historique;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Prado\Prado;

class HistoriqueConsultation extends MpeTPage
{
    protected $_refConsultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->IdConsultationSummary->setReference($this->_refConsultation);
        $this->remplirTableauxHistorique(Atexo_Util::atexoHtmlEntities($this->_refConsultation), Atexo_CurrentUser::getCurrentOrganism());
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($this->_refConsultation), Atexo_CurrentUser::getCurrentOrganism());
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
        if ($consultation && $procEquivalence instanceof CommonProcedureEquivalence) {
            if (Atexo_Module::isEnabled('ReglementConsultation')
                && ('+1' === $procEquivalence->getReglementCons()
                || '+0' === $procEquivalence->getReglementCons()
                || '0' === $procEquivalence->getReglementCons()
                || '1' === $procEquivalence->getReglementCons())) {
                $this->panelRC->Style = 'display:block';
            } else {
                $this->panelRC->Style = 'display:none';
            }
        }
    }

    public function remplirTableauxHistorique($ref, $org)
    {
        $arrayDce = (new Atexo_Historique())->getHistoriqueDce($ref, $org);
        $arrayRg = (new Atexo_Historique())->getHistoriqueRG($ref, $org);
        $arrayDateFin = (new Atexo_Historique())->getHistoriqueDateFin($ref, $org);
        $arrayRetraitDossier = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_ADRESSE_RETRAIT_DOSSIERS'), $org);
        $arrayDepotDossier = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_ADRESSE_DEPOT_OFFRES'), $org);
        $arrayOuverturePlis = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_LIEU_OUVERTURE_PLIS'), $org);
        $arrayEchantillons = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_ECHANTILLONS'), $org);
        $arrayReunion = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_REUNION'), $org);
        $arrayVisitesLieux = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_VISITES_LIEUX'), $org);
        $arrayRefrentiel = (new Atexo_Historique())->getHistoriqueLtReferentielConsultation($ref, $org, Atexo_Util::atexoHtmlEntities($_GET['page']));
        $arrayVariante = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES'), $org);
        $arrayAgrements = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_AGREMENT'), $org);
        $arrayQualifications = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_QUALIFICATION'), $org);
        $arrayCautionProvisoire = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_CAUTION_PROVISOIRE'), $org);
        $arrayAccesInformationsDceEntreprises = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_ACCES_INFORMATIONS_DCE_ENTREPRISES'), $org);
        $arrayReponsesElectroniques = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_ELECTRONIQUE'), $org);
        $arrayReponsesObligatoires = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_OBLIGATOIRE'), $org);
        $arrayCaracteristiquesReponsesElectroniques = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_CARACTERISTIQUES_REPONSES_ELECTRONIQUES'), $org);
        $arrayConstitutionDossiersReponses = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_CONSTITUTION_DOSSIERS_REPONSES'), $org);
        $arrayListeInvites = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_LISTES_INVITES'), $org);
        $arrayListeDV = (new Atexo_Historique())->getHistoriqueConsultation($ref, Atexo_Config::getParameter('ID_HISTORIQUE_DOSSIER_VOLUMINEUX'), $org);
        if ($arrayDateFin) {
            $this->setDataSource($this->RepeaterDateHeureLimiteRemisePlis, $arrayDateFin);
        }
        if ($arrayDce) {
            $this->setDataSource($this->RepeaterDce, $arrayDce);
        }
        if ($arrayRg) {
            $this->setDataSource($this->RepeaterRg, $arrayRg);
        }
        if ($arrayRetraitDossier) {
            $this->setDataSource($this->historiqueAdresseRetraitDossier->repeaterHistorique, $arrayRetraitDossier);
        }
        if ($arrayRefrentiel) {
            $this->setDataSource($this->RepeaterLtRefrentiel, $arrayRefrentiel);
        }
        if ($arrayDepotDossier) {
            $this->setDataSource($this->historiqueAdresseDepotOffre->repeaterHistorique, $arrayDepotDossier);
        }
        if ($arrayOuverturePlis) {
            $this->setDataSource($this->historiqueLieuOuverture->repeaterHistorique, $arrayOuverturePlis);
        }
        if ($arrayEchantillons) {
            $this->setDataSource($this->historiqueEchantillons->repeaterHistorique, $arrayEchantillons);
        }
        if ($arrayReunion) {
            $this->setDataSource($this->historiqueReunion->repeaterHistorique, $arrayReunion);
        }
        if ($arrayVisitesLieux) {
            $this->setDataSource($this->historiqueVisiteLieux->repeaterHistorique, $arrayVisitesLieux);
        }
        if ($arrayVariante) {
            $this->setDataSource($this->historiqueVariante->repeaterHistorique, $arrayVariante);
        }
        if ($arrayAgrements) {
            $this->setDataSource($this->historiqueAgrements->repeaterHistorique, $arrayAgrements);
        }
        if ($arrayQualifications) {
            $this->setDataSource($this->historiqueQualifications->repeaterHistorique, $arrayQualifications);
        }
        if ($arrayCautionProvisoire) {
            $this->setDataSource($this->historiqueCautionProvisoire->repeaterHistorique, $arrayCautionProvisoire);
        }
        if ($arrayAccesInformationsDceEntreprises) {
            $this->setDataSource($this->historiqueAccesInformationsDceEntreprises->repeaterHistorique, $arrayAccesInformationsDceEntreprises);
        }
        if ($arrayReponsesElectroniques) {
            $this->setDataSource($this->historiqueReponsesElectroniques->repeaterHistorique, $arrayReponsesElectroniques);
        }
        if ($arrayReponsesObligatoires) {
            $this->setDataSource($this->historiqueReponsesObligatoires->repeaterHistorique, $arrayReponsesObligatoires);
        }
        if ($arrayCaracteristiquesReponsesElectroniques) {
            $this->setDataSource($this->historiqueCaracteristiquesReponsesElectroniques->repeaterHistorique, $arrayCaracteristiquesReponsesElectroniques);
        }
        if ($arrayConstitutionDossiersReponses) {
            $this->setDataSource($this->historiqueConstitutionDossiersReponses->repeaterHistorique, $arrayConstitutionDossiersReponses);
        }
        if ($arrayListeInvites) {
            $this->fillrepeaterHistorique($arrayListeInvites);
        }
        if (Atexo_Module::isEnabled('DossierVolumineux')) {
            if ($arrayListeDV) {
                $this->setDataSource($this->historiqueDossierVolumineux->repeaterHistorique, $arrayListeDV);
            }
            $this->historiqueDossierVolumineux->setVisible(true);
        } else {
            $this->historiqueDossierVolumineux->setVisible(false);
        }
    }

    public function setDataSource($elem, $data)
    {
        $elem->dataSource = $data;
        $elem->dataBind();
    }

    public function fillrepeaterHistorique($arrayListeInvites)
    {
        $this->historiqueListeInvites->repeaterHistorique->dataSource = $arrayListeInvites;
        $this->historiqueListeInvites->repeaterHistorique->dataBind();
    }

    public function getActionOrigine($idAction)
    {
        if ($idAction == Atexo_Config::getParameter('CREATION_FILE')) {
            return Prado::localize('DEFINE_TEXT_CREATION_ANNONCE');
        } else {
            return Prado::localize('DEFINE_TEXT_MODIFICATION_ANNONCE');
        }
    }
}
