<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Operations\Atexo_Operations_CriteriaVo;

/**
 * Formulaire de le recherche des operations.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 4.6
 */
class SearchOperations extends MpeTPage
{
    /*
     * Initialisation de la page
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /*
     * Load de la page
     */
    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('GestionOperations')) {
            if (!$this->IsPostBack) {
                $this->OperationAdvancedSearch->chargerCategories();
                $this->OperationAdvancedSearch->chargerType();
                $criteria = new Atexo_Operations_CriteriaVo();
                if ($_GET['idOperation']) {
                    $idOperation = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOperation']));
                    $criteria->setRefOperation($idOperation);
                }
                $criteria->setAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                if (Atexo_Module::isEnabled('organisationCentralisee', Atexo_CurrentUser::getCurrentOrganism())) {
                    $criteria->setOrganisationCentralisee(true);
                }
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                if (isset($_GET['WithCritere'])) {
                    if (Atexo_CurrentUser::readFromSession('CriteriaVo') instanceof Atexo_Operations_CriteriaVo) {
                        $criteria = Atexo_CurrentUser::readFromSession('CriteriaVo');
                        $this->setViewState('CriteriaVo', $criteria);
                        $this->OperationAdvancedSearch->chargerSearch($criteria);
                    }
                }
                $this->fillRepeaterResut($criteria);
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
        }
    }

    /*
     *  fill de repeater des operations
     */
    public function fillRepeaterResut(Atexo_Operations_CriteriaVo $criteria)
    {
        $this->ResultSearchOperation->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function changeActiveTabAndDataForRepeater($sender)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        if (!$criteriaVo) {
            $criteriaVo = new Atexo_Operations_CriteriaVo();
        }
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case 'firstTab':
                $this->onglet1->setCssClass('tab-on');
                               $this->onglet2->setCssClass('tab');
                               $this->onglet3->setCssClass('tab');
                               $criteriaVo->setStatut(null);
                break;
            case 'secondTab':
                $this->onglet1->setCssClass('tab');
                               $this->onglet2->setCssClass('tab-on');
                               $this->onglet3->setCssClass('tab');
                               $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_OPERATION_EN_COURS'));
                break;
            case 'thirdTab':
                $this->onglet1->setCssClass('tab');
                               $this->onglet2->setCssClass('tab');
                               $this->onglet3->setCssClass('tab-on');
                               $criteriaVo->setStatut(Atexo_Config::getParameter('STATUT_OPERATION_ACHEVEE'));
                break;
        }
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->ResultSearchOperation->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Permet de maintenir la tab selectionner apres rerout  la page de recherche
     * depuis la page de modification.
     */
    public function chargerTab($statut)
    {
        if ($statut == Atexo_Config::getParameter('STATUT_OPERATION_EN_COURS')) {
            $this->onglet1->setCssClass('tab');
            $this->onglet2->setCssClass('tab-on');
            $this->onglet3->setCssClass('tab');
        } elseif ($statut == Atexo_Config::getParameter('STATUT_OPERATION_ACHEVEE')) {
            $this->onglet1->setCssClass('tab');
            $this->onglet2->setCssClass('tab');
            $this->onglet3->setCssClass('tab-on');
        } else {
            $this->onglet1->setCssClass('tab-on');
            $this->onglet2->setCssClass('tab');
            $this->onglet3->setCssClass('tab');
        }
    }
}
