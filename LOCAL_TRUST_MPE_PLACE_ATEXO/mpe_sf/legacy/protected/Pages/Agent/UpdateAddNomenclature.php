<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieConsultationQuery;
use Application\Propel\Mpe\CommonSousCategorie;
use Application\Propel\Mpe\CommonSousCategoriePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;

/**
 * Page de gestion des nomenclatures.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class UpdateAddNomenclature extends MpeTPage
{
    public string $objetCat = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $niv = null;
        if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin') || !(Atexo_Module::isEnabled('ConsultationDomainesActivites'))) {
            $this->response->redirect('agent');
        }
        $this->panelMessageErreur->setVisible(false);
        if (isset($_GET['idCat'])) {
            $idCat = Atexo_Util::atexoHtmlEntities($_GET['idCat']);
            $elements = explode('.', $idCat);
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $idRacine = $elements[0];
            if (count($elements) > 1) {
                // Je suis dans le niveau superieur à 1 alors j'affiche le racine
                $this->displayParentCategorie($idRacine);
                // retourne l'objet concerné par la modification
                $this->objetCat = CommonSousCategoriePeer::retrieveByPK($idCat, $connexion);
                $niv = 2;
            } elseif (1 == count($elements)) {
                // Je suis dans le niveau 1
                $categorieConsultationQuery = new CommonCategorieConsultationQuery();
                $this->objetCat = $categorieConsultationQuery->getCategorieConsultationById($idCat);
                $niv = 1;
            }
            if (!$this->IsPostBack && isset($_GET['new'])) {
                if (0 == $_GET['new'] && $this->objetCat) {
                    // Cas de modification
                    $this->codeNomenclature->Text = $this->objetCat->getCode();
                    $this->idNomenclature->Value = $this->objetCat->getId();
                    $this->libelleNomenclatureFr->Text = $this->objetCat->getLibelleFr();
                    $this->libelleNomenclatureAr->Text = $this->objetCat->getLibelleAr();
                    $this->libelleNomenclatureAn->Text = $this->objetCat->getLibelleEn();
                } elseif (1 == $_GET['new'] && count($elements) <= 2) {
                    // Cas d'ajout
                    if (1 == $niv) {
                        // afficher le racine de la nouvelle sous catégorie
                        $this->displayParentCategorie($idRacine);
                    }
                    // génerer le nouveau id et code qui sera insérer
                    $resultat = (new Atexo_Consultation_Category())->getNextIdEtCodeSousCategorie($idCat, $niv);
                    if (is_array($resultat) && count($resultat)) {
                        $this->codeNomenclature->Text = $resultat['code'];
                        $this->idNomenclature->Value = $resultat['id'];
                    }
                }
            }
        }
    }

    /**
     * action boutton Enregistrer.
     */
    public function creerSousCategorie($sender, $param)
    {
        if (isset($_GET['new'])) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (1 == $_GET['new']) {
                // tester l'unicité de l'id
                $sousCat = CommonSousCategoriePeer::retrieveByPK($this->idNomenclature->Value, $connexion);
                if ($sousCat) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(prado::localize('DEFINE_REFERENCE_DEJA_EXISTE'));

                    return false;
                }
                $this->objetCat = new CommonSousCategorie();
                $this->objetCat->setLibelle($this->libelleNomenclatureFr->Text);
            }

            if ($this->objetCat instanceof CommonSousCategorie) {
                //  on a une sous catégorie
                $this->objetCat->setDateModification(date('Y-m-d H:i:s'));
                $elementsId = explode('.', $this->idNomenclature->Value);
                $this->objetCat->setIdCategorie($elementsId[0]);
            }
            $this->objetCat->setCode($this->codeNomenclature->Text);
            $this->objetCat->setId($this->idNomenclature->Value);
            $this->objetCat->setLibelleFr($this->libelleNomenclatureFr->Text);
            $this->objetCat->setLibelleAr($this->libelleNomenclatureAr->Text);
            $this->objetCat->setLibelleEn($this->libelleNomenclatureAn->Text);
            $this->objetCat->save($connexion);

            Atexo_CurrentUser::deleteFromSession('cachedSousCategories');
            $this->response->redirect('index.php?page=Agent.GestionNomenclature');
        }
    }

    /**
     * action boutton Annuler
     *  retoure vers la page de gestion des nomenclatures.
     */
    public function actionBouttonAnnuler($sender, $param)
    {
        Atexo_CurrentUser::deleteFromSession('cachedSousCategories');
        $this->response->redirect('index.php?page=Agent.GestionNomenclature');
    }

    public function displayParentCategorie($idCategorie)
    {
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();
        $listeCat = $categorieConsultationQuery->getCategorieConsultationById($idCategorie);

        $categories = [$listeCat];
        $this->repeaterParentCategorie->DataSource = $categories;
        $this->repeaterParentCategorie->DataBind();
    }

    public function getLibelle($value)
    {
        $libelle = ' - ';
        if ($value) {
            $libelle = $value;
        }

        return $libelle;
    }
}
