<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpDateAnnulation extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->panelMessageErreur->setVisible(false);
            $this->panelErreur->setVisible(false);
            $this->panelResizeErreur->setVisible(false);
            $this->panelResizeContainer->setVisible(false);

            $messageError = '';
            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                if (isset($_GET['consultationId']) && isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['type']) && isset($_GET['callBackButton'])) {
                    $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);
                    $idRegistre = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $referenceConsultation = Atexo_Util::atexoHtmlEntities($_GET['consultationId']);

                    $criteria = new Atexo_Consultation_CriteriaVo();
                    $criteria->setcalledFromPortail(false);
                    $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                    $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteria->setIdReference($referenceConsultation);
                    $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                    $consultation = (new Atexo_Consultation())->search($criteria);

                    if (is_array($consultation) && count($consultation) > 0) {
                        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                            $registre = CommonOffrePapierPeer::retrieveByPK($idRegistre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if (!$registre instanceof CommonOffrePapier) {
                                $messageError = Prado::localize('ERREUR_AUCUN_REGISTRE_DEPOT');
                            }

                            $this->nomEntreprise->Text = $registre->getNomEntreprise();

                            $this->panelResizeContainer->setVisible(true);
                        } else {
                            $messageError = Prado::localize('ERREUR_TYPE_REGISTRE');
                        }
                    } else {
                        $messageError = Prado::localize('NON_AUTORISE_A_ANNULER');
                    }
                }
            } else {
                $messageError = Prado::localize('ERREUR_HABILITATION_REGISTRES');
            }

            if ($messageError) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelErreur->setVisible(true);
                $this->container->setVisible(false);
                $this->panelMessageErreur->setMessage($messageError);
                $this->panelResizeErreur->setVisible(true);
            }
        }
    }

    public function onValiderClick()
    {
        // &id=42&type=6&callBackButton=registreDepotsPapier_refreshRepeater

        $idRegistre = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);

        $dateAnnulation = Atexo_Util::frnDateTime2iso($this->dateAnnulation->Text);
        (new Atexo_Consultation_Register())->annulerRegistrePapierById($idRegistre, $typeRegistre, Atexo_CurrentUser::getCurrentOrganism(), $dateAnnulation);

        $this->userScript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackButton'])."').click();window.close();</script>";
    }
}
