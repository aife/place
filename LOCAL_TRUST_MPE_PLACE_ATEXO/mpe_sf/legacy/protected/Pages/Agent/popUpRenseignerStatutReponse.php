<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpRenseignerStatutReponse extends MpeTPage
{
    private $_consultationAlloti;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function getConsultationAlloti()
    {
        return $this->_consultationAlloti;
    }

    public function setConsultationAlloti($consultationAlloti)
    {
        $this->_consultationAlloti = $consultationAlloti;
    }

    public function onLoad($param)
    {
        $this->erreur->setVisible(false);
        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
            $messageErreur = '';
            if (!$this->IsPostBack) {
                if (isset($_GET['idsOffre'])) {
                    $this->labelPopup->Text = Prado::localize('RENSEIGNER_STATUT_REPONSE');
                    if ($_GET['idsOffre']) {
                        $idsOffreEnCode = Atexo_Util::atexoHtmlEntities($_GET['idsOffre']);
                        $typePli = Atexo_Util::atexoHtmlEntities($_GET['typePli']);
                        $idsOffre = base64_decode($idsOffreEnCode);
                        $arrayIdsOffre = explode('#', $idsOffre);
                        $dataSource = self::getDataSourceFromOffres($typePli, $arrayIdsOffre, $messageErreur);
                    } else {
                        $messageErreur = Prado::localize('AUCUN_PLI');
                    }
                } elseif (isset($_GET['idsEnveloppes'])) {
                    if ($_GET['idsEnveloppes']) {
                        $this->labelPopup->Text = Prado::localize('RENSEIGNER_STATUT_ENVELOPPE');
                        $idsEnveloppesEnCode = Atexo_Util::atexoHtmlEntities($_GET['idsEnveloppes']);
                        $typePli = Atexo_Util::atexoHtmlEntities($_GET['typePli']);
                        $idsEnveloppes = base64_decode($idsEnveloppesEnCode);
                        $arrayIdsEnveloppes = explode('#', $idsEnveloppes);
                        $dataSource = self::getDataSourceFromEnveloppes($typePli, $arrayIdsEnveloppes, $messageErreur);
                    } else {
                        $messageErreur = Prado::localize('AUCUN_PLI');
                    }
                }
                if ($messageErreur) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->erreur->setVisible(true);
                    $this->container->setVisible(false);
                    $this->panelMessageErreur->setMessage($messageErreur);
                } else {
                    $this->repeaterReponses->DataSource = $dataSource;
                    $this->repeaterReponses->DataBind();
                }
            }
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->erreur->setVisible(true);
            $this->container->setVisible(false);
            $this->panelMessageErreur->setMessage(Prado::localize('EREUR_RENSEIGNER_STATUT'));
        }
    }

    public function getDataSourceFromOffres($typePli, $arrayIdsOffre, &$messageErreur)
    {
        $dataSource = [];
        $i = 0;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($typePli == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            foreach ($arrayIdsOffre as $idOffre) {
                $offre = CommonOffresPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if (($offre instanceof CommonOffres) && $offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                    $dataSource[$i]['nomEntreprise'] = $offre->getNomEntrepriseInscrit();
                    $dataSource[$i]['numReponse'] = Prado::localize('EL_POUR_ELECTRONIQUE').' '.$offre->getNumeroReponse();
                    $dataSource[$i]['nomAgent'] = $offre->getNomAgentOuverture();
                    $dataSource[$i]['idOffre'] = $idOffre;
                    $dataSource[$i]['showEnveloppesPostulees'] = false;
                    ++$i;
                }
            }
        } else {
            foreach ($arrayIdsOffre as $idOffre) {
                $offre = CommonOffrePapierPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if (($offre instanceof CommonOffrePapier) && $offre->getStatutOffrePapier() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                    $consultation = $offre->getCommonConsultation();
                    $dataSource[$i]['nomEntreprise'] = $offre->getNomEntreprise();
                    $dataSource[$i]['numReponse'] = Prado::localize('PA_POUR_PAPIER').' '.$offre->getNumeroReponse();
                    $dataSource[$i]['nomAgent'] = $offre->getNomAgentOuverture();
                    $dataSource[$i]['idOffre'] = $idOffre;
                    if ($consultation->getAlloti()) {
                        $lots = $consultation->getAllLots();
                        $this->_consultationAlloti = true;
                        $dataSource[$i]['showEnveloppesPostulees'] = true;
                        //                 } else {
//                      $dataSource[$i]['showEnveloppesPostulees'] = true;
//                      $this->_consultationAlloti =false;
//                      $lot=new CommonCategorieLot();
//                      $lot->setLot("0");
//                      $lot->setDescription($consultation->getIntituleTraduit());
//                      $lots[]=$lot;
                    }
                    $dataSource[$i]['listeLot'] = $lots;
                    ++$i;
                }
            }
        }
        if (0 == $i) {
            $messageErreur = Prado::localize('IMPOSSIBLE_RENSEIGNER_STATUT');
        }

        return $dataSource;
    }

    public function getDataSourceFromEnveloppes($typePli, $arrayIdsEnveloppes, &$messageErreur)
    {
        $dataSource = [];
        $i = 0;
        if (is_array($arrayIdsEnveloppes)) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            if ($typePli == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                $showEnveloppesPostulees = false;
                foreach ($arrayIdsEnveloppes as $idEnveloppe) {
                    $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if (($enveloppe instanceof CommonEnveloppe) && $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                        $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($enveloppe->getOffreId(), Atexo_CurrentUser::getCurrentOrganism());
                        $idOffre = $offre->getId();
                        if ($offre instanceof CommonOffres) {
                            $dataSource[$i]['nomEntreprise'] = Atexo_Entreprise::getNomEntreprise($offre->getEntrepriseId());
                            $dataSource[$i]['numReponse'] = Prado::localize('EL_POUR_ELECTRONIQUE').' '.$offre->getNumeroReponse();
                            $dataSource[$i]['nomAgent'] = '';
                            $dataSource[$i]['idOffre'] = $idOffre;
                            $dataSource[$i]['idEnveloppe'] = $idEnveloppe;
                            $dataSource[$i]['showEnveloppesPostulees'] = $showEnveloppesPostulees;
                            if ($enveloppe->getSousPli()) {
                                $dataSource[$i]['numLot'] = '- '.Prado::localize('TEXT_LOT').' '.$enveloppe->getSousPli();
                            }
                            ++$i;
                        }
                    }
                }
            } elseif ($typePli == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                foreach ($arrayIdsEnveloppes as $idEnveloppe) {
                    $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if (($enveloppe instanceof CommonEnveloppePapier) && $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                        $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($enveloppe->getOffrePapierId(), Atexo_CurrentUser::getCurrentOrganism());
                        if ($offrePapier instanceof CommonOffrePapier) {
                            $idOffre = $offrePapier->getId();
                            $consultation = $offrePapier->getCommonConsultation();
                            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());

                            if ($typeEnveloppe == Atexo_Config::getParameter('OFFRE_SEUL') || $typeEnveloppe == Atexo_Config::getParameter('OFFRE_ET_ANONYMAT')) {
                                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idOffre, Atexo_CurrentUser::getCurrentOrganism(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                $enveloppeTraitees = false;
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $enveloppeOffre) {
                                        if ($enveloppeOffre->getStatutEnveloppe() != Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                                            $enveloppeTraitees = true;
                                        }
                                    }
                                }
                                if (!$enveloppeTraitees) {
                                    $showEnveloppesPostulees = true;
                                }
                            }
                            if ($showEnveloppesPostulees || ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'))) {
                                $showEnveloppesPostulees = true;
                                $afficherRepeater = true;
                            } else {
                                $afficherRepeater = false;
                                $showEnveloppesPostulees = false;
                            }
                            $dataSource[$i]['nomEntreprise'] = $offrePapier->getNomEntreprise();
                            $dataSource[$i]['numReponse'] = Prado::localize('PA_POUR_PAPIER').' '.$offrePapier->getNumeroReponse();
                            $dataSource[$i]['nomAgent'] = '';
                            $dataSource[$i]['idOffre'] = $idOffre;
                            $dataSource[$i]['idEnveloppe'] = $idEnveloppe;
                            $dataSource[$i]['showEnveloppesPostulees'] = $showEnveloppesPostulees;
                            if ($enveloppe->getSousPli()) {
                                $dataSource[$i]['numLot'] = '- '.Prado::localize('TEXT_LOT').' '.$enveloppe->getSousPli();
                            }

                            $lots = [];
                            if ($afficherRepeater) {
                                //$enveloppeSuivante=Atexo_Consultation_Responses::
                                if ($consultation->getAlloti()) {
                                    $lots = $consultation->getAllLots();
                                    $this->_consultationAlloti = true;
                                } else {
                                    $this->_consultationAlloti = false;
                                    $lot = new CommonCategorieLot();
                                    $lot->setLot('0');
                                    $lot->setDescription($consultation->getIntituleTraduit());
                                    $lots[] = $lot;
                                }
                            }
                            $dataSource[$i]['listeLot'] = $lots;
                            ++$i;
                        }
                    }
                }
            }
        }
        if (0 == $i) {
            $messageErreur = Prado::localize('IMPOSSIBLE_RENSEIGNER_STATUT');
        }

        return $dataSource;
    }

    /*
     * permet d'enregistrer le staut des enveloppes
     */
    public function onEnregistrerClick()
    {
        $connexionCom = null;
        try {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $typePli = Atexo_Util::atexoHtmlEntities($_GET['typePli']);
            $connexionCom->beginTransaction();
            if (isset($_GET['idsOffre'])) {
                foreach ($this->repeaterReponses->Items as $item) {
                    if ($item->statutOuverte->checked) {
                        $idOffre = $item->idOffre->Value;
                        if ($typePli == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                            $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                            if ($offre instanceof CommonOffres) {
                                $enveloppes = $offre->getCommonEnveloppes();
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $oneEnveloppe) {
                                        $oneEnveloppe->setDateheureOuverture(Atexo_Util::frnDateTime2iso($item->dateOuvertue->Text));
                                        $oneEnveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'));
                                        $oneEnveloppe->setNomAgentOuverture($item->par->Text);
                                        $oneEnveloppe->save($connexionCom);
                                    }
                                }
                                $offre->setAgentidOuverture(Atexo_CurrentUser::getIdAgentConnected());
                                $offre->setDateHeureOuverture(Atexo_Util::frnDateTime2iso($item->dateOuvertue->Text));
                                $offre->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'));
                                $offre->setNomAgentOuverture($item->par->Text);
                                $offre->save($connexionCom);
                            }
                        } else {
                            $offre = CommonOffrePapierPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                            if ($offre instanceof CommonOffrePapier) {
                                $idOffre = $offre->getId();
                                foreach ($item->listeLots->getItems() as $itemLot) {
                                    $c = new Criteria();
                                    $c->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, $idOffre);
                                    $c->add(CommonEnveloppePapierPeer::SOUS_PLI, $itemLot->sousPli->Value);
                                    $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
                                    $enveloppes = CommonEnveloppePapierPeer::doSelect($c, $connexionCom);

                                    if ($itemLot->lot->Checked) {
                                        if (is_array($enveloppes)) {
                                            foreach ($enveloppes as $enveloppeLot) {
                                                $enveloppeLot->setEnveloppePostule('1');
                                                $enveloppeLot->save($connexionCom);
                                            }
                                        }
                                    } else {
                                        if (is_array($enveloppes)) {
                                            foreach ($enveloppes as $enveloppeLot) {
                                                $enveloppeLot->setEnveloppePostule('0');
                                                $enveloppeLot->save($connexionCom);
                                            }
                                        }
                                    }
                                }
                                $enveloppes = $offre->getCommonEnveloppesPapiers();
                                if (is_array($enveloppes)) {
                                    foreach ($enveloppes as $oneEnveloppe) {
                                        $oneEnveloppe->setDateheureOuverture(Atexo_Util::frnDateTime2iso($item->dateOuvertue->Text));
                                        $oneEnveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE'));
                                        $oneEnveloppe->setCryptage('0');
                                        $oneEnveloppe->setNomAgentOuverture($item->par->Text);
                                        $oneEnveloppe->save($connexionCom);
                                    }
                                }

                                $offre->setStatutOffrePapier(Atexo_Config::getParameter('STATUT_ENV_OUVERTE'));
                                $offre->setNomAgentOuverture($item->par->Text);
                                $offre->setDateHeureOuverture(Atexo_Util::frnDateTime2iso($item->dateOuvertue->Text));
                                $offre->setAgentidOuverture($item->par->Text);
                                $offre->save($connexionCom);
                            }
                        }
                    }
                }
            } elseif (isset($_GET['idsEnveloppes'])) {
                foreach ($this->repeaterReponses->Items as $item) {
                    if ($item->statutOuverte->checked) {
                        $idOffre = $item->idOffre->Value;
                        $idEnveloppe = $item->idEnveloppe->Value;
                        if ($_GET['typePli'] == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                            $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
                        } else {
                            $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
                        }
                        if ($enveloppe instanceof CommonEnveloppePapier || $enveloppe instanceof CommonEnveloppe) {
                            $enveloppe->setDateheureOuverture(Atexo_Util::frnDateTime2iso($item->dateOuvertue->Text));
                            $enveloppe->setNomAgentOuverture($item->par->Text);

                            if ($enveloppe instanceof CommonEnveloppe) {
                                $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'));
                            } else {
                                $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE'));
                                $enveloppe->setCryptage('0');
                                /////////////////
                                if (trim($item->entreprise->Text)) {
                                    $offre = CommonOffrePapierPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                                    if ($offre instanceof CommonOffrePapier) {
                                        $offre->setNomEntreprise(trim($item->entreprise->Text));
                                        $offre->save($connexionCom);
                                    } else {
                                        throw new Exception();
                                    }
                                }
                                ////////
                                foreach ($item->listeLots->getItems() as $itemLot) {
                                    $c = new Criteria();
                                    $c->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, $idOffre);
                                    $c->add(CommonEnveloppePapierPeer::SOUS_PLI, $itemLot->sousPli->Value);
                                    $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
                                    $enveloppes = CommonEnveloppePapierPeer::doSelect($c, $connexionCom);

                                    if ($itemLot->lot->Checked) {
                                        if (is_array($enveloppes)) {
                                            foreach ($enveloppes as $enveloppeLot) {
                                                $enveloppeLot->setEnveloppePostule('1');
                                                $enveloppeLot->save($connexionCom);
                                            }
                                        }
                                    } else {
                                        if (is_array($enveloppes)) {
                                            foreach ($enveloppes as $enveloppeLot) {
                                                $enveloppeLot->setEnveloppePostule('0');
                                                $enveloppeLot->save($connexionCom);
                                            }
                                        }
                                    }
                                }
                            }
                            $enveloppe->save($connexionCom);
                        }
                    }
                }
            }
            $connexionCom->commit();
            $this->javascript->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callbackButton'])."').click();window.close();</script>";
        } catch (Exception $e) {
            $connexionCom->rollback();
            print_r($e);
            exit;
        }
    }
}
