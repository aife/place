<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * Permet de telecharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadPieceDce extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
            $result = (new Atexo_Consultation_Dce())->getPieceByIdBlobRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['IdBlob']), Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($result) {
                $this->_idFichier = $result->getIdBlob();
                $this->_nomFichier = $result->getNomPiece();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getCurrentOrganism());
            } else {
                $this->response->redirect('index.php?page=Agent.AgentHome');
            }
        } else {
            $this->response->redirect('index.php?page=Agent.AgentHome');
        }
    }
}
