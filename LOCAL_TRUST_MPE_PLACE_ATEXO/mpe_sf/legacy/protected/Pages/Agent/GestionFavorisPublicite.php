<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Favoris;

/**
 * commentaires Admministration des  Journaux d'Annonces Legales.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionFavorisPublicite extends MpeTPage
{
    private $_selectedEntity;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (Atexo_Module::isEnabled('Publicite')) {
                if (Atexo_Module::isEnabled('GestionBoampMesSousServices')) {
                    $this->ChoixEntiteAchat->setVisible(true);
                } else {
                    $this->ChoixEntiteAchat->setVisible(false);
                }
                $this->panelEntiteAchat->setVisible('true');
                $this->setViewState('idService', Atexo_CurrentUser::getCurrentServiceId());
                $this->lienAjouterFavori->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpFormulaireFavoriPublicite&idService=".$this->getViewState('idService')."','yes');";
                $favoris = Atexo_Publicite_Favoris::search($this->getViewState('idService'));
                $this->RepeaterFavorisPublicite->DataSource = $favoris;
                $this->RepeaterFavorisPublicite->DataBind();
            } else {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'agent');
            }
        }
    }

    public function getSelectedEntityPurchase()
    {
        return $this->_selectedEntity;
    }

    public function onClickSearch($sender, $param)
    {
        $this->_selectedEntity = $this->ChoixEntiteAchat->getSelectedEntityPurchase();
        $this->setViewState('idService', $this->_selectedEntity);
        $this->lienAjouterFavori->NavigateUrl = "javascript:popUp('index.php?page=Agent.popUpFormulaireFavoriPublicite&idService=".$this->_selectedEntity."','yes');";
        $favoris = Atexo_Publicite_Favoris::search($this->getViewState('idService'));
        $this->RepeaterFavorisPublicite->DataSource = $favoris;
        $this->RepeaterFavorisPublicite->DataBind();
        // Rafraichissement du ActivePanel
        $this->ApRepeaterFavorisPublicite->render($param->NewWriter);
    }

    public function onDeleteClick($sender, $param)
    {
        $idFavori = $param->CommandParameter;
        Atexo_Publicite_Favoris::delete($idFavori);
        $favoris = Atexo_Publicite_Favoris::search($this->getViewState('idService'));
        $this->RepeaterFavorisPublicite->DataSource = $favoris;
        $this->RepeaterFavorisPublicite->DataBind();
    }

    public function refreshRepeaterFavorisPublicite($sender, $param)
    {
        // Rafraichissement du ActivePanel
        $favoris = Atexo_Publicite_Favoris::search($this->getViewState('idService'));
        $this->RepeaterFavorisPublicite->DataSource = $favoris;
        $this->RepeaterFavorisPublicite->DataBind();
        $this->ApRepeaterFavorisPublicite->render($param->NewWriter);
    }
}
