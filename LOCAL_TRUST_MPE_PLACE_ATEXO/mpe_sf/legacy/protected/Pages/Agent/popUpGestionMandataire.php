<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonMandataireService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * commentaires.
 *
 * @author Maatalla Houriya <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class popUpGestionMandataire extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->remplirMandataire();
        }
    }

    public function remplirMandataire()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        // Debut remplissage de la liste des mandataires
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true, $langue);
        $this->choixMandataire->Datasource = $entities;
        $this->choixMandataire->dataBind();
        $this->choixMandataire->setSelectedValue(Atexo_CurrentUser::getIdServiceAgentConnected());
    }

    public function saveDataMandataire()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $mandataireService = new CommonMandataireService();
        $mandataireService->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $mandataireService->setMandataire($this->choixMandataire->getSelectedValue());
        $mandataireService->setServiceId(Atexo_Util::atexoHtmlEntities($_GET['serv']));
        $mandataireService->setCommentaire($this->commentaires->Text);
        $mandataireService->save($connexion);

        $this->scriptAddAndClose->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_activateAddMandataire').click();window.close();</script>";
    }
}
