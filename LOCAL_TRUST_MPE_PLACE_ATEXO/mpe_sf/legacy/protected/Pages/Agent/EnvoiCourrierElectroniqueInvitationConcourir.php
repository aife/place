<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EnvoiCourrierElectroniqueInvitationConcourir.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EnvoiCourrierElectroniqueInvitationConcourir extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE');
        $this->TemplateEnvoiCourrierElectronique->messageType->setEnabled(false);
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if (!$this->IsPostBack && !$_GET['IdEchange']) {
            $this->TemplateEnvoiCourrierElectronique->destinataires->Text = $this->remplirListeDestinataires();
            $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueUniquementLienTelechargementObligatoire->checked = true;
        }

        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function remplirListeDestinataires()
    {
        $consultation0 = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $listeDestinataires = [];
        $allotie = [];
        $allotie0 = [];
        $arrayNumLot = [];
        $arrayNumLot0 = [];
        $numLot = [];

        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if ($consultation) {
            $allotie = $consultation->getAllLots($connexionCom);
            if ($consultation->getReferenceConsultationInit()) {
                $consultation0 = (new Atexo_Consultation())->retrieveConsultation($consultation->getReferenceConsultationInit(), $consultation->getOrganismeConsultationInit());
                if ($consultation0 instanceof CommonConsultation) {
                    $allotie0 = $consultation0->getAllLots();
                }
            }
        }
        if (((is_countable($allotie) ? count($allotie) : 0) + (is_countable($allotie0) ? count($allotie0) : 0)) == 0) {
            $numLot[] = 0;
        } elseif (0 != (is_countable($allotie) ? count($allotie) : 0) && 0 != (is_countable($allotie0) ? count($allotie0) : 0)) {
            foreach ($allotie as $categorieLot) {
                $arrayNumLot[] = $categorieLot->getLot();
            }
            foreach ($allotie0 as $categorieLot0) {
                $arrayNumLot0[] = $categorieLot0->getLot();
            }

            foreach ($arrayNumLot as $lot) {
                if (in_array($lot, $arrayNumLot0)) {
                    $numLot[] = $lot;
                }
            }
        } elseif (0 != (is_countable($allotie) ? count($allotie) : 0) && 0 == (is_countable($allotie0) ? count($allotie0) : 0)) {
            $numLot[] = 0;
        } elseif (0 == (is_countable($allotie) ? count($allotie) : 0) && 0 != (is_countable($allotie0) ? count($allotie0) : 0)) {
            foreach ($allotie0 as $categorieLot0) {
                $numLot[] = $categorieLot0->getLot();
            }
        }
        $mails = [];
        if ($consultation0 instanceof CommonConsultation) {
            foreach ($numLot as $lot) {
                $mails = $this->getEntreprisesMail($consultation0->getId(), $lot, $connexionCom);
            }
        } elseif ($consultation->getIdContrat()) {
            $mails = $this->getListeMailContacts($consultation->getIdContrat(), $connexionCom);
        }

        if (is_array($mails)) {
            foreach ($mails as $mail) {
                if (!in_array($mail, $listeDestinataires)) {
                    $listeDestinataires[] = $mail;
                }
            }
        }

        return implode(' , ', $listeDestinataires);
    }

    public function getEntreprisesMail($consultationId, $idLot, $connexion)
    {
        $mails = [];
        $c = new Criteria();
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        $c->add(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTDecisionSelectionEntreprisePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTDecisionSelectionEntreprisePeer::LOT, $idLot, Criteria::EQUAL);
        $decisionEnv = CommonTDecisionSelectionEntreprisePeer::doSelect($c, $connexion);

        if (is_array($decisionEnv) && 0 != count($decisionEnv)) {
            foreach ($decisionEnv as $decision) {
                if ($decision->getTypeDepotReponse() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                    $offre = new CommonOffres();
                    $offre = CommonOffresPeer::retrieveByPK($decision->getIdOffre(), $organisme, $connexion);
                    if ($offre->getMailsignataire()) {
                        $mails[] = $offre->getMailsignataire();
                    }
                } else {
                    $offrePapier = new CommonOffrePapier();
                    $offrePapier = CommonOffrePapierPeer::retrieveByPK($decision->getIdOffre(), $organisme, $connexion);
                    if ($offrePapier->getEmail()) {
                        $mails[] = $offrePapier->getEmail();
                    }
                }
            }
        }

        return $mails;
    }

    public function getListeMailContacts($idContrat, $connexion)
    {
        $mails = [];
        if ($idContrat) {
            $contrat = CommonTContratTitulairePeer::retrieveByPK($idContrat, $connexion);
            if ($contrat instanceof CommonTContratTitulaire) {
                $contratsAttributaires = $contrat->getListeAttributaire();
                foreach ($contratsAttributaires as $contratAttributaire) {
                    if ($contratAttributaire instanceof CommonTContratTitulaire) {
                        $contact = $contratAttributaire->getCommonTContactContrat($connexion);
                        if ($contact instanceof CommonTContactContrat) {
                            $mails[] = $contact->getEmail();
                        }
                    }
                }
            }
        }

        return $mails;
    }
}
