<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Atexo_Consultation_StatutEnveloppe;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class popUpAvisMembresCommission extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $enveloppe = null;
        $messageError = '';
        $this->panelMessageErreur->setVisible(false);
        if (Atexo_Module::isEnabled('AvisMembresCommision') && Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
            if (!$this->IsPostBack) {
                $idOffre = Atexo_Util::atexoHtmlEntities($_GET['offre']);
                $formeEnv = Atexo_Util::atexoHtmlEntities($_GET['formeEnveloppe']);
                $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $typeEnv = Atexo_Util::atexoHtmlEntities($_GET['typeEnv']);
                $sousPli = Atexo_Util::atexoHtmlEntities($_GET['sousPli']);
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $connectionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($organisme);
                $criteria->setIdReference($consultationId);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = (new Atexo_Consultation())->search($criteria);
                if (is_array($consultation) && 1 == count($consultation)) {
                    $consultation = array_shift($consultation);
                }
                if ($consultation instanceof CommonConsultation) {
                    $constitutionDossierReponse = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($consultationId);
                    $criteriaResponse->setIdOffre($idOffre);
                    $criteriaResponse->setTypesEnveloppes($constitutionDossierReponse);
                    $criteriaResponse->setOrganisme($organisme);
                    $criteriaResponse->setSearchByIdOffre(true);
                    if ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme, $typeEnv, $sousPli);
                            (new Atexo_Consultation_Responses())->addAdmissibility($enveloppe, $consultationId, $organisme, $typeEnv, $sousPli);
                        }
                    } elseif ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier($consultationId, $idOffre, null, '', '', $organisme, true);
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idOffre, $organisme, $typeEnv, $sousPli);
                            (new Atexo_Consultation_Responses())->addAdmissibility($enveloppe, $consultationId, $organisme, $typeEnv, $sousPli);
                        }
                    } else {
                        $messageError .= Prado::localize('TYPE_DEPOT_INCONNU');
                    }
                    if (!$enveloppe) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $messageError = Prado::localize('AUCUNE_CANDIDATURE_TROUVE');
                        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $messageError = Prado::localize('AUCUNE_OFFRE_TECHNIQUE_TROUVE');
                        }
                    }
                    if (1 == (is_countable($enveloppe) ? count($enveloppe) : 0)) {
                        $enveloppe = array_shift($enveloppe);
                    } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                        $messageError = Prado::localize('PLUSIEURS_CANDIDATURE_TROUVE');
                    }
                    if ((!$enveloppe  instanceof CommonEnveloppe) && !($enveloppe instanceof CommonEnveloppePapier)) {
                        $messageError = Prado::localize('AUCUNE_CANDIDATURE_TROUVE');
                    }
                    if ('0' != $consultation->getEnvOffre() && ($enveloppe instanceof CommonEnveloppe)) {
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreByIdOffre($idOffre, $organisme);
                        } else {
                            $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreByIdOffre($idOffre, $organisme);
                        }
                        if (!$enveloppeOffre) {
                            $messageError = Prado::localize('ERREUR_AUCUNE_OFFRE');
                        }
                    }
                    if (!$messageError) {
                        $this->stautEnveloppe->Text = (new Atexo_Consultation_StatutEnveloppe())->getStatutEnveloppeById($enveloppe->getStatutEnveloppe());
                        if ($enveloppe  instanceof CommonEnveloppe) {
                            $this->nonEntreprise->Text = $enveloppe->getOffre($connectionCom)->getNomEntrepriseInscrit();
                            $this->avisMembesCAO->setIdEnv($enveloppe->getIdEnveloppeElectro());
                        } else {
                            $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($enveloppe->getOffrePapierId(), $organisme);
                            $this->nonEntreprise->Text = $offrePapier->getNomEntreprise();
                            $this->avisMembesCAO->setIdEnv($enveloppe->getIdEnveloppePapier());
                        }
                        $listeOffres = [];
                        $listeOffres = $enveloppe->getCollAdmissibiliteLot();
                        $this->fillRepeaterAvisCAO($listeOffres, $formeEnv, $typeEnv);
                        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && is_array($listeOffres) && count($listeOffres) > 1) {
                            $this->togglePanel->setVisible(true);
                        } else {
                            $this->togglePanel->setVisible(false);
                        }
                    }
                } else {
                    $messageError = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            }
        } else {
            $messageError = Prado::localize('HABILITATION_GERE_AVIS_CAO_DESACTIVER');
        }
        if ($messageError) {
            $this->setMessageErreur($messageError);
        }
    }

    public function setMessageErreur($messageError)
    {
        $this->panelErreur->setVisible(true);
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageError);
        $this->panelSuccess->setVisible(false);
        $this->panelBouton->setVisible(false);
    }

    /*
     * return si l'etat du lot est admis
     */
    public function isLotAdmis($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_ADMISSIBLE');
    }

    /*
     * return si l'etat du lot est non admis
     */
    public function isLotNonAdmis($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_NON_ADMISSIBLE');
    }

    /*
     * return si l'etat du lot est à traiter
     */
    public function isLotATraiter($etatLot)
    {
        return $etatLot == Atexo_Config::getParameter('LOT_A_TRAITER');
    }

    /*
     * Permet de remplir le repeaterAvicCAO du composant AtexoAvisMembresCAO
     * @param $listeOffres : liste des offres,$formeEnv : forme de l'enveloppe(papier ou electronique)
     */
    public function fillRepeaterAvisCAO($listeOffres, $formeEnv, $typeEnv)
    {
        $dataRepeater = [];
        $i = 0;
        if (is_array($listeOffres)) {
            foreach ($listeOffres as $offre) {
                if (0 == $offre->getSousPli()) {
                    $description = Prado::localize('TEXT_OBJET').' : ';
                } else {
                    $description = Prado::localize('TEXT_LOT').' '.$offre->getSousPli().' : ';
                }
                $dataRepeater[$i]['sousPli'] = $offre->getSousPli();
                $dataRepeater[$i]['description'] = $description.$offre->getIntiuleLot();
                $dataRepeater[$i]['formeEnv'] = $formeEnv;
                if ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                    $dataRepeater[$i]['idOffre'] = $offre->getIdOffre();
                } else {
                    $dataRepeater[$i]['idOffre'] = $offre->getIdOffrePapier();
                }
                $dataRepeater[$i]['typeEnv'] = $typeEnv;
                ++$i;
            }
        }
        $this->avisMembesCAO->repeaterAvisLot->DataSource = $dataRepeater;
        $this->avisMembesCAO->repeaterAvisLot->DataBind();
    }

    /*
     * permet d'enregistrer l'avis de l'agent connécté
     */
    public function saveAvisCAO($sender, $param)
    {
        $this->avisMembesCAO->saveAvisMembresCAO();
        $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackbutton'])."').click();window.close();</script>";
    }
}
