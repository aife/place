<?php

namespace Application\Pages\Agent;

use App\Service\FlashBag\MessageBagInterface;
use App\Service\Routing\RouteBuilderInterface;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_VisionRma;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Prado\Prado;

/**
 * Class pour l'affichage du tableau de bord.
 *
 * @author Adil El Kanabi <adil.kanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 *
 * @deprecated - Call of this page deprecated, will redirect to the SF Dashboard
 */
class TableauDeBord extends MpeTPage
{
    protected ?bool $isProfilRmaVisible = null;

    public const PARAM_ID = 'id';
    public const PARAM_SEARCH = 'search';
    public const PARAM_KEYWORD = 'keyWord';

    public function onInit($param)
    {
        // We redirect to the SF Dashboard only if env parameter is set to 1 (true) and if not rma
        if (Atexo_Config::getParameter('USE_SF_DASHBOARD_CONSULTATION') && empty($_GET['rma'])) {
            // Generate FlashBag Messages by GET Parameters
            Atexo_Util::getSfService(MessageBagInterface::class)->displayFlashMessages($_GET);

            // Getting path of the SF Dashboard
            $sfDashboardPath = Atexo_Util::getSfService(RouteBuilderInterface::class)->getRoute(
                'consultation_search_index',
                [
                    self::PARAM_ID => $_GET[self::PARAM_ID],
                    self::PARAM_SEARCH => 'true',
                    self::PARAM_KEYWORD => $_GET[self::PARAM_KEYWORD],
                ]
            );

            // Redirect to SF Dashboard, we do not use this Prado Dashboard
            $this->response->redirect($sfDashboardPath);
        }


        $this->Master->setCalledFrom(strtolower(Atexo_CurrentUser::getRole()));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->ResultSearch->scriptJS->Text = '';
        //echo strtr($string, '&$-_/*+.','        ');

        $this->isProfilRmaVisible = Atexo_Agent_VisionRma::isProfilRmaVisible();
        $this->AdvancedSearch->setIsProfilRmaVisible($this->isProfilRmaVisible);
        $this->messageErreur->setVisible(false);
        if (!$this->IsPostBack) {
            $launchSearch = true;
            if (isset($_GET['ref'])) {
                $id = Atexo_Util::atexoHtmlEntities($_GET['ref']);
                $org = Atexo_CurrentUser::getOrganismAcronym();
                $serviceSF = Atexo_Util::getSfService('app.mapping.consultation');
                if ($serviceSF) {
                    $idConsultation = $serviceSF->getConsultationId($id, $org);
                    $_GET['id'] = $idConsultation;
                    if (-1 == $idConsultation) {
                        $launchSearch = false;
                        $this->ResultSearch->masquerPagination();
                        $this->messageErreur->setVisible(true);
                        $this->messageErreur->setMessage(Prado::localize('MESSAGE_ERREUR_DEUX_CONSULTATIONS_EN_COLLISION'));
                    }
                }
            }
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            if (isset($_GET['wsState'])) {
                $nouveauStatutConsultation = $this->tabChoice($_GET['wsState']);
                $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutConsultation);
            }

            if (isset($_GET['keyWord'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->TableauDeBord->setVisible(true);
                $keyWord = Atexo_Util::atexoHtmlEntities($_GET['keyWord']);
                $criteriaVo->setKeyWordAdvancedSearch($keyWord);
            } elseif (isset($_GET['id'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->TableauDeBord->setVisible(true);
                $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                if (isset($_GET['save'])) {
                    $this->messageConfirmationEnregistrement->visible = true;
                    $this->messageConfirmationEnregistrement->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
                }
                if (1 == $_GET['noCheckedEnv']) {
                    $this->panelMessageAvertissement->visible = true;
                    $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_CHOISIR_TYPE_ENVELOPPE'));
                }
                if (1 == $_GET['noAvisGenerated']) {
                    $this->panelMessageAvertissement->visible = true;
                    $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_PROBLEME_GENERATION_AVIS_PDF'));
                }
                $visibiliteAvertissement = false;
                $msgAvertissement = '';
                if (isset($_GET['withoutDce']) && !isset($_GET['addDv'])) {
                    $visibiliteAvertissement = true;
                    $msgAvertissement = '<span>'.Prado::localize('MSG_AVERTISSEMENT_AUCUN_DCE_CONSULTATION').'</span><br/>';
                }

                if (isset($_GET['etatDateFin'])) {
                    $visibiliteAvertissement = true;
                    $msgAvertissement .= '<span>'.Prado::localize('MSG_AVERTISSEMENT_DATE_FIN').'</span><br/>';
                }
                if (isset($_GET['etatDateCreationDateFin'])) {
                    $visibiliteAvertissement = true;
                    $msgAvertissement .= '<span>'.str_replace('[__DELAI_DLRO__]', Atexo_Config::getParameter('DELAI_DLRO'), Prado::localize('ERREUR_DELAI_DLRO')).'</span><br/>';
                }
                if (isset($_GET['dateFinContratInvalid'])) {
                    $visibiliteAvertissement = true;
                    $sautDeLigne = '';
                    if (!empty($msgAvertissement)) {
                        $sautDeLigne = '<br/>';
                    }
                    $msgAvertissement .= $sautDeLigne.'<span>'.Prado::localize('DATE_FIN_CONTRAT_DEPASSEE').'</span><br/>';
                }

                if (isset($_GET['siretInvalid'])) {
                    $visibiliteAvertissement = true;
                    $sautDeLigne = '';
                    if (!empty($msgAvertissement)) {
                        $sautDeLigne = '<br/>';
                    }
                    $msgAvertissement .= $sautDeLigne.'<span>'.Prado::localize('MSG_CONTROLE_SIRET_ACHETEUR_TABLEAU_BORD_CONSULTATION').'</span><br/>';
                }
                $langueOblPublication = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
                if ((is_countable($langueOblPublication) ? count($langueOblPublication) : 0) && isset($_GET['traductionOgl'])) {
                    $listeLangues = "<ul class='default-list liste-langues'>";
                    foreach ($langueOblPublication as $oneLangue) {
                        if (Atexo_CurrentUser::readFromSession('lang') != $oneLangue->getLangue()) {
                            $visibiliteAvertissement = true;
                            $urlTraduction = 'index.php?page=Agent.TraduireConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&langue='.$oneLangue->getLangue();
                            $listeLangues .= "<li> <a href='".$urlTraduction."' >".ucfirst(strtolower($oneLangue->getLibelle())).'</a></li>';
                        }
                    }
                    $listeLangues .= '</ul>';
                    $this->panelMessageAvertTraduction->setVisible($visibiliteAvertissement);
                    $msgAvertissement .= '<span>'.Prado::localize('MSG_OBLIGATION_TRADUIRE_CONSULTATION', ['langues' => $listeLangues]).'</span>';
                }
                if (isset($_GET['EchecPub'])) {
                    $this->panelMessageAvertissement->visible = true;
                    $this->panelMessageAvertissement->setMessage(Prado::localize('MESSAGE_ECHEC_ENVOIE_DEPUIS_VALIDER_CONSULTATION'));
                }

                if (isset($_GET['ArchiveExpirer'])) {
                    $this->panelMessageAvertissement->visible = true;
                    $this->panelMessageAvertissement->setMessage(Prado::localize('TELECHARGEMENT_EXPIRER'));
                }

                $this->panelMessageAvertTraduction->setVisible($visibiliteAvertissement);
                $this->panelMessageAvertTraduction->setMessage($msgAvertissement);
            } elseif (isset($_GET['idAlerte'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->TableauDeBord->setVisible(true);
                $idAlerte = Atexo_Util::atexoHtmlEntities($_GET['idAlerte']);
                $criteriaVo->setIdAlerteConsultation($idAlerte);
                $criteriaVo->setAlerteConsultationCloturee(2);
            } elseif (isset($_GET['WithCritere'])) {
                if (Atexo_CurrentUser::readFromSession('criteriaCons') instanceof Atexo_Consultation_CriteriaVo) {
                    $criteriaVo = Atexo_CurrentUser::readFromSession('criteriaCons');
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                    $launchSearch = false;
                }
            } else {
                $criteriaVo->setTypeSearch('tout');
                if (isset($_GET['AS']) && '1' == $_GET['AS']) {
                    $launchSearch = false;
                    $this->rechercheAvancee->setVisible(true);
                    $this->TableauDeBord->setVisible(false);
                } else {
                    $this->rechercheAvancee->setVisible(false);
                    $this->TableauDeBord->setVisible(true);
                }
            }
            if ($launchSearch) {
                if ($this->isProfilRmaVisible && isset($_GET['rma']) && 1 == $_GET['rma']) {
                    $criteriaVo->setVisionRma(true);
                }
                $this->setViewState('CriteriaVo', $criteriaVo);
                Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                $this->dataForSearchResult($criteriaVo);
            }
            if (isset($_GET['helios'])) {
                $criteriaVo = Atexo_CurrentUser::readFromSession('criteriaCons');
                $this->dataForSearchResult($criteriaVo);
            }
            if (isset($_GET['idOperation']) && $_GET['idOperation']) {
                $criteriaVo->setIdOperation(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idOperation'])));
                $this->dataForSearchResult($criteriaVo);
            }
            if (isset($_GET['idRecherche'])) {
                $RechercheObj = (new Atexo_Entreprise_Recherches())->retreiveRecherchesByIdIdCreateur(Atexo_Util::atexoHtmlEntities($_GET['idRecherche']), Atexo_CurrentUser::getIdInscrit());
                if ($RechercheObj) {
                    $criteriaVo = (new Atexo_Entreprise_Recherches())->getCriteriaFromObject($RechercheObj);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    if (isset($_GET['search'])) {
                        $this->AdvancedSearch->displayCriteria();
                    } else {
                        $this->dataForSearchResult($criteriaVo);
                    }
                } else {
                    $this->response->redirect('?page=Entreprise.EntrepriseHome');
                }
            }
        } else {
            $criteriaVo = $this->getViewState('CriteriaVo');
            if ($criteriaVo instanceof Atexo_Consultation_CriteriaVo) {
                $this->dataForSearchResult($criteriaVo);
            }
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->TableauDeBord->setVisible(true);
        if ($criteriaVo->getVisionRma()) {
            // desactiver les onglets Elaboration et attente de validation
            $this->firstBisDiv->setVisible(false);
            $this->secondDiv->setVisible(false);
        } else {
            // desactiver les onglets Elaboration et attente de validation
            $this->firstBisDiv->setVisible(true);
            $this->secondDiv->setVisible(true);
        }
        $this->ResultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo', new Atexo_Consultation_CriteriaVo());
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;

        $nouveauStatutConsultation = $this->tabChoice($senderName);

        $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutConsultation);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    private function tabChoice($senderName)
    {
        switch ($senderName) {
            case 'firstTab':
                $this->firstDiv->setCssClass('tab-on');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE');
                break;
            case 'firstBisTab':
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab-on');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_ELABORATION');
                break;
            case 'secondTab':
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab-on');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_PREPARATION');
                break;
            case 'thirdTab':
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab-on');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_CONSULTATION');
                break;
            case 'fourthTab':
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab-on');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
                break;
            case 'fifthTab':
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab-on');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_DECISION');
                break;
            default:
                $nouveauStatutConsultation = '';
                break;
        }

        return $nouveauStatutConsultation;
    }

    /**
     * Fonctione de tri du tableau de bord.
     */
    public function Trier($sender, $param)
    {
        $this->ResultSearch->Trier($param->CommandName);
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    public function displayAvertissementTraduction()
    {
        $message = null;
        $langueOblPublication = (new Atexo_Languages())->getLanguesObligatoiresPourPublicationConsultation();
        $autreLangObligExiste = false;
        if (is_countable($langueOblPublication) ? count($langueOblPublication) : 0) {
            if (isset($_GET['id']) && ('' != $_GET['id'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $consultation = CommonConsultationPeer::retrieveByPK($this->_consultation->getId(), $connexion);
                $listeLangues = "<ul class='default-list liste-langues'>";
                foreach ($langueOblPublication as $oneLangue) {
                    if (Atexo_CurrentUser::readFromSession('lang') != $oneLangue->getLangue()) {
                        $autreLangObligExiste = true;
                        $urlTraduction = 'index.php?page=Agent.TraduireConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&langue='.$oneLangue->getLangue();
                        $listeLangues .= "<li> <a href='".$urlTraduction."' a>".Prado::localize('DEFINE_TRADUIRE_EN').' : '.$oneLangue->getLibelle().'</a></li>';
                    }
                }
                $listeLangues .= '</ul>';
                if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                    || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                    $message = '<span>'.Prado::localize('MSG_OBLIGATION_TRADUIRE_CONSULTATION', ['langues' => $listeLangues]).'</span>';
                } elseif (isset($_GET['traductionOgl'])) {
                    $autreLangObligExiste = true;
                    $message = '<span>'.Prado::localize('REPORTER_MODIFICATIONS_DANS_LANGUES', ['langues' => $listeLangues]).'</span>';
                }
            }
            $this->panelMessageAvertTraduction->setVisible($autreLangObligExiste);
            $this->panelMessageAvertTraduction->setMessage($message);
        }
    }
}
