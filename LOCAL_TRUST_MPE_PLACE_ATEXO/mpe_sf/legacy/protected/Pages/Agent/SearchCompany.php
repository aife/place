<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;

/**
 * page de recherche des entreprises.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SearchCompany extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelSearch->setPostBack($this->IsPostBack);
        $this->panelSearch->setCallBack($this->IsCallBack);
        if (!$this->IsPostBack) {
            $this->panelSearch->setVisible(true);
            $this->panelSearch->displayDomaines();
            $this->panelResultSerch->setVisible(false);
        }
        $this->panelSearch->customizeForm();
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les critères de recherches
     */
    public function dataForSearchResult(Atexo_Entreprise_CriteriaVo $criteriaVo)
    {
        $this->panelSearch->setVisible(false);
        $this->panelResultSerch->setVisible(true);
        $this->panelResultSerch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $this->panelResultSerch->Trier($param->CommandName);
    }

    /**
     * rafrechir le panel des domaines d'activités.
     */
    public function refreshComposants($sender, $param)
    {
        $this->panelSearch->qualification->panelQualification->render($param->NewWriter);
        $this->panelSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
        $this->panelSearch->agrements->panelAgrements->render($param->NewWriter);
    }
}
