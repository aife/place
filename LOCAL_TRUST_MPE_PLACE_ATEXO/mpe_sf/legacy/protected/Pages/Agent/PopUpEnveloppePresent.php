<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

class PopUpEnveloppePresent extends MpeTPage
{
    public $_consultation;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->_consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $org);
        if ($this->_consultation) {
            $offres = (new Atexo_Consultation_Responses())->retrieveEnvOff($this->_consultation, $org);
            $this->repeaterEnv->dataSource = $offres;
            $this->repeaterEnv->dataBind();
        }
    }

    public function getLibelle($param)
    {
        if (is_array($param)) {
            return Prado::localize('DEFINE_LOTS').' :'.implode(',', $param);
        } elseif ('1' == $param) {
            return Prado::localize('DEFINE_OUI');
        } else {
            return Prado::localize('DEFINE_NON');
        }
    }

    public function getTypeEnv($typeEnveloppe)
    {
        $constitutionDossierReponse = (new Atexo_Consultation_Responses())->getTypesEnveloppes($this->_consultation->getEnvCandidature(), $this->_consultation->getEnvOffre(), $this->_consultation->getEnvAnonymat(), $this->_consultation->getEnvOffreTechnique());
        if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return (new Atexo_Consultation_Responses())->enveloppeCandidatureExists($constitutionDossierReponse);
        }
        if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            return (new Atexo_Consultation_Responses())->enveloppeOffreTechniqueExists($constitutionDossierReponse);
        }
        if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            return (new Atexo_Consultation_Responses())->enveloppeOffreExists($constitutionDossierReponse);
        }
        if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            return (new Atexo_Consultation_Responses())->enveloppeAnonymatExists($constitutionDossierReponse);
        }
    }
}
