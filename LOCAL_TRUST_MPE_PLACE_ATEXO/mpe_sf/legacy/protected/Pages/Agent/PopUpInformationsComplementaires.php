<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonContrat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Contrat;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpInformationsComplementaires extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (isset($_GET['idC'])) {
                $contrat = (new Atexo_SuiviPassation_Contrat())->retrieveContratById(Atexo_Util::atexoHtmlEntities($_GET['idC']), Atexo_CurrentUser::getCurrentOrganism());
                if ($contrat instanceof CommonContrat) {
                    $this->infosComplementaires->Text = $contrat->getInformaionsComplementaires();
                }
            }
        }
    }

    public function onEnregistrerClick()
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = (new Atexo_SuiviPassation_Contrat())->retrieveContratById(Atexo_Util::atexoHtmlEntities($_GET['idC']), Atexo_CurrentUser::getCurrentOrganism());
        if ($contrat instanceof CommonContrat) {
            $contrat->setInformaionsComplementaires($this->infosComplementaires->Text);
            $contrat->save($propelConnection);
        }

        $this->js->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshInfos').click();window.close();</script>";
    }
}
