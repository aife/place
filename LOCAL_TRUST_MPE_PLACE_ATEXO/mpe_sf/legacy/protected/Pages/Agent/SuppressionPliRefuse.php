<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLot;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLotPeer;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLot;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLotPeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SuppressionPliVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SuppressionPliRefuse extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            if (Atexo_CurrentUser::hasHabilitation('SupprimerEnveloppe')) {
                $this->panelMessageErreur->setVisible(false);
                $this->panelContent->setVisible(true);
                $this->panelErreur->setVisible(false);
                $this->TemplateSuppressionPlis->fillRepeaterWithDataForSearchResult();
            } else {
                $this->panelContent->setVisible(false);
                $this->panelErreur->setVisible(true);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_SUPPRESSION_PLI'));
            }
        }
    }

    public function restaurerPli($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $suppressionPli = $param->CommandParameter;
        if ($suppressionPli instanceof Atexo_Consultation_SuppressionPliVo) {
            if ($suppressionPli->getNatureEnveloppe() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                if ($suppressionPli->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')) {
                    $enveloppe = CommonEnveloppePeer::retrieveByPK($suppressionPli->getIdEnveloppe(), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                    if ($enveloppe instanceof CommonEnveloppe) {
                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                        $enveloppe->save($connexion);
                    }
                } else {
                    $typeEnveloppe = $suppressionPli->getTypeEnveloppe();
                    $admissibilite = CommonAdmissibiliteEnveloppeLotPeer::retrieveByPK($suppressionPli->getIdOffre(), Atexo_CurrentUser::getCurrentOrganism(), $suppressionPli->getSousPli(), $typeEnveloppe, $connexion);
                    if ($admissibilite instanceof CommonAdmissibiliteEnveloppeLot) {
                        $admissibilite->setAdmissibilite(Atexo_Config::getParameter('LOT_A_TRAITER'));
                        $admissibilite->save($connexion);
                    }
                }
            } elseif ($suppressionPli->getNatureEnveloppe() == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                if ($suppressionPli->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')) {
                    $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($suppressionPli->getIdEnveloppe(), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                    if ($enveloppe instanceof CommonEnveloppePapier) {
                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                        $enveloppe->save($connexion);
                    }
                } else {
                    $typeEnveloppe = $suppressionPli->getTypeEnveloppe();
                    $admissibilite = CommonAdmissibiliteEnveloppePapierLotPeer::retrieveByPK($suppressionPli->getIdOffre(), Atexo_CurrentUser::getCurrentOrganism(), $suppressionPli->getSousPli(), $typeEnveloppe, $connexion);
                    if ($admissibilite instanceof CommonAdmissibiliteEnveloppePapierLot) {
                        $admissibilite->setAdmissibilite(Atexo_Config::getParameter('LOT_A_TRAITER'));
                        $admissibilite->save($connexion);
                    }
                }
            }
        }
        $this->TemplateSuppressionPlis->fillRepeaterWithDataForSearchResult();
    }

    public function supprimerPli($sender, $param)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $suppressionPli = $param->CommandParameter;
        if ($suppressionPli instanceof Atexo_Consultation_SuppressionPliVo) {
            if ($suppressionPli->getNatureEnveloppe() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                if ($suppressionPli->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                    $c = new Criteria();
                    $c->add(CommonEnveloppePeer::OFFRE_ID, $suppressionPli->getIdOffre());
                    $c->add(CommonEnveloppePeer::ORGANISME, $organisme);
                    $enveloppes = CommonEnveloppePeer::doSelect($c, $connexionCom);
                    $blob = new Atexo_Blob();
                    foreach ($enveloppes as $oneEnveloppe) {
                        $enveloppe = CommonEnveloppePeer::retrieveByPK($oneEnveloppe->getIdEnveloppeElectro(), $organisme, $connexionCom);
                        if ($enveloppe instanceof CommonEnveloppe) {
                            $blob->deleteBlobFile($enveloppe->getFichier(), $organisme);

                            $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'));
                            $enveloppe->setFichier('0');
                            $enveloppe->save($connexionCom);
                        }
                    }
                } else {
                    $enveloppe = CommonEnveloppePeer::retrieveByPK($suppressionPli->getIdEnveloppe(), $organisme, $connexionCom);
                    if ($enveloppe instanceof CommonEnveloppe) {
                        $blob = new Atexo_Blob();
                        $blob->deleteBlobFile($enveloppe->getFichier(), $organisme);

                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'));
                        $enveloppe->setFichier('0');
                        $enveloppe->save($connexionCom);
                    }
                }
            } elseif ($suppressionPli->getNatureEnveloppe() == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                if ($suppressionPli->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                    $c = new Criteria();
                    $c->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, $suppressionPli->getIdOffre());
                    $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
                    $enveloppes = CommonEnveloppePapierPeer::doSelect($c, $connexionCom);
                    foreach ($enveloppes as $oneEnveloppe) {
                        $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($organisme, $oneEnveloppe->getIdEnveloppePapier(), $connexionCom);
                        if ($enveloppe instanceof CommonEnveloppePapier) {
                            $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'));
                            $enveloppe->save($connexionCom);
                        }
                    }
                } else {
                    $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($suppressionPli->getIdEnveloppe(), $organisme, $connexionCom);
                    if ($enveloppe instanceof CommonEnveloppePapier) {
                        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'));
                        $enveloppe->save($connexionCom);
                    }
                }
            }
        }
        $this->TemplateSuppressionPlis->fillRepeaterWithDataForSearchResult();
    }

    public function refresfRepeater($sender, $param)
    {
        $this->TemplateSuppressionPlis->fillRepeaterWithDataForSearchResult($this->TemplateSuppressionPlis->tableauDeBordRepeater->getCurrentPageIndex());
        $this->TemplateSuppressionPlis->panelRefresh->render($param->NewWriter);
    }
}
