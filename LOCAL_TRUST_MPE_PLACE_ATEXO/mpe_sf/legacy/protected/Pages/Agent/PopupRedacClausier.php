<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-4.0
 */
class PopupRedacClausier extends MpeTPage
{
    private ?string $url = null;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $codePage = Atexo_Util::atexoHtmlEntities($_GET['cp']);
        if ($arrayDonnes = $this->getPageNameRedirect($codePage)) {
            $this->scriptJs->Text = '<script>showLoader();</script>';
            $pageName = $arrayDonnes['0'];
            $this->gotoRedaction($pageName);
        }
    }

    public function getPageNameRedirect($codePage)
    {
        $arrayPage = [
                     'RechercherClauseEditeur' => ['RechercherClauseEditeurInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('RECHERCHER')],
                     'CreationClauseEditeur' => ['CreationClauseEditeurInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('DEFINE_TEXT_CREER')],
                     'exportClauseEditeurAction' => ['exporterClause.htm?editeur=yes', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('TEXT_SOUS_MENU_EXPORTER')],
                     'RechercherCanevasEditeur' => ['RechercherCanevasEditeurInit.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('RECHERCHER')],
                     'CreationCanevasEditeur' => ['CreationCanevas1EditeurInit.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('DEFINE_TEXT_CREER')],
                     'PublicationClausier' => ['createPublicationClausier.htm', Prado::localize('TEXT_MENU_VERSION_CLAUSIER'), Prado::localize('TEXT_MENU_CREER_VERSION')],
                     'HistoriqueClausier' => ['listPublicationsClausier.htm', Prado::localize('TEXT_MENU_VERSION_CLAUSIER'), Prado::localize('TEXT_MENU_HISTORIQUES_VERSION')],
                     'RechercherClause' => ['RechercherClauseInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('RECHERCHER')],
                     'CreationClause' => ['CreationClauseInit.epm', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('DEFINE_TEXT_CREER')],
                     'exportClauseAction' => ['exporterClause.htm?editeur=no', Prado::localize('TEXT_MENU_CLAUSES'), Prado::localize('TEXT_SOUS_MENU_EXPORTER')],
                     'RechercherCanevas' => ['RechercherCanevasInit.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('RECHERCHER')],
                     'CreationCanevas' => ['CreationCanevas1Init.epm', Prado::localize('TEXT_MENU_CANEVAS'), Prado::localize('DEFINE_TEXT_CREER')],
                     'ParametrageClause' => ['ParametrageClauseInit.epm?preferences=direction', Prado::localize('TEXT_MENU_CLAUSE_PERSONNALISEES'), Prado::localize('TEXT_MENU_CLAUSE_ENTITE_ACHAT')],
                     'ParametrageClauseAgent' => ['ParametrageClauseInit.epm?preferences=agent', Prado::localize('TEXT_MENU_CLAUSE_PERSONNALISEES'), Prado::localize('TEXT_MENU_MES_CLAUSES')],
        ];

        foreach ($arrayPage as $key => $value) {
            if ($key == $codePage) {
                return $value;
            }
        }

        return false;
    }

    public function gotoRedaction($pageName)
    {
        $messageErreur = null;
        try {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
            $redaction = new Atexo_Redaction('', $agent);
            $messageErreur = '';
            if ($redaction->ticket) {
                $ticket = $redaction->initialiseContext();
                if ($ticket) {
                    $chaine = '?';
                    if (str_contains($pageName, '?')) {
                        $chaine = '&';
                    }
                    $urlReditect = Atexo_Config::getParameter('URL_RSEM_REDACTION').$pageName.$chaine.'identifiantSaaS=';
                    $urlReditect .= $ticket;
                    $this->url = $urlReditect;

                    return;
                }
                $messageErreur = Prado::localize('DEFINE_ERREUR_REDAC');
            } else {
                if ($redaction->codeErreur && $redaction->messageErreur) {
                    $messageErreur = 'Code erreur '.$redaction->codeErreur.' : '.$redaction->messageErreur;
                } else {
                    $messageErreur = Prado::localize('DEFINE_ERREUR_REDAC');
                }
            }

            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->frameRedac->Visible = false;
        } catch (Exception) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_ERREUR_REDAC'));
            $this->frameRedac->Visible = false;
        }
        if ($messageErreur) {
            $this->scriptJs->Text = '<script>hideLoader();</script>';
        }
    }

    public function getUrl()
    {
        return $this->url;
    }
}
