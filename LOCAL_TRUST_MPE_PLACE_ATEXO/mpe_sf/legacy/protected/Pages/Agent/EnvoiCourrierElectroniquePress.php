<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class EnvoiCourrierElectroniquePress extends MpeTPage
{
    protected $_typeMsg;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        if (isset($_GET['organisme'])) {
            $org = Atexo_Util::atexoHtmlEntities($_GET['organisme']);
        } else {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        if (isset($_GET['noSelectDest'])) {
            $this->PanelMessageErreur->setVisible(true);
            $this->PanelMessageErreur->setMessage(Prado::localize('TEXT_VEUILLEZ_SELECTIONNER_ORGANE_PRESSE'));
        }
        if (isset($_GET['noSelectFact'])) {
            $this->PanelMessageErreur->setVisible(true);
            $this->PanelMessageErreur->setMessage(Prado::localize('TEXT_VEUILLEZ_SELECTIONNER_ADRESSE_FACTURATION'));
        }
        $this->TemplateEnvoiCourrierElectroniquePress->setOrg($org);
        $this->TemplateEnvoiCourrierElectroniquePress->setRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectroniquePress->courrierElectroniqueSimple->Checked = true;
        $this->TemplateEnvoiCourrierElectroniquePress->setPostBack($this->IsPostBack);
    }
}
