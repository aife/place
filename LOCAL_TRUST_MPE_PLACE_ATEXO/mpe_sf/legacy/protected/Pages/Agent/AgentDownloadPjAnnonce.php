<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;

/**
 * Permet de télécharger le Pj.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentDownloadPjAnnonce extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_idFichier = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
        $pj = (new Atexo_Publicite_AnnonceJAL())->retreiveFormulairePJByIdPiece($this->_idFichier);
        $this->_nomFichier = $pj->getNomFichier();
        $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_CurrentUser::getOrganismAcronym());
    }
}
