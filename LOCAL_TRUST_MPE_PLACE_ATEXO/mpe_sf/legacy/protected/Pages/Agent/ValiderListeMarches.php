<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Marche\Atexo_Marche_Annee;
use Application\Service\Atexo\Marche\Atexo_Marche_CriteriaVo;

/**
 * Page de suivi de la liste des marchés - Article 133.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ValiderListeMarches extends MpeTPage
{
    public $_connexionOrg;
    public $_annee;
    public $_idServiceAgent;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->etapeChoixAnnee();
            (new Atexo_Marche_Annee())->displayAnnee('etapeChoixAnneePanel', 'etapeResultatsPanel', 'anneeVisibles');
        }
    }

    public function back()
    {
        (new Atexo_Marche_Annee())->displayAnnee('etapeChoixAnneePanel', 'etapeResultatsPanel', 'anneeVisibles');
    }

    public function getConnexionOrg()
    {
        if (null == $this->_connexionOrg) {
            $this->_connexionOrg = Propel::getConnection(Atexo_CurrentUser::getCurrentOrganism());
        }

        return $this->_connexionOrg;
    }

    public function getAnnee()
    {
        if (null == $this->_annee) {
            $this->_annee = $this->getViewState('annee');
        }

        return $this->_annee;
    }

    public function redirectTo($sender, $param)
    {
        $url = $param->CommandName;
        $this->response->redirect($url);
    }

    public function etapeChoixAnnee()
    {
        $this->etapeChoixAnneePanel->setVisible(true);
        $this->etapeResultatsPanel->setVisible(false);

        $listeAnneesVisibles = MarcheAnneePeer::getAnneeVisibles($this->getConnexionOrg());
        $this->anneeVisibles->setDataSource($listeAnneesVisibles);
        $this->anneeVisibles->dataBind();
    }

    public function etapeResultats()
    {
        // On sauvegarde l'annee selectionnée
        $this->setViewState('annee', $this->anneeVisibles->getSelectedValue());
        $this->etapeChoixAnneePanel->setVisible(false);
        $this->etapeResultatsPanel->setVisible(true);
        $this->afficherResultats();
    }

    public function pageChanged($sender, $param)
    {
        $this->repeaterMarches->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->populateData($criteriaVo);
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->repeaterMarches->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterMarches->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterMarches->PageSize);
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->repeaterMarches->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    private function refreshPageInfo()
    {
        $criteriaVo = new Atexo_Marche_CriteriaVo();
        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setNumeroMarcheAnnee($this->getAnnee());
        $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setSortByElement('natureMarche');

        $arrayMarches = (new Atexo_Marche())->search($criteriaVo);
        $nombreElement = is_countable($arrayMarches) ? count($arrayMarches) : 0;

        if ($nombreElement >= 1) {
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterMarches->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterMarches->PageSize);
            $this->repeaterMarches->setVirtualItemCount($nombreElement);
            $this->repeaterMarches->setCurrentPageIndex(0);

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nbrResultats->Text = $nombreElement;
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    private function populateData(Atexo_Marche_CriteriaVo $criteriaVo)
    {
        $offset = $this->repeaterMarches->CurrentPageIndex * $this->repeaterMarches->PageSize;
        $limit = $this->repeaterMarches->PageSize;
        if ($offset + $limit > $this->repeaterMarches->getVirtualItemCount()) {
            $limit = $this->repeaterMarches->getVirtualItemCount() - $offset;
        }
        $atexoMarche = new Atexo_Marche();

        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);

        $arrayMarches = $atexoMarche->search($criteriaVo);
        $this->repeaterMarches->DataSource = $arrayMarches;
        $this->repeaterMarches->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterMarches->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('criteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->repeaterMarches->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->repeaterMarches->CurrentPageIndex + 1;
        }
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->getCommandName();
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('criteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('criteriaVo', $criteriaVo);
        $this->repeaterMarches->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    private function afficherResultats()
    {
        $criteriaVo = new Atexo_Marche_CriteriaVo();
        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());

        $criteriaVo->setNumeroMarcheAnnee($this->getAnnee());
        $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setSortByElement('default');

        $this->setViewState('criteriaVo', $criteriaVo);
        $this->refreshPageInfo();
        $this->populateData($criteriaVo);
    }

    public function refreshRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->repeaterMarches->setCurrentPageIndex(0);
        $this->refreshPageInfo();
        $this->populateData($criteriaVo);
    }

    public function checkAll($sender, $param)
    {
        foreach ($this->repeaterMarches->Items as $itemMarche) {
            $itemMarche->toUpdate->checked = $sender->checked;
        }
    }

    public function validerMarches($sender, $param)
    {
        if ('valider' == $param->getCommandName()) {
            $valide = '1';
        } else {
            $valide = '0';
        }
        foreach ($this->repeaterMarches->Items as $itemMarche) {
            if ($itemMarche->toUpdate->checked) {
                $marche = CommonMarchePeer::retrieveByPK($itemMarche->idMarche->Value, $this->getConnexionOrg());
                $marche->setValide($valide);
                $marche->save($this->getConnexionOrg());
            }
        }

        $this->refreshRepeater($sender, $param);
    }
}
