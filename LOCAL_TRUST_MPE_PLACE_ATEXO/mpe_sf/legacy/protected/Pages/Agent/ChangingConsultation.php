<?php

namespace Application\Pages\Agent;

use App\Event\ModificationConsultationEvent;
use App\Event\ModificationDceEvent;
use App\Utils\Encryption;
use Application\Controls\AtexoDCE;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonComplement;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDATEFIN;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonHistoriquesConsultation;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Propel\Mpe\CommonTListeLotsCandidaturePeer;
use Application\Propel\Mpe\CommonVisiteLieux;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\DossierVolumineux\Atexo_DossierVolumineux_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Exception\ExceptionDceLimit;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Consultation;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPdf;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de modification d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ChangingConsultation extends MpeTPage
{
    private $_connexionCom = '';
    protected $_consultation = '';
    protected $_dumeContexte = '';
    public $_dce = '';
    private $_idMesssage = '';
    public $_consultationChanged = false;
    public $_activationFuseauHoraire = false;
    public $donneeComplementaire;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());

        $this->panelSendMessage->setVisible(false);
        if($this->isSendMessage()) {
            $this->panelSendMessage->setVisible(true);
        }
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultationChanged($v)
    {
        $this->_consultationChanged = $v;
    }

    public function getConsultationChanged()
    {
        return $this->_consultationChanged;
    }

    public function getDonneeComplementaire()
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->donneeComplementaire = $donneeComplementaire;
    }

    public function onLoad($param)
    {
        $this->customizeForm();
        $_GET['xml'] = Atexo_CurrentUser::readFromSession('xml');
        Atexo_CurrentUser::deleteFromSession('xml');

        $this->panelMessageErreur->setVisible(false);
        $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        $this->blocDce->setConsultation($this->_consultation[0]);
        $this->dceComposant->setConsultation($this->_consultation[0]);
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->_consultation[0]->getIdTypeProcedureOrg());
        if ($this->_consultation) {
            //Début vérification de l'activation du fuseau horaire
            $this->_activationFuseauHoraire = (new Atexo_EntityPurchase())->getEtatActivationFuseauHoraire(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
            //Fin vérification de l'activation du fuseau horaire
            if (!Atexo_Module::isEnabled('AutoriserModificationApresPhaseConsultation')
            && (
                $this->_consultation[0]->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
            || $this->_consultation[0]->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
            )
            ) {
                echo prado::localize('TEXT_ACCEDER_CETTE_PAGE');
                exit;
            }
            if (1 == (is_countable($this->_consultation) ? count($this->_consultation) : 0) && $this->_consultation[0] instanceof CommonConsultation) {
                $this->_consultation = array_shift($this->_consultation);
            } else {
                $this->_consultation = $this->getConsultation();
            }

            $this->donneeComplementaire = $this->_consultation->getDonneComplementaire();
            $this->idConsultationSummary->setConsultation($this->_consultation);
            //Gestion de l'accès à la page
            if (($this->_consultation instanceof CommonConsultation) && !(new Atexo_Consultation())->hasHabilitationModifCons($this->_consultation)) {
                $this->PanelNotAuthorizedToAccessThePage->setVisible(true);
                $this->PanelAuthorizedToAccessThePage->setVisible(false);
                $this->PanelNotAuthorizedToAccessThePage->setMessage(Prado::localize('DEFINE_MESSAGE_VOUS_N_ETES_PAS_HABILITE_MODIFIDIER_CONSULTATION'));

                return;
            } else {
                $this->PanelNotAuthorizedToAccessThePage->setVisible(false);
                $this->PanelAuthorizedToAccessThePage->setVisible(true);
            }

            if ($this->modifiDateRemisePlis->Checked) {
                $this->divDateRemisePlis->Attributes->Style = 'display:block';
                if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->_activationFuseauHoraire) {
                    $this->divDateRemisePlisLocale->Attributes->Style = 'display:block';
                }
            }

            if ($procEquivalence instanceof CommonProcedureEquivalence && ('+1' === $procEquivalence->getDelaiDateLimiteRemisePli() || '+0' === $procEquivalence->getDelaiDateLimiteRemisePli() || '0' === $procEquivalence->getDelaiDateLimiteRemisePli() || '1' === $procEquivalence->getDelaiDateLimiteRemisePli())) {
                $this->divModifAffichage->Attributes->Style = 'display:block';
            } else {
                $this->divModifAffichage->Attributes->Style = 'display:none';
            }
            if ($this->modifAffichage->Checked) {
                $this->divInterrompreAffichage->Attributes->Style = 'display:block';
            }

            if (Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables')
            && $procEquivalence instanceof CommonProcedureEquivalence && ('+1' === $procEquivalence->getAutrePieceCons()
            || '+0' === $procEquivalence->getAutrePieceCons()
            || '0' === $procEquivalence->getAutrePieceCons()
            || '1' === $procEquivalence->getAutrePieceCons())) {
                $this->AutrePieceCons->Attributes->Style = 'display:block';
            } else {
                $this->AutrePieceCons->Attributes->Style = 'display:none';
            }

            if ($this->piecesConsultation->Checked) {
                $this->divpiecesConsultation->Attributes->Style = 'display:block';
            }
            $this->_dce = (new Atexo_Consultation_Dce())->getDce($this->_consultation->getId(), Atexo_CurrentUser::getOrganismAcronym());
            if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                $this->dceComposant->_consultation = $this->_consultation;
                $this->dceComposant->PanelDownloadDce->style = "display:''";
                //RC
                if (Atexo_Module::isEnabled('ReglementConsultation') && $procEquivalence && '-0' === $procEquivalence->getReglementCons()) {
                    $this->dceComposant->panelRc->style = 'display:none';
                }
            } else {
                $this->blocDce->_consultation = $this->_consultation;
                $this->blocDce->partialDownload->Value = '0';
            }
            if (!$this->IsPostBack) {
                $nombreJour = [];
                $valeursPoursuivreAffichage = json_decode(str_replace("'", '"', Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE')), null, 512, JSON_THROW_ON_ERROR);
                foreach ($valeursPoursuivreAffichage as $valeur) {
                    $nombreJour[$valeur[0].$valeur[1]] = $valeur[0].' '.Prado::localize($valeur[1].($valeur[0] > 1 ? 'S' : ''));
                }
                $this->poursuivreAffichageNbJour->Datasource = $nombreJour;
                $this->poursuivreAffichageNbJour->dataBind();
                //Début remplissate de la date limite de remise des plis locale
                if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->_activationFuseauHoraire) {
                    $lieuResidence = '';
                    $decalageFuseauHoraireService = '';
                    $idService = Atexo_CurrentUser::getCurrentServiceId();
                    //Cas de l'organisme
                    if (empty($idService)) {
                        $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                        if ($objetOrganisme instanceof CommonOrganisme) {
                            $lieuResidence = $objetOrganisme->getLieuResidence();
                            $decalageFuseauHoraireService = $objetOrganisme->getDecalageHoraire();
                        }
                    } else {
                        $serviceAgentConnecte = (new Atexo_EntityPurchase())->getEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                        if ($serviceAgentConnecte instanceof CommonService) {
                            $lieuResidence = $serviceAgentConnecte->getLieuResidence();
                            $decalageFuseauHoraireService = $serviceAgentConnecte->getDecalageHoraire();
                        }
                    }
                    $this->lieuResidence->Text = $lieuResidence;
                    $this->decalageFuseauHoraireService->value = $decalageFuseauHoraireService;
                }
                //Fin remplissate de la date limite de remise des plis locale
                if (isset($_GET['xml']) && !empty($_GET['xml'])) {
                    $interfaceConsultation = new Atexo_Interfaces_Consultation();
                    $dateFin = $interfaceConsultation->getDateClotureFromXml(html_entity_decode(base64_decode($_GET['xml'])));
                    $this->dateRemisePlis->Text = Atexo_Util::iso2frnDateTime(str_replace('T', ' ', $dateFin));
                    $this->modifiDateRemisePlis->Checked = true;
                    $javascript = "<script>toggleMenu('".$this->divDateRemisePlis->getClientId()."');</script>";
                    $this->javascript->Text = $javascript;
                } else {
                    $this->dateRemisePlis->Text = Atexo_Util::iso2frnDateTime(substr($this->_consultation->getDatefin(), 0, 16));
                    if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->_activationFuseauHoraire) {
                        $this->dateRemisePlisLocale->Text = self::getDateRemisePlisLocale($this->_consultation);
                        $this->lieuResidence->Text = $this->_consultation->getLieuResidence();
                    }
                    $this->datemiseEnLigne->value = Atexo_Util::iso2frnDateTime(substr($this->_consultation->getDatemiseenligne(), 0, 16));
                }

                if ('0' != $this->_consultation->getPoursuivreAffichage()) {
                    $this->poursuivreAffichage->Checked = true;
                    $this->interrompreAffichage->Checked = false;
                    $this->poursuivreAffichageNbJour->SelectedValue = $this->_consultation->getPoursuivreAffichage().$this->_consultation->getPoursuivreAffichageUnite();
                } else {
                    $this->poursuivreAffichage->Checked = false;
                    $this->interrompreAffichage->Checked = true;
                }

                $this->displayConsultation();
            }
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_CONSULTER_DETAIL_CONSULTATION')); // Erreur  modification du DCE
        }

        $c = new Criteria();
        $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $this->_consultation->getId());
        $c->add(CommonTDumeContextePeer::ORGANISME, $this->_consultation->getOrganisme());
        $c->add(CommonTDumeContextePeer::STATUS, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'));
        $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
        $this->_dumeContexte = CommonTDumeContextePeer::doSelectOne($c, $this->_connexionCom);

        if (Atexo_Module::isEnabled('InterfaceDume') && $this->_dumeContexte) {
            $this->bloc_etapeDumeAcheteur->chargerDumeAcheteur($this->_consultation);
        } else {
            $c = new Criteria();
            $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $this->_consultation->getId());
            $c->add(CommonTDumeContextePeer::ORGANISME, $this->_consultation->getOrganisme());
            $c->add(CommonTDumeContextePeer::STATUS, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'));
            $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
            $this->_dumeContexte = CommonTDumeContextePeer::doSelectOne($c, $this->_connexionCom);

            $this->bloc_etapeDumeAcheteur->lt_dume->setDisplay('None');
            $this->bloc_etapeDumeAcheteur->panelBlocErreurModifiedDume->setDisplay('Dynamic');
        }
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }

    /**
     * Permet de modifier un objet consultation.
     */
    public function updateConsultation($sender, $param)
    {
        $msg = null;
        $commonLots = null;
        $visiteLieuChanged = null;
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $dceIdBlob = 0;
        $this->setConsultationChanged(false);
        $atexoBlob = new Atexo_Blob();
        $atexoCrypto = new Atexo_Crypto();
        $modifiedDlro = 0;

        $commonConsultation = $this->blocDce->getConsultation();
        if ($commonConsultation->getIdDonneeComplementaire()) {
            $this->donneeComplementaire = $commonConsultation->getDonneComplementaire();
        } else {
            $this->donneeComplementaire = new CommonTDonneeComplementaire();
        }
        $this->idReferentielCheck->saveConsultationAndLot($commonConsultation->getId(), 0);
        $this->idReferentielCheckLot->saveRefLot($commonConsultation->getId(), null);
        //Enregistrer les referentiels de type Radio
        $this->idReferentielRadio->saveConsultationAndLot($commonConsultation->getId(), 0);
        $this->idReferentielRadioLot->saveRefLot($commonConsultation->getId());
        //Retrait
        $retraitDossiersChanged = false;
        $retraitDossiers = $this->retraitDossiers->Text;
        if ($retraitDossiers && true == $this->retraitDossierCheck->Checked && $retraitDossiers != $commonConsultation->getAdresseRetraisDossiers()) {
            $this->setConsultationChanged(true);
            $retraitDossiersChanged = true;
            $commonConsultation->setAdresseRetraisDossiers($retraitDossiers);
        }
        //Adresse Depot
        $addDepotDossiersChanged = false;
        $addDepotDossiers = $this->addDepotDossiers->Text;
        if ($addDepotDossiers && true == $this->depotOffresCheck->Checked && $addDepotDossiers != $commonConsultation->getAdresseDepotOffres()) {
            $this->setConsultationChanged(true);
            $addDepotDossiersChanged = true;
            $commonConsultation->setAdresseDepotOffres($addDepotDossiers);
        }
        //Lieu Ouverture
        $lieuOuverturePlisChanged = false;
        $lieuOuverturePlis = $this->lieuOuverturePlis->Text;
        if ($lieuOuverturePlis && true == $this->ouverturePlis->Checked && $lieuOuverturePlis != $commonConsultation->getLieuOuverturePlis()) {
            $this->setConsultationChanged(true);
            $lieuOuverturePlisChanged = true;
            $commonConsultation->setLieuOuverturePlis($lieuOuverturePlis, $langue);
        }
        // Prix aquisition
        $prixAquisitaionPlansCahnged = false;
        $prixAquisitaionPlans = $this->prixAquisitaionPlans->Text;
        if (($prixAquisitaionPlans || '0' == $prixAquisitaionPlans) && true == $this->prixAquisitaionPlansCheck->Checked) {
            $prixAquisitaionPlans = str_replace(',', '.', $prixAquisitaionPlans);
            if ($prixAquisitaionPlans != $this->donneeComplementaire->getPrixAquisitionPlans()) {
                $this->setConsultationChanged(true);
                $prixAquisitaionPlansCahnged = true;
                $this->donneeComplementaire->setPrixAquisitionPlans($prixAquisitaionPlans);
            }
        }
        // Pieces Dossier Admin
        $piecesDossierAdminCahnged = false;
        $piecesDossierAdmin = $this->piecesDossierAdmin->Text;
        if ($piecesDossierAdmin && true == $this->moduleAutrePieceCheck->Checked) {
            if ($piecesDossierAdmin != $this->donneeComplementaire->getPiecesDossierAdmin()) {
                $this->setConsultationChanged(true);
                $piecesDossierAdminCahnged = true;
                $this->donneeComplementaire->setPiecesDossierAdmin($piecesDossierAdmin);
            }
        }
        // Pieces Dossier Tech
        $piecesDossierTechCahnged = false;
        $piecesDossierTech = $this->piecesDossierTech->Text;
        if ($piecesDossierTech && true == $this->moduleAutrePieceCheck->Checked && $piecesDossierTech != $this->donneeComplementaire->getPiecesDossierTech()) {
            $this->setConsultationChanged(true);
            $piecesDossierTechCahnged = true;
            $this->donneeComplementaire->setPiecesDossierTech($piecesDossierTech);
        }
        if (!$this->isConsultationAllotie()) {
            // cautionProvisoire
            $cautionProvisoire = $this->cautionProvisoire->Text;
            $cautionProvisoireChanged = false;
            if (($cautionProvisoire || '0' == $cautionProvisoire) && $this->cautionProvisoireCheck->Checked) {
                $cautionProvisoire = str_replace(',', '.', $cautionProvisoire);
                if ($cautionProvisoire != $this->donneeComplementaire->getCautionProvisoire()) {
                    $this->setConsultationChanged(true);
                    $cautionProvisoireChanged = true;
                    $this->donneeComplementaire->setCautionProvisoire($cautionProvisoire);
                }
            }
            // Qualification
            $qualification = base64_decode($this->qualification->idsQualification->Text);
            $qualificationChanged = false;
            if ($this->qualificationCheck->Checked && $qualification != $commonConsultation->getQualification()) {
                $this->setConsultationChanged(true);
                $qualificationChanged = true;
                $commonConsultation->setQualification($qualification);
            }
            // Agrements
            $agrements = $this->agrements->idsSelectedAgrements->Value;
            $agrementsChanged = false;
            if ($this->agrementsCheck->Checked && $agrements != $commonConsultation->getAgrements()) {
                $this->setConsultationChanged(true);
                $agrementsChanged = true;
                $commonConsultation->setAgrements($agrements);
            }
            // Echantillion
            $addEchantillionChanged = false;
            $dateLimiteEchantillionChanged = false;
            $addEchantillion = $this->addEchantillion->Text;
            $dateLimiteEchantillion = Atexo_Util::frnDateTime2iso($this->dateLimiteEchantillion->Text);
            if ($this->echantillion->Checked) {
                if ($this->echantillonOui->Checked) {
                    $this->donneeComplementaire->setEchantillon(1);
                    if ($addEchantillion != $this->donneeComplementaire->getAddechantillon()) {
                        $this->setConsultationChanged(true);
                        $addEchantillionChanged = true;
                        $this->donneeComplementaire->setAddechantillon($addEchantillion);
                    }
                    if ($dateLimiteEchantillion != $this->donneeComplementaire->getDatelimiteechantillon()) {
                        $this->setConsultationChanged(true);
                        $dateLimiteEchantillionChanged = true;
                        $this->donneeComplementaire->setDatelimiteechantillon(($dateLimiteEchantillion));
                    }
                } elseif ($this->echantillonNon->Checked) {
                    $this->donneeComplementaire->setEchantillon(0);
                    $this->donneeComplementaire->setDatelimiteechantillon('');
                    $this->donneeComplementaire->setAddechantillon('');
                }
            }
            // Réunion
            $addReunionChanged = false;
            $dateReunionChanged = false;
            $addReunion = $this->addReunion->Text;
            $dateReunion = Atexo_Util::frnDateTime2iso($this->dateReunion->Text);
            if ($this->reunion->Checked) {
                if ($this->reunionOui->Checked) {
                    $this->donneeComplementaire->setReunion('1');
                    if ($addReunion != $this->donneeComplementaire->getAddreunion()) {
                        $this->setConsultationChanged(true);
                        $addReunionChanged = true;
                        $this->donneeComplementaire->setAddreunion($addReunion);
                    }
                    if ($dateReunion != $this->donneeComplementaire->getDatereunion()) {
                        $this->setConsultationChanged(true);
                        $dateReunionChanged = true;
                        $this->donneeComplementaire->setDatereunion(($dateReunion));
                    }
                } else {
                    $this->donneeComplementaire->setReunion('0');
                    $this->donneeComplementaire->setDatereunion('');
                    $this->donneeComplementaire->setAddreunion('');
                }
            }
            // Visites
            if ($this->visite->Checked) {
                // AJout des visites des lieux à la consultation s'il n'est pas alloti
                // // je supprime les visites d'une consultation s'elles exisent avant de sauver les nouveaux
                $consultationId = $this->_consultation->getId();
                $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultationId, '0', Atexo_CurrentUser::getCurrentOrganism());
                if ((is_countable($visitesLieux) ? count($visitesLieux) : 0) > 0) {
                    foreach ($visitesLieux as $oneVisite) {
                        (new Atexo_Consultation())->deleteVisiteLieuById($oneVisite->getId(), Atexo_CurrentUser::getCurrentOrganism());
                    }
                }
                $visite = '0';
                if ($this->visiteLotOui->Checked) {
                    // Ajout des visites des lieux
                    foreach ($this->visiteRepeater->Items as $oneItem) {
                        $visiteLieu = new CommonVisiteLieux();
                        $visiteLieu->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                        $visiteLieu->setAdresse($oneItem->adresseValue->Value);
                        $visiteLieu->setDate($oneItem->dateValue->Value);
                        $oneItem->visiteLieuxLang->setChamps('adresse');
                        $oneItem->visiteLieuxLang->setLanguesTraduction($visiteLieu);
                        $visiteLieu->setLot('0');
                        $commonConsultation->addCommonVisiteLieux($visiteLieu);
                        $visite = '1';
                    }
                }
                $this->donneeComplementaire->setVisitesLieux($visite);
                $this->setConsultationChanged(true);
                $visiteLieuChanged = true;
            }
        } else {
            $commonLots = $commonConsultation->getAllLots();
            $qualificationChanged = false;
            $cautionProvisoireChanged = false;
            $agrementsChanged = false;
            $addEchantillionChanged = false;
            $dateLimiteEchantillionChanged = false;
            $addReunionChanged = false;
            $dateReunionChanged = false;
            foreach ($commonLots as $oneLot) {
                $donnesComplementaireChanged = false;
                $donnesComplementaire = $oneLot->getDonneComplementaire();
                if (!$donnesComplementaire instanceof CommonTDonneeComplementaire) {
                    $donnesComplementaire = new CommonTDonneeComplementaire();
                }
                if ($this->cautionProvisoireCheck->Checked) {
                    foreach ($this->cautionProvisoireRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $cautionProvisoire = $oneItem->cautionProvisoire->getText();
                            if ($cautionProvisoire != $donnesComplementaire->getCautionProvisoire()) {
                                $this->setConsultationChanged(true);
                                $cautionProvisoireChanged = true;
                                $donnesComplementaire->setCautionProvisoire($cautionProvisoire);
                                $donnesComplementaireChanged = true;
                            }
                        }
                    }
                }

                if ($this->qualificationCheck->Checked) {
                    foreach ($this->qualificationRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $qualification = base64_decode($oneItem->qualification->idsQualification->getText());
                            if ($qualification != $oneLot->getQualification()) {
                                $this->setConsultationChanged(true);
                                $qualificationChanged = true;
                                $oneLot->setQualification($qualification);
                            }
                        }
                    }
                }
                if ($this->agrementsCheck->Checked) {
                    foreach ($this->agrementsRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $agrements = $oneItem->agrements->idsSelectedAgrements->getValue();
                            if ($agrements != $oneLot->getAgrements()) {
                                $this->setConsultationChanged(true);
                                $agrementsChanged = true;
                                $oneLot->setAgrements($agrements);
                            }
                        }
                    }
                }
                //$echantillon = "0";
                if ($this->echantillion->Checked) {
                    foreach ($this->echantillonLotRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $echantillon = $oneItem->echantillonOuiLot->checked;
                            $addEchantillion = $oneItem->addEchantillion->getText();
                            $dateLimiteEchantillion = Atexo_Util::frnDateTime2iso($oneItem->dateLimiteEchantillion->getText());

                            if ($echantillon) {
                                if (($addEchantillion != $donnesComplementaire->getAddEchantillon())
                                || ($dateLimiteEchantillion != $donnesComplementaire->getDatelimiteechantillon())) {
                                    $this->setConsultationChanged(true);
                                    $dateLimiteEchantillionChanged = true;
                                    $addEchantillionChanged = true;
                                    $echantillon = '1';
                                    $donnesComplementaire->setEchantillon($echantillon);
                                    $donnesComplementaire->setAddEchantillon($addEchantillion);
                                    $donnesComplementaire->setDatelimiteechantillon($dateLimiteEchantillion);
                                    $donnesComplementaireChanged = true;
                                }
                            } else {
                                $donnesComplementaireChanged = true;
                                $donnesComplementaire->setEchantillon('0');
                                $donnesComplementaire->setAddEchantillon('');
                                $donnesComplementaire->setDatelimiteechantillon('0000-00-00 00:00:00');
                            }
                        }
                    }
                }
                $reunion = '0';
                if ($this->reunion->Checked) {
                    foreach ($this->reunionLotRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $reunion = $oneItem->reunionOuiLot->checked;
                            if ($reunion) {
                                $addReunion = $oneItem->addReunion->getText();
                                $dateReunion = Atexo_Util::frnDateTime2iso($oneItem->dateReunion->getText());
                                if (($addReunion != $donnesComplementaire->getReunion()) || ($dateReunion != $donnesComplementaire->getDateReunion())) {
                                    $this->setConsultationChanged(true);
                                    $dateReunionChanged = true;
                                    $addReunionChanged = true;
                                    $reunion = '1';
                                    $donnesComplementaire->setReunion($reunion);
                                    $donnesComplementaire->setAddReunion($addReunion);
                                    $donnesComplementaire->setDateReunion($dateReunion);
                                    $donnesComplementaireChanged = true;
                                }
                            } else {
                                $donnesComplementaire->setReunion('0');
                                $donnesComplementaire->setAddReunion('');
                                $donnesComplementaire->setDateReunion('0000-00-00 00:00:00');
                                $donnesComplementaireChanged = true;
                            }
                        }
                    }
                }
                //$visite = "0";
                if ($this->visite->Checked) {
                    //$visite = "1";
                    foreach ($this->visiteLotRepeater->getItems() as $oneItem) {
                        if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                            $consultationId = $oneLot->getConsultationId();
                            $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultationId, $oneLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
                            if ((is_countable($visitesLieux) ? count($visitesLieux) : 0) > 0) {
                                foreach ($visitesLieux as $oneVisite) {
                                    (new Atexo_Consultation())->deleteVisiteLieuById($oneVisite->getId(), Atexo_CurrentUser::getCurrentOrganism());
                                }
                            }
                            $visite = '0';
                            if ($oneItem->visiteLotOui->Checked) {
                                foreach ($oneItem->visiteRepeater->Items as $Item) {
                                    $visiteLieu = new CommonVisiteLieux();
                                    $visiteLieu->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                                    $visiteLieu->setAdresse($Item->adresseValue->Value);
                                    $visiteLieu->setDate(($Item->dateValue->Value));
                                    $visiteLieu->setLot($oneLot->getLot());
                                    $Item->visiteLieuxLotLang->setChamps('adresse');
                                    $Item->visiteLieuxLotLang->setLanguesTraduction($visiteLieu);
                                    $commonConsultation->addCommonVisiteLieux($visiteLieu);
                                    $this->setConsultationChanged(true);
                                    $visiteLieuChanged = true;
                                    $visite = '1';
                                }
                            }
                            $donnesComplementaire->setVisitesLieux($visite);
                            $donnesComplementaireChanged = true;
                        }
                    }
                }

                if ($donnesComplementaireChanged) {
                    $donnesComplementaire->save($this->_connexionCom);
                    if ($donnesComplementaire->getIdDonneeComplementaire()) {
                        $oneLot->setIdDonneeComplementaire($donnesComplementaire->getIdDonneeComplementaire());
                    }
                }
            }
        }
        // Autres Pieces
        if (true == $this->moduleAutrePieceCheck->Checked) {
            $piecesDossierAdditif = $this->piecesDossierAdditif->Text;
            $commonConsultation->setPiecesDossierAdditif($piecesDossierAdditif);
        }
        // date limite des remises des plis
        if (true == $this->modifiDateRemisePlis->Checked && '' !== $this->dateRemisePlis->Text) {
            $dateFin = Atexo_Util::frnDateTime2iso($this->dateRemisePlis->Text);
            if ($dateFin != $commonConsultation->getDateFin()) {
                $commonConsultation->setDatefin($dateFin);
                $this->setConsultationChanged(true);

                //Debut date de clôture
                if ('' != $dateFin) {
                    $dateTmpFile = Atexo_Config::getParameter('COMMON_TMP').'df'.session_id().time().'date.tmp';
                    Atexo_Util::write_file($dateTmpFile, 'Creation de la consultation -'.$this->_consultation->getId().'-, date de cloture : '.$dateFin);
                    if (is_file($dateTmpFile)) {
                        $arrayTimeStampDateFin = $atexoCrypto->timeStampFile($dateTmpFile);
                        unlink($dateTmpFile);
                        if ($arrayTimeStampDateFin) {
                            $dateFinO = new CommonDATEFIN();
                            $dateFinO->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                            $dateFinO->setHorodatage($arrayTimeStampDateFin['horodatage']);
                            $dateFinO->setUntrusteddate($arrayTimeStampDateFin['untrustedDate']);
                            $dateFinO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                            $dateFinO->setDatefin($dateFin);
                            $dateFinO->setStatut(Atexo_Config::getParameter('MODIFICATION_FILE'));
                            // date limite des remises des plis
                            if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire() && '' !== $this->dateRemisePlisLocale->Text) {
                                $dateFinLocale = Atexo_Util::frnDateTime2iso($this->dateRemisePlisLocale->Text);
                                if ($dateFinLocale != $commonConsultation->getDateFinLocale()) {
                                    $dateFinO->setDatefinLocale($dateFinLocale);
                                }
                            }
                            $commonConsultation->addCommonDATEFIN($dateFinO);
                        }
                    }
                }
            }
            // date limite des remises des plis
            if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire()) {
                if ('' !== $this->dateRemisePlisLocale->Text) {
                    $dateFinLocale = Atexo_Util::frnDateTime2iso($this->dateRemisePlisLocale->Text);
                    if ($dateFinLocale != $commonConsultation->getDateFinLocale()) {
                        $commonConsultation->setDatefinLocale($dateFinLocale);
                        $modifiedDlro = 1;
                        $this->setConsultationChanged(true);
                    }
                }
                if ('' !== $this->lieuResidence->Text && $this->lieuResidence->Text != $commonConsultation->getLieuResidence()) {
                    $commonConsultation->setLieuResidence($this->lieuResidence->Text);
                }
            }

            $event = new ModificationConsultationEvent($commonConsultation->getId(), (int) $commonConsultation->getCodeExterne());
            $dispatcher = Atexo_Util::getSfService('event_dispatcher');
            $dispatcher->dispatch($event, ModificationConsultationEvent::class);
            //Fin date de clôture
        }
        // poursuivreAffichage
        if (true == $this->modifAffichage->Checked) {
            if (true == $this->interrompreAffichage->Checked) {
                $commonConsultation->setPoursuivreAffichage('0');
                $commonConsultation->setPoursuivreAffichageUnite(preg_replace('#[0-9]#', '', $this->poursuivreAffichageNbJour->getSelectedValue()));
            } else {
                $commonConsultation->setPoursuivreAffichage((int) $this->poursuivreAffichageNbJour->getSelectedValue());
                $commonConsultation->setPoursuivreAffichageUnite(preg_replace('#[0-9]#', '', $this->poursuivreAffichageNbJour->getSelectedValue()));
            }

            $this->setConsultationChanged(true);
        }
        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
            $composantDCE = $this->dceComposant;
        } else {
            $composantDCE = $this->blocDce;
        }
        //Modifier DCE
        if (true == $this->piecesConsultation->Checked) {
            //Modifier le règlement

            if (Atexo_Module::isEnabled('ReglementConsultation')) {
                $infoFileRG = [];
                $infoFileRG = $composantDCE->getInfoFileRG();
                if ($infoFileRG['infile']) {
                    $msg = Atexo_ScanAntivirus::startScan($infoFileRG['infile']);
                }
                if ($msg) {
                    $this->afficherErreur($msg.' "'.$infoFileRG['fileName'].'"');

                    return false;
                }
                if (is_countable($infoFileRG) ? count($infoFileRG) : 0) {
                    $inFileRG = $infoFileRG['infile'];
                    $inFileNameRG = $infoFileRG['fileName'];
                    $this->remplacerDocReglement($inFileRG, $inFileNameRG, $composantDCE);
                }
            }

            //Pour modifier le DCE
            $dce = $this->_dce;
            $createZip = false;
            $zip_filename = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_update_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
            if ($dce instanceof CommonDCE && '0' != $dce->getDce()) {
                $statutDce = Atexo_Config::getParameter('MODIFICATION_FILE');
                Atexo_Blob::copyBlob($dce->getDce(), $zip_filename, Atexo_CurrentUser::getCurrentOrganism()); // On copie l'ancien DCE dans un fichier temp
                $composantDCE->_nomFileDce = $dce->getNomDce();
            } else {
                $statutDce = Atexo_Config::getParameter('AJOUT_FILE');
                $createZip = true;
                $this->dceComposant->_nomFileDce = preg_replace('/[\/\\\\:\*\?"<>|]/', '', $this->_consultation->getId()).'.zip';
            }

            $finalDce = '';
            $finalDceName = '';
            if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                $this->dceComposant->createZip = $createZip;
                $finalDce = $composantDCE->getNewZipDCE($zip_filename);
            } else {
                $finalDce = $composantDCE->getNewZipDCE($this->envoiMail->Checked);
            }
            $finalDceName = $composantDCE->_nomFileDce;
            $msg = Atexo_ScanAntivirus::startScan($finalDce);
            if ($msg) {
                $this->afficherErreur($msg.' "'.$composantDCE->_fileToScan.'"');

                return false;
            }
            if ('' != $finalDce) {
                $dceO = new CommonDCE();
                $dceO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                $atexoCrypto = new Atexo_Crypto();
                $atexoBlob = new Atexo_Blob();
                $arrayTimeStampDCE = $atexoCrypto->timeStampFile($finalDce);
                if (is_array($arrayTimeStampDCE)) {
                    $dceIdBlob = $atexoBlob->insert_blob($finalDceName, $finalDce, Atexo_CurrentUser::getOrganismAcronym(), null, Atexo_Config::getParameter('EXTENSION_DCE'));
                    if ($dceIdBlob) {
                        $composantDCE->_NewZipDCE = $dceIdBlob;
                        $dceO->setHorodatage($arrayTimeStampDCE['horodatage']);
                        $dceO->setUntrusteddate(($arrayTimeStampDCE['untrustedDate']));
                        $dceO->setAgentId(Atexo_CurrentUser::getId());
                        $dceO->setDce($dceIdBlob);
                        $dceO->setNomDce($finalDceName);
                        if ($composantDCE->statut == Atexo_Config::getParameter('REMPLACEMENT_FILE')) {
                            $dceO->setNomFichier($composantDCE->_fileAdddedName);
                            $dceO->setAncienFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($composantDCE->_oldfileName)));
                        } elseif ($composantDCE->statut == Atexo_Config::getParameter('AJOUT_FILE')) {
                            $dceO->setNomFichier($composantDCE->_fileAdddedName);
                        } else {
                            $dceO->setNomFichier($finalDceName);
                        }
                        $this->_consultation->addCommonDCE($dceO);
                        if ($composantDCE->statut) {
                            $dceO->setStatut($composantDCE->statut);
                        } else {
                            $dceO->setStatut($statutDce);
                        }
                        $event = new ModificationDceEvent($commonConsultation->getId(), (int) $commonConsultation->getCodeExterne());
                        $dispatcher = Atexo_Util::getSfService('event_dispatcher');
                        $dispatcher->dispatch($event, ModificationDceEvent::class);
                        $this->setConsultationChanged(true);
                    }
                }
            }
            if (isset($zip_filename) && is_file($zip_filename)) {
                unlink($zip_filename);
            }
            if ($composantDCE->telechargementPartiel->Checked) {
                $this->_consultation->setPartialDceDownload('1');
            } else {
                $this->_consultation->setPartialDceDownload('0');
            }

            if (Atexo_Module::isEnabled('ModuleEnvol')) {
                if ($this->_consultation->getIdDossierVolumineux() != $this->annexeTechniqueDCE->getSelectedValue()) {
                    $dvOld = $this->_consultation->getIdDossierVolumineux();
                    $this->_consultation->setIdDossierVolumineux($this->annexeTechniqueDCE->getSelectedValue() ?: null);
                    Atexo_DossierVolumineux_Responses::saveHistoriqueDossierVolumineux($dvOld, $this->_consultation, $this->_connexionCom, Atexo_CurrentUser::getCurrentOrganism(), true);
                    $this->setConsultationChanged(true);
                }
            }
            // Modifier le dossier 'En savoir plus sur l'opération'
            if ($this->nouveauDossierOperation->hasFile()) {
                $complementO = new CommonComplement();
                $complementO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                $infileAutrePiece = Atexo_Config::getParameter('COMMON_TMP').'nouveauDossierOperation'.session_id().time();
                if (copy($this->nouveauDossierOperation->getLocalName(), $infileAutrePiece)) {
                    $arrayTimeStampComplement = $atexoCrypto->timeStampFile($infileAutrePiece);
                    if (is_array($arrayTimeStampComplement)) {
                        $complementO->setHorodatage($arrayTimeStampComplement['horodatage']);
                        $complementO->setUntrusteddate(($arrayTimeStampComplement['untrustedDate']));
                        $complementO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                        $autrePieceIdBlob = $atexoBlob->insert_blob($this->nouveauDossierOperation->getFileName(), $infileAutrePiece, Atexo_CurrentUser::getOrganismAcronym());
                        $complementO->setComplement($autrePieceIdBlob);
                        $complementO->setNomFichier($this->nouveauDossierOperation->getFileName());
                        $complementO->setStatut(Atexo_Config::getParameter('REMPLACEMENT_FILE'));
                        $commonConsultation->addCommonComplement($complementO);
                        $this->setConsultationChanged(true);
                        //  $this->_idMesssage = 'rg';
                    }
                }
                @unlink($infileAutrePiece);
            }
        }

        foreach ($this->idReferentielCheck->RepeaterReferentielZoneText->getItems() as $Item) {
            if ($Item->refTexte->checked && $Item->idReferentielZoneText->Text != $Item->oldValue->Value) {
                $this->setConsultationChanged(true);
            }
        }
        foreach ($this->idReferentielCheckLot->RepeaterReferentielZoneText->getItems() as $Items) {
            if ($Items->refTexte->checked) {
                foreach ($Items->idLot->repeaterLot->Items as $oneItem) {
                    if ($oneItem->idReferentielZoneText->Text != $oneItem->oldValue->Value) {
                        $this->setConsultationChanged(true);
                    }
                }
            }
        }

        foreach ($this->idReferentielRadio->RepeaterReferentielRadio->getItems() as $Item) {
            if ($Item->refRadio->checked &&
             (('1' == $oneItem->oldValue->Value) && (!$oneItem->OptionOui->getChecked())
             || ('0' == $oneItem->oldValue->Value) && ($oneItem->OptionOui->getChecked()))) {
                $this->setConsultationChanged(true);
            }
        }
        foreach ($this->idReferentielRadioLot->RepeaterReferentielRadio->getItems() as $Items) {
            if ($Items->refRadio->checked) {
                foreach ($Items->idLot->repeaterLot->Items as $oneItem) {
                    if (('1' == $oneItem->oldValue->Value) && (!$oneItem->OptionOui->getChecked())
                     || ('0' == $oneItem->oldValue->Value) && ($oneItem->OptionOui->getChecked())) {
                        $this->setConsultationChanged(true);
                    }
                }
            }
        }

        if (true === $this->changed->getValue()) {
            $this->setConsultationChanged(true);
        }

        try {
            if (Atexo_Module::isEnabled('interfaceDume') && true == $this->dumeAcheteurPublie->Checked) {
                $dumeContexte = Atexo_Dume_AtexoDume::getTDumeContexteStatutPublie($commonConsultation);

                $retour = $this->bloc_etapeDumeAcheteur->validateDumeAcheteur($commonConsultation);
                $dumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'));

                if (is_array($retour) && $retour['erreur']) {
                    $scriptJs = "document.getElementById('divValidationSummary').style.display='';hideLoader();";
                    $this->divValidationSummary->addServerError($retour['messageErreur'], false);
                    $this->javascript->Text = "<script>$scriptJs</script>";
                    $this->ctl0_CONTENU_PAGE_pageLoader->Visible = false;

                    return -1;
                } else {
                    $newDumeContexte = $dumeContexte->copy();
                    $newDumeContexte->setDateModification(date('Y-m-d H:i:s'));
                    $newDumeContexte->save($this->_connexionCom);

                    if ($dumeContexte->getStatus() == Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE')) {
                        $this->bloc_etapeDumeAcheteur->saveDumeAcheteur($commonConsultation);
                        $responseReplacePublished = Atexo_Dume_AtexoDume::replacePublished($commonConsultation, $dumeContexte->getContexteLtDumeId());

                        $c = new Criteria();
                        $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $commonConsultation->getId());
                        $c->add(CommonTDumeContextePeer::ORGANISME, $commonConsultation->getOrganisme());
                        $c->add(CommonTDumeContextePeer::STATUS, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON'));
                        $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_OE'));
                        $dumeContextes = CommonTDumeContextePeer::doSelect($c, $this->_connexionCom);

                        foreach ($dumeContextes as $dumeContexteOE) {
                            $dumeContexteOE->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_REMPLACER'));
                            $dumeContexteOE->save($this->_connexionCom);

                            $c = new Criteria();
                            $c->add(CommonTCandidaturePeer::CONSULTATION_ID, $commonConsultation->getId());
                            $c->add(CommonTCandidaturePeer::ORGANISME, $commonConsultation->getOrganisme());
                            $c->add(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $dumeContexteOE->getId());
                            $c->add(CommonTCandidaturePeer::STATUS, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON'));
                            $candidature = CommonTCandidaturePeer::doSelectOne($c, $this->_connexionCom);

                            $newCandidature = new CommonTCandidature();
                            $newCandidature->setConsultationId($candidature->getConsultationId());
                            $newCandidature->setOrganisme($candidature->getOrganisme());
                            $newCandidature->setIdInscrit($candidature->getIdInscrit());
                            $newCandidature->setIdEntreprise($candidature->getIdEntreprise());
                            $newCandidature->setIdEtablissement($candidature->getIdEtablissement());
                            $newCandidature->setStatus($candidature->getStatus());
                            $newCandidature->save($this->_connexionCom);

                            $candidature->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_REMPLACER'));
                            $candidature->save($this->_connexionCom);

                            if ($commonConsultation->getAlloti()) {
                                $c = new Criteria();
                                $c->add(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $commonConsultation->getId());
                                $c->add(CommonTListeLotsCandidaturePeer::ORGANISME, $commonConsultation->getOrganisme());
                                $c->add(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $candidature->getId());
                                $c->add(CommonTListeLotsCandidaturePeer::STATUS, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON'));
                                $listeLotsCandidatures = CommonTListeLotsCandidaturePeer::doSelect($c, $this->_connexionCom);

                                foreach ($listeLotsCandidatures as $listeLotsCandidature) {
                                    $newListeLotsCandidature = new CommonTListeLotsCandidature();
                                    $newListeLotsCandidature->setConsultationId($listeLotsCandidature->getConsultationId());
                                    $newListeLotsCandidature->setOrganisme($listeLotsCandidature->getOrganisme());
                                    $newListeLotsCandidature->setIdInscrit($listeLotsCandidature->getIdInscrit());
                                    $newListeLotsCandidature->setIdEntreprise($listeLotsCandidature->getIdEntreprise());
                                    $newListeLotsCandidature->setIdEtablissement($listeLotsCandidature->getIdEtablissement());
                                    $newListeLotsCandidature->setStatus($listeLotsCandidature->getStatus());
                                    $newListeLotsCandidature->setIdCandidature($newCandidature->getId());
                                    $newListeLotsCandidature->setNumLot($listeLotsCandidature->getNumLot());
                                    $newListeLotsCandidature->save($this->_connexionCom);

                                    $listeLotsCandidature->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_REMPLACER'));
                                    $listeLotsCandidature->save($this->_connexionCom);
                                }
                            }
                        }

                        $jsonResponse = json_decode($responseReplacePublished);
                        if (JSON_ERROR_NONE == json_last_error()
                            && !empty($jsonResponse->isStandard)
                            && true == $jsonResponse->isStandard
                        ) {
                            $newDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'));
                        } else {
                            $newDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'));
                        }

                        $dumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_REMPLACER'));
                        $newDumeContexte->save($this->_connexionCom);
                        $dumeContexte->save($this->_connexionCom);

                        $this->setConsultationChanged(true);

                        Atexo_Dume_AtexoDume::createJmsJobToPublishDume($commonConsultation);
                    }
                }
            }

        } catch (\Exception $e) {
            Prado::log('Erreur updateMetadonnées de lt_dume'.$e->__toString().'-', TLogger::FATAL, 'Fatal');

            return -1;
        }

        try {
            $this->donneeComplementaire->save($this->_connexionCom);
            $commonConsultation->setIdDonneeComplementaire($this->donneeComplementaire->getIdDonneeComplementaire());
            if ($commonConsultation->saveConsultation($this->_connexionCom) >= 0) {
                if ($modifiedDlro) {
                    $dumeContexte = Atexo_Dume_AtexoDume::getTDumeContexte($commonConsultation);

                    if ($dumeContexte) {
                        Atexo_Dume_AtexoDume::updateDLRO($commonConsultation, $dumeContexte->getContexteLtDumeId());
                    }
                }

                // Envoi mail
                if ((true === $this->envoiMail->Checked) && (true === $this->getConsultationChanged())) {
                    // creation d'echange
                    $retour = [];
                    if (true == $this->piecesConsultation->Checked) {
                        $retour = $composantDCE->getInfoFilesAdded();
                    }
                    $this->createEchangeDCE($retour);
                    $this->_idMesssage = $this->idMessage->getValue();
                }

                if (is_array($commonLots)) {
                    foreach ($commonLots as $oneLotCom) {
                        $oneLotCom->save($this->_connexionCom);
                    }
                }
                // Traitement des alertes
                if (Atexo_Module::isEnabled('AlerteMetier') && true === $this->getConsultationChanged()) {
                    Atexo_AlerteMetier::lancerAlerte('ModificationApresValidation', $commonConsultation, $this->_connexionCom);
                }
                // update Annonce SUB
                if (Atexo_Module::isEnabled('Publicite')) {
                    (new Atexo_Publicite_AnnonceSub())->updateAnnonceSubfromConsultation($commonConsultation);
                }
                // Fin traitement des alertes

                // Creation de l'historique
                $consultationId = $commonConsultation->getId();
                $lot = '0';
                $detail2 = '';
                $valeur = '0';

                // debut : Historique du lt refrentiel
                $referentielZoneTextNew = $this->idReferentielCheck->getValueReferentielTextVo();
                $referentielZoneTextOld = $this->getViewState('referentielZoneTextOld');
                $prefixNomElement = Atexo_Config::getParameter('ID_LT_REFERENTIEL');
                if ($referentielZoneTextNew && is_array($referentielZoneTextNew)) {
                    foreach ($referentielZoneTextNew as $k => $v) {
                        if (!$referentielZoneTextOld || (trim($referentielZoneTextOld[$k]->getValuePrinc()) != trim($v->getValuePrinc()))) {
                            $nomElement = $prefixNomElement.'_'.$v->getCodeLibelle();
                            $valeur = '0';
                            $detail1 = '';
                            if ('' != $v->getValuePrinc()) {
                                $detail1 = $v->getValuePrinc();
                                $valeur = '1';
                            }
                            $detail2 = '';
                            $lot = '0';
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                }
                //historique Lt referetiel de type radio
                $referentielRadioNew = $this->idReferentielRadio->getValueReferentielRadioVo();
                $referentielRadioOld = $this->getViewState('referentielRadioOld');
                if ($referentielRadioNew && is_array($referentielRadioNew)) {
                    foreach ($referentielRadioNew as $k => $v) {
                        if (!$referentielRadioOld || (trim($referentielRadioOld[$k]->getValue()) != trim($v->getValue()))) {
                            $nomElement = $prefixNomElement.'_'.$v->getCodeLibelle();
                            $valeur = '0';
                            $detail1 = '';
                            if ('' != $v->getValue()) {
                                $detail1 = $v->getValue();
                                $valeur = '1';
                            }
                            $detail2 = '';
                            $lot = '0';
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                }
                // fin : Historique du lt refrentiel

                //ADRESSE RETRAIT DOSSIERS
                if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                    $retraitDossiers = $this->retraitDossiers->Text;
                    if ($retraitDossiers && true == $this->retraitDossierCheck->Checked && $retraitDossiersChanged) {
                        $nomElement = Atexo_Config::getParameter('ID_ADRESSE_RETRAIT_DOSSIERS');
                        $valeur = '1';
                        $detail1 = $retraitDossiers;
                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                    }
                }
                // ADRESSE RETRAIT DOSSIERS
                if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                    $addDepotDossiers = $this->addDepotDossiers->Text;
                    if ($addDepotDossiers && $this->depotOffresCheck->Checked && $addDepotDossiersChanged) {
                        $nomElement = Atexo_Config::getParameter('ID_ADRESSE_DEPOT_OFFRES');
                        $detail1 = $addDepotDossiers;
                        $valeur = '1';
                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                    }
                }
                // LIEU OUVERTURE PLIS
                if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                    $lieuOuverturePlis = $this->lieuOuverturePlis->Text;
                    if ($lieuOuverturePlis && $this->ouverturePlis->Checked && $lieuOuverturePlisChanged) {
                        $nomElement = Atexo_Config::getParameter('ID_LIEU_OUVERTURE_PLIS');
                        $detail1 = $lieuOuverturePlis;
                        $valeur = '1';
                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                    }
                }
                if ('0' == $commonConsultation->getAlloti()) { // Consultation non allotie
                    $lot = '0';

                    // Caution Provisoire
                    if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires')) {
                        $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CAUTION_PROVISOIRE');
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        $cautionProvisoire = str_replace(',', '.', $this->cautionProvisoire->Text);
                        if (($cautionProvisoire || '0' == $cautionProvisoire) && $this->cautionProvisoireCheck->Checked && $cautionProvisoireChanged) {
                            $valeur = '1';
                            $detail1 = $cautionProvisoire;
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }

                    // Qualification
                    if (Atexo_Module::isEnabled('ConsultationQualification')) {
                        $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_QUALIFICATION');
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        $qualification = base64_decode($this->qualification->idsQualification->Text);
                        if ($this->qualificationCheck->Checked && $qualificationChanged) {
                            $detail1 = $qualification;
                            $valeur = ($this->qualification->idsQualification->Text) ? '1' : '0';
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                    // Agrement
                    if (Atexo_Module::isEnabled('ConsultationAgrement')) {
                        $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_AGREMENT');
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        $agrements = $this->agrements->idsSelectedAgrements->Value;
                        if ($this->agrementsCheck->Checked && $agrementsChanged) {
                            $valeur = ($this->agrements->idsSelectedAgrements->Value) ? '1' : '0';
                            $detail1 = $agrements;
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                    // visites des lieux
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                        $nomElement = Atexo_Config::getParameter('ID_VISITES_LIEUX');
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        if ($this->visite->Checked) {
                            $valeur = '1';
                            foreach ($this->visiteRepeater->Items as $oneItem) {
                                $detail1 .= $oneItem->adresseValue->Value.Atexo_Config::getParameter('SEPARATEUR_INTRA_VISTES_LIEUX')
                                .$oneItem->dateValue->Value.Atexo_Config::getParameter('SEPARATEUR_EXTRA_VISTES_LIEUX');
                            }
                            if ($visiteLieuChanged) {
                                $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                            }
                        }
                    }
                    // Echantillons
                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                        $nomElement = Atexo_Config::getParameter('ID_ECHANTILLONS');
                        $lot = '0';
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        $addEchantillion = $this->addEchantillion->Text;
                        $dateLimiteEchantillion = Atexo_Util::frnDateTime2iso($this->dateLimiteEchantillion->Text);
                        if ($this->echantillion->Checked && ($addEchantillionChanged || $dateLimiteEchantillionChanged)) {
                            $valeur = ($addEchantillion) ? '1' : '0';
                            $detail1 = $addEchantillion;
                            $detail2 = $dateLimiteEchantillion;
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                    // Réunion
                    if (Atexo_Module::isEnabled('ConsultationReunion')) {
                        $nomElement = Atexo_Config::getParameter('ID_REUNION');
                        $lot = '0';
                        $valeur = '0';
                        $detail1 = '';
                        $detail2 = '';
                        $addReunion = $this->addReunion->Text;
                        $dateReunion = Atexo_Util::frnDateTime2iso($this->dateReunion->Text);
                        if ($this->reunion->Checked && ($addReunionChanged || $dateReunionChanged)) {
                            $valeur = ($addReunion) ? '1' : '0';
                            $detail1 = $addReunion;
                            $detail2 = $dateReunion;
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                        }
                    }
                    // Variantes selon commentaire la variante est toujours non afficher on changing consultation
//                  if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
//                      $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES');
//                      $lot = "0";
//                      $valeur = "0";
//                      $detail1 = "";
//                      $detail2 = "";
//                      $variantes = $this->variantesCheck->Checked;
//                      if ($variantes && $commonConsultation->getVariantes()) {
//                          if($variantesChanged) {
//                              $valeur = "1";
//                              $detail1 = $commonConsultation->getVariantes();
//                              $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
//                          }
//                      }
//                  }
                } else { // Consultation allotie
                    $valeur = '0';
                    $detail2 = '';
                    $lots = $commonConsultation->getAllLots();
                    $valueRefrentielNew = $this->idReferentielCheckLot->getValueReferentielLotTextVo();
                    $valueRefrentielOld = $this->getViewState('referentielZoneTextOldLot');
                    $historiqueReferentiel = 1;
                    if (0 == (is_countable($valueRefrentielNew) ? count($valueRefrentielNew) : 0) || (is_countable($valueRefrentielOld) ? count($valueRefrentielOld) : 0)) {
                        $historiqueReferentiel = 0;
                    }
                    //lt referentiel Radio (consultation Allotie)
                    $referentielRadioLotsNew = $this->idReferentielRadioLot->getValueReferentielRadioVo();
                    $referentielRadioLotsOld = $this->getViewState('referentielRadioOldLot');
                    $prefixNomElement = Atexo_Config::getParameter('ID_LT_REFERENTIEL');
                    foreach ($lots as $oneLot) {
                        // debut : Historique du lt refrentiel
                        if ($historiqueReferentiel) {
                            $referentielZoneTextNewLot = $valueRefrentielNew[$oneLot->getLot()];
                            $referentielZoneTextOldLot = $valueRefrentielOld[$oneLot->getLot()];

                            if ($referentielZoneTextNewLot && is_array($referentielZoneTextNewLot)) {
                                foreach ($referentielZoneTextNewLot as $k => $v) {
                                    if (!$referentielZoneTextOldLot || (trim($referentielZoneTextOldLot[$k]->getValuePrinc()) != trim($v->getValuePrinc()))) {
                                        $nomElement = $prefixNomElement.'_'.$v->getCodeLibelle();
                                        $valeur = '0';
                                        $detail1 = '';
                                        if ('' != $v->getValuePrinc()) {
                                            $detail1 = $v->getValuePrinc();
                                            $valeur = '1';
                                        }
                                        $detail2 = '';
                                        $lot = $oneLot->getLot();
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                                    }
                                }
                            }
                        }
                        //type Radio
                        if ($historiqueReferentiel) {
                            $referentielRadioLotNew = $referentielRadioLotsNew[$oneLot->getLot()];
                            $referentielRadioLotOld = $referentielRadioLotsOld[$oneLot->getLot()];
                            if ($referentielRadioLotNew && is_array($referentielRadioLotNew)) {
                                foreach ($referentielRadioLotNew as $k => $v) {
                                    if (!$referentielRadioLotOld || (trim($referentielRadioLotOld[$k]->getValue()) != trim($v->getValue()))) {
                                        $nomElement = $prefixNomElement.'_'.$v->getCodeLibelle();
                                        $valeur = '0';
                                        $detail1 = '';
                                        if ('' != $v->getValue()) {
                                            $detail1 = $v->getValue();
                                            $valeur = '1';
                                        }
                                        $detail2 = '';
                                        $lot = '0';
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot);
                                    }
                                }
                            }
                        }
                        // fin : Historique du lt refrentiel

                        // Caution Provisoire
                        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires') && $this->cautionProvisoireCheck->Checked) {
                            $index = 0;
                            $valeur = '0';
                            $detail2 = '';
                            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CAUTION_PROVISOIRE');
                            foreach ($this->cautionProvisoireRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                                    $valeur = '1';
                                    $cautionProvisoire = $oneItem->cautionProvisoire->getText();
                                    if ($cautionProvisoireChanged) {
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $cautionProvisoire, $detail2, $oneLot->getLot());
                                    }
                                }
                                ++$index;
                            }
                        }

                        // Qualification
                        if (Atexo_Module::isEnabled('ConsultationQualification') && $this->qualificationCheck->Checked) {
                            $index = 0;
                            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_QUALIFICATION');
                            $valeur = '0';
                            $detail1 = '';
                            $detail2 = '';
                            foreach ($this->qualificationRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                                    $qualification = base64_decode($oneItem->qualification->idsQualification->getText());
                                    if ($qualificationChanged) {
                                        if ($oneItem->qualification->idsQualification->getText()) {
                                            $valeur = '1';
                                        } else {
                                            $valeur = '0';
                                        }
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $qualification, $detail2, $oneLot->getLot());
                                    }
                                }
                                ++$index;
                            }
                        }

                        // Agrement
                        if (Atexo_Module::isEnabled('ConsultationAgrement') && $this->agrementsCheck->Checked) {
                            $index = 0;
                            $valeur = '0';
                            $detail1 = '';
                            $detail2 = '';
                            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_AGREMENT');
                            foreach ($this->agrementsRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                                    $agrements = $oneItem->agrements->idsSelectedAgrements->getValue();
                                    $valeur = '1';
                                    if ($agrementsChanged) {
                                        if ($oneItem->agrements->idsSelectedAgrements->getValue()) {
                                            $valeur = '1';
                                        } else {
                                            $valeur = '0';
                                        }
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $agrements, $detail2, $oneLot->getLot());
                                    }
                                }
                                ++$index;
                            }
                        }
                        // visites des lieux
                        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && $this->visite->Checked) {
                            $detail1Visite = '';
                            $valeur = '0';
                            $detail1 = '';
                            $detail2 = '';
                            $nomElement = Atexo_Config::getParameter('ID_VISITES_LIEUX');
                            foreach ($this->visiteLotRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText()) {
                                    foreach ($oneItem->visiteRepeater->Items as $Item) {
                                        $valeur = '1';
                                        $detail1Visite .= $Item->adresseValue->Value.Atexo_Config::getParameter('SEPARATEUR_INTRA_VISTES_LIEUX')
                                        .$Item->dateValue->Value.Atexo_Config::getParameter('SEPARATEUR_EXTRA_VISTES_LIEUX');
                                    }
                                }
                            }
                            $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1Visite, $detail2, $oneLot->getLot());
                        }
                        // Echantillons
                        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && $this->echantillion->Checked) {
                            $index = 0;
                            $nomElement = Atexo_Config::getParameter('ID_ECHANTILLONS');
                            $valeur = '0';
                            $detail1 = '';
                            $detail2 = '';
                            foreach ($this->echantillonLotRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText() && $oneItem->echantillonOuiLot->checked) {
                                    $addEchantillion = $oneItem->addEchantillion->getText();
                                    $valeur = '1';
                                    $dateLimiteEchantillion = Atexo_Util::frnDateTime2iso($oneItem->dateLimiteEchantillion->getText());
                                    if (($dateLimiteEchantillionChanged) || ($addEchantillionChanged)) {
                                        if ($addEchantillion) {
                                            $valeur = '1';
                                        } else {
                                            $valeur = '0';
                                        }
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $addEchantillion, $dateLimiteEchantillion, $oneLot->getLot());
                                    }
                                }
                                ++$index;
                            }
                        }
                        // Réunion
                        if (Atexo_Module::isEnabled('ConsultationReunion') && $this->reunion->Checked) {
                            $index = 0;
                            $nomElement = Atexo_Config::getParameter('ID_REUNION');
                            $valeur = '0';
                            $detail1 = '';
                            $detail2 = '';
                            foreach ($this->reunionLotRepeater->getItems() as $oneItem) {
                                if ($oneLot->getLot() == $oneItem->numLot->getText() && $oneItem->reunionOuiLot->checked) {
                                    $addReunion = $oneItem->addReunion->getText();
                                    $valeur = '1';
                                    $dateReunion = Atexo_Util::frnDateTime2iso($oneItem->dateReunion->getText());
                                    if (($dateReunionChanged) || ($addReunionChanged)) {
                                        if ($addReunion) {
                                            $valeur = '1';
                                        } else {
                                            $valeur = '0';
                                        }
                                        $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $addReunion, $dateReunion, $oneLot->getLot());
                                    }
                                }
                                ++$index;
                            }
                        }
                        // Variantes
//                      if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
//                          if($this->variantesCheck->Checked) {
//                              $index = 0 ;
//                              $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES');
//                              $valeur = "0";
//                              $detail1 = "";
//                              $detail2 = "";
//                              foreach($this->variantesRepeater->getItems() as $oneItem) {
//                                  if($oneLot->getLot()==  $oneItem->numLot->getText()){
//                                      if($oneItem->variantesNon->Checked){
//                                          $oneLot->setVariantes("0");
//                                      }
//                                      else {
//                                          $oneLot->setVariantes("1");
//                                      }
//                                      $valeur = "1";
//                                      $variantes  = $oneItem->variantesNon->Checked;
//                                      if($variantesChanged) {
//                                          $this->setHistoriquesConsultation($consultationId, $nomElement, $valeur, $variantes, $detail2, $oneLot->getLot());
//                                      }
//                                  }
//                                  $index++;
//                              }
//                          }
//                      }
                    }   // Fin else
                }

                return true;
            } else {
                return -1;
            }
        } catch (\Exception $e) {
            Prado::log('Erreur Modification Consultation '.$e->__toString().'-', TLogger::FATAL, 'Fatal');

            return -1;
        }
    }

    /**
     * action du boutton valider.
     */
    public function onValider($sender, $param)
    {
        if ($this->IsValid) {
            if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))){
				$composantDCE = $this->dceComposant;
			}else{
				$composantDCE = $this->blocDce;
			}
			//Commencer le scan
			if ($this->nouveauDossierOperation->hasFile()) {
				$msg = Atexo_ScanAntivirus::startScan($this->nouveauDossierOperation->getLocalName());
				if($msg) {
					$this->afficherErreur($msg . ' "' . $this->nouveauDossierOperation->getFileName() . '"');
					return;
				}
			}
			//Fin code scan antivirus
            try{
			    $result = self :: updateConsultation($sender, $param);
            } catch (ExceptionDceLimit $e){
                $errorMessage = Prado::localize('TEXT_TAILLE_AUTORISEE_DOC',
                    array(
                        'size' => Atexo_Util::arrondirSizeFile(
                            (Atexo_Config::getParameter("MAX_UPLOAD_FILE_DCE_SIZE") / 1024)
                        )
                    )
                );
                $scriptJs = "document.getElementById('divValidationSummary').style.display='';";
                $this->divValidationSummary->addServerError($errorMessage,false);
                $this->javascript->Text = "<script>$scriptJs</script>";
                $result = false;
            }
            if (false === $result) {
                return;
            }
            try {
                $consultationId = $composantDCE->_consultation->getId();
                $org = $composantDCE->_consultation->getOrganisme();

                // Reglement de consultation
                if (is_countable($composantDCE->getInfoFileRG()) ? count($composantDCE->getInfoFileRG()) : 0) {
                    $rg = (new Atexo_Consultation_Rg())->getReglement($consultationId, $org);
                    if ($rg && $composantDCE->getInfoFileRG()['fileName'] == $rg->getNomFichier()) {
                        $infoFileToDownload = Atexo_Consultation_Dce::getFileDceOrRgToDownloadForMessec('RC', $rg->getAgentId(), $consultationId, $rg->getRg(), $rg->getNomFichier());
                        $this->fileRc->setValue(json_encode($infoFileToDownload, JSON_THROW_ON_ERROR));
                    }
                }

                // Piece DCE
                if ($composantDCE->_fileAdddedName != '') {
                    $dce = (new Atexo_Consultation_Dce())->getDce($consultationId, $org);

                    if ($dce && $composantDCE->_fileAdddedName == $dce->getNomFichier()) {
                        $infoFileToDownload = Atexo_Consultation_Dce::getFileDceOrRgToDownloadForMessec('DCE', $dce->getAgentId(), $consultationId, $dce->getDce(), $dce->getNomFichier());
                        $this->fileInDce->setValue(json_encode($infoFileToDownload, JSON_THROW_ON_ERROR));
                    }
                }

                if (Atexo_Module::isEnabled('GenerationAvis')) {
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $commonCons = CommonConsultationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), $connexionCom);

                    // debut Recuperation de la liste des langue actives :tableau de langues
                    $languesActives = Atexo_Languages::retrieveActiveLangages();

                    foreach ($languesActives as $lang) {
                        $pathFile = '';
                        $pathFile = (new Atexo_Publicite_AvisPdf())->generateAvis($commonCons, Atexo_Config::getParameter('FICHIER_TEMPLATE_AVIS'), trim($lang->getLangue()), true);
                        if (-1 != $pathFile && !(new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($pathFile, $commonCons, trim($lang->getLangue()))) {
                            $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&noAvisGenerated=1');
                        }
                    }
                }
            } catch (\Exception) {
                $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])).'&noAvisGenerated=1&traductionOgl');
            }
            if (-1 == $this->result->getValue()) {
                $result = -1;
            }
            if ($result >= 0) {
                $urlDetail = 'index.php?page=Agent.DetailConsultation&id='
                    .(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])))
                    .'&traductionOgl';

                if ('' !== $this->_idMesssage && 'rg' !== $this->_idMesssage) {
                    if (2 === $this->_consultation->getVersionMessagerie()) {
                        $this->redirectMessec($urlDetail);
                    } else {
                        $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueModifConsultation&id='.
                            $this->_consultation->getId().'&IdEchange='.$this->_idMesssage.'&traductionOgl');
                    }
                } elseif ((true === $this->envoiMail->Checked) && ('rg' == $this->_idMesssage)) {
                    if (2 === $this->_consultation->getVersionMessagerie()) {
                        $this->redirectMessec($urlDetail);
                    } else {
                        $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueModifConsultation&id='.
                            $this->_consultation->getId().'&SaveSucces');
                    }
                } else {
                    $this->response->redirect('index.php?page=Agent.DetailConsultation&id='.(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']))).'&traductionOgl');
                }
            } elseif (-1 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_CONSULTATION')); // Erreur enregistrement modifié
            } elseif (-2 == $result) {
                //$this->piecesConsultation->checked=false;
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_FICHIER_DCE')); // Erreur DCE doit être au format zip
            } elseif (-3 == $result) {
                //$this->piecesConsultation->checked=false;
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('MSG_ERREUR_ZIP_DCE_INVALIDE')); // Erreur DCE doit être au format zip
            }
        }
        $this->buttonSave->setDisplay('Dynamic');
    }

    public function redirectMessec($urlDetail)
    {
        $encrypte = Atexo_Util::getSfService(Encryption::class);
        $idCrypte = $encrypte->cryptId($this->_consultation->getId());

        $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_NEW_MESSAGE_MODIFICATION'));
        //PF_URL_MESSAGERIE_NEW_MESSAGE
        $urlNewMessage = $pfUrl
            .'/'
            .$idCrypte;
        $this->scriptJs->text = "<script>isTriggered = true;
var popUp = window.open('" . $urlNewMessage . "');
if (popUp == null || typeof(popUp)=='undefined') {  
    alert('" . Prado::localize("MESSAGE_AUTORISER_POPUP") . "'); 
} 
else {  
    popUp.focus();
    window.location.href = '" .$urlDetail."';
}
 </script>";
    }

    /**
     * action boutton Annuler
     *  retoure vers la page de détail de la consultation.
     */
    public function actionBouttonAnnuler($sender, $param)
    {
        $this->response->redirect('index.php?page=Agent.DetailConsultation&id='.(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']))));
    }

    public function customizeForm()
    {
        $moduleConAdresseR = Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers');
        $this->panelAdresseRetraitDossiers->setVisible($moduleConAdresseR);
        $this->validatorRetraitDossiers->setEnabled($moduleConAdresseR);

        $moduleAdresseDepot = Atexo_Module::isEnabled('ConsultationAdresseDepotOffres');
        $this->panelAdresseDepot->setVisible($moduleAdresseDepot);
        $this->validateDepotDossiersChanging->setEnabled($moduleAdresseDepot);

        $this->panelPrixAquisition->setVisible(Atexo_Module::isEnabled('ConsultationPrixAcquisition') && Atexo_Module::isEnabled('DonneesComplementaires'));
        $this->validatorPrixAquisitionChanging->setEnabled(false);

        $moduleLieuOuverture = Atexo_Module::isEnabled('ConsultationLieuOuverturePlis');
        $this->panelLieuOuverturePlis->setVisible($moduleLieuOuverture);
        $this->validatorOuverturePlisChanging->setEnabled($moduleLieuOuverture);

        $this->panelCautionProvisoire->setVisible(Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires'));
        $this->validatorCuationProvChanging->setEnabled(false);
        $this->validatorCautionProvisoireLot->setEnabled(false);

        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->setDisplay('Dynamic');
            $this->qualification->setCallBack($this->IsCallBack);
            $this->qualification->setPostBack($this->IsPostBack);
        } else {
            $this->panelQualification->setVisible(false);
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrement->setDisplay('Dynamic');
            $this->agrements->setPostBack($this->IsPostBack);
        } else {
            $this->panelAgrement->setVisible(false);
        }

        $module_Echantillon = Atexo_Module::isEnabled('ConsultationEchantillonsDemandes');
        $this->panelEchantillonsDemandes->setVisible($module_Echantillon);
        $this->validatorEchantillonsChanging->setEnabled($module_Echantillon);
        $this->validatorEchantillonLot->setEnabled($module_Echantillon);
        $this->validatorEchantillionCompare->setEnabled($module_Echantillon);

        $moduleReunion = Atexo_Module::isEnabled('ConsultationReunion');
        $this->panelReunion->setVisible($moduleReunion);
        $this->validatorReunionChanging->setEnabled($moduleReunion);
        $this->validatorReunionLot->setEnabled($moduleReunion);
        $this->validatorReunionDateCompare->setEnabled($moduleReunion);

        $moduleVisitesLieu = Atexo_Module::isEnabled('ConsultationVisiteDesLieux');
        $this->panelVisiteLieux->setVisible($moduleVisitesLieu);
        $this->validatorVisitesLot->setEnabled($moduleVisitesLieu);
        $this->validateVisitesChanging->setEnabled($moduleVisitesLieu);
        $this->validatorVisiteDesLieuxDateCompare->setEnabled($moduleVisitesLieu);

        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {  // Même si le module est activer on masque les Variantes Autorisees dans la modificatiuon
            $this->panelVariantesAutorisees->setDisplay('None');
        } else {
            $this->panelVariantesAutorisees->setVisible(false);
        }
        $modulePiecesDossiers = Atexo_Module::isEnabled('ConsultationPiecesDossiers');
        $this->panelModuleAutrePiece->setVisible($modulePiecesDossiers);
    }

    /**
     * Afficher la consultation.
     */
    public function displayConsultation()
    {
        if ($this->_consultation) {
            $this->displayAllotissement();
            $this->retraitDossiers->Text = $this->_consultation->getAdresseRetraisDossiersTraduit();
            $this->addDepotDossiers->Text = $this->_consultation->getAdresseDepotOffresTraduit();
            $this->lieuOuverturePlis->Text = $this->_consultation->getLieuOuverturePlisTraduit();
            if ($this->donneeComplementaire instanceof CommonTDonneeComplementaire) {
                $this->cautionProvisoire->Text = $this->donneeComplementaire->getCautionProvisoire();
                $this->prixAquisitaionPlans->Text = $this->donneeComplementaire->getPrixAquisitionPlans();
            }
            $this->idReferentielCheck->setDependanceLot($this->_consultation->getAlloti());
            $this->idReferentielCheck->afficherTextConsultation($this->_consultation->getId(), 0, Atexo_CurrentUser::getCurrentOrganism());
            $this->setViewState('referentielZoneTextOld', $this->idReferentielCheck->getValueReferentielTextVo());
            //display Lt ref Raido
            $this->idReferentielRadio->setDependanceLot($this->_consultation->getAlloti());
            $this->idReferentielRadio->afficherReferentielRadio($this->_consultation->getId(), 0, Atexo_CurrentUser::getCurrentOrganism(), null, true);
            $this->setViewState('referentielRadioOld', $this->idReferentielRadio->getValueReferentielRadioVo(), null);

            //$this->prixAquisitaionPlans->Text = $this->_consultation->getPrixAquisitionPlans();
            if ('0' == $this->_consultation->getAlloti()) {
                $this->marcheUnique->checked = true;
                $this->qualification->idsQualification->Text = base64_encode($this->_consultation->getQualification());
                $this->qualification->libelleQualif->Text = base64_encode((new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($this->_consultation->getQualification()));
                $this->qualification->displayQualification();
                $this->agrements->idsSelectedAgrements->Value = $this->_consultation->getAgrements();

                if ($this->donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    if ('1' == $this->donneeComplementaire->getEchantillon()) {
                        $this->addEchantillion->Text = $this->donneeComplementaire->getAddEchantillon();
                        $this->dateLimiteEchantillion->Text = Atexo_Util::iso2frnDateTime($this->donneeComplementaire->getDateLimiteEchantillon());
                        $this->echantillonOui->checked = 'true';
                        $this->echantillonNon->checked = 'false';
                        $this->echantillonLayer->style = 'display:block';
                    } else {
                        $this->echantillonOui->checked = 'false';
                        $this->echantillonNon->checked = 'true';
                        $this->echantillonLayer->style = 'display:none';
                    }

                    if ('1' == $this->donneeComplementaire->getReunion()) {
                        $this->addReunion->Text = $this->donneeComplementaire->getAddReunion();
                        $this->dateReunion->Text = Atexo_Util::iso2frnDateTime($this->donneeComplementaire->getDateReunion());
                        $this->reunionOui->checked = 'true';
                        $this->reunionNon->checked = 'false';
                        $this->reunionLayer->style = 'display:block';
                    } else {
                        $this->reunionOui->checked = 'false';
                        $this->reunionNon->checked = 'true';
                        $this->reunionLayer->style = 'display:none';
                    }

                    if ('1' == $this->donneeComplementaire->getVisitesLieux()) {
                        $this->visiteLotOui->checked = 'true';
                        $this->visiteLotNon->checked = 'false';
                        $this->visiteLayer->style = 'display:block';
                    } else {
                        $this->visiteLotOui->checked = 'false';
                        $this->visiteLotNon->checked = 'true';
                        $this->visiteLayer->style = 'display:none';
                    }
                }
                $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($this->_consultation->getId(), '0', Atexo_CurrentUser::getCurrentOrganism());
                if ((is_countable($visitesLieux) ? count($visitesLieux) : 0) > 0) {
                    $this->addVisite->Text = '1';
                    $dataSource = [];
                    $i = 0;
                    foreach ($visitesLieux as $oneVisite) {
                        $dataSource[$i]['adresse'] = $oneVisite->getAdresse();
                        $dataSource[$i]['date'] = $oneVisite->getDate();
                        $dataSource[$i]['objetV'] = $oneVisite;
                        $dataSource[$i]['id'] = $i;
                        ++$i;
                    }
                    $this->setViewState('dataSourceVisite_0', $dataSource);
                    $this->visiteRepeater->DataSource = $dataSource;
                    $this->visiteRepeater->DataBind();

                    $j = 0;
                    foreach ($this->visiteRepeater->Items as $item) {
                        $tableau = $this->getViewState('dataSourceVisite_0');
                        if (is_array($tableau) && $tableau[$j]) {
                            $item->visiteLieuxLang->setChamps('adresse');
                            $item->visiteLieuxLang->setObject($tableau[$j]['objetV']);
                            $item->visiteLieuxLang->setIsObject(true);
                            $item->visiteLieuxLang->remplirRepeaterLang();
                        }
                        ++$j;
                    }
                }
                //              if ($this->_consultation->getVariantes() == "1") {
//                  $this->variantesOui->Checked = true;
//              } else {
//                  $this->variantesNon->Checked = true;
//              }
            } else {
                $this->idReferentielCheckLot->afficherTextConsultation($this->_consultation->getId(), null, Atexo_CurrentUser::getCurrentOrganism());
                $this->setViewState('referentielZoneTextOldLot', $this->idReferentielCheckLot->getValueReferentielLotTextVo());
                //ref radio lot
                $this->idReferentielRadioLot->afficherReferentielRadio($this->_consultation->getId(), null, Atexo_CurrentUser::getCurrentOrganism());
                $this->setViewState('referentielRadioOldLot', $this->idReferentielRadioLot->getValueReferentielRadioVo());
                $this->alloti->checked = true;
                $dataLots = $this->_consultation->getAllLots();
                $this->echantillonLotRepeater->DataSource = $dataLots;
                $this->echantillonLotRepeater->DataBind();

                $this->cautionProvisoireRepeater->DataSource = $dataLots;
                $this->cautionProvisoireRepeater->DataBind();

                $this->qualificationRepeater->DataSource = $dataLots;
                $this->qualificationRepeater->DataBind();

                $this->agrementsRepeater->DataSource = $dataLots;
                $this->agrementsRepeater->DataBind();

                $this->reunionLotRepeater->DataSource = $dataLots;
                $this->reunionLotRepeater->DataBind();

                $visite = $this->getVisiteLots($dataLots);
                $this->visiteLotRepeater->DataSource = $visite;
                $this->visiteLotRepeater->DataBind();
                $index = 0;

                foreach ($this->visiteLotRepeater->getItems() as $item) {
                    if ('1' == $item->haveVisite->Text) {
                        $item->visiteLotOui->checked = 'true';
                        $item->visiteLotNon->checked = 'false';
                        $item->visiteLayerPourUnLot->style = 'display:block';
                        if (is_array($visite[$index]['visiteLot'])) {
                            $item->visiteRepeater->DataSource = $visite[$index]['visiteLot'];
                            $item->visiteRepeater->DataBind();
                        }
                        $j = 1;
                        $i = 0;
                        foreach ($item->visiteRepeater->Items as $itemV) {
                            if ($visite[$index]['visiteLot'][$j]['objetV']) {
                                $itemV->visiteLieuxLotLang->setChamps('adresse');
                                $itemV->visiteLieuxLotLang->setObject($visite[$index]['visiteLot'][$j]['objetV']);
                                $itemV->visiteLieuxLotLang->setIsObject(true);
                                $itemV->visiteLieuxLotLang->remplirRepeaterLang();
                                ++$j;
                            } else {
                                $itemV->visiteLieuxLotLang->setChamps('adresse');
                                $itemV->visiteLieuxLotLang->setObject($visite[$index]['visiteLot'][$i]);
                                $itemV->visiteLieuxLotLang->setIsObject(false);
                                $itemV->visiteLieuxLotLang->remplirRepeaterLang();
                                ++$i;
                            }
                        }
                    } else {
                        $item->visiteLotOui->checked = 'false';
                        $item->visiteLotNon->checked = 'true';
                        $item->visiteLayerPourUnLot->style = 'display:none';
                    }
                    ++$index;
                }

                $this->variantesRepeater->DataSource = $dataLots;
                $this->variantesRepeater->DataBind();
            }
            if ($this->donneeComplementaire instanceof CommonTDonneeComplementaire) {
                $this->piecesDossierAdmin->Text = $this->donneeComplementaire->getPiecesDossierAdmin();
                $this->piecesDossierTech->Text = $this->donneeComplementaire->getPiecesDossierTech();
                $this->piecesDossierAdditif->Text = $this->donneeComplementaire->getPiecesDossierAdditif();
            }
            $this->annexeTechniqueDCE->displayListeDossierVolumineux($this->_consultation->getIdDossierVolumineux());
            $this->annexeTechniqueDCE->setUpdatemode(true);
        }
    }

    public function getDataVisite($infoVisite, $addVisite, $lot)
    {
        $data = [];
        $dataSource = [];
        $data = $this->getViewState('dataSourceVisite_'.$lot);
        $arrayInfo = explode('#_#', base64_decode($infoVisite));
        $count = 0;
        if (count($arrayInfo) > 1) {
            if ('1' == $addVisite) {
                if (is_array($data) && 0 != $data) {
                    $count = count($data);
                }
                if (0 != $lot) {
                    ++$count;
                }
                $dataSource[$count]['id'] = $count;
                $dataSource[$count]['adresse'] = $arrayInfo[0];
                $dataSource[$count]['date'] = $arrayInfo[1];
                $dataSource[$count]['objetV'] = null;
                $dataSource = Atexo_Util::arrayMerge($data, $dataSource);
            } else {
                $dataSource = $data;
                $ligne = $dataSource[$arrayInfo[2]];
                $dataSource[$ligne['id']]['adresse'] = $arrayInfo[0];
                $dataSource[$ligne['id']]['date'] = $arrayInfo[1];
                $dataSource[$ligne['id']]['objetV'] = $data[$ligne['id']]['objetV'];
            }
            $this->setViewState('dataSourceVisite_'.$lot, $dataSource);
        } else {
            $dataSource = $data;
        }

        return $dataSource;
    }

    /**
     * affiche la liste des lieux.
     */
    public function displayLieuxVisiteLot()
    {
        $lot = 1;
        $this->visiteLayerLot->setDisplay('Dynamic');
        foreach ($this->visiteLotRepeater->getItems() as $item) {
            $infoVisite = $item->infoVisite->Text;
            $addVisite = '1' == $item->addVisite->Text;
            $dataSource = $this->getDataVisite($infoVisite, $addVisite, $lot);
            $item->visiteRepeater->DataSource = $dataSource;
            $item->visiteRepeater->DataBind();
            $j = 1;
            foreach ($item->visiteRepeater->Items as $itemV) {
                $tableau = $dataSource;
                if (is_array($tableau) && $tableau[$j]['objetV']) {
                    $itemV->visiteLieuxLotLang->setChamps('adresse');
                    $itemV->visiteLieuxLotLang->setObject($tableau[$j]['objetV']);
                    $itemV->visiteLieuxLotLang->setIsObject(true);
                    $itemV->visiteLieuxLotLang->remplirRepeaterLang();
                }
                ++$j;
            }
            if ((is_countable($dataSource) ? count($dataSource) : 0) > 0) {
                $item->haveVisite->Text = '1';
                $item->visiteLotOui->checked = 'true';
                $item->visiteLotNon->checked = 'false';
                $item->visiteLayerPourUnLot->style = 'display:block';
                $item->deletingAllVisite->Text = '1';
            } else {
                $item->visiteLotOui->checked = 'false';
                $item->visiteLotNon->checked = 'true';
                $item->visiteLayerPourUnLot->style = 'display:none';
                $item->deletingAllVisite->Text = '0';
            }
            $item->infoVisite->Text = '';
            ++$lot;
        }
    }

    public function deleteVisiteLieuxLot($sender, $param)
    {
        $arrayParam = explode('-', $param->CommandName);
        $idElement = $arrayParam['0'];
        $lot = $arrayParam['1'] + 1;
        $data = $this->getViewState('dataSourceVisite_'.$lot);
        unset($data[$idElement]);
        $this->setViewState('dataSourceVisite_'.$lot, $data);
    }

    public function deleteVisiteLieuxCons($sender, $param)
    {
        $idElement = $param->CommandName;
        $data = $this->getViewState('dataSourceVisite_0');
        unset($data[$idElement]);
        $this->setViewState('dataSourceVisite_0', $data);
    }

    /**
     * rafrechir le panel des visites d'un lot.
     */
    public function onCallBackVisiteLot($sender, $param)
    {
        $this->displayLieuxVisiteLot();
        $this->visiteLayerLot->render($param->NewWriter);
    }

    public function onCallBackDeleteVisiteCons($sender, $param)
    {
        $this->displayLieuxVisiteCons();
        $this->visiteLayer->render($param->NewWriter);
    }

    /**
     * affiche la liste des lieux.
     */
    public function displayLieuxVisiteCons()
    {
        $addVisite = $this->addVisite->Text;
        $infoVisite = $this->infoVisite->Text;
        $dataSource = $this->getDataVisite($infoVisite, $addVisite, '0');
        $this->visiteRepeater->DataSource = $dataSource;
        $this->visiteRepeater->DataBind();
        if ((is_countable($dataSource) ? count($dataSource) : 0) > 0) {
            //$this->visiteLotOui->Checked = true;
            $this->visiteLayer->setDisplay('Dynamic');
            $this->infoVisite->Text = '';
            $this->deletingAllVisite->Text = '1';
            $j = 0;
            foreach ($this->visiteRepeater->Items as $item) {
                $tableau = $dataSource;
                if (is_array($tableau) && $tableau[$j]['objetV']) {
                    $item->visiteLieuxLang->setChamps('adresse');
                    $item->visiteLieuxLang->setObject($tableau[$j]['objetV']);
                    $item->visiteLieuxLang->setIsObject(true);
                    $item->visiteLieuxLang->remplirRepeaterLang();
                }
                ++$j;
            }
        } else {
            $this->visiteLayer->setDisplay('Dynamic');
            $this->visite->setChecked(false);
            $this->deletingAllVisite->Text = '0';
        }
    }

    public function onCallBackLieuxVisiteCons($sender, $param)
    {
        $this->displayLieuxVisiteCons();
        $this->visiteLayer->render($param->NewWriter);
    }

    public function setHistoriquesConsultation($consultationId, $nomElement, $valeur, $detail1, $detail2, $lot)
    {
        $newHistorique = new CommonHistoriquesConsultation();
        $newHistorique->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $newHistorique->setConsultationId($consultationId);
        $newHistorique->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
        $newHistorique->setNomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
        $newHistorique->setPrenomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
        $newHistorique->setNomElement($nomElement);
        $newHistorique->setValeur($valeur);
        $newHistorique->setValeurDetail1($detail1);
        $newHistorique->setValeurDetail2($detail2);
        $newHistorique->setNumeroLot($lot);
        $newHistorique->setStatut(Atexo_Config::getParameter('REMPLACEMENT_FILE'));
        $atexoCrypto = new Atexo_Crypto();
        $arrayTimeStamp = $atexoCrypto->timeStampData($valeur.'-'.$detail1.'-'.$detail2);
        $newHistorique->setHorodatage($arrayTimeStamp['horodatage']);
        $newHistorique->setUntrusteddate($arrayTimeStamp['untrustedDate']);
        $newHistorique->save($this->_connexionCom);
    }

    /**
     * rafrechir le panel des domaines d'activités.
     */
    public function refreshComposants($sender, $param)
    {
        $this->panelAgrementsLot->render($param->NewWriter);
        $this->qualification->panelQualification->render($param->NewWriter);
    }

    public function getVisiteLots($dataLots)
    {
        $index = 0;
        $dataVisite = [];
        foreach ($dataLots as $unLot) {
            $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($unLot->getConsultationId(), $unLot->getLot(), Atexo_CurrentUser::getCurrentOrganism());
            if ((is_countable($visitesLieux) ? count($visitesLieux) : 0) > 0) {
                $dataSource = [];
                $i = 1;
                foreach ($visitesLieux as $oneVisite) {
                    $dataSource[$i]['adresse'] = $oneVisite->getAdresse();
                    $dataSource[$i]['date'] = $oneVisite->getDate();
                    $dataSource[$i]['objetV'] = $oneVisite;
                    $dataSource[$i]['id'] = $i;
                    ++$i;
                }
                $dataVisite[$index]['visiteLot'] = $dataSource;
                $dataVisite[$index]['numLot'] = $unLot->getLot();
                $dataVisite[$index]['haveVisite'] = $unLot->getVisitesLieux();
                $item = $index + 1;
                $this->setViewState('dataSourceVisite_'.$item, $dataSource);
            } else {
                $dataVisite[$index]['visiteLot'] = [];
                $dataVisite[$index]['numLot'] = $unLot->getLot();
                $dataVisite[$index]['haveVisite'] = $unLot->getVisitesLieux();
                $item = $index + 1;
                $this->setViewState('dataSourceVisite_'.$item, []);
            }
            ++$index;
        }

        return $dataVisite;
    }

    public function displayAllotissement()
    {
        if ('0' == $this->_consultation->getAlloti()) {
            $this->echantillonConsultation->SetVisible(true);
            $this->echantillonLot->SetVisible(false);
            $this->panelCautionProvisoireCons->SetVisible(true);
            $this->panelQualifCons->SetVisible(true);
            $this->panelAgrementsLot->SetVisible(true);
            $this->reunionConsultation->SetVisible(true);
            $this->reunionLot->SetVisible(false);

            $this->visiteLayerLot->SetVisible(false);
            $this->visiteLayer->SetVisible(true);

            $this->variantesLot->SetVisible(false);
            $this->variantesConsultation->SetVisible(true);
        } else {
            $this->echantillonConsultation->SetVisible(false);
            $this->echantillonLot->SetVisible(true);
            $this->panelCautionProvisoireCons->SetVisible(false);
            $this->panelQualifCons->SetVisible(false);
            $this->panelAgrementsLot->SetVisible(false);
            $this->reunionLot->SetVisible(true);
            $this->reunionConsultation->SetVisible(false);

            $this->visiteLayerLot->SetVisible(true);
            $this->visiteLayer->SetVisible(false);

            $this->variantesLot->SetVisible(true);
            $this->variantesConsultation->SetVisible(false);
        }
    }

    /**
     * retourne true si c'est une consultation allotie.
     */
    public function isConsultationAllotie()
    {
        if ('1' == $this->_consultation->getAlloti()) {
            return true;
        } else {
            return false;
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    public function onCallBackRefreshTableau($sender, $param)
    {
        $this->tableauPiecesDCE->displayPiecesDCE();
        $this->tableauPiecesReglement->displayPiecesReglement();
        if ($sender) {
            $this->tableauPiecesDCE->panelListePiecesDCE->render($param->getNewWriter());
            $this->tableauPiecesReglement->panelListePiecesDCE->render($param->getNewWriter());
        }
    }

    public function remplacerDocReglement($infileRg, $fileName, $composantDCE)
    {
        $atexoBlob = new Atexo_Blob();
        $atexoCrypto = new Atexo_Crypto();
        $rgO = new CommonRG();
        if ('' != $infileRg && '' != $fileName) {
            $arrayTimeStampRg = $atexoCrypto->timeStampFile($infileRg);
            if (is_array($arrayTimeStampRg)) {
                $rgO->setHorodatage($arrayTimeStampRg['horodatage']);
                $rgO->setUntrusteddate($arrayTimeStampRg['untrustedDate']);
                $rgO->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $rgO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                $rgIdBlob = $atexoBlob->insert_blob($fileName, $infileRg, Atexo_CurrentUser::getCurrentOrganism(), null, Atexo_Config::getParameter('EXTENSION_RC'));
                $composantDCE->_infileRg = $rgIdBlob;
                $rgO->setRg($rgIdBlob);
                $rgO->setNomFichier($fileName);
                $rgO->setStatut(Atexo_Config::getParameter('REMPLACEMENT_FILE'));
                if ('0' != $rgIdBlob) {
                    $this->_consultation->addCommonRG($rgO);
                    $this->setConsultationChanged(true);
                }
            }
        }
    }

    public function createEchangeDCE($listeFiles)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echange = new CommonEchange();
        $echange->setIdCreateur(Atexo_CurrentUser::getId());
        $echange->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());

        $idTypeMsg = Atexo_Config::getParameter('ID_MESSAGE_MODIFICATION_CONSULTATION');
        $res = (new Atexo_Message())->remplirObjetEtCorpsMessage(Atexo_CurrentUser::getCurrentOrganism(), $this->_consultation->getId(), $idTypeMsg);
        $echange->setObjet($res['objet']);
        $echange->setCorps($res['corps']);
        $echange->setIdTypeMessage($idTypeMsg);
        $tabMails = (new Atexo_Message())->getAllAdressesRegistres($this->_consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
        $listeDestinataires = implode(' , ', $tabMails);
        $echange->setDestinataires($listeDestinataires);
        $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE'));
        $atexoBlob = new Atexo_Blob();

        foreach ($listeFiles as $file) {
            $echangePj = new CommonEchangePieceJointe();
            $fileName = $file['nameFile'];
            $tmpPath = $file['pathFile'];
            $echangePjIdBlob = $atexoBlob->insert_blob($fileName, $tmpPath, Atexo_CurrentUser::getOrganismAcronym());
            if ($echangePjIdBlob) {
                $echangePj->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $echangePj->setPiece($echangePjIdBlob);
                $echangePj->setTaille((new Atexo_Blob())->getTailFile($echangePjIdBlob, Atexo_CurrentUser::getOrganismAcronym()));
                $echangePj->setNomFichier($fileName);
                $echange->addCommonEchangePieceJointe($echangePj);
            }
        }
        if ($echange->save($connexionCom)) {
            $this->Page->idMessage->setValue($echange->getId());
        }
    }

    public function setActivationFuseauHoraire($value)
    {
        $this->_activationFuseauHoraire = $value;
    }

    public function getActivationFuseauHoraire()
    {
        return $this->_activationFuseauHoraire;
    }

    /**
     * Permet de remplir la date limite de remise des plis locale.
     */
    public function remplirDateLimiteRemisePlisLocale($sender, $param)
    {
        //Calcul de la date et heure limite de remise des plis locale
        $dateLimiteRemisePlisLocale = (new Atexo_Consultation())->calculDateLimiteRemisePlisLocale($this->dateRemisePlis->Text, $this->decalageFuseauHoraireService->value);
        //Remplissage de la zone de saisie de la date limite de remise des plis locale
        $this->dateRemisePlisLocale->setText(Atexo_Util::iso2frnDateTime($dateLimiteRemisePlisLocale, true));
    }

    /**
     * Permet de calculer la date de remise des plis heure locale.
     *
     * @param $consultation: l'objet consultation
     *
     * @return : la date limite de remise des plis heure Locale
     */
    public function getDateRemisePlisLocale($consultation)
    {
        if ($consultation->getDatefinLocale() && '0000-00-00 00:00:00' != $consultation->getDatefinLocale()) {
            return Atexo_Util::iso2frnDateTime(substr($consultation->getDatefinLocale(), 0, 16));
        } else {
            $dateLimiteRemisePlisLocale = (new Atexo_Consultation())->calculDateLimiteRemisePlisLocale($this->dateRemisePlis->Text, $this->decalageFuseauHoraireService->value);

            return Atexo_Util::iso2frnDateTime($dateLimiteRemisePlisLocale, true);
        }
    }

    /**
     * Permet de verifier le DCE.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifiyDce($sender, $param)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
            $composantDCE = $this->dceComposant;
        } else {
            $composantDCE = $this->blocDce;
        }
        $error = false;
        $errorMessage = '';
        if (!$this->verifiyExtensionDce($composantDCE)) {
            $error = true;
            $errorMessage = Prado::localize('ERREUR_FICHIER_DCE');
        } elseif (!$this->verifyDceIntegrity($composantDCE)) {
            $error = true;
            $errorMessage = Prado::localize('MSG_ERREUR_ZIP_DCE_INVALIDE');
        }
        if ($error) {
            $param->IsValid = false;
            $scriptJs = "document.getElementById('divValidationSummary').style.display='';";
            $this->divValidationSummary->addServerError($errorMessage, false);
            $this->javascript->Text = "<script>$scriptJs</script>";
        }
    }

    /**
     * Permet de verifier l'integrite du DCE.
     *
     * @param le composant DCE
     *
     * @return bool true si le dce a l'extension zip si non on retourne false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifyDceIntegrity($composantDCE)
    {
        return $composantDCE->verifyDceIntegrity();
    }

    /**
     * Permet de verifier l'extension du DCE.
     *
     * @param le composant DCE
     *
     * @return bool true si le dce a l'extension zip si non on retourne false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifiyExtensionDce($composantDCE)
    {
        return $composantDCE->verifiyExtensionDce();
    }

    /**
     * @throws PropelException
     */
    public function isDossierVolumineuxEnabled(): bool
    {
        $idAgent = Atexo_CurrentUser::getId();

        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        /** @var CommonHabilitationAgent $habilitationAgent */
        $habilitationAgent = CommonHabilitationAgentPeer::retrieveByPK($idAgent, $connexion);

        return (
            Atexo_Module::isEnabled('ModuleEnvol')
            && ($habilitationAgent instanceof CommonHabilitationAgent && $habilitationAgent->getGestionEnvol())
            && $this->_consultation->isEnvolActivation()
        );
    }

    public function isSendMessage(): bool
    {
        $idAgent = Atexo_CurrentUser::getId();
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        $habilitationAgent = CommonHabilitationAgentPeer::retrieveByPK($idAgent, $connexion);
        return (
            $habilitationAgent instanceof CommonHabilitationAgent && (bool) $habilitationAgent->getEnvoyerMessage()
        );
    }
}
