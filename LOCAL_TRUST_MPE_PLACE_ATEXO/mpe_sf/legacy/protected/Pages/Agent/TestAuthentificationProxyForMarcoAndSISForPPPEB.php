<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;

class TestAuthentificationProxyForMarcoAndSISForPPPEB extends MpeTPage
{
    public function onLoad($param)
    {
        $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
        if (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE'])) {
            echo $headersArray['EXTERNALID'].'#'.$headersArray['USERTYPE'];
        } else {
            echo 'FAIL';
        }
        exit;
    }
}
