<?php

namespace Application\Pages\Agent;

use App\Service\LotCandidature;
use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use AtexoPdf\PdfGeneratorClient;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GestionRegistres extends MpeTPage
{
    private $_idReference;
    private $_consultation;
    private $_pictoActive = 'false';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        $this->panelMessageInformation->setMessage(Prado::localize('DEFINE_MSG_TYPE_REPONSE'));
        if ((Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsElectronique') || Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsElectronique') ||
          Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsElectronique') || Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier') ||
          Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier') || Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) ||
          (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreRetraitsPapier') || Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsPapier') ||
          Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreDepotsPapier') || Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreRetraitsElectronique') ||
          Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsElectronique') || Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreDepotsElectronique'))) {
            if (isset($_GET['id'])) {
                $this->_idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteria->setIdReference($this->_idReference);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

                $consultation = (new Atexo_Consultation())->search($criteria);
                if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                    $this->ConsultationSummary->setConsultation($consultation);
                    $this->_consultation = $consultation;
                    $dateAjourdhui = date('Y-m-d H:i:s');
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                        $this->etape1->SetId('etape0');
                    } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                        $this->etape1->SetId('etape1');
                    } elseif ($consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1') && $consultation->getDatefin() > $dateAjourdhui) {
                        $this->etape1->SetId('etape234');
                    } elseif (($consultation->getStatusConsultation() == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION'))) || ($consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1') && $consultation->getDatefin() <= $dateAjourdhui)) {
                        $this->etape1->SetId('etape34');
                    } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
                        $this->etape1->SetId('etape2');
                    } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                        $this->etape1->SetId('etape3');
                    } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                        $this->etape1->SetId('etape4');
                    }
                } else {
                    $this->ConsultationSummary->setWithException(false);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
                    $this->registreRetraisElectronique->setVisible(false);
                    $this->registreRetraisPapier->setVisible(false);
                    $this->registreQuestionsElectronique->setVisible(false);
                    $this->registreQuestionPapier->setVisible(false);
                    $this->registreDepotsElectronique->setVisible(false);
                    $this->registreDepotsPapier->setVisible(false);
                    $this->panelTableauRegistres->setVisible(false);
                    $this->etape1->setVisible(false);
                }
                $typeRegistre = Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE');
                if (isset($_GET['type'])) {
                    $typeRegistre = Atexo_Util::atexoHtmlEntities($_GET['type']);
                    if ($typeRegistre != Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') && $typeRegistre != Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')
                      && $typeRegistre != Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') && $typeRegistre != Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')
                      && $typeRegistre != Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') && $typeRegistre != Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                        $this->ConsultationSummary->setWithException(false);
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_TYPE_REGISTRE'));
                        $this->registreRetraisElectronique->setVisible(false);
                        $this->registreRetraisPapier->setVisible(false);
                        $this->registreQuestionsElectronique->setVisible(false);
                        $this->registreQuestionPapier->setVisible(false);
                        $this->registreDepotsElectronique->setVisible(false);
                        $this->registreDepotsPapier->setVisible(false);
                        $this->panelTableauRegistres->setVisible(false);
                        $this->etape1->setVisible(false);

                        return;
                    }
                }
                if (!$this->IsPostBack) {
                    $this->displayRegistres($typeRegistre);
                }
            } else {
                $this->ConsultationSummary->setWithException(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
                $this->registreRetraisElectronique->setVisible(false);
                $this->registreRetraisPapier->setVisible(false);
                $this->registreQuestionsElectronique->setVisible(false);
                $this->registreQuestionPapier->setVisible(false);
                $this->registreDepotsElectronique->setVisible(false);
                $this->registreDepotsPapier->setVisible(false);
                $this->panelTableauRegistres->setVisible(false);
                $this->etape1->setVisible(false);
            }
        } else {
            $this->ConsultationSummary->setWithException(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_REGISTRES'));
            $this->registreRetraisElectronique->setVisible(false);
            $this->registreQuestionsElectronique->setVisible(false);
            $this->registreQuestionPapier->setVisible(false);
            $this->registreDepotsElectronique->setVisible(false);
            $this->registreDepotsPapier->setVisible(false);
            $this->registreRetraisPapier->setVisible(false);
            $this->panelTableauRegistres->setVisible(false);
            $this->etape1->setVisible(false);
        }
    }

    public function displayActiveTab($typeRegistre)
    {
        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
          $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            $this->retraits->setCssClass('tab-on');
            $this->questions->setCssClass('tab');
            $this->depots->setCssClass('tab');
            $this->ongletLayer1->setVisible(true);
            $this->ongletLayer2->setVisible(false);
            $this->ongletLayer3->setVisible(false);
            $this->exportRetrait->setVisible(true);
            $this->exportQuestion->setVisible(false);
            $this->exportDepot->setVisible(false);
        }

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
          $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            $this->questions->setCssClass('tab-on');
            $this->retraits->setCssClass('tab');
            $this->depots->setCssClass('tab');
            $this->ongletLayer1->setVisible(false);
            $this->ongletLayer2->setVisible(true);
            $this->ongletLayer3->setVisible(false);
            $this->exportRetrait->setVisible(false);
            $this->exportQuestion->setVisible(true);
            $this->exportDepot->setVisible(false);
        }

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
          $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $this->depots->setCssClass('tab-on');
            $this->retraits->setCssClass('tab');
            $this->questions->setCssClass('tab');
            $this->ongletLayer1->setVisible(false);
            $this->ongletLayer2->setVisible(false);
            $this->ongletLayer3->setVisible(true);
            $this->exportRetrait->setVisible(false);
            $this->exportQuestion->setVisible(false);
            $this->exportDepot->setVisible(true);
        }
    }

    public function activeTabChanged($sender, $param)
    {
        $typeRegistre = match ($sender->ID) {
            'firstTab' => Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'),
            'secondTab' => Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'),
            'thirdTab' => Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'),
            default => throw new Atexo_Exception('type de registre inconnu'),
        };

        $this->displayRegistres($typeRegistre);
    }

    public function fillDataRetraitElectronique()
    {
        $this->registreRetraisElectronique->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'));
        $this->registreRetraisElectronique->setReference($this->_idReference);
        $this->registreRetraisElectronique->setVisible(true);
    }

    public function fillDataRetraitPapier()
    {
        $this->registreRetraisPapier->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'));
        $this->registreRetraisPapier->setReference($this->_idReference);
        $this->registreRetraisPapier->setVisible(true);
    }

    public function fillDataQuestionElectronique()
    {
        $this->registreQuestionsElectronique->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'));
        $this->registreQuestionsElectronique->setReference($this->_idReference);
        $this->registreQuestionsElectronique->setVisible(true);
    }

    public function fillDataQuestionPapier()
    {
        $this->registreQuestionPapier->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'));
        $this->registreQuestionPapier->setReference($this->_idReference);
        $this->registreQuestionPapier->setVisible(true);
    }

    public function fillDataDepotElectronique()
    {
        $this->registreDepotsElectronique->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));
        $this->registreDepotsElectronique->setReference($this->_idReference);
        $this->registreDepotsElectronique->setVisible(true);
    }

    public function fillDataDepotPapier()
    {
        $this->registreDepotsPapier->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'));
        $this->registreDepotsPapier->setReference($this->_idReference);
        $this->registreDepotsPapier->setVisible(true);
    }

    public function genererExcelRetrait()
    {
        (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'));
    }

    public function genererExcelQuestion()
    {
        (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'));
    }

    public function genererExcelDepot()
    {
        (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));
    }

    public function genererPdfRetrait()
    {
        $filePath = (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'), true);
        if ($filePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $nomFichier = Prado::localize('REGISTRE_RETRAIT_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf';
            @unlink($filePath);
            DownloadFile::downloadFileContent($nomFichier, $pdfContent);
        }
    }

    public function genererPdfQuestion()
    {
        $filePath = (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'), true);
        if ($filePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $nomFichier = Prado::localize('REGISTRE_QUESTION_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf';
            @unlink($filePath);
            DownloadFile::downloadFileContent($nomFichier, $pdfContent);
        }
    }

    public function genererPdfDepot()
    {
        $filePath = (new Atexo_GenerationExcel())->generateExcelRegistre(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'), true);
        if ($filePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($filePath);
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $nomFichier = Prado::localize('REGISTRE_DEPOT_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf';
            @unlink($filePath);
            DownloadFile::downloadFileContent($nomFichier, $pdfContent);
        }
    }

    public function isEtatArchiveRealiseConsulotation()
    {
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
            return true;
        } else {
            return false;
        }
    }

    public function hasHabilitationModifierApresValidation()
    {
        if ($this->_consultation->getCurrentUserReadOnly()) {
            return false;
        }
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE')) {
            return Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation');
        } else {
            return $this->_consultation->hasHabilitationModifierApresValidation();
        }
    }

    public function displayRegistres($typeRegistre)
    {
        $this->displayActiveTab($typeRegistre);
        if (($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
           $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'))) {
            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsElectronique')) {
                $this->_pictoActive = 'true';
                $this->fillDataRetraitElectronique();
                $this->registreRetraisElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreRetraitsElectronique')) {
                $this->_pictoActive = 'false';
                $this->fillDataRetraitElectronique();
                $this->registreRetraisElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'));
            } else {
                $this->registreRetraisElectronique->setVisible(false);
            }

            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier')) {
                $this->_pictoActive = 'true';
                $this->fillDataRetraitPapier();
                $this->registreRetraisPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreRetraitsPapier')) {
                $this->_pictoActive = 'false';
                $this->fillDataRetraitPapier();
                $this->registreRetraisPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'));
            } else {
                $this->registreRetraisPapier->setVisible(false);
            }
        }

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
           $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsElectronique')) {
                $this->_pictoActive = 'true';
                $this->fillDataQuestionElectronique();
                $this->registreQuestionsElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsElectronique')) {
                $this->_pictoActive = 'false';
                $this->fillDataQuestionElectronique();
                $this->registreQuestionsElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'));
            } else {
                $this->registreQuestionsElectronique->setVisible(false);
            }

            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                $this->_pictoActive = 'true';
                $this->fillDataQuestionPapier();
                $this->registreQuestionPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreQuestionsPapier')) {
                $this->_pictoActive = 'false';
                $this->fillDataQuestionPapier();
                $this->registreQuestionPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'));
            } else {
                $this->registreQuestionPapier->setVisible(false);
            }
        }
        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
           $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsElectronique')) {
                $this->_pictoActive = 'true';
                $this->fillDataDepotElectronique();
                $this->registreDepotsElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreDepotsElectronique')) {
                $this->_pictoActive = 'false';
                $this->fillDataDepotElectronique();
                $this->registreDepotsElectronique->fillDataList(Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));
            } else {
                $this->registreDepotsElectronique->setVisible(false);
            }

            if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                $this->_pictoActive = 'true';
                $this->fillDataDepotPapier();
                $this->registreDepotsPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'));
            } elseif (Atexo_CurrentUser::hasHabilitation('SuiviSeulRegistreDepotsPapier')) {
                $this->_pictoActive = 'false';
                $this->fillDataDepotPapier();
                $this->registreDepotsPapier->fillDataList(Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'));
            } else {
                $this->registreDepotsPapier->setVisible(false);
            }
        }
    }

    public function setPictoActive($pictoActive)
    {
        $this->_pictoActive = $pictoActive;
    }

    public function getPictoActive($typeRegistre)
    {
        switch ($typeRegistre) {
            case Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsElectronique')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            case Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            case Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsElectronique')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            case Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            case Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsElectronique')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            case Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'):
                if (Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier')) {
                    return 'true';
                } else {
                    return 'false';
                }

                // no break
            default:
                 return false;
        }
    }

    public function getTiragePlan()
    {
        return $this->_consultation->getTiragePlan();
    }

    public function getLotsUsedDepot(?int $idEntreprise, $idOffre = null): string
    {
        if (empty($idEntreprise) || !$this->_consultation->getAlloti())  {
            return '';
        }

        /**
         * @var LotCandidature $lotCandidatureService
         */
        $lotCandidatureService = Atexo_Util::getSfService(LotCandidature::class);
        $lots = [];
        foreach ($lotCandidatureService->getLotsCandidature($this->_consultation->getId(), $idEntreprise, $idOffre) as ["numLot" => $number]) {
            $lots[] =  $number;
        }

       return count($lots) > 0 ? 'Lot(s) ' . implode(', ', $lots) : '';
    }
}
