<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/**
 * Popup.
 *
 * @author Loubna Ezziani <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupRedacDCE extends MpeTPage
{
    public string $consultation = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
            $consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->composantAjouter->_consultation = $consultation[0];
            $this->listePiecesDCE->_consultation = $consultation[0];
            if (!$this->isPostBack) {
                $this->composantAjouter->displayDocsRedac();
                $this->listePiecesDCE->displayPieces();
            }
        }
    }

    /**
     * retourne un objet consultation.
     *
     * @param $consultationId
     */
    public function retrieveConsultation($consultationId)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($consultationId);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }

    public function onValide($sender, $param)
    {
        $msgChangeDCE = null;
        $source = '';
        $destination = Atexo_Config::getParameter('NOUVELLE_PIECE_RACINE_ZIP');
        $infileDce = '';
        $oldFile = '';
        $fileName = '';
        /* Source */
        if ($this->composantAjouter->pieceRedac->Checked) {
            foreach ($this->composantAjouter->repeaterDocsRedac->Items as $item) {
                if ($item->idPieceRedacCheck->checked) {
                    $source .= $item->idRedac->getValue().'|'.addslashes($item->nomRedac->getValue()).'|'.'1'.'#';
                    $fileName .= addslashes($item->nomRedac->getValue()).';';
                }
            }
        } elseif ($this->composantAjouter->pieceLibre->Checked && $this->composantAjouter->fileExterne->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->composantAjouter->fileExterne->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.' "'.$this->composantAjouter->fileExterne->FileName.'"');
                $this->composantAjouter->displayDocsRedac();
                $this->listePiecesDCE->displayPieces();

                return;
            }
            if ('1' == Atexo_Config::getParameter('IS_COMMON_TMP_SHARED')) {
                $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP_SHARED').'fileUploaded/'.session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
            } else {
                $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP').'fileUploaded/'.session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
            }
            Atexo_Files::createDir($pathDirectoryTmpDceFile);
            $infileDce = $pathDirectoryTmpDceFile.'tmpDceFile';
            if (!move_uploaded_file($this->composantAjouter->fileExterne->LocalName, $infileDce)) {
                return;
            }

            $u = new Atexo_Util();
            $name = (new Atexo_Config())->toPfEncoding($this->composantAjouter->fileExterne->FileName);
            $name = addslashes(utf8_decode(mb_convert_encoding($name, "UTF-8", "HTML-ENTITIES")));
            $fileName = $u->replaceSpecialCharacters(
                $name
            );

            $source = $infileDce.'|'.$fileName.'|'.'0'.'#';
        }

        /* Destination */
        if ($this->remplacerPieceDce->Checked) {
            foreach ($this->listePiecesDCE->repeaterPiecesDCE->Items as $item) {
                if ($item->idPieceRedac->checked) {
                    $destination = $item->idDocReplace->getValue();
                    $oldFile = addslashes($item->nomFile->Text);
                    break;
                }
            }
        } elseif ($this->pieceDCENew->Checked) {
            $destination = Atexo_Config::getParameter('NOUVELLE_PIECE_RACINE_ZIP');
        }
        if ($fileName) {
            if ($oldFile) {
                $msgChangeDCE = $oldFile.' '.Prado::localize('REPLACE_WITH').' '.$fileName;
            } else {
                $msgChangeDCE = Prado::localize('AJOUT_DES_FICHIERS').rtrim($fileName, ';');
            }
            if ($_GET['response']) {
                $this->scriptJS->text = "<script>getDCEFileChange('".$msgChangeDCE."','".Atexo_Util::atexoHtmlEntities(
                        $_GET['response']
                    )."');opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_paramSource').value ='".$source."';".
                    "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities(
                        $_GET['response']
                    )."_fileToScan').value = '".$this->composantAjouter->fileExterne->FileName."';".
                    "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_paramDestination').value ='".$destination."';".
                    "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_remplacerPieceDce').checked=true;".
                    "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_filesToDelete').value = '".$this->Page->getViewState(
                        'ListPiecesToDelete'
                    )."';".
                    "opener.document.getElementById('".Atexo_Util::atexoHtmlEntities($_GET['response'])."_fileToReplace').value = '".$oldFile."';".
                    'window.close();</script>';
            }
        } else {
            $this->afficherErreur(Prado::localize("MESSAGE_AVERTISSEMENT_PIECES_NON_SELECTIONNES"));
            $this->composantAjouter->displayDocsRedac();
            $this->listePiecesDCE->displayPieces();
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($msg);
    }
}
