<?php

namespace Application\Pages\Agent;

use App\Message\GenerationArchiveDocumentaire;
use App\Service\Crypto\WebServicesCrypto;
use App\Service\MessageBusService;
use App\Utils\Encryption;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCategorieConsultationQuery;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFormulaire;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonCriteresEvaluation;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluation;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulaire;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_ActeDEngagement;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_JmsJobs;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\DossierVolumineux\Atexo_DossierVolumineux_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_ImpressionODT;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use AtexoDume\Exception\ServiceException;
use AtexoPdf\PdfGeneratorClient;
use AtexoPdf\RtfGeneratorClient;
use DOMDocument;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author     adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright  Atexo 2008
 *
 * @version    1.0
 *
 * @since      MPE-3.0
 */
class ouvertureEtAnalyse extends MpeTPage
{
    const APPID = 'DOSSIER_VOLUMINEUX_DOWNLOAD';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        MessageStatusCheckerUtil::initEntryPoints('page-decision-contrat');
    }

    private $_idService;
    public $_consultation;
    public $_refConsultation;
    public $_thColspan;
    public $_enveloppeCandidatureExist;
    public $_eneloppeOffreTechniqueExist;
    public $_eneloppeOffreExist;
    public $_eneloppeAnonymatExist;
    public $linkAnalysisRankingOffre;

    public $siretAlertFA;
    public $siretAlertMsg;

    /**
     * @return mixed
     */
    public function getSiretAlertFA()
    {
        return $this->siretAlertFA;
    }

    /**
     * @return mixed
     */
    public function getSiretAlertMsg()
    {
        return $this->siretAlertMsg;
    }

    /**
     * @param  $etablissement
     * @param  $entreprise
     *
     * @return string|null
     */
    public function getSiretAlertDiv($etablissement, $entreprise)
    {
        $siretAlert = Atexo_Util::getSiretAlertDiv($etablissement, $entreprise);
        if (empty($siretAlert)) {
            $res = '';
        } else {
            $res = '<span class="fa fa-' . $siretAlert['siretAlertFA'] . '-circle ' . $siretAlert['color'] . ' tTip_suite disable-siret" title="' . Prado::localize('VERIFICATION_DU_SIRET') . '"></span>&nbsp;';
        }

        return $res;
    }

    public function onLoad($param)
    {
        if ($this->isMAMPEnabled()) {
            MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
            MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
        }

        $this->exportDepot->setVisible(true);

        if (isset($_GET['telechargerOffres'])) {
            //$this->_thColspan='6';
            $this->setViewState('thColspan', '6');
        } else {
            $this->setViewState('thColspan', '8');
        }

        $this->panelMessageErreur->setVisible(false);
        if (!$this->getIsCallback()) {
            $this->panelMessageErreurDechiffrement->hideMessage();
        }
        $this->showDepouillement->setVisible(true);
        $this->exportDepot->setVisible(true);
        if (isset($_GET['telechargerOffres']) && !Atexo_CurrentUser::hasHabilitation('TelechargementUnitairePlisChiffres')) {
            $this->ConsultationSummary->setWithException(false);
            $this->consultationReplyTerms->setWithException(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_TELECHARER_PLI_PAR_PLI'));
            $this->showDepouillement->setVisible(false);
            $this->exportDepot->setVisible(false);
            $this->exportDecision->setVisible(false);
        }
        $launchSearch = true;
        if (isset($_GET['refConsultation'])) {
            $id = Atexo_Util::atexoHtmlEntities($_GET['refConsultation']);
            $org = Atexo_CurrentUser::getOrganismAcronym();
            $serviceSF = Atexo_Util::getSfService('app.mapping.consultation');
            if ($serviceSF) {
                $idConsultation = $serviceSF->getConsultationId($id, $org);
                $_GET['id'] = $idConsultation;
                $idConsultation = -1;
                if (-1 == $idConsultation) {
                    $launchSearch = false;
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(
                        Prado::localize('MESSAGE_ERREUR_DEUX_CONSULTATIONS_EN_COLLISION')
                    );
                }
            }
        }


        if (isset($_GET['id']) && true === $launchSearch) {
            $this->javascript->Text = sprintf(
                "<script>isBtnAnalysisEnabled(%s)</script>",
                (int)Atexo_CurrentUser::hasHabilitation('EspaceDocumentaireConsultation')
            );

            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->setViewState('organisme', Atexo_CurrentUser::getCurrentOrganism());

            $this->_idService = Atexo_CurrentUser::getCurrentServiceId();
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService($this->_idService);
            $criteria->setAcronymeOrganisme($this->getViewState('organisme'));
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
                $this->_consultation = array_shift($consultation);

                if (
                    $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')
                ) {
                    $this->ConsultationSummary->setConsultation($this->_consultation);
                    $this->consultationReplyTerms->setConsultation($this->_consultation);

                    if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                        $this->etape3->setId('etape234');
                    } elseif ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')) {
                        $this->etape3->setId('etape34');
                    } elseif ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                        $this->etape3->setId('etape4');
                    }
                    if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                        $atexoConsultationResponses = new Atexo_Consultation_Responses();
                        $constitutionDossierReponse = $atexoConsultationResponses->getTypesEnveloppes(
                            $this->_consultation->getEnvCandidature(),
                            $this->_consultation->getEnvOffre(),
                            $this->_consultation->getEnvAnonymat(),
                            $this->_consultation->getEnvOffreTechnique()
                        );

                        $this->_enveloppeCandidatureExist = $atexoConsultationResponses->enveloppeCandidatureExists($constitutionDossierReponse);
                        $this->_eneloppeOffreTechniqueExist = $atexoConsultationResponses->enveloppeOffreTechniqueExists($constitutionDossierReponse);
                        $this->_eneloppeOffreExist = $atexoConsultationResponses->enveloppeOffreExists($constitutionDossierReponse);
                        $this->_eneloppeAnonymatExist = $atexoConsultationResponses->enveloppeAnonymatExists($constitutionDossierReponse);
                    }
                    if (!$this->IsPostBack) {
                        Atexo_CurrentUser::writeToSession('PageSansApplet', false);
                        if (isset($_GET['decision'])) {
                            Atexo_CurrentUser::writeToSession('PageSansApplet', true);
                        }
                        $lotsConsultation = $this->_consultation->getAllLots();
                        $nombreLot = count($lotsConsultation);
                        if (1 == $nombreLot) {
                            $nombreLot = 0;
                        }
                        if (!is_array($lotsConsultation)) {
                            throw new Atexo_Exception('Expected function to return an array');
                        }
                        $this->setViewState('nombreLot', $nombreLot);
                        $this->setViewState('offreSigne', $this->_consultation->getSignatureOffre());
                        $this->setViewState('acteEngagement', $this->_consultation->getSignatureActeEngagement());
                        $this->setViewState('chiffrementOffre', $this->_consultation->getChiffrementOffre());

                        $this->setViewState('withCandidature', $this->_consultation->getEnvCandidature());
                        $this->setViewState('withOffreTechnique', $this->_consultation->getEnvOffreTechnique());
                        $this->setViewState('withOffre', $this->_consultation->getEnvOffre());
                        $this->setViewState('withAnonymat', $this->_consultation->getEnvAnonymat());

                        if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                            $this->setViewState('typesEnveloppes', $constitutionDossierReponse);
                            $this->setViewState('dateFin', $this->_consultation->getDatefin());

                            $this->ongletLayer0->setVisible(false);
                            $this->ongletLayer1->setVisible(false);
                            $this->ongletLayer2->setVisible(false);
                            $this->ongletLayer3->setVisible(false);
                            $this->ongletLayer4->setVisible(false);
                            $this->ongletLayer5->setVisible(false);

                            $this->reponses->setVisible(false);
                            $this->candidature->setVisible(false);
                            $this->offre->setVisible(false);
                            $this->offreTechnique->setVisible(false);
                            $this->anonymat->setVisible(false);
                            $this->decision->setVisible(false);

                            if (Atexo_Module::isEnabled('AvisMembresCommision')) {
                                $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->getViewState('organisme'));
                                if (is_array($guests) && count($guests)) {
                                    foreach ($guests as $guest) {
                                        $idGuest = $guest->getId();
                                        if ($guest->getPermanentGuest() || (!$guest->getPermanentGuest() && 1 == $guest->getTypeInvitation())) {
                                            $hasHbilitation = (new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste($idGuest, 'AccesReponses');
                                            if ($hasHbilitation) {
                                                $arayIdsGuests[] = $idGuest;
                                            }
                                        }
                                    }
                                }
                                $this->setViewState('guestsWithEnabling', $arayIdsGuests);
                            }

                            if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_enveloppeCandidatureExist) {
                                $this->candidature->setVisible(true);
                            }
                            if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreExist) {
                                $this->offre->setVisible(true);
                            }
                            if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreTechniqueExist) {
                                $this->offreTechnique->setVisible(true);
                            }
                            if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeAnonymatExist) {
                                $this->anonymat->setVisible(true);
                            }
                            if ((Atexo_Module::isEnabled('InterfaceModuleSub') || $this->_consultation->getModeOuvertureReponse())) {
                                $this->fillRepeaterReponses();
                                $this->ongletLayer0->setVisible(true);
                                $this->reponses->setVisible(true);
                                $this->reponses->setCssClass('tab-on');
                            } elseif (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_enveloppeCandidatureExist) {
                                $this->fillRepeatersCandidature();
                                $this->ongletLayer1->setVisible(true);

                                $this->candidature->setVisible(true);
                                $this->candidature->setCssClass('tab-on');

                                $this->pvButton->setVisible(false);
                            } elseif (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreTechniqueExist) {
                                $currentTypeEnveloppe = Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
                                $this->fillLotList($currentTypeEnveloppe, $constitutionDossierReponse);

                                $this->fillRepeatersOffreTechnique();
                                $this->ongletLayer5->setVisible(true);

                                $this->offreTechnique->setVisible(true);
                                $this->offreTechnique->setCssClass('tab-on');
                                $this->pvButton->setVisible(false);
                            } elseif (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreExist) {
                                $currentTypeEnveloppe = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
                                $this->fillLotList($currentTypeEnveloppe, $constitutionDossierReponse);

                                $this->fillRepeatersOffre();

                                $this->ongletLayer2->setVisible(true);

                                $this->offre->setVisible(true);
                                $this->offre->setCssClass('tab-on');
                                $this->pvButton->setVisible(true);
                            } elseif (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeAnonymatExist) {
                                $currentTypeEnveloppe = Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
                                $this->fillLotList($currentTypeEnveloppe, $constitutionDossierReponse);

                                $this->fillRepeatersAnonymat();

                                $this->ongletLayer3->setVisible(true);

                                $this->anonymat->setVisible(true);
                                $this->anonymat->setCssClass('tab-on');
                                $this->pvButton->setVisible(false);
                            }

                            if (
                                isset($_GET['decision']) && (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul'))
                                && ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                                    || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION'))
                            ) {
                                $this->ongletLayer4->setVisible(true);
                                $this->ongletLayer3->setVisible(false);
                                $this->ongletLayer2->setVisible(false);
                                $this->ongletLayer1->setVisible(false);
                                $this->ongletLayer5->setVisible(false);
                                $this->ongletLayer0->setVisible(false);
                                $this->reponses->setCssClass('tab');
                                $this->candidature->setCssClass('tab');
                                $this->offreTechnique->setCssClass('tab');
                                $this->offre->setCssClass('tab');
                                $this->anonymat->setCssClass('tab');
                                $this->decision->setCssClass('tab-on');

                                $this->fillRepeatersDecision();
                                $this->exportDepot->setVisible(false);
                            } elseif (isset($_GET['decision'])) {
                                $this->ConsultationSummary->setWithException(false);
                                $this->consultationReplyTerms->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_DECISION'));
                                $this->exportDepot->setVisible(false);
                                $this->showDepouillement->setVisible(false);
                                $this->exportDecision->setVisible(false);
                            }
                        } else {
                            if (isset($_GET['decision']) && (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul'))) {
                                $this->ongletLayer4->setVisible(true);
                                $this->ongletLayer3->setVisible(false);
                                $this->ongletLayer2->setVisible(false);
                                $this->ongletLayer1->setVisible(false);
                                $this->ongletLayer5->setVisible(false);
                                $this->ongletLayer0->setVisible(false);
                                $this->reponses->setCssClass('tab');
                                $this->candidature->setCssClass('tab');
                                $this->offre->setCssClass('tab');
                                $this->anonymat->setCssClass('tab');
                                $this->decision->setCssClass('tab-on');
                                $this->fillRepeatersDecision();
                            } elseif (isset($_GET['decision'])) {
                                $this->ConsultationSummary->setWithException(false);
                                $this->consultationReplyTerms->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_HABILITATION_DECISION'));
                                $this->exportDepot->setVisible(false);
                                $this->showDepouillement->setVisible(false);
                            } else {
                                $this->ConsultationSummary->setWithException(false);
                                $this->consultationReplyTerms->setWithException(false);
                                $this->panelMessageErreur->setVisible(true);
                                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DROIT_OA'));
                                $this->exportDepot->setVisible(false);
                                $this->showDepouillement->setVisible(false);
                            }
                        }
                    }
                    if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_enveloppeCandidatureExist) {
                        $this->candidature->setVisible(true);
                        $this->candidatureEtOffreEtAnonymat->setVisible(false);
                        $this->candidatureEtOffre->setVisible(false);
                        $this->candidatureSeul->setVisible(true);
                        $this->candidatureNonSeul->setVisible(false);
                        $this->candidatureEtOffreNonSeul->setVisible(false);
                    } else {
                        $this->candidature->setVisible(false);
                    }
                    //tgr
                    if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreTechniqueExist) {
                        $this->offreTechnique->setVisible(true);
                        $this->candidatureEtOffreEtAnonymat->setVisible(false);
                        $this->candidatureEtOffre->setVisible(false);
                        $this->candidatureEtOffreNonSeul->setVisible(false);
                        $this->offreTechniqueDernier->setVisible(true);
                    } else {
                        $this->offreTechnique->setVisible(false);
                    }
                    if (!Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_eneloppeOffreExist) {
                        $this->offre->setVisible(true);
                        $this->candidatureEtOffreEtAnonymat->setVisible(false);
                        $this->candidatureEtOffre->setVisible(true);
                        $this->candidatureSeul->setVisible(false);
                        $this->candidatureEtOffreNonSeul->setVisible(false);

                        if (Atexo_Module::isEnabled('GestionModelesFormulaire')) {
                            $this->candidatureNonSeul->setVisible(true);
                        }

                        $this->offreTechniqueDernier->setVisible(false);
                    } else {
                        $this->offre->setVisible(false);
                    }
                    if ($this->_eneloppeAnonymatExist) {
                        $this->anonymat->setVisible(true);
                        $this->candidatureEtOffreEtAnonymat->setVisible(true);
                        $this->candidatureEtOffre->setVisible(false);
                        $this->candidatureSeul->setVisible(false);

                        if (Atexo_Module::isEnabled('GestionModelesFormulaire')) {
                            $this->candidatureNonSeul->setVisible(true);
                            $this->candidatureEtOffreNonSeul->setVisible(true);
                        }

                        $this->offreTechniqueDernier->setVisible(false);
                    } else {
                        $this->anonymat->setVisible(false);
                    }
                    if (
                        (Atexo_CurrentUser::hasHabilitation('AttributionMarche') || Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul')) && isset($_GET['decision'])
                        && ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                            || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')
                            || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION'))
                    ) {
                        $this->decision->setVisible(true);
                        if (Atexo_Module::isEnabled('ExportDecision')) {
                            $this->exportDecision->setVisible(true);
                        }
                    } else {
                        $this->decision->setVisible(false);
                    }
                } else {
                    $this->ConsultationSummary->setWithException(false);
                    $this->consultationReplyTerms->setWithException(false);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_CONSULTATION_PAS_OA'));
                    $this->exportDepot->setVisible(false);
                    $this->showDepouillement->setVisible(false);
                    $this->exportDecision->setVisible(false);
                }
            } else {
                $this->ConsultationSummary->setWithException(false);
                $this->consultationReplyTerms->setWithException(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_CONSULTATION_PAS_OA'));
                $this->exportDepot->setVisible(false);
                $this->showDepouillement->setVisible(false);
                $this->exportDecision->setVisible(false);
            }
        } else {
            $this->ConsultationSummary->setWithException(false);
            $this->consultationReplyTerms->setWithException(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_AUCUNE_CONSULTATION'));
            $this->exportDepot->setVisible(false);
            $this->showDepouillement->setVisible(false);
            $this->exportDecision->setVisible(false);
        }
        if (!empty($this->_consultation) && $this->isMAMPEnabled()) {
            if ($this->_consultation->getModeOuvertureReponse() == Atexo_Config::getParameter('MODE_OUVERTURE_REPONSE')) {
                $this->assistantMarchePublicBlocReponse->setVisible(true);
                $this->assistantMarchePublicBlocCandidatures->setVisible(true);
                $this->assistantMarchePublicBlocCandidaturesEtOffres->setVisible(true);
                $this->assistantMarchePublicBlocOffre->setVisible(false);
                $this->assistantMarchePublicBlocAnonymat->setVisible(false);
                $this->assistantMarchePublicBlocEnveloppe->setVisible(false);
            } else {
                $this->assistantMarchePublicBlocReponse->setVisible(false);
                $this->assistantMarchePublicBlocCandidatures->setVisible(false);
                $this->assistantMarchePublicBlocCandidaturesEtOffres->setVisible(false);
                $this->assistantMarchePublicBlocOffre->setVisible(true);
                $this->assistantMarchePublicBlocAnonymat->setVisible(true);
                $this->assistantMarchePublicBlocEnveloppe->setVisible(true);
            }
        }
    }

    /*
    * Permet de remplir le repeater des réponses
    */
    public function fillRepeaterReponses()
    {
        $atexoConsultationResponses = new Atexo_Consultation_Responses();
        $offreNonAnnuleeByConsRef = $atexoConsultationResponses->retrieveOffreNonAnnuleeByConsRef(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        $nombreElement = is_countable($offreNonAnnuleeByConsRef)
            ? count($offreNonAnnuleeByConsRef)
            : 0;
        $this->nombreReponsesElectonique->Text = $nombreElement;
        $this->Nombre_enveloppeReponsesElectronique->value = $nombreElement;
        $dataSourceActions = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), false);
        $this->actionsGroupeeReponseElectro->DataSource = $dataSourceActions;
        $this->actionsGroupeeReponseElectro->DataBind();
        if ($nombreElement > 0) {
            $this->showComponent('ReponseElec');
            $this->setViewState('nombreElementReponseElec', $nombreElement);
            $this->nombrePageTop_reponseElec->Text = ceil($nombreElement / $this->enveloppeReponsesElectronique->PageSize);
            $this->numPageTop_reponseElec->Text = 1;
            $this->nombrePageBottom_reponseElec->Text = ceil($nombreElement / $this->enveloppeReponsesElectronique->PageSize);
            $this->numPageBottom_reponseElec->Text = 1;
            $this->enveloppeReponsesElectronique->setVirtualItemCount($nombreElement);
            $this->enveloppeReponsesElectronique->setCurrentPageIndex(0);
            $this->populateData('enveloppeReponsesElectronique', 'ReponseElec');
        } else {
            $this->hideComponent('ReponseElec');
        }

        //TODO dépôt Papier
        $dataSourcePapier = $atexoConsultationResponses->retrieveOffrePapierNonAnnuleeByConsRef(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        $nombreElement = count($dataSourcePapier);
        $this->nombreReponsePapier->Text = $nombreElement;
        $this->Nombre_enveloppeReponsesPapiers->value = $nombreElement;
        if ($nombreElement) {
            $this->showComponent('ReponsePapier');
            $this->setViewState('nombreElementReponsePapier', $nombreElement);
            $this->nombrePageTop_reponsePapier->Text = ceil($nombreElement / $this->enveloppeReponsesPapiers->PageSize);
            $this->numPageTop_reponsePapier->Text = 1;
            $this->nombrePageBottom_reponsePapier->Text = ceil($nombreElement / $this->enveloppeReponsesPapiers->PageSize);
            $this->numPageBottom_reponsePapier->Text = 1;
            $this->enveloppeReponsesPapiers->setVirtualItemCount($nombreElement);
            $this->enveloppeReponsesPapiers->setCurrentPageIndex(0);
            $this->populateData('enveloppeReponsesPapiers', 'ReponsePapier');
        } else {
            $this->hideComponent('ReponsePapier');
        }

        $dataSourceActions = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_PAPIER'), false);
        $this->actionsGroupeeReponsePapier->DataSource = $dataSourceActions;
        $this->actionsGroupeeReponsePapier->DataBind();
    }

    public function fillRepeatersCandidature()
    {
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setModeUnique($this->isModeConsultationUnique());
        $criteriaResponse->setCount(true);
        $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));
        $candidaturesElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

        if (is_array($candidaturesElectroniques)) {
            if ('0' == $this->_consultation->getModeOuvertureReponse()) {
                $actionsPossibles = $this->actionsPossibleCandidature(false);
                $this->actionsGroupee_cand_el->DataSource = $actionsPossibles;
                $this->actionsGroupee_cand_el->DataBind();
            }
            $this->nombreCandidatureElectonique->Text = count($candidaturesElectroniques);
            $nombreElement = count($candidaturesElectroniques);
            $this->Nombre_enveloppeCandidatureElectronique->value = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('Candidature');
                $this->setViewState('nombreElementCandi', $nombreElement);
                $this->nombrePageTop_Candidature->Text = ceil($nombreElement / $this->enveloppeCandidatureElectronique->PageSize);
                $this->numPageTop_Candidature->Text = 1;
                $this->nombrePageBottom_Candidature->Text = ceil($nombreElement / $this->enveloppeCandidatureElectronique->PageSize);
                $this->numPageBottom_Candidature->Text = 1;
                $this->enveloppeCandidatureElectronique->setVirtualItemCount($nombreElement);
                $this->enveloppeCandidatureElectronique->setCurrentPageIndex(0);
                $this->populateData('enveloppeCandidatureElectronique', 'Candidature');
            } else {
                $this->hideComponent('Candidature');
            }
            $this->setViewState('enveloppesCandidature', $candidaturesElectroniques);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->fillRepeatersCandidaturePapier();
    }

    public function fillRepeatersCandidaturePapier()
    {
        if ('0' == $this->_consultation->getModeOuvertureReponse()) {
            $actionsPossibles = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_PAPIER'), false);
            $this->actionsGroupee_candPapier->DataSource = $actionsPossibles;
            $this->actionsGroupee_candPapier->DataBind();
        }
        $candidaturePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            '',
            $this->getViewState('typesEnveloppes'),
            '',
            '',
            $this->getViewState('organisme'),
            false,
            $this->isModeConsultationUnique(),
            null,
            null,
            false,
            true,
            true,
            $this->getViewState('guestsWithEnabling')
        );
        if (is_array($candidaturePapier)) {
            $nombreElement = count($candidaturePapier);
            $this->nombreCandidaturePapier->Text = $nombreElement;
            $this->Nombre_enveloppeCandidaturePapier->value = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('CandiPapier');
                $this->setViewState('nombreElementCandiPapier', $nombreElement);
                $this->nombrePageTop_CandidaturePapier->Text = ceil($nombreElement / $this->enveloppeCandidaturePapier->PageSize);
                $this->numPageTop_CandidaturePapier->Text = 1;
                $this->nombrePageBottom_CandidaturePapier->Text = ceil($nombreElement / $this->enveloppeCandidaturePapier->PageSize);
                $this->numPageBottom_CandidaturePapier->Text = 1;
                $this->enveloppeCandidaturePapier->setVirtualItemCount($nombreElement);
                $this->enveloppeCandidaturePapier->setCurrentPageIndex(0);
                $this->populateData('enveloppeCandidaturePapier', 'CandiPapier');
            } else {
                $this->hideComponent('CandiPapier');
            }
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
    }

    public function fillLotList($typeEnveloppe, $constitutionDossierReponse)
    {
        $lots = (new Atexo_Consultation_Lots())->retrieveLot($this->_consultation, $this->getViewState('organisme'), $this->_idService);

        if (is_array($lots)) {
            if (count($lots) >= 1) {
                $clonelots = $lots;
                $lot = array_shift($clonelots);
                $numeroLot = $lot->getLot();
                //$intituleLot=$lot->getDescriptionTraduite();

                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                    $admissible = $this->admissibilteObligatoire();
                    $lotOffres = (new Atexo_Consultation_Lots())->getArrayLotAndNombreEnveloppeFromObject(
                        $lots,
                        Atexo_Config::getParameter('TYPE_ENV_OFFRE'),
                        $constitutionDossierReponse,
                        $this->getViewState('organisme'),
                        $this->isModeConsultationUnique(),
                        $admissible
                    );
                    $admissible = $this->admissibilteObligatoire();

                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
                    $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteriaResponse->setEnveloppeStatutOuverte($this->isModeConsultationUnique());
                    $criteriaResponse->setAdmissible($admissible);

                    $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);

                    $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        $this->getViewState('typesEnveloppes'),
                        null,
                        '',
                        '',
                        Atexo_CurrentUser::getCurrentOrganism(),
                        false,
                        true,
                        false,
                        false,
                        $admissible
                    );

                    $result = $this->insertLigneTousLots(count($offresElectroniques), count($offrePapier));
                    $lotOffres = Atexo_Util::arrayUnshift($lotOffres, $result);

                    $this->identifiantLotOffre->DataSource = $lotOffres;
                    $this->identifiantLotOffre->DataBind();

                    $this->panelMoreThanOneLot2->setStyle('display:none');
                }
                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                    $hideAttestation = true;
                    $lotAnonymats = (new Atexo_Consultation_Lots())->getArrayLotAndNombreEnveloppeFromObject(
                        $lots,
                        Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'),
                        $constitutionDossierReponse,
                        $this->getViewState('organisme')
                    );

                    $anonymatElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques(
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        null,
                        $this->getViewState('typesEnveloppes'),
                        '',
                        '',
                        Atexo_CurrentUser::getCurrentOrganism(),
                        $this->isModeConsultationUnique()
                    );

                    $anonymatPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier(
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        $this->getViewState('typesEnveloppes'),
                        null,
                        '',
                        '',
                        Atexo_CurrentUser::getCurrentOrganism(),
                        false,
                        $this->isModeConsultationUnique()
                    );

                    $result = $this->insertLigneTousLots(count($anonymatElectroniques), count($anonymatPapier));

                    $lotAnonymats = Atexo_Util::arrayUnshift($lotAnonymats, $result);

                    $this->identifiantLotAnonymat->DataSource = $lotAnonymats;
                    $this->identifiantLotAnonymat->DataBind();

                    $this->panelMoreThanOneLot4->setStyle('display:none');
                }

                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                    $admissible = $this->admissibilteObligatoire();
                    $lotOffresTechniques = (new Atexo_Consultation_Lots())->getArrayLotAndNombreEnveloppeFromObject(
                        $lots,
                        Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'),
                        $constitutionDossierReponse,
                        $this->getViewState('organisme'),
                        $this->isModeConsultationUnique(),
                        $admissible
                    );

                    $offresTechniquesElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques(
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        null,
                        $this->getViewState('typesEnveloppes'),
                        '',
                        '',
                        Atexo_CurrentUser::getCurrentOrganism(),
                        true,
                        false,
                        $this->isModeConsultationUnique(),
                        $admissible,
                        null,
                        null,
                        null,
                        $this->getViewState('guestsWithEnabling')
                    );

                    $offreTechniquesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier(
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        $this->getViewState('typesEnveloppes'),
                        null,
                        '',
                        '',
                        Atexo_CurrentUser::getCurrentOrganism(),
                        false,
                        true,
                        false,
                        $this->isModeConsultationUnique(),
                        $admissible,
                        null,
                        null,
                        null,
                        $this->getViewState('guestsWithEnabling')
                    );

                    $result = $this->insertLigneTousLots(count($offresTechniquesElectroniques), count($offreTechniquesPapier));
                    $lotOffresTechniques = Atexo_Util::arrayUnshift($lotOffresTechniques, $result);

                    $this->identifiantLotOffreTechnique->DataSource = $lotOffresTechniques;
                    $this->identifiantLotOffreTechnique->DataBind();

                    $this->panelMoreThanOneLot6->setStyle('display:none');
                }
            } else {
                $numeroLot = '0';

                $this->panelMoreThanOneLot1->setVisible(false);
                $this->panelMoreThanOneLot2->setVisible(false);
                $this->panelMoreThanOneLot3->setVisible(false);
                $this->panelMoreThanOneLot4->setVisible(false);
                $this->panelMoreThanOneLot5->setVisible(false);
                $this->panelMoreThanOneLot6->setVisible(false);
            }
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->setViewState('numLotOfre', $numeroLot);
        $this->setViewState('numLotAnonymat', $numeroLot);
        $this->setViewState('numLotOffreTechnique', $numeroLot);
    }

    public function getUrlDossierVolumineux($idDossierVolumineux)
    {
        if (Atexo_Module::isEnabled('ModuleEnvol')) {
            $dossierVolumineux = (new Atexo_DossierVolumineux_Responses())->retrieveDossierVolumineux(
                $idDossierVolumineux
            );
            if ($dossierVolumineux instanceof CommonDossierVolumineux) {
                return Atexo_Config::getParameter('URL_LT_MPE_SF') . '/agent/dossier-volumineux/download/' . $dossierVolumineux->getUuidReference();
            }
        }

        return false;
    }

    public function fillRepeatersOffre()
    {
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $admissible = $this->admissibilteObligatoire();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setEnveloppeStatutOuverte($this->isModeConsultationUnique());
        $criteriaResponse->setAdmissible($admissible);
        $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
        $this->setViewState('numLotOfre', null);
        if (is_array($offresElectroniques)) {
            if ('1' != $this->_consultation->getModeOuvertureReponse()) {
                $this->setViewState('thColspanOffre', '7');
                $actionsPossibles = $this->actionsPossibleOffre(false);
                $this->actionsGroupee_offre_el->DataSource = $actionsPossibles;
                $this->actionsGroupee_offre_el->DataBind();
            }
            $nombreElement = count($offresElectroniques);
            $this->nombreOffreElectronique->Text = $nombreElement;
            $this->Nombre_enveloppeOffreElectronique->value = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('OffreElec');
                $this->nombrePageTop_OffreElec->Text = ceil($nombreElement / $this->enveloppeOffreElectronique->PageSize);
                $this->numPageTop_OffreElec->Text = 1;
                $this->nombrePageBottom_OffreElec->Text = ceil($nombreElement / $this->enveloppeOffreElectronique->PageSize);
                $this->numPageBottom_OffreElec->Text = 1;
                $this->enveloppeOffreElectronique->setVirtualItemCount($nombreElement);
                $this->enveloppeOffreElectronique->setCurrentPageIndex(0);
                $this->populateData('enveloppeOffreElectronique', 'OffreElec', '', '', true);
            } else {
                $this->hideComponent('OffreElec');
            }
            $this->setViewState('nombreElementOffreElec', $nombreElement);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->fillRepeatersOffrePapier();
    }

    public function fillRepeatersOffrePapier()
    {
        $admissible = $this->admissibilteObligatoire();
        $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotOfre'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            true,
            false,
            $this->isModeConsultationUnique(),
            $admissible
        );
        if (is_array($offrePapier)) {
            if ('1' != $this->_consultation->getModeOuvertureReponse()) {
                $this->setViewState('thColspanOffre', '7');
                $actionsPossibles = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_PAPIER'), false);
                $this->actionsGroupee_offre_Papier->DataSource = $actionsPossibles;
                $this->actionsGroupee_offre_Papier->DataBind();
            }
            $nombreElement = count($offrePapier);
            $this->nombreOffrePapier->Text = $nombreElement;
            $this->Nombre_enveloppeOffrePapier->value = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('OffreElecPapier');
                $this->nombrePageTop_OffreElecPapier->Text = ceil($nombreElement / $this->enveloppeOffrePapier->PageSize);
                $this->numPageTop_OffreElecPapier->Text = 1;
                $this->nombrePageBottom_OffreElecPapier->Text = ceil($nombreElement / $this->enveloppeOffrePapier->PageSize);
                $this->numPageBottom_OffreElecPapier->Text = 1;
                $this->enveloppeOffrePapier->setVirtualItemCount($nombreElement);
                $this->enveloppeOffrePapier->setCurrentPageIndex(0);
                $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', '', '', true);
            } else {
                $this->hideComponent('OffreElecPapier');
            }
            $this->setViewState('nombreElementOffreElecPapier', $nombreElement);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
    }

    public function fillRepeatersOffreTechnique()
    {
        $admissible = $this->admissibilteObligatoire();
        $offresTechniquesElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            null,
            $this->getViewState('typesEnveloppes'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            true,
            false,
            $this->isModeConsultationUnique(),
            $admissible,
            null,
            null,
            null,
            $this->getViewState('guestsWithEnabling')
        );

        $this->setViewState('numLotOffreTechnique', null);
        $dataSourceActions = $this->actionsPossibleOffreTechnique(false);
        $this->actionsGroupee_offreTechniqueElect->DataSource = $dataSourceActions;
        $this->actionsGroupee_offreTechniqueElect->DataBind();
        if (is_array($offresTechniquesElectroniques)) {
            $nombreElement = count($offresTechniquesElectroniques);
            if ($nombreElement > 0) {
                $this->showComponent('OffreTechniqueElec');
                $this->setViewState('nombreElementOffreTechElec', $nombreElement);
                $this->nombrePageTop_OffreTechElec->Text = ceil($nombreElement / $this->enveloppeOffreTechniqueElectronique->PageSize);
                $this->numPageTop_OffreTechElec->Text = 1;
                $this->nombrePageBottom_OffreTechElec->Text = ceil($nombreElement / $this->enveloppeOffreTechniqueElectronique->PageSize);
                $this->numPageBottom_OffreTechElec->Text = 1;
                $this->nombreOffreTechniqueElectronique->Text = count($offresTechniquesElectroniques);
                $this->Nombre_enveloppeOffreTechniqueElectronique->value = count($offresTechniquesElectroniques);
                $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', '', '', true);
            } else {
                $this->hideComponent('OffreTechniqueElec');
            }
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $admissible = $this->admissibilteObligatoire();
        $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotOffreTechnique'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            true,
            false,
            $this->isModeConsultationUnique(),
            $admissible,
            null,
            null,
            null,
            $this->getViewState('guestsWithEnabling')
        );
        $dataSourceActions = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_PAPIER'), false);
        $this->actionsGroupee_offreTechniquePapier->DataSource = $dataSourceActions;
        $this->actionsGroupee_offreTechniquePapier->DataBind();
        if (is_array($offrePapier)) {
            $nombreElement = count($offrePapier);
            if ($nombreElement > 0) {
                $this->showComponent('OffreTechniquePapier');
                $this->setViewState('nombreElementOffreTechPapier', $nombreElement);
                $this->nombreOffreTechniquePapier->Text = count($offrePapier);
                $this->Nombre_enveloppeOffreTechniquePapier->value = count($offrePapier);
                $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', '', '', true);
                $this->nombrePageTop_OffreTechElecPapier->Text = ceil($nombreElement / $this->enveloppeOffreTechniqueElectronique->PageSize);
                $this->numPageTop_OffreTechElecPapier->Text = 1;
                $this->nombrePageBottom_OffreTechElecPapier->Text = ceil($nombreElement / $this->enveloppeOffreTechniqueElectronique->PageSize);
                $this->numPageBottom_OffreTechElecPapier->Text = 1;
                $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', '', '', true);
            } else {
                $this->hideComponent('OffreTechniquePapier');
            }
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
    }

    public function fillRepeatersAnonymat()
    {
        $anonymatElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            null,
            $this->getViewState('typesEnveloppes'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            $this->isModeConsultationUnique()
        );

        $this->setViewState('numLotAnonymat', null);
        if (is_array($anonymatElectroniques)) {
            if ('1' != $this->_consultation->getModeOuvertureReponse()) {
                $actionsPossibles = $this->actionsPossibleAnonymat(false, false);
                $this->actionsGroupee_offreAnonymat_el->DataSource = $actionsPossibles;
                $this->actionsGroupee_offreAnonymat_el->DataBind();
            }
            $nombreElement = count($anonymatElectroniques);
            $this->nombreAnonymatElectronique->Text = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('AnonymatElec');
                $this->nombrePageTop_AnonymatElec->Text = ceil($nombreElement / $this->enveloppeAnonymatEletronique->PageSize);
                $this->numPageTop_AnonymatElec->Text = 1;
                $this->nombrePageBottom_AnonymatElec->Text = ceil($nombreElement / $this->enveloppeAnonymatEletronique->PageSize);
                $this->numPageBottom_AnonymatElec->Text = 1;
                $this->enveloppeAnonymatEletronique->setVirtualItemCount($nombreElement);
                $this->enveloppeAnonymatEletronique->setCurrentPageIndex(0);
                $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', '', '', true);
            } else {
                $this->hideComponent('AnonymatElec');
            }
            $this->setViewState('nombreElementAnonymatElec', $nombreElement);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $anonymatPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotAnonymat'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            $this->isModeConsultationUnique()
        );
        if (is_array($anonymatPapier)) {
            if ('1' != $this->_consultation->getModeOuvertureReponse()) {
                $actionsPossibles = $this->actionsPossibleAnonymat(true, false);
                $this->actionsGroupee_offreAnonymat_papier->DataSource = $actionsPossibles;
                $this->actionsGroupee_offreAnonymat_papier->DataBind();
            }

            $nombreElement = count($anonymatPapier);
            $this->nombreAnonymatPapier->Text = $nombreElement;
            $this->Nombre_enveloppeAnonymatPapier->value = $nombreElement;
            if ($nombreElement > 0) {
                $this->showComponent('AnonymatPapier');
                $this->nombrePageTop_AnonymatPapier->Text = ceil($nombreElement / $this->enveloppeAnonymatPapier->PageSize);
                $this->numPageTop_AnonymatPapier->Text = 1;
                $this->nombrePageBottom_AnonymatPapier->Text = ceil($nombreElement / $this->enveloppeAnonymatPapier->PageSize);
                $this->numPageBottom_AnonymatPapier->Text = 1;
                $this->enveloppeAnonymatPapier->setVirtualItemCount($nombreElement);
                $this->enveloppeAnonymatPapier->setCurrentPageIndex(0);
                $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', '', '', true);
            } else {
                $this->hideComponent('AnonymatPapier');
            }
            $this->setViewState('nombreElementAnonymatPapier', count($anonymatPapier));
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
    }

    public function fillRepeatersDecision()
    {
        $this->blocDecision->setConsultation($this->_consultation);
        $this->blocDecision->initaliserComposant();
    }

    public function activeTabChanged($sender, $param)
    {
        $this->infoPli->setValue('');

        switch ($sender->ID) {
            case 'beforeFirstTab':
                $this->reponses->setCssClass('tab-on');
                $this->candidature->setCssClass('tab');
                $this->offre->setCssClass('tab');
                $this->anonymat->setCssClass('tab');
                $this->decision->setCssClass('tab');
                $this->offreTechnique->setCssClass('tab');
                $this->ongletLayer0->setVisible(true);
                $this->ongletLayer1->setVisible(false);
                $this->ongletLayer2->setVisible(false);
                $this->ongletLayer3->setVisible(false);
                $this->ongletLayer4->setVisible(false);
                $this->ongletLayer5->setVisible(false);
                $this->exportDecision->setVisible(false);
                $this->fillRepeaterReponses();
                $this->pvButton->setVisible(false);
                break;
            case 'firstTab':
                $this->reponses->setCssClass('tab');
                $this->candidature->setCssClass('tab-on');
                $this->offre->setCssClass('tab');
                $this->anonymat->setCssClass('tab');
                $this->decision->setCssClass('tab');
                $this->offreTechnique->setCssClass('tab');
                $this->ongletLayer0->setVisible(false);
                $this->ongletLayer1->setVisible(true);
                $this->ongletLayer2->setVisible(false);
                $this->ongletLayer3->setVisible(false);
                $this->ongletLayer4->setVisible(false);
                $this->ongletLayer5->setVisible(false);
                $this->exportDecision->setVisible(false);
                $this->fillRepeatersCandidature();
                $this->pvButton->setVisible(false);
                break;

            case 'secondTab':
                $this->reponses->setCssClass('tab');
                $this->candidature->setCssClass('tab');
                $this->offre->setCssClass('tab-on');
                $this->anonymat->setCssClass('tab');
                $this->decision->setCssClass('tab');
                $this->offreTechnique->setCssClass('tab');
                $this->ongletLayer0->setVisible(false);
                $this->ongletLayer1->setVisible(false);
                $this->ongletLayer2->setVisible(true);
                $this->ongletLayer3->setVisible(false);
                $this->ongletLayer4->setVisible(false);
                $this->ongletLayer5->setVisible(false);
                $this->pvButton->setVisible(true);
                $this->exportDecision->setVisible(false);
                if (Atexo_Module::isEnabled('DepouillementEnveloppeDependRatEnveloppePrecedente')) {
                    $enveloppePrecedente = (new Atexo_Consultation_Responses())->enveloppePrecedente(
                        Atexo_Config::getParameter('TYPE_ENV_OFFRE'),
                        $this->getViewState('withCandidature'),
                        $this->getViewState('withOffreTechnique'),
                        $this->getViewState('withOffre'),
                        $this->getViewState('withAnonymat')
                    );

                    if ($enveloppePrecedente) {
                        $enveloppesElectro = [];
                        $enveloppesPapier = [];
                        if ($enveloppePrecedente === Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                            $admissible = $this->admissibilteObligatoire();
                            $enveloppesElectro = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques(
                                Atexo_Util::atexoHtmlEntities($_GET['id']),
                                null,
                                $this->getViewState('typesEnveloppes'),
                                null,
                                null,
                                $this->getViewState('organisme'),
                                true,
                                false,
                                $this->isModeConsultationUnique(),
                                $admissible,
                                null,
                                null,
                                null,
                                $this->getViewState('guestsWithEnabling')
                            );
                            $enveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier(
                                Atexo_Util::atexoHtmlEntities($_GET['id']),
                                $this->getViewState('typesEnveloppes'),
                                null,
                                null,
                                null,
                                $this->getViewState('organisme'),
                                false,
                                true,
                                false,
                                $this->isModeConsultationUnique(),
                                $admissible,
                                null,
                                null,
                                null,
                                $this->getViewState('guestsWithEnabling')
                            );
                        } elseif ($enveloppePrecedente === Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $criteriaResponse = new Atexo_Response_CriteriaVo();
                            $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                            $criteriaResponse->setOrganisme($this->getViewState('organisme'));
                            $criteriaResponse->setModeUnique($this->isModeConsultationUnique());
                            $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));
                            $enveloppesElectro = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

                            $enveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
                                Atexo_Util::atexoHtmlEntities($_GET['id']),
                                null,
                                null,
                                null,
                                null,
                                $this->getViewState('organisme'),
                                false,
                                $this->isModeConsultationUnique(),
                                null,
                                null,
                                false,
                                true,
                                false,
                                $this->getViewState('guestsWithEnabling')
                            );
                        }

                        $enveloppeElectroEtPapier = array_merge($enveloppesElectro, $enveloppesPapier);
                        $zeroRat = (new Atexo_Consultation_Responses())->calculZeroRat($enveloppeElectroEtPapier, null, false, $this->getViewState('typesEnveloppes'));
                        if (0 != count($enveloppeElectroEtPapier) && 0 != $zeroRat) {
                            $this->panelMoreThanOneLot1->setVisible(false);
                            $this->panelMoreThanOneLot2->setVisible(false);

                            $this->nbrRepOfETop->setVisible(false);
                            $this->nbrRepOfEBottom->setVisible(false);
                            $this->nbrRepOfP->setVisible(false);
                            $this->nbrRepOfPBottom->setVisible(false);

                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::localize('DEPOUILLEMENT_ERREUR'));

                            $this->enveloppeOffrePapier->DataSource = [];
                            $this->enveloppeOffrePapier->DataBind();
                            $this->enveloppeOffreElectronique->DataSource = [];
                            $this->enveloppeOffreElectronique->DataBind();

                            $this->hideComponent('OffreElecPapier');
                            $this->hideComponent('OffreElec');

                            return false;
                        }
                    }
                }

                $this->fillLotList(Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $this->getViewState('typesEnveloppes'));
                $this->fillRepeatersOffre();

                break;

            case 'thirdTab':
                $this->reponses->setCssClass('tab');
                $this->candidature->setCssClass('tab');
                $this->offre->setCssClass('tab');
                $this->anonymat->setCssClass('tab-on');
                $this->decision->setCssClass('tab');
                $this->offreTechnique->setCssClass('tab');
                $this->ongletLayer0->setVisible(false);
                $this->ongletLayer1->setVisible(false);
                $this->ongletLayer2->setVisible(false);
                $this->ongletLayer3->setVisible(true);
                $this->ongletLayer4->setVisible(false);
                $this->ongletLayer5->setVisible(false);
                $this->pvButton->setVisible(false);
                $this->exportDecision->setVisible(false);
                $this->fillLotList(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'), $this->getViewState('typesEnveloppes'));
                $this->fillRepeatersAnonymat();

                break;
            case 'fourthTab':
                $this->reponses->setCssClass('tab');
                $this->candidature->setCssClass('tab');
                $this->offre->setCssClass('tab');
                $this->anonymat->setCssClass('tab');
                $this->decision->setCssClass('tab-on');
                $this->offreTechnique->setCssClass('tab');
                $this->ongletLayer0->setVisible(false);
                $this->ongletLayer1->setVisible(false);
                $this->ongletLayer2->setVisible(false);
                $this->ongletLayer3->setVisible(false);
                $this->ongletLayer4->setVisible(true);
                $this->ongletLayer5->setVisible(false);
                $this->exportDepot->setVisible(false);
                $this->pvButton->setVisible(false);
                if (Atexo_Module::isEnabled('ExportDecision')) {
                    $this->exportDecision->setVisible(true);
                }
                $this->fillRepeatersDecision();

                break;
            case 'fifthTab':
                $this->reponses->setCssClass('tab');
                $this->candidature->setCssClass('tab');
                $this->offre->setCssClass('tab');
                $this->anonymat->setCssClass('tab');
                $this->decision->setCssClass('tab');
                $this->offreTechnique->setCssClass('tab-on');
                $this->ongletLayer0->setVisible(false);
                $this->ongletLayer1->setVisible(false);
                $this->ongletLayer2->setVisible(false);
                $this->ongletLayer3->setVisible(false);
                $this->ongletLayer4->setVisible(false);
                $this->ongletLayer5->setVisible(true);
                $this->pvButton->setVisible(false);
                $this->exportDecision->setVisible(false);
                if (Atexo_Module::isEnabled('DepouillementEnveloppeDependRatEnveloppePrecedente')) {
                    $enveloppePrecedente = (new Atexo_Consultation_Responses())->enveloppePrecedente(
                        Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'),
                        $this->getViewState('withCandidature'),
                        $this->getViewState('withOffreTechnique'),
                        $this->getViewState('withOffre'),
                        $this->getViewState('withAnonymat')
                    );
                    if ($enveloppePrecedente) {
                        $enveloppesElectro = [];
                        $enveloppesPapier = [];

                        if ($enveloppePrecedente === Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $criteriaResponse = new Atexo_Response_CriteriaVo();
                            $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                            $criteriaResponse->setOrganisme($this->getViewState('organisme'));
                            $criteriaResponse->setModeUnique($this->isModeConsultationUnique());
                            $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));

                            $enveloppesElectro = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

                            $enveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
                                Atexo_Util::atexoHtmlEntities($_GET['id']),
                                null,
                                null,
                                null,
                                null,
                                $this->getViewState('organisme'),
                                false,
                                $this->isModeConsultationUnique(),
                                null,
                                null,
                                false,
                                true,
                                false,
                                $this->getViewState('guestsWithEnabling')
                            );
                        }
                        $enveloppeElectroEtPapier = array_merge($enveloppesElectro, $enveloppesPapier);
                        $zeroRat = (new Atexo_Consultation_Responses())->calculZeroRat($enveloppeElectroEtPapier, null, false, $this->getViewState('typesEnveloppes'));
                        if (0 != count($enveloppeElectroEtPapier) && 0 != $zeroRat) {
                            $this->panelMoreThanOneLot5->setVisible(false);
                            $this->panelMoreThanOneLot6->setVisible(false);

                            $this->nbrRepOtETop->setVisible(false);
                            $this->nbrRepOtEBottom->setVisible(false);
                            $this->nbrRepOtPTop->setVisible(false);

                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::localize('DEPOUILLEMENT_ERREUR'));

                            $this->enveloppeOffreTechniqueElectronique->DataSource = [];
                            $this->enveloppeOffreTechniqueElectronique->DataBind();
                            $this->enveloppeOffreTechniquePapier->DataSource = [];
                            $this->enveloppeOffreTechniquePapier->DataBind();

                            return;
                        }
                    }
                }

                $this->fillLotList(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), $this->getViewState('typesEnveloppes'));
                $this->fillRepeatersOffreTechnique();

                break;
            default:
                throw new Atexo_Exception('unkown tab');
        }
        if ('tab-on' == $this->decision->getCssClass()) {
            Atexo_CurrentUser::writeToSession('PageSansApplet', true);
        } else {
            Atexo_CurrentUser::writeToSession('PageSansApplet', false);
        }
    }

    public function lotOffreChanged($sender, $param)
    {
        $this->infoPli->setValue('');
        if ('0' == $this->identifiantLotOffre->SelectedValue) {
            $this->panelMoreThanOneLot2->setStyle('display:none');
            $numLot = null;
        } else {
            $numLot = $this->identifiantLotOffre->SelectedValue;
            $this->panelMoreThanOneLot2->setStyle('display:');
            $intituleLot = (new Atexo_Consultation_Lots())->retrieveIntituleLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $numLot = (new Atexo_Consultation_Lots())->retrieveNumLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $this->NumLotOffre->Text = $numLot;
            $this->descriptionLotOffre->Text = $intituleLot;
        }

        $this->setViewState('numLotOfrePapier', $numLot);
        $this->setViewState('numLotOfre', $numLot);
        $sensTriElec = $this->getViewState('sensTriElec', []);
        $triElec = $this->getViewState('triElec', '');
        $sensTriPapier = $this->getViewState('sensTriPapier', []);
        $triPapier = $this->getViewState('triPapier', '');

        if ('identifiantLotOffre' == $sender->ID) { // envelever ce test
            $this->intialiserRepeater('enveloppeOffreElectronique', 'offreElec_msg_nbResultsTop', 'numPageTop_OffreElec');
            // Offre electoronique
            $this->populateData('enveloppeOffreElectronique', 'OffreElec', $triElec, $sensTriElec, true);
            $pageSize = $this->offreElec_msg_nbResultsTop->getSelectedValue();
            $this->updatePagination($pageSize, $this->numPageTop_OffreElec->Text, 'OffreElec');
            $this->populateData('enveloppeOffreElectronique', 'OffreElec', $triElec, $sensTriElec, true);

            $this->panelEnveloppeOffre->render($param->NewWriter);
            $this->nbrRepOfETop->render($param->NewWriter);
            $this->nbrRepOfEBottom->render($param->NewWriter);
            //               $this->panelActionsGroupeesOffre->render($param->NewWriter);

            //Offre papier
            $this->intialiserRepeater('enveloppeOffrePapier', 'offreElec_msg_nbResultsTopPapier', 'numPageTop_OffreElecPapier');
            $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', $triPapier, $sensTriPapier, true); //fai pour corriger un prob
            $pageSize = $this->offreElec_msg_nbResultsTopPapier->getSelectedValue();
            $this->updatePagination($pageSize, $this->numPageTop_OffreElecPapier->Text, 'OffreElecPapier');
            $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', $triPapier, $sensTriPapier, true);
            $this->nbrRepOfP->render($param->NewWriter);
            $this->nbrRepOfPBottom->render($param->NewWriter);
            $this->panelOffrePapier->render($param->NewWriter);
        }
        $this->linkSyntheseOffreTech->render($param->NewWriter);
        $this->panelMoreThanOneLot2->render($param->NewWriter);
    }

    public function lotAnonymatChanged($sender, $param)
    {
        if ('0' == $this->identifiantLotAnonymat->SelectedValue) {
            $this->panelMoreThanOneLot4->setStyle('display:none');
            $numLot = null;
        } else {
            $numLot = $this->identifiantLotAnonymat->SelectedValue;
            $intituleLot = (new Atexo_Consultation_Lots())->retrieveIntituleLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $this->descriptionLotAnonymat->Text = $intituleLot;
            $numLot = (new Atexo_Consultation_Lots())->retrieveNumLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $this->NumLotAnonymat->Text = $numLot;
            $this->panelMoreThanOneLot4->setStyle('display:');
        }
        $this->setViewState('numLotOfre', $numLot);
        $this->setViewState('numLotAnonymat', $numLot);

        $sensTriElec = $this->getViewState('sensTriElec', []);
        $triElec = $this->getViewState('triElec', '');
        $sensTriPapier = $this->getViewState('sensTriPapier', []);
        $triPapier = $this->getViewState('triPapier', '');

        $this->intialiserRepeater('enveloppeAnonymatEletronique', 'anonymatElec_msg_nbResultsTop', 'numPageTop_AnonymatElec');
        $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $triElec, $sensTriElec, true);
        $pageSize = $this->anonymatElec_msg_nbResultsTop->getSelectedValue();
        $this->updatePagination($pageSize, $this->numPageTop_AnonymatElec->Text, 'AnonymatElec');
        $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $triElec, $sensTriElec, true);

        $this->panelEnveloppeAnonymat->render($param->NewWriter);
        $this->nbrRepAETop->render($param->NewWriter);
        $this->nbrRepAEBottom->render($param->NewWriter);
        //           $this->panelActionsGroupeesOffreAnonymat->render($param->NewWriter);

        //Anonymat Papier
        $this->intialiserRepeater('enveloppeAnonymatPapier', 'anonymatPapier_msg_nbResultsTop', 'numPageTop_AnonymatPapier');
        $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', $triPapier, $sensTriPapier, true);
        $pageSize = $this->anonymatPapier_msg_nbResultsTop->getSelectedValue();
        $this->updatePagination($pageSize, $this->numPageTop_AnonymatPapier->Text, 'AnonymatPapier');
        $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', $triPapier, $sensTriPapier, true);

        $this->panelAnonymatPapier->render($param->NewWriter);
        $this->nbrRepAPBottom->render($param->NewWriter);
        $this->nbrRepAPTop->render($param->NewWriter);

        $this->linkSyntheseOffreAnonymat->render($param->NewWriter);
        $this->panelMoreThanOneLot4->render($param->NewWriter);
    }

    public function lotOffreTechniqueChanged($sender, $param)
    {
        $this->infoPli->setValue('');
        if ('0' == $this->identifiantLotOffreTechnique->SelectedValue) {
            $this->panelMoreThanOneLot6->setStyle('display:none');
            $numLot = null;
        } else {
            $numLot = $this->identifiantLotOffreTechnique->SelectedValue;
            $this->panelMoreThanOneLot6->setStyle('display:');
            $intituleLot = (new Atexo_Consultation_Lots())->retrieveIntituleLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $numLot = (new Atexo_Consultation_Lots())->retrieveNumLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, $this->getViewState('organisme'), $this->_idService);
            $this->NumLotOffreTechnique->Text = $numLot;
            $this->descriptionLotOffreTechnique->Text = $intituleLot;
        }

        $this->setViewState('numLotOffreTechnique', $numLot);
        $sensTriElec = $this->getViewState('sensTriElec', []);
        $triElec = $this->getViewState('triElec', '');
        $sensTriPapier = $this->getViewState('sensTriPapier', []);
        $triPapier = $this->getViewState('triPapier', '');
        $this->intialiserRepeater('enveloppeOffreTechniqueElectronique', 'offreTechElec_msg_nbResultsTop', 'numPageTop_OffreTechElec');
        $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $triElec, $sensTriElec, true);
        $pageSize = $this->offreTechElec_msg_nbResultsTop->getSelectedValue();
        $this->updatePagination($pageSize, $this->numPageTop_OffreTechElec->Text, 'OffreTechniqueElec');
        $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $triElec, $sensTriElec, true);

        $this->nbrRepOtETop->render($param->NewWriter);
        $this->nbrRepOtEBottom->render($param->NewWriter);

        $this->intialiserRepeater('enveloppeOffreTechniquePapier', 'offreTechElec_msg_nbResultsTopPapier', 'numPageTop_OffreTechElecPapier');
        $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', $triPapier, $sensTriPapier, true);
        $pageSize = $this->offreTechElec_msg_nbResultsTopPapier->getSelectedValue();
        $this->updatePagination($pageSize, $this->numPageTop_OffreTechElecPapier->Text, 'OffreTechniquePapier');
        $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', $triPapier, $sensTriPapier, true);

        $this->nbrRepOtPTop->render($param->NewWriter);
        $this->nbrRepOtPBottom->render($param->NewWriter);

        $this->linkSyntheseOffreTech->render($param->NewWriter);
        $this->panelMoreThanOneLot6->render($param->NewWriter);
        $this->panelEnveloppeOffreTechnique->render($param->NewWriter);
        $this->panelEnveloppeOffreTechniquePapier->render($param->NewWriter);
    }

    //tri des réponses
    public function TrierReponse($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'reponsesElec', 'Offres', 'Elec');
    }

    public function TrierReponsePapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'reponsesPapier', 'Offre_papier', 'Papier');
    }

    //tri pour candidature
    public function TrierCandidatureEnv($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'candidatureElec', 'Enveloppe', 'Elec');
    }

    public function TrierCandidaturePapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'candidaturePapier', 'Offre_papier', 'Papier');
    }

    public function TrierCandidatureEnvPapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'candidaturePapier', 'Enveloppe_papier', 'Papier');
    }

    public function TrierCandidature($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'candidatureElec', 'Offres', 'Elec');
    }

    //tri pour offre
    public function TrierOffre($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreElec', 'Offres', 'Elec');
    }

    public function TrierOffrePapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offrePapier', 'Offre_papier', 'Papier');
    }

    public function TrierOffreEnvPapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offrePapier', 'Enveloppe_papier', 'Papier');
    }

    public function TrierOffreEnv($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreElec', 'Enveloppe', 'Elec');
    }

    //tri pour anonymat
    public function TrierAnonymat($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'anonymatElec', 'Offres', 'Elec');
    }

    public function TrierAnonymatEnv($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'anonymatElec', 'Enveloppe', 'Elec');
    }

    public function TrierAnonymatPapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'anonymatPapier', 'Offre_papier', 'Papier');
    }

    public function TrierAnonymatEnvPapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'anonymatPapier', 'Enveloppe_papier', 'Papier');
    }

    //tri pour offre technique
    public function TrierOffreTechnique($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreTechniqueElec', 'Offres', 'Elec');
    }

    public function TrierOffreTechniqueEnv($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreTechniqueElec', 'Enveloppe', 'Elec');
    }

    public function TrierOffreTechniquePapier($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreTechniquePapier', 'Offre_papier', 'Papier');
    }

    public function TrierOffreTechniquePapierEnv($sender, $param)
    {
        $this->arrangeSortParameter($param->CommandName, 'offreTechniquePapier', 'Enveloppe_papier', 'Papier');
    }

    public function arrangeSortParameter($sortBy, $arrayKey, $objet = false, $type = false)
    {
        $trierPar = $sortBy;
        $sensTri = $this->getViewState('sensTri' . $type, []);

        if (isset($sensTri[$arrayKey][$trierPar])) {
            $sensTri[$arrayKey][$trierPar] = ('DESC' == $sensTri[$arrayKey][$trierPar]) ? 'ASC' : 'DESC';
        } else {
            $sensTri = [];
            $sensTri[$arrayKey][$trierPar] = 'DESC';
        }
        $this->setViewState('sensTri' . $type, $sensTri);
        $this->setViewState('tri' . $type, $trierPar);
        if ($type) {
            $this->setViewState('typeEnv' . $type, $type);
        } else {
            $this->setViewState('typeEnv' . $type, null);
        }
        if ($objet) {
            $this->setViewState('Objet' . $type, $objet);
        }
    }

    public function chiffreOrNot($enveloppe)
    {
        $libelle = '';
        if ($enveloppe instanceof CommonEnveloppe) {
            if ('1' == $enveloppe->getCryptage()) {
                if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')) {
                    $libelle = '- ' . Prado::localize('DEFINE_EN_COURS_CHIFFREMENT_DEPOT_RECENT') . ' -';
                } elseif ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')) {
                    $libelle = '- ' . Prado::localize('DEFINE_EN_ATTENTE_CHIFFREMENT_DEPOT_RECENT') . ' -';
                } else {
                    $libelle = '- ' . Prado::localize('CHIFFRE') . ' -';
                }
            } else {
                $libelle = '- ' . Prado::localize('DECHIFFRE') . ' -';
            }
        }

        return $libelle;
    }

    /**
     * verifie si c chiffre ou pas.
     *
     * @param  $enveloppe
     */
    public function verifierEnveloppeChiffre(): string
    {
        if ($this->getChiffrementEnveloppe()) {
            return 'block;';
        }

        return 'none;';
    }

    /**
     * @return string
     */
    public function isCmsActif(): string
    {
        return $this->isMAMPEnabled() ? 'visible;' : 'none;' ;
    }

    public function getFileSize($idEnveloppe)
    {
        return (new Atexo_Consultation_Responses())->getFileSize($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism());
    }

    /*
    * Permet de récuperer les actions possible d'une offre
    */
    public function possibleActionsReponse($commonOffres, $typeDepot)
    {
        $actionsPossibles = [];
        //TODO typeDepoit à passer en paramêtre
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            //Tester si les enveloppes de cette offres est encore fermer
            $statutOffres = $commonOffres->getStatutOffres();
            if ($statutOffres == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles = $this->actionsPossibleReponse(Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'));
            }
            if ($statutOffres == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
            }
        } else {
            //TODO
            $statutOffre = $commonOffres->getStatutOffrePapier();
            if ($statutOffre == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
                if (Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
                }
            } elseif ($statutOffre == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
            }
        }
        if (count($actionsPossibles)) {
            return $actionsPossibles;
        } else {
            return ['-'];
        }
    }

    /**
     * @param  $statutEnveloppe
     * @param  $typeDepot
     * @param string $cryptage
     * @param bool   $enveloppe
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     *                                Onglet candidature
     */
    public function possibleActions($statutEnveloppe, $typeDepot, $cryptage = '0', $enveloppe = false)
    {
        $actionsPossibles = [];
        //TODO typeDepoit à passer en paramêtre
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                if (Atexo_Module::isEnabled('fourEyes')) {
                    if (($enveloppe instanceof CommonEnveloppe)) {
                        $dateOuverture = $enveloppe->getDateheureOuverture();
                        if ($dateOuverture && '0000-00-00 00:00:00' != $dateOuverture) {
                            $diff = Atexo_Util::getDiffDateMinute(date('Y-m-d H:i:s'), $dateOuverture);
                            $xMin = Atexo_Config::getParameter('X_MIN_4_EYES');
                            $idAgentConnecte = Atexo_CurrentUser::getIdAgentConnected();
                            if ($diff < $xMin && $idAgentConnecte != $enveloppe->getAgentIdOuverture2()) {
                                $actionsPossibles = $this->actionsPossibleCandidature();
                            } elseif ($diff >= $xMin) {
                                $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                            }
                        } else {
                            $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                        }
                    }
                } else {
                    $actionsPossibles = $this->actionsPossibleCandidature();
                }
            }

            if (
                $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
            ) {
                if (Atexo_CurrentUser::hasHabilitation('GererAdmissibilite')) {
                    $actionsPossibles[Atexo_Config::getParameter('GERE_ADMISSIBILITE')] = Prado::localize('GERE_ADMISSIBILITE');
                }

                if (
                    ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) && '1' == $cryptage && Atexo_CurrentUser::hasHabilitation('ImporterEnveloppe')
                ) {
                    $actionsPossibles[Atexo_Config::getParameter('IMPORTER_ENVELOPPE')] = Prado::localize('IMPORTER_ENVELOPPE');
                }

                $actionsPossibles[Atexo_Config::getParameter('DEMANDE_COMPLEMENT')] = Prado::localize('DEMANDE_COMPLEMENT');

                $actionsPossibles = $this->getActionRejete($actionsPossibles);
            }
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
            }
        } else {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
                if (Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
                }
            } elseif ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')) {
                if (Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
                }
            } else {
                $actionsPossibles[Atexo_Config::getParameter('GERE_ADMISSIBILITE')] = Prado::localize('GERE_ADMISSIBILITE');
            }
        }

        if ($actionsPossibles) {
            return $actionsPossibles;
        } else {
            return ['-'];
        }
    }

    public function possibleActionsOffre($statutEnveloppe, $typeDepot, $cryptage = '0', $enveloppe = false)
    {
        $actionsPossibles = [];
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                if (Atexo_Module::isEnabled('fourEyes')) {
                    if (($enveloppe instanceof CommonEnveloppe)) {
                        $dateOuverture = $enveloppe->getDateheureOuverture();
                        if ($dateOuverture && '0000-00-00 00:00:00' != $dateOuverture) {
                            $diff = Atexo_Util::getDiffDateMinute(date('Y-m-d H:i:s'), $dateOuverture);
                            $xMin = Atexo_Config::getParameter('X_MIN_4_EYES');
                            $idAgentConnecte = Atexo_CurrentUser::getIdAgentConnected();
                            if ($diff < $xMin && $idAgentConnecte != $enveloppe->getAgentIdOuverture2()) {
                                $actionsPossibles = $this->actionsPossibleOffre();
                            } elseif ($diff >= $xMin) {
                                $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                            }
                        } else {
                            $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                        }
                    }
                } else {
                    $actionsPossibles = $this->actionsPossibleOffre();
                }
            }
            if (
                $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
            ) {
                if (
                    ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) && '1' == $cryptage && Atexo_CurrentUser::hasHabilitation('ImporterEnveloppe')
                    && Atexo_Module::isEnabled('ImporterEnveloppe')
                ) {
                    $actionsPossibles[Atexo_Config::getParameter('IMPORTER_ENVELOPPE')] = Prado::localize('IMPORTER_ENVELOPPE');
                }
                $actionsPossibles[Atexo_Config::getParameter('DEMANDE_COMPLEMENT')] = Prado::localize('DEMANDE_COMPLEMENT');

                $actionsPossibles = $this->getActionRejete($actionsPossibles);
            }
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = prado::localize('RESTAURER');
            }
        } else {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
                if ($this->getViewState('typesEnveloppes') == Atexo_Config::getParameter('OFFRE_SEUL') && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
                }
            }
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = prado::localize('RESTAURER');
            }
        }

        if ($actionsPossibles) {
            return $actionsPossibles;
        } else {
            return [
                '-',
            ];
        }
    }

    public function possibleActionsAnonymat($statutEnveloppe, $typeDepot, $cryptage = '0', $enveloppe = false)
    {
        $actionsPossibles = [];
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                if (Atexo_Module::isEnabled('fourEyes')) {
                    if (($enveloppe instanceof CommonEnveloppe)) {
                        $dateOuverture = $enveloppe->getDateheureOuverture();
                        if ($dateOuverture && '0000-00-00 00:00:00' != $dateOuverture) {
                            $diff = Atexo_Util::getDiffDateMinute(date('Y-m-d H:i:s'), $dateOuverture);
                            $xMin = Atexo_Config::getParameter('X_MIN_4_EYES');
                            $idAgentConnecte = Atexo_CurrentUser::getIdAgentConnected();
                            if ($diff < $xMin && $idAgentConnecte != $enveloppe->getAgentIdOuverture2()) {
                                $actionsPossibles = $this->actionsPossibleAnonymat();
                            } elseif ($diff >= $xMin) {
                                $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                            }
                        } else {
                            $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                        }
                    }
                } else {
                    $actionsPossibles = $this->actionsPossibleAnonymat();
                }
            }

            if (
                $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
            ) {
                if (
                    ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) && '1' == $cryptage && Atexo_CurrentUser::hasHabilitation('ImporterEnveloppe')
                ) {
                    $actionsPossibles[Atexo_Config::getParameter('IMPORTER_ENVELOPPE')] = Prado::localize('IMPORTER_ENVELOPPE');
                }
                $actionsPossibles[Atexo_Config::getParameter('DEMANDE_COMPLEMENT')] = Prado::localize('DEMANDE_COMPLEMENT');

                $actionsPossibles = $this->getActionRejete($actionsPossibles);
            }
        } else {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
            }
        }

        if ($actionsPossibles) {
            return $actionsPossibles;
        } else {
            return [
                '-',
            ];
        }
    }

    public function possibleActionsOffreTechnique($statutEnveloppe, $typeDepot, $cryptage = '0', $enveloppe = false)
    {
        $actionsPossibles = [];
        //TODO typeDepoit à passer en paramêtre
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                if (Atexo_Module::isEnabled('fourEyes')) {
                    if (($enveloppe instanceof CommonEnveloppe)) {
                        $dateOuverture = $enveloppe->getDateheureOuverture();
                        if ($dateOuverture && '0000-00-00 00:00:00' != $dateOuverture) {
                            $diff = Atexo_Util::getDiffDateMinute(date('Y-m-d H:i:s'), $dateOuverture);
                            $xMin = Atexo_Config::getParameter('X_MIN_4_EYES');
                            $idAgentConnecte = Atexo_CurrentUser::getIdAgentConnected();
                            if ($diff < $xMin && $idAgentConnecte != $enveloppe->getAgentIdOuverture2()) {
                                $actionsPossibles = $this->actionsPossibleOffreTechnique();
                            } elseif ($diff >= $xMin) {
                                $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                            }
                        } else {
                            $actionsPossibles[Atexo_Config::getParameter('AUTORISER_OUVERTURE')] = Prado::localize('AUTORISER_OUVERTURE');
                        }
                    }
                } else {
                    $actionsPossibles = $this->actionsPossibleOffreTechnique();
                }
            }

            if (
                $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
            ) {
                if (Atexo_CurrentUser::hasHabilitation('GererAdmissibilite')) {
                    $actionsPossibles[Atexo_Config::getParameter('GERE_ADMISSIBILITE')] = Prado::localize('GERE_ADMISSIBILITE');
                }

                if (
                    ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) && '1' == $cryptage && Atexo_CurrentUser::hasHabilitation('ImporterEnveloppe')
                ) {
                    $actionsPossibles[Atexo_Config::getParameter('IMPORTER_ENVELOPPE')] = Prado::localize('IMPORTER_ENVELOPPE');
                }

                $actionsPossibles[Atexo_Config::getParameter('DEMANDE_COMPLEMENT')] = Prado::localize('DEMANDE_COMPLEMENT');

                $actionsPossibles = $this->getActionRejete($actionsPossibles);
            }

            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') && Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
            }
        } else {
            if ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
                if (Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
                }
            } elseif ($statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')) {
                if (Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe')) {
                    $actionsPossibles[Atexo_Config::getParameter('RESTAURER')] = Prado::localize('RESTAURER');
                }
            } else {
                $actionsPossibles[Atexo_Config::getParameter('GERE_ADMISSIBILITE')] = Prado::localize('GERE_ADMISSIBILITE');
            }
        }

        if ($actionsPossibles) {
            return $actionsPossibles;
        } else {
            return ['-'];
        }
    }

    public function allowedToDownloadCandidature($cryptage)
    {
        if ('1' == $cryptage) {
            return false;
        } else {
            if (Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureEnLigne') || Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function isPliDeleted($statutEnveloppe)
    {
        return $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER');
    }

    public function allowedToDownloadOffre($cryptage, $idDossierVolumineux = null)
    {
        if ('1' == $cryptage) {
            return false;
        } else {
            if (Atexo_CurrentUser::hasHabilitation('OuvrirOffreEnLigne') || Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                Atexo_MpeSf::addAllowedDossiersVolumineux($idDossierVolumineux);

                return true;
            } else {
                return false;
            }
        }
    }

    public function allowedToDownloadAnonymat($cryptage)
    {
        if ('1' == $cryptage) {
            return false;
        } else {
            if (Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatEnLigne') || Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function showAdmissibilite($statutEnveloppe)
    {
        return $this->isPliOuvert($statutEnveloppe);
    }

    public function isPliOuvert($statutEnveloppe)
    {
        if (
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERME')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_COURS')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_ATTENTE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER')
        ) {
            return false;
        }

        return true;
    }

    /*
    * Permet de savoir si une réponse est encore fermé ou non
    */
    public function isReponseOuvert($statutOffre)
    {
        if ($statutOffre == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
            return false;
        }
        return true;
    }

    public function getnombreLot()
    {
        return $this->getViewState('nombreLot');
    }

    public function showTotalLot($typeEnv = '1')
    {
        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && $this->getViewState('nombreLot') >= 2) {
            return true;
        } else {
            return false;
        }
    }

    public function getChiffrementEnveloppe()
    {
        return $this->getViewState('chiffrementOffre');
    }

    public function getUrlDownLoad()
    {
        return Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_DOWNLOAD_BLOB');
    }

    public function getUrlUpload()
    {
        return Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_RECEPTION_BLOC_DECHIFFRE');
    }

    public function onCallBackActionEnveloppe($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setTrier($tri);
        $criteriaResponse->setSensTri($sensTri['candidatureElec'][$tri]);
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));

        $candidaturesElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

        if (is_array($candidaturesElectroniques)) {
            $this->nombreCandidatureElectonique->Text = count($candidaturesElectroniques);
            $this->populateData('enveloppeCandidatureElectronique', 'Candidature', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->panelEnveloppeCandidature->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppePapier($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriPapier', []);
        $tri = $this->getViewState('triPapier', '');

        $candidaturePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            '',
            $this->getViewState('typesEnveloppes'),
            $tri,
            $sensTri['candidaturePapier'][$tri],
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            $this->isModeConsultationUnique(),
            null,
            null,
            false,
            true,
            false,
            $this->getViewState('guestsWithEnabling')
        );

        if (is_array($candidaturePapier)) {
            $this->nombreCandidaturePapier->Text = count($candidaturePapier);
            $this->populateData('enveloppeCandidaturePapier', 'CandiPapier', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $this->panelEnveloppeCandidaturePapier->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppeOffre($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');

        $admissible = $this->admissibilteObligatoire();
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setSousPli($this->getViewState('numLotOfre'));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setEnveloppeStatutOuverte($this->isModeConsultationUnique());
        $criteriaResponse->setAdmissible($admissible);
        $criteriaResponse->setTrier($tri);
        $criteriaResponse->setSensTri($sensTri['offreElec'][$tri]);
        $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);

        if (is_array($offresElectroniques)) {
            $this->nombreOffreElectronique->Text = count($offresElectroniques);
            $this->populateData('enveloppeOffreElectronique', 'OffreElec', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->panelEnveloppeOffre->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppeOffreTechnique($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');

        $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $tri, $sensTri, true);

        $this->panelEnveloppeOffreTechnique->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppeOffrePapier($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriPapier', []);
        $tri = $this->getViewState('triPapier', '');
        $admissible = $this->admissibilteObligatoire();
        $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotOfre'),
            $tri,
            $sensTri['offrePapier'][$tri],
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            true,
            false,
            $this->isModeConsultationUnique(),
            $admissible
        );
        if (is_array($offrePapier)) {
            $this->nombreOffrePapier->Text = count($offrePapier);
            $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $this->panelOffrePapier->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppeAnonymat($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');

        $anonymatElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('numLotAnonymat'),
            $this->getViewState('typesEnveloppes'),
            $tri,
            $sensTri['anonymatElec'][$tri],
            Atexo_CurrentUser::getCurrentOrganism(),
            $this->isModeConsultationUnique(),
            $this->isModeConsultationUnique()
        );

        if (is_array($anonymatElectroniques)) {
            $this->nombreAnonymatElectronique->Text = count($anonymatElectroniques);
            $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $this->panelEnveloppeAnonymat->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppeAnonymatPapier($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriPapier', []);
        $tri = $this->getViewState('triPapier', '');

        $anonymatPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotAnonymat'),
            $tri,
            $sensTri['anonymatPapier'][$tri],
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            $this->isModeConsultationUnique()
        );
        if (is_array($anonymatPapier)) {
            $this->nombreAnonymatPapier->Text = count($anonymatPapier);
            $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', $tri, $sensTri);
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }

        $this->panelAnonymatPapier->render($param->NewWriter);
    }

    public function onCallBackActionEnveloppePapierOffreTechnique($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriPapier', []);
        $tri = $this->getViewState('triPapier', '');

        $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', $tri, $sensTri, true);
        $this->panelEnveloppeOffreTechniquePapier->render($param->NewWriter);
    }

    public function EndOfAnalyseParLot($sender, $param)
    {
        $numeroLot = null;
        if ($this->_consultation->getAlloti()) {
            if ($this->identifiantLotOffre->getSelectedValue()) {
                $numeroLot = $this->identifiantLotOffre->getSelectedValue();
            } else {
                $numeroLot = $this->getViewState('numLotOfre');
            }
        }

        $this->calculZeroRat($sender->ID, $numeroLot);
    }

    public function EndOfAnalyseParLotOffreTechnique($sender, $param)
    {
        $numeroLot = null;
        if ($this->_consultation->getAlloti()) {
            if ($this->identifiantLotOffreTechnique->getSelectedValue()) {
                $numeroLot = $this->identifiantLotOffreTechnique->getSelectedValue();
            } else {
                $numeroLot = $this->getViewState('numLotOfreTechnique');
            }
        }
        $this->calculZeroRat($sender->ID, $numeroLot);
    }

    public function EndOfAnalyseParLotAnonymat($sender, $param)
    {
        $numeroLot = null;
        if ($this->_consultation->getAlloti()) {
            if ($this->identifiantLotAnonymat->getSelectedValue()) {
                $numeroLot = $this->identifiantLotAnonymat->getSelectedValue();
            } else {
                $numeroLot = $this->getViewState('numLotAnonymat');
            }
        }
        $this->calculZeroRat($sender->ID, $numeroLot);
    }

    public function EndOfAnalyse($sender, $param)
    {
        $this->calculZeroRat($sender->ID);
    }

    public function calculZeroRat($tabToRefresh, $numeroLot = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultation = CommonConsultationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), $connexion);
        $listeLot = $consultation->getAllLots();
        if (!($consultation instanceof CommonConsultation)) {
            throw new Atexo_Exception("Consultation wasn't found");
        }
        if ($numeroLot) {
            $arrayLot[] = $numeroLot;
        } else {
            if (count($listeLot) > 0) {
                foreach ($listeLot as $lot) {
                    $arrayLot[] = $lot->getLot();
                }
            } else {
                $arrayLot[] = '0';
            }
        }
        $resteATraiterParLot = [];
        $zeroATraiter = false;
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
            $zeroATraiter = true;
        }

        $uneOffreFermer = 0;
        $this->getResteATraiteParLot($consultation->getModeOuvertureReponse(), $resteATraiterParLot, $arrayLot, $zeroATraiter, $uneOffreFermer);
        $unLotTraiter = false;
        foreach ($resteATraiterParLot as $lot => $resteATraiter) {
            if (0 == $resteATraiter) {
                $unLotTraiter = true;
                if ($numeroLot) {
                    $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($this->getViewState('organisme'), Atexo_Util::atexoHtmlEntities($_GET['id']), $numeroLot);
                    if ($categorieLot instanceof CommonCategorieLot) {
                        $categorieLot->setDecision('1');
                        $categorieLot->save($connexion);
                    }
                } else {
                    $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($this->getViewState('organisme'), Atexo_Util::atexoHtmlEntities($_GET['id']), $lot);
                    if ($categorieLot instanceof CommonCategorieLot) {
                        $categorieLot->setDecision('1');
                        $categorieLot->save($connexion);
                    }
                }
            } elseif ($consultation->getModeOuvertureReponse() == Atexo_Config::getParameter('MODE_OUVERTURE_REPONSE') && 0 == $uneOffreFermer) {
                $unLotTraiter = false;
                break;
            }
        }
        if ($unLotTraiter) {
            $etatDecision = true;
            $listeLot = $consultation->getAllLots();
            foreach ($listeLot as $lot) {
                if ($lot instanceof CommonCategorieLot && $lot->getDecision() != Atexo_Config::getParameter('LOT_EN_DECISION')) {
                    $etatDecision = false;
                }
            }
            if ($etatDecision) {
                $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
                $consultation->setDateDecision(date('Y-m-d'));
                $consultation->setDecisionPartielle(Atexo_Config::getParameter('DECISION_PARTIELLE_NON'));
            } else {
                $consultation->setDecisionPartielle(Atexo_Config::getParameter('DECISION_PARTIELLE_OUI'));
            }
            if ($consultation->getDatefin() < date('Y-m-d H:i:s')) {
                $consultation->setDepouillablePhaseConsultation('0');
            }
            $consultation->save($connexion);
            $this->response->redirect('?page=Agent.TableauDeBord&id=' . $consultation->getId());
        } else {
            if ($consultation->getModeOuvertureReponse() == Atexo_Config::getParameter('MODE_OUVERTURE_REPONSE') && 0 == $uneOffreFermer) {
                $this->javascriptFinAnalyse->Text = "<script>openModal('modal-confirmation-decision','popup-800',document.getElementById('ctl0_CONTENU_PAGE_titrePopin'));</script>";
            } else {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_FIN_ANALYSE'));
            }

            if (!Atexo_Module::isEnabled('DepouillementEnveloppeDependRatEnveloppePrecedente')) {
                switch ($tabToRefresh) {
                    case 'linkEndReponses':
                        $this->fillRepeaterReponses();
                        break;
                    case 'linkEndCandidature':
                        $this->fillRepeatersCandidature();
                        break;
                    case 'linkEndOffreTechnique':
                        $this->fillRepeatersOffreTechnique();
                        break;
                    case 'linkEndOffre':
                        $this->fillRepeatersOffre();
                        break;
                    case 'linkEndAnonymat':
                        $this->fillRepeatersAnonymat();
                        break;
                }
            }
        }
    }

    public function enveloppeStillClosed($statutEnveloppe)
    {
        if (
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
        ) {
            return false;
        }
        return true;
    }

    public function openNotChiffredEnveloppe($idEnveloppe, $offreId, $typeEnv, $sousPli, $formatEnveloppe)
    {
        return (new Atexo_Consultation_Responses())->openUnChiffredEnveloppe($idEnveloppe, $offreId, $typeEnv, $sousPli, $formatEnveloppe, $this->getViewState('organisme'));
    }

    public function writeAdmissibilite($nombreAdmissiblite, $typeAdmissibilite, $typeEnv = '1')
    {
        if (($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && $this->getViewState('nombreLot') <= 1) || $typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if ($nombreAdmissiblite > 0) {
                if ('1' == $typeAdmissibilite) {
                    return Prado::localize('DEFINE_OUI');
                } elseif ('2' == $typeAdmissibilite) {
                    return Prado::localize('DEFINE_NON');
                } else {
                    return Prado::localize('A_TRAITER');
                }
            } else {
                return '-';
            }
        } else {
            return $nombreAdmissiblite . ' ' . (($nombreAdmissiblite < 2) ? Prado::localize('LOT') : Prado::localize('LOTS'));
        }
    }

    /**
     * Permet de formater le nom du dossier volumineux à afficher.
     *
     * @param  $enveloppe
     * @param  $index
     * @param bool $numLot
     *
     * @return string
     */
    public function writeNomFichierDossierVolumineux($enveloppe, $index, $numLot = false)
    {
        $fileName = 'EL' . ($index) . '_Dossier_Volumineux_Offre';

        if ($numLot) {
            $fileName .= '_' . Prado::localize('LOT') . '_' . $numLot;
        }

        return $fileName;
    }

    /**
     * Permet d'afficher le complément d'information concernant le dossier volumineux.
     *
     * @param  $enveloppe
     *
     * @return string|null
     *
     * @throws PropelException
     */
    public function writeComplementDossierVolumineux($enveloppe)
    {
        if (!$enveloppe instanceof CommonEnveloppe) {
            return false;
        }

        $dossierVolumineux = (new Atexo_DossierVolumineux_Responses())->retrieveDossierVolumineux(
            $enveloppe->getIdDossierVolumineux()
        );
        $complement = null;
        if (!empty($dossierVolumineux)) {
            $complement = '('
                . $dossierVolumineux->getNom()
                . ' ' . $dossierVolumineux->getDateCreation('d/m/Y')
                . ' - '
                . Atexo_Util::GetSizeName($dossierVolumineux->getTaille(), 'Mo')
                . ')';
        }

        return $complement;
    }

    /**
     * Permet de savoir si un dossier volumineux est rattaché à l'enveloppe courante.
     *
     * @param  $enveloppe
     *
     * @return bool
     */
    public function dossierVolumineuxNotNull($enveloppe)
    {
        if ($enveloppe instanceof CommonEnveloppe && !empty($enveloppe->getIdDossierVolumineux())) {
            return true;
        }

        return false;
    }

    public function writeNomFichier($enveloppe, $index, $numLot = false)
    {
        $fileName = 'EL' . ($index) . '_';
        if ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $fileName .= Prado::localize('ENVELOPPE_CANDIDATURE');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $fileName .= Prado::localize('ENVELOPPE_OFFRE');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $fileName .= Prado::localize('ENVELOPPE_ANONYMAT');
        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $fileName .= Prado::localize('ENVELOPPE_OFFRE_TECHNIQUE');
        } else {
            throw new Atexo_Exception('type enveloppe inconnu');
        }
        if ($numLot) {
            $fileName .= '_' . Prado::localize('LOT') . '_' . $numLot;
        }

        return $fileName . '.zip';
    }

    public function pliApresDateLimite($dateRemisePli)
    {
        if (strcmp($this->getViewState('dateFin', null), Atexo_Util::frnDateTime2iso($dateRemisePli)) < 0) {
            return 'time-red';
        }
    }

    /**
     * Téléchargement des plis chiffrés ou non.
     *
     * @param $sender Paramètre prado
     * @param $param  Paramètre prado
     *
     * @return void
     */
    public function downloadPlis($sender, $param)
    {
        $arrayIdsEnveloppe = [];
        $arrayIdsOffre = [];
        $scriptIdEnv = '';
        $repeaterName = $param->CommandName;
        $typeEnveloppe = $param->CommandParameter;

        $checkBoxName = 'check' . $typeEnveloppe;
        $folderBrowserName = $typeEnveloppe . 'ElRepDestination';

        $referenceCons = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $organisme = $this->getViewState('organisme');

        if ('Reponse' == $typeEnveloppe) {
            foreach ($this->$repeaterName->Items as $itemConsultation) {
                if ($itemConsultation->$checkBoxName->getChecked()) {
                    $arrayIdsOffre[] = $itemConsultation->idOffre->Value;
                }
            }
        } else {
            foreach ($this->$repeaterName->Items as $itemConsultation) {
                if ($itemConsultation->$checkBoxName->getChecked()) {
                    $arrayIdsOffre[] = $itemConsultation->idOffre->Value;
                    $arrayIdsEnveloppe[] = $itemConsultation->idEnveloppes->Value;
                    $scriptIdEnv .= 'ajouterIdEnveloppe(' . $itemConsultation->idEnveloppes->Value . '); ';
                }
            }
        }
        $AllReponsesAnnonce =  (new Atexo_Entreprise_Reponses())->getReponsesAnnoncesXml([$referenceCons], $organisme, $arrayIdsOffre);
        $AllReponsesAnnonceEnCode = base64_encode($AllReponsesAnnonce);

        $urlDownloadBloc = Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_DOWNLOAD_BLOC_CHIFFRE');
        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
            // Atexo crypto
            $offres = (new Atexo_Entreprise_Reponses())->getOffres($referenceCons, $organisme, $arrayIdsOffre);
            (new Atexo_Entreprise_Reponses())->addEnveloppesToDechiffre($offres, $arrayIdsEnveloppe);

            if (is_array($offres)) {
                $resultat = $this->getDechiffrementCrypto($offres, Atexo_CurrentUser::getId(), $this->_consultation, false);
                if ($resultat) {
                    $name = str_replace('[COMPLET]', $this->_consultation->getReferenceUtilisateur() . '-' . date('Y-m-d H:i'), utf8_decode(Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_TELECHARGEMENT_PLI')));
                    $jnpl = ['content' => $resultat, 'name' => $name];
                    $this->setViewState('resultatJnlp', $jnpl);
                    if ($this->isMAMPEnabled()) {
                        $this->javascript->Text = MessageStatusCheckerUtil::getScript($resultat);
                    } else {
                        $this->javascript->Text = "<script> document.getElementById('ctl0_CONTENU_PAGE_downloadJnlp').click();</script>";
                    }
                } else {
                    $this->panelMessageErreurDechiffrement->setMessage(Prado::localize('ERREUR_RECUPERATION_JNPL_TELCHARGEMENT'));
                    $this->panelMessageErreurDechiffrement->showMessage();
                }
            }
        } else {
            $this->javascript->Text = "<script> launchDownloadPlis('" . $AllReponsesAnnonceEnCode . "', '" .
                $organisme . "', '" . $urlDownloadBloc . "', '" . $folderBrowserName . "');" . $scriptIdEnv . ' document.MpeDechiffrementApplet.executer();</script>';
        }

        $this->setViewState('idsEnveloppes', $arrayIdsEnveloppe);
        $this->setViewState('idsOffres', $arrayIdsOffre);
        $this->setViewState('repertoire', $this->$folderBrowserName->Text);
        $this->setViewState('typeEnveloppe', $typeEnveloppe);
    }

    public function refreshPlis($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $arrayIdsEnveloppe = $this->getViewState('idsEnveloppes');
        $arrayIdsOffre = $this->getViewState('idsOffres');
        $organisme = $this->getViewState('organisme');
        $repertoire = $this->getViewState('repertoire');

        $this->saveTelechargementInfoEnveloppes($arrayIdsEnveloppe, $organisme, $repertoire, $connexion);
        $this->saveTelechargementInfoOffres($arrayIdsOffre, $organisme, $repertoire, $connexion);

        switch ($this->getViewState('typeEnveloppe')) {
            case 'Candidature':
                $this->onCallBackActionEnveloppe($sender, $param);
                echo "refresh repeater candidature \n";
                break;
            case 'Offre':
                $this->onCallBackActionEnveloppeOffre($sender, $param);
                echo "refresh repeater offre \n";
                break;
            case 'OffreTechnique':
                $this->onCallBackActionEnveloppeOffreTechnique($sender, $param);
                echo "refresh repeater offre \n";
                break;
            case 'Anonymat':
                $this->onCallBackActionEnveloppeAnonymat($sender, $param);
                echo "refresh repeater anonymat \n";
                break;
            case 'Reponse':
                $this->onCallBackActionReponseElectronique($sender, $param);
                echo "refresh repeater reponse \n";
                break;
            default:
                break;
        }
    }

    /*
    * permet d'enregistrer les information de telechargement des enveloppes
    */
    public function saveTelechargementInfoEnveloppes($arrayIdsEnveloppe, $organisme, $repertoire, $connexion)
    {
        if (is_array($arrayIdsEnveloppe)) {
            foreach ($arrayIdsEnveloppe as $idEnveloppe) {
                $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexion);
                if (!$enveloppe->getAgentTelechargement()) {
                    $enveloppe->setAgentTelechargement(Atexo_CurrentUser::getIdAgentConnected());
                }
                if (!$enveloppe->getDateTelechargement()) {
                    $enveloppe->setDateTelechargement(date('Y-m-d H:i'));
                }
                $enveloppe->setRepertoireTelechargement($repertoire);
                $enveloppe->save($connexion);
            }
        }
    }

    /*
    * permet d'enregistrer les information de telechargement des offres
    */
    public function saveTelechargementInfoOffres($arrayIdsOffre, $organisme, $repertoire, $connexion)
    {
        if (is_array($arrayIdsOffre)) {
            foreach ($arrayIdsOffre as $idOffre) {
                $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexion);
                if ($offre instanceof CommonOffres) {
                    if (!$offre->getAgentTelechargementOffre()) {
                        $offre->setAgentTelechargementOffre(Atexo_CurrentUser::getIdAgentConnected());
                    }
                    if (!$offre->getDateTelechargementOffre()) {
                        $offre->setDateTelechargementOffre(date('Y-m-d H:i'));
                    }
                    $offre->setRepertoireTelechargementOffre($repertoire);
                    $offre->save($connexion);
                }
            }
        }
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function printDepouillementConsPdf($sender, $param)
    {
        $odtFilePath = $this->printDepouillementConsOdt();
        if ($odtFilePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(
                Atexo_Config::getParameter('NOM_FICHIER_DEPOUILLEMENT_N_LOTS_CONSULTATION') . '_' .
                $this->_consultation->getReferenceUtilisateur(true) . '.pdf',
                $pdfContent
            );
        }
    }

    public function printDepouillementConsRtf($sender, $param)
    {
        $odtFilePath = $this->printDepouillementConsOdt();
        if ($odtFilePath) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(
                Atexo_Config::getParameter('NOM_FICHIER_DEPOUILLEMENT_N_LOTS_CONSULTATION') . '_' .
                $this->_consultation->getReferenceUtilisateur(true) . '.rtf',
                $rtfContent
            );
        }
    }

    public function printDepouillementConsOdt()
    {
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        if ('0' != $this->_consultation->getEnvCandidature()) {
            $tableKeys['<!--lineContentCandidature-->'] = $impr->imprimerTableauCandidature($this->_consultation);
        }
        if ('0' != $this->_consultation->getEnvOffreTechnique()) {
            $tableKeys['<!--lineContentOffresTechniques-->'] = $impr->imprimerTableauxOffresTechniques($this->_consultation);
        }
        if ('0' != $this->_consultation->getEnvOffre()) {
            $tableKeys['<!--lineContentOffres-->'] = $impr->imprimerTableauxOffres($this->_consultation);
        }
        if ('0' != $this->_consultation->getEnvAnonymat()) {
            $tableKeys['<!--lineContentAnonymat-->'] = $impr->imprimerTableauxAnonymat($this->_consultation);
        }
        $consSammury = $this->arrayConsultationSummary();
        $tableKeys = array_merge($consSammury, $tableKeys);
        /* if(is_file(Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION'))) {
            $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION');
        }else {
            $modelePath = "../../../".Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION');
        }*/
        $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION');

        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }

    public function arrayConsultationSummary()
    {
        $tableKeys = [];
        $org = $this->_consultation->getOrganisme();
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
        $entitePublique = '';
        //Correctif pour résoudre le problème de ArchiverConsultations ($organismeO=null)
        if (!empty($organismeO)) {
            $entitePublique = Atexo_Util::escapeXmlChars($organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg());
        }
        $tableKeys['entitePublique'] = $entitePublique;
        $tableKeys['entiteAchat'] = Atexo_Util::escapeXmlChars(Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceId(), $org));
        $tableKeys['refConsultation'] = Atexo_Util::escapeXmlChars($this->_consultation->getReferenceUtilisateur());
        $tableKeys['intituleConsultation'] = Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($this->_consultation->getIntitule()));
        $tableKeys['objetConsultation'] = Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($this->_consultation->getObjet()));
        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($this->_consultation->getIdTypeAvis());
        if ($typeAvis) {
            $tableKeys['typeAnnonce'] = Atexo_Util::escapeXmlChars($typeAvis->getIntituleAvis());
        }
        $tableKeys['typeProcedure'] = Atexo_Util::escapeXmlChars((new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->_consultation->getIdTypeProcedureOrg(), false));
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();
        $categorie = $categorieConsultationQuery->getCategorieConsultationById($this->_consultation->getCategorie());
        $tableKeys['categoriePrincipale'] = Atexo_Util::escapeXmlChars($categorie->getLibelleTraduit());
        $tableKeys['dateRemisePlis'] = Atexo_Util::iso2frnDateTime($this->_consultation->getDatefin());
        $dateMiseEnLigne = Atexo_Util::iso2frnDateTime(Atexo_Consultation::getDateMiseEnLigne($this->_consultation));
        if ($dateMiseEnLigne) {
            $tableKeys['dateMiseLigne'] = Atexo_Util::escapeXmlChars($dateMiseEnLigne);
        } else {
            $tableKeys['dateMiseLigne'] = '-';
        }
        $lots = $this->_consultation->getAllLots();
        $scriptXmlLots = '';
        if (0 != count($lots)) {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_OUI');
            foreach ($lots as $oneLot) {
                //<text:p text:style-name="P7">lot</text:p>
                $scriptXmlLots .= '<text:p text:style-name="P7">Lot :' . $oneLot->getLot() . ' ' . Atexo_Util::escapeXmlChars($oneLot->getDescription()) . '</text:p>';
            }
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        } else {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_NON');
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        }

        return $tableKeys;
    }

    public function showCoffreFort($nomEntreprise)
    {
        if (Atexo_Module::isEnabled('AccesAgentsCfeOuvertureAnalyse', Atexo_CurrentUser::getCurrentOrganism()) && '-' != $nomEntreprise && $nomEntreprise) {
            return true;
        }

        return false;
    }

    public function isConsultationMPS()
    {
        if ('1' == $this->_consultation->getMarchePublicSimplifie()) {
            return true;
        }

        return false;
    }

    /**
     * Cette methode definit s'il faut afficher le bouton MPS
     * si le param "PF_MPS_EXTERNE" est activé le lien redirige vers une pf externe sinn il pertmet de telecharger la candidatureMps
     * On affiche le lien si la consultation est tagguee MPS et si l'entreprise
     * ayant deposee l'offre est residante en France avec un numero de siret dans son compte.
     *
     * @param CommonOffres    $objEnveloppe objet de type CommonOffres,cas d'une ouverture par reponse
     * @param CommonEnveloppe $objEnveloppe objet de type CommonEnveloppe,cas d'une ouverture par dossier
     *
     * @return bool true or false
     *
     * @author    ASO <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.7.0
     *
     * @copyright Atexo 2014
     */
    public function showMPS($objEnveloppeouOffre)
    {
        if (Atexo_Module::isEnabled('MarchePublicSimplifie') && '1' == $this->_consultation->getMarchePublicSimplifie()) {
            $offre = false;
            if ($objEnveloppeouOffre instanceof CommonOffres) {//cas d'une ouverture par reponse
                $offre = $objEnveloppeouOffre;
            }
            if ($objEnveloppeouOffre instanceof CommonEnveloppe && $objEnveloppeouOffre->getCommonOffres() instanceof CommonOffres) {//cas d'une ouverture par dossier
                $offre = $objEnveloppeouOffre->getCommonOffres();
            }
            if ($offre) {
                $candidatureMpsObject = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($offre->getId(), $this->_consultation->getId());
                if ($offre->getSiretEntreprise() && ($candidatureMpsObject instanceof CommonTCandidatureMps || $offre->getCandidatureIdExterne())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Cette methode permet de recuperer l'url MPS
     * si le param "PF_MPS_EXTERNE" est activé le lien redirige vers une pf externe sinn il pertmet de telecharger la candidatureMps.
     *
     * @param CommonOffres    $objEnveloppe objet de type CommonOffres,cas d'une ouverture par reponse
     * @param CommonEnveloppe $objEnveloppe objet de type CommonEnveloppe,cas d'une ouverture par dossier
     *
     * @return string $urlMPS lienMps
     *
     * @author    ASO <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlMPS($objEnveloppeouOffre)
    {
        $urlMPS = '#';
        if ($objEnveloppeouOffre instanceof CommonOffres) {//cas d'une ouverture par reponse
            $offre = $objEnveloppeouOffre;
        }
        if ($objEnveloppeouOffre instanceof CommonEnveloppe && $objEnveloppeouOffre->getCommonOffres() instanceof CommonOffres) {//cas d'une ouverture par dossier
            $offre = $objEnveloppeouOffre->getCommonOffres();
        }
        if ($offre) {
            $candidatureMpsObject = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($offre->getId(), $this->_consultation->getId());
            if ($candidatureMpsObject instanceof CommonTCandidatureMps) {
                $urlMPS = '?page=Agent.DownloadCandidatureMps&idOffre=' . base64_encode($offre->getId()) . '&id=' . base64_encode($this->_consultation->getId());
            }
        }

        return $urlMPS;
    }

    /***
     * affiche l'icone de l'offre variante
     */
    public function isIconeOffreVarianteDisplayed($objEnveloppe)
    {
        $varianteCalcule = '0';
        $donnesComplementaire = $this->_consultation->getDonneComplementaire();
        if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
            $varianteCalcule = $donnesComplementaire->getVarianteCalcule();
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires') && '1' == $varianteCalcule) {
            if ($objEnveloppe instanceof CommonEnveloppe) {
                if ($objEnveloppe->getCommonOffres() && $objEnveloppe->getCommonOffres() instanceof CommonOffres && '1' == $objEnveloppe->getCommonOffres()->getOffreVariante()) {
                    return true;
                }
            } elseif ($objEnveloppe instanceof CommonEnveloppePapier) {
                $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($objEnveloppe->getOffrePapierId(), Atexo_CurrentUser::getCurrentOrganism());
                if ($offrePapier && '1' == $offrePapier->getOffreVariante()) {
                    return true;
                }
            }
        }

        return false;
    }

    /***
     * affiche l'icone de l'offre de base
     */
    public function isIconeOffreBaseDisplayed($objEnveloppe)
    {
        $varianteCalcule = '0';
        $donnesComplementaire = $this->_consultation->getDonneComplementaire();
        if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
            $varianteCalcule = $donnesComplementaire->getVarianteCalcule();
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires') && '1' == $varianteCalcule) {
            if ($objEnveloppe instanceof CommonEnveloppe) {
                if ($objEnveloppe->getCommonOffres() && $objEnveloppe->getCommonOffres() instanceof CommonOffres && '0' == $objEnveloppe->getCommonOffres()->getOffreVariante()) {
                    return true;
                }
            } elseif ($objEnveloppe instanceof CommonEnveloppePapier) {
                $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($objEnveloppe->getOffrePapierId(), Atexo_CurrentUser::getCurrentOrganism());
                if ($offrePapier && '0' == $offrePapier->getOffreVariante()) {
                    return true;
                }
            }
        }

        return false;
    }

    /****
     * verifiier si la consultation accepte la variante aussi si le module est activé
     */
    public function isConsultationWithVariante()
    {
        $varianteCalcule = '0';
        $donnesComplementaire = $this->_consultation->getDonneComplementaire();
        if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
            $varianteCalcule = $donnesComplementaire->getVarianteCalcule();
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires') && '1' == $varianteCalcule) {
            return true;
        }

        return false;
    }

    public function getExcelPvOuvertureOffres()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if ($this->getViewState('numLotOfre')) {
            $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($this->getViewState('organisme'), Atexo_Util::atexoHtmlEntities($_GET['id']), $this->getViewState('numLotOfre'));
        }
        $admissible = $this->admissibilteObligatoire();
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setSousPli($this->getViewState('numLotOfre'));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setEnveloppeStatutOuverte($this->isModeConsultationUnique());
        $criteriaResponse->setAdmissible($admissible);

        $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            $this->getViewState('typesEnveloppes'),
            $this->getViewState('numLotOfre'),
            '',
            '',
            Atexo_CurrentUser::getCurrentOrganism(),
            false,
            true,
            false,
            $this->isModeConsultationUnique(),
            $admissible
        );

        $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
        $offres = array_merge($offresElectroniques, $offrePapier);
        if ($categorieLot) {
            $idLot = $categorieLot->getLot();
        } else {
            $idLot = '0';
        }
        foreach ($offres as $one) {
            if ($one instanceof CommonEnveloppe) {
                $idEnveloppe = $one->getIdEnveloppeElectro();
            }
            (new Atexo_ActeDEngagement())->saveMontantOffre(
                $idEnveloppe,
                $this->_consultation->getId(),
                $idLot,
                Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'),
                $this->isConsultationWithVariante()
            );
        }
        (new Atexo_GenerationExcel())->generationPvOuvertureDesOffres(
            $this->_consultation, $offres, $categorieLot, null, $this->isConsultationWithVariante()
        );
    }

    public function getEvaluationEnveloppe($idEnvElect)
    {
        $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById($idEnvElect, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
        if ($enveloppe instanceof CommonEnveloppe) {
            $critEval = (new Atexo_CriteresEvaluation())->retrieveCriteresEvaluationByRefConsAndTypeEnvAndLot(
                Atexo_Util::atexoHtmlEntities($_GET['id']),
                Atexo_CurrentUser::getCurrentOrganism(),
                $enveloppe->getTypeEnv(),
                $enveloppe->getSousPli()
            );
            if ($critEval instanceof CommonCriteresEvaluation) {
                $evalEnv = Atexo_CriteresEvaluation::retreiveEnveloppeCriteval($critEval->getId(), $enveloppe->getIdEnveloppeElectro());

                return $evalEnv;
            }
        }

        return false;
    }

    public function getNoteEvaluationEnveloppe($idEnvElect)
    {
        $evalEnv = $this->getEvaluationEnveloppe($idEnvElect);
        if ($evalEnv instanceof CommonEnveloppeCritereEvaluation) {
            return floor($evalEnv->getNoteTotale());
        } else {
            return '';
        }
    }

    public function getRejetEvaluationEnveloppe($idEnvElect)
    {
        $evalEnv = $this->getEvaluationEnveloppe($idEnvElect);
        if ($evalEnv instanceof CommonEnveloppeCritereEvaluation) {
            if ('1' == $evalEnv->getRejet()) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getTitleStatutCritEval($idEnvElect)
    {
        $evalEnv = $this->getEvaluationEnveloppe($idEnvElect);
        if ($evalEnv instanceof CommonEnveloppeCritereEvaluation) {
            $statut = $evalEnv->getStatutCritereEvaluation();
            if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
                return Prado::localize('TEXT_BROUILLON_NON_CONFORME');
            }
            if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
                return Prado::localize('EN_COURS');
            }
            if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
                return Prado::localize('TEXT_FINI_VALIDE');
            }
        }

        return Prado::localize('TEXT_BROUILLON_NON_CONFORME');
    }

    public function getPictoCritEval($idEnvElect)
    {
        $evalEnv = $this->getEvaluationEnveloppe($idEnvElect);
        if ($evalEnv instanceof CommonEnveloppeCritereEvaluation) {
            $statut = $evalEnv->getStatutCritereEvaluation();
            if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
                return 'picto-feu-rouge.gif';
            }
            if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
                return 'picto-feu-orange.gif';
            }
            if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
                return 'picto-feu-vert.gif';
            }
        }

        return 'picto-feu-rouge.gif';
    }

    public function getUrlVisualisationFormCons($idEnvElectr, $typeFormCons)
    {
        $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById($idEnvElectr, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
        if ($enveloppe instanceof CommonEnveloppe) {
            $formCons = (new Atexo_FormConsultation())->getFormconsByTypeFormRefConsLotAndTypeEnv(
                Atexo_Util::atexoHtmlEntities($_GET['id']),
                Atexo_CurrentUser::getCurrentOrganism(),
                $enveloppe->getTypeEnv(),
                $typeFormCons,
                $enveloppe->getSousPli()
            );
            if ($formCons instanceof CommonConsultationFormulaire) {
                if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('QUESTIONNAIRE')) {
                    return "javascript:popUp('index.php?page=Agent.PopUpDetailQuestionnaireAgent&id=" . $formCons->getId() . '&evaluation&idPli=' . $idEnvElectr . "','yes');";
                }
                if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
                    return "javascript:popUp('index.php?page=Agent.PopUpDetailBordereauDePrixAgent&id=" . $formCons->getId() . '&evaluation&idPli=' . $idEnvElectr . "','yes');";
                }
            }

            return '';
        }
    }

    public function openPopUpSynthese($typeEnv, $callBackbutton)
    {
        return "javascript:popUp('?page=Agent.PopUpSyntheseEvaluation&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&numLot=' .
            $this->getViewState('numLotOfre') . '&typeEnv=' . $typeEnv . '&callBackbutton=' . $callBackbutton . "','yes');";
    }

    /*
    * permet d'ouvrir la popup des gestion des avis des membres de commission
    */
    public function openPopUpAvisCAO($offre, $typeEnv, $formeEnveloppe, $sousPli, $callBackbutton)
    {
        return "javascript:popUp('?page=Agent.popUpAvisMembresCommission&offre=" . $offre . '&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) .
            '&typeEnv=' . $typeEnv . '&formeEnveloppe=' . $formeEnveloppe . '&callBackbutton=' . $callBackbutton . '&sousPli=' . $sousPli . "','yes');";
    }

    public function actionsPossibleCandidature($actionUnique = true)
    {
        $actionsPossibles = [];
        if (Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureEnLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('OUVRIR_EN_LIGNE')] = Prado::localize('OUVRIR_EN_LIGNE');
        }

        if (Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureHorsLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('OUVERTURE_HORS_LIGNE');
        }

        if ($actionUnique && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
            $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
        }
        if (!$actionUnique && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $actionsPossibles[Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS')] = Prado::localize('TELECHARGER_PLIS_OUVERTS');
        }

        return $actionsPossibles;
    }

    public function actionsPossibleOffreTechnique($actionUnique = true)
    {
        $actionsPossibles = [];
        if (Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueEnLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('OUVRIR_EN_LIGNE')] = Prado::localize('OUVRIR_EN_LIGNE');
        }

        if (Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueHorsLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('OUVERTURE_HORS_LIGNE');
        }
        if ($actionUnique && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
            $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
        }
        if (!$actionUnique && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $actionsPossibles[Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS')] = Prado::localize('TELECHARGER_PLIS_OUVERTS');
        }

        return $actionsPossibles;
    }

    public function actionsPossibleOffre($actionUnique = true)
    {
        $actionsPossibles = [];
        if (Atexo_CurrentUser::hasHabilitation('OuvrirOffreEnLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('OUVRIR_EN_LIGNE')] = Prado::localize('OUVRIR_EN_LIGNE');
        }

        if (Atexo_CurrentUser::hasHabilitation('OuvrirOffreHorsLigne')) {
            $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('OUVERTURE_HORS_LIGNE');
        }
        if ($actionUnique && ($this->getViewState('typesEnveloppes') == Atexo_Config::getParameter('OFFRE_SEUL')) && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
            $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
        }
        if (!$actionUnique && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $actionsPossibles[Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS')] = Prado::localize('TELECHARGER_PLIS_OUVERTS');
        }

        return $actionsPossibles;
    }

    public function actionsPossibleAnonymat($depotPapier = false, $actionUnique = true)
    {
        $actionsPossibles = [];
        if (!$depotPapier) {
            if (Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatEnLigne')) {
                $actionsPossibles[Atexo_Config::getParameter('OUVRIR_EN_LIGNE')] = Prado::localize('OUVRIR_EN_LIGNE');
            }

            if (Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatHorsLigne')) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('OUVERTURE_HORS_LIGNE');
            }
            if (!$actionUnique && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                $actionsPossibles[Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS')] = Prado::localize('TELECHARGER_PLIS_OUVERTS');
            }
        } else {
            $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
        }

        return $actionsPossibles;
    }

    /*
     * les action possible d'une réponse fermée
     */
    public function actionsPossibleReponse($typeDepot, $actionUnique = true)
    {
        $actionsPossibles = [];
        if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if (
                ($this->_enveloppeCandidatureExist && Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureEnLigne'))
                || ($this->_eneloppeOffreExist && Atexo_CurrentUser::hasHabilitation('OuvrirOffreEnLigne'))
                || ($this->_eneloppeAnonymatExist && Atexo_Module::isEnabled('EnveloppeAnonymat') && Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatEnLigne'))
                || ($this->_eneloppeOffreTechniqueExist && Atexo_Module::isEnabled('EnveloppeOffreTechnique') && Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueEnLigne'))
            ) {
                $actionsPossibles[Atexo_Config::getParameter('OUVRIR_EN_LIGNE')] = Prado::localize('OUVRIR_EN_LIGNE');
            }

            if (
                ($this->_enveloppeCandidatureExist && Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureHorsLigne'))
                || ($this->_eneloppeOffreExist && Atexo_CurrentUser::hasHabilitation('OuvrirOffreHorsLigne'))
                || ($this->_eneloppeAnonymatExist && Atexo_Module::isEnabled('EnveloppeAnonymat') && Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatHorsLigne'))
                || ($this->_eneloppeOffreTechniqueExist && Atexo_Module::isEnabled('EnveloppeOffreTechnique') && Atexo_CurrentUser::hasHabilitation('OuvrirOffreTechniqueHorsLigne'))
            ) {
                $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('OUVERTURE_HORS_LIGNE');
            }

            if ($actionUnique && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
            }
            if (!$actionUnique && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                $actionsPossibles[Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS')] = Prado::localize('TELECHARGER_PLIS_OUVERTS');
            }
        } elseif ($typeDepot == Atexo_Config::getParameter('DEPOT_PAPIER')) {
            $actionsPossibles[Atexo_Config::getParameter('RENSEIGNER_STATUT')] = Prado::localize('RENSEIGNER_STATUT');
            if ($actionUnique && Atexo_CurrentUser::hasHabilitation('RefuserEnveloppe')) {
                $actionsPossibles[Atexo_Config::getParameter('REFUSER')] = Prado::localize('REFUSER');
            }
        }

        return $actionsPossibles;
    }

    public function displayPanelActions($enveloppe)
    {
        if (
            Atexo_Module::isEnabled('fourEyes')
            && $enveloppe instanceof CommonEnveloppe
            && $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME')
        ) {
            $dateOuverture = $enveloppe->getDateheureOuverture();
            if ($dateOuverture && '0000-00-00 00:00:00' != $dateOuverture) {
                $diff = Atexo_Util::getDiffDateMinute(date('Y-m-d H:i:s'), $dateOuverture);
                $xMin = Atexo_Config::getParameter('X_MIN_4_EYES');
                $idAgentConnecte = Atexo_CurrentUser::getIdAgentConnected();
                if ($diff < $xMin && $idAgentConnecte == $enveloppe->getAgentIdOuverture()) {
                    return 'display:none';
                }
            }
        }

        return '';
    }

    public function printDecisionConsOdt()
    {
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        if ('1' == $this->_consultation->getAlloti()) {
            $allLots = $this->_consultation->getAllLots();
            foreach ($allLots as $lot) {
                $tableKeys['<!-- lineEnteteDecision -->'] .= $impr->printTableauDecision($this->_consultation, $lot->getLot());
            }
        } else {
            $tableKeys['<!-- lineEnteteDecision -->'] = $impr->printTableauDecision($this->_consultation, 0);
        }
        $consSammury = $this->arrayConsultationSummary();
        $tableKeys = array_merge($consSammury, $tableKeys);
        $modelePath = dirname(__FILE__) . '/../../../' . Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DECISION_CONSULTATION');
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }

    public function printDecisionConsPdf($sender, $param)
    {
        $odtFilePath = $this->printDecisionConsOdt();
        if ($odtFilePath) {
            $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(
                Atexo_Config::getParameter('NOM_FICHIER_DECISION_N_LOTS_CONSULTATION') . '_' .
                $this->_consultation->getReferenceUtilisateur(true) . '.pdf',
                $pdfContent
            );
        }
    }

    public function printDecisionConsRtf($sender, $param)
    {
        $odtFilePath = $this->printDecisionConsOdt();
        if ($odtFilePath) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DECISION_N_LOTS_CONSULTATION') . '_' . $this->_consultation->getReferenceUtilisateur(true) . '.rtf', $rtfContent);
        }
    }

    /*
     * Permet d'avoir les parametres pour l'applet MpeDechiffrementApplet
     */
    public function getParamsMpeDechiffrementApplet()
    {
        $methodeRenvoi = 'renvoiResultatMpeDechiffrement';
        if (isset($_GET['telechargerOffres'])) {
            $methodeRenvoi = 'jsPlisDownloaded';
        }

        return ['methodeJavascriptDebutAttente' => 'afficherVeuillezPatienter', 'methodeJavascriptFinAttente' => 'masquerVeuillezPatienter', 'methodeJavascriptRenvoiResultat' => $methodeRenvoi];
    }

    /*
     * Permet de retourner xml de la réponse de l'annonce en base 64 de l'offre d'un enveloppe
     * @param : $enveloppe
     */
    public function getReponseAnnonceXml($enveloppe)
    {
        $xml = '';
        if ($enveloppe->getOffre() instanceof CommonOffres) {
            $xmlString = $enveloppe->getOffre()->getXmlString();
            $domDocument = new DOMDocument();
            $xmlString = substr($xmlString, strpos($xmlString, '?>') + 2);
            Atexo_Xml::addElement($domDocument, $domDocument, 'ReponsesAnnonce', ($xmlString));
            $xml = $domDocument->saveXml();
            $xml = Atexo_Util::xmlStrReplaceSpecCarc($xml);
            $xml = base64_encode($xml);
        }

        return $xml;
    }

    /*
      * Permet d'avoir Url de téléchargement des blocs chiffrés
      */
    public function getUrlDownLoadPliChiffre()
    {
        return Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_DOWNLOAD_BLOC_CHIFFRE');
    }

    /*
    *    Permet d'avoir Url d'envoi des blocs déchiffrés
    */
    public function getUrlReceptionBlocDechiffre()
    {
        return Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_RECEPTION_BLOC_DECHIFFRE_APPLET');
    }

    /*
     * Permet d'avoir les parametres pour l'applet SelectionFichierApplet
     */
    public function getParamsSelectionFichierApplet()
    {
        return ['methodeJavascriptDebutAttente' => 'showLoader', 'methodeJavascriptFinAttente' => 'showLoader', 'methodeJavascriptRenvoiResultat' => 'selectionEffectuee'];
    }

    /*
     *  Cette fonction envoie une demande a SUB (xml des ids des candidatures)
     *  afin qu'il ouvre leurs dossiers correpondant cote SUB
     *  les candidatures valideés ouvertes cote SUB (recu dans le fichier xml de reponse)
     *  on les ouvrent cote MPE et on envoi les IDS de leurs offres correpondantes pour demande
     *  d'ouverture de leurs dossier cote SUB si dans le fichier de reponse se sont validées on
     *  change leurs statut cote MPE a ouvert
     */
    public function passerEnInscrutionSub($liseIdOffre)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $arrayDossiers = [];
        $arrayIdDossiersCandidature = []; // array des cles dossier candidature
        if (is_array($liseIdOffre)) {
            $arrayAllOffreEnveloppe = [];
            foreach ($liseIdOffre as $idOffre) {
                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme);
                $cleCandidature = 0;
                $arrayEnv = []; // array temporaire infos offre avec ces enveloppes
                foreach ($enveloppes as $enveloppe) {
                    $idEnv = $enveloppe->getIdEnveloppeElectro();
                    $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($idEnv, $organisme);
                    if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
                        if ($dossier->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $cleCandidature = $dossier->getCleExterneDossier();
                            $arrayIdDossiersCandidature[] = $cleCandidature;
                            $arrayEnv['IdEnvCandidature'] = $dossier->getIdEnveloppe();
                        } else {
                            $arrayEnv['IdsEnvOffres'][$dossier->getCleExterneDossier()] = $dossier->getIdEnveloppe();
                            $arrayAllOffreEnveloppe[$idOffre][] = $dossier->getIdEnveloppe();
                        }
                        //Mise a jour d'information au niveau de sub
                        try {
                            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);

                            if ($offre instanceof CommonOffres) {
                                $arrayDate = explode(' ', $offre->getDateRemisePlie());
                                $arrayInfos = [];
                                $arrayInfos[0]['binding']['type'] = 'dateType';
                                $arrayInfos[0]['binding']['nom'] = 'AOF_Candidature_Date_Reception';
                                $arrayInfos[0]['binding']['valeur'] = $arrayDate[0];
                                $arrayInfos[1]['binding']['type'] = 'dateType';
                                $arrayInfos[1]['binding']['nom'] = 'AOF_Candidature_Heure_Reception';
                                $arrayInfos[1]['binding']['valeur'] = $arrayDate[1];
                                $arrayInfos[2]['binding']['type'] = 'longType';
                                $arrayInfos[2]['binding']['nom'] = 'AOF_Candidature_numero_pli';
                                $arrayInfos[2]['binding']['valeur'] = $offre->getNumeroReponse();

                                if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
                                    (new Atexo_FormulaireSub_Echange())->updateDossierInterfaceSub($dossier->getCleExterneDossier(), $arrayInfos);
                                }
                            }
                        } catch (\Exception $e) {
                            $logger = Atexo_LoggerManager::getLogger('publicite');
                            $logger->error("Erreur lors de l'envoi de la requete a l'interface SUB : " . $e->getMessage());
                        }
                    }
                }

                if (0 != $cleCandidature) {
                    $arrayDossiers[$cleCandidature] = $arrayEnv;
                }
                unset($arrayEnv);
            }
        }
        // l'envoi de données vers SUB
        $echange = new Atexo_FormulaireSub_Echange();
        $arrayID = $echange->sendListeIdDossiersToSUB($arrayIdDossiersCandidature);
        //change le statut des enveloppe de type candidatures retourne valide dans SUB
        $arrayIdoffresAEnvoye = $this->getIdEnveloppeValideSub($arrayID, $arrayDossiers);
        $arrayIdoffres = [];
        //Envoi de la deuxieme liste des IDS Offre
        $arrayRetourIdOffres = $echange->sendListeIdDossiersToSUB($arrayIdoffresAEnvoye);
        $arrayIdoffres = $this->getIdEnveloppeValideSub($arrayRetourIdOffres, $arrayDossiers, true);
        $resut = $this->getOffreMiseEnInstruction($arrayAllOffreEnveloppe, $arrayIdoffres);

        return $resut;
    }

    /*
    * Permet de renvoyer les id enveloppe passe en instruction cote SUB
    * param $arrayAllOffreEnveloppe contient l'ensemble des enveloppes type offres
    * param $arrayIdoffresRetourSub contient les ids des enveloppe de type offres retourne pas le webservice
    */
    public function getOffreMiseEnInstruction(&$arrayAllOffreEnveloppe, &$arrayIdoffresRetourSub)
    {
        $arrayResult = [];
        foreach ($arrayAllOffreEnveloppe as $cle => $offre) {
            $notfind = true;
            foreach ($offre as $value) {
                $trouve = false;
                foreach ($arrayIdoffresRetourSub as $EnveloppValide) {
                    if ($value == $EnveloppValide) {
                        $trouve = true;
                    }
                }
                if (!$trouve) {
                    $notfind = false;
                }
            }
            if ($notfind) {
                $arrayResult[] = $cle;
            }
        }

        return $arrayResult;
    }

    /*
    * Permet de renvoyer les id enveloppe passe en instruction cote SUB
    * param $listeIdDossierValide: une liste des dossier retourne par le webservice
    * param $arrayAllEnvOffre contient l'ensemble des enveloppes
    * param $offre boolean pour diferencier entre les candidatures et les offres
    */
    public function getIdEnveloppeValideSub($listeIdDossierValide, $arrayAllEnvOffre, $offre = false)
    {
        $arrayIdEnvOffres = [];
        foreach ($listeIdDossierValide as $idDossier) {
            foreach ($arrayAllEnvOffre as $key => $arrayEnv) {
                if ($offre) {
                    $this->getIdEnvOffresValiderSub($arrayEnv, $idDossier, $arrayIdEnvOffres);
                } else {
                    if ($key == $idDossier) {
                        $this->getIdEnvOffreForCandidatureValide($arrayEnv, $arrayIdEnvOffres);
                    }
                }
            }
        }

        return $arrayIdEnvOffres;
    }

    /*
    * Permet retourne la liste des ids enveloppe offres pour une candidatures valide cote SUB
    */
    public function getIdEnvOffreForCandidatureValide($arrayEnv, &$arrayIdEnvOffres)
    {
        foreach ($arrayEnv as $cle => $value) {
            if ('IdsEnvOffres' == $cle) {
                $idOffre = null;
                foreach ($value as $cleArr => $idOffre) {
                    $arrayIdEnvOffres[] = $cleArr;
                }
            }
        }
    }

    /*
    * Permet retourne l'id enveloppe des dossier offres valide cote sub
    */
    public function getIdEnvOffresValiderSub($arrayEnv, $idDossier, &$arrayIdEnvOffres)
    {
        foreach ($arrayEnv as $cle => $value) {
            if ('IdsEnvOffres' == $cle) {
                foreach ($value as $cleArr => $idEnvOffre) {
                    if ($cleArr == $idDossier) {
                        $arrayIdEnvOffres[$cleArr] = $idEnvOffre;
                        break;
                    }
                }
            }
        }
    }

    /**
     * Permet de retourne les IDs des offres de la consultation.
     */
    public function getIdOffresCOnsultation()
    {
        $listeIdOffre = [];
        $enveloppeCandidature = $this->getViewState('enveloppesCandidature');
        if ($enveloppeCandidature) {
            foreach ($enveloppeCandidature as $candidature) {
                $listeIdOffre[] = $candidature->getOffreId();
            }
        }

        return $listeIdOffre;
    }

    public function refreshFormulaire($sender, $param)
    {
        //candidature
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');

        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setTrier($tri);
        $criteriaResponse->setSensTri($sensTri['candidatureElec'][$tri]);
        $criteriaResponse->setModeUnique($this->isModeConsultationUnique());
        $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));

        $candidaturesElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

        if (is_array($candidaturesElectroniques)) {
            $this->nombreCandidatureElectonique->Text = count($candidaturesElectroniques);
            $this->enveloppeCandidatureElectronique->DataSource = $candidaturesElectroniques;
            $this->enveloppeCandidatureElectronique->DataBind();
        } else {
            throw new Atexo_Exception('Expected function to return an array.');
        }
        $this->panelEnveloppeCandidature->render($param->getNewWriter());
    }

    public function downloadJnlp()
    {
        $resultat = $this->getViewState('resultatJnlp');
        if (!empty($resultat['content'])) {
            $this->setViewState('resultatJnlp', null);
            header('Content-Length: ' . strlen($resultat['content']));
            header('Content-disposition: attachment; filename="' . $resultat['name'] . '"');
            header('Content-type: application/x-java-jnlp-file');
            echo $resultat['content'];
            exit;
        }
    }

    public function actionReponseElectronique($sender, $param)
    {
        if ($param->CommandParameter) {
            $paramAction = $param->CommandParameter;
            $arrayIdsOffre = $paramAction['idsOffre'];
            $arrayIdsEnveloppe = $paramAction['idsEnveloppes'];
            $formatEnveloppe = $paramAction['typeDepot'];
            $callbackButton = $paramAction['refreshButton'];
            $typeData = $paramAction['typeData'];
            $repeaterName = $paramAction['repeaterName'];
            $checkElement = $paramAction['checkElement'];
            $listeActionsUnique = $paramAction['listeActionsUnique'];
            $listeActionsGroupees = $paramAction['listeActionsGroupees'];
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $messageErreur = '';
            $taille = 0;
            if ('reponse' == $typeData) {
                if ('unique' == $paramAction['typeAction']) {
                    $actionVal = '';
                    if (is_array($arrayIdsOffre) && 1 == count($arrayIdsOffre)) {
                        foreach ($this->$repeaterName->Items as $item) {
                            if ($item->idOffre->value == $arrayIdsOffre[0]) {
                                $actionVal = $item->$listeActionsUnique->getSelectedValue();
                            }
                        }
                    }
                } else {
                    $actionVal = $this->$listeActionsGroupees->getSelectedValue();
                    $arrayIdsOffre = [];
                    foreach ($this->$repeaterName->Items as $item) {
                        if ($item->$checkElement && $item->$checkElement->checked) {
                            $arrayIdsOffre[] = $item->idOffre->Value;
                            if ($actionVal == Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS') && !$this->isReponseOuvert($item->statut->Value)) {
                                $messageErreur = Prado::localize('MESSAGE_ERREUR_TELECHARGER_PLIS_OUVERTS');
                            } elseif ($actionVal != Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS') && $this->isReponseOuvert($item->statut->Value)) {
                                $messageErreur = Prado::localize('MESSAGE_ERREUR_ENVELOPPE_OUVERTE');
                            }
                        }
                    }
                }
            } elseif ('enveloppe' == $typeData) {
                if ('unique' == $paramAction['typeAction']) {
                    $actionVal = '';
                    if (is_array($arrayIdsEnveloppe) && 1 == count($arrayIdsEnveloppe)) {
                        foreach ($this->$repeaterName->Items as $item) {
                            if ($item->idEnveloppe->value == $arrayIdsEnveloppe[0]) {
                                $actionVal = $item->$listeActionsUnique->getSelectedValue();
                            }
                        }
                    }
                } else {
                    $actionVal = $this->$listeActionsGroupees->getSelectedValue();
                    $arrayIdsEnveloppe = [];
                    $arrayIdsOffre = [];
                    foreach ($this->$repeaterName->Items as $item) {
                        if ($item->$checkElement && $item->$checkElement->checked) {
                            $arrayIdsEnveloppe[] = $item->idEnveloppe->Value;
                            $arrayIdsOffre[] = $item->idOffre->Value;
                            if ($actionVal == Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS') && !$this->isReponseOuvert($item->statut->Value)) {
                                $messageErreur = Prado::localize('MESSAGE_ERREUR_TELECHARGER_PLIS_OUVERTS');
                            } elseif ($actionVal != Atexo_Config::getParameter('TELECHARGER_PLIS_OUVERTS') && $this->isReponseOuvert($item->statut->Value)) {
                                $messageErreur = Prado::localize('MESSAGE_ERREUR_ENVELOPPE_OUVERTE');
                            }
                        }
                    }
                }
            }
            if (!empty($messageErreur)) {
                $this->panelMessageErreurDechiffrement->setMessage($messageErreur);
                $this->panelMessageErreurDechiffrement->showMessage();

                return false;
            } else {
                $this->panelMessageErreurDechiffrement->hideMessage();
            }
            $actionToDo = $this->getActionTodoFromValAction($actionVal);
            $scriptText = '<script>';
            if ('openNotSignedReponse' == $actionToDo) {// ouvrir en ligne
                if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
                    if (Atexo_Module::isEnabled('InterfaceModuleSub') && $this->_consultation->getModeOuvertureReponse() && 'reponse' == $typeData) {
                        $arrayIdsOffre = $this->passerEnInscrutionSub($arrayIdsOffre);
                    }
                    if ($this->getChiffrementEnveloppe()) {
                        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                            $organisme = Atexo_CurrentUser::getOrganismAcronym();
                            $referenceCons = Atexo_Util::atexoHtmlEntities($_GET['id']);
                            $offres = (new Atexo_Entreprise_Reponses())->getOffres($referenceCons, $organisme, $arrayIdsOffre);
                            (new Atexo_Entreprise_Reponses())->addEnveloppesToDechiffre($offres, $arrayIdsEnveloppe);
                            if (is_array($offres)) {
                                $context = $this->getDechiffrementCrypto(
                                    $offres,
                                    Atexo_CurrentUser::getId(),
                                    $this->_consultation
                                );
                            }
                            $this->setInfoAjax($repeaterName, $arrayIdsOffre, $arrayIdsEnveloppe);

                            if ($this->isMAMPEnabled()) {
                                $this->javascript->Text = MessageStatusCheckerUtil::getScript($context);
                            } else {
                                $resultat = $this->getDechiffrementCrypto($offres, Atexo_CurrentUser::getId(), $this->_consultation);
                                if ($resultat) {
                                    $this->setInfoAjax($repeaterName, $arrayIdsOffre, $arrayIdsEnveloppe);
                                    $name = str_replace("[COMPLET]", $this->_consultation->getReferenceUtilisateur() . "-" . date("Y-m-d H:i"), utf8_decode(Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_DECHIFFREMNT')));
                                    $jnpl = array( "content" => $resultat, "name" => $name);
                                    $this->setViewState('resultatJnlp', $jnpl);
                                    $scriptText .= "document.getElementById('ctl0_CONTENU_PAGE_downloadJnlp').click();";
                                } else {
                                    $this->panelMessageErreurDechiffrement->setMessage(Prado::localize('ERREUR_RECUPERATION_JNPL'));
                                    $this->panelMessageErreurDechiffrement->showMessage();
                                }
                            }
                        }
                    } else {
                        if ('reponse' == $typeData) {
                            $this->openOffreEnLigne($arrayIdsOffre, $formatEnveloppe);
                        } elseif ('enveloppe' == $typeData) {
                            $this->openEnveloppeEnLigne($arrayIdsEnveloppe, $formatEnveloppe);
                        }
                    }
                }
                $this->javascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_" . $callbackButton . "').click();</script>";
            } elseif ('openMixte' == $actionToDo) { // Ouverture Mixte
                if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
                    if ($this->getChiffrementEnveloppe()) {
                        //appelle à l'applet
                        $organisme = Atexo_CurrentUser::getCurrentOrganism();
                        $referenceCons = Atexo_Util::atexoHtmlEntities($_GET['id']);

                        if ('unique' == $paramAction['typeAction']) {
                            $destDir = $this->getPathTelechargementPlis($organisme, $arrayIdsOffre, $arrayIdsEnveloppe);
                        } else {
                            $destDir = addslashes($this->_consultation->getPathTelechargementPlis());
                        }
                        $urlReceprionBlocDechiffre = $this->getUrlReceptionBlocDechiffre();
                        if ('' == $destDir) {
                            $scriptText .= 'bootbox.alert("' . addslashes(Atexo_Config::toPfEncoding(Prado::localize('MSG_TELECHARGER_PLIS'))) . '");';
                        } else {
                            if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
                                $offres = (new Atexo_Entreprise_Reponses())->getOffres($referenceCons, $organisme, $arrayIdsOffre);
                                (new Atexo_Entreprise_Reponses())->addEnveloppesToDechiffre($offres, $arrayIdsEnveloppe);
                                if (is_array($offres)) {
                                    $resultat = $this->getDechiffrementCrypto($offres, Atexo_CurrentUser::getId(), $this->_consultation, false);
                                    if ($resultat) {
                                        $this->setInfoAjax($repeaterName, $arrayIdsOffre, $arrayIdsEnveloppe);
                                        $name = str_replace('[COMPLET]', $this->_consultation->getReferenceUtilisateur() . '-' . date('Y-m-d H:i'), utf8_decode(Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_DECHIFFREMNT')));
                                        $jnpl = ['content' => $resultat, 'name' => $name];
                                        $this->setViewState('resultatJnlp', $jnpl);
                                        if ($this->isMAMPEnabled($this->_consultation->getId())) {
                                            $scriptText .= MessageStatusCheckerUtil::getScript($resultat);
                                        } else {
                                            $scriptText .= "document.getElementById('ctl0_CONTENU_PAGE_downloadJnlp').click();";
                                        }
                                    } else {
                                        $this->panelMessageErreurDechiffrement->setMessage(Prado::localize('ERREUR_RECUPERATION_JNPL'));
                                        $this->panelMessageErreurDechiffrement->showMessage();
                                    }
                                }
                            } else {
                                $xmlReponsesAnnonces = (new Atexo_Entreprise_Reponses())->getReponsesAnnoncesXml([$referenceCons], $organisme, $arrayIdsOffre);
                                $reponseAnnonceXmlEnBase64 = base64_encode($xmlReponsesAnnonces);
                                $msgConfirm = addslashes(Atexo_Util::toUtf8(Prado::localize('OUVERTURE_CONFIRMATION'))) . '  '
                                    . addslashes(Atexo_Util::toUtf8(Prado::localize('OUVERTURE_ENVELOPPE_EN_LIGNE_SUR_PF_ET_ENV_STOCKEE_HORS_LIGNE')));
                                $scriptText .= " if(confirm('" . Atexo_Config::toPfEncoding($msgConfirm) . "')){ document.MpeDechiffrementApplet.initialiserMixtePhaseDechiffrerEtEnvoyer('"
                                    . $reponseAnnonceXmlEnBase64 . "', '" . $organisme . "', '" . $urlReceprionBlocDechiffre . "', '" . Atexo_Util::utf8ToIso($destDir) . "'); ";
                                if (is_array($arrayIdsEnveloppe)) {
                                    foreach ($arrayIdsEnveloppe as $idEnv) {
                                        $scriptText .= " ajouterIdEnveloppe('" . $idEnv . "');	";
                                    }
                                }
                                $scriptText .= ' document.MpeDechiffrementApplet.executer(); }	';
                            }
                        }
                    } else {
                        $scriptText .= 'bootbox.alert("' . addslashes(Atexo_Config::toPfEncoding(Prado::localize('MSG_MODE_NON_DISPO'))) . '");';
                    }
                }
            } elseif ('refuserReponse' == $actionToDo) {
                if ('reponse' == $typeData) {
                    $this->refuseOffre($arrayIdsOffre, $formatEnveloppe);
                } elseif ('enveloppe' == $typeData) {
                    $this->refuseEnveloppe($arrayIdsEnveloppe, $formatEnveloppe);
                }
                $this->javascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_" . $callbackButton . "').click();</script>";
            } elseif ('restaurerReponse' == $actionToDo) {
                if ('reponse' == $typeData) {
                    $this->restaurerOffre($arrayIdsOffre, $formatEnveloppe);
                } elseif ('enveloppe' == $typeData) {
                    $this->restaurerEnveloppe($arrayIdsEnveloppe, $formatEnveloppe);
                }
                $this->javascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_" . $callbackButton . "').click();</script>";
            } elseif ('renseignerStatut' == $actionToDo) {
                if ('reponse' == $typeData) {
                    if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
                        $idsOffre = implode('#', $arrayIdsOffre);
                        $idOffreDecode = base64_encode($idsOffre);
                        $scriptText .= " popUp('index.php?page=Agent.popUpRenseignerStatutReponse&idsOffre=" . $idOffreDecode
                            . '&callbackButton=' . $callbackButton . '&typePli=' . $formatEnveloppe . "','yes');";
                    }
                } elseif ('enveloppe' == $typeData) {
                    if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
                        $idsEnveloppe = implode('#', $arrayIdsEnveloppe);
                        $idsEnveloppeEncode = base64_encode($idsEnveloppe);
                        $scriptText .= " popUp('index.php?page=Agent.popUpRenseignerStatutReponse&idsEnveloppes=" . $idsEnveloppeEncode
                            . '&callbackButton=' . $callbackButton . '&typePli=' . $formatEnveloppe . "','yes');";
                    }
                }
            } elseif ('gererAdmissibilite' == $actionToDo) {
                if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
                    $idEnv = $arrayIdsEnveloppe[0];
                    if ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                        $oneEnveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                        if ($oneEnveloppe instanceof CommonEnveloppe) {
                            $typeEnv = $oneEnveloppe->getTypeEnv();
                            $sousPli = $oneEnveloppe->getSousPli();
                            $idOffre = $oneEnveloppe->getOffreId();
                            $ref = $this->_consultation->getId();
                        }
                    } elseif ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                        $oneEnveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                        if ($oneEnveloppe instanceof CommonEnveloppePapier) {
                            $idOffre = $oneEnveloppe->getOffrePapierId();
                            $typeEnv = $oneEnveloppe->getTypeEnv();
                            $sousPli = $oneEnveloppe->getSousPli();
                            $ref = $this->_consultation->getId();
                        }
                    }
                    $scriptText .= " popUp('?page=Agent.popUpGestionAdmissibilite&offre=" . $idOffre . '&id=' . $ref . '&formeEnveloppe=' . $formatEnveloppe
                        . '&callbackButton=' . $callbackButton . '&typeEnv=' . $typeEnv . '&sousPli=' . $sousPli . "','yes'); ";
                }
            } elseif ('demandeComplement' == $actionToDo) {
                if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
                    $idOffre = $arrayIdsOffre[0];
                    $offre = CommonOffresPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffres) {
                        $idInscrit = $offre->getInscritId();
                        $offreId = $offre->getId();
                        $ref = $this->_consultation->getId();
                        $messecVersion = $this->_consultation->getVersionMessagerie();
                        if (2 === $messecVersion) {
                            $encrypte = Atexo_Util::getSfService(Encryption::class);
                            $idCrypte = $encrypte->cryptId($this->_consultation->getId());
                            $offreIdCrypte = $encrypte->cryptId($offreId);

                            $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_DEMANDE_COMPLEMENT_CONSULTATION'));
                            $urlMessec = $pfUrl
                                . '/'
                                . $idCrypte . '/' . $offreIdCrypte;
                            $scriptText .= " window.open('" . $urlMessec . "') ;";
                        } else {
                            $scriptText .= " window.location.assign('?page=Agent.EnvoiCourrierElectroniqueDemandeComplement&id=" . $ref . '&IdInscrit=' . $idInscrit . "') ;";
                        }
                    }
                }
            } elseif ('ImporterEnveloppe' == $actionToDo) {
                if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
                    $idEnv = $arrayIdsEnveloppe[0];
                    $oneEnveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($oneEnveloppe instanceof CommonEnveloppe) {
                        $typeEnv = $oneEnveloppe->getTypeEnv();
                        $sousPli = $oneEnveloppe->getSousPli();
                        $idOffre = $oneEnveloppe->getOffreId();
                        $ref = $this->_consultation->getId();
                        $scriptText .= " popUp('?page=Agent.popUpImporterEnveloppe&idEnveloppe=" . $idEnv . '&idOffre=' . $idOffre . '&callbackButton=' . $callbackButton
                            . '&id=' . $ref . '&typePli=' . $formatEnveloppe . '&typeEnv=' . $typeEnv . "','yes');";
                    }
                }
            } elseif ('AutoriserOuverture' == $actionToDo) {
                if (is_array($arrayIdsEnveloppe) && 1 == count($arrayIdsEnveloppe)) {
                    (new Atexo_Consultation_Responses())->updateEnveloppeAutoriserOuverture($arrayIdsEnveloppe[0], Atexo_CurrentUser::getCurrentOrganism());
                    $this->javascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_" . $callbackButton . "').click();</script>";
                }
            } elseif ('telechargerPlisOuvert' == $actionToDo) {
                if (('reponse' == $typeData && empty($arrayIdsOffre)) || ('enveloppe' == $typeData && empty($arrayIdsEnveloppe))) {
                    $messageErreur = Prado::localize('MESSAGE_ERREUR_TELECHARGER_PLIS_OUVERTS_AUCUN_PLI_SELECTIONNE');
                }
                if ('reponse' == $typeData) {
                    $taille = (new Atexo_Consultation_Responses())->getFilesSizeOffres($arrayIdsOffre, Atexo_CurrentUser::getCurrentOrganism());
                } elseif ('enveloppe' == $typeData) {
                    $taille = (new Atexo_Consultation_Responses())->getFilesSizeEnveloppes($arrayIdsEnveloppe, Atexo_CurrentUser::getCurrentOrganism());
                }
                if ($taille > Atexo_Config::getParameter('TAILLE_MAX_ZIP_PLIS')) {
                    $tailleLimit = Atexo_Util::GetSizeName(Atexo_Config::getParameter('TAILLE_MAX_ZIP_PLIS'));

                    $messageErreur =
                        Prado::localize(
                            'DEFINE_NOTIFICATION_TELECHARGEMENT_PLIS_SELECTIONNES_DEPASSEMENT_TAILLE',
                            ['__TAILLE__' => $tailleLimit]
                        );
                }
                if (!empty($messageErreur)) {
                    $this->panelMessageErreurDechiffrement->setMessage($messageErreur);
                    $this->panelMessageErreurDechiffrement->showMessage();

                    return false;
                }
                $array = ('reponse' == $typeData) ? $arrayIdsOffre : $arrayIdsEnveloppe;
                $this->setViewState('idsForDownloadPli', ['type' => $typeData, 'data' => $array]);
                $scriptText .= "J('.modal-confirmation-download').show();";
            } elseif ('NotifierRejet' == $actionToDo) {
                if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
                    $idOffre = $arrayIdsOffre[0];
                    $offre = CommonOffresPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffres) {
                        $idInscrit = $offre->getInscritId();
                        $offreId = $offre->getId();
                        $ref = $this->_consultation->getId();
                        $messecVersion = $this->_consultation->getVersionMessagerie();
                        if (2 === $messecVersion) {
                            $encrypte = Atexo_Util::getSfService(Encryption::class);
                            $idCrypte = $encrypte->cryptId($this->_consultation->getId());
                            $offreIdCrypte = $encrypte->cryptId($offreId);
                            $pfUrl = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_NOTIFICATION_REJET_CONSULTATION'));
                            $urlMessec = $pfUrl
                                . '/'
                                . $idCrypte . '/' . $offreIdCrypte;
                            $scriptText .= " window.open('" . $urlMessec . "') ;";
                        }
                    }
                }
            }

            $scriptText .= '</script>';
            $this->javascript->Text = $scriptText;
        }
    }

    /*
     * call back de l'action reponse elctronique permet de modifier le statut de l'offre
     */
    public function onCallBackActionReponseElectronique($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriElec', []);
        $tri = $this->getViewState('triElec', '');

        $dataSource = (new Atexo_Consultation_Responses())->retrieveOffreNonAnnuleeByConsRef(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            Atexo_CurrentUser::getCurrentOrganism(),
            null,
            null,
            $tri,
            $sensTri['reponsesElec'][$tri]
        );

        if (is_array($dataSource)) {
            $this->nombreReponsesElectonique->Text = count($dataSource);
            $this->Nombre_enveloppeReponsesElectronique->value = count($dataSource);
            $this->populateData('enveloppeReponsesElectronique', 'ReponseElec', $tri, $sensTri);
        }
        $this->panelReponseElectronique->render($param->getNewWriter());
    }

    /*
     * call back de l'action reponse elctronique permet de modifier le statut de l'offre
     */
    public function onCallBackActionReponsePapier($sender, $param)
    {
        $sensTri = $this->getViewState('sensTriPapier', []);
        $tri = $this->getViewState('triPapier', '');

        $dataSourcePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierNonAnnuleeByConsRef(
            Atexo_Util::atexoHtmlEntities($_GET['id']),
            Atexo_CurrentUser::getCurrentOrganism(),
            null,
            null,
            $tri,
            $sensTri['reponsesPapier'][$tri]
        );
        if (is_array($dataSourcePapier)) {
            $this->nombreReponsePapier->Text = count($dataSourcePapier);
            $this->Nombre_enveloppeReponsesPapiers->value = count($dataSourcePapier);
            $this->populateData('enveloppeReponsesPapiers', 'ReponsePapier', $tri, $sensTri);
        }
        $this->panelReponsePapier->render($param->getNewWriter());
    }

    /**
     * Permet de retourner si le télechargement de la réponse est autorisé ou non.
     *
     * @param $cryptage : si on a un chiffrement ou nn
     */
    public function allowedToDownloadReponse(CommonOffres $offre)
    {
        if ('1' == $offre->getCryptageReponse()) {
            return false;
        } else {
            //Ajouter les habillitation d'ouverture
            if (Atexo_CurrentUser::hasHabilitation('AccesReponses')) {
                //Tester si toutes les enveloppes de la reponse sont dechiffrées alors retourner "true" si "false"
                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($offre->getId(), $offre->getOrganisme());
                if (is_array($enveloppes) && count($enveloppes)) {
                    foreach ($enveloppes as $uneEnv) {
                        if ($uneEnv instanceof CommonEnveloppe && '0' != $uneEnv->getCryptage()) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /*
    * retourne le nom du dossier reponse
    */
    public function writeNomFichierReponse($index)
    {
        $fileName = 'EL' . ($index) . '_';

        return $fileName .= Prado::localize('DEFINE_REPONSE') . '.zip';
    }

    /*
    * Permet d'ouvrir une offre en ligne inclus les enveloppes de cette offre
    */
    public function openOffreEnLigne($arrayIdsOffre, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsOffre) && count($arrayIdsOffre)) {
            foreach ($arrayIdsOffre as $offreId) {
                $offre = CommonOffresPeer::retrieveByPK($offreId, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if ($offre instanceof CommonOffres) {
                    $enveloppes = $offre->getCommonEnveloppes();
                    if (is_array($enveloppes)) {
                        $resultatSucces = true;
                        foreach ($enveloppes as $oneEnveloppe) {
                            $typeEnv = $oneEnveloppe->getTypeEnv();
                            $sousPli = $oneEnveloppe->getSousPli();
                            $resultatOuverture = $this->openNotChiffredEnveloppe($oneEnveloppe->getIdEnveloppeElectro(), $offreId, $typeEnv, $sousPli, $formatEnveloppe);
                            if (!$resultatOuverture) {
                                $resultatSucces = false;
                            }
                        }
                        if (!$resultatSucces) {
                            $this->javascript->Text = '<script>bootbox.alert("' . Prado::localize('ERREUR_OUVERTURE') . '")</script>';
                        } else {
                            $offre->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
                            $offre->setCryptageReponse('0');
                            if (Atexo_Module::isEnabled('fourEyes')) {
                                $offre->setDateHeureOuvertureAgent2(date('Y-m-d H:i:s'));
                                $offre->setAgentidOuverture2(Atexo_CurrentUser::getIdAgentConnected());
                            } else {
                                $offre->setDateHeureOuverture(date('Y-m-d H:i:s'));
                                $offre->setAgentidOuverture(Atexo_CurrentUser::getIdAgentConnected());
                            }
                            $offre->save($connexionCom);
                        }
                    }
                }
            }
        }
    }

    /*
    * permet d'ouvrir des enveloppe en ligne
    */
    public function openEnveloppeEnLigne($arrayIdsEnveloppe, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
            foreach ($arrayIdsEnveloppe as $idEnv) {
                $oneEnveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                if ($oneEnveloppe instanceof CommonEnveloppe) {
                    $typeEnv = $oneEnveloppe->getTypeEnv();
                    $sousPli = $oneEnveloppe->getSousPli();
                    $offreId = $oneEnveloppe->getOffreId();
                    $result = $this->openNotChiffredEnveloppe($oneEnveloppe->getIdEnveloppeElectro(), $offreId, $typeEnv, $sousPli, $formatEnveloppe);
                    $nbreEnveloppeFerme = Atexo_Consultation_Responses::nombreEnveloppeFerme($offreId, Atexo_CurrentUser::getOrganismAcronym(), $connexionCom);
                    if ($result && !$nbreEnveloppeFerme) {
                        $offre = CommonOffresPeer::retrieveByPK($offreId, Atexo_CurrentUser::getOrganismAcronym(), $connexionCom);
                        $offre->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
                        $offre->setCryptageReponse('0');
                        if (Atexo_Module::isEnabled('fourEyes')) {
                            $offre->setDateHeureOuvertureAgent2(date('Y-m-d H:i:s'));
                            $offre->setAgentidOuverture2(Atexo_CurrentUser::getIdAgentConnected());
                        } else {
                            $offre->setDateHeureOuverture(date('Y-m-d H:i:s'));
                            $offre->setAgentidOuverture(Atexo_CurrentUser::getIdAgentConnected());
                        }
                        $offre->save($connexionCom);
                    }
                }
            }
        }
    }

    /*
    * Permet de refuser une offre inclus les enveloppes de cette offre
    */
    public function refuseOffre($arrayIdsOffre, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsOffre)) {
            if ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                foreach ($arrayIdsOffre as $offreId) {
                    $offre = CommonOffresPeer::retrieveByPK($offreId, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffres) {
                        $enveloppes = $offre->getCommonEnveloppes();
                        if (is_array($enveloppes)) {
                            foreach ($enveloppes as $oneEnveloppe) {
                                (new Atexo_Consultation_Responses())->updateStatutEnveloppe(
                                    $oneEnveloppe->getIdEnveloppeElectro(),
                                    Atexo_Config::getParameter('STATUT_ENV_REFUSEE'),
                                    Atexo_CurrentUser::getCurrentOrganism()
                                );
                            }
                        }
                        $offre->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_REFUSEE'));
                        $offre->save($connexionCom);
                    }
                }
            } elseif ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                foreach ($arrayIdsOffre as $offreId) {
                    $offre = CommonOffrePapierPeer::retrieveByPK($offreId, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffrePapier) {
                        $enveloppes = $offre->getCommonEnveloppesPapiers();
                        if (is_array($enveloppes)) {
                            foreach ($enveloppes as $oneEnveloppe) {
                                (new Atexo_Consultation_Responses())->updateStatutEnveloppePapier($oneEnveloppe->getIdEnveloppePapier(), Atexo_Config::getParameter('STATUT_ENV_REFUSEE'));
                            }
                        }
                        $offre->setStatutOffrePapier(Atexo_Config::getParameter('STATUT_ENV_REFUSEE'));
                        $offre->save($connexionCom);
                    }
                }
            }
        }
    }

    /*
    * permet de refuser des enveloppe
    */
    public function refuseEnveloppe($arrayIdsEnveloppe, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
            if ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                foreach ($arrayIdsEnveloppe as $idEnv) {
                    $oneEnveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($oneEnveloppe instanceof CommonEnveloppe) {
                        (new Atexo_Consultation_Responses())->updateStatutEnveloppe(
                            $oneEnveloppe->getIdEnveloppeElectro(),
                            Atexo_Config::getParameter('STATUT_ENV_REFUSEE'),
                            Atexo_CurrentUser::getCurrentOrganism()
                        );
                    }
                }
            } elseif ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                foreach ($arrayIdsEnveloppe as $idEnv) {
                    $oneEnveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($oneEnveloppe instanceof CommonEnveloppePapier) {
                        (new Atexo_Consultation_Responses())->updateStatutEnveloppePapier(
                            $oneEnveloppe->getIdEnveloppePapier(),
                            Atexo_Config::getParameter('STATUT_ENV_REFUSEE'),
                            Atexo_CurrentUser::getCurrentOrganism()
                        );
                    }
                }
            }
        }
    }

    /*
    * permet de restaurer l'offres
    */
    public function restaurerOffre($arrayIdsOffre, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsOffre)) {
            if ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                foreach ($arrayIdsOffre as $offreId) {
                    $offre = CommonOffresPeer::retrieveByPK($offreId, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffres) {
                        $enveloppes = $offre->getCommonEnveloppes();
                        if (is_array($enveloppes)) {
                            foreach ($enveloppes as $oneEnveloppe) {
                                (new Atexo_Consultation_Responses())->updateStatutEnveloppe(
                                    $oneEnveloppe->getIdEnveloppeElectro(),
                                    Atexo_Config::getParameter('STATUT_ENV_FERME'),
                                    Atexo_CurrentUser::getCurrentOrganism()
                                );
                            }
                        }
                        $offre->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                        $offre->save($connexionCom);
                    }
                }
            } elseif ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                foreach ($arrayIdsOffre as $offreId) {
                    $offre = CommonOffrePapierPeer::retrieveByPK($offreId, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($offre instanceof CommonOffrePapier) {
                        $enveloppes = $offre->getCommonEnveloppesPapiers();
                        if (is_array($enveloppes)) {
                            foreach ($enveloppes as $oneEnveloppe) {
                                (new Atexo_Consultation_Responses())->updateStatutEnveloppePapier($oneEnveloppe->getIdEnveloppePapier(), Atexo_Config::getParameter('STATUT_ENV_FERME'));
                            }
                        }
                        $offre->setStatutOffrePapier(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                        $offre->save($connexionCom);
                    }
                }
            }
        }
    }

    /*
    * permet de restaurer des enveloppe
    */
    public function restaurerEnveloppe($arrayIdsEnveloppe, $formatEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe)) {
            if ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                foreach ($arrayIdsEnveloppe as $idEnv) {
                    $oneEnveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($oneEnveloppe instanceof CommonEnveloppe) {
                        (new Atexo_Consultation_Responses())->updateStatutEnveloppe(
                            $oneEnveloppe->getIdEnveloppeElectro(),
                            Atexo_Config::getParameter('STATUT_ENV_FERME'),
                            Atexo_CurrentUser::getCurrentOrganism()
                        );
                    }
                }
            } elseif ($formatEnveloppe == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                foreach ($arrayIdsEnveloppe as $idEnv) {
                    $oneEnveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnv, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                    if ($oneEnveloppe instanceof CommonEnveloppePapier) {
                        (new Atexo_Consultation_Responses())->updateStatutEnveloppePapier(
                            $oneEnveloppe->getIdEnveloppePapier(),
                            Atexo_Config::getParameter('STATUT_ENV_FERME'),
                            Atexo_CurrentUser::getCurrentOrganism()
                        );
                    }
                }
            }
        }
    }

    /*
    * retourne si la consultation est alloti
    */
    public function isConsultationAlloti()
    {
        return $this->_consultation->getAlloti();
    }


    public function getIdConsultation()
    {
        return $this->_consultation->getId();
    }

    /*
    * permet de savoir est ce qu'on l'admissiblité entre enveloppe est obligatoire
    */
    public function admissibilteObligatoire()
    {
        if (Atexo_Module::isEnabled('InterfaceModuleSub') || $this->_consultation->getModeOuvertureReponse()) {
            return false;
        } else {
            return true;
        }
    }

    /*
    * permet de formater la date en paramettre
    */
    public function formaterDateRemisePlie($date)
    {
        return str_replace(' ', '<br/>', $date);
    }

    /*
    * retourne si la consultation est MultiEnveloppe/Unique
    */
    public function isEnveloppeUnique()
    {
        return $this->_consultation->getModeOuvertureReponse();
    }

    /*
    * populateData methode generale pour tout les repeaters
    */
    public function populateData($repeater, $type, $tri = null, $sensTri = null, $changeNombreElement = false)
    {
        if (!$this->isEnveloppeUnique() && !isset($_GET['telechargerOffres'])) {
            $this->setViewState('thColspan', '9');
            $this->setViewState('thColspanOffre', '7');
        }
        $datesource = [];
        $offset = $this->$repeater->CurrentPageIndex * $this->$repeater->PageSize;
        $limit = $this->$repeater->PageSize;
        if ($offset + $limit > $this->$repeater->getVirtualItemCount()) {
            $limit = $this->$repeater->getVirtualItemCount() - $offset;
        }
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setLimit($limit);
        $criteriaResponse->setOffset($offset);
        $criteriaResponse->setModeUnique($this->isModeConsultationUnique());
        $criteriaResponse->setArrayIdsGuests($this->getViewState('guestsWithEnabling'));
        if ('Candidature' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetElec', null);
                $criteriaResponse->setTrier($tri);
                $criteriaResponse->setSensTri($sensTri['candidatureElec'][$tri]);
                $criteriaResponse->setObjet($objet);
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
            }
        }
        if ('CandiPapier' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetPapier', null);
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    '',
                    $this->getViewState('typesEnveloppes'),
                    $tri,
                    $sensTri['candidaturePapier'][$tri],
                    $this->getViewState('organisme'),
                    false,
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset,
                    $objet,
                    true,
                    false,
                    $this->getViewState('guestsWithEnabling')
                );
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    '',
                    $this->getViewState('typesEnveloppes'),
                    '',
                    '',
                    $this->getViewState('organisme'),
                    false,
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset,
                    false,
                    true,
                    false,
                    $this->getViewState('guestsWithEnabling')
                );
            }
        }

        if ('OffreTechniqueElec' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetElec');
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('numLotOffreTechnique'),
                    $this->getViewState('typesEnveloppes'),
                    $tri,
                    $sensTri['offreTechniqueElec'][$tri],
                    Atexo_CurrentUser::getCurrentOrganism(),
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset,
                    $objet,
                    $this->getViewState('guestsWithEnabling')
                );
            } else {
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('numLotOffreTechnique'),
                    $this->getViewState('typesEnveloppes'),
                    '',
                    '',
                    Atexo_CurrentUser::getCurrentOrganism(),
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset,
                    null,
                    $this->getViewState('guestsWithEnabling')
                );
            }
            if ($changeNombreElement) {
                $this->setViewState('nombreElementOffreTechElec', count($datesource));
                $this->nombreOffreTechniqueElectronique->Text = count($datesource);
                $this->Nombre_enveloppeOffreTechniqueElectronique->value = count($datesource);
            }
        }

        if ('OffreTechniquePapier' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetPapier');
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotOffreTechnique'),
                    $tri,
                    $sensTri['offreTechniquePapier'][$tri],
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset,
                    $objet,
                    $this->getViewState('guestsWithEnabling')
                );
            } else {
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotOffreTechnique'),
                    '',
                    '',
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset,
                    null,
                    $this->getViewState('guestsWithEnabling')
                );
            }
            if ($changeNombreElement) {
                $this->setViewState('nombreElementOffreTechPapier', count($datesource));
                $this->nombreOffreTechniquePapier->Text = count($datesource);
                $this->Nombre_enveloppeOffreTechniquePapier->value = count($datesource);
            }
        }
        $admissible = $this->admissibilteObligatoire();
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteriaResponse->setSousPli($this->getViewState('numLotOfre'));
        $criteriaResponse->setTypesEnveloppes($this->getViewState('typesEnveloppes'));
        $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaResponse->setEnveloppeStatutOuverte($this->isModeConsultationUnique());
        $criteriaResponse->setAdmissible($admissible);
        $criteriaResponse->setLimit($limit);
        $criteriaResponse->setOffset($offset);
        if ('OffreElec' == $type) {
            if ($tri && $sensTri) {
                $criteriaResponse->setTrier($tri);
                $criteriaResponse->setSensTri($sensTri['offreElec'][$tri]);
                $criteriaResponse->setObjet($this->getViewState('ObjetElec'));
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
            }
            if ($changeNombreElement) {
                $this->setViewState('nombreElementOffreElec', count($datesource));
                $this->nombreOffreElectronique->Text = count($datesource);
                $this->Nombre_enveloppeOffreElectronique->value = count($datesource);
            }
        }

        if ('OffreElecPapier' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetPapier');
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotOfrePapier'),
                    $tri,
                    $sensTri['offrePapier'][$tri],
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset,
                    $objet
                );
            } else {
                $admissible = $this->admissibilteObligatoire();
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotOfrePapier'),
                    '',
                    '',
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    true,
                    false,
                    $this->isModeConsultationUnique(),
                    $admissible,
                    $limit,
                    $offset
                );
            }
            if ($changeNombreElement) {
                $this->setViewState('nombreElementOffreElecPapier', count($datesource));
                $this->nombreOffrePapier->Text = count($datesource);
                $this->Nombre_enveloppeOffrePapier->value = count($datesource);
            }
        }

        if ('AnonymatElec' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetElec');
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('numLotAnonymat'),
                    $this->getViewState('typesEnveloppes'),
                    $tri,
                    $sensTri['anonymatElec'][$tri],
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset,
                    $objet
                );
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('numLotAnonymat'),
                    $this->getViewState('typesEnveloppes'),
                    '',
                    '',
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset
                );
            }
            if ($changeNombreElement) {
                $this->nombreAnonymatElectronique->Text = count($datesource);
                $this->Nombre_enveloppeAnonymatEletronique->value = count($datesource);
                $this->setViewState('nombreElementAnonymatElec', count($datesource));
            }
        }

        if ('AnonymatPapier' == $type) {
            if ($tri && $sensTri) {
                $objet = $this->getViewState('ObjetPapier');
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotAnonymat'),
                    $tri,
                    $sensTri['anonymatPapier'][$tri],
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset,
                    $objet
                );
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $this->getViewState('typesEnveloppes'),
                    $this->getViewState('numLotAnonymat'),
                    '',
                    '',
                    Atexo_CurrentUser::getCurrentOrganism(),
                    false,
                    $this->isModeConsultationUnique(),
                    $limit,
                    $offset
                );
            }
            if ($changeNombreElement) {
                $this->nombreAnonymatPapier->Text = count($datesource);
                $this->Nombre_enveloppeAnonymatPapier->value = count($datesource);
                $this->setViewState('nombreElementAnonymatPapier', count($datesource));
            }
        }
        if ('ReponseElec' == $type) {
            if ($tri && $sensTri) {
                $datesource = (new Atexo_Consultation_Responses())->retrieveOffreNonAnnuleeByConsRef(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $limit,
                    $offset,
                    $tri,
                    $sensTri['reponsesElec'][$tri]
                );
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveOffreNonAnnuleeByConsRef(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $limit,
                    $offset
                );
            }
        }

        if ('ReponsePapier' == $type) {
            if ($tri && $sensTri) {
                $datesource = (new Atexo_Consultation_Responses())->retrieveOffrePapierNonAnnuleeByConsRef(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $limit,
                    $offset,
                    $tri,
                    $sensTri['reponsesPapier'][$tri]
                );
            } else {
                $datesource = (new Atexo_Consultation_Responses())->retrieveOffrePapierNonAnnuleeByConsRef(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $limit,
                    $offset
                );
            }
        }

        //TODO les autres types
        if (is_array($datesource) && (count($datesource) > 0)) {
            $this->$repeater->DataSource = $datesource;
            $this->$repeater->DataBind();
            $this->showComponent($type);
        } else {
            $datesource = [];
            $this->$repeater->DataSource = $datesource;
            $this->$repeater->DataBind();
            $this->hideComponent($type);
        }
    }

    public function pageChanged($sender, $param)
    {
        $arrayId = explode('_', $sender->ID);
        if ('Candidature' == $arrayId[1]) {
            $repeater = 'enveloppeCandidatureElectronique';
            $numPageTop = 'numPageTop_Candidature';
            $numPageBottom = 'numPageBottom_Candidature';
            $select = 'candElec_msg_nbResultsTop';
            $type = 'Candidature';
            $typeTri = 'Elec';
            $repeaterLiee = 'enveloppeCandidaturePapier';
            $numPageTopLiee = 'numPageTop_CandidaturePapier';
            $typeRepeater = 'CandiPapier';
            $typeTriLier = 'Papier';
        }
        if ('OffreElec' == $arrayId[1]) {
            $repeater = 'enveloppeOffreElectronique';
            $numPageTop = 'numPageTop_OffreElec';
            $numPageBottom = 'numPageBottom_OffreElec';
            $type = 'OffreElec';
            $select = 'offreElec_msg_nbResultsTop';
            $typeTri = 'Elec';
            $repeaterLiee = 'enveloppeOffrePapier';
            $numPageTopLiee = 'numPageTop_OffreElecPapier';
            $typeRepeater = 'OffreElecPapier';
            $typeTriLier = 'Papier';
        }
        if ('OffreElecPapier' == $arrayId[1]) {
            $repeater = 'enveloppeOffrePapier';
            $numPageTop = 'numPageTop_OffreElecPapier';
            $numPageBottom = 'numPageBottom_OffreElecPapier';
            $type = 'OffreElecPapier';
            $select = 'offreElec_msg_nbResultsBottomPapier';
            $typeTri = 'Papier';
            $repeaterLiee = 'enveloppeOffreElectronique';
            $numPageTopLiee = 'numPageTop_OffreElec';
            $typeRepeater = 'OffreElec';
            $typeTriLier = 'Elec';
        }
        if ('CandidaturePapier' == $arrayId[1]) {
            $repeater = 'enveloppeCandidaturePapier';
            $numPageTop = 'numPageTop_CandidaturePapier';
            $numPageBottom = 'numPageBottom_CandidaturePapier';
            $type = 'CandiPapier';
            $select = 'candPapier_msg_nbResultsTop';
            $typeTri = 'Papier';
            $repeaterLiee = 'enveloppeCandidatureElectronique';
            $numPageTopLiee = 'numPageTop_Candidature';
            $typeRepeater = 'Candidature';
            $typeTriLier = 'Elec';
        }
        if ('AnonymatElec' == $arrayId[1]) {
            $repeater = 'enveloppeAnonymatEletronique';
            $numPageTop = 'numPageTop_AnonymatElec';
            $numPageBottom = 'numPageBottom_AnonymatElec';
            $type = 'AnonymatElec';
            $select = 'anonymatElec_msg_nbResultsTop';
            $typeTri = 'Elec';
            $repeaterLiee = 'enveloppeAnonymatPapier';
            $numPageTopLiee = 'numPageTop_AnonymatPapier';
            $typeRepeater = 'AnonymatPapier';
            $typeTriLier = 'Papier';
        }
        if ('AnonymatPapier' == $arrayId[1]) {
            $repeater = 'enveloppeAnonymatPapier';
            $numPageTop = 'numPageTop_AnonymatPapier';
            $numPageBottom = 'numPageBottom_AnonymatPapier';
            $type = 'AnonymatPapier';
            $select = 'anonymatPapier_msg_nbResultsTop';
            $typeTri = 'Papier';
            $repeaterLiee = 'enveloppeAnonymatEletronique';
            $numPageTopLiee = 'numPageBottom_AnonymatElec';
            $typeRepeater = 'AnonymatElec';
            $typeTriLier = 'Elec';
        }
        if ('ReponseElec' == $arrayId[1]) {
            $repeater = 'enveloppeReponsesElectronique';
            $numPageTop = 'numPageTop_reponseElec';
            $numPageBottom = 'numPageBottom_reponseElec';
            $type = 'ReponseElec';
            $select = 'reponseElec_msg_nbResultsTop';
            $typeTri = 'Elec';
            $repeaterLiee = 'enveloppeReponsesPapiers';
            $numPageTopLiee = 'numPageTop_reponsePapier';
            $typeRepeater = 'ReponsePapier';
            $typeTriLier = 'Papier';
        }
        if ('ReponsePapier' == $arrayId[1]) {
            $repeater = 'enveloppeReponsesPapiers';
            $numPageTop = 'numPageTop_reponsePapier';
            $numPageBottom = 'numPageBottom_reponsePapier';
            $type = 'ReponsePapier';
            $select = 'reponsePapier_msg_nbResultsTop';
            $typeTri = 'Papier';
            $repeaterLiee = 'enveloppeReponsesElectronique';
            $numPageTopLiee = 'numPageTop_reponseElec';
            $typeRepeater = 'ReponseElec';
            $typeTriLier = 'Elec';
        }
        if ('OffreTechElec' == $arrayId[1]) {
            $repeater = 'enveloppeOffreTechniqueElectronique';
            $numPageTop = 'numPageTop_OffreTechElec';
            $numPageBottom = 'numPageBottom_OffreTechElec';
            $select = 'offreTechElec_msg_nbResultsTop';
            $type = 'OffreTechniqueElec';
            $typeTri = 'Elec';
            $repeaterLiee = 'enveloppeOffreTechniquePapier';
            $numPageTopLiee = 'numPageTop_OffreTechElecPapier';
            $typeRepeater = 'OffreTechniquePapier';
            $typeTriLier = 'Papier';
        }
        if ('OffreTechElecPapier' == $arrayId[1]) {
            $repeater = 'enveloppeOffreTechniquePapier';
            $numPageTop = 'numPageTop_OffreTechElecPapier';
            $numPageBottom = 'numPageBottom_OffreTechElecPapier';
            $type = 'OffreTechniquePapier';
            $select = 'offreTechElec_msg_nbResultsTopPapier';
            $typeTri = 'Papier';
            $repeaterLiee = 'enveloppeOffreTechniqueElectronique';
            $numPageTopLiee = 'numPageTop_OffreTechElec';
            $typeRepeater = 'OffreTechniqueElec';
            $typeTriLier = 'Elec';
        }
        //TODO les autres types
        if ($repeater) {
            $sensTri = $this->getViewState('sensTri' . $typeTri, []);
            $this->$repeater->CurrentPageIndex = $param->NewPageIndex;
            $this->$numPageTop->Text = $param->NewPageIndex + 1;
            $this->$numPageBottom->Text = $param->NewPageIndex + 1;
            $this->updatePagination($this->$select->getSelectedValue(), $param->NewPageIndex + 1, $type);
            $tri = $this->getViewState('tri' . $typeTri, '');
            $this->populateData($repeater, $type, $tri, $sensTri);
        }
        if ($repeaterLiee) {
            $sensTri = $this->getViewState('sensTri' . $typeTriLier, []);
            $tri = $this->getViewState('tri' . $typeTriLier, '');
            $this->$repeaterLiee->CurrentPageIndex = $this->$repeaterLiee->CurrentPageIndex;
            $this->$numPageTopLiee->Text = $this->$repeaterLiee->CurrentPageIndex + 1;
            $this->populateData($repeaterLiee, $typeRepeater, $tri, $sensTri);
        }
    }

    public function changePagerLenght($sender, $param)
    {
        $sensTriElec = $this->getViewState('sensTriElec', []);
        $sensTriPapier = $this->getViewState('sensTriPapier', []);
        $triElec = $this->getViewState('triElec', '');
        $triPapier = $this->getViewState('triPapier', '');
        switch ($sender->ID) {
            case 'candElec_msg_nbResultsTop':
                $pageSize = $this->candElec_msg_nbResultsTop->getSelectedValue();
                $this->candElec_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_Candidature->Text, 'Candidature');
                $this->populateData('enveloppeCandidatureElectronique', 'Candidature', $triElec, $sensTriElec);
                $this->panelEnveloppeCandidature->render($param->NewWriter);
                $this->pager_Candidature->render($param->NewWriter);
                $this->pager_CandidatureBottom->render($param->NewWriter);
                break;
            case 'candElec_msg_nbResultsBottom':
                $pageSize = $this->candElec_msg_nbResultsBottom->getSelectedValue();
                $this->candElec_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_Candidature->Text, 'Candidature');
                $this->populateData('enveloppeCandidatureElectronique', 'Candidature', $triElec, $sensTriElec);
                $this->panelEnveloppeCandidature->render($param->NewWriter);
                $this->pager_CandidatureBottom->render($param->NewWriter);
                $this->pager_Candidature->render($param->NewWriter);
                break;
            case 'candPapier_msg_nbResultsTop':
                $pageSize = $this->candPapier_msg_nbResultsTop->getSelectedValue();
                $this->candPapier_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_CandidaturePapier->Text, 'CandiPapier');
                $this->populateData('enveloppeCandidaturePapier', 'CandiPapier', $triPapier, $sensTriPapier);
                $this->panelEnveloppeCandidaturePapier->render($param->NewWriter);
                $this->pager_papier->render($param->NewWriter);
                $this->pager_papierBottom->render($param->NewWriter);
                break;
            case 'candPapier_msg_nbResultsBottom':
                $pageSize = $this->candPapier_msg_nbResultsBottom->getSelectedValue();
                $this->candPapier_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_CandidaturePapier->Text, 'CandiPapier');
                $this->populateData('enveloppeCandidaturePapier', 'CandiPapier', $triPapier, $sensTriPapier);
                $this->panelEnveloppeCandidaturePapier->render($param->NewWriter);
                $this->pager_papier->render($param->NewWriter);
                $this->pager_papierBottom->render($param->NewWriter);
                break;
            case 'offreElec_msg_nbResultsTop':
                $pageSize = $this->offreElec_msg_nbResultsTop->getSelectedValue();
                $this->offreElec_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_OffreElec->Text, 'OffreElec');
                $this->populateData('enveloppeOffreElectronique', 'OffreElec', $triElec, $sensTriElec);
                $this->panelEnveloppeOffre->render($param->NewWriter);
                $this->nbrRepOfETop->render($param->NewWriter);
                $this->nbrRepOfEBottom->render($param->NewWriter);
                break;
            case 'offreElec_msg_nbResultsBottom':
                $pageSize = $this->offreElec_msg_nbResultsBottom->getSelectedValue();
                $this->offreElec_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_OffreElec->Text, 'OffreElec');
                $this->populateData('enveloppeOffreElectronique', 'OffreElec', $triElec, $sensTriElec);
                $this->panelEnveloppeOffre->render($param->NewWriter);
                $this->nbrRepOfETop->render($param->NewWriter);
                $this->nbrRepOfEBottom->render($param->NewWriter);
                break;
            case 'offreElec_msg_nbResultsTopPapier':
                $pageSize = $this->offreElec_msg_nbResultsTopPapier->getSelectedValue();
                $this->offreElec_msg_nbResultsBottomPapier->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_OffreElecPapier->Text, 'OffreElecPapier');
                $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', $triPapier, $sensTriPapier);
                $this->panelOffrePapier->render($param->NewWriter);
                $this->nbrRepOfP->render($param->NewWriter);
                $this->nbrRepOfPBottom->render($param->NewWriter);
                break;
            case 'offreElec_msg_nbResultsBottomPapier':
                $pageSize = $this->offreElec_msg_nbResultsBottomPapier->getSelectedValue();
                $this->offreElec_msg_nbResultsTopPapier->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_OffreElecPapier->Text, 'OffreElecPapier');
                $this->populateData('enveloppeOffrePapier', 'OffreElecPapier', $triPapier, $sensTriPapier);
                $this->panelOffrePapier->render($param->NewWriter);
                $this->nbrRepOfP->render($param->NewWriter);
                $this->nbrRepOfPBottom->render($param->NewWriter);
                break;
            case 'anonymatElec_msg_nbResultsTop':
                $pageSize = $this->anonymatElec_msg_nbResultsTop->getSelectedValue();
                $this->anonymatElec_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_AnonymatElec->Text, 'AnonymatElec');
                $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $triElec, $sensTriElec);
                $this->panelEnveloppeAnonymat->render($param->NewWriter);
                $this->nbrRepAEBottom->render($param->NewWriter);
                $this->nbrRepAETop->render($param->NewWriter);
                break;
            case 'anonymatElec_msg_nbResultsBottom':
                $pageSize = $this->anonymatElec_msg_nbResultsBottom->getSelectedValue();
                $this->anonymatElec_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_AnonymatElec->Text, 'AnonymatElec');
                $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $triElec, $sensTriElec);
                $this->panelEnveloppeAnonymat->render($param->NewWriter);
                $this->nbrRepAEBottom->render($param->NewWriter);
                $this->nbrRepAETop->render($param->NewWriter);
                break;
            case 'anonymatPapier_msg_nbResultsTop':
                $pageSize = $this->anonymatPapier_msg_nbResultsTop->getSelectedValue();
                $this->anonymatPapier_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_AnonymatPapier->Text, 'AnonymatPapier');
                $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', $triPapier, $sensTriPapier);
                $this->panelAnonymatPapier->render($param->NewWriter);
                $this->nbrRepAPBottom->render($param->NewWriter);
                $this->nbrRepAPTop->render($param->NewWriter);
                break;
            case 'anonymatPapier_msg_nbResultsBottom':
                $pageSize = $this->anonymatPapier_msg_nbResultsBottom->getSelectedValue();
                $this->anonymatPapier_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_AnonymatPapier->Text, 'AnonymatPapier');
                $this->populateData('enveloppeAnonymatPapier', 'AnonymatPapier', $triPapier, $sensTriPapier);
                $this->panelAnonymatPapier->render($param->NewWriter);
                $this->nbrRepAPBottom->render($param->NewWriter);
                $this->nbrRepAPTop->render($param->NewWriter);
                break;
            case 'reponseElec_msg_nbResultsTop':
                $pageSize = $this->reponseElec_msg_nbResultsTop->getSelectedValue();
                $this->reponseElec_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_reponseElec->Text, 'ReponseElec');
                $this->populateData('enveloppeReponsesElectronique', 'ReponseElec', $triElec, $sensTriElec);
                $this->panelReponseElectronique->render($param->NewWriter);
                $this->panelPagerReponseElecTop->render($param->NewWriter);
                $this->panelPagerReponseElecBottom->render($param->NewWriter);
                break;
            case 'reponseElec_msg_nbResultsBottom':
                $pageSize = $this->reponseElec_msg_nbResultsBottom->getSelectedValue();
                $this->reponseElec_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_reponseElec->Text, 'ReponseElec');
                $this->populateData('enveloppeReponsesElectronique', 'ReponseElec', $triElec, $sensTriElec);
                $this->panelReponseElectronique->render($param->NewWriter);
                $this->panelPagerReponseElecTop->render($param->NewWriter);
                $this->panelPagerReponseElecBottom->render($param->NewWriter);
                break;

            case 'reponsePapier_msg_nbResultsTop':
                $pageSize = $this->reponsePapier_msg_nbResultsTop->getSelectedValue();
                $this->reponsePapier_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_reponsePapier->Text, 'ReponsePapier');
                $this->populateData('enveloppeReponsesPapiers', 'ReponsePapier', $triPapier, $sensTriPapier);
                $this->panelReponsePapier->render($param->NewWriter);
                $this->panelPagerReponsePapierTop->render($param->NewWriter);
                $this->panelPagerReponsePapierBottom->render($param->NewWriter);
                break;
            case 'reponsePapier_msg_nbResultsBottom':
                $pageSize = $this->reponsePapier_msg_nbResultsBottom->getSelectedValue();
                $this->reponsePapier_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_reponsePapier->Text, 'ReponsePapier');
                $this->populateData('enveloppeReponsesPapiers', 'ReponsePapier', $triPapier, $sensTriPapier);
                $this->panelReponsePapier->render($param->NewWriter);
                $this->panelPagerReponsePapierTop->render($param->NewWriter);
                $this->panelPagerReponsePapierBottom->render($param->NewWriter);
                break;
            case 'offreTechElec_msg_nbResultsTop':
                $pageSize = $this->offreTechElec_msg_nbResultsTop->getSelectedValue();
                $this->offreTechElec_msg_nbResultsBottom->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_OffreTechElec->Text, 'OffreTechniqueElec');
                $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $triElec, $sensTriElec);
                $this->panelEnveloppeOffreTechnique->render($param->NewWriter);
                $this->nbrRepOtETop->render($param->NewWriter);
                $this->nbrRepOtEBottom->render($param->NewWriter);
                break;
            case 'offreTechElec_msg_nbResultsBottom':
                $pageSize = $this->offreTechElec_msg_nbResultsBottom->getSelectedValue();
                $this->offreTechElec_msg_nbResultsTop->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_OffreTechElec->Text, 'OffreTechniqueElec');
                $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $triElec, $sensTriElec);
                $this->panelEnveloppeOffreTechnique->render($param->NewWriter);
                $this->nbrRepOtETop->render($param->NewWriter);
                $this->nbrRepOtEBottom->render($param->NewWriter);
                break;
            case 'offreTechElec_msg_nbResultsTopPapier':
                $pageSize = $this->offreTechElec_msg_nbResultsTopPapier->getSelectedValue();
                $this->offreTechElec_msg_nbResultsBottomPapier->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageTop_OffreTechElecPapier->Text, 'OffreTechniquePapier');
                $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', $triPapier, $sensTriPapier);
                $this->panelEnveloppeOffreTechniquePapier->render($param->NewWriter);
                $this->nbrRepOtPTop->render($param->NewWriter);
                $this->nbrRepOtPBottom->render($param->NewWriter);
                break;
            case 'offreTechElec_msg_nbResultsBottomPapier':
                $pageSize = $this->offreTechElec_msg_nbResultsBottomPapier->getSelectedValue();
                $this->offreTechElec_msg_nbResultsTopPapier->setSelectedValue($pageSize);
                $this->updatePagination($pageSize, $this->numPageBottom_OffreTechElecPapier->Text, 'OffreTechniquePapier');
                $this->populateData('enveloppeOffreTechniquePapier', 'OffreTechniquePapier', $triPapier, $sensTriPapier);
                $this->panelEnveloppeOffreTechniquePapier->render($param->NewWriter);
                $this->nbrRepOtPTop->render($param->NewWriter);
                $this->nbrRepOtPBottom->render($param->NewWriter);
                break;
        }
    }

    /*
    * Permet de mettre à jour la pagination
    */
    public function updatePagination($pageSize, $numPage, $type)
    {
        if ('Candidature' == $type) {
            $nombreElement = $this->getViewState('nombreElementCandi');
            $repeater = 'enveloppeCandidatureElectronique';
            $nombrePageTop = 'nombrePageTop_Candidature';
            $numPageTop = 'numPageTop_Candidature';
            $nombrePageBottom = 'nombrePageBottom_Candidature';
            $numPageBottom = 'numPageBottom_Candidature';
        }
        if ('OffreElec' == $type) {
            $nombreElement = $this->getViewState('nombreElementOffreElec');
            $repeater = 'enveloppeOffreElectronique';
            $nombrePageTop = 'nombrePageTop_OffreElec';
            $numPageTop = 'numPageTop_OffreElec';
            $nombrePageBottom = 'nombrePageBottom_OffreElec';
            $numPageBottom = 'numPageBottom_OffreElec';
        }
        if ('OffreTechniqueElec' == $type) {
            $nombreElement = $this->getViewState('nombreElementOffreTechElec');
            $repeater = 'enveloppeOffreTechniqueElectronique';
            $nombrePageTop = 'nombrePageTop_OffreTechElec';
            $numPageTop = 'numPageTop_OffreTechElec';
            $nombrePageBottom = 'nombrePageBottom_OffreTechElec';
            $numPageBottom = 'numPageBottom_OffreTechElec';
        }
        if ('OffreTechniquePapier' == $type) {
            $nombreElement = $this->getViewState('nombreElementOffreTechPapier');
            $repeater = 'enveloppeOffreTechniquePapier';
            $nombrePageTop = 'nombrePageTop_OffreTechElecPapier';
            $numPageTop = 'numPageTop_OffreTechElecPapier';
            $nombrePageBottom = 'nombrePageBottom_OffreTechElecPapier';
            $numPageBottom = 'numPageBottom_OffreTechElecPapier';
        }
        if ('CandiPapier' == $type) {
            $nombreElement = $this->getViewState('nombreElementCandiPapier');
            $repeater = 'enveloppeCandidaturePapier';
            $nombrePageTop = 'nombrePageTop_CandidaturePapier';
            $numPageTop = 'numPageTop_CandidaturePapier';
            $nombrePageBottom = 'nombrePageBottom_CandidaturePapier';
            $numPageBottom = 'numPageBottom_CandidaturePapier';
        }

        if ('OffreElecPapier' == $type) {
            $nombreElement = $this->getViewState('nombreElementOffreElecPapier');
            $repeater = 'enveloppeOffrePapier';
            $nombrePageTop = 'nombrePageTop_OffreElecPapier';
            $numPageTop = 'numPageTop_OffreElecPapier';
            $nombrePageBottom = 'nombrePageBottom_OffreElecPapier';
            $numPageBottom = 'numPageBottom_OffreElecPapier';
        }
        if ('AnonymatElec' == $type) {
            $nombreElement = $this->getViewState('nombreElementAnonymatElec');
            $repeater = 'enveloppeAnonymatEletronique';
            $nombrePageTop = 'nombrePageTop_AnonymatElec';
            $numPageTop = 'numPageTop_AnonymatElec';
            $nombrePageBottom = 'nombrePageBottom_AnonymatElec';
            $numPageBottom = 'numPageBottom_AnonymatElec';
        }
        if ('AnonymatPapier' == $type) {
            $nombreElement = $this->getViewState('nombreElementAnonymatPapier');
            $repeater = 'enveloppeAnonymatPapier';
            $nombrePageTop = 'nombrePageTop_AnonymatPapier';
            $numPageTop = 'numPageTop_AnonymatPapier';
            $nombrePageBottom = 'nombrePageBottom_AnonymatPapier';
            $numPageBottom = 'numPageBottom_AnonymatPapier';
        }
        if ('ReponseElec' == $type) {
            $nombreElement = $this->getViewState('nombreElementReponseElec');
            $repeater = 'enveloppeReponsesElectronique';
            $nombrePageTop = 'nombrePageTop_reponseElec';
            $numPageTop = 'numPageTop_reponseElec';
            $nombrePageBottom = 'nombrePageBottom_reponseElec';
            $numPageBottom = 'numPageBottom_reponseElec';
        }
        if ('ReponsePapier' == $type) {
            $nombreElement = $this->getViewState('nombreElementReponsePapier');
            $repeater = 'enveloppeReponsesPapiers';
            $nombrePageTop = 'nombrePageTop_reponsePapier';
            $numPageTop = 'numPageTop_reponsePapier';
            $nombrePageBottom = 'nombrePageBottom_reponsePapier';
            $numPageBottom = 'numPageBottom_reponsePapier';
        }
        $this->$repeater->PageSize = $pageSize;
        $this->$nombrePageTop->Text = ceil($nombreElement / $this->$repeater->PageSize);
        $this->$nombrePageBottom->Text = ceil($nombreElement / $this->$repeater->PageSize);
        if ($numPage > $this->$nombrePageTop->Text) {
            $numPage = $this->$nombrePageTop->Text;
        } elseif ($numPage > $this->$nombrePageBottom->Text) {
            $numPage = $this->$nombrePageBottom->Text;
        }
        $this->$repeater->setVirtualItemCount($nombreElement);
        $this->$repeater->setCurrentPageIndex($numPage - 1);
        $this->$numPageTop->Text = $numPage;
        $this->$numPageBottom->Text = $numPage;
    }

    /*
    * Permet d'afficher les composants liee au pager
    */
    public function showComponent($type)
    {
        if ('Candidature' == $type && null != $this->enveloppeCandidatureElectronique) {
            $this->Pager_Candidature_Top->Visible = true;
            $this->panelBouttonGotoPageTop_Candi->Visible = true;
            $this->candElec_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_Candi->Visible = true;
            $this->labelAfficherTop_candi->Visible = true;
            $this->labelSlashTop_candi->Visible = true;
            $this->nombrePageTop_Candidature->Visible = true;

            $this->Pager_Candidature_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_Candi->Visible = true;
            $this->candElec_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_Candi->Visible = true;
            $this->labelAfficherBottom_candi->Visible = true;
            $this->labelSlashBottom_candi->Visible = true;
            $this->nombrePageBottom_Candidature->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesCanditature->setVisible(true);
            } else {
                $this->panelActionsGroupeesCanditature->setVisible(false);
            }
        }
        if ('CandiPapier' == $type && null != $this->enveloppeCandidaturePapier) {
            $this->Pager_CandidaturePapier_Top->Visible = true;
            $this->panelBouttonGotoPageTop_CandiPapier->Visible = true;
            $this->candPapier_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_CandiPapier->Visible = true;
            $this->labelAfficherTop_candiPapier->Visible = true;
            $this->labelSlashTop_candiPapier->Visible = true;
            $this->nombrePageTop_CandidaturePapier->Visible = true;
            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesCanditaturePapier->setVisible(true);
            } else {
                $this->panelActionsGroupeesCanditaturePapier->setVisible(false);
            }
        }
        if ('OffreElec' == $type && null != $this->enveloppeOffreElectronique) {
            $this->Pager_OffreElec_Top->Visible = true;
            $this->panelBouttonGotoPageTop_OffreElec->Visible = true;
            $this->offreElec_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_OffreElec->Visible = true;
            $this->labelAfficherTop_OffreElec->Visible = true;
            $this->labelSlashTop_OffreElec->Visible = true;
            $this->nombrePageTop_OffreElec->Visible = true;

            $this->Pager_OffreElec_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_OffreElec->Visible = true;
            $this->offreElec_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_OffreElec->Visible = true;
            $this->labelAfficherBottom_OffreElec->Visible = true;
            $this->labelSlashBottom_OffreElec->Visible = true;
            $this->nombrePageBottom_OffreElec->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffre->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffre->setVisible(false);
            }
        }
        if ('OffreElecPapier' == $type && null != $this->enveloppeOffrePapier) {
            $this->Pager_OffreElecPapier_Top->Visible = true;
            $this->panelBouttonGotoPageTop_OffreElecPapier->Visible = true;
            $this->offreElec_msg_nbResultsTopPapier->Visible = true;
            $this->resultParPageTop_OffreElecPapier->Visible = true;
            $this->labelAfficherTop_OffreElecPapier->Visible = true;
            $this->labelSlashTop_OffreElecPapier->Visible = true;
            $this->nombrePageTop_OffreElecPapier->Visible = true;

            $this->Pager_OffreElecPapier_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_OffreElecPapier->Visible = true;
            $this->offreElec_msg_nbResultsBottomPapier->Visible = true;
            $this->resultParPageBottom_OffreElecPapier->Visible = true;
            $this->labelAfficherBottom_OffreElecPapier->Visible = true;
            $this->labelSlashBottom_OffreElecPapier->Visible = true;
            $this->nombrePageBottom_OffreElecPapier->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffrePapier->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffrePapier->setVisible(false);
            }
        }
        if ('AnonymatElec' == $type && null != $this->enveloppeAnonymatEletronique) {
            $this->Pager_AnonymatElec_Top->Visible = true;
            $this->panelBouttonGotoPageTop_AnonymatElec->Visible = true;
            $this->anonymatElec_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_AnonymatElec->Visible = true;
            $this->labelAfficherTop_AnonymatElec->Visible = true;
            $this->labelSlashTop_AnonymatElec->Visible = true;
            $this->nombrePageTop_AnonymatElec->Visible = true;

            $this->Pager_AnonymatElec_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_AnonymatElec->Visible = true;
            $this->anonymatElec_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_AnonymatElec->Visible = true;
            $this->labelAfficherBottom_AnonymatElec->Visible = true;
            $this->labelSlashBottom_AnonymatElec->Visible = true;
            $this->nombrePageBottom_AnonymatElec->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffreAnonymat->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffreAnonymat->setVisible(false);
            }
        }
        if ('AnonymatPapier' == $type && null != $this->enveloppeAnonymatPapier) {
            $this->Pager_AnonymatPapier_Top->Visible = true;
            $this->panelBouttonGotoPageTop_AnonymatPapier->Visible = true;
            $this->anonymatPapier_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_AnonymatPapier->Visible = true;
            $this->labelAfficherTop_AnonymatPapier->Visible = true;
            $this->labelSlashTop_AnonymatPapier->Visible = true;
            $this->nombrePageTop_AnonymatPapier->Visible = true;

            $this->Pager_AnonymatPapier_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_AnonymatPapier->Visible = true;
            $this->anonymatPapier_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_AnonymatPapier->Visible = true;
            $this->labelAfficherBottom_AnonymatPapier->Visible = true;
            $this->labelSlashBottom_AnonymatPapier->Visible = true;
            $this->nombrePageBottom_AnonymatPapier->Visible = true;
            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffreAnonymatPapier->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffreAnonymatPapier->setVisible(false);
            }
        }
        if ('ReponseElec' == $type && null != $this->enveloppeReponsesElectronique) {
            $this->Pager_ReponseElec_Top->Visible = true;
            $this->panelBouttonGotoPageTop_reponseElec->Visible = true;
            $this->reponseElec_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_reponseElec->Visible = true;
            $this->labelAfficherTop_reponseElec->Visible = true;
            $this->labelSlashTop_reponseElec->Visible = true;
            $this->nombrePageTop_reponseElec->Visible = true;

            $this->Pager_ReponseElec_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_reponseElec->Visible = true;
            $this->reponseElec_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_reponseElec->Visible = true;
            $this->labelAfficherBottom_reponseElec->Visible = true;
            $this->labelSlashBottom_reponseElec->Visible = true;
            $this->nombrePageBottom_reponseElec->Visible = true;

            $this->panelActionsGroupeesElectro->setVisible(true);
            if (!isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesElectro->setVisible(true);
            } else {
                $this->panelActionsGroupeesElectro->setVisible(false);
            }
        }
        if ('ReponsePapier' == $type && null != $this->enveloppeReponsesPapiers) {
            $this->Pager_ReponsePapier_Top->Visible = true;
            $this->panelBouttonGotoPageTop_reponsePapier->Visible = true;
            $this->reponsePapier_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_reponsePapier->Visible = true;
            $this->labelAfficherTop_reponsePapier->Visible = true;
            $this->labelSlashTop_reponsePapier->Visible = true;
            $this->nombrePageTop_reponsePapier->Visible = true;

            $this->Pager_ReponsePapier_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_reponsePapier->Visible = true;
            $this->reponsePapier_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_reponsePapier->Visible = true;
            $this->labelAfficherBottom_reponsePapier->Visible = true;
            $this->labelSlashBottom_reponsePapier->Visible = true;
            $this->nombrePageBottom_reponsePapier->Visible = true;

            $this->panelActionsGroupeesPapier->setVisible(true);
            if (!isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesPapier->setVisible(true);
            } else {
                $this->panelActionsGroupeesPapier->setVisible(false);
            }
        }
        if ('OffreTechniqueElec' == $type && null != $this->enveloppeOffreTechniqueElectronique) {
            $this->Pager_OffreTechElec_Top->Visible = true;
            $this->panelBouttonGotoPageTop_OffreTechElec->Visible = true;
            $this->offreTechElec_msg_nbResultsTop->Visible = true;
            $this->resultParPageTop_OffreTechElec->Visible = true;
            $this->labelAfficherTop_OffreTechElec->Visible = true;
            $this->labelSlashTop_OffreTechElec->Visible = true;
            $this->nombrePageTop_OffreTechElec->Visible = true;

            $this->Pager_OffreTechElec_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_OffreTechElec->Visible = true;
            $this->offreTechElec_msg_nbResultsBottom->Visible = true;
            $this->resultParPageBottom_OffreTechElec->Visible = true;
            $this->labelAfficherBottom_OffreTechElec->Visible = true;
            $this->labelSlashBottom_OffreTechElec->Visible = true;
            $this->nombrePageBottom_OffreTechElec->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffreTechniqueElect->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffreTechniqueElect->setVisible(false);
            }
        }
        if ('OffreTechniquePapier' == $type && null != $this->enveloppeOffreTechniqueElectronique) {
            $this->Pager_OffreTechElecPapier_Top->Visible = true;
            $this->panelBouttonGotoPageTop_OffreTechElecPapier->Visible = true;
            $this->offreTechElec_msg_nbResultsTopPapier->Visible = true;
            $this->resultParPageTop_OffreTechElecPapier->Visible = true;
            $this->labelAfficherTop_OffreTechElecPapier->Visible = true;
            $this->labelSlashTop_OffreTechElecPapier->Visible = true;
            $this->nombrePageTop_OffreTechElecPapier->Visible = true;

            $this->Pager_OffreTechElecPapier_Bottom->Visible = true;
            $this->panelBouttonGotoPageBottom_OffreTechElecPapier->Visible = true;
            $this->offreElec_msg_nbResultsBottomPapier->Visible = true;
            $this->resultParPageBottom_OffreTechElecPapier->Visible = true;
            $this->labelAfficherBottom_OffreTechElecPapier->Visible = true;
            $this->labelSlashBottom_OffreTechElecPapier->Visible = true;
            $this->nombrePageBottom_OffreTechElecPapier->Visible = true;

            if (!$this->isModeConsultationUnique() && !isset($_GET['telechargerOffres'])) {
                $this->panelActionsGroupeesOffreTechniquePapier->setVisible(true);
            } else {
                $this->panelActionsGroupeesOffreTechniquePapier->setVisible(false);
            }
        }
    }

    /*
    * Permet de masquer les composants liee au pager
    */
    public function hideComponent($type)
    {
        if ('Candidature' == $type && null != $this->enveloppeCandidatureElectronique) {
            $this->Pager_Candidature_Top->Visible = false;
            $this->panelBouttonGotoPageTop_Candi->Visible = false;
            $this->candElec_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_Candi->Visible = false;
            $this->labelAfficherTop_candi->Visible = false;
            $this->labelSlashTop_candi->Visible = false;
            $this->nombrePageTop_Candidature->Visible = false;

            $this->Pager_Candidature_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_Candi->Visible = false;
            $this->candElec_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_Candi->Visible = false;
            $this->labelAfficherBottom_candi->Visible = false;
            $this->labelSlashBottom_candi->Visible = false;
            $this->nombrePageBottom_Candidature->Visible = false;

            $this->panelActionsGroupeesCanditature->setVisible(false);
        }
        if ('CandiPapier' == $type && null != $this->enveloppeCandidaturePapier) {
            $this->Pager_CandidaturePapier_Top->Visible = false;
            $this->panelBouttonGotoPageTop_CandiPapier->Visible = false;
            $this->candPapier_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_CandiPapier->Visible = false;
            $this->labelAfficherTop_candiPapier->Visible = false;
            $this->labelSlashTop_candiPapier->Visible = false;
            $this->nombrePageTop_CandidaturePapier->Visible = false;

            $this->Pager_CandidaturePapier_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_CandiPapier->Visible = false;
            $this->candPapier_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_CandiPapier->Visible = false;
            $this->labelAfficherBottom_candiPapier->Visible = false;
            $this->labelSlashBottom_candiPapier->Visible = false;
            $this->nombrePageBottom_CandidaturePapier->Visible = false;

            $this->panelActionsGroupeesCanditaturePapier->Visible = false;
        }
        if ('OffreElec' == $type && null != $this->enveloppeOffreElectronique) {
            $this->Pager_OffreElec_Top->Visible = false;
            $this->panelBouttonGotoPageTop_OffreElec->Visible = false;
            $this->offreElec_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_OffreElec->Visible = false;
            $this->labelAfficherTop_OffreElec->Visible = false;
            $this->labelSlashTop_OffreElec->Visible = false;
            $this->nombrePageTop_OffreElec->Visible = false;

            $this->Pager_OffreElec_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_OffreElec->Visible = false;
            $this->offreElec_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_OffreElec->Visible = false;
            $this->labelAfficherBottom_OffreElec->Visible = false;
            $this->labelSlashBottom_OffreElec->Visible = false;
            $this->nombrePageBottom_OffreElec->Visible = false;

            $this->panelActionsGroupeesOffre->setVisible(false);
        }
        if ('OffreElecPapier' == $type && null != $this->enveloppeOffrePapier) {
            $this->Pager_OffreElecPapier_Top->Visible = false;
            $this->panelBouttonGotoPageTop_OffreElecPapier->Visible = false;
            $this->offreElec_msg_nbResultsTopPapier->Visible = false;
            $this->resultParPageTop_OffreElecPapier->Visible = false;
            $this->labelAfficherTop_OffreElecPapier->Visible = false;
            $this->labelSlashTop_OffreElecPapier->Visible = false;
            $this->nombrePageTop_OffreElecPapier->Visible = false;

            $this->Pager_OffreElecPapier_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_OffreElecPapier->Visible = false;
            $this->offreElec_msg_nbResultsBottomPapier->Visible = false;
            $this->resultParPageBottom_OffreElecPapier->Visible = false;
            $this->labelAfficherBottom_OffreElecPapier->Visible = false;
            $this->labelSlashBottom_OffreElecPapier->Visible = false;
            $this->nombrePageBottom_OffreElecPapier->Visible = false;
            $this->panelActionsGroupeesOffrePapier->setVisible(false);
        }
        if ('AnonymatElec' == $type && null != $this->enveloppeAnonymatEletronique) {
            $this->Pager_AnonymatElec_Top->Visible = false;
            $this->panelBouttonGotoPageTop_AnonymatElec->Visible = false;
            $this->anonymatElec_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_AnonymatElec->Visible = false;
            $this->labelAfficherTop_AnonymatElec->Visible = false;
            $this->labelSlashTop_AnonymatElec->Visible = false;
            $this->nombrePageTop_AnonymatElec->Visible = false;

            $this->Pager_AnonymatElec_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_AnonymatElec->Visible = false;
            $this->anonymatElec_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_AnonymatElec->Visible = false;
            $this->labelAfficherBottom_AnonymatElec->Visible = false;
            $this->labelSlashBottom_AnonymatElec->Visible = false;
            $this->nombrePageBottom_AnonymatElec->Visible = false;

            $this->panelActionsGroupeesOffreAnonymat->setVisible(false);
        }
        if ('AnonymatPapier' == $type && null != $this->enveloppeAnonymatPapier) {
            $this->Pager_AnonymatPapier_Top->Visible = false;
            $this->panelBouttonGotoPageTop_AnonymatPapier->Visible = false;
            $this->anonymatPapier_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_AnonymatPapier->Visible = false;
            $this->labelAfficherTop_AnonymatPapier->Visible = false;
            $this->labelSlashTop_AnonymatPapier->Visible = false;
            $this->nombrePageTop_AnonymatPapier->Visible = false;

            $this->Pager_AnonymatPapier_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_AnonymatPapier->Visible = false;
            $this->anonymatPapier_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_AnonymatPapier->Visible = false;
            $this->labelAfficherBottom_AnonymatPapier->Visible = false;
            $this->labelSlashBottom_AnonymatPapier->Visible = false;
            $this->nombrePageBottom_AnonymatPapier->Visible = false;

            $this->panelActionsGroupeesOffreAnonymatPapier->setVisible(false);
        }
        if ('ReponseElec' == $type && null != $this->enveloppeReponsesElectronique) {
            $this->Pager_ReponseElec_Top->Visible = false;
            $this->panelBouttonGotoPageTop_reponseElec->Visible = false;
            $this->reponseElec_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_reponseElec->Visible = false;
            $this->labelAfficherTop_reponseElec->Visible = false;
            $this->labelSlashTop_reponseElec->Visible = false;
            $this->nombrePageTop_reponseElec->Visible = false;

            $this->Pager_ReponseElec_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_reponseElec->Visible = false;
            $this->reponseElec_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_reponseElec->Visible = false;
            $this->labelAfficherBottom_reponseElec->Visible = false;
            $this->labelSlashBottom_reponseElec->Visible = false;
            $this->nombrePageBottom_reponseElec->Visible = false;

            $this->panelActionsGroupeesElectro->setVisible(false);
        }
        if ('ReponsePapier' == $type && null != $this->enveloppeReponsesPapiers) {
            $this->Pager_ReponsePapier_Top->Visible = false;
            $this->panelBouttonGotoPageTop_reponsePapier->Visible = false;
            $this->reponsePapier_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_reponsePapier->Visible = false;
            $this->labelAfficherTop_reponsePapier->Visible = false;
            $this->labelSlashTop_reponsePapier->Visible = false;
            $this->nombrePageTop_reponsePapier->Visible = false;

            $this->Pager_ReponsePapier_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_reponsePapier->Visible = false;
            $this->reponsePapier_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_reponsePapier->Visible = false;
            $this->labelAfficherBottom_reponsePapier->Visible = false;
            $this->labelSlashBottom_reponsePapier->Visible = false;
            $this->nombrePageBottom_reponsePapier->Visible = false;

            $this->panelActionsGroupeesPapier->setVisible(false);
        }
        if ('OffreTechniqueElec' == $type && null != $this->enveloppeOffreTechniqueElectronique) {
            $this->Pager_OffreTechElec_Top->Visible = false;
            $this->panelBouttonGotoPageTop_OffreTechElec->Visible = false;
            $this->offreTechElec_msg_nbResultsTop->Visible = false;
            $this->resultParPageTop_OffreTechElec->Visible = false;
            $this->labelAfficherTop_OffreTechElec->Visible = false;
            $this->labelSlashTop_OffreTechElec->Visible = false;
            $this->nombrePageTop_OffreTechElec->Visible = false;

            $this->Pager_OffreTechElec_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_OffreTechElec->Visible = false;
            $this->offreTechElec_msg_nbResultsBottom->Visible = false;
            $this->resultParPageBottom_OffreTechElec->Visible = false;
            $this->labelAfficherBottom_OffreTechElec->Visible = false;
            $this->labelSlashBottom_OffreTechElec->Visible = false;
            $this->nombrePageBottom_OffreTechElec->Visible = false;

            $this->panelActionsGroupeesOffreTechniqueElect->setVisible(false);
        }
        if ('OffreTechniquePapier' == $type && null != $this->enveloppeOffreTechniqueElectronique) {
            $this->Pager_OffreTechElecPapier_Top->Visible = false;
            $this->panelBouttonGotoPageTop_OffreTechElecPapier->Visible = false;
            $this->offreTechElec_msg_nbResultsTopPapier->Visible = false;
            $this->resultParPageTop_OffreTechElecPapier->Visible = false;
            $this->labelAfficherTop_OffreTechElecPapier->Visible = false;
            $this->labelSlashTop_OffreTechElecPapier->Visible = false;
            $this->nombrePageTop_OffreTechElecPapier->Visible = false;

            $this->Pager_OffreTechElecPapier_Bottom->Visible = false;
            $this->panelBouttonGotoPageBottom_OffreTechElecPapier->Visible = false;
            $this->offreTechElec_msg_nbResultsBottomPapier->Visible = false;
            $this->resultParPageBottom_OffreTechElecPapier->Visible = false;
            $this->labelAfficherBottom_OffreTechElecPapier->Visible = false;
            $this->labelSlashBottom_OffreTechElecPapier->Visible = false;
            $this->nombrePageBottom_OffreTechElecPapier->Visible = false;

            $this->panelActionsGroupeesOffreTechniquePapier->setVisible(false);
        }
    }

    public function goToPage($sender, $param)
    {
        if (('DefaultButtonTop_candi' == $sender->ID) || ('DefaultButtonTop_candiPapier' == $sender->ID)) {
            $nombreElement = $this->getViewState('nombreElementCandi');
            $repeater = 'enveloppeCandidatureElectronique';
            $type = 'Candidature';
            $nombrePageTop = 'nombrePageTop_Candidature';
            $numPageTop = 'numPageTop_Candidature';
            $select = 'candElec_msg_nbResultsTop';

            $nombreElementPapier = $this->getViewState('nombreElementCandiPapier');
            $repeaterPapier = 'enveloppeCandidaturePapier';
            $typePapier = 'CandiPapier';
            $nombrePageTopPapier = 'nombreCandidaturePapier';
            $numPageTopPapier = 'numPageTop_CandidaturePapier';
            $selectPapier = 'candPapier_msg_nbResultsTop';
        } elseif ('DefaultButtonBottom_candi' == $sender->ID || 'DefaultButtonBottom_candiPapier' == $sender->ID) {
            $nombreElement = $this->getViewState('nombreElementCandi');
            $repeater = 'enveloppeCandidatureElectronique';
            $type = 'Candidature';
            $nombrePageTop = 'nombrePageBottom_Candidature';
            $numPageTop = 'numPageBottom_Candidature';
            $select = 'candElec_msg_nbResultsBottom';

            $nombreElementPapier = $this->getViewState('nombreElementCandiPapier');
            $repeaterPapier = 'enveloppeCandidaturePapier';
            $typePapier = 'CandiPapier';
            $nombrePageTopPapier = 'nombreCandidaturePapier';
            $numPageTopPapier = 'numPageBottom_CandidaturePapier';
            $selectPapier = 'candPapier_msg_nbResultsBottom';
        } elseif (('DefaultButtonTop_OffreElec' == $sender->ID) || ('DefaultButtonTop_OffreElecPapier' == $sender->ID)) {
            $repeater = 'enveloppeOffreElectronique';
            $type = 'OffreElec';
            $nombrePageTop = 'nombrePageTop_OffreElec';
            $numPageTop = 'numPageTop_OffreElec';
            $select = 'offreElec_msg_nbResultsTop';

            $repeaterPapier = 'enveloppeOffrePapier';
            $typePapier = 'OffreElecPapier';
            $nombrePageTopPapier = 'nombrePageTop_OffreElecPapier';
            $numPageTopPapier = 'numPageTop_OffreElecPapier';
            $selectPapier = 'offreElec_msg_nbResultsTopPapier';
        } elseif (('DefaultButtonBottom_OffreElec' == $sender->ID) || ('DefaultButtonBottom_OffreElecPapier' == $sender->ID)) {
            $repeater = 'enveloppeOffreElectronique';
            $type = 'OffreElec';
            $nombrePageTop = 'nombrePageBottom_OffreElec';
            $numPageTop = 'numPageBottom_OffreElec';
            $select = 'offreElec_msg_nbResultsBottom';

            $repeaterPapier = 'enveloppeOffrePapier';
            $typePapier = 'OffreElecPapier';
            $nombrePageTopPapier = 'nombrePageBottom_OffreElecPapier';
            $numPageTopPapier = 'numPageBottom_OffreElecPapier';
            $selectPapier = 'offreElec_msg_nbResultsBottomPapier';
        } elseif (('DefaultButtonTop_AnonymatElec' == $sender->ID) || ('DefaultButtonTop_AnonymatPapier' == $sender->ID)) {
            $repeater = 'enveloppeAnonymatEletronique';
            $type = 'AnonymatElec';
            $nombrePageTop = 'nombrePageTop_AnonymatElec';
            $numPageTop = 'numPageTop_AnonymatElec';
            $select = 'anonymatElec_msg_nbResultsTop';

            $repeaterPapier = 'enveloppeAnonymatPapier';
            $typePapier = 'AnonymatPapier';
            $nombrePageTopPapier = 'nombrePageTop_AnonymatPapier';
            $numPageTopPapier = 'numPageTop_AnonymatPapier';
            $selectPapier = 'anonymatPapier_msg_nbResultsTop';
        } elseif (('DefaultButtonBottom_AnonymatElec' == $sender->ID) || ('DefaultButtonBottom_AnonymatPapier' == $sender->ID)) {
            $repeater = 'enveloppeAnonymatEletronique';
            $type = 'AnonymatElec';
            $nombrePageTop = 'nombrePageBottom_AnonymatElec';
            $numPageTop = 'numPageBottom_AnonymatElec';
            $select = 'anonymatElec_msg_nbResultsBottom';

            $repeaterPapier = 'enveloppeAnonymatPapier';
            $typePapier = 'AnonymatPapier';
            $nombrePageTopPapier = 'nombrePageBottom_AnonymatPapier';
            $numPageTopPapier = 'numPageBottom_AnonymatPapier';
            $selectPapier = 'anonymatPapier_msg_nbResultsBottom';
        } elseif (('DefaultButtonTop_OffreTechElec' == $sender->ID) || ('DefaultButtonTop_OffreTechElecPapier' == $sender->ID)) {
            $repeater = 'enveloppeOffreTechniqueElectronique';
            $type = 'OffreTechniqueElec';
            $nombrePageTop = 'nombrePageTop_OffreTechElec';
            $numPageTop = 'numPageTop_OffreTechElec';
            $select = 'offreTechElec_msg_nbResultsTop';

            $repeaterPapier = 'enveloppeOffreTechniquePapier';
            $typePapier = 'OffreTechniquePapier';
            $nombrePageTopPapier = 'nombrePageTop_OffreTechElecPapier';
            $numPageTopPapier = 'numPageTop_OffreTechElecPapier';
            $selectPapier = 'offreTechElec_msg_nbResultsTopPapier';
        } elseif (('DefaultButtonBottom_OffreTechElec' == $sender->ID) || ('DefaultButtonBottom_OffreTechElecPapier' == $sender->ID)) {
            $repeater = 'enveloppeOffreTechniqueElectronique';
            $type = 'OffreTechniqueElec';
            $nombrePageTop = 'nombrePageBottom_OffreTechElec';
            $numPageTop = 'numPageBottom_OffreTechElec';
            $select = 'offreTechElec_msg_nbResultsBottom';

            $repeaterPapier = 'enveloppeOffreTechniquePapier';
            $typePapier = 'OffreTechniquePapier';
            $nombrePageTopPapier = 'nombrePageBottom_OffreTechElecPapier';
            $numPageTopPapier = 'numPageBottom_OffreTechElecPapier';
            $selectPapier = 'offreTechElec_msg_nbResultsBottomPapier';
        }
        //TODO
        if (('DefaultButtonTop_reponseElec' == $sender->ID) || ('DefaultButtonTop_reponsePapier' == $sender->ID)) {
            $nombreElement = $this->getViewState('nombreElementReponseElec');
            $repeater = 'enveloppeReponsesElectronique';
            $type = 'ReponseElec';
            $nombrePageTop = 'nombrePageTop_reponseElec';
            $numPageTop = 'numPageTop_reponseElec';
            $select = 'reponseElec_msg_nbResultsTop';

            $nombreElementPapier = $this->getViewState('nombreElementReponsePapier');
            $repeaterPapier = 'enveloppeReponsesPapiers';
            $typePapier = 'ReponsePapier';
            $nombrePageTopPapier = 'nombrePageTop_reponsePapier';
            $numPageTopPapier = 'numPageTop_reponsePapier';
            $selectPapier = 'reponsePapier_msg_nbResultsTop';
        }
        if ('DefaultButtonBottom_reponseElec' == $sender->ID || 'DefaultButtonBottom_reponsePapier' == $sender->ID) {
            $nombreElement = $this->getViewState('nombreElementReponseElec');
            $repeater = 'enveloppeReponsesElectronique';
            $type = 'ReponseElec';
            $nombrePageTop = 'nombrePageBottom_reponseElec';
            $numPageTop = 'numPageBottom_reponseElec';
            $select = 'reponseElec_msg_nbResultsBottom';

            $nombreElementPapier = $this->getViewState('nombreElementReponsePapier');
            $repeaterPapier = 'enveloppeReponsesPapiers';
            $typePapier = 'ReponsePapier';
            $nombrePageTopPapier = 'nombrePageBottom_reponsePapier';
            $numPageTopPapier = 'numPageBottom_reponsePapier';
            $selectPapier = 'reponsePapier_msg_nbResultsBottom';
        }
        $sensTriElec = $this->getViewState('sensTriElec', []);
        $triElec = $this->getViewState('triElec', '');
        $sensTriPapier = $this->getViewState('sensTriPapier', []);
        $triPapier = $this->getViewState('triPapier', '');
        //TODO les autres cas
        if ($repeater) {
            $numPage = $this->$numPageTop->Text;
            if (Atexo_Util::isEntier($numPage)) {
                if ($numPage >= $this->$nombrePageTop->Text) {
                    $numPage = $this->$nombrePageTop->Text;
                } elseif ($numPage <= 0) {
                    $numPage = 1;
                }
                $this->updatePagination($this->$select->getSelectedValue(), $numPage, $type);
                $this->populateData($repeater, $type, $triElec, $sensTriElec);
            } else {
                $this->$nombrePageTop->Text = $this->$repeater->CurrentPageIndex + 1;
                $this->$nombrePageTop->Text = $this->$repeater->CurrentPageIndex + 1;
            }
        }
        //on refait le chargement du repeater papier
        if ($repeaterPapier) {
            $numPagePapier = $this->$numPageTopPapier->Text;
            if (Atexo_Util::isEntier($numPagePapier)) {
                if ($numPagePapier >= $this->$nombrePageTopPapier->Text) {
                    $numPagePapier = $this->$nombrePageTopPapier->Text;
                } elseif ($numPagePapier <= 0) {
                    $numPagePapier = 1;
                }
                $this->updatePagination($this->$selectPapier->getSelectedValue(), $numPagePapier, $typePapier);
                $this->populateData($repeaterPapier, $typePapier, $triPapier, $sensTriPapier);
            } else {
                $this->$nombrePageTopPapier->Text = $this->$repeaterPapier->CurrentPageIndex + 1;
                $this->$nombrePageTopPapier->Text = $this->$repeaterPapier->CurrentPageIndex + 1;
            }
        }
    }

    /*
       * retourne si le mode de la consultation est unique
    */
    public function isModeConsultationUnique()
    {
        $modeUnique = false;
        if ('1' == $this->_consultation->getModeOuvertureReponse()) {
            $modeUnique = true;
        }

        return $modeUnique;
    }

    public function getActionTodoFromValAction($actionVal)
    {
        switch ($actionVal) {
            case '1':// ouvrir en ligne
                return 'openNotSignedReponse';
            case '2': // Ouverture Mixte
                return 'openMixte';
            case '3': // Refuser
                return 'refuserReponse';
            case '5': // Restaurer
                return 'restaurerReponse';
            case '6': // renseigner statut
                return 'renseignerStatut';
            case '10': // Accéder à l'instruction
                return 'accederInstructionReponse';
            case '4': // Accéder à l'instruction
                return 'gererAdmissibilite';
            case '7': // Accéder à l'instruction
                return 'demandeComplement';
            case '8': // Importer l'enveloppe
                return 'ImporterEnveloppe';
            case '9': // Autoriser Ouverture
                return 'AutoriserOuverture';
            case '11': // Télécharger Plis Ouverts
                return 'telechargerPlisOuvert';
            case '12': // Notifier le rejet
                return 'NotifierRejet';
            default:
                return '';
        }
    }

    /*
       * retourne la premiere ligne de la liste des lots ( tous les lots)
       */
    public function insertLigneTousLots($nbreReponseElectronique, $nbreReponsePapier)
    {
        return Prado::localize('TEXT_TOUT_LOTS_CONFONDUS') . ' : ' . Prado::localize('TEXT_ELECTRONIQUE') . " = $nbreReponseElectronique // " . Prado::localize('TEXT_PAPIER') . ' = ' . $nbreReponsePapier;
    }

    /*
    * Permet d'intialiser les valeurs de pagination et le repeater
    */
    public function intialiserRepeater($repeater, $nbResultTop, $pageNumberTop)
    {
        $this->$repeater->DataSource = [];
        $this->$repeater->DataBind();
        $this->$nbResultTop->setSelectedValue(10);
        $this->$pageNumberTop->Text = 1;
        $this->$repeater->setCurrentPageIndex(0);
    }

    /*
    * Permet de retouner le nombre de lots de la consultation
    */
    public function getCountLotConsultation()
    {
        if (($this->_consultation instanceof CommonConsultation) && $this->isConsultationAlloti()) {
            return count($this->_consultation->countCategorielots());
        }
    }

    /*
    * Permet de retouner array avec chaque lot avec le reste a traiter
    */
    public function getResteATraiteParLot($modeOuvertureReponse, &$resteATraiterParLot, $arrayLot, $zeroATraiter, &$uneOffreFermer = null)
    {
        $resteaTraite = 0;
        if ($modeOuvertureReponse == Atexo_Config::getParameter('MODE_OUVERTURE_REPONSE')) {
            $offres = (new Atexo_Consultation_Responses())->retrieveOffreNonAnnuleeByConsRef(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            $offresPapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierNonAnnuleeByConsRef(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            $allOffres = array_merge($offres, $offresPapier);
            foreach ($allOffres as $offre) {
                if (!$offre->isOffreOuvertOuRejete()) {
                    ++$resteaTraite;
                }
            }
        }
        foreach ($arrayLot as $lot) {
            if ($modeOuvertureReponse == Atexo_Config::getParameter('MODE_OUVERTURE_REPONSE') && $resteaTraite) {
                $resteATraiterParLot[$lot] = $resteaTraite;
                $uneOffreFermer = $resteaTraite;
            } else {
                $enveloppesParLot = (new Atexo_Consultation_Responses())->retrieveAllEnveloppes(
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    $lot,
                    $this->getViewState('typesEnveloppes'),
                    Atexo_CurrentUser::getCurrentOrganism()
                );
                if (!is_array($enveloppesParLot)) {
                    throw new Atexo_Exception('Expected funcion to return an array');
                }
                $resteATraiterParLot[$lot] = (new Atexo_Consultation_Responses())->calculZeroRat($enveloppesParLot, $lot, $zeroATraiter, $this->getViewState('typesEnveloppes'));
            }
        }
    }

    /*
    * Permet de retourner le chemin de telechargment de pli selon l'ordre suivant
    * chemin dans la consultation si non chemin dans l'offre si non dans l'enveloppe
    */
    public function getPathTelechargementPlis($organisme, $arrayIdsOffre, $arrayIdsEnveloppe)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $destDir = addslashes($this->_consultation->getPathTelechargementPlis());
        if (!$destDir && is_array($arrayIdsOffre) && 1 == count($arrayIdsOffre)) {
            $idOffre = $arrayIdsOffre[0];
            $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexion);
            if ($offre instanceof CommonOffres) {
                $destDir = addslashes($offre->getRepertoireTelechargementOffre());
                if (!$destDir && is_array($arrayIdsEnveloppe) && 1 == count($arrayIdsEnveloppe)) {
                    $enveloppee = CommonEnveloppePeer::retrieveByPK($arrayIdsEnveloppe[0], $organisme, $connexion);
                    if ($enveloppee instanceof CommonEnveloppe) {
                        $destDir = addslashes($enveloppee->getRepertoireTelechargement());
                    }
                }
            }
        }

        return $destDir;
    }

    public function showPictoMps($statutEnveloppe)
    {
        if (
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
        ) {
            return true;
        }

        return false;
    }

    /**
     * permet de gerer la visibilité des attestations d'un entreprise et l'accès au donnees candidat.
     *
     * @param CommonOffres $offre l'offre
     * @param string       $type  le type d'enveloppe
     *
     * @return bool
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   2.0
     *
     * @since     4.10.0
     *
     * @copyright Atexo 2015
     */
    public function showAttestation($offre, $type)
    {
        if (Atexo_Module::isEnabled('DonneesCandidat')) {
            if (Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
                return true;
            }
            if ($offre instanceof CommonOffres) {
                $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeByIdOffreAndType($offre->getId(), $offre->getOrganisme(), $type);
                if ( ($enveloppe instanceof CommonEnveloppe) &&
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                    || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                    || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
                    || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getIdEntrepriseFromSiren($siren)
    {
        if ($siren) {
            $siren = substr($siren, 0, 9);
            $entreprise = Atexo_Entreprise::retrieveCompanyBdeBySiren($siren);
            if ($entreprise instanceof Entreprise) {
                return $entreprise->getId();
            }
        }

        return false;
    }

    public function getUrlFicheEntreprise($siren, $idOffre, $type)
    {
        if ($siren) {
            $idEntreprise = $this->getIdEntrepriseFromSiren($siren);
            $ats = sha1($idEntreprise . '1');
            if ($idEntreprise) {
                return "javascript:popUpSetSize('index.php?page=Agent.DetailEntreprise&callFrom=agent&idEntreprise=" . $idEntreprise . '&idO=' . $idOffre . '&type=' . $type . '&ats=' . $ats . "','1060px','800px','yes');";
            }
        }

        return 'javascript:void();';
    }

    public function calculJeton($idEntreprise)
    {
        $ats = sha1($idEntreprise . '1');

        return $ats;
    }

    /**
     * @return string
     */
    public function getReferenceConsultation()
    {
        return Atexo_Util::atexoHtmlEntities($_GET['id']);
    }

    /**
     * @return false|string
     */
    public function getOrganisme()
    {
        return Atexo_CurrentUser::getCurrentOrganism();
    }

    /**
     * Permet de recuperer un etablissement.
     *
     * @param string $idEtablissement : identifiant de l'etablissement
     *
     * @return CommonTEtablissement
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version   1.0
     *
     * @since     2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getEtablissement($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();

        return $etablissementQuery->getEtabissementbyId($idEtablissement);
    }

    /**
     * Permet de determiner le type de reponse d'une offre.
     *
     * @param CommonOffres|CommonOffrePapier $offre : offre entreprise
     *
     * @return string
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version   1.0
     *
     * @since     2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getTypeReponse($offre)
    {
        return ($offre instanceof CommonOffres) ? Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') : Atexo_Config::getParameter('DEPOT_PAPIER');
    }

    public function passerAEtatDecision($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultation = CommonConsultationPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), $connexion);
        if ($consultation instanceof CommonConsultation) {
            $listeLot = $consultation->getAllLots();
            foreach ($listeLot as $lot) {
                if ($lot instanceof CommonCategorieLot && $lot->getDecision() != Atexo_Config::getParameter('LOT_EN_DECISION')) {
                    $lot->setDecision('1');
                    $lot->save($connexion);
                }
            }
            $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
            $consultation->setDateDecision(date('Y-m-d'));
            $consultation->setDecisionPartielle(Atexo_Config::getParameter('DECISION_PARTIELLE_NON'));
            $consultation->save($connexion);
            $this->javascriptFinAnalyse->setText('');

            $this->response->redirect('?page=Agent.TableauDeBord&id=' . $consultation->getId());
        }
    }

    public function fermerPopinAdmissibilite($sender, $param)
    {
        $this->javascriptFinAnalyse->setText('');
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param int   $idAgent
     * @param array $params
     *
     * @return Dehiffrement
     */
    public function getDechiffrementCrypto(array $offres, $idAgent, $consultation, $enLigne = true)
    {
        $logger = Atexo_LoggerManager::getLogger('crypto');
        $params = [];
        $params['MpeURLPrivee'] = Atexo_Config::getParameter('MPE_WS_URL');
        $params['ServeurCryptoURLPublic'] = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO'));
        $params['MpeFilePath'] = Atexo_Config::getParameter('BASE_ROOT_DIR') . $consultation->getOrganisme() . Atexo_Config::getParameter('FILES_DIR');
        $params['MpeLogin'] = Atexo_Config::getParameter('MPE_WS_LOGIN_SRV_CRYPTO');
        $params['MpePassword'] = Atexo_Config::getParameter('MPE_WS_PASSWORD_SRV_CRYPTO');
        $params['Format'] = Atexo_Config::getParameter('FORMAT_RETOUR_CRYPTO');
        $params['EnLigne'] = $enLigne;
        $service = new Atexo_Crypto_Service(
            Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')),
            $logger,
            Atexo_Config::getParameter('ENABLE_SIGNATURE_SERVER_V2'),
            null,
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );
        try {
            if ($consultation->getChiffrementOffre() !== '0') {
                return $service->demandeDechiffrement($offres, $idAgent, $consultation, $params);
            }
        } catch (ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement" . $e->errorCode . ' - ' . $e->getEtatObjet() . ' - ' . $e->getMessage());
        }

        return false;
    }

    public function refreshRepeater($sender, $param)
    {
        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
            $json = $this->infoPli->getValue();
            if (!empty($json)) {
                $arrayInfo = \json_decode($json, true);
                $repeater = $arrayInfo['repeaterName'];
                $this->updateInfoAjax($arrayInfo);

                $sensTriElec = $this->getViewState('sensTriElec', []);
                $triElec = $this->getViewState('triElec', '');
                switch ($repeater) {
                    case 'enveloppeCandidatureElectronique':
                        $this->populateData('enveloppeCandidatureElectronique', 'Candidature', $triElec, $sensTriElec);
                        $this->panelEnveloppeCandidature->render($param->NewWriter);
                        break;
                    case 'enveloppeOffreElectronique':
                        $this->populateData('enveloppeOffreElectronique', 'OffreElec', $triElec, $sensTriElec);
                        $this->panelEnveloppeOffre->render($param->NewWriter);
                        break;
                    case 'enveloppeAnonymatEletronique':
                        $this->populateData('enveloppeAnonymatEletronique', 'AnonymatElec', $triElec, $sensTriElec);
                        $this->panelEnveloppeAnonymat->render($param->NewWriter);
                        break;
                    case 'enveloppeReponsesElectronique':
                        $this->populateData('enveloppeReponsesElectronique', 'ReponseElec', $triElec, $sensTriElec);
                        $this->panelReponseElectronique->render($param->NewWriter);
                        break;
                    case 'enveloppeOffreTechniqueElectronique':
                        $this->populateData('enveloppeOffreTechniqueElectronique', 'OffreTechniqueElec', $triElec, $sensTriElec);
                        $this->panelEnveloppeOffreTechnique->render($param->NewWriter);
                        break;
                }
            }
        }
    }

    public function setInfoAjax($nameRepeater, $idOffres, $idEnveloppes)
    {
        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
            $statut = Atexo_Config::getParameter('STATUT_ENV_FEREME');
            $json = $this->infoPli->getValue();
            $arrayInfo = [];
            $arrayInfo['repeaterName'] = $nameRepeater;
            $arrayInfo['enveloppes'] = [];
            $arrayInfo['offres'] = [];
            if (!empty($json)) {
                $arrayInfo = json_decode($json, true);
            }
            if (is_array($idEnveloppes) && count($idEnveloppes)) {
                foreach ($idEnveloppes as $idEnveloppe) {
                    $arrayInfo['enveloppes'][$idEnveloppe] = ['id' => $idEnveloppe, 'statut' => $statut];
                }
            } elseif (is_array($idOffres) && count($idOffres)) {
                foreach ($idOffres as $idOffre) {
                    $arrayInfo['offres'][$idOffre] = ['id' => $idOffre, 'statut' => $statut];
                }
            }
            $this->infoPli->setValue(json_encode($arrayInfo));
        }
    }

    public function updateInfoAjax($arrayInfo)
    {
        $enveloppes = [];
        $offres = [];
        if (!empty($arrayInfo['enveloppes']) && is_array($arrayInfo['enveloppes'])) {
            foreach ($arrayInfo['enveloppes'] as $enveloppe) {
                if ($enveloppe['statut'] != Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')) {
                    $enveloppes[$enveloppe['id']] = $enveloppe;
                }
            }
            $arrayInfo['enveloppes'] = $enveloppes;
        } elseif (!empty($arrayInfo['offres']) && is_array($arrayInfo['offres'])) {
            foreach ($arrayInfo['offres'] as $offre) {
                if ($offre['statut'] != Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')) {
                    $offres[$offre['id']] = $offre;
                }
            }
            $arrayInfo['offres'] = $offres;
        }
        $json = '';
        if (!empty($arrayInfo['enveloppes']) || !empty($arrayInfo['offres'])) {
            $json = json_encode($arrayInfo);
        }
        $this->infoPli->setValue($json);
    }

    public function getLibelleStatutOffre($offre)
    {
        $libelle = '';
        if ($offre instanceof CommonOffres) {
            if ($offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')) {
                $libelle = '- ' . Prado::localize('DEFINE_EN_COURS_CHIFFREMENT_DEPOT_RECENT') . ' -';
            } elseif ($offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')) {
                $libelle = '- ' . Prado::localize('DEFINE_EN_ATTENTE_CHIFFREMENT_DEPOT_RECENT') . ' -';
            } else {
                $libelle = '- ' . Prado::localize('CHIFFRE') . ' -';
            }
        }

        return $libelle;
    }

    public function showDume($objEnveloppeouOffre)
    {
        if (Atexo_Module::isEnabled('InterfaceDume') && '1' == $this->_consultation->getDumeDemande()) {
            $offre = false;
            if ($objEnveloppeouOffre instanceof CommonOffres) {//cas d'une ouverture par reponse
                $offre = $objEnveloppeouOffre;
            }
            if ($objEnveloppeouOffre instanceof CommonEnveloppe && $objEnveloppeouOffre->getCommonOffres() instanceof CommonOffres) {//cas d'une ouverture par dossier
                $offre = $objEnveloppeouOffre->getCommonOffres();
            }
            if ($offre instanceof CommonOffres) {
                $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);
                if ($candidature instanceof CommonTCandidature) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isOuvert($statutEnveloppe)
    {
        if (
            $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
            || $statutEnveloppe == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Cette methode permet de recuperer l'url MPS
     * si le param "PF_MPS_EXTERNE" est activé le lien redirige vers une pf externe sinn il pertmet de telecharger la candidatureMps.
     *
     * @param CommonOffres    $objEnveloppe objet de type CommonOffres,cas d'une ouverture par reponse
     * @param CommonEnveloppe $objEnveloppe objet de type CommonEnveloppe,cas d'une ouverture par dossier
     *
     * @return string $urlMPS lienMps
     *
     * @author    ASO <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlDume($objEnveloppeouOffre)
    {
        $offre = null;
        $urlDume = '#';
        $lot = 0;
        if ($objEnveloppeouOffre instanceof CommonOffres) {//cas d'une ouverture par reponse
            $offre = $objEnveloppeouOffre;
        }
        if ($objEnveloppeouOffre instanceof CommonEnveloppe) {//cas d'une ouverture par dossier
            $offre = $objEnveloppeouOffre->getCommonOffres();
            $lot = $objEnveloppeouOffre->getSousPli();
        }
        if ($offre instanceof CommonOffres) {
            $urlDume = '?page=Agent.DownloadCandidatureDume&idOffre=' . base64_encode($offre->getId()) . '&id=' . base64_encode($this->_consultation->getId());
        }

        return $urlDume;
    }

    /**
     * Permet d'afficher la liste des fichiers pour les quels le hash est ko.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author    loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     2017-place
     *
     * @copyright Atexo 2018
     */
    public function actionListeFichiersHashKo($sender, $param)
    {
        if ($param->CallbackParameter) {
            $arrayParam = explode('#', $param->CallbackParameter);
            $idOffre = $arrayParam[0];
            $idenveloppe = $arrayParam[1];
            if ($idOffre) {
                $files = (new Atexo_Entreprise_Reponses())->retrieveFichierEnveloppeHashKo($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $idenveloppe);
                $this->displayFilesHashKo($files);
            }
        }
    }

    /**
     * Permet d'afficher la liste des fichiers.
     *
     * @param array $files
     *
     * @return void
     *
     * @author    loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     2017-place
     *
     * @copyright Atexo 2018
     */
    public function displayFilesHashKo($files)
    {
        if (is_array($files)) {
            $this->repeaterFilesHashKo->DataSource = $files;
            $this->repeaterFilesHashKo->DataBind();
        }
        $this->javascript->Text = "<script>openModal('modal_detail_hash_files','popup-800',document.getElementById('ctl0_CONTENU_PAGE_titrePopinDetailFiles'));</script>";
    }

    public function doDownloadPlis()
    {
        $arrayInfo = $this->getViewState('idsForDownloadPli');
        $res = (new Atexo_Consultation_Responses())->prepereJsonForDownloadPlis($arrayInfo['data'], $arrayInfo['type'], Atexo_CurrentUser::getCurrentOrganism());

        $serviceMessageBus = Atexo_Util::getSfService('app.publish.messageBus');
        $serviceMessageBus->publishGenerationArchiveDocumentaire(
            $this->_consultation->getId(),
            Atexo_CurrentUser::getId(),
            base64_encode(json_encode($res)),
            1,
            Atexo_Config::getParameter('RETENTION_ZIP_PLIS')
        );
    }

    public function annulerDownloadPlis()
    {
        $this->setViewState('idsForDownloadPli', null);
    }

    public function isPassedDLRO(): bool
    {
        return $this->_consultation->getDatefin() < date('Y-m-d H:i:s');
    }

    private function getActionRejete($actionsPossibles)
    {
        $idRef = $_GET['id'];
        $consultationQuery = new CommonConsultationQuery();
        $consultation = $consultationQuery->findOneBy('id', $idRef);
        if (2 === $consultation->getVersionMessagerie()) {
            $actionsPossibles[Atexo_Config::getParameter('NOTIFIER_REJET')] = Prado::localize('NOTIFIER_REJET');
        }

        return $actionsPossibles;
    }

    /**
     * @param string $idDossierVolumineux
     * @return string
     */
    public function getJsonDossierVolumineux(string $idDossierVolumineux) : string
    {
        $idDossierVolumineux = (Atexo_Util::getSfService(Encryption::class))->decryptId($idDossierVolumineux);
        $serviceDossierVolumineuxService = Atexo_Util::getSfService('atexo.dossier_volumineux_service');
        $serviceUtil = Atexo_Util::getSfService('atexo.atexo_util');
        $dossierVolumineux = $serviceDossierVolumineuxService->getDossierVolumineuxById($idDossierVolumineux);
        $paramsDV = [
            'reference' => $dossierVolumineux->getUuidReference(),
            'nomDossierVolumineux' => $dossierVolumineux->getNom(),
            'serverLogin' => Atexo_Config::getParameter('LOGIN_TIERS_TUS'),
            'serverPassword' => Atexo_Config::getParameter('PWD_TIERS_TUS'),
            'serverURL' => $serviceUtil->getPfUrlDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE')),
            'statut' => $dossierVolumineux->isActif() ? '1' : '0',
        ];
        $callback = $serviceDossierVolumineuxService->callTusSession($paramsDV, $dossierVolumineux->getUuidTechnique());
        return json_encode([
            "sessionId" => $callback['uuid'],
            "appId" =>  self::APPID,
            "urlEnvol" => Atexo_Config::getParameter('ENVOL_URL')
        ]);
    }

    public function crypteIdDossierVolumineux($idDossierVolumineux) {
        return  (Atexo_Util::getSfService(Encryption::class))->cryptId($idDossierVolumineux);
    }
}
