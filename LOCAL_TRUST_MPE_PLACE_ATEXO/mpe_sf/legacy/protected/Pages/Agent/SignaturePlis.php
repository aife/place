<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SignaturePlis extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public bool $_enveloppeOuverte = false;
    public $_avecSignature;
    public ?string $_typeSignature = null;
    public ?string $_enveloppeCrypte = null;

    public function onLoad($param)
    {
        $message = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        $this->errorPart->setVisible(false);
        $this->mainPart->setVisible(true);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            $this->_avecSignature = $consultation->getSignatureOffre();
            $this->consultationSummury->setConsultation($consultation);
            $this->detailPli->setConsultation($consultation);
            $this->detailPli->setIdPli(Atexo_Util::atexoHtmlEntities($_GET['idPli']));
            $this->detailPli->setTypeEnv(Atexo_Util::atexoHtmlEntities($_GET['type']));

            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById(Atexo_Util::atexoHtmlEntities($_GET['idPli']), Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());

            if ($enveloppe instanceof CommonEnveloppe && !($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME') ||
                                                            $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') ||
                                                            $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'))) {
                $this->_enveloppeCrypte = $enveloppe->getCryptage();
                $offre = $enveloppe->getOffre($connexionCom);

                if ($offre instanceof CommonOffres && $offre->getConsultationId() == $_GET['id']) {
                    $xmlStr = $offre->getXmlString();
                    $pos = strpos($xmlStr, 'typeSignature');
                    if ($pos > 0) {
                        $this->_typeSignature = 'XML';
                    }

                    $this->_enveloppeOuverte = ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'));
                    if ($enveloppe instanceof CommonEnveloppe) {
                        //if($enveloppe->getCryptage()=='0') {
                        $c = new Criteria();
                        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, Atexo_Util::atexoHtmlEntities($_GET['idPli']));
                        $c->add(CommonFichierEnveloppePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
                        $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexionCom);

                        if ($fichiers && is_array($fichiers)) {
                            /*
                             *Ancien traitement signature avec erreur.
                             foreach($fichiers as $fichier) {
                             if(!$fichier->getSignatureFichier()) {
                             $message=" Il n'est pas possible de vérifier la signature"
                             .(count($fichiers)? " de cette enveloppe " :" d'un des fichiers de cette enveloppe ")
                             ."car la signature n'existe pas";
                             }
                             }*/
                            $arrayInfos = (new Atexo_Consultation_Responses())->getInfosCertif($fichiers, $this->_avecSignature, $this->_typeSignature, $this->_enveloppeCrypte, 'agent');
                            $this->setViewState('infosFichierSoumis', $arrayInfos);
                            $this->repeaterFichiers->DataSource = $fichiers;
                            $this->repeaterFichiers->DataBind();
                        }
                        //} else {

                        //$message="Il n'est pas possible de vérifier la signature de cette enveloppe car l'enveloppe est toujours chiffrée";
                        //}
                    }
                }
            } else {
                $message = Prado::localize('ERREUR_DROIT_DETAIL_SIGNATURE');
            }
        }

        if ($message) {
            $this->errorPart->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);
        }
    }

    public function exportRapportSignaturePdf($toDisk = false, $path = false)
    {
        $arrayInfosCons = $this->getViewState('infoRapportPdfCons');
        $arrayInfosPli = $this->getViewState('infoRapportPdfPli');
        $arrayInfosFichierSoumis = $this->getViewState('infosFichierSoumis');
        (new Atexo_Consultation_Responses())->printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis);
    }
}
