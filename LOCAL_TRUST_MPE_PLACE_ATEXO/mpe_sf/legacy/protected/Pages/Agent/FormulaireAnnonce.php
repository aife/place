<?php

namespace Application\Pages\Agent;

use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInterneConsultation;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LieuxExecution;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Traduction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_NumerotationRefCons;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;

class FormulaireAnnonce extends MpeTPage
{
    public bool $_creation = true;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        // creation ou modification
        if (!isset($_GET['id']) ||
            (
                isset($_GET['newAttribution']) ||
                isset($_GET['newExtraitPv']) ||
                isset($_GET['newRapportAchevement']) ||
                isset($_GET['newDecisionResiliation'])
            )
        ) {
            $this->_creation = true;
        } else {
            $this->_creation = false;
        }
        // Affichage code NUTS
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $atexoCodesNutsRef = new Atexo_Ref();
            $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
            $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
        }
        if (!$this->IsPostBack) {
            $this->referentielCPV->onInitComposant();
            if (isset($_GET['id'])
                && (
                    (Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')
                        || isset($_GET['newAttribution']))
                     || (Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')
                        || isset($_GET['newExtraitPv']))
                     || (Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')
                        || isset($_GET['newRapportAchevement']))
                     || (Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')
                        || isset($_GET['newDecisionResiliation']))
                )
                    ) {
                if (!Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation')
                    && !Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')
                    && !Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')
                    && !Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')
                    && !Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')
                  ) {
                    $message = Prado::localize('DEFINE_MESSAGE_MODIFICATION_ANNONCE_NON_HABILITE');
                    if (isset($_GET['newAttribution'])) {
                        $message = Prado::Localize('TEXT_NOT_HABILITE_ANNONCE_ATTRIBUTION');
                    }
                    if (isset($_GET['newExtraitPv'])) {
                        $message = Prado::Localize('TEXT_NOT_HABILITE_CREER_EXTRAIT_PV');
                    }
                    if (isset($_GET['newRapportAchevement'])) {
                        $message = Prado::Localize('TEXT_NOT_HABILITE_CREER_RAPPORT_ACHEVEMENT');
                    }
                    if (isset($_GET['newDecisionResiliation'])) {
                        $message = Prado::Localize('TEXT_NOT_HABILITE_CREER_DECISION_RESILIATION');
                    }
                    self::afficherPanelErreur($message);
                }
                $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
                $this->panelMessage->setVisible(false);
                $this->displayCreation();
                $criteriaVo = new Atexo_Consultation_CriteriaVo();
                $criteriaVo->setIdReference($consultationId);
                $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
                $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);
                if (1 == count($arrayConsultation)) {
                    $consultation = array_shift($arrayConsultation);
                    $this->setViewState('consultation', $consultation);

                    $this->procedureType->selectedValue = $consultation->getIdTypeProcedureOrg();
                    $this->reference->Text = $consultation->getReferenceUtilisateur();
                    $this->categorie->selectedValue = $consultation->getCategorie();
                    $this->intituleConsultation->Text = $consultation->getIntitule();
                    $this->objet->Text = $consultation->getObjetTraduit();
                    $this->commentaire->Text = $consultation->getChampSuppInvisible();
                    if (Atexo_Module::isEnabled('LieuxExecution')) {
                        $this->displayGeoN2InModification($consultation->getLieuExecution());
                    }
                    if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                        $this->referentielCPV->chargerReferentielCpv($consultation->getCodeCpv1(), $consultation->getCodeCpv2());
                    }
                    //Affichage codes nuts Lt-Ref
                    if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                        $atexoCodesNutsRef = new Atexo_Ref();
                        $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                        $atexoCodesNutsRef->setSecondaires($consultation->getCodesNuts());
                        $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                    }
                    if (!Atexo_Module::isEnabled('CreerAutreAnnonce')) {
                        $this->intituleAvis->setEnabled(false);
                        $this->procedureType->setEnabled(false);
                        $this->reference->setEnabled(false);
                        $this->categorie->setEnabled(false);
                        $this->intituleConsultation->setEnabled(false);
                        $this->objet->setEnabled(false);
                        $this->commentaire->setEnabled(false);
                        $this->dateFinAffichage->text = date('d/m/').(date('Y') + 2).date(' H:i');
                        $this->dateFinAffichages->style = 'display:none';
                    }

                    $this->ltrefCons->afficherReferentielConsultation(
                        $consultation->getId(),
                        0,
                        Atexo_CurrentUser::getOrganismAcronym()
                    );
                    $this->idReferentielZoneText->afficherTextConsultation(
                        $consultation->getId(),
                        0,
                        Atexo_CurrentUser::getOrganismAcronym()
                    );

                    if (Atexo_Module::isEnabled('NumerotationRefCons')) {
                        $this->reference->setEnabled(false);
                    }
                    if (isset($_GET['newAttribution'])) {
                        $this->intituleAvis->selectedValue = Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION');
                    } elseif (isset($_GET['newExtraitPv'])) {
                        $this->intituleAvis->selectedValue = Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV');
                    } elseif (isset($_GET['newRapportAchevement'])) {
                        $this->intituleAvis->selectedValue = Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT');
                    } elseif (isset($_GET['newDecisionResiliation'])) {
                        $this->intituleAvis->selectedValue = Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION');
                    } else {
                        $this->procedureType->setEnabled(false);
                        $message = Prado::localize('DEFINE_MESSAGE_MODIFICATION_ANNONCE_NON_HABILITE');
                        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')) {
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        } elseif ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')) {
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        } elseif ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')) {
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        } elseif ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')) {
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        } elseif ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation')) {
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        } elseif ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT')) {
                            if (Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation') || Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')) {
                                $this->procedureType->setEnabled(true);
                                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
                            } else {
                                self::afficherPanelErreur($message);
                            }
                        }

                        $this->dateFinAffichage->Text = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
                        //Affichage de la date de mise en ligne souhaitée
                        if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
                            $this->dateMiseEnLigneSouhaitee->Text = Atexo_Util::iso2frnDate($consultation->getDateMiseEnLigneSouhaitee());
                        }
                        $this->detailAnnonce->Text = $consultation->getDetailConsultation();
                    }
                } else {
                    if (isset($_GET['newAttribution']) || isset($_GET['newExtraitPv']) || isset($_GET['newRapportAchevement']) || isset($_GET['newDecisionResiliation'])) {
                        //TODO à remplacer par Prado::localize('DEFINE_CONSULTATION_ORIGINE_NON_TROUVE'), en attente du feu vert de chartrin pour pouvoir commiter message
                        $message = Prado::localize('TEXT_CONSULTATION_ORIGINE_NON_TROUVE');
                    } else {
                        $message = Prado::localize('DEFINE_MESSAGE_MODIFICATION_ANNONCE_NON_HABILITE');
                    }
                    self::afficherPanelErreur($message);
                }
            } else {
                //Affichage des préferences des lieux d'éxecution de l'agent connecté
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
                if (Atexo_Module::isEnabled('LieuxExecution')) {
                    $this->displayGeoN2InModification($agent->getLieuExecution());
                } elseif (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                    $atexoCodesNutsRef = new Atexo_Ref();
                    $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                    if ($agent->getCodesNuts()) {
                        $atexoCodesNutsRef->setSecondaires($agent->getCodesNuts());
                    }
                    $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                }

                if (!Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation')
                &&
                    !Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution')
                &&
                    !Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv')
                &&
                    !Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')
                &&
                    !Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')
                ) {
                    self::afficherPanelErreur(Prado::localize('DEFINE_MESSAGE_CREATION_ANNONCE_NON_HABILITE'));
                }

                if ((Atexo_Module::isEnabled('NumerotationRefCons'))) {
                    //Reference Utilisateur par défault
                    $this->reference->Text = prado::localize('TEXT_GENEREE_ENREGISTREMENT');
                    $this->reference->setEnabled(false);
                }

                $this->panelMessage->setVisible(false);
                $this->displayCreation();
                $time = mktime(date('H'), date('i'), date('s'), date('m'), date('d') + Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE_VALEUR2'), date('Y'));
                $this->dateFinAffichage->Text = date('d/m/Y H:i', $time);
                $this->ltrefCons->afficherReferentielConsultation(null, 0, Atexo_CurrentUser::getOrganismAcronym());
                $this->idReferentielZoneText->afficherTextConsultation(null, 0, Atexo_CurrentUser::getOrganismAcronym());
            }
        }
    }

    /**
     * Permet de charger les lieux d'exécution de la consultation au bonn format pour la carte.
     */
    public function getNumSelectedGeoN2($buffer)
    {
        $text = '';
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];

        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }

        $text = implode('_', $new_arrayId);

        return $text;
    }

    /**
     * Raffraichit le bloc des lieux d'executions.
     */
    public function displayGeoN2InModification($lieuExecution)
    {
        $this->idsSelectedGeoN2->Value = $lieuExecution;
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
    }

    /**
     * affiche les dénominations des lieux d'éxecutions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
    }

    /**
     * affiche les dénominations des lieux d'éxecutions.
     */
    public function refreshDisplaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->Page->lieuExecution->render($param->getNewWriter());

        $this->getLieuExecutionUrl();
    }

    public function getSelectedGeoN2($ids)
    {
        $lieuxExecutionsType1 = null;
        $text = '';
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        if ($list) {
            foreach ($list as $oneGeoN2) {
                $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1Traduit().', ';
            }
            if ($lieuxExecutionsType1) {
                $text = substr($lieuxExecutionsType1, 0, -2);
            }
        }

        return $text;
    }

    /**
     * dsplayCreation remplie la page de création (liste des types d'annonce, types procedure, catégorie.
     */
    public function displayCreation()
    {
        $dataAvis = [];
        $dataProcedures = [];
        $Categories = [];
        //Debut remplissage de la liste Types avis
        $avisTypes = Atexo_Consultation_TypeAvis::retrieveAvisTypes(true, Atexo_CurrentUser::readFromSession('lang'));

        if ($avisTypes) {
            $dataAvis[' '] = '---'.Prado::localize('DEFINE_TOUS_ANNOCES').'---';
            foreach ($avisTypes as $avis) {
                if (($avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION') &&
                    Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation'))
                ) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                } elseif (($avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION') &&
                    Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution'))
                ) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                } elseif (($avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV')
                        && Atexo_Module::isEnabled('AutreAnnonceExtraitPv')
                        && Atexo_CurrentUser::hasHabilitation('CreerAnnonceExtraitPv'))) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                }
                if ($avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT')
                        && Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')
                        && Atexo_CurrentUser::hasHabilitation('CreerAnnonceRapportAchevement')) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                }
                if ($avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION')
                        && Atexo_Module::isEnabled('AutreAnnonceDecisionResiliation')
                        && Atexo_CurrentUser::hasHabilitation('CreerAnnonceDecisionResiliation')) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                }
                if ((
                    $avis->getId() == Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT')
                            && (Atexo_CurrentUser::hasHabilitation('CreerAnnonceInformation')
                                    || Atexo_CurrentUser::hasHabilitation('CreerAnnonceAttribution'))
                )) {
                    $dataAvis[$avis->getId()] = $avis->getIntituleAvisTraduit();
                }
            }
        }
        $this->intituleAvis->Datasource = $dataAvis;
        $this->intituleAvis->dataBind();
        //Fin remplissage de la liste Types avis

        //Debut remplissage de la liste Types procedure
        if (Atexo_Config::getParameter('ACTIVE_HABILITATION_V2')) {
            $proceduresTypes = Atexo_Util::getSfService(TypeProcedureOrganismeRepository::class)
                ->getNewTypeProcedureOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $libelleMethod = 'getLibelleTypeProcedure';
        } else {
            $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureType();
            $libelleMethod = 'getLibeleTypeProcedureTraduit';
        }
        if ($proceduresTypes) {
            $dataProcedures[' '] = '---'.Prado::localize('DEFAULT_PROCEDURE_TYPE_SELECTED').'---';
            if ($_GET['id']) {
                foreach ($proceduresTypes as $procedureType) {
                    $dataProcedures[$procedureType->getIdTypeProcedure()] = $procedureType->{$libelleMethod}();
                }
            } else {
                foreach ($proceduresTypes as $procedureType) {
                    if (Atexo_CurrentUser::hasHabilitation('GererMapaInferieurMontant') &&
                        1 == $procedureType->getMapa() && 1 == $procedureType->getActiverMapa() &&
                        $procedureType->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')
                    ) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = $procedureType->{$libelleMethod}();
                    }
                    if (Atexo_CurrentUser::hasHabilitation('GererMapaSuperieurMontant') &&
                        1 == $procedureType->getMapa() && 1 == $procedureType->getActiverMapa() &&
                        $procedureType->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = $procedureType->{$libelleMethod}();
                    }
                    if (Atexo_CurrentUser::hasHabilitation('AdministrerProceduresFormalisees')
                             && 0 == $procedureType->getMapa()) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = $procedureType->{$libelleMethod}();
                    }
                }
            }
        }
        $this->procedureType->Datasource = $dataProcedures;
        $this->procedureType->dataBind();
        //Fin remplissage de la liste Types procedure

        //Debut remplissage de la liste Categorie
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories(
            $categories,
            Atexo_CurrentUser::readFromSession('lang')
        );
        $Categories[' '] = '---'.Prado::localize('DEFAULT_CATEGORY_SELECTED').'---';
        foreach ($dataCategories as $key => $Value) {
            if (0 != $key) {
                $Categories[$key] = $Value;
            }
        }
        $this->categorie->Datasource = $Categories;
        $this->categorie->dataBind();
        //Fin remplissage de la liste Categorie
    }

    public function createAnnonce()
    {
        $annonceConsultation = null;
        //commencer le scan Antivirus
        if ($this->ajoutFichier->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                return;
            }
        }
        //Fin du code scan Antivirus

        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        $connexionCom->beginTransaction();

        $organisme = Atexo_CurrentUser::getOrganismAcronym();

        if (isset($_GET['id']) && !isset($_GET['newAttribution']) &&
            !isset($_GET['newExtraitPv']) &&
            !isset($_GET['newRapportAchevement']) &&
            !isset($_GET['newDecisionResiliation'])
        ) { // mode modification d'une annonce
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $commonConsultationO = CommonConsultationPeer::retrieveByPK($consultationId, $connexionCom);
        } else {
            $commonConsultationO = new CommonConsultation();
            $commonConsultationO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $commonConsultationO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
            $commonConsultationO->setServiceAssocieId(Atexo_CurrentUser::getCurrentServiceId());
        }

        if ($this->_creation && isset($_GET['id'])) {
            $annonceConsultation = $this->getViewState('consultation');
            $commonConsultationO->setTitre($annonceConsultation->getTitre());
            $commonConsultationO->setResume($annonceConsultation->getResume());
            $languesActive = Atexo_Languages::retrieveActiveLangages();
            $idTraduction = 0;

            foreach ($languesActive as $langue) {
                $idTraduction = (new Atexo_Traduction())->
                setTraduction(
                    $idTraduction,
                    $langue->getLangue(),
                    Atexo_Traduction::getTraduction(
                        $annonceConsultation->getIdTrObjet(),
                        $langue->getLangue()
                    )
                );
                $commonConsultationO->setIdTrObjet($idTraduction);
                $getResumeTraduit = 'getObjet'.ucfirst($langue->getLangue());
                $getTitreTraduit = 'getIntitule'.ucfirst($langue->getLangue());
                $getAccessibiliteTraduit = 'getAccessibilite'.ucfirst($langue->getLangue());
                $setResumeTraduit = 'setObjet'.ucfirst($langue->getLangue());
                $setTitreTraduit = 'setIntitule'.ucfirst($langue->getLangue());
                $setAccessibiliteTraduit = 'setAccessibilite'.ucfirst($langue->getLangue());
                $commonConsultationO->$setTitreTraduit($annonceConsultation->$getTitreTraduit());
                $commonConsultationO->$setResumeTraduit($annonceConsultation->$getResumeTraduit());
                $commonConsultationO->$setAccessibiliteTraduit($annonceConsultation->$getAccessibiliteTraduit());
            }
        }

        $commonConsultationO->setIdCreateur(Atexo_CurrentUser::getIdAgentConnected());
        $commonConsultationO->setNomCreateur(Atexo_CurrentUser::getFirstNameAgentConnected());
        $commonConsultationO->setPrenomCreateur(Atexo_CurrentUser::getLastNameAgentConnected());

        if (Atexo_Module::isEnabled('NumerotationRefCons') && !isset($_GET['id'])) {
            $UserRef = Atexo_Consultation_NumerotationRefCons::generateUserRef($connexionCom);
            $commonConsultationO->setReferenceUtilisateur($UserRef);
        } else {
            $commonConsultationO->setReferenceUtilisateur($this->reference->Text);
        }

        $commonConsultationO->setIdTypeAvis($this->intituleAvis->getSelectedValue());

        $commonConsultationO->setIdTypeProcedureOrg($this->procedureType->getSelectedValue());
        $commonConsultationO->setIdTypeProcedure($this->procedureType->getSelectedValue());
        $idTypeProcedurePortail = Atexo_Consultation_ProcedureType::getIdTypeProcedurePortail(
            $this->procedureType->getSelectedValue(),
            Atexo_CurrentUser::getOrganismAcronym()
        );
        if ($idTypeProcedurePortail) {
            $commonConsultationO->setIdTypeProcedure($idTypeProcedurePortail);
        } else {
            $commonConsultationO->setIdTypeProcedure($this->procedureType->getSelectedValue());
        }

        $commonConsultationO->setCategorie($this->categorie->getSelectedValue());

        //Enregistrement dans la langue de navigation
        $resume = 'setObjet'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
        $titre = 'setIntitule'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
        $accessibilite = 'setAccessibilite'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));

        $commonConsultationO->$resume($this->objet->Text);

        $commonConsultationO->$titre($this->intituleConsultation->Text);
        //Si la langue de navigation est une langue obligatoire elle est automatiquement activée
        if ((new Atexo_Languages())->estObligatoire(Atexo_CurrentUser::readFromSession('lang'))) {
            $commonConsultationO->$accessibilite('1');
        }
        //Objet de l'annonce
        $commonConsultationO->setObjetTraduit(
            strtolower(Atexo_CurrentUser::readFromSession('lang')),
            (new Atexo_Config())->toPfEncoding($this->objet->Text)
        );

        //Intitulé de l'annonce
        $commonConsultationO->setIntituleTraduit(
            strtolower(Atexo_CurrentUser::readFromSession('lang')),
            (new Atexo_Config())->toPfEncoding($this->intituleConsultation->Text)
        );

        $commonConsultationO->setObjet($this->objet->Text);

        $commonConsultationO->setIntitule($this->intituleConsultation->Text);

        $commonConsultationO->setChampSuppInvisible($this->commentaire->Text);
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            $commonConsultationO->setLieuExecution($this->idsSelectedGeoN2->Value);
        }
        //Enregistrement codes nuts
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $codesSec = $this->idAtexoRefNuts->codesRefSec->value;
            if ('' != $codesSec) {
                $commonConsultationO->setCodesNuts($codesSec);
            } else {
                $commonConsultationO->setCodesNuts(null);
            }
        }
        $commonConsultationO->setDetailConsultation($this->detailAnnonce->Text);

        $commonConsultationO->setServiceValidation(Atexo_CurrentUser::getCurrentServiceId());
        $commonConsultationO->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1'));

        $commonConsultationO->setTypeAcces(1);
        if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
            $commonConsultationO->setRegleMiseEnLigne(
                Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ENTITE_COORDINATRICE')
            );
        } else {
            $commonConsultationO->setRegleMiseEnLigne(
                Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE')
            );
        }

        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            $codePrincipal = $this->referentielCPV->getCpvPrincipale();
            if ('' != $codePrincipal) {
                $commonConsultationO->setCodeCpv1($codePrincipal);
            } else {
                $commonConsultationO->setCodeCpv1(null);
            }
            $codesSec = $this->referentielCPV->getCpvSecondaires();
            if ('' != $codesSec) {
                $commonConsultationO->setCodeCpv2($codesSec);
            } else {
                $commonConsultationO->setCodeCpv2(null);
            }
        }

        if ($this->_creation && isset($_GET['id'])) {
            $commonConsultationO->setPoursuivreAffichage($annonceConsultation->getPoursuivreAffichage());
            $commonConsultationO->setPoursuivreAffichageUnite($annonceConsultation->getPoursuivreAffichageUnite());
        }

        $dateFin = Atexo_Util::frnDateTime2iso($this->dateFinAffichage->Text);
        $commonConsultationO->setDatefin($dateFin);
        $commonConsultationO->setDateFinAffichage($dateFin);
        $commonConsultationO->setDateFinUnix(
            strtotime(
                $commonConsultationO->getDatefin().' + '.
                $commonConsultationO->getPoursuivreAffichage().
                ' '.$commonConsultationO->getPoursuivreAffichageUnite()
            )
        );
        //Stockage de la date de mise en ligne souhaitée
        if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
            $commonConsultationO->setDateMiseEnLigneSouhaitee(
                Atexo_Util::frnDate2iso($this->dateMiseEnLigneSouhaitee->Text)
            );
        }

        $commonConsultationO->setAlloti('0');

        $maxNumeroPhase = (new Atexo_Consultation())->retrieveMaxNumeroPhaseByReferenceUtilisateur(
            $commonConsultationO->getReferenceUtilisateur(),
            Atexo_CurrentUser::getOrganismAcronym()
        );
        ++$maxNumeroPhase;

        $commonConsultationO->setNumeroPhase($maxNumeroPhase);
        $commonConsultationO->setEtatEnAttenteValidation(1);

        try {
            $denominationOrg = (new Atexo_Consultation())
                ->getOrganismeDenominationByService(
                    Atexo_CurrentUser::getOrganismAcronym(),
                    Atexo_CurrentUser::getCurrentServiceId()
                );
            $commonConsultationO->setOrgDenomination($denominationOrg);
            $commonConsultationO->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($denominationOrg)));
            $commonConsultationO->setUuid((new Atexo_MpeSf())->uuid());
            $commonConsultationO->save($connexionCom);
            $consultationId = $commonConsultationO->getId();

            if (!Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) {
                $interneConsultation = new CommonInterneConsultation();
                $interneConsultation->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $interneConsultation->setInterneId(Atexo_CurrentUser::getIdAgentConnected());
                $interneConsultation->setConsultationId($consultationId);
                $interneConsultation->save($connexionCom);
            }

            if ($this->_creation) {
                if ($this->ajoutFichier->HasFile) {
                    $this->ajouterPj($consultationId, $organisme);
                }
            }
            $this->ltrefCons->saveReferentielConsultationAndLot($consultationId, 0);
            $this->idReferentielZoneText->saveConsultationAndLot($consultationId, 0);

            $connexionCom->commit();
        } catch (Exception $e) {
            $connexionCom->rollback();
            throw $e;
        }
        $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$commonConsultationO->getIdTypeAvis().'&id='.$commonConsultationO->getId().'&creationSucces');
    }

    public function ajouterPj($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $infile = Atexo_Config::getParameter('COMMON_TMP').'pieceJointe'.session_id().time();
        if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);

            $avis = new CommonAVIS();
            $avis->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $avis->setConsultationId($consultationId);
            $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_VIDE'));

            if (is_array($arrayTimeStampAvis)) {
                $avis->setHorodatage($arrayTimeStampAvis['horodatage']);
                $avis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
            }
            $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, $organisme);
            $avis->setAgentId(Atexo_CurrentUser::getId());
            $avis->setAvis($avisIdBlob);
            $avis->setNomFichier($this->ajoutFichier->FileName);
            $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            $avis->setDatePub(date('Y-m-d'));
            $avis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            $avis->save($connexionCom);
        }
    }

    /**
     * Permet de retourner l'url du lieu d'exécution.
     */
    public function getLieuExecutionUrl()
    {
        $param = '';
        if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
            $param = $this->numSelectedGeoN2->Value;
        }

        return Atexo_LieuxExecution::getLieuExecutionUrl($param);
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    /**
     *affiche un msg en cas d'erreur.
     */
    public function afficherPanelErreur($message)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($message);
        $this->formulaireAnnonce->setVisible(false);

        return true;
    }
}
