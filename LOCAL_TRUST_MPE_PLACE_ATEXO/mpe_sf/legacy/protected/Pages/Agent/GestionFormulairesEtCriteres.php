<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/*
 * Created on 4 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class GestionFormulairesEtCriteres extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->AtexoFormulaire->setCalledFrom('agent');
        $this->AtexoCriteres->setCalledFrom('agent');
        $refCons = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $consultation = $this->retrieveConsultation($refCons);
        if ($consultation instanceof CommonConsultation) {
            $this->IdConsultationSummary->setConsultation($consultation);
        }
    }

    public function retrieveConsultation($consultaionId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultaionId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function redirectToTableauDeBord()
    {
        $refCons = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $consultation = $this->retrieveConsultation($refCons);
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.$refCons);
        } else {
            $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.$refCons);
        }
    }
}
