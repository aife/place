<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

/*
 * Created on 14 sept. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */

class PopupEsender extends MpeTPage
{
    private string $_isFluxInit = '1';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->resourceFormulaire->Text = Atexo_Util::atexoHtmlEntities($_GET['resourceFormulaire']);
            $this->isFluxBase64->Text = '1';
            $this->resourceLang->Text = strtoupper(Atexo_CurrentUser::readFromSession('lang'));
            $this->jetonSession->Text = Atexo_Util::atexoHtmlEntities($_GET['itemIndex']);
            $this->fluxXmlBase64->Text = self::getXmlAvisOpoce(Atexo_Util::atexoHtmlEntities($_GET['idDest']), Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->isFluxInit->Text = $this->_isFluxInit;
            $this->isConsultation->Text = Atexo_Util::atexoHtmlEntities($_GET['isConsultation']);
            $this->login->Text = Atexo_Config::getParameter('USERNAME_TED');
            $this->password->Text = Atexo_Config::getParameter('PASS_TED');

            $this->script->Text = '<script language="javascript">document.getElementById(\'ctl0_CONTENU_PAGE_buttonSend\').click();</script>';
        }
    }

    public function getXmlAvisOpoce($idDestinataire, $refCons)
    {
        $xml = null;
        $xmlOpoce = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($idDestinataire, Atexo_CurrentUser::getOrganismAcronym());

        if ($xmlOpoce && $xmlOpoce->getXml()) {
            //cas modification $this->_isFluxInit doit etre vide
            $this->_isFluxInit = '';
            $xml = $xmlOpoce->getXml();
        } elseif (!$xmlOpoce || ($xmlOpoce && null == $xmlOpoce->getXml())) {
            //xml Initialisation
            $xml = (new Atexo_Publicite_AvisPub())->construireXmlInitialisation($refCons, $idDestinataire);
        }//echo base64_decode($xml);exit;

        return $xml;
    }
}
