<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Marche\Atexo_Marche_CriteriaVo;

/**
 * Classe de.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ExportMarchesNotifies extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        self::afficherTableauRecherche();
        if (!$this->isPostBack) {
            self::remplirListeServices();
            self::displayNaturePrestations();
        }
    }

    /*
     * Permet de remplir la liste des services
     */
    public function remplirListeServices()
    {
        // Si l'agent est affecté à un service onb affiche tout les services en doussous de son pôle
        $currentServiceId = Atexo_CurrentUser::getCurrentServiceId();
        if (!empty($currentServiceId)) {
            $listeChilds = Atexo_EntityPurchase::getSubServices($currentServiceId, Atexo_CurrentUser::getCurrentOrganism(), false, true);
            $serviceAgent = Atexo_EntityPurchase::retrieveEntityById($currentServiceId, Atexo_CurrentUser::getCurrentOrganism());
            if ($serviceAgent) {
                $path = Atexo_EntityPurchase::getPathServiceById($serviceAgent->getId(), Atexo_CurrentUser::getCurrentOrganism());
                if (is_array($listeChilds) && count($listeChilds)) {
                    $arrayEntity = [];
                    $arrayEntity[$serviceAgent->getId()] = $path;
                    foreach ($listeChilds as $key => $value) {
                        $arrayEntity[$key] = $value;
                    }
                    $listeChilds = $arrayEntity;
                } else {
                    $listeChilds = [$serviceAgent->getId() => $path];
                }
                $this->idServiceAgentConnecte->Value = $serviceAgent->getId();
            }
        } else {
            // Si l'agent n'est pas affecté à un service on affiche tout les services
            $listeChilds = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
        }
        $this->entiteAchat->Datasource = $listeChilds;
        $this->entiteAchat->dataBind();
    }

    /*
     * Permet de remplir la liste des natures des prestations
     */
    public function displayNaturePrestations()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $prestations = new Atexo_Consultation_Category();
        $objetPrestations = $prestations->retrieveCategories(false, true, $langue);
        $listPrestations = $prestations->retrieveDataCategories($objetPrestations, $langue);
        $this->naturePrestations->DataSource = $listPrestations;
        $this->naturePrestations->DataBind();
    }

    public function rechercherMarchesNotifies()
    {
        $criteriaVo = self::getCriteresRecherche();
        $this->setViewState('CriteriaVo', $criteriaVo);
        self::fillRepeaterWithDataForSearchResult($criteriaVo);
        self::afficherTableauResultats();
    }

    /*
     * Recupération des critères de recherche
     * Retourne un objet CriteriaVo rempli des critères de recherche choisis par l'utilisateur
    */
    public function getCriteresRecherche()
    {
        $criteriaVo = new Atexo_Marche_CriteriaVo();
        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        if ('' != $this->entiteAchat->getSelectedValue()) {
            $criteriaVo->setIdService(trim($this->entiteAchat->getSelectedValue()));
            if (true == $this->inclureDescendances->Checked) {
                $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme());
                $criteriaVo->setIdsService($listOfServicesAllowed);
            }
        } elseif (!Atexo_Module::isEnabled('OrganisationCentralisee', Atexo_CurrentUser::getCurrentOrganism()) && '' == $this->entiteAchat->getSelectedValue()) {
            $criteriaVo->setIdService($this->idServiceAgentConnecte->Value);
        }
        if (trim($this->attributaire->Text)) {
            $criteriaVo->setNomAttributaire(trim($this->attributaire->Text));
        }
        if ('' != $this->naturePrestations->getSelectedValue()) {
            $criteriaVo->setNatureMarche($this->naturePrestations->getSelectedValue());
        }
        if (trim($this->dateNotificationStart->Text)) {
            $criteriaVo->setDate1(trim($this->dateNotificationStart->Text));
        }
        if (trim($this->dateNotificationEnd->Text)) {
            $criteriaVo->setDate2(trim($this->dateNotificationEnd->Text));
        }
        if (trim($this->keywordSearch->Text)) {
            $criteriaVo->setMotclef(trim($this->keywordSearch->Text));
        }

        return $criteriaVo;
    }

    /*
     * Charge le tableau d'affichage avec les résultats de la recherche
    */
    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $marche = new Atexo_Marche();
        $nombreElement = $marche->searchMarchesNotifies($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->nbrResultats->Text = $nombreElement;
            self::showComponents();
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
            $this->repeaterListMarches->setVirtualItemCount($nombreElement);
            $this->repeaterListMarches->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            self::hideComponents();
        }
    }

    /*
     * Fonction de remplissage du tableau d'affichage
     */
    public function populateData(Atexo_Marche_CriteriaVo $criteriaVo)
    {
        $offset = $this->repeaterListMarches->CurrentPageIndex * $this->repeaterListMarches->PageSize;
        $limit = $this->repeaterListMarches->PageSize;
        if ($offset + $limit > $this->repeaterListMarches->getVirtualItemCount()) {
            $limit = $this->repeaterListMarches->getVirtualItemCount() - $offset;
        }
        $marche = new Atexo_Marche();
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayListeMarchesNotifies = $marche->searchMarchesNotifies($criteriaVo);
        $this->repeaterListMarches->DataSource = $arrayListeMarchesNotifies;
        $this->repeaterListMarches->DataBind();
    }

    /*
     * Pagination
     */
    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->repeaterListMarches->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterListMarches->PageSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
        self::afficherTableauResultats();
    }

    /*
     * Pagination
     */
    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterListMarches->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->repeaterListMarches->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->repeaterListMarches->CurrentPageIndex + 1;
        }
        self::afficherTableauResultats();
    }

    /*
     * Pagination
     */
    public function pageChanged($sender, $param)
    {
        $this->repeaterListMarches->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
        self::afficherTableauResultats();
    }

    /*
     * Masque les composants dependant du tableau d'affichage
     */
    public function hideComponents()
    {
        $this->PagerBottom->setVisible(false);
        $this->PagerTop->setVisible(false);
        $this->nbrResultats->setVisible(false);
        $this->nombreResultatAfficherBottom->setVisible(false);
        $this->nombreResultatAfficherTop->setVisible(false);
        $this->textAfficherTop->setVisible(false);
        $this->textAfficherBottom->setVisible(false);
        $this->texteNbrResultatParPageTop->setVisible(false);
        $this->texteNbrResultatParPageBottom->setVisible(false);
        $this->numPageTop->setVisible(false);
        $this->numPageBottom->setVisible(false);
        $this->labelSlashTop->setVisible(false);
        $this->labelSlashBottom->setVisible(false);
    }

    /*
     * Affiche les composants dependant du tableau d'affichage
     */
    public function showComponents()
    {
        $this->PagerBottom->setVisible(true);
        $this->PagerTop->setVisible(true);
        $this->nbrResultats->setVisible(true);
        $this->nombreResultatAfficherBottom->setVisible(true);
        $this->nombreResultatAfficherTop->setVisible(true);
        $this->textAfficherTop->setVisible(true);
        $this->textAfficherBottom->setVisible(true);
        $this->texteNbrResultatParPageTop->setVisible(true);
        $this->texteNbrResultatParPageBottom->setVisible(true);
        $this->numPageTop->setVisible(true);
        $this->numPageBottom->setVisible(true);
        $this->labelSlashTop->setVisible(true);
        $this->labelSlashBottom->setVisible(true);
    }

    /*
     * Affiche le tableau de resultats et masque le formulaire de recherche
     */
    public function afficherTableauResultats()
    {
        $this->panelRechercheAvancee->setVisible(false);
        $this->panelResultatRecherche->setVisible(true);
        $this->panelExportMarchesNotifies->setVisible(true);
    }

    /*
     * Affiche le formulaire de recherche
     */
    public function afficherTableauRecherche()
    {
        $this->panelRechercheAvancee->setVisible(true);
        $this->panelResultatRecherche->setVisible(false);
        $this->panelExportMarchesNotifies->setVisible(false);
    }

    public function genererExcelMarchesNotifies()
    {
        $marche = new Atexo_Marche();
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setExportXls(true);
        $arrayListeMarchesNotifies = $marche->searchMarchesNotifies($criteriaVo);
        (new Atexo_GenerationExcel())->genererExcelMarchesNotifies($arrayListeMarchesNotifies, $criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);

        if ('ASC' == $arraySensTri[$champsOrderBy]) {
            $arraySensTri[$champsOrderBy] = 'DESC';
        } else {
            $arraySensTri[$champsOrderBy] = 'ASC';
        }
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);

        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->repeaterListMarches->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
        self::afficherTableauResultats();
    }
}
