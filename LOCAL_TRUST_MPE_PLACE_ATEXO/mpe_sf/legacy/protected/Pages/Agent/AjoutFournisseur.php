<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SuppliersOfDocuments;

/**
 * Admministration des fournisseurs de documents.
 */
class AjoutFournisseur extends MpeTPage
{
    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            if (isset($_GET['idFournisseur'])) {
                $supplier = Atexo_Consultation_SuppliersOfDocuments::getDataSupplier(Atexo_Util::atexoHtmlEntities($_GET['idFournisseur']));
                $this->nomFournisseur->Text = $supplier->getNom();
                $this->mailFournisseur->Text = $supplier->getEmail();
            }
        }

        $this->customizeForm();
    }

    /**
     * Récupère les données du fournisseur de document.
     *
     * @return array données du fournisseur
     */
    public function getDataSupplier()
    {
        $idServices = null;
        $idServiceAgent = Atexo_CurrentUser::getCurrentServiceId();
        $arraySupplier = [];
        if (isset($_GET['idFournisseur'])) {
            $arraySupplier['Id'] = Atexo_Util::atexoHtmlEntities($_GET['idFournisseur']);
        }
        $arraySupplier['Nom'] = Atexo_Util::atexoHtmlEntities($this->nomFournisseur->Text);
        $arraySupplier['Email'] = Atexo_Util::atexoHtmlEntities($this->mailFournisseur->Text);
        if ($idServiceAgent) {
            $subServices = Atexo_EntityPurchase::getSubServices($idServiceAgent, Atexo_CurrentUser::getCurrentOrganism(), true);
            if (is_array($subServices)) {
                $idServices = array_values($subServices);
            }
        } else {
            // Si l'agent n'est pas affecté à un service on affiche tout ses sous-services
            $subServices = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), false);
            unset($subServices[0]);
            if (is_array($subServices)) {
                $idServices = array_keys($subServices);
            }
        }

        $serviceId = Atexo_Util::atexoHtmlEntities($_GET['idService']);
        if ($subServices && is_array($subServices) && $_GET['idService'] != $idServiceAgent) {
            if (in_array($_GET['idService'], $idServices)) {
                $arraySupplier['IdService'] = empty($serviceId) ? null : $serviceId;
            }
        } elseif ($_GET['idService'] == $idServiceAgent) {
            $arraySupplier['IdService'] = empty($serviceId) ? null : $serviceId;
        }

        return $arraySupplier;
    }

    /**
     * Ajout ou modification d'un fournisseur de document.
     */
    public function onClickAddOrModify()
    {
        if ($this->IsValid) {
            if (isset($_GET['idFournisseur'])) {
                // En mode modification
                Atexo_Consultation_SuppliersOfDocuments::modify($this->getDataSupplier());
            } else {
                // En mode création
                Atexo_Consultation_SuppliersOfDocuments::add($this->getDataSupplier());
            }

            $this->scriptAddAndClose->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_activateAddSupplier').click();window.close();</script>";
        }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
        }
    }
}
