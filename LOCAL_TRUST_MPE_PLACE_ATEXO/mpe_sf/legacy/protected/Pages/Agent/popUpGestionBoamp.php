<?php

namespace Application\Pages\Agent;

use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Propel\Mpe\CommonAcheteurPublicPeer;
use Application\Propel\Mpe\CommonRenseignementsBoampPeer;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Publicite\Atexo_Publicite_RenseignementsBoamp;
use Application\Service\Atexo\Atexo_Referentiel;
use Application\Propel\Mpe\CommonRenseignementsBoamp;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Service\Atexo\Atexo_Ref;
use Application\Controls\MpeTPage;
use Prado\Web\UI\TPage;
use Prado\Prado;
use Exception;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Config;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Library\Propel\Propel;

/**
 * commentaires
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package
 * @subpackage
 */

class popUpGestionBoamp extends MpeTPage
{
    protected $_publicPurchaser;
    protected $_displayBlocComplementaire = false;
    protected $_displayBlocProcedureRecours = false;
    protected $_renseignementsBoamp ;

    public function onInit($param)
    {

        Atexo_Languages :: setLanguageCatalogue("agent");
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack && Atexo_Config :: getParameter('PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP') != 3) {
            if (Atexo_Config :: getParameter('PARAMETRAGE_DEMONSTRATION_BOAMP') == Atexo_Config :: getParameter('PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP')) {
                $this->blocDemo->setDisplay('Dynamic');
                $this->blocProd->setDisplay('None');
            } elseif (Atexo_Config :: getParameter('PARAMETRAGE_PRODUCTION_BOAMP') == Atexo_Config :: getParameter('PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP')) {
                $this->blocDemo->setDisplay('None');
                $this->blocProd->setDisplay('Dynamic');
            }
        }
        if (isset($_GET['id'])) {
            $publicPurchaser = (new Atexo_PublicPurchaser())->retrievePublicPurchasersById(Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($publicPurchaser instanceof CommonAcheteurPublic) {
                $this->_publicPurchaser = $publicPurchaser;
                if (!$this->IsPostBack) {
                    //$this->paysLivraison->SelectedValue = $this->_publicPurchaser->getLivraisonPays();
                    //$this->paysService->SelectedValue = $this->_publicPurchaser->getFacturePays();
                        // Début affichage code NUTS LT-Ref
                        $atexoCodesNutsRef = new Atexo_Ref();
                        $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                    if ($this->getPublicPurchaser()->getCodeNuts()) {
                        $atexoCodesNutsRef->setSecondaires($this->getPublicPurchaser()->getCodeNuts());
                    }
                        $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                        // Fin affichage code NUTS  LT-Ref
                        $this->refCommande->setText($publicPurchaser->getReferenceCommande());
                        $this->displayGeoN2InModification($publicPurchaser->getDepartementMiseEnLigne());
                }
            } else {
                throw new Atexo_Exception('No public purchaser found');
            }
        } else {
            $achteurPublic = new CommonAcheteurPublic();
            $achteurPublic->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            if (Atexo_Config :: getParameter('PARAMETRAGE_DEMONSTRATION_BOAMP') == Atexo_Config :: getParameter('PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP')) {
                $achteurPublic->setBoampTarget(0);
            } elseif (Atexo_Config :: getParameter('PARAMETRAGE_PRODUCTION_BOAMP') == Atexo_Config :: getParameter('PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP')) {
                $achteurPublic->setBoampTarget(1);
            }
            $this->_publicPurchaser = $achteurPublic;
            if (!$this->IsPostBack) {
                /*
                $this->paysLivraison->SelectedValue = Atexo_Config::getParameter('ID_GEON2_FRANCE');
                $this->paysService->SelectedValue = Atexo_Config::getParameter('ID_GEON2_FRANCE');
                */
                    //Début affichage code NUTS
                    $atexoCodesNutsRef = new Atexo_Ref();
                    $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                    $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                    //Fin affichage code NUTS
            }
        }
        if (!$this->IsPostBack) {
            $listeTypeOrg = (new Atexo_Referentiel())->retrieveArrayValEurReferentielByReferentiel(Atexo_Config:: getParameter('REFERENCE_TYPE_ORGANISME'), Atexo_CurrentUser:: getCurrentOrganism());

            $this->typeOrganisme->DataSource = $listeTypeOrg;
            $this->typeOrganisme->DataBind();

            if ($this->_publicPurchaser instanceof CommonAcheteurPublic) {
                $this->_renseignementsBoamp = (new Atexo_Publicite_RenseignementsBoamp())->retrieveRenseignementsBoampByIdType($this->_publicPurchaser->getId());
            }
            $this->displayBlocComplementaire();
            $this->displayBlocProcedureRecours();
            $this->displayBlocTypePouvoirAdjudicateur();
        }
            $this->customizeForm();
    }

    public function saveDataCompteBoamp()
    {
        $connexion = Propel :: getConnection(Atexo_Config :: getParameter('COMMON_DB') . Atexo_Config :: getParameter('CONST_READ_WRITE'));
        if (isset($_GET['id'])) {
            $publicPurchaser = CommonAcheteurPublicPeer :: retrieveByPk(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        } else {
            $publicPurchaser = new CommonAcheteurPublic();
            $publicPurchaser->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $serviceId = Atexo_Util::atexoHtmlEntities($_GET['serv']);
            $publicPurchaser->setServiceId(empty($serviceId) ? null : $serviceId);
        }
        $resultat = $this->fillObjectPublicPurchaser($publicPurchaser);

        if ($resultat == -1) {
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(Prado::localize("TEXTE_LOGIN_COMPTE_BOAMP_INCORRECT"));
            return;
        } elseif ($resultat == -2) {
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(Prado::localize("TEXTE_PW_COMPTE_BOAMP_INCORRECT"));
            return;
        } elseif ($resultat == -3) {
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(Prado::localize("TEXTE_MAIL_COMPTE_BOAMP_INCORRECT"));
            return;
        }
        $connexion->beginTransaction();
        try {
            $publicPurchaser->save($connexion);
            $this->saveObjectRenseignementsBoamp($publicPurchaser->getId());
            $connexion->commit();
        } catch (\Exception $e) {
            $connexion->rollback();
        }


        $script = "<script>if(opener.document.getElementById('ctl0_CONTENU_PAGE_activateAddBoamp')) opener.document.getElementById('ctl0_CONTENU_PAGE_activateAddBoamp').click();";
        $script .= "window.close();</script>";

        $this->scriptAddAndClose->Text = $script;
    }

    public function getPublicPurchaser()
    {
        return $this->_publicPurchaser;
    }

    public function fillObjectPublicPurchaser(&$achteurPublic)
    {
        if ($achteurPublic instanceof CommonAcheteurPublic) {
            $achteurPublic->setBoampLogin($this->idBoamp->Text);
            $achteurPublic->setBoampPassword($this->passwordBoamp->Text);
            $achteurPublic->setBoampMail($this->mailBoamp->Text);
            if ($this->demo->Checked) {
                $achteurPublic->setBoampTarget('0');
            } else {
                $achteurPublic->setBoampTarget('1');
            }

                $achteurPublic->setTypeOrg($this->typeOrganisme->getSelectedValue());
                $achteurPublic->setPrm($this->correspondant->Text);
                $achteurPublic->setDenomination($this->nomOrga->Text);
                $achteurPublic->setAdresse($this->adresseOrga->Text);
                $achteurPublic->setCp($this->cpOrga->Text);
                $achteurPublic->setVille(ucfirst(strtolower($this->villeOrg->Text)));
                $achteurPublic->setTelephone($this->telOrga->Text);
                $achteurPublic->setFax($this->faxOrga->Text);
                $achteurPublic->setUrl($this->urlPa->Text);
                $achteurPublic->setUrlAcheteur($this->urlAcheteur->Text);
                $achteurPublic->setFactureNumero($this->comptePayeur->Text);
                $achteurPublic->setFactureDenomination($this->denomination->Text);
                $achteurPublic->setFacturationService($this->service->Text);
                $achteurPublic->setFactureAdresse($this->adresseService->Text);
                $achteurPublic->setFactureCp($this->cpService->Text);
                $achteurPublic->setFactureVille(ucfirst(strtolower($this->villeService->Text)));
                $achteurPublic->setLivraisonService($this->serviceLivraison->Text);
                $achteurPublic->setLivraisonAdresse($this->adresseLivraison->Text);
                $achteurPublic->setLivraisonCodePostal($this->cpLivraison->Text);
                $achteurPublic->setLivraisonVille(ucfirst(strtolower($this->villeLivraison->Text)));
                $achteurPublic->setDepartementMiseEnLigne($this->idsSelectedGeoN2->Value);
                $achteurPublic->setReferenceCommande($this->refCommande->getText());

                $typePouvoirAdjudicateur = "";
            foreach ($this->repeaterTypePouvoirAdjudicateur->getItems() as $item) {
                if ($item->pouvoirAdjuActivitePrincipalesType->checked) {
                    foreach ($item->repeaterPaType->getItems() as $itemPaType) {
                        if ($itemPaType->paType->checked) {
                            $typePouvoirAdjudicateur .= $itemPaType->idLabelPaType->Value . "#";
                        }
                    }
                }
            }
                $achteurPublic->setTypePouvoirActivite($typePouvoirAdjudicateur);
                $achteurPublic->setCodeNuts($this->idAtexoRefNuts->codesRefSec->value);
                $achteurPublic->setModalitesFinancement($this->modaliteFinancement->Text);
                //Début calcul du code accès logiciel et code provenance
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            if ($organisme instanceof CommonOrganisme) {
                if (!Atexo_Module::isEnabled('VerifierCompteBoamp')) {
                    $achteurPublic->setCodeAccesLogiciel($organisme->getCodeAccesLogiciel());
                    $achteurPublic->setMoniteurProvenance($organisme->getMoniteurProvenance());
                } else {
                    $reponse = self::getInfosConcentrateursBoamp($achteurPublic);
                    if ($reponse == 'ERREUR_CONNEXION_RESEAU') {
                        $achteurPublic->setCodeAccesLogiciel($organisme->getCodeAccesLogiciel());
                        $achteurPublic->setMoniteurProvenance($organisme->getMoniteurProvenance());

                        $log = "\n\n";
                        $log .= "\n\t____________________________________\n";
                        $log .= "\n\nVerification du compte BOAMP " . $achteurPublic->getBoampLogin() . " " . $achteurPublic->getBoampPassword() . "<br/>";
                        $log .= "\n\n";
                        $log .= "\n\t____________________________________\n";
                        $log .= "\n\t Erreur connexion application BOAMP \n";
                        $filePath = Atexo_Config:: getParameter('LOG_FILE_PATH_BOAMP') . "verifCompteBoamp" . date('YmdHisu') . ".log";
                        Atexo_Util::writeLogFile(Atexo_Config:: getParameter('LOG_FILE_PATH_BOAMP_SUIVI'), $filePath, $log, 'a');
                    }
                    if ($reponse == 'ERR') {
                        return -1;
                    } elseif ($reponse == 'PASS') {
                        return -2;
                    } elseif ($reponse == 'MEL') {
                        return -3;
                    } elseif (strstr($reponse, '#')) {//Cas OK
                        $arrayCode = explode('#', $reponse);
                        $achteurPublic->setCodeAccesLogiciel($arrayCode[0]);
                        $achteurPublic->setMoniteurProvenance($arrayCode[1]);
                    }
                }
            }
        }
            //Fin sauvegarde du code accès logiciel et code provenance

        return $achteurPublic;
    }

    public function customizeForm()
    {
        if (!Atexo_Module :: isEnabled('PradoValidateurFormatEmail')) {
            $this->validateur1->Enabled = false;
        }
    }

    public function retrieveRenseignementsBoamp()
    {
        if ($this->_publicPurchaser instanceof CommonAcheteurPublic) {
            $data = ( new Atexo_Publicite_RenseignementsBoamp() )->retrieveRenseignementsBoampByIdType($this->_publicPurchaser->getId());
            $this->_renseignementsBoamp = array();
            if ($data && count($data)) {
                foreach ($data as $oneType) {
                    $this->_renseignementsBoamp[$oneType->getIdType()] = $oneType;
                }
            }
        }
    }
    public function displayBlocComplementaire()
    {
        $arrayData = array();
        foreach ($this->_renseignementsBoamp as $oneType) {
            $arrayData[$oneType->getIdType()] = $oneType;
        }

        if (
            is_array($arrayData)
            && !empty($arrayData)
            && (
                (isset($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1')])
                    && !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1')])
                )
                || (isset($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2')])
                    && !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2')])
                )
                || (isset($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3')])
                    && !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3')])
                )
                || (isset($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4')])
                    && !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4')])
                )
                || (isset($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5')])
                    && !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5')])
                )
            )
        ) {
            $this->_displayBlocComplementaire = true;
        }
        $type1 = array (
            0 => Prado::localize('ADRESSE_COMPLEMENTS_1'),
            1 => Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1'),
            2 => $arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1')]);

        $type2 = array (
            0 => Prado::localize('ADRESSE_COMPLEMENTS_2'),
            1 => Atexo_Config :: getParameter('ADRESSE_COMPLEMENTAIRES_2'),
            2 => $arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2')]);
        $type3 = array (
            0 => Prado::localize('ADRESSE_COMPLEMENTS_3'),
            1 => Atexo_Config :: getParameter('ADRESSE_COMPLEMENTAIRES_3'),
            2 => $arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3')]
            );
        $type4 = array (
            0 => Prado::localize('ADRESSE_COMPLEMENTS_4'),
            1 => Atexo_Config :: getParameter('ADRESSE_COMPLEMENTAIRES_4'),
            2 => $arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4')]
            );
        $type5 = array (
            0 => Prado::localize('ADRESSE_COMPLEMENTS_5'),
            1 => Atexo_Config :: getParameter('ADRESSE_COMPLEMENTAIRES_5'),
            2 => $arrayData[Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5')]
        );
        $dataLabel = array (
            0 => $type1,
            1 => $type2,
            2 => $type3,
            3 => $type4,
            4 => $type5);
        $this->repeaterBlocComplementaire->DataSource = $dataLabel;
        $this->repeaterBlocComplementaire->DataBind();
    }

    public function displayBlocProcedureRecours()
    {
        $arrayData = array();
        foreach ($this->_renseignementsBoamp as $oneType) {
            $arrayData[$oneType->getIdType()] = $oneType;
        }

        if (
            is_array($arrayData) &&
            !empty($arrayData) &&
            (
                (isset($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_1')]) &&
                    !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_1')])
                ) ||
                (isset($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_2')]) &&
                    !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_2')])
                ) ||
                (isset($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_3')]) &&
                    !$this->isEmptyRenseignementBoamp($arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_3')])
                )
            )
        ) {
            $this->_displayBlocProcedureRecours = true;
        }

        $type1 = [
            0 => Prado::localize('PROCEDURE_RECOURS_1'),
            1 => Atexo_Config::getParameter('PROCEDURE_RECOURS_1'),
            2 => $arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_1')]
        ];

        $type2 = [
            0 => Prado::localize('PROCEDURE_RECOURS_2'),
            1 => Atexo_Config:: getParameter('PROCEDURE_RECOURS_2'),
            2 => $arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_2')]
        ];

        $type3 = [
            0 => Prado::localize('PROCEDURE_RECOURS_3'),
            1 => Atexo_Config:: getParameter('PROCEDURE_RECOURS_3'),
            2 => $arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_3')]
            ];
        $type4 = [
            0 => Prado::localize('PROCEDURE_RECOURS_4'),
            1 => Atexo_Config :: getParameter('PROCEDURE_RECOURS_4'),
            2 => $arrayData[Atexo_Config::getParameter('PROCEDURE_RECOURS_4')]
            ];

        $dataLabel = [
            0 => $type1,
            1 => $type2,
            2 => $type3,
            4 => $type4
        ];

        $this->repeaterBlocProcedureRecours->DataSource = $dataLabel;
        $this->repeaterBlocProcedureRecours->DataBind();
    }

    public function displayBlocTypePouvoirAdjudicateur()
    {
        $dataType2 =  (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(
            Atexo_Config :: getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR')
        );
        $dataType3 = ( new Atexo_CommonReferentiel())->retrieveValeurReferentiel(
            Atexo_Config :: getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE')
        );

        $dataSource[1] = [
            -1 => Atexo_Config :: getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR'),
             0 => prado :: localize('PRINCIPALE_ACTIVITE_POUVOIR_ADJUCATEUR'),
            1 => prado :: localize('DETAIL_ACTIVITE_PRINCIPALE_PRINCIPALE_POUVOIR_ADJUDICATEUR'),
            2 => $dataType2,
            3 => '1'
        ];
        $dataSource[2] = [
            -1 => Atexo_Config :: getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE'),
             0 => prado :: localize('PINCIPALE_ENTITE_ADJUDICATRICE'),
            1 => prado :: localize('DETAILS_PRINCIPALE_ENTITE_ADJUDICATRICE'),
            2 => $dataType3,
            3 => '1'
        ];

        $this->repeaterTypePouvoirAdjudicateur->DataSource = $dataSource;
        $this->repeaterTypePouvoirAdjudicateur->DataBind();
    }

    public function isEmptyRenseignementBoamp(?CommonRenseignementsBoamp $renseignementsBoamp)
    {
        if (empty($renseignementsBoamp)) {
            return true;
        }

        return empty($renseignementsBoamp->getCorrespondant()) &&
            empty($renseignementsBoamp->getOrganisme()) &&
            empty($renseignementsBoamp->getAdresse()) &&
            empty($renseignementsBoamp->getCp()) &&
            empty($renseignementsBoamp->getVille()) &&
            empty($renseignementsBoamp->getTelephone()) &&
            empty($renseignementsBoamp->getPoste()) &&
            empty($renseignementsBoamp->getFax()) &&
            empty($renseignementsBoamp->getMail()) &&
            empty($renseignementsBoamp->getUrl()) &&
            empty($renseignementsBoamp->getOrganeChargeProcedure());
    }

    public function saveObjectRenseignementsBoamp($idCompte)
    {
        foreach ($this->repeaterBlocComplementaire->getItems() as $itemBlocComplementaire) {
            $this->fillObjectRenseignementsBoamp($idCompte, $itemBlocComplementaire);
        }
        foreach ($this->repeaterBlocProcedureRecours->getItems() as $itemBlocProcedureRecours) {
            $this->fillObjectRenseignementsBoamp($idCompte, $itemBlocProcedureRecours);
        }
    }

    public function fillObjectRenseignementsBoamp($idCompte, $item)
    {
        $connexion = Propel :: getConnection(Atexo_Config :: getParameter('COMMON_DB')
            . Atexo_Config :: getParameter('CONST_READ_WRITE'));
        $idRenseignementObject = $item->idRenseignementObject->Value;
        if ($idRenseignementObject == "") {
            $newRenseignementsBoamp = new CommonRenseignementsBoamp();
            $newRenseignementsBoamp->setAcronymeorg(Atexo_CurrentUser::getOrganismAcronym());
        } else {
            $newRenseignementsBoamp = CommonRenseignementsBoampPeer::retrieveByPK(
                $idRenseignementObject,
                Atexo_CurrentUser::getOrganismAcronym(),
                $connexion
            );
        }
        if (!$newRenseignementsBoamp instanceof CommonRenseignementsBoamp) {
            return;
        }

        if (
            empty($item->corresppondant->Text) &&
            empty($item->nomOrganisme->Text) &&
            empty($item->adresse->Text) &&
            empty($item->cp->Text) &&
            empty($item->ville->Text) &&
            empty($item->telephone->Text) &&
            empty($item->poste->Text) &&
            empty($item->telecopieur->Text) &&
            empty($item->email->Text) &&
            empty($item->adresseInternet->Text) &&
            empty($item->organeCharge->Text)
        ) {
            return;
        }

        $corresppondant = $item->corresppondant->Text;
            $newRenseignementsBoamp->setCorrespondant($corresppondant);
        $nomOrganisme = $item->nomOrganisme->Text;
            $newRenseignementsBoamp->setOrganisme($nomOrganisme);
        $adresse = $item->adresse->Text;
            $newRenseignementsBoamp->setAdresse($adresse);
        $cp = $item->cp->Text;
            $newRenseignementsBoamp->setCp($cp);
        $ville = $item->ville->Text;
            $newRenseignementsBoamp->setVille(ucfirst(strtolower($ville)));
        $telephone = $item->telephone->Text;
            $newRenseignementsBoamp->setTelephone($telephone);
        $poste = $item->poste->Text;
            $newRenseignementsBoamp->setPoste($poste);
        $telecopieur = $item->telecopieur->Text;
            $newRenseignementsBoamp->setFax($telecopieur);
        $email = $item->email->Text;
            $newRenseignementsBoamp->setMail(trim($email));
        $adresseInternet = $item->adresseInternet->Text;
            $newRenseignementsBoamp->setUrl($adresseInternet);
        $organeProcedureMediation = $item->organeCharge->Text;
            $newRenseignementsBoamp->setOrganeChargeProcedure($organeProcedureMediation);

        $newRenseignementsBoamp->setIdType($item->idType->Value);
        $newRenseignementsBoamp->setIdCompte($idCompte);
        $newRenseignementsBoamp->save($connexion);
    }


    public function existingTypePouvoirAdjudicateur($idType)
    {
        if ($this->_publicPurchaser) {
            $arrayType = explode('#', $this->_publicPurchaser->getTypePouvoirActivite());
            if (in_array($idType, $arrayType)) {
                return true;
            }
        }
        return false;
    }


    public function existingAdjuActivitePrincipalesType($idType)
    {
        if ($this->_publicPurchaser) {
            $arrayType = explode('#', $this->_publicPurchaser->getTypePouvoirActivite());
            foreach ($arrayType as $oneType) {
                if ($oneType[0] == $idType) {
                    return true;
                }
            }
        }
        return false;
    }


    public function haveAdjuActivitePrincipalesType()
    {
        if ($this->_publicPurchaser) {
            $arrayType = explode('#', $this->_publicPurchaser->getTypePouvoirActivite());
            if (is_array($arrayType) && count($arrayType) > 1) {
                return true;
            }
        }
        return false;
    }


    public function isRenseignementObject($renseignementObject)
    {
        return ($renseignementObject instanceof CommonRenseignementsBoamp) ? true : false;
    }


    public function openPanelBlocComplementaire()
    {
        // pour open panel BlocComplementaire
        return ($this->_displayBlocComplementaire) ? true : false;
    }

    public function openBlocProcedureRecours()
    {
        // pour open panel BlocProcedureRecours
        return ($this->_displayBlocProcedureRecours) ? true : false;
    }

    public function openBlocInfosMarche()
    {
        return ($this->getPublicPurchaser()->getCodeNuts() || $this->getPublicPurchaser()->getModalitesFinancement()) ? "block" : "none";
    }

    public function openBlocInfoMarche()
    {
        if (($this->_publicPurchaser->getCodeNuts()) || ($this->_publicPurchaser->getModalitesFinancement())) {
            return true;
        } else {
            return false;
        }
    }

    public function openBlocInfosFacturation()
    {
        $achteurPublic = $this->getPublicPurchaser();
        if ($achteurPublic) {
            return ($achteurPublic->getFactureNumero()  || $achteurPublic->getLivraisonVille() ||
                $achteurPublic->getFacturationService() || $achteurPublic->getFactureAdresse() ||
                $achteurPublic->getFactureAdresse() || $achteurPublic->getFactureCp() ||
                $achteurPublic->getFactureVille() || $achteurPublic->getLivraisonService() ||
                $achteurPublic->getLivraisonCodePostal() || $achteurPublic->getLivraisonAdresse()) ? true : false;
        }
        return false;
    }

    /**
     * Permet de recuperer des informations du compte BOAMP
     */
    public function getInfosConcentrateursBoamp($compteBoamp)
    {
        $postData = array("login" => $compteBoamp->getBoampLogin(), "password" => $compteBoamp->getBoampPassword(), "mail" => $compteBoamp->getBoampMail(),
                            "BoampTarget" => $compteBoamp->getBoampTarget());
        $url = Atexo_Config::getParameter('URL_VERIFICATION_COMPTE_BOAMP');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter("URL_PROXY");
        $portProxy = Atexo_Config::getParameter("PORT_PROXY");
        $curlOptProxy = $urlProxy . ":" . $portProxy;
        if ($urlProxy != "") {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $response = curl_exec($ch);

        return trim($response);
    }


    /**
     *
     * Permet de retourner l'url du lieu d'exécution
     */
    public function getLieuExecutionUrl()
    {
        $param = $this->numSelectedGeoN2->Value;
        $url = "index.php?page=Commun.LieuxExecutionCarte&lieux=" . $param;
        return "javascript:popUpSetSize('" . $url . "','850px','750px','yes');";
    }

    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
    }

    public function getSelectedGeoN2($ids)
    {
        $text = '';
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2 :: retrieveGeolocalisationN2byIds($arIds);
        if (is_array($list)) {
            $lieuxExecutionsType1 = "";
            foreach ($list as $oneGeoN2) {
                $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1Traduit() . ', ';
            }
            if ($lieuxExecutionsType1) {
                $text = substr($lieuxExecutionsType1, 0, -2);
            }
        }
        return $text;
    }

    public function displayGeoN2InModification($lieuExecution)
    {
        $this->idsSelectedGeoN2->Value = $lieuExecution;
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
    }

    public function getNumSelectedGeoN2($buffer)
    {
        $arrayId     = explode(',', $buffer);
        $arrayId     = array_filter($arrayId);
        $new_arrayId = array();
        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
        if (is_array($geoN2)) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }
        $text = implode('_', $new_arrayId);
        return $text;
    }

    /**
     * Raffraichit le bloc des lieux d'executions
     *
     */
    public function refreshDisplaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->dptMiseEnLigne->setNavigateUrl($this->getLieuExecutionUrl());
        $this->panelDepartement->render($param->getNewWriter());
    }
}
