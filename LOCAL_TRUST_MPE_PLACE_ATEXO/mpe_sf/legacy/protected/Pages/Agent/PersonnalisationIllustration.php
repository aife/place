<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTIllustrationFond;
use Application\Propel\Mpe\CommonTIllustrationFondPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_IllustrationFond;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PersonnalisationIllustration extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('PersonnaliserAffichageThemeEtIllustration')) {
            if (!$this->isPostBack) {
                self::fillRepeaterIllustration();
            } else {
                $this->script->Text = '';
                $this->labelClose->Text = '';
            }
        } else {
            $this->response->redirect('agent');
        }
    }

    /*
     * permet de remplir le repeater par les illustration qui existent
     */
    public function fillRepeaterIllustration()
    {
        $actifTrouver = null;
        $dataSource = [];
        $i = 0;
        $dataSource[$i]['id'] = '0';
        $dataSource[$i]['libelle'] = Prado::localize('DEFINE_DEFAULT_ILLUSTRATION');
        $dataSource[$i++]['default'] = true;
        $idAgent = Atexo_CurrentUser::getIdAgentConnected();
        $illustrations = (new Atexo_IllustrationFond())->getIllustrationFondByAgent($idAgent);
        if (is_array($illustrations)) {
            foreach ($illustrations as $illustration) {
                $dataSource[$i]['id'] = $illustration->getIdIllustrationFond();
                $dataSource[$i]['libelle'] = $illustration->getLibelle();
                if ($illustration->getActif()) {
                    $actifTrouver = true;
                    setcookie('idIllustration', $illustration->getIdIllustrationFond());
                    $_COOKIE['idIllustration'] = $illustration->getIdIllustrationFond();
                }
                $dataSource[$i]['actif'] = $illustration->getActif();
                $dataSource[$i]['default'] = false;
                ++$i;
            }
        }
        if (!$actifTrouver) {
            $dataSource[0]['actif'] = true;
            setcookie('idIllustration', null);
            $_COOKIE['idIllustration'] = null;
        }
        $this->repeaterIllustrationFond->dataSource = $dataSource;
        $this->repeaterIllustrationFond->DataBind();
    }

    /*
     * permet de supprimer une illustration
     */
    public function deleteIllustration($sender, $param)
    {
        $idIllustration = $sender->TemplateControl->idIllustraction->value;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $illustrationToDelete = CommonTIllustrationFondPeer::retrieveByPK($idIllustration, $connexionCom);
        if ($illustrationToDelete instanceof CommonTIllustrationFond) {
            //supprimer le blob
            $blob = new Atexo_Blob();
            $idBlob = $illustrationToDelete->getIdBlobImage();
            $blob->deleteBlobFile($idBlob, Atexo_Config::getParameter('COMMON_BLOB'));

            //supprimer l'objet
            CommonTIllustrationFondPeer::doDelete($idIllustration, $connexionCom);
        }
        $this->fillRepeaterIllustration();
        $this->panelPrincipal->render($param->NewWriter);
        $this->response->redirect('index.php?page=Agent.PersonnalisationIllustration');
    }

    /*
     * Permet de faire un refresh
     */
    public function refreshRepeater($sender, $param)
    {
        $this->fillRepeaterIllustration();
    }

    /*
     * Permet d'enregistrer l'illustration choisi pour l'agent
     */
    public function saveIllustration($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idAgent = Atexo_CurrentUser::getIdAgentConnected();
        //désactiver l'ancien illustration
        (new Atexo_IllustrationFond())->desactiverAgentIllustrationFond($idAgent, $connexionCom);
        foreach ($this->repeaterIllustrationFond->getItems() as $item) {
            if ($item->choix_illustration->Checked && '0' != $item->idIllustraction->value) {
                $illustrationToUpdate = CommonTIllustrationFondPeer::retrieveByPK($item->idIllustraction->value, $connexionCom);
                if ($illustrationToUpdate instanceof CommonTIllustrationFond) {
                    $illustrationToUpdate->setActif(Atexo_Config::getParameter('ILLUSTRATION_DE_FOND_ACTIVER'));
                    $illustrationToUpdate->save($connexionCom);
                }
            }
        }
        $this->fillRepeaterIllustration();
    }

    /*
     * Permet d'enregistrer une nouvelle illustration pour l'agent
     */
    public function saveNewIllustration($sender, $param)
    {
        $nomIllustration = $this->nomIllustration->Text;
        if ($this->uploadIllustration->HasFile && $nomIllustration && $this->uploadIllustration->FileSize) {
            if ('JPG' == strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName())) || 'GIF ' == strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName())) || 'PNG' == strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName()))) {
                $infileIllustration = Atexo_Config::getParameter('COMMON_TMP').'illustration'.session_id().time();
                if (move_uploaded_file($this->uploadIllustration->LocalName, $infileIllustration)) {
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $atexoBlob = new Atexo_Blob();
                    $illustrationIdBlob = $atexoBlob->insert_blob($this->uploadIllustration->FileName, $infileIllustration, Atexo_Config::getParameter('COMMON_BLOB'));
                    $newIllustration = new CommonTIllustrationFond();
                    $newIllustration->setLibelle($nomIllustration);
                    $newIllustration->setNomImage($this->uploadIllustration->getFileName());
                    $newIllustration->setIdBlobImage($illustrationIdBlob);
                    $newIllustration->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $newIllustration->save($connexionCom);
                }
                @unlink($infileIllustration);
                $this->fillRepeaterIllustration();
            }
        }
    }

    /**
     * Verifier la taille et l'extension de l'image à ajouter.
     */
    public function verifyImageIllustration($sender, $param)
    {
        $sizedocAttache = $this->uploadIllustration->FileSize;
        if (0 == $sizedocAttache) {
            $param->IsValid = false;
            $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_FICHIER_VIDE');
            $this->labelClose->Text = '<script>errorAddIllustration();</script>';
            $this->fillRepeaterIllustration();

            return;
        }
        if ($this->uploadIllustration->HasFile) {
            if ('JPG' != strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName())) && 'GIF ' != strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName())) && 'PNG' != strtoupper(Atexo_Util::getExtension($this->uploadIllustration->getFileName()))) {
                $param->IsValid = false;
                $this->ajoutFichierValidator->ErrorMessage = Prado::localize('TEXT_MAUVAISE_EXTENSION_ILLUSTRATION');
                $this->labelClose->Text = '<script>errorAddIllustration();</script>';
                $this->fillRepeaterIllustration();

                return;
            }
        }
    }

    /**
     * Verifier le nom de l'image à ajouter.
     */
    public function verifyNomIllustration($sender, $param)
    {
        $nomIllustration = trim($this->nomIllustration->Text);
        if (!$nomIllustration) {
            $param->IsValid = false;
            $this->nomIllustrationValidator->ErrorMessage = Prado::localize('TEXT_NOM_ILLUSTRATION_FOND');
            $this->labelClose->Text = '<script>errorAddIllustration();</script>';
            $this->fillRepeaterIllustration();

            return;
        }
    }
}
