<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * permet permet de telecharger un fichier local.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class DownloadFileArchive extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * permet de telecharger un fichier local.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $pathFile = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['file']));
        $array = explode('/', $pathFile);
        if (isset($_GET['name']) && '' != $_GET['name']) {
            $fileName = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['name']));
        } else {
            $fileName = $array[count($array) - 1];
        }
        if (is_file($pathFile)) {
            Atexo_Util::downloadLocalFile($pathFile, $fileName);
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_FICHIER_NON_TROUVE'));
        }
    }
}
