<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * Page de gestion compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 20010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentGestionEntreprises extends MpeTPage
{
    private string|\Application\Propel\Mpe\Entreprise|bool $_company = '';
    protected $critereTri = '';
    protected $triAscDesc = '';
    private string|int $_idCompany = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['rc']) && '' != $_GET['rc']) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(Atexo_Util::atexoHtmlEntities($_GET['rc']));
        } elseif ((isset($_GET['idCompany']) && '' != $_GET['idCompany']) && (isset($_GET['pays']) && '' != $_GET['pays'])) {
            $this->_company = (new Atexo_Entreprise())->retrieveCompanyByIdNational(Atexo_Util::atexoHtmlEntities($_GET['idCompany']), Atexo_Util::atexoHtmlEntities($_GET['pays']));
        }
        if ($this->_company instanceof Entreprise) {
            $this->_idCompany = $this->_company->getId();
            $this->nomEntreprise->Text = $this->_company->getNom();
            if ($this->_company->getSiren()) {
                $this->rc->Text = $this->_company->getSiren();
                $this->intituleIdEntreprise->Text = Prado::localize('REGISTRE_DU_COMMERCE');
            } else {
                $this->rc->Text = $this->_company->getAcronymePays().Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->_company->getSirenetranger();
                $this->intituleIdEntreprise->Text = Prado::localize('TEXT_IDENTIFIANT_NATIONNAL');
            }
            if (!$this->IsPostBack) {
                $this->displyUtilisateurs();
            }
        }
    }

    /**
     * affiche la liste des utilisateurs d'une entreprise.
     */
    public function displyUtilisateurs()
    {
        $dataSource = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay($this->_company->getId());
        Atexo_CurrentUser::writeToSession('cachedUES', $dataSource);
        $this->repeaterUsers->DataSource = $dataSource;
        $this->repeaterUsers->DataBind();
    }

    /* Actualiser le tableau des utilisateurs dans le cas d'une nouvelle action
      *
      */
    public function refreshRepeater($sender, $param)
    {
        $this->displyUtilisateurs();
        $this->panelReaterUser->render($param->NewWriter);
    }

    /**
     * Trier le tableau des utlisateurs.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $cachedUES = Atexo_CurrentUser::readFromSession('cachedUES');
        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $this->setViewState('sortByElement', 'Nom');
            $dataSourceTriee = $this->getDataSourceTriee($cachedUES, 'Nom', '');
        } elseif ('Email' == $sender->getActiveControl()->CallbackParameter) {
            $this->setViewState('sortByElement', 'Email');
            $dataSourceTriee = $this->getDataSourceTriee($cachedUES, 'Email', '');
        } else {
            return;
        }
        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedUES');
        Atexo_CurrentUser::writeToSession('cachedUES', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->repeaterUsers->DataSource = $dataSourceTriee[0];
        $this->repeaterUsers->DataBind();

        // Re-affichage du TActivePanel
        $this->panelReaterUser->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, $typeCritere);
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function sortedWithNom()
    {
        if ('Nom' == $this->getViewState('sortByElement')) {
            return 'on';
        } else {
            return '';
        }
    }

    public function sortedWithEmail()
    {
        if ('Email' == $this->getViewState('sortByElement')) {
            return 'on';
        } else {
            return '';
        }
    }

    public function getIdCompany()
    {
        return $this->_idCompany;
    }

    public function refreshRepeaterCompany($sender, $param)
    {
        //if(!$this->_idCompany) {
        if (isset($_GET['rc']) && '' != $_GET['rc']) {
            $rc = $this->identifiantCompany->Value;
            $this->response->redirect('?page=Agent.AgentGestionEntreprises&rc='.$rc);
        } elseif ((isset($_GET['idCompany']) && '' != $_GET['idCompany']) && (isset($_GET['pays']) && '' != $_GET['pays'])) {
            $identifiant = explode('-', $this->identifiantCompany->Value);
            $this->response->redirect('?page=Agent.AgentGestionEntreprises&idCompany='.$identifiant[0].'&pays='.$identifiant[1]);
        }
        //}
    }
}
