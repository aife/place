<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;

/**
 * permet de lister les differents telechargement des archives effectues.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class TousLesTelechargement extends MpeTPage
{
    /**
     * permet d'initialiser la page.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * permet de charger la page en precisant les param necessaire.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $this->atexoTelechargement->initComposant();
    }
}
