<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;

/**
 * @author HANNAD Hajar <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 4
 */
class DownloadDocument extends DownloadFile
{
    public function onLoad($param)
    {
        if ($_GET['id'] && $_GET['org'] && $_GET['typeDoc']) {
            $this->downloadDocument1(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_Util::atexoHtmlEntities($_GET['typeDoc']), Atexo_Util::atexoHtmlEntities($_GET['idAvis']));
        }
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
    }

    public function downloadDocument1($consultationId, $org, $typeDoc, $idAvis = null)
    {
        $document = (new Atexo_Publicite_Avis())->retreiveListFormulaireLibreByTypeDocGenere($consultationId, $org, $typeDoc, null, $idAvis);
        if ($document) {
            $this->_idFichier = $document[0]->getAvis();
            $this->_nomFichier = $document[0]->getNomFichier();
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
        }
        exit;
    }
}
