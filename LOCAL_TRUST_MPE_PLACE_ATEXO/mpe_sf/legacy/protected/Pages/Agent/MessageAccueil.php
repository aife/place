<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTMessageAccueil;
use Application\Propel\Mpe\CommonTMessageAccueilQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;

/**
 * Class MessageAccueilInfo.
 *
 * @Package protected\pages\agent
 */
class MessageAccueil extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('GererMessagesAccueil')) {
            if (!$this->IsPostBack) {
                $messageType = $this->getTypeMessage();
                $this->comboTypeMessageAccueilO1->DataSource = $messageType;
                $this->comboTypeMessageAccueilO1->DataBind();
                $this->comboTypeMessageAccueilO2->DataSource = $messageType;
                $this->comboTypeMessageAccueilO2->DataBind();
                $this->comboTypeMessageAccueilO3->DataSource = $messageType;
                $this->comboTypeMessageAccueilO3->DataBind();

                $messageEntrepriseObj = Atexo_Message::retrieveMessageAccueilByDestinataire('entreprise', null);
                if ($messageEntrepriseObj instanceof CommonTMessageAccueil) {
                    $this->entreprise_messageBody->setText($messageEntrepriseObj->getContenu());
                    $this->comboTypeMessageAccueilO1->setSelectedValue($messageEntrepriseObj->getTypeMessage());
                } else {
                    if ('MSG_ACCUEIL_PONCTUEL' != Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE')) {
                        $this->entreprise_messageBody->setText(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE'));
                        $this->comboTypeMessageAccueilO1->setSelectedValue(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE_TYPE'));
                    }
                }

                $messageAgentObj = Atexo_Message::retrieveMessageAccueilByDestinataire('agent', 1);
                if ($messageAgentObj instanceof CommonTMessageAccueil) {
                    $this->agent_messageBody->setText($messageAgentObj->getContenu());
                    $this->comboTypeMessageAccueilO2->setSelectedValue($messageAgentObj->getTypeMessage());
                } else {
                    if ('MSG_ACCUEIL_PONCTUEL' != Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT')) {
                        $this->agent_messageBody->setText(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT'));
                        $this->comboTypeMessageAccueilO2->setSelectedValue(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT_TYPE'));
                    }
                }
                $messageAgentNonAuthentifieObj = Atexo_Message::retrieveMessageAccueilByDestinataire('agent', 0);
                if ($messageAgentNonAuthentifieObj instanceof CommonTMessageAccueil) {
                    $this->agentNonAuthentifie_messageBody->setText($messageAgentNonAuthentifieObj->getContenu());
                    $this->comboTypeMessageAccueilO3->setSelectedValue($messageAgentNonAuthentifieObj->getTypeMessage());
                } else {
                    if ('MSG_ACCUEIL_PONCTUEL' != Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT_NON_AUTHENTIFIE')) {
                        $this->agentNonAuthentifie_messageBody->setText(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT_NON_AUTHENTIFIE'));
                        $this->comboTypeMessageAccueilO3->setSelectedValue(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_AGENT_NON_AUTHENTIFIE_TYPE'));
                    }
                }
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentHome');
        }
    }

    /**
     * @return array
     */
    protected function getTypeMessage()
    {
        return ['info' => 'Informatif (bleu)', 'avertissement' => 'Avertissement (orange)', 'erreur' => 'Erreur (rouge)'];
    }

    public function onSaveClick($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $messageEntrepriseObj = Atexo_Message::retrieveMessageAccueilByDestinataire('entreprise', null);
        if (!$messageEntrepriseObj instanceof CommonTMessageAccueil) {
            $messageEntrepriseObj = new CommonTMessageAccueil();
            $messageEntrepriseObj->setDestinataire('entreprise');
            $messageEntrepriseObj->setConfig(Atexo_Config::getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL'));
        }
        $messageEntrepriseObj->setContenu($this->entreprise_messageBody->getText());

        if (isset($_POST['ctl0$CONTENU_PAGE$entreprise_messageBody'])) {
            $messageEntrepriseObj->setContenu($_POST['ctl0$CONTENU_PAGE$entreprise_messageBody']);
        }

        $messageEntrepriseObj->setTypeMessage($this->comboTypeMessageAccueilO1->getSelectedValue());
        $messageEntrepriseObj->save($connexion);
        CommonTMessageAccueilQuery::create()->reloadCachedMessageAccueil('entreprise', null, $messageEntrepriseObj);

        $messageAgentObj = Atexo_Message::retrieveMessageAccueilByDestinataire('agent', 1);
        if (!$messageAgentObj instanceof CommonTMessageAccueil) {
            $messageAgentObj = new CommonTMessageAccueil();
            $messageAgentObj->setDestinataire('agent');
            $messageAgentObj->setAuthentifier(1);
            $messageAgentObj->setConfig(Atexo_Config::getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL'));
        }
        $messageAgentObj->setContenu($this->agent_messageBody->getText());

        if (isset($_POST['ctl0$CONTENU_PAGE$agent_messageBody'])) {
            $messageAgentObj->setContenu($_POST['ctl0$CONTENU_PAGE$agent_messageBody']);
        }

        $messageAgentObj->setTypeMessage($this->comboTypeMessageAccueilO2->getSelectedValue());
        $messageAgentObj->save($connexion);
        CommonTMessageAccueilQuery::create()->reloadCachedMessageAccueil('agent', 1, $messageAgentObj);

        $messageAgentNonAuthentifieObj = Atexo_Message::retrieveMessageAccueilByDestinataire('agent', 0);
        if (!$messageAgentNonAuthentifieObj instanceof CommonTMessageAccueil) {
            $messageAgentNonAuthentifieObj = new CommonTMessageAccueil();
            $messageAgentNonAuthentifieObj->setDestinataire('agent');
            $messageAgentNonAuthentifieObj->setAuthentifier(0);
            $messageAgentNonAuthentifieObj->setConfig(Atexo_Config::getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL'));
        }
        $messageAgentNonAuthentifieObj->setContenu($this->agentNonAuthentifie_messageBody->getText());
        if (isset($_POST['ctl0$CONTENU_PAGE$agentNonAuthentifie_messageBody'])) {
            $messageAgentNonAuthentifieObj->setContenu($_POST['ctl0$CONTENU_PAGE$agentNonAuthentifie_messageBody']);
        }

        $messageAgentNonAuthentifieObj->setTypeMessage($this->comboTypeMessageAccueilO3->getSelectedValue());
        $messageAgentNonAuthentifieObj->save($connexion);
        CommonTMessageAccueilQuery::create()->reloadCachedMessageAccueil('agent', 0, $messageAgentNonAuthentifieObj);

        $this->javascriptLabel->Text = "<script>J('.demander-publication').dialog('destroy');</script>";
    }
}
