<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFormulaire;
use Application\Propel\Mpe\CommonCriteresEvaluation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluation;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use AtexoCrypto\Dto\Enveloppe;

/*
 * Created on 14 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */
class PopUpEvaluationOffre extends MpeTPage
{
    public $_criteresEval;
    public ?\Application\Propel\Mpe\CommonEnveloppe $_enveloppe = null;
    public $_uid_enveloppeFormulaire;
    public $_uidEnveloppeCritereEval;

    public function getUidEnveloppeCritereEval()
    {
        return $this->_uidEnveloppeCritereEval;
    }

    public function setUidEnveloppeCritereEval($value)
    {
        $this->_uidEnveloppeCritereEval = $value;
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);
            $this->ConsultationSummary->setConsultation($consultation);
            //Enveloppe :
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById(Atexo_Util::atexoHtmlEntities($_GET['idPli']), Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism());
            if ($enveloppe instanceof CommonEnveloppe) {
                $this->_enveloppe = $enveloppe;
                //Nom Entrp
                $this->nomEntrp->Text = $this->getNomEntreprise($enveloppe->getOffreId(), Atexo_CurrentUser::getCurrentOrganism()).'- El.'.Atexo_Util::atexoHtmlEntities($_GET['numPli']);
                //Criteres Evaluation
                $this->_criteresEval = (new Atexo_CriteresEvaluation())->retrieveCriteresEvaluationByRefConsAndTypeEnvAndLot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $enveloppe->getTypeEnv(), $enveloppe->getSousPli());

                if ($this->_criteresEval instanceof CommonCriteresEvaluation) {
                    if (!$this->isPostBack) {
                        $envCritEval = (new Atexo_CriteresEvaluation())->retreiveEnveloppeCriteval($this->_criteresEval->getId(), $this->_enveloppe->getIdEnveloppeElectro());
                        if (!$envCritEval) {
                            $envCritEval = new CommonEnveloppeCritereEvaluation();
                            $envCritEval->setIdEnveloppe($this->_enveloppe->getIdEnveloppeElectro());
                            $envCritEval->setIdcritereevaluation($this->_criteresEval->getId());
                            $envCritEval->setRejet(0);
                            $envCritEval->setStatutCritereEvaluation(1);
                            $envCritEval->setNoteTotale('0');
                            $envCritEval->setCommentaireTotal('');
                            $envCritEval->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                            $envCritEval->save($connexionCom);
                        }
                        if ($envCritEval instanceof CommonEnveloppeCritereEvaluation) {
                            $this->_uidEnveloppeCritereEval = Atexo_Util::builUidPlateforme($envCritEval->getId());
                        }
                        $bordereauPrix = (new Atexo_FormConsultation())->getFormconsByTypeFormRefConsLotAndTypeEnv(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $enveloppe->getTypeEnv(), Atexo_Config::getParameter('BORDEREAU_DE_PRIX'), $enveloppe->getSousPli());
                        if ($bordereauPrix instanceof CommonConsultationFormulaire) {
                            $envFormCons = (new Atexo_CriteresEvaluation())->retreiveEnveloppeFormulaireConsultationByIdEnv($bordereauPrix->getId(), Atexo_Util::atexoHtmlEntities($_GET['idPli']));
                            if ($envFormCons) {
                                $this->_uid_enveloppeFormulaire = Atexo_Util::builUidPlateforme($envFormCons->getId());
                            }
                        }
                    }
                } else {
                    $this->contenteval->setVisible(false);
                }
            }
        }
    }

    public function getDescriptionEnveloppe()
    {
        $descLot = '';
        if ($this->_criteresEval instanceof CommonCriteresEvaluation) {
            if ('0' != $this->_criteresEval->getLot()) {
                $descLot = (new Atexo_CriteresEvaluation())->getDescriptionLot($this->_criteresEval->getLot(), $this->_criteresEval->getOrganisme(), $this->_criteresEval->getConsultationId()).'- ';
            }

            return $descLot.$this->_criteresEval->getLibelleTypeEnveloppe();
        }

        return $descLot;
    }

    public function getNomEntreprise($offreID, $organisme)
    {
        $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($offreID, $organisme);

        return $offre->getNomEntrepriseInscrit();
    }

    /**
     * Retourne l'enveloppe electronique.
     */
    public function getEnveloppe()
    {
        return $this->_enveloppe;
    }

    /**
     * Retourne le critère d'évaluation.
     */
    public function getCritereEvaluation()
    {
        return $this->_criteresEval;
    }

    /**
     * Retourne l'uid de l'enveloppe formulaire encodé en base64.
     */
    public function getUidEnveloppeFormulaire()
    {
        return $this->_uid_enveloppeFormulaire;
    }

    /**
     * Permet de mettre à jour le critère d'évaluation.
     *
     * @param $sender
     * @param $param
     */
    public function updateEnvCriteresEvaluation($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $envCritEval = CommonEnveloppeCritereEvaluationPeer::retrieveByPK(Atexo_Util::builIdFromUidPlateforme($this->uidEnvCritereEvaluation->value), $connexion);
        if ($envCritEval instanceof CommonEnveloppeCritereEvaluation) {
            $envCritEval->setIdEnveloppe($this->idEnveloppe->value);
            $envCritEval->setIdcritereevaluation(Atexo_Util::builIdFromUidPlateforme($this->uidCriteresEvaluation->value));
            //if($this->rejet->value !== ''){
            $envCritEval->setRejet($this->rejet->value);
            //}
            $envCritEval->setStatutCritereEvaluation($this->statutCritereEvaluation->value);
            $envCritEval->setNoteTotale($this->noteTotale->value);
            $envCritEval->setCommentaireTotal($this->totalCommentaireAcheteur->value);
            $envCritEval->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
            $envCritEval->save($connexion);
        }
        //Mise à jour de la page
        $script = '<script>';
        $script .= "opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callBackbutton'])."').click();window.close();";
        $script .= '</script>';
        $this->javascript->Text = $script;
    }
}
