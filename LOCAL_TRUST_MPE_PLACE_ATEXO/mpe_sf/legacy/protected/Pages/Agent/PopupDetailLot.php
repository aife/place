<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\Om\BaseCommonConfigurationClientPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;

/**
 * Contient le detail des lot d'une consultation.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.0
 *
 * @since MPE-4.0
 */
class PopupDetailLot extends MpeTPage
{
    protected $reference;
    protected $organisme;
    protected ?\Application\Propel\Mpe\CommonConsultation $consultation = null;
    protected $langueEnSession;
    public ?string $_langue = null;

    /**
     *  recupère la langue.
     */
    public function getLangue()
    {
        return $this->_langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->_langue !== $langue) {
            $this->_langue = $langue;
        }
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    // setLangue()

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
            $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();
            $sessionLang = Atexo_CurrentUser::readFromSession('lang');
            if (isset($_GET['lang']) && in_array($_GET['lang'], $arrayActiveLangages)) {
                $this->langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            } elseif (isset($sessionLang) && in_array($sessionLang, $arrayActiveLangages)) {
                $this->langueEnSession = $sessionLang;
            } else {
                $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            }
        } else {
            $this->langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        // Enregistrement en session
        Atexo_CurrentUser::writeToSession('lang', $this->langueEnSession);

        $dataSource = [];
        $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);
        $this->reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->reference, $connexionCom);
        $this->consultation = $consultation;
        if ($consultation instanceof CommonConsultation) {
            if (!Atexo_Module::isEnabled('Publicite')
                || !$this->consultation->getDonneePubliciteObligatoire()) {
                $this->showHideDonnesRedac($this->consultation->getDonneeComplementaireObligatoire());
            } else {
                if (
                    $this->consultation->getDonneeComplementaireObligatoire()
                    ||
                    $this->consultation->getDonneePubliciteObligatoire()
                ) {
                    $this->script->Text .= '<script>activerBlocsDonneesComplementaires();</script>';
                } else {
                    $this->script->Text .= '<script>desactiverBlocsDonneesComplementaires();</script>';
                }
            }

            if (isset($_GET['idLot'])) {
                //afficher un seul lot
                $idLot = Atexo_Util::atexoHtmlEntities($_GET['idLot']);
                $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultation->getId(), $idLot, $consultation->getOrganisme());
                $dataSource[] = $categorieLot;
            } else {
                // afficher plusieurs lots
                $lots = $consultation->getAllLots($connexionCom, $consultation->getOrganisme(), $consultation->getId());
                $dataSource = $lots;
            }
        }
        $this->fillRepeaterDetailLots($dataSource, $consultation);
    }

    /*
     * permet de remplir le repeater des lot
     */
    public function fillRepeaterDetailLots($lots, $consultation = false)
    {
        $this->tableauDetailDesLots->DataSource = $lots;
        $this->tableauDetailDesLots->DataBind();
        $indexLot = 0;
        $afficherCodeCpv = false;
        if ($consultation instanceof CommonConsultation) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
            if (($procEquivalence instanceof CommonProcedureEquivalence)
                    && Atexo_Module::isEnabled('AffichageCodeCpv')
                    && $procEquivalence->getAfficherCodeCpv()) {
                $afficherCodeCpv = true;
            }
        }
        foreach ($this->tableauDetailDesLots->getItems() as $item) {
            if ($afficherCodeCpv) {
                $item->panelCodeRef->setVisible(true);
                //<!-- Module referentiel CPV
                $atexoRef = new Atexo_Ref();
                $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'));
                $atexoRef->setPrincipal($lots[$indexLot]->getCodeCpv1());
                $atexoRef->setSecondaires($lots[$indexLot]->getCodeCpv2());
                $item->idAtexoRef->afficherReferentiel($atexoRef);
            } else {
                $item->panelCodeRef->setVisible(false);
            }

            $item->ltRefLot->afficherReferentielConsultation($lots[$indexLot]->getConsultationId(), $lots[$indexLot]->getLot(), $lots[$indexLot]->getOrganisme());
            $item->idReferentielZoneTextLot->afficherTextConsultation($lots[$indexLot]->getConsultationId(), $lots[$indexLot]->getLot(), $lots[$indexLot]->getOrganisme());
            $item->idRefRadioLot->afficherReferentielRadio($lots[$indexLot]->getConsultationId(), $lots[$indexLot]->getLot(), $lots[$indexLot]->getOrganisme(), null, true);
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $item->achatResponsableConsultation->afficherClausesConsultation($lots[$indexLot]);
            }

            $item->donneeFormePrix->afficherDataFormeMarcheFormePrix($lots[$indexLot]->getDonneComplementaire());
            $item->donneeDureeMarche->setDonneeComplementaire($lots[$indexLot]->getDonneComplementaire());
            if (Atexo_Module::isEnabled('DonneesRedac')
                && Atexo_Module::isEnabled('Publicite')
                && $lots[$indexLot]->getDonneComplementaire()
            ) {
                $item->donneeMontantMarche->setDonneeComplementaire($lots[$indexLot]->getDonneComplementaire());
            }
            $item->donneeVariante->setDonneeComplementaire($lots[$indexLot]->getDonneComplementaire());
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                $donnneComplementarie = $lots[$indexLot]->getDonneComplementaire();
                if ($donnneComplementarie instanceof CommonTDonneeComplementaire) {
                    //caution provisoire
                    if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
                        $item->cautionProvisoire->Text = $donnneComplementarie->getCautionProvisoire();
                    }
                    //variante
                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                        if ('1' == $donnneComplementarie->getVariantes()) {
                            $item->variantesOui->Checked = true;
                        } else {
                            $item->variantesNon->Checked = true;
                        }
                    }
                    //Qualifications
                    if (Atexo_Module::isEnabled('ConsultationQualification')) {
                        $item->qualification->setObjet($lots[$indexLot]);
                        $item->qualification->setAfficher(true);
                        $item->qualification->chargerComposant();
                    } else {
                        $item->panelQualification->Visible = false;
                    }
                    //Agrements
                    if (Atexo_Module::isEnabled('ConsultationAgrement') && Atexo_Module::isEnabled('DonneesComplementaires')) {
                        $item->agrements->setAfficher(true);
                        $item->agrements->setObjet($lots[$indexLot]);
                        $item->agrements->chargerComposant();
                    } else {
                        $item->agrements->setAfficher(false);
                        $item->panelAgrement->Visible = false;
                    }
                    //Echantillons demandés
                    if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                        $item->echantillonsDemandes->setDonneesComplementaires($donnneComplementarie);
                        $item->echantillonsDemandes->setAfficher(true);
                        $item->echantillonsDemandes->setActif(false);
                        $item->echantillonsDemandes->setLangue($this->getLangue());
                        $item->echantillonsDemandes->setValidationGroup('validateInfosConsultation');
                        $item->echantillonsDemandes->chargerComposant();
                    }
                    //Reunions
                    if (Atexo_Module::isEnabled('ConsultationReunion')) {
                        $item->reunions->setDonneesComplementaires($donnneComplementarie);
                        $item->reunions->setAfficher(true);
                        $item->reunions->setActif(false);
                        $item->reunions->setLangue($this->getLangue());
                        $item->reunions->setValidationGroup('validateInfosConsultation');
                        $item->reunions->chargerComposant();
                    }
                    //Visites des lieux
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                        $item->visitesLieux->setDonneesComplementaires($donnneComplementarie);
                        $item->visitesLieux->setConsultation($this->consultation);
                        $item->visitesLieux->setLot($lots[$indexLot]->getLot());
                        $item->visitesLieux->setAfficher(true);
                        $item->visitesLieux->setActif(false);
                        $item->visitesLieux->setLangue($this->getLangue());
                        $item->visitesLieux->setValidationGroup('validateInfosConsultation');
                        $item->visitesLieux->chargerComposant();
                    }
                }
            }
            ++$indexLot;
        }
    }

    /*
     * permet de remplir le repeater des lot
     */
    public function afficherListOfLot()
    {
        return isset($_GET['idLot']) ? false : true;
    }

    /**
     * retourne la categorie du lot.
     */
    public function getCategoryLot($categorie)
    {
        $categorieConsultationObj = Atexo_Consultation_Category::retrieveCategorie($categorie, true);

        $abbreviationLangue = '';

        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $abbreviationLangue = strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }
        $libelleTraduit = 'getLibelle'.$abbreviationLangue;

        if ($categorieConsultationObj) {
            if ($categorieConsultationObj->$libelleTraduit()) {
                return $categorieConsultationObj->$libelleTraduit();
            } else {
                return $categorieConsultationObj->getLibelle();
            }
        }
    }

    /**
				 * Permet de recuperer le montant estime d'un lot.
				 *
				 * @param CommonCategorieLot $lot : l'objet lot
				 *
				 *
				 * @author Oumar KONATE <oumar.konate@atexo.com>
				 *
				 * @version 1.0
				 *
				 * @since 2016-les-echos
				 * @copyright Atexo 2016
				 */
				public function getValeurEstimeeLot($lot): float|string
    {
        $montant = '';
        if ($lot instanceof CommonCategorieLot) {
            $donneeComplementaire = $lot->getDonneComplementaire();
            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                $montant = Atexo_Util::getMontantArronditEspace($donneeComplementaire->getMontantMarche());
            }
        }

        return $montant;
    }

    /*
     * Retourne description du lot
     */
    public function getDescriptionLot($categorieLot)
    {
        $nomFonction = 'getDescription';
        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $nomFonction .= strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }

        if ($categorieLot) {
            return $categorieLot->$nomFonction() ?: $categorieLot->getDescription();
        }
    }

    /*
     * Retourne description du lot
     */
    public function getDescriptionLotOnInput($categorieLot)
    {
        $nomFonction = 'getDescription';
        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $nomFonction .= strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }

        if ($categorieLot) {
            return stripslashes(htmlspecialchars(($categorieLot->$nomFonction() ?: $categorieLot->getDescription())));
        }
    }

    /*
     * Retourne description détailé du lot
     */
    public function getDescriptionDetail($categorieLot)
    {
        $nomFonction = 'getDescriptionDetail';
        if ($this->langueEnSession != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $nomFonction .= strtoupper(substr($this->langueEnSession, 0, 1)).substr($this->langueEnSession, 1);
        }
        if ($categorieLot) {
            return $categorieLot->$nomFonction() ?: $categorieLot->getDescriptionDetail();
        }
    }

    public function showHideDonnesRedac($choix)
    {
        if ('1' == $choix) {
            $this->script->Text = "<script>
					J('.panel-donnees-redac .panel').css('display','block');
					J('.title-toggle').removeClass('title-toggle').addClass('title-toggle-open')
					J('.title-toggle-open').removeClass('detail-toggle-inactive');
					J('.donnees-redac').removeClass('panel-off');
					</script>";
        } elseif ('0' == $choix) {
            $this->script->Text = "<script>
					J('.panel-donnees-redac .panel').css('display','none');
					J('.title-toggle').addClass('detail-toggle-inactive');
					J('.donnees-redac').addClass('panel-off');
					J('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
				   </script>";
        }
    }
}
