<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_SocietesExclues;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger les documents des sociétés exclues
 * Attention à ne pas passer l'id du blob en parametre à la fonction downloadDocument() pour des mésures de securité.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DownloadDocumentSocietesExclues extends DownloadFile
{
    public function onLoad($param)
    {

        if ($_GET['id']) {
            $this->downloadDocument(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    public function downloadDocument($idSocietesExclues)
    {
        $societesExclues = (new Atexo_SocietesExclues())->retrieveSocietesExcluesByPk($idSocietesExclues);
        if ($societesExclues) {
            $this->_idFichier = $societesExclues->getIdBlob();
            $this->_nomFichier = Atexo_Util::utf8ToIso($societesExclues->getNomDocument());
            $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        }
        exit;
    }
}
