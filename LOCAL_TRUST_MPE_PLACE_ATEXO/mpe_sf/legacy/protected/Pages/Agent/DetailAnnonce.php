<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Validation;
use Exception;
use Prado\Prado;

/**
 * Contient le detail d'une Annonce.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailAnnonce extends MpeTPage
{
    public $_consultation;
    public $_refConsultation;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (0 == strcmp($_GET['id'], $_GET['id'])) {
            $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($this->_consultation) {
                $this->IdConsultationSummary->setReference($this->_refConsultation);
                if (Atexo_Module::isEnabled('AfficherBlocActionsDansDetailsAnnonces')) {
                    $this->displayActions();
                }
            } else {
                $this->panelDetailsConsultation->visible = false;
                $this->errorPanel->visible = true;
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        $this->_consultation = array_shift($consultationArray);

        return $this->_consultation;
    }

    public function displayActions()
    {
        $activeLangues = (new Atexo_Languages())->getAllActiveLanguagesButCurrentOne();

        if (!is_array($activeLangues)) {
            $activeLangues = [];
        }

        $this->repeaterTraduirePreparation->DataSource = $activeLangues;
        $this->repeaterTraduirePreparation->DataBind();

        $this->repeaterTraduireConsultation->DataSource = $activeLangues;
        $this->repeaterTraduireConsultation->DataBind();

        if (!Atexo_Module::isEnabled('TraduireConsultation')) {
            foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                $item->panelTraduire->setVisible(false);
            }
            foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                $item->panelTraduire->setVisible(false);
            }
        }

        $etatConsultation = $this->_consultation->getStatusConsultation();
        if ($etatConsultation == Atexo_Config::getParameter('ETAT_EN_ATTENTE')) {
            $this->etape3_annonces->setId('etape1_annonces');
            if (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelModifierPreparation->setCssClass('picto-link');
                    $this->ImageModifierPreparation->setActivate('true');
                    $this->ImageModifierPreparation->setClickable('true');
                    $this->linkModifierPreparation->setVisible(true);
                    $this->labelModifierPreparation->setVisible(false);
                } else {
                    $this->ImageModifierPreparation->setActivate('false');
                    $this->ImageModifierPreparation->setClickable('false');
                }
                $this->linkModifierPreparation->Text = Prado::localize('BOUTON_MODIFIER');
            }

            if ($this->_consultation->getValidationHabilitation()) {
                $this->panelValidate->setVisible(true);
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelValidate->setCssClass('picto-link');
                    $this->ImageValidate->setActivate('true');
                    $this->ImageValidate->setClickable('true');
                    $this->linkValidate->setVisible(true);
                    $this->linkValidate->setNavigateUrl(Atexo_Consultation_Validation::validateConsultation($this->_consultation->getId()));
                    $this->labelValidate->setVisible(false);
                } else {
                    $this->ImageValidate->setActivate('false');
                    $this->ImageValidate->setClickable('false');
                }
            } else {
                $this->panelValidate->setVisible(true);
                $this->ImageValidate->setActivate('false');
                $this->ImageValidate->setClickable('false');
            }

            if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelPublicitePreparation->setCssClass('picto-link');
                    $this->ImagePublicitePreparation->setActivate('true');
                    $this->ImagePublicitePreparation->setClickable('true');
                    $this->linkPublicitePreparation->setVisible(true);
                    $this->labelPublicitePreparation->setVisible(false);
                } else {
                    $this->ImagePublicitePreparation->setActivate('false');
                    $this->ImagePublicitePreparation->setClickable('false');
                }
            }

            if (!$this->_consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                $this->panelDeleteConsultation->setCssClass('picto-link');
                $this->ImageDeleteConsultation->setActivate('true');
                $this->ImageDeleteConsultation->setClickable('true');
                $this->ImageDeleteConsultation->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->linkDeleteConsultation->setVisible(true);
                $this->linkDeleteConsultation->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->labelDeleteConsultation->setVisible(false);
            } else {
                $this->ImageDeleteConsultation->setActivate('false');
                $this->ImageDeleteConsultation->setClickable('false');
            }

            if (Atexo_Module::isEnabled('TraduireConsultation')) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $item->panelTraduire->setCssClass('picto-link');
                        $item->ImageTraduire->setActivate('true');
                        $item->ImageTraduire->setClickable('true');
                        $item->linkTraduire->setVisible(true);
                        $item->labelTraduire->setVisible(false);
                    }
                } else {
                    foreach ($this->repeaterTraduirePreparation->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $this->ImageTraduire->setActivate('false');
                        $this->ImageTraduire->setClickable('false');
                    }
                }
            }
        } elseif ($etatConsultation == Atexo_Config::getParameter('ETAT_EN_LIGNE')) {
            $this->etape3_annonces->setId('etape2_annonces');

            if ($this->_consultation->hasHabilitationModifierApresValidation()) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelModifierConsultation->setCssClass('picto-link');
                    $this->ImageModifierConsultation->setActivate('true');
                    $this->ImageModifierConsultation->setClickable('true');
                    $this->linkModifierConsultation->setVisible(true);
                    $this->labelModifierConsultation->setVisible(false);
                } else {
                    $this->ImageModifierConsultation->setActivate('false');
                    $this->ImageModifierConsultation->setClickable('false');
                }
            }

            if (Atexo_CurrentUser::hasHabilitation('PublierConsultation')) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelPubConsultation->setCssClass('picto-link');
                    $this->ImagePubConsultation->setActivate('true');
                    $this->ImagePubConsultation->setClickable('true');
                    $this->linkPubConsultation->setVisible(true);
                    $this->labelPubConsultation->setVisible(false);
                } else {
                    $this->ImagePubConsultation->setActivate('false');
                    $this->ImagePubConsultation->setClickable('false');
                }
            }

            if (!$this->_consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                $this->panelDeleteConsultationEnLigne->setCssClass('picto-link');
                $this->ImageDeleteConsultationEnLigne->setActivate('true');
                $this->ImageDeleteConsultationEnLigne->setClickable('true');
                $this->ImageDeleteConsultationEnLigne->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->linkDeleteConsultationEnLigne->setVisible(true);
                $this->linkDeleteConsultationEnLigne->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->labelDeleteConsultationEnLigne->setVisible(false);
            } else {
                $this->ImageDeleteConsultationEnLigne->setActivate('false');
                $this->ImageDeleteConsultationEnLigne->setClickable('false');
            }

            if (Atexo_Module::isEnabled('TraduireConsultation')) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $item->panelTraduire->setCssClass('picto-link');
                        $item->ImageTraduire->setActivate('true');
                        $item->ImageTraduire->setClickable('true');
                        $item->linkTraduire->setVisible(true);
                        $item->labelTraduire->setVisible(false);
                    }
                } else {
                    foreach ($this->repeaterTraduireConsultation->getItems() as $item) {
                        $item->panelTraduire->setVisible(true);
                        $item->ImageTraduire->setActivate('false');
                        $item->ImageTraduire->setClickable('false');
                    }
                }
            }
        } elseif ($etatConsultation == Atexo_Config::getParameter('ETAT_CLOTURE')) {
            $this->etape3_annonces->setId('etape3_annonces');

            if ($this->_consultation->hasHabilitationModifierApresValidation()) {
                if (!$this->_consultation->getCurrentUserReadOnly()) {
                    $this->panelModifierOuverture->setCssClass('picto-link');
                    $this->ImageModifierOuverture->setActivate('true');
                    $this->ImageModifierOuverture->setClickable('true');
                    $this->linkModifierOuverture->setVisible(true);
                    $this->labelModifierOuverture->setVisible(false);
                } else {
                    $this->ImageModifierOuverture->setActivate('false');
                    $this->ImageModifierOuverture->setClickable('false');
                }
            }

            if (!$this->_consultation->getCurrentUserReadOnly() && Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')) {
                $this->panelDeleteConsultationHorsLigne->setCssClass('picto-link');
                $this->ImageDeleteConsultationHorsLigne->setActivate('true');
                $this->ImageDeleteConsultationHorsLigne->setClickable('true');
                $this->ImageDeleteConsultationHorsLigne->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->linkDeleteConsultationHorsLigne->setVisible(true);
                $this->linkDeleteConsultationHorsLigne->Attributes->onclick = "return confirm('Etes-vous certain de vouloir supprimer cette annonce?');";
                $this->labelDeleteConsultationHorsLigne->setVisible(false);
            } else {
                $this->ImageDeleteConsultationHorsLigne->setActivate('false');
                $this->ImageDeleteConsultationHorsLigne->setClickable('false');
            }
        }
    }

    public function redirectTo($sender, $param)
    {
        $url = $param->CommandName;
        $this->response->redirect($url);
    }

    public function deleteAvis($sender, $param)
    {
        $typeAnnonce = null;
        try {
            $consultationId = $param->CommandParameter;
            $organisme = Atexo_CurrentUser::getCurrentOrganism();

            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $consultationObj = CommonConsultationPeer::retrieveByPK($consultationId, $connexionCom);
            if ($consultationObj) {
                if ($consultationObj->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                    $typeAnnonce = Atexo_Config::getParameter('TYPE_AVIS_INFORMATION');
                } elseif ($consultationObj->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) {
                    $typeAnnonce = Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION');
                }
            }
            $consultation = new Atexo_Consultation();
            $consultation->deleteConsultation($consultationId, $organisme);
        } catch (Exception $e) {
            echo $e;
            exit;
        }

        $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$typeAnnonce);
    }
}
