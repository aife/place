<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Exceptions\THttpException;

class deleteFavoris extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom(strtolower(Atexo_CurrentUser::getRole()));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::isAgent() && Atexo_CurrentUser::isConnected()) {
            if (!$this->isPostBack) {
                if (isset($_POST['data'])) {
                    $data = explode(',', base64_decode($_POST['data']));
                    if (2 != count($data)) {
                        throw new THttpException(500, 'Problème de mise à jour de la consultation favoris');
                    }

                    $reference = $data[0];
                    $user = $data[1];

                    if ($user === Atexo_CurrentUser::getId() && Atexo_CurrentUser::isAgent()) {
                        try {
                            $list = Atexo_Util::getSfService('atexo.consultation_favoris')->deleteFavoris($reference, $user);

                            header('Content-type: application/json');
                            echo json_encode($list, JSON_THROW_ON_ERROR);
                            exit;
                        } catch (Exception) {
                            throw new THttpException(500, 'Problème de mise à jour de la consultation favoris');
                        }
                    }
                }
            }
        } else {
            throw new THttpException(403, 'Vous n avez pas les droits');
        }
    }
}
