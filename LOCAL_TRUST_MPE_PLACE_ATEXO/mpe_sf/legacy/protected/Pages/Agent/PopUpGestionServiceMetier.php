<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpGestionServiceMetier extends MpeTPage
{
    protected $idAgent;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $agentConnectedO = new CommonAgent();
        $agentConnectedO->setId(Atexo_CurrentUser::getIdAgentConnected());
        $agentConnectedO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $agentConnectedO->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $this->idAgent = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $agentToUpO = (new CommonAgentQuery())->findOneById($this->idAgent);
        if (!(new Atexo_Agent())->verifyCapacityToModify($agentConnectedO, $agentToUpO) && !Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->panelMessage->setVisible('true');
            $this->servicesMetiersAccessibles->setVisible('false');
            $this->panelMessage->setMessage(Prado::localize('TEXT_PAS_AUTORISE_MODIFIER_INFORMATIONS_AGENT'));
            $this->panelApplicationsMetier->visible = false;
        } else {
            $this->panelMessage->setVisible('false');
            $this->servicesMetiersAccessibles->setOrg(Atexo_CurrentUser::getOrganismAcronym());
            $this->servicesMetiersAccessibles->setVisible('true');
            $this->servicesMetiersAccessibles->setIdAgent($this->idAgent);
            if (!$this->IsPostBack) {
                $this->servicesMetiersAccessibles->displayCompenenant();
            }
        }
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function updateServicesMetiers($sender, $param)
    {
        if ($this->IsValid) {
            if ($this->idAgent) {
                $this->servicesMetiersAccessibles->onEnregistrerClick($this->idAgent);
            }
            $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }
}
