<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class downloadArchives extends MpeTPage
{
    protected string $critereTri = '';
    protected string $triAscDesc = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $retour = $this->listDir(Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_TELECHARGEMENT').Atexo_CurrentUser::getOrganismAcronym());
        if ('<ul></ul>' == $retour) {
            $this->panelMessageAvertissement->setVisible(true);
            $this->panelMessageAvertissement->setMessage(Prado::localize('AUCUN_ARCHIVE_GENERE'));
        }
    }

    /**
     * Retourne une chaine HTML Decrivant l'arborescence d'un repertoire.
     *
     * @param repertoire
     */
    public function listDir($name, $level = 0, $div = 0)
    {
        $html = null;
        if (is_dir($name)) {
            if ($dir = opendir($name)) {
                $r = $level;
                $f = $level;
                $html .= '<ul>';
                while ($file = readdir($dir)) {
                    $style = 'style="display:none;"';
                    if ('.' != $file && '..' != $file) {
                        if (is_dir($name.'/'.$file) && !in_array($name.'/'.$file, ['.', '..'])) {
                            $html .= '<li style="display:block;" id="rep'.$r.'" ><img src="'.$this->themesPath().'/images/picto-plus.gif" onclick="openSousCat(this,\'div'.$r.$div.'\')" alt="Ouvrir/Fermer" title="Ouvrir/Fermer">'.$file;
                            $html .= '<div id="div'.$r.$div.'" '.$style.' >';
                            $html .= $this->listDir($name.'/'.$file, $level + 1, $r.$div);
                            $html .= '</div></li>';
                            ++$r;
                        } else {
                            $html .= '<li style="display:block;" id="'.$file.'"  >'.
                            '<img src="'.$this->themesPath().'/images/picto-tiret.gif" alt="Ouvrir/Fermer" title="Ouvrir/Fermer">  '.
                            '<a href="index.php?page=Agent.DownloadFileArchive&file='.base64_encode($name.'/'.$file).'" alt="télécharger" title="télécharger">'.$file.
                            '</a>';
                            ++$f;
                        }
                    }
                }
                $html .= '</ul>';
                closedir($dir);
            }
        } else {
            $this->panelMessageAvertissement->setVisible(true);
            $this->panelMessageAvertissement->setMessage(Prado::localize('REPERTOIRE_NON_TROUVE'));
        }

        return $html;
    }
}
