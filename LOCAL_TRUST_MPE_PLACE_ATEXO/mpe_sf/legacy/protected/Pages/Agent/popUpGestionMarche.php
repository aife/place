<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonMarche;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpGestionMarche extends MpeTPage
{
    private $_idServiceAgent;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->pmiPmeList->setDataSource($this->getDataSourcePmePmi());
            $this->pmiPmeList->dataBind();
            $this->getTrancheBudgetaire($this->trancheBudgetaire, true);
            $categories = Atexo_Consultation_Category::retrieveCategories(false, false, Atexo_CurrentUser::readFromSession('lang'));
            $this->naturePrestations->setDataSource($categories);
            $this->naturePrestations->dataBind();

            if (isset($_GET['idMarche'])) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $marche = CommonMarchePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idMarche'])), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                $this->populateForm($marche);
                $this->setViewState('marche', $marche);
            } elseif (isset($_GET['annee'])) {
                $this->setViewState('annee', Atexo_Util::atexoHtmlEntities($_GET['annee']));
            }
        } else {
            $this->getTrancheBudgetaire($this->trancheBudgetaire, 'encod');
        }
    }

    public function getIdServiceAgent()
    {
        if (null == $this->_idServiceAgent) {
            $this->_idServiceAgent = 0 == Atexo_CurrentUser::getIdServiceAgentConnected() ? null : Atexo_CurrentUser::getIdServiceAgentConnected();
        }

        return $this->_idServiceAgent;
    }

    public function populateForm(CommonMarche $marche)
    {
        if ($marche) {
            $this->titrePopup->Text = Prado::localize('MODIFIER_UN_MARCHE');
            $this->nomAttributaire->Text = $marche->getNomattributaire();
            $this->cp->Text = $marche->getCodepostal();
            $this->ville->Text = $marche->getVille();
            $this->dateNotification->Text = Atexo_Util::iso2frnDate($marche->getDatenotification());
            $this->getTrancheBudgetaire($this->trancheBudgetaire, true);
            if ($marche->getIdmarchetranchebudgetaire()) {
                $this->trancheBudgetaire->setSelectedValue($marche->getIdmarchetranchebudgetaire());
                $borne = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireById($marche->getIdmarchetranchebudgetaire(), Atexo_CurrentUser::getCurrentOrganism());
                $borneSup = $borne['BorneSup'];
                if ($borneSup) {
                    $borneSup = Atexo_Util::formatterMontant($borne['BorneSup'], 2, true);
                }
                $borneInf = Atexo_Util::formatterMontant($borne['BorneInf'], 2, true);
                $this->bornSuperieur->Value = $borneSup;
                $this->bornInferieur->Value = $borneInf;
            }
            if ($marche->getNaturemarche()) {
                $this->naturePrestations->setSelectedValue($marche->getNaturemarche());
            }
            $this->objetMarche->Text = $marche->getObjetmarche();
            $this->montant->Text = Atexo_Util::getMontantArronditEspace($marche->getMontantmarche());
            if ($marche->getPmepmi()) {
                $this->pmiPmeList->setSelectedValue($marche->getPmepmi());
            }
        }
    }

    private function getDataSourcePmePmi()
    {
        $arrayReferentiel = (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_PME_PMI'), Atexo_CurrentUser::getCurrentOrganism());

        return $arrayReferentiel;
    }

    public function valider($sender, $param)
    {
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $marche = $this->getViewState('marche');
            if (null == $marche) {
                $marche = new CommonMarche();
                $marche->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $marche->setNumeromarcheannee($this->getViewState('annee'));
                $marche->setIdservice($this->getIdServiceAgent());
                $marche->setIsmanuel('1');
            }

            $anneeNotification = explode('/', $this->dateNotification->Text);

            if ($anneeNotification[2] != $marche->getNumeromarcheannee()) {
                $this->divValidationSummary->addServerError(Prado::localize('VERIFIER_DATE_NOTIFICATION').$marche->getNumeromarcheannee(), false);
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';
	        	</script>";

                return;
            }
            $marche->setNomattributaire($this->nomAttributaire->Text);
            $marche->setCodepostal($this->cp->Text);
            $marche->setVille($this->ville->Text);
            $marche->setDatenotification(Atexo_Util::frnDate2iso($this->dateNotification->Text));
            $marche->setIdmarchetranchebudgetaire($this->trancheBudgetaire->getSelectedValue());
            $marche->setNaturemarche((int) $this->naturePrestations->getSelectedValue());
            $marche->setObjetmarche($this->objetMarche->Text);
            $montant = str_replace(' ', '', $this->montant->Text);
            $marche->setMontantmarche(str_replace('.', ',', Atexo_Util::atexoHtmlEntities($montant)));
            $marche->setPmepmi($this->pmiPmeList->getSelectedValue());
            if ($this->blocEntreprise->attributaire_Nationale->checked) {
                if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                    $marche->setRcVilleAttributaire($this->blocEntreprise->rcVille->selectedItem->Text);
                    $marche->setRcNumAttributaire($this->blocEntreprise->rcVille->selectedValue.'-'.$this->blocEntreprise->numero_Rc->Text);
                    $marche->setSirenAttributaire('');
                    $marche->setNicAttributaire('');
                } else {
                    $marche->setSirenAttributaire($this->blocEntreprise->siren->Text);
                    $marche->setNicAttributaire($this->blocEntreprise->siret->Text);
                    $marche->setRcVilleAttributaire('');
                    $marche->setRcNumAttributaire('');
                }
                $marche->setIdentifiantNationalAttributaire('');
                $marche->setAcronymepaysAttributaire(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                $marche->setPaysAttributaire(Atexo_Util::encodeToHttp(Prado::localize('DEFINE_FRANCE')));
            } elseif ($this->blocEntreprise->attributaire_etranger->checked) {
                $nomPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->blocEntreprise->listPays->SelectedValue);
                $marche->setPaysAttributaire($nomPays);
                $acronyme = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryById($this->blocEntreprise->listPays->SelectedValue);
                $marche->setAcronymePaysAttributaire($acronyme);
                if ($this->blocEntreprise->identifiant_national->Text) {
                    $marche->setIdentifiantnationalAttributaire($this->blocEntreprise->identifiant_national->Text);
                }
                $marche->setSirenAttributaire('');
                $marche->setNicAttributaire('');
                $marche->setRcVilleAttributaire('');
                $marche->setRcNumAttributaire('');
            }
            $marche->save($connexionCom);
            $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }

    public function getTrancheBudgetaire($sender, $param)
    {
        $annee = $this->dateNotification->Text;
        $idTranche = $this->trancheBudgetaire->SelectedValue;
        $tranchesBudg = [];
        if ($annee) {
            $annee = Atexo_Util::getAnneeFromDate2($annee);
        }
        if ((!$this->IsCallback && !$this->IsPostback) || ('encod' != $param)) {
            $tranchesBudg = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireByOrgAnne($annee, Atexo_CurrentUser::getCurrentOrganism(), Prado::localize('MESSAGE_VALEUR_TOUTE_TRANCHES'));
        } else {
            $tranchesBudg = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireByOrgAnne($annee, Atexo_CurrentUser::getCurrentOrganism(), Prado::localize('MESSAGE_VALEUR_TOUTE_TRANCHES'), true);
        }

        $this->trancheBudgetaire->setDataSource($tranchesBudg);
        $this->trancheBudgetaire->dataBind();
        if ($idTranche) {
            $value = '';
            foreach ($tranchesBudg as $KeyTranche => $value) {
                if ($KeyTranche == $idTranche) {
                    $this->trancheBudgetaire->SelectedValue = $idTranche;
                }
            }
        }
    }

    /*
     * Permet de gerer la class Css du code postale
     *
     */
    public function getCssCp()
    {
        if ($this->blocEntreprise->attributaire_Nationale->checked) {
            $this->cp->Enabled = true;

            return 'cp-long';
        } else {
            $this->cp->Enabled = false;

            return 'cp-long disabled';
        }
    }

    /**
     * permet de remplir les valeurs des bornes inferieur / superieur au chargement de la poge.
     */
    public function setBornsTrancheBudgetaire($sender, $param)
    {
        $idTranche = $sender->SelectedValue;
        $borne = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireById($idTranche, Atexo_CurrentUser::getCurrentOrganism());
        $borneSup = $borne['BorneSup'];
        if ($borneSup) {
            $borneSup = Atexo_Util::formatterMontant($borne['BorneSup'], 2, true);
        }
        $borneInf = Atexo_Util::formatterMontant($borne['BorneInf'], 2, true);
        $this->bornSuperieur->Value = $borneSup;
        $this->bornInferieur->Value = $borneInf;
    }
}
