<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAdresseFacturationJal;
use Application\Propel\Mpe\CommonAdresseFacturationJalPeer;
use Application\Propel\Mpe\Om\BaseCommonAdresseFacturationJalPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/*
 * Created on 25 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class popUpAjoutAdresseFacturationJal extends MpeTPage
{
    protected $_idService = null;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['idService']) && !empty($_GET['idService'])) {
            $this->setIdService(Atexo_Util::atexoHtmlEntities($_GET['idService']));
        } else {
            $this->setIdService(null);
        }
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                self::displayJal(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
            $this->facurationSipOui->checked = 'true';
        }
    }

    public function setIdService($val)
    {
        $this->_idService = $val;
    }

    public function getIdService()
    {
        return $this->_idService;
    }

    public function displayJal($idAdresseFacturation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $adresseFacturation = BaseCommonAdresseFacturationJalPeer::retrieveByPK($idAdresseFacturation, Atexo_CurrentUser::getOrganismAcronym(), $connexionCom);
        if ($adresseFacturation) {
            $this->emailAr->Text = $adresseFacturation->getEmailAr();
            $this->telecopieur->Text = $adresseFacturation->getTelecopie();
            $this->infosFacturation->Text = $adresseFacturation->getInformationFacturation();
            if ('0' == $adresseFacturation->getFacturationSip()) {
                $this->facurationSipNon->checked = 'true';
            } else {
                $this->facurationSipOui->checked = 'true';
            }
        }
    }

    public function saveAdresseFacturationJal()
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (!isset($_GET['id'])) {
                $adresseFactObj = new CommonAdresseFacturationJal();
                $adresseFactObj->setServiceId($this->getIdService());
            } else {
                $adresseFactObj = CommonAdresseFacturationJalPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getOrganismAcronym(), $connexionCom);
            }
            $adresseFactObj->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $adresseFactObj->setEmailAr(trim($this->emailAr->Text));
            $adresseFactObj->setTelecopie($this->telecopieur->Text);
            $adresseFactObj->setInformationFacturation($this->infosFacturation->Text);
            if ($this->facurationSipOui->checked) {
                $adresseFactObj->setFacturationSip('1');
            } else {
                $adresseFactObj->setFacturationSip('0');
            }
            if ($this->logo->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'logo_'.session_id().time();
                if (move_uploaded_file($this->logo->LocalName, $infile)) {
                    $atexoBlob = new Atexo_Blob();
                    $Org = Atexo_CurrentUser::getOrganismAcronym();
                    $idBlob = $atexoBlob->insert_blob($this->logo->FileName, $infile, $Org);
                    $adresseFactObj->setIdBlobLogo($idBlob);
                    $adresseFactObj->setNomFichier($this->logo->FileName);
                }
                if (is_file($infile)) {
                    unlink($infile);
                }
            }
            $adresseFactObj->save($connexionCom);
            echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateAdresseFacturationJal').click();window.close();</script>";
        } catch (Exception $e) {
            Prado::log("Impossible de creer ue adresse de facturation '-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }
}
