<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/*
 * Created on 13 juin 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class RedactionPiecesConsultation extends MpeTPage
{
    private $_consultation = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_consultation = $this->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        if ($this->_consultation instanceof CommonConsultation) {
            $this->mainPanel->visible = true;
            if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
               || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                $urlRetour = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().
                                '&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $urlRetour = 'index.php?page=Agent.TableauDeBord&AS=0&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            } else {
                $urlRetour = '#';
            }
            $this->boutonRetour->setNavigateUrl($urlRetour);
            $this->boutonRetourH->setNavigateUrl($urlRetour);
            $this->errorPanel->visible = false;
        } else {
            $this->mainPanel->visible = false;
            $this->errorPanel->visible = true;
            $this->errorPanel->setMessage('Error Consultation');

            return;
        }

        if ('DEFINE_TEXT_ORME' != Prado::localize('DEFINE_TEXT_ORME')) {
            $this->breadcrumbORME->visible = true;
        }
    }

    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    public function actionBouttonAnnuler()
    {
        if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
        || $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
            $link = 'index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$this->_consultation->getIdTypeAvis().'&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            $this->response->redirect($link);
        } elseif ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $link = 'index.php?page=Agent.TableauDeBord&AS=0&id='.Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            $this->response->redirect($link);
        }
    }
}
