<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Agent\Atexo_Agent_AssociationComptes;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Echange pour association des comptes.
 *
 * @author khadija chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @since MPE-4.4
 */
class EchangeAssociationsCompte extends MpeTPage
{
    public ?string $message = null;

    public function onInit($param)
    {
        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities('agent'));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['uid'])) {
            $identifiant = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['uid']));
            $action = substr($identifiant, -1, 1);
            $uid = substr($identifiant, 0, -2);
            $agAssocie = (new Atexo_Agent_AssociationComptes())->getAgentByUI($uid);
            if ((1 == $action) && $agAssocie) {
                $this->message = Prado::localize('TEXT_DEMANDE_ASSOCIATION_CONFIRME');
                $notificaton = Prado::localize('TEXT_GESTION_ASSOCIATIONS_COMPTE_SUIVANTS');
                $message = Prado::localize('TEXT_MESSAGE_ASSOCIATION_CONFIRME');
                $this->afficherBlocs($agAssocie->getComptePrincipal(), $agAssocie->getCompteSecondaire(), $notificaton, $message);
                (new Atexo_Agent_AssociationComptes())->activerAssociation($uid);
            } elseif ((0 == $action) && $agAssocie) {
                $this->message = Prado::localize('TEXT_DEMANDE_ASSOCIATION_ANNULE');
                $notificaton = Prado::localize('TEXT_GESTION_ASSOCIATIONS_COMPTE_SUIVANTS');
                $message = Prado::localize('TEXT_MESSAGE_ASSOCIATION_ANNULE');
                $this->afficherBlocs($agAssocie->getComptePrincipal(), $agAssocie->getCompteSecondaire(), $notificaton, $message);
                (new Atexo_Agent_AssociationComptes())->supprimerAssociation($uid);
            } else {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentHome');
            }
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.AgentHome');
        }
    }

    public function afficherBlocs($idAgentPrincipale, $idAgentSecondaire, $notificaton, $message)
    {
        $this->blocInformationsAgent->chargerAgent($idAgentPrincipale, $notificaton);
        $this->blocInformationsAssocie->chargerAgent($idAgentSecondaire, $message);
    }
}
