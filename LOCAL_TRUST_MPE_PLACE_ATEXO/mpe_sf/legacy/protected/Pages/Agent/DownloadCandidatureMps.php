<?php

namespace Application\Pages\Agent;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/*
 * permet de recuperer le fichier CandidatureMps d'une offre
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class DownloadCandidatureMps extends DownloadFile
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Cette methode permet telecharger la candidature Mps lie a l'offre passee en param(url).
     *
     * @param void
     *
     * @return stream candidatureMps le fichier candidatureMps lié a l'offre passee en param(url)
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        // Seul les agents ayant le droit de voir la consultation qui peuvent télécharger autres pieces
        $consultation = new Atexo_Consultation();
        $consultationCriteria = new Atexo_Consultation_CriteriaVo();
        $consultationCriteria->setIdReference($consultationId);
        $consultationCriteria->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $consultationCriteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $consultationCriteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationFound = $consultation->search($consultationCriteria);
        if (count($consultationFound) > 0) {
            $idOffre = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOffre']));
            $candidatureMpsObject = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($idOffre, base64_decode($_GET['id']));
            if ($candidatureMpsObject) {
                $idFichier = $candidatureMpsObject->getIdBlob();
                $nomFichier = Prado::localize('DEFINE_CANDIDATURE_MPS');
                $this->downloadFiles($idFichier, $nomFichier, Atexo_CurrentUser::getOrganismAcronym());
                exit;
            }
        }
    }
}
