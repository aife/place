<?php

namespace Application\Pages\Agent;

use Application\Controls\BandeauAgent;
use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonTMessageAccueil;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Page d'accueil non authentifie des agents.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AccueilAgentAuthentifie extends MpeTPage
{
    public function onInit($param)
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT') . 'agent');
    }
}
