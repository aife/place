<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonGroupeMoniteur;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Moniteur;

/**
 * Classe PopUpGroupeMoniteur.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpGroupeMoniteur extends MpeTPage
{
    private $_idService = null;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (!empty($_GET['idService'])) {
            $this->_idService = Atexo_Util::atexoHtmlEntities($_GET['idService']);
        }
        if (!$this->IsPostBack) {
            if (isset($_GET['id'])) {
                self::displayGroupe(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
        }
    }

    public function saveGroupe()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (!isset($_GET['id'])) {
            $groupe = new CommonGroupeMoniteur();
            $groupe->setServiceId($this->_idService);
        } else {
            $groupe = Atexo_Publicite_Moniteur::retrieveGroupeById(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
        $groupe->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $groupe->setIdentifiant($this->identifant->Text);
        $groupe->setMdp($this->passwordMoniteur->Text);
        $groupe->setNumAbonnement($this->numAbonnement->Text);
        $groupe->setNumAbonne($this->numAbonne->Text);
        $groupe->save($connexionCom);
        echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_validateGroupe').click();window.close();</script>";
    }

    public function displayGroupe($IdGroupe)
    {
        $groupe = Atexo_Publicite_Moniteur::retrieveGroupeById($IdGroupe);

        if ($groupe) {
            $this->identifant->Text = $groupe->getIdentifiant();
            $this->passwordMoniteur->Text = $groupe->getMdp();
            $this->numAbonnement->Text = $groupe->getNumAbonnement();
            $this->numAbonne->Text = $groupe->getNumAbonne();
        }
    }
}
