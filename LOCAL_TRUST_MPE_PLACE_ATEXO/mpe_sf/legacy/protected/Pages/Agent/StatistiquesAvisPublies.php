<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class StatistiquesAvisPublies extends MpeTPage
{
    public string $_useAjax = 'false';
    public int $_sousTotalAoInf = 0;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->formulaireRechercheStatistique->setDateDebut('01/01/'.date('Y'));
            $this->formulaireRechercheStatistique->setDateFin(date('d/m/Y'));
            $this->formulaireRechercheStatistique->setStatistiqueTransversales((isset($_GET['allOrgs']) ? 'true' : 'false'));
            $this->formulaireRechercheStatistique->displayFormulaireRecherche();
            $service = '0';
            if (!isset($_GET['allOrgs'])) {
                $valeursCumules = false;
            } else {
                $valeursCumules = true;
            }
            $this->setViewState('service', $service);
            $this->setViewState('valeursCumules', $valeursCumules);
            $this->setViewState('dateDebut', '01/01/'.date('Y'));
            $this->setViewState('dateFin', date('d/m/Y'));

            $valeursCumules = true;
            $dateDebut = $this->formulaireRechercheStatistique->getDateDebut();
            $dateFin = $this->formulaireRechercheStatistique->getDateFin();

            $dataSource = $this->retrieveDataSource($service, $valeursCumules, $dateDebut, $dateFin, isset($_GET['allOrgs']));

            $this->statistique->DataSource = $dataSource;
            $this->statistique->DataBind();
        }
    }

    public function fillTableauStatistique($sender, $param)
    {
        $dateDebut = $this->formulaireRechercheStatistique->getDateDebut();
        $dateFin = $this->formulaireRechercheStatistique->getDateFin();

        if (!isset($_GET['allOrgs'])) {
            $service = $this->formulaireRechercheStatistique->getSelectedService();
            $valeursCumules = $this->formulaireRechercheStatistique->getCumulValeurs();
        } else {
            $service = '0';
            $valeursCumules = true;
        }

        $this->setViewState('service', $service);
        $this->setViewState('valeursCumules', $valeursCumules);
        $this->setViewState('dateDebut', $dateDebut);
        $this->setViewState('dateFin', $dateFin);

        $dataSource = $this->retrieveDataSource($service, $valeursCumules, $dateDebut, $dateFin, isset($_GET['allOrgs']));

        $this->statistique->DataSource = $dataSource;
        $this->statistique->DataBind();

        if ('true' == $this->_useAjax) {
            $this->resultat->render($param->NewWriter);
        }
    }

    public function retrieveStatistiquesAvisPublies($organisme, $service, $valeursCumules, $dateDebut, $dateFin)
    {
        $dataStatistique = [];
        $dataSource = [];

        //BOAMP
        $consultationBoampConsultation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
        $consultationBoampAnnonceInformation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
        $consultationBoampAnnonceAttribution = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);

        //JAL
        $consultationJalConsultation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
        $consultationJalAnnonceInformation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
        $consultationJalAnnonceAttribution = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);

        //JOUE
        $consultationJoueConsultation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);
        $consultationJoueAnnonceInformation = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);
        $consultationJouelAnnonceAttribution = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);

        $dataStatistique['information']['boamp'] = $consultationBoampAnnonceInformation;
        $dataStatistique['information']['joue'] = $consultationJoueAnnonceInformation;
        $dataStatistique['information']['jal'] = $consultationJalAnnonceInformation;

        $dataStatistique['consultation']['boamp'] = $consultationBoampConsultation;
        $dataStatistique['consultation']['joue'] = $consultationJoueConsultation;
        $dataStatistique['consultation']['jal'] = $consultationJalConsultation;

        $dataStatistique['attribution']['boamp'] = $consultationBoampAnnonceAttribution;
        $dataStatistique['attribution']['joue'] = $consultationJouelAnnonceAttribution;
        $dataStatistique['attribution']['jal'] = $consultationJalAnnonceAttribution;

        $dataSource[] = $dataStatistique;

        return $dataSource;
    }

    public function addStatistiquesValuesToArray(&$globalArray, $arrayToAdd, $typeDonnee, $typePublication)
    {
        $globalArray[$typeDonnee][$typePublication]['ao'] += $arrayToAdd[$typeDonnee][$typePublication]['ao'];
        $globalArray[$typeDonnee][$typePublication]['mn'] += $arrayToAdd[$typeDonnee][$typePublication]['mn'];
        $globalArray[$typeDonnee][$typePublication]['pn'] += $arrayToAdd[$typeDonnee][$typePublication]['pn'];
        $globalArray[$typeDonnee][$typePublication]['dc'] += $arrayToAdd[$typeDonnee][$typePublication]['dc'];
        $globalArray[$typeDonnee][$typePublication]['autre'] += $arrayToAdd[$typeDonnee][$typePublication]['autre'];
        $globalArray[$typeDonnee][$typePublication]['total'] += $arrayToAdd[$typeDonnee][$typePublication]['total'];
        $globalArray[$typeDonnee][$typePublication]['mapaInf'] += $arrayToAdd[$typeDonnee][$typePublication]['mapaInf'];
        $globalArray[$typeDonnee][$typePublication]['mapaSupp'] += $arrayToAdd[$typeDonnee][$typePublication]['mapaSupp'];
        $globalArray[$typeDonnee][$typePublication]['totalMapa'] += $arrayToAdd[$typeDonnee][$typePublication]['totalMapa'];
    }

    public function retrieveDataSource($service, $valeursCumules, $dateDebut, $dateFin, $allOranismes)
    {
        $dataStatistique = [];
        $dataSource = [];

        if ($allOranismes) {
            $dataStatistiquesTransversales = ['information' => ['boamp' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0,
            'totalMapa' => 0, ]], 'information' => ['joue' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'information' => ['jal' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'consultation' => ['boamp' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'consultation' => ['joue' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'consultation' => ['jal' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'attribution' => ['boamp' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'attribution' => ['joue' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]], 'attribution' => ['jal' => ['ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0]],
            ];

            $organismes = Atexo_Organismes::retrieveOrganismes();
            if (is_array($organismes)) {
                foreach ($organismes as $organisme) {
                    if ($organisme instanceof CommonOrganisme) {
                        //BOAMP
                        $dataStatistique['consultation']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
                        $dataStatistique['information']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
                        $dataStatistique['attribution']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);
                        //print_r($dataStatistique['consultation']['boamp']);
                        //JAL
                        $dataStatistique['consultation']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
                        $dataStatistique['information']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
                        $dataStatistique['attribution']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);

                        //JOUE
                        $dataStatistique['consultation']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme());
                        $dataStatistique['information']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme());
                        $dataStatistique['attribution']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme());

                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'information', 'boamp');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'information', 'joue');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'information', 'jal');

                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'consultation', 'boamp');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'consultation', 'joue');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'consultation', 'jal');

                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'attribution', 'boamp');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'attribution', 'joue');
                        $this->addStatistiquesValuesToArray($dataStatistiquesTransversales, $dataStatistique, 'attribution', 'jal');
                        Atexo_Db::closeOrganism($organisme->getAcronyme());
                    }
                }

                $dataSource[] = $dataStatistiquesTransversales;
            }
        } else {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            //BOAMP
            $dataStatistique['consultation']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
            $dataStatistique['information']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
            $dataStatistique['attribution']['boamp'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);

            //JAL
            $dataStatistique['consultation']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $service, $valeursCumules, $dateDebut, $dateFin);
            $dataStatistique['information']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $service, $valeursCumules, $dateDebut, $dateFin);
            $dataStatistique['attribution']['jal'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $service, $valeursCumules, $dateDebut, $dateFin);

            //JOUE
            $dataStatistique['consultation']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);
            $dataStatistique['information']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);
            $dataStatistique['attribution']['joue'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme);

            $dataSource[] = $dataStatistique;
        }

        return $dataSource;
    }

    public function exportXls()
    {
        $dataStatistique = [];
        $nomEntite = null;
        $acronymeOrganisme = Atexo_CurrentUser::getCurrentOrganism();
        $service = $this->getViewState('service');
        $valeursCumules = $this->getViewState('valeursCumules');
        $dateDebut = $this->getViewState('dateDebut');
        $dateFin = $this->getViewState('dateFin');
        if (isset($_GET['allOrgs']) || '0' === $service || 0 === $service) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
            if ($organisme instanceof CommonOrganisme) {
                $nomEntite = $organisme->getDenominationOrg();
            }
        } else {
            if ('0' === $service || 0 === $service) {
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                if ($organisme instanceof CommonOrganisme) {
                    $nomEntite = $organisme->getDenominationOrg();
                }
            } else {
                $entite = Atexo_EntityPurchase::retrieveEntityById($service, $acronymeOrganisme);
                if ($entite instanceof CommonService) {
                    $nomEntite = $entite->getLibelle();
                }
            }
        }
        $idEntite = $service;
        if (isset($_GET['allOrgs'])) {
            $organismes = Atexo_Organismes::retrieveOrganismes();
            $arrayInitResults = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(isset($_GET['allOrgs']));
            if (is_array($organismes)) {
                foreach ($organismes as $organisme) {
                    $data = [];
                    if ($organisme instanceof CommonOrganisme && 'org-mpe-3_0' != $organisme->getAcronyme()) {
                        $arrayTypeProcedure = (new Atexo_Consultation_ProcedureType())->rerieveAllTypeProcedureAndTypeProcedurePortail($organisme->getAcronyme());
                        //BOAMP
                        $data['AAPC']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        $data['Information']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        $data['Attribution']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        //JAL
                        $data['AAPC']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        $data['Information']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        $data['Attribution']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($organisme->getAcronyme(), Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, true);
                        //JOUE
                        $data['AAPC']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme(), true, true);
                        $data['Information']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme(), true, true);
                        $data['Attribution']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($organisme->getAcronyme(), true, true);
                        foreach (['AAPC', 'Information', 'Attribution'] as $typeDonnee) {
                            foreach (['BOAMP', 'JAL', 'JOUE', 'Pmi-Seule'] as $categorie) {
                                $newData = [];
                                if (is_array($data[$typeDonnee][$categorie])) {
                                    $newData = (new Atexo_Statistiques_RequetesStatistiques())->correspondanceTypeProcedure($data[$typeDonnee][$categorie], $arrayTypeProcedure, $arrayInitResults);
                                    foreach ($newData as $key => $value) {
                                        $dataStatistique[$typeDonnee][$categorie][$key] += $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            //BOAMP
            $dataStatistique['AAPC']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            $dataStatistique['Information']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            $dataStatistique['Attribution']['BOAMP'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieBoamp($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            //JAL
            $dataStatistique['AAPC']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            $dataStatistique['Information']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            $dataStatistique['Attribution']['JAL'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationPublieJal($acronymeOrganisme, Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'), $idEntite, $valeursCumules, $dateDebut, $dateFin, true, false);
            //JOUE
            $dataStatistique['AAPC']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($acronymeOrganisme, true, false);
            $dataStatistique['Information']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($acronymeOrganisme, true, false);
            $dataStatistique['Attribution']['JOUE'] = (new Atexo_Statistiques_RequetesStatistiques())->retrieveConsultationJoue($acronymeOrganisme, true, false);
        }
        (new Atexo_GenerationExcel())->genererExcelAvisPublies($acronymeOrganisme, $service, $nomEntite, $valeursCumules, $dateDebut, $dateFin, isset($_GET['allOrgs']), $dataStatistique);
    }

    public function agentExcel()
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $serviceId = Atexo_CurrentUser::getIdServiceAgentConnected();
        (new Atexo_GenerationExcel())->generateExcelHabilitation($organisme, $serviceId, false);
    }
}
