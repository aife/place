<?php

namespace Application\Pages\Agent;
use App\Entity\Agent;
use App\Service\PradoPasswordEncoderInterface;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;

use Application\Controls\MpeTPage;
use Exception;
use Prado\Util\TLogger;
use Prado\Web\UI\TPage;
use Prado\Prado;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Agent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;



/**
 * Page d'accueil non authentifié des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AgentModifierCompte extends MpeTPage
{
    public $idAgent;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->idAgent = Atexo_CurrentUser::getIdAgentConnected();

 	    $agent = Atexo_Agent::retrieveAgent($this->idAgent, false);

 	    if(!$agent instanceof CommonAgent) {
            Prado::log("Utilisateur introuvale");
            throw new Exception('Error');
        }

 	    if(!$this->isPostBack)
 	    {
            $this->script->text = "<script>checkRgpd(" . (($agent->getRgpdCommunicationPlace() === true) ? '1' : '0') . "," . (($agent->getRgpdEnquete() === true) ? '1' : '0') . ")</script>";
            $this->enqueteRgpd->Checked = $agent->getRgpdEnquete();
            $this->communicationPlaceRgpd->Checked = $agent->getRgpdCommunicationPlace();

 	       $this->getDataAgent($this->idAgent);
 	       $this->AtexoReferentielAgent->afficherReferentielAgent($this->idAgent);
 	    }
        $this->passwordValidator->SetEnabled(false);
        $this->confPasswordValidator->SetEnabled(false);
        $this->panelMessageErreur->setVisible(false);
        //$this->confPasswordCompareValidator->SetEnabled(false);
         /*
          * demande de OTA suie au probleme d'acces a mon compte
          */
         //$this->customizeForm();
    }

    /**
     * retourne les informations relatives à l'agent.
     */
    public function getDataAgent($idAgent)
    {
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        if ($agent) {
            $this->nomAgent->Text = $agent->getNom();
            $this->prenomAgent->Text = $agent->getPrenom();
            $this->entiteAchat->Text = (new Atexo_EntityPurchase())->getEntityPathById($agent->getServiceId(), $agent->getOrganisme(), true, true);
            $this->tel->Text = $agent->getNumTel();
            $this->fax->Text = $agent->getNumFax();
            $this->email->Text = trim($agent->getEmail());
            $this->setViewState('email', $agent->getEmail());

            if (Atexo_Module::isEnabled('AuthenticateAgentByLogin')) {
                $this->identifiant->Text = $agent->getLogin();
            }

            if (Atexo_Module::isEnabled('AuthenticateAgentByCert')) {
                $this->proprietaireCertificat->Text = (new Atexo_Crypto_Certificat())->getCnCertificat($agent->getCertificat(), session_id());
                $this->delivrePar->Text = (new Atexo_Crypto_Certificat())->getACCertificat($agent->getCertificat());
            }

            $this->setViewState('identifiant', $agent->getLogin());
        }
    }

    /**
     * met à jour les informations de l'agent.
     */
    public function updateAgentInformations($sender, $param)
    {
        if ($this->IsValid) {

	 	    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
	 	    $agent = CommonAgentPeer::retrieveByPK(Atexo_CurrentUser::getIdAgentConnected(), $connexionCom);

	 	    $agent
                ->setNom($this->nomAgent->Text)
                ->setPrenom($this->prenomAgent->Text)
                ->setNumTel($this->tel->Text)
                ->setNumFax($this->fax->Text)
                ->setEmail(trim($this->email->Text));

            //on ne met la date à jour que si un de 2 choix a été changé. Un test étant fait pour cette condition on set tout le rgpd dans le if
            if (
                $agent->getRgpdCommunicationPlace() !== $this->communicationPlaceRgpd->Checked ||
                $agent->getRgpdEnquete() !== $this->enqueteRgpd->Checked
            ) {
                $agent
                    ->setDateValidationRgpd(new \DateTime())
                    ->setRgpdEnquete($this->enqueteRgpd->Checked)
                    ->setRgpdCommunicationPlace($this->communicationPlaceRgpd->Checked);
            }



 	        if(Atexo_Module::isEnabled('AuthenticateAgentByCert') && '' != $this->authentificationByCer->getCertificat()) {
                $certif = $this->authentificationByCer->getCertificat();
                $serial = $this->authentificationByCer->getSerial();
                $agent->setCertificat($certif);
                $agent->setNumCertificat($serial);
                if ($this->authentificationByCer->getCertFileTmp()) {
                    unlink($this->authentificationByCer->getCertFileTmp());
                }
            }

            if (Atexo_Module::isEnabled('AuthenticateAgentByLogin')) {
                $agent->setLogin($this->identifiant->Text);
            }

            $agent->setDateModification(date('Y-m-d H:i:s'));
            if (('' != $this->passwordAgent->Text) && (Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->passwordAgent->Text)) {
                Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $this->passwordAgent->Text);
                $agent->setTentativesMdp('0');
            }
            $agent->save($connexionCom);

            $c = new Criteria();
            $c->add(CommonAgentPeer::ID, Atexo_CurrentUser::getIdAgentConnected(), Criteria::EQUAL);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $cachedAgent = CommonAgentPeer::doSelectOne($c, $connexionCom);
            Atexo_CurrentUser::writeToSession('cachedAgent', $cachedAgent);

            $this->AtexoReferentielAgent->saveReferentielAgent($agent->getId(), $agent->getOrganisme());
            if ($agent->getAlerteCreationModificationAgent()) {
                $message = Atexo_Message::getMessageModificationCompte($agent);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $this->email->Text, Prado::localize('DEFINE_TEXT_OBJET_CREATION_MODIFICATION_MON_COMPTE'), $message, '', '', false, true);
            }
            if (Atexo_Module::isEnabled('SocleInterne')) {
                $this->script->Text = '<script>'.$this->authentificationByCer->getMessage()." document.location.href='index.php?page=Agent.AccueilAgentAuthentifieSocleinterne';</script>";
            } else {
                $this->script->Text = '<script>'.$this->authentificationByCer->getMessage()." document.location.href='agent';</script>";
            }
            //$this->response->redirect("index.php?page=Agent.AccueilAgentAuthentifie");
        }
    }

    /**
     * Verifier que l'adresse mail est unique.
     */
    public function verifyMailModification($sender, $param)
    {
        if (strtoupper($this->getViewState('email')) != strtoupper($this->email->getText())) {
            $email = $this->email->getText();
            if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('TEXT_ADRESSE_ELECTRONIQUE');

                return;
            }
            if ($email) {
                $agent = (new Atexo_Agent())->retrieveAgentByMail($email);
                if ($agent) {
                    $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                    $param->IsValid = false;
                    $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('TEXT_EMAIL_EXISTE_DEJA', ['adresse mail' => $email]);

                    return;
                }
            }
        }
    }

    /**
     * Annuler et retour à la page d'accueil agent.
     */
    public function annuler($sender, $param)
    {
        if (Atexo_Module::isEnabled('SocleInterne')) {
            $this->response->redirect('index.php?page=Agent.AccueilAgentAuthentifieSocleinterne');
        } else {
            $this->response->redirect('agent');
        }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->pradoValidatorEmail->Enabled = false;
        }
    }

    public function verifierMdp($sender, $param)
    {
        if (Atexo_CurrentUser::PASSWORD_INIT_VALUE !== $this->passwordAgent->text && '' !== $this->passwordAgent->text) {
            $param->IsValid = preg_match(Atexo_CurrentUser::PASSWORD_FORMAT, $this->passwordAgent->text);
            $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
        }
    }

    public function checkOldPassword($sender, $param)
    {
        if (Atexo_CurrentUser::PASSWORD_INIT_VALUE !== $this->passwordAgent->text && '' !== $this->passwordAgent->text) {
            $encodedPassword = Atexo_CurrentUser::getPassword();
            $encoder = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class);
            $param->IsValid = $encoder->isPasswordValid(Agent::class, $encodedPassword, $this->oldPassword->text);
        }
    }
}
