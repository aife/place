<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\CommonHistoriqueAvisPub;
use Application\Propel\Mpe\CommonTypeAvisPub;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPdf;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Exception;
use Prado\Prado;

class PubliciteAvis extends MpeTPage
{
    private $_consultation = '';
    private $_refCons = null;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->scriptOpoce->Text = '';
        $this->jsOpoce->Text = '';
        $this->_consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
        if ($this->_consultation instanceof CommonConsultation) {
            $this->_refCons = $this->_consultation->getId();
            $this->mainPanel->visible = true;
            $this->errorPanel->visible = false;
            $this->fillRepeaterFormPubliciteAvis();
        } else {
            $this->mainPanel->visible = false;
            $this->errorPanel->visible = true;
            $this->errorPanel->setMessage('Error Consultation');

            return null;
        }
        //Affichage d'un message d'erreur en cas d'invalidité de la transmission de la publicité à l'entité coordinatrice
        if (isset($_GET['rechargement'])) {
            $this->errorPanel->visible = true;
            if ($_GET['idAvis']) {
                $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idAvis'])), $this->_consultation->getOrganisme(), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
                if ($avisPub instanceof CommonAvisPub) {
                    $typeAvis = (new Atexo_Publicite_AvisPub())->retrieveLibelleTypeAvisByIdAvis($avisPub->getTypeAvis());
                    if (isset($_GET['consValidKo']) && isset($_GET['destIncomplet'])) {
                        $this->errorPanel->setMessage(str_replace('[__TYPE_AVIS__]', $typeAvis, Prado::localize('MESSAGE_ERREUR_TRANSMISSION_AVIS_CONS_NON_VALIDEE_DEST_INCOMPLETS')));
                    } elseif (isset($_GET['consValidKo']) || isset($_GET['destIncomplet'])) {
                        if (isset($_GET['consValidKo'])) {
                            $this->errorPanel->setMessage(str_replace('[__TYPE_AVIS__]', $typeAvis, Prado::localize('MESSAGE_ERREUR_TRANSMISSION_AVIS_CONS_NON_VALIDEE')));
                        } elseif (isset($_GET['destIncomplet'])) {
                            $this->errorPanel->setMessage(str_replace('[__TYPE_AVIS__]', $typeAvis, Prado::localize('MESSAGE_ERREUR_TRANSMISSION_AVIS_DEST_INCOMPLETS')));
                        }
                    } elseif (isset($_GET['consDateMiseLigneKo'])) {
                        $this->errorPanel->setMessage(str_replace('[__TYPE_AVIS__]', $typeAvis, Prado::localize('MESSAGE_ERREUR_TRANSMISSION_AVIS_CONS_DATE_MISE_LIGNE_SOUHAITEE_KO')));
                    }
                }
            }
        }
        $this->idConsultationSummary->setConsultation($this->_consultation);
        $this->boutonRetour->setNavigateUrl('#');
        //Gestion de l'accès au module publicité par l'agent
        if (($this->_consultation instanceof CommonConsultation) && !($this->_consultation->getPublicationHabilitation() && !$this->_consultation->getCurrentUserReadOnly())) {
            $this->PanelNotAuthorizedToAccessThePage->setVisible(true);
            $this->PanelAuthorizedToAccessThePage->setVisible(false);
            $this->PanelNotAuthorizedToAccessThePage->setMessage(Prado::localize('DEFINE_MESSAGE_VOUS_N_ETES_PAS_HABILITE_ACCEDER_PAGE'));

            return null;
        } else {
            $this->PanelNotAuthorizedToAccessThePage->setVisible(false);
            $this->PanelAuthorizedToAccessThePage->setVisible(true);
        }
    }

    public function fillRepeaterFormPubliciteAvis()
    {
        $data = (new Atexo_Publicite_AvisPub())->getAvisByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getOrganismAcronym());
        if ((is_countable($data) ? count($data) : 0) > 0) {
            $this->repeaterFormPubliciteAvis->DataSource = $data;
            $this->repeaterFormPubliciteAvis->dataBind();
        }
    }

    public function displayListeDistinataires($idAvis)
    {
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $data = (new Atexo_Publicite_AvisPub())->getListeDestinataires($idAvis, $organisme);

        return $data;
    }

    public function activateSendButton($idAvis)
    {
        $organisme = Atexo_CurrentUser::getOrganismAcronym();

        return (new Atexo_Publicite_AvisPub())->activateSendButton($idAvis, $organisme);
    }

    public function activateDeleteButton($idAvis)
    {
        $organisme = Atexo_CurrentUser::getOrganismAcronym();

        return (new Atexo_Publicite_AvisPub())->activateDeleteButton($idAvis, $organisme);
    }

    public function isDetailSupport($idAvis)
    {
        return (new Atexo_Publicite_AvisPub())->isDetailSupport($idAvis, Atexo_CurrentUser::getOrganismAcronym(), $this->_consultation->getId());
    }

    public function actionFormulaire($sender, $param)
    {
        (new Atexo_Publicite_AvisPub())->deleteAvis($param->CommandName, Atexo_CurrentUser::getOrganismAcronym());
    }

    /**
     * Afficher le nouveau tableau si avis ajouté ou supprimé.
     */
    public function onCallBackRefreshRepeater($sender, $param)
    {
        $listeFormulaire = (new Atexo_Publicite_AvisPub())->getAvisByRefConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getOrganismAcronym());
        if (is_array($listeFormulaire)) {
            $datatriee = Atexo_Util::sortArrayOfObjects($listeFormulaire, 'DateCreation', 'ASC');
            Atexo_CurrentUser::writeToSession('cachedFormPub', $datatriee);
            if (!is_array($datatriee)) {
                $datatriee = [];
            }
            $this->repeaterFormPubliciteAvis->DataSource = $datatriee;
            $this->repeaterFormPubliciteAvis->DataBind();
            $this->panelAvisPub->render($param->NewWriter);
        }
    }

    public function sendAvis($sender, $param)
    {
        if ($this->isValid) {
            $idAvis = $param->CommandName;
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $refCons = $this->_consultation->getId();
            (new Atexo_Publicite_AvisPub())->sendAvis($idAvis, $organisme, $refCons);
        }
    }

    public function actionPublicite($sender, $param)
    {
        if ('actionDestinataire' == $param->CommandName) {
            $this->actionDestinataire($sender, $param);
        }
        if ('supprimerAvis' == $param->CommandName) {
            $this->deleteAvisDestinataire($sender, $param);
        }

        if ('enregistrementEsender' == $param->CommandName) {
            $this->enregistrementEsender($sender, $param);
        }

        if ('enregistrementValidationEsender' == $param->CommandName) {
            $this->enregistrementValidationEsender($sender, $param);
        }

        if ('enregistrementValidationEnvoiEsender' == $param->CommandName) {
            $this->enregistrementValidationEnvoiEsender($sender, $param);
        }
    }

    public function actionDestinataire($sender, $param)
    {
        $script = null;
        $parametters = explode('-', $param->CommandParameter);
        $idDestinataire = $parametters[0];
        $idTypeDest = $parametters[1];
        $itemIndex = $parametters[2];
        $idTypeAvis = $parametters[3];
        $idAvis = $parametters[4];
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $typeDoc = Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS');
        if ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {//Call Esender
            $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, Atexo_CurrentUser::getOrganismAcronym());
            if ($destinataire instanceof CommonDestinatairePub) {
                if ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_ESENDER')) {
                    $resourceForm = (new CommonTypeAvisPub())->getResourceForm($idTypeAvis);
                    if (self::isDetailSupport($idAvis)) {
                        $this->scriptOpoce->Text = "<script>popUpSetSize('?page=Agent.PopupEsender&idTypeSupport=".$idTypeDest.
                                '&idDest='.$idDestinataire.'&resourceFormulaire='.$resourceForm.'&itemIndex='.$itemIndex.
                                '&id='.$this->_consultation->getId().'&isConsultation='.$idAvis.
                                "','900px','1000px','yes');</script>";
                    } else {
                        $this->scriptOpoce->Text = "<script>popUpSetSize('?page=Agent.PopupEsender&idTypeSupport=".$idTypeDest.
                                '&idDest='.$idDestinataire.'&resourceFormulaire='.$resourceForm.'&itemIndex='.$itemIndex.
                                '&id='.$this->_consultation->getId()."','900px','1000px','yes');</script>";
                    }
                } elseif ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB')) {
                    if ($this->isEtatDestinataireOpoceTransmis($destinataire->getAvisPub())) {
                        if ($this->_consultation instanceof CommonConsultation) {
                            $url = 'index.php?page='.Atexo_Util::getTypeUserCalledForPradoPages().'.DownloadAvisTED&orgAcronyme='.$this->_consultation->getOrganisme().'&idDestinataire='.$idDestinataire;
                            $url .= '&id='.$this->_consultation->getId().'&idAvis='.$idAvis.'&pdf=sub';
                            $script = "<script>popUpSetSize('".$url."')</script>";
                        }
                    } else {
                        $script = "<script>javascript:document.cookie = 'JSESSIONID=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/sub';";
                        $script .= " javascript:popUpSetSize('?page=Agent.PopupFormulaireModificationPublicite&id=".base64_decode($_GET['id']).'&idDestinataire='.$idDestinataire."','988px','768px','yes');";
                        $script .= '</script>';
                    }
                    $this->scriptOpoce->setText($script);
                } elseif ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_TED')) {
                    $this->scriptOpoce->Text = "<script>popUpSetSize('".Atexo_Config::getParameter('URL_TED_ANNONCE')."','1000px','750px','yes');</script>";
                }
            }
        } elseif ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {//Send To press
            if (self::isDetailSupport($idAvis)) {
                $this->Response->Redirect('?page=Agent.EnvoiCourrierElectroniquePress&idDest='.$idDestinataire.'&id='.$this->_consultation->getId().'&consult');
            } else {
                $this->Response->Redirect('?page=Agent.EnvoiCourrierElectroniquePress&idDest='.$idDestinataire.'&id='.$this->_consultation->getId());
            }
        } elseif ($idTypeDest == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {//National pub
            //Recuperation de la piece jointe de l'annonce envoyée au portail
            $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvis, $organisme, $this->_consultation->getId());
            $idAvisPortail = '';
            if ($avisPub->getIdAvisPortail()) {//L'avis portail est deja stockée, alors on le télécharge
                if ($avisPub instanceof CommonAvisPub) {
                    $idAvisPortail = $avisPub->getIdAvisPortail();
                }
                $this->scriptOpoce->Text = "<script>popUpSetSize('?page=Agent.DownloadDocument&id=".$this->_consultation->getId().
                                                                 '&org='.$organisme.'&typeDoc='.$typeDoc.'&idAvis='.$idAvisPortail."');</script>";
            } else {//L'avis portail n'est pas stockée, alors on le génère à la vollée
                $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avisPub->getTypeAvis());
                if ($modele) {
                    /*
                         //Génération du document à envoyer à la presse
                         $file = Atexo_Publicite_AvisPdf::generationAvisPublicite($this->_consultation, $modele, true, "fr", $avisPub->getId());
                        $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis.pdf';
                         $pdfContent = PdfGeneratorClient::genererPdf($file);
                         Atexo_Util::write_file($filePdf, $pdfContent);
                    */
                    $this->scriptOpoce->Text = "<script>popUpSetSize('?page=Agent.GenerationDocPub&id=".$this->_consultation->getId().
                                                                        '&idAvis='.$avisPub->getId().'&organisme='.$organisme."');</script>";
                }
            }
        }
    }

    public function enregistrementEsender($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $xml = $this->xmlAvis->Text;
        $noDocExt = $this->noDocExt->Text;
        $idDest = $param->CommandParameter;
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $FormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($idDest, $organisme);
        if ($FormXml && $FormXml->getXml()) {
            $FormXml->setXml($xml);
            $FormXml->setNoDocExt($noDocExt);
            $historiqueAvis = new CommonHistoriqueAvisPub();
            $historiqueAvis->setOrganisme($organisme);
            //$destPub = Atexo_Publicite_AvisPub::getDestinatairePub($idDest);
            /*if($destPub){
                Atexo_Publicite_AvisPub::createHistorique($organisme,$destPub->getIdAvis(),$destPub->getDateModification(),$destPub->getEtat(), $connexion);
            }*/
            $FormXml->save($connexion);
        } else {
            (new Atexo_Publicite_AvisPub())->createNewXmlDestOpoce($idDest, $organisme, $xml, $noDocExt);
        }
        (new Atexo_Publicite_AvisPub())->updateDestAvis($idDest, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_BROUILLON'));
        $this->jsOpoce->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();</script>";
    }

    public function enregistrementValidationEsender($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $xml = $this->xmlAvis->Text;
        $noDocExt = $this->noDocExt->Text;
        $idDest = $param->CommandParameter;
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $FormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($idDest, $organisme);
        if ($FormXml && $FormXml->getXml()) {
            $FormXml->setXml($xml);
            $FormXml->setNoDocExt($noDocExt);
            //$destPub = Atexo_Publicite_AvisPub::getDestinatairePub($idDest);
            /*if($destPub){
               Atexo_Publicite_AvisPub::createHistorique($organisme,$destPub->getIdAvis(),$destPub->getDateModification(),$destPub->getEtat(), $connexion);
            }*/
            $FormXml->save($connexion);
        } else {
            (new Atexo_Publicite_AvisPub())->createNewXmlDestOpoce($idDest, $organisme, $xml, $noDocExt);
        }
        (new Atexo_Publicite_AvisPub())->updateDestAvis($idDest, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'));
        $this->jsOpoce->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();</script>";
    }

    public function enregistrementValidationEnvoiEsender($sender, $param)
    {
        echo 'enregistrementValidationEnvoiEsender';
        exit;
    }

    public function getNoDocExt($idDestinataire, $organisme)
    {
        $noDocExt = (new Atexo_Publicite_AvisPub())->getNoDocExt($idDestinataire, $organisme);
        if ($noDocExt) {
            return '('.$noDocExt.')';
        }

        return '';
    }

    /**
     * Fonction qui valide l'envoi de l'avis de publicité à l'entité coordinatrice.
     */
    public function validerEnvoiAvisPub($sender, $param)
    {
        $arrayParam = explode('_', $param->Value);
        $idexItem = $arrayParam[0];
        foreach ($this->repeaterFormPubliciteAvis->Items as $item) {
            if ($item->itemIndex == $idexItem) {
                $listeDestAvisPub = (new Atexo_Publicite_AvisPub())->getListeDestinataires($arrayParam[1], $arrayParam[2]);
                if (is_array($listeDestAvisPub) && count($listeDestAvisPub)) {
                    foreach ($listeDestAvisPub as $dest) {
                        if ($dest->getEtat() != Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET')) {
                            //Cas destinataires incomplets et consultation non validée
                            if (!$this->_consultation->getEtatValidation()) {
                                $param->IsValid = false;
                                $this->response->redirect('?page=Agent.PubliciteAvis&idAvis='.base64_encode($arrayParam[1]).
                                                          '&id='.base64_encode($this->_refCons).'&rechargement&consValidKo&destIncomplet');

                                return;
                            }
                            //Cas destinataires incomplets
                            $param->IsValid = false;
                            $this->response->redirect('?page=Agent.PubliciteAvis&idAvis='.base64_encode($arrayParam[1]).
                                                        '&id='.base64_encode($this->_refCons).'&rechargement&destIncomplet');
                        }
                    }
                }
                //Cas consultation non validée
                if (!$this->_consultation->getEtatValidation()) {
                    $param->IsValid = false;
                    $this->response->redirect('?page=Agent.PubliciteAvis&idAvis='.base64_encode($arrayParam[1]).'&id='.base64_encode($this->_refCons).
                                                '&rechargement&consValidKo');

                    return;
                }
                //Cas consultation sans date de mise en ligne souhaitée
                if (!$this->_consultation->getDateMiseEnLigneSouhaitee()) {
                    $param->IsValid = false;
                    $this->response->redirect('?page=Agent.PubliciteAvis&idAvis='.base64_encode($arrayParam[1]).'&id='.base64_encode($this->_refCons).
                    '&rechargement&consDateMiseLigneKo');

                    return;
                }
            }
        }
    }

    /**
     * Permet de recuperer l'objet consultation.
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function getLibelleTypeAvis($idTypeAvis)
    {
        $typeAvis = (new CommonTypeAvisPub())->getTypeAvisById($idTypeAvis);
        if ($typeAvis instanceof CommonTypeAvisPub) {
            return $typeAvis->getLibelleTraduit();
        } else {
            return '-';
        }
    }

    /**
     * Permet de recuperer l'identifiant Joue de l'avis.
     *
     * @param int    $idDestinataire
     * @param string $organisme
     */
    public function getIndentifiantJoue($idDestinataire, $organisme)
    {
        $formXmlDestOpoce = (new Atexo_Publicite_AvisPub())->getObjetAvisOpoceByIdDest($idDestinataire, $organisme);
        $idJoue = '';
        if ($formXmlDestOpoce instanceof CommonFormXmlDestinataireOpoce) {
            $idJoue = $formXmlDestOpoce->getIdJoue();
        }

        return $idJoue;
    }

    /**
     * Permet de recuperer la date de publication au JOUE de l'avis.
     *
     * @param int    $idDestinataire
     * @param string $organisme
     */
    public function getDatePubJoue($idDestinataire, $organisme)
    {
        $formXmlDestOpoce = (new Atexo_Publicite_AvisPub())->getObjetAvisOpoceByIdDest($idDestinataire, $organisme);
        $datePubJoue = '-';
        if ($formXmlDestOpoce instanceof CommonFormXmlDestinataireOpoce && $formXmlDestOpoce->getDatePubJoue()) {
            $datePubJoue = Atexo_Util::iso2frnDateTime($formXmlDestOpoce->getDatePubJoue());
        }

        return $datePubJoue;
    }

    /**
     * Permet de recuperer le lien de publication de l'avis au JOUE.
     *
     * @param int    $idDestinataire
     * @param string $organisme
     */
    public function getLienPubAvisJoue($idDestinataire, $organisme)
    {
        $formXmlDestOpoce = (new Atexo_Publicite_AvisPub())->getObjetAvisOpoceByIdDest($idDestinataire, $organisme);
        $lienPubJoue = '#';
        if ($formXmlDestOpoce instanceof CommonFormXmlDestinataireOpoce && $formXmlDestOpoce->getLienPublication()) {
            $lienPubJoue = $formXmlDestOpoce->getLienPublication();
        }

        return $lienPubJoue;
    }

    /**
     * Permet de construire l'url du lien de publication de l'avis.
     *
     * @param int    $idDestinataire
     * @param string $organisme
     */
    public function getUrlTelechargementAvisTed($idDestinataire, $idAvis, $idEtatDest)
    {
        if ($this->_consultation instanceof CommonConsultation && $this->isEtatDestinataireOpoceAuMoinsComplet($idEtatDest)) {
            $url = 'index.php?page=Agent.DownloadAvisTED&orgAcronyme='.$this->_consultation->getOrganisme().'&idDestinataire='.$idDestinataire;
            $url .= '&id='.$this->_consultation->getId().'&idAvis='.$idAvis;

            return $url;
        }

        return '#';
    }

    /**
     * Permet de préciser si l'avis OPOCE est au statut au moins complet.
     *
     * @param int    $idDestinataire
     * @param string $idAvis
     */
    public function isEtatDestinataireOpoceAuMoinsComplet($idEtatDestinataire)
    {
        $arrayDestAuMoinsComplet = [];
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_ECO');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_SIP');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_INTEGRE');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE');
        $arrayDestAuMoinsComplet[] = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_REJETE');
        if (in_array($idEtatDestinataire, $arrayDestAuMoinsComplet)) {
            return true;
        }

        return false;
    }

    /**
     * Permet de préciser si l'avis OPOCE est au statut au moins complet.
     *
     * @param int    $idDestinataire
     * @param string $idAvis
     */
    public function isEtatDestinataireOpoceTransmis($avisPub)
    {
        $return = false;
        if ($avisPub instanceof CommonAvisPub) {
            if ($avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')
                    || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')
                    || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')
                    || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                $return = true;
            }
        }

        return $return;
    }

    public function fileUploaded($sender, $param)
    {
        $connexion = null;
        try {
            if ($sender->HasFile) {
                $idDestinataire = $this->destFile->getValue();
                $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, Atexo_CurrentUser::getOrganismAcronym());
                if ($destinataire instanceof CommonDestinatairePub) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $connexion->beginTransaction();
                    (new Atexo_Publicite_AvisPub())->updateDestAvis($idDestinataire, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'), $connexion);
                    $idAvis = $destinataire->getIdAvis();
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvis, $this->_consultation->getOrganisme(), $this->_consultation->getId());
                    $pdfContent = file_get_contents($sender->LocalName);
                    if ($pdfContent && '' != $pdfContent && 'null' != $pdfContent) {
                        $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s');
                        system('touch '.$filePdf);
                        $fileNew = $sender->FileName;
                        $avisPortail = (new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($filePdf, $this->_consultation, 'fr', true, $pdfContent, $fileNew, true);
                        if ($avisPortail instanceof CommonAVIS && $avisPortail->getStatut() != Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
                            $avisPortail->setTypeAvisPub($avisPub->getTypeAvis());
                            $avisPortail->setType(Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED_HORS_PF'));
                            $avisPortail->setTypeDocGenere('0');
                            $avisPortail->setAgentId($avisPub->getIdAgent());
                            $avisPortail->save($connexion);
                            $avisPub->setIdAvisPdfOpoce($avisPortail->getId());
                            $avisPub->save($connexion);
                        }
                    }
                    $connexion->commit();
                }
            }
            $this->scriptOpoce->Text = '<script>hideLoader();</script>';
            $this->onCallBackRefreshRepeater($sender, $param);
        } catch (Exception) {
            $connexion->rollBack();
        }
    }

    public function isTypePubTED($destinataire)
    {
        $return = false;
        if ($destinataire instanceof CommonDestinatairePub) {
            if ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_TED')) {
                $return = true;
            }
        }

        return $return;
    }

    public function isTypePubSUB($destinataire)
    {
        $return = false;
        if ($destinataire instanceof CommonDestinatairePub) {
            if ($destinataire->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB')) {
                $return = true;
            }
        }

        return $return;
    }

    /**
     * Permet de telecharger un avis de publicite de la consultation.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <mal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2015
     */
    public function DownloadAvis($sender, $param)
    {
        $idAvis = $param->CommandParameter;
        if ($this->_consultation instanceof CommonConsultation) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $avis = (new Atexo_Publicite_Avis())->retreiveAvisByIdAndRefCons($idAvis, $this->_consultation->getId(), $this->_consultation->getOrganisme(), $connexion);
            if ($avis instanceof CommonAVIS) {
                //Nom du fichier joint
                $nomFichier = $avis->getNomFichier();
                (new DownloadFile())->downloadFiles($avis->getAvis(), $nomFichier, $avis->getOrganisme());
            }
        }
    }

    public function hasFileAvisTedExterne($destinataire)
    {
        $return = false;
        if ($destinataire instanceof CommonDestinatairePub) {
            $avisPub = $destinataire->getAvisPub();
            if ($avisPub instanceof CommonAvisPub) {
                if ($avisPub->getIdAvisPdfOpoce()) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $avis = (new Atexo_Publicite_Avis())->retreiveAvisByIdAndRefCons($avisPub->getIdAvisPdfOpoce(), $avisPub->getConsultationId(), $avisPub->getOrganisme(), $connexion);
                    if ($avis instanceof CommonAVIS && $avis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                        $return = true;
                    }
                }
            }
        }

        return $return;
    }

    public function deleteAvisDestinataire($sender, $param)
    {
        $connexion = null;
        try {
            $idDestinataire = $param->CommandParameter;
            $destinataire = (new Atexo_Publicite_AvisPub())->getDestinatairePub($idDestinataire, Atexo_CurrentUser::getOrganismAcronym());
            if ($destinataire instanceof CommonDestinatairePub) {
                $idAvis = $destinataire->getIdAvisOpoce();
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                (new Atexo_Publicite_AvisPub())->updateDestAvis($idDestinataire, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_BROUILLON'), $connexion);
                $avis = (new Atexo_Publicite_Avis())->retreiveAvisById($idAvis, $connexion, $destinataire->getOrganisme());
                if ($avis instanceof CommonAVIS) {
                    (new Atexo_Blob())->deleteBlobFile($avis->getAvis(), $avis->getOrganisme());
                    $avis->delete($connexion);
                }
                $connexion->commit();
            }
        } catch (Exception) {
            $connexion->rollBack();
        }
    }

    public function afficherBtnSupprimerAvis($destinataire)
    {
        $return = false;
        if ($destinataire instanceof CommonDestinatairePub && $destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')
                && $this->isTypePubTED($destinataire) && $this->hasFileAvisTedExterne($destinataire)) {
            $avisPub = $destinataire->getAvisPub();
            if ($avisPub instanceof CommonAvisPub) {
                if ($avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE')
                        || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                        || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    public function afficherBtnTelecharger($destinataire)
    {
        if ($destinataire instanceof CommonDestinatairePub) {
            return $destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE') && $this->isEtatDestinataireOpoceAuMoinsComplet($destinataire->getEtat()) && !$this->isTypePubTED($destinataire) && !$this->isTypePubSUB($destinataire);
        }
    }

    public function hasFileAvisTedOpoce($destinataire)
    {
        $return = false;
        if ($destinataire instanceof CommonDestinatairePub && $this->isTypePubSUB($destinataire)) {
            $avisPub = $destinataire->getAvisPub();
            if ($avisPub instanceof CommonAvisPub) {
                if ($avisPub->getIdAvisPdfOpoce()) {
                    $return = true;
                }
            }
        }

        return $return;
    }
}
