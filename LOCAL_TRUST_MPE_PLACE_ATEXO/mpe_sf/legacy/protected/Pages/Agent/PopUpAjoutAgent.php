<?php

namespace Application\Pages\Agent;

use App\Service\Agent\HabilitationTypeProcedureService;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopUpAjoutAgent extends MpeTPage
{
    public $organisme;
    public $idService;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        if (isset($_GET['organisme'])) {
            $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['organisme']);
        } else {
            $this->organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        if (
            isset($_GET['idService']) &&
            $_GET['idService'] &&
            (
                Atexo_CurrentUser::hasHabilitation('HyperAdmin') ||
                !Atexo_CurrentUser::getIdServiceAgentConnected()
            )
        ) {
            $this->idService = Atexo_Util::atexoHtmlEntities($_GET['idService']);
        } else {
            $this->idService = Atexo_CurrentUser::getCurrentServiceId();
        }
        if (!$this->IsPostBack) {
            $this->servicesMetiersAccessibles->setOrg($this->organisme);
            $this->servicesMetiersAccessibles->displayCompenenant();
            $this->AtexoReferentielAgent->afficherReferentielAgent();
        }
        $this->getEntiteAchat();
    }

    /**
     * récupère le libellé d'une entité d'achat.
     */
    public function getEntiteAchat()
    {
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->entiteAchat->Text = (new Atexo_EntityPurchase())->getEntityPathById($this->idService, $this->organisme, true, true);
        } else {
            $this->entiteAchat->Text = (new Atexo_EntityPurchase())->getEntityPathById($this->idService, $this->organisme, true);
        }
    }

    /**
     * Ajoute un compte Agent.
     */
    public function updateAgentAccountInformation($sender, $param)
    {
        $pwd = null;
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $idService = $this->idService;

            $agent = new CommonAgent();
            $agent->setNom($this->nomAgent->Text);
            $agent->setPrenom($this->prenomAgent->Text);
            $agent->setNumTel($this->tel->Text);
            $agent->setNumFax($this->fax->Text);
            $agent->setEmail(trim($this->email->Text));
            $agent->setOrganisme($this->organisme);
            $agent->setServiceId($idService);
            $agent->setDateCreation(date('Y-m-d H:i:s'));
            $agent->setDateModification(date('Y-m-d H:i:s'));
            //Ajout d'alertes par defauts
            $agent->setAlerteReponseElectronique('1');
            $agent->setAlerteClotureConsultation('1');
            $agent->setAlerteReceptionMessage('1');
            $agent->setAlertePublicationBoamp('1');
            $agent->setAlerteEchecPublicationBoamp('1');
            $agent->setAlerteQuestionEntreprise('1');

            if ($this->agent->checked) {
                $agent->setElu('0');
            } else {
                $agent->setElu('1');
            }
            // Il faut vérifier l'unicité du login et mot de passe

            if (Atexo_Module::isEnabled('AuthenticateAgentByCert')) {
                $certif = $this->authentificationByCer->getCertificat();
                $serial = $this->authentificationByCer->getSerial();
                $agent->setCertificat($certif);
                $agent->setNumCertificat($serial);
                unlink($this->authentificationByCer->getCertFileTmp());
            }
            if (Atexo_Module::isEnabled('AuthenticateAgentByLogin')) {
                $agent->setLogin(trim($this->idAgent->Text));
                if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpAgent') && true == $this->pwdAutomatique->checked) {
                    $pwd = Atexo_Util::randomMdp(8);
                } else {
                    $pwd = $this->passwordAgent->Text;
                }
                Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $pwd);
            }

            //check on saisie_manuelle in configuration_organisme
            if ((new Atexo_Module())->isSaisieManuelleActive($this->organisme)) {
                $agent->setIdExterne($this->idExterneSSO->Text);
            }

            if ($this->verouille->checked) {
                $agent->setActif(0);
            }
            $connexionCom->beginTransaction();
            try {
                $result = $agent->save($connexionCom);
                if ($result > 0) {
                    $this->servicesMetiersAccessibles->onEnregistrerClick($agent->getId());
                    if (Atexo_Module::isEnabled('AuthenticateAgentByCert')) {
                        $agent->setLogin($agent->getId());
                        Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $agent->getId());
                        $agent->save($connexionCom);
                    }
                }
                $connexionCom->commit();
                $this->AtexoReferentielAgent->saveReferentielAgent($agent->getId(), $agent->getOrganisme());
                if (Atexo_Module::isEnabled('AuthenticateAgentByLogin')) {
                    if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpAgent') && true == $this->pwdAutomatique->checked) {
                        (new Atexo_Agent())->notifierAgent($agent, $pwd);
                    }
                }

                if (Atexo_Config::getParameter('ACTIVE_HABILITATION_V2')) {
                    $habilitationTypeProcedureService = Atexo_Util::getSfService(HabilitationTypeProcedureService::class);
                    $habilitationTypeProcedureService->createHabilitationsByProfile($agent->getId(), $this->servicesMetiersAccessibles->getProfilId());
                }
            } catch (Exception $e) {
                $connexionCom->rollback();
                throw $e;
            }

            $this->script->Text = '<script>'.$this->authentificationByCer->getMessage()."opener.document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();window.close();</script>";
        }
    }

    public function updateHabilitationProfil(int $agentId)
    {
        /** @var HabilitationTypeProcedureService $habilitationTypeProcService */
        $habilitationTypeProcService = Atexo_Util::getSfService(HabilitationTypeProcedureService::class);
        $habilitationTypeProcService->setTypeProcedureFromAgentServiceMetier($agentId);
    }

    /**
     * Verifier que le login est unique.
     */
    public function verifyLoginCreation($sender, $param)
    {
        $login = $this->idAgent->getText();
        if ($login) {
            if (strstr($login, ' ')) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_ERRONE', ['identifiant' => $login]);

                return;
            }
            $agent = (new Atexo_Agent())->retrieveAgentByLogin($login);
            if ($agent) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                return;
            }
        }
    }

    /**
     * Verifier que l'id externe est unique.
     */
    public function verifyIdExterneSsoCreation($sender, $param)
    {
        $idExterneSSO = $this->idExterneSSO->getText();

        if ($idExterneSSO) {
            if (strstr($idExterneSSO, ' ')) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->IdExterneSSOCreationValidator->ErrorMessage = Prado::localize('ID_EXTERNE_SSO', ['idExterneSSO' => $idExterneSSO]);

                return;
            }
            $agents = (new Atexo_Agent())->retrieveAgentsByExternalIdAndOrganisme($idExterneSSO, Atexo_CurrentUser::getOrganismAcronym());
            if ((is_countable($agents) ? count($agents) : 0) > 0) {
                $this->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";
                $param->IsValid = false;
                $this->IdExterneSSOCreationValidator->ErrorMessage = Prado::localize('ID_EXTERNE_SSO_ALREADY_EXISTS', ['idExterneSSO' => $idExterneSSO]);

                return;
            }
        }
    }

    /**
     * Verifier que l'adresse mail est unique.
     */
    public function verifyMailCreation($sender, $param): void
    {
        $scriptText = "<script>document.getElementById('divValidationSummary').style.display='';goToTopPage('divValidationSummary');</script>";

        $email = $this->email->getText();
        if ($email) {
            if (false == Atexo_Util::checkEmailFormat($email)) {
                $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('MSG_ERROR_FORMAT_EMAIL').' : '.$email;
                $this->script->Text = $scriptText;
                $param->IsValid = false;

                return;
            }
            if (Atexo_Module::isEnabled('UniciteMailAgent') && (new Atexo_Agent())->retrieveAgentByMail($email)) {
                $this->script->Text = $scriptText;
                $param->IsValid = false;
                $this->loginCreationValidatorEmail->ErrorMessage = Prado::localize('TEXT_EMAIL_EXISTE_DEJA', ['adresse mail' => $email]);

                return;
            }
        }
    }

    /**
     * Gere l'etat d'activation des validateurs selon un module et le choix de l'utilisateur
     * pour la génération automatique du mot de passe.
     *
     * @return bool
     *
     * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function DisableValidatorPwd()
    {
        if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpAgent')) {
            if ($this->pwdAutomatique->checked) {
                return false;
            }
        }

        return true;
    }
}
