<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe FormeEnvoiCourrierElectroniqueJAL.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FormeEnvoiCourrierElectroniqueJAL extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->IdConsultationSummary->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectroniqueJAL->setOrg(Atexo_CurrentUser::getCurrentOrganism());
        $this->TemplateEnvoiCourrierElectroniqueJAL->setRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectroniqueJAL->setIdAnnonceJAL(Atexo_Util::atexoHtmlEntities($_GET['idAnnonceJAL']));
        $this->TemplateEnvoiCourrierElectroniqueJAL->setPostBack($this->IsPostBack);
    }
}
