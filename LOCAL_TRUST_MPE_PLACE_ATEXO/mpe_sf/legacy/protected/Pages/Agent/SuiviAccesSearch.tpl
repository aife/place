		<com:ChoixEntiteAchat id="IdChoixEntiteAchat" />
		<com:ChoixOrganisme id="IdChoixOrganisme" callPagePanelRefresh="false" showPictoOk="false"/>
		<!--Debut Bloc Filtres acces-->
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
			<h3><com:TTranslate>DEFINE_FILTRES_ACCES</com:TTranslate></h3>
				<div class="line">
					<div class="intitule-150"><label for="nomAgent"><com:TTranslate>DEFINE_NOM_AGENT</com:TTranslate></label> :</div>
					<com:TTextBox id="nomAgent" Attributes.title="<%=Prado::localize('DEFINE_NOM_MAJ')%>" CssClass="small"  />
				</div>
				<div class="line">
					<div class="intitule-150"><label for="prenomAgent"><com:TTranslate>DEFINE_PRENOM_AGENT</com:TTranslate></label> :</div>
					<com:TTextBox id="prenomAgent" Attributes.title="<%=Prado::localize('DEFINE_PRENOM')%>" CssClass="small" />
				</div>
				<div class="line">
					<div class="intitule-150"><com:TTranslate>DEFINE_DATE_ACCES_ENTRE_LE</com:TTranslate> :</div>
					<div class="calendar">
					    <com:TTextBox  Text="" ID="datemiseAcccesStart" Attributes.title="<%=Prado::localize('DEFINE_ENTRE_LE')%>" CssClass="heure" />
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_IdSuiviAccesSearch_datemiseAcccesStart'),'dd/mm/yyyy','');" />
					</div>
					<div class="intitule-auto indent-10"><label for="datemiseEnLigneEnd"><com:TTranslate>DEFINE_DATE_ACCES_ET_LE</com:TTranslate> </label></div>
					<div class="calendar">
						<com:TTextBox Attributes.title="<%=Prado::localize('DEFINE_DATE_ACCES_ET_LE')%>" id="datemiseAcccesEnd" />
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_IdSuiviAccesSearch_datemiseAcccesEnd'),'dd/mm/yyyy','');"/>
					</div>
					<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
				</div>
				<!--Debut Line bouton recherche multi-criteres-->
				<div class="spacer"></div>
				<div class="boutons-line">
					<com:TButton CssClass="bouton-long-190 float-left" Text="<%=Prado::localize('DEFINE_EFFACER_CRITERES_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_EFFACER_CRITERES_RECHERCHE')%>" onClick="Page.deleteSearchCriteria"/>
					<com:TButton CssClass="bouton-moyen-120 float-right" Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" onclick="Page.search" />
				</div>
				<!--Fin Line bouton recherche multi-criteres-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Filtres acces-->
		<div class="breaker"></div>


