<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonResultatAnalyseDecision;
use Application\Propel\Mpe\CommonResultatAnalyseDecisionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class resultatAnalyse extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_CurrentUser::hasHabilitation('ResultatAnalyse')) {
            $this->response->redirect('index.php?page=Agent.TableauDeBord&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        }

        if (isset($_GET['ok'])) {
            $this->panelMessage->setVisible(true);
            $this->panelMessage->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
        }

        $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);

        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($idReference);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            $this->ConsultationSummary->setConsultation($consultation);

            if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_C_OA_DECISION')) {
                $this->etape3->setId('etape234');
            } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OA_DECISION')) {
                $this->etape3->setId('etape34');
            } elseif ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                $this->etape3->setId('etape4');
            }

            $lots = $consultation->getAllLots();
            if (!is_array($lots) || 0 == count($lots)) {
                $lots = [];
                $lots[] = $consultation;
            }

            $this->lots->DataSource = $lots;
            $this->lots->DataBind();
        }
    }

    public function infosProposition($dataItem)
    {
        if ($dataItem instanceof CommonCategorieLot) {
            $consultationId = $dataItem->getConsultationId();
            $lot = $dataItem->getLot();
        } else {
            $consultationId = $dataItem->getId();
            $lot = '0';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonResultatAnalyseDecisionPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonResultatAnalyseDecisionPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonResultatAnalyseDecisionPeer::LOT, $lot);
        $resultatAnalyse = CommonResultatAnalyseDecisionPeer::doSelectOne($c, $connexionCom);
        if (!$resultatAnalyse instanceof CommonResultatAnalyseDecision) {
            $resultatAnalyse = new CommonResultatAnalyseDecision();
        }

        $natureDecision = match ($resultatAnalyse->getTypeDecision()) {
            Atexo_Config::getParameter('RESULTAT_ANALYSE_A_DEFINIR') => Prado::localize('A_DEFINIR'),
            Atexo_Config::getParameter('RESULTAT_ANALYSE_ATTRIBUTION_PROPOSEE') => Prado::localize('ATTRIBUTION_PROPOSEE'),
            Atexo_Config::getParameter('RESULTAT_ANALYSE_INFRUCTUEUX') => Prado::localize('MARCHE_INFRUCTUEUX'),
            Atexo_Config::getParameter('RESULTAT_ANALYSE_AUTRE') => Prado::localize('AUTRE').' : '.$resultatAnalyse->getAutreTypeDecision(),
            default => Prado::localize('A_DEFINIR'),
        };

        $dateAnalyse = $resultatAnalyse->getDateDecision() ? '<br/>'.Atexo_Util::iso2frnDate($resultatAnalyse->getDateDecision()) : '';

        return $natureDecision.$dateAnalyse;
    }
}
