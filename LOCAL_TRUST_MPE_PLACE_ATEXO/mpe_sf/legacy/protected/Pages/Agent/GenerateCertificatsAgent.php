<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonCertificatAgent;
use Application\Propel\Mpe\CommonCertificatAgentPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class GenerateCertificatsAgent extends MpeTPage
{
    public string|\Application\Propel\Mpe\CommonOrganisme $_organisme = '';
    public string $_agent = '';
    public $_connexionCom;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->vbScript->Text = '';

        if (isset($_GET['idAgent']) && '' != $_GET['idAgent']) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->_agent = (new Atexo_Agent())->retrieveAgent(Atexo_Util::atexoHtmlEntities($_GET['idAgent']));
            $this->nomInscrit->Text = $this->_agent->getNom();
            $this->prenomInscrit->Text = $this->_agent->getPrenom();
            $this->emailInscrit->Text = $this->_agent->getEmail();
            $this->service->Text = $this->displayLibelleService();
            if (!$this->IsPostBack) {
                $certificat = (new Atexo_Agent())->retrieveCertificatAgentById($this->_agent->getId());
                if ($certificat instanceof CommonCertificatAgent) {
                    CommonCertificatAgentPeer::doDelete($certificat->getId(), $this->_connexionCom);
                }
            }
            if ($this->_agent instanceof CommonAgent) {
                $this->_organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_agent->getOrganisme());
            }
        }
        if ('OK' == $_POST['install_result']) {
            $this->response->redirect('index.php?page=Agent.GestionCertificatsAgents&idService='.$this->_agent->getServiceId().'&acronyme='.$this->_agent->getOrganisme());
        }
        if (!$this->IsPostBack) {
            $this->vbScript->Text = '<script language="VBSCRIPT">postLoad</script>';
        }
    }

    public function generateCert()
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (Atexo_Util::isCurrentClientWindows()) {
            if (!$_POST['pkcs10_req']) {
                self::writeErrorMessage(Prado::localize('REQUETE_CERT_INTROUVABLE'));

                return;
            }

            $arrayCert = (new Atexo_Crypto_Certificat())->generateCertificat($_POST['pkcs10_req'], $org);
            if (is_array($arrayCert)) {
                if ($arrayCert['pemCert']) {
                    $newCertificat = new CommonCertificatAgent();
                    $newCertificat->setIdAgent($this->_agent->getId());
                    $newCertificat->setNomAgent($this->_agent->getNom());
                    $newCertificat->setPrenomAgent($this->_agent->getPrenom());
                    $newCertificat->setMailAgent(trim($this->_agent->getEmail()));
                    $newCertificat->setIdService($this->_agent->getServiceId());
                    $newCertificat->setDateDebut(date('Y-m-d H:i:s'));
                    $newCertificat->setDateFin((new Atexo_Crypto_Certificat())->getDateExpiration($arrayCert['pemCert']));
                    $newCertificat->setStatutRevoque((Atexo_Config::getParameter('STATUT_CERTIFICAT_NON_REVOQUEE')));
                    $newCertificat->setCertificat($arrayCert['pemCert']);
                    $newCertificat->save($this->_connexionCom);
                }

                $this->certificate->Value = $arrayCert['pemCert'];
                $this->vbScript->Text = '<script language="VBSCRIPT">
												  		postLoad
												  		CertAcceptSub2
														</script>';
            } else {
                self::writeErrorMessage($arrayCert);

                return;
            }
        }
    }

    public function writeErrorMessage($errorMessage)
    {
        $this->javaScript->Text = "<script>document.getElementById('divValidationSummary').style.display='';document.getElementById('sslError').style.display='';document.getElementById('erreurSaisie').style.display='none';</script>";
        $this->error->Text = $errorMessage;
        $this->vbScript->Text = '<script language="VBSCRIPT">postLoad</script>';
    }

    public function displayLibelleService()
    {
        return Atexo_EntityPurchase::getSigleLibelleEntityById($this->_agent->getServiceId(), $this->_agent->getOrganisme());
    }
}
