<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class popUpGestionBicle extends MpeTPage
{
    protected $certificat;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
        Atexo_CurrentUser::writeToSession('certificat', null);
        if (isset($_GET['id'])) {
            $this->certificat = (new Atexo_Certificat())->retrieveCertificatById(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
            $this->nomBiCle->Text = $this->certificat->getNom();
            $this->destinataire->Checked = ('1' === $this->certificat->getMasterKey()) ? true : false;
        } else {
            throw new Atexo_Exception('Modification Impossible.');
        }
    }

    public function onLoad($param)
    {
    }

    public function modifierCertification($sender, $param): void
    {
        if (isset($_GET['id'])) {
            if ($this->certificat) {
                $this->certificat->setNom(Atexo_Util::atexoHtmlEntities($this->nomBiCle->Text));
                if ($this->destinataire->Checked) {
                    $this->certificat->setMasterKey('1');
                } else {
                    $this->certificat->setMasterKey('0');
                }
                //Ajout de l'id de l'agent qui effectue la création ou modification, le CSP et la date de modification
                $this->certificat->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                $date = new \DateTime();
                $this->certificat->setDateModification($date->format('Y-m-d H:i:s'));
                (new Atexo_Certificat())->saveCertificat($this->certificat, Atexo_CurrentUser::getCurrentOrganism());
                $this->scriptAddAndClose->Text = '<script>window.close();</script>';
            } else {
                $this->modification->Value = '0';
            }
        }
    }
}
