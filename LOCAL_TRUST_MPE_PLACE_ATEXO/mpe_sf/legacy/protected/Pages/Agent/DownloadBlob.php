<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;
use Prado\Util\TLogger;

class DownloadBlob extends MpeTPage
{
    private $idBlob;
    private $organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_CurrentUser::hasHabilitation('telechargementUnitairePlisChiffres') || Atexo_CurrentUser::hasHabilitation('TelechargementGroupeAnticipePlisChiffres')
          || Atexo_CurrentUser::hasHabilitation('OuvrirCandidatureEnLigne') || Atexo_CurrentUser::hasHabilitation('OuvrirOffreEnLigne')
          || Atexo_CurrentUser::hasHabilitation('OuvrirAnonymatEnLigne')) {
            if (!isset($_GET['organisme']) || !isset($_GET['idBlob'])) {
                Prado::log('DownloadBlob : Erreur manque des arguments', TLogger::ERROR, 'Error');
            } else {
                $this->organisme = Atexo_Util::atexoHtmlEntities($_GET['organisme']);
                $this->idBlob = Atexo_Util::atexoHtmlEntities($_GET['idBlob']);
            }

            $sql = "Select consultation_id from Offres, Enveloppe, fichierEnveloppe, blocFichierEnveloppe where blocFichierEnveloppe.id_blob_chiffre='".(new Atexo_Db())->quote($this->idBlob)."'"
                    .' AND blocFichierEnveloppe.id_fichier=fichierEnveloppe.id_fichier AND fichierEnveloppe.id_enveloppe=Enveloppe.id_enveloppe_electro AND Enveloppe.offre_id=Offres.id  '
                    ." AND Offres.organisme=Enveloppe.organisme AND fichierEnveloppe.organisme=blocFichierEnveloppe.organisme AND Enveloppe.organisme=fichierEnveloppe.organisme AND Offres.organisme= '".$this->organisme."'";

            $statement = Atexo_Db::getLinkCommon(true)->query($sql);

            $consultationId = '';
            foreach ($statement as $row) {
                $consultationId = $row[0];
            }

            if ($consultationId) {
                $critere = new Atexo_Consultation_CriteriaVo();
                $critere->setIdReference($consultationId);
                $critere->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['organisme']));
                $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultationArray = (new Atexo_Consultation())->search($critere);
                if (is_array($consultationArray)) {
                    $consultation = array_shift($consultationArray);
                    if ($consultation instanceof CommonConsultation && !$consultation->getCurrentUserReadOnly()) {
                        $atexoBlob = new Atexo_Blob();
                        echo $atexoBlob->return_blob_to_client($this->idBlob, $this->organisme);
                        exit;
                    }
                }
            }
        }
        echo 'Accès au blob non autorisé'.
        exit;
    }
}
