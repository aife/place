<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;
use Application\Service\Atexo\Atexo_Module;

class TelechargementPlisChiffres extends MpeTPage
{
    public function onInit($param)
    {

        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($this->isMAMPEnabled()) {
            MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
            MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
        }

        if (!$this->isPostBack) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            // TODO se limiter auc consultations OA seulement ??
            $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'));

            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->fillRepeaterWithDataForSearchResult($criteriaVo);
            if (!$this->getIsCallback()) {
                $this->panelMessageErreurTelechargement->hideMessage();
            }
        }
    }

    public function fillRepeaterWithDataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $consultation = new Atexo_Consultation();
        $nombreElement = $consultation->search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->panelBouttonGotoPageBottom->setVisible(true);
            $this->panelBouttonGotoPageTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $pageSize = $this->tableauDeBordRepeater->PageSize;
            $nombrePages = ceil((int) $nombreElement / (int) $pageSize);
            $this->nombrePageTop->Text = $nombrePages;
            $this->nombrePageBottom->Text = $nombrePages;
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->panelBouttonGotoPageBottom->setVisible(false);
            $this->panelBouttonGotoPageTop->setVisible(false);
        }
    }

    public function populateData(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $consultation = new Atexo_Consultation();
        $arrayConsultation = $consultation->search($criteriaVo);
        $this->tableauDeBordRepeater->DataSource = $arrayConsultation;
        $this->tableauDeBordRepeater->DataBind();
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->commandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->setViewState('sensTriArray', $arraySensTri);
        $this->setViewState('CriteriaVo', $criteriaVo);
        $this->populateData($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauDeBordRepeater->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauDeBordRepeater->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                              $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                           $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauDeBordRepeaterSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeaterSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeaterSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->tableauDeBordRepeater->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function infoBulleEtapeConnsultation($statusConsultation)
    {
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            return Prado::localize('ETAPE_OUVERTURE_ET_ANALYSE');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            return Prado::localize('ETAPE_ELABORATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            return Prado::localize('ETAPE_PREPARATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            return Prado::localize('ETAPE_CONSULTATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('ETAPE_DECISION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_O_A_D');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION').Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_C_O_A_D');
        }
    }

    /**
     * Pour insérer un retour à la ligne (<br>), tous les 22 caractères, dans une chaine.
     *
     * @param  $texte
     * @param  $everyHowManyCaracter
     *
     * @return string : retourne la chaine après y avoir insérer un retour à la
     *                ligne tous les $everyHowManyCaracter caractères
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br />', true);
    }

    public function truncate($texte)
    {
        $maximumNumberOfCaracterToReturn = 200;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText = null;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 200) ? '' : 'display:none';
    }

    public function checkAll($sender, $param)
    {
        foreach ($this->tableauDeBordRepeater->Items as $itemConsultation) {
            $itemConsultation->selection->setChecked($sender->getChecked());
        }
        $this->refreshButton->render($param->getNewWriter());
    }

    public function refreshButton($sender, $param)
    {
        $this->refreshButton->render($param->getNewWriter());
    }

    /**
     * Téléchargement en masse des plis avec l'assistant Marchés publics.
     *
     * @param $sender paramètre prado
     * @param $param  paramètre prado
     *
     * @return void
     *
     * @throws Atexo_Exception
     * @throws PropelException
     */
    public function telechargerPlis($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $offres = [];

        if ($this->isMAMPEnabled()) {
            $arraySelectedConsultations = [];

            foreach ($this->tableauDeBordRepeater->Items as $itemConsultation) {
                if (true === $itemConsultation->selection->getChecked()) {
                    $arraySelectedConsultations[] = $itemConsultation->idConsultation->Value;
                    $consultation = (new Atexo_Consultation())->retrieveConsultation(
                        $itemConsultation->idConsultation->Value,
                        Atexo_CurrentUser::getOrganismAcronym()
                    );
                    if (!$consultation) {
                        $exception = 'Consultation %s introuvable';
                        $exception = sprintf(
                            $exception,
                            $itemConsultation->idConsultation->Value
                        );
                        throw new Atexo_Exception($exception);
                    } else {
                        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                            $offresCons = $consultation->getOffres();
                            if (is_array($offresCons) && count($offresCons)) {
                                $offres = array_merge($offres, $offresCons);
                            }
                        }
                    }

                    if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                        $repDestination = $this->consultation_repDestination->Text;
                        $repDestination = (new Atexo_Config())->toPfEncoding($repDestination);
                        $consultation->setPathTelechargementPlis($repDestination);
                    }
                    // enregistrement de la date de premier téléchargement
                    if (!$consultation->getDateTelechargementPlis()) {
                        $consultation->setDateTelechargementPlis(date('Y-m-d H:i'));
                        $consultation->setIdAgentTelechargementPlis(Atexo_CurrentUser::getId());
                    }
                    $consultation->save($connexion);
                }
            }
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            if (is_array($arraySelectedConsultations) && count($arraySelectedConsultations) > 0) {
                if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                    if (is_array($offres) && count($offres) && $consultation->getChiffrementOffre() !== '0') {
                        $context = (new Atexo_Crypto())->getTelechargementCrypto(
                            $offres,
                            Atexo_CurrentUser::getId(),
                            $organisme,
                            false
                        );
                        if ($context) {
                            $this->panelMessageErreurTelechargement->hideMessage();
                            $this->JScript->Text = '';
                            if ($this->isMAMPEnabled()) {
                                $this->JScript->Text = MessageStatusCheckerUtil::getScript($context);
                            } else {
                                $this->JScript->Text = $this->getScript($context);
                            }
                        } else {
                            $this->panelMessageErreurTelechargement->setMessage(
                                Prado::localize('ERREUR_RECUPERATION_JNPL_TELCHARGEMENT')
                            );
                            $this->panelMessageErreurTelechargement->showMessage();
                        }
                    }
                }
            }
        } else {
            $arraySelectedConsultations = array();
            $liteOffres = array();
            foreach ($this->tableauDeBordRepeater->Items as $itemConsultation) {
                if ($itemConsultation->selection->getChecked() === true) {
                    $arraySelectedConsultations[] = $itemConsultation->idConsultation->Value;

                    // récupération de la consultation
                    $consultation = (new Atexo_Consultation)->retrieveConsultation($itemConsultation->idConsultation->Value, Atexo_CurrentUser::getOrganismAcronym());
                    if (!$consultation) {
                        throw new Atexo_Exception("Consultation " . $itemConsultation->idConsultation->Value ." introuvable");
                    } else {
                        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
                            $offresCons = $consultation->getOffres();
                            if (is_array($offresCons) && count($offresCons)) {
                                $liteOffres = array_merge($liteOffres, $offresCons);
                                $offres = array_merge($offres, $offresCons);
                            }
                        }
                    }

                    if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') || Atexo_Module::isEnabled('ModeApplet')) {
                        $consultation->setPathTelechargementPlis(Atexo_Config::toPfEncoding($this->consultation_repDestination->Text));
                    }
                    if (!$consultation->getDateTelechargementPlis()) { // enregistrement de la date de premier téléchargement
                        $consultation->setDateTelechargementPlis(date("Y-m-d H:i"));
                        $consultation->setIdAgentTelechargementPlis(Atexo_CurrentUser::getId());
                    }

                    $consultation->save($connexion);
                }
            }
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $fileXmlReponse = (new Atexo_Entreprise_Reponses)->getReponsesAnnoncesXml($arraySelectedConsultations, $organisme);
            $AllReponsesAnnonceEnCode = base64_encode($fileXmlReponse);
            if (is_array($arraySelectedConsultations) && count($arraySelectedConsultations)>0) {
                $urlDownloadBloc = Atexo_Util::getAbsoluteUrl() . Atexo_Config::getParameter('URL_DOWNLOAD_BLOC_CHIFFRE');
                if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
                    //serveur crypto
                    if (is_array($liteOffres) && count($liteOffres) && $consultation->getChiffrementOffre() !== '0') {
                        $resultat = (new Atexo_Crypto)->getTelechargementCrypto($liteOffres, Atexo_CurrentUser::getId(), $organisme, false);
                        if ($resultat) {
                            $this->panelMessageErreurTelechargement->hideMessage();
                            $name = str_replace("[COMPLET]", date("Y-m-d H:i"), utf8_decode(Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_TELECHARGEMENT_PLIS_CHIFFRER')));
                            $jnpl = array( "content" => $resultat, "name"=> $name);
                            $this->setViewState('resultatJnlp', $jnpl);
                            $this->JScript->Text = "<script> document.getElementById('ctl0_CONTENU_PAGE_downloadJnlp').click();</script>";
                        } else {
                            $this->panelMessageErreurTelechargement->setMessage(Prado::localize('ERREUR_RECUPERATION_JNPL_TELCHARGEMENT'));
                            $this->panelMessageErreurTelechargement->showMessage();
                        }
                    }
                } else {
                    $this->JScript->Text="<script>launchAppletOuverture('" .$urlDownloadBloc . "', '" . Atexo_CurrentUser::getCurrentOrganism() . "', '" . $AllReponsesAnnonceEnCode . "')</script>";
                }
            }
        }
        $this->refreshButton($sender, $param);
    }



    /*
     * Permet d'avoir les parametres pour l'applet MpeDechiffrementApplet
     */
    public function getParamsMpeDechiffrementApplet()
    {
        return array('methodeJavascriptDebutAttente'=>'afficherVeuillezPatienter','methodeJavascriptFinAttente'=>'masquerVeuillezPatienter','methodeJavascriptRenvoiResultat'=>'renvoiResultatTelechargement');
    }

    /*
     * Permet d'avoir les parametres pour l'applet SelectionFichierApplet
     */
    public function getParamsSelectionFichierApplet()
    {
        return array('methodeJavascriptDebutAttente'=>'showLoader','methodeJavascriptFinAttente'=>'showLoader','methodeJavascriptRenvoiResultat'=>'selectionEffectuee');
    }

    public function downloadJnlp()
    {
        $resultat = $this->getViewState('resultatJnlp');
        if (!empty($resultat["content"])) {
            $this->setViewState('resultatJnlp', null);
            header('Content-Length: '.strlen($resultat["content"]));
            header('Content-disposition: attachment; filename="' . $resultat["name"] .'"');
            header('Content-type: application/x-java-jnlp-file');
            echo $resultat["content"];
            exit;
        }
    }


    /**
     * Retourne l'appel js qui déclenche l'ouverture de l'assistant
     *
     * @param String $context Chaîne javascript  à renvoyer au navigateur
     *
     * @return string
     */
    public function getScript(String $context)
    {
        $scriptText = "<script>";
        $scriptText .= "document.getElementById('assistance-launch').setAttribute('data-context', '%s');";
        $scriptText .= "document.getElementById('assistance-launch-link').click();";
        $scriptText .= "</script>";
        return sprintf($scriptText, base64_encode($context));
    }
}
