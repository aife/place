<?php

namespace Application\Pages\Agent;

use App\Repository\ConsultationRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use App\Repository\FormePrixRepository;
use App\Repository\LotRepository;
use App\Service\AtexoGroupement;
use App\Service\ClausesService;
use App\Service\WebServices\WebServicesExec;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffrePapierQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprise;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTListeLotsCandidaturePeer;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeContratMpePivotQuery;
use Application\Propel\Mpe\CommonTypeContratPivotQuery;
use Application\Propel\Mpe\CommonTypeProcedureQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_CriteriaVo;
use Application\Service\Atexo\Consultation\InvitePermanentContrat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Popup d'attribution de la décision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class PopupDecisionAttribution extends MpeTPage
{
    public array $objectsForDuplicateClauses = [];

    public function setDataSource($v)
    {
        $this->setViewState('DataSource', $v);
    }

    public function getDataSource()
    {
        return $this->getViewState('DataSource');
    }

    public function setTypeDecision($v)
    {
        $this->setViewState('typeDecision', $v);
    }

    public function getTypeDecision()
    {
        return $this->getViewState('typeDecision');
    }

    public function setConsultation($v)
    {
        $this->setViewState('consultation', $v);
    }

    public function getConsultation()
    {
        return $this->getViewState('consultation');
    }

    public function setEnabledElement($v)
    {
        $this->setViewState('enabledElement', $v);
    }

    public function getEnabledElement()
    {
        return $this->getViewState('enabledElement');
    }

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $date = null;
        $commentaire = null;
        if (!$this->getIsPostBack()) {
            $messageErreur = null;
            $this->panelMessageErreur->setVisible(false);
            $logger = Atexo_LoggerManager::getLogger('contrat');
            $msgInfo = Prado::localize('MSG_INFO_SELECTION_TYPE_CONTRAT');
            if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                $msgInfo = Prado::localize('MSG_INFO_SELECTION_TYPE_CONTRAT_CHORUS');
            }
            $this->panelInfoTypeContrat->setMessage($msgInfo);
            if (isset($_GET['id']) && isset($_GET['lots'])) {
                $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                $lots = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots']));
                $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
                $this->setViewState('consultation', $consultation);
                $this->recapConsultation->initialiserComposant($consultationId, $lots);
                if ($consultation instanceof CommonConsultation) {
                    $listeIdsLot = explode('#', $lots);
                    $organisme = $consultation->getOrganisme();
                    $idTypeContrat = $consultation->getTypeMarche();
                    $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, $listeIdsLot[0], $organisme);
                    if ($decisionLot instanceof CommonDecisionLot) {
                        $enabledElement = false;
                        $date = $decisionLot->getDateDecision('d/m/Y');
                        $commentaire = $decisionLot->getCommentaire();
                        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                            /** @var $execWS WebServicesExec */
                            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                            $query = [
                                "page"                      => 0,
                                "size"                      => 50,
                                "idExternesConsultation"    => $consultationId,
                                "numeroLot"                 => $decisionLot->getLot()
                            ];

                            $response = $execWS->getContent('getContrats', $query);
                            $contratsFromExec = $response['content'] ?? [];

                            if (!empty($contratsFromExec[0]['type']['codeExterne'])) {
                                $firstTypeContrat = (new Atexo_TypeContrat())->retrieveTypeContratByLibelle('TYPE_CONTRAT_' . $contratsFromExec[0]['type']['codeExterne']);
                                $idTypeContrat = $firstTypeContrat->getIdTypeContrat();
                            }
                        } else {
                            $listeContrats = (new Atexo_Consultation_Contrat())->getListeContrats($decisionLot->getConsultationId(), $decisionLot->getOrganisme(), $listeIdsLot[0]);
                            if (is_array($listeContrats) && count($listeContrats)) {
                                $firstContrat = $listeContrats[0];
                                if ($firstContrat instanceof CommonTContratTitulaire) {
                                    $idTypeContrat = $firstContrat->getIdTypeContrat();
                                }
                            }
                        }
                    } else {
                        $enabledElement = true;
                    }
                    $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:none;');
                    if (isset($_GET['typeDecision'])) {
                        $this->setTypeDecision(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['typeDecision'])));
                    }

                    $this->fillTypeContrat();
                    $this->selectTypeContrat($idTypeContrat);
                    $this->fillInfoDecision($date, $commentaire);
                    $this->setEnabledElement($enabledElement);

                    if ($this->getTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR')) {
                        $this->panelTypeContrat->setVisible(true);
                        $this->panelInfo->setVisible(true);
                        $this->titreDecision->Text = Prado::Localize('DECISION_D_ATTRIBUTION');
                        $this->titrePopup->Text = Prado::Localize('ATTRUBUER_LOTS_A_UN_MEME_FOURNISSEUR');
                        $this->fillEntrepriseAttributaireAuMemeFournisseur($consultation, $listeIdsLot, $lots);
                    } else {
                        $this->panelInfo->setVisible(false);
                        if ($this->getTypeDecision() == Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
                            $this->panelTypeContrat->setVisible(false);
                            $this->titreDecision->Text = Prado::Localize('DECISION').' - '.Prado::Localize('SELECTION_ENTREPRISES');
                            $this->titrePopup->Text = Prado::Localize('DETAIL_DECISION');
                        } else {
                            if (
                                Atexo_Module::isEnabled('AcSadTransversaux')
                                && !Atexo_Config::getParameter('ACTIVE_EXEC_V2')
                                && (
                                    (new Atexo_Consultation_Contrat())
                                        ->isTypeContratAccordCadre($consultation->getTypeMarche())
                                    || (new Atexo_Consultation_Contrat())->isTypeContratSad($consultation->getTypeMarche())
                                )
                            ) {
                                $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:block;');
                                $this->decisionEntitesEligibles->setIdRef($consultationId);
                                $this->decisionEntitesEligibles->setNumLot($listeIdsLot[0]);
                                $this->decisionEntitesEligibles->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                                $this->decisionEntitesEligibles->setPostBack($this->getIsPostBack());
                            }
                            $this->panelTypeContrat->setVisible(true);
                            $this->titreDecision->Text = Prado::Localize('DECISION_D_ATTRIBUTION');
                            $this->titrePopup->Text = Prado::Localize('DETAIL_DE_L_ATTRIBUTION');
                        }
                        $this->fillEntrepriseAttributaire($consultation, $listeIdsLot);
                    }
                } else {
                    $logger->error('La consultation dont la reference est '.$consultationId." n'existe pas");
                    $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
                }
            } else {
                $logger->error("Erreur d'accés à la popup d'attribution sans les bons parametres ");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }

            if ($messageErreur) {
                $this->afficherMessageErreur($messageErreur);
            }
        }
    }

    /**
     * Permet de remplir la liste de type de contrat
     * Si $codeLibelle est renseigne, la fonction rempli la liste avec uniquement le type de contrat specifie.
     *
     * @param string $codeLibelle : le code du libelle du type de contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillTypeContrat()
    {
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat();
        $this->idTypeContrat->DataSource = $listeContrats;
        $this->idTypeContrat->DataBind();
    }

    /**
     * Permet d'initialiser la liste des types de contrat.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function selectTypeContrat($idTypeContrat)
    {
        if ($idTypeContrat) {
            $this->idTypeContrat->setSelectedValue($idTypeContrat);
            self::typeContratChanged();
        }
    }

    /**
     * Permet de remplir la liste des entreprise attributaire.
     *
     * @param CommonSconultation $consultation l'objet consultation
     * @param array              $lots         la liste des lots concernes
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillEntrepriseAttributaire($consultation, $lots)
    {
        $resultat = [];
        if ($consultation instanceof CommonConsultation && is_array($lots)) {
            $reponses = [];
            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
            $consultationId = (int) $consultation->getId();
            $organisme = $consultation->getOrganisme();
            if ($typeEnveloppe && $typeEnveloppe != Atexo_Config::getParameter('CANDIDATURE_SEUL')) {
                $i = 0;
                foreach ($lots as $lot) {
                    $resultat[$i]['lot'] = $lot;
                    $resultat[$i]['offresWithContrat'] = $this->getOffresWithContrat($lot);
                    if ($lot) {
                        $objetLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, (int) $lot, $consultation->getOrganisme());
                        if ($objetLot instanceof CommonCategorieLot) {
                            $resultat[$i]['elementTitle'] = Prado::Localize('LOT').' '.$objetLot->getLot().' - '.$objetLot->getDescription();
                        }
                    } else {
                        $resultat[$i]['elementTitle'] = $consultation->getReferenceUtilisateur().' - '.$consultation->getObjetTraduit();
                    }
                    $resultat[$i]['indexElement'] = $i;

                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($consultationId);
                    $criteriaResponse->setSousPli($lot);
                    $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                    $criteriaResponse->setOrganisme($organisme);
                    $criteriaResponse->setReturnReponses(false);
                    $criteriaResponse->setNomEntrepriseVisible(true);
                    $criteriaResponse->setEnveloppeStatutOuverte(true);
                    $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);

                    $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultationId, $typeEnveloppe, $lot, '', '', $organisme, false, false, true, true);
                    $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
                    $resultat[$i++]['reponses'] = $reponses;
                }
            } else {
                $i = 0;
                foreach ($lots as $lot) {
                    $resultat[$i]['lot'] = $lot;
                    $resultat[$i]['offresWithContrat'] = $this->getOffresWithContrat($lot);
                    if ($lot) {
                        $objetLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, (int) $lot, $consultation->getOrganisme());
                        if ($objetLot instanceof CommonCategorieLot) {
                            $resultat[$i]['elementTitle'] = Prado::Localize('LOT').' '.$objetLot->getLot().' - '.$objetLot->getDescription();
                        }
                    } else {
                        $resultat[$i]['elementTitle'] = $consultation->getReferenceUtilisateur().' - '.$consultation->getObjetTraduit();
                    }
                    $resultat[$i]['indexElement'] = $i;
                    $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveReponseAdmissible($consultationId, $lot, $organisme);
                    $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveReponsePapierAdmissible($consultationId, $lot, $organisme);
                    $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
                    $resultat[$i++]['reponses'] = $reponses;
                }
            }
        }
        $this->setDataSource($resultat);
        self::populateData();
    }

    /**
     * @param $consultation
     * @param $lots
     *
     * @throws Atexo_Config_Exception
     * @throws Atexo_Exception
     *                                TODO
     */
    public function fillEntrepriseAttributaireAuMemeFournisseur(
        CommonConsultation $consultation,
        array $lots,
        string $decodedLots
    ): void {
        $resultat = [];

        if ($consultation instanceof CommonConsultation && is_array($lots)) {
            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
            $consultationId = (int) $consultation->getId();
            $organisme = $consultation->getOrganisme();
            $j = 0;
            $resultat[$j]['elementTitle'] = '';
            $resultat[$j]['indexElement'] = $j;
            $resultat[$j]['lot'] = $decodedLots;
            $reponsesElectronique = [];
            $reponsesPapier = [];
            $i = 0;
            if ($typeEnveloppe && $typeEnveloppe != Atexo_Config::getParameter('CANDIDATURE_SEUL')) {
                foreach ($lots as $lot) {
                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($consultationId);
                    $criteriaResponse->setSousPli($lot);
                    $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                    $criteriaResponse->setOrganisme($organisme);
                    $criteriaResponse->setReturnReponses(false);
                    $criteriaResponse->setNomEntrepriseVisible(true);
                    $criteriaResponse->setEnveloppeStatutOuverte(true);
                    $reponsesElectronique[$i] = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                    $reponsesPapier[$i] = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultationId, $typeEnveloppe, $lot, '', '', $organisme, false, false, true, true);
                    ++$i;
                }
            } else {
                foreach ($lots as $lot) {
                    $reponsesElectronique[$i] = (new Atexo_Consultation_Responses())->retrieveReponseAdmissible($consultationId, $lot, $organisme);
                    $reponsesPapier[$i] = (new Atexo_Consultation_Responses())->retrieveReponsePapierAdmissible($consultationId, $lot, $organisme);
                    ++$i;
                }
            }

            $resultatReponsesElectronique = [];
            if (is_array($reponsesElectronique) && count($reponsesElectronique)) {
                $firstElement = $reponsesElectronique[0];
                unset($reponsesElectronique[0]);
                if (is_array($firstElement)) {
                    foreach ($firstElement as $oneElement) {
                        if ($oneElement instanceof CommonOffres) {
                            $notExiste = self::isElementNotExiste($reponsesElectronique, $oneElement->getId());
                            if (!$notExiste) {
                                $resultatReponsesElectronique[$oneElement->getId()] = $oneElement;
                            }
                        }
                    }
                }
            }
            $resultatReponsesPapier = [];
            if (is_array($reponsesPapier) && count($reponsesPapier)) {
                $firstElementPapier = $reponsesPapier[0];
                unset($reponsesPapier[0]);
                if (is_array($firstElementPapier)) {
                    foreach ($firstElementPapier as $oneElement) {
                        if ($oneElement instanceof CommonOffrePapier) {
                            $notExiste = self::isElementNotExiste($reponsesPapier, $oneElement->getId());
                            if (!$notExiste) {
                                $resultatReponsesPapier[$oneElement->getId()] = $oneElement;
                            }
                        }
                    }
                }
            }

            $reponses = Atexo_Util::arrayMerge(true, $resultatReponsesElectronique, $resultatReponsesPapier);
            $resultat[$j]['reponses'] = $reponses;
        }
        $this->setDataSource($resultat);
        self::populateData();
    }

    /**
     * TODO.
     */
    public function isElementNotExiste($reponsesElectronique, $idOffre)
    {
        $notExiste = false;
        if (is_array($reponsesElectronique)) {
            foreach ($reponsesElectronique as $reponseElectronique) {
                if (is_array($reponseElectronique)) {
                    $notExiste = true;
                    foreach ($reponseElectronique as $offre) {
                        if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
                            if ($idOffre == $offre->getId()) {
                                $notExiste = false;
                            }
                        }
                    }
                }
            }
        }

        return $notExiste;
    }

    /**
     * Permet de sauvegarde l'attribution.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function saveAttribution($sender, $param)
    {
        $connexion = null;
        $enableButtonScript = "<script type='text/javascript'>
                var button = document.getElementById('ctl0_CONTENU_PAGE_buttonSave');
                button.style.cursor = 'pointer';
                button.style.pointerEvents = 'auto';
                button.disabled = false;
            </script>";
        if ($this->isValid) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                $lots = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots']));
                $listeIdsLot = explode('#', $lots);
                $elementChecked = false;
                $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId);
                if ($consultation instanceof CommonConsultation) {
                    $organisme = $consultation->getOrganisme();
                    $serviceId = $consultation->getServiceId();
                    $idTypeContrat = $this->idTypeContrat->getSelectedValue();
                    if ($this->getTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR')) {
                        //Initier le contrat multi si le type de contrat est parametre avec chapeau
                        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idTypeContrat)) {
                            $this->initialiserObjetContratMulti($idTypeContrat, $connexion);
                        }
                        self::saveAttributionAuMemeFournisseur($elementChecked, $listeIdsLot, $consultation, $consultationId, $organisme, $connexion, $serviceId, $idTypeContrat);
                    } else {
                        self::saveDecisionAttribution($elementChecked, $consultation, $consultationId, $organisme, $connexion, $serviceId, $idTypeContrat);
                    }
                    if ($elementChecked) {
                        $connexion->commit();
                        //Duplication des clauses après la création rééles des contrats
                        foreach ($this->objectsForDuplicateClauses as $forDuplicateClause) {
                            Atexo_Consultation::duplicateAchatsResponsablesFromConsultation(
                                $forDuplicateClause[0],
                                $forDuplicateClause[1]
                            );
                        }
                    } else {
                        $connexion->rollback();
                    }
                    $this->labelScript->Text = "<script type='text/javascript'>
                            window.opener.location.reload(true);
                            window.close();
                    </script>";
                }
            } catch (\Exception $e) {
                $connexion->rollback();
                Prado::log("Erreur d'attribution de marche : ".$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionAttribution.php');
                $this->labelScript->Text = $enableButtonScript;
            }
        } else {
            $this->labelScript->Text = $enableButtonScript;
        }
    }

    /**
     * TODO.
     */
    public function saveDecisionAttribution(&$elementChecked, CommonConsultation $consultation, $consultationId, $organisme, $connexion, $serviceId, $idTypeContrat)
    {
        $logger = Atexo_LoggerManager::getLogger('contrat');
        foreach ($this->repeaterAttribution->Items as $parentItem) {
            $lot = $parentItem->numeroLot->value;
            //Initier le contrat multi si le type de contrat est parametre avec chapeau
            if (!Atexo_Config::getParameter('ACTIVE_EXEC_V2') && (new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idTypeContrat)) {
                $this->initialiserObjetContratMulti($idTypeContrat, $connexion, $lot);
            }
            $contratMultiO = $this->getViewState('ContratMulti');
            $numeroLong = $this->getViewState('numeroLongContratMulti');
            $dateDecision = Atexo_Util::frnDate2iso($this->dateDecision->getSafeText());
            $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, $lot, $organisme);
            if (!$decisionLot instanceof CommonDecisionLot) {
                $decisionLot = new CommonDecisionLot();
                $decisionLot->setConsultationId($consultationId);
                $decisionLot->setLot($lot);
                $decisionLot->setIdTypeDecision($this->getTypeDecision());
                $decisionLot->setDateDecision($dateDecision);
                $decisionLot->setDateMaj(date('Y-m-d H:i:s'));
                $decisionLot->setCommentaire($this->commentaire->getSafeText());
                $decisionLot->setOrganisme($organisme);
                $decisionLot->save($connexion);
            }

            $j = (new Atexo_Consultation_Contrat())->getNextIncrementNumeroLong($consultationId, $organisme, $lot, $connexion);
            foreach ($parentItem->repeaterAttributionDetail->Items as $item) {
                if ($item->attributaire->checked) {
                    $idEntrepriseValue = $item->idEntreprise->Value;
                    $idEntreprise = intval($idEntrepriseValue);
                    $idOffre = $item->idOffre->Value;
                    $idEtablissement = $item->idEtablissement->Value;

                    $logger->info("Debut de creation du contrat pour la consulation '".$consultationId."' lot = '".$lot."' id entreprise '".$idEntrepriseValue."' id etablissement '".$idEtablissement."' id offre '".$idOffre."'");
                    if ($idEntreprise && is_int($idEntreprise)) {
                        $company = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexion, false);
                        if ($company instanceof Entreprise) {
                            $elementChecked = true;
                            $typeEnv = $item->typeEnv->Value;
                            $idInscrit = ('null' === $item->idInscrit->Value || empty($item->idInscrit->Value)) ? null : $item->idInscrit->Value;
                            if ($this->getTypeDecision() != Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
                                $contratTitulaire = new CommonTContratTitulaire();
                                $contratTitulaire->setOrganisme($organisme);
                                $contratTitulaire->setServiceId($serviceId);
                                $contratTitulaire->setIdAgent(Atexo_CurrentUser::getId());
                                $contratTitulaire->setNomAgent(Atexo_CurrentUser::getLastName());
                                $contratTitulaire->setPrenomAgent(Atexo_CurrentUser::getFirstName());
                                $contratTitulaire->setIdTypeContrat($idTypeContrat);
                                $contratTitulaire->setStatutContrat(Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR'));
                                $contratTitulaire->setIdTitulaire($idEntreprise);
                                $contratTitulaire->setIdTitulaireEtab($idEtablissement);
                                $contratTitulaire->setIdOffre($idOffre);
                                $contratTitulaire->setTypeDepotReponse($typeEnv);
                                $contratTitulaire->setDateCreation(date('Y-m-d H:i:s'));
                                $contratTitulaire->setDateModification(date('Y-m-d H:i:s'));
                                $contratTitulaire->setDateAttribution($dateDecision);

                                $contratTitulaire->setSiret((new Atexo_Consultation())->getSiretByService($organisme, $serviceId));
                                $nomEntiteAcheteur = (new Atexo_Consultation())->getOrganismeDenominationByService($organisme, $serviceId);
                                $nomEntiteAcheteur = explode('(', $nomEntiteAcheteur);
                                $contratTitulaire->setNomEntiteAcheteur($nomEntiteAcheteur[0]);
                                $contratTitulaire->setUuid((new Atexo_MpeSf())->uuid());

                                [$labelCategorie, $codeCPV1, $codeCPV2, $ccag, $lotId] = $this->fillContratByDataComplementaire($contratTitulaire, $lot, $consultation);
                                if ($contratMultiO instanceof CommonTContratMulti) {
                                    $contratTitulaire->setIdContratMulti($contratMultiO->getIdContratTitulaire());
                                    if ($this->numerotationAGenerer($contratTitulaire)) {
                                        $numeroCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contratTitulaire, $connexion);
                                        $contratTitulaire->setNumeroContrat($numeroCourt);
                                        $contratTitulaire->setNumLongOeap($numeroLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                    }
                                } else {
                                    if ($this->numerotationAGenerer($contratTitulaire)) {
                                        $numeroCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contratTitulaire, $connexion);
                                        $numeroLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($contratTitulaire, $connexion);
                                        $contratTitulaire->setNumeroContrat($numeroCourt);
                                        $contratTitulaire->setNumLongOeap($numeroLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                    }
                                }
                                $contratTitulaire->setReferenceLibre($consultation->getReferenceUtilisateur());
                                $contratTitulaire->setReferenceConsultation($consultation->getReferenceUtilisateur());

                                $typeContratMpeQuery = new CommonTypeContratMpePivotQuery();
                                $typeContratMpePivot = $typeContratMpeQuery->findOneById($idTypeContrat);
                                $idTypeContratPivot = $typeContratMpePivot->getIdTypeContratPivot();

                                $typeContratPivotQuery = new CommonTypeContratPivotQuery();
                                $typeContratPivot = $typeContratPivotQuery->findOneById($idTypeContratPivot);
                                $contratTitulaire->setLibelleTypeContratPivot($typeContratPivot->getLibelle());

                                $organismeQuery = new CommonOrganismeQuery();
                                $organismeObj = $organismeQuery->findOneByAcronyme($organisme);
                                $contratTitulaire->setSiretPaAccordCadre($organismeObj->getSiren().$organismeObj->getComplement());

                                $typeContratQuery = new CommonTTypeContratQuery();
                                $typeContrat = $typeContratQuery->findOneByIdTypeContrat($idTypeContrat);

                                $contratTitulaire->setAcMarcheSubsequent(1 == $typeContrat->getAccordCadreSad() ? true : false);

                                $typeProcedureQuery = new CommonTypeProcedureQuery();
                                $typeProcedure = $typeProcedureQuery->findOneByIdTypeProcedure($consultation->getIdTypeProcedure());

                                $contratTitulaire->setLibelleTypeProcedureMpe($typeProcedure->getLibelleTypeProcedure());

                                if ($consultation->getAlloti()) {
                                    $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);

                                    $criteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
                                    $criteria->add(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $consultation->getId());
                                    $criteria->add(CommonTListeLotsCandidaturePeer::STATUS, Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                    $criteria->addGroupByColumn(CommonTListeLotsCandidaturePeer::NUM_LOT);

                                    $nbOffreDemat = CommonTListeLotsCandidaturePeer::doCount($criteria, $con);

                                    $criteria2 = new Criteria(CommonOffrePapierPeer::DATABASE_NAME);
                                    $criteria2->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultation->getId());
                                    $criteria2->add(CommonOffrePapierPeer::STATUT_OFFRE_PAPIER, Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                    $criteria2->addJoin(CommonOffrePapierPeer::ID, CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, Criteria::LEFT_JOIN);
                                    $criteria->addGroupByColumn(CommonEnveloppePapierPeer::SOUS_PLI);

                                    $nbOffrePapier = CommonOffrePapierPeer::doCount($criteria2, $con);
                                } else {
                                    $nbOffreDemat = $consultation->getNombreOffreElectronique();
                                    $nbOffrePapier = $consultation->getNombreOffrePapier();
                                }

                                $contratTitulaire->setNbTotalPropositionsLot($nbOffreDemat + $nbOffrePapier);
                                $contratTitulaire->setNbTotalPropositionsDematLot($nbOffreDemat);

                                if ($consultation->getIdContrat()) {
                                    $lienACSad = (new Atexo_Consultation_Contrat())->getLienACByIdContratAndIdTitulaire($consultation->getIdContrat(), $idEntreprise);
                                    if (is_null($lienACSad)) {
                                        $lienACSad = $consultation->getIdContrat();
                                    }
                                    $contratTitulaire->setLienAcSad($lienACSad);
                                }

                                $contactContrat = new CommonTContactContrat();
                                $contactContrat->setNom($item->nom->Text);
                                $contactContrat->setPrenom($item->prenom->Text);
                                $contactContrat->setEmail($item->email->Text);
                                $contactContrat->setTelephone($item->telephone->Text);
                                $contactContrat->setFax($item->fax->Text);
                                $contactContrat->setIdEtablissement($idEtablissement);
                                $contactContrat->setIdEntreprise($idEntreprise);
                                $contactContrat->setIdInscrit($idInscrit);
                                $contratTitulaire->setCommonTContactContrat($contactContrat);

                                $responses = null;
                                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                                    $formePrixMapping = [
                                        'PF'    => 'FORFAITAIRE',
                                        'PU'    => 'UNITAIRE',
                                        'PM'    => 'MIXTE'
                                    ];
                                    $typePrixMapping = [
                                        'Prix révisables'                   => ['TP_REVISABLE'],
                                        'Prix actualisables'                => ['TP_ACTUALISABLE'],
                                        'Prix fermes'                       => ['TP_FERME'],
                                        'Prix fermes et actualisables'      => ['TP_FERME', 'TP_ACTUALISABLE'],
                                        'Prix actualisables et révisables'  => ['TP_ACTUALISABLE', 'TP_REVISABLE']
                                    ];
                                    $typeGroupementMapping = [
                                        'TYPE_GROUPEMENT_SOLIDAIRE'                 => 'SOLIDAIRE',
                                        'TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE'    => 'CONJOINT',
                                        'TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE'        => 'CONJOINT'
                                    ];

                                    $etablissementQuery = new CommonTEtablissementQuery();
                                    $etablissement = $etablissementQuery->getEtabissementbyId($idEtablissement);
                                    $entrepriseQuery = new CommonEntrepriseQuery();
                                    $entreprise = $entrepriseQuery->getEntrepriseById($idEntreprise);
                                    $offreQuery = new CommonOffresQuery();
                                    $offre = $offreQuery->getOffreById($idOffre);

                                    $refCCAG = null;
                                    if (!empty($ccag)) {
                                        $refCCAG = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), $ccag);
                                        $refCCAG = !empty($refCCAG) ? $refCCAG->getLibelleValeurReferentiel() : null;
                                    }
                                    $lieuExecution = $consultation->getLieuExecution();
                                    $arIds = explode(',', $lieuExecution);
                                    $geolocalisations = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
                                    $lieuExecutionForExec = '';
                                    foreach ($geolocalisations as $geolocalisation) {
                                        if (!empty($lieuExecutionForExec)) {
                                            $lieuExecutionForExec .= ', ';
                                        }
                                        $lieuExecutionForExec .= $geolocalisation->getDenomination1();
                                    }
                                    $typeContratQuery = new CommonTTypeContratQuery();
                                    $typeContrat = $typeContratQuery->findOneByIdTypeContrat($idTypeContrat);
                                    $typeContratForExec = str_replace('TYPE_CONTRAT_', '', $typeContrat->getLibelleTypeContrat());

                                    $trimedCodeCpv = trim($codeCPV2, '#');
                                    $cpvSecondaires = $trimedCodeCpv ? explode('#', $trimedCodeCpv) : [];

                                    if (empty($lotId)) {
                                        /** @var ConsultationRepository $consultationRepository */
                                        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
                                        $objectSf = $consultationRepository->find($consultationId);
                                    } else {
                                        /** @var LotRepository $lotRepository */
                                        $lotRepository = Atexo_Util::getSfService(LotRepository::class);
                                        $objectSf = $lotRepository->find($lotId);
                                    }
                                    if (!empty($objectSf->getDonneeComplementaire()?->getIdFormePrix())) {
                                        /** @var FormePrixRepository $formePrixRepository */
                                        $formePrixRepository = Atexo_Util::getSfService(FormePrixRepository::class);
                                        $formePrixObject = $formePrixRepository->find($objectSf->getDonneeComplementaire()->getIdFormePrix());
                                        if ($formePrixObject) {
                                            $formePrix = $formePrixMapping[$formePrixObject->getFormePrix()];

                                            $typeBorne = empty($formePrixObject->getIdMinMax()) ? 'AUCUNE' : 'MINIMALE_MAXIMALE';
                                            $borneMinimale = $formePrixObject->getPuMin();
                                            $borneMaximale = $formePrixObject->getPuMax();

                                            $typesPrix = [];
                                            /** @var FormePrixPuHasRefVariationRepository $formePrixPuVariationRepository */
                                            $formePrixPuVariationRepository = Atexo_Util::getSfService(FormePrixPuHasRefVariationRepository::class);
                                            $variationPu = $formePrixPuVariationRepository->findOneBy(['idFormePrix' => $formePrixObject->getIdFormePrix()]);
                                            if (!empty($variationPu)) {
                                                $refs = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef(
                                                    $variationPu->getIdVariation(),
                                                    Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX')
                                                );
                                                $typesPrix = array_merge($typesPrix, $typePrixMapping[$refs->getLibelleValeurReferentiel()]);
                                            }
                                            /** @var FormePrixPfHasRefVariationRepository $formePrixPfVariationRepository */
                                            $formePrixPfVariationRepository = Atexo_Util::getSfService(FormePrixPfHasRefVariationRepository::class);
                                            $variationPf = $formePrixPfVariationRepository->findOneBy(['idFormePrix' => $formePrixObject->getIdFormePrix()]);
                                            if (!empty($variationPf)) {
                                                $refs = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef(
                                                    $variationPf->getIdVariation(),
                                                    Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX')
                                                );
                                                $typesPrix = array_merge($typesPrix, $typePrixMapping[$refs->getLibelleValeurReferentiel()]);
                                            }
                                            $typesPrix = ['typePrix' => $typesPrix];
                                        }
                                    }

                                    /** @var AtexoGroupement $groupementEntrepriseService */
                                    $groupementEntrepriseService = Atexo_Util::getSfService(AtexoGroupement::class);
                                    $groupementEntreprise = $groupementEntrepriseService->getGroupementByOffreId($idOffre);
                                    $typeGroupementOperateurs = 'AUCUN';
                                    if (!empty($groupementEntreprise)) {
                                        $typeGroupementOperateurs = $typeGroupementMapping[$groupementEntreprise->getIdTypeGroupement()->getLibelleTypeGroupement()];
                                    }

                                    /** @var ClausesService $clausesService */
                                    $clausesService = Atexo_Util::getSfService(ClausesService::class);
                                    $clauses = $clausesService->getFormatedClausesForApiInjectionWithSlug($objectSf);
                                    $clauses = empty($clauses) ? null : ['clauses' => $clauses];

                                    $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
                                    $criteriaVo->setOrgAcronym(Atexo_CurrentUser::getOrganismAcronym());
                                    $criteriaVo->setIdReference($consultationId);
                                    $criteriaVo->setReadOnlyGuests(false);
                                    $invitedGuests = (new Atexo_Consultation_Guests())->retrieveInvitedGuests($criteriaVo);
                                    $criteriaVo->setReadOnlyGuests(true);
                                    $readOnlyGuests = (new Atexo_Consultation_Guests())->retrieveInvitedGuests($criteriaVo);
                                    $invitesPonctuels = [];
                                    foreach ($invitedGuests as $invitedGuest) {
                                        $invitesPonctuels[] = [
                                            'agentId'       => $invitedGuest->getId(),
                                            'hasHabilitation'  => true
                                        ];
                                    }
                                    foreach ($readOnlyGuests as $readOnlyGuest) {
                                        $invitesPonctuels[] = [
                                            'agentId'       => $readOnlyGuest->getId(),
                                            'hasHabilitation'  => false
                                        ];
                                    }
                                    $invitesPonctuels = ['invitePonctuel' => $invitesPonctuels];

                                    $intituleContrat = $consultation->getIntitule() . (!empty($lotId) ? (' - ' . $objectSf->getDescription()) : '');
                                    $objetContrat = $consultation->getObjet() . (!empty($lotId) ? (' - ' . $objectSf->getDescription()) : '');
                                    $bodyContrat = [
                                        'envoi' => [
                                            'contrat'   => [
                                                "idConsultation"                        => $consultation->getId(),
                                                "organisme"                             => $organisme,
                                                "idCreateur"                            => Atexo_CurrentUser::getId(),
                                                "idTitulaire"                           => $idEntreprise,
                                                "horsPassation"                         => 0,
                                                "idService"                             => $serviceId,
                                                "type"                                  => [
                                                    "codeExterne"                           => $typeContratForExec
                                                ],
                                                "referenceConsultation"                 => $consultation->getReferenceUtilisateur(),
                                                "intitule"                              => $intituleContrat,
                                                "objet"                                 => $objetContrat,
                                                "naturePrestation"                      => strtoupper($labelCategorie),
                                                "lieuExecutions"                        => $lieuExecutionForExec,
                                                "ccagApplicable"                        => $refCCAG,
                                                "cpv"                                   => [
                                                    "codePrincipal"                         => $codeCPV1,
                                                    "codeSecondaire1"                       => $cpvSecondaires[0] ?? null,
                                                    "codeSecondaire2"                       => $cpvSecondaires[1] ?? null,
                                                    "codeSecondaire3"                       => $cpvSecondaires[2] ?? null
                                                ],
                                                "formePrix"                             => $formePrix ?? null,
                                                "typeBorne"                             => $typeBorne ?? null,
                                                "borneMinimale"                         => $borneMinimale ?? null,
                                                "borneMaximale"                         => $borneMaximale ?? null,
                                                "typesPrix"                             => $typesPrix ?? null,
                                                "publicationDonneesEssentielles"        => 1,
                                                "defenseOuSecurite"                     => 0,
                                                "idOffre"                               => $idOffre,
                                                "clausesRSE"                            => $clauses,
                                                "typeProcedure"                         => $consultation->getTypeProcedureOrg()->getAbbreviation(),
                                                "numeroProjetAchat"                     => $consultation->getNumeroProjetAchat() ?? $consultation->getCodeExterne(),
                                                "decisionAttribution"                   => $dateDecision,
                                                "numeroLot"                             => empty($lot) ? null : $lot,
                                                "offresRecues"                          => $consultation->getNombreOffreElectronique(),
                                                "typeGroupementOperateurs"              => $typeGroupementOperateurs,
                                                "origineUE"                             => $offre?->getTauxProductionEurope(),
                                                "origineFrance"                         => $offre?->getTauxProductionFrance(),
                                                "etablissementTitulaire"                => [
                                                    "id"                                    => $etablissement->getIdEtablissement(),
                                                    "siege"                                 => $etablissement->getEstSiege() === '1',
                                                    "adresse"                               => [
                                                        "codePostal"                            => $etablissement->getCodePostal(),
                                                        "pays"                                  => $etablissement->getPays(),
                                                        "rue"                                   => $etablissement->getAdresse(),
                                                        "ville"                                 => $etablissement->getVille(),
                                                    ],
                                                    "siret"                                 => $entreprise->getSiren() . $etablissement->getCodeEtablissement(),
                                                    "fournisseur"                           => [
                                                        "id"                                    => $entreprise->getId(),
                                                        "nom"                                   => $entreprise->getNom(),
                                                        "raisonSociale"                         => $entreprise->getNom(),
                                                        "siren"                                 => $entreprise->getSiren(),
                                                    ]
                                                ],
                                                "contactTitulaire"                      => [
                                                    "id"                                    => $idInscrit,
                                                    "nom"                                   => $item->nom->Text,
                                                    "prenom"                                => $item->prenom->Text,
                                                    "telephone"                             => $item->telephone->Text,
                                                    "email"                                 => $item->email->Text
                                                ],
                                                "invitesPonctuels"                      => $invitesPonctuels,
                                                "lienAcSad"                             => $consultation->getContratExecUuid()
                                            ]
                                        ]
                                    ];

                                    /** @var $execWS WebServicesExec */
                                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                    $responses = $execWS->getContent('createContrat', $bodyContrat);
                                }
                                $donneesConsultation = new CommonTDonneesConsultation();
                                (new Atexo_Consultation_Contrat())->fillDonneesConsultation($donneesConsultation, $consultation);

                                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                                    $uuidExterneExec = $responses['content'][0]['uuid'] ?? null;
                                    $donneesConsultation->setUuidExterneExec($uuidExterneExec);
                                } else {
                                    $donneesConsultation->setCommonTContratTitulaire($contratTitulaire);
                                }

                                $donneesConsultation->save($connexion);

                                if (empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
                                    //Reprendre les donnees achats responsables
                                    if (Atexo_Module::isEnabled('ConsultationClause')) {
                                        $this->reprendreClausesAchatsResponsablesDepuisConsultation($contratTitulaire, [$lot]);
                                    }

                                    $lotContrat = new CommonTConsLotContrat();
                                    $lotContrat->setOrganisme($organisme);
                                    $lotContrat->setConsultationId($consultationId);
                                    $lotContrat->setLot($lot);
                                    $lotContrat->setIdContratTitulaire($contratTitulaire->getIdContratTitulaire());
                                    $lotContrat->setCommonTContratTitulaire($contratTitulaire);
                                    $lotContrat->save($connexion);

                                    self::saveInvitationConsultationTransverse($contratTitulaire, $consultation->getId(), $dateDecision, $lot, $connexion);
                                }
                            } else {
                                $decisionEntreprise = new CommonTDecisionSelectionEntreprise();
                                $decisionEntreprise->setConsultationId($consultationId);
                                $decisionEntreprise->setOrganisme($organisme);
                                $decisionEntreprise->setServiceId($serviceId);
                                $decisionEntreprise->setLot($lot);
                                $decisionEntreprise->setIdEntreprise($idEntreprise);
                                $decisionEntreprise->setIdEtablissement($idEtablissement);
                                $decisionEntreprise->setIdOffre($idOffre);
                                $decisionEntreprise->setTypeDepotReponse($typeEnv);
                                $decisionEntreprise->setDateCreation(date('Y-m-d H:i:s'));
                                $decisionEntreprise->setDateModification(date('Y-m-d H:i:s'));

                                $contactContrat = new CommonTContactContrat();
                                $contactContrat->setNom($item->nom->Text);
                                $contactContrat->setPrenom($item->prenom->Text);
                                $contactContrat->setEmail($item->email->Text);
                                $contactContrat->setTelephone($item->telephone->Text);
                                $contactContrat->setFax($item->fax->Text);
                                $contactContrat->setIdEtablissement($idEtablissement);
                                $contactContrat->setIdEntreprise($idEntreprise);
                                $contactContrat->setIdInscrit($idInscrit);
                                $decisionEntreprise->setCommonTContactContrat($contactContrat);
                                $decisionEntreprise->save($connexion);
                            }
                        } else {
                            $logger->error("L'entreprise dont l'id => ".$idEntreprise." n'existe pas");
                        }
                    } else {
                        $logger->error('Id entreprise => '.$idEntreprise.' est invalide');
                    }
                    $logger->info("Fin de creation du contrat pour la consulation '".$consultationId."' lot = '".$lot."' id entreprise '".$idEntrepriseValue."' id etablissement '".$idEtablissement."' id offre '".$idOffre."'");
                }
            }
        }
    }

    /**
     *TODO.
     */
    public function saveAttributionAuMemeFournisseur(&$elementChecked, $lots, $consultation, $consultationId, $organisme, $connexion, $serviceId, $idTypeContrat)
    {
        $contratMultiO = $this->getViewState('ContratMulti');
        $numeroLong = $this->getViewState('numeroLongContratMulti');
        $dateDecision = Atexo_Util::frnDate2iso($this->dateDecision->getSafeText());
        $logger = Atexo_LoggerManager::getLogger('contrat');
        foreach ($this->repeaterAttribution->Items as $parentItem) {
            $j = (new Atexo_Consultation_Contrat())->getNextIncrementNumeroLong($consultationId, $organisme, $lots, $connexion);

            foreach ($parentItem->repeaterAttributionDetail->Items as $item) {
                if ($item->attributaire->checked) {
                    $idEntrepriseValue = $item->idEntreprise->Value;
                    $idEntreprise = intval($idEntrepriseValue);
                    $idEtablissement = $item->idEtablissement->Value;
                    $idOffre = $item->idOffre->Value;
                    $logger->info("Debut de creation du contrat pour la consulation '".$consultationId." id entreprise '".$idEntreprise."' id etablissement '".$idEtablissement."' id Offre '".$idOffre."'");
                    if ($idEntreprise && is_int($idEntreprise)) {
                        $company = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexion, false);
                        if ($company instanceof Entreprise) {
                            $elementChecked = true;

                            $typeEnv = $item->typeEnv->Value;
                            $idInscrit = 'null' === $item->idInscrit->Value ? null : $item->idInscrit->Value;
                            ;

                            $contratTitulaire = new CommonTContratTitulaire();
                            $contratTitulaire->setOrganisme($organisme);
                            $contratTitulaire->setServiceId($serviceId);
                            $contratTitulaire->setIdAgent(Atexo_CurrentUser::getId());
                            $contratTitulaire->setNomAgent(Atexo_CurrentUser::getLastName());
                            $contratTitulaire->setPrenomAgent(Atexo_CurrentUser::getFirstName());
                            $contratTitulaire->setIdTypeContrat($idTypeContrat);
                            $contratTitulaire->setStatutContrat(Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR'));
                            $contratTitulaire->setIdTitulaire($idEntreprise);
                            $contratTitulaire->setIdTitulaireEtab($idEtablissement);
                            $contratTitulaire->setIdOffre($idOffre);
                            $contratTitulaire->setTypeDepotReponse($typeEnv);
                            $contratTitulaire->setDateCreation(date('Y-m-d H:i:s'));
                            $contratTitulaire->setDateModification(date('Y-m-d H:i:s'));
                            $contratTitulaire->setDateAttribution($dateDecision);

                            $contratTitulaire->setSiret((new Atexo_Consultation())->getSiretByService($organisme, $serviceId));
                            $nomEntiteAcheteur = (new Atexo_Consultation())->getOrganismeDenominationByService($organisme, $serviceId);
                            $nomEntiteAcheteur = explode('(', $nomEntiteAcheteur);
                            $contratTitulaire->setNomEntiteAcheteur($nomEntiteAcheteur[0]);

                            if ($consultation->getIdContrat()) {
                                $lienACSad = (new Atexo_Consultation_Contrat())->getLienACByIdContratAndIdTitulaire($consultation->getIdContrat(), $idEntreprise);
                                if (is_null($lienACSad)) {
                                    $lienACSad = $consultation->getIdContrat();
                                }
                                $contratTitulaire->setLienAcSad($lienACSad);
                            }
                            $this->fillContratByDataComplementaire($contratTitulaire, null, $consultation);
                            $contactContrat = new CommonTContactContrat();
                            $contactContrat->setNom($item->nom->Text);
                            $contactContrat->setPrenom($item->prenom->Text);
                            $contactContrat->setEmail($item->email->Text);
                            $contactContrat->setTelephone($item->telephone->Text);
                            $contactContrat->setFax($item->fax->Text);
                            $contactContrat->setIdEtablissement($idEtablissement);
                            $contactContrat->setIdEntreprise($idEntreprise);
                            $contactContrat->setIdInscrit($idInscrit);
                            $contratTitulaire->setCommonTContactContrat($contactContrat);
                            if ($contratMultiO instanceof CommonTContratMulti) {
                                $contratTitulaire->setIdContratMulti($contratMultiO->getIdContratTitulaire());
                                if ($this->numerotationAGenerer($contratTitulaire)) {
                                    $numeroCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contratTitulaire, $connexion);
                                    $contratTitulaire->setNumeroContrat($numeroCourt);
                                    $contratTitulaire->setNumLongOeap($numeroLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                }
                            } else {
                                if ($this->numerotationAGenerer($contratTitulaire)) {
                                    $numeroContratCourt = (new Atexo_Consultation_Contrat())->genererNumerotationCourtContrat($contratTitulaire, $connexion);
                                    $numeroContratLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($contratTitulaire, $connexion);
                                    $contratTitulaire->setNumeroContrat($numeroContratCourt);
                                    $contratTitulaire->setNumLongOeap($numeroContratLong.Atexo_Util::getSuffixNumeroContrat($j++));
                                }
                            }
                            $contratTitulaire->setReferenceLibre($consultation->getReferenceUtilisateur());
                            $contratTitulaire->setReferenceConsultation($consultation->getReferenceUtilisateur());

                            if ($consultation->getAlloti()) {
                                $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);

                                $criteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
                                $criteria->add(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $consultation->getId());
                                $criteria->add(CommonTListeLotsCandidaturePeer::STATUS, Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                $criteria->addGroupByColumn(CommonTListeLotsCandidaturePeer::NUM_LOT);

                                $nbOffreDemat = CommonTListeLotsCandidaturePeer::doCount($criteria, $con);

                                $criteria2 = new Criteria(CommonOffrePapierPeer::DATABASE_NAME);
                                $criteria2->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultation->getId());
                                $criteria2->add(CommonOffrePapierPeer::STATUT_OFFRE_PAPIER, Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                $criteria2->addJoin(CommonOffrePapierPeer::ID, CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, Criteria::LEFT_JOIN);
                                $criteria->addGroupByColumn(CommonEnveloppePapierPeer::SOUS_PLI);

                                $nbOffrePapier = CommonOffrePapierPeer::doCount($criteria2, $con);
                            } else {
                                $nbOffreDemat = $consultation->getNombreOffreElectronique();
                                $nbOffrePapier = $consultation->getNombreOffrePapier();
                            }

                            $contratTitulaire->setNbTotalPropositionsLot($nbOffreDemat + $nbOffrePapier);
                            $contratTitulaire->setNbTotalPropositionsDematLot($nbOffreDemat);
                            $contratTitulaire->setUuid((new Atexo_MpeSf())->uuid());
                            $contratTitulaire->save($connexion);

                            //Reprendre les donnees achats responsables
                            if (Atexo_Module::isEnabled('ConsultationClause')) {
                                $this->reprendreClausesAchatsResponsablesDepuisConsultation($contratTitulaire, $lots);
                            }
                            $donneesConsultation = new CommonTDonneesConsultation();
                            (new Atexo_Consultation_Contrat())->fillDonneesConsultation($donneesConsultation, $consultation);
                            $donneesConsultation->setCommonTContratTitulaire($contratTitulaire);
                            $donneesConsultation->save($connexion);

                            if (is_array($lots)) {
                                foreach ($lots as $lot) {
                                    $lotContrat = new CommonTConsLotContrat();
                                    $lotContrat->setOrganisme($organisme);
                                    $lotContrat->setConsultationId($consultationId);
                                    $lotContrat->setLot($lot);
                                    $lotContrat->setIdContratTitulaire($contratTitulaire->getIdContratTitulaire());
                                    $lotContrat->setCommonTContratTitulaire($contratTitulaire);
                                    $lotContrat->save($connexion);
                                    if ($this->getTypeDecision() != Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
                                        self::saveInvitationConsultationTransverse($contratTitulaire, $consultationId, Atexo_Util::frnDate2iso($this->dateDecision->getSafeText()), $lot, $connexion);
                                    }
                                }
                            }
                        } else {
                            $logger->error("L'entreprise dont l'id => ".$idEntreprise." n'existe pas");
                        }
                    } else {
                        $logger->error('Id entreprise => '.$idEntreprise.' est invalide');
                    }
                    $logger->info("Fin de creation du contrat pour la consulation '".$consultationId." id entreprise '".$idEntreprise."' id etablissement '".$idEtablissement."' id Offre '".$idOffre."'");
                }
            }
        }
        if ($elementChecked) {
            if (is_array($lots)) {
                foreach ($lots as $lot) {
                    $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, $lot, $organisme);
                    if (!$decisionLot instanceof CommonDecisionLot) {
                        $decisionLot = new CommonDecisionLot();
                        $decisionLot->setConsultationId($consultationId);
                        $decisionLot->setLot($lot);
                        $decisionLot->setIdTypeDecision(Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR'));
                        $decisionLot->setDateDecision($dateDecision);
                        $decisionLot->setDateMaj(date('Y-m-d H:i:s'));
                        $decisionLot->setCommentaire($this->commentaire->getSafeText());
                        $decisionLot->setOrganisme($organisme);
                        $decisionLot->save($connexion);
                    }
                }
            }
        }
    }

    /**
     * Permet de verifier si la numerotation est a generer.
     *
     * @param $contratTitulaire
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function numerotationAGenerer($contratTitulaire)
    {
        return (new Atexo_Consultation_Contrat())->getCasTypesMarchesPourNumerotation($contratTitulaire);
    }

    /**
     * Permet de remplir contrat titulaire avec les données complementaire.
     *
     * @param CommonTContratTitulaire $contratTitulaire
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillContratByDataComplementaire(&$contratTitulaire, $lot, $consultation): array
    {
        $categorie = null;
        $labelCategorie = null;
        $codeCPV1 = null;
        $codeCPV2 = null;
        $ccag = null;
        $lotId = null;
        if ($consultation instanceof CommonConsultation) {
            $objet = $consultation->getObjetTraduit();
            $lieuxExecution = $consultation->getLieuExecution();
            if ($lot) {
                $categorieLot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultation->getId(), $lot, $consultation->getOrganisme());
                if ($categorieLot instanceof CommonCategorieLot) {
                    $objet .= ' - ' . $categorieLot->getDescription();
                    $categorie = $categorieLot->getCategorie();
                    $labelCategorie = $categorieLot->getLibelleCategorieLot();
                    $codeCPV1 = $categorieLot->getCodeCpv1();
                    $codeCPV2 = $categorieLot->getCodeCpv2();
                    $lotId = $categorieLot->getId();
                    if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {// Données complementaire
                        $donneeCmp = $categorieLot->getDonneComplementaire();
                        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
                            $ccag = $donneeCmp->getIdCcagReference();
                        }
                    }
                }
            } else {
                $categorie = $consultation->getCategorie();
                $labelCategorie = $consultation->getLibelleCategorieConsultation();
                $codeCPV1 = $consultation->getCodeCpv1();
                $codeCPV2 = $consultation->getCodeCpv2();
                if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {// Données complementaire
                    $donneeCmp = $consultation->getDonneComplementaire();
                    if ($donneeCmp instanceof CommonTDonneeComplementaire) {
                        $ccag = $donneeCmp->getIdCcagReference();
                    }
                }
            }
            $contratTitulaire->setObjetContrat((new Atexo_Config())->toPfEncoding($objet));
            if (!(new Atexo_Consultation_Contrat())->isTypeContratConcession($contratTitulaire->getIdTypeContrat())) {
                $procedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivotObjectByIdTypeProcedureMpe($consultation->getIdTypeProcedure());
                $contratTitulaire->setProcedurePassationPivot($procedure->getId());
            }
            $contratTitulaire->setCategorie($categorie);
            $contratTitulaire->setLieuExecution($lieuxExecution);
            $contratTitulaire->setCodeCpv1($codeCPV1);
            $contratTitulaire->setCodeCpv2($codeCPV2);
            if ($ccag) {
                $contratTitulaire->setCcagApplicable($ccag);
            }
        }

        return [$labelCategorie, $codeCPV1, $codeCPV2, $ccag, $lotId];
    }

    /**
     * Permet de valider les donnees d'attribution.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function validerDonneesAttribution($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $scriptJs = '';
        $elemntNotChechked = false;
        $moreThanOneElemntChechked = false;
        foreach ($this->repeaterAttribution->Items as $parentItem) {
            $nbrElementChecked = 0;
            foreach ($parentItem->repeaterAttributionDetail->Items as $item) {
                if ($item->attributaire->checked) {
                    ++$nbrElementChecked;
                }
            }
            if (!$nbrElementChecked) {
                $elemntNotChechked = true;
            } elseif ($this->getTypeDecision() != Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')
                && $nbrElementChecked > 1 && !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($this->idTypeContrat->getSelectedValue())) {
                $moreThanOneElemntChechked = true;
            }
        }

        if ($elemntNotChechked || $moreThanOneElemntChechked) {
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            if ($elemntNotChechked) {
                $arrayMsgErreur[] = Prado::Localize('CHOISIR_UN_ATTRIBUTAIRE');
            }

            if ($moreThanOneElemntChechked) {
                $arrayMsgErreur[] = Prado::Localize('CHOISIR_UN_SEUL_ATTRIBUTAIRE');
            }
        }

        $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs);
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param bool   $isValid
     * @param array  $arrayMsgErreur tableau des message d'erreurs
     * @param string $scriptJs       le script js
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs, $idDivSummary = 'divValidationSummary')
    {
        if ($isValid) {
            $param->IsValid = true;
            $this->labelScript->Text = "<script>document.getElementById('".$idDivSummary."').style.display='none';</script>";
        } else {
            $param->IsValid = false;
            $scriptJs .= "document.getElementById('".$idDivSummary."').style.display='';";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->$idDivSummary->addServerError($errorMsg, false);
            }
        }
        $this->labelScript->Text = "<script>$scriptJs</script>";
    }

    /**
     * Permet d'ajouter une entreprise.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function ajouterEntreprise($sender, $param)
    {
        $company = null;
        $etablissement = null;
        $connexion = null;
        $arrayParams = (array) $param->CallbackParameter;
        if (is_array($arrayParams) && !$arrayParams['idEntreprise'] && $arrayParams['typeEnveloppe'] == Atexo_Config::getParameter('DEPOT_PAPIER')) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                $idOffre = $arrayParams['idOffre'];
                $offreQuery = new CommonOffrePapierQuery();
                $offre = $offreQuery->getOffrePapierById($idOffre);
                $this->checkBoxId->value = $arrayParams['checkBoxId'];
                $this->elementIndex->value = $indexElement = $arrayParams['elementIndex'];
                if ($offre) {
                    if ($offre->getSiret()) {
                        $this->siren->Text = substr($offre->getSiret(), 0, 9);
                        $this->siret->Text = substr($offre->getSiret(), 9, 14);
                    }
                    if ($offre->isEntrepriseOffreLocale() && $offre->getSiret()) {
                        $siren = substr($offre->getSiret(), 0, 9);
                        $company = Atexo_Entreprise::retrieveCompanyBySiren($siren);
                        $etablissement = self::getEtablissementFromEntreprise($company, $connexion, false);
                    } elseif ($offre->getIdentifiantNational() && $offre->getPays()) {
                        $idNational = $offre->getIdentifiantNational();
                        $pays = $offre->getPays();
                        $company = (new Atexo_Entreprise())->retrieveCompanyByIdNational($idNational, $pays);
                        $etablissement = true;
                    }
                    if ($company && $etablissement) {
                        self::refreshOffre($indexElement, $idOffre, $company, $connexion);
                    } else {
                        $this->afficherBlocAjoutEntreprise($offre);
                        $this->labelScript->Text = "<script>openModal('modal-entreprise','popup-small2',document.getElementById('".$this->titlePopin->getClientId()."'));</script>";
                    }
                }
                $connexion->commit();
            } catch (Exception $e) {
                $connexion->rollBack();
                Prado::log("Erreur lors d'enregistremen de l'entreprise : ".$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionAttribution.php');
                $logger = Atexo_LoggerManager::getLogger('contrat');
                $logger->error("Erreur lors d'enregistremen de l'entreprise => ".$e->getMessage().' '.$e->getTraceAsString());
            }
        }
    }

    /**
     * Permet d'afficher le bloc d'ajout entreprise.
     *
     * @param CommonOffrePapier $offre
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function afficherBlocAjoutEntreprise($offre)
    {
        if ($offre instanceof CommonOffrePapier) {
            $entrepriseLocale = $offre->isEntrepriseOffreLocale();
            $this->idOffre->value = $offre->getId();
            $this->entrepriseLocale->value = $entrepriseLocale ? '1' : '0';
            if ($entrepriseLocale) {
                $this->panelSiren->setDisplay('Dynamic');
                $this->panelEtranger->setDisplay('None');
                if ($offre->getSiret()) {
                    $this->siren->Text = substr($offre->getSiret(), 0, 9);
                    $this->siret->Text = substr($offre->getSiret(), 9, 14);
                }
            } else {
                $this->panelSiren->setDisplay('None');
                $this->panelEtranger->setDisplay('Dynamic');
                $this->identifiantNational->Text = $offre->getIdentifiantNational();
                if ($offre->getPays()) {
                    $this->listPays->SelectedValue = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdCountry($offre->getPays());
                }
            }
        }
    }

    /**
     * Permet de sauvegarder une entreprise.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function saveEntreprise($sender, $param)
    {
        $newCompany = null;
        $connexion = null;
        if ($this->isValid) {
            $companyCreated = false;
            $etablissementCreated = false;
            $idOffre = $this->idOffre->value;
            $indexElement = $this->elementIndex->value;
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                if ($this->entrepriseLocale->value) {
                    $siren = $this->siren->getSafeText();
                    $siret = $this->siret->getSafeText();
                    $newCompany = Atexo_Entreprise::retrieveCompanyBySiren($siren);
                    if (!$newCompany) {
                        //SynchronisationSGMAP
                        if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                            $newCompany = Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($siren, '1', $mode, false, $connexion);
                            if ($newCompany) {
                                if ($newCompany->getNicsiege() != $siret) {
                                    $newEtablissement = Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($siren.$siret, '1', $mode, false, $connexion);
                                    if ($newEtablissement) {
                                        $newEtablissement->setSaisieManuelle('0');
                                        $newCompany->setEtablissements($newEtablissement);
                                    }
                                }
                                $newCompany->setListeEtablissements($newCompany->getEtablissements());
                                $newCompany->setSaisieManuelle('0');
                                $newCompany->setDateModification(date('Y-m-d H:i:s'));
                                $newCompany->setDateCreation(date('Y-m-d H:i:s'));
                                $companyCreated = true;
                            }
                        }
                        if (!$companyCreated) {
                            $newCompany = new Atexo_Entreprise_EntrepriseVo();
                            $newEtablissement = new Atexo_Entreprise_EtablissementVo();
                            self::fillEntrepriseByData($idOffre, $newEtablissement, $newCompany, $siren, $siret);
                            $companyCreated = true;
                        }
                    } else {
                        $newEtablissement = self::getEtablissementFromEntreprise($newCompany, $connexion, false);
                        if (!$newEtablissement) {
                            $newEtablissement = new Atexo_Entreprise_EtablissementVo();
                            self::fillEtablissementByData($idOffre, $newEtablissement, $newCompany, $siret, null, Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                            $etablissementCreated = true;
                            $newCompanyVo = new Atexo_Entreprise_EntrepriseVo();
                            $newCompanyVo = Atexo_Entreprise::fillInfosEse($newCompany, $newCompanyVo);
                            $newCompanyVo->setId($newCompany->getId());
                            $newCompany = $newCompanyVo;
                            $newCompany->setEtablissements($newEtablissement);
                            $newCompany->setListeEtablissements($newCompany->getEtablissements());
                        }
                    }
                } else {
                    $idNational = $this->identifiantNational->getSafeText();
                    if ((0 != $this->listPays->SelectedIndex) && '' != $idNational) {
                        $pays = substr($this->listPays->getSelectedItem()->getText(), 0, -6);
                        $newCompany = (new Atexo_Entreprise())->retrieveCompanyByIdNational($idNational, $pays);

                        if (!$newCompany) {
                            $newCompany = new Atexo_Entreprise_EntrepriseVo();
                            $newEtablissement = new Atexo_Entreprise_EtablissementVo();

                            self::fillEntrepriseByData($idOffre, $newEtablissement, $newCompany, null, null, $idNational, $pays, $pays, (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCodeCountryByLibelle((new Atexo_Config())->toPfEncoding($pays)));
                            $companyCreated = true;
                        }
                    }
                }
                if ($companyCreated) {
                    $newCompany->setCreatedFromDecision('1');
                }
                if ($companyCreated || $etablissementCreated) {
                    $newCompany = (new Atexo_Entreprise())->save($newCompany, $connexion);
                }
                $connexion->commit();
                self::refreshOffre($indexElement, $idOffre, $newCompany, $connexion);
                self::effacerDonneeAjoutEntreprise();
            } catch (Exception $e) {
                $connexion->rollback();
                Prado::log("Erreur lors d'enregistremen de l'entreprise : ".$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionAttribution.php');
                $logger = Atexo_LoggerManager::getLogger('contrat');
                $logger->error("Erreur lors d'enregistremen de l'entreprise => ".$e->getMessage().' '.$e->getTraceAsString());
            }

            $this->labelScript->Text = "<script>J('.modal-entreprise').dialog('destroy');</script>";
        }
    }

    /**
     * Permet de dd'annuler l'enregistrement d'une entreprise.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function annulerEnregistrement()
    {
        self::effacerDonneeAjoutEntreprise();
        $this->labelScript->Text = "<script>document.getElementById('".$this->checkBoxId->value."').checked=false;J('.modal-entreprise').dialog('destroy');</script>";
    }

    /**
     * Permet de setter les information de l'offre dans l'etablissement et l'entreprise.
     *
     * @param int             $idOffre       l'id de l'offre
     * @param EtablissementVo $etablissement l'etablissement
     * @param EntrpriseVo     $newCompany    l'entreprise
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setInfoFromOffre($idOffre, &$etablissement, &$newCompany = false)
    {
        $offreQuery = new CommonOffrePapierQuery();
        $offre = $offreQuery->getOffrePapierById($idOffre);
        if ($offre) {
            $etablissement->setAdresse($offre->getAdresse());
            $etablissement->setAdresse2($offre->getAdresse2());
            $etablissement->setCodePostal($offre->getCodePostal());
            $etablissement->setVille($offre->getVille());
            if ($newCompany) {
                $newCompany->setNom($offre->getNomEntreprise());
            }
        }
    }

    /**
     * Permet de remplir l'entreprise et l'etablissement avec les donnees.
     *
     * @param int             $idOffre           l'id offre
     * @param EtablissementVo $newEtablissement  l'etablissement
     * @param EntrepriseVo    $newCompany        l'entreprise
     * @param string          $siren             le siren
     * @param string          $siret             le siret
     * @param string          $idNational        l'id national
     * @param string          $payEnregistrement pays d'enregistrement
     * @param string          $payAdresse        pays d'adresse
     * @param string          $acronymePays      l'acronyme pays
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillEntrepriseByData($idOffre, &$newEtablissement, &$newCompany, $siren, $siret, $idNational = null, $payEnregistrement = null, $payAdresse = null, $acronymePays = null)
    {
        if ($siren) {
            $newCompany->setSiren($siren);
            $payEnregistrement = Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            $payAdresse = Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE');
            $acronymePays = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
        }
        if ($siret) {
            $newCompany->setNicsiege($siret);
        }
        if ($idNational) {
            $newCompany->setSirenetranger($idNational);
        }
        $newCompany->setPaysenregistrement($payEnregistrement);
        $newCompany->setPaysadresse($payAdresse);
        $newCompany->setAcronymePays($acronymePays);
        $newCompany->setDateModification(date('Y-m-d H:i:s'));
        $newCompany->setDateCreation(date('Y-m-d H:i:s'));
        $newCompany->setSaisieManuelle('1');
        self::fillEtablissementByData($idOffre, $newEtablissement, $newCompany, $siret, $idNational, $payAdresse);
        $newCompany->setListeEtablissements([$newEtablissement]);
    }

    /**
     * Permet de remplir l'etablissement avec les donnees.
     *
     * @param int             $idOffre           l'id offre
     * @param EtablissementVo $newEtablissement  l'etablissement
     * @param string          $siret             le siret
     * @param string          $idNational        l'id national
     * @param string          $payEnregistrement pays d'enregistrement
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillEtablissementByData($idOffre, &$newEtablissement, &$newCompany, $siret, $idNational = null, $payEnregistrement = null)
    {
        if ($siret) {
            $newEtablissement->setCodeEtablissement($siret);
        }
        if ($idNational) {
            $newEtablissement->setCodeEtablissement('1');
        }
        $newEtablissement->setPays($payEnregistrement);
        $newEtablissement->setDateCreation(date('Y-m-d H:i:s'));
        $newEtablissement->setDateModification(date('Y-m-d H:i:s'));
        $newEtablissement->setSaisieManuelle('1');
        $newEtablissement->setEstSiege('1');
        self::setInfoFromOffre($idOffre, $newEtablissement, $newCompany);
    }

    /**
     * Permet de rafraichir l'offre.
     *
     * @param int        $indexElement l'index d'élement to refresh
     * @param int        $idOffre      l'id offre
     * @param Entreprise $entreprise   l'entreprise
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function refreshOffre($indexElement, $idOffre, $entreprise, $connexion)
    {
        $etablissement = null;
        $dataSource = $this->getDataSource();
        $reponses = $dataSource[$indexElement]['reponses'];
        foreach ($reponses as $reponse) {
            if ($reponse instanceof CommonOffrePapier) {
                if ($reponse->getId() == $idOffre) {
                    $reponse->setEntrepriseId($entreprise->getId());
                    if ($entreprise->getNom()) {
                        $reponse->setNomEntreprise($entreprise->getNom());
                    }

                    $etablissement = self::getEtablissementFromEntreprise($entreprise, $connexion);
                    $codeEtablissement = $entreprise->getNicsiege();
                    if ($etablissement instanceof CommonTEtablissement) {
                        $codeEtablissement = $etablissement->getCodeEtablissement();
                        $reponse->setAdresse($etablissement->getAdresse());
                        $reponse->setAdresse2($etablissement->getAdresse2());
                        $reponse->setCodePostal($etablissement->getCodePostal());
                        $reponse->setVille($etablissement->getVille());
                        $reponse->setPays($etablissement->getPays());
                        $reponse->setIdEtablissement($etablissement->getIdEtablissement());
                    }
                    if ($entreprise->getSiren()) {
                        $reponse->setSiret($entreprise->getSiren().$codeEtablissement);
                        $reponse->setIdentifiantNational('');
                    } else {
                        $reponse->setIdentifiantNational($entreprise->getSirenetranger());
                        $reponse->setSiret('');
                    }
                }
            }
        }
        $codeEtablissement = '';
        $offreQuery = new CommonOffrePapierQuery();
        $offre = $offreQuery->getOffrePapierById($idOffre);
        $offre->setEntrepriseId($entreprise->getId());
        if ($etablissement instanceof CommonTEtablissement) {
            $offre->setIdEtablissement($etablissement->getIdEtablissement());
            if ($etablissement->getAdresse()) {
                $offre->setAdresse($etablissement->getAdresse());
            }
            if ($etablissement->getAdresse2()) {
                $offre->setAdresse2($etablissement->getAdresse2());
            }
            if ($etablissement->getCodePostal()) {
                $offre->setCodePostal($etablissement->getCodePostal());
            }
            if ($etablissement->getVille()) {
                $offre->setVille($etablissement->getVille());
            }
            if ($etablissement->getPays()) {
                $offre->setPays($etablissement->getPays());
            }
            $codeEtablissement = $etablissement->getCodeEtablissement();
        }
        $offre->setSiret($entreprise->getSiren().$codeEtablissement);
        if ($entreprise->getNom()) {
            $offre->setNomEntreprise($entreprise->getNom());
        }

        $offre->save($connexion);

        $dataSource[$indexElement]['reponses'] = $reponses;
        $this->setDataSource($dataSource);
        self::populateData();
    }

    /**
     * Permet de remplir le repeater d'attribution.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function populateData()
    {
        $dataSource = $this->getDataSource();
        $this->repeaterAttribution->DataSource = $dataSource;
        $this->repeaterAttribution->DataBind();
    }

    /**
     * Permet de recuperer l'etablissement de l'entreprise.
     *
     * @param Entreprise $entreprise
     *
     * @return CommonTEtablissement
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementFromEntreprise($entreprise, $connexion, $etabOrSiege = true)
    {
        if ($entreprise instanceof Entreprise) {
            $etablissement = null;
            $siret = $this->siret->Text;
            $siren = $this->siren->Text;
            if ($siret && $siren) {
                $etablissementQuery = new CommonTEtablissementQuery();
                $etablissement = $etablissementQuery->getEtabissementbyIdEseAndCodeEtab($siret, $entreprise->getId(), $connexion);
                if (!$etablissement) {
                    $etablissement = (new Atexo_Entreprise_Etablissement())->synchroEtablissementAvecApiGouvEntreprise($siren.$siret, null, $connexion);
                    if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
                        $etablissement = $etablissement->getCommonTEtablissement();
                    }
                }
            }
            if ($etabOrSiege && !$etablissement) {
                $etablissement = $entreprise->getMonEtablissementSiege();
            }
            if ($etablissement instanceof CommonTEtablissement) {
                return $etablissement;
            }
        }

        return false;
    }

    /**
     * Permet d'effacer les donnees d'ajout d'entreprise.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function effacerDonneeAjoutEntreprise()
    {
        $this->identifiantNational->Text = '';
        $this->listPays->SelectedValue = '0';
        $this->siren->Text = '';
        $this->siret->Text = '';
    }

    /**
     * Permet d'avoir l'identifiant de l'entreprise qui y a deposer l'offre.
     *
     * @param CommonOffres ou CommonOffrePapier $offre l'offre
     *
     * @return string l'identifiant de l'entreprise
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getIdentifiantEntreprise($offre)
    {
        $identifiant = '';
        $idEntreprise = $offre->getEntrepriseId();
        if ($idEntreprise) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, null, false);
            if ($entreprise instanceof Entreprise) {
                $identifiant = (new Atexo_Entreprise())->getIndentifiantLocalOuEtranger($entreprise);
            }
        } else {
            $identifiant = $offre->getSiret() ?: ($offre->getIdentifiantNational() ?: '');
        }

        return $identifiant;
    }

    /**
     * Permet d'avoir l'identifiant de l'etablissement qui y a deposer l'offre.
     *
     * @param CommonOffres ou CommonOffrePapier $offre l'offre
     *
     * @return string l'identifiant de l'etaplissement
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getIdentifiantEtablissement($offre)
    {
        $identifiant = '';
        $idEtablissement = $offre->getIdEtablissement();
        if ($idEtablissement) {
            $etablissementQuery = new CommonTEtablissementQuery();
            $etablissement = $etablissementQuery->getEtabissementbyId($idEtablissement);
            if ($etablissement instanceof CommonTEtablissement && '1' != $etablissement->getCodeEtablissement()) {
                $identifiant = $etablissement->getCodeEtablissement();
            }
        }

        return $identifiant;
    }

    /*
     * Permet de valider les donnees d'ajout d'entreprise
     *
     * @param $sender
     * @param $param
     * @return void
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function validerDonneeAjoutEntreprise($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $scriptJs = '';
        if ($this->entrepriseLocale->value) {
            if (!Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorListePays->getClientId()."').style.display = '';";
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantNational->getClientId()."').style.display = 'none';";
                $siren = $this->siren->Text;
                $siret = $this->siret->Text;
                if (!$siren || !$siret) {
                    $scriptJs .= "document.getElementById('".$this->ImgErrorSiret->getClientId()."').style.display = '';";
                    $arrayMsgErreur[] = Prado::localize('SIREN_SIRET');
                    $isValid = false;
                } elseif (!Atexo_Util::isSiretValide($siren.$siret)) {
                    $scriptJs .= "document.getElementById('".$this->ImgErrorSiret->getClientId()."').style.display = '';";
                    $arrayMsgErreur[] = Prado::localize('MSG_ERROR_SIRET_INVALIDE');
                    $isValid = false;
                } else {
                    $scriptJs .= "document.getElementById('".$this->ImgErrorSiret->getClientId()."').style.display = 'none';";
                }
            }
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorSiret->getClientId()."').style.display = 'none';";
            if (!$this->listPays->getSelectedValue()) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorListePays->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('TEXT_PAYS');
                $isValid = false;
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgErrorListePays->getClientId()."').style.display = 'none';";
            }
            if (!$this->identifiantNational->Text) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantNational->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('TEXT_IDENTIFIANT_NATIONNAL');
                $isValid = false;
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantNational->getClientId()."').style.display = 'none';";
            }
        }
        $this->afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur, $scriptJs, 'divValidationSummaryEntreprise');
    }

    /**
     * Permet d'initialiser un contrat multi attributaire.
     *
     * @param string $idTypeContrat
     *
     * @throws Exception
     * @throws PropelException
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initialiserObjetContratMulti($idTypeContrat, $connexion, $idLot = null)
    {
        $listeIdsLot = null;
        $categorie = null;
        $objet = null;
        $codeCPV1 = null;
        $codeCPV2 = null;
        $ccag = null;
        $numeroContratMulti = null;
        $consultation = $this->getViewState('consultation');
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();

            $lieuExecution = $consultation->getLieuExecution();

            if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {// Données complementaire
                $donneeCmp = $consultation->getDonneComplementaire();
                if ($donneeCmp instanceof CommonTDonneeComplementaire) {
                    $ccag = $donneeCmp->getIdCcagReference();
                }
            }
            if (null !== $idLot) {
                $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $idLot, $organisme);
                $objet = $consultation->getObjetTraduit();

                if ($lot instanceof CommonCategorieLot) {
                    $categorie = $lot->getCategorie();
                    $objet .= ' - '.$lot->getDescription();
                    $codeCPV1 = $lot->getCodeCpv1();
                    $codeCPV2 = $lot->getCodeCpv2();
                    if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {// Données complementaire
                        $donneeCmp = $lot->getDonneComplementaire();
                        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
                            $ccag = $donneeCmp->getIdCcagReference();
                        }
                    }
                } else {
                    $categorie = $consultation->getCategorie();
                    $codeCPV1 = $consultation->getCodeCpv1();
                    $codeCPV2 = $consultation->getCodeCpv2();
                }
                $listeIdsLot = [$idLot];
            } elseif (isset($_GET['lots']) && $_GET['lots']) {
                $categorie = $consultation->getCategorie();
                $codeCPV1 = $consultation->getCodeCpv1();
                $codeCPV2 = $consultation->getCodeCpv2();
                $objet = '';
                $lots = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['lots']));
                $listeIdsLot = explode('#', $lots);
                if (is_array($listeIdsLot)) {
                    foreach ($listeIdsLot as $oneIdLot) {
                        $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $oneIdLot, $organisme);
                        if ($lot instanceof CommonCategorieLot) {
                            $objet .= $lot->getLot().' - '.$lot->getDescription()."\n";
                        }
                    }
                }
            }

            $contratMulti = (new Atexo_Consultation_Contrat())->retrieveOneContratMultiByLot($consultationId, $organisme, $listeIdsLot, $connexion);
            if (!$contratMulti instanceof CommonTContratMulti) {
                $contratMulti = new CommonTContratMulti();
                $contratMulti->setOrganisme($consultation->getOrganisme());
                $contratMulti->setServiceId($consultation->getServiceId());
                $contratMulti->setIdAgent(Atexo_CurrentUser::getId());
                $contratMulti->setNomAgent(Atexo_CurrentUser::getLastName());
                $contratMulti->setPrenomAgent(Atexo_CurrentUser::getFirstName());
                $contratMulti->setDateCreation(date('Y-m-d H:i:s'));
                $contratMulti->setDateModification(date('Y-m-d H:i:s'));
                $contratMulti->setIdTypeContrat($idTypeContrat);
                $contratMulti->setReferenceLibre($consultation->getReferenceUtilisateur());
                $contratMulti->setReferenceConsultation($consultation->getReferenceUtilisateur());
                $contratMulti->setDateAttribution(Atexo_Util::frnDate2iso($this->dateDecision->getSafeText()));
                $contratMulti->setCategorie($categorie);
                $contratMulti->setObjetContrat((new Atexo_Config())->toPfEncoding($objet));
                $contratMulti->setLieuExecution($lieuExecution);
                $contratMulti->setCodeCpv1($codeCPV1);
                $contratMulti->setCodeCpv2($codeCPV2);
                if ($ccag) {
                    $contratMulti->setCcagApplicable($ccag);
                }
                $numeroContratLong = (new Atexo_Consultation_Contrat())->genererNumerotationLongContrat($contratMulti, $connexion);
                if ($numeroContratLong) {
                    $numeroContratMulti = $numeroContratLong.Atexo_Config::getParameter('SEPARATEUR_NUMEROTATION_CONTRAT').'00';
                }

                $contratMulti->setNumeroContrat($numeroContratMulti);
                $contratMulti->setUuid((new Atexo_MpeSf())->uuid());
                $contratMulti->save($connexion);
                //Reprendre les donnees achats responsables
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $this->reprendreClausesAchatsResponsablesDepuisConsultation($contratMulti, $listeIdsLot);
                }
            } else {
                $numeroContratMulti = $contratMulti->getNumeroContrat();
                $numeroContratLong = $numeroContratMulti ? substr($numeroContratMulti, 0, -3) : '';
            }

            if (is_array($listeIdsLot)) {
                foreach ($listeIdsLot as $oneIdLot) {
                    if ($this->getTypeDecision() != Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
                        self::saveInvitationConsultationTransverse(
                            $contratMulti,
                            $consultationId,
                            Atexo_Util::frnDate2iso($this->dateDecision->getSafeText()),
                            $oneIdLot,
                            $connexion
                        );
                    }
                }
            }

            $this->setViewState('numeroLongContratMulti', $numeroContratLong);
            $this->setViewState('numeroContratMulti', $numeroContratMulti);
            $this->setViewState('ContratMulti', $contratMulti);
        }
    }

    /**
     * Permet d'enregistrer les entités éligible.
     *
     * @param CommonTContratTitulaire $contrat        le contrat
     * @param string                  $consultationId refernce consultation
     * @param Date                    $dateDecision   la date de decision
     * @param string                  $lot            numero du lot
     * @param $connexion
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function saveInvitationConsultationTransverse($contrat, $consultationId, $dateDecision, $lot, $connexion)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            if (Atexo_Module::isEnabled('AcSadTransversaux')
                && (
                    (new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($contrat->getIdTypeContrat())
                    || (new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat())
                )
            ) {
                (new Atexo_Consultation_InvitationConsTransverse())->deleteInvitationConsultationTransverse($consultationId, $lot, $contrat->getOrganisme(), $contrat->getIdContratTitulaire());
                if ('1' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                    $this->decisionEntitesEligibles->addInvitationConsultationTransverse(
                        $consultationId,
                        $dateDecision,
                        $lot,
                        $contrat->getIdContratTitulaire(),
                        $connexion
                    );
                }

                /**
                 * Suppresion des données invité permanent sur le contrat !
                 */
                $criteres = [
                    'idConsultation' => $consultationId,
                    'organisme' => $contrat->getOrganisme(),
                    'lot' => $lot,
                    'idContratTitulaire' => $contrat->getIdContratTitulaire(),
                ];
                (new InvitePermanentContrat())->deleteForCriteres($criteres);
                if ('2' == $this->decisionEntitesEligibles->isMyEntity->Value) {
                    $this->decisionEntitesEligibles->addInvitePermanentContrat(
                        $consultationId,
                        $dateDecision,
                        $lot,
                        $contrat->getIdContratTitulaire()
                    );
                }
            }
        }
    }

    /**
     * Permet de reprendre les donnees des clauses sociales et environnementales de la consultation ou des lots.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function reprendreClausesAchatsResponsablesDepuisConsultation(&$contrat, $lotsContrats = [])
    {
        $consultation = $this->getViewState('consultation');
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->isAllotie()) {
                if (is_array($lotsContrats) && count($lotsContrats)) {
                    foreach ($lotsContrats as $numLot) {
                        if ($numLot) {
                            $collectionslot = CommonCategorieLotQuery::create()->filterByLot($numLot)
                                ->filterByOrganisme($consultation->getOrganisme())
                                ->filterByConsultationId($consultation->getId())
                                ->find();
                            if ($collectionslot instanceof PropelObjectCollection) {
                                $dataLots = $collectionslot->getData();

                                $this->objectsForDuplicateClauses[] = [$dataLots[0], $contrat->getIdContratTitulaire()];
                            }
                        }
                    }
                }
            } else {
                $this->objectsForDuplicateClauses[] = [$consultation, $contrat->getIdContratTitulaire()];
            }
        }
    }

    /**
     * Permet de reprendre les donnees achats responsables de la consultation ou des lots
     * Pour les consultations alloties, ce cas concerne: 1 contrat par lot lors de l'attribution.
     *
     * @param CommonConsultation|CommonCategorieLot $objet : objet consultation ou lot
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function recupererClausesAchatsResponsablesFromObjet(\Application\Propel\Mpe\CommonConsultation|\Application\Propel\Mpe\CommonCategorieLot $objet, &$contrat)
    {
        if ($this->isClauseNull($contrat->getClauseSociale()) && $this->isClauseNull($contrat->getClauseEnvironnementale())) {
            $contrat->setClauseSociale($objet->getClauseSociale());
            $contrat->setClauseSocialeConditionExecution($objet->getClauseSocialeConditionExecution());
            $contrat->setClauseSocialeInsertion($objet->getClauseSocialeInsertion());
            $contrat->setClauseSocialeAteliersProteges($objet->getClauseSocialeAteliersProteges());
            $contrat->setClauseSocialeSiae($objet->getClauseSocialeSiae());
            $contrat->setClauseSocialeEss($objet->getClauseSocialeEss());
            $contrat->setClauseEnvironnementale($objet->getClauseEnvironnementale());
            $contrat->setClauseEnvSpecsTechniques($objet->getClauseEnvSpecsTechniques());
            $contrat->setClauseEnvCondExecution($objet->getClauseEnvCondExecution());
            $contrat->setClauseEnvCriteresSelect($objet->getClauseEnvCriteresSelect());
            $contrat->setMarcheInsertion($objet->getMarcheInsertion());
        }
    }

    /**
     * Permet de reprendre les donnees achats responsables des lots
     * Ce cas concerne: 1 contrat par plusieurs lots lors de l'attribution.
     *
     * @param CommonCategorieLot $objet : objet lot
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function recupererClausesAchatsResponsablesFromLotsMultiples($lot, &$contrat)
    {
        if ($this->isClauseNull($contrat->getClauseSociale()) && $this->isClauseNull($contrat->getClauseEnvironnementale())) {
            if ('1' == $lot->getClauseSocialeConditionExecution()) {
                $contrat->setClauseSocialeConditionExecution($lot->getClauseSocialeConditionExecution());
            }
            if ('1' == $lot->getClauseSocialeInsertion()) {
                $contrat->setClauseSocialeInsertion($lot->getClauseSocialeInsertion());
            }
            if ('1' == $lot->getClauseSocialeAteliersProteges()) {
                $contrat->setClauseSocialeAteliersProteges($lot->getClauseSocialeAteliersProteges());
            }
            if ('1' == $lot->getClauseSocialeSiae()) {
                $contrat->setClauseSocialeSiae($lot->getClauseSocialeSiae());
            }
            if ('1' == $lot->getClauseSocialeEss()) {
                $contrat->setClauseSocialeEss($lot->getClauseSocialeEss());
            }
            if ('1' == $lot->getClauseEnvSpecsTechniques()) {
                $contrat->setClauseEnvSpecsTechniques($lot->getClauseEnvSpecsTechniques());
            }
            if ('1' == $lot->getClauseEnvCondExecution()) {
                $contrat->setClauseEnvCondExecution($lot->getClauseEnvCondExecution());
            }
            if ('1' == $lot->getClauseEnvCriteresSelect()) {
                $contrat->setClauseEnvCriteresSelect($lot->getClauseEnvCriteresSelect());
            }
        }
    }

    /**
     * Permet de deduire les valeurs des champs "clause_sociale" et "clause_environnementale" de l'objet contrat.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function affecterValeursClausesGlobales(&$contrat)
    {
        $clausesSociales = ('1' == $contrat->getClauseSocialeConditionExecution() || '1' == $contrat->getClauseSocialeInsertion() || '1' == $contrat->getClauseSocialeAteliersProteges() || '1' == $contrat->getClauseSocialeSiae() || '1' == $contrat->getClauseSocialeEss()) ? '1' : '2';
        $clausesEnv = ('1' == $contrat->getClauseEnvSpecsTechniques() || '1' == $contrat->getClauseEnvCondExecution() || '1' == $contrat->getClauseEnvCriteresSelect()) ? '1' : '2';
        $contrat->setClauseSociale($clausesSociales);
        $contrat->setClauseEnvironnementale($clausesEnv);
    }

    /**
     * Permet d'initialiser l'objet contrat avec les donnees non renseignees
     * Cette fonction permet de gerer le decalage entre:
     *     - la valeur par defaut de la base de donnees (NULL)
     *     - la valeur par defaut des champs des clauses de l'objet contrat titulaire (0).
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initialiserContratsClausesNonRenseignes(&$contrat)
    {
        if ($this->isClauseNull($contrat->getClauseSociale())) {
            $contrat->setClauseSociale('0');
        }
        if ($this->isClauseNull($contrat->getClauseSocialeConditionExecution())) {
            $contrat->setClauseSocialeConditionExecution('0');
        }
        if ($this->isClauseNull($contrat->getClauseSocialeInsertion())) {
            $contrat->setClauseSocialeInsertion('0');
        }
        if ($this->isClauseNull($contrat->getClauseSocialeAteliersProteges())) {
            $contrat->setClauseSocialeAteliersProteges('0');
        }
        if ($this->isClauseNull($contrat->getClauseSocialeSiae())) {
            $contrat->setClauseSocialeSiae('0');
        }
        if ($this->isClauseNull($contrat->getClauseSocialeEss())) {
            $contrat->setClauseSocialeEss('0');
        }
        if ($this->isClauseNull($contrat->getClauseEnvironnementale())) {
            $contrat->setClauseEnvironnementale('0');
        }
        if ($this->isClauseNull($contrat->getClauseEnvSpecsTechniques())) {
            $contrat->setClauseEnvSpecsTechniques('0');
        }
        if ($this->isClauseNull($contrat->getClauseEnvCondExecution())) {
            $contrat->setClauseEnvCondExecution('0');
        }
        if ($this->isClauseNull($contrat->getClauseEnvCriteresSelect())) {
            $contrat->setClauseEnvCriteresSelect('0');
        }
    }

    /**
     * @return bool
     */
    protected function getContratMulti($contrat)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratMultiById($contrat->getIdTypeContrat())) {
            return true;
        }

        return false;
    }

    /**
     * @param $methode
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isClauseNull($methode)
    {
        return 'NULL' == $methode || null == $methode;
    }

    /*
     * Permet d'afficher le message d'erreur et cacher les autres composants
     *
     * @param string $messageErreur le message d'erreur
     * @return void
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function afficherMessageErreur($messageErreur)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($messageErreur);
        $this->panelInfo->setVisible(false);
        $this->panelPopupAttribution->setVisible(false);
    }

    /**
     * Permet de charger les comoposant selon le type de contrat.
     *
     * @return void
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function typeContratChanged($sender = null, $param = null)
    {
        $idTypeContrat = $this->idTypeContrat->getSelectedValue();
        if (
            Atexo_Module::isEnabled('AcSadTransversaux')
            && !Atexo_Config::getParameter('ACTIVE_EXEC_V2')
            && ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($idTypeContrat) || (new Atexo_Consultation_Contrat())->isTypeContratSad($idTypeContrat))
        ) {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:block;');
        } else {
            $this->decisionEntitesEligibles->panelEntitesEligibles->setStyle('display:none;');
        }

        if ($param) {
            $this->decisionEntitesEligibles->panelEntitesEligibles->render($param->getNewWriter());
        }
    }

    /**
     * Permet de recuperer l'url d'accés au détail entreprise.
     *
     * @param int $idEntreprise id entreprise
     *
     * @return string l'url détail entreprise
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlDetailEntreprise($idEntreprise)
    {
        if ($idEntreprise) {
            $ats = sha1($idEntreprise.'1');

            return "javascript:popUpSetSize('index.php?page=Agent.DetailEntreprise&callFrom=agent&idEntreprise=".$idEntreprise.'&ats='.$ats."','1060px','800px','yes');";
        }

        return '';
    }

    /**
     * Permet d'initaliser les champs d'info décision.
     *
     * @param null $date
     * @param null $commentaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function fillInfoDecision($date = null, $commentaire = null)
    {
        if ($date) {
            $this->dateDecision->setText(Atexo_Util::iso2frnDate($date));
        } else {
            $this->dateDecision->setText(date('d/m/Y'));
        }
        if ($commentaire) {
            $this->commentaire->setText($commentaire);
        }
    }

    /**
     * Retourne des information sur l'attribution.
     *
     * @param array $result
     * @param $lot
     *
     * @return array
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getInfoAttributaires($result, $lot)
    {
        $infoAttributaires = [];
        if (is_array($result) && count($result)) {
            foreach ($result as $onElement) {
                $lot = $onElement['lot'];
                $offres = $onElement['reponses'];
                $i = 0;
                $infoOffre = [];
                foreach ($offres as $offre) {
                    if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
                        $contrat = (new Atexo_Consultation_Contrat())->getContratByIdOffre($offre->getId(), $offre->getOrganisme(), $offre->getTypeEnveloppe(), $lot);
                        $infoOffre[$i]['contrat'] = $contrat;
                        $infoOffre[$i++]['offre'] = $offre;
                    }
                }
                $onElement['reponses'] = $infoOffre;
                $infoAttributaires[] = $onElement;
            }
        }

        return $infoAttributaires;
    }

    /**
     * Permet de retourner le contrat selon l'offre et le lot.
     *
     * @param mixed objet $offre
     * @param $lot
     *
     * @return bool|Liste
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getContrat($offre, $lot)
    {
        if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
            if (is_array($lot) && count($lot)) {
                //dans le cas d'attribution au meme fournisseur il suffit d'utiliser juste le premier lot
                //(pour les autre lots ils ont le meme contrat)
                $lot = $lot[0];
            }
            $contrat = (new Atexo_Consultation_Contrat())->getContratByIdOffre($offre->getId(), $offre->getOrganisme(), $offre->getTypeEnveloppe(), $lot);
            if ($contrat instanceof CommonTContratTitulaire) {
                return $contrat;
            }
        }

        return false;
    }

    public function getOffresWithContrat($lot): array
    {
        $idsOffres = [];
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            $consultationId = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
            /** @var $execWS WebServicesExec */
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $query = [
                "page"                      => 0,
                "size"                      => 5000,
                "idExternesConsultation"    => $consultationId,
                "numeroLot"           => $lot
            ];

            $response = $execWS->getContent('getContrats', $query);
            $contratsFromExec = $response['content'] ?? [];
            foreach ($contratsFromExec as $value) {
                if (!empty($value['idOffre']) || '0' != $value['idOffre']) {
                    $idsOffres [] = $value['idOffre'];
                }
            }
        }
        return $idsOffres;
    }
    public function isOffreTaken($offre, $lot, $idsOffreWithContrat): bool
    {
        $res = false;
        if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                if(!empty($idsOffreWithContrat) && in_array($offre->getId(), $idsOffreWithContrat)) {
                    $res = true;
                }
            } else {
                if (is_array($lot) && count($lot)) {
                    //dans le cas d'attribution au meme fournisseur il suffit d'utiliser juste le premier lot
                    //(pour les autre lots ils ont le meme contrat)
                    $lot = $lot[0];
                }
                $contrat = (new Atexo_Consultation_Contrat())->getContratByIdOffre($offre->getId(), $offre->getOrganisme(), $offre->getTypeEnveloppe(), $lot);
                if ($contrat instanceof CommonTContratTitulaire) {
                    $res =  true;
                }
            }
        }
        return $res;
    }

    /**
     * Permet de retourner la reference libre du contrat selon l'offre et le lot.
     *
     * @param $offre
     * @param $lot
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getContratReferenceLibre($offre, $lot)
    {
        $contrat = self::getContrat($offre, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat->getReferenceLibre();
        }

        return '-';
    }

    /**
     * Permet de retourner le numero  du contrat selon l'offre et le lot.
     *
     * @param $offre
     * @param $lot
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016     *
     */
    public function getNumeroContrat($offre, $lot)
    {
        $contrat = self::getContrat($offre, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat->getNumeroContrat();
        }

        return '-';
    }

    /**
     * Permet de retourner le numero long du contrat selon l'offre et le lot.
     *
     * @param $offre
     * @param $lot
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016     *
     */
    public function getNumLongOeap($offre, $lot)
    {
        $contrat = self::getContrat($offre, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat->getNumLongOeap();
        }

        return '-';
    }

    /**
     * Permet de retourner le titre de l'etape.
     *
     * @param string $statut le statut du contrat
     *
     * @return string le titre de l'etape
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getTitreEtape($offre, $lot)
    {
        $contrat = self::getContrat($offre, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            $statut = $contrat->getStatutContrat();
            return match ($statut) {
                Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => Prado::Localize('DONNEES_DU_CONTRAT_A_SAISIR'),
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') => Prado::Localize('ETAPE_NOTIFICATION'),
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => Prado::Localize('ETAPE_NOTIFICATION_EFFECTUEE'),
                default => '',
            };
        }

        return '';
    }

    /**
     * Permet de retourner l'etape de decision.
     *
     * @param string $statut le statut du contrat
     *
     * @return string l'etape
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getEtapeDecision($offre, $lot)
    {
        $contrat = self::getContrat($offre, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            $statut = $contrat->getStatutContrat();
            return match ($statut) {
                Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => 'contrat-etape  contrat-etape-bis contrat-etape2sur3',
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') => 'contrat-etape  contrat-etape-bis contrat-etape3asur3',
                Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => 'contrat-etape  contrat-etape-bis contrat-etape3bsur3',
                default => '',
            };
        }

        return '';
    }
}
