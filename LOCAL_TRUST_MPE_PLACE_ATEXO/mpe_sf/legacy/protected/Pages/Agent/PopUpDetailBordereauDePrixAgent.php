<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/*
 * Created on 20 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */
class PopUpDetailBordereauDePrixAgent extends MpeTPage
{
    private $_formulaireConsultation;
    public ?int $_idEnv = null;

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $formulaireConsultation = (new Atexo_FormConsultation())->retrieveFormulaireConsultationById(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->_formulaireConsultation = $formulaireConsultation;
        if ($formulaireConsultation) {
            $consultation = $this->retrieveConsultation($formulaireConsultation->getConsultationId());
            if ($consultation instanceof CommonConsultation) {
                $this->IdConsultationSummary->setConsultation($consultation);
                if (!$this->isPostBack) {
                    $this->refFormulaire->Text = $formulaireConsultation->getConsultationId();
                    $this->enveloppeFormulaire->Text = $formulaireConsultation->getLibelleTypeEnveloppe();
                    if ($formulaireConsultation->getLot()) {
                        $this->numLot->Text = Prado::localize('TEXT_LOT').' '.$formulaireConsultation->getLot().' - ';
                    }
                    if (isset($_GET['evaluation'])) {
                        $envFormulaire = (new Atexo_FormConsultation())->retreiveEnveloppeformulaireCons(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['idPli']));
                        if ($envFormulaire && ($envFormulaire instanceof CommonEnveloppeFormulaireConsultation)) {
                            $this->_idEnv = $envFormulaire->getId();
                        }
                    }
                }
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }
}
