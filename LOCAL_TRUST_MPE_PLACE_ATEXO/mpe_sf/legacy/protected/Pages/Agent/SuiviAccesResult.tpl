		<div class="link-line">
			<com:TLinkButton Id="idModifierSup" 
			OnCommand="Page.modifySearchCriteria"
		    CssClass="bouton-retour" 
		    Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>">
		    <com:TTranslate>TEXT_MODIFIER_MA_RECHERCHE</com:TTranslate>
		    </com:TLinkButton>
			<a href="index.php?page=Agent.SuiviAcces" class="bouton-suivant" title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"><com:TTranslate>TEXT_NOUVELLE_RECHERCHE</com:TTranslate></a>
		</div>
		
		<!--Debut Bloc Liste des acces-->
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h3 class="no-indent"><com:TPanel id="libelleNouvelleRecherche"><com:TTranslate>DEFINE_LISTE_ACCES</com:TTranslate></com:TPanel></h3>
				<h2><com:TLabel id="IdMessageErreur" Text="<%=Prado::localize('DEFINE_AUCUN_RESULTAT')%>" visible="false"></com:TLabel></h2>
						<h2><com:TLabel id="libelleNbrResultats" ><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate></com:TLabel>&nbsp;<com:TLabel ID="nombreElement1"/></h2>
						<!--Debut partitionneur-->
						<div class="line-partitioner">
						<com:TPanel id="PartionneurTop" CssClass="partitioner">
							<div class="intitule"><strong><label for="cons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
							
							<com:TDropDownList ID="PageSizeTop" AutoPostBack="true" onSelectedIndexChanged="Page.changePagerLenght" Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>" >
							<com:TListItem Text="10" Selected="10"/>
                  			<com:TListItem Text="20" Value="20"/>
                  			<com:TListItem Text="50" Value="50" />
                  			<com:TListItem Text="100" Value="100" />
                  			<com:TListItem Text="500" Value="500" />
                  			</com:TDropDownList>
                  			
							<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div> 
							
							<label style="display:none;" for="cons_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
							
							<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
								<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
								<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
								<com:TButton ID="DefaultButtonTop"  OnClick="Page.goToPage" Attributes.style="display:none"/>
								</com:TPanel>
							  
							<div class="liens">
								<com:TPager ID="PagerTop"
								ControlToPaginate="idRepeaterResultatsAcces"
            	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
            		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
            	     	        Mode="NextPrev"
            	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
            		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
               	                OnPageIndexChanged="Page.changePageResults"/>
							</div>
					</com:TPanel>
					</div>
					<com:TActivePanel id="panelRefresh">
                    <!--Fin partitionneur-->
				<com:TRepeater id="idRepeaterResultatsAcces" 
				       AllowPaging="true"
        			   PageSize="10"
          			   EnableViewState="true"
          			   DataKeyField="IdAgent" 
          			   >
				<prop:HeaderTemplate>
				<table class="table-results" summary="<%=Prado::localize('DEFINE_LISTE_ACCES')%>">
					<caption><com:TTranslate>DEFINE_LISTE_ACCES</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<!--th class="col-200" id="agentName"><a href="#" class="on"><com:TTranslate>DEFINE_NOM_MAJ</com:TTranslate> <com:TTranslate>DEFINE_PRENOM</com:TTranslate><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('TEXT_TRIER')%>" title="<%=Prado::localize('TEXT_TRIER')%>" /></a> /<br /><com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate></th-->
							 <th class="col-200" id="agentName">
								<com:TTranslate>DEFINE_NOM_MAJ</com:TTranslate>
								<com:TTranslate>DEFINE_PRENOM</com:TTranslate>
								<com:TActiveImageButton 
									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
									OnCallback="Page.sortAgents" 
									ActiveControl.CallbackParameter="Nom" 
									AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
									Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" /> /<br> <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
							 </th>
							<th class="col-450" id="entiteAchat">
							<com:TTranslate>ENTITE_PUBLIQUE</com:TTranslate><br /><com:TTranslate>TEXT_LIBELLE_ENTITE_ACHAT</com:TTranslate>
							</th>
							<th class="col-100" id="dateAcces">
							    <com:TActiveLinkButton 
									OnCallback="Page.sortAgents" 
									Text="<%=Prado::localize('DEFINE_DATE_ACCES')%>"  
									ActiveControl.CallbackParameter="DateAcces"/>
								<com:TActiveImageButton 
									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
									AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
									OnCallback="Page.sortAgents" 
									ActiveControl.CallbackParameter="DateAcces" 
									Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
							</th>
						</tr>
					</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
					<tr>
						<td class="col-200" headers="agentName"><%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom())%>&nbsp;<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom())%><br /><%#$this->Data->getEmail()%></td>
						<td class="col-450" headers="entiteAchat"><%#Application\Service\Atexo\Atexo_EntityPurchase::getEntitePublicEntityAchatById($this->Data->getServiceId(), $this->Page->getOrganismeAgent($this->Data->getIdAgent()))%></td>
						<td class="col-100" headers="dateAcces"><%#Application\Service\Atexo\Atexo_Util::insertBrWhenTextTooLong(Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateAcces()),10)%></td>
					</tr>
					</prop:ItemTemplate>
					<prop:AlternatingItemTemplate>
					<tr class="on">
						<td class="col-200" headers="agentName"><%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom())%>&nbsp;<%#Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom())%><br /><%#$this->Data->getEmail()%></td>
						<td class="col-450" headers="entiteAchat"><%#Application\Service\Atexo\Atexo_EntityPurchase::getEntitePublicEntityAchatById($this->Data->getServiceId(), $this->Page->getOrganismeAgent($this->Data->getIdAgent()))%></td>
						<td class="col-100" headers="dateAcces"><%#Application\Service\Atexo\Atexo_Util::insertBrWhenTextTooLong(Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateAcces()),10)%></td>
					</tr>
					</prop:AlternatingItemTemplate>
					<prop:FooterTemplate>
						</table>					
					</prop:FooterTemplate>								 
				   </com:TRepeater>
				   </com:TActivePanel>
				<!--Debut partitionneur-->
				
					<com:TPanel id="PartionneurButtom" CssClass="line-partitioner">
							<div class="partitioner">
							<div class="intitule"><strong><label for="cons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
							
							<com:TDropDownList ID="PageSizeBottom"  AutoPostBack="true" onSelectedIndexChanged="Page.changePagerLenght" Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>" >
							<com:TListItem Text="10" Selected="10"/>
                  			<com:TListItem Text="20" Value="20"/>
                  			<com:TListItem Text="50" Value="50" />
                  			<com:TListItem Text="100" Value="100" />
                  			<com:TListItem Text="500" Value="500" />
                  			</com:TDropDownList>
                  			
							<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement2" visible="false"/></div> 
							
							<label style="display:none;" for="cons_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
							<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
								<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
								<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
								<com:TButton ID="DefaultButtonBottom"  OnClick="Page.goToPage" Attributes.style="display:none"/>
							</com:TPanel>
							<div class="liens">
								<com:TPager ID="PagerBottom"
								ControlToPaginate="idRepeaterResultatsAcces"
            	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
            		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
            	     	        Mode="NextPrev"
            	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
            		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
               	                OnPageIndexChanged="Page.changePageResults"/>
							</div>
						
						</div>
					</com:TPanel>
					<!--Fin partitionneur-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Liste des acces-->
		<div class="link-line">
			<com:TLinkButton Id="idModifierInf" 
			OnCommand="Page.modifySearchCriteria"
		    CssClass="bouton-retour" 
		    Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>">
		    <com:TTranslate>TEXT_MODIFIER_MA_RECHERCHE</com:TTranslate>
		    </com:TLinkButton>
			<a href="index.php?page=Agent.SuiviAcces" class="bouton-suivant" title="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"><com:TTranslate>TEXT_NOUVELLE_RECHERCHE</com:TTranslate></a>
		</div>
		<div class="breaker"></div>
