<?php

namespace Application\Pages\Agent;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_ReferentielFormXml;

/**
 * Popup de chois des destinatires.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PopupDestinataire extends MpeTPage
{
    private string $_idFormXml = '';

    public function onInit($param)
    {

        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->_idFormXml = Atexo_Util::atexoHtmlEntities($_GET['idFormXml']);
        if (!$this->IsPostBack) {
            $this->displayListDestinataire();
        }
    }

    /**
     * afficher la liste des déstinataire existrant dans la base.
     */
    public function displayListDestinataire()
    {
        $listDestinataire = (new Atexo_Publicite_Destinataire())->retreiveDestinataireType();
        $this->listeDestinataires->DataSource = $listDestinataire;
        $this->listeDestinataires->DataBind();
    }

    /**
     * Ajouter destinataire dans la base.
     */
    public function addDestinataire($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $arrayDest = $this->checkedDestinataire();
        foreach ($arrayDest as $oneDest) {
            $newAnnonce = new CommonAnnonceBoamp();
            $newAnnonce->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $newAnnonce->setIdDestinationFormXml($oneDest);
            $newAnnonce->setIdFormXml($this->_idFormXml);
            $formXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveReferentielFormXmlById($this->_idFormXml);
            $newAnnonce->setConsultationId($formXml[0]->getConsultationId());
            $newAnnonce->setIdTypeXml(1);
            $newAnnonce->setStatutDestinataire(Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
            $newAnnonce->setAccuseReception(Atexo_Config::getParameter('ACCUSE_RECEPTION_NA'));
            $newAnnonce->save($connexion);
            $this->scriptClose->Text = '<script>refreshRepeater();</script>';
        }
    }

    /**
     * Ajouter les destinataires choisi dans la base.
     */
    public function checkedDestinataire()
    {
        $arCheckedDestinataire = [];
        $index = 0;
        foreach ($this->listeDestinataires->getItems() as $item) {
            if ($item->destinataire->Checked) {
                $arCheckedDestinataire[$index] = $item->idDestinataire->getValue();
                ++$index;
            }
        }

        return $arCheckedDestinataire;
    }

    /**
     * cocher les destinataire correspendont au type du formualaire xml.
     */
    public function existingDestinataire($idDestinataire)
    {
        $formXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveReferentielFormXmlById($this->_idFormXml);
        $arDest = explode(';', $formXml[0]->getReferentieltypexml()->getIdDestinataire());
        $listDest = (new Atexo_Publicite_Destinataire())->retreiveListDestinataire($this->_idFormXml);
        if (in_array($idDestinataire, $arDest)) {
            foreach ($listDest as $oneDestinataire) {
                if ($idDestinataire == $oneDestinataire->getIdDestinationFormXml()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
