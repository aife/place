<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCommission;
use Application\Propel\Mpe\CommonCommissionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Commissions;

/**
 * Formulaire de creation et modification des commissions
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class FormulaireCommissions extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            self::remplirListeTypeCommission();
            self::remplirListeStatutCommission();
            if (isset($_GET['suivi']) && isset($_GET['id'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $commission = CommonCommissionPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                if ($commission instanceof CommonCommission) {
                    $this->dateHeure->Text = Atexo_Util::iso2frnDateTime(Atexo_Util::atexoHtmlEntities($commission->getDate()), true);
                    $this->statutCommission->setSelectedValue($commission->getStatusCao());
                    $this->typeCommission->setSelectedValue($commission->getType());
                    $this->lieuCommisssion->Text = Atexo_Util::atexoHtmlEntities($commission->getLieu());
                    $this->salleCommisssion->Text = Atexo_Util::atexoHtmlEntities($commission->getSalle());
                }
            }
        }
    }

    /**
     * Permet de sauvegarder les commissions
     *
     * @param $sender
     * @param $param
     */
    public function save($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (isset($_GET['id'])) {
            $commission = CommonCommissionPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        } else {
            $commission = new CommonCommission();
        }
        $commission->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $commission->setDate(Atexo_Util::frnDateTime2iso(Atexo_Util::atexoHtmlEntities($this->dateHeure->Text)));
        $commission->setType($this->typeCommission->getSelectedValue());
        $commission->setStatusCao($this->statutCommission->getSelectedValue());
        $commission->setLieu(Atexo_Util::atexoHtmlEntities($this->lieuCommisssion->Text));
        $commission->setSalle(Atexo_Util::atexoHtmlEntities($this->salleCommisssion->Text));
        $commission->save($connexion);
        self::goToPageSuiviCommission($sender, $param);
    }

    /**
     * Permet de rediriger vers la page de suivi des commissions
     */
    public function goToPageSuiviCommission($sender, $param)
    {
        $this->response->redirect("?page=Commission.SuiviCommission&id=" . $_GET['id']);
    }

    /**
     * Permet de remplir la liste des types des commissions
     */
    public function remplirListeTypeCommission()
    {
        /** @var @TODO  */
        $data = null;
        $data = Atexo_Commission_Commissions::remplirListeTypeCommission();
        $this->typeCommission->DataSource = $data;
        $this->typeCommission->DataBind();
    }

    /**
     * Permet de remplir la liste des statuts des commissions
     */
    public function remplirListeStatutCommission()
    {
        /** @var @TODO  */
        $data = null;
        $data = Atexo_Commission_Commissions::remplirListeStatutCommission();
        $this->statutCommission->DataSource = $data;
        $this->statutCommission->DataBind();
    }
}
