<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExternePeer;
use Application\Propel\Mpe\CommonTCAOCommissionPeer;
use Application\Service\Atexo\Atexo_Languages;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Library\Propel\Query\Criteria;

/**
 * Formulaire de creation et modification des intervenents externes
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class GestionSelectionMembrePonctuel extends MpeTPage {
	
	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
    public function onLoad($param)
    { 
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
			$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
			$org = Atexo_CurrentUser::getCurrentOrganism();
			$commission = CommonTCAOCommissionPeer::retrieveByPK($idCommission, $org, $connexion);
			$this->fillDataAgents($idOrdreJour, $org, $connexion);
    		$this->commissionSummary->setDataSource($commission);
	    	$this->fillDataIntervenants($idOrdreJour, $org, $connexion);
	    	
	    	$this->setViewState('idOrdreJour',$idOrdreJour);
    }

    public function fillDataIntervenants($idOrdreJour, $org, $connexion=null){
        $listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsOrdreDuJour($idOrdreJour, $org, $connexion);
        $c = new Criteria();
        $c->add(CommonTCAOCommissionIntervenantExternePeer::ORGANISME, $org);
        $c->add(CommonTCAOCommissionIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $listIds, CRITERIA::IN);
        $c->addAscendingOrderByColumn(CommonTCAOCommissionIntervenantExternePeer::ORGANISME);
        $this->listeIntervenant->fillRepeaterWithDataForSearchResult($c);
    }
    
    public function supprimer($id, $type=false){
    	$org = Atexo_CurrentUser::getCurrentOrganism();
    	$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
    	Atexo_Commission_IntervenantExterne::deleteIntervenantOrdreJour($id, $idOrdreJour, $org, $type);
    	$this->fillDataIntervenants($idOrdreJour, $org);
    	$this->fillDataAgents($idOrdreJour, $org);
    }
    
    public function refreshRepeterIntervenants(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
    	$this->fillDataIntervenants($idOrdreJour, $org);
    }
   
    public function refreshRepeterAgents(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idOdj = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
    	$this->fillDataAgents($idOdj, $org);
    }
    
	/**
     * Permet de charger la liste de tous les agents
     */
    public function fillAllAgents()
    {
    	self::refreshRepeterIntervenants();
    	self::refreshRepeterAgents();	
    }

    public function fillDataAgents($idOrdreJour, $org, $connexion=null){
        $listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsOrdreDuJour($idOrdreJour, $org, $connexion, null, true);
        $c = new Criteria();
        $c->add(CommonAgentPeer::ORGANISME, $org);
        $c->add(CommonAgentPeer::ID, $listIds, CRITERIA::IN);
        $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
        $this->listeAgent->fillRepeaterWithDataForSearchResult($c);
    }
    
   
}