<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Application\Controls\MpeTPage;

/**
 * Classe d'envoi des documents de convocation des seances de commissions
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class EnvoiCourrierConvocationCommission extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->TemplateEnvoiCourrierElectronique->courrierElectroniqueSimple->Checked = true;
        }
        $this->TemplateEnvoiCourrierElectronique->messageType->SelectedValue = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_CONVOCATION_COMMISSION');
        $this->TemplateEnvoiCourrierElectronique->messageType->Enabled = false;
        $this->TemplateEnvoiCourrierElectronique->setOrg(Atexo_CurrentUser::getCurrentOrganism());
        $this->TemplateEnvoiCourrierElectronique->setRedirect("?page=Commission.GestionDocumentsSeance&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));
        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public static function EnvoyerEmail(CommonEchange $objetMessageVO, $org, $callFrom = false)
    {

        $connexionCom = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $org);
        $cc = '';
        $taille = 0;
        $validePJ = false;
        $envoiMail = false;
        if ($objetMessageVO->getId()) {
            $objetMessage = (new Atexo_Message())->retrieveMessageById($objetMessageVO->getId(), $org);
        } else {
            $objetMessage = $objetMessageVO->copy(true);
        }

        $objetMessage->setOrganisme($org);
        $objetMessage->setIdTypeMessage($objetMessageVO->getIdTypeMessage());
        $objetMessage->setObjet($objetMessageVO->getObjet());
        $objetMessage->setCorps($objetMessageVO->getCorps());
        $objetMessage->setOptionEnvoi($objetMessageVO->getOptionEnvoi());
        $PJs = $objetMessage->getCommonEchangepiecejointes($c, $connexionCom);
        if (is_array($PJs) && count($PJs) != 0) {
            $validePJ = true;
        }

        $from = $objetMessage->getEmailExpediteur();
        $subject = $objetMessage->getObjet();
        $destinataires = $objetMessageVO->getCommonEchangeDestinataires(null, $connexionCom);
        $zipPj = (new Atexo_Message())->getZipPj($PJs, $taille, $org);
        // TODO test sur lecture du fichier
        if ($zipPj) {
            $code = (new Atexo_Files())->read_file($zipPj);
        }
        $fileName = Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
        if ($taille >= Atexo_Config:: getParameter("MAX_FILE_SIZE_IN_MAIL_COMMISSION") && $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {//application
            $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
        }
        $arrayMailsDestinataires = array();
        if (is_array($destinataires) && count($destinataires) != 0) {
            foreach ($destinataires as $destinataire) {
                if ($destinataire instanceof CommonEchangeDestinataire) {
                    if (!in_array($destinataire->getMailDestinataire(), $arrayMailsDestinataires)) {
                        $arrayMailsDestinataires[] = $destinataire->getMailDestinataire();
                        $message = '';
                        if ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                            $message = $objetMessage->getCorps();
                        } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                            $message = self::AccuseReception($objetMessage->getCorps(), $destinataire->getUid(), $destinataire->getIdEchange(), Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'), $org);
                        }

                        if ($validePJ
                            &&
                            $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')
                            &&
                            $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')
                        ) {
                            if ((new Atexo_Message())->mailWithAttachement($from, $destinataire->getMailDestinataire(), $subject, $message, $cc, $code, $fileName, $from)) {
                                $envoiMail = true;
                            }
                        } else {
                            if (Atexo_Message::simpleMail($from, $destinataire->getMailDestinataire(), $subject, $message, $cc, $from)) {
                                $envoiMail = true;
                            }
                        }
                    }
                }
            }
        }
        if ($envoiMail) {
            $objetMessage->setDateMessage(date("Y-m-d H:i:s"));
            $objetMessage->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE'));
            $objetMessage->save($connexionCom);
            return $objetMessage->getId();
        } else {
            return false;
        }
    }

    private function AccuseReception($bodyMsg, $arNum, $idEmail, $cas, $acrOrg)
    {
        if ($idEmail) {
            $confirmUrl = self::getUrlAR($arNum, $acrOrg, $idEmail);
            $message = self::getTextARNum($confirmUrl, $cas);
            $body = $message . $bodyMsg;
        }
        return $body;
    }

    private function getUrlAR($numAR, $acrOrg, $idEmail = null)
    {
        if (!$numAR) {
            PrintMessage(Prado::localize('GENERATION_URL_ACCUSE_RECEPTION_ECHEC') . " : " . Prado::localize('NUMERO_ACCUSE_RECEPTION') . ".\n");
            return false;
        }

        $confirmationUrl = Atexo_Config::getParameter('PF_URL') . "?page=Commun.CommissionDetailEchangeMail&num_ar=" . $numAR . "&orgAcronyme=" . $acrOrg;
        $confirmationUrl .= '
';
        return $confirmationUrl;
    }

    private function getTextARNum($confirmUrlText, $cas)
    {
        if ($cas == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
            return Prado::localize('VEUILLEZ_CLIQUER_LIEN_CONFIRMER_RECEPTION_MAIL_CAS_2') . "\n\n" . $confirmUrlText . "\n______________________\n\n";
        } elseif ($cas == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
            return Prado::localize('VEUILLEZ_CLIQUER_LIEN_CONFIRMER_RECEPTION_MAIL_CAS_3') . "\n\n" . $confirmUrlText;
        }
    }
}