<?php

namespace Application\Pages\Commission;

use Application\Propel\Mpe\CommonTCAOIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOIntervenantExterneQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado\Prado;
use Application\Controls\MpeTPage;
use Prado\Util\TLogger;

/**
 * Formulaire de creation et modification des intervenents externes
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class FormulaireIntervenantExterne extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            self::remplirCivilite();
            self::remplirTypeVoix();
            self::remplirModeComminucation();

            if (isset($_GET["id"]) && !empty($_GET["id"]) && !isset($_GET["creation"])) {

                self::remplirChamps($_GET["id"]);

            }
        }
    }

    public function remplirCivilite()
    {
        $datasource [0] = Prado::localize("TEXT_SELECTIONNER") . "...";
        $datasource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_CIVILITE'), $datasource);
        $this->civilite->DataSource = $datasource;
        $this->civilite->DataBind();
    }

    public function remplirTypeVoix()
    {
        $datasource [0] = Prado::localize("TEXT_SELECTIONNER") . "...";
        $datasource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'), $datasource);
        $this->typeVoix->DataSource = $datasource;
        $this->typeVoix->DataBind();
    }

    public function remplirModeComminucation()
    {
        $datasource [0] = Prado::localize("TEXT_SELECTIONNER") . "...";
        $datasource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_MODE_COMMUNICATION_INTERVENANTS'), $datasource);
        $this->modeComm->DataSource = $datasource;
        $this->modeComm->DataBind();
    }

    public function save()
    {

        try {
            $idIntervenant = $_GET["id"];
            $organisme = Atexo_CurrentUser::getOrganismAcronym();

            $intervenant = CommonTCAOIntervenantExterneQuery::create()
                ->findPk(array($idIntervenant, $organisme));

            if (!($intervenant instanceof CommonTCAOIntervenantExterne)) {
                $intervenant = new CommonTCAOIntervenantExterne();
            }


            $intervenant->setOrganisme($organisme);
            $intervenant->setIdRefValCivilite(Atexo_Util::atexoHtmlEntities($this->civilite->SelectedValue));
            $intervenant->setNom(Atexo_Util::atexoHtmlEntities($this->nom->text));
            $intervenant->setPrenom(Atexo_Util::atexoHtmlEntities($this->prenom->text));
            $intervenant->setOrganisation(Atexo_Util::atexoHtmlEntities($this->organisation->text));
            $intervenant->setFonction(Atexo_Util::atexoHtmlEntities($this->fonction->text));
            $intervenant->setAdresse(Atexo_Util::atexoHtmlEntities($this->addresse->text));
            $intervenant->setEmail(Atexo_Util::atexoHtmlEntities($this->email->text));
            $intervenant->setCodePostal(Atexo_Util::atexoHtmlEntities($this->cp->text));
            $intervenant->setVille(Atexo_Util::atexoHtmlEntities($this->ville->text));
            $intervenant->setIdRefValModeCommunication(Atexo_Util::atexoHtmlEntities($this->modeComm->SelectedValue));
            $intervenant->setIdRefValTypeVoixDefaut(Atexo_Util::atexoHtmlEntities($this->typeVoix->SelectedValue));

            $intervenant->save();

            self::goBackToPreviousPage($intervenant->getIdIntervenantExterne(), $organisme);

        } catch (\Exception $e) {
            //print_r($e->getMessage());
            Prado::log("Erreur enregistrement intervenant :\n" . $e->getMessage(), TLogger::ERROR, "Atexo.Pages");
            throw new Atexo_Exception("Erreur enregistrement intervenant :\n" . $e->getMessage());
        }

    }

    public function remplirChamps($idIntervenant)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = CommonTCAOIntervenantExterneQuery::create()
            ->findPk(array($idIntervenant, $organisme));

        if ($intervenant instanceof CommonTCAOIntervenantExterne) {
            $this->nom->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getNom());
            $this->prenom->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getPrenom());
            $this->organisation->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getOrganisation());
            $this->fonction->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getFonction());
            $this->addresse->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getAdresse());
            $this->email->text = $intervenant->getEmail();
            if ($intervenant->getIdRefValCivilite()) {
                $this->civilite->SelectedValue = $intervenant->getIdRefValCivilite();
            }
            if ($intervenant->getIdRefValModeCommunication()) {
                $this->modeComm->SelectedValue = $intervenant->getIdRefValModeCommunication();
            }
            if ($intervenant->getIdRefValTypeVoixDefaut()) {
                $this->typeVoix->SelectedValue = $intervenant->getIdRefValTypeVoixDefaut();
            }
            $this->cp->text = $intervenant->getCodePostal();
            $this->ville->text = Atexo_Util::atexoHtmlEntitiesDecode($intervenant->getVille());
        }
    }

    public function goBackToPreviousPage($idIntExterne, $org)
    {

        if (isset($_GET['calledFrom'])) {

            $calledFrom = $_GET['calledFrom'];
            $id = $_GET['id'];

            if ($calledFrom == 'com') {

                //Ajouter l'intervenant a la Commission
                Atexo_Commission_IntervenantExterne::addIntervenantToCommission($idIntExterne, base64_decode(Atexo_Util::atexoHtmlEntities($id)), $org);
                //Redirection vers la page d'origine
                $this->response->redirect("?page=Commission.FormulaireCommission&id=" . $id);

            } elseif ($calledFrom == 'seance') {

                //Ajouter l'intervenant a la seance
                Atexo_Commission_IntervenantExterne::addIntervenantToSeance($idIntExterne, base64_decode(Atexo_Util::atexoHtmlEntities($id)), $org);
                //Redirection vers la page d'origine
                $this->response->redirect("?page=Commission.GestionMembresSeance&id=" . $id);

            } elseif ($calledFrom == 'odj') {

                //Ajouter l'intervant a l'ordre du jour
                Atexo_Commission_IntervenantExterne::addIntervenantToOrdreDePassage($idIntExterne, base64_decode(Atexo_Util::atexoHtmlEntities($id)), $org);
                //Redirection vers la page d'origine
                $this->response->redirect("?page=Commission.GestionMembresPonctuels&idS=" . $_GET['idS'] . "&id=" . $id);

            }
        }

        $this->response->redirect("?page=Commission.ListeIntervenantsExternes");
    }


    public function goToListIntervenants()
    {
        $this->response->redirect("?page=Commission.ListeIntervenantsExternes");
    }
}