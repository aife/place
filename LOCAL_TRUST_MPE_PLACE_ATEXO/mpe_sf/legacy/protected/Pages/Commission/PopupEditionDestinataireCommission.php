<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;


/**
 * Popup de selection des destinataires des convocations electroniques aux seances de Commission
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package agent
 * @subpackage Pages
 */
class PopupEditionDestinataireCommission extends MpeTPage {

	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
    	Atexo_Languages::setLanguageCatalogue("agent");
    }
    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->loadCourrielsMembresPermanents();
            $this->loadCourrielsInvitesPonctuels();    
        }
        $this->adressesIntervenants->Enabled = false;
        $this->adressesInvitesPonctuels->Enabled = false;
    }
    /**
     * Permet de charger la liste des courriels des membres permanents de la seance de Commission
     * @return : chaine contenant les emails des invites permanents de la seance de Commission
     */
    public function loadCourrielsMembresPermanents()
    {
		//Recuperation des membres permanents de la seance
		$idSeance   = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
		$organisme  = Atexo_CurrentUser::getCurrentOrganism();
		$_t_permanents = (new Atexo_Commission_Seance())->getAllPermanentsSeance($idSeance,$organisme);
		
		//Recuperation de la liste des courriels sous forme de chaine
		$_t_mail_adresses = array();
		foreach ($_t_permanents as $_permanent)
		{
			$_t_mail_adresses[] = $_permanent->getEmail();			
		}		
		$_b_mail_adresses = implode(',', $_t_mail_adresses);
		
		$this->adressesIntervenants->Text = $_b_mail_adresses;		
		
    }
    
	/**
     * Permet de charger la liste des courriels des invites ponctuels de la seance de Commission
     * @return : chaine contenant les emails des invites ponctuels de la seance de Commission
     */
    public function loadCourrielsInvitesPonctuels()
    {
    	
    	//Recuperation des membres permanents de la seance
    	$idSeance   = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
    	$organisme  = Atexo_CurrentUser::getCurrentOrganism();
    	$_t_invites = (new Atexo_Commission_Seance())->getAllInvitesSeance($idSeance,$organisme);
    	
    	//Recuperation de la liste des courriels sous forme de chaine
    	$_t_mail_adresses = array();
    	foreach ($_t_invites as $_invite)
    	{
    		$_t_mail_adresses[] = $_invite->getEmail();
    	}
    	$_b_mail_adresses = implode(',', $_t_mail_adresses);
    	
    	$this->adressesInvitesPonctuels->Text = $_b_mail_adresses;
    	
    }
    /**
     * Permet de valider les informations et revenir a la page d'envoi des mails
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function valider($sender, $param)
    {
    	if ($this->IsValid)
		{
		    $tabMailsIntervenants = array();
		    $tabMailsInvitesPonctuels = array();
		    $tabMailsLibres = array();
			if($this->checkAdressesIntervenants->Checked){
				$trimmedIntervenants = str_replace(" ","",$this->adressesIntervenants->Text);
				$mailsIntervenants = explode(',',$trimmedIntervenants);
			    if(is_array($mailsIntervenants) && count($mailsIntervenants) != 0)
			    foreach($mailsIntervenants as $mail){
			   		if(!in_array($mail,$tabMailsIntervenants) && !empty($mail)){
			   			$tabMailsIntervenants[] = $mail;
			   		}
			    }
			}
			
			if($this->checkAdressesInvitesPonctuels->Checked){
				$trimmedInvitesPonctuels = str_replace(" ","",$this->adressesInvitesPonctuels->Text);
				$mailsInvitesPonctuels = explode(',',$trimmedInvitesPonctuels);
			    if(is_array($mailsInvitesPonctuels) && count($mailsInvitesPonctuels) != 0)
			    foreach($mailsInvitesPonctuels as $mail){
			   		if(!in_array($mail,$tabMailsInvitesPonctuels) && !empty($mail)){
			   			$tabMailsInvitesPonctuels[] = $mail;
			   		}
			    }
			}
			
			if($this->checkAdressesLibres->Checked){
				$trimmedAdressesLibres = str_replace(" ","",$this->adressesLibres->Text);
				$mailsAdressesLibres = explode(',',$trimmedAdressesLibres);
			    if(is_array($mailsAdressesLibres) && count($mailsAdressesLibres) != 0)
			    foreach($mailsAdressesLibres as $mail){
			   		if(!in_array($mail,$tabMailsLibres) && !empty($mail)){
			   			$tabMailsLibres[] = $mail;
			   		}
			    }
			}
			
			$_t_mails = array_merge($tabMailsIntervenants,$tabMailsInvitesPonctuels,$tabMailsLibres);
			$_b_mails = implode(',',$_t_mails);
			
			echo "<script>
					var destinataires = opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value;
					if(destinataires != null && destinataires !='') {
						destinataires = destinataires+',';
					} 
					opener.document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectronique_destinataires').value=destinataires+'".$_b_mails."';
					window.close();
				 </script>";
		}    
    }
    
	public function checkMultipleEmailIntervenantsExternes($sender, $param) {
		if($this->checkAdressesIntervenants->Checked) {
			if (trim($this->adressesIntervenants->Text) !== "") {
				$emailsErrone = Atexo_Util::checkMultipleEmail(Atexo_Util::atexoHtmlEntities($this->adressesIntervenants->Text));
				if (is_array($emailsErrone) && count($emailsErrone) > 0 ) {
					$this->validateurEmailsIntervenants->ErrorMessage = Prado::localize('DEFINE_ADRESSES_INTERVENANTS')." : ".implode(",",$emailsErrone);
					$this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
			  		$param->IsValid = false;
				}
				else {
					 $param->IsValid = true;
				}
			}
		 }	
	}
 	public function checkMultipleEmailInvitesPonctuels($sender, $param) {
 			if($this->checkAdressesInvitesPonctuels->Checked) {
			if (trim($this->adressesInvitesPonctuels->Text) !== "") {
				$emailsErrone = Atexo_Util::checkMultipleEmail(Atexo_Util::atexoHtmlEntities($this->adressesInvitesPonctuels->Text));
				if (is_array($emailsErrone)) {
					$this->validateurEmailInvitesPonctuels->ErrorMessage = Prado::localize('DEFINE_ADRESSE_INVITES_PONCTUELS')." : ".implode(",",$emailsErrone);
					$this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
			  		$param->IsValid = false;
				}
				else {
					 $param->IsValid = true;
				}
			}
		 }	
	}
  	public function checkMultipleEmailsLibres($sender, $param) {
 			if($this->checkAdressesLibres->Checked) {
			if (trim($this->adressesLibres->Text) !== "") {
				$emailsErrone = Atexo_Util::checkMultipleEmail(Atexo_Util::atexoHtmlEntities($this->adressesLibres->Text));
				if (is_array($emailsErrone)) {
					$this->validateurEmailsLibres->ErrorMessage = Prado::localize('DEFINE_TEXT_LIST_ADRESSE_LIBRE')." : ".implode(",",$emailsErrone);
					$this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
			  		$param->IsValid = false;
				}
				else {
					 $param->IsValid = true;
				}
			}
		 }	
	}
}    