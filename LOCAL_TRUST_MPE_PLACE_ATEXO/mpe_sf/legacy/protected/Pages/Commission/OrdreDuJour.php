<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTCAOOrdreDePassageQuery;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;

/**
 * Classe de gestion de l'ordre du jour pour les commissions d'appel d'offre
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class OrdreDuJour extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            if (isset($_GET['id'])) {

                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $primary_key = array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
                $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);

                if ($seance instanceof CommonTCAOSeance) {
                    //Recuperation des informations sur la seance
                    $this->seanceSummary->setDataSource($seance);
                    $this->setViewState('seance', $seance);
                    //Recuperation des informations sur l'ordre du jour
                    self::remplirOrdreDuJour($seance);
                }
            }
        }
    }

    public function remplirOrdreDuJour($seance)
    {
        //Recuperation des champs d'interet
        if ($seance instanceof CommonTCAOSeance) {
            $id = $seance->getIdSeance();
            $org = $seance->getOrganisme();

            //recuperation de l'ensemble des ordres de passage
            $_t_ordre_de_passage = (new Atexo_Commission_Seance())->getOrdreDuJour($id, $org);

            $this->listeOrdresDuJour->dataSource = $_t_ordre_de_passage;
            $this->listeOrdresDuJour->databind();
        }
    }


    public function getDataSourceHeure()
    {
        $dataSource = array();
        $dataSource['08'] = '08';
        $dataSource['09'] = '09';
        $dataSource['10'] = '10';
        $dataSource['11'] = '11';
        $dataSource['12'] = '12';
        $dataSource['13'] = '13';
        $dataSource['14'] = '14';
        $dataSource['15'] = '15';
        $dataSource['16'] = '16';
        $dataSource['17'] = '17';
        $dataSource['18'] = '18';
        $dataSource['19'] = '19';
        return $dataSource;
    }

    public function getDataSourceMin()
    {
        $dataSource = array();
        $dataSource['00'] = '00';
        $dataSource['05'] = '05';
        $dataSource['10'] = '10';
        $dataSource['15'] = '15';
        $dataSource['20'] = '20';
        $dataSource['25'] = '25';
        $dataSource['30'] = '30';
        $dataSource['35'] = '35';
        $dataSource['40'] = '40';
        $dataSource['45'] = '45';
        $dataSource['50'] = '50';
        $dataSource['55'] = '55';
        return $dataSource;
    }

    public function getEtapeListe()
    {
        return (new Atexo_Commission_Seance())->getEtapeListe();
    }

    public function getSelectedHeure($dataSource, $date)
    {
        $t_date = explode(':', $date);
        $t_heure = explode(' ', $t_date[0]);
        $value = $t_heure[1];

        if (isset($dataSource[$value])) {
            return $value;
        } else {
            return '';
        }
    }

    public function getSelectedMin($dataSource, $date)
    {
        $t_heure = explode(':', $date);
        $value = $t_heure[1];

        if (isset($dataSource[$value])) {
            return $value;
        } else {
            return '';
        }
    }

    public function getLibelleTypeProcedure($idTypeProcedure)
    {
        $proceduresTypes = Atexo_Consultation_ProcedureType:: retrieveProcedureType();
        $dataProcedures = array();
        if (is_array($proceduresTypes)) {
            foreach ($proceduresTypes as $typeProc) {
                $dataProcedures[$typeProc->getIdTypeProcedure()] = $typeProc->getLibelleTypeProcedure();
            }
        }
        return ($dataProcedures[$idTypeProcedure]) ? $dataProcedures[$idTypeProcedure] : '-';
    }

    public function getReferenceOrdreDuJour($consultation)
    {
        if ($consultation->getRefLibre()) {
            return $consultation->getRefLibre();
        } else if ($consultation->getRefConsultation()) {
            return $consultation->getRefConsultation();
        } else {
            return '-';
        }
    }

    public function getLibelleCategorie($idCategorie)
    {

        return Atexo_Consultation_Category::retrieveLibelleCategorie($idCategorie);
    }

    public function getPictoCategorie($idCategorie)
    {

        return (new Atexo_Consultation_Category())->retrievePictoCategorie($idCategorie);
    }

    public function getServiceGestionnaire($consultation)
    {

        if ($consultation->getServiceGestionnaire()) {
            return $consultation->getServiceGestionnaire();
        } else {
            $service_g_path = Atexo_EntityPurchase::getSigleLibelleEntityById($consultation->getIdServiceGestionnaire(), $consultation->getOrganisme());
            if ($service_g_path) {
                return $service_g_path;
            } else {
                return '-';
            }
        }
    }

    public function getServiceAssocie($consultation)
    {

        if ($consultation->getServiceAssocie()) {
            return $consultation->getServiceAssocie();
        } else {
            $service_g_path = Atexo_EntityPurchase::getSigleLibelleEntityById($consultation->getIdServiceGestionnaire(), $consultation->getOrganisme());
            $service_a_path = Atexo_EntityPurchase::getSigleLibelleEntityById($consultation->getIdServiceAssocie(), $consultation->getOrganisme());

            if ($service_a_path && $service_a_path <> $service_g_path) {
                return $service_a_path;
            } else {
                return '-';
            }
        }
    }

    /**
     * @TODO Verifier la valeur de $etape avant d'en afficher le contenu
     */
    public function getPrecedenteSeance($idSeance, $dateSeance, $consultation)
    {

        $buffer = '-';
        $ordre_de_passage = (new Atexo_Commission_Seance())->getPrecedenteSeance($idSeance, $dateSeance, $consultation);
        if ($ordre_de_passage) {
            //Recuperation de la date de la seance precedente
            $t_date = explode(' ', $ordre_de_passage->getDateSeance());
            $date = $t_date[0];
            //Recuperation de l'etape de la seance precedente
            $etape = (new Atexo_Commission_Seance())->getEtapeListe($ordre_de_passage->getIdRefOrgValEtape());
            //Recuperation du sigle de la Commission
            $sigle = $ordre_de_passage->getCommonTCAOCommission()->getSigle();
            //Generation du texte a afficher

            $buffer = '<a href="?page=Commission.OrdreDuJour&id=' . base64_encode($ordre_de_passage->getIdSeance()) . '&calledFrom=' . base64_encode($idSeance) . '">';
            $buffer .= Atexo_Util::iso2frnDate($date) . '&nbsp;-&nbsp;' . $etape . '&nbsp;(' . $sigle . ')';
            $buffer .= '</a>';

        }
        return $buffer;
    }

    public function goToPageSuiviSeance($sender, $param)
    {
        if (isset($_GET['calledFrom'])) {

            $id = $_GET['calledFrom'];
            if ($id) {
                $this->response->redirect("?page=Commission.OrdreDuJour&id=" . $id);
            }
        }

        $this->response->redirect("?page=Commission.SuiviSeance");
    }


    public function save($sender, $param)
    {
        foreach ($this->listeOrdresDuJour->Items as $item) {
            //Recuperation de l'ordre de passage
            $ordre_de_passage = CommonTCAOOrdreDePassageQuery::create()
                ->findPk(array($item->id_ordre_de_passage->value, $item->organisme->value));

            if ($ordre_de_passage) {
                //Recuperation des champs du formulaire
                $heure = $item->heure->getSelectedValue();
                $minutes = $item->minutesheure->getSelectedValue();
                $horaire = $heure . ':' . $minutes;

                //Preparation de la date de passage
                $t_date = explode(' ', $ordre_de_passage->getDateSeance());
                $date_seance = $t_date[0];
                $horaire_seance = $t_date[1];

                //Recuperation de l'horaire de la seance si l'horaire du passage est inférieur
                if ($horaire < $horaire_seance) {
                    $horaire = $horaire_seance;
                }

                $date_passage = $date_seance . ' ' . $horaire;
                //Preparation de l'id de l'etape
                $etape = $item->etapeList->getSelectedValue();
                //Recuperation du numero d'ordre
                $numOdj = $item->numOdj->Text;

                //Mise a jour de l'enregistrement
                $ordre_de_passage->setOrdreDePassage($numOdj);
                $ordre_de_passage->setIdRefOrgValEtape($etape);
                $ordre_de_passage->setDatePassage($date_passage);

                //Sauvegarde
                $ordre_de_passage->save();
            }
        }
        self::goToPageSuiviSeance($sender, $param);
    }

    public function supprimerOrdreDePassage($sender, $param)
    {
        //Recuperation de l'id de l'ordre de passage a supprimer
        $idOrdreDePassage = $param->CommandName;
        $organisme = Atexo_CurrentUser::getOrganismAcronym();

        (new Atexo_Commission_Seance())->supprimerOrdreDePassage($idOrdreDePassage, $organisme);
    }

    /**
     * Afficher le nouveau tableau apres la suppression d'un ordre_du_jour
     */
    public function onCallBackRefreshRepeater($sender, $param)
    {
        self::remplirOrdreDuJour($this->getViewState('seance'));
        $this->panelOrdreDuJour->render($param->NewWriter);
    }


    public function truncate($texte)
    {
        $maximumNumberOfCaracterToReturn = 230;
        $arrayText = explode(" ", $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            $indexArrayText++;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= " " . $arrayText[$indexArrayText];
        }
        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 230) ? "" : "display:none";
    }
}