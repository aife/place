<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\Classes\Util;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCommission;
use Application\Propel\Mpe\CommonCommissionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_generationDocumentsCommission;
use AtexoPdf\RtfGeneratorClient;
use Prado\Prado;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Service\Atexo\Commission\Atexo_Commission_Commissions;
use Application\Propel\Mpe\CommonTCAOSeancePeer;

/**
 * Classe de gestion de gestion des documents de la Commission
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class GestionDocumentsCommission extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->messageErreur->visible = false;
            if (isset($_GET['idCom'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $commission = CommonCommissionPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom'])), Atexo_CurrentUser::getCurrentOrganism(), $connexion);
                if ($commission instanceof CommonCommission) {
                    $this->commissionSummary->setDataSource($commission);
                    $this->fillRepeaterWithDataForSearchResult(new Criteria());
                }
            }
        }
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $nombreElement = self::search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState("nombreElement", $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
            $this->tableauMessage->setVirtualItemCount($nombreElement);
            $this->tableauMessage->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData($criteriaVo)
    {
        $offset = $this->tableauMessage->CurrentPageIndex * $this->tableauMessage->PageSize;
        $limit = $this->tableauMessage->PageSize;
        if ($offset + $limit > $this->tableauMessage->getVirtualItemCount()) {
            $limit = $this->tableauMessage->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayIntervenant = self::search($criteriaVo);
        $this->setViewState("elements", $arrayIntervenant);
        $this->tableauMessage->DataSource = $arrayIntervenant;
        $this->tableauMessage->DataBind();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauMessage->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState("CriteriaVo");
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ($openCloseTag == "open") {
                return "<strong>";
            } else {
                return "</strong>";
            }
        } else {
            return "";
        }
    }

    public function goToPage($sender, $param)
    {
        switch ($sender->ID) {
            case "DefaultButtonTop" :
                $numPage = $this->numPageTop->Text;
                break;
            case "DefaultButtonBottom" :
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } else if ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauMessage->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState("CriteriaVo");
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauMessage->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauMessage->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case "nombreResultatAfficherBottom" :
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case "nombreResultatAfficherTop" :
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauMessage->PageSize = $pageSize;
        $nombreElement = $this->getViewState("nombreElement");
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
        $criteriaVo = $this->getViewState("CriteriaVo");
        $this->tableauMessage->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function search($c, $nbreElement = false)
    {
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        return Atexo_Message::getAllEchangeCommission($idCommission, $org, $c, $nbreElement);
    }

    public function getDestinataires($destinataires)
    {
        return implode("<br/>", $destinataires);
    }

    /**
     * Permet de generer l'excel des ordres du jour de la Commission
     */
    public function telechargerOrdreDuJour($sender, $param)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $listeOrdreDuJour = [];
        $listeOrdreDuJour = Atexo_Commission_Commissions::remplirListeOrdreDuJour(Atexo_Util::atexoHtmlEntities($_GET['idCom']), $organisme);
        $seance = CommonTCAOSeancePeer::retrieveByPK(null, $organisme);
//        Atexo_GenerationExcel::telechargerOrdreDuJour($seance, $listeOrdreDuJour, $organisme);
    }

    /**
     * Permet de generer et telecharger le PV de la CAO
     */
    public function telechargerPvCao($sender, $param)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $langue = Atexo_Languages::readLanguageFromSession();
        $connexion = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_ONLY'));
        $commission = CommonCommissionPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom'])), $organisme, $connexion);
        $modele = self::getModeleDocSpecifique($commission->getType());
        $nomFichier = '';
        if (!is_file($modele . $langue . '.odt')) {
            $nomFichier = Atexo_Config:: getParameter('CAO_NOM_FICHIER_PV');
            $modele = self::getCheminTemplatesDocsStandards() . $nomFichier;
        }
        //Ordres du jour
        $listeOrdreDuJour = Atexo_Commission_Commissions::remplirListeOrdreDuJour(Atexo_Util::atexoHtmlEntities($_GET['idCom']), $organisme);
        $ordreDuJour = new CommonTCAOOrdreDePassage();
        $tmpFileDir = Atexo_Config::getParameter('COMMON_TMP') . "/dir_pv" . uniqid();
        $tmpZipFile = $tmpFileDir . '.zip';
        if (is_file($tmpZipFile)) {
            system("rm -f " . $tmpZipFile, $sys_answer);
        }

        system(" mkdir $tmpFileDir ; chmod 777 $tmpFileDir ", $sys_answer);
        system(" cd $tmpFileDir; zip -r $tmpZipFile .  > " . Atexo_Config::getParameter('COMMON_TMP') . "zip_error 2>&1", $sys_answer);
        if (is_array($listeOrdreDuJour) && count($listeOrdreDuJour)) {
            foreach ($listeOrdreDuJour as $ordreDuJour) {
                $fileGenere = Atexo_GenerationFichier_generationDocumentsCommission::generationPvCommission($commission, $modele, true, $langue, $ordreDuJour, true);
                //Generation rtf
                $rtfContent = (new RtfGeneratorClient())->genererRtf($fileGenere);
                if (!$nomFichier) {
                    $nomFichier = self::getNomFichierSpecifique($commission->getType());
                }
                //Telechargement du document e la volee
                //DownloadFile::downloadFileContent($nomFichier, $rtfContent);
                $tmpFile = '';
                if ($ordreDuJour->getRefConsultation()) {
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . "/" . Atexo_Util::clean(Atexo_Util::OterAccents($nomFichier)) . '_' . str_replace('/', '_', $ordreDuJour->getRefConsultation()) . '.rtf';
                } else {
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . "/" . Atexo_Util::clean(Atexo_Util::OterAccents($nomFichier)) . '_' . str_replace('/', '_', $ordreDuJour->getRefLibre()) . '.rtf';
                }
                $fp = fopen($tmpFile, "w");
                fwrite($fp, $rtfContent);
                fclose($fp);
                //Si c'est le premier fichier alors le zipper
                (new Atexo_Zip())->addFileToZip($tmpFile, $tmpZipFile);
            }
        } else {//Afficher un message d'erreur
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_ORDRE_DU_JOUR_TELECHARGER_PV'));
            return;
        }
        //Telechargement du document e la volee
        DownloadFile::downloadFileContent(prado::localize(DEFINE_PV) . '.zip', file_get_contents($tmpZipFile));

    }

    /**
     *
     * Retourne le chemin du projet MPE
     */
    public function getCheminMpe()
    {
        return realpath(dirname(__FILE__) . Atexo_Config::getParameter('CHEMIN_MPE_DEPUIS_PAGE_COMMISSION'));
    }

    /**
     *
     * Permet de telecharger l'ensemble des convocations
     */
    public function telechargerConvocations($sender, $param)
    {
        //Si zip n'existe pas, zipper le fichier
        //Si zip existe, ajouter le fichier au zip
        //Ordres du jour
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $connexion = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_ONLY'));
        $listeOrdreDuJour = Atexo_Commission_Commissions::remplirListeOrdreDuJour(Atexo_Util::atexoHtmlEntities($_GET['idCom']), $organisme);
        $tmpFileDir = Atexo_Config::getParameter('COMMON_TMP') . "/dir_convocation" . uniqid();
        $tmpZipFile = $tmpFileDir . '.zip';
        if (is_file($tmpZipFile)) {
            system("rm -f " . $tmpZipFile, $sys_answer);
        }

        system(" mkdir $tmpFileDir ; chmod 777 $tmpFileDir ", $sys_answer);
        system(" cd $tmpFileDir; zip -r $tmpZipFile .  > " . Atexo_Config::getParameter('COMMON_TMP') . "zip_error 2>&1", $sys_answer);

        if (is_array($listeOrdreDuJour) && count($listeOrdreDuJour)) {
            foreach ($listeOrdreDuJour as $ordreDuJour) {
                $listeAgentsCommission = array();
                //Liste des agents Elus
                $agentsElusCommission = array();
                $idsAgentsElusCommission = Atexo_Commission_AgentCommission::getAllIdsAgentsCommission(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom'])), $organisme, $connexion);
                $criteria = new Criteria();
                $criteria->add(CommonAgentPeer::ORGANISME, $organisme);;
                if (is_array($idsAgentsElusCommission) && count($idsAgentsElusCommission) > 0) {
                    $query = "id in (" . implode(",", $idsAgentsElusCommission) . ")";
                    $criteria->add(CommonAgentPeer::ID, $query, Criteria::CUSTOM);
                    $agentsElusCommission = CommonAgentPeer::doSelect($criteria, $connexion);
                }
                $listeAgentsCommission = array_merge($listeAgentsCommission, $agentsElusCommission);
                //Liste des agents intervenants externes Commission
                $agentsIntervenantsExternesCommission = array();
                $idsAgentsIntervenantsExternesCommission = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsCommission(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom'])), $organisme, $connexion);
                $c = new Criteria();
                $c->add(CommonIntervenantExternePeer::ORGANISME, $organisme);;
                if (is_array($idsAgentsIntervenantsExternesCommission) && count($idsAgentsIntervenantsExternesCommission) > 0) {
                    $query = "id in (" . implode(",", $idsAgentsIntervenantsExternesCommission) . ")";
                    $c->add(CommonIntervenantExternePeer::ID, $query, Criteria::CUSTOM);
                    $agentsIntervenantsExternesCommission = Atexo_Commission_IntervenantExterne::getIntervenants($c);
                }
                $listeAgentsCommission = array_merge($listeAgentsCommission, $agentsIntervenantsExternesCommission);
                //Liste des agents intervenants externes Ordre du jour Commission
                $agentsIntervenantsExternesOdjCommission = array();
                $idsAgentsIntervenantsExternesOdjCommission = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsOrdreDuJour($ordreDuJour->getId(), $organisme, $connexion);
                $c1 = new Criteria();
                $c1->add(CommonIntervenantExternePeer::ORGANISME, $organisme);;
                if (is_array($idsAgentsIntervenantsExternesOdjCommission) && count($idsAgentsIntervenantsExternesOdjCommission) > 0) {
                    $query = "id in (" . implode(",", $idsAgentsIntervenantsExternesOdjCommission) . ")";
                    $c1->add(CommonIntervenantExternePeer::ID, $query, Criteria::CUSTOM);
                    $agentsIntervenantsExternesOdjCommission = Atexo_Commission_IntervenantExterne::getIntervenants($c1);
                }
                $listeAgentsCommission = array_merge($listeAgentsCommission, $agentsIntervenantsExternesOdjCommission);
                //Liste des agents intervenants ponctuels Commission
                $listeInvitesPonctuelODJ = Atexo_Commission_Commissions::getInvitesPonctuelsOrdreDuJour($ordreDuJour->getId(), $organisme);
                if (!is_array($listeInvitesPonctuelODJ)) {
                    $listeInvitesPonctuelODJ = array();
                }
                $listeAgentsCommission = array_merge($listeAgentsCommission, $listeInvitesPonctuelODJ);

                $prenomNomInvites = '';
                if (is_array($listeAgentsCommission) && count($listeAgentsCommission)) {
                    foreach ($listeAgentsCommission as $invite) {
                        $prenomNomInvites .= $invite->getPrenom() . ' ' . $invite->getNom() . "\n";
                        $commission = CommonCommissionPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom'])), $organisme, $connexion);
                        $langue = Atexo_Languages::readLanguageFromSession();
                        $modele = self::getCheminModeleDocConvocation();
                        if (!is_file($modele . $langue . '.odt')) {
                            $modele = self::getCheminTemplatesDocsStandards() . Atexo_Config:: getParameter('CAO_MODELE_CONVOCATION');
                        }
                        //Generation modele odt
                        $fileGenere = Atexo_GenerationFichier_generationDocumentsCommission::generationConvocCommission($commission, $invite, $modele, true, $langue, true);
                        //Generation rtf
                        $rtfContent = (new RtfGeneratorClient())->genererRtf($fileGenere);
                        $nomFichier = prado::localize(DEFINE_CONVOCATION) . '_' . $invite->getPrenom() . '_' . $invite->getNom();
                        $nomFichier = Atexo_Util::clean(Atexo_Util::OterAccents($nomFichier));
                        $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . "/" . $nomFichier . '.rtf';
                        $fp = fopen($tmpFile, "w");
                        fwrite($fp, $rtfContent);
                        fclose($fp);
                        //Si c'est le premier fichier alors le zipper
                        (new Atexo_Zip())->addFileToZip($tmpFile, $tmpZipFile);
                    }
                } else {//Afficher un message d'erreur
                    $this->messageErreur->visible = true;
                    $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_INVITES_TELECHARGER_ENSEMBLE_CONVOCATION'));
                    return;
                }
            }
        } else {//Afficher un message d'erreur
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_ORDRE_DU_JOUR_TELECHARGER_ENSEMBLE_CONVOCATION'));
            return;
        }
        //Telechargement du document e la volee
        DownloadFile::downloadFileContent(prado::localize(DEFINE_CONVOCATION) . '.zip', file_get_contents($tmpZipFile));

    }

    /**
     *
     * Retourne le chemin du modele de documents de convocations
     */
    public function getCheminModeleDocConvocation()
    {
        return self::getCheminTemplatesDocs() . Atexo_Config:: getParameter('CAO_MODELE_CONVOCATION');
    }

    /**
     *
     * Retourne le chemin des templates des modeles de document
     */
    public function getCheminTemplatesDocs()
    {
        return Atexo_Config:: getParameter('CAO_REP_TEMPLATES');
    }

    /**
     *
     * Retourne le chemin des templates des modeles de document
     */
    public function getCheminTemplatesDocsStandards()
    {
        return './ressources/templateDocsCommission/';
    }

    /**
     *
     * Permet de telecharger l'ensemble des documents du destinataire
     */
    public function telechargerDocsDestinataire($sender, $param)
    {
        $idEchange = $param->CommandParameter;
        $connexion = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_WRITE'));

        $Pjs = array();
        $c = new Criteria();
        $echange = Atexo_Message::retrieveMessageById($idEchange, Atexo_CurrentUser::getCurrentOrganism());
        if ($echange instanceof CommonEchange) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $Pjs = $echange->getCommonEchangePieceJointes($c, $connexion);
        }

        $tmpFileDir = Atexo_Config::getParameter('COMMON_TMP') . "/dir_docs_destinataire";
        $tmpZipFile = $tmpFileDir . '.zip';
        if (is_file($tmpZipFile)) {
            system("rm -f " . $tmpZipFile, $sys_answer);
        }

        system(" mkdir $tmpFileDir ; chmod 777 $tmpFileDir ", $sys_answer);
        system(" cd $tmpFileDir; zip -r $tmpZipFile .  > " . Atexo_Config::getParameter('COMMON_TMP') . "zip_error 2>&1", $sys_answer);
        if (is_array($Pjs) && count($Pjs)) {
            $atexoBlob = new Atexo_Blob();
            foreach ($Pjs as $piece) {
                $blobContent = $atexoBlob->return_blob_to_client($piece->getPiece(), Atexo_CurrentUser::getCurrentOrganism());
                $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . "/" . $piece->getPiece() . $piece->getNomFichier();
                $fp = fopen($tmpFile, "w");
                fwrite($fp, $blobContent);
                fclose($fp);

                (new Atexo_Zip())->addFileToZip($tmpFile, $tmpZipFile);

            }
        }
        //Telechargement du document e la volee
        DownloadFile::downloadFileContent(prado::localize(DEFINE_DOCUMENTS_ENVOYES) . '.zip', file_get_contents($tmpZipFile));
    }

    /**
     * Permet de determiner le modele de document specifique
     * @param  $typeCommission
     * @return $modele: le chemin du modele specifique
     */
    public function getModeleDocSpecifique($typeCommission)
    {
        switch ($typeCommission) {
            case 1 :
                $modele = self::getCheminTemplatesDocs() . 'PvCao_';
                break;
            case 2 :
                $modele = self::getCheminTemplatesDocs() . 'PvCtdo_';
                break;
            case 3 :
                $modele = self::getCheminTemplatesDocs() . 'PvCaoGag_';
                break;
            default :
                $modele = '';
                break;
        }
        return $modele;
    }

    /**
     * Permet de determiner le nom du document specifique genere
     * @param  $typeCommission
     * @return $nomFichier: nom du fichier specifique genere
     */
    public function getNomFichierSpecifique($typeCommission)
    {
        switch ($typeCommission) {
            case 1 :
                $nomFichier = prado::localize(DEFINE_PV_COMMISSION_APPEL_OFFRE);
                break;
            case 2 :
                $nomFichier = prado::localize(DEFINE_PV_COMMISSION_TECHNIQUE_DEPOUILLEMENT_OFFRES);
                break;
            case 3 :
                $nomFichier = prado::localize(DEFINE_PV_COMMISSION_APPEL_OFFRE_GRE_A_GRE);
                break;
            default :
                $nomFichier = '';
                break;
        }
        return $nomFichier;
    }
}