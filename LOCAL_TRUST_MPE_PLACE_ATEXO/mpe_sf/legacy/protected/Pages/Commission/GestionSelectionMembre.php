<?php
namespace Application\Pages\Commission;

use Application\Propel\Mpe\CommonTCAOCommissionPeer;
use Prado\Web\UI\TPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExternePeer;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;

/**
 * Formulaire de cretion et modification des intervenents externes
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class GestionSelectionMembre extends MpeTPage {
	
	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
    public function onLoad($param)
    { 
    	if(!$this->isPostBack){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
			$org = Atexo_CurrentUser::getCurrentOrganism();
			$commission = CommonTCAOCommissionPeer::retrieveByPK($idCommission, $org, $connexion);
    		$this->commissionSummary->setDataSource($commission);
	    	$this->fillDataIntervenants($idCommission, $org, $connexion);
	    	$this->fillDataAgents($idCommission, $org, $connexion);
    	}
    }
    
    public function supprimer($id, $type=false){
    	$org = Atexo_CurrentUser::getCurrentOrganism();
    	$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
    	if($type == "true"){
    		Atexo_Commission_IntervenantExterne::deleteIntervenantCommission($id, $idCommission, $org);
    		$this->fillRepeaterAgent($idCommission, $org);
    	}else{
            Atexo_Commission_Agent::deleteAgentCommission($id, $idCommission, $org);
    		$this->fillRepeaterAgent($idCommission, $org);
    	}
    }
    
    public function fillDataIntervenants($idCommission, $org, $connexion=null){
    	$listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsCommission($idCommission, $org, $connexion);
 		$c = new Criteria();
 		$c->add(CommonTCAOCommissionIntervenantExternePeer::ORGANISME, $org);
 		$c->add(CommonTCAOCommissionIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $listIds, CRITERIA::IN);
 		$c->addAscendingOrderByColumn(CommonTCAOCommissionIntervenantExternePeer::ORGANISME);
    	$this->listeIntervenant->fillRepeaterWithDataForSearchResult($c);
    }
    
	public function fillDataAgents($idCommission, $org, $connexion=null){
		$listIds = Atexo_Commission_Agent::getAllIdsAgentsCommission($idCommission, $org, $connexion);
		$c = new Criteria();
 		$c->add(CommonAgentPeer::ORGANISME, $org);
 		$c->add(CommonAgentPeer::ID, $listIds, CRITERIA::IN);
 		$c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
    	$this->listeAgent->fillRepeaterWithDataForSearchResult($c);
    }
    
    public function refreshRepeterIntervenants(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
    	$this->fillDataIntervenants($idCommission, $org);
    }
 	public function refreshRepeterAgents(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
    	$this->fillDataAgents($idCommission, $org);
    }
    /**
     * Permet de charger la liste de tous les agents
     */
    public function fillAllAgents()
    {
    	self::refreshRepeterAgents();
    	self::refreshRepeterIntervenants();	
    }
    /**
     * 
     * Permet de charger les tableaux des agents et des intervenants
     * @param int $idCommission: l'identifiant technique de la Commission
     * @param varchar $org : l'organisme de la Commission
     */
	public function fillRepeaterAgent($idCommission, $org)
    {
    	$this->fillDataIntervenants($idCommission, $org);	
    	$this->fillDataAgents($idCommission, $org);
    }
}