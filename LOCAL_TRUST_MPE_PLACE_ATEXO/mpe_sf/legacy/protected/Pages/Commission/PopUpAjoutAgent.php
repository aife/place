<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Service\Atexo\Atexo_Config;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Library\Propel\Query\Criteria;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;

/**
 * Ajout des intervenents externes Commission
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class PopUpAjoutAgent extends MpeTPage {
	
	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue("agent");
    }
    
	public function onLoad($param)
    { 
		if(!$this->isPostBack){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
			$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idODJ']));
			$org = Atexo_CurrentUser::getCurrentOrganism();
    		if(isset($_GET['idCom'])){
    	    	$this->fillDataAgentsCommission($idCommission, $org, $connexion);
        	}elseif(isset($_GET['idODJ'])){
        	     $this->fillDataAgentsOrdreDuJour($idOrdreJour, $org, $connexion);
        	}
		}
		
    }
    
  
    public function valider(){
    	if(isset($_GET['idCom'])){
    	    self::validerAgentsCommission();
    	}elseif(isset($_GET['idODJ'])){
    	     self::validerAgentsOrdreDuJour();
    	}
    }
    
 public function fillDataAgentsCommission($idCommission, $org, $connexion){
    	$listIds = Atexo_Commission_Agent::getAllIdsAgentsCommission($idCommission, $org, $connexion);
 		$c = new Criteria();
 		
 		$c->add(CommonAgentPeer::ORGANISME, $org);;
 		if(is_array($listIds) && count($listIds) > 0){
	 		$query = "id not in (" . implode(",", $listIds) . ")" ;
	 		$c->add(CommonAgentPeer::ID, $query, Criteria::CUSTOM);
 		}
 		$c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
    	$this->listeAgents->fillRepeaterWithDataForSearchResult($c);
    }
    
    public function validerAgentsCommission(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
	    
		$data = $this->listeAgents->getIntervenantSelectionnes();
		
    	foreach($data as $d){
    		$id = $d ["ID"];
 			$idTypeVoix = $d["TYPE_VOIX"];
 			
 			$agentCom = Atexo_Commission_Agent::getCommissionAgent($id, $idCommission, $org, $connexion);
 			if(!($agentCom instanceOf CommonTCAOCommissionAgent)){
 				$agentCom = new CommonTCAOCommissionAgent();
 			}
 			
 			$agentCom->setOrganisme($org);
 			$agentCom->setIdCommission($idCommission);
 			$agentCom->setIdAgent($id);
 			$agentCom->setTypeVoix($idTypeVoix);
 			
 			$agentCom->save($connexion);
    	}
    	
    	echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refrechAgent').click();window.close();</script>";
    }
    
    public function fillDataAgentsOrdreDuJour($idOrdreJour, $org, $connexion){
    	$listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsOrdreDuJour($idOrdreJour, $org, $connexion, null, true);
 		$c = new Criteria();
 		
 		$c->add(CommonAgentPeer::ORGANISME, $org);;
 		if(is_array($listIds) && count($listIds) > 0){
	 		$query = "id not in (" . implode(",", $listIds) . ")" ;
	 		$c->add(CommonAgentPeer::ID, $query, Criteria::CUSTOM);
 		}
 		$c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
    	$this->listeAgents->fillRepeaterWithDataForSearchResult($c);
    }
    
    public function validerAgentsOrdreDuJour(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idODJ']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
	    
		$data = $this->listeAgents->getIntervenantSelectionnes();
		
    	foreach($data as $d){
    		$id = $d ["ID"];
    		$idTypeVoix = $d["TYPE_VOIX"];
 			/*$intervenant = Atexo_Commission_IntervenantExterne::getIntervenantOrdreJour($id, $idOrdreJour, $org, $connexion,true);
 			if(!($intervenant instanceOf CommonIntervenantOrdreDuJour)){
 				$intervenant = new CommonIntervenantOrdreDuJour();
 			}
 			
 			$intervenant->setOrganisme($org);
 			$intervenant->setIdOrdreDuJour($idOrdreJour);
 			$intervenant->setIdAgent($id);    
 			$intervenant->setTypeVoix($idTypeVoix);
 			
 			$intervenant->save($connexion);*/
    	}
    	
    	echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refrechAgent').click();window.close();</script>";
    }
}