<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_CriteriaVo;
use Application\Service\Atexo\Commission\Atexo_Commission_Commissions;
use Application\Pages\Commun\Classes\Util;
use Prado\Prado;
use Prado\Web\UI\WebControls\TContent;

/**
 * Formulaire de creation et modification des commissions
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class ListeCommissions extends MpeTPage {
    
    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
    public function onLoad($param)
    {
    	//Ecriture du service metier en session
    	if(Atexo_CurrentUser::readFromSession('ServiceMetier') != 'cao') {
    		Atexo_CurrentUser::writeToSession('ServiceMetier', 'cao');
    	}
        $criteriaVo = new Atexo_Commission_CriteriaVo();
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
    	$this->getCommissions($criteriaVo);     
    }
    
	public function getCommissions($criteriaVo)
    {
		$criteriaVo->setCommandeName($this->getViewState('commandeName'));
		$criteriaVo->setSensTri($this->getViewState('sensTri'));
		//Criteres de pagination
		$offset = $this->tableauListeCommissions->CurrentPageIndex * $this->tableauListeCommissions->PageSize;
        $limit = $this->tableauListeCommissions->PageSize;
        $criteriaVo->setLimit($limit);
		$criteriaVo->setOffset($offset);
    	
		$this->setViewState("CriteriaVo", $criteriaVo);
	    $nombreElementTotal= (new Atexo_Commission_Commissions())->getCommissions($criteriaVo, true);
        $listeCommissions = array();
        if($nombreElementTotal >= 1) {
	        $listeCommissions = (new Atexo_Commission_Commissions())->getCommissions($criteriaVo, false);
	        
            $this->panelMoreThanOneElementFound->setStyle('display:block');
            $this->panelNoElementFound->setStyle('display:none');
            $this->nombreElement->Text=$nombreElementTotal;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->panelBouttonGotoPageTop->setVisible(true);
            $this->panelBouttonGotoPageBottom->setVisible(true);
//            $this->panelMessageErreur->setVisible(false);
            self::afficherElementspager();
            $this->setViewState("nombreElement",$nombreElementTotal);
            $this->setViewState("listeCommissions",$listeCommissions);
            $this->nombrePageTop->Text=ceil($nombreElementTotal/$this->tableauListeCommissions->PageSize);
            $this->nombrePageBottom->Text=ceil($nombreElementTotal/$this->tableauListeCommissions->PageSize);
            $this->tableauListeCommissions->setVirtualItemCount($nombreElementTotal);
            $this->tableauListeCommissions->dataSource = $listeCommissions;
            $this->tableauListeCommissions->dataBind();
        } else {
            $this->panelMoreThanOneElementFound->setStyle('display:none');
            $this->panelNoElementFound->setStyle('display:block');
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            self::masquerElementspager();
            $this->panelBouttonGotoPageTop->setVisible(false);
            $this->panelBouttonGotoPageBottom->setVisible(false);
//            $this->panelMessageErreur->setVisible(false);
            $this->tableauListeCommissions->dataSource = array();
            $this->tableauListeCommissions->dataBind();
        }
        $this->setViewState("criteres",$criteriaVo);
    }
    
    public function populateData()
    {
        $offset = $this->tableauListeCommissions->CurrentPageIndex * $this->tableauListeCommissions->PageSize;
        $limit = $this->tableauListeCommissions->PageSize;
        if ($offset + $limit > $this->tableauListeCommissions->getVirtualItemCount()) {
            $limit = $this->tableauListeCommissions->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet',$offset);
        $this->setViewState('limit',$limit);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $this->getCommissions($criteriaVo);
    }
 	
    public function pageChanged($sender,$param)
    {
        $this->tableauListeCommissions->CurrentPageIndex =$param->NewPageIndex;
        $this->numPageTop->Text=$param->NewPageIndex+1;
        $this->numPageBottom->Text=$param->NewPageIndex+1;
        $this->populateData();
    }

     public function goToPage($sender,$param)
  {
      switch ($sender->ID) {
          case "DefaultButtonTop" :    $numPage=$this->numPageTop->Text;
          break;
          case "DefaultButtonBottom" : $numPage=$this->numPageBottom->Text;
          break;
      }
      if (Util::isEntier($numPage)) {
          if ($numPage>=$this->nombrePageTop->Text) {
              $numPage=$this->nombrePageTop->Text;
          } else if ($numPage<=0) {
              $numPage=1;
          }
          $this->tableauListeCommissions->CurrentPageIndex =$numPage-1;
          $this->numPageBottom->Text=$numPage;
          $this->numPageTop->Text=$numPage;
          $this->populateData();
      } else {
          $this->numPageTop->Text=$this->tableauListeCommissions->CurrentPageIndex+1;
          $this->numPageBottom->Text=$this->tableauListeCommissions->CurrentPageIndex+1;
      }
  }
  public function changePagerLenght($sender,$param)
  {
      switch ($sender->ID) {
          case "listePageSizeTop" : $pageSize=$this->listePageSizeTop->getSelectedValue();
          $this->listePageSizeBottom->setSelectedValue($pageSize);
          break;
          case "listePageSizeBottom" : $pageSize=$this->listePageSizeBottom->getSelectedValue();
          $this->listePageSizeTop->setSelectedValue($pageSize);
          break;
      }
      // echo $pageSize;exit;
      $this->tableauListeCommissions->PageSize=$pageSize;
      $nombreElement=$this->getViewState("nombreElement");
      $this->nombrePageTop->Text=ceil($nombreElement/$this->tableauListeCommissions->PageSize);
      $this->nombrePageBottom->Text=ceil($nombreElement/$this->tableauListeCommissions->PageSize);
      $this->tableauListeCommissions->setCurrentPageIndex(0);
      $this->populateData();
  }
    public function afficherElementspager()
    {
    	$this->listePageSizeBottom->visible = true;
    	$this->texteAfficher->visible = true;
    	$this->texteResultParPage->visible = true;
    	$this->texteResultParPage->visible = true;
    	 
    }
    
	public function masquerElementspager()
    {
    	$this->listePageSizeBottom->visible = false;
    	$this->texteAfficher->visible = false;
    	$this->texteResultParPage->visible = false;
    	$this->texteResultParPage->visible = false;
    	 
    }
    
	public function critereTri($sender, $param)
 	{
 		$tri=$param->CommandName;
		$sensTriDefaut=$this->getViewState('sensTriDefaut','ASC');
		
 		$sensTri=$this->getViewState('sensTri');
 		
 		if($tri) {
 			if($sensTri=='ASC') {
	 			$sensTri='DESC';
 			} else {
 				$sensTri='ASC';
 			}
 		} else {
 			if($sensTriDefaut=='ASC') {
 				$sensTriDefaut='DESC';
	 		} else {
	 			$sensTriDefaut='ASC';
	 		}
	 		$sensTri='';
 		}	
 		
 		$this->setViewState('sensTri',$sensTri);
 		$this->setViewState('sensTriDefaut',$sensTriDefaut);
 		$this->setViewState('tri',$tri);
 		$this->setViewState('commandeName',$param->CommandName);
 	}
 	
	public function Trier($sender, $param)
 	{
	 	$this->populateData();
		$this->panelTableauListeCommissions->render($param->NewWriter);
 	}
  
 	
 	/**
 	 * Permet de supprimer une Commission
 	 */
 	public function supprimerCommission($sender, $param){
 	
		$idCommission = $param->CommandParameter;	
		$org     = Atexo_CurrentUser::getCurrentOrganism();
		
		//Verifier d'abord si la Commission n'a pas des seances qui lui sont rattachees
		if(!(new Atexo_Commission_Commissions())->hasDependantSeances($idCommission,$org)){
			//Suppression de la Commission
			(new Atexo_Commission_Commissions())->supprimer($idCommission,$org);
			//Actualisation de la liste des commissions
			$this->populateData();
			
		} else {
//			$this->panelMessageErreur->setVisible(true);
//			$this->panelMessageErreur->setMessage(Prado::localize(TEXT_SUPPRESSION_COMMISSION_IMPOSSIBLE));
		}
 	}

	  
}