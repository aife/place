<?php

namespace Application\Pages\Commission;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Controls\MpeTPage;

/**
 * Classe d'ajout d'un nouvel ordre du jour par recherche de la consultation
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class AjoutOrdreDuJourRechercheConsultation extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function checkIfAgent(){
        return Atexo_CurrentUser::isAgent();
    }
    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteriaVo->setTypeSearch("tout");
            $this->rechercheAvancee->setVisible(true);
            $this->TableauDeBord->setVisible(false);
        }
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord
     * @param : $criteriaVo : objet contenant les criteres de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $this->rechercheAvancee->setVisible(false);
        $this->TableauDeBord->setVisible(true);
        $this->ResultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    /**
     * Fonction qui permet de changer d'onglet et donc le contenu du tableau de bord.
     *
     */
    public function changeActiveTabAndDataForRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState("CriteriaVo", new Atexo_Consultation_CriteriaVo());
        $criteriaVo->setOffset(0);
        $criteriaVo->setLimit(0);
        $senderName = $sender->ID;
        switch ($senderName) {
            case "firstTab" :
                $this->firstDiv->setCssClass('tab-on');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE');
                break;
            case "firstBisTab" :
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab-on');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_ELABORATION');
                break;
            case "secondTab" :
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab-on');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_PREPARATION');
                break;
            case "thirdTab" :
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab-on');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_CONSULTATION');
                break;
            case "fourthTab" :
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab-on');
                $this->fifthDiv->setCssClass('tab');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
                break;
            case "fifthTab" :
                $this->firstDiv->setCssClass('tab');
                $this->firstBisDiv->setCssClass('tab');
                $this->secondDiv->setCssClass('tab');
                $this->thirdDiv->setCssClass('tab');
                $this->fourthDiv->setCssClass('tab');
                $this->fifthDiv->setCssClass('tab-on');
                $nouveauStatutConsultation = Atexo_Config::getParameter('STATUS_DECISION');
                break;
            default :
                $nouveauStatutConsultation = '';
                break;
        }

        $criteriaVo->setSecondConditionOnEtatConsultation($nouveauStatutConsultation);
        $this->setViewState("CriteriaVo", $criteriaVo);
        $this->dataForSearchResult($criteriaVo);
    }

    /**
     * Fonctione de tri du tableau de bord
     */
    public function Trier($sender, $param)
    {
        $this->ResultSearch->Trier($param->CommandName);
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    public function styleNavigationOnglet()
    {
        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
            return "tab-6item";
        } else {
            return "tab-5item";
        }
    }
}