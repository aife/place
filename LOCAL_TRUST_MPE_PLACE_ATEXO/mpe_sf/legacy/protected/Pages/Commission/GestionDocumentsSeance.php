<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\Classes\Util;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeancePeer;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_GenerationDocumentsCAO;
use AtexoPdf\RtfGeneratorClient;
use Prado\Prado;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Blob;
/**
 * Classe de gestion des documents de la seance de Commission
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class GestionDocumentsSeance extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $this->messageErreur->visible = false;
            if (isset($_GET['id'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                $org = Atexo_CurrentUser::getCurrentOrganism();
                $seance = CommonTCAOSeancePeer::retrieveByPK($idSeance, $org, $connexion);

                if ($seance instanceof CommonTCAOSeance) {
                    $this->seanceSummary->setDataSource($seance);
                    $this->fillRepeaterWithDataForSearchResult(new Criteria());
                }
            }
        }
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $nombreElement = self::search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState("nombreElement", $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
            $this->tableauMessage->setVirtualItemCount($nombreElement);
            $this->tableauMessage->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData($criteriaVo)
    {
        $offset = $this->tableauMessage->CurrentPageIndex * $this->tableauMessage->PageSize;
        $limit = $this->tableauMessage->PageSize;
        if ($offset + $limit > $this->tableauMessage->getVirtualItemCount()) {
            $limit = $this->tableauMessage->getVirtualItemCount() - $offset;
        }
        if (!$criteriaVo instanceof Criteria) {
            $criteriaVo = new Criteria();
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayIntervenant = self::search($criteriaVo);
        $this->setViewState("elements", $arrayIntervenant);
        $this->tableauMessage->DataSource = $arrayIntervenant;
        $this->tableauMessage->DataBind();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauMessage->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState("CriteriaVo");
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ($openCloseTag == "open") {
                return "<strong>";
            } else {
                return "</strong>";
            }
        } else {
            return "";
        }
    }

    public function goToPage($sender, $param)
    {
        switch ($sender->ID) {
            case "DefaultButtonTop" :
                $numPage = $this->numPageTop->Text;
                break;
            case "DefaultButtonBottom" :
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } else if ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauMessage->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState("CriteriaVo");
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauMessage->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauMessage->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case "nombreResultatAfficherBottom" :
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case "nombreResultatAfficherTop" :
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauMessage->PageSize = $pageSize;
        $nombreElement = $this->getViewState("nombreElement");
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauMessage->PageSize);
        $criteriaVo = $this->getViewState("CriteriaVo");
        $this->tableauMessage->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function search($c, $nbreElement = false)
    {
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        return (new Atexo_Message())->getAllEchangeCAOSeance($idSeance, $org, $c, $nbreElement);
    }

    public function getDestinataires($destinataires)
    {
        return implode("<br/>", $destinataires);
    }


    /**
     * Renvoie l'acronyme de l'organisme, la seance et la liste des consultations a l'ordre du jour
     * @param null
     * @return (string|CommonTCAOSeance|CommonTCAOOrdreDePassage[]) l'acronyme de l'organisme, la seance et la liste des consultations a l'ordre du jour
     */
    public function getOrdreDuJour()
    {
        $connexion = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_ONLY'));
        $id = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $primary_key = array($id, $organisme);
        $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
        $_ordre_du_jour = (new Atexo_Commission_Seance())->getOrdreDuJour($id, $organisme);

        return array($organisme, $seance, $_ordre_du_jour);
    }


    /**
     * Affiche une fenetre de telechargement du fichier Excel listant les consultations a l'ordre du jour de la seance de Commission
     * @return void
     */
    public function telechargerOrdreDuJour($sender, $param)
    {
        list($organisme, $seance, $_ordre_du_jour) = self::getOrdreDuJour();

        //Verifier si l'ordre du jour est non vide
        if (empty($_ordre_du_jour) || !count($_ordre_du_jour)) {
            $this->populateData($this->getViewState("CriteriaVo"));
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_LIGNE_ORDRE_DU_JOUR'));
            return;
        }

        (new Atexo_GenerationExcel())->telechargerOrdreDuJour($seance, $_ordre_du_jour, $organisme);
    }

    /**
     * Affiche une fenetre de telechargement du fichier ZIP contenant l'ensemble des PV individuels generes pour les passages en Commission de consultations
     * @return void
     * @throws void si une erreur systeme apparait lors de l'ecriture d'un PV individuel au format RTF
     * @throws void si le fichier ZIP contenant l'ensemble des PV individuels generes pour les passages en Commission de consultations
     */
    public function telechargerPvCao($sender, $param)
    {
        //Recuperation des parametres d'interet
        list($organisme, $seance, $_ordre_du_jour) = self::getOrdreDuJour();
        $dateSeance = new \DateTime($seance->getDate());
        $dateSeance = $dateSeance->format('Y-m-d_H-i');
        $commission = $seance->getCommonTCAOCommission();
        $acroCommission = $commission->getSigle();

        $langue = Atexo_Languages::readLanguageFromSession();

        //Recuperation du modele de document
        $modele = self::getModeleDocument('CAO_MODELE_PV_CONSULTATION', $langue);

        //Creation du repertoire compresse
        list ($tmp_dir, $zip_file) = self::getZipDirectory('CAO_REP_PV');

        //Verifier si l'ordre du jour est non vide
        if (empty($_ordre_du_jour) || !count($_ordre_du_jour)) {
            $this->populateData($this->getViewState("CriteriaVo"));
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_ORDRE_DU_JOUR_TELECHARGER_PV'));
            return;
        }

        foreach ($_ordre_du_jour as $ordreDePassage) {
            //Recuperation des informations sur le marche
            $commission_consultation = $ordreDePassage->getCommonTCAOCommissionConsultation();

            if ($commission_consultation->getRefConsultation()) {
                $reference = $commission_consultation->getRefConsultation();
            } else {
                $reference = $commission_consultation->getRefLibre();
            }

            //Generation document odt
            $odt_file = (new Atexo_GenerationFichier_GenerationDocumentsCAO())->generationPVConsultation($seance, $tmp_dir, $modele, true, $ordreDePassage, true);

            //Generation document rtf
            $rtf_content = (new RtfGeneratorClient())->genererRtf($odt_file);
            $file_name = Atexo_Config::getParameter('CAO_NOM_FICHIER_PV_CONSULTATION') . '_' . $dateSeance . '_' . $acroCommission . '_';
            $file_name .= Atexo_Util::clean(Atexo_Util::OterAccents($reference));
            $tmp_file = $tmp_dir . '/' . $file_name . Atexo_Config::getParameter('CAO_EXTENSION_RTF');

            try {
                //Ecriture du fichier
                $handle = fopen($tmp_file, "w+");
                fwrite($handle, $rtf_content);
                fclose($handle);

                //Ajout du fichier a l'archive ZIP
                Atexo_Zip::addFile2Zip($zip_file, $tmp_file, $file_name . Atexo_Config::getParameter('CAO_EXTENSION_RTF'), 'CREATE');

            } catch (\Exception $e) {
                Prado::log("Erreur dans la generation des pv individuels au format RTF : " . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, "atexo.Commission/protected/Pages/GestionDocumentsSeance.php");
                exit;
            }
        }

        //Envoi de l'archive
        try {
            //Telechargement du document e la volee

            $zip_file_name = Atexo_Config::getParameter('CAO_NOM_REP_PV_CONSULTATION') . '_' . $dateSeance . '_' . $acroCommission . Atexo_Config::getParameter('CAO_EXTENSION_ZIP');
            DownloadFile::downloadFileContent($zip_file_name, file_get_contents($zip_file));
        } catch (\Exception $e) {
            Prado::log("Erreur lors de l'envoi de l'archive : " . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, "atexo.Commission/protected/Pages/GestionDocumentsSeance.php");
            exit;
        }

    }

    /**
     * Affiche une fenetre de telechargement du fichier ZIP contenant l'ensemble des convocations generees
     * @return void
     * @throws void si une erreur systeme apparait lors de l'ecriture d'une convocation individuelle au format RTF
     * @throws void si le fichier ZIP contenant l'ensemble des convocations individuelles au format RTF ne peut etre envoye
     */
    public function telechargerConvocations($sender, $param)
    {
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $langue = Atexo_Languages::readLanguageFromSession();

        //Recuperation de l'ordre du jour
        $_ordre_du_jour = (new Atexo_Commission_Seance())->getOrdreDuJour($idSeance, $organisme);
        $dataOrdreDuJour = $_ordre_du_jour->getData();
        if (empty($dataOrdreDuJour)) {
            $this->populateData($this->getViewState("CriteriaVo"));
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_MESSAGE_ERREUR_AUCUN_ORDRE_DU_J0UR_TELECHARGER_ENSEMBLE_CONVOCATION'));
            return;
        }

        //Recuperation des parametres d'interet
        list($organisme, $seance, $_ordre_du_jour) = self::getOrdreDuJour();
        $commission = $seance->getCommonTCAOCommission();
        $acroCommission = $commission->getSigle();
        $dateSeance = new \DateTime($seance->getDate());
        $dateSeance = $dateSeance->format('Y-m-d_H-i');

        //Creation du repertoire compresse
        list ($tmp_dir, $zip_file) = self::getZipDirectory('CAO_REP_CONVOCATIONS');

        //Recuperation du modele de document
        $modele = self::getModeleDocument('CAO_MODELE_CONVOCATION', $langue);

        //Recuperation des membres permanents de la seance
        $_t_permanents = (new Atexo_Commission_Seance())->getAllPermanentsSeance($idSeance, $organisme);

        //Initialisation variable comptant le nombre de fichiers generes
        $nb_file = 0;

        foreach ($_ordre_du_jour as $ordreDePassage) {
            //Recuperation des membres ponctuels (invites a un ordre du jour)
            $_t_invites = (new Atexo_Commission_Seance())->getAllInvitesOrdreDePassage($ordreDePassage->getIdOrdreDePassage(), $organisme);

            //Liste de tous les membres de la procedure a l'ordre du jour
            $_t_membres = array_merge((array)$_t_permanents, (array)$_t_invites);

            if (is_array($_t_membres) && count($_t_membres)) {
                foreach ($_t_membres as $invite) {
                    //Generation document odt
                    $odt_file = (new Atexo_GenerationFichier_GenerationDocumentsCAO())->generationConvocSeance($invite, $tmp_dir, $modele, true, $langue, true);

                    //Generation document rtf
                    $rtf_content = (new RtfGeneratorClient())->genererRtf($odt_file);
                    $file_name = Atexo_Config::getParameter('CAO_NOM_FICHIER_CONVOCATION') . '_' . $dateSeance . '_' . $acroCommission . '_';
                    $file_name .= Atexo_Util::clean(Atexo_Util::OterAccents($invite->getNom() . '_' . $invite->getPrenom()));
                    $tmp_file = $tmp_dir . '/' . $file_name . Atexo_Config::getParameter('CAO_EXTENSION_RTF');

                    try {
                        //Ecriture du fichier
                        $handle = fopen($tmp_file, "w+");
                        fwrite($handle, $rtf_content);
                        fclose($handle);

                        //Ajout du fichier a l'archive ZIP
                        (new Atexo_Zip())->addFileToZip($tmp_file, $zip_file);
                        $nb_file++;

                    } catch (\Exception $e) {
                        Prado::log("Erreur dans la generation des convocations au format RTF : " . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, "atexo.Commission/protected/Pages/GestionDocumentsSeance.php");
                        exit;
                    }
                }
            }
        }

        //Verifier si le zip contient au moins 1 element
        if (!$nb_file) {
            $this->populateData($this->getViewState("CriteriaVo"));
            $this->messageErreur->visible = true;
            $this->messageErreur->setMessage(prado::localize('DEFINE_AJOUTER_INVITES_TELECHARGER_ENSEMBLE_CONVOCATION'));
            return;
        }

        //Envoi de l'archive
        try {
            $zip_file_name = Atexo_Config::getParameter('CAO_NOM_REP_CONVOCATIONS') . '_' . $dateSeance . '_' . $acroCommission . Atexo_Config::getParameter('CAO_EXTENSION_ZIP');
            DownloadFile::downloadFileContent($zip_file_name, file_get_contents($zip_file));
        } catch (\Exception $e) {
            Prado::log("Erreur lors de l'envoi de l'archive : " . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, "atexo.Commission/protected/Pages/GestionDocumentsSeance.php");
            exit;
        }
    }

    /**
     * Retourne le chemin du modele de document correspondant au type et a la langue indiques
     * @param string $type_modele nom du type du modele de document
     * @param string $langue abbreviation de la langue pour laquelle rechercher un modele de document
     * @return string le chemin du modele de document correspondant au type et a la langue indiques
     */
    public function getModeleDocument($type_modele, $langue = '')
    {
        //Recuperation des parametres d'interet
        $directory = Atexo_Config::getParameter('CAO_REP_TEMPLATES');
        //Creation du repertoire si il n'existe pas
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }
        $file = Atexo_Config::getParameter($type_modele);
        $ext = Atexo_Config::getParameter('CAO_EXTENSION_ODT');

        $lang_file = $directory . $file . '_' . $langue . $ext;
        $default_file = $directory . $file . $ext;

        if (is_file($lang_file)) {
            return $lang_file;
        } else {
            return $default_file;
        }
    }


    /**
     * Retourne le nom du repertoire temporaire cree et le nom du fichier zip correspondant pour generer des documents
     * @param string $type_dir nom du type de repertoire a creer selon le type de document gere
     * @return (string|string)  le nom du repertoire temporaire cree et le nom du fichier zip correspondant pour generer des documents
     * @throws void si une erreur systeme apparait lors de la creation du repertoire temporaire ou de la suppression du zip correspondant
     */
    public function getZipDirectory($type_dir)
    {
        try {
            //Creation du repertoire temporaire pour les convocations
            $directory = Atexo_Config::getParameter($type_dir) . uniqid();
            $zip_file = $directory . '.zip';

            //Suppression du fichier ZIP si il existe deja
            if (is_file($zip_file)) {
                unlink($zip_file);
            }

            //Suppresion du repertoire si il existe deja
            if (is_dir($directory)) {
                rmdir($directory);
            }
            //Creation du repertoire avec toute l'arborescence si elle n'existe pas
            mkdir($directory, 0755, true);

            return array($directory, $zip_file);

        } catch (\Exception $e) {
            Prado::log("Erreur dans la generation du repertoire " . $type_dir . " : " . $e->getMessage() . $e->getTraceAsString(), TLogger::ERROR, "atexo.Commission/protected/Pages/GestionDocumentsSeance.php");
            exit;
        }
    }

    /**
     *
     * Permet de telecharger l'ensemble des documents du destinataire
     */
    public function telechargerDocsDestinataire($sender, $param)
    {
        $idEchange = $param->CommandParameter;
        $connexion = Propel:: getConnection(Atexo_Config:: getParameter('COMMON_DB') . Atexo_Config:: getParameter('CONST_READ_WRITE'));

        $Pjs = array();
        $c = new Criteria();
        $echange = Atexo_Message::retrieveMessageById($idEchange, Atexo_CurrentUser::getCurrentOrganism());
        if ($echange instanceof CommonEchange) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $Pjs = $echange->getCommonEchangePieceJointes($c, $connexion);
        }

        $tmpFileDir = Atexo_Config::getParameter('COMMON_TMP') . "/dir_docs_destinataire";
        $tmpZipFile = $tmpFileDir . '.zip';
        if (is_file($tmpZipFile)) {
            system("rm -f " . $tmpZipFile, $sys_answer);
        }

        system(" mkdir $tmpFileDir ; chmod 777 $tmpFileDir ", $sys_answer);
        system(" cd $tmpFileDir; zip -r $tmpZipFile .  > " . Atexo_Config::getParameter('COMMON_TMP') . "zip_error 2>&1", $sys_answer);
        if (is_array($Pjs) && count($Pjs)) {
            $atexoBlob = new Atexo_Blob();
            foreach ($Pjs as $piece) {
                $blobContent = $atexoBlob->return_blob_to_client($piece->getPiece(), Atexo_CurrentUser::getCurrentOrganism());
                $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . "/" . $piece->getPiece() . $piece->getNomFichier();
                $fp = fopen($tmpFile, "w");
                fwrite($fp, $blobContent);
                fclose($fp);
                (new Atexo_Zip())->addFileToZip($tmpFile, $tmpZipFile);

            }
        }
        //Telechargement du document e la volee
// 	    $dateSeance = new DateTime($seance->getDate());
// 	    $dateSeance = $dateSeance->format('Y-m-d_H-i');
// 	    $Commission = $seance->getCommonTCAOCommission();
// 		$acroCommission = $Commission->getSigle();
// 	    $zip_file_name = prado::localize(DEFINE_DOCUMENTS_ENVOYES).'_'.$dateSeance.'_'.$acroCommission.'_'.Atexo_Config::getParameter('CAO_EXTENSION_ZIP');
        $zip_file_name = prado::localize(DEFINE_DOCUMENTS_ENVOYES) . Atexo_Config::getParameter('CAO_EXTENSION_ZIP');
        DownloadFile::downloadFileContent($zip_file_name, file_get_contents($tmpZipFile));
    }

}