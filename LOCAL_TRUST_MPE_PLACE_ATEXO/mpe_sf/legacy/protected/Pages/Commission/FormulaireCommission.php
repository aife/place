<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonTCAOCommission;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOCommissionPeer;
use Application\Propel\Mpe\CommonTCAOCommissionQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado\Prado;
use Application\Controls\MpeTPage;

/**
 * Formulaire de creation et modification des commissions
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class FormulaireCommission extends MpeTPage
{

    private $idAgent;
    private $idIntExterne;

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getCurrentOrganism();

            if ($idCommission) {
                $commission = CommonTCAOCommissionPeer::retrieveByPK($idCommission, $org, $connexion);

                $this->sigleCommission->Text = Atexo_Util::atexoHtmlEntitiesDecode($commission->getSigle());
                $this->intituleCommission->Text = Atexo_Util::atexoHtmlEntitiesDecode($commission->getIntitule());
            }

            $this->fillDataAgents($idCommission, $org);
            $this->fillDataIntervenants($idCommission, $org);

            $this->setViewState('idCom', $idCommission);
        }
    }

    /**
     * Permet de sauvegarder les commissions
     *
     * @param $sender
     * @param $param
     */
    public function save($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $commission = CommonTCAOCommissionQuery::create()
                ->findPk(array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), $org));
        } else {
            $commission = new CommonTCAOCommission();
        }


        $commission->setOrganisme($org);
        $commission->setSigle(Atexo_Util::atexoHtmlEntities($this->sigleCommission->Text));
        $commission->setIntitule(Atexo_Util::atexoHtmlEntities($this->intituleCommission->Text));

        $commission->save();

        if (isset($_GET['id'])) {
            self::saveListeAgents($commission->getIdCommission(), $commission->getOrganisme());
            self::saveListeIntervenants($commission->getIdCommission(), $commission->getOrganisme());

            self::goToPageListeCommissions($sender, $param);
        } else {
            $idCommission = base64_encode($commission->getIdCommission());
            self::refresh($idCommission);
        }

    }


    public function saveListeAgents($idCommission, $org)
    {

        $data = $this->listeAgents->getAgents();

        foreach ($data as $item) {

            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $agent = (new Atexo_Commission_Agent())->getCommissionAgent($id, $idCommission, $org);
            if ($agent instanceof CommonTCAOCommissionAgent) {
                $agent->setOrganisme($org);
                $agent->setIdCommission($idCommission);
                $agent->setIdAgent($id);
                $agent->setIdRefValTypeVoixDefaut($idTypeVoix);

                $agent->save();
            }
        }
    }

    public function saveListeIntervenants($idCommission, $org)
    {

        $data = $this->listeIntervenants->getIntervenantsExternes();

        foreach ($data as $item) {

            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $intervenant = (new Atexo_Commission_IntervenantExterne())->getCommissionIntervenantExterne($id, $idCommission, $org, $connexion);
            if (!($intervenant instanceof CommonTCAOCommissionIntervenantExterne)) {
                $intervenant = new CommonTCAOCommissionIntervenantExterne();
            }

            $intervenant->setOrganisme($org);
            $intervenant->setIdCommission($idCommission);
            $intervenant->setIdIntervenantExterne($id);
            $intervenant->setIdRefValTypeVoixDefaut($idTypeVoix);

            $intervenant->save();
        }

    }

    /**
     * Permet de rediriger vers la page listant les commissions
     */
    public function goToPageListeCommissions()
    {
        $this->response->redirect("?page=Commission.ListeCommissions");
    }

    /**
     * Permet de rafraichir la page actuelle
     */
    public function refresh($idCommission)
    {
        $this->response->redirect("?page=Commission.FormulaireCommission&id=" . $idCommission);
    }

    public function fillDataIntervenants($idCommission, $org, $tri = null, $sensTri = null, $connexion = null)
    {
        $listIds = (new Atexo_Commission_IntervenantExterne())->getAllIdsIntervenantsCommission($idCommission, $org, $connexion);
        $criteria = (new Atexo_Commission_IntervenantExterne())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeIntervenants->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function fillDataAgents($idCommission, $org, $tri = null, $sensTri = null, $connexion = null)
    {
        $listIds = (new Atexo_Commission_Agent())->getAllIdsAgentsCommission($idCommission, $org, $connexion);
        $criteria = (new Atexo_Commission_Agent())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeAgents->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function refreshRepeterIntervenants()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataIntervenants($idCommission, $org);
    }

    public function refreshRepeterAgents($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idCommission = $this->getViewState('idCom');
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataAgents($idCommission, $org);
        $this->listeAgents->render($param->NewWriter);

    }

    /** Permet de charger la liste de tous les agents
     */
    public function fillAllAgents()
    {
        self::refreshRepeterAgents();
        self::refreshRepeterIntervenants();
    }

    /**
     *
     * Permet de charger les tableaux des agents et des intervenants
     * @param int $idSeance : l'identifiant technique de la Commission
     * @param varchar $org : l'organisme de la Commission
     */
    public function fillRepeaterAgent($idCommission, $org)
    {
        $this->fillDataIntervenants($idCommission, $org);
        $this->fillDataAgents($idCommission, $org);
    }

    public function suggestAgent($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idCommission = $this->getViewState('idCom');

        $buffer = (new Atexo_Commission_Agent())->searchAgentsByOrganisme($idCommission, $org, $token);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['service_path'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();
        } else {
            $buffer = array('id' => 0, 'text' => Prado::localize('DEFINE_AUCUN_RESULTAT'));
            $sender->DataSource = array($buffer);
            $sender->dataBind();
        }
    }

    public function selectAgent($sender, $param)
    {
        $this->idAgent = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idAgent', $this->idAgent);
    }

    public function addAgentToCommission($sender, $param)
    {

        //Connexion a la base de donnees
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        //Recuperation de l'ID de l'agent a ajouter
        $idAgent = $this->getViewState('idAgent');

        //Recuperation de l'agent
        $agent = Atexo_Agent::retrieveAgent($idAgent);

        if ($agent instanceof CommonAgent) {

            //Recuperation des parametres d'interet
            $idCommission = $this->getViewState('idCom');
            $org = Atexo_CurrentUser::getCurrentOrganism();

            //Type de voix par defaut
            $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $last_item = array_slice($_t_typeVoix, -1, 1, true);
            $idTypeVoix = key($last_item);

            //Verification de l'existence de cet agent pour cette Commission
            $commission_agent = (new Atexo_Commission_Agent())->getCommissionAgent($idAgent, $idCommission, $org, $connexion);

            if (!($commission_agent instanceof CommonTCAOCommissionAgent)) {
                $commission_agent = new CommonTCAOCommissionAgent();
            }

            //Mise a jour des champs de l'enregistrement
            $commission_agent->setOrganisme($org);
            $commission_agent->setIdCommission($idCommission);
            $commission_agent->setIdAgent($agent->getId());
            $commission_agent->setIdRefValTypeVoixDefaut($idTypeVoix);
            $commission_agent->setTypeCompte($agent->getElu());
            //Sauvegarde de l'enregistrement
            $commission_agent->save($connexion);

        }
    }

    public function addAgentToCommissionCallback()
    {

        //Rechargement de la page pour rafraichir les composants
        $this->response->redirect("?page=Commission.FormulaireCommission&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));

    }

    public function suggestIntExterne($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idCommission = $this->getViewState('idCom');

        $new_buffer = array();
        $buffer = (new Atexo_Commission_IntervenantExterne())->searchIntervenantExterneByOrganisme($idCommission, $org, $token);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['organisation'] . ' - ' . $line['fonction'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();
        } else {

            $line = prado::localize('DEFINE_AUCUN_RESULTAT') . '&nbsp;&gt;&nbsp;';
            $line .= '<a href="?page=Commission.FormulaireIntervenantExterne&creation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&calledFrom=com" >';
            $line .= prado::localize('DEFINE_CREER') . '</a>';

            $buffer = array('id' => 0, 'text' => $line);
            $sender->DataSource = array($buffer);
            $sender->dataBind();
        }
    }

    public function selectIntExterne($sender, $param)
    {
        $this->idIntExterne = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idIntExterne', $this->idIntExterne);
    }

    public function addIntervenantExterneToCommission($sender, $param)
    {
        //Recuperation de l'ID de l'intervenant externe a ajouter
        $idIntExterne = $this->getViewState('idIntExterne');
        //Recuperation des parametres d'interet
        $idCommission = $this->getViewState('idCom');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        (new Atexo_Commission_IntervenantExterne())->addIntervenantToCommission($idIntExterne, $idCommission, $org);
    }

    public function addIntervenantExterneToCommissionCallback()
    {

        //Rechargement de la page pour rafraichir les composants
        $this->response->redirect("?page=Commission.FormulaireCommission&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));

    }

    public function removeIntervenant($id, $intervenant = false)
    {

        //Recuperation des parametres d'interet
        $idCommission = $this->getViewState('idCom');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        if ($intervenant) {
            //On retire l'intervenant externe de la Commission
            (new Atexo_Commission_IntervenantExterne())->deleteIntervenantCommission($id, $idCommission, $org);
            $this->fillRepeaterAgent($idCommission, $org);
        } else {
            //On retire l'agent de la Commission
            (new Atexo_Commission_Agent())->deleteAgentCommission($id, $idCommission, $org);
            $this->fillRepeaterAgent($idCommission, $org);

        }
    }

    public function critereTriAgent($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriAgentDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriAgent');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriAgent', $sensTri);
        $this->setViewState('sensTriAgentDefaut', $sensTriDefaut);
        $this->setViewState('triAgent', $tri);
        $this->setViewState('commandeNameAgent', $param->CommandName);

    }

    public function TrierAgent($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $tri = $this->getViewState('triAgent');
        $sensTri = $this->getViewState('sensTriAgent');
        $idCommission = $this->getViewState('idCom');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataAgents($idCommission, $org, $tri, $sensTri);
        $this->panelTableauListeAgents->render($param->NewWriter);

    }

    public function critereTriInt($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriIntDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriInt');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriInt', $sensTri);
        $this->setViewState('sensTriIntDefaut', $sensTriDefaut);
        $this->setViewState('triInt', $tri);
        $this->setViewState('commandeNameInt', $param->CommandName);

    }

    public function TrierInt($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $tri = $this->getViewState('triInt');
        $sensTri = $this->getViewState('sensTriInt');
        $idCommission = $this->getViewState('idCom');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataIntervenants($idCommission, $org, $tri, $sensTri);
        $this->panelTableauListeIntervenants->render($param->NewWriter);

    }
}
