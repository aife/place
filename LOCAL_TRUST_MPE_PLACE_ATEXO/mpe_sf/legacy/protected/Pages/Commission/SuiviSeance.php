<?php


namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_CriteriaVo;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Pages\Commun\Classes\Util;

/**
 * Formulaire de creation et modification des seances
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class SuiviSeance extends MpeTPage {
    
    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
    public function onLoad($param)
    {
    	//Ecriture du service metier en session
    	if(Atexo_CurrentUser::readFromSession('ServiceMetier') != 'cao') {
    		Atexo_CurrentUser::writeToSession('ServiceMetier', 'cao');
    	}
        $criteriaVo = new Atexo_Commission_CriteriaVo();
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
    	$this->remplirListeSeances($criteriaVo);     
    }
    
	public function remplirListeSeances($criteriaVo)
    {
		$criteriaVo->setCommandeName($this->getViewState('commandeName'));
		$criteriaVo->setSensTri($this->getViewState('sensTri'));
		//Criteres de pagination
		$offset = $this->tableauSuiviSeance->CurrentPageIndex * $this->tableauSuiviSeance->PageSize;
        $limit = $this->tableauSuiviSeance->PageSize;
        $criteriaVo->setLimit($limit);
		$criteriaVo->setOffset($offset);
    	
		$this->setViewState("CriteriaVo", $criteriaVo);
	    $nombreElementTotal= (new Atexo_Commission_Seance())->getSeances($criteriaVo, true);
        $listeSeances = array();
        if($nombreElementTotal >= 1) {
	        $listeSeances = (new Atexo_Commission_Seance())->getSeances($criteriaVo, false);
	        
            $this->panelMoreThanOneElementFound->setStyle('display:block');
            $this->panelNoElementFound->setStyle('display:none');
            $this->nombreElement->Text=$nombreElementTotal;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->panelBouttonGotoPageTop->setVisible(true);
            $this->panelBouttonGotoPageBottom->setVisible(true);
            self::afficherElementspager();
            $this->setViewState("nombreElement",$nombreElementTotal);
            $this->setViewState("listeSeances",$listeSeances);
            $this->nombrePageTop->Text=ceil($nombreElementTotal/$this->tableauSuiviSeance->PageSize);
            $this->nombrePageBottom->Text=ceil($nombreElementTotal/$this->tableauSuiviSeance->PageSize);
            $this->tableauSuiviSeance->setVirtualItemCount($nombreElementTotal);
            $this->tableauSuiviSeance->dataSource = $listeSeances;
            $this->tableauSuiviSeance->dataBind();
        } else {
            $this->panelMoreThanOneElementFound->setStyle('display:none');
            $this->panelNoElementFound->setStyle('display:block');
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            self::masquerElementspager();
            $this->panelBouttonGotoPageTop->setVisible(false);
            $this->panelBouttonGotoPageBottom->setVisible(false);
            $this->tableauSuiviSeance->dataSource = array();
            $this->tableauSuiviSeance->dataBind();
        }
        $this->setViewState("criteres",$criteriaVo);
    }
    
    public function populateData()
    {
        $offset = $this->tableauSuiviSeance->CurrentPageIndex * $this->tableauSuiviSeance->PageSize;
        $limit = $this->tableauSuiviSeance->PageSize;
        if ($offset + $limit > $this->tableauSuiviSeance->getVirtualItemCount()) {
            $limit = $this->tableauSuiviSeance->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet',$offset);
        $this->setViewState('limit',$limit);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $this->remplirListeSeances($criteriaVo);
    }
 	
    public function pageChanged($sender,$param)
    {
        $this->tableauSuiviSeance->CurrentPageIndex =$param->NewPageIndex;
        $this->numPageTop->Text=$param->NewPageIndex+1;
        $this->numPageBottom->Text=$param->NewPageIndex+1;
        $this->populateData();
    }

     public function goToPage($sender,$param)
  {
      switch ($sender->ID) {
          case "DefaultButtonTop" :    $numPage=$this->numPageTop->Text;
          break;
          case "DefaultButtonBottom" : $numPage=$this->numPageBottom->Text;
          break;
      }
      if ((new Util())->isEntier($numPage)) {
          if ($numPage>=$this->nombrePageTop->Text) {
              $numPage=$this->nombrePageTop->Text;
          } else if ($numPage<=0) {
              $numPage=1;
          }
          $this->tableauSuiviSeance->CurrentPageIndex =$numPage-1;
          $this->numPageBottom->Text=$numPage;
          $this->numPageTop->Text=$numPage;
          $this->populateData();
      } else {
          $this->numPageTop->Text=$this->tableauSuiviSeance->CurrentPageIndex+1;
          $this->numPageBottom->Text=$this->tableauSuiviSeance->CurrentPageIndex+1;
      }
  }
  public function changePagerLenght($sender,$param)
  {
      switch ($sender->ID) {
          case "listePageSizeTop" : $pageSize=$this->listePageSizeTop->getSelectedValue();
          $this->listePageSizeBottom->setSelectedValue($pageSize);
          break;
          case "listePageSizeBottom" : $pageSize=$this->listePageSizeBottom->getSelectedValue();
          $this->listePageSizeTop->setSelectedValue($pageSize);
          break;
      }
      // echo $pageSize;exit;
      $this->tableauSuiviSeance->PageSize=$pageSize;
      $nombreElement=$this->getViewState("nombreElement");
      $this->nombrePageTop->Text=ceil($nombreElement/$this->tableauSuiviSeance->PageSize);
      $this->nombrePageBottom->Text=ceil($nombreElement/$this->tableauSuiviSeance->PageSize);
      $this->tableauSuiviSeance->setCurrentPageIndex(0);
      $this->populateData();
  }
    public function afficherElementspager()
    {
    	$this->listePageSizeBottom->visible = true;
    	$this->texteAfficher->visible = true;
    	$this->texteResultParPage->visible = true;
    	$this->texteResultParPage->visible = true;
    	 
    }
    
	public function masquerElementspager()
    {
    	$this->listePageSizeBottom->visible = false;
    	$this->texteAfficher->visible = false;
    	$this->texteResultParPage->visible = false;
    	$this->texteResultParPage->visible = false;
    	 
    }
    
	public function critereTri($sender, $param)
 	{
 		$tri=$param->CommandName;
		$sensTriDefaut=$this->getViewState('sensTriDefaut','ASC');
		
 		$sensTri=$this->getViewState('sensTri');
 		
 		if($tri) {
 			if($sensTri=='ASC') {
	 			$sensTri='DESC';
 			} else {
 				$sensTri='ASC';
 			}
 		} else {
 			if($sensTriDefaut=='ASC') {
 				$sensTriDefaut='DESC';
	 		} else {
	 			$sensTriDefaut='ASC';
	 		}
	 		$sensTri='';
 		}	
 		
 		$this->setViewState('sensTri',$sensTri);
 		$this->setViewState('sensTriDefaut',$sensTriDefaut);
 		$this->setViewState('tri',$tri);
 		$this->setViewState('commandeName',$param->CommandName);
 	}
 	
	public function Trier($sender, $param)
 	{
	 	$this->populateData();
		$this->panelTableauSuiviSeance->render($param->NewWriter);
 	}
}
