<?php

namespace Application\Pages\Commission;



use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInviteQuery;
use Prado\Prado;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterneQuery;



/**
 * Formulaire de creation et modification des intervenents externes
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class ListeIntervenantsExternes extends MpeTPage {
	
	 public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }
    
    public function onLoad($param)
    { 
    	$org = Atexo_CurrentUser::getCurrentOrganism();
    	$this->fillDataIntervenants($org);
    }
    
    public function fillDataIntervenants($org, $tri=null, $sensTri=null){
    	$criteria = (new Atexo_Commission_IntervenantExterne())->generateCriteria($org,$tri,$sensTri,null,true);
    	$this->listeIntervenants->fillRepeaterWithDataForSearchResult($criteria);
    }
    
    public function removeIntervenant ($id, $intervenant=true){
    
    	//Recuperation des parametres d'interet
    	$org = Atexo_CurrentUser::getCurrentOrganism();
    	if($intervenant){
            
            //Verification du lien de l'intervenant avec des commissions
    	    $count = CommonTCAOCommissionIntervenantExterneQuery::create()
            	    ->filterByIdIntervenantExterne($id)
            	    ->filterByOrganisme($org)
                    ->count();    	    
    	    $is_used = $count > 0;
    	    
    	    //Verification du lien de l'intervenant avec des seances de commissions
    	    $count = CommonTCAOSeanceIntervenantExterneQuery::create()
            	    ->filterByIdIntervenantExterne($id)
            	    ->filterByOrganisme($org)
            	    ->count();    	    
    	    $is_used = $is_used || ($count > 0);
    	    
    	    //Verification du lien de l'intervenant avec des ordres du jour de seances de commissions
    	    $count = CommonTCAOSeanceInviteQuery::create()
        	    ->filterByIdIntervenantExterne($id)
        	    ->filterByOrganisme($org)
        	    ->count();
    	    $is_used = $is_used || ($count > 0);
    	    
    	    //Si l'intervenant est associe a des commissions/seances/lignes d'ordres du jour, on ne peut le supprimer
    	    if($is_used){
    	    
    	        //Envoi d'un message d'erreur
    	        $this->panelMessageErreur->setVisible(true);
			    $this->panelMessageErreur->setMessage(Prado::localize(TEXT_SUPPRESSION_INTERVENANT_IMPOSSIBLE));
    	    
    	    } else {
    	    
        		//On supprime l'intervenant externe
                (new Atexo_Commission_IntervenantExterne())->deleteIntervenant($id, $org);
        		//Rechargement de la page pour rafraichir les composants -> a revoir
        		$this->response->redirect("?page=Commission.ListeIntervenantsExternes");
        		
    	    }
       	}    	
    }
    
       
    public function critereTriInt($sender,$param){
    
    	$tri=$param->CommandName;
    	$sensTriDefaut=$this->getViewState('sensTriIntDefaut','ASC');
    
    	$sensTri=$this->getViewState('sensTriInt');
    
    	if($tri) {
    		if($sensTri=='ASC') {
    			$sensTri='DESC';
    		} else {
    			$sensTri='ASC';
    		}
    	} else {
    		if($sensTriDefaut=='ASC') {
    			$sensTriDefaut='DESC';
    		} else {
    			$sensTriDefaut='ASC';
    		}
    		$sensTri='';
    	}
    
    	$this->setViewState('sensTriInt',$sensTri);
    	$this->setViewState('sensTriIntDefaut',$sensTriDefaut);
    	$this->setViewState('triInt',$tri);
    	$this->setViewState('commandeNameInt',$param->CommandName);
    
    }
    
    public function TrierInt($sender, $param)
    {
    	$org          = Atexo_CurrentUser::getCurrentOrganism();
    	$tri          = $this->getViewState('triInt');
    	$sensTri      = $this->getViewState('sensTriInt');
    	    
    	$this->fillDataIntervenants($org,$tri,$sensTri);
    	$this->panelTableauListeIntervenants->render($param->NewWriter);
    
    }

}