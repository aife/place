<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado\Prado;
use Application\Controls\MpeTPage;

/**
 * Formulaire de creation et modification des membres de la seance
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class GestionMembresSeance extends MpeTPage
{

    private $idAgent;
    private $idIntExterne;

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $primary_key = array($idSeance, $org);
        $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
        $this->seanceSummary->setDataSource($seance);


        $this->fillDataAgents($idSeance, $org, $connexion);
        $this->fillDataIntervenants($idSeance, $org, $connexion);

        $this->setViewState('idSeance', $idSeance);
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getCurrentOrganism();
            $primary_key = array($idSeance, $org);
            $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
            $this->seanceSummary->setDataSource($seance);


            $this->fillDataAgents($idSeance, $org, $connexion);
            $this->fillDataIntervenants($idSeance, $org, $connexion);

            $this->setViewState('idSeance', $idSeance);
        }
    }

    public function removeIntervenant($id, $intervenant = false)
    {

        //Recuperation des parametres d'interet
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();

        if ($intervenant) {
            //On retire l'intervenant externe de la seance
            (new Atexo_Commission_IntervenantExterne())->deleteSeanceIntervenant($id, $idSeance, $org);
            $this->fillRepeaterAgent($idSeance, $org);

        } else {
            //On retire l'agent de la seance
            (new Atexo_Commission_Agent())->deleteSeanceAgent($id, $idSeance, $org);
            $this->fillRepeaterAgent($idSeance, $org);

        }
    }

    public function fillDataIntervenants($idSeance, $org, $tri = null, $sensTri = null, $connexion = null)
    {
        $listIds = (new Atexo_Commission_IntervenantExterne())->getAllIdsIntervenantsSeance($idSeance, $org, $connexion);
        $criteria = (new Atexo_Commission_IntervenantExterne())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeIntervenants->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function fillDataAgents($idSeance, $org, $tri = null, $sensTri = null)
    {
        $listIds = (new Atexo_Commission_Agent())->getAllIdsAgentsSeance($idSeance, $org);
        $criteria = (new Atexo_Commission_Agent())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeAgents->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function refreshRepeterIntervenants()
    {

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataIntervenants($idSeance, $org);
    }

    public function refreshRepeterAgents()
    {

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataAgents($idSeance, $org);
    }

    /**
     * Permet de charger la liste de tous les agents
     */
    public function fillAllAgents()
    {
        self::refreshRepeterAgents();
        self::refreshRepeterIntervenants();
    }

    /**
     *
     * Permet de charger les tableaux des agents et des intervenants
     * @param int $idSeance : l'identifiant technique de la seance
     * @param varchar $org : l'organisme de la seance
     */
    public function fillRepeaterAgent($idSeance, $org)
    {
        $this->fillDataIntervenants($idSeance, $org);
        $this->fillDataAgents($idSeance, $org);
    }

    public function suggestAgent($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idSeance = $this->getViewState('idSeance');
        $seance = true;
        $buffer = (new Atexo_Commission_Agent())->searchAgentsByOrganisme($idSeance, $org, $token, $seance);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['service_path'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();

        } else {

            $buffer = array('id' => 0, 'text' => Prado::localize('DEFINE_AUCUN_RESULTAT'));
            $sender->DataSource = array($buffer);
            $sender->dataBind();

        }
    }

    public function selectAgent($sender, $param)
    {
        $this->idAgent = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idAgent', $this->idAgent);
    }

    public function addAgentToSeance($sender, $param)
    {

        //Connexion a la base de donnees
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        //Recuperation de l'ID de l'agent a ajouter
        $idAgent = $this->getViewState('idAgent');

        //Recuperation de l'agent
        $agent = Atexo_Agent::retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {

            //Recuperation des parametres d'interet
            $idSeance = $this->getViewState('idSeance');
            $org = Atexo_CurrentUser::getCurrentOrganism();

            //Type de voix par defaut
            $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $last_item = array_slice($_t_typeVoix, -1, 1, true);
            $idTypeVoix = key($last_item);

            //Verification de l'existence de cet agent pour cette seance
            $seance_agent = (new Atexo_Commission_Agent())->getSeanceAgent($idAgent, $idSeance, $org, $connexion);

            if (!($seance_agent instanceof CommonTCAOSeanceAgent)) {
                $seance_agent = new CommonTCAOSeanceAgent();
            }

            //Mise a jour des champs de l'enregistrement
            $seance_agent->setOrganisme($org);
            $seance_agent->setIdSeance($idSeance);
            $seance_agent->setIdAgent($agent->getId());
            $seance_agent->setIdRefValTypeVoix($idTypeVoix);
            $seance_agent->setTypeCompte($agent->getElu());
            //Sauvegarde de l'enregistrement
            $seance_agent->save($connexion);

        }

    }

    public function addAgentToSeanceCallback()
    {

        //Rechargement de la page pour rafraichir les composants
        $this->response->redirect("?page=Commission.GestionMembresSeance&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));

    }

    public function suggestIntExterne($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idSeance = $this->getViewState('idSeance');
        $seance = true;

        $new_buffer = array();
        $buffer = (new Atexo_Commission_IntervenantExterne())->searchIntervenantExterneByOrganisme($idSeance, $org, $token, $seance);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['organisation'] . ' - ' . $line['fonction'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();

        } else {

            $line = Prado::localize('DEFINE_AUCUN_RESULTAT') . '&nbsp;&gt;&nbsp;';
            $line .= '<a href="?page=Commission.FormulaireIntervenantExterne&creation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&calledFrom=seance" >';
            $line .= Prado::localize('DEFINE_CREER') . '</a>';

            $buffer = array('id' => 0, 'text' => $line);
            $sender->DataSource = array($buffer);
            $sender->dataBind();

        }
    }

    public function selectIntExterne($sender, $param)
    {
        $this->idIntExterne = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idIntExterne', $this->idIntExterne);
    }

    public function addIntervenantExterneToSeance($sender, $param)
    {

        //Connexion a la base de donnees
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        //Recuperation de l'ID de l'intervenant externe a ajouter
        $idIntExterne = $this->getViewState('idIntExterne');
        //Recuperation des parametres d'interet
        $idSeance = $this->getViewState('idSeance');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        //Ajout de l'intervenant a la seance
        (new Atexo_Commission_IntervenantExterne())->addIntervenantToSeance($idIntExterne, $idSeance, $org);

    }

    public function addIntervenantExterneToSeanceCallback()
    {

        //Rechargement de la page pour rafraichir les composants -> a revoir
        $this->response->redirect("?page=Commission.GestionMembresSeance&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));

    }


    public function save($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (isset($_GET['id'])) {

            $primary_key = array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
            $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);

            if ($seance instanceof CommonTCAOSeance) {

                self::saveListeAgents($seance->getIdSeance(), $seance->getOrganisme(), $connexion);
                self::saveListeIntervenants($seance->getIdSeance(), $seance->getOrganisme(), $connexion);

            }
        }

        self::goToPageSuiviSeance();

    }

    public function saveListeAgents($idSeance, $org, $connexion)
    {

        $data = $this->listeAgents->getAgents();
        foreach ($data as $item) {

            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $agent = (new Atexo_Commission_Agent())->getSeanceAgent($id, $idSeance, $org, $connexion);

            if ($agent instanceof CommonTCAOSeanceAgent) {

                $agent->setOrganisme($org);
                $agent->setIdSeance($idSeance);
                $agent->setIdAgent($id);
                $agent->setIdRefValTypeVoix($idTypeVoix);
                $agent->save($connexion);

            }
        }

    }

    public function saveListeIntervenants($idSeance, $org, $connexion)
    {

        $data = $this->listeIntervenants->getIntervenantsExternes();

        foreach ($data as $item) {

            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $intervenant = (new Atexo_Commission_IntervenantExterne())->getSeanceIntervenantExterne($id, $idSeance, $org, $connexion);
            if ($intervenant instanceof CommonTCAOSeanceIntervenantExterne) {

                $intervenant->setOrganisme($org);
                $intervenant->setIdSeance($idSeance);
                $intervenant->setIdIntervenantExterne($id);
                $intervenant->setIdRefValTypeVoix($idTypeVoix);
                $intervenant->save($connexion);
            }
        }
    }

    /**
     * Permet de rediriger vers la page listant les séances
     */
    public function goToPageSuiviSeance()
    {
        $this->response->redirect("?page=Commission.SuiviSeance&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));

    }

    public function critereTriAgent($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriAgentDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriAgent');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriAgent', $sensTri);
        $this->setViewState('sensTriAgentDefaut', $sensTriDefaut);
        $this->setViewState('triAgent', $tri);
        $this->setViewState('commandeNameAgent', $param->CommandName);

    }

    public function TrierAgent($sender, $param)
    {
        $tri = $this->getViewState('triAgent');
        $sensTri = $this->getViewState('sensTriAgent');
        $idSeance = $this->getViewState('idSeance');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataAgents($idSeance, $org, $tri, $sensTri);
        $this->panelTableauListeAgents->render($param->NewWriter);

    }

    public function critereTriInt($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriIntDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriInt');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriInt', $sensTri);
        $this->setViewState('sensTriIntDefaut', $sensTriDefaut);
        $this->setViewState('triInt', $tri);
        $this->setViewState('commandeNameInt', $param->CommandName);

    }

    public function TrierInt($sender, $param)
    {
        $tri = $this->getViewState('triInt');
        $sensTri = $this->getViewState('sensTriInt');
        $idSeance = $this->getViewState('idSeance');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataIntervenants($idSeance, $org, $tri, $sensTri);
        $this->panelTableauListeIntervenants->render($param->NewWriter);

    }

}