<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOOrdreDePassageQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado;
use Application\Controls\MpeTPage;

/**
 * Formulaire de creation et modification des intervenents externes
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class GestionMembresPonctuels extends MpeTPage
{

    private $idAgent;
    private $idIntExterne;

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {

            $id_ordre_de_passage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idS']));
            $org = Atexo_CurrentUser::getCurrentOrganism();

            $seance = CommonTCAOSeanceQuery::create()
                ->findPk(array($idSeance, $org));
            $this->seanceSummary->setDataSource($seance);

            $this->fillDataAgents($id_ordre_de_passage, $org);
            $this->fillDataIntervenants($id_ordre_de_passage, $org, $connexion);

            $this->setViewState('idS', $idSeance);
            $this->setViewState('id', $id_ordre_de_passage);
        }
    }

    public function fillDataIntervenants($id_ordre_de_passage, $org, $tri = null, $sensTri = null)
    {
        $listIds = (new Atexo_Commission_IntervenantExterne())->getAllIdInvitesOrdreDePassage($id_ordre_de_passage, $org, null, true);
        $criteria = (new Atexo_Commission_IntervenantExterne())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeIntervenants->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function refreshRepeterIntervenants()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $id_ordre_de_passage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataIntervenants($id_ordre_de_passage, $org);
    }

    public function fillDataAgents($id_ordre_de_passage, $org, $tri = null, $sensTri = null)
    {
        $listIds = (new Atexo_Commission_IntervenantExterne())->getAllIdInvitesOrdreDePassage($id_ordre_de_passage, $org);
        $criteria = (new Atexo_Commission_Agent())->generateCriteria($org, $tri, $sensTri, $listIds);
        $this->listeAgents->fillRepeaterWithDataForSearchResult($criteria);
    }

    public function refreshRepeterAgents()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idOdj = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $this->fillDataAgents($idOdj, $org);
    }

    /**
     *
     * Permet de charger les tableaux des agents et des intervenants
     * @param int $idSeance : l'identifiant technique de la seance
     * @param varchar $org : l'organisme de la seance
     */
    public function fillRepeaterAgent($id_ordre_de_passage, $org)
    {
        $this->fillDataIntervenants($id_ordre_de_passage, $org);
        $this->fillDataAgents($id_ordre_de_passage, $org);
    }

    /**
     * Permet de charger la liste de tous les agents
     */
    public function fillAllAgents()
    {
        self::refreshRepeterIntervenants();
        self::refreshRepeterAgents();
    }

    public function suggestAgent($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $id = $this->getViewState('id');

        $buffer = (new Atexo_Commission_Agent())->searchAgentsForOrdreDuJour($id, $org, $token);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['service_path'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();

        } else {

            $buffer = array('id' => 0, 'text' => Prado::localize('DEFINE_AUCUN_RESULTAT'));
            $sender->DataSource = array($buffer);
            $sender->dataBind();

        }
    }

    public function selectAgent($sender, $param)
    {
        $this->idAgent = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idAgent', $this->idAgent);
    }

    public function addAgentToOrdreDePassage($sender, $param)
    {
        //Recuperation de l'ID de l'agent a ajouter
        $idAgent = $this->getViewState('idAgent');

        //Recuperation de l'agent
        $agent = Atexo_Agent::retrieveAgent($idAgent);

        if ($agent instanceof CommonAgent) {
            //Recuperation des parametres d'interet
            $id = $this->getViewState('id');
            $org = Atexo_CurrentUser::getCurrentOrganism();

            //Type de voix par defaut
            $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $last_item = array_slice($_t_typeVoix, -1, 1, true);
            $idTypeVoix = key($last_item);

            //Verification de l'existence de cet agent pour cette consultation
            $invite = (new Atexo_Commission_IntervenantExterne())->getInviteOrdreDePassage($idAgent, $id, $org, true);

            if (!($invite instanceof CommonTCAOSeanceInvite)) {
                $invite = new CommonTCAOSeanceInvite();
            }

            //Mise a jour des champs de l'enregistrement
            $invite->setOrganisme($org);
            $invite->setIdOrdreDePassage($id);
            $invite->setIdAgent($agent->getId());
            $invite->setIdRefValTypeVoix($idTypeVoix);

            //Sauvegarde de l'enregistrement
            $invite->save();
        }
    }

    public function addAgentToOrdreDePassageCallback()
    {
        //Rechargement de la page pour rafraichir les composants
        $this->response->redirect("?page=Commission.GestionMembresPonctuels&idS=" . Atexo_Util::atexoHtmlEntities($_GET['idS']) . "&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function suggestIntExterne($sender, $param)
    {

        $token = $param->getToken();
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $id = $this->getViewState('id');

        $new_buffer = array();
        $buffer = (new Atexo_Commission_IntervenantExterne())->searchIntervenantExterneForOrdreDuJour($id, $org, $token);

        if (!empty($buffer)) {

            foreach ($buffer as $line) {

                $item['id'] = $line['id'];
                $item['text'] = strtoupper($line['nom']) . ' ' . $line['prenom'] . ' / ' . $line['email'] . ' <br/> ' . $line['organisation'] . ' - ' . $line['fonction'];
                $new_buffer[] = $item;

            }

            $sender->DataSource = $new_buffer;
            $sender->dataBind();

        } else {

            $line = prado::localize('DEFINE_AUCUN_RESULTAT') . '&nbsp;&gt;&nbsp;';
            $line .= '<a href="?page=Commission.FormulaireIntervenantExterne&creation&idS=' . Atexo_Util::atexoHtmlEntities($_GET['idS']) . '&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&calledFrom=odj" >';
            $line .= prado::localize('DEFINE_CREER') . '</a>';

            $buffer = array('id' => 0, 'text' => $line);
            $sender->DataSource = array($buffer);
            $sender->dataBind();
        }
    }

    public function selectIntExterne($sender, $param)
    {
        $this->idIntExterne = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->setViewState('idIntExterne', $this->idIntExterne);
    }

    public function addIntervenantExterneToOrdreDePassage($sender, $param)
    {
        //Connexion a la base de donnees
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        //Recuperation de l'ID de l'intervenant externe a ajouter
        $idIntExterne = $this->getViewState('idIntExterne');
        //Recuperation des parametres d'interet
        $id = $this->getViewState('id');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        //Ajout de l'intervenant a l'ordre du jour
        (new Atexo_Commission_IntervenantExterne())->addIntervenantToOrdreDePassage($idIntExterne, $id, $org);
    }

    public function addIntervenantExterneToOrdreDePassageCallback()
    {
        //Rechargement de la page pour rafraichir les composants
        $this->response->redirect("?page=Commission.GestionMembresPonctuels&idS=" . Atexo_Util::atexoHtmlEntities($_GET['idS']) . "&id=" . Atexo_Util::atexoHtmlEntities($_GET['id']));
    }

    public function removeIntervenant($id, $intervenant = false)
    {

        //Recuperation des parametres d'interet
        $id_ordre_de_passage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();

        if ($intervenant) {
            //On retire l'intervenant externe de l'ordre du jour
            (new Atexo_Commission_IntervenantExterne())->deleteOrdreDePassageIntervenant($id, $id_ordre_de_passage, $org);
            $this->fillRepeaterAgent($id_ordre_de_passage, $org);

        } else {
            //On retire l'agent de l'ordre du jour
            (new Atexo_Commission_IntervenantExterne())->deleteOrdreDePassageIntervenant($id, $id_ordre_de_passage, $org, true);
            $this->fillRepeaterAgent($id_ordre_de_passage, $org);

        }
    }

    public function save($sender, $param)
    {
        if (isset($_GET['id'])) {
            $idOrdreDePassage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $org = Atexo_CurrentUser::getCurrentOrganism();

            $ordre_de_passage = CommonTCAOOrdreDePassageQuery::create()
                ->findPk(array($idOrdreDePassage, $org));

            if ($ordre_de_passage instanceof CommonTCAOOrdreDePassage) {
                self::saveListeAgents($ordre_de_passage->getIdOrdreDePassage(), $org);
                self::saveListeIntervenants($ordre_de_passage->getIdOrdreDePassage(), $org);
                self::fillAllAgents();
            }
        }

        self::goToOrdreDuJour();
    }

    public function saveListeAgents($idOrdreDePassage, $org)
    {

        $data = $this->listeAgents->getAgents();

        foreach ($data as $item) {
            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $invite = (new Atexo_Commission_IntervenantExterne())->getInviteOrdreDePassage($id, $idOrdreDePassage, $org, true);
            if ($invite instanceof CommonTCAOSeanceInvite) {
                $invite->setOrganisme($org);
                $invite->setIdOrdreDePassage($idOrdreDePassage);
                $invite->setIdAgent($id);
                $invite->setIdRefValTypeVoix($idTypeVoix);
                $invite->save($connexion);
            }
        }
    }

    public function saveListeIntervenants($idOrdreDePassage, $org)
    {
        $data = $this->listeIntervenants->getIntervenantsExternes();

        foreach ($data as $item) {
            $id = $item ["ID"];
            $idTypeVoix = $item["ID_REF_VAL_TYPE_VOIX_DEFAUT"];

            $invite = (new Atexo_Commission_IntervenantExterne())->getInviteOrdreDePassage($id, $idOrdreDuJour, $org);
            if ($invite instanceof CommonTCAOSeanceInvite) {
                $invite->setOrganisme($org);
                $invite->setIdOrdreDePassage($idOrdreDuJour);
                $invite->setIdIntervenantExterne($id);
                $invite->setIdRefValTypeVoix($idTypeVoix);
                $invite->save($connexion);
            }
        }
    }

    public function critereTriAgent($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriAgentDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriAgent');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriAgent', $sensTri);
        $this->setViewState('sensTriAgentDefaut', $sensTriDefaut);
        $this->setViewState('triAgent', $tri);
        $this->setViewState('commandeNameAgent', $param->CommandName);

    }

    public function TrierAgent($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $tri = $this->getViewState('triAgent');
        $sensTri = $this->getViewState('sensTriAgent');
        $id_ordre_de_passage = $this->getViewState('id');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataAgents($id_ordre_de_passage, $org, $tri, $sensTri);
        $this->panelTableauListeAgents->render($param->NewWriter);

    }

    public function critereTriInt($sender, $param)
    {

        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriIntDefaut', 'ASC');

        $sensTri = $this->getViewState('sensTriInt');

        if ($tri) {
            if ($sensTri == 'ASC') {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ($sensTriDefaut == 'ASC') {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTriInt', $sensTri);
        $this->setViewState('sensTriIntDefaut', $sensTriDefaut);
        $this->setViewState('triInt', $tri);
        $this->setViewState('commandeNameInt', $param->CommandName);

    }

    public function TrierInt($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $tri = $this->getViewState('triInt');
        $sensTri = $this->getViewState('sensTriInt');
        $id_ordre_de_passage = $this->getViewState('id');
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataIntervenants($id_ordre_de_passage, $org, $tri, $sensTri);
        $this->panelTableauListeIntervenants->render($param->NewWriter);

    }

    public function goToOrdreDuJour()
    {

        $this->response->redirect("?page=Commission.OrdreDuJour&id=" . Atexo_Util::atexoHtmlEntities($_GET['idS']));

    }
}