<?php

namespace Application\Pages\Commission;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;

/**
 * Permet de telecharger les pieces jointes liees e la Commission
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Commission
 */
class DownloadPjEchangeCommission extends DownloadFile
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idPj = $_GET['idPj'];
        if ($_GET['org']) {
            $org = $_GET['org'];
        } else {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $pj = Atexo_Message::getPjByIdPiece($idPj, $org);

        if ($pj instanceof CommonEchangePieceJointe) {
            $echange = Atexo_Message::retrieveMessageById($pj->getIdMessage(), $org);
            if ($echange) {
                $this->_idFichier = $idPj;
                $this->_nomFichier = $pj->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
            }
        }
    }

}     