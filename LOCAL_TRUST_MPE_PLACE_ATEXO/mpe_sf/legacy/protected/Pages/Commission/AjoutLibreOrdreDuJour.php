<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTCAOCommissionConsultation;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Prado\Prado;

/**
 * Classe d'ajout d'un nouvel ordre du jour
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class AjoutLibreOrdreDuJour extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            self::remplirListeTypeProcedure();
            self::remplirListeCategorie();
        }
        if (isset($_GET['id'])) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $primary_key = array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
            $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
            if ($seance instanceof CommonTCAOSeance) {
                $this->seanceSummary->setDataSource($seance);
            }
        }
    }

    public function goToPageOrdreDuJour($sender, $param)
    {
        $this->response->redirect("?page=Commission.OrdreDuJour&id=" . $_GET['id']);
    }

    /**
     * Permet de sauvegarder les consultations libres a l'ordre du jour de la seance de Commission
     *
     * @param $sender
     * @param $param
     */
    public function save($sender, $param)
    {
        //Recuperation des parametres d'interet
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        //Chargement de la seance
        $seance = CommonTCAOSeanceQuery::create()
            ->findPk(array($idSeance, $organisme));

        if ($seance) {

            //Creation d'une consultation de Commission
            $consultation = new CommonTCAOCommissionConsultation();
            $consultation->setOrganisme($organisme);
            $consultation->setIdCommission($seance->getIdCommission());
            $consultation->setRefLibre(Atexo_Util::atexoHtmlEntities($this->reference->Text));
            $consultation->setIntitule(Atexo_Util::atexoHtmlEntities($this->objet->Text));
            $consultation->setIdTypeProcedure($this->procedureType->getSelectedValue());
            $consultation->setIdCategorie($this->categorie->getSelectedValue());
            $consultation->setDateCloture(Atexo_Util::frnDateTime2iso(Atexo_Util::atexoHtmlEntities($this->dateHeure->Text)));
            $consultation->setServiceGestionnaire(Atexo_Util::atexoHtmlEntities($this->serviceGestionnaire->Text));
            $consultation->setServiceAssocie(Atexo_Util::atexoHtmlEntities($this->serviceAssocie->Text));

            if ($consultation->save()) {

                //Creation de l'ordre de passage en Commission de la nouvelle consultation
                $ordre_de_passage = new CommonTCAOOrdreDePassage();
                $ordre_de_passage->setIdSeance($idSeance);
                $ordre_de_passage->setOrganisme($organisme);
                $ordre_de_passage->setIdCommission($seance->getIdCommission());
                $ordre_de_passage->setDateSeance($seance->getDate());
                $ordre_de_passage->setDatePassage($seance->getDate());
                $ordre_de_passage->setIdCommissionConsultation($consultation->getIdCommissionConsultation());
                $dataSource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_ETAPES_ORDRES_DU_JOUR'));
                if (is_array($dataSource) && count($dataSource)) {
                    foreach ($dataSource as $key => $data) {
                        if ($key) {
                            $ordre_de_passage->setIdRefOrgValEtape($key);
                            break;
                        }
                    }
                }
                $ordre_de_passage->save();
            }
        }

        self::goToPageOrdreDuJour($sender, $param);

    }


    public function remplirListeTypeProcedure()
    {
        $proceduresTypes = Atexo_Consultation_ProcedureType:: retrieveProcedureType();
        $dataProcedures = array();
        $dataProcedures[0] = ' --- ' . Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES') . ' --- ';
        if (is_array($proceduresTypes)) {
            foreach ($proceduresTypes as $typeProc) {
                $dataProcedures[$typeProc->getIdTypeProcedure()] = $typeProc->getLibelleTypeProcedure();
            }
        }
        $this->procedureType->Datasource = $dataProcedures;
        $this->procedureType->dataBind();
    }

    public function remplirListeCategorie()
    {
        $_t_categorie = Atexo_Consultation_Category:: retrieveCategories();
        $data = array();
        $data[0] = ' --- ' . Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUTES_LES_CATEGORIES') . ' --- ';
        if (is_array($_t_categorie)) {
            foreach ($_t_categorie as $item_categorie) {
                $data[$item_categorie->getId()] = $item_categorie->getLibelle();
            }
        }
        $this->categorie->Datasource = $data;
        $this->categorie->dataBind();
    }
}