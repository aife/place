<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExternePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;

/**
 * Ajout des intervenents externes Commission
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class PopUpAjoutIntervenant extends MpeTPage
{

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue("agent");
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
            $org = Atexo_CurrentUser::getCurrentOrganism();
            $this->fillDataIntervenants($idCommission, $org, $connexion);
        }

    }

    public function fillDataIntervenants($idCommission, $org, $connexion)
    {
        $listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsCommission($idCommission, $org, $connexion);
        $c = new Criteria();

        $c->add(CommonTCAOCommissionIntervenantExternePeer::ORGANISME, $org);
        if (is_array($listIds) && count($listIds) > 0) {
            $query = "id not in (" . implode(",", $listIds) . ")";
            $c->add(CommonTCAOCommissionIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $query, Criteria::CUSTOM);
        }
        $c->addAscendingOrderByColumn(CommonTCAOCommissionIntervenantExternePeer::ORGANISME);
        $this->listeIntervenant->fillRepeaterWithDataForSearchResult($c);
    }

    public function valider(){
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $data = $this->listeIntervenant->getIntervenantSelectionnes();

        foreach($data as $d) {
            $id = $d ["ID"];
            $idTypeVoix = $d["TYPE_VOIX"];

            $intervenantCom = Atexo_Commission_IntervenantExterne::getIntervenantCommission($id, $idCommission, $org, $connexion);
            if (!($intervenantCom instanceof CommonTCAOCommissionIntervenantExterne)) {
                $intervenantCom = new CommonTCAOCommissionIntervenantExterne();
            }

            $intervenantCom->setOrganisme($org);
            $intervenantCom->setIdCommission($idCommission);
            $intervenantCom->setIdIntervenantExterne($id);
            $intervenantCom->setTypeVoix($idTypeVoix);

            $intervenantCom->save($connexion);
        }

        echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refrechIntervenant').click();window.close();</script>";
    }
}