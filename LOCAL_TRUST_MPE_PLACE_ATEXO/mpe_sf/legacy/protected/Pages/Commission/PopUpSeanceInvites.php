<?php

namespace Application\Pages\Commission;


use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;


/**
 * Affiche les membres invites a la seance de Commission
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2014
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class PopUpSeanceInvites extends MpeTPage {
	
	private $idAgent;
	private $idIntExterne;
	
	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue("agent");
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();

        $this->fillDataAgents($idSeance, $org, $connexion);
        $this->fillDataIntervenants($idSeance, $org, $connexion);

        $this->setViewState('idSeance', $idSeance);
    }
    
    public function onLoad($param)
    { 
    	if(!$this->isPostBack){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
			$org = Atexo_CurrentUser::getCurrentOrganism();
				    	
	    	$this->fillDataAgents($idSeance, $org, $connexion);
	    	$this->fillDataIntervenants($idSeance, $org, $connexion);
	    	
	    	$this->setViewState('idSeance',$idSeance);
    	}
    }
    
    
    
    public function fillDataIntervenants($idSeance, $org, $tri=null, $sensTri=null, $connexion=null){
    	$listIds = (new Atexo_Commission_IntervenantExterne())->getAllIdsIntervenantsSeance($idSeance, $org, $connexion);
    	$criteria = (new Atexo_Commission_IntervenantExterne())->generateCriteria($org,$tri,$sensTri,$listIds);
    	$this->listeIntervenants->fillRepeaterWithDataForSearchResult($criteria);
    }
    
	public function fillDataAgents($idSeance, $org, $tri=null, $sensTri=null){
		$listIds = (new Atexo_Commission_Agent())->getAllIdsAgentsSeance($idSeance, $org);
		$criteria = (new Atexo_Commission_Agent())->generateCriteria($org,$tri,$sensTri,$listIds);
		$this->listeAgents->fillRepeaterWithDataForSearchResult($criteria);
    }
    
    public function critereTriAgent($sender,$param){
    
        $tri=$param->CommandName;
        $sensTriDefaut=$this->getViewState('sensTriAgentDefaut','ASC');
    
        $sensTri=$this->getViewState('sensTriAgent');
    
        if($tri) {
            if($sensTri=='ASC') {
                $sensTri='DESC';
            } else {
                $sensTri='ASC';
            }
        } else {
            if($sensTriDefaut=='ASC') {
                $sensTriDefaut='DESC';
            } else {
                $sensTriDefaut='ASC';
            }
            $sensTri='';
        }
    
        $this->setViewState('sensTriAgent',$sensTri);
        $this->setViewState('sensTriAgentDefaut',$sensTriDefaut);
        $this->setViewState('triAgent',$tri);
        $this->setViewState('commandeNameAgent',$param->CommandName);
    
    }
    
    public function TrierAgent($sender, $param)
    {
        $tri       = $this->getViewState('triAgent');
        $sensTri   = $this->getViewState('sensTriAgent');
        $idSeance  = $this->getViewState('idSeance');
        $org       = Atexo_CurrentUser::getCurrentOrganism();
    
        $this->fillDataAgents($idSeance,$org,$tri,$sensTri);
        $this->panelTableauListeAgents->render($param->NewWriter);
    
    }
    
    public function critereTriInt($sender,$param){
    
        $tri=$param->CommandName;
        $sensTriDefaut=$this->getViewState('sensTriIntDefaut','ASC');
    
        $sensTri=$this->getViewState('sensTriInt');
    
        if($tri) {
            if($sensTri=='ASC') {
                $sensTri='DESC';
            } else {
                $sensTri='ASC';
            }
        } else {
            if($sensTriDefaut=='ASC') {
                $sensTriDefaut='DESC';
            } else {
                $sensTriDefaut='ASC';
            }
            $sensTri='';
        }
    
        $this->setViewState('sensTriInt',$sensTri);
        $this->setViewState('sensTriIntDefaut',$sensTriDefaut);
        $this->setViewState('triInt',$tri);
        $this->setViewState('commandeNameInt',$param->CommandName);
    
    }
    
    public function TrierInt($sender, $param)
    {
        $tri       = $this->getViewState('triInt');
        $sensTri   = $this->getViewState('sensTriInt');
        $idSeance  = $this->getViewState('idSeance');
        $org       = Atexo_CurrentUser::getCurrentOrganism();
    
        $this->fillDataIntervenants($idSeance,$org,$tri,$sensTri);
        $this->panelTableauListeIntervenants->render($param->NewWriter);
    
    }
    
}