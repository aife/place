<?php

namespace Application\Pages\Commission;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExternePeer;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Config;
use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Library\Propel\Query\Criteria;
use Application\Service\Atexo\Atexo_Util;

/**
 * Ajout des intervenents externes Commission
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
 
class PopUpAjoutIntervenantPonctuel extends MpeTPage {
	
	public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
    	Atexo_Languages::setLanguageCatalogue("agent");
    }
    
	public function onLoad($param)
    { 
		if(!$this->isPostBack){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
			$org = Atexo_CurrentUser::getCurrentOrganism();
	    	$this->fillDataIntervenants($idOrdreJour, $org, $connexion);
		}
		
    }
    
    public function fillDataIntervenants($idOrdreJour, $org, $connexion){
    	$listIds = Atexo_Commission_IntervenantExterne::getAllIdIntervenantsOrdreDuJour($idOrdreJour, $org, $connexion);
 		$c = new Criteria();
 		
 		$c->add(CommonTCAOCommissionIntervenantExternePeer::ORGANISME, $org);
 		if(is_array($listIds) && count($listIds) > 0){
	 		$query = "id not in (" . implode(",", $listIds) . ")" ;
	 		$c->add(CommonTCAOCommissionIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $query, Criteria::CUSTOM);
 		}
 		$c->addAscendingOrderByColumn(CommonTCAOCommissionIntervenantExternePeer::ORGANISME);
    	$this->listeIntervenant->fillRepeaterWithDataForSearchResult($c);
    }
    
    public function valider(){
    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
		$org = Atexo_CurrentUser::getCurrentOrganism();
	    
		$data = $this->listeIntervenant->getIntervenantSelectionnes();
		
    	foreach($data as $d){
    		$id = $d ["ID"];
 			$idTypeVoix = $d["TYPE_VOIX"];
 			
 			/*$intervenant = Atexo_Commission_IntervenantExterne::getIntervenantOrdreJour($id, $idOrdreJour, $org, $connexion);
 			if(!($intervenant instanceOf CommonIntervenantOrdreDuJour)){
 				$intervenant = new CommonIntervenantOrdreDuJour();
 			}
 			
 			$intervenant->setOrganisme($org);
 			$intervenant->setIdOrdreDuJour($idOrdreJour);
 			$intervenant->setIdIntervenant($id);
 			$intervenant->setTypeVoix($idTypeVoix);
 			
 			$intervenant->save($connexion);*/
    	}
    	
    	echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refrechIntervenant').click();window.close();</script>";
    }
}