<?php

namespace Application\Pages\Commission;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_CriteriaVo;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Controls\MpeTPage;

/**
 * Formulaire de creation et modification des séances
 * @author Améline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 * @since MPE4
 * @package Pages
 * @subpackage protected
 */
class FormulaireSeance extends MpeTPage
{

    public function onInit($param)
    {
        $this->Master->setCalledFrom("agent");
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->isPostBack) {

            self::remplirListeTypeCommission();
            self::remplirListeStatut();
            if (isset($_GET['suivi']) && isset($_GET['id'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $primary_key = array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
                $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
                if ($seance instanceof CommonTCAOSeance) {
                    $this->dateHeure->Text = Atexo_Util::iso2frnDateTime(Atexo_Util::atexoHtmlEntities($seance->getDate()), true);
                    $this->statutSeance->setSelectedValue($seance->getIdRefValStatut());
                    $this->typeCommission->setSelectedValue($seance->getIdCommission());
                    $this->lieuSeance->Text = Atexo_Util::atexoHtmlEntitiesDecode($seance->getLieu());
                    $this->salleSeance->Text = Atexo_Util::atexoHtmlEntitiesDecode($seance->getSalle());
                }
            }
        }
    }

    /**
     * Permet de sauvegarder les commissions
     *
     * @param $sender
     * @param $param
     */
    public function save($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (isset($_GET['id'])) {
            $primary_key = array(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_CurrentUser::getCurrentOrganism());
            $seance = CommonTCAOSeanceQuery::create()->findPk($primary_key, $connexion);
        } else {
            //Creation de la seance
            $seance = new CommonTCAOSeance();
        }

        //Recuperation de la Commission
        $updateIntervenants = false;
        $n_idCom = $this->typeCommission->getSelectedValue();
        $p_idCom = $seance->getIdCommission();
        if (!empty($p_idCom) && $n_idCom <> $p_idCom) {
            $updateIntervenants = true;
        }

        //Recuperation de la date de la seance
        $date_heure = Atexo_Util::atexoHtmlEntities($this->dateHeure->Text);
        $_t_date = explode(' ', $date_heure);
        $date = $_t_date[0];
        $heure = $_t_date[1];
        $_h = explode(':', $heure);
        if ($_h[0] == '00') {
            $heure = '09:00';
        }
        $date_heure = implode(' ', array($date, $heure));

        $seance->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $seance->setDate(Atexo_Util::frnDateTime2iso($date_heure));
        $seance->setIdCommission($n_idCom);
        $seance->setIdRefValStatut($this->statutSeance->getSelectedValue());
        $seance->setLieu(Atexo_Util::atexoHtmlEntities($this->lieuSeance->Text));
        $seance->setSalle(Atexo_Util::atexoHtmlEntities($this->salleSeance->Text));
        $seance->save($connexion);

        if (!isset($_GET['id']) && isset($_GET['creation']) && !$updateIntervenants) {

            //Ajout par defaut des agents et des intervenants externes de la Commission a la creation de la seance
            self::addIntervenants($seance->getIdSeance(), $n_idCom, $connexion);

        } elseif ($updateIntervenants) {

            //Mise a jour des agents et des intervenants si le type de Commission change
            self::updateIntervenants($seance->getIdSeance(), $n_idCom, $connexion);

        }

        self::goToPageSuiviSeance($sender, $param);

    }

    public function updateIntervenants($idSeance, $idCommission, $connexion)
    {

        $org = Atexo_CurrentUser::getCurrentOrganism();

        //Supprimer les agents de seance actuels
        (new Atexo_Commission_Agent())->removeAgentsFromSeance($idSeance, $org, $connexion);

        //Supprimer les intervenants de seance actuels
        (new Atexo_Commission_IntervenantExterne())->removeIntervenantsFromSeance($idSeance, $org, $connexion);

        //Ajouter les agents et les intervenants de la nouvelle Commission
        self::addIntervenants($idSeance, $idCommission, $connexion);

    }

    /**
     * Permet de d'affecter par defaut les agents et les intervenants rattaches a la Commission
     */
    public function addIntervenants($idSeance, $idCommission, $connexion)
    {

        $org = Atexo_CurrentUser::getCurrentOrganism();
        //Ajout par defaut des agents de la Commission
        (new Atexo_Commission_Agent())->addCommissionAgentsToSeance($idSeance, $idCommission, $org, $connexion);

        //Ajout par defaut des intervenants de la Commission
        (new Atexo_Commission_IntervenantExterne())->addCommissionIntervenantsToSeance($idSeance, $idCommission, $org, $connexion);
    }

    /**
     * Permet de rediriger vers la page de suivi des séances
     */
    public function goToPageSuiviSeance($sender, $param)
    {
        $this->response->redirect("?page=Commission.SuiviSeance");
    }

    /**
     * Permet de remplir la liste des types des commissions
     */
    public function remplirListeTypeCommission()
    {
        $criteriaVo = new Atexo_Commission_CriteriaVo();
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $data = (new Atexo_Commission_Seance())->getCommissionType($criteriaVo);
        $this->typeCommission->DataSource = $data;
        $this->typeCommission->DataBind();
    }

    /**
     * Permet de remplir la liste des statuts des séances
     */
    public function remplirListeStatut()
    {
        $data = (new Atexo_Commission_Seance())->getStatut();
        $this->statutSeance->DataSource = $data;
        $this->statutSeance->DataBind();
    }
}
