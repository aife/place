<?php

namespace Application\Pages\Zabbix;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\SuperviseurServices\Atexo_SuperviseurServices_Facade;

class UrlDeVie extends MpeTPage
{
    public function onInit($param)
    {
        header('Content-Type: application/json');
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }

    public function run($writer)
    {
        $facade = new Atexo_SuperviseurServices_Facade();

        $result = $facade->getResult();

        $this->getResponse()->setContentType('Content-Type: application/json');

        echo json_encode($result, JSON_THROW_ON_ERROR);
    }
}
