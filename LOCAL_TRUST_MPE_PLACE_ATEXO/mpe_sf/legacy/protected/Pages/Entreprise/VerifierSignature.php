<?php

namespace Application\Pages\Entreprise;

use App\Service\Crypto\WebServicesCrypto;
use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Signature;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Crypto\Atexo_Crypto_CurlSend;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Application\Service\Atexo\Signature\Atexo_Signature_InfosVo;
use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Dto\Signature;
use DateTime;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class VerifierSignature extends MpeTPage
{
    public string $_typeSignature = '';

    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->masquerErreur();
        }

        if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
            $this->docVerif->setAfficherProgress(false);
            $this->fichierSignatureAssocie->setAfficherProgress(false);
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @return bool
     */
    public function verifierSignatureFiles($sender, $param)
    {
        if ($this->IsValid) {
            $verificationFaite = false;
            if ($this->docVerif->hasFile()) {
                $signedDocumentFile = Atexo_Config::getParameter('COMMON_TMP').'documentVerif'.session_id().time();
                $signatureFile = Atexo_Config::getParameter('COMMON_TMP').'signatureVerif'.session_id().time();
                $resultHash = Atexo_Config::getParameter('COMMON_TMP').'resultHash'.session_id().time();

                $fileContentBase64 = null;
                $computedHash = null;
                $computedHash256 = null;
                $signatureFileContent = null;
                $signatureFileContentBase64 = null;

                if (copy($this->docVerif->getLocalName(), $signedDocumentFile)) {
                    $msg = Atexo_ScanAntivirus::startScan($signedDocumentFile);

                    if ($msg) {
                        $this->afficherErreur($msg.'"'.$this->docVerif->getFileName().'"', $param);

                        return false;
                    }

                    if ($this->fichierSignatureAssocie->hasFile()) {
                        if (copy($this->fichierSignatureAssocie->getLocalName(), $signatureFile)) {
                            $msg = Atexo_ScanAntivirus::startScan($signatureFile);

                            if ($msg) {
                                $this->afficherErreur($msg.'"'.$this->fichierSignatureAssocie->getFileName().'"', $param);

                                return false;
                            }
                        } else {
                            $this->afficherErreur(Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER_ERREUR'), $param);

                            return false;
                        }

                        $signatureFileContent = (file_get_contents($signatureFile));
                    }
                    //Fin du code scan Antivirus

                    if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                        $typeSignature = $this->typeSignature->getValue();

                        if ($typeSignature == Atexo_Config::getParameter('PADES_SIGNATURE')) {
                            // cas signature XADES
                            $fileContent = file_get_contents($signedDocumentFile);
                            $fileContentBase64 = base64_encode($fileContent);
                            $cheminSignature = $this->docVerif->getFileName();
                            $signaturePath = $this->docVerif->getLocalName();
                        } else {
                            $computedHash = sha1_file($signedDocumentFile);
                            $computedHash256 = hash_file('sha256', $signedDocumentFile);
                            $signatureFileContentBase64 = base64_encode(trim($signatureFileContent));
                            // Appel serveur de validation
                            $cheminSignature = $this->fichierSignatureAssocie->getFileName();
                            $signaturePath = $this->fichierSignatureAssocie->getLocalName();
                        }

                        $dataSourceFromValidationSignatures = self::getResultatVerificationSignature(
                            $typeSignature,
                            $computedHash,
                            $signatureFileContentBase64,
                            $fileContentBase64,
                            $computedHash256,
                            $signaturePath
                        );

                        $data = [];
                        $signatureKo = 0;
                        $signatureUnknown = 0;
                        $signatureOk = 0;

                        if (null != $dataSourceFromValidationSignatures && is_array($dataSourceFromValidationSignatures)) {
                            foreach ($dataSourceFromValidationSignatures as $dataSourceFromValidation) {
                                if ($dataSourceFromValidation instanceof InfosSignature &&
                                    null !== $dataSourceFromValidation->getSignatureValide() &&
                                    null !== $dataSourceFromValidation->getEmetteur() &&
                                    null !== $dataSourceFromValidation->getPeriodiciteValide()
                                ) {
                                    $fichierVo = new Atexo_Signature_FichierVo();
                                    $fichierVo->setCheminSignatureXML($cheminSignature);
                                    $this->blocDetailsSignature->setPathFileJeton($cheminSignature);
                                    $fichierVo->setCheminFichier($this->docVerif->getFileName());
                                    $infosSignature = new Atexo_Signature_InfosVo();
                                    $infosSignature->setInfoFromObject((array) $dataSourceFromValidation);
                                    $fichierVo->setInfosSignature($infosSignature);
                                    Atexo_Signature::fillInfoVoByInfosSiganture($infosSignature, $dataSourceFromValidation);
                                    $data[] = $fichierVo;
                                    $this->fichierverifie->Text = $this->docVerif->getFileName();
                                    $resStatut = $fichierVo->getResultatValiditeSignatureWithMessage();

                                    if ($resStatut['statut'] == Atexo_Config::getParameter('statut_referentiel_certificat_ok')) {
                                        ++$signatureOk;
                                    } elseif ($resStatut['statut'] == Atexo_Config::getParameter('NOK')) {
                                        ++$signatureKo;
                                    } elseif ($resStatut['statut'] == Atexo_Config::getParameter('statut_referentiel_certificat_unknown')) {
                                        ++$signatureUnknown;
                                    }

                                    $this->masquerErreur($param);
                                    $this->resultat->setStyle("display:''");
                                    $this->resultat->render($param->getNewWriter());
                                    $verificationFaite = true;

                                    if ($typeSignature == Atexo_Config::getParameter('PADES_SIGNATURE')) {
                                        $this->scriptJs->setText("<script>J('#ctl0_CONTENU_PAGE_panelDocSignature').hide();J('#panelInfoJeton').hide();</script>");
                                    } else {
                                        $this->scriptJs->setText("<script>J('#ctl0_CONTENU_PAGE_panelDocSignature').show();</script>");
                                    }
                                }
                            }
                        } elseif ( //TODO enlever le elseif quand le nouveau serveur crypto sera mis en prod
                            $dataSourceFromValidationSignatures instanceof InfosSignature &&
                            null !== $dataSourceFromValidationSignatures->getSignatureValide() &&
                            null !== $dataSourceFromValidationSignatures->getEmetteur() &&
                            null !== $dataSourceFromValidationSignatures->getPeriodiciteValide()
                        ) {
                            $fichierVo = new Atexo_Signature_FichierVo();
                            $fichierVo->setCheminSignatureXML($cheminSignature);
                            $this->blocDetailsSignature->setPathFileJeton($cheminSignature);
                            $fichierVo->setCheminFichier($this->docVerif->getFileName());
                            $infosSignature = new Atexo_Signature_InfosVo();
                            $infosSignature->setInfoFromObject((array) $dataSourceFromValidationSignatures);
                            $fichierVo->setInfosSignature($infosSignature);
                            Atexo_Signature::fillInfoVoByInfosSiganture($infosSignature, $dataSourceFromValidationSignatures);
                            $data[] = $fichierVo;
                            $this->fichierverifie->Text = $this->docVerif->getFileName();
                            $resStatut = $fichierVo->getResultatValiditeSignatureWithMessage();

                            if ($resStatut['statut'] == Atexo_Config::getParameter('statut_referentiel_certificat_ok')) {
                                ++$signatureOk;
                            } elseif ($resStatut['statut'] == Atexo_Config::getParameter('NOK')) {
                                ++$signatureKo;
                            } elseif ($resStatut['statut'] == Atexo_Config::getParameter('statut_referentiel_certificat_unknown')) {
                                ++$signatureUnknown;
                            }

                            $this->masquerErreur($param);
                            $this->resultat->setStyle("display:''");
                            $this->resultat->render($param->getNewWriter());
                            $verificationFaite = true;

                            if ($typeSignature == Atexo_Config::getParameter('PADES_SIGNATURE')) {
                                $this->scriptJs->setText("<script>J('#ctl0_CONTENU_PAGE_panelDocSignature').hide();J('#panelInfoJeton').hide();</script>");
                            } else {
                                $this->scriptJs->setText("<script>J('#ctl0_CONTENU_PAGE_panelDocSignature').show();</script>");
                            }
                        } else {
                            if ($typeSignature != Atexo_Config::getParameter('PADES_SIGNATURE')) {
                                $this->resultat->setStyle('display:none');
                                $this->afficherErreur(Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER_ERREUR_CERTIFICAT'), $param);

                                return false;
                            } else {
                                $this->typeSignature->setValue(Atexo_Config::getParameter('INCONNUE_SIGNATURE'));
                                $this->scriptJs->setText("<script>J('#ctl0_CONTENU_PAGE_panelDocSignature').show();</script>");
                            }
                        }

                        if (isset($dataSourceFromValidationSignatures)) {
                            $messageInfoSignature = ($signatureOk + $signatureKo + $signatureUnknown).' signature(s) détectée(s) dont : ';

                            if ($signatureOk > 0) {
                                $this->resultatControleSignature->setCssClass($this->getClassCss(Atexo_Config::getParameter('statut_referentiel_certificat_ok')));
                                $this->statutSignatureFichier->Text = $this->getStatutSignatureFichier(Atexo_Config::getParameter('statut_referentiel_certificat_ok'));
                                $this->signature->setStatut(Atexo_Config::getParameter('statut_referentiel_certificat_ok'));

                                if (0 == $signatureUnknown && 0 == $signatureKo) {
                                    $messageInfoSignature = trim($messageInfoSignature, ' dont : ');
                                }
                            }

                            if ($signatureUnknown > 0) {
                                $this->resultatControleSignature->setCssClass($this->getClassCss(Atexo_Config::getParameter('statut_referentiel_certificat_unknown')));
                                $this->statutSignatureFichier->Text = $this->getStatutSignatureFichier(Atexo_Config::getParameter('statut_referentiel_certificat_unknown'));
                                $this->signature->setStatut(Atexo_Config::getParameter('statut_referentiel_certificat_unknown'));
                                $messageInfoSignature .= $signatureUnknown.' INCERTAINE(S),';
                            }

                            if ($signatureKo > 0) {
                                $this->resultatControleSignature->setCssClass($this->getClassCss(Atexo_Config::getParameter('NOK')));
                                $this->statutSignatureFichier->Text = $this->getStatutSignatureFichier(Atexo_Config::getParameter('NOK'));
                                $this->signature->setStatut(Atexo_Config::getParameter('NOK'));
                                $messageInfoSignature .= $signatureKo.' KO,';
                            }

                            $this->messageInfoSignature->Text = trim($messageInfoSignature, ',');
                            $this->setViewState('infosSignature', $data);

                            $this->blocDetailsSignature->fillRepeater($data);
                        }
                    } else {
                        $pos = strpos($signatureFileContent, 'SignedInfo');

                        if (!$pos) {
                            // cas signature PKCS7
                            if (Atexo_Util::is_base64(trim($signatureFileContent))) {
                                $signatureFileContent = trim($signatureFileContent);
                            }

                            if (!strstr($signatureFileContent, '-----BEGIN PKCS7-----')) {
                                $signatureFileContent = "-----BEGIN PKCS7-----\n".$signatureFileContent."\n-----END PKCS7-----";
                            }

                            Atexo_Util::write_file($signatureFile, $signatureFileContent);
                        } else {
                            // cas signature XADES
                            $this->_typeSignature = 'XML';
                            $computedHash = $this->calculHashSignature($signedDocumentFile, $resultHash);
                            // Appel serveur de validation
                            $json = self::callModuleValidation($computedHash, $signatureFile);
                            $dataSourceFromValidation = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                            $ceritifcatFileName = (new Atexo_Crypto())->getCertificatFromSignature($signatureFileContent, false, true, $this->_typeSignature);

                            if ('Error' == $ceritifcatFileName) {
                                $this->resultat->setStyle('display:none');
                                $this->afficherErreur(Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER_ERREUR_CERTIFICAT'), $param);

                                return false;
                            }

                            if (0 == $dataSourceFromValidation['codeRetour']) {
                                $fichierVo = new Atexo_Signature_FichierVo();
                                $fichierVo->setCheminSignatureXML($this->fichierSignatureAssocie->getFileName());
                                $fichierVo->setCheminFichier($this->docVerif->getFileName());
                                $infosSignature = new Atexo_Signature_InfosVo();
                                $infosSignature->setSignatureValide($dataSourceFromValidation['signatureValide']);

                                if (0 === $dataSourceFromValidation['resultat']['revocation']) {
                                    $infosSignature->setAbsenceRevocationCRL(true);
                                } elseif (2 === $dataSourceFromValidation['resultat']['revocation']) {
                                    $infosSignature->setAbsenceRevocationCRL(false);
                                }

                                if (0 === $dataSourceFromValidation['resultat']['chaineCertification']) {
                                    $infosSignature->setChaineDeCertificationValide(true);
                                } elseif (2 === $dataSourceFromValidation['resultat']['chaineCertification']) {
                                    $infosSignature->setChaineDeCertificationValide(false);
                                }

                                $infosSignature->setRepertoiresChaineCertification($dataSourceFromValidation['resultat']['repertoiresChaineCertification']);
                                $infosSignature = self::getInfosCertificatFromFile($infosSignature, $ceritifcatFileName);
                                $fichierVo->setInfosSignature($infosSignature);
                                $this->blocDetailsSignature->setFichier($fichierVo);
                                $this->fichierverifie->Text = $this->docVerif->getFileName();
                                $resStatut = $fichierVo->getResultatValiditeSignatureWithMessage();
                                $this->resultatControleSignature->setCssClass($this->getClassCss($resStatut['statut']));
                                $this->statutSignatureFichier->Text = $this->getStatutSignatureFichier($resStatut['statut']);
                                $this->signature->setStatut($resStatut['statut']);
                                $this->masquerErreur($param);
                                $this->resultat->setStyle("display:''");
                                $this->setViewState('infosSignature', $fichierVo);
                                $verificationFaite = true;
                            } else {
                                if ($dataSourceFromValidation['message']) {
                                    $filePath = 'serveurValidation.log';
                                    $log = '[ '.date('d-M-Y H:i:s').' ] : '.$dataSourceFromValidation['message']."\n";
                                    Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
                                }
                            }
                        }
                    }
                } else {
                    $this->afficherErreur(Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER_ERREUR'), $param);

                    return false;
                }

                $this->masquerErreur($param);
                $this->resultat->setStyle("display:''");

                if ($verificationFaite) {
                    $this->docVerif->refrechCompsant();
                    $this->fichierSignatureAssocie->refrechCompsant();
                    $this->clean->setText("<script>J('#ctl0_CONTENU_PAGE_docVeriffileName').html(\"\");J('#ctl0_CONTENU_PAGE_fichierSignatureAssociefileName').html(\"\");J(\"#ctl0_CONTENU_PAGE_panelDocSignature\").hide();</script>");
                    if (file_exists($this->docVerif->getLocalName())) {
                        @unlink($this->docVerif->getLocalName());
                    }
                    if (file_exists($this->fichierSignatureAssocie->getLocalName())) {
                        @unlink($this->fichierSignatureAssocie->getLocalName());
                    }
                }

                @unlink($signedDocumentFile);
                @unlink($signatureFile);
                @unlink($resultHash);
            } else {
                $this->resultat->setStyle('display:none');
                $this->afficherErreur(Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER'), $param);

                return false;
            }
        }
    }

    /**
     * @param $msg
     * @param null $param
     */
    public function afficherErreur($msg, $param = null)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setDisplay('Dynamic');

        if ($param) {
            $this->activePanelErreur->render($param->getNewWriter());
        }
    }

    /**
     * @param null $param
     */
    public function masquerErreur($param = null)
    {
        $this->panelMessageErreur->setMessage('');
        $this->panelMessageErreur->setDisplay('None');

        if ($param) {
            $this->activePanelErreur->render($param->getNewWriter());
        }
    }

    /**
     * @param $computedHash
     * @param $signatureFile
     *
     * @return mixed|string
     */
    public function callModuleValidation($computedHash, $signatureFile)
    {
        $postData = [];
        try {
            $postData['contenuFichierXML'] = base64_encode(trim(file_get_contents($signatureFile)));
            $postData['hashFichier'] = $computedHash;

            $postData['typeEchange'] = 'VerificationSignatureServeur';
            $postData['item'] = Atexo_Config::getParameter('ORIGINE_ITEM');
            $postData['organisme'] = Atexo_Config::getParameter('ORIGINE_ORGANISME');
            $postData['plateforme'] = Atexo_Config::getParameter('ORIGINE_PLATEFORME');
            $postData['typeAlgorithmHash'] = Atexo_Config::getParameter('TYPE_ALGORITHM_HASH');
            $postData['contexteMetier'] = 'MPE_Verification_jeton';

            return (new Atexo_Crypto_CurlSend())->sendDataToModuleValidation($postData);
        } catch (Exception $e) {
            Prado::log($e->getMessage());

            return '3-3-3##3';
        }
    }

    /**
     * @param $type
     * @param null $computedHash
     * @param null $signatureFile
     * @param null $signedFile
     * @param null $computedHash256
     * @param null $cheminSignature
     *
     *
     * @throws Atexo_Config_Exception
     */
    public function getResultatVerificationSignature(
        $type,
        $computedHash = null,
        $signatureFile = null,
        $signedFile = null,
        $computedHash256 = null,
        $cheminSignature = null
    ) {
        $logger = Atexo_LoggerManager::getLogger('crypto');

        $service = new Atexo_Crypto_Service(
            Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')),
            $logger,
            Atexo_Config::getParameter('ENABLE_SIGNATURE_SERVER_V2'),
            Atexo_Config::getParameter('UID_PF_MPE'),
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );

        try {
            $logger->info('Appele au ws de vérification de la signature de type =>'.$type);

            return match ($type) {
                Atexo_Config::getParameter('INCONNUE_SIGNATURE') => $service->verifierSignature($computedHash, $signatureFile, Atexo_Config::getParameter('INCONNUE_SIGNATURE'), $computedHash256),
                Atexo_Config::getParameter('PADES_SIGNATURE') => $service->verifierSignaturePades($cheminSignature),
                default => false,
            };
        } catch (\AtexoCrypto\Exception\ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement ".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return false;
    }

    /**
     * Permet la génération et le téléchargement du rapport de vérification des signatures.
     */
    public function printRapportSignatureDocument()
    {
        $infoSignature = [];
        $fichiersVo = $this->getViewState('infosSignature');

        $html = '<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8"/>
<style>
/* Body default value for font-family */
body {
    font-family: Arial;
}

/* Title for pdf file */
.title-pdf {
    margin-bottom:12px;
    color: #ffffff;
    background-color: #0078ff;
    border-color: #0078ff;
    padding:10px 50px;
    width: 100%;
    text-align:center;
}

/* Table with file informations */
.file-table {
    width: 100%;
    border-collapse:collapse;
    margin-bottom: 12px;
}

.file-td-title {
    border: 1px solid #b7b7b7;
    width: 30%;
    padding: 7px;
    background-color: #eaeaea;
    color: #31708f;
}

.file-td-content {
    border: 1px solid #b7b7b7;
    width: 70%;
    padding: 7px;
}

/* bloc signature */
.info-border {
    border:1px solid #ddd;
    border-radius: 4px;
    padding: 0px 15px;
}

.info-title {
    margin: 10px;
    border-bottom: 1px solid #eeeeee;
    padding-bottom: 7px;
}

/* table design in bloc signature */
.table-info {
    margin-bottom: 10px;
    background-color: #fff;
    border: 1px solid #0078ff;
    border-radius: 4px;
}

.table-title {
    color: #ffffff;
    background-color: #0078ff;
    border-color: #0078ff;
    padding: 10px 15px;
}

/* Other */
.padding-10-15 {
    padding: 10px 15px 0 15px;
}

.margin-top-0 {
    margin-top: 0;
}

.margin-left-10 {
    margin-left: 10px;
}

.flex {
    display: flex;
}

.width-100-percentage {
    width: 100%;
}

.width-50-percentage {
    width: 50%;
}

.float-right {
    float: right;
}

.font-size-11 {
    font-size: 11px;
}

/* Saut de page */
.break-before {
    page-break-before: always;
}
</style>
</head>
<body>
<table class="title-pdf">
<tr>
<td><strong>Rapport de vérification de signature</strong></td>
</tr>
</table>
<table class="file-table font-size-11">
<tr>
<td class="file-td-title">Nom du fichier principal</td>
<td class="file-td-content">'.$fichiersVo[0]->getCheminFichier().'</td>
</tr>
<tr>
<td class="file-td-title">Nom du fichier de signature</td>
<td class="file-td-content">'.$fichiersVo[0]->getCheminSignatureXML().'</td>
</tr>
</table>';

        foreach ($fichiersVo as $key => $fichier) {
            ++$key;
            $infoSignature = self::getInfosSignatureforWkHtmlToPdf($fichier);

            if ($key > 1) {
                $html .= '<div class="info-border break-before">';
            } else {
                $html .= '<div class="info-border">';
            }

            $html .= '<div class="info-title">
Signature '.$key.'
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Signataire
</div>
<div class="padding-10-15">
<p class="margin-top-0">CN : '.$infoSignature['cnSignataire'].'</p>
<p>E : '.$infoSignature['emailSignataire'].'</p>
<p>OU : '.$infoSignature['ouSignataire'].'</p>
<p>O : '.$infoSignature['oSignataire'].'</p>
<p>C : '.$infoSignature['cSignataire'].'</p>
</div>
</div>
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Emetteur du certificat
</div>
<div class="padding-10-15">
<p class="margin-top-0">CN : '.$infoSignature['cnEmetteur'].'</p>
<p>OU : '.$infoSignature['ouEmetteur'].'</p>
<p>O : '.$infoSignature['oEmetteur'].'</p>
<p>C : '.$infoSignature['cEmetteur'].'</p>
</div>
</div>
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Date de validité de certificat
</div>
<div class="padding-10-15">
<p class="margin-top-0">A partir du : '.$infoSignature['dateValiditeDu'].'</p>
<p>Jusqu\'au : '.$infoSignature['dateValiditeAu'].'</p>
</div>
</div>
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Contrôles de validité du certificat
</div>
<div class="padding-10-15">
<p class="margin-top-0">Contrôles réalisés le '.$infoSignature['dateVerification'].'</p>
<p>Période de validité : <img src="'.Atexo_Config::getParameter('ATEXO_MPE_PATH').'/protected/themes/images/'.$infoSignature['statutPeriodeValiditeCert'].'"/></p>
<p>Non révocation : <img src="'.Atexo_Config::getParameter('ATEXO_MPE_PATH').'/protected/themes/images/'.$infoSignature['statutRevocationCert'].'"/></p>
<p>Chaîne de certification : <img src="'.Atexo_Config::getParameter('ATEXO_MPE_PATH').'/protected/themes/images/'.$infoSignature['statutVerifChaineCert'].'"/></p>
<p> - '.$infoSignature['referentielCertificat'].'</p>
</div>
</div>
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Contrôle de l\'intégrité du fichier signé
</div>
<div class="padding-10-15">
<p class="margin-top-0">Contrôles réalisés le '.$infoSignature['dateVerification'].'</p>
<p>Non répudiation / Intégrité : <img src="'.Atexo_Config::getParameter('ATEXO_MPE_PATH').'/protected/themes/images/'.$infoSignature['statutRejeuSignature'].'"/></p>
</div>
</div>
</div>
<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
Résultat du contrôle de la signature du fichier
</div>
<div class="padding-10-15">
<p class="margin-top-0">'.$infoSignature['statutSignatureFichier'].'</p>
</div>
</div>
</div>';
            if (isset($infoSignature['hasInfoComp']) && $infoSignature['hasInfoComp']) {
                $html .= '<div class="width-100-percentage font-size-11">
<div class="table-info">
<div class="table-title">
'.Prado::localize('INFOROMATIONS_COMPLEMANTAIRES_SIGNATURE').'
</div>
<div class="padding-10-15">
<p class="margin-top-0">'.Prado::localize('CERTIFICAT_SIGNATURE').' : '.$infoSignature['certificatSignature'].($infoSignature['certificatSignatureInfoBulle'] ? '('.$infoSignature['certificatSignatureInfoBulle'].')' : '').'</p>
<p class="margin-top-0">'.Prado::localize('FORMAT_SIGNATURE').' : '.$infoSignature['formatSignature'].'</p>
<p class="margin-top-0">'.Prado::localize('DATE_INDICATIVE').' : '.$infoSignature['dateIndicative'].'</p>
<p class="margin-top-0">'.Prado::localize('JETON_SIGNATURE_HORODATE').' : '.$infoSignature['jetonSignatureHorodate'].'</p>
</div>
</div>
</div>';
            }
            $html .= '</div>';
        }

        if (isset($infoSignature)) {
            $html .= '<footer>
<p class="float-right font-size-11">Document édité le '.$infoSignature['dateVerification'].'</p>
</footer>
</body>
</html>';
            $nomFichierPdf = Atexo_Config::getParameter('COMMON_TMP').'Rapport_'.uniqid().'.pdf';
            $tmpHtmlFile = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.uniqid().'.html';

            Atexo_Util::write_file($tmpHtmlFile, $html);

            $cmd = Atexo_Config::getParameter('PATH_BIN_WKHTMLPDF').' '.$tmpHtmlFile.' '.$nomFichierPdf;
            exec($cmd);

            $pdfContent = file_get_contents($nomFichierPdf);
            @unlink($tmpHtmlFile);
            @unlink($nomFichierPdf);
            DownloadFile::downloadFileContent(
                Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE').'.pdf',
                $pdfContent
            );
        }
    }

    /**
     * @param $fichier
     *
     * @return array
     */
    public function getInfosSignatureforWkHtmlToPdf($fichier)
    {
        $data = [];
        $infoSignature = $fichier->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            $data['fileName'] = $fichier->getCheminFichier();
            $data['signatureName'] = $fichier->getCheminSignatureXML();

            if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                $email = $infoSignature->getAlternativeNameSignataire();
            } else {
                $email = $infoSignature->getEmailSignataire();
            }

            $data['emailSignataire'] = $email;
            $data['cnSignataire'] = $infoSignature->getCnSignataire();
            $data['ouSignataire'] = $infoSignature->getOuSignataire();
            $data['oSignataire'] = $infoSignature->getOSignataire();
            $data['cSignataire'] = $infoSignature->getCnSignataire();
            $data['cnEmetteur'] = $infoSignature->getCnEmetteur();
            $data['ouEmetteur'] = $infoSignature->getOuEmetteur();
            $data['oEmetteur'] = $infoSignature->getOEmetteur();
            $data['cEmetteur'] = $infoSignature->getCEmetteur();
            $data['statutPeriodeValiditeCert'] = self::getPictoByValidite($fichier->getStatutPeriodeValiditeCert());
            $data['statutVerifChaineCert'] = self::getPictoByValidite($fichier->getStatutVerifChaineCert());

            if (Atexo_Module::isEnabled('SurchargeReferentiels')) {
                $referentielCertificat = Prado::localize('DEFINE_REFERENTIEL_DERTIFICAT').' : '.$fichier->getNomReferentielCertificat(true);
            } else {
                $referentielCertificat = '';
            }

            $data['referentielCertificat'] = $referentielCertificat;
            $data['statutRevocationCert'] = self::getPictoByValidite($fichier->getStatutRevocationCert());
            $data['statutRejeuSignature'] = self::getPictoByValidite($fichier->getStatutRejeuSignature());

            $data['dateValiditeDu'] = (new DateTime($infoSignature->getDateValiditeDu()))->format('Y-m-d H:i:s');
            $data['dateValiditeAu'] = (new DateTime($infoSignature->getDateValiditeAu()))->format('Y-m-d H:i:s');
            $data['dateVerification'] = (new DateTime())->format('Y-m-d H:i:s');

            $resStatut = $fichier->getResultatValiditeSignatureWithMessage();
            $data['statutSignatureFichier'] = $this->getStatutSignatureFichier($resStatut['statut']);
            $data['hasInfoComp'] = $infoSignature->hasInformationsComplementaires();
            $data['certificatSignature'] = $infoSignature->getInfoCertificatSignature();
            $data['certificatSignatureInfoBulle'] = $infoSignature->getInfoBulleCertificatSignature();
            $data['formatSignature'] = $infoSignature->getFormatSignature();
            $data['dateIndicative'] = $infoSignature->getDateIndicative();
            $data['jetonSignatureHorodate'] = $infoSignature->getInfoHorodatageSignature();
        }

        return $data;
    }

    /**
     * @param $infosSignature
     * @param $ceritifcatFileName
     *
     * @return mixed
     */
    public function getInfosCertificatFromFile($infosSignature, $ceritifcatFileName)
    {
        $infosSignature->setDateValiditeDu((new Atexo_Crypto_Certificat())->getDateCertficateValidFrom($ceritifcatFileName));
        $infosSignature->setDateValiditeAu((new Atexo_Crypto_Certificat())->getDateCertficateValidTo($ceritifcatFileName));

        $infosSignature->setEmailSignataire((new Atexo_Crypto_Certificat())->getEmailCertificatFromFile($ceritifcatFileName));
        $infosSignature->setCnSignataire((new Atexo_Crypto_Certificat())->getCnCertificatFromFile($ceritifcatFileName));
        $infosSignature->setOuSignataire((new Atexo_Crypto_Certificat())->getOuCertificatFromFile($ceritifcatFileName));
        $infosSignature->setOSignataire((new Atexo_Crypto_Certificat())->getOCertificatFromFile($ceritifcatFileName));
        $infosSignature->setCSignataire((new Atexo_Crypto_Certificat())->getCCertificatFromFile($ceritifcatFileName));

        $infosSignature->setCnEmetteur((new Atexo_Crypto_Certificat())->getCnEmetteurCertificatFromFile($ceritifcatFileName));
        $infosSignature->setOuEmetteur((new Atexo_Crypto_Certificat())->getOuEmetteurCertificatFromFile($ceritifcatFileName));
        $infosSignature->setOEmetteur((new Atexo_Crypto_Certificat())->getOEmetteurCertificatFromFile($ceritifcatFileName));
        $infosSignature->setCEmetteur((new Atexo_Crypto_Certificat())->getCEmetteurCertificatFromFile($ceritifcatFileName));

        $signataire = '';
        $signataire .= 'E : '.$infosSignature->getEmailSignataire().', ';
        $signataire .= 'CN : '.$infosSignature->getCnSignataire().', ';
        $signataire .= 'OU : '.$infosSignature->getOuSignataire().', ';
        $signataire .= 'O : '.$infosSignature->getOSignataire().', ';
        $signataire .= 'C :'.$infosSignature->getCSignataire();
        $infosSignature->setSignataireComplet($signataire);

        $emetteur = '';
        $emetteur .= 'CN : '.$infosSignature->getCnEmetteur().', ';
        $emetteur .= 'OU : '.$infosSignature->getOuEmetteur().', ';
        $emetteur .= 'O : '.$infosSignature->getOEmetteur().', ';
        $emetteur .= 'C : '.$infosSignature->getCEmetteur();
        $infosSignature->setEmetteur($emetteur);

        return $infosSignature;
    }

    /**
     * @param $statut
     *
     * @return string
     */
    public function getClassCss($statut)
    {
        return match (strtoupper($statut)) {
            Atexo_Config::getParameter('OK') => 'text-success statut-resultat statut-valide',
            Atexo_Config::getParameter('NOK') => 'text-danger statut-resultat statut-erreur',
            Atexo_Config::getParameter('UNKNOWN') => 'text-warning statut-resultat statut-avertissement',
            Atexo_Config::getParameter('SANS') => 'text-default statut-resultat statut-vide',
            default => 'statut-resultat statut-vide',
        };
    }

    /**
     * @param $statut
     *
     * @return mixed|string
     */
    public function getStatutSignatureFichier($statut)
    {
        return match (strtoupper($statut)) {
            Atexo_Config::getParameter('OK') => Prado::localize('DEFINE_FICHIER_SIGNE_SIGNATURE_VALIDE'),
            Atexo_Config::getParameter('NOK') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INVALIDE'),
            Atexo_Config::getParameter('UNKNOWN') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INCERTAINE'),
            Atexo_Config::getParameter('SANS') => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
            default => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
        };
    }

    /**
     * retourne le nom de l'image selon l'etat de validité.
     *
     * @param $validite
     *
     * @return string
     */
    public function getPictoValidite($validite)
    {
        return match ($validite) {
            'OK' => 'valide.png',
            'KO' => 'invalide.png',
            'INCONNUE' => 'incertaine.png',
            'UNKNOWN' => 'incertaine.png',
            'SANS' => 'nonSigne.png',
            default => 'invalide.png',
        };
    }

    /**
     * Permet de verfier l'extension de fichier de signature.
     *
     * @param $sender
     * @param $param
     */
    public function verifyDocAttache($sender, $param)
    {
        $extensionSignatureFile = strtolower(Atexo_Util::getExtension($this->fichierSignatureAssocie->getFileName()));
        $erreur = false;
        $messagesErreur = '';

        if (!$this->docVerif->hasFile()) {
            $erreur = true;
            $messagesErreur = Prado::localize('DEFINE_TEXT_FICHIERS_A_VERIFIER');
        }

        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
            if ($extensionSignatureFile != Atexo_Config::getParameter('XML_EXTENTION')) {
                $erreur = true;
                $messagesErreur = Prado::localize('FORMAT_FICHIER_SIGNATURE_INVALIDE');
            }
        }

        if ($erreur) {
            $param->IsValid = false;
            $scriptJs = "<script>document.getElementById('validerAjoutDoc').style.display='';</script>";
            $this->Page->validerAjoutDoc->addServerError($messagesErreur, false);
        } else {
            $scriptJs = "<script>document.getElementById('validerAjoutDoc').style.display='none';</script>";
        }

        $this->scriptJs->setText($scriptJs);
    }

    /**
     * @param $signedDocumentFile
     * @param $resultHash
     *
     * @return string
     */
    public function calculHashSignature($signedDocumentFile, $resultHash)
    {
        //Debut calcul hash file
        system('sha1sum '.$signedDocumentFile.' > '.$resultHash.' 2>&1');

        return strtoupper(substr(file_get_contents($resultHash), 0, 40));
        //Fin calcul hash file
    }

    /**
     * retourne le nom de l'image selon l'etat de validité.
     *
     * @param $validite
     *
     * @return string
     */
    public function getPictoByValidite($validite)
    {
        $picto = match ($validite) {
            'OK' => 'picto-check-ok-small.gif',
            'KO' => 'picto-check-not-ok.gif',
            'INCONNUE' => 'picto-validation-incertaine.gif',
            'UNKNOWN' => 'picto-validation-incertaine.gif',
            'SANS' => 'picto-vide.gif',
            default => 'picto-check-not-ok.gif',
        };
        $this->assetPicture($picto);

        return $picto;
    }

    /**
     * @param $picto
     *
     * @throws Atexo_Config_Exception
     */
    protected function assetPicture($picto)
    {
        $repPicture = Atexo_Config::getParameter('ATEXO_MPE_PATH').'/themes/images/';
        if (!file_exists($repPicture.$picto)) {
            $urlPf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
            $urlPicture = $urlPf.'/themes/images/'.$picto;
            file_get_contents($urlPicture);
        }
    }
}
