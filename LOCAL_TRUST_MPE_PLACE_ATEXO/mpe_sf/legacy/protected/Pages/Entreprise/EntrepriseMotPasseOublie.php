<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Page de récupération du mot de passe d'un inscrit.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseMotPasseOublie extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }
}
