<div class="poserQuestion">
    <!--BEGIN PANEL FORMULAIRE DEMANDE-->
    <div class="form-field">


        <div class="alert alert-info">
            <h2 class="h4">
                <com:TLabel id="titreFormulaire"/>
            </h2>

            <com:TActiveLabel Text="This is a label" ID="descriptionConditions"/>
            <com:TPanel id="descriptionAnonyme">
                <com:TPanel ID="blocInscriptionAuthentification">
                    <div class="check-bloc">
                        <com:TRadioButton ID="inscriptionAuthentification" GroupName="RadioGroup" cssClass="check-bloc" Attributes.onclick="gererBlocCoordonnees()"/>
                        <label for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_inscriptionAuthentification">
                            <com:TLabel id="textInscriptionAuthentification"></com:TLabel>
                        </label>
                    </div>
                </com:TPanel>

                <com:TPanel ID="blocChoixTelechargement">
                    <div>
                        <com:TRadioButton ID="choixTelechargement" GroupName="RadioGroup" cssClass="check-bloc" Attributes.onclick="gererBlocCoordonnees()"/>
                        <label for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixTelechargement">
                            <com:TLabel id="completerFormulaireDemande"></com:TLabel>
                        </label>
                    </div>
                </com:TPanel>
                <com:TPanel ID="blocChoixAnonyme">
                    <div>
                        <com:TRadioButton ID="choixAnonyme" GroupName="RadioGroup" cssClass="check-bloc" Attributes.onclick="gererBlocCoordonnees()"/>
                        <label for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_choixAnonyme">
                            <com:TTranslate>TELECHERGER_ANONYME_DCE</com:TTranslate>
                        </label>
                    </div>
                </com:TPanel>
            </com:TPanel>

        </div>
        <com:TPanel ID="libelleConditionsFormulaireDemandePart2" cssClass="alert alert-danger">
            <com:TTranslate>DEFINE_CONDITIONS_FORMULAIRE_DEMANDE_PART_2</com:TTranslate>
        </com:TPanel>
        <!--BEGIN CONDITION D'UTILISATION-->
        <div ID="boutonAcceptationCGU">
            <div class="checkbox">
                <label for="<%=$this->accepterConditions->getClientID()%>">
                    <com:TCheckBox id="accepterConditions" Attributes.title="<%=Prado::localize('TEXT_ACCEPTATION_CONDITIONS_GENERALES_UTILISATION')%>"/>
                    <%=Prado::localize('DEFINE_ACCEPTATION_REGLEMENT_ENTREPRISE')%>
                </label>

                <com:THiddenField ID="clientId" Value="<%=$this->getClientId()%>"/>
                <com:TCustomValidator
                        ClientValidationFunction="accepterConditionsValidator"
                        ID="conditionsValidator"
                        ControlToValidate="clientId"
                        ValidationGroup="ValidateInfoUser"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEVOIR_ACCEPTER_CGU')%>"
                        Text=" "
                >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('divValidationSummary').style.display='';
                        document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCustomValidator>
            </div>
        </div>
        <!--END CONDITION D'UTILISATION-->
    </div>
    <!--END PANEL FORMULAIRE DEMANDE-->

    <!--BEGIN MES COORDONNEES-->
    <com:TPanel id="blocMesCoordonnees" cssClass="form-field form-horizontal" style="display:block;">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="h3 panel-title">
                    <com:TTranslate>DEFINE_MES_COORDONNEES</com:TTranslate>
                </h2>
            </div>
            <div class="panel-body">
                <!--BEGIN INFO PERSONNELLES-->
                <div class="clearfix">
                    <!--BEGIN NOM -->
                    <fieldset>
                    <div class="form-group form-group-sm no-margin-right-left row">
                        <legend for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_nom" class="control-label col-md-2">
                            <com:TTranslate>TEXT_NOM_MAJ</com:TTranslate>
                            <span class="text-danger">* </span> :
                        </legend>
                        <div class="col-md-10">
                            <com:TTextBox id="nom" CssClass="form-control" Attributes.title="<%=Prado::localize('NOM')%>"/>
                            <com:TCustomValidator
                                    ClientValidationFunction="nomValidatorDownloadDce"
                                    ID="nomValidator"
                                    ControlToValidate="clientId"
                                    ValidationGroup="ValidateInfoUser"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('TEXT_NOM_MAJ')%>"
                                    Text="<span class='text-danger check-invalide' title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>'><i class='fa fa-times-circle-o p-r-1' aria-hidden='true'></i><%=Prado::localize('CHAMPS_OBLIGATOIRE')%></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                    </div>
                    </fieldset>
                    <!--END NOM -->

                    <!--BEGIN PRENOM-->
                    <fieldset>
                    <div class="form-group form-group-sm">
                        <legend class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_prenom">
                            <com:TTranslate>PRENOM</com:TTranslate>
                            <span class="text-danger">* </span> :
                        </legend>
                        <div class="col-md-10">
                            <com:TTextBox CssClass="form-control" id="prenom" Attributes.title="<%=Prado::localize('PRENOM')%>"/>
                            <com:TCustomValidator
                                    ClientValidationFunction="prenomValidatorDownloadDce"
                                    ControlToValidate="clientId"
                                    ID="prenomValidator"
                                    ValidationGroup="ValidateInfoUser"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('PRENOM')%>"
                                    Text="<span class='text-danger check-invalide' title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>'><i class='fa fa-times-circle-o p-r-1' aria-hidden='true'></i><%=Prado::localize('CHAMPS_OBLIGATOIRE')%></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                    </div>
                    </fieldset>
                    <!--END PRENOM-->

                    <!--BEGIN ADRESSE MAIL-->
                    <fieldset>
                    <div class="form-group form-group-sm">
                        <legend class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_email">
                            <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
                            <span class="text-danger">* </span> :
                        </legend>
                        <div class="col-md-10">
                            <com:TTextBox CssClass="form-control" id="email" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"/>
                            <com:TCustomValidator
                                    ClientValidationFunction="emailValidatorDownloadDce"
                                    ID="emailValidator"
                                    ControlToValidate="clientId"
                                    ValidationGroup="ValidateInfoUser"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
                                    Text="<span title='<%=Prado::localize('EMAIL_FORMAT_ERROR')%>' class='text-danger pull-left'><i class='fa fa-times-circle-o p-r-1' aria-hidden='true'></i><%=Prado::localize('EMAIL_FORMAT_ERROR')%></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                    </div>
                    </fieldset>
                    <!--END ADRESSE MAIL-->

                    <!--BEGIN RAISON SOCIALE-->
                    <fieldset>
                    <div class="form-group form-group-sm">
                        <legend class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_raisonSocial">
                            <com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate>
                            :
                        </legend>
                        <div class="col-md-10">
                            <com:TTextBox CssClass="form-control" id="raisonSocial" Attributes.title="<%=Prado::localize('TEXT_RAISON_SOCIALE')%>"/>
                        </div>
                    </div>
                    </fieldset>
                    <!--END RAISON SOCIALE-->
                    <hr/>
                </div>
                <!--END INFO PERSONNELLES-->

                <!--BEGIN RADIO BTN-->
                <div class="clearfix">
                    <!--BEGIN ENTREPRISE ETABLIE EN FRANCE-->
                    <div class="">
                        <div class="radio">
                            <label for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_france">
                                <com:TRadioButton GroupName="etablissementEntreprise" id="france" checked="true" Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETABLIE_FRANCE')%>" CssClass="radio"
                                                  Attributes.onclick="isCheckedShowDiv(this,'etablieFrance');isCheckedHideDiv(this,'nonEtablieFrance');" />
                                <com:TTranslate>TEXT_ENTREPRISE_ETABLIE_FRANCE</com:TTranslate>
                            </label>
                        </div>
                        <div id="etablieFrance" style="display: block;">
                            <fieldset>
                            <com:TPanel ID="panelSiren">
                                <div class="form-group form-group-sm m-t-2">
                                    <legend class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_siren">
                                        <com:TTranslate>TEXT_SIREN</com:TTranslate>
                                        <com:TTranslate>TEXT_SLASH</com:TTranslate>
                                        <com:TTranslate>TEXT_SIRET</com:TTranslate>
                                        :
                                    </legend>
                                    <div class="col-md-10 form-inline">
                                        <com:TTextBox CssClass="form-control col-md-6" id="siren" Attributes.title="SIREN" MaxLength="9"/>
                                        <com:TTextBox CssClass="form-control col-md-6" id="siret" MaxLength="5" Attributes.title="SIRET"/>
                                        <com:TCustomValidator
                                                ID="validatorSiren"
                                                ValidationGroup="ValidateInfoUser"
                                                ControlToValidate="siren"
                                                ClientValidationFunction="validateSirenForLocalCompany"
                                                ErrorMessage="<%=Prado::localize('SIRET_INVALIDE')%>"
                                                Display="Dynamic"
                                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        >
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                                document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                        <i class="fa fa-question-circle text-info p-l-1" data-toggle="tooltip" data-placement="right" title="<%=Prado::localize('TEXT_INFO_BULLE_SIREN')%>"></i>
                                    </div>
                                </div>
                            </com:TPanel>
                            </fieldset>
                            <com:TPanel ID="panelRc">
                                <div class="intitule-120 indent-30">
                                    <label for="<%=$this->getClientId()%>villeRc">
                                        <com:TTranslate>DEFINE_VILLE_RC</com:TTranslate>
                                        :</label>
                                </div>
                                <com:TDropDownList ID="RcVille" Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>" CssClass="select-185"/>
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule"
                                     alt="Info-bulle" title="Info-bulle"/>
                                <div class="breaker"></div>
                                <div class="intitule-120 indent-30">
                                    <label for="<%=$this->getClientId()%>numeroRc">
                                        <com:TTranslate>DEFINE_NUMERO</com:TTranslate>
                                    </label> :
                                </div>
                                <com:TTextBox id="RcNumero" CssClass="input-185" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>"/>
                                <com:THiddenField ID="Rc" Value="<%=$this->Rc->getClientId()%>"/>
                                <com:TCustomValidator
                                        ID="rcValidator"
                                        ValidationGroup="ValidateInfoUser"
                                        ControlToValidate="Rc"
                                        ErrorMessage="<%=Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC')%>"
                                        Display="Dynamic"
                                        Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        ClientValidationFunction="controlValidationNumRc">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </com:TPanel>
                            <com:TPanel ID="panelIdentifiantUnique">
                                <div class="intitule-120 indent-30">
                                    <div class="float-left">
                                        <label for="siren" style="display:block;">
                                            <com:TTranslate>TEXT_SIREN</com:TTranslate>
                                            :</label>
                                    </div>
                                </div>
                                <com:TTextBox id="identifiantUnique" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" CssClass="input-185"/>
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule"
                                     alt="Info-bulle" title="Info-bulle"/>
                            </com:TPanel>
                        </div>
                    </div>
                    <!--END ENTREPRISE ETABLIE EN FRANCE-->

                    <!--BEGIN ENTREPRISE NON ETABLIE EN FRANCE-->
                    <div class="">
                        <div class="radio">
                            <label for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_etranger">
                                <com:TRadioButton GroupName="etablissementEntreprise"
                                                  Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETRANGER')%>"
                                                  id="etranger"
                                                  Attributes.onclick="isCheckedShowDiv(this,'nonEtablieFrance');isCheckedHideDiv(this,'etablieFrance');"/>
                                <com:TTranslate>TEXT_ENTREPRISE_ETRANGER</com:TTranslate>
                            </label>
                        </div>
                        <div class="m-t-1 clearfix" id="nonEtablieFrance" style="display:none;">
                            <div class="form-group form-group-sm">
                                <label class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_pays">
                                    <com:TTranslate>TEXT_COUNTRY</com:TTranslate>
                                    :
                                </label>
                                <div class="col-md-10">
                                    <com:DropDownListCountries id="pays"
                                                               CssClass="form-control"
                                                               TypeAffichage="withoutCode"
                                                               Attributes.title="<%=Prado::localize('TEXT_PAYS')%>"/>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-md-2" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_idNational">
                                    <com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate>
                                    :
                                </label>
                                <div class="col-md-10">
                                    <com:TTextBox id="idNational"
                                                  CssClass="form-control"
                                                  Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>"/>
                                    <span class="example small">
									<com:TTranslate>TEXT_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate>
								</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END ENTREPRISE NON ETABLIE EN FRANCE-->
                    <hr/>
                </div>
                <!--END RADIO BTN-->

                <!--BEGIN INFO ADRESSE-->
                <div class="clearfix form-group form-group-sm">
                    <div class="col-md-6">
                        <!--BEGIN ADRESSE-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-4" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_address">
                                <com:TTranslate>TEXT_ADRESSE</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-8">
                                <com:TTextBox CssClass="form-control" id="address"/>
                            </div>
                        </div>
                        </fieldset>
                        <!--END ADRESSE-->

                        <!--BEGIN ADRESSE SUITE-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-4" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_address2">
                                <com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-8">
                                <com:TTextBox CssClass="form-control" id="address2"/>
                            </div>
                        </div>
                        </fieldset>
                        <!--END ADRESSE SUITE-->

                        <!--BEGIN TELEPHONE-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-4" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_tel">
                                <com:TTranslate>TEXT_TELEPHONE</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-8">
                                <com:TTextBox CssClass="form-control" id="tel" Attributes.placeholder="<%=Prado::localize('TEXT_HUIT_CHIFFRES_SANS_ESPACE')%>"/>
                                <small class="example">
                                    <com:TLabel ID="labelInfoTel" cssClass="info-aide-right" Text="<%=Prado::localize('TEXT_HUIT_CHIFFRES_SANS_ESPACE')%>"/>
                                </small>
                            </div>
                            <com:TCustomValidator
                                    ID="nombreCaracteresNumTel"
                                    ValidationGroup="ValidateInfoUser"
                                    ControlToValidate="tel"
                                    ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                    Display="Dynamic"
                                    ErrorMessage="<%=str_replace('XXX', Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_TEL'))%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                    EnableClientScript="true"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        </fieldset>
                        <!--END TELEPHONE-->
                    </div>
                    <div class="col-md-6">
                        <!--BEGIN CODE POSTALE-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-3" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_cp">
                                <com:TTranslate>TEXT_CP</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-9">
                                <com:TTextBox CssClass="form-control" id="cp"/>
                            </div>
                        </div>
                        </fieldset>
                        <!--END CODE POSTALE-->

                        <!--BEGIN VILLE-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-3" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_ville">
                                <com:TTranslate>TEXT_VILLE</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-9">
                                <com:TTextBox CssClass="form-control" id="ville"/>
                            </div>
                        </div>
                        </fieldset>
                        <!--END VILLE-->

                        <!--BEGIN FAX-->
                        <fieldset>
                        <div class="form-group form-group-sm">
                            <legend class="control-label col-md-3" for="ctl0_CONTENU_PAGE_EntrepriseFormulaireDemande_fax">
                                <com:TTranslate>TEXT_FAX</com:TTranslate>
                                :
                            </legend>
                            <div class="col-md-9">
                                <com:TTextBox CssClass="form-control" id="fax"/>
                                <small class="example">
                                    <com:TLabel ID="labelInfoFax" cssClass="info-aide-right" Text="<%=Prado::localize('TEXT_HUIT_CHIFFRES_SANS_ESPACE')%>"/>
                                </small>
                            </div>
                            <com:TCustomValidator
                                    ID="nombreCaracteresNumFax"
                                    ValidationGroup="ValidateInfoUser"
                                    ControlToValidate="fax"
                                    ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                    Display="Dynamic"
                                    ErrorMessage="<%=str_replace('XXX', Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_FAX'))%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                    EnableClientScript="true"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_validateButton').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        </fieldset>
                        <!--END FAX-->

                    </div>
                </div>
                <!--END INFO ADRESSE-->
            </div>
        </div>
    </com:TPanel>
    <!--BEGIN MES COORDONNEES-->
</div>
<com:TLabel id="script"/>
<input type="hidden" id="moduleAvecAuth" value="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TelechargerDceAvecAuthentification'))?1:0%>"/>
<input type="hidden" id="moduleSansIden" value="<%=Application\Service\Atexo\Atexo_Module::isEnabled('TelechargerDceSansIdentification')%>"/>
<!--Fin partie centrale-->
