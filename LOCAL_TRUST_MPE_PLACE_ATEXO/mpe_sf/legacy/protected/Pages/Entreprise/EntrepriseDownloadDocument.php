<?php

namespace Application\Pages\Entreprise;

use App\Utils\Encryption;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Documents;

/**
 * Permet de télécharger le document du coffre fort.
 *
 * @author Mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadDocument extends DownloadFile
{
    public function onLoad($param)
    {
        $encrypt = Atexo_Util::getSfService(Encryption::class);
        $idDocument =  $encrypt->decryptId($_GET['id']);
        if ($idDocument) {
            $this->downloadDocument(Atexo_Util::atexoHtmlEntities($idDocument));
        }
    }

    public function downloadDocument($idDocument)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $document = (new Atexo_Entreprise_Documents())->retrieveDocumentById($idDocument, Atexo_CurrentUser::getIdEntreprise());
        if ($document) {
            $this->_idFichier = $document[0]->getJustificatif();
            $this->_nomFichier = $document[0]->getIntituleJustificatif();
            Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));

            $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        }

        exit;
    }
}
