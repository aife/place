<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Client\Client;

/**
 * Page UrlViadeo.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class UrlViadeo extends MpeTPage
{
    public function onLoad($param)
    {
        echo $this->getUrlViadeo(urlencode(Atexo_Util::atexoHtmlEntities($_GET['url']).'&mobile=out'));
        exit;
    }

    public function getUrlViadeo($url)
    {
        $ch = curl_init();

        $accessToken = Atexo_Config::getParameter('ACCESS_TOKEN_VIADEO'); //Param Client
        curl_setopt($ch, CURLOPT_URL, Atexo_Config::getParameter('URL_PARTAGE_VIADEO').$url.'&access_token='.$accessToken);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        curl_close($ch);

        $var = json_decode($result, null, 512, JSON_THROW_ON_ERROR);

        return $var->beans[0]->short_url;
    }
}
