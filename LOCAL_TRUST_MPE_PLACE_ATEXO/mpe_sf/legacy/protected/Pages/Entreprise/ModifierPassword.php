<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonTInformationModificationPassword;
use Application\Propel\Mpe\CommonTInformationModificationPasswordQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_Authentifier;
use Application\Service\Atexo\Atexo_Admin;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_HistorisationMotDePasse;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;


use App\Entity\Agent;
use App\Service\PradoPasswordEncoderInterface;
use App\Entity\Inscrit;

/**
 * permet de modifier le mot de passe.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class ModifierPassword extends MpeTPage
{
    /**
     * Permet d'initialiser la page.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onInit($param)
    {

        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['typeUser']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Permet de charger la page.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        if (!$this->isPostBack) {
            if (isset($_GET['updateSuccess'])) {
                $this->panelMessage->setVisible(true);
                $userLogin = $_GET['login'] ?? (new Atexo_CurrentUser())->getLoginUser();
                $this->panelConfigmation->setMessage(str_replace('{_login_}', $userLogin, Prado::Localize('MOT_DE_PASSE_MODIFIER_AVEC_SUCCES')));
                $this->panelConfigmation->setCssClass('alert alert-success msg-confirmation');
                $this->updateMdp->setVisible(false);
            } else {
                $typeUser = Atexo_Util::atexoHtmlEntities($_GET['typeUser']);
                $informationPsw = self::getInformationPswByEmail();
                if ($informationPsw instanceof CommonTInformationModificationPassword) {
                    if ($informationPsw->getDateFinValidite() < date('Y-m-d H:i:s')) {
                        $urlRedirect = self::getUrlRedirectionToMDPOublie($typeUser);
                        $this->response->redirect($urlRedirect);
                    }
                } else {
                    $urlRedirect = self::getUrlRedirectionToMDPOublie($typeUser);
                    $this->response->redirect($urlRedirect);
                }
            }
        }
        if (isset($_GET['expire'])) {
            $this->panelHistorisationInfo->setStyle('display:block');
            $this->panelTitleMotDePassExpire->setStyle('display:block');
            $this->panelTitleMotDePassOublie->setStyle('display:none');
        }
    }

    /**
     * Permet de verfifier si on va faire la validation du format du mot de passe.
     *
     * @return bool
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function validerFormatPassWord()
    {
        if (('entreprise' == $_GET['typeUser'] && Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))
            || ('agent' == $_GET['typeUser'] && Atexo_Module::isEnabled('AgentControleFormatMotDePasse'))) {
            return true;
        }

        return false;
    }

    /**
     * Permet de retourner les information du modification du mot de passe.
     *
     * @param string $email l'adresse mail
     *
     * @return Objet CommonTInformationModificationPassword
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getInformationPswByEmail($email = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeUser = Atexo_Util::atexoHtmlEntities($_GET['typeUser']);
        $jeton = Atexo_Util::atexoHtmlEntities($_GET['jeton']);
        $informationPswQuery = CommonTInformationModificationPasswordQuery::create()->filterByJeton($jeton)->filterByTypeUser($typeUser)->filterByModificationFaite('0');
        if ($email) {
            $informationPswQuery = $informationPswQuery->filterByEmail($email);
        }
        return $informationPswQuery->findOne($connexion);
    }

    /**
     * Permet de retourner l'url de redirection vers la page d'acceuil.
     *
     * @param string $typeUser type d'utilisateur
     *
     * @return string l'url de redirection
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlRedirectionToHome($typeUser)
    {
        if ('agent' == $typeUser) {
            return 'index.php?page=Agent.AgentHome';
        } elseif ('admin' == $typeUser) {
            return 'index.php?page=Administration.Home';
        } else {
            return 'index.php?page=Entreprise.EntrepriseHome';
        }
    }

    /**
     * Permet de retourner l'url de redirection vers la page du mot de passe oublie.
     *
     * @param string $typeUser type d'utilisateur
     *
     * @return string l'url de redirection
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlRedirectionToMDPOublie($typeUser)
    {
        if ('agent' == $typeUser) {
            return 'index.php?page=Agent.MotPasseOublie&updateFailed';
        } elseif ('admin' == $typeUser) {
            return 'index.php?page=Administration.AdminMotPasseOublie&updateFailed';
        } else {
            return 'index.php?page=Entreprise.EntrepriseMotPasseOublie&updateFailed';
        }
    }

    /**
     * Permet d'enregistrer le nouveau mot de passe.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function savePassword()
    {
        $connexion = null;
        if ($this->isValid) {
            if (Atexo_Module::isEnabled('entrepriseMotsDePasseHistorises') && $this->checkRecentPassword()) {
                $this->Page->divValidationSummary->addServerError(
                    Prado::localize(
                        'HISTORISATION_MESSAGE_ERROR',
                        ['nb' => Atexo_Module::retrieveModules()->getEntrepriseMotsDePasseHistorises()]
                    ),
                    false
                );
                $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

                return true;
            }

            try {
                $email = $this->emailPersonnel->SafeText;
                $typeUser = Atexo_Util::atexoHtmlEntities($_GET['typeUser']);
                $jeton = Atexo_Util::atexoHtmlEntities($_GET['jeton']);
                $pwd = $this->password->SafeText;
                $hashPwd = Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS') ? hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE').$pwd) : sha1($pwd);
                $typeHashPwd = Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS') ? 'SHA256' : 'SHA1';
                $informationPsw = self::getInformationPswByEmail($email);
                if ($informationPsw) {
                    if ('agent' == $typeUser) {
                        $agent = (new Atexo_Agent())->retrieveAgentByMail($email);
                        if ($agent instanceof CommonAgent) {
                            if ('' != $this->password->SafeText) {
                                $agent->setTentativesMdp('0');
                                $agent->setPassword($hashPwd);
                                $agent->setTypeHash($typeHashPwd);
                            }
                            $agent->setDateModification(date('Y-m-d H:i:s'));
                            $informationPsw->setModificationFaite('1');
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $connexion->beginTransaction();
                            $agent->save($connexion);
                            $informationPsw->save($connexion);
                            $connexion->commit();
                            $userLogin = $agent->getLogin();
                            $this->response->redirect('index.php?page=Entreprise.ModifierPassword&updateSuccess&typeUser='.$typeUser . '&login=' . $userLogin);
                        }
                    } elseif ('entreprise' == $typeUser) {
                        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($email);
                        if ($inscrit instanceof CommonInscrit) {
                            $userVo = new Atexo_UserVo();
                            $userVo->setLogin($inscrit->getLogin());
                            $userVo->setPassword($hashPwd);
                            $userVo->setType('entreprise');
                            $inscrit->setMdp($hashPwd);
                            $inscrit->setTypeHash($typeHashPwd);
                            $inscrit->setTentativesMdp('0');
                            $inscrit->setDateModification(date('Y-m-d H:i:s'));
                            $informationPsw->setModificationFaite('1');
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $connexion->beginTransaction();
                            $inscrit->Save($connexion);
                            $informationPsw->save($connexion);
                            $connexion->commit();
                            //@TODO in the next version
                            $action = Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE');
                            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
                            $email2 = $inscrit->getEmail();
                            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action, $inscrit2, $inscrit->getEmail(), $inscrit->getId());
                            $this->response->redirect('index.php?page=Entreprise.ModifierPassword&updateSuccess&typeUser='.$typeUser . '&login=' . $inscrit->getLogin());
                        }
                    } elseif ('admin' == $typeUser) {
                        $admin = (new Atexo_Admin())->retrieveAdminByMail($email);
                        if ($admin) {
                            $userVo = new Atexo_UserVo();
                            $userVo->setLogin($admin->getLogin());
                            $userVo->setPassword($pwd);
                            $userVo->setType('admin');
                            $admin->setMdp($pwd);
                            $admin->setTentativesMdp('0');
                            $informationPsw->setModificationFaite('1');
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $connexion->beginTransaction();
                            $admin->Save($connexion);
                            $informationPsw->save($connexion);
                            $connexion->commit();
                            $this->response->redirect('index.php?page=Entreprise.ModifierPassword&updateSuccess&typeUser='.$typeUser);

                        }
                    }
                }
            } catch (Exception $e) {
                $connexion->rollback();
                Prado::log('Erreur de modification du mot de passe : '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ModifierPassword.php');
            }
        }
    }

    /**
     * Peremet de valider l'email saisi.
     *
     * @param
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifyEmail($sender, $param)
    {
        $email = $this->emailPersonnel->SafeText;
        $informationPsw = self::getInformationPswByEmail($email);

        if (!$informationPsw) {
            $param->IsValid = false;
            $this->emailPersonnelValidator->ErrorMessage = Prado::localize('TEXT_ADRESSE_ELECTRONIQUE');
            $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
        } else {
            $this->script->text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
        }
    }

    private function checkRecentPassword()
    {
        $email = $this->emailPersonnel->SafeText;
        $nouveauMotDePasse = $this->password->SafeText;

        return (new Atexo_HistorisationMotDePasse())->checkRecentPassword($email, $nouveauMotDePasse);
    }
}
