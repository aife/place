<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AutresOutils extends MpeTPage
{
    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/aide/outils-informatiques';
        $this->response->redirect($url);
    }
}
