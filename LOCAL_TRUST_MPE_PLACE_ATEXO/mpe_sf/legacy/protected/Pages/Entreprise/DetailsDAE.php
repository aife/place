<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Class DetailsDAE.
 */
class DetailsDAE extends MpeTPage
{
    protected $connexion;
    protected $consultation;
    protected $contrat;
    public bool $_calledFrom = false;

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $messageErreur = null;
        $logger = Atexo_LoggerManager::getLogger('contrat');

        if (isset($_GET['idContrat'])) {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $this->contrat = $contratQuery->getTContratTitulaireById($idContrat, $this->connexion);

            if ($this->contrat instanceof CommonTContratTitulaire) {
                $commonDonneesAnnuellesConcessionQuery = new CommonDonneesAnnuellesConcessionQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $donneesAnnuellesConcessions = $commonDonneesAnnuellesConcessionQuery->getDonneesAnnuellesConcession($idContrat);
                $this->fillRepeater($donneesAnnuellesConcessions);
            } else {
                $logger->error("Le contrat dont l'id = ".$idContrat." n'existe pas");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }
        } else {
            $logger->error("Erreur d'accès à la page DetailsModificationsContrat sans le parametre idContrat");
            $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
        }
    }

    /**
     * @param $modifications
     */
    public function fillRepeater($donnees)
    {
        $nombreElement = is_countable($donnees) ? count($donnees) : 0;

        if ($nombreElement >= 1) {
            $this->nbrResultats->Text = $nombreElement;

            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);

            $this->setViewState('nombreElement', $nombreElement);

            $this->nombrePageTop->Text = ceil($nombreElement / $this->listeDonneeAnnuellesConcessions->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeDonneeAnnuellesConcessions->PageSize);

            $this->listeDonneeAnnuellesConcessions->setVirtualItemCount($nombreElement);
            $this->listeDonneeAnnuellesConcessions->setCurrentPageIndex(0);

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);

            $this->listeDonneeAnnuellesConcessions->DataSource = $donnees;
            $this->listeDonneeAnnuellesConcessions->DataBind();
        } else {
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
        }
    }

    /**
     * @param $idEtablissement
     *
     * @return Entreprise
     *
     * @throws PropelException
     */
    public function getNomAttributaire($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();

        return $entrepriseQuery->findOneById($etablissement->getIdEntreprise())->getNom();
    }

    /**
     * @param $idEtablissement
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getSiretEntreprise($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();
        $entreprise = $entrepriseQuery->findOneById($etablissement->getIdEntreprise());

        return $entreprise->getSiren().$etablissement->getCodeEtablissement();
    }
}
