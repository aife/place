<?php

namespace Application\Pages\Entreprise;

use App\Service\Routing\RouteBuilderInterface;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page de détail des évenements des administrateur de l'entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseEvenementsAdmin extends MpeTPage
{

    public function onInit($param)
    {

        // Redirect to SF Page, we do not use this Prado Page anymore
        $sfPath = Atexo_Util::getSfService(RouteBuilderInterface::class)
            ->getRoute('evenement_admin_entreprise');
        $this->response->redirect($sfPath); /**/
    }
}
