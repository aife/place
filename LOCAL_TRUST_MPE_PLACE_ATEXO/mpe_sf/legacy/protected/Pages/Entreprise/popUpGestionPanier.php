<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Prado\Prado;

/**
 * Classe: popUp d'ajout d'une consultation au panier.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class popUpGestionPanier extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if ('AddLot' == $_GET['action']) {
            $this->messageConfirmation->Text = Prado::localize('TEXT_CONSULTATIONS_SELECTINNEES_ENREGISTREES_DANS_PANIER');
            $this->messageConfirmation->visible = true;

            return;
        }
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation instanceof CommonConsultation) {
            $panierEntreprise = Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
            $this->messageConfirmation->visible = true;
            if ('ajout' == $_GET['action']) {
                if ($panierEntreprise instanceof CommonPanierEntreprise) {
                    $this->messageConfirmation->Text = str_replace('[__REF_UTILISATEUR_]', '<strong>'.$consultation->getReferenceUtilisateur().'</strong>', Prado::localize('MESSAGE_CONSULTATION_EXISTE_DEJA_DANS_VOTRE_PANIER'));
                } elseif (Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), base64_decode($_GET['id']), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit())) {
                    $this->messageConfirmation->Text = str_replace('[__REF_UTILISATEUR_]', '<strong>'.$consultation->getReferenceUtilisateur().'</strong>', Prado::localize('CONFIRMATION_ENREGISTREMENT_CONSULTATION_PANIER'));
                }
            } elseif ('suppression' == $_GET['action']) {
                if ($panierEntreprise instanceof CommonPanierEntreprise) {
                    $this->messageConfirmation->Text = str_replace('[__REF_UTILISATEUR_]', '<strong>'.$consultation->getReferenceUtilisateur().'</strong>', Prado::localize('MESSAGE_SUPPRESSION_CONSULTATION_PANIER'));
                }
            }
        }
    }

    public function supprimerConsultationPanier()
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation instanceof CommonConsultation) {
            $panierEntreprise = Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
            if ($panierEntreprise instanceof CommonPanierEntreprise) {
                if (Atexo_Entreprise_PaniersEntreprises::deletePanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit())) {
                    $script = "<script language='JavaScript' type='text/JavaScript'> window.close();window.opener.document.getElementById('ctl0_CONTENU_PAGE_afficherMonPanier').click(); </script>";
                    $this->javascript->Text = $script;
                }
            }
        }
    }
}
