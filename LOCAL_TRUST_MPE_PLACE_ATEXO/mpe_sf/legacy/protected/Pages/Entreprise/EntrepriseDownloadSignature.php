<?php

namespace Application\Pages\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger les signatures.
 *
 * @author mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadSignature extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $commonConnexon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_FICHIER, Atexo_Util::atexoHtmlEntities($_GET['fichier']));
        $c->add(CommonFichierEnveloppePeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $commonFichierEnveloppe = CommonFichierEnveloppePeer::doSelectOne($c, $commonConnexon);
        if ($commonFichierEnveloppe) {
            $cc = new Criteria();
            $cc->add(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO, $commonFichierEnveloppe->getIdEnveloppe());
            $cc->add(CommonEnveloppePeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            $commonEnveloppe = CommonEnveloppePeer::doSelectOne($cc, $commonConnexon);

            if ($commonEnveloppe) {
                $offre = $commonEnveloppe->getOffre($commonConnexon);
                if ($offre && $offre->getInscritId() == Atexo_CurrentUser::getIdInscrit()) {
                    Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
                    $content = base64_decode(trim($commonFichierEnveloppe->getSignatureFichier()));
                    static::downloadFileContent($commonFichierEnveloppe->getJetonFileName(), $content);
                } else {
                    exit;
                }
            }
        }
    }
}
